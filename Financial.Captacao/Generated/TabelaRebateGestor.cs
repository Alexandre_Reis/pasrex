/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:32:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		















using Financial.Common;











		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esTabelaRebateGestorCollection : esEntityCollection
	{
		public esTabelaRebateGestorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRebateGestorCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRebateGestorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRebateGestorQuery);
		}
		#endregion
		
		virtual public TabelaRebateGestor DetachEntity(TabelaRebateGestor entity)
		{
			return base.DetachEntity(entity) as TabelaRebateGestor;
		}
		
		virtual public TabelaRebateGestor AttachEntity(TabelaRebateGestor entity)
		{
			return base.AttachEntity(entity) as TabelaRebateGestor;
		}
		
		virtual public void Combine(TabelaRebateGestorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRebateGestor this[int index]
		{
			get
			{
				return base[index] as TabelaRebateGestor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRebateGestor);
		}
	}



	[Serializable]
	abstract public class esTabelaRebateGestor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRebateGestorQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRebateGestor()
		{

		}

		public esTabelaRebateGestor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idAgenteGestor, faixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idAgenteGestor, faixa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaRebateGestorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdAgenteGestor == idAgenteGestor, query.Faixa == faixa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idAgenteGestor, faixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idAgenteGestor, faixa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			esTabelaRebateGestorQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdAgenteGestor == idAgenteGestor, query.Faixa == faixa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdAgenteGestor",idAgenteGestor);			parms.Add("Faixa",faixa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdAgenteGestor": this.str.IdAgenteGestor = (string)value; break;							
						case "Faixa": this.str.Faixa = (string)value; break;							
						case "PercentualRebateAdministracao": this.str.PercentualRebateAdministracao = (string)value; break;							
						case "PercentualRebatePerformance": this.str.PercentualRebatePerformance = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdAgenteGestor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteGestor = (System.Int32?)value;
							break;
						
						case "Faixa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Faixa = (System.Decimal?)value;
							break;
						
						case "PercentualRebateAdministracao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebateAdministracao = (System.Decimal?)value;
							break;
						
						case "PercentualRebatePerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebatePerformance = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRebateGestor.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaRebateGestorMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRebateGestorMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateGestor.IdAgenteGestor
		/// </summary>
		virtual public System.Int32? IdAgenteGestor
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor, value))
				{
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateGestor.Faixa
		/// </summary>
		virtual public System.Decimal? Faixa
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.Faixa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.Faixa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateGestor.PercentualRebateAdministracao
		/// </summary>
		virtual public System.Decimal? PercentualRebateAdministracao
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.PercentualRebateAdministracao);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.PercentualRebateAdministracao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateGestor.PercentualRebatePerformance
		/// </summary>
		virtual public System.Decimal? PercentualRebatePerformance
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.PercentualRebatePerformance);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateGestorMetadata.ColumnNames.PercentualRebatePerformance, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteGestor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRebateGestor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdAgenteGestor
			{
				get
				{
					System.Int32? data = entity.IdAgenteGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteGestor = null;
					else entity.IdAgenteGestor = Convert.ToInt32(value);
				}
			}
				
			public System.String Faixa
			{
				get
				{
					System.Decimal? data = entity.Faixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Faixa = null;
					else entity.Faixa = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRebateAdministracao
			{
				get
				{
					System.Decimal? data = entity.PercentualRebateAdministracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebateAdministracao = null;
					else entity.PercentualRebateAdministracao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRebatePerformance
			{
				get
				{
					System.Decimal? data = entity.PercentualRebatePerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebatePerformance = null;
					else entity.PercentualRebatePerformance = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaRebateGestor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRebateGestorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRebateGestor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRebateGestor : esTabelaRebateGestor
	{

				
		#region UpToAgenteMercadoByIdAgenteGestor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TabelaRebateGestor_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteGestor
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteGestor == null
					&& IdAgenteGestor != null					)
				{
					this._UpToAgenteMercadoByIdAgenteGestor = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Where(this._UpToAgenteMercadoByIdAgenteGestor.Query.IdAgente == this.IdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteGestor;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteGestor");
				

				if(value == null)
				{
					this.IdAgenteGestor = null;
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
				else
				{
					this.IdAgenteGestor = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteGestor = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteGestor != null)
			{
				this.IdAgenteGestor = this._UpToAgenteMercadoByIdAgenteGestor.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRebateGestorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateGestorMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaRebateGestorMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdAgenteGestor
		{
			get
			{
				return new esQueryItem(this, TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Faixa
		{
			get
			{
				return new esQueryItem(this, TabelaRebateGestorMetadata.ColumnNames.Faixa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRebateAdministracao
		{
			get
			{
				return new esQueryItem(this, TabelaRebateGestorMetadata.ColumnNames.PercentualRebateAdministracao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRebatePerformance
		{
			get
			{
				return new esQueryItem(this, TabelaRebateGestorMetadata.ColumnNames.PercentualRebatePerformance, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRebateGestorCollection")]
	public partial class TabelaRebateGestorCollection : esTabelaRebateGestorCollection, IEnumerable<TabelaRebateGestor>
	{
		public TabelaRebateGestorCollection()
		{

		}
		
		public static implicit operator List<TabelaRebateGestor>(TabelaRebateGestorCollection coll)
		{
			List<TabelaRebateGestor> list = new List<TabelaRebateGestor>();
			
			foreach (TabelaRebateGestor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRebateGestorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateGestorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRebateGestor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRebateGestor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRebateGestorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateGestorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRebateGestorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRebateGestor AddNew()
		{
			TabelaRebateGestor entity = base.AddNewEntity() as TabelaRebateGestor;
			
			return entity;
		}

		public TabelaRebateGestor FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idAgenteGestor, System.Decimal faixa)
		{
			return base.FindByPrimaryKey(dataReferencia, idAgenteGestor, faixa) as TabelaRebateGestor;
		}


		#region IEnumerable<TabelaRebateGestor> Members

		IEnumerator<TabelaRebateGestor> IEnumerable<TabelaRebateGestor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRebateGestor;
			}
		}

		#endregion
		
		private TabelaRebateGestorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRebateGestor' table
	/// </summary>

	[Serializable]
	public partial class TabelaRebateGestor : esTabelaRebateGestor
	{
		public TabelaRebateGestor()
		{

		}
	
		public TabelaRebateGestor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateGestorMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRebateGestorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateGestorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRebateGestorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateGestorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRebateGestorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRebateGestorQuery query;
	}



	[Serializable]
	public partial class TabelaRebateGestorQuery : esTabelaRebateGestorQuery
	{
		public TabelaRebateGestorQuery()
		{

		}		
		
		public TabelaRebateGestorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRebateGestorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRebateGestorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRebateGestorMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRebateGestorMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateGestorMetadata.PropertyNames.IdAgenteGestor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateGestorMetadata.ColumnNames.Faixa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateGestorMetadata.PropertyNames.Faixa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateGestorMetadata.ColumnNames.PercentualRebateAdministracao, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateGestorMetadata.PropertyNames.PercentualRebateAdministracao;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateGestorMetadata.ColumnNames.PercentualRebatePerformance, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateGestorMetadata.PropertyNames.PercentualRebatePerformance;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRebateGestorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string Faixa = "Faixa";
			 public const string PercentualRebateAdministracao = "PercentualRebateAdministracao";
			 public const string PercentualRebatePerformance = "PercentualRebatePerformance";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string Faixa = "Faixa";
			 public const string PercentualRebateAdministracao = "PercentualRebateAdministracao";
			 public const string PercentualRebatePerformance = "PercentualRebatePerformance";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRebateGestorMetadata))
			{
				if(TabelaRebateGestorMetadata.mapDelegates == null)
				{
					TabelaRebateGestorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRebateGestorMetadata.meta == null)
				{
					TabelaRebateGestorMetadata.meta = new TabelaRebateGestorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdAgenteGestor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Faixa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRebateAdministracao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRebatePerformance", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaRebateGestor";
				meta.Destination = "TabelaRebateGestor";
				
				meta.spInsert = "proc_TabelaRebateGestorInsert";				
				meta.spUpdate = "proc_TabelaRebateGestorUpdate";		
				meta.spDelete = "proc_TabelaRebateGestorDelete";
				meta.spLoadAll = "proc_TabelaRebateGestorLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRebateGestorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRebateGestorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
