/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:32:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		














using Financial.Common;
using Financial.Investidor;












		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esTabelaRebateCorretagemCollection : esEntityCollection
	{
		public esTabelaRebateCorretagemCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRebateCorretagemCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRebateCorretagemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRebateCorretagemQuery);
		}
		#endregion
		
		virtual public TabelaRebateCorretagem DetachEntity(TabelaRebateCorretagem entity)
		{
			return base.DetachEntity(entity) as TabelaRebateCorretagem;
		}
		
		virtual public TabelaRebateCorretagem AttachEntity(TabelaRebateCorretagem entity)
		{
			return base.AttachEntity(entity) as TabelaRebateCorretagem;
		}
		
		virtual public void Combine(TabelaRebateCorretagemCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRebateCorretagem this[int index]
		{
			get
			{
				return base[index] as TabelaRebateCorretagem;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRebateCorretagem);
		}
	}



	[Serializable]
	abstract public class esTabelaRebateCorretagem : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRebateCorretagemQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRebateCorretagem()
		{

		}

		public esTabelaRebateCorretagem(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente, idAgenteCorretora);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente, idAgenteCorretora);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaRebateCorretagemQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdCliente == idCliente, query.IdAgenteCorretora == idAgenteCorretora);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente, idAgenteCorretora);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente, idAgenteCorretora);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			esTabelaRebateCorretagemQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdCliente == idCliente, query.IdAgenteCorretora == idAgenteCorretora);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdCliente",idCliente);			parms.Add("IdAgenteCorretora",idAgenteCorretora);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "PercentualRebate": this.str.PercentualRebate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "PercentualRebate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebate = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRebateCorretagem.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateCorretagem.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateCorretagem.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora, value))
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateCorretagem.PercentualRebate
		/// </summary>
		virtual public System.Decimal? PercentualRebate
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateCorretagemMetadata.ColumnNames.PercentualRebate);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateCorretagemMetadata.ColumnNames.PercentualRebate, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteCorretora;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRebateCorretagem entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String PercentualRebate
			{
				get
				{
					System.Decimal? data = entity.PercentualRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebate = null;
					else entity.PercentualRebate = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaRebateCorretagem entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRebateCorretagemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRebateCorretagem can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRebateCorretagem : esTabelaRebateCorretagem
	{

				
		#region UpToAgenteMercadoByIdAgenteCorretora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TabelaRebateCorretagem_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteCorretora
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteCorretora == null
					&& IdAgenteCorretora != null					)
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Where(this._UpToAgenteMercadoByIdAgenteCorretora.Query.IdAgente == this.IdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteCorretora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteCorretora");
				

				if(value == null)
				{
					this.IdAgenteCorretora = null;
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
				else
				{
					this.IdAgenteCorretora = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteCorretora = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TabelaRebateCorretagem_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteCorretora != null)
			{
				this.IdAgenteCorretora = this._UpToAgenteMercadoByIdAgenteCorretora.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRebateCorretagemQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateCorretagemMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TabelaRebateCorretagemMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PercentualRebate
		{
			get
			{
				return new esQueryItem(this, TabelaRebateCorretagemMetadata.ColumnNames.PercentualRebate, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRebateCorretagemCollection")]
	public partial class TabelaRebateCorretagemCollection : esTabelaRebateCorretagemCollection, IEnumerable<TabelaRebateCorretagem>
	{
		public TabelaRebateCorretagemCollection()
		{

		}
		
		public static implicit operator List<TabelaRebateCorretagem>(TabelaRebateCorretagemCollection coll)
		{
			List<TabelaRebateCorretagem> list = new List<TabelaRebateCorretagem>();
			
			foreach (TabelaRebateCorretagem emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRebateCorretagemMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateCorretagemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRebateCorretagem(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRebateCorretagem();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRebateCorretagemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateCorretagemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRebateCorretagemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRebateCorretagem AddNew()
		{
			TabelaRebateCorretagem entity = base.AddNewEntity() as TabelaRebateCorretagem;
			
			return entity;
		}

		public TabelaRebateCorretagem FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgenteCorretora)
		{
			return base.FindByPrimaryKey(dataReferencia, idCliente, idAgenteCorretora) as TabelaRebateCorretagem;
		}


		#region IEnumerable<TabelaRebateCorretagem> Members

		IEnumerator<TabelaRebateCorretagem> IEnumerable<TabelaRebateCorretagem>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRebateCorretagem;
			}
		}

		#endregion
		
		private TabelaRebateCorretagemQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRebateCorretagem' table
	/// </summary>

	[Serializable]
	public partial class TabelaRebateCorretagem : esTabelaRebateCorretagem
	{
		public TabelaRebateCorretagem()
		{

		}
	
		public TabelaRebateCorretagem(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateCorretagemMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRebateCorretagemQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateCorretagemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRebateCorretagemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateCorretagemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRebateCorretagemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRebateCorretagemQuery query;
	}



	[Serializable]
	public partial class TabelaRebateCorretagemQuery : esTabelaRebateCorretagemQuery
	{
		public TabelaRebateCorretagemQuery()
		{

		}		
		
		public TabelaRebateCorretagemQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRebateCorretagemMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRebateCorretagemMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRebateCorretagemMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRebateCorretagemMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateCorretagemMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateCorretagemMetadata.PropertyNames.IdAgenteCorretora;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateCorretagemMetadata.ColumnNames.PercentualRebate, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateCorretagemMetadata.PropertyNames.PercentualRebate;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRebateCorretagemMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string PercentualRebate = "PercentualRebate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string PercentualRebate = "PercentualRebate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRebateCorretagemMetadata))
			{
				if(TabelaRebateCorretagemMetadata.mapDelegates == null)
				{
					TabelaRebateCorretagemMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRebateCorretagemMetadata.meta == null)
				{
					TabelaRebateCorretagemMetadata.meta = new TabelaRebateCorretagemMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PercentualRebate", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaRebateCorretagem";
				meta.Destination = "TabelaRebateCorretagem";
				
				meta.spInsert = "proc_TabelaRebateCorretagemInsert";				
				meta.spUpdate = "proc_TabelaRebateCorretagemUpdate";		
				meta.spDelete = "proc_TabelaRebateCorretagemDelete";
				meta.spLoadAll = "proc_TabelaRebateCorretagemLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRebateCorretagemLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRebateCorretagemMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
