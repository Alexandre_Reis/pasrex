/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:32:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

















using Financial.Fundo;
using Financial.CRM;









		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esTabelaRebateOfficerCollection : esEntityCollection
	{
		public esTabelaRebateOfficerCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRebateOfficerCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRebateOfficerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRebateOfficerQuery);
		}
		#endregion
		
		virtual public TabelaRebateOfficer DetachEntity(TabelaRebateOfficer entity)
		{
			return base.DetachEntity(entity) as TabelaRebateOfficer;
		}
		
		virtual public TabelaRebateOfficer AttachEntity(TabelaRebateOfficer entity)
		{
			return base.AttachEntity(entity) as TabelaRebateOfficer;
		}
		
		virtual public void Combine(TabelaRebateOfficerCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRebateOfficer this[int index]
		{
			get
			{
				return base[index] as TabelaRebateOfficer;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRebateOfficer);
		}
	}



	[Serializable]
	abstract public class esTabelaRebateOfficer : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRebateOfficerQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRebateOfficer()
		{

		}

		public esTabelaRebateOfficer(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idOfficer, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idOfficer, idPessoa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaRebateOfficerQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdOfficer == idOfficer, query.IdPessoa == idPessoa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idOfficer, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idOfficer, idPessoa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			esTabelaRebateOfficerQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdOfficer == idOfficer, query.IdPessoa == idPessoa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdOfficer",idOfficer);			parms.Add("IdPessoa",idPessoa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdOfficer": this.str.IdOfficer = (string)value; break;							
						case "PercentualRebate": this.str.PercentualRebate = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdOfficer":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOfficer = (System.Int32?)value;
							break;
						
						case "PercentualRebate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebate = (System.Decimal?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRebateOfficer.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaRebateOfficerMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRebateOfficerMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateOfficer.IdOfficer
		/// </summary>
		virtual public System.Int32? IdOfficer
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer, value))
				{
					this._UpToOfficerByIdOfficer = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateOfficer.PercentualRebate
		/// </summary>
		virtual public System.Decimal? PercentualRebate
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateOfficerMetadata.ColumnNames.PercentualRebate);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateOfficerMetadata.ColumnNames.PercentualRebate, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateOfficer.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Officer _UpToOfficerByIdOfficer;
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRebateOfficer entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOfficer
			{
				get
				{
					System.Int32? data = entity.IdOfficer;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOfficer = null;
					else entity.IdOfficer = Convert.ToInt32(value);
				}
			}
				
			public System.String PercentualRebate
			{
				get
				{
					System.Decimal? data = entity.PercentualRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebate = null;
					else entity.PercentualRebate = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
			

			private esTabelaRebateOfficer entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRebateOfficerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRebateOfficer can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRebateOfficer : esTabelaRebateOfficer
	{

				
		#region UpToOfficerByIdOfficer - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Officer_TabelaRebateOfficer_FK1
		/// </summary>

		[XmlIgnore]
		public Officer UpToOfficerByIdOfficer
		{
			get
			{
				if(this._UpToOfficerByIdOfficer == null
					&& IdOfficer != null					)
				{
					this._UpToOfficerByIdOfficer = new Officer();
					this._UpToOfficerByIdOfficer.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOfficerByIdOfficer", this._UpToOfficerByIdOfficer);
					this._UpToOfficerByIdOfficer.Query.Where(this._UpToOfficerByIdOfficer.Query.IdOfficer == this.IdOfficer);
					this._UpToOfficerByIdOfficer.Query.Load();
				}

				return this._UpToOfficerByIdOfficer;
			}
			
			set
			{
				this.RemovePreSave("UpToOfficerByIdOfficer");
				

				if(value == null)
				{
					this.IdOfficer = null;
					this._UpToOfficerByIdOfficer = null;
				}
				else
				{
					this.IdOfficer = value.IdOfficer;
					this._UpToOfficerByIdOfficer = value;
					this.SetPreSave("UpToOfficerByIdOfficer", this._UpToOfficerByIdOfficer);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_TabelaRebateOfficer_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRebateOfficerQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateOfficerMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaRebateOfficerMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOfficer
		{
			get
			{
				return new esQueryItem(this, TabelaRebateOfficerMetadata.ColumnNames.IdOfficer, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PercentualRebate
		{
			get
			{
				return new esQueryItem(this, TabelaRebateOfficerMetadata.ColumnNames.PercentualRebate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, TabelaRebateOfficerMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRebateOfficerCollection")]
	public partial class TabelaRebateOfficerCollection : esTabelaRebateOfficerCollection, IEnumerable<TabelaRebateOfficer>
	{
		public TabelaRebateOfficerCollection()
		{

		}
		
		public static implicit operator List<TabelaRebateOfficer>(TabelaRebateOfficerCollection coll)
		{
			List<TabelaRebateOfficer> list = new List<TabelaRebateOfficer>();
			
			foreach (TabelaRebateOfficer emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRebateOfficerMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateOfficerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRebateOfficer(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRebateOfficer();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRebateOfficerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateOfficerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRebateOfficerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRebateOfficer AddNew()
		{
			TabelaRebateOfficer entity = base.AddNewEntity() as TabelaRebateOfficer;
			
			return entity;
		}

		public TabelaRebateOfficer FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idOfficer, System.Int32 idPessoa)
		{
			return base.FindByPrimaryKey(dataReferencia, idOfficer, idPessoa) as TabelaRebateOfficer;
		}


		#region IEnumerable<TabelaRebateOfficer> Members

		IEnumerator<TabelaRebateOfficer> IEnumerable<TabelaRebateOfficer>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRebateOfficer;
			}
		}

		#endregion
		
		private TabelaRebateOfficerQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRebateOfficer' table
	/// </summary>

	[Serializable]
	public partial class TabelaRebateOfficer : esTabelaRebateOfficer
	{
		public TabelaRebateOfficer()
		{

		}
	
		public TabelaRebateOfficer(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateOfficerMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRebateOfficerQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateOfficerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRebateOfficerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateOfficerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRebateOfficerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRebateOfficerQuery query;
	}



	[Serializable]
	public partial class TabelaRebateOfficerQuery : esTabelaRebateOfficerQuery
	{
		public TabelaRebateOfficerQuery()
		{

		}		
		
		public TabelaRebateOfficerQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRebateOfficerMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRebateOfficerMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRebateOfficerMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRebateOfficerMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateOfficerMetadata.PropertyNames.IdOfficer;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateOfficerMetadata.ColumnNames.PercentualRebate, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateOfficerMetadata.PropertyNames.PercentualRebate;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateOfficerMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRebateOfficerMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdOfficer = "IdOfficer";
			 public const string PercentualRebate = "PercentualRebate";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdOfficer = "IdOfficer";
			 public const string PercentualRebate = "PercentualRebate";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRebateOfficerMetadata))
			{
				if(TabelaRebateOfficerMetadata.mapDelegates == null)
				{
					TabelaRebateOfficerMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRebateOfficerMetadata.meta == null)
				{
					TabelaRebateOfficerMetadata.meta = new TabelaRebateOfficerMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOfficer", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PercentualRebate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaRebateOfficer";
				meta.Destination = "TabelaRebateOfficer";
				
				meta.spInsert = "proc_TabelaRebateOfficerInsert";				
				meta.spUpdate = "proc_TabelaRebateOfficerUpdate";		
				meta.spDelete = "proc_TabelaRebateOfficerDelete";
				meta.spLoadAll = "proc_TabelaRebateOfficerLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRebateOfficerLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRebateOfficerMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
