/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:32:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


















using Financial.Investidor;








		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esOfficerCollection : esEntityCollection
	{
		public esOfficerCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OfficerCollection";
		}

		#region Query Logic
		protected void InitQuery(esOfficerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOfficerQuery);
		}
		#endregion
		
		virtual public Officer DetachEntity(Officer entity)
		{
			return base.DetachEntity(entity) as Officer;
		}
		
		virtual public Officer AttachEntity(Officer entity)
		{
			return base.AttachEntity(entity) as Officer;
		}
		
		virtual public void Combine(OfficerCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Officer this[int index]
		{
			get
			{
				return base[index] as Officer;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Officer);
		}
	}



	[Serializable]
	abstract public class esOfficer : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOfficerQuery GetDynamicQuery()
		{
			return null;
		}

		public esOfficer()
		{

		}

		public esOfficer(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOfficer)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfficer);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfficer);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOfficer)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOfficerQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOfficer == idOfficer);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOfficer)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfficer);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfficer);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOfficer)
		{
			esOfficerQuery query = this.GetDynamicQuery();
			query.Where(query.IdOfficer == idOfficer);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOfficer)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOfficer",idOfficer);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOfficer": this.str.IdOfficer = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOfficer":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOfficer = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Officer.IdOfficer
		/// </summary>
		virtual public System.Int32? IdOfficer
		{
			get
			{
				return base.GetSystemInt32(OfficerMetadata.ColumnNames.IdOfficer);
			}
			
			set
			{
				base.SetSystemInt32(OfficerMetadata.ColumnNames.IdOfficer, value);
			}
		}
		
		/// <summary>
		/// Maps to Officer.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(OfficerMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(OfficerMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Officer.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(OfficerMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(OfficerMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to Officer.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(OfficerMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(OfficerMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOfficer entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOfficer
			{
				get
				{
					System.Int32? data = entity.IdOfficer;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOfficer = null;
					else entity.IdOfficer = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esOfficer entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOfficerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOfficer can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Officer : esOfficer
	{

				
		#region ClienteCollectionByIdOfficer - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Officer_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdOfficer
		{
			get
			{
				if(this._ClienteCollectionByIdOfficer == null)
				{
					this._ClienteCollectionByIdOfficer = new ClienteCollection();
					this._ClienteCollectionByIdOfficer.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdOfficer", this._ClienteCollectionByIdOfficer);
				
					if(this.IdOfficer != null)
					{
						this._ClienteCollectionByIdOfficer.Query.Where(this._ClienteCollectionByIdOfficer.Query.IdOfficer == this.IdOfficer);
						this._ClienteCollectionByIdOfficer.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdOfficer.fks.Add(ClienteMetadata.ColumnNames.IdOfficer, this.IdOfficer);
					}
				}

				return this._ClienteCollectionByIdOfficer;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdOfficer != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdOfficer"); 
					this._ClienteCollectionByIdOfficer = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdOfficer;
		#endregion

				
		#region TabelaRebateOfficerCollectionByIdOfficer - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Officer_TabelaRebateOfficer_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateOfficerCollection TabelaRebateOfficerCollectionByIdOfficer
		{
			get
			{
				if(this._TabelaRebateOfficerCollectionByIdOfficer == null)
				{
					this._TabelaRebateOfficerCollectionByIdOfficer = new TabelaRebateOfficerCollection();
					this._TabelaRebateOfficerCollectionByIdOfficer.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateOfficerCollectionByIdOfficer", this._TabelaRebateOfficerCollectionByIdOfficer);
				
					if(this.IdOfficer != null)
					{
						this._TabelaRebateOfficerCollectionByIdOfficer.Query.Where(this._TabelaRebateOfficerCollectionByIdOfficer.Query.IdOfficer == this.IdOfficer);
						this._TabelaRebateOfficerCollectionByIdOfficer.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateOfficerCollectionByIdOfficer.fks.Add(TabelaRebateOfficerMetadata.ColumnNames.IdOfficer, this.IdOfficer);
					}
				}

				return this._TabelaRebateOfficerCollectionByIdOfficer;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateOfficerCollectionByIdOfficer != null) 
				{ 
					this.RemovePostSave("TabelaRebateOfficerCollectionByIdOfficer"); 
					this._TabelaRebateOfficerCollectionByIdOfficer = null;
					
				} 
			} 			
		}

		private TabelaRebateOfficerCollection _TabelaRebateOfficerCollectionByIdOfficer;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdOfficer", typeof(ClienteCollection), new Cliente()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateOfficerCollectionByIdOfficer", typeof(TabelaRebateOfficerCollection), new TabelaRebateOfficer()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOfficerQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OfficerMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOfficer
		{
			get
			{
				return new esQueryItem(this, OfficerMetadata.ColumnNames.IdOfficer, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, OfficerMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, OfficerMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, OfficerMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OfficerCollection")]
	public partial class OfficerCollection : esOfficerCollection, IEnumerable<Officer>
	{
		public OfficerCollection()
		{

		}
		
		public static implicit operator List<Officer>(OfficerCollection coll)
		{
			List<Officer> list = new List<Officer>();
			
			foreach (Officer emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OfficerMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OfficerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Officer(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Officer();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OfficerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OfficerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OfficerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Officer AddNew()
		{
			Officer entity = base.AddNewEntity() as Officer;
			
			return entity;
		}

		public Officer FindByPrimaryKey(System.Int32 idOfficer)
		{
			return base.FindByPrimaryKey(idOfficer) as Officer;
		}


		#region IEnumerable<Officer> Members

		IEnumerator<Officer> IEnumerable<Officer>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Officer;
			}
		}

		#endregion
		
		private OfficerQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Officer' table
	/// </summary>

	[Serializable]
	public partial class Officer : esOfficer
	{
		public Officer()
		{

		}
	
		public Officer(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OfficerMetadata.Meta();
			}
		}
		
		
		
		override protected esOfficerQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OfficerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OfficerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OfficerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OfficerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OfficerQuery query;
	}



	[Serializable]
	public partial class OfficerQuery : esOfficerQuery
	{
		public OfficerQuery()
		{

		}		
		
		public OfficerQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OfficerMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OfficerMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OfficerMetadata.ColumnNames.IdOfficer, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OfficerMetadata.PropertyNames.IdOfficer;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfficerMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = OfficerMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfficerMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OfficerMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfficerMetadata.ColumnNames.Tipo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OfficerMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OfficerMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOfficer = "IdOfficer";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOfficer = "IdOfficer";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OfficerMetadata))
			{
				if(OfficerMetadata.mapDelegates == null)
				{
					OfficerMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OfficerMetadata.meta == null)
				{
					OfficerMetadata.meta = new OfficerMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOfficer", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "Officer";
				meta.Destination = "Officer";
				
				meta.spInsert = "proc_OfficerInsert";				
				meta.spUpdate = "proc_OfficerUpdate";		
				meta.spDelete = "proc_OfficerDelete";
				meta.spLoadAll = "proc_OfficerLoadAll";
				meta.spLoadByPrimaryKey = "proc_OfficerLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OfficerMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
