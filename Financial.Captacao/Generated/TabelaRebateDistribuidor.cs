/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:32:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		











using Financial.Common;
using Financial.Fundo;















		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esTabelaRebateDistribuidorCollection : esEntityCollection
	{
		public esTabelaRebateDistribuidorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRebateDistribuidorCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRebateDistribuidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRebateDistribuidorQuery);
		}
		#endregion
		
		virtual public TabelaRebateDistribuidor DetachEntity(TabelaRebateDistribuidor entity)
		{
			return base.DetachEntity(entity) as TabelaRebateDistribuidor;
		}
		
		virtual public TabelaRebateDistribuidor AttachEntity(TabelaRebateDistribuidor entity)
		{
			return base.AttachEntity(entity) as TabelaRebateDistribuidor;
		}
		
		virtual public void Combine(TabelaRebateDistribuidorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRebateDistribuidor this[int index]
		{
			get
			{
				return base[index] as TabelaRebateDistribuidor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRebateDistribuidor);
		}
	}



	[Serializable]
	abstract public class esTabelaRebateDistribuidor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRebateDistribuidorQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRebateDistribuidor()
		{

		}

		public esTabelaRebateDistribuidor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCarteira, idAgenteDistribuidor, faixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCarteira, idAgenteDistribuidor, faixa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaRebateDistribuidorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdCarteira == idCarteira, query.IdAgenteDistribuidor == idAgenteDistribuidor, query.Faixa == faixa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCarteira, idAgenteDistribuidor, faixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCarteira, idAgenteDistribuidor, faixa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			esTabelaRebateDistribuidorQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdCarteira == idCarteira, query.IdAgenteDistribuidor == idAgenteDistribuidor, query.Faixa == faixa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdCarteira",idCarteira);			parms.Add("IdAgenteDistribuidor",idAgenteDistribuidor);			parms.Add("Faixa",faixa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdAgenteDistribuidor": this.str.IdAgenteDistribuidor = (string)value; break;							
						case "Faixa": this.str.Faixa = (string)value; break;							
						case "PercentualRebateAdministracao": this.str.PercentualRebateAdministracao = (string)value; break;							
						case "PercentualRebatePerformance": this.str.PercentualRebatePerformance = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdAgenteDistribuidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteDistribuidor = (System.Int32?)value;
							break;
						
						case "Faixa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Faixa = (System.Decimal?)value;
							break;
						
						case "PercentualRebateAdministracao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebateAdministracao = (System.Decimal?)value;
							break;
						
						case "PercentualRebatePerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRebatePerformance = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.IdAgenteDistribuidor
		/// </summary>
		virtual public System.Int32? IdAgenteDistribuidor
		{
			get
			{
				return base.GetSystemInt32(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor, value))
				{
					this._UpToAgenteMercadoByIdAgenteDistribuidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.Faixa
		/// </summary>
		virtual public System.Decimal? Faixa
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.Faixa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.Faixa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.PercentualRebateAdministracao
		/// </summary>
		virtual public System.Decimal? PercentualRebateAdministracao
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebateAdministracao);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebateAdministracao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRebateDistribuidor.PercentualRebatePerformance
		/// </summary>
		virtual public System.Decimal? PercentualRebatePerformance
		{
			get
			{
				return base.GetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebatePerformance);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebatePerformance, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteDistribuidor;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRebateDistribuidor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteDistribuidor
			{
				get
				{
					System.Int32? data = entity.IdAgenteDistribuidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteDistribuidor = null;
					else entity.IdAgenteDistribuidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Faixa
			{
				get
				{
					System.Decimal? data = entity.Faixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Faixa = null;
					else entity.Faixa = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRebateAdministracao
			{
				get
				{
					System.Decimal? data = entity.PercentualRebateAdministracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebateAdministracao = null;
					else entity.PercentualRebateAdministracao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRebatePerformance
			{
				get
				{
					System.Decimal? data = entity.PercentualRebatePerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRebatePerformance = null;
					else entity.PercentualRebatePerformance = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaRebateDistribuidor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRebateDistribuidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRebateDistribuidor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRebateDistribuidor : esTabelaRebateDistribuidor
	{

				
		#region UpToAgenteMercadoByIdAgenteDistribuidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TabelaRebateDistribuidor_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteDistribuidor
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteDistribuidor == null
					&& IdAgenteDistribuidor != null					)
				{
					this._UpToAgenteMercadoByIdAgenteDistribuidor = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteDistribuidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDistribuidor", this._UpToAgenteMercadoByIdAgenteDistribuidor);
					this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.Where(this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.IdAgente == this.IdAgenteDistribuidor);
					this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteDistribuidor;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteDistribuidor");
				

				if(value == null)
				{
					this.IdAgenteDistribuidor = null;
					this._UpToAgenteMercadoByIdAgenteDistribuidor = null;
				}
				else
				{
					this.IdAgenteDistribuidor = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteDistribuidor = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDistribuidor", this._UpToAgenteMercadoByIdAgenteDistribuidor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_TabelaRebateDistribuidor_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteDistribuidor != null)
			{
				this.IdAgenteDistribuidor = this._UpToAgenteMercadoByIdAgenteDistribuidor.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRebateDistribuidorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateDistribuidorMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteDistribuidor
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Faixa
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.Faixa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRebateAdministracao
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebateAdministracao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRebatePerformance
		{
			get
			{
				return new esQueryItem(this, TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebatePerformance, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRebateDistribuidorCollection")]
	public partial class TabelaRebateDistribuidorCollection : esTabelaRebateDistribuidorCollection, IEnumerable<TabelaRebateDistribuidor>
	{
		public TabelaRebateDistribuidorCollection()
		{

		}
		
		public static implicit operator List<TabelaRebateDistribuidor>(TabelaRebateDistribuidorCollection coll)
		{
			List<TabelaRebateDistribuidor> list = new List<TabelaRebateDistribuidor>();
			
			foreach (TabelaRebateDistribuidor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRebateDistribuidorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateDistribuidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRebateDistribuidor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRebateDistribuidor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRebateDistribuidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateDistribuidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRebateDistribuidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRebateDistribuidor AddNew()
		{
			TabelaRebateDistribuidor entity = base.AddNewEntity() as TabelaRebateDistribuidor;
			
			return entity;
		}

		public TabelaRebateDistribuidor FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor, System.Decimal faixa)
		{
			return base.FindByPrimaryKey(dataReferencia, idCarteira, idAgenteDistribuidor, faixa) as TabelaRebateDistribuidor;
		}


		#region IEnumerable<TabelaRebateDistribuidor> Members

		IEnumerator<TabelaRebateDistribuidor> IEnumerable<TabelaRebateDistribuidor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRebateDistribuidor;
			}
		}

		#endregion
		
		private TabelaRebateDistribuidorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRebateDistribuidor' table
	/// </summary>

	[Serializable]
	public partial class TabelaRebateDistribuidor : esTabelaRebateDistribuidor
	{
		public TabelaRebateDistribuidor()
		{

		}
	
		public TabelaRebateDistribuidor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRebateDistribuidorMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRebateDistribuidorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRebateDistribuidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRebateDistribuidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRebateDistribuidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRebateDistribuidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRebateDistribuidorQuery query;
	}



	[Serializable]
	public partial class TabelaRebateDistribuidorQuery : esTabelaRebateDistribuidorQuery
	{
		public TabelaRebateDistribuidorQuery()
		{

		}		
		
		public TabelaRebateDistribuidorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRebateDistribuidorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRebateDistribuidorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.IdAgenteDistribuidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.Faixa, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.Faixa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebateAdministracao, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.PercentualRebateAdministracao;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRebateDistribuidorMetadata.ColumnNames.PercentualRebatePerformance, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRebateDistribuidorMetadata.PropertyNames.PercentualRebatePerformance;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRebateDistribuidorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string Faixa = "Faixa";
			 public const string PercentualRebateAdministracao = "PercentualRebateAdministracao";
			 public const string PercentualRebatePerformance = "PercentualRebatePerformance";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string Faixa = "Faixa";
			 public const string PercentualRebateAdministracao = "PercentualRebateAdministracao";
			 public const string PercentualRebatePerformance = "PercentualRebatePerformance";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRebateDistribuidorMetadata))
			{
				if(TabelaRebateDistribuidorMetadata.mapDelegates == null)
				{
					TabelaRebateDistribuidorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRebateDistribuidorMetadata.meta == null)
				{
					TabelaRebateDistribuidorMetadata.meta = new TabelaRebateDistribuidorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteDistribuidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Faixa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRebateAdministracao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRebatePerformance", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaRebateDistribuidor";
				meta.Destination = "TabelaRebateDistribuidor";
				
				meta.spInsert = "proc_TabelaRebateDistribuidorInsert";				
				meta.spUpdate = "proc_TabelaRebateDistribuidorUpdate";		
				meta.spDelete = "proc_TabelaRebateDistribuidorDelete";
				meta.spLoadAll = "proc_TabelaRebateDistribuidorLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRebateDistribuidorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRebateDistribuidorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
