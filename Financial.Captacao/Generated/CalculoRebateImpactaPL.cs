/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:45
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Investidor;



namespace Financial.Captacao
{

	[Serializable]
	abstract public class esCalculoRebateImpactaPLCollection : esEntityCollection
	{
		public esCalculoRebateImpactaPLCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoRebateImpactaPLCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoRebateImpactaPLQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoRebateImpactaPLQuery);
		}
		#endregion
		
		virtual public CalculoRebateImpactaPL DetachEntity(CalculoRebateImpactaPL entity)
		{
			return base.DetachEntity(entity) as CalculoRebateImpactaPL;
		}
		
		virtual public CalculoRebateImpactaPL AttachEntity(CalculoRebateImpactaPL entity)
		{
			return base.AttachEntity(entity) as CalculoRebateImpactaPL;
		}
		
		virtual public void Combine(CalculoRebateImpactaPLCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoRebateImpactaPL this[int index]
		{
			get
			{
				return base[index] as CalculoRebateImpactaPL;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoRebateImpactaPL);
		}
	}



	[Serializable]
	abstract public class esCalculoRebateImpactaPL : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoRebateImpactaPLQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoRebateImpactaPL()
		{

		}

		public esCalculoRebateImpactaPL(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataLancamento, System.Int32 idCarteira, System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataLancamento, idCarteira, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataLancamento, idCarteira, idCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataLancamento, System.Int32 idCarteira, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataLancamento, idCarteira, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataLancamento, idCarteira, idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataLancamento, System.Int32 idCarteira, System.Int32 idCliente)
		{
			esCalculoRebateImpactaPLQuery query = this.GetDynamicQuery();
			query.Where(query.DataLancamento == dataLancamento, query.IdCarteira == idCarteira, query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataLancamento, System.Int32 idCarteira, System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("DataLancamento",dataLancamento);			parms.Add("IdCarteira",idCarteira);			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateImpactaPLMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoRebateImpactaPLMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateImpactaPLMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoRebateImpactaPLMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoRebateImpactaPLMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoRebateImpactaPLMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoRebateImpactaPLMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoRebateImpactaPLMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateImpactaPLMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateImpactaPLMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateImpactaPL.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateImpactaPLMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateImpactaPLMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoRebateImpactaPL entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoRebateImpactaPL entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoRebateImpactaPLQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoRebateImpactaPL can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoRebateImpactaPL : esCalculoRebateImpactaPL
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CalculoRebateImpactaPL_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CalculoRebateImpactaPL_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoRebateImpactaPLQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateImpactaPLMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoRebateImpactaPLMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoRebateImpactaPLCollection")]
	public partial class CalculoRebateImpactaPLCollection : esCalculoRebateImpactaPLCollection, IEnumerable<CalculoRebateImpactaPL>
	{
		public CalculoRebateImpactaPLCollection()
		{

		}
		
		public static implicit operator List<CalculoRebateImpactaPL>(CalculoRebateImpactaPLCollection coll)
		{
			List<CalculoRebateImpactaPL> list = new List<CalculoRebateImpactaPL>();
			
			foreach (CalculoRebateImpactaPL emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoRebateImpactaPLMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateImpactaPLQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoRebateImpactaPL(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoRebateImpactaPL();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoRebateImpactaPLQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateImpactaPLQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoRebateImpactaPLQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoRebateImpactaPL AddNew()
		{
			CalculoRebateImpactaPL entity = base.AddNewEntity() as CalculoRebateImpactaPL;
			
			return entity;
		}

		public CalculoRebateImpactaPL FindByPrimaryKey(System.DateTime dataLancamento, System.Int32 idCarteira, System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(dataLancamento, idCarteira, idCliente) as CalculoRebateImpactaPL;
		}


		#region IEnumerable<CalculoRebateImpactaPL> Members

		IEnumerator<CalculoRebateImpactaPL> IEnumerable<CalculoRebateImpactaPL>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoRebateImpactaPL;
			}
		}

		#endregion
		
		private CalculoRebateImpactaPLQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoRebateImpactaPL' table
	/// </summary>

	[Serializable]
	public partial class CalculoRebateImpactaPL : esCalculoRebateImpactaPL
	{
		public CalculoRebateImpactaPL()
		{

		}
	
		public CalculoRebateImpactaPL(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateImpactaPLMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoRebateImpactaPLQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateImpactaPLQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoRebateImpactaPLQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateImpactaPLQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoRebateImpactaPLQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoRebateImpactaPLQuery query;
	}



	[Serializable]
	public partial class CalculoRebateImpactaPLQuery : esCalculoRebateImpactaPLQuery
	{
		public CalculoRebateImpactaPLQuery()
		{

		}		
		
		public CalculoRebateImpactaPLQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoRebateImpactaPLMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoRebateImpactaPLMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.DataLancamento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.DataPagamento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.ValorDia, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateImpactaPLMetadata.ColumnNames.ValorAcumulado, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateImpactaPLMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoRebateImpactaPLMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoRebateImpactaPLMetadata))
			{
				if(CalculoRebateImpactaPLMetadata.mapDelegates == null)
				{
					CalculoRebateImpactaPLMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoRebateImpactaPLMetadata.meta == null)
				{
					CalculoRebateImpactaPLMetadata.meta = new CalculoRebateImpactaPLMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoRebateImpactaPL";
				meta.Destination = "CalculoRebateImpactaPL";
				
				meta.spInsert = "proc_CalculoRebateImpactaPLInsert";				
				meta.spUpdate = "proc_CalculoRebateImpactaPLUpdate";		
				meta.spDelete = "proc_CalculoRebateImpactaPLDelete";
				meta.spLoadAll = "proc_CalculoRebateImpactaPLLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoRebateImpactaPLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoRebateImpactaPLMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
