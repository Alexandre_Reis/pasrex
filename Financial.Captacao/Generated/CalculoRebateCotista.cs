/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:31:59
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
















using Financial.Fundo;
using Financial.InvestidorCotista;










		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esCalculoRebateCotistaCollection : esEntityCollection
	{
		public esCalculoRebateCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoRebateCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoRebateCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoRebateCotistaQuery);
		}
		#endregion
		
		virtual public CalculoRebateCotista DetachEntity(CalculoRebateCotista entity)
		{
			return base.DetachEntity(entity) as CalculoRebateCotista;
		}
		
		virtual public CalculoRebateCotista AttachEntity(CalculoRebateCotista entity)
		{
			return base.AttachEntity(entity) as CalculoRebateCotista;
		}
		
		virtual public void Combine(CalculoRebateCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoRebateCotista this[int index]
		{
			get
			{
				return base[index] as CalculoRebateCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoRebateCotista);
		}
	}



	[Serializable]
	abstract public class esCalculoRebateCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoRebateCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoRebateCotista()
		{

		}

		public esCalculoRebateCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataCalculo, idCarteira, idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(dataCalculo, idCarteira, idCotista);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoRebateCotistaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataCalculo == dataCalculo, query.IdCarteira == idCarteira, query.IdCotista == idCotista);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataCalculo, idCarteira, idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(dataCalculo, idCarteira, idCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			esCalculoRebateCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.DataCalculo == dataCalculo, query.IdCarteira == idCarteira, query.IdCotista == idCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("DataCalculo",dataCalculo);			parms.Add("IdCarteira",idCarteira);			parms.Add("IdCotista",idCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataCalculo": this.str.DataCalculo = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "RebateAdministracaoDia": this.str.RebateAdministracaoDia = (string)value; break;							
						case "RebateAdministracaoAcumulado": this.str.RebateAdministracaoAcumulado = (string)value; break;							
						case "RebatePerformanceDia": this.str.RebatePerformanceDia = (string)value; break;							
						case "RebatePerformanceAcumulado": this.str.RebatePerformanceAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataCalculo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCalculo = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "RebateAdministracaoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebateAdministracaoDia = (System.Decimal?)value;
							break;
						
						case "RebateAdministracaoAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebateAdministracaoAcumulado = (System.Decimal?)value;
							break;
						
						case "RebatePerformanceDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebatePerformanceDia = (System.Decimal?)value;
							break;
						
						case "RebatePerformanceAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebatePerformanceAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoRebateCotista.DataCalculo
		/// </summary>
		virtual public System.DateTime? DataCalculo
		{
			get
			{
				return base.GetSystemDateTime(CalculoRebateCotistaMetadata.ColumnNames.DataCalculo);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoRebateCotistaMetadata.ColumnNames.DataCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoRebateCotistaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateCotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(CalculoRebateCotistaMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.RebateAdministracaoDia
		/// </summary>
		virtual public System.Decimal? RebateAdministracaoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.RebateAdministracaoAcumulado
		/// </summary>
		virtual public System.Decimal? RebateAdministracaoAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.RebatePerformanceDia
		/// </summary>
		virtual public System.Decimal? RebatePerformanceDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCotista.RebatePerformanceAcumulado
		/// </summary>
		virtual public System.Decimal? RebatePerformanceAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoRebateCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataCalculo
			{
				get
				{
					System.DateTime? data = entity.DataCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCalculo = null;
					else entity.DataCalculo = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String RebateAdministracaoDia
			{
				get
				{
					System.Decimal? data = entity.RebateAdministracaoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateAdministracaoDia = null;
					else entity.RebateAdministracaoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebateAdministracaoAcumulado
			{
				get
				{
					System.Decimal? data = entity.RebateAdministracaoAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateAdministracaoAcumulado = null;
					else entity.RebateAdministracaoAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebatePerformanceDia
			{
				get
				{
					System.Decimal? data = entity.RebatePerformanceDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebatePerformanceDia = null;
					else entity.RebatePerformanceDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebatePerformanceAcumulado
			{
				get
				{
					System.Decimal? data = entity.RebatePerformanceAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebatePerformanceAcumulado = null;
					else entity.RebatePerformanceAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoRebateCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoRebateCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoRebateCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoRebateCotista : esCalculoRebateCotista
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoRebateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoRebateCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataCalculo
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.DataCalculo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RebateAdministracaoDia
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebateAdministracaoAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebatePerformanceDia
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebatePerformanceAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoRebateCotistaCollection")]
	public partial class CalculoRebateCotistaCollection : esCalculoRebateCotistaCollection, IEnumerable<CalculoRebateCotista>
	{
		public CalculoRebateCotistaCollection()
		{

		}
		
		public static implicit operator List<CalculoRebateCotista>(CalculoRebateCotistaCollection coll)
		{
			List<CalculoRebateCotista> list = new List<CalculoRebateCotista>();
			
			foreach (CalculoRebateCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoRebateCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoRebateCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoRebateCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoRebateCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoRebateCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoRebateCotista AddNew()
		{
			CalculoRebateCotista entity = base.AddNewEntity() as CalculoRebateCotista;
			
			return entity;
		}

		public CalculoRebateCotista FindByPrimaryKey(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idCotista)
		{
			return base.FindByPrimaryKey(dataCalculo, idCarteira, idCotista) as CalculoRebateCotista;
		}


		#region IEnumerable<CalculoRebateCotista> Members

		IEnumerator<CalculoRebateCotista> IEnumerable<CalculoRebateCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoRebateCotista;
			}
		}

		#endregion
		
		private CalculoRebateCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoRebateCotista' table
	/// </summary>

	[Serializable]
	public partial class CalculoRebateCotista : esCalculoRebateCotista
	{
		public CalculoRebateCotista()
		{

		}
	
		public CalculoRebateCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoRebateCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoRebateCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoRebateCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoRebateCotistaQuery query;
	}



	[Serializable]
	public partial class CalculoRebateCotistaQuery : esCalculoRebateCotistaQuery
	{
		public CalculoRebateCotistaQuery()
		{

		}		
		
		public CalculoRebateCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoRebateCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoRebateCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.DataCalculo, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.DataCalculo;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.IdCotista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoDia, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.RebateAdministracaoDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.RebateAdministracaoAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.RebateAdministracaoAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceDia, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.RebatePerformanceDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCotistaMetadata.ColumnNames.RebatePerformanceAcumulado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCotistaMetadata.PropertyNames.RebatePerformanceAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoRebateCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataCalculo = "DataCalculo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotista = "IdCotista";
			 public const string RebateAdministracaoDia = "RebateAdministracaoDia";
			 public const string RebateAdministracaoAcumulado = "RebateAdministracaoAcumulado";
			 public const string RebatePerformanceDia = "RebatePerformanceDia";
			 public const string RebatePerformanceAcumulado = "RebatePerformanceAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataCalculo = "DataCalculo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotista = "IdCotista";
			 public const string RebateAdministracaoDia = "RebateAdministracaoDia";
			 public const string RebateAdministracaoAcumulado = "RebateAdministracaoAcumulado";
			 public const string RebatePerformanceDia = "RebatePerformanceDia";
			 public const string RebatePerformanceAcumulado = "RebatePerformanceAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoRebateCotistaMetadata))
			{
				if(CalculoRebateCotistaMetadata.mapDelegates == null)
				{
					CalculoRebateCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoRebateCotistaMetadata.meta == null)
				{
					CalculoRebateCotistaMetadata.meta = new CalculoRebateCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataCalculo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RebateAdministracaoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebateAdministracaoAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebatePerformanceDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebatePerformanceAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoRebateCotista";
				meta.Destination = "CalculoRebateCotista";
				
				meta.spInsert = "proc_CalculoRebateCotistaInsert";				
				meta.spUpdate = "proc_CalculoRebateCotistaUpdate";		
				meta.spDelete = "proc_CalculoRebateCotistaDelete";
				meta.spLoadAll = "proc_CalculoRebateCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoRebateCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoRebateCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
