/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2012 18:31:58
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		










using Financial.Common;
using Financial.Fundo;
















		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Captacao
{

	[Serializable]
	abstract public class esCalculoRebateCarteiraCollection : esEntityCollection
	{
		public esCalculoRebateCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoRebateCarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoRebateCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoRebateCarteiraQuery);
		}
		#endregion
		
		virtual public CalculoRebateCarteira DetachEntity(CalculoRebateCarteira entity)
		{
			return base.DetachEntity(entity) as CalculoRebateCarteira;
		}
		
		virtual public CalculoRebateCarteira AttachEntity(CalculoRebateCarteira entity)
		{
			return base.AttachEntity(entity) as CalculoRebateCarteira;
		}
		
		virtual public void Combine(CalculoRebateCarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoRebateCarteira this[int index]
		{
			get
			{
				return base[index] as CalculoRebateCarteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoRebateCarteira);
		}
	}



	[Serializable]
	abstract public class esCalculoRebateCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoRebateCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoRebateCarteira()
		{

		}

		public esCalculoRebateCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataCalculo, idCarteira, idAgenteDistribuidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataCalculo, idCarteira, idAgenteDistribuidor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoRebateCarteiraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataCalculo == dataCalculo, query.IdCarteira == idCarteira, query.IdAgenteDistribuidor == idAgenteDistribuidor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataCalculo, idCarteira, idAgenteDistribuidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataCalculo, idCarteira, idAgenteDistribuidor);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			esCalculoRebateCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.DataCalculo == dataCalculo, query.IdCarteira == idCarteira, query.IdAgenteDistribuidor == idAgenteDistribuidor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			esParameters parms = new esParameters();
			parms.Add("DataCalculo",dataCalculo);			parms.Add("IdCarteira",idCarteira);			parms.Add("IdAgenteDistribuidor",idAgenteDistribuidor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataCalculo": this.str.DataCalculo = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdAgenteDistribuidor": this.str.IdAgenteDistribuidor = (string)value; break;							
						case "RebateAdministracaoDia": this.str.RebateAdministracaoDia = (string)value; break;							
						case "RebateAdministracaoAcumulado": this.str.RebateAdministracaoAcumulado = (string)value; break;							
						case "RebatePerformanceDia": this.str.RebatePerformanceDia = (string)value; break;							
						case "RebatePerformanceAcumulado": this.str.RebatePerformanceAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataCalculo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCalculo = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdAgenteDistribuidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteDistribuidor = (System.Int32?)value;
							break;
						
						case "RebateAdministracaoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebateAdministracaoDia = (System.Decimal?)value;
							break;
						
						case "RebateAdministracaoAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebateAdministracaoAcumulado = (System.Decimal?)value;
							break;
						
						case "RebatePerformanceDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebatePerformanceDia = (System.Decimal?)value;
							break;
						
						case "RebatePerformanceAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RebatePerformanceAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.DataCalculo
		/// </summary>
		virtual public System.DateTime? DataCalculo
		{
			get
			{
				return base.GetSystemDateTime(CalculoRebateCarteiraMetadata.ColumnNames.DataCalculo);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoRebateCarteiraMetadata.ColumnNames.DataCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateCarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoRebateCarteiraMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.IdAgenteDistribuidor
		/// </summary>
		virtual public System.Int32? IdAgenteDistribuidor
		{
			get
			{
				return base.GetSystemInt32(CalculoRebateCarteiraMetadata.ColumnNames.IdAgenteDistribuidor);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoRebateCarteiraMetadata.ColumnNames.IdAgenteDistribuidor, value))
				{
					this._UpToAgenteMercadoByIdAgenteDistribuidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.RebateAdministracaoDia
		/// </summary>
		virtual public System.Decimal? RebateAdministracaoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.RebateAdministracaoAcumulado
		/// </summary>
		virtual public System.Decimal? RebateAdministracaoAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.RebatePerformanceDia
		/// </summary>
		virtual public System.Decimal? RebatePerformanceDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoRebateCarteira.RebatePerformanceAcumulado
		/// </summary>
		virtual public System.Decimal? RebatePerformanceAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteDistribuidor;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoRebateCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataCalculo
			{
				get
				{
					System.DateTime? data = entity.DataCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCalculo = null;
					else entity.DataCalculo = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteDistribuidor
			{
				get
				{
					System.Int32? data = entity.IdAgenteDistribuidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteDistribuidor = null;
					else entity.IdAgenteDistribuidor = Convert.ToInt32(value);
				}
			}
				
			public System.String RebateAdministracaoDia
			{
				get
				{
					System.Decimal? data = entity.RebateAdministracaoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateAdministracaoDia = null;
					else entity.RebateAdministracaoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebateAdministracaoAcumulado
			{
				get
				{
					System.Decimal? data = entity.RebateAdministracaoAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateAdministracaoAcumulado = null;
					else entity.RebateAdministracaoAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebatePerformanceDia
			{
				get
				{
					System.Decimal? data = entity.RebatePerformanceDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebatePerformanceDia = null;
					else entity.RebatePerformanceDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String RebatePerformanceAcumulado
			{
				get
				{
					System.Decimal? data = entity.RebatePerformanceAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebatePerformanceAcumulado = null;
					else entity.RebatePerformanceAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoRebateCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoRebateCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoRebateCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoRebateCarteira : esCalculoRebateCarteira
	{

				
		#region UpToAgenteMercadoByIdAgenteDistribuidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_CalculoRebateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteDistribuidor
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteDistribuidor == null
					&& IdAgenteDistribuidor != null					)
				{
					this._UpToAgenteMercadoByIdAgenteDistribuidor = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteDistribuidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDistribuidor", this._UpToAgenteMercadoByIdAgenteDistribuidor);
					this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.Where(this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.IdAgente == this.IdAgenteDistribuidor);
					this._UpToAgenteMercadoByIdAgenteDistribuidor.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteDistribuidor;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteDistribuidor");
				

				if(value == null)
				{
					this.IdAgenteDistribuidor = null;
					this._UpToAgenteMercadoByIdAgenteDistribuidor = null;
				}
				else
				{
					this.IdAgenteDistribuidor = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteDistribuidor = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDistribuidor", this._UpToAgenteMercadoByIdAgenteDistribuidor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoRebateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteDistribuidor != null)
			{
				this.IdAgenteDistribuidor = this._UpToAgenteMercadoByIdAgenteDistribuidor.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoRebateCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateCarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem DataCalculo
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.DataCalculo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteDistribuidor
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.IdAgenteDistribuidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RebateAdministracaoDia
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebateAdministracaoAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebatePerformanceDia
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RebatePerformanceAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoRebateCarteiraCollection")]
	public partial class CalculoRebateCarteiraCollection : esCalculoRebateCarteiraCollection, IEnumerable<CalculoRebateCarteira>
	{
		public CalculoRebateCarteiraCollection()
		{

		}
		
		public static implicit operator List<CalculoRebateCarteira>(CalculoRebateCarteiraCollection coll)
		{
			List<CalculoRebateCarteira> list = new List<CalculoRebateCarteira>();
			
			foreach (CalculoRebateCarteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoRebateCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoRebateCarteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoRebateCarteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoRebateCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoRebateCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoRebateCarteira AddNew()
		{
			CalculoRebateCarteira entity = base.AddNewEntity() as CalculoRebateCarteira;
			
			return entity;
		}

		public CalculoRebateCarteira FindByPrimaryKey(System.DateTime dataCalculo, System.Int32 idCarteira, System.Int32 idAgenteDistribuidor)
		{
			return base.FindByPrimaryKey(dataCalculo, idCarteira, idAgenteDistribuidor) as CalculoRebateCarteira;
		}


		#region IEnumerable<CalculoRebateCarteira> Members

		IEnumerator<CalculoRebateCarteira> IEnumerable<CalculoRebateCarteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoRebateCarteira;
			}
		}

		#endregion
		
		private CalculoRebateCarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoRebateCarteira' table
	/// </summary>

	[Serializable]
	public partial class CalculoRebateCarteira : esCalculoRebateCarteira
	{
		public CalculoRebateCarteira()
		{

		}
	
		public CalculoRebateCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoRebateCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoRebateCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoRebateCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoRebateCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoRebateCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoRebateCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoRebateCarteiraQuery query;
	}



	[Serializable]
	public partial class CalculoRebateCarteiraQuery : esCalculoRebateCarteiraQuery
	{
		public CalculoRebateCarteiraQuery()
		{

		}		
		
		public CalculoRebateCarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoRebateCarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoRebateCarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.DataCalculo, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.DataCalculo;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.IdAgenteDistribuidor, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.IdAgenteDistribuidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoDia, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.RebateAdministracaoDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.RebateAdministracaoAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.RebateAdministracaoAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceDia, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.RebatePerformanceDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoRebateCarteiraMetadata.ColumnNames.RebatePerformanceAcumulado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoRebateCarteiraMetadata.PropertyNames.RebatePerformanceAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoRebateCarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataCalculo = "DataCalculo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string RebateAdministracaoDia = "RebateAdministracaoDia";
			 public const string RebateAdministracaoAcumulado = "RebateAdministracaoAcumulado";
			 public const string RebatePerformanceDia = "RebatePerformanceDia";
			 public const string RebatePerformanceAcumulado = "RebatePerformanceAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataCalculo = "DataCalculo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string RebateAdministracaoDia = "RebateAdministracaoDia";
			 public const string RebateAdministracaoAcumulado = "RebateAdministracaoAcumulado";
			 public const string RebatePerformanceDia = "RebatePerformanceDia";
			 public const string RebatePerformanceAcumulado = "RebatePerformanceAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoRebateCarteiraMetadata))
			{
				if(CalculoRebateCarteiraMetadata.mapDelegates == null)
				{
					CalculoRebateCarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoRebateCarteiraMetadata.meta == null)
				{
					CalculoRebateCarteiraMetadata.meta = new CalculoRebateCarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataCalculo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteDistribuidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RebateAdministracaoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebateAdministracaoAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebatePerformanceDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RebatePerformanceAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoRebateCarteira";
				meta.Destination = "CalculoRebateCarteira";
				
				meta.spInsert = "proc_CalculoRebateCarteiraInsert";				
				meta.spUpdate = "proc_CalculoRebateCarteiraUpdate";		
				meta.spDelete = "proc_CalculoRebateCarteiraDelete";
				meta.spLoadAll = "proc_CalculoRebateCarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoRebateCarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoRebateCarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
