﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/06/2009 07:44:19
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao
{
	public partial class TabelaRebateOfficerCollection : esTabelaRebateOfficerCollection
	{
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Procura Elemento na Collection por idCarteira e idOfficer
        /// Operação feita na memoria 
        /// </summary>
        /// <param name="t">Objeto a comparar</param>
        /// <returns>Booleano indicando se existe Objeto na Collection
        /// Comparação é feita por idCarteira e idOfficer
        /// </returns>
        public bool Contains(TabelaRebateOfficer t) {           
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                TabelaRebateOfficer tabelaRebateOfficer = this[i];
                if (tabelaRebateOfficer.IdPessoa.Value == t.IdPessoa.Value &&
                    tabelaRebateOfficer.IdOfficer.Value == t.IdOfficer.Value) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Procura Elemento na Collection por idCarteira e idOfficer
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idOfficer"></param>
        /// <returns>Indice do Primeiro Objeto TabelaRebateOfficer que Satisfaz os Critérios 
        ///          de Busca ou -1 se não Achar</returns>
        /// </returns>
        public int IndexOf(int idPessoa, int idOfficer) {
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                TabelaRebateOfficer tabelaRebateOfficer = this[i];
                if (tabelaRebateOfficer.IdPessoa.Value == idPessoa &&
                    tabelaRebateOfficer.IdOfficer.Value == idOfficer) {
                    return i;
                }
            }

            return -1;
        }
	}
}
