﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao {
    public partial class CalculoRebateCarteiraCollection : esCalculoRebateCarteiraCollection {
        
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && ! this.Table.Columns.Contains(columnName) ) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Procura elemento na collection por idCarteira e idAgente
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idAgente"></param>
        /// <returns>Primeiro Objeto CalculoRebateCarteira que Satisfaz os Critérios de Busca ou Null se não Achar</returns>
        public CalculoRebateCarteira FindByValues(int idCarteira, int idAgente) {
            
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                CalculoRebateCarteira c = this[i];
                if (c.IdCarteira == idCarteira && c.IdAgenteDistribuidor == idAgente) {
                    return c;
                }
            }

            return null;
        }

        /// <summary>
        /// Procura Elemento na Collection Por idCarteira e idAgente
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idAgente"></param>
        /// <returns>Indice do Primeiro Objeto CalculoRebateCarteira que Satisfaz os Critérios 
        ///          de Busca ou -1 se não Achar</returns>
        public int IndexOf(int idCarteira, int idAgente) {
            
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                CalculoRebateCarteira c = this[i];
                if (c.IdCarteira == idCarteira && c.IdAgenteDistribuidor == idAgente) {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Carrega o objeto CalculoRebateCarteiraCollection com todos os campos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns>Indica se trouxe registros</returns>
        public bool BuscaValoresRebate(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.DataCalculo.Equal(data));

            return this.Query.Load();
        }

        /// <summary>
        /// Deleta baseado no idCarteira e data passados.
        /// Filtra por DataCalculo.GreaterThanOrEqual(data).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns>Indica se trouxe registros</returns>
        public void DeletaValoresRebate(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.DataCalculo.GreaterThanOrEqual(data));

            this.Query.Load();
            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}