﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo.Enums;
using Financial.Captacao.Enums;
using Financial.Util;
using Financial.CRM;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;

namespace Financial.Captacao
{
    /// <summary>
    /// Classe exclusivamente voltada para o cálculo de rebate de tx adm/pfee dos gestores e distribuidores.
    /// </summary>
	public class CalculoRebate
	{
        private class PosicaoRebate
        {
            public int IdCotista;
            public decimal ValorPosicao;
        }

        /// <summary>
        /// Calcula o rebate de tx adm e pfee de todos que sejam distribuidores da carteira passada.
        /// Calculado a partir da associação dos cotistas com os distribuidores (cadastro do cotista).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void CalculaRebateDistribuidor(int idCarteira, DateTime data)
        {
            //TODO Hoje calculando apenas o rebate para tx administração

            CalculoRebateCarteiraCollection calculoRebateCarteiraCollection = new CalculoRebateCarteiraCollection();
            calculoRebateCarteiraCollection.Query.Where(calculoRebateCarteiraCollection.Query.IdCarteira.Equal(idCarteira),
                                                        calculoRebateCarteiraCollection.Query.DataCalculo.Equal(data));
            calculoRebateCarteiraCollection.Query.Load();
            calculoRebateCarteiraCollection.MarkAllAsDeleted();
            calculoRebateCarteiraCollection.Save();

            HistoricoCota historicoCota = new HistoricoCota();
            try
            {
                historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
            }
            catch 
            {
                return; //Não tem PL na data, não faz nada
            }

            decimal valorPLCarteira = historicoCota.PLFechamento.Value;

            if (valorPLCarteira == 0)
                return;

            CalculoRebateCarteiraCollection calculoRebateCarteiraCollectionDeletar = new CalculoRebateCarteiraCollection();
            calculoRebateCarteiraCollectionDeletar.DeletaValoresRebate(idCarteira, data);

            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.BuscaAgenteMercadoDistribuidor();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            bool historico = cliente.DataDia.Value > data;

            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            if (!tabelaTaxaAdministracaoCollection.BuscaTabelaTaxaAdministracao(idCarteira, data))
                return;

            decimal baseApuracao = 1M;
            decimal baseAno = 252M;
            decimal taxaAdmCarteira = 1;

            foreach (TabelaTaxaAdministracao tabelaTaxaAdministracao in tabelaTaxaAdministracaoCollection)
            {
                if (tabelaTaxaAdministracao.IdCadastro == (int)TipoCadastroAdministracao.TaxaAdministracao)
                {
                    baseApuracao = tabelaTaxaAdministracao.BaseApuracao.Value;
                    baseAno = tabelaTaxaAdministracao.BaseAno.Value;
                    taxaAdmCarteira = tabelaTaxaAdministracao.Taxa.Value;
                    break;
                }
            }

            foreach (AgenteMercado agenteMercado in agenteMercadoCollection)
            {
                int idDistribuidor = agenteMercado.IdAgente.Value;
                //Acha o PL por distribuidor (cada cliente/cotista tem 1 distribuidor associado)
                decimal valorPLDistribuidor = this.RetornaSaldoBrutoDistribuidor(idCarteira, idDistribuidor);
                //
                if (valorPLDistribuidor > 0)
                {
                    TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();
                    if (tabelaRebateDistribuidor.BuscaTabela(idCarteira, data, idDistribuidor, valorPLDistribuidor))
                    {
                        decimal taxaAdmDistribuidor = tabelaRebateDistribuidor.PercentualRebateAdministracao.Value;

                        //Busca o valor dia de administração da carteira (hoje Tx Gestão fica de fora)
                        CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                        decimal valorAdministracaoDia = calculoAdministracao.RetornaValorAdministracaoDia(idCarteira, (byte)TipoCadastroAdministracao.TaxaAdministracao);
                        decimal valorAdministracaoAcumulado = calculoAdministracao.RetornaValorAdministracao(idCarteira, (byte)TipoCadastroAdministracao.TaxaAdministracao);

                        bool zeragemAdm = false;
                        if (valorAdministracaoDia == valorAdministracaoAcumulado)
                        {
                            zeragemAdm = true;
                        }

                        decimal valorRebateDia = Math.Round(valorPLDistribuidor * (taxaAdmCarteira / 100M) * (baseApuracao / baseAno) * (taxaAdmDistribuidor / 100M), 2);

                        if (valorRebateDia != 0)
                        {
                            CalculoRebateCarteira calculoRebateCarteiraInsert = new CalculoRebateCarteira();

                            //Carrega os valores acumulados de rebate do dia anterior, senão cria do zero
                            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                            CalculoRebateCarteira calculoRebateAnterior = new CalculoRebateCarteira();
                            if (calculoRebateAnterior.LoadByPrimaryKey(dataAnterior, idCarteira, idDistribuidor))
                            {
                                calculoRebateCarteiraInsert.DataCalculo = data;
                                calculoRebateCarteiraInsert.IdCarteira = idCarteira;
                                calculoRebateCarteiraInsert.IdAgenteDistribuidor = idDistribuidor;
                                calculoRebateCarteiraInsert.RebateAdministracaoDia = valorRebateDia;

                                if (zeragemAdm)
                                {
                                    calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = valorRebateDia;
                                }
                                else
                                {
                                    calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = calculoRebateAnterior.RebateAdministracaoAcumulado.Value +
                                                                                                        valorRebateDia;
                                }
                                calculoRebateCarteiraInsert.RebatePerformanceDia = 0;
                                calculoRebateCarteiraInsert.RebatePerformanceAcumulado = 0;

                                calculoRebateCarteiraInsert.Save();
                            }
                            else
                            {
                                calculoRebateCarteiraInsert.DataCalculo = data;
                                calculoRebateCarteiraInsert.IdCarteira = idCarteira;
                                calculoRebateCarteiraInsert.IdAgenteDistribuidor = idDistribuidor;
                                calculoRebateCarteiraInsert.RebateAdministracaoDia = valorRebateDia;
                                calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = valorRebateDia;
                                calculoRebateCarteiraInsert.RebatePerformanceDia = 0;
                                calculoRebateCarteiraInsert.RebatePerformanceAcumulado = 0;
                                calculoRebateCarteiraInsert.Save();
                            }
                        }

                    }

                }

                //Acha a Performance por distribuidor (cada cliente/cotista tem 1 distribuidor associado)
                decimal valorPerformanceDistribuidor = this.RetornaSaldoPerformanceDistribuidor(idCarteira, idDistribuidor);

                if (valorPerformanceDistribuidor != 0)
                {
                    TabelaRebateDistribuidor tabelaRebateDistribuidor = new TabelaRebateDistribuidor();
                    if (tabelaRebateDistribuidor.BuscaTabela(idCarteira, data, idDistribuidor, valorPLDistribuidor))
                    {
                        decimal taxaPerformanceDistribuidor = tabelaRebateDistribuidor.PercentualRebatePerformance.Value;

                        decimal valorRebatePerformanceDia = Math.Round(valorPerformanceDistribuidor * (taxaPerformanceDistribuidor / 100M), 2);

                        if (valorRebatePerformanceDia != 0)
                        {
                            CalculoRebateCarteira calculoRebateCarteiraInsert = new CalculoRebateCarteira();

                            if (calculoRebateCarteiraInsert.LoadByPrimaryKey(data, idCarteira, idDistribuidor))
                            {
                                calculoRebateCarteiraInsert.RebatePerformanceDia = valorRebatePerformanceDia;
                                calculoRebateCarteiraInsert.RebatePerformanceAcumulado = valorRebatePerformanceDia; // Rebate de performance nao acumula, entao repete o dia
                                calculoRebateCarteiraInsert.Save();
                            }
                            else
                            {
                                calculoRebateCarteiraInsert.DataCalculo = data;
                                calculoRebateCarteiraInsert.IdCarteira = idCarteira;
                                calculoRebateCarteiraInsert.IdAgenteDistribuidor = idDistribuidor;
                                calculoRebateCarteiraInsert.RebatePerformanceDia = valorRebatePerformanceDia;
                                calculoRebateCarteiraInsert.RebatePerformanceAcumulado = valorRebatePerformanceDia; // Rebate de performance nao acumula, entao repete o dia
                                calculoRebateCarteiraInsert.Save();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calcula o rebate de tx adm e pfee para o gestor da carteira passada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void CalculaRebateGestor(int idCarteira, DateTime data)
        {
            //TODO Hoje calculando apenas o rebate para tx administração

            CalculoRebateCarteiraCollection calculoRebateCarteiraCollection = new CalculoRebateCarteiraCollection();
            calculoRebateCarteiraCollection.Query.Where(calculoRebateCarteiraCollection.Query.IdCarteira.Equal(idCarteira),
                                                        calculoRebateCarteiraCollection.Query.DataCalculo.GreaterThanOrEqual(data));
            calculoRebateCarteiraCollection.Query.Load();
            calculoRebateCarteiraCollection.MarkAllAsDeleted();
            calculoRebateCarteiraCollection.Save();

            HistoricoCota historicoCota = new HistoricoCota();
            try
            {
                historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
            }
            catch
            {
                return; //Não tem PL na data, não faz nada
            }

            decimal valorPLCarteira = historicoCota.PLFechamento.Value;

            if (valorPLCarteira == 0)
                return;

            bool achouTabela = false;
            decimal percentualTaxaAdm = 0;
            //Percentual do rebate pela faixa do PL da carteira
            TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();
            if (tabelaRebateCarteira.BuscaTabela(idCarteira, data, valorPLCarteira))
            {
                percentualTaxaAdm = tabelaRebateCarteira.PercentualRebateAdministracao.Value;
                achouTabela = true;
            }
            else
            {
                //Pega IdGestor da carteira
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.IdAgenteGestor);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int idGestor = carteira.IdAgenteGestor.Value;
                
                //Acha o PL por gestor
                HistoricoCota historicoCotaGestor = new HistoricoCota();
                decimal valorPLGestor = historicoCotaGestor.RetornaPLPorGestor(idGestor, data);
                
                //Percentual do rebate pela faixa do PL total do gestor
                TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();
                if (tabelaRebateGestor.BuscaTabela(idGestor, data, valorPLGestor))
                {
                    percentualTaxaAdm = tabelaRebateGestor.PercentualRebateAdministracao.Value;
                    achouTabela = true;
                }
            }

            if (achouTabela)
            {
                CalculoRebateCarteiraCollection calculoRebateCarteiraCollectionDeletar = new CalculoRebateCarteiraCollection();                    
                calculoRebateCarteiraCollectionDeletar.DeletaValoresRebate(idCarteira, data);

                //Busca o valor dia de administração da carteira (hoje Tx Gestão fica de fora)
                CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                decimal valorAdministracaoDia = calculoAdministracao.RetornaValorAdministracaoDia(idCarteira, (byte)TipoCadastroAdministracao.TaxaAdministracao);
                decimal valorAdministracaoAcumulado = calculoAdministracao.RetornaValorAdministracao(idCarteira, (byte)TipoCadastroAdministracao.TaxaAdministracao);

                bool zeragemAdm = false;
                if (valorAdministracaoDia == valorAdministracaoAcumulado)
                {
                    zeragemAdm = true;
                }

                //Aqui calcula direto em cima da tx adm "total" já que o PL/Adm nesse caso diz respeito
                //somente a posições de cotistas controladas para o rebate!
                decimal valorRebateDia = Math.Round(valorAdministracaoDia * percentualTaxaAdm / 100M, 2);

                if (valorRebateDia != 0)
                {
                    if (ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.HasValue)
                    {
                        int idDistribuidor = Convert.ToInt32(ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.Value);

                        CalculoRebateCarteira calculoRebateCarteiraInsert = new CalculoRebateCarteira();

                        //Carrega os valores acumulados de rebate do dia anterior, senão cria do zero
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                        CalculoRebateCarteira calculoRebateAnterior = new CalculoRebateCarteira();
                        if (calculoRebateAnterior.LoadByPrimaryKey(dataAnterior, idCarteira, idDistribuidor))
                        {
                            calculoRebateCarteiraInsert.DataCalculo = data;
                            calculoRebateCarteiraInsert.IdCarteira = idCarteira;
                            calculoRebateCarteiraInsert.IdAgenteDistribuidor = idDistribuidor;
                            calculoRebateCarteiraInsert.RebateAdministracaoDia = valorRebateDia;
                            calculoRebateCarteiraInsert.RebatePerformanceDia = 0; //TODO Hoje não está sendo calculado

                            if (zeragemAdm)
                            {
                                calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = valorRebateDia;
                            }
                            else
                            {
                                calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = calculoRebateAnterior.RebateAdministracaoAcumulado.Value +
                                                                                                    valorRebateDia;
                            }

                            calculoRebateCarteiraInsert.RebatePerformanceAcumulado = 0; //TODO Hoje não está sendo calculado
                            calculoRebateCarteiraInsert.Save();
                        }
                        else
                        {
                            calculoRebateCarteiraInsert.DataCalculo = data;
                            calculoRebateCarteiraInsert.IdCarteira = idCarteira;
                            calculoRebateCarteiraInsert.IdAgenteDistribuidor = idDistribuidor;
                            calculoRebateCarteiraInsert.RebateAdministracaoDia = valorRebateDia;
                            calculoRebateCarteiraInsert.RebatePerformanceDia = 0; //TODO Hoje não está sendo calculado
                            calculoRebateCarteiraInsert.RebateAdministracaoAcumulado = valorRebateDia;
                            calculoRebateCarteiraInsert.RebatePerformanceAcumulado = 0; //TODO Hoje não está sendo calculado
                            calculoRebateCarteiraInsert.Save();
                        }                        
                    }
                }
            }
            
        }

        /// <summary>
        /// Calcula o rebate de tx adm para fundos comprados
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private CalculoRebateImpactaPL CalculaRebateGestorFundosComprados(int idCliente, int idCarteira, DateTime data)
        {
            //TODO Hoje calculando apenas o rebate para tx administração

            CalculoRebateImpactaPLCollection calculoRebateImpactaPLCollection = new CalculoRebateImpactaPLCollection();
            calculoRebateImpactaPLCollection.Query.Where(calculoRebateImpactaPLCollection.Query.IdCliente.Equal(idCliente),
                                                        calculoRebateImpactaPLCollection.Query.IdCarteira.Equal(idCarteira),
                                                        calculoRebateImpactaPLCollection.Query.DataLancamento.GreaterThanOrEqual(data));
            calculoRebateImpactaPLCollection.Query.Load();
            calculoRebateImpactaPLCollection.MarkAllAsDeleted();
            calculoRebateImpactaPLCollection.Save();

            HistoricoCota historicoCota = new HistoricoCota();
            try
            {
                historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
            }
            catch
            {
                return null; //Não tem PL na data, não faz nada
            }

            decimal valorPLCarteira = historicoCota.PLFechamento.Value;

            if (valorPLCarteira == 0)
                return null;

            bool achouTabela = false;
            decimal percentualTaxaAdm = 0;
            //Percentual do rebate pela faixa do PL da carteira
            TabelaRebateCarteira tabelaRebateCarteira = new TabelaRebateCarteira();
            if (tabelaRebateCarteira.BuscaTabela(idCarteira, data, valorPLCarteira))
            {
                percentualTaxaAdm = tabelaRebateCarteira.PercentualRebateAdministracao.Value;
                achouTabela = true;
            }
            else
            {
                //Pega IdGestor da carteira
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.IdAgenteGestor);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int idGestor = carteira.IdAgenteGestor.Value;

                //Acha o PL por gestor
                HistoricoCota historicoCotaGestor = new HistoricoCota();
                decimal valorPLGestor = historicoCotaGestor.RetornaPLPorGestor(idGestor, data);

                //Percentual do rebate pela faixa do PL total do gestor
                TabelaRebateGestor tabelaRebateGestor = new TabelaRebateGestor();
                if (tabelaRebateGestor.BuscaTabela(idGestor, data, valorPLGestor))
                {
                    percentualTaxaAdm = tabelaRebateGestor.PercentualRebateAdministracao.Value;
                    achouTabela = true;
                }
            }

            decimal valorRebateDia = 0;
            CalculoRebateImpactaPL calculoRebateImpactaPL = null;

            if (achouTabela)
            {
                TabelaTaxaAdministracao tabelaTxAdm = new TabelaTaxaAdministracao();
                tabelaTxAdm.Query.Where(tabelaTxAdm.Query.IdCarteira == idCarteira);
                tabelaTxAdm.Query.Load();

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                bool isIniciando = DateTime.Compare(data, cliente.DataImplantacao.Value) == 0;

                //Busca o valor dia de administração da carteira (hoje Tx Gestão fica de fora)
                CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                calculoAdministracao = calculoAdministracao.CalculaTxAdmImpactaPL(tabelaTxAdm, cliente.IdLocal.Value, data, idCarteira, cliente, isIniciando);
                

                PosicaoFundo posicaoTotalFundo = new PosicaoFundo();

                posicaoTotalFundo.Query.Select(posicaoTotalFundo.Query.ValorAplicacao.Sum());
                posicaoTotalFundo.Query.Where(posicaoTotalFundo.Query.IdCarteira == idCarteira);
                posicaoTotalFundo.Query.Load();

                decimal valorTotalAplicacao = posicaoTotalFundo.ValorAplicacao.Value;

                PosicaoFundo posicao = new PosicaoFundo();
                posicao.Query.Where(posicao.Query.IdCarteira == idCarteira);
                posicao.Query.Where(posicao.Query.IdCliente == idCliente);
                posicao.Query.Load();

                decimal fator = (posicao.ValorAplicacao.Value / valorTotalAplicacao);

                //Aqui calcula direto em cima da tx adm "total" já que o PL/Adm nesse caso diz respeito
                //somente a posições de cotistas controladas para o rebate!
                valorRebateDia = Math.Round(calculoAdministracao.ValorDia.Value * fator * percentualTaxaAdm / 100M, 2);

                //Efetua calculo do valor acumulado e persiste objeto
                //CalculaRebateImpactaPLQuery calculaRebateImpactaPLQuery = new CalculaRebateImpactaPLQuery();
                CalculoRebateImpactaPLQuery calculoRebateImpactaPLQuery = new CalculoRebateImpactaPLQuery();
                calculoRebateImpactaPLQuery.Select(calculoRebateImpactaPLQuery.ValorDia.Sum());
                calculoRebateImpactaPLQuery.Where(calculoRebateImpactaPLQuery.IdCliente.Equal(idCliente));
                calculoRebateImpactaPLQuery.Where(calculoRebateImpactaPLQuery.IdCarteira.Equal(idCarteira));
                calculoRebateImpactaPLQuery.Where(calculoRebateImpactaPLQuery.DataPagamento.Equal(calculoAdministracao.DataPagamento));

                CalculoRebateImpactaPL calculoRebateAcumulado = new CalculoRebateImpactaPL();
                calculoRebateAcumulado.Load(calculoRebateImpactaPLQuery);

                decimal valorAcumulado = 0;
                if (calculoRebateAcumulado.ValorDia == null)
                {
                    valorAcumulado = valorRebateDia;
                }
                else
                {
                    valorAcumulado = calculoRebateAcumulado.ValorDia.Value + valorRebateDia;
                }

                calculoRebateImpactaPL = new CalculoRebateImpactaPL();
                calculoRebateImpactaPL.IdCliente = idCliente;
                calculoRebateImpactaPL.IdCarteira = idCarteira;
                calculoRebateImpactaPL.ValorAcumulado = valorAcumulado;
                calculoRebateImpactaPL.ValorDia = valorRebateDia;
                calculoRebateImpactaPL.DataLancamento = data;
                calculoRebateImpactaPL.DataPagamento = calculoAdministracao.DataPagamento;
                calculoRebateImpactaPL.Save();
            }

            return calculoRebateImpactaPL;
        }

        /// <summary>
        /// Gera lancamento de liquidacao para 
        /// </summary>
        /// <param name="calculoRebateCarteira"></param>
        private void GeraLancamentoLiquidacao(CalculoRebateImpactaPL calculoRebateImpactaPL)
        {
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(calculoRebateImpactaPL.IdCliente.Value);
            //

            //Busca nome do Fundo que está pagando o rebate
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(calculoRebateImpactaPL.IdCarteira.Value);

            string descricao = "Rebate -" + carteira.Apelido + " (Vl Dia: " + calculoRebateImpactaPL.ValorDia.Value.ToString("0.00") + ")";
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.DataLancamento = calculoRebateImpactaPL.DataLancamento;
            liquidacao.DataVencimento = calculoRebateImpactaPL.DataPagamento;
            liquidacao.Descricao = descricao;
            liquidacao.Valor = calculoRebateImpactaPL.ValorAcumulado;
            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
            liquidacao.IdCliente = calculoRebateImpactaPL.IdCliente;
            liquidacao.IdConta = idContaDefault;
            liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao;
            liquidacao.Save();

        }

        /// <summary>
        /// Distribui a receita de rebates, proporcional a cada cotista. Se aplica a gestor e distribuidor.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void TrataDistribuicaoReceita(int idCarteira, DateTime data)
        {
            //TODO Hoje calculando apenas o rebate para tx administração

            CalculoRebateCarteiraCollection calculoRebateCarteiraCollection = new CalculoRebateCarteiraCollection();
            if (calculoRebateCarteiraCollection.BuscaValoresRebate(idCarteira, data))
            {
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

                CalculoRebateCotistaCollection calculoRebateCotistaCollection = new CalculoRebateCotistaCollection();
                calculoRebateCotistaCollection.DeletaValoresRebate(idCarteira, data);

                foreach (CalculoRebateCarteira calculoRebateCarteira in calculoRebateCarteiraCollection)
                {
                    int idAgenteDistribuidor = calculoRebateCarteira.IdAgenteDistribuidor.Value;
                    decimal valorRebateAdministracao = calculoRebateCarteira.RebateAdministracaoDia.Value;
                    decimal valorRebateAdministracaoAcumulado = calculoRebateCarteira.RebateAdministracaoAcumulado.Value;

                    PosicaoCotista posicaoCotistaDistribuidor = new PosicaoCotista();
                    List<PosicaoRebate> listaPosicao = this.BuscaPosicaoPorDistribuidor(idCarteira, idAgenteDistribuidor);
                    decimal valorBrutoDistribuidor = 0;
                    foreach (PosicaoRebate posicaoRebate in listaPosicao)
                    {
                        valorBrutoDistribuidor += posicaoRebate.ValorPosicao;
                    }

                    decimal valorResidual = valorRebateAdministracao;
                    int i = 0;
                    foreach (PosicaoRebate posicao in listaPosicao)
                    {
                        int idCotista = posicao.IdCotista;
                        decimal valorBrutoCotista = posicao.ValorPosicao;

                        if (valorBrutoDistribuidor != 0)
                        {
                            decimal valorRebateCotistaDia = Math.Round(valorBrutoCotista / valorBrutoDistribuidor
                                                                        * valorRebateAdministracao, 2);
                            if (i == (listaPosicao.Count - 1))
                            {
                                valorRebateCotistaDia = valorResidual;
                            }
                            else
                            {
                                valorResidual -= valorRebateCotistaDia;
                            }

                            CalculoRebateCotista calculoRebateCotista = new CalculoRebateCotista();
                            decimal rebateAdministracaoAcumulado = valorRebateCotistaDia;
                            if (calculoRebateCotista.LoadByPrimaryKey(dataAnterior, idCarteira, idCotista))
                            {
                                rebateAdministracaoAcumulado += calculoRebateCotista.RebateAdministracaoAcumulado.Value;
                            }

                            CalculoRebateCotista calculoRebateCotistaInsert = new CalculoRebateCotista();
                            calculoRebateCotistaInsert.DataCalculo = data;
                            calculoRebateCotistaInsert.IdCarteira = idCarteira;
                            calculoRebateCotistaInsert.IdCotista = idCotista;

                            if (valorRebateAdministracaoAcumulado == valorRebateAdministracao)
                            {
                                calculoRebateCotistaInsert.RebateAdministracaoAcumulado = valorRebateCotistaDia;
                            }
                            else
                            {
                                calculoRebateCotistaInsert.RebateAdministracaoAcumulado = rebateAdministracaoAcumulado;
                            }

                            calculoRebateCotistaInsert.RebateAdministracaoDia = valorRebateCotistaDia;
                            calculoRebateCotistaInsert.RebatePerformanceAcumulado = 0; //TODO Hoje não está sendo calculado
                            calculoRebateCotistaInsert.RebatePerformanceDia = 0; //TODO Hoje não está sendo calculado
                            calculoRebateCotistaInsert.Save();
                        }

                        i++;
                        
                    }                                        
                }
            }
        }

        /// <summary>
        /// Verifica se o tratamento é por tipo visão fundo Gestor ou Distribuidor e calcula de acordo.
        /// Em qq um dos casos, é feita a distribuição de receita proporcional a cada cotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoVisaoFundo"></param>
        public void TrataRebateDiario(int idCarteira, DateTime data, byte tipoVisaoFundo)
        {
            if (tipoVisaoFundo == (byte)TipoVisaoFundoRebate.VisaoDistribuidor)
            {
                CalculaRebateGestor(idCarteira, data);
            }
            else if (tipoVisaoFundo == (byte)TipoVisaoFundoRebate.VisaoGestor)
            {
                CalculaRebateDistribuidor(idCarteira, data);
            }
            
            TrataDistribuicaoReceita(idCarteira, data);            
        }

        /// <summary>
        /// Efetua calcula do rebate pelo cliente comprador de um fundo que efetua o pagamento de rebate
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaRebateImpactaPL(int idCliente, DateTime data)
        {
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("PFQ");
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == idCliente);

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                CalculoRebateImpactaPL calculoRebateImpactaPL = CalculaRebateGestorFundosComprados(idCliente, posicaoFundo.IdCarteira.Value, data);

                if (calculoRebateImpactaPL != null)
                {
                    this.GeraLancamentoLiquidacao(calculoRebateImpactaPL);
                }
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdCotista, ValorBruto.Sum(), agrupando por idCotista.
        /// Caso não ache em PosicaoCotistaCollection, realiza a mesma query e PosicaoFundoCollection, agrupando por idCliente.
        /// Filtra pelo join com Pessoa onde IdDistribuidor = idDistribuidor passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idDistribuidor"></param>
        /// <returns>bool indicando se achou registro</returns>
        private List<PosicaoRebate> BuscaPosicaoPorDistribuidor(int idCarteira, int idDistribuidor)
        {
            List<PosicaoRebate> lista = new List<PosicaoRebate>();

            PosicaoCotistaCollection col = new PosicaoCotistaCollection();
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            posicaoCotistaQuery.Select(posicaoCotistaQuery.IdCotista,
                                       posicaoCotistaQuery.ValorBruto.Sum());
            posicaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);
            posicaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            posicaoCotistaQuery.Where(pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor),
                                      posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                                      posicaoCotistaQuery.IdCarteira.NotEqual(posicaoCotistaQuery.IdCotista));
            posicaoCotistaQuery.GroupBy(posicaoCotistaQuery.IdCotista);
            col.Load(posicaoCotistaQuery);

            foreach (PosicaoCotista posicaoCotista in col)
            {
                PosicaoRebate p = new PosicaoRebate();
                p.IdCotista = posicaoCotista.IdCotista.Value;
                p.ValorPosicao = posicaoCotista.ValorBruto.Value;

                lista.Add(p);
            }

            if (lista.Count == 0)
            {
                PosicaoFundoCollection col2 = new PosicaoFundoCollection();
                PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                pessoaQuery = new PessoaQuery("E");
                ClienteQuery clienteQuery = new ClienteQuery("C");

                posicaoFundoQuery.Select(posicaoFundoQuery.IdCliente,
                                         posicaoFundoQuery.ValorBruto.Sum());
                posicaoFundoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoFundoQuery.IdCliente);
                posicaoFundoQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);
                posicaoFundoQuery.Where(pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor),
                                        posicaoFundoQuery.IdCarteira.Equal(idCarteira));
                posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCliente);

                col2.Load(posicaoFundoQuery);

                foreach (PosicaoFundo posicaoFundo in col2)
                {
                    Cliente cliente = new Cliente();
                    if (!cliente.IsMae(posicaoFundo.IdCliente.Value))
                    {
                        PosicaoRebate p = new PosicaoRebate();
                        p.IdCotista = posicaoFundo.IdCliente.Value;
                        p.ValorPosicao = posicaoFundo.ValorBruto.Value;

                        lista.Add(p);
                    }
                }
            }

            return lista;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira e o IdDistribuidor passados.
        /// Tenta pegar o valor em PosicaoCotista, se não achar tenta em PosicaoFundo.
        /// Join com Pessoa para achar o distribuidor relacionado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idDistribuidor"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSaldoBrutoDistribuidor(int idCarteira, int idDistribuidor)
        {
            decimal valor = 0;

            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            CotistaQuery cotistaQuery = new CotistaQuery("C");

            posicaoCotistaQuery.Select(posicaoCotistaQuery.ValorBruto.Sum());
            posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            posicaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                                      posicaoCotistaQuery.IdCarteira.NotEqual(posicaoCotistaQuery.IdCotista),  
                                      pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor));

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Load(posicaoCotistaQuery);

            if (posicaoCotista.ValorBruto.HasValue)
            {
                valor = posicaoCotista.ValorBruto.Value;
            }

            if (valor == 0)
            {
                PosicaoFundoCollection col2 = new PosicaoFundoCollection();
                PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                pessoaQuery = new PessoaQuery("E");
                ClienteQuery clienteQuery = new ClienteQuery("C");

                posicaoFundoQuery.Select(posicaoFundoQuery.IdCliente,
                                         posicaoFundoQuery.ValorBruto.Sum());
                posicaoFundoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoFundoQuery.IdCliente);
                posicaoFundoQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);
                posicaoFundoQuery.Where(pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor),
                                        posicaoFundoQuery.IdCarteira.Equal(idCarteira));
                posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCliente);

                col2.Load(posicaoFundoQuery);

                foreach (PosicaoFundo posicaoFundo in col2)
                {
                    Cliente cliente = new Cliente();
                    if (!cliente.IsMae(posicaoFundo.IdCliente.Value))
                    {
                        valor += posicaoFundo.ValorBruto.Value;
                    }
                }
            }

            return valor;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira e o IdDistribuidor passados.
        /// Tenta pegar o valor em PosicaoCotista, se não achar tenta em PosicaoFundo.
        /// Join com Pessoa para achar o distribuidor relacionado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idDistribuidor"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSaldoPerformanceDistribuidor(int idCarteira, int idDistribuidor)
        {
            decimal valor = 0;

            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");

            posicaoCotistaQuery.Select(posicaoCotistaQuery.ValorPerformance.Sum());
            posicaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == posicaoCotistaQuery.IdCotista);
            posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                                      posicaoCotistaQuery.IdCarteira.NotEqual(posicaoCotistaQuery.IdCotista),
                                      pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor));

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Load(posicaoCotistaQuery);

            if (posicaoCotista.ValorPerformance.HasValue)
            {
                valor = posicaoCotista.ValorPerformance.Value;
            }

            if (valor == 0)
            {
                PosicaoFundoCollection col2 = new PosicaoFundoCollection();
                PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                pessoaQuery = new PessoaQuery("E");
                posicaoFundoQuery.Select(posicaoFundoQuery.IdCliente,
                                         posicaoFundoQuery.ValorPerformance.Sum());
                posicaoFundoQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == posicaoFundoQuery.IdCliente);
                posicaoFundoQuery.Where(pessoaQuery.IdAgenteDistribuidor.Equal(idDistribuidor),
                                        posicaoFundoQuery.IdCarteira.Equal(idCarteira));
                posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCliente);

                col2.Load(posicaoFundoQuery);

                foreach (PosicaoFundo posicaoFundo in col2)
                {
                    Cliente cliente = new Cliente();
                    if (!cliente.IsMae(posicaoFundo.IdCliente.Value))
                    {
                        valor += posicaoFundo.ValorPerformance.Value;
                    }
                }
            }

            return valor;
        }
	}
}
