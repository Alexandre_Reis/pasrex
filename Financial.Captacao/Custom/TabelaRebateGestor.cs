﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao
{
	public partial class TabelaRebateGestor : esTabelaRebateGestor
	{
        /// <summary>
        /// Carrega o objeto TabelaRebateGestor com os campos DataReferencia, Faixa,
        /// IdGestor, PercentualRebateAdministracao, PercentualRebatePerformance.
        /// A data de referência é filtrada para todas as datas anteriores ou iguais a ela.
        /// A Faixa é filtrada para todas as faixas anteriores ou iguais a ela.
        /// </summary>
        /// <param name="idGestor"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="faixa"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaTabela(int idGestor, DateTime dataReferencia, decimal faixa)
        {
            TabelaRebateGestorCollection tabelaRebateGestorCollection = new TabelaRebateGestorCollection();

            // Busca todos os registros menores que a data de referencia ordenado decrescente pela data
            tabelaRebateGestorCollection.Query
                 .Select(tabelaRebateGestorCollection.Query.DataReferencia,
                         tabelaRebateGestorCollection.Query.Faixa,
                         tabelaRebateGestorCollection.Query.IdAgenteGestor,
                         tabelaRebateGestorCollection.Query.PercentualRebateAdministracao,
                         tabelaRebateGestorCollection.Query.PercentualRebatePerformance)
                 .Where(tabelaRebateGestorCollection.Query.IdAgenteGestor.Equal(idGestor),
                        tabelaRebateGestorCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        tabelaRebateGestorCollection.Query.Faixa.LessThanOrEqual(faixa))
                 .OrderBy(tabelaRebateGestorCollection.Query.DataReferencia.Descending,
                          tabelaRebateGestorCollection.Query.Faixa.Descending);

            tabelaRebateGestorCollection.Query.Load();

            if (tabelaRebateGestorCollection.HasData)
            {
                // Copia informações de TabelaRebateGestorCollection para TabelaRebateGestor
                this.AddNew();
                this.DataReferencia = tabelaRebateGestorCollection[0].DataReferencia;
                this.Faixa = tabelaRebateGestorCollection[0].Faixa;
                this.IdAgenteGestor = tabelaRebateGestorCollection[0].IdAgenteGestor;
                this.PercentualRebateAdministracao = tabelaRebateGestorCollection[0].PercentualRebateAdministracao;
                this.PercentualRebatePerformance = tabelaRebateGestorCollection[0].PercentualRebatePerformance;
            }

            return tabelaRebateGestorCollection.HasData;
        }
	}
}
