﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Captacao
{
	public partial class TabelaRebateOfficer : esTabelaRebateOfficer
	{
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o Registro com a Data Mais Proxima da Data de Referência Passada
        /// </summary>
        /// <param name="idPessoa"></param>
        /// <param name="idOfficer"></param>
        /// <param name="dataReferencia"></param>
        /// <returns>Booleano Indicando se Achou registro</returns>
        public bool BuscaRebateOfficer(int idPessoa, int idOfficer, DateTime dataReferencia) {
            TabelaRebateOfficerCollection tabelaRebateOfficerCollection = new TabelaRebateOfficerCollection();

            // Busca todos os Rebates menores que a data de Referencia ordenado decrescente pela data
            tabelaRebateOfficerCollection.Query
                 .Select(tabelaRebateOfficerCollection.Query.IdPessoa,
                         tabelaRebateOfficerCollection.Query.DataReferencia,
                         tabelaRebateOfficerCollection.Query.IdOfficer,
                         tabelaRebateOfficerCollection.Query.PercentualRebate)
                 .Where(tabelaRebateOfficerCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        tabelaRebateOfficerCollection.Query.IdPessoa == idPessoa,
                        tabelaRebateOfficerCollection.Query.IdOfficer == idOfficer)
                 .OrderBy(tabelaRebateOfficerCollection.Query.DataReferencia.Descending);

            tabelaRebateOfficerCollection.Query.Load();

            if (tabelaRebateOfficerCollection.HasData) {
                // Copia informações de tabelaRebateOfficerCollection para TabelaRebateOfficer
                this.AddNew();
                this.IdPessoa = ((TabelaRebateOfficer)tabelaRebateOfficerCollection[0]).IdPessoa;
                this.IdOfficer = ((TabelaRebateOfficer)tabelaRebateOfficerCollection[0]).IdOfficer;
                this.DataReferencia = ((TabelaRebateOfficer)tabelaRebateOfficerCollection[0]).DataReferencia;
                this.PercentualRebate = ((TabelaRebateOfficer)tabelaRebateOfficerCollection[0]).PercentualRebate;
            }

            return tabelaRebateOfficerCollection.HasData;
        }
	}
}
