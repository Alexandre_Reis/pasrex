﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao
{
	public partial class TabelaRebateDistribuidor : esTabelaRebateDistribuidor
	{
        /// <summary>
        /// Carrega o objeto TabelaRebateDistribuidor com os campos DataReferencia, Faixa, IdAgenteDistribuidor,
        /// IdCarteira, PercentualRebateAdministracao, PercentualRebatePerformance.
        /// A data de referência é filtrada para todas as datas anteriores ou iguais a ela.
        /// A Faixa é filtrada para todas as faixas anteriores ou iguais a ela.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="idDistribuidor"></param>
        /// <param name="faixa"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaTabela(int idCarteira, DateTime dataReferencia, int idDistribuidor, decimal faixa)
        {
            TabelaRebateDistribuidorCollection tabelaRebateDistribuidorCollection = new TabelaRebateDistribuidorCollection();

            // Busca todos os fatores menores que a data de referencia ordenado decrescente pela data
            tabelaRebateDistribuidorCollection.Query
                 .Select(tabelaRebateDistribuidorCollection.Query.DataReferencia,
                         tabelaRebateDistribuidorCollection.Query.Faixa,
                         tabelaRebateDistribuidorCollection.Query.IdAgenteDistribuidor,
                         tabelaRebateDistribuidorCollection.Query.IdCarteira,
                         tabelaRebateDistribuidorCollection.Query.PercentualRebateAdministracao,
                         tabelaRebateDistribuidorCollection.Query.PercentualRebatePerformance)
                 .Where(tabelaRebateDistribuidorCollection.Query.IdCarteira.Equal(idCarteira),
                        tabelaRebateDistribuidorCollection.Query.IdAgenteDistribuidor.Equal(idDistribuidor),
                        tabelaRebateDistribuidorCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        tabelaRebateDistribuidorCollection.Query.Faixa.LessThanOrEqual(faixa))
                 .OrderBy(tabelaRebateDistribuidorCollection.Query.DataReferencia.Descending,
                          tabelaRebateDistribuidorCollection.Query.Faixa.Descending);

            tabelaRebateDistribuidorCollection.Query.Load();

            if (tabelaRebateDistribuidorCollection.HasData)
            {
                // Copia informações de TabelaRebateDistribuidorCollection para TabelaRebateDistribuidor
                this.AddNew();
                this.DataReferencia = tabelaRebateDistribuidorCollection[0].DataReferencia;
                this.Faixa = tabelaRebateDistribuidorCollection[0].Faixa;
                this.IdAgenteDistribuidor = tabelaRebateDistribuidorCollection[0].IdAgenteDistribuidor;
                this.IdCarteira = tabelaRebateDistribuidorCollection[0].IdCarteira;
                this.PercentualRebateAdministracao = tabelaRebateDistribuidorCollection[0].PercentualRebateAdministracao;
                this.PercentualRebatePerformance = tabelaRebateDistribuidorCollection[0].PercentualRebatePerformance;
            }

            return tabelaRebateDistribuidorCollection.HasData;
        }

	}
}
