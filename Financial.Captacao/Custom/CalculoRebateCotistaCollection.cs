using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao
{
	public partial class CalculoRebateCotistaCollection : esCalculoRebateCotistaCollection
	{

        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Deleta baseado no idCarteira e data passados.
        /// Filtra por GreaterThanOrEqual(data).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns>Indica se trouxe registros</returns>
        public void DeletaValoresRebate(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.DataCalculo.GreaterThanOrEqual(data));

            this.Query.Load();
            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
