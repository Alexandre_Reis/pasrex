﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Captacao
{
	public partial class TabelaRebateCarteira : esTabelaRebateCarteira
	{
        /// <summary>
        /// Carrega o objeto TabelaRebateCarteira com os campos DataReferencia, Faixa,
        /// IdCarteira, PercentualRebateAdministracao, PercentualRebatePerformance.
        /// A data de referência é filtrada para todas as datas anteriores ou iguais a ela.
        /// A Faixa é filtrada para todas as faixas anteriores ou iguais a ela.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="faixa"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaTabela(int idCarteira, DateTime dataReferencia, decimal faixa)
        {
            TabelaRebateCarteiraCollection tabelaRebateCarteiraCollection = new TabelaRebateCarteiraCollection();

            // Busca todos os registros menores que a data de referencia ordenado decrescente pela data
            tabelaRebateCarteiraCollection.Query
                 .Select(tabelaRebateCarteiraCollection.Query.DataReferencia,
                         tabelaRebateCarteiraCollection.Query.Faixa,
                         tabelaRebateCarteiraCollection.Query.IdCarteira,
                         tabelaRebateCarteiraCollection.Query.PercentualRebateAdministracao,
                         tabelaRebateCarteiraCollection.Query.PercentualRebatePerformance)
                 .Where(tabelaRebateCarteiraCollection.Query.IdCarteira.Equal(idCarteira),
                        tabelaRebateCarteiraCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        tabelaRebateCarteiraCollection.Query.Faixa.LessThanOrEqual(faixa))
                 .OrderBy(tabelaRebateCarteiraCollection.Query.DataReferencia.Descending,
                          tabelaRebateCarteiraCollection.Query.Faixa.Descending);

            tabelaRebateCarteiraCollection.Query.Load();

            if (tabelaRebateCarteiraCollection.HasData)
            {
                // Copia informações de TabelaRebateCarteiraCollection para TabelaRebateCarteira
                this.AddNew();
                this.DataReferencia = tabelaRebateCarteiraCollection[0].DataReferencia;
                this.Faixa = tabelaRebateCarteiraCollection[0].Faixa;
                this.IdCarteira = tabelaRebateCarteiraCollection[0].IdCarteira;
                this.PercentualRebateAdministracao = tabelaRebateCarteiraCollection[0].PercentualRebateAdministracao;
                this.PercentualRebatePerformance = tabelaRebateCarteiraCollection[0].PercentualRebatePerformance;
            }

            return tabelaRebateCarteiraCollection.HasData;
        }
	}
}
