﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using Financial.Fundo;
using Financial.Investidor;
using Financial.CRM;
using Financial.Investidor.Enums;
using Financial.Security;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.CRM.Enums;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using System.Data;
using Financial.Common;

namespace Financial.Captacao.Custom
{
    public class Periodo
    {
        private DateTime data;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        private decimal valor;
        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
    }

    public class ConsultaPrimaria
    {
        private int idAgrupamento;
        public int IdAgrupamento
        {
            get { return idAgrupamento; }
            set { idAgrupamento = value; }
        }

        private int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private string descricao1;
        public string Descricao1
        {
            get { return descricao1; }
            set { descricao1 = value; }
        }

        private string descricao2;
        public string Descricao2
        {
            get { return descricao2; }
            set { descricao2 = value; }
        }

        private string descricao3;
        public string Descricao3
        {
            get { return descricao3; }
            set { descricao3 = value; }
        }
    }

    public class ConsultaDados
    {
        private int idAgrupamento;
        public int IdAgrupamento
        {
            get { return idAgrupamento; }
            set { idAgrupamento = value; }
        }

        private int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private string descricao1;
        public string Descricao1
        {
            get { return descricao1; }
            set { descricao1 = value; }
        }

        private string descricao2;
        public string Descricao2
        {
            get { return descricao2; }
            set { descricao2 = value; }
        }

        private string descricao3;
        public string Descricao3
        {
            get { return descricao3; }
            set { descricao3 = value; }
        }

        public Dictionary<DateTime, Periodo> DicDatePeriodo = new Dictionary<DateTime, Periodo>();

        private List<Periodo> listaValores;
        public List<Periodo> ListaValores
        {
            get { return listaValores; }
            set { listaValores = value; }
        }

        /// <summary>
        /// Retorna se todos os elementos da lista tem Valor zero
        /// </summary>
        /// <returns></returns>
        public bool AllElementsZero()
        {
            bool retorno = true;
            for (int i = 0; i < listaValores.Count; i++)
            {
                if (listaValores[i].Valor != 0)
                {
                    retorno = false;
                    break;
                }
            }

            return retorno;
        }
    }

    public class ConsultaCaptacao
    {
        #region Atributos
        public enum TipoColunaData
        {
            Mes = 1,
            Trimestre = 2,
            Semestre = 3,
            Ano = 4
        }

        public enum TipoInformacao
        {
            Aportes = 1,
            Resgates = 2,
            CaptacoesLiquidas = 3,
            Volume = 10,
            ReceitaAdm = 100,
            ReceitaPfee = 101,
            ReceitaRebateAdm = 102,
            ReceitaRebatePfee = 103,
            ReceitaRebateCorretagem = 104,
            ReceitaTotal = 110
        }

        public enum TipoAgrupamento
        {
            TipoCliente = 1,
            Officer = 2,
            Gestor = 3,
            Distribuidor = 4,
            AreaGeografica = 5,
            TipoProduto = 6
        }

        private string login;
        private DateTime dataInicio;
        private DateTime dataFim;

        private TipoColunaData colunaData;
        private TipoAgrupamento agrupamento;
        #endregion

        #region Construtor
        public ConsultaCaptacao(DateTime dataInicio, DateTime dataFim, TipoColunaData colunaData, string login)
        {
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            this.colunaData = colunaData;
            this.login = login;
        }
        #endregion

        #region Movimento (Aporte, Resgate, Captacao Liquida)
        public DataTable RetornaMovimentoCliente(TipoInformacao tipoInformacao, TipoAgrupamento tipoAgrupamento)
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();
            List<DateTime> listaDatas = this.RetornaListaDatas();
            List<ConsultaPrimaria> listaPrimaria = new List<ConsultaPrimaria>();

            if (tipoAgrupamento == TipoAgrupamento.Officer)
            {
                DataTable tableOfficer = this.RetornaMovimentoClienteOfficer(tipoInformacao);

                return tableOfficer;
            }
            else if (tipoAgrupamento == TipoAgrupamento.AreaGeografica)
            {
                listaPrimaria = RetornaListaMovimentoAreaGeografica();
            }
            else if (tipoAgrupamento == TipoAgrupamento.Distribuidor)
            {
                listaPrimaria = RetornaListaMovimentoDistribuidor();
            }
            else if (tipoAgrupamento == TipoAgrupamento.Gestor)
            {
                listaPrimaria = RetornaListaMovimentoGestor();
            }
            else if (tipoAgrupamento == TipoAgrupamento.TipoCliente)
            {
                listaPrimaria = RetornaListaMovimentoTipoCliente();
            }
            else if (tipoAgrupamento == TipoAgrupamento.TipoProduto)
            {
                listaPrimaria = RetornaListaMovimentoTipoProduto();
            }

            Dictionary<string, ConsultaPrimaria> dicConsultaPrimaria = new Dictionary<string, ConsultaPrimaria>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                string key = consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente; //Chave primaria juntando info da carteira e do cliente
                dicConsultaPrimaria.Add(key, consultaPrimaria);
            }

            //Criar estrutura de dados
            Dictionary<string, ConsultaDados> dicConsultaDados = new Dictionary<string, ConsultaDados>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                    ConsultaDados newConsultaDados = new ConsultaDados();
                    newConsultaDados.Descricao1 = consultaPrimaria.Descricao1;

                    Pessoa pessoaCliente = new Pessoa();
                    pessoaCliente.Query.Select(pessoaCliente.Query.CodigoInterface);
                    pessoaCliente.Query.Where(pessoaCliente.Query.IdPessoa.Equal(consultaPrimaria.IdCliente));
                    pessoaCliente.Query.Load();

                    string codigoInterfaceCliente = String.IsNullOrEmpty(pessoaCliente.CodigoInterface) ? "" : "(" + pessoaCliente.CodigoInterface + ")";
                    
                    newConsultaDados.Descricao2 = consultaPrimaria.Descricao2;
                    newConsultaDados.Descricao3 = consultaPrimaria.Descricao3 + " " + codigoInterfaceCliente;

                    newConsultaDados.IdCliente = consultaPrimaria.IdCliente;
                    newConsultaDados.IdAgrupamento = consultaPrimaria.IdAgrupamento;
                    newConsultaDados.ListaValores = new List<Periodo>();
                    foreach(DateTime data in listaDatas){
                        Periodo newPeriodo = new Periodo();
                        newPeriodo.Data = data;
                        newPeriodo.Valor = 0;
                        newConsultaDados.DicDatePeriodo.Add(data, newPeriodo);
                    }


                    //Chave primaria juntando info de Descricao1, da carteira e do cliente
                    string key = consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente; 
                    dicConsultaDados.Add(key, newConsultaDados);
            }

            #region Tratar OperacaoFundo
            //Carregar in memory todas as operacoes de fundo e cotista
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.DataOperacao,
                                                 operacaoFundoCollection.Query.IdCarteira,
                                                 operacaoFundoCollection.Query.IdCliente,
                                                 operacaoFundoCollection.Query.TipoOperacao,
                                                 operacaoFundoCollection.Query.ValorBruto);
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.DataOperacao.Between(this.dataInicio, this.dataFim));
            operacaoFundoCollection.Load(operacaoFundoCollection.Query);

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                //Descobrir se devo processar operacao
                string key = operacaoFundo.IdCarteira.Value + "|" + operacaoFundo.IdCliente.Value; //Chave primaria juntando info da carteira e do cliente
                if (!dicConsultaPrimaria.ContainsKey(key))
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                ConsultaDados consultaDados = dicConsultaDados[key];

                //Encontrar periodo da operacao
                DateTime dataPeriodo = new DateTime();
                foreach (DateTime dataAtual in listaDatas)
                {
                    if (operacaoFundo.DataOperacao.Value.Date <= dataAtual)
                    {
                        dataPeriodo = dataAtual;
                        break;
                    }
                }

                
                List<Periodo> listaValores = new List<Periodo>();

                if (tipoInformacao == TipoInformacao.Aportes)
                {
                    if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao && operacaoFundo.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoFundo.ValorBruto.Value;
                    }
                }
                else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas)
                {
                    if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao && operacaoFundo.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoFundo.ValorBruto.Value;
                    }
                    else if (
                        (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal) &&
                        operacaoFundo.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor -= operacaoFundo.ValorBruto.Value;
                    }
                }
                else
                {
                    if (
                        (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                        operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal) &&
                        operacaoFundo.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoFundo.ValorBruto.Value;
                    }
                }
            }
            #endregion

            #region Tratar OperacaoCotista
            //Carregar in memory todas as operacoes de cotista e cotista
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoFundoCollection.Query.Select(operacaoCotistaCollection.Query.DataOperacao,
                                                 operacaoCotistaCollection.Query.IdCarteira,
                                                 operacaoCotistaCollection.Query.IdCotista,
                                                 operacaoCotistaCollection.Query.TipoOperacao,
                                                 operacaoCotistaCollection.Query.ValorBruto);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.DataOperacao.Between(this.dataInicio, this.dataFim),
                                                  operacaoCotistaCollection.Query.IdCarteira.NotEqual(operacaoCotistaCollection.Query.IdCotista));
            operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                //Descobrir se devo processar operacao
                string key = operacaoCotista.IdCarteira.Value + "|" + operacaoCotista.IdCotista.Value; //Chave primaria juntando info da carteira e do cliente
                if (!dicConsultaPrimaria.ContainsKey(key))
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                ConsultaDados consultaDados = dicConsultaDados[key];

                //Encontrar periodo da operacao
                DateTime dataPeriodo = new DateTime();
                foreach (DateTime dataAtual in listaDatas)
                {
                    if (operacaoCotista.DataOperacao.Value.Date <= dataAtual)
                    {
                        dataPeriodo = dataAtual;
                        break;
                    }
                }

                if (!consultaDados.DicDatePeriodo.ContainsKey(dataPeriodo))
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = dataPeriodo;
                    newPeriodo.Valor = 0;
                    consultaDados.DicDatePeriodo.Add(dataPeriodo, newPeriodo);
                }

                List<Periodo> listaValores = new List<Periodo>();

                if (tipoInformacao == TipoInformacao.Aportes)
                {
                    if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao && operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoCotista.ValorBruto.Value;
                    }
                }
                else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas)
                {
                    if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao && operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoCotista.ValorBruto.Value;
                    }
                    else if (
                        (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal) &&
                        operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor -= operacaoCotista.ValorBruto.Value;
                    }
                }
                else
                {
                    if (
                        (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal) &&
                        operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoCotista.ValorBruto.Value;
                    }
                }
            }
            #endregion

            //Transformar pro output esperado:
            foreach (KeyValuePair<string, ConsultaDados> kvpConsultaDados in dicConsultaDados)
            {
                ConsultaDados consultaDados = kvpConsultaDados.Value;
                decimal total = 0;
                foreach(KeyValuePair<DateTime, Periodo> kvpDatePeriodo in consultaDados.DicDatePeriodo)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = kvpDatePeriodo.Key;
                    newPeriodo.Valor = kvpDatePeriodo.Value.Valor;
                    consultaDados.ListaValores.Add(newPeriodo);

                    total += newPeriodo.Valor;
                }

                if (total != 0)
                {
                    lista.Add(consultaDados);
                }
            }

            DataTable table = this.GeraDataTable(lista);

            return table;
        }

        public DataTable RetornaMovimentoClienteOfficer(TipoInformacao tipoInformacao)
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();
            List<DateTime> listaDatas = this.RetornaListaDatas();
            List<ConsultaPrimaria> listaPrimaria = new List<ConsultaPrimaria>();

            listaPrimaria = RetornaListaMovimentoOfficer();
        
            Dictionary<string, ConsultaPrimaria> dicConsultaPrimaria = new Dictionary<string, ConsultaPrimaria>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                string key = consultaPrimaria.Descricao1 + "|" + consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente;
                dicConsultaPrimaria.Add(key, consultaPrimaria);
            }

            //Criar estrutura de dados
            Dictionary<string, ConsultaDados> dicConsultaDados = new Dictionary<string, ConsultaDados>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                ConsultaDados newConsultaDados = new ConsultaDados();
                newConsultaDados.Descricao1 = consultaPrimaria.Descricao1;

                Pessoa pessoaCliente = new Pessoa();
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(consultaPrimaria.IdCliente);

                pessoaCliente.Query.Select(pessoaCliente.Query.CodigoInterface);
                pessoaCliente.Query.Where(pessoaCliente.Query.IdPessoa.Equal(cliente.IdPessoa));
                pessoaCliente.Query.Load();

                string codigoInterfaceCliente = String.IsNullOrEmpty(pessoaCliente.CodigoInterface) ? "" : "(" + pessoaCliente.CodigoInterface + ")";
                
                newConsultaDados.Descricao2 = consultaPrimaria.Descricao2;
                newConsultaDados.Descricao3 = consultaPrimaria.Descricao3 + " " + codigoInterfaceCliente;

                newConsultaDados.IdCliente = consultaPrimaria.IdCliente;
                newConsultaDados.IdAgrupamento = consultaPrimaria.IdAgrupamento;
                newConsultaDados.ListaValores = new List<Periodo>();
                foreach (DateTime data in listaDatas)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = data;
                    newPeriodo.Valor = 0;
                    newConsultaDados.DicDatePeriodo.Add(data, newPeriodo);
                }

                //Chave primaria juntando info de Descricao1, da carteira e do cliente
                string key = consultaPrimaria.Descricao1 + "|" + consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente;
                dicConsultaDados.Add(key, newConsultaDados);
            }

            #region Tratar OperacaoFundo
            //Carregar in memory todas as operacoes de fundo e cotista
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.DataOperacao,
                                                 operacaoFundoCollection.Query.IdCarteira,
                                                 operacaoFundoCollection.Query.IdCliente,
                                                 operacaoFundoCollection.Query.TipoOperacao,
                                                 operacaoFundoCollection.Query.ValorBruto);
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.DataOperacao.Between(this.dataInicio, this.dataFim));
            operacaoFundoCollection.Load(operacaoFundoCollection.Query);

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
                OfficerQuery officerQuery = new OfficerQuery("O");
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(operacaoFundo.IdCliente.Value);

                officerQuery.Select(officerQuery.Nome);
                officerQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdOfficer == officerQuery.IdOfficer);
                officerQuery.Where(tabelaRebateOfficerQuery.IdPessoa.Equal(cliente.IdPessoa));
                officerQuery.es.Distinct = true;

                OfficerCollection officerCollection = new OfficerCollection();
                officerCollection.Load(officerQuery);

                if (officerCollection.Count > 0)
                {
                    string nomeOfficer = officerCollection[0].Nome;

                    //Descobrir se devo processar operacao
                    string key = nomeOfficer + "|" + operacaoFundo.IdCarteira.Value + "|" + operacaoFundo.IdCliente.Value; //Chave primaria juntando info da carteira e do cliente
                    if (!dicConsultaPrimaria.ContainsKey(key))
                    {
                        continue;
                    }

                    ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                    ConsultaDados consultaDados = dicConsultaDados[key];

                    //Encontrar periodo da operacao
                    DateTime dataPeriodo = new DateTime();
                    foreach (DateTime dataAtual in listaDatas)
                    {
                        if (operacaoFundo.DataOperacao.Value.Date <= dataAtual)
                        {
                            dataPeriodo = dataAtual;
                            break;
                        }
                    }


                    List<Periodo> listaValores = new List<Periodo>();

                    if (tipoInformacao == TipoInformacao.Aportes)
                    {
                        if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao && operacaoFundo.ValorBruto.HasValue)
                        {
                            consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoFundo.ValorBruto.Value;
                        }
                    }
                    else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += 0; //Nao estah fazendo nada por enquanto
                    }
                    else
                    {
                        if (
                            (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                            operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                            operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                            operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal) &&
                            operacaoFundo.ValorBruto.HasValue)
                        {
                            consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoFundo.ValorBruto.Value;
                        }
                    }
                }
            }
            #endregion

            #region Tratar OperacaoCotista
            //Carregar in memory todas as operacoes de cotista e cotista
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoFundoCollection.Query.Select(operacaoCotistaCollection.Query.DataOperacao,
                                                 operacaoCotistaCollection.Query.IdCarteira,
                                                 operacaoCotistaCollection.Query.IdCotista,
                                                 operacaoCotistaCollection.Query.TipoOperacao,
                                                 operacaoCotistaCollection.Query.ValorBruto);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.DataOperacao.Between(this.dataInicio, this.dataFim),
                                                  operacaoCotistaCollection.Query.IdCarteira.NotEqual(operacaoCotistaCollection.Query.IdCotista));
            operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                //Descobrir se devo processar operacao
                string key = operacaoCotista.IdCarteira.Value + "|" + operacaoCotista.IdCotista.Value; //Chave primaria juntando info da carteira e do cliente
                if (!dicConsultaPrimaria.ContainsKey(key))
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                ConsultaDados consultaDados = dicConsultaDados[key];

                //Encontrar periodo da operacao
                DateTime dataPeriodo = new DateTime();
                foreach (DateTime dataAtual in listaDatas)
                {
                    if (operacaoCotista.DataOperacao.Value.Date <= dataAtual)
                    {
                        dataPeriodo = dataAtual;
                        break;
                    }
                }

                if (!consultaDados.DicDatePeriodo.ContainsKey(dataPeriodo))
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = dataPeriodo;
                    newPeriodo.Valor = 0;
                    consultaDados.DicDatePeriodo.Add(dataPeriodo, newPeriodo);
                }

                List<Periodo> listaValores = new List<Periodo>();

                if (tipoInformacao == TipoInformacao.Aportes)
                {
                    if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao && operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoCotista.ValorBruto.Value;
                    }
                }
                else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas)
                {
                    consultaDados.DicDatePeriodo[dataPeriodo].Valor += 0; //Nao estah fazendo nada por enquanto
                }
                else
                {
                    if (
                        (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal) &&
                        operacaoCotista.ValorBruto.HasValue)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += operacaoCotista.ValorBruto.Value;
                    }
                }
            }
            #endregion

            //Transformar pro output esperado:
            foreach (KeyValuePair<string, ConsultaDados> kvpConsultaDados in dicConsultaDados)
            {
                ConsultaDados consultaDados = kvpConsultaDados.Value;
                decimal total = 0;
                foreach (KeyValuePair<DateTime, Periodo> kvpDatePeriodo in consultaDados.DicDatePeriodo)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = kvpDatePeriodo.Key;
                    newPeriodo.Valor = kvpDatePeriodo.Value.Valor;
                    consultaDados.ListaValores.Add(newPeriodo);

                    total += newPeriodo.Valor;
                }

                if (total != 0)
                {
                    lista.Add(consultaDados);
                }
            }

            DataTable table = this.GeraDataTable(lista);

            return table;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoTipoCliente()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            ClienteQuery clienteQuery = new ClienteQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

            clienteQuery.Select(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);
            clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
            clienteQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            clienteQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection)
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;
                string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            //
            pessoaQuery.Select(pessoaQuery.Tipo,
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            //
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            //
            pessoaQuery.GroupBy(pessoaQuery.Tipo,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "Pessoa Fisica" : "Pessoa Juridica";
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoOfficer()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            OfficerQuery officerQuery = new OfficerQuery("F");
            TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");

            pessoaQuery.Select(officerQuery.Nome.As("NomeOfficer"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);
            pessoaQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(officerQuery).On(officerQuery.IdOfficer == tabelaRebateOfficerQuery.IdOfficer);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            pessoaQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            pessoaQuery.GroupBy(officerQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeOfficer"));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            pessoaQuery = new PessoaQuery("P");
            clienteQuery = new ClienteQuery("C");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            officerQuery = new OfficerQuery("F");
            tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");

            pessoaQuery.Select(officerQuery.Nome.As("NomeOfficer"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);
            pessoaQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(officerQuery).On(officerQuery.IdOfficer == tabelaRebateOfficerQuery.IdOfficer);
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == operacaoCotistaQuery.IdCarteira);            
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            pessoaQuery.GroupBy(officerQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeOfficer"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente &&
                        descricao1 == consultaPrimariaTeste.Descricao1)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoGestor()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            ClienteQuery clienteQuery = new ClienteQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("M");

            clienteQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            clienteQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            clienteQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);
                        
            clienteQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            clienteQuery.GroupBy(agenteMercadoQuery.Nome.As("NomeAgente"),
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);
                        
            foreach (Cliente cliente in clienteCollection)
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;
                string descricao1 = Convert.ToString(cliente.GetColumn("NomeAgente"));
                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            agenteMercadoQuery = new AgenteMercadoQuery("A");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);            
            //
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            //
            pessoaQuery.GroupBy(agenteMercadoQuery.Nome.As("NomeAgente"),
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"));

            PessoaCollection pessoaCollection = new PessoaCollection();

            try
            {
                pessoaCollection.Load(pessoaQuery);
            }
            catch (Exception e2)
            {
                string s = pessoaCollection.Query.es.LastQuery;
            }
            

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoDistribuidor()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            PessoaQuery pessoaQuery = new PessoaQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("M");
            ClienteQuery clienteQuery = new ClienteQuery("CL");

            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);
            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == pessoaQuery.IdAgenteDistribuidor);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            pessoaQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            pessoaQuery.GroupBy(agenteMercadoQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            agenteMercadoQuery = new AgenteMercadoQuery("A");
            pessoaQuery = new PessoaQuery("E");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == pessoaQuery.IdAgenteDistribuidor);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            //
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            //
            pessoaQuery.GroupBy(agenteMercadoQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoAreaGeografica()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            ClienteQuery clienteQuery = new ClienteQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            
            clienteQuery.Select(carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);
            clienteQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            clienteQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            clienteQuery.GroupBy(carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection)
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Select(pessoaEnderecoCollection.Query.Uf);
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(idCliente));
                pessoaEnderecoCollection.Query.Load();

                string descricao1 = "";
                if (pessoaEnderecoCollection.Count > 0 && !String.IsNullOrEmpty(pessoaEnderecoCollection[0].Uf))
                {
                    descricao1 = pessoaEnderecoCollection[0].Uf;
                }
                else
                {
                    continue;
                }

                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            //
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            //
            pessoaQuery.GroupBy(pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;

                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Select(pessoaEnderecoCollection.Query.Uf);
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(idPessoa));
                pessoaEnderecoCollection.Query.Load();

                string descricao1 = "";
                if (pessoaEnderecoCollection.Count > 0 && !String.IsNullOrEmpty(pessoaEnderecoCollection[0].Uf))
                {
                    descricao1 = pessoaEnderecoCollection[0].Uf;
                }
                else
                {
                    continue;
                }

                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaMovimentoTipoProduto()
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            #region Tratamento para OperacaoFundo
            ClienteQuery clienteQuery = new ClienteQuery("C");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");
            PessoaQuery pessoaQuery = new PessoaQuery("P");

            pessoaQuery.Select(tipoClienteQuery.Descricao,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);
            pessoaQuery.InnerJoin(operacaoFundoQuery).On(clienteQuery.IdCliente == operacaoFundoQuery.IdCliente);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            pessoaQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
            pessoaQuery.Where(operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            pessoaQuery.GroupBy(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
                carteiraMaeCollection.Query.Load();

                if (carteiraMaeCollection.Count > 0)
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para OperacaoCotista
            clienteQuery = new ClienteQuery("L");
            tipoClienteQuery = new TipoClienteQuery("T");
            pessoaQuery = new PessoaQuery("E");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(tipoClienteQuery.Descricao,
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"));

            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            pessoaQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
            //
            pessoaQuery.Where(operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            //
            pessoaQuery.GroupBy(tipoClienteQuery.Descricao,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }
        #endregion

        #region Saldos
        public DataTable RetornaSaldoCliente(TipoAgrupamento tipoAgrupamento)
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();
            List<DateTime> listaDatas = this.RetornaListaDatasUteis();
            List<ConsultaPrimaria> listaPrimaria = new List<ConsultaPrimaria>();

            if (tipoAgrupamento == TipoAgrupamento.Officer)
            {
                DataTable tableOfficer = this.RetornaSaldoClienteOfficer();

                return tableOfficer;
            }
            else if (tipoAgrupamento == TipoAgrupamento.AreaGeografica)
            {
                listaPrimaria = RetornaListaSaldoAreaGeografica(listaDatas);
            }
            else if (tipoAgrupamento == TipoAgrupamento.Distribuidor)
            {
                listaPrimaria = RetornaListaSaldoDistribuidor(listaDatas);
            }
            else if (tipoAgrupamento == TipoAgrupamento.Gestor)
            {
                listaPrimaria = RetornaListaSaldoGestor(listaDatas);
            }
            else if (tipoAgrupamento == TipoAgrupamento.TipoCliente)
            {
                listaPrimaria = RetornaListaSaldoTipoCliente(listaDatas);
            }
            else if (tipoAgrupamento == TipoAgrupamento.TipoProduto)
            {
                listaPrimaria = RetornaListaSaldoTipoProduto(listaDatas);
            }

            Dictionary<string, ConsultaPrimaria> dicConsultaPrimaria = new Dictionary<string, ConsultaPrimaria>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                string key = consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente; //Chave primaria juntando info da carteira e do cliente
                dicConsultaPrimaria.Add(key, consultaPrimaria);
            }

            //Criar estrutura de dados
            Dictionary<string, ConsultaDados> dicConsultaDados = new Dictionary<string, ConsultaDados>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                ConsultaDados newConsultaDados = new ConsultaDados();
                newConsultaDados.Descricao1 = consultaPrimaria.Descricao1;

                Pessoa pessoaCliente = new Pessoa();
                pessoaCliente.Query.Select(pessoaCliente.Query.CodigoInterface);
                pessoaCliente.Query.Where(pessoaCliente.Query.IdPessoa.Equal(consultaPrimaria.IdCliente));
                pessoaCliente.Query.Load();
                
                string codigoInterfaceCliente = String.IsNullOrEmpty(pessoaCliente.CodigoInterface) ? "" : "(" + pessoaCliente.CodigoInterface + ")";
                
                newConsultaDados.Descricao2 = consultaPrimaria.Descricao2;
                newConsultaDados.Descricao3 = consultaPrimaria.Descricao3 + " " + codigoInterfaceCliente;
                newConsultaDados.IdCliente = consultaPrimaria.IdCliente;
                newConsultaDados.IdAgrupamento = consultaPrimaria.IdAgrupamento;
                newConsultaDados.ListaValores = new List<Periodo>();
                foreach (DateTime data in listaDatas)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = data;
                    newPeriodo.Valor = 0;
                    newConsultaDados.DicDatePeriodo.Add(data, newPeriodo);
                }


                //Chave primaria juntando info de Descricao1, da carteira e do cliente
                string key = consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente;
                dicConsultaDados.Add(key, newConsultaDados);
            }

            #region Tratar PosicaoFundoHistorico
            //Carregar in memory todas as operacoes de fundo e cotista
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.DataHistorico,
                                                         posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                         posicaoFundoHistoricoCollection.Query.IdCliente,
                                                         posicaoFundoHistoricoCollection.Query.ValorBruto);


            posicaoFundoHistoricoCollection.Query.es.DefaultConjunction = esConjunction.Or;

            for (int i = 0; i < listaDatas.Count; i++) {
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.DataHistorico == listaDatas[i]);
            }
            
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoCollection.Query);

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                //Descobrir se devo processar operacao
                string key = posicaoFundoHistorico.IdCarteira.Value + "|" + posicaoFundoHistorico.IdCliente.Value; //Chave primaria juntando info da carteira e do cliente
                if (!dicConsultaPrimaria.ContainsKey(key))
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                ConsultaDados consultaDados = dicConsultaDados[key];

                //Encontrar periodo da operacao
                bool achou = false;
                DateTime dataPeriodo = new DateTime();
                foreach (DateTime dataAtual in listaDatas)
                {
                    if (posicaoFundoHistorico.DataHistorico.Value.Date == dataAtual)
                    {
                        dataPeriodo = dataAtual;
                        achou = true;
                        break;
                    }
                }

                if (achou == true)
                {
                    consultaDados.DicDatePeriodo[dataPeriodo].Valor += posicaoFundoHistorico.ValorBruto.Value;
                }
            }
            #endregion

            #region Tratar PosicaoCotistaHistorico
            //Carregar in memory todas as operacoes de cotista e cotista
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataHistorico,
                                                           posicaoCotistaHistoricoCollection.Query.IdCarteira,
                                                           posicaoCotistaHistoricoCollection.Query.IdCotista,
                                                           posicaoCotistaHistoricoCollection.Query.ValorBruto);

            posicaoCotistaHistoricoCollection.Query.es.DefaultConjunction = esConjunction.Or;

            for (int i = 0; i < listaDatas.Count; i++) {
                posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.DataHistorico == listaDatas[i]);
            }
            //posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.DataHistorico.In(listaDatas));

            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoCollection.Query);

            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                //Descobrir se devo processar
                string key = posicaoCotistaHistorico.IdCarteira.Value + "|" + posicaoCotistaHistorico.IdCotista.Value; //Chave primaria juntando info da carteira e do cliente
                if (!dicConsultaPrimaria.ContainsKey(key))
                {
                    continue;
                }

                ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                ConsultaDados consultaDados = dicConsultaDados[key];

                //Encontrar periodo da operacao
                DateTime dataPeriodo = new DateTime();
                bool achou = false;
                foreach (DateTime dataAtual in listaDatas)
                {
                    if (posicaoCotistaHistorico.DataHistorico.Value.Date == dataAtual)
                    {
                        dataPeriodo = dataAtual;
                        achou = true;
                        break;
                    }
                }

                if (!consultaDados.DicDatePeriodo.ContainsKey(dataPeriodo) && achou)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = dataPeriodo;
                    newPeriodo.Valor = 0;
                    consultaDados.DicDatePeriodo.Add(dataPeriodo, newPeriodo);
                }

                if (achou)
                {
                    consultaDados.DicDatePeriodo[dataPeriodo].Valor += posicaoCotistaHistorico.ValorBruto.Value;
                }
            }
            #endregion

            //Transformar pro output esperado:
            foreach (KeyValuePair<string, ConsultaDados> kvpConsultaDados in dicConsultaDados)
            {
                ConsultaDados consultaDados = kvpConsultaDados.Value;
                decimal total = 0;
                foreach (KeyValuePair<DateTime, Periodo> kvpDatePeriodo in consultaDados.DicDatePeriodo)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = kvpDatePeriodo.Key;
                    newPeriodo.Valor = kvpDatePeriodo.Value.Valor;
                    consultaDados.ListaValores.Add(newPeriodo);

                    total += newPeriodo.Valor;
                }

                if (total != 0)
                {
                    lista.Add(consultaDados);
                }
            }

            DataTable table = this.GeraDataTable(lista);

            return table;
        }

        public DataTable RetornaSaldoClienteOfficer()
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();
            List<DateTime> listaDatas = this.RetornaListaDatasUteis();
            List<ConsultaPrimaria> listaPrimaria = new List<ConsultaPrimaria>();

            listaPrimaria = RetornaListaSaldoOfficer(listaDatas);

            Dictionary<string, ConsultaPrimaria> dicConsultaPrimaria = new Dictionary<string, ConsultaPrimaria>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                string key = consultaPrimaria.Descricao1 + "|" + consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente;
                dicConsultaPrimaria.Add(key, consultaPrimaria);
            }

            //Criar estrutura de dados
            Dictionary<string, ConsultaDados> dicConsultaDados = new Dictionary<string, ConsultaDados>();
            foreach (ConsultaPrimaria consultaPrimaria in listaPrimaria)
            {
                ConsultaDados newConsultaDados = new ConsultaDados();
                newConsultaDados.Descricao1 = consultaPrimaria.Descricao1;

                Pessoa pessoaCliente = new Pessoa();
                pessoaCliente.Query.Select(pessoaCliente.Query.CodigoInterface);
                pessoaCliente.Query.Where(pessoaCliente.Query.IdPessoa.Equal(consultaPrimaria.IdCliente));
                pessoaCliente.Query.Load();

                string codigoInterfaceCliente = String.IsNullOrEmpty(pessoaCliente.CodigoInterface) ? "" : "(" + pessoaCliente.CodigoInterface + ")";
                
                newConsultaDados.Descricao2 = consultaPrimaria.Descricao2;
                newConsultaDados.Descricao3 = consultaPrimaria.Descricao3 + " " + codigoInterfaceCliente;

                newConsultaDados.IdCliente = consultaPrimaria.IdCliente;
                newConsultaDados.IdAgrupamento = consultaPrimaria.IdAgrupamento;
                newConsultaDados.ListaValores = new List<Periodo>();
                foreach (DateTime data in listaDatas)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = data;
                    newPeriodo.Valor = 0;
                    newConsultaDados.DicDatePeriodo.Add(data, newPeriodo);
                }

                //Chave primaria juntando info de Descricao1, da carteira e do cliente
                string key = consultaPrimaria.Descricao1 + "|" + consultaPrimaria.IdAgrupamento + "|" + consultaPrimaria.IdCliente;
                dicConsultaDados.Add(key, newConsultaDados);
            }

            #region Tratar PosicaoFundoHistorico
            //Carregar in memory todas as operacoes de fundo e cotista
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.DataHistorico,
                                                         posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                         posicaoFundoHistoricoCollection.Query.IdCliente,
                                                         posicaoFundoHistoricoCollection.Query.ValorBruto);
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.DataHistorico.Between(this.dataInicio, this.dataFim));
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoCollection.Query);

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
                OfficerQuery officerQuery = new OfficerQuery("O");

                officerQuery.Select(officerQuery.Nome);
                officerQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdOfficer == officerQuery.IdOfficer);
                officerQuery.Where(tabelaRebateOfficerQuery.IdPessoa.Equal(posicaoFundoHistorico.IdCliente.Value));
                officerQuery.es.Distinct = true;

                OfficerCollection officerCollection = new OfficerCollection();
                officerCollection.Load(officerQuery);

                if (officerCollection.Count > 0)
                {
                    string nomeOfficer = officerCollection[0].Nome;

                    //Descobrir se devo processar operacao
                    string key = nomeOfficer + "|" + posicaoFundoHistorico.IdCarteira.Value + "|" + posicaoFundoHistorico.IdCliente.Value; //Chave primaria juntando info da carteira e do cliente
                    if (!dicConsultaPrimaria.ContainsKey(key))
                    {
                        continue;
                    }

                    ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                    ConsultaDados consultaDados = dicConsultaDados[key];

                    //Encontrar periodo da operacao
                    DateTime dataPeriodo = new DateTime();
                    bool achou = false;
                    foreach (DateTime dataAtual in listaDatas)
                    {
                        if (posicaoFundoHistorico.DataHistorico.Value.Date == dataAtual)
                        {
                            dataPeriodo = dataAtual;
                            achou = true;
                            break;
                        }
                    }

                    List<Periodo> listaValores = new List<Periodo>();

                    if (achou)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += posicaoFundoHistorico.ValorBruto.Value;
                    }
                }
            }
            #endregion

            #region Tratar PosicaoCotistaHistorico
            //Carregar in memory todas as operacoes de cotista e cotista
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataHistorico,
                                                           posicaoCotistaHistoricoCollection.Query.IdCarteira,
                                                           posicaoCotistaHistoricoCollection.Query.IdCotista,                                                           
                                                           posicaoCotistaHistoricoCollection.Query.ValorBruto);
            posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.DataHistorico.Between(this.dataInicio, this.dataFim));
            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoCollection.Query);

            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
                OfficerQuery officerQuery = new OfficerQuery("O");
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(posicaoCotistaHistorico.IdCotista.Value);

                officerQuery.Select(officerQuery.Nome);
                officerQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdOfficer == officerQuery.IdOfficer);
                officerQuery.Where(tabelaRebateOfficerQuery.IdPessoa.Equal(cotista.IdPessoa.Value));
                officerQuery.es.Distinct = true;

                OfficerCollection officerCollection = new OfficerCollection();
                officerCollection.Load(officerQuery);

                if (officerCollection.Count > 0)
                {
                    string nomeOfficer = officerCollection[0].Nome;

                    //Descobrir se devo processar operacao
                    string key = nomeOfficer + "|" + posicaoCotistaHistorico.IdCarteira.Value + "|" + posicaoCotistaHistorico.IdCotista.Value; //Chave primaria juntando info da carteira e do cliente
                    if (!dicConsultaPrimaria.ContainsKey(key))
                    {
                        continue;
                    }

                    ConsultaPrimaria consultaPrimaria = dicConsultaPrimaria[key];
                    ConsultaDados consultaDados = dicConsultaDados[key];

                    //Encontrar periodo da operacao
                    DateTime dataPeriodo = new DateTime();
                    bool achou = false;
                    foreach (DateTime dataAtual in listaDatas)
                    {
                        if (posicaoCotistaHistorico.DataHistorico.Value.Date == dataAtual)
                        {
                            dataPeriodo = dataAtual;
                            achou = true;
                            break;
                        }
                    }
                                        
                    if (!consultaDados.DicDatePeriodo.ContainsKey(dataPeriodo) && achou)
                    {
                        Periodo newPeriodo = new Periodo();
                        newPeriodo.Data = dataPeriodo;
                        newPeriodo.Valor = 0;
                        consultaDados.DicDatePeriodo.Add(dataPeriodo, newPeriodo);
                    }

                    List<Periodo> listaValores = new List<Periodo>();

                    if (achou)
                    {
                        consultaDados.DicDatePeriodo[dataPeriodo].Valor += posicaoCotistaHistorico.ValorBruto.Value;
                    }
                }
            }
            #endregion

            //Transformar pro output esperado:
            foreach (KeyValuePair<string, ConsultaDados> kvpConsultaDados in dicConsultaDados)
            {
                ConsultaDados consultaDados = kvpConsultaDados.Value;
                decimal total = 0;
                foreach (KeyValuePair<DateTime, Periodo> kvpDatePeriodo in consultaDados.DicDatePeriodo)
                {
                    Periodo newPeriodo = new Periodo();
                    newPeriodo.Data = kvpDatePeriodo.Key;
                    newPeriodo.Valor = kvpDatePeriodo.Value.Valor;
                    consultaDados.ListaValores.Add(newPeriodo);

                    total += newPeriodo.Valor;
                }

                if (total != 0)
                {
                    lista.Add(consultaDados);
                }
            }

            DataTable table = this.GeraDataTable(lista);

            return table;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoTipoCliente(List<DateTime> listaDatas) 
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }
            
            #region Tratamento para PosicaoFundoHistorico
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

            clienteQuery.Select(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
            clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            
            //           
            if (listaDatas.Count != 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.Or;
                clienteQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                clienteQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.And;
                clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMae));
            }

            clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                clienteQuery.IdCliente,
                                clienteQuery.Nome,
                                posicaoFundoHistoricoQuery.DataHistorico);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection) 
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;
                string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;

                //
                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;
                //

                ConsultaPrimaria c = new ConsultaPrimaria();
                c = lista.Find(
                    delegate(ConsultaPrimaria item) {
                        return item.IdAgrupamento == idAgrupamento &&
                               item.IdCliente == idCliente;
                    });

                if (c == null) { // não achou adiciona
                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            carteiraQuery = new CarteiraQuery("A");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            //
            pessoaQuery.Select(pessoaQuery.Tipo,
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }
            //
            pessoaQuery.GroupBy(pessoaQuery.Tipo,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "Pessoa Fisica" : "Pessoa Juridica";
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoOfficer(List<DateTime> listaDatas)
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }

            #region Tratamento para PosicaoFundoHistorico
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            OfficerQuery officerQuery = new OfficerQuery("F");
            TabelaRebateOfficerQuery tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");

            pessoaQuery.Select(officerQuery.Nome.As("NomeOfficer"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);
            pessoaQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(officerQuery).On(officerQuery.IdOfficer == tabelaRebateOfficerQuery.IdOfficer);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(pessoaQuery.IdPessoa.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(officerQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeOfficer"));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            pessoaQuery = new PessoaQuery("P");
            clienteQuery = new ClienteQuery("C");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            officerQuery = new OfficerQuery("F");
            tabelaRebateOfficerQuery = new TabelaRebateOfficerQuery("T");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");

            pessoaQuery.Select(officerQuery.Nome.As("NomeOfficer"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);

            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(tabelaRebateOfficerQuery).On(tabelaRebateOfficerQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(officerQuery).On(officerQuery.IdOfficer == tabelaRebateOfficerQuery.IdOfficer);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoCotistaHistoricoQuery.IdCarteira);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(officerQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeOfficer"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente &&
                        descricao1 == consultaPrimariaTeste.Descricao1)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoGestor(List<DateTime> listaDatas)
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }

            #region Tratamento para PosicaoFundoHistorico
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("M");

            clienteQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            clienteQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);

            if (listaDatas.Count != 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.Or;
                clienteQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                clienteQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.And;
                clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMae));
            }

            clienteQuery.GroupBy(agenteMercadoQuery.Nome.As("NomeAgente"),
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection)
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;
                string descricao1 = Convert.ToString(cliente.GetColumn("NomeAgente"));
                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;
                
                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            agenteMercadoQuery = new AgenteMercadoQuery("A");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );

            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);
            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }

            //
            pessoaQuery.GroupBy(agenteMercadoQuery.Nome.As("NomeAgente"),
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"));

            PessoaCollection pessoaCollection = new PessoaCollection();

            try
            {
                pessoaCollection.Load(pessoaQuery);
            }
            catch (Exception e2)
            {
                string s = pessoaCollection.Query.es.LastQuery;
            }


            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoDistribuidor(List<DateTime> listaDatas)
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }

            #region Tratamento para PosicaoFundoHistorico
            PessoaQuery pessoaQuery = new PessoaQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("M");
            ClienteQuery clienteQuery = new ClienteQuery();

            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);

            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == pessoaQuery.IdAgenteDistribuidor);
            pessoaQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
            pessoaQuery.InnerJoin(posicaoFundoHistoricoQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(pessoaQuery.IdPessoa.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(agenteMercadoQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;
                                
                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            agenteMercadoQuery = new AgenteMercadoQuery("A");
            pessoaQuery = new PessoaQuery("E");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == pessoaQuery.IdAgenteDistribuidor);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(agenteMercadoQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn("NomeAgente"));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoAreaGeografica(List<DateTime> listaDatas)
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }

            #region Tratamento para PosicaoFundoHistorico
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            clienteQuery.Select(carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);
            clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.Or;
                clienteQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                clienteQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                clienteQuery.es.DefaultConjunction = esConjunction.And;
                clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMae));
            }

            clienteQuery.GroupBy(carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection)
            {
                int idAgrupamento = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = cliente.IdCliente.Value;

                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Select(pessoaEnderecoCollection.Query.Uf);
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(idCliente));
                pessoaEnderecoCollection.Query.Load();

                string descricao1 = "";
                if (pessoaEnderecoCollection.Count > 0 && !String.IsNullOrEmpty(pessoaEnderecoCollection[0].Uf))
                {
                    descricao1 = pessoaEnderecoCollection[0].Uf;
                }
                else
                {
                    continue;
                }

                string descricao2 = Convert.ToString(cliente.GetColumn("NomeCarteira"));
                string descricao3 = cliente.Nome;

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            //
            pessoaQuery.Select(pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }

            //
            pessoaQuery.GroupBy(pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;

                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Select(pessoaEnderecoCollection.Query.Uf);
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(idPessoa));
                pessoaEnderecoCollection.Query.Load();

                string descricao1 = "";
                if (pessoaEnderecoCollection.Count > 0 && !String.IsNullOrEmpty(pessoaEnderecoCollection[0].Uf))
                {
                    descricao1 = pessoaEnderecoCollection[0].Uf;
                }
                else
                {
                    continue;
                }

                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }

        public List<ConsultaPrimaria> RetornaListaSaldoTipoProduto(List<DateTime> listaDatas)
        {
            List<ConsultaPrimaria> lista = new List<ConsultaPrimaria>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection(); //Carteira mae não é adicionada
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            List<int> listaMae = new List<int>();
            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMae.Add(Convert.ToInt32(carteiraMae.IdCarteiraMae.Value));
            }

            #region Tratamento para PosicaoFundoHistorico
            PessoaQuery pessoaQuery = new PessoaQuery("C");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

            pessoaQuery.Select(tipoClienteQuery.Descricao,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira"),
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome);

            pessoaQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            pessoaQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(pessoaQuery.IdPessoa.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome);

            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idCliente = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                consultaPrimaria.Descricao1 = descricao1;
                consultaPrimaria.Descricao2 = descricao2;
                consultaPrimaria.Descricao3 = descricao3;
                consultaPrimaria.IdAgrupamento = idAgrupamento;
                consultaPrimaria.IdCliente = idCliente;

                lista.Add(consultaPrimaria);
            }
            #endregion

            #region Tratamento para PosicaoCotistaHistorico
            clienteQuery = new ClienteQuery("L");
            tipoClienteQuery = new TipoClienteQuery("T");
            pessoaQuery = new PessoaQuery("E");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("CO");
            //
            pessoaQuery.Select(tipoClienteQuery.Descricao,
                               pessoaQuery.IdPessoa,
                               pessoaQuery.Nome,
                               carteiraQuery.IdCarteira,
                               carteiraQuery.Nome.As("NomeCarteira") );

            pessoaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            pessoaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            pessoaQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);

            if (listaDatas.Count != 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.Or;
                pessoaQuery.Where(new esWhereItem(esParenthesis.Open));

                for (int i = 0; i < listaDatas.Count; i++)
                {
                    pessoaQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico == listaDatas[i]);
                }

                pessoaQuery.Where(new esWhereItem(esParenthesis.Close));
            }

            if (listaMae.Count > 0)
            {
                pessoaQuery.es.DefaultConjunction = esConjunction.And;
                pessoaQuery.Where(carteiraQuery.IdCarteira.NotIn(listaMae));
            }

            pessoaQuery.GroupBy(tipoClienteQuery.Descricao,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome);

            pessoaCollection = new PessoaCollection();
            pessoaCollection.Load(pessoaQuery);

            foreach (Pessoa pessoa in pessoaCollection)
            {
                int idAgrupamento = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                int idPessoa = pessoa.IdPessoa.Value;
                string descricao1 = Convert.ToString(pessoa.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                string descricao2 = idAgrupamento == idPessoa ? "Carteira Administrada" : Convert.ToString(pessoa.GetColumn("NomeCarteira"));
                string descricao3 = pessoa.Nome;

                bool existente = false;
                foreach (ConsultaPrimaria consultaPrimariaTeste in lista)
                {
                    if (idAgrupamento == consultaPrimariaTeste.IdAgrupamento && idPessoa == consultaPrimariaTeste.IdCliente)
                    {
                        existente = true;
                    }
                }

                if (!existente)
                {
                    ConsultaPrimaria consultaPrimaria = new ConsultaPrimaria();
                    consultaPrimaria.Descricao1 = descricao1;
                    consultaPrimaria.Descricao2 = descricao2;
                    consultaPrimaria.Descricao3 = descricao3;
                    consultaPrimaria.IdAgrupamento = idAgrupamento;
                    consultaPrimaria.IdCliente = idPessoa;

                    lista.Add(consultaPrimaria);
                }
            }
            #endregion

            return lista;
        }
        #endregion

        /// <summary>
        /// A partir da Classe ConsultaDados gera um DataTable no formato Id/Descricao1/Descricao2/Descricao3/IdCliente
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private DataTable GeraDataTable(List<ConsultaDados> c)
        {
            DataTable dt = new DataTable();
            //
            dt.Columns.Add("Id", typeof(int)); // Chave do Grid
            dt.Columns.Add("Descricao1", typeof(string));
            dt.Columns.Add("Descricao2", typeof(string));
            dt.Columns.Add("Descricao3", typeof(string));
            dt.Columns.Add("IdCliente", typeof(int));
            //

            if (c.Count != 0)
            {
                // Pega o numero de colunas e caption da primeira lista
                List<Periodo> l = c[0].ListaValores;

                for (int j = 1; j <= l.Count; j++)
                {
                    DataColumn column = new DataColumn();
                    column.DataType = System.Type.GetType("System.Decimal");
                    column.ColumnName = "Valor" + j;

                    column.Caption = this.colunaData == TipoColunaData.Ano
                                     ? l[j - 1].Data.ToString("yyyy") : l[j - 1].Data.ToString("MM/yyyy");
                    //
                    dt.Columns.Add(column);
                }
            }

            // Adiciona Dados
            for (int i = 0; i < c.Count; i++)
            {
                dt.Rows.Add(new object[] { i, c[i].Descricao1.Trim(), c[i].Descricao2.Trim(), c[i].Descricao3.Trim(), c[i].IdCliente });
                //
                DataRow dr = dt.Rows[i];
                //

                int colunas = c[i].ListaValores.Count;
                for (int j = 0; j < colunas; j++)
                {
                    dr[j + 5] = c[i].ListaValores[j].Valor;
                }
            }

            return dt;
        }

        /// <summary>
        /// Retorna datas de acordo com o tipo de agrupamento definido no objeto da classe (mes, trimestre, semestre, ano).
        /// </summary>
        /// <returns></returns>
        public List<DateTime> RetornaListaDatas()
        {
            List<DateTime> lista = new List<DateTime>();

            DateTime dataAux = this.dataInicio;

            int i = 0;
            while (dataAux < this.dataFim)
            {
                if (i > 0)
                {
                    lista.Add(dataAux);
                }

                if (this.colunaData == TipoColunaData.Mes)
                {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(this.dataInicio, i);

                    i = i + 1;
                }
                else if (this.colunaData == TipoColunaData.Trimestre)
                {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(this.dataInicio, i);

                    i = i + 3;
                }
                else if (this.colunaData == TipoColunaData.Semestre)
                {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(this.dataInicio, i);

                    i = i + 6;
                }
                else if (this.colunaData == TipoColunaData.Ano)
                {
                    dataAux = Calendario.RetornaUltimoDiaCorridoAno(this.dataInicio, i);

                    i = i + 1;
                }
            }

            lista.Add(this.dataFim);

            return lista;
        }

        /// <summary>
        /// Retorna datas de acordo com o tipo de agrupamento definido no objeto da classe (mes, trimestre, semestre, ano).
        /// </summary>
        /// <returns></returns>
        public List<DateTime> RetornaListaDatasUteis()
        {
            List<DateTime> lista = new List<DateTime>();

            DateTime dataAux = this.dataInicio;

            int i = 0;
            while (dataAux < this.dataFim)
            {
                if (i > 0)
                {
                    lista.Add(dataAux);
                }

                if (this.colunaData == TipoColunaData.Mes)
                {
                    dataAux = Calendario.RetornaUltimoDiaUtilMes(this.dataInicio, i);

                    i = i + 1;
                }
                else if (this.colunaData == TipoColunaData.Trimestre)
                {
                    dataAux = Calendario.RetornaUltimoDiaUtilMes(this.dataInicio, i);

                    i = i + 3;
                }
                else if (this.colunaData == TipoColunaData.Semestre)
                {
                    dataAux = Calendario.RetornaUltimoDiaUtilMes(this.dataInicio, i);

                    i = i + 6;
                }
                else if (this.colunaData == TipoColunaData.Ano)
                {
                    dataAux = Calendario.RetornaUltimoDiaUtilAno(this.dataInicio, i);

                    i = i + 1;
                }
            }

            lista.Add(this.dataFim);

            return lista;
        }

        public string RetornaFaixaEtaria(int anos)
        {
            string faixaEtaria = "";

            if (anos < 10)
            {
                faixaEtaria = "abaixo de 10 anos";
            }
            else if (anos <= 20)
            {
                faixaEtaria = "entre 10 e 20 anos";
            }
            else if (anos <= 30)
            {
                faixaEtaria = "entre 21 e 30 anos";
            }
            else if (anos <= 40)
            {
                faixaEtaria = "entre 31 e 40 anos";
            }
            else if (anos <= 50)
            {
                faixaEtaria = "entre 41 e 50 anos";
            }
            else if (anos <= 60)
            {
                faixaEtaria = "entre 51 e 60 anos";
            }
            else if (anos <= 70)
            {
                faixaEtaria = "entre 61 e 70 anos";
            }
            else if (anos <= 80)
            {
                faixaEtaria = "entre 71 e 80 anos";
            }
            else if (anos <= 90)
            {
                faixaEtaria = "entre 81 e 90 anos";
            }
            else if (anos <= 100)
            {
                faixaEtaria = "entre 91 e 100 anos";
            }
            else if (anos <= 110)
            {
                faixaEtaria = "entre 101 e 110 anos";
            }
            else if (anos <= 120)
            {
                faixaEtaria = "entre 111 e 120 anos";
            }
            else if (anos <= 130)
            {
                faixaEtaria = "entre 121 e 130 anos";
            }
            else if (anos <= 140)
            {
                faixaEtaria = "entre 131 e 140 anos";
            }

            return faixaEtaria;
        }

        public string RetornaDiasCadastro(int dias)
        {
            if (dias < 7)
            {
                return "menos de 1 semana";
            }
            else if (dias < 28)
            {
                return "menos de 1 mês";
            }
            else if (dias <= 31)
            {
                return "aprox. 1 mês";
            }
            else if (dias < 84)
            {
                return "menos de 3 meses";
            }
            else if (dias < 92)
            {
                return "aprox. 3 meses";
            }
            else if (dias < 180)
            {
                return "menos de 6 meses";
            }
            else if (dias < 186)
            {
                return "aprox. 6 meses";
            }
            else if (dias < 360)
            {
                return "menos de 1 ano";
            }
            else if (dias > 1830)
            {
                return "mais de 5 anos";
            }
            else
            {
                return "mais de 5 anos";
            }
        }
    }
}