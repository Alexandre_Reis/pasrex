using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Captacao.Enums 
{
    public enum TipoVisaoFundoRebate
    {
        NaoTrata = 1,
        VisaoDistribuidor = 2,
        VisaoGestor = 3,        
    }
}
