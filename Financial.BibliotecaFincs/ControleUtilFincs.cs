﻿using System;
using System.Collections.Generic;
using System.Text;
using ITreasuryBond = Financial.BibliotecaFincs.ServicosFINCs_ITreasuryBond;
using IListedBonds = Financial.BibliotecaFincs.ServicosFINCs_IListedBonds;
using IGeneral = Financial.BibliotecaFincs.ServicosFINCs_IGeneral;
using System.Collections;
using Financial.Common;
using Financial.RendaFixa.Enums;

namespace Financial.BibliotecaFincs
{
    public enum FincsIndicador
    {
        //moeda
        Dolar = 163,
        Iene = 265,
        Euro = 162,
        Rand = 267,
        NovaLiraTurca = 269,
        
        //indice
        IGPM = 126,//1,
        IPCA = 127,//2,
        IGPDI = 3,
        INPC = 4,

        //taxa
        CDI = 123,
        SELIC = 130,

        //libor
        Libor = 164,
        Libor1M = 204,
        Libor3M = 205,
        Libor6M = 206,
        Libor12M = 224,

        //título da divida agraria
        TDA1 = 225,

        SemCorrecao = 226,
        
        Pre,

        TJLP,

        TR
    }

    public enum Fincs_eJurosTipo
    {
        Simples = -1,
        Composto = 0
    }

    public enum Fincs_eJuros
    {        
        SemJuros = 0,        
        Fixo = 1,
        DiasCorridos = 2,
        DiasCorridos360 = 3,
        DiasCorridos365 = 4,
        DiasUteis = 5,
        DiasUteis252 = 6,
        N12 = 7,
        Meses21252 = 8,
        Meses30360 = 9,
        Meses30365 = 10,        
        USA30360 = 11,
        EUR30360 = 12,
        Libor = 13,
        AnoProRataMes = 14
    }    

    public class ControleUtilFincs
    {
        IGeneral.GeneralClient generalClient = new IGeneral.GeneralClient("BasicHttpBinding_IGeneral");

        public IListedBonds.eIndicador RetornaTipoIndexadorFincs(short? idIndexador, ref Hashtable hsMemoria)
        {
            if (idIndexador.HasValue)
            {
                int tipoDeParaFincs = (int)Financial.Common.Enums.EnumTipoDePara.Fincs_Indexador;
                int tipoCampoFincs = (int)Financial.Common.Enums.TipoDado.Inteiro;

                #region Controle Memória
                Hashtable hsDePara;
                int keyFincsIndexador = (int)Financial.Util.Enums.KeyHashTable.FincsIndexador;
                if (hsMemoria.Contains(keyFincsIndexador))
                    hsDePara = (Hashtable)hsMemoria[keyFincsIndexador];
                else
                {
                    hsDePara = new Hashtable();
                    if (!hsDePara.Contains(tipoDeParaFincs))
                    {
                        TipoDePara tipoDePara = new TipoDePara();
                        tipoDePara.Query.Where(tipoDePara.Query.EnumTipoDePara.Equal(tipoDeParaFincs) & tipoDePara.Query.TipoCampo.Equal(tipoCampoFincs));

                        if (tipoDePara.Query.Load())
                        {
                            DeParaCollection deParaColl = new DeParaCollection();
                            deParaColl.Query.Where(deParaColl.Query.IdTipoDePara.Equal(tipoDePara.IdTipoDePara.Value));

                            if (deParaColl.Query.Load())
                            {
                                hsDePara.Add(tipoDeParaFincs, deParaColl);
                                hsMemoria.Add(keyFincsIndexador, hsDePara);
                            }
                        }
                    }
                }
                #endregion

                List<DePara> lstDePara = new List<DePara>();
                if (hsDePara.Contains(tipoDeParaFincs))
                    lstDePara = (List<DePara>)((DeParaCollection)hsDePara[tipoDeParaFincs]);
                else
                    throw new Exception("Não existe cadastro de DePara (Fincs - Indexador)!");

                DePara dePara = lstDePara.Find(delegate(DePara x) { return x.CodigoInterno.Trim().ToUpper() == idIndexador.Value.ToString().Trim().ToUpper(); });
                if (dePara == null || string.IsNullOrEmpty(dePara.CodigoExterno))
                    throw new Exception("DePara não cadastrado para o Indexador ID - " + idIndexador + " (Fincs - Indexador)!");

                int idExterno = Convert.ToInt32(dePara.CodigoExterno);

                switch (idExterno)
                {
                    case (int)FincsIndicador.CDI:
                        return IListedBonds.eIndicador.CDI;
                    case (int)FincsIndicador.Dolar:
                        return IListedBonds.eIndicador.Dolar;
                    case (int)FincsIndicador.Euro:
                        return IListedBonds.eIndicador.Euro;
                    case (int)FincsIndicador.Iene:
                        return IListedBonds.eIndicador.Iene;
                    case (int)FincsIndicador.IGPDI:
                        return IListedBonds.eIndicador.IGPDI;
                    case (int)FincsIndicador.IGPM:
                        return IListedBonds.eIndicador.IGPM;
                    case (int)FincsIndicador.INPC:
                        return IListedBonds.eIndicador.INPC;
                    case (int)FincsIndicador.IPCA:
                        return IListedBonds.eIndicador.IPCA;
                    case (int)FincsIndicador.Libor:
                        return IListedBonds.eIndicador.Libor;
                    case (int)FincsIndicador.Libor12M:
                        return IListedBonds.eIndicador.Libor12M;
                    case (int)FincsIndicador.Libor1M:
                        return IListedBonds.eIndicador.Libor1M;
                    case (int)FincsIndicador.Libor3M:
                        return IListedBonds.eIndicador.Libor3M;
                    case (int)FincsIndicador.Libor6M:
                        return IListedBonds.eIndicador.Libor6M;
                    case (int)FincsIndicador.NovaLiraTurca:
                        return IListedBonds.eIndicador.NovaLiraTurca;
                    case (int)FincsIndicador.Rand:
                        return IListedBonds.eIndicador.Rand;
                    case (int)FincsIndicador.SELIC:
                        return IListedBonds.eIndicador.SELIC;
                    case (int)FincsIndicador.TDA1:
                        return IListedBonds.eIndicador.TDA1;
                    //case (int)FincsIndicador.Pre:
                    //    return IListedBonds.eIndicador.Pre;
                    //case (int)FincsIndicador.TJLP:
                    //    return IListedBonds.eIndicador.TJLP;
                    //case (int)FincsIndicador.TR:
                    //    return IListedBonds.eIndicador.TR;
                }
            }

            return IListedBonds.eIndicador.SemCorrecao;
        }

        public IListedBonds.eAtivoTipo RetornaAtivoTipoFincs(IListedBonds.eIndicador eIndicador)
        {
            #region Moeda
            List<int> lstMoeda = new List<int>();
            lstMoeda.Add((int)FincsIndicador.Dolar);
            lstMoeda.Add((int)FincsIndicador.Iene);
            lstMoeda.Add((int)FincsIndicador.Euro);
            lstMoeda.Add((int)FincsIndicador.Rand);
            lstMoeda.Add((int)FincsIndicador.NovaLiraTurca);
            #endregion 

            #region Indice
            List<int> lstIndice = new List<int>();
            lstIndice.Add((int)FincsIndicador.IGPM);
            lstIndice.Add((int)FincsIndicador.IPCA);
            lstIndice.Add((int)FincsIndicador.IGPDI);
            lstIndice.Add((int)FincsIndicador.INPC);
            #endregion

            #region Moeda
            List<int> lstTaxa = new List<int>();
            lstTaxa.Add((int)FincsIndicador.CDI);
            lstTaxa.Add((int)FincsIndicador.SELIC);
            #endregion

            #region Libor
            List<int> lstLibor = new List<int>();
            lstLibor.Add((int)FincsIndicador.Libor);
            lstLibor.Add((int)FincsIndicador.Libor1M);
            lstLibor.Add((int)FincsIndicador.Libor3M);
            lstLibor.Add((int)FincsIndicador.Libor6M);
            lstLibor.Add((int)FincsIndicador.Libor12M);
            #endregion

            #region Pre
            List<int> lstPre = new List<int>();
            lstPre.Add((int)FincsIndicador.Pre);
            #endregion

            #region TR
            List<int> lstTR = new List<int>();
            lstTR.Add((int)FincsIndicador.TR);
            #endregion

            #region TJLP
            List<int> lstTJLP = new List<int>();
            lstTJLP.Add((int)FincsIndicador.TJLP);
            #endregion


            if (lstMoeda.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.FC;
            }
            else if (lstIndice.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.Indice;
            }
            else if (lstTaxa.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.Taxa;
            }
            else if (lstLibor.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.Libor;
            }
            else if (lstPre.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.Pre;
            }
            else if (lstTR.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.TR;
            }
            else if (lstTJLP.Contains((int)eIndicador))
            {
                return IListedBonds.eAtivoTipo.TJLP;
            }

            return IListedBonds.eAtivoTipo.Pre; 
        }

        public IListedBonds.eAmortizacaoTipo RetornaCriterioAmortizacao(int? intCriterioAmortizacao)
        {
            if (intCriterioAmortizacao.HasValue)
            {
                switch (intCriterioAmortizacao.Value)
                {
                    case (int)CriterioAmortizacao.PUPar:
                        return IListedBonds.eAmortizacaoTipo.PUPAR;
                    case (int)CriterioAmortizacao.ValorNominalInicialFixo:
                        return IListedBonds.eAmortizacaoTipo.ValorNominalInicialFixo;
                    case (int)CriterioAmortizacao.ValorNominalAtualizado:
                        return IListedBonds.eAmortizacaoTipo.ValorNominalAtualizado;
                    case (int)CriterioAmortizacao.ValorNominalInicial:
                        return IListedBonds.eAmortizacaoTipo.ValorNominalInicial;
                }
            }

            return IListedBonds.eAmortizacaoTipo.ValorNominal;
        }

        public IListedBonds.eValorTipo RetornaValorTipo(IListedBonds.eIndicador eIndicador)
        {
            List<IListedBonds.eIndicador> lstIndicador_Variacao = new List<IListedBonds.eIndicador>();
            lstIndicador_Variacao.Add(IListedBonds.eIndicador.CDI);

            if (lstIndicador_Variacao.Contains(eIndicador))
                return IListedBonds.eValorTipo.Variacao;

            return IListedBonds.eValorTipo.Indice;
        }

        public IListedBonds.ePagamentoTipo RetornaPagamentoTipo(IListedBonds.eIndicador eIndicador)
        {
            List<IListedBonds.eIndicador> lstIndicador_PagamentoTipo = new List<IListedBonds.eIndicador>();
            lstIndicador_PagamentoTipo.Add(IListedBonds.eIndicador.CDI);

            if (lstIndicador_PagamentoTipo.Contains(eIndicador))
                return IListedBonds.ePagamentoTipo.IntegralCorrecaoJuros;

            return IListedBonds.ePagamentoTipo.IntegralJuros;
        }

        public double RetornaCurrentIndex(IListedBonds.eIndicador eIndicador, double currentIndex)
        {
            List<IListedBonds.eIndicador> lstIndicador_SemCurrentIndex = new List<IListedBonds.eIndicador>();
            lstIndicador_SemCurrentIndex.Add(IListedBonds.eIndicador.CDI);
            lstIndicador_SemCurrentIndex.Add(IListedBonds.eIndicador.SELIC);
            lstIndicador_SemCurrentIndex.Add(IListedBonds.eIndicador.SemCorrecao);

            if (lstIndicador_SemCurrentIndex.Contains(eIndicador))
                return double.NaN;

            return currentIndex;
        }

        public IListedBonds.eProRataEmissao RetornaProRataEmissao(string ExecutaProRataEmissao)
        {
            if (ExecutaProRataEmissao.Equals("S"))
                return IListedBonds.eProRataEmissao.Sim;

            return IListedBonds.eProRataEmissao.Nao;
        }

        public IListedBonds.eProRata RetornaProRata(int tipoProRata)
        {
            if (tipoProRata == (int)TipoProRata.DiasCorridos)
                return IListedBonds.eProRata.DiasCorridos;

            return IListedBonds.eProRata.DiasUteis;
        }

        public IListedBonds.eProRataLiquidacao RetornaProRataLiquidacao(string ExecutaProRataLiquidacao)
        {
            if (ExecutaProRataLiquidacao.Equals("S"))
                return IListedBonds.eProRataLiquidacao.Sim;

            return IListedBonds.eProRataLiquidacao.Nao;
        }

        public int RetornaAtivoRegra(string AtivoRegra)
        {
            if (!string.IsNullOrEmpty(AtivoRegra))
                return Convert.ToInt32(AtivoRegra.Trim());

            return 1000;
        }

        public IListedBonds.eJuros Retorna_eJuros(int? eJuros)
        {
            if(eJuros != null)
                return (IListedBonds.eJuros)eJuros;

            return IListedBonds.eJuros.DiasUteis252;
        }

        public IListedBonds.eJurosTipo Retorna_eJurosTipo(int? eJurosTipo)
        {
            if (eJurosTipo != null)
                return (IListedBonds.eJurosTipo)eJurosTipo;

            return IListedBonds.eJurosTipo.Composto;
        }

        public IListedBonds.eTaxa Retorna_eTaxa(int? eTaxa)
        {
            if (eTaxa != null)
                return (IListedBonds.eTaxa)eTaxa;

            return IListedBonds.eTaxa.OverAno;
        }

        /// <summary>
        /// Retorna o objeto eTaxa equivalente ao eJuros cadastrado no título
        /// </summary>
        public IGeneral.eTaxa DeParaEjurosEtaxa(IListedBonds.eJuros eJuros)
        {
            if (eJuros == IListedBonds.eJuros.DiasCorridos360)
                return IGeneral.eTaxa.Ano360BR;

            if (eJuros == IListedBonds.eJuros.DiasCorridos365)
                return IGeneral.eTaxa.Ano365BR;

            if (eJuros == IListedBonds.eJuros.EUR30360)
                return IGeneral.eTaxa.YTM_EUR;

            if (eJuros == IListedBonds.eJuros.USA30360)
                return IGeneral.eTaxa.YTM_USA;

            if (eJuros == IListedBonds.eJuros.Libor)
                return IGeneral.eTaxa.Libor;

            if (eJuros == IListedBonds.eJuros.DiasUteis252)
                return IGeneral.eTaxa.OverAno;            
            
            return IGeneral.eTaxa.OverAno;
        }
    }
}
