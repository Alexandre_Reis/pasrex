using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.BibliotecaFincs.Enums
{
    public static class AtivoRegra
    {
        public const string SemRegraEspecifica = "1";
        public const string Tipo1 = "2000";
        public const string Tipo2 = "3000";
        public const string Tipo3 = "3001";
        public const string Default = "1000";
    }
}
