﻿using System;
using System.Collections.Generic;
using System.Text;
using ITreasuryBond = Financial.BibliotecaFincs.ServicosFINCs_ITreasuryBond;
using IPriceCD = Financial.BibliotecaFincs.ServicosFINCs_IPriceCD;
using IListedBonds = Financial.BibliotecaFincs.ServicosFINCs_IListedBonds;
using IGeneral = Financial.BibliotecaFincs.ServicosFINCs_IGeneral;
using Financial.RendaFixa;
using Financial.Common;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using Financial.Util;
using System.Collections;
using Financial.Common.Enums;
using Financial.RendaFixa.Enums;
using Financial.BibliotecaFincs.ServicosFINCs_IListedBonds;
using System.ServiceModel;
using System.Diagnostics;

namespace Financial.BibliotecaFincs.RendaFixa
{
    public class CalculoRendaFixaFincs
    {
        ITreasuryBond.TreasuryBondClient treasuryBond = new ITreasuryBond.TreasuryBondClient("BasicHttpBinding_ITreasuryBond");
        IListedBonds.ListedBondsClient listed = new IListedBonds.ListedBondsClient("BasicHttpBinding_IListedBonds");
        IPriceCD.CDClient cDClient = new IPriceCD.CDClient("BasicHttpBinding_ICD");
        IGeneral.GeneralClient generalClient = new IGeneral.GeneralClient("BasicHttpBinding_IGeneral");

        public CalculoRendaFixaFincs()
        {            
            generalClient.EmptyCache2();
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idOperacao"></param>
        /// <param name="previa"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_NTNB(DateTime data, DateTime dataVencimento, OperacaoRendaFixa operacaoRendaFixa, double previa, List<double> lstTaxa, double? pu, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            ITreasuryBond.TreasuryBondInfo treasuryBondInfo = new ITreasuryBond.TreasuryBondInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            double? TaxaMemoria = null;

            //Casos em que o título vence em dia não útil
            dataVencimento = data > dataVencimento ? data : dataVencimento;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            if (lstTaxa != null)
                treasuryBondInfo = treasuryBond.NTNB_MTM_Frac_ModeloAnbima(data, dataVencimento, lstTaxa.ToArray(), previa, dataAnterior, double.NaN, ITreasuryBond.eTaxa.OverAno);
            else
            {
                treasuryBondInfo = treasuryBond.NTNB_Rate_Frac_ModeloAnbima(data, dataVencimento, pu.Value, previa, dataAnterior, double.NaN, ITreasuryBond.eTaxa.OverAno);
                TaxaMemoria = treasuryBondInfo.Taxa;
            }

            if (persisteMemoria)
                this.GeraMemoriaCalculoPagamentosFincs(data, dataAnterior, treasuryBondInfo.Pagamentos, null, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

            if (data >= dataVencimento)
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(treasuryBondInfo.Pagamentos[treasuryBondInfo.Pagamentos.Length - 1].FinalValue);
            else
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : treasuryBondInfo.MTMValueSum);       

            //Calcula a TIR depois de persistir a memória de cálculo (Caso tenha vários vertices)
            if (lstTaxa != null && lstTaxa.Count > 1)
                posicaoAtualizada.Taxa = Convert.ToDecimal((treasuryBond.NTNB_Rate_Frac_ModeloAnbima(data, dataVencimento, (double)posicaoAtualizada.PuAtualizado, previa, dataAnterior, double.NaN, ITreasuryBond.eTaxa.OverAno)).Taxa);
            else
                posicaoAtualizada.Taxa = Convert.ToDecimal(treasuryBondInfo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idOperacao"></param>
        /// <param name="previa"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_NTNC(DateTime data, DateTime dataVencimento, OperacaoRendaFixa operacaoRendaFixa, double previa, List<double> lstTaxa, double? pu, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            ITreasuryBond.TreasuryBondInfo treasuryBondInfo = new ITreasuryBond.TreasuryBondInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            double? TaxaMemoria = null;

            //Casos em que o título vence em dia não útil
            dataVencimento = data > dataVencimento ? data : dataVencimento;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            if (lstTaxa != null)
                treasuryBondInfo = treasuryBond.NTNC_MTM_Frac_ModeloAnbima(data, dataVencimento, lstTaxa.ToArray(), previa, dataAnterior, ITreasuryBond.eTaxa.OverAno);
            else
            {
                treasuryBondInfo = treasuryBond.NTNC_Rate_Frac_ModeloAnbima(data, dataVencimento, pu.Value, previa, dataAnterior, ITreasuryBond.eTaxa.OverAno);
                TaxaMemoria = treasuryBondInfo.Taxa;
            }

            if (persisteMemoria)
                this.GeraMemoriaCalculoPagamentosFincs(data, dataAnterior, treasuryBondInfo.Pagamentos, null, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

            if (data >= dataVencimento)
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(treasuryBondInfo.Pagamentos[treasuryBondInfo.Pagamentos.Length - 1].FinalValue);
            else
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : treasuryBondInfo.MTMValueSum);  

            //Calcula a TIR depois de persistir a memória de cálculo (Caso tenha vários vertices)
            if (lstTaxa != null && lstTaxa.Count > 1)
                posicaoAtualizada.Taxa = Convert.ToDecimal((treasuryBond.NTNC_Rate_Frac_ModeloAnbima(data, dataVencimento, (double)posicaoAtualizada.PuAtualizado, previa, dataAnterior, ITreasuryBond.eTaxa.OverAno)).Taxa);
            else
                posicaoAtualizada.Taxa = Convert.ToDecimal(treasuryBondInfo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idOperacao"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_NTNF(DateTime data, DateTime dataVencimento, OperacaoRendaFixa operacaoRendaFixa, List<double> lstTaxa, double? pu, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            ITreasuryBond.TreasuryBondInfo treasuryBondInfo = new ITreasuryBond.TreasuryBondInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            double? TaxaMemoria = null;

            //Casos em que o título vence em dia não útil
            dataVencimento = data > dataVencimento ? data : dataVencimento;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            if (lstTaxa != null)
                treasuryBondInfo = treasuryBond.NTNF_MTM_Frac_ModeloAnbima(data, dataVencimento, lstTaxa.ToArray(), dataAnterior, ITreasuryBond.eTaxa.OverAno);
            else
                treasuryBondInfo = treasuryBond.NTNF_Rate_Frac_ModeloAnbima(data, dataVencimento, new double[] { pu.Value }, dataAnterior, ITreasuryBond.eTaxa.OverAno);

            if (persisteMemoria)
                this.GeraMemoriaCalculoPagamentosFincs(data, treasuryBondInfo.Pagamentos, null, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

            if (data >= dataVencimento)
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(treasuryBondInfo.Pagamentos[treasuryBondInfo.Pagamentos.Length - 1].FinalValue);
            else
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : treasuryBondInfo.MTMValueSum);  

            //Calcula a TIR depois de persistir a memória de cálculo (Caso tenha vários vertices)
            if (lstTaxa != null && lstTaxa.Count > 1)
                posicaoAtualizada.Taxa = Convert.ToDecimal((treasuryBond.NTNF_Rate_Frac_ModeloAnbima(data, dataVencimento, new double[] { (double)posicaoAtualizada.PuAtualizado }, dataAnterior, ITreasuryBond.eTaxa.OverAno)).Taxa);
            else
                posicaoAtualizada.Taxa = Convert.ToDecimal(treasuryBondInfo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idOperacao"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_LFT(DateTime data, DateTime dataVencimento, OperacaoRendaFixa operacaoRendaFixa, List<double> lstTaxa, double? pu, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            double VNA = 0, taxa = 0, mtm = 0;
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();

            //Casos em que o título vence em dia não útil
            dataVencimento = data > dataVencimento ? data : dataVencimento;

            VNA = treasuryBond.VNA_LFT_Anbima(data);

            if (lstTaxa != null)
                mtm = treasuryBond.LFT_MTM(data, dataVencimento, lstTaxa[0], null, null, ITreasuryBond.eTaxa.OverAno);
            else
                taxa = treasuryBond.LFT_Rate(data, dataVencimento, (double)pu, null, null, ITreasuryBond.eTaxa.OverAno);

            if (persisteMemoria)
            {
                double taxaMemoria = (lstTaxa != null ? lstTaxa[0] : taxa);
                double puMemoria = (pu.HasValue ? pu.Value : mtm);
                this.GeraMemoriaCalculoFincs(data, dataVencimento, taxaMemoria, puMemoria, VNA, 0, 0, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa);
            }

            posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : mtm);

            if (lstTaxa != null)
                posicaoAtualizada.Taxa = Convert.ToDecimal(lstTaxa[0]);
            else
                posicaoAtualizada.Taxa = Convert.ToDecimal(taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idOperacao"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_LTN(DateTime data, DateTime dataVencimento, OperacaoRendaFixa operacaoRendaFixa, List<double> lstTaxa, double? pu, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            double taxa = 0, mtm = 0;
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();

            //Casos em que o título vence em dia não útil
            dataVencimento = data > dataVencimento ? data : dataVencimento;

            if (lstTaxa != null)
                mtm = treasuryBond.LTN_MTM(data, dataVencimento, lstTaxa[0], ITreasuryBond.eTaxa.OverAno);
            else
                taxa = treasuryBond.LTN_Rate(data, dataVencimento, pu.Value, ITreasuryBond.eTaxa.OverAno);

            if (persisteMemoria)
            {
                double taxaMemoria = (lstTaxa != null ? lstTaxa[0] : taxa);
                double puMemoria = (pu.HasValue ? pu.Value : mtm);
                this.GeraMemoriaCalculoFincs(data, dataVencimento, taxaMemoria, puMemoria, 1000, 0, 0, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa);
            }

            posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : mtm);

            if (lstTaxa != null)
                posicaoAtualizada.Taxa = Convert.ToDecimal(lstTaxa[0]);
            else
                posicaoAtualizada.Taxa = Convert.ToDecimal(taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dtEmissao"></param>
        /// <param name="dtVencimento"></param>
        /// <param name="taxaTitulo"></param>
        /// <param name="periodicidade"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="idOperacao"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaOffShoreGLOBALS(DateTime data, DateTime dtEmissao, DateTime dtVencimento, int defasagem, double taxaTitulo, int periodicidade, double valorNominal, List<double> lstTaxa, double? pu, OperacaoRendaFixa operacaoRendaFixa, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            ITreasuryBond.AtivoInfo ativoInfo = new ITreasuryBond.AtivoInfo();
            ITreasuryBond.AtivoInfo ativoInfoTaxa = new ITreasuryBond.AtivoInfo();
            ITreasuryBond.AtivoInfo ativoInfoTaxaLimpo = new ITreasuryBond.AtivoInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            double? TaxaMemoria = null;

            if (lstTaxa != null)
                ativoInfo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, lstTaxa.ToArray(), ITreasuryBond.eTaxaPUTipo.Taxa, dtEmissao, ITreasuryBond.eTaxa.YTM_USA, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);            
            else
                ativoInfo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { pu.Value }, ITreasuryBond.eTaxaPUTipo.PU, dtEmissao, ITreasuryBond.eTaxa.YTM_USA, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);

            ativoInfoTaxa = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { ativoInfo.MTMValueSum }, ITreasuryBond.eTaxaPUTipo.PU, dtEmissao, ITreasuryBond.eTaxa.YTM_USA, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);
            ativoInfoTaxaLimpo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { ativoInfoTaxa.PULimpo }, ITreasuryBond.eTaxaPUTipo.PULimpo, dtEmissao, ITreasuryBond.eTaxa.YTM_USA, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);
            
            if (persisteMemoria)
                this.GeraMemoriaCalculoPagamentosFincs(data, ativoInfoTaxaLimpo.Pagamentos, null, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

            posicaoAtualizada.PuAtualizado = Convert.ToDecimal((pu.HasValue ? pu.Value : ativoInfoTaxaLimpo.MTMValueSum));
            posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfoTaxaLimpo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dtEmissao"></param>
        /// <param name="dtVencimento"></param>
        /// <param name="prazoLiquidacao"></param>
        /// <param name="taxaTitulo"></param>
        /// <param name="periodicidade"></param>
        /// <param name="valorNominal"></param>
        /// <param name="puLimpo"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaOffShoreGLOBALS(DateTime data, DateTime dtEmissao, DateTime dtVencimento, int defasagem, double taxaTitulo, int periodicidade, double valorNominal, double? puLimpo)
        {
            ITreasuryBond.AtivoInfo ativoInfoTaxaLimpo = new ITreasuryBond.AtivoInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();

            ativoInfoTaxaLimpo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { puLimpo.Value }, ITreasuryBond.eTaxaPUTipo.PULimpo, dtEmissao, ITreasuryBond.eTaxa.YTM_USA, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);

            try
            {
                posicaoAtualizada.PuAtualizado = Convert.ToDecimal(ativoInfoTaxaLimpo.MTMValueSum);
            }
            catch (OverflowException ex)
            {
                posicaoAtualizada.PuAtualizado = 0;
            }

            try
            {
                posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfoTaxaLimpo.Taxa);
            }
            catch (OverflowException ex)
            {
                posicaoAtualizada.Taxa = 0;
            }

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dtEmissao"></param>
        /// <param name="dtVencimento"></param>
        /// <param name="taxaTitulo"></param>
        /// <param name="periodicidade"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="idOperacao"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaOffShoreTBILL(DateTime data, DateTime dtEmissao, DateTime dtVencimento, int defasagem, double taxaTitulo, int periodicidade, double valorNominal, List<double> lstTaxa, double? pu, OperacaoRendaFixa operacaoRendaFixa, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria)
        {
            ITreasuryBond.AtivoInfo ativoInfo = new ITreasuryBond.AtivoInfo();
            ITreasuryBond.AtivoInfo ativoInfoTaxa = new ITreasuryBond.AtivoInfo();
            ITreasuryBond.AtivoInfo ativoInfoTaxaLimpo = new ITreasuryBond.AtivoInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            double? TaxaMemoria = null;
            
            if (lstTaxa != null)
                ativoInfo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, lstTaxa.ToArray(), ITreasuryBond.eTaxaPUTipo.Taxa, dtEmissao, ITreasuryBond.eTaxa.Calendar360, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);
            else
                ativoInfo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { pu.Value }, ITreasuryBond.eTaxaPUTipo.PU, dtEmissao, ITreasuryBond.eTaxa.Calendar360, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);

            ativoInfoTaxa = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { ativoInfo.MTMValueSum }, ITreasuryBond.eTaxaPUTipo.PU, dtEmissao, ITreasuryBond.eTaxa.Calendar360, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);
            ativoInfoTaxaLimpo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { ativoInfoTaxa.PULimpo }, ITreasuryBond.eTaxaPUTipo.PULimpo, dtEmissao, ITreasuryBond.eTaxa.Calendar360, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);
            
            if (persisteMemoria)
                this.GeraMemoriaCalculoPagamentosFincs(data, ativoInfoTaxaLimpo.Pagamentos, null, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

            posicaoAtualizada.PuAtualizado = Convert.ToDecimal((pu.HasValue ? pu.Value : ativoInfoTaxaLimpo.MTMValueSum));
            posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfoTaxaLimpo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dtEmissao"></param>
        /// <param name="dtVencimento"></param>
        /// <param name="prazoLiquidacao"></param>
        /// <param name="taxaTitulo"></param>
        /// <param name="periodicidade"></param>
        /// <param name="valorNominal"></param>
        /// <param name="puLimpo"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaOffShoreTBILL(DateTime data, DateTime dtEmissao, DateTime dtVencimento, int defasagem, double taxaTitulo, int periodicidade, double valorNominal, double? puLimpo)
        {
            ITreasuryBond.AtivoInfo ativoInfoTaxaLimpo = new ITreasuryBond.AtivoInfo();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();

            ativoInfoTaxaLimpo = treasuryBond.GlobalBonds_MTM2(data, dtVencimento, defasagem, valorNominal, taxaTitulo, periodicidade, ITreasuryBond.eJuros.USA30360, new double[] { puLimpo.Value }, ITreasuryBond.eTaxaPUTipo.PULimpo, dtEmissao, ITreasuryBond.eTaxa.Calendar360, "NYC", ITreasuryBond.eDaysCounterType.CalendarDays);

            posicaoAtualizada.PuAtualizado = Convert.ToDecimal(ativoInfoTaxaLimpo.MTMValueSum);
            posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfoTaxaLimpo.Taxa);

            return posicaoAtualizada;
        }

        /// <summary>
        /// Retorna apenas o valor nominal atualizado
        /// </summary>
        /// <param name="data"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="previa"></param>
        /// <returns></returns>
        public decimal RetornaValorNominalAtualizado(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, double previa, ref Hashtable hsMemoria)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            IListedBonds.eTaxa eTaxa = controleUtilFincs.Retorna_eTaxa(tituloRendaFixa.ETaxa);
            //Lista de Eventos
            List<IListedBonds.Evento> listaEventos = this.MontaListaEventos(data, tituloRendaFixa);

            //Ativo
            IListedBonds.Ativo bondObj = MontaBondObj(tituloRendaFixa, listaEventos, ref hsMemoria);

            previa = controleUtilFincs.RetornaCurrentIndex(bondObj.Indicador, previa);

            List<double> lstExpectedRateRange = new List<double>();
            lstExpectedRateRange.Add(0);

            List<Pagamento> lstPagamento = new List<Pagamento>();
            lstPagamento.AddRange(listed.ListedBond_Frac_MTMValueArray(bondObj, data, lstExpectedRateRange.ToArray(), IListedBonds.eTaxaPUTipo.Taxa, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN).Pagamentos);
            
            decimal valorNominal = RetornaValorNominalCriterioAmortizacao(data, (CriterioAmortizacao)tituloRendaFixa.CriterioAmortizacao, lstPagamento);

            return valorNominal;
        }

        /// <summary>
        /// Retorna apenas o valor nominal atualizado
        /// </summary>
        /// <param name="data"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="previa"></param>
        /// <returns></returns>
        public decimal RetornaValorNominalAtualizadoAtivoExotico(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, double previa, ref Hashtable hsMemoria)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            IListedBonds.eTaxa eTaxa = controleUtilFincs.Retorna_eTaxa(tituloRendaFixa.ETaxa);

            //Lista de Eventos
            List<IListedBonds.EventoExotico> listaEventos = this.MontaListaEventosExoticos(data, tituloRendaFixa);

            //Ativo
            IListedBonds.AtivoExotico bondObj = MontaBondObjExotico(tituloRendaFixa, listaEventos, ref hsMemoria);

            previa = controleUtilFincs.RetornaCurrentIndex(bondObj.Indicador, previa);

            List<Pagamento> lstPagamento = new List<Pagamento>();

            lstPagamento.AddRange(listed.ListedBond_Frac_ExoticIndexMTM(bondObj, data, new double[] { 0 }, IListedBonds.eTaxaPUTipo.PU, eTaxa, new double[] { 0 }, previa, null, double.NaN).Pagamentos);

            decimal valorNominal = RetornaValorNominalCriterioAmortizacao(data, (CriterioAmortizacao)tituloRendaFixa.CriterioAmortizacao, lstPagamento);

            return valorNominal;
        }

        /// <summary>
        /// Calcula os valores da Posicao usando a biblioteca da Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="previa"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <param name="hsMemoria">HashTable para diminuir o acesso ao BD repetitivamente, a Key deve ser setada no Enum do Projeto Util</param>
        /// <param name="CalculaNominalAtualizado">Calcula nominal atualizado com outro método (Usado para converter Pu em taxa e vice-versa na tela de Agenda)</param>
        /// <returns>Posição Atualizada</returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_Geral(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, List<double> lstTaxa, double? pu, double previa, List<double> lstExpectedRateRange, OperacaoRendaFixa operacaoRendaFixa, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria, ref Hashtable hsMemoria)
        {
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            double? TaxaMemoria = null;

            IListedBonds.eTaxa eTaxa = controleUtilFincs.Retorna_eTaxa(tituloRendaFixa.ETaxa);

            try
            {
                //Lista de Eventos
                List<IListedBonds.Evento> listaEventos = this.MontaListaEventos(data, tituloRendaFixa);

                //Ativo
                IListedBonds.Ativo bondObj = MontaBondObj(tituloRendaFixa, listaEventos, ref hsMemoria);

                previa = controleUtilFincs.RetornaCurrentIndex(bondObj.Indicador, previa);

                if (lstExpectedRateRange == null || lstExpectedRateRange.Count == 0)
                {
                    lstExpectedRateRange = new List<double>();
                    lstExpectedRateRange.Add(0);
                }

                IListedBonds.AtivoInfo ativoInfo;
                if (lstTaxa != null)
                {
                    ativoInfo = listed.ListedBond_Frac_MTMValueArray(bondObj, data, lstTaxa.ToArray(), IListedBonds.eTaxaPUTipo.Taxa, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                }
                else
                {
                    ativoInfo = listed.ListedBond_Frac_MTMValueArray(bondObj, data, new double[] { (double)pu.Value }, IListedBonds.eTaxaPUTipo.PU, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                    TaxaMemoria = ativoInfo.Taxa;
                }

                if (persisteMemoria)
                    this.GeraMemoriaCalculoPagamentosFincs(data, null, ativoInfo.Pagamentos, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

                if (data >= tituloRendaFixa.DataVencimento.Value)
                    posicaoAtualizada.PuAtualizado = Convert.ToDecimal(ativoInfo.Pagamentos[ativoInfo.Pagamentos.Length - 1].FinalValue);
                else
                    posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : ativoInfo.MTMValueSum);

                //Chama novamente para o cálculo da TIR (Caso tenha vários vertices)
                if (lstTaxa != null && lstTaxa.Count > 1)
                {
                    ativoInfo = listed.ListedBond_Frac_MTMValueArray(bondObj, data, new double[] { (double)posicaoAtualizada.PuAtualizado }, IListedBonds.eTaxaPUTipo.PU, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                }

                if (!double.NaN.Equals(ativoInfo.Taxa))
                    posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfo.Taxa);
                else
                {
                    if (data >= tituloRendaFixa.DataVencimento.Value)
                        posicaoAtualizada.Taxa = 0;
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                string descricaoTitulo = string.IsNullOrEmpty(tituloRendaFixa.DescricaoCompleta.Trim()) ? tituloRendaFixa.Descricao : tituloRendaFixa.DescricaoCompleta;
                descricaoTitulo = tituloRendaFixa.IdTitulo + " - " + descricaoTitulo;
                throw new Exception("Não foi possivel calcular a taxa/pu com os parâmetros informados, verificar cadastro do título - " + RetornaDescricaoTitulo(tituloRendaFixa) + "!!! ERRO - " + ex.Message);
            }

            return posicaoAtualizada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="lstTaxa"></param>
        /// <param name="pu"></param>
        /// <param name="previa"></param>
        /// <param name="lstExpectedRateRange"></param>
        /// <param name="idOperacao"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <param name="hsMemoria"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada Calcula_AtivoExotico(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, List<double> lstTaxa, double? pu, double previa, List<double> lstExpectedRateRange, OperacaoRendaFixa operacaoRendaFixa, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria, ref Hashtable hsMemoria)
        {
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            IListedBonds.eTaxa eTaxa = controleUtilFincs.Retorna_eTaxa(tituloRendaFixa.ETaxa);
            double? TaxaMemoria = null;

            try
            {
                //Lista de Eventos
                List<IListedBonds.EventoExotico> listaEventos = this.MontaListaEventosExoticos(data, tituloRendaFixa);

                //Ativo
                IListedBonds.AtivoExotico ativoExotico = this.MontaBondObjExotico(tituloRendaFixa, listaEventos, ref hsMemoria);

                previa = controleUtilFincs.RetornaCurrentIndex(ativoExotico.Indicador, previa);

                if (lstExpectedRateRange == null || lstExpectedRateRange.Count == 0)
                {
                    lstExpectedRateRange = new List<double>();
                    lstExpectedRateRange.Add(0);
                }

                //RetornaStringManutencaoExotico(data, previa, eTaxa, ativoExotico, listaEventos);

                IListedBonds.AtivoInfo ativoInfo;
                if (lstTaxa != null)
                {
                    ativoInfo = listed.ListedBond_Frac_ExoticIndexMTM(ativoExotico, data, lstTaxa.ToArray(), IListedBonds.eTaxaPUTipo.Taxa, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                }
                else
                {
                    ativoInfo = listed.ListedBond_Frac_ExoticIndexMTM(ativoExotico, data, new double[] { (double)pu.Value }, IListedBonds.eTaxaPUTipo.PU, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                    TaxaMemoria = ativoInfo.Taxa;
                }                

                if (persisteMemoria)
                    this.GeraMemoriaCalculoPagamentosFincs(data, null, ativoInfo.Pagamentos, TaxaMemoria, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, true);

                if (data >= tituloRendaFixa.DataVencimento.Value)
                    posicaoAtualizada.PuAtualizado = Convert.ToDecimal(ativoInfo.Pagamentos[ativoInfo.Pagamentos.Length - 1].FinalValue);
                else
                    posicaoAtualizada.PuAtualizado = Convert.ToDecimal(pu.HasValue ? pu.Value : ativoInfo.MTMValueSum);

                //Chama novamente para o cálculo da TIR (Caso tenha vários vertices)
                if (lstTaxa != null && lstTaxa.Count > 1)
                {
                    ativoInfo = listed.ListedBond_Frac_ExoticIndexMTM(ativoExotico, data, new double[] { (double)posicaoAtualizada.PuAtualizado }, IListedBonds.eTaxaPUTipo.PU, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);
                }

                if (!double.NaN.Equals(ativoInfo.Taxa))
                    posicaoAtualizada.Taxa = Convert.ToDecimal(ativoInfo.Taxa);
                else
                {
                    if (data >= tituloRendaFixa.DataVencimento.Value)
                        posicaoAtualizada.Taxa = 0;
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                string descricaoTitulo = string.IsNullOrEmpty(tituloRendaFixa.DescricaoCompleta.Trim()) ? tituloRendaFixa.Descricao : tituloRendaFixa.DescricaoCompleta;
                descricaoTitulo = tituloRendaFixa.IdTitulo + " - " + descricaoTitulo;
                throw new Exception("Não foi possivel calcular a taxa/pu com os parâmetros informados, verificar cadastro do título - " + RetornaDescricaoTitulo(tituloRendaFixa) + "!!! ERRO - " + ex.Message);
            }

            return posicaoAtualizada;
        }

        /// <summary>
        /// O método tenta construir uma classe, para ser enviada a Fincs facilitando a analise deles
        /// </summary>
        /// <param name="data"></param>
        /// <param name="previa"></param>
        /// <param name="ativoExotico"></param>
        /// <param name="listaEventos"></param>
        [Conditional("DEBUG")]
        private void RetornaStringManutencaoExotico(DateTime data, double previa, eTaxa eTaxa, IListedBonds.AtivoExotico ativoExotico, List<IListedBonds.EventoExotico> listaEventos)
        {
            StringBuilder strMetodo = new StringBuilder();

            strMetodo.AppendLine("public void MetodoTeste()");
            strMetodo.AppendLine("{");

            StringBuilder strParametros = new StringBuilder();
            strParametros.AppendLine("\t\t double previa = " + previa.ToString().Replace(',', '.') + ";");
            strParametros.AppendLine("\t\t DateTime data = new DateTime(" + data.Year + "," + data.Month + "," + data.Day + ");");
            strParametros.AppendLine("\t\t List<IListedBonds.Arredondamento> listArredondamentoCorrecao = new List<IListedBonds.Arredondamento>(); ");
            strParametros.AppendLine("\t\t List<IListedBonds.Arredondamento> listArredondamentoJuros = new List<IListedBonds.Arredondamento>(); ");

            strParametros.AppendLine("\t\t List<double> lstExpectedRateRange = new List<double>();");
            strParametros.AppendLine("\t\t lstExpectedRateRange.Add(0);");

            strParametros.AppendLine("\t\t IListedBonds.eTaxa eTaxa = (eTaxa)" + (int)eTaxa + ";");
            strParametros.AppendLine(string.Empty);

            #region Eventos
            StringBuilder strEventos = new StringBuilder();
            strEventos.AppendLine("\t\t List<IListedBonds.EventoExotico> listaEventos = new List<IListedBonds.EventoExotico>();");
            strEventos.AppendLine("\t\t IListedBonds.EventoExotico evento = new IListedBonds.EventoExotico();");
            foreach (EventoExotico evento in listaEventos)
            {
                strEventos.AppendLine(string.Empty);
                strEventos.AppendLine("\t\t evento.AmortizacaoPercentual = " + evento.AmortizacaoPercentual.ToString().Replace(',', '.') + ";");
                strEventos.AppendLine("\t\t evento.CorrecaoData = new DateTime(" + evento.CorrecaoData.Value.Year + "," + evento.CorrecaoData.Value.Month + "," + evento.CorrecaoData.Value.Day + ");");
                strEventos.AppendLine("\t\t evento.CorrecaoIncorporacaoPercentual = " + evento.CorrecaoIncorporacaoPercentual + ";");
                strEventos.AppendLine("\t\t evento.Data = new DateTime(" + evento.Data.Year + "," + evento.Data.Month + "," + evento.Data.Day + ");");
                strEventos.AppendLine("\t\t evento.Juros = (eJuros)" + (int)evento.Juros + ";");
                strEventos.AppendLine("\t\t evento.JurosIncorporacaoPercentual = " + evento.JurosIncorporacaoPercentual.ToString().Replace(',', '.') + ";");
                strEventos.AppendLine("\t\t evento.JurosPercentual = " + evento.JurosPercentual.ToString().Replace(',', '.') + ";");
                strEventos.AppendLine("\t\t evento.JurosTipo = (eJurosTipo)" + (int)evento.JurosTipo + ";");

                strEventos.AppendLine("\t\t listaEventos.Add(evento);");
            }
            #endregion

            #region Ativo
            StringBuilder strAtivo = new StringBuilder();
            strAtivo.AppendLine(string.Empty);
            strAtivo.AppendLine("\t\t IListedBonds.AtivoExotico ativoExotico = new IListedBonds.AtivoExotico();");
            strAtivo.AppendLine("\t\t ativoExotico.AmortizacaoTipo = (eAmortizacaoTipo)" + (int)ativoExotico.AmortizacaoTipo + ";");
            strAtivo.AppendLine("\t\t ativoExotico.Indicador = (eIndicador)" + (int)ativoExotico.Indicador + ";");
            strAtivo.AppendLine("\t\t ativoExotico.AtivoTipo = (eAtivoTipo)" + (int)ativoExotico.AtivoTipo + ";");
            strAtivo.AppendLine("\t\t ativoExotico.ValorTipo = (eValorTipo)" + (int)ativoExotico.ValorTipo + ";");
            strAtivo.AppendLine("\t\t ativoExotico.PagamentoTipo = (ePagamentoTipo)" + (int)ativoExotico.PagamentoTipo + ";");
            strAtivo.AppendLine("\t\t ativoExotico.ProRata = (eProRata)" + (int)ativoExotico.ProRata + ";");
            strAtivo.AppendLine("\t\t ativoExotico.DataTipo = (eProRataEmissao)" + (int)ativoExotico.DataTipo + ";");
            strAtivo.AppendLine("\t\t ativoExotico.Emissao = new DateTime(" + ativoExotico.Emissao.Year + "," + ativoExotico.Emissao.Month + "," + ativoExotico.Emissao.Day + ");");
            strAtivo.AppendLine("\t\t ativoExotico.ValorEmissao = " + ativoExotico.ValorEmissao.ToString().Replace(',', '.') + ";");
            strAtivo.AppendLine("\t\t ativoExotico.Vencimento = new DateTime(" + ativoExotico.Vencimento.Value.Year + "," + ativoExotico.Vencimento.Value.Month + "," + ativoExotico.Vencimento.Value.Day + ");");
            strAtivo.AppendLine("\t\t ativoExotico.Eventos = listaEventos.ToArray();");
            strAtivo.AppendLine("\t\t ativoExotico.ArredondamentosCorrecao = listArredondamentoCorrecao.ToArray();");
            strAtivo.AppendLine("\t\t ativoExotico.ArredondamentosJuros = listArredondamentoJuros.ToArray();");
            strAtivo.AppendLine("\t\t ativoExotico.DataInicioCorrecao = new DateTime(" + ativoExotico.DataInicioCorrecao.Year + "," + ativoExotico.DataInicioCorrecao.Month + "," + ativoExotico.DataInicioCorrecao.Day + ");");
            strAtivo.AppendLine("\t\t ativoExotico.ProRataEmissaoDia = " + (int)ativoExotico.ProRataEmissaoDia + ";");
            strAtivo.AppendLine("\t\t ativoExotico.ProRataLiquidacao = (eProRataLiquidacao)" + (int)ativoExotico.ProRataLiquidacao + ";");
            strAtivo.AppendLine("\t\t ativoExotico.AtivoRegra = " + (int)ativoExotico.AtivoRegra + ";");

            strAtivo.AppendLine(string.Empty);
            #endregion

            #region Lista Arredondamento de Correcao
            StringBuilder strArredondamento = new StringBuilder();
            strArredondamento.AppendLine("\t\t Arredondamento arredondamento = new IListedBonds.Arredondamento();");
            foreach (Arredondamento arredondamento in ativoExotico.ArredondamentosCorrecao)
            {
                strArredondamento.AppendLine("\t\t arredondamento = new IListedBonds.Arredondamento();");
                strArredondamento.AppendLine("\t\t arredondamento.CasasDecimais = " + arredondamento.CasasDecimais + ";");
                if ((int)arredondamento.Forma == (int)eForma.Truncar)
                    strArredondamento.AppendLine("\t\t arredondamento.Forma = eForma.Truncar;");
                else
                    strArredondamento.AppendLine("\t\t arredondamento.Forma = (eForma)" + (int)arredondamento.Forma + ";");
                strArredondamento.AppendLine("\t\t listArredondamentoCorrecao.Add(arredondamento);");

                strArredondamento.AppendLine(string.Empty);
            }

            foreach (Arredondamento arredondamento in ativoExotico.ArredondamentosJuros)
            {
                strArredondamento.AppendLine("\t\t arredondamento = new IListedBonds.Arredondamento();");
                strArredondamento.AppendLine("\t\t arredondamento.CasasDecimais = " + arredondamento.CasasDecimais + ";");
                if ((int)arredondamento.Forma == (int)eForma.Truncar)
                    strArredondamento.AppendLine("\t\t arredondamento.Forma = eForma.Truncar;");
                else
                    strArredondamento.AppendLine("\t\t arredondamento.Forma = (eForma)" + (int)arredondamento.Forma + ";");
                strArredondamento.AppendLine("\t\t listArredondamentoJuros.Add(arredondamento);");

                strArredondamento.AppendLine(string.Empty);
            }           
            #endregion

            StringBuilder strChamada = new StringBuilder();
            strChamada.AppendLine(string.Empty);
            strChamada.AppendLine("\t\t listed.ListedBond_Frac_ExoticIndexMTM(ativoExotico, data, lstExpectedRateRange.ToArray(), IListedBonds.eTaxaPUTipo.Taxa, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN);");

            strMetodo.Append(strParametros.ToString() + strEventos.ToString() + strArredondamento.ToString() + strAtivo.ToString() + strChamada.ToString());

            strMetodo.AppendLine("}");
        }    

        #region Memoria de Calculo
        private void GeraMemoriaCalculoPagamentosFincs(DateTime data, ITreasuryBond.Pagamento[] TreasuryBondsPagamentos, IListedBonds.Pagamento[] ListedBondsPagamentos, double? taxa, int enumTipoPreco, int enumTipoProcessamento, OperacaoRendaFixa operacaoRendaFixa, bool quebraRegistros)
        {
            this.GeraMemoriaCalculoPagamentosFincs(data, null, TreasuryBondsPagamentos, ListedBondsPagamentos, taxa, enumTipoPreco, enumTipoProcessamento, operacaoRendaFixa, quebraRegistros);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="TreasuryBondsPagamentos"></param>
        /// <param name="ListedBondsPagamentos"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="idOperacao"></param>
        private void GeraMemoriaCalculoPagamentosFincs(DateTime data, DateTime? dataAnterior, ITreasuryBond.Pagamento[] TreasuryBondsPagamentos, IListedBonds.Pagamento[] ListedBondsPagamentos, double? taxa, int enumTipoPreco, int enumTipoProcessamento, OperacaoRendaFixa operacaoRendaFixa, bool quebraRegistros)
        {
            int idOperacao = operacaoRendaFixa.IdOperacao.Value;
            int idCliente = operacaoRendaFixa.IdCliente.Value;

            MemoriaCalculoRendaFixaCollection memoriaColl = new MemoriaCalculoRendaFixaCollection();
            memoriaColl.DeletaMemoriaCalculo(idCliente, data, idOperacao, enumTipoPreco, enumTipoProcessamento);

            if (TreasuryBondsPagamentos != null)
            {
                foreach (ITreasuryBond.Pagamento pagamento in TreasuryBondsPagamentos)
                {
                    if (dataAnterior.HasValue)
                    {
                        if (pagamento.EventDate < dataAnterior.Value)
                            continue;
                    }
                    else
                    {
                        if (pagamento.EventDate < data)
                            continue;
                    }

                    if (quebraRegistros)
                    {
                        if (pagamento.Amortization + pagamento.Interest == 0 && pagamento.IndexValue > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.IndexValue);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.PagamentoCorrecao;
                            memoriaCalculo.TaxaDesconto = 0;
                            memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.IndexValue);
                        }

                        if (pagamento.Amortization > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Amortization + pagamento.IndexValue);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Amortizacao;
                            memoriaCalculo.TaxaDesconto = Convert.ToDecimal(pagamento.Amortization / pagamento.Notional) * 100;

                            if (data == pagamento.EventDate)
                            {
                                memoriaCalculo.ValorPresente = 0;
                                memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.FinalValue) - Convert.ToDecimal(pagamento.Interest);
                            }
                            else
                            {
                                memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMAmortization);
                            }
                        }

                        if (pagamento.Interest > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Interest);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Juros;

                            if (taxa.HasValue)
                                memoriaCalculo.TaxaDesconto = Convert.ToDecimal(taxa.Value);
                            else
                                memoriaCalculo.TaxaDesconto = Convert.ToDecimal(pagamento.Rate);

                            if (data == pagamento.EventDate)
                            {
                                memoriaCalculo.ValorPresente = 0;
                            }
                            else
                            {
                                memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMInterest);
                            }
                        }
                    }
                    else
                    {
                        MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                        memoriaCalculo.TipoPreco = enumTipoPreco;
                        memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                        memoriaCalculo.IdCliente = idCliente;
                        memoriaCalculo.DataAtual = data;
                        memoriaCalculo.IdOperacao = idOperacao;
                        memoriaCalculo.DataFluxo = pagamento.EventDate;
                        memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Amortization + pagamento.Interest + pagamento.IndexValue);
                        memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                        memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                        memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Amortizacao;
                        memoriaCalculo.TaxaDesconto = 0; 

                        if (data == pagamento.EventDate)
                        {
                            memoriaCalculo.ValorPresente = 0;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.FinalValue);
                        }
                        else
                        {
                            memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMAmortization + pagamento.MTMIndex + pagamento.MTMInterest);
                        }
                    }
                }
            }

            if (ListedBondsPagamentos != null)
            {
                foreach (IListedBonds.Pagamento pagamento in ListedBondsPagamentos)
                {
                    if (dataAnterior.HasValue)
                    {
                        if (pagamento.EventDate < dataAnterior.Value)
                            continue;
                    }
                    else
                    {
                        if (pagamento.EventDate < data)
                            continue;
                    }

                    if (quebraRegistros)
                    {
                        if (pagamento.Amortization + pagamento.Interest == 0 && pagamento.IndexValue > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.IndexValue);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.PagamentoCorrecao;
                            memoriaCalculo.TaxaDesconto = 0;
                            memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.IndexValue);
                        }

                        if (pagamento.Amortization > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Amortization + pagamento.IndexValue);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Amortizacao;
                            memoriaCalculo.TaxaDesconto = Convert.ToDecimal(pagamento.Amortization / pagamento.Notional) * 100;

                            if (data == pagamento.EventDate)
                            {
                                memoriaCalculo.ValorPresente = 0;
                                memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.FinalValue) - Convert.ToDecimal(pagamento.Interest);
                            }
                            else
                            {
                                memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMAmortization);
                            }
                        }

                        if (pagamento.Interest > 0)
                        {
                            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                            memoriaCalculo.IdCliente = idCliente;
                            memoriaCalculo.TipoPreco = enumTipoPreco;
                            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                            memoriaCalculo.DataAtual = data;
                            memoriaCalculo.IdOperacao = idOperacao;
                            memoriaCalculo.DataFluxo = pagamento.EventDate;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Interest);
                            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                            memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Juros;

                            if (taxa.HasValue)
                                memoriaCalculo.TaxaDesconto = Convert.ToDecimal(taxa.Value);
                            else
                                memoriaCalculo.TaxaDesconto = Convert.ToDecimal(pagamento.Rate);

                            if (data == pagamento.EventDate)
                            {
                                memoriaCalculo.ValorPresente = 0;
                            }
                            else
                            {
                                memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMInterest);
                            }
                        }
                    }
                    else
                    {
                        MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
                        memoriaCalculo.IdCliente = idCliente;
                        memoriaCalculo.TipoPreco = enumTipoPreco;
                        memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
                        memoriaCalculo.DataAtual = data;
                        memoriaCalculo.IdOperacao = idOperacao;
                        memoriaCalculo.DataFluxo = pagamento.EventDate;
                        memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.Amortization + pagamento.Interest + pagamento.IndexValue);
                        memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(pagamento.UpdatedNotional);
                        memoriaCalculo.ValorNominal = Convert.ToDecimal(pagamento.Notional);
                        memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Amortizacao;
                        memoriaCalculo.TaxaDesconto = 0;

                        if (data == pagamento.EventDate)
                        {
                            memoriaCalculo.ValorPresente = 0;
                            memoriaCalculo.ValorVencimento = Convert.ToDecimal(pagamento.FinalValue);
                        }
                        else
                        {
                            memoriaCalculo.ValorPresente = Convert.ToDecimal(pagamento.MTMAmortization + pagamento.MTMIndex + pagamento.MTMInterest);
                        }
                    }                    
                }
            }

            memoriaColl.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="taxa"></param>
        /// <param name="valorPresente"></param>
        /// <param name="valorVencimento"></param>
        /// <param name="ValorNominal"></param>
        /// <param name="ValorNominalAjustado"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="idOperacao"></param>
        private void GeraMemoriaCalculoFincs(DateTime data, DateTime dataVencimento, double taxa, double valorPresente, double valorVencimento, double ValorNominal, double ValorNominalAjustado, int enumTipoPreco, int enumTipoProcessamento, OperacaoRendaFixa operacaoRendaFixa)
        {
            int idOperacao = operacaoRendaFixa.IdOperacao.Value;
            int idCliente = operacaoRendaFixa.IdCliente.Value;

            MemoriaCalculoRendaFixaCollection memoriaColl = new MemoriaCalculoRendaFixaCollection();
            memoriaColl.DeletaMemoriaCalculo(idCliente, data, idOperacao, enumTipoPreco, enumTipoProcessamento);

            MemoriaCalculoRendaFixa memoriaCalculo = memoriaColl.AddNew();
            memoriaCalculo.TipoPreco = enumTipoPreco;
            memoriaCalculo.TipoEventoTitulo = (int)TipoEventoTitulo.Amortizacao;
            memoriaCalculo.TipoProcessamento = enumTipoProcessamento;
            memoriaCalculo.DataAtual = data;
            memoriaCalculo.IdOperacao = idOperacao;
            memoriaCalculo.IdCliente = idCliente;
            memoriaCalculo.DataFluxo = dataVencimento;
            memoriaCalculo.TaxaDesconto = Convert.ToDecimal(taxa);
            memoriaCalculo.ValorVencimento = Convert.ToDecimal(valorVencimento);
            memoriaCalculo.ValorPresente = Convert.ToDecimal(valorPresente);
            memoriaCalculo.ValorNominalAjustado = Convert.ToDecimal(ValorNominal);
            memoriaCalculo.ValorNominal = Convert.ToDecimal(ValorNominalAjustado);

            memoriaColl.Save();
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <returns></returns>
        private List<IListedBonds.EventoExotico> MontaListaEventosExoticos(DateTime data, TituloRendaFixa tituloRendaFixa)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            double valorNominal = (double)tituloRendaFixa.ValorNominal.Value;
            double correcaoPercentual = (double)this.RetornaPercenturalCorrecao(tituloRendaFixa, data);
            double valorTaxa = (double)(tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 100);

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataAgenda,
                                                   agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.Taxa,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.TipoEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                  agendaRendaFixaCollection.Query.TipoEvento.In((byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Juros,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.JurosCorrecao,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.IncorporacaoJuros,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Amortizacao,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataAgenda.Ascending, agendaRendaFixaCollection.Query.TipoEvento.Ascending);

            int day = 0;
            if (tituloRendaFixa.IdIndice.HasValue && tituloRendaFixa.IdIndice.Value > 0)
            {
                if (tituloRendaFixa.IdIndice.Value == (int)ListaIndiceFixo.IGPM)
                    day = 1;
                else if (tituloRendaFixa.IdIndice.Value == (int)ListaIndiceFixo.IPCA)
                    day = 15;
            }
            else
            {
                day = data.Day;
            }

            List<IListedBonds.EventoExotico> listaEventos = new List<IListedBonds.EventoExotico>();
            #region Eventos
            IListedBonds.EventoExotico evento;
            if (agendaRendaFixaCollection.Query.Load())
            {               
                #region Separa Juros, Amortização e correção
                List<AgendaRendaFixa> lstAgendaRendaFixaJuros = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                                                 delegate(AgendaRendaFixa x)
                                                                                                 {
                                                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.JurosCorrecao
                                                                                                            || x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Juros);
                                                                                                 });

                List<AgendaRendaFixa> lstAgendaRendaFixaAmortz = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                                                 delegate(AgendaRendaFixa x)
                                                                                                 {
                                                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Amortizacao
                                                                                                            || x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.AmortizacaoCorrigida);
                                                                                                 });

                List<AgendaRendaFixa> lstAgendaRendaFixaIncorporacaoJuros = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                 delegate(AgendaRendaFixa x)
                                                                 {
                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.IncorporacaoJuros);
                                                                 });

                List<AgendaRendaFixa> lstAgendaRendaFixaPagamentoCorrecao = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                 delegate(AgendaRendaFixa x)
                                                 {
                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.PagamentoCorrecao);
                                                 });
                #endregion

                #region Trata Evento de Juros
                foreach (AgendaRendaFixa agendaJuros in lstAgendaRendaFixaJuros)
                {
                    evento = new IListedBonds.EventoExotico();
                    evento.Data = agendaJuros.DataAgenda.Value;
                    evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                    evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                    DateTime dataAux = agendaJuros.DataAgenda.Value.AddMonths(Math.Abs(tituloRendaFixa.DefasagemMeses.GetValueOrDefault(0)) * (-1));
                    if (day != 0)
                        evento.CorrecaoData = new DateTime(dataAux.Year, dataAux.Month, day);
                    else
                        evento.CorrecaoData = dataAux;
                    evento.JurosIncorporacaoPercentual = 0;
                    evento.CorrecaoIncorporacaoPercentual = 0;
                    evento.AmortizacaoPercentual = 0;
                    evento.CorrecaoPercentual = correcaoPercentual;
                    if (agendaJuros.Taxa.HasValue && agendaJuros.Taxa.Value > 0)
                    {
                        evento.JurosPercentual = (double)agendaJuros.Taxa.Value;
                    }

                    listaEventos.Add(evento);
                }
                #endregion

                #region Trata Evento de Amortização
                foreach (AgendaRendaFixa agendaAmortz in lstAgendaRendaFixaAmortz)
                {
                    double taxaAmortz;

                    if (tituloRendaFixa.CriterioAmortizacao == (int)CriterioAmortizacao.ValorNominalInicialFixo)
                    {
                        taxaAmortz = (double)agendaAmortz.Valor.Value;
                    }
                    else
                    {
                        taxaAmortz = (double)agendaAmortz.Taxa.GetValueOrDefault(0);
                    }

                    evento = listaEventos.Find(delegate(IListedBonds.EventoExotico x) { return (x.Data == agendaAmortz.DataAgenda.Value); });
                    if (evento != null)
                    {
                        evento.AmortizacaoPercentual = taxaAmortz;
                        continue;
                    }
                    else
                    {
                        evento = new IListedBonds.EventoExotico();
                        evento.Data = agendaAmortz.DataAgenda.Value;
                        evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                        evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                        DateTime dataAux = agendaAmortz.DataAgenda.Value.AddMonths(Math.Abs(tituloRendaFixa.DefasagemMeses.GetValueOrDefault(0)) * (-1));
                        if (day != 0)
                            evento.CorrecaoData = new DateTime(dataAux.Year, dataAux.Month, day);
                        else
                            evento.CorrecaoData = dataAux; 
                        evento.AmortizacaoPercentual = taxaAmortz;
                        evento.CorrecaoPercentual = correcaoPercentual;
                        evento.JurosPercentual = valorTaxa;
                        evento.JurosIncorporacaoPercentual = 0;
                        evento.CorrecaoIncorporacaoPercentual = 0;
                        listaEventos.Add(evento);
                    }
                }
                #endregion

                #region Trata Evento de Incorporação de Juros
                foreach (AgendaRendaFixa agendaIncorporacaoJuros in lstAgendaRendaFixaIncorporacaoJuros)
                {
                    double taxaIncorporacaoJuros;
                    if (agendaIncorporacaoJuros.Taxa.HasValue && agendaIncorporacaoJuros.Taxa.Value > 0)
                    {
                        taxaIncorporacaoJuros = (double)agendaIncorporacaoJuros.Taxa.Value;
                    }
                    else
                    {
                        throw new Exception("Agenda de Incorporação de Juros sem Taxa cadastrada! ( " + tituloRendaFixa.DescricaoCompleta + ")");
                    }

                    evento = listaEventos.Find(delegate(IListedBonds.EventoExotico x) { return (x.Data == agendaIncorporacaoJuros.DataAgenda.Value); });
                    if (evento != null)
                    {
                        evento.JurosIncorporacaoPercentual = taxaIncorporacaoJuros;
                        continue;
                    }
                    else
                    {
                        evento = new IListedBonds.EventoExotico();
                        evento.Data = agendaIncorporacaoJuros.DataAgenda.Value;
                        evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                        evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                        DateTime dataAux = agendaIncorporacaoJuros.DataAgenda.Value.AddMonths(tituloRendaFixa.DefasagemMeses.GetValueOrDefault(0));
                        if (day != 0)
                            evento.CorrecaoData = new DateTime(dataAux.Year, dataAux.Month, day);
                        else
                            evento.CorrecaoData = dataAux;
                        evento.AmortizacaoPercentual = 0;
                        evento.CorrecaoPercentual = correcaoPercentual;
                        evento.JurosPercentual = valorTaxa;
                        evento.JurosIncorporacaoPercentual = taxaIncorporacaoJuros;
                        evento.CorrecaoIncorporacaoPercentual = 0;
                        listaEventos.Add(evento);
                    }
                }
                #endregion

                #region Trata Evento de Pagamento de Correçao
                foreach (AgendaRendaFixa agendaPagamentoCorrecao in lstAgendaRendaFixaPagamentoCorrecao)
                {
                    double taxaPagamentoCorrecao;
                    taxaPagamentoCorrecao = (double)agendaPagamentoCorrecao.Taxa.GetValueOrDefault(0);

                    evento = listaEventos.Find(delegate(IListedBonds.EventoExotico x) { return (x.Data == agendaPagamentoCorrecao.DataAgenda.Value); });
                    if (evento != null)
                    {
                        evento.CorrecaoPercentual = taxaPagamentoCorrecao;
                        continue;
                    }
                    else
                    {
                        evento = new IListedBonds.EventoExotico();
                        evento.Data = agendaPagamentoCorrecao.DataAgenda.Value;
                        evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                        evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                        DateTime dataAux = agendaPagamentoCorrecao.DataAgenda.Value.AddMonths(tituloRendaFixa.DefasagemMeses.GetValueOrDefault(0));
                        if (day != 0)
                            evento.CorrecaoData = new DateTime(dataAux.Year, dataAux.Month, day);
                        else
                            evento.CorrecaoData = dataAux;
                        evento.AmortizacaoPercentual = 0;
                        evento.CorrecaoPercentual = taxaPagamentoCorrecao;
                        evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                        evento.JurosPercentual = valorTaxa;

                        listaEventos.Add(evento);
                    }
                }
                #endregion

                listaEventos.Sort(delegate(IListedBonds.EventoExotico x, IListedBonds.EventoExotico y) { return x.Data.CompareTo(y.Data); });

            }
            else //Vencimento
            {
                evento = new IListedBonds.EventoExotico();

                evento.Data = tituloRendaFixa.DataVencimento.Value;
                evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                evento.CorrecaoData = tituloRendaFixa.DataVencimento.Value;
                evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                evento.CorrecaoData = tituloRendaFixa.DataEmissao.Value;
                evento.JurosIncorporacaoPercentual = 0;
                evento.AmortizacaoPercentual = 100;
                evento.CorrecaoPercentual = correcaoPercentual;
                evento.JurosPercentual = 0;

                listaEventos.Add(evento);
            }
            #endregion

            return listaEventos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="listaEventos"></param>
        /// <param name="hsMemoria"></param>
        /// <returns></returns>
        private IListedBonds.AtivoExotico MontaBondObjExotico(TituloRendaFixa tituloRendaFixa, List<IListedBonds.EventoExotico> listaEventos, ref Hashtable hsMemoria)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();

            List<IListedBonds.Arredondamento> listArredondamentoCorrecao = this.RetornaListaArredondamentoCorrecao(tituloRendaFixa);
            List<IListedBonds.Arredondamento> listArredondamentoJuros = this.RetornaListaArredondamentoJuros(tituloRendaFixa);

            IListedBonds.AtivoExotico ativoExotico = new IListedBonds.AtivoExotico();
            #region Caracteristicas do Papel
            ativoExotico.AmortizacaoTipo = controleUtilFincs.RetornaCriterioAmortizacao(tituloRendaFixa.CriterioAmortizacao);
            ativoExotico.Indicador = controleUtilFincs.RetornaTipoIndexadorFincs(tituloRendaFixa.IdIndice, ref hsMemoria);
            ativoExotico.AtivoTipo = controleUtilFincs.RetornaAtivoTipoFincs(ativoExotico.Indicador);
            ativoExotico.ValorTipo = controleUtilFincs.RetornaValorTipo(ativoExotico.Indicador);
            ativoExotico.PagamentoTipo = controleUtilFincs.RetornaPagamentoTipo(ativoExotico.Indicador);
            ativoExotico.ProRata = controleUtilFincs.RetornaProRata(tituloRendaFixa.TipoProRata.Value);
            ativoExotico.DataTipo = controleUtilFincs.RetornaProRataEmissao(tituloRendaFixa.ExecutaProRataEmissao);
            ativoExotico.Emissao = tituloRendaFixa.DataEmissao.Value;
            ativoExotico.ValorEmissao = (double)tituloRendaFixa.PUNominal.Value;
            ativoExotico.Vencimento = tituloRendaFixa.DataVencimento.Value;
            ativoExotico.Eventos = listaEventos.ToArray();
            ativoExotico.ArredondamentosCorrecao = listArredondamentoCorrecao.ToArray(); // Arredondamentos
            ativoExotico.ArredondamentosJuros = listArredondamentoJuros.ToArray(); // Arredondamentos    
            ativoExotico.DataInicioCorrecao = tituloRendaFixa.DataInicioCorrecao.GetValueOrDefault(tituloRendaFixa.DataEmissao.Value);
            ativoExotico.ProRataEmissaoDia = tituloRendaFixa.ProRataEmissaoDia.GetValueOrDefault(0);
            ativoExotico.ProRataLiquidacao = controleUtilFincs.RetornaProRataLiquidacao(tituloRendaFixa.ProRataLiquidacao);
            ativoExotico.AtivoRegra = controleUtilFincs.RetornaAtivoRegra(tituloRendaFixa.AtivoRegra);
            #endregion

            return ativoExotico;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <returns></returns>
        private List<IListedBonds.Evento> MontaListaEventos(DateTime data, TituloRendaFixa tituloRendaFixa)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            double valorTaxa = (double)(tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0);
            double correcaoPercentual = (double)this.RetornaPercenturalCorrecao(tituloRendaFixa, data);

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataAgenda,
                                                   agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.Taxa,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.TipoEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                  agendaRendaFixaCollection.Query.TipoEvento.In((byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Juros,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.JurosCorrecao,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Amortizacao,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataAgenda.Ascending, agendaRendaFixaCollection.Query.TipoEvento.Ascending);

            List<IListedBonds.Evento> listaEventos = new List<IListedBonds.Evento>();
            #region Eventos
            IListedBonds.Evento evento;
            if (agendaRendaFixaCollection.Query.Load())
            {
                #region Separa Juros, Amortização 
                List<AgendaRendaFixa> lstAgendaRendaFixaJuros = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                                                 delegate(AgendaRendaFixa x)
                                                                                                 {
                                                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.JurosCorrecao
                                                                                                            || x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Juros);
                                                                                                 });

                List<AgendaRendaFixa> lstAgendaRendaFixaAmortz = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                                                 delegate(AgendaRendaFixa x)
                                                                                                 {
                                                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.Amortizacao
                                                                                                            || x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.AmortizacaoCorrigida);
                                                                                                 });

                List<AgendaRendaFixa> lstAgendaRendaFixaPagamentoCorrecao = ((List<AgendaRendaFixa>)agendaRendaFixaCollection).FindAll(
                                                                 delegate(AgendaRendaFixa x)
                                                                 {
                                                                     return (x.TipoEvento == (byte)Financial.RendaFixa.Enums.TipoEventoTitulo.PagamentoCorrecao);
                                                                 });
                #endregion

                #region Trata Evento de Juros
                foreach (AgendaRendaFixa agendaJuros in lstAgendaRendaFixaJuros)
                {
                    evento = new IListedBonds.Evento();
                    evento.Data = agendaJuros.DataAgenda.Value;
                    evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                    evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                    evento.AmortizacaoPercentual = 0;
                    evento.CorrecaoPercentual = correcaoPercentual;
                    evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                    if (agendaJuros.Taxa.HasValue && agendaJuros.Taxa.Value > 0)
                    {
                        evento.JurosPercentual = (double)agendaJuros.Taxa.Value;
                    }

                    listaEventos.Add(evento);
                }
                #endregion

                #region Trata Evento de Amortização
                foreach (AgendaRendaFixa agendaAmortz in lstAgendaRendaFixaAmortz)
                {
                    double taxaAmortz;

                    if (tituloRendaFixa.CriterioAmortizacao == (int)CriterioAmortizacao.ValorNominalInicialFixo)
                    {
                        taxaAmortz = (double)agendaAmortz.Valor.Value;
                    }
                    else
                    {
                        taxaAmortz = (double)agendaAmortz.Taxa.GetValueOrDefault(0);
                    }

                    evento = listaEventos.Find(delegate(IListedBonds.Evento x) { return (x.Data == agendaAmortz.DataAgenda.Value); });
                    if (evento != null)
                    {
                        evento.AmortizacaoPercentual = taxaAmortz;
                        continue;
                    }
                    else
                    {
                        evento = new IListedBonds.Evento();
                        evento.Data = agendaAmortz.DataAgenda.Value;
                        evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                        evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                        evento.AmortizacaoPercentual = taxaAmortz;
                        evento.CorrecaoPercentual = correcaoPercentual;
                        evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                        evento.JurosPercentual = valorTaxa;

                        listaEventos.Add(evento);
                    }
                }
                #endregion

                #region Trata Evento de Pagamento de Correçao
                foreach (AgendaRendaFixa agendaPagamentoCorrecao in lstAgendaRendaFixaPagamentoCorrecao)
                {
                    double taxaPagamentoCorrecao;
                    taxaPagamentoCorrecao = (double)agendaPagamentoCorrecao.Taxa.GetValueOrDefault(0);

                    evento = listaEventos.Find(delegate(IListedBonds.Evento x) { return (x.Data == agendaPagamentoCorrecao.DataAgenda.Value); });
                    if (evento != null)
                    {
                        evento.CorrecaoPercentual = taxaPagamentoCorrecao;
                        continue;
                    }
                    else
                    {
                        evento = new IListedBonds.Evento();
                        evento.Data = agendaPagamentoCorrecao.DataAgenda.Value;
                        evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                        evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                        evento.AmortizacaoPercentual = 0;
                        evento.CorrecaoPercentual = taxaPagamentoCorrecao;
                        evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                        evento.JurosPercentual = valorTaxa;

                        listaEventos.Add(evento);
                    }
                }
                #endregion

                listaEventos.Sort(delegate(IListedBonds.Evento x, IListedBonds.Evento y) { return x.Data.CompareTo(y.Data); });
            }
            else //Vencimento
            {
                evento = new IListedBonds.Evento();

                evento.Data = tituloRendaFixa.DataVencimento.Value;
                evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                evento.AmortizacaoPercentual = 100;
                evento.CorrecaoPercentual = correcaoPercentual;
                evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                evento.JurosPercentual = valorTaxa;

                listaEventos.Add(evento);
            }
            #endregion

            return listaEventos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="listaEventos"></param>
        /// <param name="hsMemoria"></param>
        /// <returns></returns>
        private IListedBonds.Ativo MontaBondObj(TituloRendaFixa tituloRendaFixa, List<IListedBonds.Evento> listaEventos, ref Hashtable hsMemoria)
        {
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();

            List<IListedBonds.Arredondamento> listArredondamentoCorrecao = this.RetornaListaArredondamentoCorrecao(tituloRendaFixa);
            List<IListedBonds.Arredondamento> listArredondamentoJuros = this.RetornaListaArredondamentoJuros(tituloRendaFixa);

            IListedBonds.Ativo bondObj = new IListedBonds.Ativo();
            #region Caracteristicas do Papel
            bondObj.AmortizacaoTipo = controleUtilFincs.RetornaCriterioAmortizacao(tituloRendaFixa.CriterioAmortizacao);
            bondObj.Indicador = controleUtilFincs.RetornaTipoIndexadorFincs(tituloRendaFixa.IdIndice, ref hsMemoria);
            bondObj.AtivoTipo = controleUtilFincs.RetornaAtivoTipoFincs(bondObj.Indicador);
            bondObj.ValorTipo = controleUtilFincs.RetornaValorTipo(bondObj.Indicador);
            bondObj.PagamentoTipo = controleUtilFincs.RetornaPagamentoTipo(bondObj.Indicador);
            bondObj.ProRata = controleUtilFincs.RetornaProRata(tituloRendaFixa.TipoProRata.Value);
            bondObj.DataTipo = controleUtilFincs.RetornaProRataEmissao(tituloRendaFixa.ExecutaProRataEmissao);
            bondObj.Emissao = tituloRendaFixa.DataEmissao.Value;
            bondObj.ValorEmissao = (double)tituloRendaFixa.PUNominal.Value;
            bondObj.Vencimento = tituloRendaFixa.DataVencimento.Value;
            bondObj.Eventos = listaEventos.ToArray();
            bondObj.ArredondamentosCorrecao = listArredondamentoCorrecao.ToArray(); // Arredondamentos
            bondObj.ArredondamentosJuros = listArredondamentoJuros.ToArray(); // Arredondamentos    
            #endregion

            return bondObj;
        }

        /// <summary>
        /// Retorna Valor Nominal de acordo com o critério de amortização
        /// </summary>
        /// <param name="data"></param>
        /// <param name="criterio"></param>
        /// <param name="lstPagamento"></param>
        /// <returns></returns>
        private decimal RetornaValorNominalCriterioAmortizacao(DateTime data, CriterioAmortizacao criterio, List<Pagamento> lstPagamento)
        {
            decimal valorNominal = 0;

            if (criterio == CriterioAmortizacao.ValorNominal)
            {
                Pagamento pagamento = lstPagamento.Find(delegate(Pagamento x) { return x.EventDate == data; });
                valorNominal = Convert.ToDecimal(pagamento.UpdatedNotional);
            }
            else
            {
                Pagamento pagamento = (Pagamento)lstPagamento[0];
                valorNominal = Convert.ToDecimal(pagamento.UpdatedNotional);
            }

            return valorNominal;
        }

        /// <summary>
        /// Método que converte valor em taxa e vice versa
        /// </summary>
        /// <param name="data">Data do Evento</param>
        /// <param name="papelRendaFixa">Papel de Renda Fixa</param>
        /// <param name="tituloRendaFixa">Título de Renda Fixa</param>
        /// <param name="previa">Prévia do indexador</param>
        /// <param name="valorOriginal">Valor original do Evento</param>
        /// <param name="novaTaxa">Nova taxa do evento</param>
        /// <param name="novoValor">Novo valor do evento</param>
        /// <param name="hsMemoria">Hashtable que guarda</param>
        /// <returns></returns>
        public decimal ConverteValorTaxa(AgendaRendaFixa agendaRendaFixa, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, double previa, double valorOriginal, double? novaTaxa, double? novoValor, ref Hashtable hsMemoria)
        {
            bool objExotico = tituloRendaFixa.AtivoRegra != Financial.BibliotecaFincs.Enums.AtivoRegra.SemRegraEspecifica;

            List<Pagamento> lstPagamento = new List<Pagamento>();
            ControleUtilFincs controleUtilFincs = new ControleUtilFincs();
            double valorTaxa = (double)(tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0);
            double correcaoPercentual = (double)(tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 100);

            DateTime dataEventoAnt = new DateTime();            
            DateTime data = agendaRendaFixa.DataEvento.Value;

            IListedBonds.eTaxa eTaxa = controleUtilFincs.Retorna_eTaxa(tituloRendaFixa.ETaxa);

            List<byte> lstEventoJuros = new List<byte>();
            lstEventoJuros.Add((byte)TipoEventoTitulo.Juros);
            lstEventoJuros.Add((byte)TipoEventoTitulo.JurosCorrecao);

            if (objExotico)
            {
                //Lista de Eventos
                List<IListedBonds.EventoExotico> listaEventos = this.MontaListaEventosExoticos(data, tituloRendaFixa);

                //Busca taxa anterior e seta a nova taxa, caso tenha.
                EventoExotico evento = listaEventos.Find(delegate(EventoExotico x) { return x.Data == data; });

                if (evento == null || (evento != null && evento.Data == new DateTime()))
                {
                    int day = 0;
                    if (tituloRendaFixa.IdIndice.HasValue && tituloRendaFixa.IdIndice.Value > 0)
                    {
                        if (tituloRendaFixa.IdIndice.Value == (int)ListaIndiceFixo.IGPM)
                            day = 1;
                        else if (tituloRendaFixa.IdIndice.Value == (int)ListaIndiceFixo.IPCA)
                            day = 15;
                    }
                    else
                    {
                        day = data.Day;
                    }

                    evento = new IListedBonds.EventoExotico();
                    evento.Data = agendaRendaFixa.DataAgenda.Value;
                    DateTime dataAux = agendaRendaFixa.DataAgenda.Value.AddMonths(Math.Abs(tituloRendaFixa.DefasagemMeses.GetValueOrDefault(0)) * (-1));
                    if (day != 0)
                        evento.CorrecaoData = new DateTime(dataAux.Year, dataAux.Month, day);
                    evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                    evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                    evento.AmortizacaoPercentual = (double)agendaRendaFixa.Taxa.Value;
                    evento.CorrecaoPercentual = correcaoPercentual;
                    evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                    evento.JurosPercentual = valorTaxa;

                    listaEventos.Add(evento);
                }

                listaEventos.Sort(delegate(IListedBonds.EventoExotico x, IListedBonds.EventoExotico y) { return x.Data.CompareTo(y.Data); });

                //Seta novos valores
                if (lstEventoJuros.Contains(agendaRendaFixa.TipoEvento.Value))
                {
                    if (novaTaxa.HasValue)
                    {
                        evento.JurosPercentual = novaTaxa.Value;
                        evento.AmortizacaoPercentual = 100;

                    }

                    if (novoValor.HasValue)
                        evento.JurosPercentual = (double)10;
                }
                else //Amortização
                {
                    if (novaTaxa.HasValue)
                        evento.AmortizacaoPercentual = novaTaxa.Value;

                    if (novoValor.HasValue)
                        evento.AmortizacaoPercentual = (double)10;
                }

                //Busca Evento anterior
                EventoExotico eventoAnterior = listaEventos.FindLast(delegate(EventoExotico x) { return x.Data < data; });
                if (eventoAnterior != null && eventoAnterior.Data != new DateTime())
                {
                    dataEventoAnt = eventoAnterior.Data;
                }
                else
                {
                    dataEventoAnt = tituloRendaFixa.DataEmissao.Value;
                }

                //Ativo
                IListedBonds.AtivoExotico bondObj = MontaBondObjExotico(tituloRendaFixa, listaEventos, ref hsMemoria);

                //Prévia de acordo com o indexador
                previa = controleUtilFincs.RetornaCurrentIndex(bondObj.Indicador, previa);

                //Popula Lista
                lstPagamento.AddRange(listed.ListedBond_Frac_ExoticIndexMTM(bondObj, data, new double[] { 0 }, IListedBonds.eTaxaPUTipo.PU, eTaxa, new double[] { 0 }, previa, null, double.NaN).Pagamentos);

            }
            else
            {
                //Lista de Eventos
                List<IListedBonds.Evento> listaEventos = this.MontaListaEventos(data, tituloRendaFixa);

                //Busca taxa anterior e seta a nova taxa, caso tenha.
                Evento evento = listaEventos.Find(delegate(Evento x) { return x.Data == data; });

                if (evento == null || (evento != null && evento.Data == new DateTime()))
                {
                    evento = new IListedBonds.Evento();
                    evento.Data = agendaRendaFixa.DataAgenda.Value;
                    evento.Juros = controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros);
                    evento.JurosTipo = controleUtilFincs.Retorna_eJurosTipo(tituloRendaFixa.EJurosTipo);
                    evento.AmortizacaoPercentual = (double)agendaRendaFixa.Taxa.Value;
                    evento.CorrecaoPercentual = correcaoPercentual;
                    evento.LiborPeriod = IListedBonds.eLiborPeriod.EuroLiborON;
                    evento.JurosPercentual = valorTaxa;

                    listaEventos.Add(evento);
                }
               
                listaEventos.Sort(delegate(IListedBonds.Evento x, IListedBonds.Evento y) { return x.Data.CompareTo(y.Data); });

                //Seta novos valores
                if (lstEventoJuros.Contains(agendaRendaFixa.TipoEvento.Value))
                {
                    if (novaTaxa.HasValue)
                    {
                        evento.JurosPercentual = novaTaxa.Value;
                        evento.AmortizacaoPercentual = 100;
                    }

                    if (novoValor.HasValue)
                        evento.JurosPercentual = (double)agendaRendaFixa.Taxa.Value;
                }
                else //Amortização
                {
                    if (novaTaxa.HasValue)
                        evento.AmortizacaoPercentual = novaTaxa.Value;

                    if (novoValor.HasValue)
                        evento.AmortizacaoPercentual = (double)10;
                }

                //Busca Evento anterior
                Evento eventoAnterior = listaEventos.FindLast(delegate(Evento x) { return x.Data < data; });
                if (eventoAnterior != null && eventoAnterior.Data != new DateTime())
                {
                    dataEventoAnt = eventoAnterior.Data;
                }
                else
                {
                    dataEventoAnt = tituloRendaFixa.DataEmissao.Value;
                }

                //Ativo
                IListedBonds.Ativo bondObj = MontaBondObj(tituloRendaFixa, listaEventos, ref hsMemoria);

                //Prévia de acordo com o indexador
                previa = controleUtilFincs.RetornaCurrentIndex(bondObj.Indicador, previa);

                //
                List<double> lstExpectedRateRange = new List<double>();
                lstExpectedRateRange.Add(0);   

                //Popula Lista
                lstPagamento.AddRange(listed.ListedBond_Frac_MTMValueArray(bondObj, data, lstExpectedRateRange.ToArray(), IListedBonds.eTaxaPUTipo.Taxa, eTaxa, lstExpectedRateRange.ToArray(), previa, null, double.NaN).Pagamentos);
            }

            Pagamento pagamento = lstPagamento.Find(delegate(Pagamento x) { return x.EventDate == data; });

            double resultado = 0;
            if (lstEventoJuros.Contains(agendaRendaFixa.TipoEvento.Value))
            {
                if (novoValor.HasValue)
                {
                    resultado = (novoValor.Value / pagamento.UpdatedNotional);

                    double fator = resultado + 1;

                    IGeneral.eTaxa eTaxaConversao = controleUtilFincs.DeParaEjurosEtaxa(controleUtilFincs.Retorna_eJuros(tituloRendaFixa.EJuros));
                    resultado = generalClient.Conversao_FatorToTaxa(eTaxaConversao, dataEventoAnt, agendaRendaFixa.DataEvento.Value, fator);
                }
                else
                    resultado = pagamento.Interest;
            }
            else
            {
                if (novoValor.HasValue)
                    resultado = (novoValor.Value * 10) / pagamento.Amortization;
                else
                    resultado = pagamento.Amortization;
            }
            
            return Convert.ToDecimal(resultado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titulo"></param>
        /// <returns></returns>
        private string RetornaDescricaoTitulo(TituloRendaFixa tituloRendaFixa)
        {
            string descricaoTitulo = string.IsNullOrEmpty(tituloRendaFixa.DescricaoCompleta.Trim()) ? tituloRendaFixa.Descricao : tituloRendaFixa.DescricaoCompleta;
            descricaoTitulo = "ID - " + tituloRendaFixa.IdTitulo + " - " + descricaoTitulo;

            return descricaoTitulo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="dataCalculo"></param>
        /// <returns></returns>
        private decimal RetornaPercenturalCorrecao(TituloRendaFixa tituloRendaFixa, DateTime dataCalculo)
        {
            decimal? percentual = null;
            int idTitulo = tituloRendaFixa.IdTitulo.Value;
            DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;

            TabelaEscalonamentoRF tabelaEscalonamentoRF = new TabelaEscalonamentoRF();
            percentual = tabelaEscalonamentoRF.RetornaTaxaEscalonada(idTitulo, dataEmissao, dataCalculo);

            return percentual ?? tituloRendaFixa.Percentual.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendafixa"></param>
        /// <returns></returns>
        private List<IListedBonds.Arredondamento> RetornaListaArredondamentoCorrecao(TituloRendaFixa tituloRendafixa)
        {
            List<IListedBonds.Arredondamento> listArredondamentoCorrecao = new List<IListedBonds.Arredondamento>();
            IListedBonds.Arredondamento arredondamento = new IListedBonds.Arredondamento();

            if (tituloRendafixa.IdIndice.HasValue)
            {
                if (tituloRendafixa.IdIndice.Value == (short)ListaIndiceFixo.CDI)
                {
                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 15;
                    arredondamento.Forma = IListedBonds.eForma.Arredondar;
                    listArredondamentoCorrecao.Add(arredondamento);

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 8;
                    arredondamento.Forma = IListedBonds.eForma.Arredondar;
                    listArredondamentoCorrecao.Add(arredondamento);

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 16;
                    arredondamento.Forma = IListedBonds.eForma.Truncar;
                    listArredondamentoCorrecao.Add(arredondamento);

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 16;
                    arredondamento.Forma = IListedBonds.eForma.Truncar;
                    listArredondamentoCorrecao.Add(arredondamento);

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 8;
                    arredondamento.Forma = IListedBonds.eForma.Arredondar;
                    listArredondamentoCorrecao.Add(arredondamento);
                }        
                else //Default
                {
                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 11;
                    arredondamento.Forma = IListedBonds.eForma.Truncar;
                    listArredondamentoCorrecao.Add(arredondamento);
                }
            }
            else //Pré Fixado
            {
                arredondamento = new IListedBonds.Arredondamento();
                arredondamento.CasasDecimais = 15;
                arredondamento.Forma = IListedBonds.eForma.Arredondar;
                listArredondamentoCorrecao.Add(arredondamento);

                arredondamento = new IListedBonds.Arredondamento();
                arredondamento.CasasDecimais = 8;
                arredondamento.Forma = IListedBonds.eForma.Arredondar;
                listArredondamentoCorrecao.Add(arredondamento);
            }

            return listArredondamentoCorrecao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tituloRendafixa"></param>
        /// <returns></returns>
        private List<IListedBonds.Arredondamento> RetornaListaArredondamentoJuros(TituloRendaFixa tituloRendafixa)
        {
            List<IListedBonds.Arredondamento> listArredondamentoJuros = new List<IListedBonds.Arredondamento>();
            IListedBonds.Arredondamento arredondamento = new IListedBonds.Arredondamento();

            #region Default
            arredondamento = new Arredondamento();
            arredondamento.CasasDecimais = 15;
            arredondamento.Forma = eForma.Arredondar;
            listArredondamentoJuros.Add(arredondamento);

            arredondamento = new Arredondamento();
            arredondamento.CasasDecimais = 8;
            arredondamento.Forma = eForma.Arredondar;
            listArredondamentoJuros.Add(arredondamento);
            #endregion

            if (tituloRendafixa.IdIndice.HasValue)
            {
                if (tituloRendafixa.IdIndice.Value == (short)ListaIndiceFixo.CDI && tituloRendafixa.Taxa.GetValueOrDefault(0) > (0))
                {
                    arredondamento = new IListedBonds.Arredondamento();

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 15;
                    arredondamento.Forma = IListedBonds.eForma.Arredondar;
                    listArredondamentoJuros.Add(arredondamento);

                    arredondamento = new IListedBonds.Arredondamento();
                    arredondamento.CasasDecimais = 9;
                    arredondamento.Forma = IListedBonds.eForma.Arredondar;
                    listArredondamentoJuros.Add(arredondamento);                    
                }
            }

            return listArredondamentoJuros;
        }
    }
}
