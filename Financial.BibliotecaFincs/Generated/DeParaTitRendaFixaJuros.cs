/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/05/2015 11:44:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.BibliotecaFincs
{

	[Serializable]
	abstract public class esDeParaTitRendaFixaJurosCollection : esEntityCollection
	{
		public esDeParaTitRendaFixaJurosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DeParaTitRendaFixaJurosCollection";
		}

		#region Query Logic
		protected void InitQuery(esDeParaTitRendaFixaJurosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDeParaTitRendaFixaJurosQuery);
		}
		#endregion
		
		virtual public DeParaTitRendaFixaJuros DetachEntity(DeParaTitRendaFixaJuros entity)
		{
			return base.DetachEntity(entity) as DeParaTitRendaFixaJuros;
		}
		
		virtual public DeParaTitRendaFixaJuros AttachEntity(DeParaTitRendaFixaJuros entity)
		{
			return base.AttachEntity(entity) as DeParaTitRendaFixaJuros;
		}
		
		virtual public void Combine(DeParaTitRendaFixaJurosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DeParaTitRendaFixaJuros this[int index]
		{
			get
			{
				return base[index] as DeParaTitRendaFixaJuros;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DeParaTitRendaFixaJuros);
		}
	}



	[Serializable]
	abstract public class esDeParaTitRendaFixaJuros : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDeParaTitRendaFixaJurosQuery GetDynamicQuery()
		{
			return null;
		}

		public esDeParaTitRendaFixaJuros()
		{

		}

		public esDeParaTitRendaFixaJuros(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 baseAnoPAS, System.Byte contagemDiasPAS, System.Byte pagamentoJurosPAS, System.Byte tipoCurvaPAS)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(baseAnoPAS, contagemDiasPAS, pagamentoJurosPAS, tipoCurvaPAS);
			else
				return LoadByPrimaryKeyStoredProcedure(baseAnoPAS, contagemDiasPAS, pagamentoJurosPAS, tipoCurvaPAS);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 baseAnoPAS, System.Byte contagemDiasPAS, System.Byte pagamentoJurosPAS, System.Byte tipoCurvaPAS)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(baseAnoPAS, contagemDiasPAS, pagamentoJurosPAS, tipoCurvaPAS);
			else
				return LoadByPrimaryKeyStoredProcedure(baseAnoPAS, contagemDiasPAS, pagamentoJurosPAS, tipoCurvaPAS);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 baseAnoPAS, System.Byte contagemDiasPAS, System.Byte pagamentoJurosPAS, System.Byte tipoCurvaPAS)
		{
			esDeParaTitRendaFixaJurosQuery query = this.GetDynamicQuery();
			query.Where(query.BaseAnoPAS == baseAnoPAS, query.ContagemDiasPAS == contagemDiasPAS, query.PagamentoJurosPAS == pagamentoJurosPAS, query.TipoCurvaPAS == tipoCurvaPAS);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 baseAnoPAS, System.Byte contagemDiasPAS, System.Byte pagamentoJurosPAS, System.Byte tipoCurvaPAS)
		{
			esParameters parms = new esParameters();
			parms.Add("BaseAnoPAS",baseAnoPAS);			parms.Add("ContagemDiasPAS",contagemDiasPAS);			parms.Add("PagamentoJurosPAS",pagamentoJurosPAS);			parms.Add("TipoCurvaPAS",tipoCurvaPAS);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TipoCurvaPAS": this.str.TipoCurvaPAS = (string)value; break;							
						case "ContagemDiasPAS": this.str.ContagemDiasPAS = (string)value; break;							
						case "BaseAnoPAS": this.str.BaseAnoPAS = (string)value; break;							
						case "PagamentoJurosPAS": this.str.PagamentoJurosPAS = (string)value; break;							
						case "EJurosFincs": this.str.EJurosFincs = (string)value; break;							
						case "EJurosTipoFincs": this.str.EJurosTipoFincs = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TipoCurvaPAS":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCurvaPAS = (System.Byte?)value;
							break;
						
						case "ContagemDiasPAS":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDiasPAS = (System.Byte?)value;
							break;
						
						case "BaseAnoPAS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.BaseAnoPAS = (System.Int32?)value;
							break;
						
						case "PagamentoJurosPAS":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.PagamentoJurosPAS = (System.Byte?)value;
							break;
						
						case "EJurosFincs":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.EJurosFincs = (System.Byte?)value;
							break;
						
						case "EJurosTipoFincs":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.EJurosTipoFincs = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.TipoCurvaPAS
		/// </summary>
		virtual public System.Byte? TipoCurvaPAS
		{
			get
			{
				return base.GetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.TipoCurvaPAS);
			}
			
			set
			{
				base.SetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.TipoCurvaPAS, value);
			}
		}
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.ContagemDiasPAS
		/// </summary>
		virtual public System.Byte? ContagemDiasPAS
		{
			get
			{
				return base.GetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.ContagemDiasPAS);
			}
			
			set
			{
				base.SetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.ContagemDiasPAS, value);
			}
		}
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.BaseAnoPAS
		/// </summary>
		virtual public System.Int32? BaseAnoPAS
		{
			get
			{
				return base.GetSystemInt32(DeParaTitRendaFixaJurosMetadata.ColumnNames.BaseAnoPAS);
			}
			
			set
			{
				base.SetSystemInt32(DeParaTitRendaFixaJurosMetadata.ColumnNames.BaseAnoPAS, value);
			}
		}
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.PagamentoJurosPAS
		/// </summary>
		virtual public System.Byte? PagamentoJurosPAS
		{
			get
			{
				return base.GetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.PagamentoJurosPAS);
			}
			
			set
			{
				base.SetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.PagamentoJurosPAS, value);
			}
		}
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.EJurosFincs
		/// </summary>
		virtual public System.Byte? EJurosFincs
		{
			get
			{
				return base.GetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosFincs);
			}
			
			set
			{
				base.SetSystemByte(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosFincs, value);
			}
		}
		
		/// <summary>
		/// Maps to DeParaTitRendaFixaJuros.EJurosTipoFincs
		/// </summary>
		virtual public System.Int32? EJurosTipoFincs
		{
			get
			{
				return base.GetSystemInt32(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosTipoFincs);
			}
			
			set
			{
				base.SetSystemInt32(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosTipoFincs, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDeParaTitRendaFixaJuros entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TipoCurvaPAS
			{
				get
				{
					System.Byte? data = entity.TipoCurvaPAS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCurvaPAS = null;
					else entity.TipoCurvaPAS = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDiasPAS
			{
				get
				{
					System.Byte? data = entity.ContagemDiasPAS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasPAS = null;
					else entity.ContagemDiasPAS = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAnoPAS
			{
				get
				{
					System.Int32? data = entity.BaseAnoPAS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAnoPAS = null;
					else entity.BaseAnoPAS = Convert.ToInt32(value);
				}
			}
				
			public System.String PagamentoJurosPAS
			{
				get
				{
					System.Byte? data = entity.PagamentoJurosPAS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PagamentoJurosPAS = null;
					else entity.PagamentoJurosPAS = Convert.ToByte(value);
				}
			}
				
			public System.String EJurosFincs
			{
				get
				{
					System.Byte? data = entity.EJurosFincs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EJurosFincs = null;
					else entity.EJurosFincs = Convert.ToByte(value);
				}
			}
				
			public System.String EJurosTipoFincs
			{
				get
				{
					System.Int32? data = entity.EJurosTipoFincs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EJurosTipoFincs = null;
					else entity.EJurosTipoFincs = Convert.ToInt32(value);
				}
			}
			

			private esDeParaTitRendaFixaJuros entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDeParaTitRendaFixaJurosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDeParaTitRendaFixaJuros can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DeParaTitRendaFixaJuros : esDeParaTitRendaFixaJuros
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDeParaTitRendaFixaJurosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DeParaTitRendaFixaJurosMetadata.Meta();
			}
		}	
		

		public esQueryItem TipoCurvaPAS
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.TipoCurvaPAS, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDiasPAS
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.ContagemDiasPAS, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAnoPAS
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.BaseAnoPAS, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PagamentoJurosPAS
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.PagamentoJurosPAS, esSystemType.Byte);
			}
		} 
		
		public esQueryItem EJurosFincs
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosFincs, esSystemType.Byte);
			}
		} 
		
		public esQueryItem EJurosTipoFincs
		{
			get
			{
				return new esQueryItem(this, DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosTipoFincs, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DeParaTitRendaFixaJurosCollection")]
	public partial class DeParaTitRendaFixaJurosCollection : esDeParaTitRendaFixaJurosCollection, IEnumerable<DeParaTitRendaFixaJuros>
	{
		public DeParaTitRendaFixaJurosCollection()
		{

		}
		
		public static implicit operator List<DeParaTitRendaFixaJuros>(DeParaTitRendaFixaJurosCollection coll)
		{
			List<DeParaTitRendaFixaJuros> list = new List<DeParaTitRendaFixaJuros>();
			
			foreach (DeParaTitRendaFixaJuros emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DeParaTitRendaFixaJurosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DeParaTitRendaFixaJurosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DeParaTitRendaFixaJuros(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DeParaTitRendaFixaJuros();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DeParaTitRendaFixaJurosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DeParaTitRendaFixaJurosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DeParaTitRendaFixaJurosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DeParaTitRendaFixaJuros AddNew()
		{
			DeParaTitRendaFixaJuros entity = base.AddNewEntity() as DeParaTitRendaFixaJuros;
			
			return entity;
		}

		public DeParaTitRendaFixaJuros FindByPrimaryKey(System.Int32 baseAnoPAS, System.Byte contagemDiasPAS, System.Byte pagamentoJurosPAS, System.Byte tipoCurvaPAS)
		{
			return base.FindByPrimaryKey(baseAnoPAS, contagemDiasPAS, pagamentoJurosPAS, tipoCurvaPAS) as DeParaTitRendaFixaJuros;
		}


		#region IEnumerable<DeParaTitRendaFixaJuros> Members

		IEnumerator<DeParaTitRendaFixaJuros> IEnumerable<DeParaTitRendaFixaJuros>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DeParaTitRendaFixaJuros;
			}
		}

		#endregion
		
		private DeParaTitRendaFixaJurosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DeParaTitRendaFixaJuros' table
	/// </summary>

	[Serializable]
	public partial class DeParaTitRendaFixaJuros : esDeParaTitRendaFixaJuros
	{
		public DeParaTitRendaFixaJuros()
		{

		}
	
		public DeParaTitRendaFixaJuros(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DeParaTitRendaFixaJurosMetadata.Meta();
			}
		}
		
		
		
		override protected esDeParaTitRendaFixaJurosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DeParaTitRendaFixaJurosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DeParaTitRendaFixaJurosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DeParaTitRendaFixaJurosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DeParaTitRendaFixaJurosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DeParaTitRendaFixaJurosQuery query;
	}



	[Serializable]
	public partial class DeParaTitRendaFixaJurosQuery : esDeParaTitRendaFixaJurosQuery
	{
		public DeParaTitRendaFixaJurosQuery()
		{

		}		
		
		public DeParaTitRendaFixaJurosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DeParaTitRendaFixaJurosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DeParaTitRendaFixaJurosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.TipoCurvaPAS, 0, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.TipoCurvaPAS;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.ContagemDiasPAS, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.ContagemDiasPAS;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.BaseAnoPAS, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.BaseAnoPAS;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.PagamentoJurosPAS, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.PagamentoJurosPAS;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosFincs, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.EJurosFincs;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaTitRendaFixaJurosMetadata.ColumnNames.EJurosTipoFincs, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DeParaTitRendaFixaJurosMetadata.PropertyNames.EJurosTipoFincs;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DeParaTitRendaFixaJurosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TipoCurvaPAS = "TipoCurvaPAS";
			 public const string ContagemDiasPAS = "ContagemDiasPAS";
			 public const string BaseAnoPAS = "BaseAnoPAS";
			 public const string PagamentoJurosPAS = "PagamentoJurosPAS";
			 public const string EJurosFincs = "EJurosFincs";
			 public const string EJurosTipoFincs = "EJurosTipoFincs";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TipoCurvaPAS = "TipoCurvaPAS";
			 public const string ContagemDiasPAS = "ContagemDiasPAS";
			 public const string BaseAnoPAS = "BaseAnoPAS";
			 public const string PagamentoJurosPAS = "PagamentoJurosPAS";
			 public const string EJurosFincs = "EJurosFincs";
			 public const string EJurosTipoFincs = "EJurosTipoFincs";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DeParaTitRendaFixaJurosMetadata))
			{
				if(DeParaTitRendaFixaJurosMetadata.mapDelegates == null)
				{
					DeParaTitRendaFixaJurosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DeParaTitRendaFixaJurosMetadata.meta == null)
				{
					DeParaTitRendaFixaJurosMetadata.meta = new DeParaTitRendaFixaJurosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("TipoCurvaPAS", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDiasPAS", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAnoPAS", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PagamentoJurosPAS", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("EJurosFincs", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("EJurosTipoFincs", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "DeParaTitRendaFixaJuros";
				meta.Destination = "DeParaTitRendaFixaJuros";
				
				meta.spInsert = "proc_DeParaTitRendaFixaJurosInsert";				
				meta.spUpdate = "proc_DeParaTitRendaFixaJurosUpdate";		
				meta.spDelete = "proc_DeParaTitRendaFixaJurosDelete";
				meta.spLoadAll = "proc_DeParaTitRendaFixaJurosLoadAll";
				meta.spLoadByPrimaryKey = "proc_DeParaTitRendaFixaJurosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DeParaTitRendaFixaJurosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
