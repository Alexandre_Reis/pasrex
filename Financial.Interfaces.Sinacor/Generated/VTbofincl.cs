/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 08/10/2013 16:04:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVTbofinclCollection : esEntityCollection
	{
		public esVTbofinclCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VTbofinclCollection";
		}

		#region Query Logic
		protected void InitQuery(esVTbofinclQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVTbofinclQuery);
		}
		#endregion
		
		virtual public VTbofincl DetachEntity(VTbofincl entity)
		{
			return base.DetachEntity(entity) as VTbofincl;
		}
		
		virtual public VTbofincl AttachEntity(VTbofincl entity)
		{
			return base.AttachEntity(entity) as VTbofincl;
		}
		
		virtual public void Combine(VTbofinclCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VTbofincl this[int index]
		{
			get
			{
				return base[index] as VTbofincl;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VTbofincl);
		}
	}



	[Serializable]
	abstract public class esVTbofincl : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVTbofinclQuery GetDynamicQuery()
		{
			return null;
		}

		public esVTbofincl()
		{

		}

		public esVTbofincl(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVTbofinclQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVTbofinclQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DtDatmov": this.str.DtDatmov = (string)value; break;							
						case "DtNegocio": this.str.DtNegocio = (string)value; break;							
						case "CdBolfat": this.str.CdBolfat = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "CdClienteLiqfin": this.str.CdClienteLiqfin = (string)value; break;							
						case "CdClienteCc": this.str.CdClienteCc = (string)value; break;							
						case "VlTotneg": this.str.VlTotneg = (string)value; break;							
						case "VlValcor": this.str.VlValcor = (string)value; break;							
						case "VlRepcor": this.str.VlRepcor = (string)value; break;							
						case "VlTaxregDt": this.str.VlTaxregDt = (string)value; break;							
						case "VlTaxregCp": this.str.VlTaxregCp = (string)value; break;							
						case "VlTaxregOf": this.str.VlTaxregOf = (string)value; break;							
						case "VlTaxpta": this.str.VlTaxpta = (string)value; break;							
						case "VlTaxana": this.str.VlTaxana = (string)value; break;							
						case "VlTaxiof": this.str.VlTaxiof = (string)value; break;							
						case "VlValdes": this.str.VlValdes = (string)value; break;							
						case "VlIrcorr": this.str.VlIrcorr = (string)value; break;							
						case "VlLiqnot": this.str.VlLiqnot = (string)value; break;							
						case "VlLiqnotBv": this.str.VlLiqnotBv = (string)value; break;							
						case "VlLiqnotCb": this.str.VlLiqnotCb = (string)value; break;							
						case "VlComass": this.str.VlComass = (string)value; break;							
						case "VlAgente": this.str.VlAgente = (string)value; break;							
						case "VlCorresp": this.str.VlCorresp = (string)value; break;							
						case "VlVdavis": this.str.VlVdavis = (string)value; break;							
						case "VlCpavis": this.str.VlCpavis = (string)value; break;							
						case "VlVdaopc": this.str.VlVdaopc = (string)value; break;							
						case "VlCpaopc": this.str.VlCpaopc = (string)value; break;							
						case "VlTermo": this.str.VlTermo = (string)value; break;							
						case "VlDebent": this.str.VlDebent = (string)value; break;							
						case "VlTitpub": this.str.VlTitpub = (string)value; break;							
						case "VlFuturo": this.str.VlFuturo = (string)value; break;							
						case "VlAjuste": this.str.VlAjuste = (string)value; break;							
						case "CdGrupCont": this.str.CdGrupCont = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DtDatmov":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatmov = (System.DateTime?)value;
							break;
						
						case "DtNegocio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNegocio = (System.DateTime?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "CdClienteLiqfin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdClienteLiqfin = (System.Decimal?)value;
							break;
						
						case "CdClienteCc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdClienteCc = (System.Decimal?)value;
							break;
						
						case "VlTotneg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTotneg = (System.Decimal?)value;
							break;
						
						case "VlValcor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlValcor = (System.Decimal?)value;
							break;
						
						case "VlRepcor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlRepcor = (System.Decimal?)value;
							break;
						
						case "VlTaxregDt":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxregDt = (System.Decimal?)value;
							break;
						
						case "VlTaxregCp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxregCp = (System.Decimal?)value;
							break;
						
						case "VlTaxregOf":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxregOf = (System.Decimal?)value;
							break;
						
						case "VlTaxpta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxpta = (System.Decimal?)value;
							break;
						
						case "VlTaxana":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxana = (System.Decimal?)value;
							break;
						
						case "VlTaxiof":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxiof = (System.Decimal?)value;
							break;
						
						case "VlValdes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlValdes = (System.Decimal?)value;
							break;
						
						case "VlIrcorr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlIrcorr = (System.Decimal?)value;
							break;
						
						case "VlLiqnot":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlLiqnot = (System.Decimal?)value;
							break;
						
						case "VlLiqnotBv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlLiqnotBv = (System.Decimal?)value;
							break;
						
						case "VlLiqnotCb":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlLiqnotCb = (System.Decimal?)value;
							break;
						
						case "VlComass":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlComass = (System.Decimal?)value;
							break;
						
						case "VlAgente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlAgente = (System.Decimal?)value;
							break;
						
						case "VlCorresp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCorresp = (System.Decimal?)value;
							break;
						
						case "VlVdavis":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlVdavis = (System.Decimal?)value;
							break;
						
						case "VlCpavis":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCpavis = (System.Decimal?)value;
							break;
						
						case "VlVdaopc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlVdaopc = (System.Decimal?)value;
							break;
						
						case "VlCpaopc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCpaopc = (System.Decimal?)value;
							break;
						
						case "VlTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTermo = (System.Decimal?)value;
							break;
						
						case "VlDebent":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDebent = (System.Decimal?)value;
							break;
						
						case "VlTitpub":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTitpub = (System.Decimal?)value;
							break;
						
						case "VlFuturo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlFuturo = (System.Decimal?)value;
							break;
						
						case "VlAjuste":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlAjuste = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to V_TBOFINCL.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.DT_DATMOV
		/// </summary>
		virtual public System.DateTime? DtDatmov
		{
			get
			{
				return base.GetSystemDateTime(VTbofinclMetadata.ColumnNames.DtDatmov);
			}
			
			set
			{
				base.SetSystemDateTime(VTbofinclMetadata.ColumnNames.DtDatmov, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.DT_NEGOCIO
		/// </summary>
		virtual public System.DateTime? DtNegocio
		{
			get
			{
				return base.GetSystemDateTime(VTbofinclMetadata.ColumnNames.DtNegocio);
			}
			
			set
			{
				base.SetSystemDateTime(VTbofinclMetadata.ColumnNames.DtNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.CD_BOLFAT
		/// </summary>
		virtual public System.String CdBolfat
		{
			get
			{
				return base.GetSystemString(VTbofinclMetadata.ColumnNames.CdBolfat);
			}
			
			set
			{
				base.SetSystemString(VTbofinclMetadata.ColumnNames.CdBolfat, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.CD_CLIENTE_LIQFIN
		/// </summary>
		virtual public System.Decimal? CdClienteLiqfin
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.CdClienteLiqfin);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.CdClienteLiqfin, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.CD_CLIENTE_CC
		/// </summary>
		virtual public System.Decimal? CdClienteCc
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.CdClienteCc);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.CdClienteCc, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TOTNEG
		/// </summary>
		virtual public System.Decimal? VlTotneg
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTotneg);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTotneg, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_VALCOR
		/// </summary>
		virtual public System.Decimal? VlValcor
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlValcor);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlValcor, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_REPCOR
		/// </summary>
		virtual public System.Decimal? VlRepcor
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlRepcor);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlRepcor, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXREG_DT
		/// </summary>
		virtual public System.Decimal? VlTaxregDt
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregDt);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregDt, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXREG_CP
		/// </summary>
		virtual public System.Decimal? VlTaxregCp
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregCp);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregCp, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXREG_OF
		/// </summary>
		virtual public System.Decimal? VlTaxregOf
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregOf);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxregOf, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXPTA
		/// </summary>
		virtual public System.Decimal? VlTaxpta
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxpta);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxpta, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXANA
		/// </summary>
		virtual public System.Decimal? VlTaxana
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxana);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxana, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TAXIOF
		/// </summary>
		virtual public System.Decimal? VlTaxiof
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxiof);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTaxiof, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_VALDES
		/// </summary>
		virtual public System.Decimal? VlValdes
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlValdes);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlValdes, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_IRCORR
		/// </summary>
		virtual public System.Decimal? VlIrcorr
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlIrcorr);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlIrcorr, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_LIQNOT
		/// </summary>
		virtual public System.Decimal? VlLiqnot
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnot);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnot, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_LIQNOT_BV
		/// </summary>
		virtual public System.Decimal? VlLiqnotBv
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnotBv);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnotBv, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_LIQNOT_CB
		/// </summary>
		virtual public System.Decimal? VlLiqnotCb
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnotCb);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlLiqnotCb, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_COMASS
		/// </summary>
		virtual public System.Decimal? VlComass
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlComass);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlComass, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_AGENTE
		/// </summary>
		virtual public System.Decimal? VlAgente
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlAgente);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_CORRESP
		/// </summary>
		virtual public System.Decimal? VlCorresp
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCorresp);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCorresp, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_VDAVIS
		/// </summary>
		virtual public System.Decimal? VlVdavis
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlVdavis);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlVdavis, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_CPAVIS
		/// </summary>
		virtual public System.Decimal? VlCpavis
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCpavis);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCpavis, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_VDAOPC
		/// </summary>
		virtual public System.Decimal? VlVdaopc
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlVdaopc);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlVdaopc, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_CPAOPC
		/// </summary>
		virtual public System.Decimal? VlCpaopc
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCpaopc);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlCpaopc, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TERMO
		/// </summary>
		virtual public System.Decimal? VlTermo
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTermo);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_DEBENT
		/// </summary>
		virtual public System.Decimal? VlDebent
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlDebent);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlDebent, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_TITPUB
		/// </summary>
		virtual public System.Decimal? VlTitpub
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTitpub);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlTitpub, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_FUTURO
		/// </summary>
		virtual public System.Decimal? VlFuturo
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlFuturo);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlFuturo, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.VL_AJUSTE
		/// </summary>
		virtual public System.Decimal? VlAjuste
		{
			get
			{
				return base.GetSystemDecimal(VTbofinclMetadata.ColumnNames.VlAjuste);
			}
			
			set
			{
				base.SetSystemDecimal(VTbofinclMetadata.ColumnNames.VlAjuste, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOFINCL.CD_GRUP_CONT
		/// </summary>
		virtual public System.String CdGrupCont
		{
			get
			{
				return base.GetSystemString(VTbofinclMetadata.ColumnNames.CdGrupCont);
			}
			
			set
			{
				base.SetSystemString(VTbofinclMetadata.ColumnNames.CdGrupCont, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVTbofincl entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtDatmov
			{
				get
				{
					System.DateTime? data = entity.DtDatmov;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatmov = null;
					else entity.DtDatmov = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtNegocio
			{
				get
				{
					System.DateTime? data = entity.DtNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNegocio = null;
					else entity.DtNegocio = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdBolfat
			{
				get
				{
					System.String data = entity.CdBolfat;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdBolfat = null;
					else entity.CdBolfat = Convert.ToString(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClienteLiqfin
			{
				get
				{
					System.Decimal? data = entity.CdClienteLiqfin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClienteLiqfin = null;
					else entity.CdClienteLiqfin = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClienteCc
			{
				get
				{
					System.Decimal? data = entity.CdClienteCc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClienteCc = null;
					else entity.CdClienteCc = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTotneg
			{
				get
				{
					System.Decimal? data = entity.VlTotneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTotneg = null;
					else entity.VlTotneg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlValcor
			{
				get
				{
					System.Decimal? data = entity.VlValcor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlValcor = null;
					else entity.VlValcor = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlRepcor
			{
				get
				{
					System.Decimal? data = entity.VlRepcor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlRepcor = null;
					else entity.VlRepcor = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxregDt
			{
				get
				{
					System.Decimal? data = entity.VlTaxregDt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxregDt = null;
					else entity.VlTaxregDt = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxregCp
			{
				get
				{
					System.Decimal? data = entity.VlTaxregCp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxregCp = null;
					else entity.VlTaxregCp = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxregOf
			{
				get
				{
					System.Decimal? data = entity.VlTaxregOf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxregOf = null;
					else entity.VlTaxregOf = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxpta
			{
				get
				{
					System.Decimal? data = entity.VlTaxpta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxpta = null;
					else entity.VlTaxpta = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxana
			{
				get
				{
					System.Decimal? data = entity.VlTaxana;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxana = null;
					else entity.VlTaxana = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxiof
			{
				get
				{
					System.Decimal? data = entity.VlTaxiof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxiof = null;
					else entity.VlTaxiof = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlValdes
			{
				get
				{
					System.Decimal? data = entity.VlValdes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlValdes = null;
					else entity.VlValdes = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlIrcorr
			{
				get
				{
					System.Decimal? data = entity.VlIrcorr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlIrcorr = null;
					else entity.VlIrcorr = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlLiqnot
			{
				get
				{
					System.Decimal? data = entity.VlLiqnot;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlLiqnot = null;
					else entity.VlLiqnot = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlLiqnotBv
			{
				get
				{
					System.Decimal? data = entity.VlLiqnotBv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlLiqnotBv = null;
					else entity.VlLiqnotBv = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlLiqnotCb
			{
				get
				{
					System.Decimal? data = entity.VlLiqnotCb;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlLiqnotCb = null;
					else entity.VlLiqnotCb = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlComass
			{
				get
				{
					System.Decimal? data = entity.VlComass;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlComass = null;
					else entity.VlComass = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlAgente
			{
				get
				{
					System.Decimal? data = entity.VlAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlAgente = null;
					else entity.VlAgente = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCorresp
			{
				get
				{
					System.Decimal? data = entity.VlCorresp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCorresp = null;
					else entity.VlCorresp = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlVdavis
			{
				get
				{
					System.Decimal? data = entity.VlVdavis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlVdavis = null;
					else entity.VlVdavis = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCpavis
			{
				get
				{
					System.Decimal? data = entity.VlCpavis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCpavis = null;
					else entity.VlCpavis = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlVdaopc
			{
				get
				{
					System.Decimal? data = entity.VlVdaopc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlVdaopc = null;
					else entity.VlVdaopc = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCpaopc
			{
				get
				{
					System.Decimal? data = entity.VlCpaopc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCpaopc = null;
					else entity.VlCpaopc = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTermo
			{
				get
				{
					System.Decimal? data = entity.VlTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTermo = null;
					else entity.VlTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlDebent
			{
				get
				{
					System.Decimal? data = entity.VlDebent;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDebent = null;
					else entity.VlDebent = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTitpub
			{
				get
				{
					System.Decimal? data = entity.VlTitpub;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTitpub = null;
					else entity.VlTitpub = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlFuturo
			{
				get
				{
					System.Decimal? data = entity.VlFuturo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlFuturo = null;
					else entity.VlFuturo = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlAjuste
			{
				get
				{
					System.Decimal? data = entity.VlAjuste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlAjuste = null;
					else entity.VlAjuste = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdGrupCont
			{
				get
				{
					System.String data = entity.CdGrupCont;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdGrupCont = null;
					else entity.CdGrupCont = Convert.ToString(value);
				}
			}
			

			private esVTbofincl entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVTbofinclQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVTbofincl can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VTbofincl : esVTbofincl
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVTbofinclQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VTbofinclMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtDatmov
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.DtDatmov, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtNegocio
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.DtNegocio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdBolfat
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.CdBolfat, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClienteLiqfin
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.CdClienteLiqfin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClienteCc
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.CdClienteCc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTotneg
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTotneg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlValcor
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlValcor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlRepcor
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlRepcor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxregDt
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxregDt, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxregCp
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxregCp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxregOf
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxregOf, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxpta
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxpta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxana
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxana, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxiof
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTaxiof, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlValdes
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlValdes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlIrcorr
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlIrcorr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlLiqnot
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlLiqnot, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlLiqnotBv
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlLiqnotBv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlLiqnotCb
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlLiqnotCb, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlComass
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlComass, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlAgente
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlAgente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCorresp
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlCorresp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlVdavis
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlVdavis, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCpavis
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlCpavis, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlVdaopc
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlVdaopc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCpaopc
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlCpaopc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTermo
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlDebent
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlDebent, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTitpub
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlTitpub, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlFuturo
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlFuturo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlAjuste
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.VlAjuste, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdGrupCont
		{
			get
			{
				return new esQueryItem(this, VTbofinclMetadata.ColumnNames.CdGrupCont, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VTbofinclCollection")]
	public partial class VTbofinclCollection : esVTbofinclCollection, IEnumerable<VTbofincl>
	{
		public VTbofinclCollection()
		{

		}
		
		public static implicit operator List<VTbofincl>(VTbofinclCollection coll)
		{
			List<VTbofincl> list = new List<VTbofincl>();
			
			foreach (VTbofincl emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VTbofinclMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTbofinclQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VTbofincl(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VTbofincl();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VTbofinclQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTbofinclQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VTbofinclQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VTbofincl AddNew()
		{
			VTbofincl entity = base.AddNewEntity() as VTbofincl;
			
			return entity;
		}

		public VTbofincl FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as VTbofincl;
		}


		#region IEnumerable<VTbofincl> Members

		IEnumerator<VTbofincl> IEnumerable<VTbofincl>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VTbofincl;
			}
		}

		#endregion
		
		private VTbofinclQuery query;
	}


	/// <summary>
	/// Encapsulates the 'V_TBOFINCL' table
	/// </summary>

	[Serializable]
	public partial class VTbofincl : esVTbofincl
	{
		public VTbofincl()
		{

		}
	
		public VTbofincl(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VTbofinclMetadata.Meta();
			}
		}
		
		
		
		override protected esVTbofinclQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTbofinclQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VTbofinclQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTbofinclQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VTbofinclQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VTbofinclQuery query;
	}



	[Serializable]
	public partial class VTbofinclQuery : esVTbofinclQuery
	{
		public VTbofinclQuery()
		{

		}		
		
		public VTbofinclQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VTbofinclMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VTbofinclMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.DtDatmov, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTbofinclMetadata.PropertyNames.DtDatmov;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.DtNegocio, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTbofinclMetadata.PropertyNames.DtNegocio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.CdBolfat, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = VTbofinclMetadata.PropertyNames.CdBolfat;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.CdCliente, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.CdClienteLiqfin, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.CdClienteLiqfin;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.CdClienteCc, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.CdClienteCc;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTotneg, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTotneg;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlValcor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlValcor;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlRepcor, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlRepcor;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxregDt, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxregDt;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxregCp, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxregCp;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxregOf, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxregOf;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxpta, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxpta;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxana, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxana;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTaxiof, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTaxiof;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlValdes, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlValdes;	
			c.NumericPrecision = 10;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlIrcorr, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlIrcorr;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlLiqnot, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlLiqnot;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlLiqnotBv, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlLiqnotBv;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlLiqnotCb, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlLiqnotCb;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlComass, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlComass;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlAgente, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlAgente;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlCorresp, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlCorresp;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlVdavis, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlVdavis;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlCpavis, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlCpavis;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlVdaopc, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlVdaopc;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlCpaopc, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlCpaopc;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTermo, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTermo;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlDebent, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlDebent;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlTitpub, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlTitpub;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlFuturo, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlFuturo;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.VlAjuste, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbofinclMetadata.PropertyNames.VlAjuste;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbofinclMetadata.ColumnNames.CdGrupCont, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = VTbofinclMetadata.PropertyNames.CdGrupCont;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VTbofinclMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DtDatmov = "DT_DATMOV";
			 public const string DtNegocio = "DT_NEGOCIO";
			 public const string CdBolfat = "CD_BOLFAT";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string CdClienteLiqfin = "CD_CLIENTE_LIQFIN";
			 public const string CdClienteCc = "CD_CLIENTE_CC";
			 public const string VlTotneg = "VL_TOTNEG";
			 public const string VlValcor = "VL_VALCOR";
			 public const string VlRepcor = "VL_REPCOR";
			 public const string VlTaxregDt = "VL_TAXREG_DT";
			 public const string VlTaxregCp = "VL_TAXREG_CP";
			 public const string VlTaxregOf = "VL_TAXREG_OF";
			 public const string VlTaxpta = "VL_TAXPTA";
			 public const string VlTaxana = "VL_TAXANA";
			 public const string VlTaxiof = "VL_TAXIOF";
			 public const string VlValdes = "VL_VALDES";
			 public const string VlIrcorr = "VL_IRCORR";
			 public const string VlLiqnot = "VL_LIQNOT";
			 public const string VlLiqnotBv = "VL_LIQNOT_BV";
			 public const string VlLiqnotCb = "VL_LIQNOT_CB";
			 public const string VlComass = "VL_COMASS";
			 public const string VlAgente = "VL_AGENTE";
			 public const string VlCorresp = "VL_CORRESP";
			 public const string VlVdavis = "VL_VDAVIS";
			 public const string VlCpavis = "VL_CPAVIS";
			 public const string VlVdaopc = "VL_VDAOPC";
			 public const string VlCpaopc = "VL_CPAOPC";
			 public const string VlTermo = "VL_TERMO";
			 public const string VlDebent = "VL_DEBENT";
			 public const string VlTitpub = "VL_TITPUB";
			 public const string VlFuturo = "VL_FUTURO";
			 public const string VlAjuste = "VL_AJUSTE";
			 public const string CdGrupCont = "CD_GRUP_CONT";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DtDatmov = "DtDatmov";
			 public const string DtNegocio = "DtNegocio";
			 public const string CdBolfat = "CdBolfat";
			 public const string CdCliente = "CdCliente";
			 public const string CdClienteLiqfin = "CdClienteLiqfin";
			 public const string CdClienteCc = "CdClienteCc";
			 public const string VlTotneg = "VlTotneg";
			 public const string VlValcor = "VlValcor";
			 public const string VlRepcor = "VlRepcor";
			 public const string VlTaxregDt = "VlTaxregDt";
			 public const string VlTaxregCp = "VlTaxregCp";
			 public const string VlTaxregOf = "VlTaxregOf";
			 public const string VlTaxpta = "VlTaxpta";
			 public const string VlTaxana = "VlTaxana";
			 public const string VlTaxiof = "VlTaxiof";
			 public const string VlValdes = "VlValdes";
			 public const string VlIrcorr = "VlIrcorr";
			 public const string VlLiqnot = "VlLiqnot";
			 public const string VlLiqnotBv = "VlLiqnotBv";
			 public const string VlLiqnotCb = "VlLiqnotCb";
			 public const string VlComass = "VlComass";
			 public const string VlAgente = "VlAgente";
			 public const string VlCorresp = "VlCorresp";
			 public const string VlVdavis = "VlVdavis";
			 public const string VlCpavis = "VlCpavis";
			 public const string VlVdaopc = "VlVdaopc";
			 public const string VlCpaopc = "VlCpaopc";
			 public const string VlTermo = "VlTermo";
			 public const string VlDebent = "VlDebent";
			 public const string VlTitpub = "VlTitpub";
			 public const string VlFuturo = "VlFuturo";
			 public const string VlAjuste = "VlAjuste";
			 public const string CdGrupCont = "CdGrupCont";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VTbofinclMetadata))
			{
				if(VTbofinclMetadata.mapDelegates == null)
				{
					VTbofinclMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VTbofinclMetadata.meta == null)
				{
					VTbofinclMetadata.meta = new VTbofinclMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_DATMOV", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_NEGOCIO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_BOLFAT", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE_LIQFIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE_CC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TOTNEG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_VALCOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_REPCOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXREG_DT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXREG_CP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXREG_OF", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXPTA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXANA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXIOF", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_VALDES", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_IRCORR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_LIQNOT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_LIQNOT_BV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_LIQNOT_CB", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_COMASS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_AGENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_CORRESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_VDAVIS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_CPAVIS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_VDAOPC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_CPAOPC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TERMO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_DEBENT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TITPUB", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_FUTURO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_AJUSTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_GRUP_CONT", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "V_TBOFINCL";
				meta.Destination = "V_TBOFINCL";
				
				meta.spInsert = "proc_V_TBOFINCLInsert";				
				meta.spUpdate = "proc_V_TBOFINCLUpdate";		
				meta.spDelete = "proc_V_TBOFINCLDelete";
				meta.spLoadAll = "proc_V_TBOFINCLLoadAll";
				meta.spLoadByPrimaryKey = "proc_V_TBOFINCLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VTbofinclMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
