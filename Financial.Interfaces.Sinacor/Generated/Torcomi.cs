/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 18/06/2009 14:04:30
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTorcomiCollection : esEntityCollection
	{
		public esTorcomiCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TorcomiCollection";
		}

		#region Query Logic
		protected void InitQuery(esTorcomiQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTorcomiQuery);
		}
		#endregion
		
		virtual public Torcomi DetachEntity(Torcomi entity)
		{
			return base.DetachEntity(entity) as Torcomi;
		}
		
		virtual public Torcomi AttachEntity(Torcomi entity)
		{
			return base.AttachEntity(entity) as Torcomi;
		}
		
		virtual public void Combine(TorcomiCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Torcomi this[int index]
		{
			get
			{
				return base[index] as Torcomi;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Torcomi);
		}
	}



	[Serializable]
	abstract public class esTorcomi : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTorcomiQuery GetDynamicQuery()
		{
			return null;
		}

		public esTorcomi()
		{

		}

		public esTorcomi(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTorcomiQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTorcomiQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DtDatord": this.str.DtDatord = (string)value; break;							
						case "NrSeqord": this.str.NrSeqord = (string)value; break;							
						case "NrSubseq": this.str.NrSubseq = (string)value; break;							
						case "DtDatmov": this.str.DtDatmov = (string)value; break;							
						case "DtNegocio": this.str.DtNegocio = (string)value; break;							
						case "CdBolsamov": this.str.CdBolsamov = (string)value; break;							
						case "NrNegocio": this.str.NrNegocio = (string)value; break;							
						case "DvNegocio": this.str.DvNegocio = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "DvCliente": this.str.DvCliente = (string)value; break;							
						case "CdClienteBro": this.str.CdClienteBro = (string)value; break;							
						case "DvClienteBro": this.str.DvClienteBro = (string)value; break;							
						case "CdClienteFin": this.str.CdClienteFin = (string)value; break;							
						case "DvClienteFin": this.str.DvClienteFin = (string)value; break;							
						case "QtQtdesp": this.str.QtQtdesp = (string)value; break;							
						case "CdCarliq": this.str.CdCarliq = (string)value; break;							
						case "PcRedacr": this.str.PcRedacr = (string)value; break;							
						case "CdUsufim": this.str.CdUsufim = (string)value; break;							
						case "CdClifim": this.str.CdClifim = (string)value; break;							
						case "DvClifim": this.str.DvClifim = (string)value; break;							
						case "InOriger": this.str.InOriger = (string)value; break;							
						case "NrSeqdet": this.str.NrSeqdet = (string)value; break;							
						case "InCorresp": this.str.InCorresp = (string)value; break;							
						case "CdNegocio": this.str.CdNegocio = (string)value; break;							
						case "CdCorresp": this.str.CdCorresp = (string)value; break;							
						case "VlDolar": this.str.VlDolar = (string)value; break;							
						case "InLiquida": this.str.InLiquida = (string)value; break;							
						case "NrSeqope": this.str.NrSeqope = (string)value; break;							
						case "InHomebroker": this.str.InHomebroker = (string)value; break;							
						case "NrSeqcomi": this.str.NrSeqcomi = (string)value; break;							
						case "TpNegocio": this.str.TpNegocio = (string)value; break;							
						case "NmUsuario": this.str.NmUsuario = (string)value; break;							
						case "DtSistema": this.str.DtSistema = (string)value; break;							
						case "InExterno": this.str.InExterno = (string)value; break;							
						case "InFop": this.str.InFop = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtDatord":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatord = (System.DateTime?)value;
							break;
						
						case "NrSeqord":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqord = (System.Decimal?)value;
							break;
						
						case "NrSubseq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSubseq = (System.Decimal?)value;
							break;
						
						case "DtDatmov":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatmov = (System.DateTime?)value;
							break;
						
						case "DtNegocio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNegocio = (System.DateTime?)value;
							break;
						
						case "NrNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNegocio = (System.Decimal?)value;
							break;
						
						case "DvNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvNegocio = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DvCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvCliente = (System.Decimal?)value;
							break;
						
						case "CdClienteBro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdClienteBro = (System.Decimal?)value;
							break;
						
						case "DvClienteBro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvClienteBro = (System.Decimal?)value;
							break;
						
						case "CdClienteFin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdClienteFin = (System.Decimal?)value;
							break;
						
						case "DvClienteFin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvClienteFin = (System.Decimal?)value;
							break;
						
						case "QtQtdesp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtQtdesp = (System.Decimal?)value;
							break;
						
						case "CdCarliq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCarliq = (System.Decimal?)value;
							break;
						
						case "PcRedacr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcRedacr = (System.Decimal?)value;
							break;
						
						case "CdUsufim":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsufim = (System.Decimal?)value;
							break;
						
						case "DvClifim":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvClifim = (System.Decimal?)value;
							break;
						
						case "NrSeqdet":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqdet = (System.Decimal?)value;
							break;
						
						case "CdCorresp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCorresp = (System.Decimal?)value;
							break;
						
						case "VlDolar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDolar = (System.Decimal?)value;
							break;
						
						case "NrSeqope":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqope = (System.Decimal?)value;
							break;
						
						case "NrSeqcomi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqcomi = (System.Decimal?)value;
							break;
						
						case "DtSistema":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtSistema = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TORCOMI.DT_DATORD
		/// </summary>
		virtual public System.DateTime? DtDatord
		{
			get
			{
				return base.GetSystemDateTime(TorcomiMetadata.ColumnNames.DtDatord);
			}
			
			set
			{
				base.SetSystemDateTime(TorcomiMetadata.ColumnNames.DtDatord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_SEQORD
		/// </summary>
		virtual public System.Decimal? NrSeqord
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqord);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_SUBSEQ
		/// </summary>
		virtual public System.Decimal? NrSubseq
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrSubseq);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrSubseq, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DT_DATMOV
		/// </summary>
		virtual public System.DateTime? DtDatmov
		{
			get
			{
				return base.GetSystemDateTime(TorcomiMetadata.ColumnNames.DtDatmov);
			}
			
			set
			{
				base.SetSystemDateTime(TorcomiMetadata.ColumnNames.DtDatmov, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DT_NEGOCIO
		/// </summary>
		virtual public System.DateTime? DtNegocio
		{
			get
			{
				return base.GetSystemDateTime(TorcomiMetadata.ColumnNames.DtNegocio);
			}
			
			set
			{
				base.SetSystemDateTime(TorcomiMetadata.ColumnNames.DtNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_BOLSAMOV
		/// </summary>
		virtual public System.String CdBolsamov
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.CdBolsamov);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.CdBolsamov, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_NEGOCIO
		/// </summary>
		virtual public System.Decimal? NrNegocio
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DV_NEGOCIO
		/// </summary>
		virtual public System.Decimal? DvNegocio
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.DvNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.DvNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DV_CLIENTE
		/// </summary>
		virtual public System.Decimal? DvCliente
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.DvCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.DvCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CLIENTE_BRO
		/// </summary>
		virtual public System.Decimal? CdClienteBro
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdClienteBro);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdClienteBro, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DV_CLIENTE_BRO
		/// </summary>
		virtual public System.Decimal? DvClienteBro
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.DvClienteBro);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.DvClienteBro, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CLIENTE_FIN
		/// </summary>
		virtual public System.Decimal? CdClienteFin
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdClienteFin);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdClienteFin, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DV_CLIENTE_FIN
		/// </summary>
		virtual public System.Decimal? DvClienteFin
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.DvClienteFin);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.DvClienteFin, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.QT_QTDESP
		/// </summary>
		virtual public System.Decimal? QtQtdesp
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.QtQtdesp);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.QtQtdesp, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CARLIQ
		/// </summary>
		virtual public System.Decimal? CdCarliq
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdCarliq);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdCarliq, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.PC_REDACR
		/// </summary>
		virtual public System.Decimal? PcRedacr
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.PcRedacr);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.PcRedacr, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_USUFIM
		/// </summary>
		virtual public System.Decimal? CdUsufim
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdUsufim);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdUsufim, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CLIFIM
		/// </summary>
		virtual public System.String CdClifim
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.CdClifim);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.CdClifim, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DV_CLIFIM
		/// </summary>
		virtual public System.Decimal? DvClifim
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.DvClifim);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.DvClifim, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_ORIGER
		/// </summary>
		virtual public System.String InOriger
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InOriger);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InOriger, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_SEQDET
		/// </summary>
		virtual public System.Decimal? NrSeqdet
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqdet);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqdet, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_CORRESP
		/// </summary>
		virtual public System.String InCorresp
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InCorresp);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InCorresp, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_NEGOCIO
		/// </summary>
		virtual public System.String CdNegocio
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.CdNegocio);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.CdNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.CD_CORRESP
		/// </summary>
		virtual public System.Decimal? CdCorresp
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.CdCorresp);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.CdCorresp, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.VL_DOLAR
		/// </summary>
		virtual public System.Decimal? VlDolar
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.VlDolar);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.VlDolar, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_LIQUIDA
		/// </summary>
		virtual public System.String InLiquida
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InLiquida);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InLiquida, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_SEQOPE
		/// </summary>
		virtual public System.Decimal? NrSeqope
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqope);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_HOMEBROKER
		/// </summary>
		virtual public System.String InHomebroker
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InHomebroker);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InHomebroker, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NR_SEQCOMI
		/// </summary>
		virtual public System.Decimal? NrSeqcomi
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqcomi);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.NrSeqcomi, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.TP_NEGOCIO
		/// </summary>
		virtual public System.String TpNegocio
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.TpNegocio);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.TpNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.NM_USUARIO
		/// </summary>
		virtual public System.String NmUsuario
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.NmUsuario);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.NmUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.DT_SISTEMA
		/// </summary>
		virtual public System.DateTime? DtSistema
		{
			get
			{
				return base.GetSystemDateTime(TorcomiMetadata.ColumnNames.DtSistema);
			}
			
			set
			{
				base.SetSystemDateTime(TorcomiMetadata.ColumnNames.DtSistema, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_EXTERNO
		/// </summary>
		virtual public System.String InExterno
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InExterno);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.IN_FOP
		/// </summary>
		virtual public System.String InFop
		{
			get
			{
				return base.GetSystemString(TorcomiMetadata.ColumnNames.InFop);
			}
			
			set
			{
				base.SetSystemString(TorcomiMetadata.ColumnNames.InFop, value);
			}
		}
		
		/// <summary>
		/// Maps to TORCOMI.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TorcomiMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TorcomiMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTorcomi entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DtDatord
			{
				get
				{
					System.DateTime? data = entity.DtDatord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatord = null;
					else entity.DtDatord = Convert.ToDateTime(value);
				}
			}
				
			public System.String NrSeqord
			{
				get
				{
					System.Decimal? data = entity.NrSeqord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqord = null;
					else entity.NrSeqord = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrSubseq
			{
				get
				{
					System.Decimal? data = entity.NrSubseq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSubseq = null;
					else entity.NrSubseq = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtDatmov
			{
				get
				{
					System.DateTime? data = entity.DtDatmov;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatmov = null;
					else entity.DtDatmov = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtNegocio
			{
				get
				{
					System.DateTime? data = entity.DtNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNegocio = null;
					else entity.DtNegocio = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdBolsamov
			{
				get
				{
					System.String data = entity.CdBolsamov;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdBolsamov = null;
					else entity.CdBolsamov = Convert.ToString(value);
				}
			}
				
			public System.String NrNegocio
			{
				get
				{
					System.Decimal? data = entity.NrNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNegocio = null;
					else entity.NrNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvNegocio
			{
				get
				{
					System.Decimal? data = entity.DvNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvNegocio = null;
					else entity.DvNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvCliente
			{
				get
				{
					System.Decimal? data = entity.DvCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvCliente = null;
					else entity.DvCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClienteBro
			{
				get
				{
					System.Decimal? data = entity.CdClienteBro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClienteBro = null;
					else entity.CdClienteBro = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvClienteBro
			{
				get
				{
					System.Decimal? data = entity.DvClienteBro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvClienteBro = null;
					else entity.DvClienteBro = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClienteFin
			{
				get
				{
					System.Decimal? data = entity.CdClienteFin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClienteFin = null;
					else entity.CdClienteFin = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvClienteFin
			{
				get
				{
					System.Decimal? data = entity.DvClienteFin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvClienteFin = null;
					else entity.DvClienteFin = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtQtdesp
			{
				get
				{
					System.Decimal? data = entity.QtQtdesp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtQtdesp = null;
					else entity.QtQtdesp = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCarliq
			{
				get
				{
					System.Decimal? data = entity.CdCarliq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCarliq = null;
					else entity.CdCarliq = Convert.ToDecimal(value);
				}
			}
				
			public System.String PcRedacr
			{
				get
				{
					System.Decimal? data = entity.PcRedacr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcRedacr = null;
					else entity.PcRedacr = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdUsufim
			{
				get
				{
					System.Decimal? data = entity.CdUsufim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsufim = null;
					else entity.CdUsufim = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClifim
			{
				get
				{
					System.String data = entity.CdClifim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClifim = null;
					else entity.CdClifim = Convert.ToString(value);
				}
			}
				
			public System.String DvClifim
			{
				get
				{
					System.Decimal? data = entity.DvClifim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvClifim = null;
					else entity.DvClifim = Convert.ToDecimal(value);
				}
			}
				
			public System.String InOriger
			{
				get
				{
					System.String data = entity.InOriger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InOriger = null;
					else entity.InOriger = Convert.ToString(value);
				}
			}
				
			public System.String NrSeqdet
			{
				get
				{
					System.Decimal? data = entity.NrSeqdet;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqdet = null;
					else entity.NrSeqdet = Convert.ToDecimal(value);
				}
			}
				
			public System.String InCorresp
			{
				get
				{
					System.String data = entity.InCorresp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InCorresp = null;
					else entity.InCorresp = Convert.ToString(value);
				}
			}
				
			public System.String CdNegocio
			{
				get
				{
					System.String data = entity.CdNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNegocio = null;
					else entity.CdNegocio = Convert.ToString(value);
				}
			}
				
			public System.String CdCorresp
			{
				get
				{
					System.Decimal? data = entity.CdCorresp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCorresp = null;
					else entity.CdCorresp = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlDolar
			{
				get
				{
					System.Decimal? data = entity.VlDolar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDolar = null;
					else entity.VlDolar = Convert.ToDecimal(value);
				}
			}
				
			public System.String InLiquida
			{
				get
				{
					System.String data = entity.InLiquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InLiquida = null;
					else entity.InLiquida = Convert.ToString(value);
				}
			}
				
			public System.String NrSeqope
			{
				get
				{
					System.Decimal? data = entity.NrSeqope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqope = null;
					else entity.NrSeqope = Convert.ToDecimal(value);
				}
			}
				
			public System.String InHomebroker
			{
				get
				{
					System.String data = entity.InHomebroker;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InHomebroker = null;
					else entity.InHomebroker = Convert.ToString(value);
				}
			}
				
			public System.String NrSeqcomi
			{
				get
				{
					System.Decimal? data = entity.NrSeqcomi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqcomi = null;
					else entity.NrSeqcomi = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpNegocio
			{
				get
				{
					System.String data = entity.TpNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpNegocio = null;
					else entity.TpNegocio = Convert.ToString(value);
				}
			}
				
			public System.String NmUsuario
			{
				get
				{
					System.String data = entity.NmUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmUsuario = null;
					else entity.NmUsuario = Convert.ToString(value);
				}
			}
				
			public System.String DtSistema
			{
				get
				{
					System.DateTime? data = entity.DtSistema;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtSistema = null;
					else entity.DtSistema = Convert.ToDateTime(value);
				}
			}
				
			public System.String InExterno
			{
				get
				{
					System.String data = entity.InExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InExterno = null;
					else entity.InExterno = Convert.ToString(value);
				}
			}
				
			public System.String InFop
			{
				get
				{
					System.String data = entity.InFop;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InFop = null;
					else entity.InFop = Convert.ToString(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTorcomi entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTorcomiQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTorcomi can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Torcomi : esTorcomi
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTorcomiQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TorcomiMetadata.Meta();
			}
		}	
		

		public esQueryItem DtDatord
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DtDatord, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NrSeqord
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrSeqord, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrSubseq
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrSubseq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtDatmov
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DtDatmov, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtNegocio
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DtNegocio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdBolsamov
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdBolsamov, esSystemType.String);
			}
		} 
		
		public esQueryItem NrNegocio
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvNegocio
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DvNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvCliente
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DvCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClienteBro
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdClienteBro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvClienteBro
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DvClienteBro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClienteFin
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdClienteFin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvClienteFin
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DvClienteFin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtQtdesp
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.QtQtdesp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCarliq
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdCarliq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PcRedacr
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.PcRedacr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdUsufim
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdUsufim, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClifim
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdClifim, esSystemType.String);
			}
		} 
		
		public esQueryItem DvClifim
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DvClifim, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InOriger
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InOriger, esSystemType.String);
			}
		} 
		
		public esQueryItem NrSeqdet
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrSeqdet, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InCorresp
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InCorresp, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNegocio
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCorresp
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.CdCorresp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlDolar
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.VlDolar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InLiquida
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InLiquida, esSystemType.String);
			}
		} 
		
		public esQueryItem NrSeqope
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrSeqope, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InHomebroker
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InHomebroker, esSystemType.String);
			}
		} 
		
		public esQueryItem NrSeqcomi
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NrSeqcomi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpNegocio
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.TpNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem NmUsuario
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.NmUsuario, esSystemType.String);
			}
		} 
		
		public esQueryItem DtSistema
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.DtSistema, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem InExterno
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem InFop
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.InFop, esSystemType.String);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TorcomiMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TorcomiCollection")]
	public partial class TorcomiCollection : esTorcomiCollection, IEnumerable<Torcomi>
	{
		public TorcomiCollection()
		{

		}
		
		public static implicit operator List<Torcomi>(TorcomiCollection coll)
		{
			List<Torcomi> list = new List<Torcomi>();
			
			foreach (Torcomi emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TorcomiMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorcomiQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Torcomi(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Torcomi();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TorcomiQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorcomiQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TorcomiQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Torcomi AddNew()
		{
			Torcomi entity = base.AddNewEntity() as Torcomi;
			
			return entity;
		}

		public Torcomi FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Torcomi;
		}


		#region IEnumerable<Torcomi> Members

		IEnumerator<Torcomi> IEnumerable<Torcomi>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Torcomi;
			}
		}

		#endregion
		
		private TorcomiQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TORCOMI' table
	/// </summary>

	[Serializable]
	public partial class Torcomi : esTorcomi
	{
		public Torcomi()
		{

		}
	
		public Torcomi(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TorcomiMetadata.Meta();
			}
		}
		
		
		
		override protected esTorcomiQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorcomiQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TorcomiQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorcomiQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TorcomiQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TorcomiQuery query;
	}



	[Serializable]
	public partial class TorcomiQuery : esTorcomiQuery
	{
		public TorcomiQuery()
		{

		}		
		
		public TorcomiQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TorcomiMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TorcomiMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DtDatord, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorcomiMetadata.PropertyNames.DtDatord;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrSeqord, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrSeqord;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrSubseq, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrSubseq;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DtDatmov, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorcomiMetadata.PropertyNames.DtDatmov;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DtNegocio, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorcomiMetadata.PropertyNames.DtNegocio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdBolsamov, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdBolsamov;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrNegocio, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrNegocio;	
			c.NumericPrecision = 9;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DvNegocio, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.DvNegocio;	
			c.NumericPrecision = 1;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdNatope, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdCliente, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DvCliente, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.DvCliente;	
			c.NumericPrecision = 1;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdClienteBro, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdClienteBro;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DvClienteBro, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.DvClienteBro;	
			c.NumericPrecision = 1;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdClienteFin, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdClienteFin;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DvClienteFin, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.DvClienteFin;	
			c.NumericPrecision = 1;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.QtQtdesp, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.QtQtdesp;	
			c.NumericPrecision = 15;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdCarliq, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdCarliq;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.PcRedacr, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.PcRedacr;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdUsufim, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdUsufim;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdClifim, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdClifim;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DvClifim, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.DvClifim;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InOriger, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InOriger;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrSeqdet, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrSeqdet;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InCorresp, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InCorresp;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdNegocio, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdNegocio;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.CdCorresp, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.CdCorresp;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.VlDolar, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.VlDolar;	
			c.NumericPrecision = 7;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InLiquida, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InLiquida;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrSeqope, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrSeqope;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InHomebroker, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InHomebroker;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NrSeqcomi, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.NrSeqcomi;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.TpNegocio, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.TpNegocio;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.NmUsuario, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.NmUsuario;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.DtSistema, 33, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorcomiMetadata.PropertyNames.DtSistema;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InExterno, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InExterno;
			c.CharacterMaxLength = 18;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.InFop, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = TorcomiMetadata.PropertyNames.InFop;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorcomiMetadata.ColumnNames.Id, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorcomiMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TorcomiMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DtDatord = "DT_DATORD";
			 public const string NrSeqord = "NR_SEQORD";
			 public const string NrSubseq = "NR_SUBSEQ";
			 public const string DtDatmov = "DT_DATMOV";
			 public const string DtNegocio = "DT_NEGOCIO";
			 public const string CdBolsamov = "CD_BOLSAMOV";
			 public const string NrNegocio = "NR_NEGOCIO";
			 public const string DvNegocio = "DV_NEGOCIO";
			 public const string CdNatope = "CD_NATOPE";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string DvCliente = "DV_CLIENTE";
			 public const string CdClienteBro = "CD_CLIENTE_BRO";
			 public const string DvClienteBro = "DV_CLIENTE_BRO";
			 public const string CdClienteFin = "CD_CLIENTE_FIN";
			 public const string DvClienteFin = "DV_CLIENTE_FIN";
			 public const string QtQtdesp = "QT_QTDESP";
			 public const string CdCarliq = "CD_CARLIQ";
			 public const string PcRedacr = "PC_REDACR";
			 public const string CdUsufim = "CD_USUFIM";
			 public const string CdClifim = "CD_CLIFIM";
			 public const string DvClifim = "DV_CLIFIM";
			 public const string InOriger = "IN_ORIGER";
			 public const string NrSeqdet = "NR_SEQDET";
			 public const string InCorresp = "IN_CORRESP";
			 public const string CdNegocio = "CD_NEGOCIO";
			 public const string CdCorresp = "CD_CORRESP";
			 public const string VlDolar = "VL_DOLAR";
			 public const string InLiquida = "IN_LIQUIDA";
			 public const string NrSeqope = "NR_SEQOPE";
			 public const string InHomebroker = "IN_HOMEBROKER";
			 public const string NrSeqcomi = "NR_SEQCOMI";
			 public const string TpNegocio = "TP_NEGOCIO";
			 public const string NmUsuario = "NM_USUARIO";
			 public const string DtSistema = "DT_SISTEMA";
			 public const string InExterno = "IN_EXTERNO";
			 public const string InFop = "IN_FOP";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DtDatord = "DtDatord";
			 public const string NrSeqord = "NrSeqord";
			 public const string NrSubseq = "NrSubseq";
			 public const string DtDatmov = "DtDatmov";
			 public const string DtNegocio = "DtNegocio";
			 public const string CdBolsamov = "CdBolsamov";
			 public const string NrNegocio = "NrNegocio";
			 public const string DvNegocio = "DvNegocio";
			 public const string CdNatope = "CdNatope";
			 public const string CdCliente = "CdCliente";
			 public const string DvCliente = "DvCliente";
			 public const string CdClienteBro = "CdClienteBro";
			 public const string DvClienteBro = "DvClienteBro";
			 public const string CdClienteFin = "CdClienteFin";
			 public const string DvClienteFin = "DvClienteFin";
			 public const string QtQtdesp = "QtQtdesp";
			 public const string CdCarliq = "CdCarliq";
			 public const string PcRedacr = "PcRedacr";
			 public const string CdUsufim = "CdUsufim";
			 public const string CdClifim = "CdClifim";
			 public const string DvClifim = "DvClifim";
			 public const string InOriger = "InOriger";
			 public const string NrSeqdet = "NrSeqdet";
			 public const string InCorresp = "InCorresp";
			 public const string CdNegocio = "CdNegocio";
			 public const string CdCorresp = "CdCorresp";
			 public const string VlDolar = "VlDolar";
			 public const string InLiquida = "InLiquida";
			 public const string NrSeqope = "NrSeqope";
			 public const string InHomebroker = "InHomebroker";
			 public const string NrSeqcomi = "NrSeqcomi";
			 public const string TpNegocio = "TpNegocio";
			 public const string NmUsuario = "NmUsuario";
			 public const string DtSistema = "DtSistema";
			 public const string InExterno = "InExterno";
			 public const string InFop = "InFop";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TorcomiMetadata))
			{
				if(TorcomiMetadata.mapDelegates == null)
				{
					TorcomiMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TorcomiMetadata.meta == null)
				{
					TorcomiMetadata.meta = new TorcomiMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DT_DATORD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NR_SEQORD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_SUBSEQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_DATMOV", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_NEGOCIO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_BOLSAMOV", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NR_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE_BRO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_CLIENTE_BRO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE_FIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_CLIENTE_FIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QT_QTDESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CARLIQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PC_REDACR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_USUFIM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIFIM", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DV_CLIFIM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_ORIGER", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NR_SEQDET", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_CORRESP", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CORRESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_DOLAR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_LIQUIDA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NR_SEQOPE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_HOMEBROKER", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NR_SEQCOMI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_NEGOCIO", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NM_USUARIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_SISTEMA", new esTypeMap("TIMESTAMP", "System.DateTime"));
				meta.AddTypeMap("IN_EXTERNO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_FOP", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TORCOMI";
				meta.Destination = "TORCOMI";
				
				meta.spInsert = "proc_TORCOMIInsert";				
				meta.spUpdate = "proc_TORCOMIUpdate";		
				meta.spDelete = "proc_TORCOMIDelete";
				meta.spLoadAll = "proc_TORCOMILoadAll";
				meta.spLoadByPrimaryKey = "proc_TORCOMILoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TorcomiMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
