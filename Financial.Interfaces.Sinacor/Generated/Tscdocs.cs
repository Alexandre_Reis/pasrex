/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 13/09/2012 10:19:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Interfaces.Sinacor
{

    [Serializable]
    abstract public class esTscdocsCollection : esEntityCollection
    {
        public esTscdocsCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "TscdocsCollection";
        }

        #region Query Logic
        protected void InitQuery(esTscdocsQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esTscdocsQuery);
        }
        #endregion

        virtual public Tscdocs DetachEntity(Tscdocs entity)
        {
            return base.DetachEntity(entity) as Tscdocs;
        }

        virtual public Tscdocs AttachEntity(Tscdocs entity)
        {
            return base.AttachEntity(entity) as Tscdocs;
        }

        virtual public void Combine(TscdocsCollection collection)
        {
            base.Combine(collection);
        }

        new public Tscdocs this[int index]
        {
            get
            {
                return base[index] as Tscdocs;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(Tscdocs);
        }
    }



    [Serializable]
    abstract public class esTscdocs : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esTscdocsQuery GetDynamicQuery()
        {
            return null;
        }

        public esTscdocs()
        {

        }

        public esTscdocs(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Decimal id)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esTscdocsQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.Id == id);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        private bool LoadByPrimaryKeyDynamic(System.Decimal id)
        {
            esTscdocsQuery query = this.GetDynamicQuery();
            query.Where(query.Id == id);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
        {
            esParameters parms = new esParameters();
            parms.Add("ID", id);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "Id": this.str.Id = (string)value; break;
                        case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;
                        case "DtBalPatrimonial": this.str.DtBalPatrimonial = (string)value; break;
                        case "DtFichCad": this.str.DtFichCad = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "Id":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Id = (System.Decimal?)value;
                            break;

                        case "CdCpfcgc":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CdCpfcgc = (System.Decimal?)value;
                            break;

                        case "DtBalPatrimonial":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DtBalPatrimonial = (System.DateTime?)value;
                            break;

                        case "DtFichCad":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DtFichCad = (System.DateTime?)value;
                            break;

                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to TSCDOCS.ID
        /// </summary>
        virtual public System.Decimal? Id
        {
            get
            {
                return base.GetSystemDecimal(TscdocsMetadata.ColumnNames.Id);
            }

            set
            {
                base.SetSystemDecimal(TscdocsMetadata.ColumnNames.Id, value);
            }
        }

        /// <summary>
        /// Maps to TSCDOCS.CD_CPFCGC
        /// </summary>
        virtual public System.Decimal? CdCpfcgc
        {
            get
            {
                return base.GetSystemDecimal(TscdocsMetadata.ColumnNames.CdCpfcgc);
            }

            set
            {
                base.SetSystemDecimal(TscdocsMetadata.ColumnNames.CdCpfcgc, value);
            }
        }


        /// <summary>
        /// Maps to TSCDOCS.DT_BAL_PATRIMONIAL
        /// </summary>
        virtual public System.DateTime? DtBalPatrimonial
        {
            get
            {
                return base.GetSystemDateTime(TscdocsMetadata.ColumnNames.DtBalPatrimonial);
            }

            set
            {
                base.SetSystemDateTime(TscdocsMetadata.ColumnNames.DtBalPatrimonial, value);
            }
        }

        /// <summary>
        /// Maps to TSCDOCS.DT_FICH_CAD
        /// </summary>
        virtual public System.DateTime? DtFichCad
        {
            get
            {
                return base.GetSystemDateTime(TscdocsMetadata.ColumnNames.DtFichCad);
            }

            set
            {
                base.SetSystemDateTime(TscdocsMetadata.ColumnNames.DtFichCad, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esTscdocs entity)
            {
                this.entity = entity;
            }


            public System.String Id
            {
                get
                {
                    System.Decimal? data = entity.Id;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Id = null;
                    else entity.Id = Convert.ToDecimal(value);
                }
            }

            public System.String CdCpfcgc
            {
                get
                {
                    System.Decimal? data = entity.CdCpfcgc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CdCpfcgc = null;
                    else entity.CdCpfcgc = Convert.ToDecimal(value);
                }
            }


            public System.String DtBalPatrimonial
            {
                get
                {
                    System.DateTime? data = entity.DtBalPatrimonial;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DtBalPatrimonial = null;
                    else entity.DtBalPatrimonial = Convert.ToDateTime(value);
                }
            }

            public System.String DtFichCad
            {
                get
                {
                    System.DateTime? data = entity.DtFichCad;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DtFichCad = null;
                    else entity.DtFichCad = Convert.ToDateTime(value);
                }
            }

            private esTscdocs entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esTscdocsQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esTscdocs can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class Tscdocs : esTscdocs
    {


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esTscdocsQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return TscdocsMetadata.Meta();
            }
        }


        public esQueryItem Id
        {
            get
            {
                return new esQueryItem(this, TscdocsMetadata.ColumnNames.Id, esSystemType.Decimal);
            }
        }

        public esQueryItem CdCpfcgc
        {
            get
            {
                return new esQueryItem(this, TscdocsMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
            }
        }

        public esQueryItem DtBalPatrimonial
        {
            get
            {
                return new esQueryItem(this, TscdocsMetadata.ColumnNames.DtBalPatrimonial, esSystemType.DateTime);
            }
        }

        public esQueryItem DtFichCad
        {
            get
            {
                return new esQueryItem(this, TscdocsMetadata.ColumnNames.DtFichCad, esSystemType.DateTime);
            }
        }
    }



    [Serializable]
    [XmlType("TscdocsCollection")]
    public partial class TscdocsCollection : esTscdocsCollection, IEnumerable<Tscdocs>
    {
        public TscdocsCollection()
        {

        }

        public static implicit operator List<Tscdocs>(TscdocsCollection coll)
        {
            List<Tscdocs> list = new List<Tscdocs>();

            foreach (Tscdocs emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TscdocsMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TscdocsQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new Tscdocs(row);
        }

        override protected esEntity CreateEntity()
        {
            return new Tscdocs();
        }


        #endregion


        [BrowsableAttribute(false)]
        public TscdocsQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TscdocsQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(TscdocsQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public Tscdocs AddNew()
        {
            Tscdocs entity = base.AddNewEntity() as Tscdocs;

            return entity;
        }

        public Tscdocs FindByPrimaryKey(System.Decimal id)
        {
            return base.FindByPrimaryKey(id) as Tscdocs;
        }


        #region IEnumerable<Tscdocs> Members

        IEnumerator<Tscdocs> IEnumerable<Tscdocs>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as Tscdocs;
            }
        }

        #endregion

        private TscdocsQuery query;
    }


    /// <summary>
    /// Encapsulates the 'TSCDOCS' table
    /// </summary>

    [Serializable]
    public partial class Tscdocs : esTscdocs
    {
        public Tscdocs()
        {

        }

        public Tscdocs(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TscdocsMetadata.Meta();
            }
        }



        override protected esTscdocsQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TscdocsQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public TscdocsQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TscdocsQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(TscdocsQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private TscdocsQuery query;
    }



    [Serializable]
    public partial class TscdocsQuery : esTscdocsQuery
    {
        public TscdocsQuery()
        {

        }

        public TscdocsQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class TscdocsMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected TscdocsMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(TscdocsMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TscdocsMetadata.PropertyNames.Id;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(TscdocsMetadata.ColumnNames.CdCpfcgc, 1, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TscdocsMetadata.PropertyNames.CdCpfcgc;
            c.NumericPrecision = 15;
            _columns.Add(c);

            c = new esColumnMetadata(TscdocsMetadata.ColumnNames.DtBalPatrimonial, 2, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TscdocsMetadata.PropertyNames.DtBalPatrimonial;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);

            c = new esColumnMetadata(TscdocsMetadata.ColumnNames.DtFichCad, 2, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TscdocsMetadata.PropertyNames.DtFichCad;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);
        }
        #endregion

        static public TscdocsMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string Id = "ID";
            public const string CdCpfcgc = "CD_CPFCGC";
            public const string DtBalPatrimonial = "DT_BAL_PATRIMONIAL";
            public const string DtFichCad = "DT_FICH_CAD";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string Id = "Id";
            public const string CdCpfcgc = "CdCpfcgc";
            public const string DtBalPatrimonial = "DtBalPatrimonial";
            public const string DtFichCad = "DtFichCad";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(TscdocsMetadata))
            {
                if (TscdocsMetadata.mapDelegates == null)
                {
                    TscdocsMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (TscdocsMetadata.meta == null)
                {
                    TscdocsMetadata.meta = new TscdocsMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DT_BAL_PATRIMONIAL", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("DT_FICH_CAD", new esTypeMap("DATE", "System.DateTime"));

                meta.Source = "TSCDOCS";
                meta.Destination = "TSCDOCS";

                meta.spInsert = "proc_TSCDOCSInsert";
                meta.spUpdate = "proc_TSCDOCSUpdate";
                meta.spDelete = "proc_TSCDOCSDelete";
                meta.spLoadAll = "proc_TSCDOCSLoadAll";
                meta.spLoadByPrimaryKey = "proc_TSCDOCSLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private TscdocsMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
