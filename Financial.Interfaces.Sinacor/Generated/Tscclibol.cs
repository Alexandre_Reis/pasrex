/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/10/2013 12:42:32
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscclibolCollection : esEntityCollection
	{
		public esTscclibolCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscclibolCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscclibolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscclibolQuery);
		}
		#endregion
		
		virtual public Tscclibol DetachEntity(Tscclibol entity)
		{
			return base.DetachEntity(entity) as Tscclibol;
		}
		
		virtual public Tscclibol AttachEntity(Tscclibol entity)
		{
			return base.AttachEntity(entity) as Tscclibol;
		}
		
		virtual public void Combine(TscclibolCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscclibol this[int index]
		{
			get
			{
				return base[index] as Tscclibol;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscclibol);
		}
	}



	[Serializable]
	abstract public class esTscclibol : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscclibolQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscclibol()
		{

		}

		public esTscclibol(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscclibolQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscclibolQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;							
						case "CdAssessor": this.str.CdAssessor = (string)value; break;							
						case "CdClienteLiqfin": this.str.CdClienteLiqfin = (string)value; break;							
						case "InSituac": this.str.InSituac = (string)value; break;							
						case "CdAfinidade": this.str.CdAfinidade = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "CdCpfcgc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfcgc = (System.Decimal?)value;
							break;
						
						case "CdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAssessor = (System.Decimal?)value;
							break;
						
						case "CdClienteLiqfin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdClienteLiqfin = (System.Decimal?)value;
							break;
						
						case "CdAfinidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAfinidade = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCCLIBOL.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.CD_CPFCGC
		/// </summary>
		virtual public System.Decimal? CdCpfcgc
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.CdCpfcgc);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.CdCpfcgc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.CD_ASSESSOR
		/// </summary>
		virtual public System.Decimal? CdAssessor
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.CdAssessor);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.CdAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.CD_CLIENTE_LIQFIN
		/// </summary>
		virtual public System.Decimal? CdClienteLiqfin
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.CdClienteLiqfin);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.CdClienteLiqfin, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.IN_SITUAC
		/// </summary>
		virtual public System.String InSituac
		{
			get
			{
				return base.GetSystemString(TscclibolMetadata.ColumnNames.InSituac);
			}
			
			set
			{
				base.SetSystemString(TscclibolMetadata.ColumnNames.InSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIBOL.CD_AFINIDADE
		/// </summary>
		virtual public System.Decimal? CdAfinidade
		{
			get
			{
				return base.GetSystemDecimal(TscclibolMetadata.ColumnNames.CdAfinidade);
			}
			
			set
			{
				base.SetSystemDecimal(TscclibolMetadata.ColumnNames.CdAfinidade, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscclibol entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCpfcgc
			{
				get
				{
					System.Decimal? data = entity.CdCpfcgc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfcgc = null;
					else entity.CdCpfcgc = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAssessor
			{
				get
				{
					System.Decimal? data = entity.CdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAssessor = null;
					else entity.CdAssessor = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdClienteLiqfin
			{
				get
				{
					System.Decimal? data = entity.CdClienteLiqfin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClienteLiqfin = null;
					else entity.CdClienteLiqfin = Convert.ToDecimal(value);
				}
			}
				
			public System.String InSituac
			{
				get
				{
					System.String data = entity.InSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InSituac = null;
					else entity.InSituac = Convert.ToString(value);
				}
			}
				
			public System.String CdAfinidade
			{
				get
				{
					System.Decimal? data = entity.CdAfinidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAfinidade = null;
					else entity.CdAfinidade = Convert.ToDecimal(value);
				}
			}
			

			private esTscclibol entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscclibolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscclibol can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscclibol : esTscclibol
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscclibolQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscclibolMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCpfcgc
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAssessor
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.CdAssessor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdClienteLiqfin
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.CdClienteLiqfin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InSituac
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.InSituac, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAfinidade
		{
			get
			{
				return new esQueryItem(this, TscclibolMetadata.ColumnNames.CdAfinidade, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscclibolCollection")]
	public partial class TscclibolCollection : esTscclibolCollection, IEnumerable<Tscclibol>
	{
		public TscclibolCollection()
		{

		}
		
		public static implicit operator List<Tscclibol>(TscclibolCollection coll)
		{
			List<Tscclibol> list = new List<Tscclibol>();
			
			foreach (Tscclibol emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscclibolMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclibolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscclibol(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscclibol();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscclibolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclibolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscclibolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscclibol AddNew()
		{
			Tscclibol entity = base.AddNewEntity() as Tscclibol;
			
			return entity;
		}

		public Tscclibol FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscclibol;
		}


		#region IEnumerable<Tscclibol> Members

		IEnumerator<Tscclibol> IEnumerable<Tscclibol>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscclibol;
			}
		}

		#endregion
		
		private TscclibolQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCCLIBOL' table
	/// </summary>

	[Serializable]
	public partial class Tscclibol : esTscclibol
	{
		public Tscclibol()
		{

		}
	
		public Tscclibol(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscclibolMetadata.Meta();
			}
		}
		
		
		
		override protected esTscclibolQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclibolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscclibolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclibolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscclibolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscclibolQuery query;
	}



	[Serializable]
	public partial class TscclibolQuery : esTscclibolQuery
	{
		public TscclibolQuery()
		{

		}		
		
		public TscclibolQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscclibolMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscclibolMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.CdCliente, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.CdCpfcgc, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.CdCpfcgc;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.CdAssessor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.CdAssessor;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.CdClienteLiqfin, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.CdClienteLiqfin;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.InSituac, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclibolMetadata.PropertyNames.InSituac;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclibolMetadata.ColumnNames.CdAfinidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclibolMetadata.PropertyNames.CdAfinidade;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscclibolMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string CdCpfcgc = "CD_CPFCGC";
			 public const string CdAssessor = "CD_ASSESSOR";
			 public const string CdClienteLiqfin = "CD_CLIENTE_LIQFIN";
			 public const string InSituac = "IN_SITUAC";
			 public const string CdAfinidade = "CD_AFINIDADE";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdCliente = "CdCliente";
			 public const string CdCpfcgc = "CdCpfcgc";
			 public const string CdAssessor = "CdAssessor";
			 public const string CdClienteLiqfin = "CdClienteLiqfin";
			 public const string InSituac = "InSituac";
			 public const string CdAfinidade = "CdAfinidade";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscclibolMetadata))
			{
				if(TscclibolMetadata.mapDelegates == null)
				{
					TscclibolMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscclibolMetadata.meta == null)
				{
					TscclibolMetadata.meta = new TscclibolMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_ASSESSOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE_LIQFIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_SITUAC", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_AFINIDADE", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TSCCLIBOL";
				meta.Destination = "TSCCLIBOL";
				
				meta.spInsert = "proc_TSCCLIBOLInsert";				
				meta.spUpdate = "proc_TSCCLIBOLUpdate";		
				meta.spDelete = "proc_TSCCLIBOLDelete";
				meta.spLoadAll = "proc_TSCCLIBOLLoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCCLIBOLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscclibolMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
