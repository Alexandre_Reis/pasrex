/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/10/2013 12:42:39
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscclictaCollection : esEntityCollection
	{
		public esTscclictaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscclictaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscclictaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscclictaQuery);
		}
		#endregion
		
		virtual public Tscclicta DetachEntity(Tscclicta entity)
		{
			return base.DetachEntity(entity) as Tscclicta;
		}
		
		virtual public Tscclicta AttachEntity(Tscclicta entity)
		{
			return base.AttachEntity(entity) as Tscclicta;
		}
		
		virtual public void Combine(TscclictaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscclicta this[int index]
		{
			get
			{
				return base[index] as Tscclicta;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscclicta);
		}
	}



	[Serializable]
	abstract public class esTscclicta : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscclictaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscclicta()
		{

		}

		public esTscclicta(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscclictaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscclictaQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "CdBanco": this.str.CdBanco = (string)value; break;							
						case "CdAgencia": this.str.CdAgencia = (string)value; break;							
						case "DvAgencia": this.str.DvAgencia = (string)value; break;							
						case "NrConta": this.str.NrConta = (string)value; break;							
						case "DvConta": this.str.DvConta = (string)value; break;							
						case "NrContaCompl": this.str.NrContaCompl = (string)value; break;							
						case "InPrincipal": this.str.InPrincipal = (string)value; break;							
						case "InInativa": this.str.InInativa = (string)value; break;							
						case "CdEmpresa": this.str.CdEmpresa = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "TpOcorrencia": this.str.TpOcorrencia = (string)value; break;							
						case "TpConta": this.str.TpConta = (string)value; break;							
						case "InConjunta": this.str.InConjunta = (string)value; break;							
						case "CdCpfConjunta": this.str.CdCpfConjunta = (string)value; break;							
						case "NmConjunta": this.str.NmConjunta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "CdEmpresa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEmpresa = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "CdCpfConjunta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfConjunta = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCCLICTA.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscclictaMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscclictaMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TscclictaMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TscclictaMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_BANCO
		/// </summary>
		virtual public System.String CdBanco
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.CdBanco);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.CdBanco, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_AGENCIA
		/// </summary>
		virtual public System.String CdAgencia
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.CdAgencia);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.CdAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.DV_AGENCIA
		/// </summary>
		virtual public System.String DvAgencia
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.DvAgencia);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.DvAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.NR_CONTA
		/// </summary>
		virtual public System.String NrConta
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.NrConta);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.NrConta, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.DV_CONTA
		/// </summary>
		virtual public System.String DvConta
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.DvConta);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.DvConta, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.NR_CONTA_COMPL
		/// </summary>
		virtual public System.String NrContaCompl
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.NrContaCompl);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.NrContaCompl, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.IN_PRINCIPAL
		/// </summary>
		virtual public System.String InPrincipal
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.InPrincipal);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.InPrincipal, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.IN_INATIVA
		/// </summary>
		virtual public System.String InInativa
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.InInativa);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.InInativa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_EMPRESA
		/// </summary>
		virtual public System.Decimal? CdEmpresa
		{
			get
			{
				return base.GetSystemDecimal(TscclictaMetadata.ColumnNames.CdEmpresa);
			}
			
			set
			{
				base.SetSystemDecimal(TscclictaMetadata.ColumnNames.CdEmpresa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TscclictaMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TscclictaMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.TP_OCORRENCIA
		/// </summary>
		virtual public System.String TpOcorrencia
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.TpOcorrencia);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.TpOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.TP_CONTA
		/// </summary>
		virtual public System.String TpConta
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.TpConta);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.TpConta, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.IN_CONJUNTA
		/// </summary>
		virtual public System.String InConjunta
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.InConjunta);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.InConjunta, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.CD_CPF_CONJUNTA
		/// </summary>
		virtual public System.Decimal? CdCpfConjunta
		{
			get
			{
				return base.GetSystemDecimal(TscclictaMetadata.ColumnNames.CdCpfConjunta);
			}
			
			set
			{
				base.SetSystemDecimal(TscclictaMetadata.ColumnNames.CdCpfConjunta, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICTA.NM_CONJUNTA
		/// </summary>
		virtual public System.String NmConjunta
		{
			get
			{
				return base.GetSystemString(TscclictaMetadata.ColumnNames.NmConjunta);
			}
			
			set
			{
				base.SetSystemString(TscclictaMetadata.ColumnNames.NmConjunta, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscclicta entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdBanco
			{
				get
				{
					System.String data = entity.CdBanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdBanco = null;
					else entity.CdBanco = Convert.ToString(value);
				}
			}
				
			public System.String CdAgencia
			{
				get
				{
					System.String data = entity.CdAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAgencia = null;
					else entity.CdAgencia = Convert.ToString(value);
				}
			}
				
			public System.String DvAgencia
			{
				get
				{
					System.String data = entity.DvAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvAgencia = null;
					else entity.DvAgencia = Convert.ToString(value);
				}
			}
				
			public System.String NrConta
			{
				get
				{
					System.String data = entity.NrConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrConta = null;
					else entity.NrConta = Convert.ToString(value);
				}
			}
				
			public System.String DvConta
			{
				get
				{
					System.String data = entity.DvConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvConta = null;
					else entity.DvConta = Convert.ToString(value);
				}
			}
				
			public System.String NrContaCompl
			{
				get
				{
					System.String data = entity.NrContaCompl;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrContaCompl = null;
					else entity.NrContaCompl = Convert.ToString(value);
				}
			}
				
			public System.String InPrincipal
			{
				get
				{
					System.String data = entity.InPrincipal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPrincipal = null;
					else entity.InPrincipal = Convert.ToString(value);
				}
			}
				
			public System.String InInativa
			{
				get
				{
					System.String data = entity.InInativa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InInativa = null;
					else entity.InInativa = Convert.ToString(value);
				}
			}
				
			public System.String CdEmpresa
			{
				get
				{
					System.Decimal? data = entity.CdEmpresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEmpresa = null;
					else entity.CdEmpresa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpOcorrencia
			{
				get
				{
					System.String data = entity.TpOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOcorrencia = null;
					else entity.TpOcorrencia = Convert.ToString(value);
				}
			}
				
			public System.String TpConta
			{
				get
				{
					System.String data = entity.TpConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpConta = null;
					else entity.TpConta = Convert.ToString(value);
				}
			}
				
			public System.String InConjunta
			{
				get
				{
					System.String data = entity.InConjunta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InConjunta = null;
					else entity.InConjunta = Convert.ToString(value);
				}
			}
				
			public System.String CdCpfConjunta
			{
				get
				{
					System.Decimal? data = entity.CdCpfConjunta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfConjunta = null;
					else entity.CdCpfConjunta = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmConjunta
			{
				get
				{
					System.String data = entity.NmConjunta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmConjunta = null;
					else entity.NmConjunta = Convert.ToString(value);
				}
			}
			

			private esTscclicta entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscclictaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscclicta can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscclicta : esTscclicta
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscclictaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscclictaMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdBanco
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdBanco, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAgencia
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdAgencia, esSystemType.String);
			}
		} 
		
		public esQueryItem DvAgencia
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.DvAgencia, esSystemType.String);
			}
		} 
		
		public esQueryItem NrConta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.NrConta, esSystemType.String);
			}
		} 
		
		public esQueryItem DvConta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.DvConta, esSystemType.String);
			}
		} 
		
		public esQueryItem NrContaCompl
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.NrContaCompl, esSystemType.String);
			}
		} 
		
		public esQueryItem InPrincipal
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.InPrincipal, esSystemType.String);
			}
		} 
		
		public esQueryItem InInativa
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.InInativa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEmpresa
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdEmpresa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpOcorrencia
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.TpOcorrencia, esSystemType.String);
			}
		} 
		
		public esQueryItem TpConta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.TpConta, esSystemType.String);
			}
		} 
		
		public esQueryItem InConjunta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.InConjunta, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCpfConjunta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.CdCpfConjunta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmConjunta
		{
			get
			{
				return new esQueryItem(this, TscclictaMetadata.ColumnNames.NmConjunta, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscclictaCollection")]
	public partial class TscclictaCollection : esTscclictaCollection, IEnumerable<Tscclicta>
	{
		public TscclictaCollection()
		{

		}
		
		public static implicit operator List<Tscclicta>(TscclictaCollection coll)
		{
			List<Tscclicta> list = new List<Tscclicta>();
			
			foreach (Tscclicta emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscclictaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclictaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscclicta(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscclicta();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscclictaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclictaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscclictaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscclicta AddNew()
		{
			Tscclicta entity = base.AddNewEntity() as Tscclicta;
			
			return entity;
		}

		public Tscclicta FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscclicta;
		}


		#region IEnumerable<Tscclicta> Members

		IEnumerator<Tscclicta> IEnumerable<Tscclicta>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscclicta;
			}
		}

		#endregion
		
		private TscclictaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCCLICTA' table
	/// </summary>

	[Serializable]
	public partial class Tscclicta : esTscclicta
	{
		public Tscclicta()
		{

		}
	
		public Tscclicta(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscclictaMetadata.Meta();
			}
		}
		
		
		
		override protected esTscclictaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclictaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscclictaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclictaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscclictaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscclictaQuery query;
	}



	[Serializable]
	public partial class TscclictaQuery : esTscclictaQuery
	{
		public TscclictaQuery()
		{

		}		
		
		public TscclictaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscclictaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscclictaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclictaMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdCliente, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdBanco, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdBanco;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdAgencia, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdAgencia;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.DvAgencia, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.DvAgencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.NrConta, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.NrConta;
			c.CharacterMaxLength = 13;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.DvConta, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.DvConta;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.NrContaCompl, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.NrContaCompl;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.InPrincipal, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.InPrincipal;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.InInativa, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.InInativa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdEmpresa, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdEmpresa;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdUsuario, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.TpOcorrencia, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.TpOcorrencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.TpConta, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.TpConta;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.InConjunta, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.InConjunta;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.CdCpfConjunta, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclictaMetadata.PropertyNames.CdCpfConjunta;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclictaMetadata.ColumnNames.NmConjunta, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclictaMetadata.PropertyNames.NmConjunta;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscclictaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string CdBanco = "CD_BANCO";
			 public const string CdAgencia = "CD_AGENCIA";
			 public const string DvAgencia = "DV_AGENCIA";
			 public const string NrConta = "NR_CONTA";
			 public const string DvConta = "DV_CONTA";
			 public const string NrContaCompl = "NR_CONTA_COMPL";
			 public const string InPrincipal = "IN_PRINCIPAL";
			 public const string InInativa = "IN_INATIVA";
			 public const string CdEmpresa = "CD_EMPRESA";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string TpOcorrencia = "TP_OCORRENCIA";
			 public const string TpConta = "TP_CONTA";
			 public const string InConjunta = "IN_CONJUNTA";
			 public const string CdCpfConjunta = "CD_CPF_CONJUNTA";
			 public const string NmConjunta = "NM_CONJUNTA";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdCliente = "CdCliente";
			 public const string CdBanco = "CdBanco";
			 public const string CdAgencia = "CdAgencia";
			 public const string DvAgencia = "DvAgencia";
			 public const string NrConta = "NrConta";
			 public const string DvConta = "DvConta";
			 public const string NrContaCompl = "NrContaCompl";
			 public const string InPrincipal = "InPrincipal";
			 public const string InInativa = "InInativa";
			 public const string CdEmpresa = "CdEmpresa";
			 public const string CdUsuario = "CdUsuario";
			 public const string TpOcorrencia = "TpOcorrencia";
			 public const string TpConta = "TpConta";
			 public const string InConjunta = "InConjunta";
			 public const string CdCpfConjunta = "CdCpfConjunta";
			 public const string NmConjunta = "NmConjunta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscclictaMetadata))
			{
				if(TscclictaMetadata.mapDelegates == null)
				{
					TscclictaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscclictaMetadata.meta == null)
				{
					TscclictaMetadata.meta = new TscclictaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_BANCO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_AGENCIA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DV_AGENCIA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NR_CONTA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DV_CONTA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NR_CONTA_COMPL", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_PRINCIPAL", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("IN_INATIVA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_EMPRESA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_OCORRENCIA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_CONTA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_CONJUNTA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CPF_CONJUNTA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_CONJUNTA", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "TSCCLICTA";
				meta.Destination = "TSCCLICTA";
				
				meta.spInsert = "proc_TSCCLICTAInsert";				
				meta.spUpdate = "proc_TSCCLICTAUpdate";		
				meta.spDelete = "proc_TSCCLICTADelete";
				meta.spLoadAll = "proc_TSCCLICTALoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCCLICTALoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscclictaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
