/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 28/01/2010 16:39:36
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		









		
				
				








	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTbotitfatCollection : esEntityCollection
	{
		public esTbotitfatCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TbotitfatCollection";
		}

		#region Query Logic
		protected void InitQuery(esTbotitfatQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTbotitfatQuery);
		}
		#endregion
		
		virtual public Tbotitfat DetachEntity(Tbotitfat entity)
		{
			return base.DetachEntity(entity) as Tbotitfat;
		}
		
		virtual public Tbotitfat AttachEntity(Tbotitfat entity)
		{
			return base.AttachEntity(entity) as Tbotitfat;
		}
		
		virtual public void Combine(TbotitfatCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tbotitfat this[int index]
		{
			get
			{
				return base[index] as Tbotitfat;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tbotitfat);
		}
	}



	[Serializable]
	abstract public class esTbotitfat : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTbotitfatQuery GetDynamicQuery()
		{
			return null;
		}

		public esTbotitfat()
		{

		}

		public esTbotitfat(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTbotitfatQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTbotitfatQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCodneg": this.str.CdCodneg = (string)value; break;							
						case "DtInifat": this.str.DtInifat = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "NrFatcot": this.str.NrFatcot = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtInifat":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtInifat = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "NrFatcot":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrFatcot = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TBOTITFAT.CD_CODNEG
		/// </summary>
		virtual public System.String CdCodneg
		{
			get
			{
				return base.GetSystemString(TbotitfatMetadata.ColumnNames.CdCodneg);
			}
			
			set
			{
				base.SetSystemString(TbotitfatMetadata.ColumnNames.CdCodneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITFAT.DT_ININEG
		/// </summary>
		virtual public System.DateTime? DtInifat
		{
			get
			{
				return base.GetSystemDateTime(TbotitfatMetadata.ColumnNames.DtInifat);
			}
			
			set
			{
				base.SetSystemDateTime(TbotitfatMetadata.ColumnNames.DtInifat, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITFAT.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TbotitfatMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TbotitfatMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITFAT.NR_FATCOT
		/// </summary>
		virtual public System.Decimal? NrFatcot
		{
			get
			{
				return base.GetSystemDecimal(TbotitfatMetadata.ColumnNames.NrFatcot);
			}
			
			set
			{
				base.SetSystemDecimal(TbotitfatMetadata.ColumnNames.NrFatcot, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTbotitfat entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCodneg
			{
				get
				{
					System.String data = entity.CdCodneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodneg = null;
					else entity.CdCodneg = Convert.ToString(value);
				}
			}
				
			public System.String DtInifat
			{
				get
				{
					System.DateTime? data = entity.DtInifat;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtInifat = null;
					else entity.DtInifat = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrFatcot
			{
				get
				{
					System.Decimal? data = entity.NrFatcot;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrFatcot = null;
					else entity.NrFatcot = Convert.ToDecimal(value);
				}
			}
			

			private esTbotitfat entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTbotitfatQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTbotitfat can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tbotitfat : esTbotitfat
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTbotitfatQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TbotitfatMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCodneg
		{
			get
			{
				return new esQueryItem(this, TbotitfatMetadata.ColumnNames.CdCodneg, esSystemType.String);
			}
		} 
		
		public esQueryItem DtInifat
		{
			get
			{
				return new esQueryItem(this, TbotitfatMetadata.ColumnNames.DtInifat, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TbotitfatMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrFatcot
		{
			get
			{
				return new esQueryItem(this, TbotitfatMetadata.ColumnNames.NrFatcot, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TbotitfatCollection")]
	public partial class TbotitfatCollection : esTbotitfatCollection, IEnumerable<Tbotitfat>
	{
		public TbotitfatCollection()
		{

		}
		
		public static implicit operator List<Tbotitfat>(TbotitfatCollection coll)
		{
			List<Tbotitfat> list = new List<Tbotitfat>();
			
			foreach (Tbotitfat emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TbotitfatMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbotitfatQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tbotitfat(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tbotitfat();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TbotitfatQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbotitfatQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TbotitfatQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tbotitfat AddNew()
		{
			Tbotitfat entity = base.AddNewEntity() as Tbotitfat;
			
			return entity;
		}

		public Tbotitfat FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tbotitfat;
		}


		#region IEnumerable<Tbotitfat> Members

		IEnumerator<Tbotitfat> IEnumerable<Tbotitfat>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tbotitfat;
			}
		}

		#endregion
		
		private TbotitfatQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TBOTITFAT' table
	/// </summary>

	[Serializable]
	public partial class Tbotitfat : esTbotitfat
	{
		public Tbotitfat()
		{

		}
	
		public Tbotitfat(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TbotitfatMetadata.Meta();
			}
		}
		
		
		
		override protected esTbotitfatQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbotitfatQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TbotitfatQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbotitfatQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TbotitfatQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TbotitfatQuery query;
	}



	[Serializable]
	public partial class TbotitfatQuery : esTbotitfatQuery
	{
		public TbotitfatQuery()
		{

		}		
		
		public TbotitfatQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TbotitfatMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TbotitfatMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TbotitfatMetadata.ColumnNames.CdCodneg, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitfatMetadata.PropertyNames.CdCodneg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitfatMetadata.ColumnNames.DtInifat, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbotitfatMetadata.PropertyNames.DtInifat;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitfatMetadata.ColumnNames.Id, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbotitfatMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitfatMetadata.ColumnNames.NrFatcot, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbotitfatMetadata.PropertyNames.NrFatcot;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TbotitfatMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCodneg = "CD_CODNEG";
			 public const string DtInifat = "DT_INIFAT";
			 public const string Id = "ID";
			 public const string NrFatcot = "NR_FATCOT";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCodneg = "CdCodneg";
			 public const string DtInifat = "DtInifat";
			 public const string Id = "Id";
			 public const string NrFatcot = "NrFatcot";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TbotitfatMetadata))
			{
				if(TbotitfatMetadata.mapDelegates == null)
				{
					TbotitfatMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TbotitfatMetadata.meta == null)
				{
					TbotitfatMetadata.meta = new TbotitfatMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CODNEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_ININEG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_FATCOT", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TBOTITFAT";
				meta.Destination = "TBOTITFAT";
				
				meta.spInsert = "proc_TBOTITFATInsert";				
				meta.spUpdate = "proc_TBOTITFATUpdate";		
				meta.spDelete = "proc_TBOTITFATDelete";
				meta.spLoadAll = "proc_TBOTITFATLoadAll";
				meta.spLoadByPrimaryKey = "proc_TBOTITFATLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TbotitfatMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
