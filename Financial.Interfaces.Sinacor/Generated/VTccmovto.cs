/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 17/11/2009 18:42:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		























		






	
















				
		
		




		









		
				
				








	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVTccmovtoCollection : esEntityCollection
	{
		public esVTccmovtoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VTccmovtoCollection";
		}

		#region Query Logic
		protected void InitQuery(esVTccmovtoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVTccmovtoQuery);
		}
		#endregion
		
		virtual public VTccmovto DetachEntity(VTccmovto entity)
		{
			return base.DetachEntity(entity) as VTccmovto;
		}
		
		virtual public VTccmovto AttachEntity(VTccmovto entity)
		{
			return base.AttachEntity(entity) as VTccmovto;
		}
		
		virtual public void Combine(VTccmovtoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VTccmovto this[int index]
		{
			get
			{
				return base[index] as VTccmovto;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VTccmovto);
		}
	}



	[Serializable]
	abstract public class esVTccmovto : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVTccmovtoQuery GetDynamicQuery()
		{
			return null;
		}

		public esVTccmovto()
		{

		}

		public esVTccmovto(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVTccmovtoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVTccmovtoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "DtLancamento": this.str.DtLancamento = (string)value; break;							
						case "DtLiquidacao": this.str.DtLiquidacao = (string)value; break;							
						case "CdAtividade": this.str.CdAtividade = (string)value; break;							
						case "CdHistorico": this.str.CdHistorico = (string)value; break;							
						case "DsLancamento": this.str.DsLancamento = (string)value; break;							
						case "VlLancamento": this.str.VlLancamento = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DtLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtLancamento = (System.DateTime?)value;
							break;
						
						case "DtLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtLiquidacao = (System.DateTime?)value;
							break;
						
						case "CdHistorico":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdHistorico = (System.Decimal?)value;
							break;
						
						case "VlLancamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlLancamento = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to V_TCCMOVTO.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(VTccmovtoMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(VTccmovtoMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.DT_LANCAMENTO
		/// </summary>
		virtual public System.DateTime? DtLancamento
		{
			get
			{
				return base.GetSystemDateTime(VTccmovtoMetadata.ColumnNames.DtLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(VTccmovtoMetadata.ColumnNames.DtLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.DT_LIQUIDACAO
		/// </summary>
		virtual public System.DateTime? DtLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(VTccmovtoMetadata.ColumnNames.DtLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(VTccmovtoMetadata.ColumnNames.DtLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.CD_ATIVIDADE
		/// </summary>
		virtual public System.String CdAtividade
		{
			get
			{
				return base.GetSystemString(VTccmovtoMetadata.ColumnNames.CdAtividade);
			}
			
			set
			{
				base.SetSystemString(VTccmovtoMetadata.ColumnNames.CdAtividade, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.CD_HISTORICO
		/// </summary>
		virtual public System.Decimal? CdHistorico
		{
			get
			{
				return base.GetSystemDecimal(VTccmovtoMetadata.ColumnNames.CdHistorico);
			}
			
			set
			{
				base.SetSystemDecimal(VTccmovtoMetadata.ColumnNames.CdHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.DS_LANCAMENTO
		/// </summary>
		virtual public System.String DsLancamento
		{
			get
			{
				return base.GetSystemString(VTccmovtoMetadata.ColumnNames.DsLancamento);
			}
			
			set
			{
				base.SetSystemString(VTccmovtoMetadata.ColumnNames.DsLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.VL_LANCAMENTO
		/// </summary>
		virtual public System.Decimal? VlLancamento
		{
			get
			{
				return base.GetSystemDecimal(VTccmovtoMetadata.ColumnNames.VlLancamento);
			}
			
			set
			{
				base.SetSystemDecimal(VTccmovtoMetadata.ColumnNames.VlLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TCCMOVTO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VTccmovtoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VTccmovtoMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVTccmovto entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtLancamento
			{
				get
				{
					System.DateTime? data = entity.DtLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtLancamento = null;
					else entity.DtLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DtLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtLiquidacao = null;
					else entity.DtLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtividade
			{
				get
				{
					System.String data = entity.CdAtividade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtividade = null;
					else entity.CdAtividade = Convert.ToString(value);
				}
			}
				
			public System.String CdHistorico
			{
				get
				{
					System.Decimal? data = entity.CdHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdHistorico = null;
					else entity.CdHistorico = Convert.ToDecimal(value);
				}
			}
				
			public System.String DsLancamento
			{
				get
				{
					System.String data = entity.DsLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsLancamento = null;
					else entity.DsLancamento = Convert.ToString(value);
				}
			}
				
			public System.String VlLancamento
			{
				get
				{
					System.Decimal? data = entity.VlLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlLancamento = null;
					else entity.VlLancamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esVTccmovto entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVTccmovtoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVTccmovto can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VTccmovto : esVTccmovto
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVTccmovtoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VTccmovtoMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtLancamento
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.DtLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtLiquidacao
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.DtLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtividade
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.CdAtividade, esSystemType.String);
			}
		} 
		
		public esQueryItem CdHistorico
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.CdHistorico, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DsLancamento
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.DsLancamento, esSystemType.String);
			}
		} 
		
		public esQueryItem VlLancamento
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.VlLancamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VTccmovtoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VTccmovtoCollection")]
	public partial class VTccmovtoCollection : esVTccmovtoCollection, IEnumerable<VTccmovto>
	{
		public VTccmovtoCollection()
		{

		}
		
		public static implicit operator List<VTccmovto>(VTccmovtoCollection coll)
		{
			List<VTccmovto> list = new List<VTccmovto>();
			
			foreach (VTccmovto emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VTccmovtoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTccmovtoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VTccmovto(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VTccmovto();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VTccmovtoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTccmovtoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VTccmovtoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VTccmovto AddNew()
		{
			VTccmovto entity = base.AddNewEntity() as VTccmovto;
			
			return entity;
		}

		public VTccmovto FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as VTccmovto;
		}


		#region IEnumerable<VTccmovto> Members

		IEnumerator<VTccmovto> IEnumerable<VTccmovto>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VTccmovto;
			}
		}

		#endregion
		
		private VTccmovtoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'V_TCCMOVTO' table
	/// </summary>

	[Serializable]
	public partial class VTccmovto : esVTccmovto
	{
		public VTccmovto()
		{

		}
	
		public VTccmovto(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VTccmovtoMetadata.Meta();
			}
		}
		
		
		
		override protected esVTccmovtoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTccmovtoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VTccmovtoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTccmovtoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VTccmovtoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VTccmovtoQuery query;
	}



	[Serializable]
	public partial class VTccmovtoQuery : esVTccmovtoQuery
	{
		public VTccmovtoQuery()
		{

		}		
		
		public VTccmovtoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VTccmovtoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VTccmovtoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.CdCliente, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.DtLancamento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.DtLancamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.DtLiquidacao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.DtLiquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.CdAtividade, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.CdAtividade;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.CdHistorico, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.CdHistorico;	
			c.NumericPrecision = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.DsLancamento, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.DsLancamento;
			c.CharacterMaxLength = 80;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.VlLancamento, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.VlLancamento;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTccmovtoMetadata.ColumnNames.Id, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTccmovtoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VTccmovtoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCliente = "CD_CLIENTE";
			 public const string DtLancamento = "DT_LANCAMENTO";
			 public const string DtLiquidacao = "DT_LIQUIDACAO";
			 public const string CdAtividade = "CD_ATIVIDADE";
			 public const string CdHistorico = "CD_HISTORICO";
			 public const string DsLancamento = "DS_LANCAMENTO";
			 public const string VlLancamento = "VL_LANCAMENTO";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCliente = "CdCliente";
			 public const string DtLancamento = "DtLancamento";
			 public const string DtLiquidacao = "DtLiquidacao";
			 public const string CdAtividade = "CdAtividade";
			 public const string CdHistorico = "CdHistorico";
			 public const string DsLancamento = "DsLancamento";
			 public const string VlLancamento = "VlLancamento";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VTccmovtoMetadata))
			{
				if(VTccmovtoMetadata.mapDelegates == null)
				{
					VTccmovtoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VTccmovtoMetadata.meta == null)
				{
					VTccmovtoMetadata.meta = new VTccmovtoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_LANCAMENTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_LIQUIDACAO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_ATIVIDADE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_HISTORICO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DS_LANCAMENTO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VL_LANCAMENTO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "V_TCCMOVTO";
				meta.Destination = "V_TCCMOVTO";
				
				meta.spInsert = "proc_V_TCCMOVTOInsert";				
				meta.spUpdate = "proc_V_TCCMOVTOUpdate";		
				meta.spDelete = "proc_V_TCCMOVTODelete";
				meta.spLoadAll = "proc_V_TCCMOVTOLoadAll";
				meta.spLoadByPrimaryKey = "proc_V_TCCMOVTOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VTccmovtoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
