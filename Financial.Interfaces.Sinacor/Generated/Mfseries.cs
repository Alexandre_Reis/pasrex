/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 28/01/2010 16:39:24
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		









		
				
				








	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esMfseriesCollection : esEntityCollection
	{
		public esMfseriesCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "MfseriesCollection";
		}

		#region Query Logic
		protected void InitQuery(esMfseriesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esMfseriesQuery);
		}
		#endregion
		
		virtual public Mfseries DetachEntity(Mfseries entity)
		{
			return base.DetachEntity(entity) as Mfseries;
		}
		
		virtual public Mfseries AttachEntity(Mfseries entity)
		{
			return base.AttachEntity(entity) as Mfseries;
		}
		
		virtual public void Combine(MfseriesCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Mfseries this[int index]
		{
			get
			{
				return base[index] as Mfseries;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Mfseries);
		}
	}



	[Serializable]
	abstract public class esMfseries : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esMfseriesQuery GetDynamicQuery()
		{
			return null;
		}

		public esMfseries()
		{

		}

		public esMfseries(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esMfseriesQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esMfseriesQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Commod": this.str.Commod = (string)value; break;							
						case "Mercad": this.str.Mercad = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "Datvct": this.str.Datvct = (string)value; break;							
						case "Preexe": this.str.Preexe = (string)value; break;							
						case "Qtdpads": this.str.Qtdpads = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Datvct":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Datvct = (System.DateTime?)value;
							break;
						
						case "Preexe":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Preexe = (System.Decimal?)value;
							break;
						
						case "Qtdpads":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Qtdpads = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to MFSERIES.COMMOD
		/// </summary>
		virtual public System.String Commod
		{
			get
			{
				return base.GetSystemString(MfseriesMetadata.ColumnNames.Commod);
			}
			
			set
			{
				base.SetSystemString(MfseriesMetadata.ColumnNames.Commod, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.MERCAD
		/// </summary>
		virtual public System.String Mercad
		{
			get
			{
				return base.GetSystemString(MfseriesMetadata.ColumnNames.Mercad);
			}
			
			set
			{
				base.SetSystemString(MfseriesMetadata.ColumnNames.Mercad, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.SERIE
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(MfseriesMetadata.ColumnNames.Serie);
			}
			
			set
			{
				base.SetSystemString(MfseriesMetadata.ColumnNames.Serie, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.DATVCT
		/// </summary>
		virtual public System.DateTime? Datvct
		{
			get
			{
				return base.GetSystemDateTime(MfseriesMetadata.ColumnNames.Datvct);
			}
			
			set
			{
				base.SetSystemDateTime(MfseriesMetadata.ColumnNames.Datvct, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.PREEXE
		/// </summary>
		virtual public System.Decimal? Preexe
		{
			get
			{
				return base.GetSystemDecimal(MfseriesMetadata.ColumnNames.Preexe);
			}
			
			set
			{
				base.SetSystemDecimal(MfseriesMetadata.ColumnNames.Preexe, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.QTDPADS
		/// </summary>
		virtual public System.Decimal? Qtdpads
		{
			get
			{
				return base.GetSystemDecimal(MfseriesMetadata.ColumnNames.Qtdpads);
			}
			
			set
			{
				base.SetSystemDecimal(MfseriesMetadata.ColumnNames.Qtdpads, value);
			}
		}
		
		/// <summary>
		/// Maps to MFSERIES.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(MfseriesMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(MfseriesMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esMfseries entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Commod
			{
				get
				{
					System.String data = entity.Commod;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Commod = null;
					else entity.Commod = Convert.ToString(value);
				}
			}
				
			public System.String Mercad
			{
				get
				{
					System.String data = entity.Mercad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mercad = null;
					else entity.Mercad = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String Datvct
			{
				get
				{
					System.DateTime? data = entity.Datvct;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Datvct = null;
					else entity.Datvct = Convert.ToDateTime(value);
				}
			}
				
			public System.String Preexe
			{
				get
				{
					System.Decimal? data = entity.Preexe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Preexe = null;
					else entity.Preexe = Convert.ToDecimal(value);
				}
			}
				
			public System.String Qtdpads
			{
				get
				{
					System.Decimal? data = entity.Qtdpads;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Qtdpads = null;
					else entity.Qtdpads = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esMfseries entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esMfseriesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esMfseries can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Mfseries : esMfseries
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esMfseriesQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MfseriesMetadata.Meta();
			}
		}	
		

		public esQueryItem Commod
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Commod, esSystemType.String);
			}
		} 
		
		public esQueryItem Mercad
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Mercad, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem Datvct
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Datvct, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Preexe
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Preexe, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Qtdpads
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Qtdpads, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, MfseriesMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("MfseriesCollection")]
	public partial class MfseriesCollection : esMfseriesCollection, IEnumerable<Mfseries>
	{
		public MfseriesCollection()
		{

		}
		
		public static implicit operator List<Mfseries>(MfseriesCollection coll)
		{
			List<Mfseries> list = new List<Mfseries>();
			
			foreach (Mfseries emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  MfseriesMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MfseriesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Mfseries(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Mfseries();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public MfseriesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MfseriesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(MfseriesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Mfseries AddNew()
		{
			Mfseries entity = base.AddNewEntity() as Mfseries;
			
			return entity;
		}

		public Mfseries FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Mfseries;
		}


		#region IEnumerable<Mfseries> Members

		IEnumerator<Mfseries> IEnumerable<Mfseries>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Mfseries;
			}
		}

		#endregion
		
		private MfseriesQuery query;
	}


	/// <summary>
	/// Encapsulates the 'MFSERIES' table
	/// </summary>

	[Serializable]
	public partial class Mfseries : esMfseries
	{
		public Mfseries()
		{

		}
	
		public Mfseries(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MfseriesMetadata.Meta();
			}
		}
		
		
		
		override protected esMfseriesQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MfseriesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public MfseriesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MfseriesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(MfseriesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private MfseriesQuery query;
	}



	[Serializable]
	public partial class MfseriesQuery : esMfseriesQuery
	{
		public MfseriesQuery()
		{

		}		
		
		public MfseriesQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class MfseriesMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected MfseriesMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Commod, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = MfseriesMetadata.PropertyNames.Commod;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Mercad, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = MfseriesMetadata.PropertyNames.Mercad;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Serie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = MfseriesMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Datvct, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = MfseriesMetadata.PropertyNames.Datvct;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Preexe, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MfseriesMetadata.PropertyNames.Preexe;	
			c.NumericPrecision = 12;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Qtdpads, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MfseriesMetadata.PropertyNames.Qtdpads;	
			c.NumericPrecision = 15;
			c.NumericScale = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MfseriesMetadata.ColumnNames.Id, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MfseriesMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public MfseriesMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Commod = "COMMOD";
			 public const string Mercad = "MERCAD";
			 public const string Serie = "SERIE";
			 public const string Datvct = "DATVCT";
			 public const string Preexe = "PREEXE";
			 public const string Qtdpads = "QTDPADS";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Commod = "Commod";
			 public const string Mercad = "Mercad";
			 public const string Serie = "Serie";
			 public const string Datvct = "Datvct";
			 public const string Preexe = "Preexe";
			 public const string Qtdpads = "Qtdpads";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MfseriesMetadata))
			{
				if(MfseriesMetadata.mapDelegates == null)
				{
					MfseriesMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (MfseriesMetadata.meta == null)
				{
					MfseriesMetadata.meta = new MfseriesMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("COMMOD", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("MERCAD", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SERIE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATVCT", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("PREEXE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDPADS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "MFSERIES";
				meta.Destination = "MFSERIES";
				
				meta.spInsert = "proc_MFSERIESInsert";				
				meta.spUpdate = "proc_MFSERIESUpdate";		
				meta.spDelete = "proc_MFSERIESDelete";
				meta.spLoadAll = "proc_MFSERIESLoadAll";
				meta.spLoadByPrimaryKey = "proc_MFSERIESLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private MfseriesMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
