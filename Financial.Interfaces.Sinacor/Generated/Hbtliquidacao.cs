/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/05/2012 12:47:02
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	
















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esHbtliquidacaoCollection : esEntityCollection
	{
		public esHbtliquidacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "HbtliquidacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esHbtliquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esHbtliquidacaoQuery);
		}
		#endregion
		
		virtual public Hbtliquidacao DetachEntity(Hbtliquidacao entity)
		{
			return base.DetachEntity(entity) as Hbtliquidacao;
		}
		
		virtual public Hbtliquidacao AttachEntity(Hbtliquidacao entity)
		{
			return base.AttachEntity(entity) as Hbtliquidacao;
		}
		
		virtual public void Combine(HbtliquidacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Hbtliquidacao this[int index]
		{
			get
			{
				return base[index] as Hbtliquidacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Hbtliquidacao);
		}
	}



	[Serializable]
	abstract public class esHbtliquidacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esHbtliquidacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esHbtliquidacao()
		{

		}

		public esHbtliquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esHbtliquidacaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esHbtliquidacaoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DataMvto": this.str.DataMvto = (string)value; break;							
						case "DataRef": this.str.DataRef = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "NumCotr": this.str.NumCotr = (string)value; break;							
						case "DataLiqd": this.str.DataLiqd = (string)value; break;							
						case "QtdeAcoeLiqd": this.str.QtdeAcoeLiqd = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "PrecMed": this.str.PrecMed = (string)value; break;							
						case "ValBrut": this.str.ValBrut = (string)value; break;							
						case "ValBrutDoad": this.str.ValBrutDoad = (string)value; break;							
						case "ValComi": this.str.ValComi = (string)value; break;							
						case "ValEmolCblc": this.str.ValEmolCblc = (string)value; break;							
						case "ValIr": this.str.ValIr = (string)value; break;							
						case "ValLiq": this.str.ValLiq = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DataMvto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMvto = (System.DateTime?)value;
							break;
						
						case "DataRef":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRef = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "NumCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotr = (System.Decimal?)value;
							break;
						
						case "DataLiqd":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiqd = (System.DateTime?)value;
							break;
						
						case "QtdeAcoeLiqd":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoeLiqd = (System.Decimal?)value;
							break;
						
						case "PrecMed":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecMed = (System.Decimal?)value;
							break;
						
						case "ValBrut":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValBrut = (System.Decimal?)value;
							break;
						
						case "ValBrutDoad":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValBrutDoad = (System.Decimal?)value;
							break;
						
						case "ValComi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValComi = (System.Decimal?)value;
							break;
						
						case "ValEmolCblc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValEmolCblc = (System.Decimal?)value;
							break;
						
						case "ValIr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValIr = (System.Decimal?)value;
							break;
						
						case "ValLiq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValLiq = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.DATA_MVTO
		/// </summary>
		virtual public System.DateTime? DataMvto
		{
			get
			{
				return base.GetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataMvto);
			}
			
			set
			{
				base.SetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.DATA_REF
		/// </summary>
		virtual public System.DateTime? DataRef
		{
			get
			{
				return base.GetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataRef);
			}
			
			set
			{
				base.SetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataRef, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.NUM_COTR
		/// </summary>
		virtual public System.Decimal? NumCotr
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.NumCotr);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.NumCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.DATA_LIQD
		/// </summary>
		virtual public System.DateTime? DataLiqd
		{
			get
			{
				return base.GetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataLiqd);
			}
			
			set
			{
				base.SetSystemDateTime(HbtliquidacaoMetadata.ColumnNames.DataLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.QTDE_ACOE_LIQD
		/// </summary>
		virtual public System.Decimal? QtdeAcoeLiqd
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.QtdeAcoeLiqd);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.QtdeAcoeLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(HbtliquidacaoMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(HbtliquidacaoMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.PREC_MED
		/// </summary>
		virtual public System.Decimal? PrecMed
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.PrecMed);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.PrecMed, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_BRUT
		/// </summary>
		virtual public System.Decimal? ValBrut
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValBrut);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValBrut, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_BRUT_DOAD
		/// </summary>
		virtual public System.Decimal? ValBrutDoad
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValBrutDoad);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValBrutDoad, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_COMI
		/// </summary>
		virtual public System.Decimal? ValComi
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValComi);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValComi, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_EMOL_CBLC
		/// </summary>
		virtual public System.Decimal? ValEmolCblc
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValEmolCblc);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValEmolCblc, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_IR
		/// </summary>
		virtual public System.Decimal? ValIr
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValIr);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValIr, value);
			}
		}
		
		/// <summary>
		/// Maps to HBTLIQUIDACAO.VAL_LIQ
		/// </summary>
		virtual public System.Decimal? ValLiq
		{
			get
			{
				return base.GetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValLiq);
			}
			
			set
			{
				base.SetSystemDecimal(HbtliquidacaoMetadata.ColumnNames.ValLiq, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esHbtliquidacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataMvto
			{
				get
				{
					System.DateTime? data = entity.DataMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMvto = null;
					else entity.DataMvto = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataRef
			{
				get
				{
					System.DateTime? data = entity.DataRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRef = null;
					else entity.DataRef = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumCotr
			{
				get
				{
					System.Decimal? data = entity.NumCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotr = null;
					else entity.NumCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataLiqd
			{
				get
				{
					System.DateTime? data = entity.DataLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiqd = null;
					else entity.DataLiqd = Convert.ToDateTime(value);
				}
			}
				
			public System.String QtdeAcoeLiqd
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoeLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoeLiqd = null;
					else entity.QtdeAcoeLiqd = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String PrecMed
			{
				get
				{
					System.Decimal? data = entity.PrecMed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecMed = null;
					else entity.PrecMed = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValBrut
			{
				get
				{
					System.Decimal? data = entity.ValBrut;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValBrut = null;
					else entity.ValBrut = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValBrutDoad
			{
				get
				{
					System.Decimal? data = entity.ValBrutDoad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValBrutDoad = null;
					else entity.ValBrutDoad = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValComi
			{
				get
				{
					System.Decimal? data = entity.ValComi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValComi = null;
					else entity.ValComi = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValEmolCblc
			{
				get
				{
					System.Decimal? data = entity.ValEmolCblc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValEmolCblc = null;
					else entity.ValEmolCblc = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValIr
			{
				get
				{
					System.Decimal? data = entity.ValIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValIr = null;
					else entity.ValIr = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValLiq
			{
				get
				{
					System.Decimal? data = entity.ValLiq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValLiq = null;
					else entity.ValLiq = Convert.ToDecimal(value);
				}
			}
			

			private esHbtliquidacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esHbtliquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esHbtliquidacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Hbtliquidacao : esHbtliquidacao
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esHbtliquidacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return HbtliquidacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataMvto
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.DataMvto, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataRef
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.DataRef, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumCotr
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataLiqd
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.DataLiqd, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QtdeAcoeLiqd
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.QtdeAcoeLiqd, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem PrecMed
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.PrecMed, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValBrut
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValBrut, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValBrutDoad
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValBrutDoad, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValComi
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValComi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValEmolCblc
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValEmolCblc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValIr
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValIr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValLiq
		{
			get
			{
				return new esQueryItem(this, HbtliquidacaoMetadata.ColumnNames.ValLiq, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("HbtliquidacaoCollection")]
	public partial class HbtliquidacaoCollection : esHbtliquidacaoCollection, IEnumerable<Hbtliquidacao>
	{
		public HbtliquidacaoCollection()
		{

		}
		
		public static implicit operator List<Hbtliquidacao>(HbtliquidacaoCollection coll)
		{
			List<Hbtliquidacao> list = new List<Hbtliquidacao>();
			
			foreach (Hbtliquidacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  HbtliquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HbtliquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Hbtliquidacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Hbtliquidacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public HbtliquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HbtliquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(HbtliquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Hbtliquidacao AddNew()
		{
			Hbtliquidacao entity = base.AddNewEntity() as Hbtliquidacao;
			
			return entity;
		}

		public Hbtliquidacao FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Hbtliquidacao;
		}


		#region IEnumerable<Hbtliquidacao> Members

		IEnumerator<Hbtliquidacao> IEnumerable<Hbtliquidacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Hbtliquidacao;
			}
		}

		#endregion
		
		private HbtliquidacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'HBTLIQUIDACAO' table
	/// </summary>

	[Serializable]
	public partial class Hbtliquidacao : esHbtliquidacao
	{
		public Hbtliquidacao()
		{

		}
	
		public Hbtliquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HbtliquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esHbtliquidacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HbtliquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public HbtliquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HbtliquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(HbtliquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private HbtliquidacaoQuery query;
	}



	[Serializable]
	public partial class HbtliquidacaoQuery : esHbtliquidacaoQuery
	{
		public HbtliquidacaoQuery()
		{

		}		
		
		public HbtliquidacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class HbtliquidacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HbtliquidacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.DataMvto, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.DataMvto;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.DataRef, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.DataRef;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.CodCli, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.NumCotr, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.NumCotr;	
			c.NumericPrecision = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.DataLiqd, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.DataLiqd;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.QtdeAcoeLiqd, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.QtdeAcoeLiqd;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.CodNeg, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.PrecMed, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.PrecMed;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValBrut, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValBrut;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValBrutDoad, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValBrutDoad;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValComi, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValComi;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValEmolCblc, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValEmolCblc;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValIr, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValIr;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HbtliquidacaoMetadata.ColumnNames.ValLiq, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HbtliquidacaoMetadata.PropertyNames.ValLiq;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public HbtliquidacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DataMvto = "DATA_MVTO";
			 public const string DataRef = "DATA_REF";
			 public const string CodCli = "COD_CLI";
			 public const string NumCotr = "NUM_COTR";
			 public const string DataLiqd = "DATA_LIQD";
			 public const string QtdeAcoeLiqd = "QTDE_ACOE_LIQD";
			 public const string CodNeg = "COD_NEG";
			 public const string PrecMed = "PREC_MED";
			 public const string ValBrut = "VAL_BRUT";
			 public const string ValBrutDoad = "VAL_BRUT_DOAD";
			 public const string ValComi = "VAL_COMI";
			 public const string ValEmolCblc = "VAL_EMOL_CBLC";
			 public const string ValIr = "VAL_IR";
			 public const string ValLiq = "VAL_LIQ";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DataMvto = "DataMvto";
			 public const string DataRef = "DataRef";
			 public const string CodCli = "CodCli";
			 public const string NumCotr = "NumCotr";
			 public const string DataLiqd = "DataLiqd";
			 public const string QtdeAcoeLiqd = "QtdeAcoeLiqd";
			 public const string CodNeg = "CodNeg";
			 public const string PrecMed = "PrecMed";
			 public const string ValBrut = "ValBrut";
			 public const string ValBrutDoad = "ValBrutDoad";
			 public const string ValComi = "ValComi";
			 public const string ValEmolCblc = "ValEmolCblc";
			 public const string ValIr = "ValIr";
			 public const string ValLiq = "ValLiq";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HbtliquidacaoMetadata))
			{
				if(HbtliquidacaoMetadata.mapDelegates == null)
				{
					HbtliquidacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HbtliquidacaoMetadata.meta == null)
				{
					HbtliquidacaoMetadata.meta = new HbtliquidacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_MVTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_REF", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_LIQD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("QTDE_ACOE_LIQD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("PREC_MED", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_BRUT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_BRUT_DOAD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_COMI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_EMOL_CBLC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_IR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_LIQ", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "HBTLIQUIDACAO";
				meta.Destination = "HBTLIQUIDACAO";
				
				meta.spInsert = "proc_HBTLIQUIDACAOInsert";				
				meta.spUpdate = "proc_HBTLIQUIDACAOUpdate";		
				meta.spDelete = "proc_HBTLIQUIDACAODelete";
				meta.spLoadAll = "proc_HBTLIQUIDACAOLoadAll";
				meta.spLoadByPrimaryKey = "proc_HBTLIQUIDACAOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HbtliquidacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
