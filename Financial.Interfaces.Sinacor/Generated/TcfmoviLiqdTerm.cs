/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 24/09/2012 13:09:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTcfmoviLiqdTermCollection : esEntityCollection
	{
		public esTcfmoviLiqdTermCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TcfmoviLiqdTermCollection";
		}

		#region Query Logic
		protected void InitQuery(esTcfmoviLiqdTermQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTcfmoviLiqdTermQuery);
		}
		#endregion
		
		virtual public TcfmoviLiqdTerm DetachEntity(TcfmoviLiqdTerm entity)
		{
			return base.DetachEntity(entity) as TcfmoviLiqdTerm;
		}
		
		virtual public TcfmoviLiqdTerm AttachEntity(TcfmoviLiqdTerm entity)
		{
			return base.AttachEntity(entity) as TcfmoviLiqdTerm;
		}
		
		virtual public void Combine(TcfmoviLiqdTermCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TcfmoviLiqdTerm this[int index]
		{
			get
			{
				return base[index] as TcfmoviLiqdTerm;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TcfmoviLiqdTerm);
		}
	}



	[Serializable]
	abstract public class esTcfmoviLiqdTerm : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTcfmoviLiqdTermQuery GetDynamicQuery()
		{
			return null;
		}

		public esTcfmoviLiqdTerm()
		{

		}

		public esTcfmoviLiqdTerm(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTcfmoviLiqdTermQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTcfmoviLiqdTermQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DataMvto": this.str.DataMvto = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "CodIsin": this.str.CodIsin = (string)value; break;							
						case "NumDist": this.str.NumDist = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "CodLoc": this.str.CodLoc = (string)value; break;							
						case "CodCart": this.str.CodCart = (string)value; break;							
						case "DataPreg": this.str.DataPreg = (string)value; break;							
						case "NumNego": this.str.NumNego = (string)value; break;							
						case "ValNego": this.str.ValNego = (string)value; break;							
						case "CodUsuaConp": this.str.CodUsuaConp = (string)value; break;							
						case "DataVenc": this.str.DataVenc = (string)value; break;							
						case "QtdeLqdo": this.str.QtdeLqdo = (string)value; break;							
						case "TipoLiqd": this.str.TipoLiqd = (string)value; break;							
						case "ValLqdo": this.str.ValLqdo = (string)value; break;							
						case "DataLiqd": this.str.DataLiqd = (string)value; break;							
						case "NomeTmnl": this.str.NomeTmnl = (string)value; break;							
						case "NomeUsua": this.str.NomeUsua = (string)value; break;							
						case "DataSist": this.str.DataSist = (string)value; break;							
						case "CodIsinLqdo": this.str.CodIsinLqdo = (string)value; break;							
						case "NumDistLqdo": this.str.NumDistLqdo = (string)value; break;							
						case "IndIntrEftv": this.str.IndIntrEftv = (string)value; break;							
						case "NumCotr": this.str.NumCotr = (string)value; break;							
						case "DigCotr": this.str.DigCotr = (string)value; break;							
						case "IndIntrExec": this.str.IndIntrExec = (string)value; break;							
						case "NomeUsuaIntr": this.str.NomeUsuaIntr = (string)value; break;							
						case "NomeTmnlIntr": this.str.NomeTmnlIntr = (string)value; break;							
						case "DataSistIntr": this.str.DataSistIntr = (string)value; break;							
						case "IndLibe": this.str.IndLibe = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DataMvto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMvto = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "NumDist":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumDist = (System.Decimal?)value;
							break;
						
						case "CodLoc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodLoc = (System.Decimal?)value;
							break;
						
						case "CodCart":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCart = (System.Decimal?)value;
							break;
						
						case "DataPreg":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPreg = (System.DateTime?)value;
							break;
						
						case "NumNego":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumNego = (System.Decimal?)value;
							break;
						
						case "ValNego":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValNego = (System.Decimal?)value;
							break;
						
						case "CodUsuaConp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodUsuaConp = (System.Decimal?)value;
							break;
						
						case "DataVenc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVenc = (System.DateTime?)value;
							break;
						
						case "QtdeLqdo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeLqdo = (System.Decimal?)value;
							break;
						
						case "TipoLiqd":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoLiqd = (System.Decimal?)value;
							break;
						
						case "ValLqdo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValLqdo = (System.Decimal?)value;
							break;
						
						case "DataLiqd":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiqd = (System.DateTime?)value;
							break;
						
						case "DataSist":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataSist = (System.DateTime?)value;
							break;
						
						case "NumDistLqdo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumDistLqdo = (System.Decimal?)value;
							break;
						
						case "NumCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotr = (System.Decimal?)value;
							break;
						
						case "DigCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DigCotr = (System.Decimal?)value;
							break;
						
						case "DataSistIntr":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataSistIntr = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_MVTO
		/// </summary>
		virtual public System.DateTime? DataMvto
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataMvto);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_ISIN
		/// </summary>
		virtual public System.String CodIsin
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodIsin);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NUM_DIST
		/// </summary>
		virtual public System.Decimal? NumDist
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumDist);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumDist, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_LOC
		/// </summary>
		virtual public System.Decimal? CodLoc
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodLoc);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodLoc, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_CART
		/// </summary>
		virtual public System.Decimal? CodCart
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodCart);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodCart, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_PREG
		/// </summary>
		virtual public System.DateTime? DataPreg
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataPreg);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataPreg, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NUM_NEGO
		/// </summary>
		virtual public System.Decimal? NumNego
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumNego);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumNego, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.VAL_NEGO
		/// </summary>
		virtual public System.Decimal? ValNego
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.ValNego);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.ValNego, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_USUA_CONP
		/// </summary>
		virtual public System.Decimal? CodUsuaConp
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodUsuaConp);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.CodUsuaConp, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_VENC
		/// </summary>
		virtual public System.DateTime? DataVenc
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataVenc);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataVenc, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.QTDE_LQDO
		/// </summary>
		virtual public System.Decimal? QtdeLqdo
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.QtdeLqdo);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.QtdeLqdo, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.TIPO_LIQD
		/// </summary>
		virtual public System.Decimal? TipoLiqd
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.TipoLiqd);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.TipoLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.VAL_LQDO
		/// </summary>
		virtual public System.Decimal? ValLqdo
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.ValLqdo);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.ValLqdo, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_LIQD
		/// </summary>
		virtual public System.DateTime? DataLiqd
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataLiqd);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NOME_TMNL
		/// </summary>
		virtual public System.String NomeTmnl
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnl);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnl, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NOME_USUA
		/// </summary>
		virtual public System.String NomeUsua
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsua);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsua, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_SIST
		/// </summary>
		virtual public System.DateTime? DataSist
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataSist);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataSist, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.COD_ISIN_LQDO
		/// </summary>
		virtual public System.String CodIsinLqdo
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodIsinLqdo);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.CodIsinLqdo, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NUM_DIST_LQDO
		/// </summary>
		virtual public System.Decimal? NumDistLqdo
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumDistLqdo);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumDistLqdo, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.IND_INTR_EFTV
		/// </summary>
		virtual public System.String IndIntrEftv
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrEftv);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrEftv, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NUM_COTR
		/// </summary>
		virtual public System.Decimal? NumCotr
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumCotr);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.NumCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DIG_COTR
		/// </summary>
		virtual public System.Decimal? DigCotr
		{
			get
			{
				return base.GetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.DigCotr);
			}
			
			set
			{
				base.SetSystemDecimal(TcfmoviLiqdTermMetadata.ColumnNames.DigCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.IND_INTR_EXEC
		/// </summary>
		virtual public System.String IndIntrExec
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrExec);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrExec, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NOME_USUA_INTR
		/// </summary>
		virtual public System.String NomeUsuaIntr
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsuaIntr);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsuaIntr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.NOME_TMNL_INTR
		/// </summary>
		virtual public System.String NomeTmnlIntr
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnlIntr);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnlIntr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.DATA_SIST_INTR
		/// </summary>
		virtual public System.DateTime? DataSistIntr
		{
			get
			{
				return base.GetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataSistIntr);
			}
			
			set
			{
				base.SetSystemDateTime(TcfmoviLiqdTermMetadata.ColumnNames.DataSistIntr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFMOVI_LIQD_TERM.IND_LIBE
		/// </summary>
		virtual public System.String IndLibe
		{
			get
			{
				return base.GetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndLibe);
			}
			
			set
			{
				base.SetSystemString(TcfmoviLiqdTermMetadata.ColumnNames.IndLibe, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTcfmoviLiqdTerm entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataMvto
			{
				get
				{
					System.DateTime? data = entity.DataMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMvto = null;
					else entity.DataMvto = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodIsin
			{
				get
				{
					System.String data = entity.CodIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsin = null;
					else entity.CodIsin = Convert.ToString(value);
				}
			}
				
			public System.String NumDist
			{
				get
				{
					System.Decimal? data = entity.NumDist;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDist = null;
					else entity.NumDist = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String CodLoc
			{
				get
				{
					System.Decimal? data = entity.CodLoc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodLoc = null;
					else entity.CodLoc = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodCart
			{
				get
				{
					System.Decimal? data = entity.CodCart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCart = null;
					else entity.CodCart = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataPreg
			{
				get
				{
					System.DateTime? data = entity.DataPreg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPreg = null;
					else entity.DataPreg = Convert.ToDateTime(value);
				}
			}
				
			public System.String NumNego
			{
				get
				{
					System.Decimal? data = entity.NumNego;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumNego = null;
					else entity.NumNego = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValNego
			{
				get
				{
					System.Decimal? data = entity.ValNego;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValNego = null;
					else entity.ValNego = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodUsuaConp
			{
				get
				{
					System.Decimal? data = entity.CodUsuaConp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodUsuaConp = null;
					else entity.CodUsuaConp = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVenc
			{
				get
				{
					System.DateTime? data = entity.DataVenc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVenc = null;
					else entity.DataVenc = Convert.ToDateTime(value);
				}
			}
				
			public System.String QtdeLqdo
			{
				get
				{
					System.Decimal? data = entity.QtdeLqdo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeLqdo = null;
					else entity.QtdeLqdo = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoLiqd
			{
				get
				{
					System.Decimal? data = entity.TipoLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiqd = null;
					else entity.TipoLiqd = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValLqdo
			{
				get
				{
					System.Decimal? data = entity.ValLqdo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValLqdo = null;
					else entity.ValLqdo = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataLiqd
			{
				get
				{
					System.DateTime? data = entity.DataLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiqd = null;
					else entity.DataLiqd = Convert.ToDateTime(value);
				}
			}
				
			public System.String NomeTmnl
			{
				get
				{
					System.String data = entity.NomeTmnl;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeTmnl = null;
					else entity.NomeTmnl = Convert.ToString(value);
				}
			}
				
			public System.String NomeUsua
			{
				get
				{
					System.String data = entity.NomeUsua;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeUsua = null;
					else entity.NomeUsua = Convert.ToString(value);
				}
			}
				
			public System.String DataSist
			{
				get
				{
					System.DateTime? data = entity.DataSist;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataSist = null;
					else entity.DataSist = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodIsinLqdo
			{
				get
				{
					System.String data = entity.CodIsinLqdo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsinLqdo = null;
					else entity.CodIsinLqdo = Convert.ToString(value);
				}
			}
				
			public System.String NumDistLqdo
			{
				get
				{
					System.Decimal? data = entity.NumDistLqdo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDistLqdo = null;
					else entity.NumDistLqdo = Convert.ToDecimal(value);
				}
			}
				
			public System.String IndIntrEftv
			{
				get
				{
					System.String data = entity.IndIntrEftv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndIntrEftv = null;
					else entity.IndIntrEftv = Convert.ToString(value);
				}
			}
				
			public System.String NumCotr
			{
				get
				{
					System.Decimal? data = entity.NumCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotr = null;
					else entity.NumCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String DigCotr
			{
				get
				{
					System.Decimal? data = entity.DigCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigCotr = null;
					else entity.DigCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String IndIntrExec
			{
				get
				{
					System.String data = entity.IndIntrExec;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndIntrExec = null;
					else entity.IndIntrExec = Convert.ToString(value);
				}
			}
				
			public System.String NomeUsuaIntr
			{
				get
				{
					System.String data = entity.NomeUsuaIntr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeUsuaIntr = null;
					else entity.NomeUsuaIntr = Convert.ToString(value);
				}
			}
				
			public System.String NomeTmnlIntr
			{
				get
				{
					System.String data = entity.NomeTmnlIntr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeTmnlIntr = null;
					else entity.NomeTmnlIntr = Convert.ToString(value);
				}
			}
				
			public System.String DataSistIntr
			{
				get
				{
					System.DateTime? data = entity.DataSistIntr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataSistIntr = null;
					else entity.DataSistIntr = Convert.ToDateTime(value);
				}
			}
				
			public System.String IndLibe
			{
				get
				{
					System.String data = entity.IndLibe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndLibe = null;
					else entity.IndLibe = Convert.ToString(value);
				}
			}
			

			private esTcfmoviLiqdTerm entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTcfmoviLiqdTermQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTcfmoviLiqdTerm can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TcfmoviLiqdTerm : esTcfmoviLiqdTerm
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTcfmoviLiqdTermQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TcfmoviLiqdTermMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataMvto
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataMvto, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodIsin
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem NumDist
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NumDist, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem CodLoc
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodLoc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodCart
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodCart, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataPreg
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataPreg, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NumNego
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NumNego, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValNego
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.ValNego, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodUsuaConp
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodUsuaConp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVenc
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QtdeLqdo
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.QtdeLqdo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoLiqd
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.TipoLiqd, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValLqdo
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.ValLqdo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataLiqd
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataLiqd, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NomeTmnl
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnl, esSystemType.String);
			}
		} 
		
		public esQueryItem NomeUsua
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NomeUsua, esSystemType.String);
			}
		} 
		
		public esQueryItem DataSist
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataSist, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodIsinLqdo
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.CodIsinLqdo, esSystemType.String);
			}
		} 
		
		public esQueryItem NumDistLqdo
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NumDistLqdo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IndIntrEftv
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.IndIntrEftv, esSystemType.String);
			}
		} 
		
		public esQueryItem NumCotr
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DigCotr
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DigCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IndIntrExec
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.IndIntrExec, esSystemType.String);
			}
		} 
		
		public esQueryItem NomeUsuaIntr
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NomeUsuaIntr, esSystemType.String);
			}
		} 
		
		public esQueryItem NomeTmnlIntr
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnlIntr, esSystemType.String);
			}
		} 
		
		public esQueryItem DataSistIntr
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.DataSistIntr, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IndLibe
		{
			get
			{
				return new esQueryItem(this, TcfmoviLiqdTermMetadata.ColumnNames.IndLibe, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TcfmoviLiqdTermCollection")]
	public partial class TcfmoviLiqdTermCollection : esTcfmoviLiqdTermCollection, IEnumerable<TcfmoviLiqdTerm>
	{
		public TcfmoviLiqdTermCollection()
		{

		}
		
		public static implicit operator List<TcfmoviLiqdTerm>(TcfmoviLiqdTermCollection coll)
		{
			List<TcfmoviLiqdTerm> list = new List<TcfmoviLiqdTerm>();
			
			foreach (TcfmoviLiqdTerm emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TcfmoviLiqdTermMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfmoviLiqdTermQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TcfmoviLiqdTerm(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TcfmoviLiqdTerm();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TcfmoviLiqdTermQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfmoviLiqdTermQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TcfmoviLiqdTermQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TcfmoviLiqdTerm AddNew()
		{
			TcfmoviLiqdTerm entity = base.AddNewEntity() as TcfmoviLiqdTerm;
			
			return entity;
		}

		public TcfmoviLiqdTerm FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as TcfmoviLiqdTerm;
		}


		#region IEnumerable<TcfmoviLiqdTerm> Members

		IEnumerator<TcfmoviLiqdTerm> IEnumerable<TcfmoviLiqdTerm>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TcfmoviLiqdTerm;
			}
		}

		#endregion
		
		private TcfmoviLiqdTermQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TCFMOVI_LIQD_TERM' table
	/// </summary>

	[Serializable]
	public partial class TcfmoviLiqdTerm : esTcfmoviLiqdTerm
	{
		public TcfmoviLiqdTerm()
		{

		}
	
		public TcfmoviLiqdTerm(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TcfmoviLiqdTermMetadata.Meta();
			}
		}
		
		
		
		override protected esTcfmoviLiqdTermQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfmoviLiqdTermQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TcfmoviLiqdTermQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfmoviLiqdTermQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TcfmoviLiqdTermQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TcfmoviLiqdTermQuery query;
	}



	[Serializable]
	public partial class TcfmoviLiqdTermQuery : esTcfmoviLiqdTermQuery
	{
		public TcfmoviLiqdTermQuery()
		{

		}		
		
		public TcfmoviLiqdTermQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TcfmoviLiqdTermMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TcfmoviLiqdTermMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataMvto, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataMvto;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodCli, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodIsin, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NumDist, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NumDist;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodNeg, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodLoc, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodLoc;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodCart, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodCart;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataPreg, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataPreg;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NumNego, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NumNego;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.ValNego, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.ValNego;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodUsuaConp, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodUsuaConp;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataVenc, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataVenc;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.QtdeLqdo, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.QtdeLqdo;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.TipoLiqd, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.TipoLiqd;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.ValLqdo, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.ValLqdo;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataLiqd, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataLiqd;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnl, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NomeTmnl;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsua, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NomeUsua;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataSist, 19, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataSist;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.CodIsinLqdo, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.CodIsinLqdo;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NumDistLqdo, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NumDistLqdo;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrEftv, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.IndIntrEftv;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NumCotr, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NumCotr;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DigCotr, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DigCotr;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.IndIntrExec, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.IndIntrExec;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NomeUsuaIntr, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NomeUsuaIntr;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.NomeTmnlIntr, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.NomeTmnlIntr;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.DataSistIntr, 28, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.DataSistIntr;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfmoviLiqdTermMetadata.ColumnNames.IndLibe, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfmoviLiqdTermMetadata.PropertyNames.IndLibe;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TcfmoviLiqdTermMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DataMvto = "DATA_MVTO";
			 public const string CodCli = "COD_CLI";
			 public const string CodIsin = "COD_ISIN";
			 public const string NumDist = "NUM_DIST";
			 public const string CodNeg = "COD_NEG";
			 public const string CodLoc = "COD_LOC";
			 public const string CodCart = "COD_CART";
			 public const string DataPreg = "DATA_PREG";
			 public const string NumNego = "NUM_NEGO";
			 public const string ValNego = "VAL_NEGO";
			 public const string CodUsuaConp = "COD_USUA_CONP";
			 public const string DataVenc = "DATA_VENC";
			 public const string QtdeLqdo = "QTDE_LQDO";
			 public const string TipoLiqd = "TIPO_LIQD";
			 public const string ValLqdo = "VAL_LQDO";
			 public const string DataLiqd = "DATA_LIQD";
			 public const string NomeTmnl = "NOME_TMNL";
			 public const string NomeUsua = "NOME_USUA";
			 public const string DataSist = "DATA_SIST";
			 public const string CodIsinLqdo = "COD_ISIN_LQDO";
			 public const string NumDistLqdo = "NUM_DIST_LQDO";
			 public const string IndIntrEftv = "IND_INTR_EFTV";
			 public const string NumCotr = "NUM_COTR";
			 public const string DigCotr = "DIG_COTR";
			 public const string IndIntrExec = "IND_INTR_EXEC";
			 public const string NomeUsuaIntr = "NOME_USUA_INTR";
			 public const string NomeTmnlIntr = "NOME_TMNL_INTR";
			 public const string DataSistIntr = "DATA_SIST_INTR";
			 public const string IndLibe = "IND_LIBE";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DataMvto = "DataMvto";
			 public const string CodCli = "CodCli";
			 public const string CodIsin = "CodIsin";
			 public const string NumDist = "NumDist";
			 public const string CodNeg = "CodNeg";
			 public const string CodLoc = "CodLoc";
			 public const string CodCart = "CodCart";
			 public const string DataPreg = "DataPreg";
			 public const string NumNego = "NumNego";
			 public const string ValNego = "ValNego";
			 public const string CodUsuaConp = "CodUsuaConp";
			 public const string DataVenc = "DataVenc";
			 public const string QtdeLqdo = "QtdeLqdo";
			 public const string TipoLiqd = "TipoLiqd";
			 public const string ValLqdo = "ValLqdo";
			 public const string DataLiqd = "DataLiqd";
			 public const string NomeTmnl = "NomeTmnl";
			 public const string NomeUsua = "NomeUsua";
			 public const string DataSist = "DataSist";
			 public const string CodIsinLqdo = "CodIsinLqdo";
			 public const string NumDistLqdo = "NumDistLqdo";
			 public const string IndIntrEftv = "IndIntrEftv";
			 public const string NumCotr = "NumCotr";
			 public const string DigCotr = "DigCotr";
			 public const string IndIntrExec = "IndIntrExec";
			 public const string NomeUsuaIntr = "NomeUsuaIntr";
			 public const string NomeTmnlIntr = "NomeTmnlIntr";
			 public const string DataSistIntr = "DataSistIntr";
			 public const string IndLibe = "IndLibe";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TcfmoviLiqdTermMetadata))
			{
				if(TcfmoviLiqdTermMetadata.mapDelegates == null)
				{
					TcfmoviLiqdTermMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TcfmoviLiqdTermMetadata.meta == null)
				{
					TcfmoviLiqdTermMetadata.meta = new TcfmoviLiqdTermMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_MVTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_ISIN", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_DIST", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("COD_LOC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_CART", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_PREG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NUM_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_USUA_CONP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("QTDE_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_LIQD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_LIQD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NOME_TMNL", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NOME_USUA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATA_SIST", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_ISIN_LQDO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_DIST_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IND_INTR_EFTV", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DIG_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IND_INTR_EXEC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NOME_USUA_INTR", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NOME_TMNL_INTR", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATA_SIST_INTR", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("IND_LIBE", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "TCFMOVI_LIQD_TERM";
				meta.Destination = "TCFMOVI_LIQD_TERM";
				
				meta.spInsert = "proc_TCFMOVI_LIQD_TERMInsert";				
				meta.spUpdate = "proc_TCFMOVI_LIQD_TERMUpdate";		
				meta.spDelete = "proc_TCFMOVI_LIQD_TERMDelete";
				meta.spLoadAll = "proc_TCFMOVI_LIQD_TERMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TCFMOVI_LIQD_TERMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TcfmoviLiqdTermMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
