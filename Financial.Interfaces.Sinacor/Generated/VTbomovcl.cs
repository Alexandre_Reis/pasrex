/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 07/10/2013 13:22:55
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVTbomovclCollection : esEntityCollection
	{
		public esVTbomovclCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VTbomovclCollection";
		}

		#region Query Logic
		protected void InitQuery(esVTbomovclQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVTbomovclQuery);
		}
		#endregion
		
		virtual public VTbomovcl DetachEntity(VTbomovcl entity)
		{
			return base.DetachEntity(entity) as VTbomovcl;
		}
		
		virtual public VTbomovcl AttachEntity(VTbomovcl entity)
		{
			return base.AttachEntity(entity) as VTbomovcl;
		}
		
		virtual public void Combine(VTbomovclCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VTbomovcl this[int index]
		{
			get
			{
				return base[index] as VTbomovcl;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VTbomovcl);
		}
	}



	[Serializable]
	abstract public class esVTbomovcl : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVTbomovclQuery GetDynamicQuery()
		{
			return null;
		}

		public esVTbomovcl()
		{

		}

		public esVTbomovcl(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVTbomovclQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVTbomovclQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "CdNegocio": this.str.CdNegocio = (string)value; break;							
						case "DtNegocio": this.str.DtNegocio = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "InNegocio": this.str.InNegocio = (string)value; break;							
						case "NrNegocio": this.str.NrNegocio = (string)value; break;							
						case "QtQtdesp": this.str.QtQtdesp = (string)value; break;							
						case "VlCortot": this.str.VlCortot = (string)value; break;							
						case "VlEmolum": this.str.VlEmolum = (string)value; break;							
						case "VlNegocio": this.str.VlNegocio = (string)value; break;							
						case "VlTaxreg": this.str.VlTaxreg = (string)value; break;							
						case "VlTotneg": this.str.VlTotneg = (string)value; break;							
						case "VlIrretido": this.str.VlIrretido = (string)value; break;							
						case "PzProjCc": this.str.PzProjCc = (string)value; break;							
						case "FtValorizacao": this.str.FtValorizacao = (string)value; break;							
						case "VlIss": this.str.VlIss = (string)value; break;							
						case "VlBaseirdt": this.str.VlBaseirdt = (string)value; break;							
						case "VlEmolumBv": this.str.VlEmolumBv = (string)value; break;							
						case "VlEmolumCb": this.str.VlEmolumCb = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DtNegocio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNegocio = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "NrNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNegocio = (System.Decimal?)value;
							break;
						
						case "QtQtdesp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtQtdesp = (System.Decimal?)value;
							break;
						
						case "VlCortot":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCortot = (System.Decimal?)value;
							break;
						
						case "VlEmolum":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlEmolum = (System.Decimal?)value;
							break;
						
						case "VlNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlNegocio = (System.Decimal?)value;
							break;
						
						case "VlTaxreg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxreg = (System.Decimal?)value;
							break;
						
						case "VlTotneg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTotneg = (System.Decimal?)value;
							break;
						
						case "VlIrretido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlIrretido = (System.Decimal?)value;
							break;
						
						case "PzProjCc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.PzProjCc = (System.DateTime?)value;
							break;
						
						case "FtValorizacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FtValorizacao = (System.Decimal?)value;
							break;
						
						case "VlIss":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlIss = (System.Decimal?)value;
							break;
						
						case "VlBaseirdt":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlBaseirdt = (System.Decimal?)value;
							break;
						
						case "VlEmolumBv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlEmolumBv = (System.Decimal?)value;
							break;
						
						case "VlEmolumCb":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlEmolumCb = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to V_TBOMOVCL.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(VTbomovclMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(VTbomovclMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.CD_NEGOCIO
		/// </summary>
		virtual public System.String CdNegocio
		{
			get
			{
				return base.GetSystemString(VTbomovclMetadata.ColumnNames.CdNegocio);
			}
			
			set
			{
				base.SetSystemString(VTbomovclMetadata.ColumnNames.CdNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.DT_NEGOCIO
		/// </summary>
		virtual public System.DateTime? DtNegocio
		{
			get
			{
				return base.GetSystemDateTime(VTbomovclMetadata.ColumnNames.DtNegocio);
			}
			
			set
			{
				base.SetSystemDateTime(VTbomovclMetadata.ColumnNames.DtNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.IN_NEGOCIO
		/// </summary>
		virtual public System.String InNegocio
		{
			get
			{
				return base.GetSystemString(VTbomovclMetadata.ColumnNames.InNegocio);
			}
			
			set
			{
				base.SetSystemString(VTbomovclMetadata.ColumnNames.InNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.NR_NEGOCIO
		/// </summary>
		virtual public System.Decimal? NrNegocio
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.NrNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.NrNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.QT_QTDESP
		/// </summary>
		virtual public System.Decimal? QtQtdesp
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.QtQtdesp);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.QtQtdesp, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_CORTOT
		/// </summary>
		virtual public System.Decimal? VlCortot
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlCortot);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlCortot, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_EMOLUM
		/// </summary>
		virtual public System.Decimal? VlEmolum
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolum);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolum, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_NEGOCIO
		/// </summary>
		virtual public System.Decimal? VlNegocio
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_TAXREG
		/// </summary>
		virtual public System.Decimal? VlTaxreg
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlTaxreg);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlTaxreg, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_TOTNEG
		/// </summary>
		virtual public System.Decimal? VlTotneg
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlTotneg);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlTotneg, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_IRRETIDO
		/// </summary>
		virtual public System.Decimal? VlIrretido
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlIrretido);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlIrretido, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.PZ_PROJ_CC
		/// </summary>
		virtual public System.DateTime? PzProjCc
		{
			get
			{
				return base.GetSystemDateTime(VTbomovclMetadata.ColumnNames.PzProjCc);
			}
			
			set
			{
				base.SetSystemDateTime(VTbomovclMetadata.ColumnNames.PzProjCc, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.FT_VALORIZACAO
		/// </summary>
		virtual public System.Decimal? FtValorizacao
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.FtValorizacao);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.FtValorizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_ISS
		/// </summary>
		virtual public System.Decimal? VlIss
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlIss);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlIss, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_BASEIRDT
		/// </summary>
		virtual public System.Decimal? VlBaseirdt
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlBaseirdt);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlBaseirdt, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_EMOLUM_BV
		/// </summary>
		virtual public System.Decimal? VlEmolumBv
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolumBv);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolumBv, value);
			}
		}
		
		/// <summary>
		/// Maps to V_TBOMOVCL.VL_EMOLUM_CB
		/// </summary>
		virtual public System.Decimal? VlEmolumCb
		{
			get
			{
				return base.GetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolumCb);
			}
			
			set
			{
				base.SetSystemDecimal(VTbomovclMetadata.ColumnNames.VlEmolumCb, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVTbomovcl entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String CdNegocio
			{
				get
				{
					System.String data = entity.CdNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNegocio = null;
					else entity.CdNegocio = Convert.ToString(value);
				}
			}
				
			public System.String DtNegocio
			{
				get
				{
					System.DateTime? data = entity.DtNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNegocio = null;
					else entity.DtNegocio = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String InNegocio
			{
				get
				{
					System.String data = entity.InNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InNegocio = null;
					else entity.InNegocio = Convert.ToString(value);
				}
			}
				
			public System.String NrNegocio
			{
				get
				{
					System.Decimal? data = entity.NrNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNegocio = null;
					else entity.NrNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtQtdesp
			{
				get
				{
					System.Decimal? data = entity.QtQtdesp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtQtdesp = null;
					else entity.QtQtdesp = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCortot
			{
				get
				{
					System.Decimal? data = entity.VlCortot;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCortot = null;
					else entity.VlCortot = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlEmolum
			{
				get
				{
					System.Decimal? data = entity.VlEmolum;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlEmolum = null;
					else entity.VlEmolum = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlNegocio
			{
				get
				{
					System.Decimal? data = entity.VlNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlNegocio = null;
					else entity.VlNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxreg
			{
				get
				{
					System.Decimal? data = entity.VlTaxreg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxreg = null;
					else entity.VlTaxreg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTotneg
			{
				get
				{
					System.Decimal? data = entity.VlTotneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTotneg = null;
					else entity.VlTotneg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlIrretido
			{
				get
				{
					System.Decimal? data = entity.VlIrretido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlIrretido = null;
					else entity.VlIrretido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PzProjCc
			{
				get
				{
					System.DateTime? data = entity.PzProjCc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PzProjCc = null;
					else entity.PzProjCc = Convert.ToDateTime(value);
				}
			}
				
			public System.String FtValorizacao
			{
				get
				{
					System.Decimal? data = entity.FtValorizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FtValorizacao = null;
					else entity.FtValorizacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlIss
			{
				get
				{
					System.Decimal? data = entity.VlIss;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlIss = null;
					else entity.VlIss = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlBaseirdt
			{
				get
				{
					System.Decimal? data = entity.VlBaseirdt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlBaseirdt = null;
					else entity.VlBaseirdt = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlEmolumBv
			{
				get
				{
					System.Decimal? data = entity.VlEmolumBv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlEmolumBv = null;
					else entity.VlEmolumBv = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlEmolumCb
			{
				get
				{
					System.Decimal? data = entity.VlEmolumCb;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlEmolumCb = null;
					else entity.VlEmolumCb = Convert.ToDecimal(value);
				}
			}
			

			private esVTbomovcl entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVTbomovclQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVTbomovcl can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VTbomovcl : esVTbomovcl
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVTbomovclQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VTbomovclMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNegocio
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.CdNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem DtNegocio
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.DtNegocio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InNegocio
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.InNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem NrNegocio
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.NrNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtQtdesp
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.QtQtdesp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCortot
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlCortot, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlEmolum
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlEmolum, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlNegocio
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxreg
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlTaxreg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTotneg
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlTotneg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlIrretido
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlIrretido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PzProjCc
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.PzProjCc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FtValorizacao
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.FtValorizacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlIss
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlIss, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlBaseirdt
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlBaseirdt, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlEmolumBv
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlEmolumBv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlEmolumCb
		{
			get
			{
				return new esQueryItem(this, VTbomovclMetadata.ColumnNames.VlEmolumCb, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VTbomovclCollection")]
	public partial class VTbomovclCollection : esVTbomovclCollection, IEnumerable<VTbomovcl>
	{
		public VTbomovclCollection()
		{

		}
		
		public static implicit operator List<VTbomovcl>(VTbomovclCollection coll)
		{
			List<VTbomovcl> list = new List<VTbomovcl>();
			
			foreach (VTbomovcl emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VTbomovclMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTbomovclQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VTbomovcl(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VTbomovcl();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VTbomovclQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTbomovclQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VTbomovclQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VTbomovcl AddNew()
		{
			VTbomovcl entity = base.AddNewEntity() as VTbomovcl;
			
			return entity;
		}

		public VTbomovcl FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as VTbomovcl;
		}


		#region IEnumerable<VTbomovcl> Members

		IEnumerator<VTbomovcl> IEnumerable<VTbomovcl>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VTbomovcl;
			}
		}

		#endregion
		
		private VTbomovclQuery query;
	}


	/// <summary>
	/// Encapsulates the 'V_TBOMOVCL' table
	/// </summary>

	[Serializable]
	public partial class VTbomovcl : esVTbomovcl
	{
		public VTbomovcl()
		{

		}
	
		public VTbomovcl(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VTbomovclMetadata.Meta();
			}
		}
		
		
		
		override protected esVTbomovclQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VTbomovclQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VTbomovclQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VTbomovclQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VTbomovclQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VTbomovclQuery query;
	}



	[Serializable]
	public partial class VTbomovclQuery : esVTbomovclQuery
	{
		public VTbomovclQuery()
		{

		}		
		
		public VTbomovclQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VTbomovclMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VTbomovclMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.CdCliente, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.NumericScale = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.CdNatope, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = VTbomovclMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.CdNegocio, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = VTbomovclMetadata.PropertyNames.CdNegocio;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.DtNegocio, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTbomovclMetadata.PropertyNames.DtNegocio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.Id, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.InNegocio, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = VTbomovclMetadata.PropertyNames.InNegocio;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.NrNegocio, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.NrNegocio;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.QtQtdesp, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.QtQtdesp;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlCortot, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlCortot;	
			c.NumericPrecision = 11;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlEmolum, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlEmolum;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlNegocio, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlNegocio;	
			c.NumericPrecision = 13;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlTaxreg, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlTaxreg;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlTotneg, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlTotneg;	
			c.NumericPrecision = 11;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlIrretido, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlIrretido;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.PzProjCc, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VTbomovclMetadata.PropertyNames.PzProjCc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.FtValorizacao, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.FtValorizacao;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlIss, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlIss;	
			c.NumericPrecision = 10;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlBaseirdt, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlBaseirdt;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlEmolumBv, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlEmolumBv;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VTbomovclMetadata.ColumnNames.VlEmolumCb, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VTbomovclMetadata.PropertyNames.VlEmolumCb;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VTbomovclMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCliente = "CD_CLIENTE";
			 public const string CdNatope = "CD_NATOPE";
			 public const string CdNegocio = "CD_NEGOCIO";
			 public const string DtNegocio = "DT_NEGOCIO";
			 public const string Id = "ID";
			 public const string InNegocio = "IN_NEGOCIO";
			 public const string NrNegocio = "NR_NEGOCIO";
			 public const string QtQtdesp = "QT_QTDESP";
			 public const string VlCortot = "VL_CORTOT";
			 public const string VlEmolum = "VL_EMOLUM";
			 public const string VlNegocio = "VL_NEGOCIO";
			 public const string VlTaxreg = "VL_TAXREG";
			 public const string VlTotneg = "VL_TOTNEG";
			 public const string VlIrretido = "VL_IRRETIDO";
			 public const string PzProjCc = "PZ_PROJ_CC";
			 public const string FtValorizacao = "FT_VALORIZACAO";
			 public const string VlIss = "VL_ISS";
			 public const string VlBaseirdt = "VL_BASEIRDT";
			 public const string VlEmolumBv = "VL_EMOLUM_BV";
			 public const string VlEmolumCb = "VL_EMOLUM_CB";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCliente = "CdCliente";
			 public const string CdNatope = "CdNatope";
			 public const string CdNegocio = "CdNegocio";
			 public const string DtNegocio = "DtNegocio";
			 public const string Id = "Id";
			 public const string InNegocio = "InNegocio";
			 public const string NrNegocio = "NrNegocio";
			 public const string QtQtdesp = "QtQtdesp";
			 public const string VlCortot = "VlCortot";
			 public const string VlEmolum = "VlEmolum";
			 public const string VlNegocio = "VlNegocio";
			 public const string VlTaxreg = "VlTaxreg";
			 public const string VlTotneg = "VlTotneg";
			 public const string VlIrretido = "VlIrretido";
			 public const string PzProjCc = "PzProjCc";
			 public const string FtValorizacao = "FtValorizacao";
			 public const string VlIss = "VlIss";
			 public const string VlBaseirdt = "VlBaseirdt";
			 public const string VlEmolumBv = "VlEmolumBv";
			 public const string VlEmolumCb = "VlEmolumCb";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VTbomovclMetadata))
			{
				if(VTbomovclMetadata.mapDelegates == null)
				{
					VTbomovclMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VTbomovclMetadata.meta == null)
				{
					VTbomovclMetadata.meta = new VTbomovclMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_NEGOCIO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NR_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QT_QTDESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_CORTOT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_EMOLUM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXREG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TOTNEG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_IRRETIDO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PZ_PROJ_CC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("FT_VALORIZACAO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_ISS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_BASEIRDT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_EMOLUM_BV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_EMOLUM_CB", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "V_TBOMOVCL";
				meta.Destination = "V_TBOMOVCL";
				
				meta.spInsert = "proc_V_TBOMOVCLInsert";				
				meta.spUpdate = "proc_V_TBOMOVCLUpdate";		
				meta.spDelete = "proc_V_TBOMOVCLDelete";
				meta.spLoadAll = "proc_V_TBOMOVCLLoadAll";
				meta.spLoadByPrimaryKey = "proc_V_TBOMOVCLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VTbomovclMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
