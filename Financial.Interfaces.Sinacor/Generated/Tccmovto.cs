/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 28/01/2010 16:39:50
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		









		
				
				








	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTccmovtoCollection : esEntityCollection
	{
		public esTccmovtoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TccmovtoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTccmovtoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTccmovtoQuery);
		}
		#endregion
		
		virtual public Tccmovto DetachEntity(Tccmovto entity)
		{
			return base.DetachEntity(entity) as Tccmovto;
		}
		
		virtual public Tccmovto AttachEntity(Tccmovto entity)
		{
			return base.AttachEntity(entity) as Tccmovto;
		}
		
		virtual public void Combine(TccmovtoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tccmovto this[int index]
		{
			get
			{
				return base[index] as Tccmovto;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tccmovto);
		}
	}



	[Serializable]
	abstract public class esTccmovto : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTccmovtoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTccmovto()
		{

		}

		public esTccmovto(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTccmovtoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTccmovtoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "DtLancamento": this.str.DtLancamento = (string)value; break;							
						case "DtLiquidacao": this.str.DtLiquidacao = (string)value; break;							
						case "CdAtividade": this.str.CdAtividade = (string)value; break;							
						case "CdHistorico": this.str.CdHistorico = (string)value; break;							
						case "DsLancamento": this.str.DsLancamento = (string)value; break;							
						case "VlLancamento": this.str.VlLancamento = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DtLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtLancamento = (System.DateTime?)value;
							break;
						
						case "DtLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtLiquidacao = (System.DateTime?)value;
							break;
						
						case "CdHistorico":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdHistorico = (System.Decimal?)value;
							break;
						
						case "VlLancamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlLancamento = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TCCMOVTO.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TccmovtoMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TccmovtoMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.DT_LANCAMENTO
		/// </summary>
		virtual public System.DateTime? DtLancamento
		{
			get
			{
				return base.GetSystemDateTime(TccmovtoMetadata.ColumnNames.DtLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(TccmovtoMetadata.ColumnNames.DtLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.DT_LIQUIDACAO
		/// </summary>
		virtual public System.DateTime? DtLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(TccmovtoMetadata.ColumnNames.DtLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(TccmovtoMetadata.ColumnNames.DtLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.CD_ATIVIDADE
		/// </summary>
		virtual public System.String CdAtividade
		{
			get
			{
				return base.GetSystemString(TccmovtoMetadata.ColumnNames.CdAtividade);
			}
			
			set
			{
				base.SetSystemString(TccmovtoMetadata.ColumnNames.CdAtividade, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.CD_HISTORICO
		/// </summary>
		virtual public System.Decimal? CdHistorico
		{
			get
			{
				return base.GetSystemDecimal(TccmovtoMetadata.ColumnNames.CdHistorico);
			}
			
			set
			{
				base.SetSystemDecimal(TccmovtoMetadata.ColumnNames.CdHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.DS_LANCAMENTO
		/// </summary>
		virtual public System.String DsLancamento
		{
			get
			{
				return base.GetSystemString(TccmovtoMetadata.ColumnNames.DsLancamento);
			}
			
			set
			{
				base.SetSystemString(TccmovtoMetadata.ColumnNames.DsLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.VL_LANCAMENTO
		/// </summary>
		virtual public System.Decimal? VlLancamento
		{
			get
			{
				return base.GetSystemDecimal(TccmovtoMetadata.ColumnNames.VlLancamento);
			}
			
			set
			{
				base.SetSystemDecimal(TccmovtoMetadata.ColumnNames.VlLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCMOVTO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TccmovtoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TccmovtoMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTccmovto entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtLancamento
			{
				get
				{
					System.DateTime? data = entity.DtLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtLancamento = null;
					else entity.DtLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DtLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtLiquidacao = null;
					else entity.DtLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtividade
			{
				get
				{
					System.String data = entity.CdAtividade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtividade = null;
					else entity.CdAtividade = Convert.ToString(value);
				}
			}
				
			public System.String CdHistorico
			{
				get
				{
					System.Decimal? data = entity.CdHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdHistorico = null;
					else entity.CdHistorico = Convert.ToDecimal(value);
				}
			}
				
			public System.String DsLancamento
			{
				get
				{
					System.String data = entity.DsLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsLancamento = null;
					else entity.DsLancamento = Convert.ToString(value);
				}
			}
				
			public System.String VlLancamento
			{
				get
				{
					System.Decimal? data = entity.VlLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlLancamento = null;
					else entity.VlLancamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTccmovto entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTccmovtoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTccmovto can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tccmovto : esTccmovto
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTccmovtoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TccmovtoMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtLancamento
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.DtLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtLiquidacao
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.DtLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtividade
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.CdAtividade, esSystemType.String);
			}
		} 
		
		public esQueryItem CdHistorico
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.CdHistorico, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DsLancamento
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.DsLancamento, esSystemType.String);
			}
		} 
		
		public esQueryItem VlLancamento
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.VlLancamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TccmovtoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TccmovtoCollection")]
	public partial class TccmovtoCollection : esTccmovtoCollection, IEnumerable<Tccmovto>
	{
		public TccmovtoCollection()
		{

		}
		
		public static implicit operator List<Tccmovto>(TccmovtoCollection coll)
		{
			List<Tccmovto> list = new List<Tccmovto>();
			
			foreach (Tccmovto emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TccmovtoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TccmovtoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tccmovto(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tccmovto();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TccmovtoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TccmovtoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TccmovtoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tccmovto AddNew()
		{
			Tccmovto entity = base.AddNewEntity() as Tccmovto;
			
			return entity;
		}

		public Tccmovto FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tccmovto;
		}


		#region IEnumerable<Tccmovto> Members

		IEnumerator<Tccmovto> IEnumerable<Tccmovto>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tccmovto;
			}
		}

		#endregion
		
		private TccmovtoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TCCMOVTO' table
	/// </summary>

	[Serializable]
	public partial class Tccmovto : esTccmovto
	{
		public Tccmovto()
		{

		}
	
		public Tccmovto(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TccmovtoMetadata.Meta();
			}
		}
		
		
		
		override protected esTccmovtoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TccmovtoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TccmovtoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TccmovtoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TccmovtoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TccmovtoQuery query;
	}



	[Serializable]
	public partial class TccmovtoQuery : esTccmovtoQuery
	{
		public TccmovtoQuery()
		{

		}		
		
		public TccmovtoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TccmovtoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TccmovtoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.CdCliente, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccmovtoMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.DtLancamento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TccmovtoMetadata.PropertyNames.DtLancamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.DtLiquidacao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TccmovtoMetadata.PropertyNames.DtLiquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.CdAtividade, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TccmovtoMetadata.PropertyNames.CdAtividade;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.CdHistorico, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccmovtoMetadata.PropertyNames.CdHistorico;	
			c.NumericPrecision = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.DsLancamento, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TccmovtoMetadata.PropertyNames.DsLancamento;
			c.CharacterMaxLength = 80;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.VlLancamento, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccmovtoMetadata.PropertyNames.VlLancamento;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccmovtoMetadata.ColumnNames.Id, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccmovtoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TccmovtoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCliente = "CD_CLIENTE";
			 public const string DtLancamento = "DT_LANCAMENTO";
			 public const string DtLiquidacao = "DT_LIQUIDACAO";
			 public const string CdAtividade = "CD_ATIVIDADE";
			 public const string CdHistorico = "CD_HISTORICO";
			 public const string DsLancamento = "DS_LANCAMENTO";
			 public const string VlLancamento = "VL_LANCAMENTO";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCliente = "CdCliente";
			 public const string DtLancamento = "DtLancamento";
			 public const string DtLiquidacao = "DtLiquidacao";
			 public const string CdAtividade = "CdAtividade";
			 public const string CdHistorico = "CdHistorico";
			 public const string DsLancamento = "DsLancamento";
			 public const string VlLancamento = "VlLancamento";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TccmovtoMetadata))
			{
				if(TccmovtoMetadata.mapDelegates == null)
				{
					TccmovtoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TccmovtoMetadata.meta == null)
				{
					TccmovtoMetadata.meta = new TccmovtoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_LANCAMENTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_LIQUIDACAO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_ATIVIDADE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_HISTORICO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DS_LANCAMENTO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VL_LANCAMENTO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TCCMOVTO";
				meta.Destination = "TCCMOVTO";
				
				meta.spInsert = "proc_TCCMOVTOInsert";				
				meta.spUpdate = "proc_TCCMOVTOUpdate";		
				meta.spDelete = "proc_TCCMOVTODelete";
				meta.spLoadAll = "proc_TCCMOVTOLoadAll";
				meta.spLoadByPrimaryKey = "proc_TCCMOVTOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TccmovtoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
