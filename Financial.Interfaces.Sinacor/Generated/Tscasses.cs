/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/10/2013 12:42:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscassesCollection : esEntityCollection
	{
		public esTscassesCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscassesCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscassesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscassesQuery);
		}
		#endregion
		
		virtual public Tscasses DetachEntity(Tscasses entity)
		{
			return base.DetachEntity(entity) as Tscasses;
		}
		
		virtual public Tscasses AttachEntity(Tscasses entity)
		{
			return base.AttachEntity(entity) as Tscasses;
		}
		
		virtual public void Combine(TscassesCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscasses this[int index]
		{
			get
			{
				return base[index] as Tscasses;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscasses);
		}
	}



	[Serializable]
	abstract public class esTscasses : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscassesQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscasses()
		{

		}

		public esTscasses(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscassesQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscassesQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdAssessor": this.str.CdAssessor = (string)value; break;							
						case "NmAssessor": this.str.NmAssessor = (string)value; break;							
						case "NmResuAsses": this.str.NmResuAsses = (string)value; break;							
						case "PcAdiantamento": this.str.PcAdiantamento = (string)value; break;							
						case "InSituac": this.str.InSituac = (string)value; break;							
						case "CdEmpresa": this.str.CdEmpresa = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "TpOcorrencia": this.str.TpOcorrencia = (string)value; break;							
						case "CdMunicipio": this.str.CdMunicipio = (string)value; break;							
						case "NmEMail": this.str.NmEMail = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAssessor = (System.Decimal?)value;
							break;
						
						case "PcAdiantamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcAdiantamento = (System.Decimal?)value;
							break;
						
						case "CdEmpresa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEmpresa = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "CdMunicipio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdMunicipio = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCASSES.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.CD_ASSESSOR
		/// </summary>
		virtual public System.Decimal? CdAssessor
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.CdAssessor);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.CdAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.NM_ASSESSOR
		/// </summary>
		virtual public System.String NmAssessor
		{
			get
			{
				return base.GetSystemString(TscassesMetadata.ColumnNames.NmAssessor);
			}
			
			set
			{
				base.SetSystemString(TscassesMetadata.ColumnNames.NmAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.NM_RESU_ASSES
		/// </summary>
		virtual public System.String NmResuAsses
		{
			get
			{
				return base.GetSystemString(TscassesMetadata.ColumnNames.NmResuAsses);
			}
			
			set
			{
				base.SetSystemString(TscassesMetadata.ColumnNames.NmResuAsses, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.PC_ADIANTAMENTO
		/// </summary>
		virtual public System.Decimal? PcAdiantamento
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.PcAdiantamento);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.PcAdiantamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.IN_SITUAC
		/// </summary>
		virtual public System.String InSituac
		{
			get
			{
				return base.GetSystemString(TscassesMetadata.ColumnNames.InSituac);
			}
			
			set
			{
				base.SetSystemString(TscassesMetadata.ColumnNames.InSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.CD_EMPRESA
		/// </summary>
		virtual public System.Decimal? CdEmpresa
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.CdEmpresa);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.CdEmpresa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.TP_OCORRENCIA
		/// </summary>
		virtual public System.String TpOcorrencia
		{
			get
			{
				return base.GetSystemString(TscassesMetadata.ColumnNames.TpOcorrencia);
			}
			
			set
			{
				base.SetSystemString(TscassesMetadata.ColumnNames.TpOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.CD_MUNICIPIO
		/// </summary>
		virtual public System.Decimal? CdMunicipio
		{
			get
			{
				return base.GetSystemDecimal(TscassesMetadata.ColumnNames.CdMunicipio);
			}
			
			set
			{
				base.SetSystemDecimal(TscassesMetadata.ColumnNames.CdMunicipio, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCASSES.NM_E_MAIL
		/// </summary>
		virtual public System.String NmEMail
		{
			get
			{
				return base.GetSystemString(TscassesMetadata.ColumnNames.NmEMail);
			}
			
			set
			{
				base.SetSystemString(TscassesMetadata.ColumnNames.NmEMail, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscasses entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAssessor
			{
				get
				{
					System.Decimal? data = entity.CdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAssessor = null;
					else entity.CdAssessor = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmAssessor
			{
				get
				{
					System.String data = entity.NmAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmAssessor = null;
					else entity.NmAssessor = Convert.ToString(value);
				}
			}
				
			public System.String NmResuAsses
			{
				get
				{
					System.String data = entity.NmResuAsses;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmResuAsses = null;
					else entity.NmResuAsses = Convert.ToString(value);
				}
			}
				
			public System.String PcAdiantamento
			{
				get
				{
					System.Decimal? data = entity.PcAdiantamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcAdiantamento = null;
					else entity.PcAdiantamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String InSituac
			{
				get
				{
					System.String data = entity.InSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InSituac = null;
					else entity.InSituac = Convert.ToString(value);
				}
			}
				
			public System.String CdEmpresa
			{
				get
				{
					System.Decimal? data = entity.CdEmpresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEmpresa = null;
					else entity.CdEmpresa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpOcorrencia
			{
				get
				{
					System.String data = entity.TpOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOcorrencia = null;
					else entity.TpOcorrencia = Convert.ToString(value);
				}
			}
				
			public System.String CdMunicipio
			{
				get
				{
					System.Decimal? data = entity.CdMunicipio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdMunicipio = null;
					else entity.CdMunicipio = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmEMail
			{
				get
				{
					System.String data = entity.NmEMail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEMail = null;
					else entity.NmEMail = Convert.ToString(value);
				}
			}
			

			private esTscasses entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscassesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscasses can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscasses : esTscasses
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscassesQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscassesMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAssessor
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.CdAssessor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmAssessor
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.NmAssessor, esSystemType.String);
			}
		} 
		
		public esQueryItem NmResuAsses
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.NmResuAsses, esSystemType.String);
			}
		} 
		
		public esQueryItem PcAdiantamento
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.PcAdiantamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InSituac
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.InSituac, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEmpresa
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.CdEmpresa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpOcorrencia
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.TpOcorrencia, esSystemType.String);
			}
		} 
		
		public esQueryItem CdMunicipio
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.CdMunicipio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmEMail
		{
			get
			{
				return new esQueryItem(this, TscassesMetadata.ColumnNames.NmEMail, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscassesCollection")]
	public partial class TscassesCollection : esTscassesCollection, IEnumerable<Tscasses>
	{
		public TscassesCollection()
		{

		}
		
		public static implicit operator List<Tscasses>(TscassesCollection coll)
		{
			List<Tscasses> list = new List<Tscasses>();
			
			foreach (Tscasses emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscassesMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscassesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscasses(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscasses();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscassesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscassesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscassesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscasses AddNew()
		{
			Tscasses entity = base.AddNewEntity() as Tscasses;
			
			return entity;
		}

		public Tscasses FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscasses;
		}


		#region IEnumerable<Tscasses> Members

		IEnumerator<Tscasses> IEnumerable<Tscasses>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscasses;
			}
		}

		#endregion
		
		private TscassesQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCASSES' table
	/// </summary>

	[Serializable]
	public partial class Tscasses : esTscasses
	{
		public Tscasses()
		{

		}
	
		public Tscasses(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscassesMetadata.Meta();
			}
		}
		
		
		
		override protected esTscassesQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscassesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscassesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscassesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscassesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscassesQuery query;
	}



	[Serializable]
	public partial class TscassesQuery : esTscassesQuery
	{
		public TscassesQuery()
		{

		}		
		
		public TscassesQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscassesMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscassesMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscassesMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.CdAssessor, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.CdAssessor;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.NmAssessor, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TscassesMetadata.PropertyNames.NmAssessor;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.NmResuAsses, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TscassesMetadata.PropertyNames.NmResuAsses;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.PcAdiantamento, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.PcAdiantamento;	
			c.NumericPrecision = 6;
			c.NumericScale = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.InSituac, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TscassesMetadata.PropertyNames.InSituac;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.CdEmpresa, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.CdEmpresa;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.CdUsuario, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.TpOcorrencia, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TscassesMetadata.PropertyNames.TpOcorrencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.CdMunicipio, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscassesMetadata.PropertyNames.CdMunicipio;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscassesMetadata.ColumnNames.NmEMail, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TscassesMetadata.PropertyNames.NmEMail;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscassesMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdAssessor = "CD_ASSESSOR";
			 public const string NmAssessor = "NM_ASSESSOR";
			 public const string NmResuAsses = "NM_RESU_ASSES";
			 public const string PcAdiantamento = "PC_ADIANTAMENTO";
			 public const string InSituac = "IN_SITUAC";
			 public const string CdEmpresa = "CD_EMPRESA";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string TpOcorrencia = "TP_OCORRENCIA";
			 public const string CdMunicipio = "CD_MUNICIPIO";
			 public const string NmEMail = "NM_E_MAIL";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdAssessor = "CdAssessor";
			 public const string NmAssessor = "NmAssessor";
			 public const string NmResuAsses = "NmResuAsses";
			 public const string PcAdiantamento = "PcAdiantamento";
			 public const string InSituac = "InSituac";
			 public const string CdEmpresa = "CdEmpresa";
			 public const string CdUsuario = "CdUsuario";
			 public const string TpOcorrencia = "TpOcorrencia";
			 public const string CdMunicipio = "CdMunicipio";
			 public const string NmEMail = "NmEMail";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscassesMetadata))
			{
				if(TscassesMetadata.mapDelegates == null)
				{
					TscassesMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscassesMetadata.meta == null)
				{
					TscassesMetadata.meta = new TscassesMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_ASSESSOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_ASSESSOR", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_RESU_ASSES", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("PC_ADIANTAMENTO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_SITUAC", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_EMPRESA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_OCORRENCIA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_MUNICIPIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_E_MAIL", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "TSCASSES";
				meta.Destination = "TSCASSES";
				
				meta.spInsert = "proc_TSCASSESInsert";				
				meta.spUpdate = "proc_TSCASSESUpdate";		
				meta.spDelete = "proc_TSCASSESDelete";
				meta.spLoadAll = "proc_TSCASSESLoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCASSESLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscassesMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
