/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 11/12/2013 17:00:32
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscclicompCollection : esEntityCollection
	{
		public esTscclicompCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscclicompCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscclicompQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscclicompQuery);
		}
		#endregion
		
		virtual public Tscclicomp DetachEntity(Tscclicomp entity)
		{
			return base.DetachEntity(entity) as Tscclicomp;
		}
		
		virtual public Tscclicomp AttachEntity(Tscclicomp entity)
		{
			return base.AttachEntity(entity) as Tscclicomp;
		}
		
		virtual public void Combine(TscclicompCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscclicomp this[int index]
		{
			get
			{
				return base[index] as Tscclicomp;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscclicomp);
		}
	}



	[Serializable]
	abstract public class esTscclicomp : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscclicompQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscclicomp()
		{

		}

		public esTscclicomp(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscclicompQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscclicompQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;							
						case "DtNascFund": this.str.DtNascFund = (string)value; break;							
						case "CdConDep": this.str.CdConDep = (string)value; break;							
						case "CdDocIdent": this.str.CdDocIdent = (string)value; break;							
						case "CdTipoDoc": this.str.CdTipoDoc = (string)value; break;							
						case "CdOrgEmit": this.str.CdOrgEmit = (string)value; break;							
						case "SgPaisEmis": this.str.SgPaisEmis = (string)value; break;							
						case "SgEstadoEmis": this.str.SgEstadoEmis = (string)value; break;							
						case "CdAtiv": this.str.CdAtiv = (string)value; break;							
						case "CdNacion": this.str.CdNacion = (string)value; break;							
						case "CdCapac": this.str.CdCapac = (string)value; break;							
						case "IdSexo": this.str.IdSexo = (string)value; break;							
						case "CdEstCivil": this.str.CdEstCivil = (string)value; break;							
						case "NmConjuge": this.str.NmConjuge = (string)value; break;							
						case "NmPai": this.str.NmPai = (string)value; break;							
						case "NmMae": this.str.NmMae = (string)value; break;							
						case "CdTipoFili": this.str.CdTipoFili = (string)value; break;							
						case "SgPais": this.str.SgPais = (string)value; break;							
						case "InRecDivi": this.str.InRecDivi = (string)value; break;							
						case "InDistVal": this.str.InDistVal = (string)value; break;							
						case "CdCosif": this.str.CdCosif = (string)value; break;							
						case "DtDocIdent": this.str.DtDocIdent = (string)value; break;							
						case "NmLocNasc": this.str.NmLocNasc = (string)value; break;							
						case "NmEmpresa": this.str.NmEmpresa = (string)value; break;							
						case "CdEmpresa": this.str.CdEmpresa = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "TpOcorrencia": this.str.TpOcorrencia = (string)value; break;							
						case "CdNire": this.str.CdNire = (string)value; break;							
						case "TpRegcas": this.str.TpRegcas = (string)value; break;							
						case "DsCargo": this.str.DsCargo = (string)value; break;							
						case "SgEstadoNasc": this.str.SgEstadoNasc = (string)value; break;							
						case "CdCosifCi": this.str.CdCosifCi = (string)value; break;							
						case "CdEscolaridade": this.str.CdEscolaridade = (string)value; break;							
						case "CdCpfConjuge": this.str.CdCpfConjuge = (string)value; break;							
						case "DtNascConjuge": this.str.DtNascConjuge = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdCpfcgc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfcgc = (System.Decimal?)value;
							break;
						
						case "DtNascFund":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNascFund = (System.DateTime?)value;
							break;
						
						case "CdConDep":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdConDep = (System.Decimal?)value;
							break;
						
						case "CdAtiv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAtiv = (System.Decimal?)value;
							break;
						
						case "CdNacion":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdNacion = (System.Decimal?)value;
							break;
						
						case "CdCapac":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCapac = (System.Decimal?)value;
							break;
						
						case "CdEstCivil":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEstCivil = (System.Decimal?)value;
							break;
						
						case "CdTipoFili":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdTipoFili = (System.Decimal?)value;
							break;
						
						case "CdCosif":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCosif = (System.Decimal?)value;
							break;
						
						case "DtDocIdent":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDocIdent = (System.DateTime?)value;
							break;
						
						case "CdEmpresa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEmpresa = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "CdNire":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdNire = (System.Decimal?)value;
							break;
						
						case "TpRegcas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpRegcas = (System.Decimal?)value;
							break;
						
						case "CdCosifCi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCosifCi = (System.Decimal?)value;
							break;
						
						case "CdEscolaridade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEscolaridade = (System.Decimal?)value;
							break;
						
						case "CdCpfConjuge":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfConjuge = (System.Decimal?)value;
							break;
						
						case "DtNascConjuge":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNascConjuge = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCCLICOMP.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_CPFCGC
		/// </summary>
		virtual public System.Decimal? CdCpfcgc
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdCpfcgc);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdCpfcgc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.DT_NASC_FUND
		/// </summary>
		virtual public System.DateTime? DtNascFund
		{
			get
			{
				return base.GetSystemDateTime(TscclicompMetadata.ColumnNames.DtNascFund);
			}
			
			set
			{
				base.SetSystemDateTime(TscclicompMetadata.ColumnNames.DtNascFund, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_CON_DEP
		/// </summary>
		virtual public System.Decimal? CdConDep
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdConDep);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdConDep, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_DOC_IDENT
		/// </summary>
		virtual public System.String CdDocIdent
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.CdDocIdent);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.CdDocIdent, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_TIPO_DOC
		/// </summary>
		virtual public System.String CdTipoDoc
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.CdTipoDoc);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.CdTipoDoc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_ORG_EMIT
		/// </summary>
		virtual public System.String CdOrgEmit
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.CdOrgEmit);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.CdOrgEmit, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.SG_PAIS_EMIS
		/// </summary>
		virtual public System.String SgPaisEmis
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.SgPaisEmis);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.SgPaisEmis, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.SG_ESTADO_EMIS
		/// </summary>
		virtual public System.String SgEstadoEmis
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.SgEstadoEmis);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.SgEstadoEmis, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_ATIV
		/// </summary>
		virtual public System.Decimal? CdAtiv
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdAtiv);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdAtiv, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_NACION
		/// </summary>
		virtual public System.Decimal? CdNacion
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdNacion);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdNacion, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_CAPAC
		/// </summary>
		virtual public System.Decimal? CdCapac
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdCapac);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdCapac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.ID_SEXO
		/// </summary>
		virtual public System.String IdSexo
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.IdSexo);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.IdSexo, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_EST_CIVIL
		/// </summary>
		virtual public System.Decimal? CdEstCivil
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdEstCivil);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdEstCivil, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.NM_CONJUGE
		/// </summary>
		virtual public System.String NmConjuge
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.NmConjuge);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.NmConjuge, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.NM_PAI
		/// </summary>
		virtual public System.String NmPai
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.NmPai);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.NmPai, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.NM_MAE
		/// </summary>
		virtual public System.String NmMae
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.NmMae);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.NmMae, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_TIPO_FILI
		/// </summary>
		virtual public System.Decimal? CdTipoFili
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdTipoFili);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdTipoFili, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.SG_PAIS
		/// </summary>
		virtual public System.String SgPais
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.SgPais);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.SgPais, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.IN_REC_DIVI
		/// </summary>
		virtual public System.String InRecDivi
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.InRecDivi);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.InRecDivi, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.IN_DIST_VAL
		/// </summary>
		virtual public System.String InDistVal
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.InDistVal);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.InDistVal, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_COSIF
		/// </summary>
		virtual public System.Decimal? CdCosif
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdCosif);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdCosif, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.DT_DOC_IDENT
		/// </summary>
		virtual public System.DateTime? DtDocIdent
		{
			get
			{
				return base.GetSystemDateTime(TscclicompMetadata.ColumnNames.DtDocIdent);
			}
			
			set
			{
				base.SetSystemDateTime(TscclicompMetadata.ColumnNames.DtDocIdent, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.NM_LOC_NASC
		/// </summary>
		virtual public System.String NmLocNasc
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.NmLocNasc);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.NmLocNasc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.NM_EMPRESA
		/// </summary>
		virtual public System.String NmEmpresa
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.NmEmpresa);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.NmEmpresa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_EMPRESA
		/// </summary>
		virtual public System.Decimal? CdEmpresa
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdEmpresa);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdEmpresa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.TP_OCORRENCIA
		/// </summary>
		virtual public System.String TpOcorrencia
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.TpOcorrencia);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.TpOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_NIRE
		/// </summary>
		virtual public System.Decimal? CdNire
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdNire);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdNire, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.TP_REGCAS
		/// </summary>
		virtual public System.Decimal? TpRegcas
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.TpRegcas);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.TpRegcas, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.DS_CARGO
		/// </summary>
		virtual public System.String DsCargo
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.DsCargo);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.DsCargo, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.SG_ESTADO_NASC
		/// </summary>
		virtual public System.String SgEstadoNasc
		{
			get
			{
				return base.GetSystemString(TscclicompMetadata.ColumnNames.SgEstadoNasc);
			}
			
			set
			{
				base.SetSystemString(TscclicompMetadata.ColumnNames.SgEstadoNasc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_COSIF_CI
		/// </summary>
		virtual public System.Decimal? CdCosifCi
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdCosifCi);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdCosifCi, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_ESCOLARIDADE
		/// </summary>
		virtual public System.Decimal? CdEscolaridade
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdEscolaridade);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdEscolaridade, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.CD_CPF_CONJUGE
		/// </summary>
		virtual public System.Decimal? CdCpfConjuge
		{
			get
			{
				return base.GetSystemDecimal(TscclicompMetadata.ColumnNames.CdCpfConjuge);
			}
			
			set
			{
				base.SetSystemDecimal(TscclicompMetadata.ColumnNames.CdCpfConjuge, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLICOMP.DT_NASC_CONJUGE
		/// </summary>
		virtual public System.DateTime? DtNascConjuge
		{
			get
			{
				return base.GetSystemDateTime(TscclicompMetadata.ColumnNames.DtNascConjuge);
			}
			
			set
			{
				base.SetSystemDateTime(TscclicompMetadata.ColumnNames.DtNascConjuge, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscclicomp entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCpfcgc
			{
				get
				{
					System.Decimal? data = entity.CdCpfcgc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfcgc = null;
					else entity.CdCpfcgc = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtNascFund
			{
				get
				{
					System.DateTime? data = entity.DtNascFund;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNascFund = null;
					else entity.DtNascFund = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdConDep
			{
				get
				{
					System.Decimal? data = entity.CdConDep;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdConDep = null;
					else entity.CdConDep = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdDocIdent
			{
				get
				{
					System.String data = entity.CdDocIdent;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDocIdent = null;
					else entity.CdDocIdent = Convert.ToString(value);
				}
			}
				
			public System.String CdTipoDoc
			{
				get
				{
					System.String data = entity.CdTipoDoc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdTipoDoc = null;
					else entity.CdTipoDoc = Convert.ToString(value);
				}
			}
				
			public System.String CdOrgEmit
			{
				get
				{
					System.String data = entity.CdOrgEmit;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOrgEmit = null;
					else entity.CdOrgEmit = Convert.ToString(value);
				}
			}
				
			public System.String SgPaisEmis
			{
				get
				{
					System.String data = entity.SgPaisEmis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SgPaisEmis = null;
					else entity.SgPaisEmis = Convert.ToString(value);
				}
			}
				
			public System.String SgEstadoEmis
			{
				get
				{
					System.String data = entity.SgEstadoEmis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SgEstadoEmis = null;
					else entity.SgEstadoEmis = Convert.ToString(value);
				}
			}
				
			public System.String CdAtiv
			{
				get
				{
					System.Decimal? data = entity.CdAtiv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtiv = null;
					else entity.CdAtiv = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdNacion
			{
				get
				{
					System.Decimal? data = entity.CdNacion;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNacion = null;
					else entity.CdNacion = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCapac
			{
				get
				{
					System.Decimal? data = entity.CdCapac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCapac = null;
					else entity.CdCapac = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdSexo
			{
				get
				{
					System.String data = entity.IdSexo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSexo = null;
					else entity.IdSexo = Convert.ToString(value);
				}
			}
				
			public System.String CdEstCivil
			{
				get
				{
					System.Decimal? data = entity.CdEstCivil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEstCivil = null;
					else entity.CdEstCivil = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmConjuge
			{
				get
				{
					System.String data = entity.NmConjuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmConjuge = null;
					else entity.NmConjuge = Convert.ToString(value);
				}
			}
				
			public System.String NmPai
			{
				get
				{
					System.String data = entity.NmPai;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmPai = null;
					else entity.NmPai = Convert.ToString(value);
				}
			}
				
			public System.String NmMae
			{
				get
				{
					System.String data = entity.NmMae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmMae = null;
					else entity.NmMae = Convert.ToString(value);
				}
			}
				
			public System.String CdTipoFili
			{
				get
				{
					System.Decimal? data = entity.CdTipoFili;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdTipoFili = null;
					else entity.CdTipoFili = Convert.ToDecimal(value);
				}
			}
				
			public System.String SgPais
			{
				get
				{
					System.String data = entity.SgPais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SgPais = null;
					else entity.SgPais = Convert.ToString(value);
				}
			}
				
			public System.String InRecDivi
			{
				get
				{
					System.String data = entity.InRecDivi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InRecDivi = null;
					else entity.InRecDivi = Convert.ToString(value);
				}
			}
				
			public System.String InDistVal
			{
				get
				{
					System.String data = entity.InDistVal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InDistVal = null;
					else entity.InDistVal = Convert.ToString(value);
				}
			}
				
			public System.String CdCosif
			{
				get
				{
					System.Decimal? data = entity.CdCosif;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCosif = null;
					else entity.CdCosif = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtDocIdent
			{
				get
				{
					System.DateTime? data = entity.DtDocIdent;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDocIdent = null;
					else entity.DtDocIdent = Convert.ToDateTime(value);
				}
			}
				
			public System.String NmLocNasc
			{
				get
				{
					System.String data = entity.NmLocNasc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmLocNasc = null;
					else entity.NmLocNasc = Convert.ToString(value);
				}
			}
				
			public System.String NmEmpresa
			{
				get
				{
					System.String data = entity.NmEmpresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEmpresa = null;
					else entity.NmEmpresa = Convert.ToString(value);
				}
			}
				
			public System.String CdEmpresa
			{
				get
				{
					System.Decimal? data = entity.CdEmpresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEmpresa = null;
					else entity.CdEmpresa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpOcorrencia
			{
				get
				{
					System.String data = entity.TpOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOcorrencia = null;
					else entity.TpOcorrencia = Convert.ToString(value);
				}
			}
				
			public System.String CdNire
			{
				get
				{
					System.Decimal? data = entity.CdNire;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNire = null;
					else entity.CdNire = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpRegcas
			{
				get
				{
					System.Decimal? data = entity.TpRegcas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpRegcas = null;
					else entity.TpRegcas = Convert.ToDecimal(value);
				}
			}
				
			public System.String DsCargo
			{
				get
				{
					System.String data = entity.DsCargo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsCargo = null;
					else entity.DsCargo = Convert.ToString(value);
				}
			}
				
			public System.String SgEstadoNasc
			{
				get
				{
					System.String data = entity.SgEstadoNasc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SgEstadoNasc = null;
					else entity.SgEstadoNasc = Convert.ToString(value);
				}
			}
				
			public System.String CdCosifCi
			{
				get
				{
					System.Decimal? data = entity.CdCosifCi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCosifCi = null;
					else entity.CdCosifCi = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdEscolaridade
			{
				get
				{
					System.Decimal? data = entity.CdEscolaridade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEscolaridade = null;
					else entity.CdEscolaridade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCpfConjuge
			{
				get
				{
					System.Decimal? data = entity.CdCpfConjuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfConjuge = null;
					else entity.CdCpfConjuge = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtNascConjuge
			{
				get
				{
					System.DateTime? data = entity.DtNascConjuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNascConjuge = null;
					else entity.DtNascConjuge = Convert.ToDateTime(value);
				}
			}
			

			private esTscclicomp entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscclicompQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscclicomp can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscclicomp : esTscclicomp
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscclicompQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscclicompMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCpfcgc
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtNascFund
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.DtNascFund, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdConDep
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdConDep, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdDocIdent
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdDocIdent, esSystemType.String);
			}
		} 
		
		public esQueryItem CdTipoDoc
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdTipoDoc, esSystemType.String);
			}
		} 
		
		public esQueryItem CdOrgEmit
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdOrgEmit, esSystemType.String);
			}
		} 
		
		public esQueryItem SgPaisEmis
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.SgPaisEmis, esSystemType.String);
			}
		} 
		
		public esQueryItem SgEstadoEmis
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.SgEstadoEmis, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtiv
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdAtiv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdNacion
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdNacion, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCapac
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdCapac, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdSexo
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.IdSexo, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEstCivil
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdEstCivil, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmConjuge
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.NmConjuge, esSystemType.String);
			}
		} 
		
		public esQueryItem NmPai
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.NmPai, esSystemType.String);
			}
		} 
		
		public esQueryItem NmMae
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.NmMae, esSystemType.String);
			}
		} 
		
		public esQueryItem CdTipoFili
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdTipoFili, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem SgPais
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.SgPais, esSystemType.String);
			}
		} 
		
		public esQueryItem InRecDivi
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.InRecDivi, esSystemType.String);
			}
		} 
		
		public esQueryItem InDistVal
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.InDistVal, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCosif
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdCosif, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtDocIdent
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.DtDocIdent, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NmLocNasc
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.NmLocNasc, esSystemType.String);
			}
		} 
		
		public esQueryItem NmEmpresa
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.NmEmpresa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEmpresa
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdEmpresa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpOcorrencia
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.TpOcorrencia, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNire
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdNire, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpRegcas
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.TpRegcas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DsCargo
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.DsCargo, esSystemType.String);
			}
		} 
		
		public esQueryItem SgEstadoNasc
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.SgEstadoNasc, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCosifCi
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdCosifCi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdEscolaridade
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdEscolaridade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCpfConjuge
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.CdCpfConjuge, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtNascConjuge
		{
			get
			{
				return new esQueryItem(this, TscclicompMetadata.ColumnNames.DtNascConjuge, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscclicompCollection")]
	public partial class TscclicompCollection : esTscclicompCollection, IEnumerable<Tscclicomp>
	{
		public TscclicompCollection()
		{

		}
		
		public static implicit operator List<Tscclicomp>(TscclicompCollection coll)
		{
			List<Tscclicomp> list = new List<Tscclicomp>();
			
			foreach (Tscclicomp emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscclicompMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclicompQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscclicomp(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscclicomp();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscclicompQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclicompQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscclicompQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscclicomp AddNew()
		{
			Tscclicomp entity = base.AddNewEntity() as Tscclicomp;
			
			return entity;
		}

		public Tscclicomp FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscclicomp;
		}


		#region IEnumerable<Tscclicomp> Members

		IEnumerator<Tscclicomp> IEnumerable<Tscclicomp>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscclicomp;
			}
		}

		#endregion
		
		private TscclicompQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCCLICOMP' table
	/// </summary>

	[Serializable]
	public partial class Tscclicomp : esTscclicomp
	{
		public Tscclicomp()
		{

		}
	
		public Tscclicomp(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscclicompMetadata.Meta();
			}
		}
		
		
		
		override protected esTscclicompQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscclicompQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscclicompQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscclicompQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscclicompQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscclicompQuery query;
	}



	[Serializable]
	public partial class TscclicompQuery : esTscclicompQuery
	{
		public TscclicompQuery()
		{

		}		
		
		public TscclicompQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscclicompMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscclicompMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdCpfcgc, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdCpfcgc;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.DtNascFund, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TscclicompMetadata.PropertyNames.DtNascFund;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdConDep, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdConDep;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdDocIdent, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdDocIdent;
			c.CharacterMaxLength = 16;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdTipoDoc, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdTipoDoc;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdOrgEmit, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdOrgEmit;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.SgPaisEmis, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.SgPaisEmis;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.SgEstadoEmis, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.SgEstadoEmis;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdAtiv, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdAtiv;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdNacion, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdNacion;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdCapac, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdCapac;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.IdSexo, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.IdSexo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdEstCivil, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdEstCivil;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.NmConjuge, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.NmConjuge;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.NmPai, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.NmPai;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.NmMae, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.NmMae;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdTipoFili, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdTipoFili;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.SgPais, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.SgPais;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.InRecDivi, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.InRecDivi;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.InDistVal, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.InDistVal;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdCosif, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdCosif;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.DtDocIdent, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TscclicompMetadata.PropertyNames.DtDocIdent;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.NmLocNasc, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.NmLocNasc;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.NmEmpresa, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.NmEmpresa;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdEmpresa, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdEmpresa;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdUsuario, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.TpOcorrencia, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.TpOcorrencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdNire, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdNire;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.TpRegcas, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.TpRegcas;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.DsCargo, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.DsCargo;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.SgEstadoNasc, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TscclicompMetadata.PropertyNames.SgEstadoNasc;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdCosifCi, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdCosifCi;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdEscolaridade, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdEscolaridade;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.CdCpfConjuge, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscclicompMetadata.PropertyNames.CdCpfConjuge;	
			c.NumericPrecision = 11;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscclicompMetadata.ColumnNames.DtNascConjuge, 35, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TscclicompMetadata.PropertyNames.DtNascConjuge;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscclicompMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdCpfcgc = "CD_CPFCGC";
			 public const string DtNascFund = "DT_NASC_FUND";
			 public const string CdConDep = "CD_CON_DEP";
			 public const string CdDocIdent = "CD_DOC_IDENT";
			 public const string CdTipoDoc = "CD_TIPO_DOC";
			 public const string CdOrgEmit = "CD_ORG_EMIT";
			 public const string SgPaisEmis = "SG_PAIS_EMIS";
			 public const string SgEstadoEmis = "SG_ESTADO_EMIS";
			 public const string CdAtiv = "CD_ATIV";
			 public const string CdNacion = "CD_NACION";
			 public const string CdCapac = "CD_CAPAC";
			 public const string IdSexo = "ID_SEXO";
			 public const string CdEstCivil = "CD_EST_CIVIL";
			 public const string NmConjuge = "NM_CONJUGE";
			 public const string NmPai = "NM_PAI";
			 public const string NmMae = "NM_MAE";
			 public const string CdTipoFili = "CD_TIPO_FILI";
			 public const string SgPais = "SG_PAIS";
			 public const string InRecDivi = "IN_REC_DIVI";
			 public const string InDistVal = "IN_DIST_VAL";
			 public const string CdCosif = "CD_COSIF";
			 public const string DtDocIdent = "DT_DOC_IDENT";
			 public const string NmLocNasc = "NM_LOC_NASC";
			 public const string NmEmpresa = "NM_EMPRESA";
			 public const string CdEmpresa = "CD_EMPRESA";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string TpOcorrencia = "TP_OCORRENCIA";
			 public const string CdNire = "CD_NIRE";
			 public const string TpRegcas = "TP_REGCAS";
			 public const string DsCargo = "DS_CARGO";
			 public const string SgEstadoNasc = "SG_ESTADO_NASC";
			 public const string CdCosifCi = "CD_COSIF_CI";
			 public const string CdEscolaridade = "CD_ESCOLARIDADE";
			 public const string CdCpfConjuge = "CD_CPF_CONJUGE";
			 public const string DtNascConjuge = "DT_NASC_CONJUGE";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdCpfcgc = "CdCpfcgc";
			 public const string DtNascFund = "DtNascFund";
			 public const string CdConDep = "CdConDep";
			 public const string CdDocIdent = "CdDocIdent";
			 public const string CdTipoDoc = "CdTipoDoc";
			 public const string CdOrgEmit = "CdOrgEmit";
			 public const string SgPaisEmis = "SgPaisEmis";
			 public const string SgEstadoEmis = "SgEstadoEmis";
			 public const string CdAtiv = "CdAtiv";
			 public const string CdNacion = "CdNacion";
			 public const string CdCapac = "CdCapac";
			 public const string IdSexo = "IdSexo";
			 public const string CdEstCivil = "CdEstCivil";
			 public const string NmConjuge = "NmConjuge";
			 public const string NmPai = "NmPai";
			 public const string NmMae = "NmMae";
			 public const string CdTipoFili = "CdTipoFili";
			 public const string SgPais = "SgPais";
			 public const string InRecDivi = "InRecDivi";
			 public const string InDistVal = "InDistVal";
			 public const string CdCosif = "CdCosif";
			 public const string DtDocIdent = "DtDocIdent";
			 public const string NmLocNasc = "NmLocNasc";
			 public const string NmEmpresa = "NmEmpresa";
			 public const string CdEmpresa = "CdEmpresa";
			 public const string CdUsuario = "CdUsuario";
			 public const string TpOcorrencia = "TpOcorrencia";
			 public const string CdNire = "CdNire";
			 public const string TpRegcas = "TpRegcas";
			 public const string DsCargo = "DsCargo";
			 public const string SgEstadoNasc = "SgEstadoNasc";
			 public const string CdCosifCi = "CdCosifCi";
			 public const string CdEscolaridade = "CdEscolaridade";
			 public const string CdCpfConjuge = "CdCpfConjuge";
			 public const string DtNascConjuge = "DtNascConjuge";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscclicompMetadata))
			{
				if(TscclicompMetadata.mapDelegates == null)
				{
					TscclicompMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscclicompMetadata.meta == null)
				{
					TscclicompMetadata.meta = new TscclicompMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_NASC_FUND", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_CON_DEP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_DOC_IDENT", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_TIPO_DOC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_ORG_EMIT", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SG_PAIS_EMIS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SG_ESTADO_EMIS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_ATIV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_NACION", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CAPAC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID_SEXO", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_EST_CIVIL", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_CONJUGE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_PAI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_MAE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_TIPO_FILI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("SG_PAIS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_REC_DIVI", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("IN_DIST_VAL", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_COSIF", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_DOC_IDENT", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NM_LOC_NASC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_EMPRESA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_EMPRESA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_OCORRENCIA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_NIRE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_REGCAS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DS_CARGO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SG_ESTADO_NASC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_COSIF_CI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_ESCOLARIDADE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CPF_CONJUGE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_NASC_CONJUGE", new esTypeMap("DATE", "System.DateTime"));			
				
				
				
				meta.Source = "TSCCLICOMP";
				meta.Destination = "TSCCLICOMP";
				
				meta.spInsert = "proc_TSCCLICOMPInsert";				
				meta.spUpdate = "proc_TSCCLICOMPUpdate";		
				meta.spDelete = "proc_TSCCLICOMPDelete";
				meta.spLoadAll = "proc_TSCCLICOMPLoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCCLICOMPLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscclicompMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
