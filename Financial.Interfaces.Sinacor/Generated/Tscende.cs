/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 05/03/2010 11:36:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		









		
				
				










	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscendeCollection : esEntityCollection
	{
		public esTscendeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscendeCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscendeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscendeQuery);
		}
		#endregion
		
		virtual public Tscende DetachEntity(Tscende entity)
		{
			return base.DetachEntity(entity) as Tscende;
		}
		
		virtual public Tscende AttachEntity(Tscende entity)
		{
			return base.AttachEntity(entity) as Tscende;
		}
		
		virtual public void Combine(TscendeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscende this[int index]
		{
			get
			{
				return base[index] as Tscende;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscende);
		}
	}



	[Serializable]
	abstract public class esTscende : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscendeQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscende()
		{

		}

		public esTscende(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscendeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscendeQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;							
						case "NmLogradouro": this.str.NmLogradouro = (string)value; break;							
						case "NrPredio": this.str.NrPredio = (string)value; break;							
						case "NmCompEnde": this.str.NmCompEnde = (string)value; break;							
						case "NmBairro": this.str.NmBairro = (string)value; break;							
						case "NmCidade": this.str.NmCidade = (string)value; break;							
						case "SgEstado": this.str.SgEstado = (string)value; break;							
						case "CdCep": this.str.CdCep = (string)value; break;							
						case "CdCepExt": this.str.CdCepExt = (string)value; break;							
						case "CdDddTel": this.str.CdDddTel = (string)value; break;							
						case "NrTelefone": this.str.NrTelefone = (string)value; break;							
						case "NrRamal": this.str.NrRamal = (string)value; break;							
						case "CdDddFax": this.str.CdDddFax = (string)value; break;							
						case "NrFax": this.str.NrFax = (string)value; break;							
						case "InTipoEnde": this.str.InTipoEnde = (string)value; break;							
						case "InEndeCorr": this.str.InEndeCorr = (string)value; break;							
						case "NmEMail": this.str.NmEMail = (string)value; break;							
						case "CdDddCelular1": this.str.CdDddCelular1 = (string)value; break;							
						case "NrCelular1": this.str.NrCelular1 = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCpfcgc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfcgc = (System.Decimal?)value;
							break;
						
						case "CdCep":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCep = (System.Decimal?)value;
							break;
						
						case "CdCepExt":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCepExt = (System.Decimal?)value;
							break;
						
						case "CdDddTel":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdDddTel = (System.Decimal?)value;
							break;
						
						case "NrTelefone":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrTelefone = (System.Decimal?)value;
							break;
						
						case "NrRamal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrRamal = (System.Decimal?)value;
							break;
						
						case "CdDddFax":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdDddFax = (System.Decimal?)value;
							break;
						
						case "NrFax":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrFax = (System.Decimal?)value;
							break;
						
						case "CdDddCelular1":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdDddCelular1 = (System.Decimal?)value;
							break;
						
						case "NrCelular1":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrCelular1 = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCENDE.CD_CPFCGC
		/// </summary>
		virtual public System.Decimal? CdCpfcgc
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdCpfcgc);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdCpfcgc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NM_LOGRADOURO
		/// </summary>
		virtual public System.String NmLogradouro
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NmLogradouro);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NmLogradouro, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NR_PREDIO
		/// </summary>
		virtual public System.String NrPredio
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NrPredio);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NrPredio, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NM_COMP_ENDE
		/// </summary>
		virtual public System.String NmCompEnde
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NmCompEnde);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NmCompEnde, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NM_BAIRRO
		/// </summary>
		virtual public System.String NmBairro
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NmBairro);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NmBairro, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NM_CIDADE
		/// </summary>
		virtual public System.String NmCidade
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NmCidade);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NmCidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.SG_ESTADO
		/// </summary>
		virtual public System.String SgEstado
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.SgEstado);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.SgEstado, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.CD_CEP
		/// </summary>
		virtual public System.Decimal? CdCep
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdCep);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdCep, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.CD_CEP_EXT
		/// </summary>
		virtual public System.Decimal? CdCepExt
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdCepExt);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdCepExt, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.CD_DDD_TEL
		/// </summary>
		virtual public System.Decimal? CdDddTel
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdDddTel);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdDddTel, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NR_TELEFONE
		/// </summary>
		virtual public System.Decimal? NrTelefone
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.NrTelefone);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.NrTelefone, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NR_RAMAL
		/// </summary>
		virtual public System.Decimal? NrRamal
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.NrRamal);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.NrRamal, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.CD_DDD_FAX
		/// </summary>
		virtual public System.Decimal? CdDddFax
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdDddFax);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdDddFax, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NR_FAX
		/// </summary>
		virtual public System.Decimal? NrFax
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.NrFax);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.NrFax, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.IN_TIPO_ENDE
		/// </summary>
		virtual public System.String InTipoEnde
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.InTipoEnde);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.InTipoEnde, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.IN_ENDE_CORR
		/// </summary>
		virtual public System.String InEndeCorr
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.InEndeCorr);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.InEndeCorr, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NM_E_MAIL
		/// </summary>
		virtual public System.String NmEMail
		{
			get
			{
				return base.GetSystemString(TscendeMetadata.ColumnNames.NmEMail);
			}
			
			set
			{
				base.SetSystemString(TscendeMetadata.ColumnNames.NmEMail, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.CD_DDD_CELULAR1
		/// </summary>
		virtual public System.Decimal? CdDddCelular1
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.CdDddCelular1);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.CdDddCelular1, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.NR_CELULAR1
		/// </summary>
		virtual public System.Decimal? NrCelular1
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.NrCelular1);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.NrCelular1, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCENDE.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscendeMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscendeMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscende entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCpfcgc
			{
				get
				{
					System.Decimal? data = entity.CdCpfcgc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfcgc = null;
					else entity.CdCpfcgc = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmLogradouro
			{
				get
				{
					System.String data = entity.NmLogradouro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmLogradouro = null;
					else entity.NmLogradouro = Convert.ToString(value);
				}
			}
				
			public System.String NrPredio
			{
				get
				{
					System.String data = entity.NrPredio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrPredio = null;
					else entity.NrPredio = Convert.ToString(value);
				}
			}
				
			public System.String NmCompEnde
			{
				get
				{
					System.String data = entity.NmCompEnde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmCompEnde = null;
					else entity.NmCompEnde = Convert.ToString(value);
				}
			}
				
			public System.String NmBairro
			{
				get
				{
					System.String data = entity.NmBairro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmBairro = null;
					else entity.NmBairro = Convert.ToString(value);
				}
			}
				
			public System.String NmCidade
			{
				get
				{
					System.String data = entity.NmCidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmCidade = null;
					else entity.NmCidade = Convert.ToString(value);
				}
			}
				
			public System.String SgEstado
			{
				get
				{
					System.String data = entity.SgEstado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SgEstado = null;
					else entity.SgEstado = Convert.ToString(value);
				}
			}
				
			public System.String CdCep
			{
				get
				{
					System.Decimal? data = entity.CdCep;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCep = null;
					else entity.CdCep = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCepExt
			{
				get
				{
					System.Decimal? data = entity.CdCepExt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCepExt = null;
					else entity.CdCepExt = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdDddTel
			{
				get
				{
					System.Decimal? data = entity.CdDddTel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDddTel = null;
					else entity.CdDddTel = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrTelefone
			{
				get
				{
					System.Decimal? data = entity.NrTelefone;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrTelefone = null;
					else entity.NrTelefone = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrRamal
			{
				get
				{
					System.Decimal? data = entity.NrRamal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrRamal = null;
					else entity.NrRamal = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdDddFax
			{
				get
				{
					System.Decimal? data = entity.CdDddFax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDddFax = null;
					else entity.CdDddFax = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrFax
			{
				get
				{
					System.Decimal? data = entity.NrFax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrFax = null;
					else entity.NrFax = Convert.ToDecimal(value);
				}
			}
				
			public System.String InTipoEnde
			{
				get
				{
					System.String data = entity.InTipoEnde;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InTipoEnde = null;
					else entity.InTipoEnde = Convert.ToString(value);
				}
			}
				
			public System.String InEndeCorr
			{
				get
				{
					System.String data = entity.InEndeCorr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InEndeCorr = null;
					else entity.InEndeCorr = Convert.ToString(value);
				}
			}
				
			public System.String NmEMail
			{
				get
				{
					System.String data = entity.NmEMail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEMail = null;
					else entity.NmEMail = Convert.ToString(value);
				}
			}
				
			public System.String CdDddCelular1
			{
				get
				{
					System.Decimal? data = entity.CdDddCelular1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDddCelular1 = null;
					else entity.CdDddCelular1 = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrCelular1
			{
				get
				{
					System.Decimal? data = entity.NrCelular1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrCelular1 = null;
					else entity.NrCelular1 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTscende entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscendeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscende can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscende : esTscende
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscendeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscendeMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCpfcgc
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmLogradouro
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NmLogradouro, esSystemType.String);
			}
		} 
		
		public esQueryItem NrPredio
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NrPredio, esSystemType.String);
			}
		} 
		
		public esQueryItem NmCompEnde
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NmCompEnde, esSystemType.String);
			}
		} 
		
		public esQueryItem NmBairro
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NmBairro, esSystemType.String);
			}
		} 
		
		public esQueryItem NmCidade
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NmCidade, esSystemType.String);
			}
		} 
		
		public esQueryItem SgEstado
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.SgEstado, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCep
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdCep, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCepExt
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdCepExt, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdDddTel
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdDddTel, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrTelefone
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NrTelefone, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrRamal
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NrRamal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdDddFax
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdDddFax, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrFax
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NrFax, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InTipoEnde
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.InTipoEnde, esSystemType.String);
			}
		} 
		
		public esQueryItem InEndeCorr
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.InEndeCorr, esSystemType.String);
			}
		} 
		
		public esQueryItem NmEMail
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NmEMail, esSystemType.String);
			}
		} 
		
		public esQueryItem CdDddCelular1
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.CdDddCelular1, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrCelular1
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.NrCelular1, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscendeMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscendeCollection")]
	public partial class TscendeCollection : esTscendeCollection, IEnumerable<Tscende>
	{
		public TscendeCollection()
		{

		}
		
		public static implicit operator List<Tscende>(TscendeCollection coll)
		{
			List<Tscende> list = new List<Tscende>();
			
			foreach (Tscende emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscendeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscendeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscende(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscende();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscendeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscendeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscendeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscende AddNew()
		{
			Tscende entity = base.AddNewEntity() as Tscende;
			
			return entity;
		}

		public Tscende FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscende;
		}


		#region IEnumerable<Tscende> Members

		IEnumerator<Tscende> IEnumerable<Tscende>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscende;
			}
		}

		#endregion
		
		private TscendeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCENDE' table
	/// </summary>

	[Serializable]
	public partial class Tscende : esTscende
	{
		public Tscende()
		{

		}
	
		public Tscende(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscendeMetadata.Meta();
			}
		}
		
		
		
		override protected esTscendeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscendeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscendeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscendeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscendeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscendeQuery query;
	}



	[Serializable]
	public partial class TscendeQuery : esTscendeQuery
	{
		public TscendeQuery()
		{

		}		
		
		public TscendeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscendeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscendeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdCpfcgc, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdCpfcgc;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NmLogradouro, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NmLogradouro;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NrPredio, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NrPredio;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NmCompEnde, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NmCompEnde;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NmBairro, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NmBairro;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NmCidade, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NmCidade;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.SgEstado, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.SgEstado;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdCep, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdCep;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdCepExt, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdCepExt;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdDddTel, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdDddTel;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NrTelefone, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.NrTelefone;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NrRamal, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.NrRamal;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdDddFax, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdDddFax;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NrFax, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.NrFax;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.InTipoEnde, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.InTipoEnde;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.InEndeCorr, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.InEndeCorr;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NmEMail, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TscendeMetadata.PropertyNames.NmEMail;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.CdDddCelular1, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.CdDddCelular1;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.NrCelular1, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.NrCelular1;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscendeMetadata.ColumnNames.Id, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscendeMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscendeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCpfcgc = "CD_CPFCGC";
			 public const string NmLogradouro = "NM_LOGRADOURO";
			 public const string NrPredio = "NR_PREDIO";
			 public const string NmCompEnde = "NM_COMP_ENDE";
			 public const string NmBairro = "NM_BAIRRO";
			 public const string NmCidade = "NM_CIDADE";
			 public const string SgEstado = "SG_ESTADO";
			 public const string CdCep = "CD_CEP";
			 public const string CdCepExt = "CD_CEP_EXT";
			 public const string CdDddTel = "CD_DDD_TEL";
			 public const string NrTelefone = "NR_TELEFONE";
			 public const string NrRamal = "NR_RAMAL";
			 public const string CdDddFax = "CD_DDD_FAX";
			 public const string NrFax = "NR_FAX";
			 public const string InTipoEnde = "IN_TIPO_ENDE";
			 public const string InEndeCorr = "IN_ENDE_CORR";
			 public const string NmEMail = "NM_E_MAIL";
			 public const string CdDddCelular1 = "CD_DDD_CELULAR1";
			 public const string NrCelular1 = "NR_CELULAR1";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCpfcgc = "CdCpfcgc";
			 public const string NmLogradouro = "NmLogradouro";
			 public const string NrPredio = "NrPredio";
			 public const string NmCompEnde = "NmCompEnde";
			 public const string NmBairro = "NmBairro";
			 public const string NmCidade = "NmCidade";
			 public const string SgEstado = "SgEstado";
			 public const string CdCep = "CdCep";
			 public const string CdCepExt = "CdCepExt";
			 public const string CdDddTel = "CdDddTel";
			 public const string NrTelefone = "NrTelefone";
			 public const string NrRamal = "NrRamal";
			 public const string CdDddFax = "CdDddFax";
			 public const string NrFax = "NrFax";
			 public const string InTipoEnde = "InTipoEnde";
			 public const string InEndeCorr = "InEndeCorr";
			 public const string NmEMail = "NmEMail";
			 public const string CdDddCelular1 = "CdDddCelular1";
			 public const string NrCelular1 = "NrCelular1";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscendeMetadata))
			{
				if(TscendeMetadata.mapDelegates == null)
				{
					TscendeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscendeMetadata.meta == null)
				{
					TscendeMetadata.meta = new TscendeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_LOGRADOURO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NR_PREDIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_COMP_ENDE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_BAIRRO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_CIDADE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SG_ESTADO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CEP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CEP_EXT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_DDD_TEL", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_TELEFONE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_RAMAL", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_DDD_FAX", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_FAX", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_TIPO_ENDE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("IN_ENDE_CORR", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NM_E_MAIL", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_DDD_CELULAR1", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_CELULAR1", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TSCENDE";
				meta.Destination = "TSCENDE";
				
				meta.spInsert = "proc_TSCENDEInsert";				
				meta.spUpdate = "proc_TSCENDEUpdate";		
				meta.spDelete = "proc_TSCENDEDelete";
				meta.spLoadAll = "proc_TSCENDELoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCENDELoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscendeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
