/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 10/02/2010 10:56:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;































































































































































































namespace Financial.Interfaces.Sinacor {

    [Serializable]
    abstract public class esVcfposicaoCollection : esEntityCollection {
        public esVcfposicaoCollection() {

        }

        protected override string GetCollectionName() {
            return "VcfposicaoCollection";
        }

        #region Query Logic
        protected void InitQuery(esVcfposicaoQuery query) {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table) {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query) {
            this.InitQuery(query as esVcfposicaoQuery);
        }
        #endregion

        virtual public Vcfposicao DetachEntity(Vcfposicao entity) {
            return base.DetachEntity(entity) as Vcfposicao;
        }

        virtual public Vcfposicao AttachEntity(Vcfposicao entity) {
            return base.AttachEntity(entity) as Vcfposicao;
        }

        virtual public void Combine(VcfposicaoCollection collection) {
            base.Combine(collection);
        }

        new public Vcfposicao this[int index] {
            get {
                return base[index] as Vcfposicao;
            }
        }

        public override Type GetEntityType() {
            return typeof(Vcfposicao);
        }
    }



    [Serializable]
    abstract public class esVcfposicao : esEntity {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esVcfposicaoQuery GetDynamicQuery() {
            return null;
        }

        public esVcfposicao() {

        }

        public esVcfposicao(DataRow row)
            : base(row) {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Decimal id) {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id) {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esVcfposicaoQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.Id == id);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id) {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        private bool LoadByPrimaryKeyDynamic(System.Decimal id) {
            esVcfposicaoQuery query = this.GetDynamicQuery();
            query.Where(query.Id == id);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id) {
            esParameters parms = new esParameters();
            parms.Add("ID", id);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values) {
            foreach (string propertyName in values.Keys) {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value) {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null) {
                if (value == null || value.GetType().ToString() == "System.String") {
                    // Use the strongly typed property
                    switch (name) {
                        case "TipoGrup": this.str.TipoGrup = (string)value; break;
                        case "CodCli": this.str.CodCli = (string)value; break;
                        case "CodNeg": this.str.CodNeg = (string)value; break;
                        case "TipoMerc": this.str.TipoMerc = (string)value; break;
                        case "CodCart": this.str.CodCart = (string)value; break;
                        case "QtdeTot": this.str.QtdeTot = (string)value; break;
                        case "QtdeDisp": this.str.QtdeDisp = (string)value; break;
                        case "QtdePend": this.str.QtdePend = (string)value; break;
                        case "QtdePendCpa": this.str.QtdePendCpa = (string)value; break;
                        case "QtdePendVda": this.str.QtdePendVda = (string)value; break;
                        case "QtdeProv": this.str.QtdeProv = (string)value; break;
                        case "Id": this.str.Id = (string)value; break;
                        case "IdtAtiv": this.str.IdtAtiv = (string)value; break;
                        case "DataVenc": this.str.DataVenc = (string)value; break;
                        case "CodSeri": this.str.CodSeri = (string)value; break;
                    }
                }
                else {
                    switch (name) {
                        case "CodCli":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodCli = (System.Decimal?)value;
                            break;

                        case "CodCart":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodCart = (System.Decimal?)value;
                            break;

                        case "QtdeTot":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdeTot = (System.Decimal?)value;
                            break;

                        case "QtdeDisp":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdeDisp = (System.Decimal?)value;
                            break;

                        case "QtdePend":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdePend = (System.Decimal?)value;
                            break;

                        case "QtdePendCpa":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdePendCpa = (System.Decimal?)value;
                            break;

                        case "QtdePendVda":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdePendVda = (System.Decimal?)value;
                            break;

                        case "QtdeProv":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdeProv = (System.Decimal?)value;
                            break;

                        case "Id":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Id = (System.Decimal?)value;
                            break;

                        case "DataVenc":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataVenc = (System.DateTime?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name)) {
                this.Row[name] = value;
            }
            else {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to VCFPOSICAO.TIPO_GRUP
        /// </summary>
        virtual public System.String TipoGrup {
            get {
                return base.GetSystemString(VcfposicaoMetadata.ColumnNames.TipoGrup);
            }

            set {
                base.SetSystemString(VcfposicaoMetadata.ColumnNames.TipoGrup, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.COD_CLI
        /// </summary>
        virtual public System.Decimal? CodCli {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.CodCli);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.CodCli, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.COD_NEG
        /// </summary>
        virtual public System.String CodNeg {
            get {
                return base.GetSystemString(VcfposicaoMetadata.ColumnNames.CodNeg);
            }

            set {
                base.SetSystemString(VcfposicaoMetadata.ColumnNames.CodNeg, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.TIPO_MERC
        /// </summary>
        virtual public System.String TipoMerc {
            get {
                return base.GetSystemString(VcfposicaoMetadata.ColumnNames.TipoMerc);
            }

            set {
                base.SetSystemString(VcfposicaoMetadata.ColumnNames.TipoMerc, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.COD_CART
        /// </summary>
        virtual public System.Decimal? CodCart {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.CodCart);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.CodCart, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_TOT
        /// </summary>
        virtual public System.Decimal? QtdeTot {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeTot);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeTot, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_DISP
        /// </summary>
        virtual public System.Decimal? QtdeDisp {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeDisp);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeDisp, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_PEND
        /// </summary>
        virtual public System.Decimal? QtdePend {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePend);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePend, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_PEND_CPA
        /// </summary>
        virtual public System.Decimal? QtdePendCpa {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePendCpa);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePendCpa, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_PEND_VDA
        /// </summary>
        virtual public System.Decimal? QtdePendVda {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePendVda);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdePendVda, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.QTDE_PROV
        /// </summary>
        virtual public System.Decimal? QtdeProv {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeProv);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.QtdeProv, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.ID
        /// </summary>
        virtual public System.Decimal? Id {
            get {
                return base.GetSystemDecimal(VcfposicaoMetadata.ColumnNames.Id);
            }

            set {
                base.SetSystemDecimal(VcfposicaoMetadata.ColumnNames.Id, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.IDT_ATIV
        /// </summary>
        virtual public System.String IdtAtiv {
            get {
                return base.GetSystemString(VcfposicaoMetadata.ColumnNames.IdtAtiv);
            }

            set {
                base.SetSystemString(VcfposicaoMetadata.ColumnNames.IdtAtiv, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.DATA_VENC
        /// </summary>
        virtual public System.DateTime? DataVenc {
            get {
                return base.GetSystemDateTime(VcfposicaoMetadata.ColumnNames.DataVenc);
            }

            set {
                base.SetSystemDateTime(VcfposicaoMetadata.ColumnNames.DataVenc, value);
            }
        }

        /// <summary>
        /// Maps to VCFPOSICAO.COD_SERI
        /// </summary>
        virtual public System.String CodSeri {
            get {
                return base.GetSystemString(VcfposicaoMetadata.ColumnNames.CodSeri);
            }

            set {
                base.SetSystemString(VcfposicaoMetadata.ColumnNames.CodSeri, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str {
            get {
                if (esstrings == null) {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings {
            public esStrings(esVcfposicao entity) {
                this.entity = entity;
            }


            public System.String TipoGrup {
                get {
                    System.String data = entity.TipoGrup;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.TipoGrup = null;
                    else entity.TipoGrup = Convert.ToString(value);
                }
            }

            public System.String CodCli {
                get {
                    System.Decimal? data = entity.CodCli;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodCli = null;
                    else entity.CodCli = Convert.ToDecimal(value);
                }
            }

            public System.String CodNeg {
                get {
                    System.String data = entity.CodNeg;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodNeg = null;
                    else entity.CodNeg = Convert.ToString(value);
                }
            }

            public System.String TipoMerc {
                get {
                    System.String data = entity.TipoMerc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.TipoMerc = null;
                    else entity.TipoMerc = Convert.ToString(value);
                }
            }

            public System.String CodCart {
                get {
                    System.Decimal? data = entity.CodCart;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodCart = null;
                    else entity.CodCart = Convert.ToDecimal(value);
                }
            }

            public System.String QtdeTot {
                get {
                    System.Decimal? data = entity.QtdeTot;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdeTot = null;
                    else entity.QtdeTot = Convert.ToDecimal(value);
                }
            }

            public System.String QtdeDisp {
                get {
                    System.Decimal? data = entity.QtdeDisp;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdeDisp = null;
                    else entity.QtdeDisp = Convert.ToDecimal(value);
                }
            }

            public System.String QtdePend {
                get {
                    System.Decimal? data = entity.QtdePend;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdePend = null;
                    else entity.QtdePend = Convert.ToDecimal(value);
                }
            }

            public System.String QtdePendCpa {
                get {
                    System.Decimal? data = entity.QtdePendCpa;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdePendCpa = null;
                    else entity.QtdePendCpa = Convert.ToDecimal(value);
                }
            }

            public System.String QtdePendVda {
                get {
                    System.Decimal? data = entity.QtdePendVda;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdePendVda = null;
                    else entity.QtdePendVda = Convert.ToDecimal(value);
                }
            }

            public System.String QtdeProv {
                get {
                    System.Decimal? data = entity.QtdeProv;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdeProv = null;
                    else entity.QtdeProv = Convert.ToDecimal(value);
                }
            }

            public System.String Id {
                get {
                    System.Decimal? data = entity.Id;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.Id = null;
                    else entity.Id = Convert.ToDecimal(value);
                }
            }

            public System.String IdtAtiv {
                get {
                    System.String data = entity.IdtAtiv;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.IdtAtiv = null;
                    else entity.IdtAtiv = Convert.ToString(value);
                }
            }

            public System.String DataVenc {
                get {
                    System.DateTime? data = entity.DataVenc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataVenc = null;
                    else entity.DataVenc = Convert.ToDateTime(value);
                }
            }

            public System.String CodSeri {
                get {
                    System.String data = entity.CodSeri;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodSeri = null;
                    else entity.CodSeri = Convert.ToString(value);
                }
            }


            private esVcfposicao entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esVcfposicaoQuery query) {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table) {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1) {
                throw new Exception("esVcfposicao can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class Vcfposicao : esVcfposicao {


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties() {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys() {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys() {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys() {
        }

    }



    [Serializable]
    abstract public class esVcfposicaoQuery : esDynamicQuery {
        override protected IMetadata Meta {
            get {
                return VcfposicaoMetadata.Meta();
            }
        }


        public esQueryItem TipoGrup {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.TipoGrup, esSystemType.String);
            }
        }

        public esQueryItem CodCli {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.CodCli, esSystemType.Decimal);
            }
        }

        public esQueryItem CodNeg {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.CodNeg, esSystemType.String);
            }
        }

        public esQueryItem TipoMerc {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.TipoMerc, esSystemType.String);
            }
        }

        public esQueryItem CodCart {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.CodCart, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdeTot {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdeTot, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdeDisp {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdeDisp, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdePend {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdePend, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdePendCpa {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdePendCpa, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdePendVda {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdePendVda, esSystemType.Decimal);
            }
        }

        public esQueryItem QtdeProv {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.QtdeProv, esSystemType.Decimal);
            }
        }

        public esQueryItem Id {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.Id, esSystemType.Decimal);
            }
        }

        public esQueryItem IdtAtiv {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.IdtAtiv, esSystemType.String);
            }
        }

        public esQueryItem DataVenc {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
            }
        }

        public esQueryItem CodSeri {
            get {
                return new esQueryItem(this, VcfposicaoMetadata.ColumnNames.CodSeri, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("VcfposicaoCollection")]
    public partial class VcfposicaoCollection : esVcfposicaoCollection, IEnumerable<Vcfposicao> {
        public VcfposicaoCollection() {

        }

        public static implicit operator List<Vcfposicao>(VcfposicaoCollection coll) {
            List<Vcfposicao> list = new List<Vcfposicao>();

            foreach (Vcfposicao emp in coll) {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta {
            get {
                return VcfposicaoMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery() {
            if (this.query == null) {
                this.query = new VcfposicaoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row) {
            return new Vcfposicao(row);
        }

        override protected esEntity CreateEntity() {
            return new Vcfposicao();
        }


        #endregion


        [BrowsableAttribute(false)]
        public VcfposicaoQuery Query {
            get {
                if (this.query == null) {
                    this.query = new VcfposicaoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset() {
            this.query = null;
        }

        public bool Load(VcfposicaoQuery query) {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public Vcfposicao AddNew() {
            Vcfposicao entity = base.AddNewEntity() as Vcfposicao;

            return entity;
        }

        public Vcfposicao FindByPrimaryKey(System.Decimal id) {
            return base.FindByPrimaryKey(id) as Vcfposicao;
        }


        #region IEnumerable<Vcfposicao> Members

        IEnumerator<Vcfposicao> IEnumerable<Vcfposicao>.GetEnumerator() {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext()) {
                yield return iterator.Current as Vcfposicao;
            }
        }

        #endregion

        private VcfposicaoQuery query;
    }


    /// <summary>
    /// Encapsulates the 'VCFPOSICAO' table
    /// </summary>

    [Serializable]
    public partial class Vcfposicao : esVcfposicao {
        public Vcfposicao() {

        }

        public Vcfposicao(DataRow row)
            : base(row) {

        }

        #region Housekeeping methods
        override protected IMetadata Meta {
            get {
                return VcfposicaoMetadata.Meta();
            }
        }



        override protected esVcfposicaoQuery GetDynamicQuery() {
            if (this.query == null) {
                this.query = new VcfposicaoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public VcfposicaoQuery Query {
            get {
                if (this.query == null) {
                    this.query = new VcfposicaoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset() {
            this.query = null;
        }


        public bool Load(VcfposicaoQuery query) {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private VcfposicaoQuery query;
    }



    [Serializable]
    public partial class VcfposicaoQuery : esVcfposicaoQuery {
        public VcfposicaoQuery() {

        }

        public VcfposicaoQuery(string joinAlias) {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class VcfposicaoMetadata : esMetadata, IMetadata {
        #region Protected Constructor
        protected VcfposicaoMetadata() {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.TipoGrup, 0, typeof(System.String), esSystemType.String);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.TipoGrup;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.CodCli, 1, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.CodCli;
            c.NumericPrecision = 7;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.CodNeg, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.CodNeg;
            c.CharacterMaxLength = 12;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.TipoMerc, 3, typeof(System.String), esSystemType.String);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.TipoMerc;
            c.CharacterMaxLength = 5;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.CodCart, 4, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.CodCart;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdeTot, 5, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdeTot;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdeDisp, 6, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdeDisp;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdePend, 7, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdePend;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdePendCpa, 8, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdePendCpa;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdePendVda, 9, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdePendVda;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.QtdeProv, 10, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.QtdeProv;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.Id, 11, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.Id;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.IdtAtiv, 12, typeof(System.String), esSystemType.String);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.IdtAtiv;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.DataVenc, 13, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.DataVenc;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(VcfposicaoMetadata.ColumnNames.CodSeri, 14, typeof(System.String), esSystemType.String);
            c.PropertyName = VcfposicaoMetadata.PropertyNames.CodSeri;
            c.CharacterMaxLength = 10;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


        }
        #endregion

        static public VcfposicaoMetadata Meta() {
            return meta;
        }

        public Guid DataID {
            get { return base._dataID; }
        }

        public bool MultiProviderMode {
            get { return false; }
        }

        public esColumnMetadataCollection Columns {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames {
            public const string TipoGrup = "TIPO_GRUP";
            public const string CodCli = "COD_CLI";
            public const string CodNeg = "COD_NEG";
            public const string TipoMerc = "TIPO_MERC";
            public const string CodCart = "COD_CART";
            public const string QtdeTot = "QTDE_TOT";
            public const string QtdeDisp = "QTDE_DISP";
            public const string QtdePend = "QTDE_PEND";
            public const string QtdePendCpa = "QTDE_PEND_CPA";
            public const string QtdePendVda = "QTDE_PEND_VDA";
            public const string QtdeProv = "QTDE_PROV";
            public const string Id = "ID";
            public const string IdtAtiv = "IDT_ATIV";
            public const string DataVenc = "DATA_VENC";
            public const string CodSeri = "COD_SERI";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames {
            public const string TipoGrup = "TipoGrup";
            public const string CodCli = "CodCli";
            public const string CodNeg = "CodNeg";
            public const string TipoMerc = "TipoMerc";
            public const string CodCart = "CodCart";
            public const string QtdeTot = "QtdeTot";
            public const string QtdeDisp = "QtdeDisp";
            public const string QtdePend = "QtdePend";
            public const string QtdePendCpa = "QtdePendCpa";
            public const string QtdePendVda = "QtdePendVda";
            public const string QtdeProv = "QtdeProv";
            public const string Id = "Id";
            public const string IdtAtiv = "IdtAtiv";
            public const string DataVenc = "DataVenc";
            public const string CodSeri = "CodSeri";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName) {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault() {
            // This is only executed once per the life of the application
            lock (typeof(VcfposicaoMetadata)) {
                if (VcfposicaoMetadata.mapDelegates == null) {
                    VcfposicaoMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (VcfposicaoMetadata.meta == null) {
                    VcfposicaoMetadata.meta = new VcfposicaoMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName) {
            if (!_providerMetadataMaps.ContainsKey(mapName)) {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("TIPO_GRUP", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("TIPO_MERC", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("COD_CART", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_TOT", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_DISP", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_PEND", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_PEND_CPA", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_PEND_VDA", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QTDE_PROV", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("IDT_ATIV", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("COD_SERI", new esTypeMap("VARCHAR2", "System.String"));



                meta.Source = "VCFPOSICAO";
                meta.Destination = "VCFPOSICAO";

                meta.spInsert = "proc_VCFPOSICAOInsert";
                meta.spUpdate = "proc_VCFPOSICAOUpdate";
                meta.spDelete = "proc_VCFPOSICAODelete";
                meta.spLoadAll = "proc_VCFPOSICAOLoadAll";
                meta.spLoadByPrimaryKey = "proc_VCFPOSICAOLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private VcfposicaoMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}