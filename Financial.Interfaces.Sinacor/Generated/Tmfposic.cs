/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 05/11/2012 19:40:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.Interfaces.Sinacor {

    [Serializable]
    abstract public class esTmfposicCollection : esEntityCollection {
        public esTmfposicCollection() {

        }

        protected override string GetCollectionName() {
            return "TmfposicCollection";
        }

        #region Query Logic
        protected void InitQuery(esTmfposicQuery query) {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table) {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query) {
            this.InitQuery(query as esTmfposicQuery);
        }
        #endregion

        virtual public Tmfposic DetachEntity(Tmfposic entity) {
            return base.DetachEntity(entity) as Tmfposic;
        }

        virtual public Tmfposic AttachEntity(Tmfposic entity) {
            return base.AttachEntity(entity) as Tmfposic;
        }

        virtual public void Combine(TmfposicCollection collection) {
            base.Combine(collection);
        }

        new public Tmfposic this[int index] {
            get {
                return base[index] as Tmfposic;
            }
        }

        public override Type GetEntityType() {
            return typeof(Tmfposic);
        }
    }



    [Serializable]
    abstract public class esTmfposic : esEntity {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esTmfposicQuery GetDynamicQuery() {
            return null;
        }

        public esTmfposic() {

        }

        public esTmfposic(DataRow row)
            : base(row) {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Decimal id) {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id) {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esTmfposicQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.Id == id);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id) {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(id);
            else
                return LoadByPrimaryKeyStoredProcedure(id);
        }

        private bool LoadByPrimaryKeyDynamic(System.Decimal id) {
            esTmfposicQuery query = this.GetDynamicQuery();
            query.Where(query.Id == id);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id) {
            esParameters parms = new esParameters();
            parms.Add("ID", id);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values) {
            foreach (string propertyName in values.Keys) {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value) {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null) {
                if (value == null || value.GetType().ToString() == "System.String") {
                    // Use the strongly typed property
                    switch (name) {
                        case "Id": this.str.Id = (string)value; break;
                        case "DtDatmov": this.str.DtDatmov = (string)value; break;
                        case "CdCliente": this.str.CdCliente = (string)value; break;
                        case "CdCommod": this.str.CdCommod = (string)value; break;
                        case "CdMercad": this.str.CdMercad = (string)value; break;
                        case "CdSerie": this.str.CdSerie = (string)value; break;
                        case "QtDiaAnt": this.str.QtDiaAnt = (string)value; break;
                        case "QtDiaAtu": this.str.QtDiaAtu = (string)value; break;
                        case "VlDiaAnt": this.str.VlDiaAnt = (string)value; break;
                        case "VlDiaAtu": this.str.VlDiaAtu = (string)value; break;
                        case "VlAjupos": this.str.VlAjupos = (string)value; break;
                        case "VlAjuperAnt": this.str.VlAjuperAnt = (string)value; break;
                        case "VlAjuper": this.str.VlAjuper = (string)value; break;
                        case "VlAjuposAcuAnt": this.str.VlAjuposAcuAnt = (string)value; break;
                        case "VlAjuposAcu": this.str.VlAjuposAcu = (string)value; break;
                        case "CodTipoOpc": this.str.CodTipoOpc = (string)value; break;
                        case "ValComn": this.str.ValComn = (string)value; break;
                        case "DataMvto": this.str.DataMvto = (string)value; break;
                        case "CodCli": this.str.CodCli = (string)value; break;
                        case "CodIsin": this.str.CodIsin = (string)value; break;
                        case "NumDist": this.str.NumDist = (string)value; break;
                        case "CodNeg": this.str.CodNeg = (string)value; break;
                        case "CodLoc": this.str.CodLoc = (string)value; break;
                        case "CodCart": this.str.CodCart = (string)value; break;
                        case "DataPreg": this.str.DataPreg = (string)value; break;
                        case "NumNego": this.str.NumNego = (string)value; break;
                        case "ValNego": this.str.ValNego = (string)value; break;
                        case "CodUsuaConp": this.str.CodUsuaConp = (string)value; break;
                        case "DataVenc": this.str.DataVenc = (string)value; break;
                        case "QtdeLqdo": this.str.QtdeLqdo = (string)value; break;
                        case "TipoLiqd": this.str.TipoLiqd = (string)value; break;
                        case "ValLqdo": this.str.ValLqdo = (string)value; break;
                        case "DataLiqd": this.str.DataLiqd = (string)value; break;
                        case "NomeTmnl": this.str.NomeTmnl = (string)value; break;
                        case "NomeUsua": this.str.NomeUsua = (string)value; break;
                        case "DataSist": this.str.DataSist = (string)value; break;
                        case "CodIsinLqdo": this.str.CodIsinLqdo = (string)value; break;
                        case "NumDistLqdo": this.str.NumDistLqdo = (string)value; break;
                        case "IndIntrEftv": this.str.IndIntrEftv = (string)value; break;
                        case "NumCotr": this.str.NumCotr = (string)value; break;
                        case "DigCotr": this.str.DigCotr = (string)value; break;
                        case "IndIntrExec": this.str.IndIntrExec = (string)value; break;
                        case "NomeUsuaIntr": this.str.NomeUsuaIntr = (string)value; break;
                        case "NomeTmnlIntr": this.str.NomeTmnlIntr = (string)value; break;
                        case "DataSistIntr": this.str.DataSistIntr = (string)value; break;
                        case "IndLibe": this.str.IndLibe = (string)value; break;
                        case "PrMedioAtu": this.str.PrMedioAtu = (string)value; break;
                    }
                }
                else {
                    switch (name) {
                        case "Id":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Id = (System.Decimal?)value;
                            break;

                        case "DtDatmov":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DtDatmov = (System.DateTime?)value;
                            break;

                        case "CdCliente":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CdCliente = (System.Decimal?)value;
                            break;

                        case "QtDiaAnt":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtDiaAnt = (System.Decimal?)value;
                            break;

                        case "QtDiaAtu":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtDiaAtu = (System.Decimal?)value;
                            break;

                        case "VlDiaAnt":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlDiaAnt = (System.Decimal?)value;
                            break;

                        case "VlDiaAtu":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlDiaAtu = (System.Decimal?)value;
                            break;

                        case "VlAjupos":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlAjupos = (System.Decimal?)value;
                            break;

                        case "VlAjuperAnt":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlAjuperAnt = (System.Decimal?)value;
                            break;

                        case "VlAjuper":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlAjuper = (System.Decimal?)value;
                            break;

                        case "VlAjuposAcuAnt":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlAjuposAcuAnt = (System.Decimal?)value;
                            break;

                        case "VlAjuposAcu":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.VlAjuposAcu = (System.Decimal?)value;
                            break;

                        case "ValComn":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.ValComn = (System.Decimal?)value;
                            break;

                        case "DataMvto":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataMvto = (System.DateTime?)value;
                            break;

                        case "CodCli":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodCli = (System.Decimal?)value;
                            break;

                        case "NumDist":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.NumDist = (System.Decimal?)value;
                            break;

                        case "CodLoc":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodLoc = (System.Decimal?)value;
                            break;

                        case "CodCart":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodCart = (System.Decimal?)value;
                            break;

                        case "DataPreg":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataPreg = (System.DateTime?)value;
                            break;

                        case "NumNego":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.NumNego = (System.Decimal?)value;
                            break;

                        case "ValNego":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.ValNego = (System.Decimal?)value;
                            break;

                        case "CodUsuaConp":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CodUsuaConp = (System.Decimal?)value;
                            break;

                        case "DataVenc":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataVenc = (System.DateTime?)value;
                            break;

                        case "QtdeLqdo":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QtdeLqdo = (System.Decimal?)value;
                            break;

                        case "TipoLiqd":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.TipoLiqd = (System.Decimal?)value;
                            break;

                        case "ValLqdo":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.ValLqdo = (System.Decimal?)value;
                            break;

                        case "DataLiqd":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataLiqd = (System.DateTime?)value;
                            break;

                        case "DataSist":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataSist = (System.DateTime?)value;
                            break;

                        case "NumDistLqdo":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.NumDistLqdo = (System.Decimal?)value;
                            break;

                        case "NumCotr":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.NumCotr = (System.Decimal?)value;
                            break;

                        case "DigCotr":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.DigCotr = (System.Decimal?)value;
                            break;

                        case "DataSistIntr":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataSistIntr = (System.DateTime?)value;
                            break;

                        case "PrMedioAtu":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.PrMedioAtu = (System.Decimal?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name)) {
                this.Row[name] = value;
            }
            else {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to TMFPOSIC.ID
        /// </summary>
        virtual public System.Decimal? Id {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.Id);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.Id, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DT_DATMOV
        /// </summary>
        virtual public System.DateTime? DtDatmov {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DtDatmov);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DtDatmov, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.CD_CLIENTE
        /// </summary>
        virtual public System.Decimal? CdCliente {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.CdCliente);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.CdCliente, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.CD_COMMOD
        /// </summary>
        virtual public System.String CdCommod {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CdCommod);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CdCommod, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.CD_MERCAD
        /// </summary>
        virtual public System.String CdMercad {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CdMercad);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CdMercad, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.CD_SERIE
        /// </summary>
        virtual public System.String CdSerie {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CdSerie);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CdSerie, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.QT_DIA_ANT
        /// </summary>
        virtual public System.Decimal? QtDiaAnt {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.QtDiaAnt);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.QtDiaAnt, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.QT_DIA_ATU
        /// </summary>
        virtual public System.Decimal? QtDiaAtu {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.QtDiaAtu);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.QtDiaAtu, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_DIA_ANT
        /// </summary>
        virtual public System.Decimal? VlDiaAnt {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlDiaAnt);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlDiaAnt, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_DIA_ATU
        /// </summary>
        virtual public System.Decimal? VlDiaAtu {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlDiaAtu);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlDiaAtu, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_AJUPOS
        /// </summary>
        virtual public System.Decimal? VlAjupos {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjupos);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjupos, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_AJUPER_ANT
        /// </summary>
        virtual public System.Decimal? VlAjuperAnt {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuperAnt);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuperAnt, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_AJUPER
        /// </summary>
        virtual public System.Decimal? VlAjuper {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuper);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuper, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_AJUPOS_ACU_ANT
        /// </summary>
        virtual public System.Decimal? VlAjuposAcuAnt {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuposAcuAnt);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuposAcuAnt, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VL_AJUPOS_ACU
        /// </summary>
        virtual public System.Decimal? VlAjuposAcu {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuposAcu);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.VlAjuposAcu, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_TIPO_OPC
        /// </summary>
        virtual public System.String CodTipoOpc {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CodTipoOpc);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CodTipoOpc, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VAL_COMN
        /// </summary>
        virtual public System.Decimal? ValComn {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.ValComn);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.ValComn, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_MVTO
        /// </summary>
        virtual public System.DateTime? DataMvto {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataMvto);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataMvto, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_CLI
        /// </summary>
        virtual public System.Decimal? CodCli {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.CodCli);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.CodCli, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_ISIN
        /// </summary>
        virtual public System.String CodIsin {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CodIsin);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CodIsin, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NUM_DIST
        /// </summary>
        virtual public System.Decimal? NumDist {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.NumDist);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.NumDist, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_NEG
        /// </summary>
        virtual public System.String CodNeg {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CodNeg);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CodNeg, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_LOC
        /// </summary>
        virtual public System.Decimal? CodLoc {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.CodLoc);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.CodLoc, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_CART
        /// </summary>
        virtual public System.Decimal? CodCart {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.CodCart);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.CodCart, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_PREG
        /// </summary>
        virtual public System.DateTime? DataPreg {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataPreg);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataPreg, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NUM_NEGO
        /// </summary>
        virtual public System.Decimal? NumNego {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.NumNego);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.NumNego, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VAL_NEGO
        /// </summary>
        virtual public System.Decimal? ValNego {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.ValNego);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.ValNego, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_USUA_CONP
        /// </summary>
        virtual public System.Decimal? CodUsuaConp {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.CodUsuaConp);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.CodUsuaConp, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_VENC
        /// </summary>
        virtual public System.DateTime? DataVenc {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataVenc);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataVenc, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.QTDE_LQDO
        /// </summary>
        virtual public System.Decimal? QtdeLqdo {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.QtdeLqdo);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.QtdeLqdo, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.TIPO_LIQD
        /// </summary>
        virtual public System.Decimal? TipoLiqd {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.TipoLiqd);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.TipoLiqd, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.VAL_LQDO
        /// </summary>
        virtual public System.Decimal? ValLqdo {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.ValLqdo);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.ValLqdo, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_LIQD
        /// </summary>
        virtual public System.DateTime? DataLiqd {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataLiqd);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataLiqd, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NOME_TMNL
        /// </summary>
        virtual public System.String NomeTmnl {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.NomeTmnl);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.NomeTmnl, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NOME_USUA
        /// </summary>
        virtual public System.String NomeUsua {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.NomeUsua);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.NomeUsua, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_SIST
        /// </summary>
        virtual public System.DateTime? DataSist {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataSist);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataSist, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.COD_ISIN_LQDO
        /// </summary>
        virtual public System.String CodIsinLqdo {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.CodIsinLqdo);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.CodIsinLqdo, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NUM_DIST_LQDO
        /// </summary>
        virtual public System.Decimal? NumDistLqdo {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.NumDistLqdo);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.NumDistLqdo, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.IND_INTR_EFTV
        /// </summary>
        virtual public System.String IndIntrEftv {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.IndIntrEftv);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.IndIntrEftv, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NUM_COTR
        /// </summary>
        virtual public System.Decimal? NumCotr {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.NumCotr);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.NumCotr, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DIG_COTR
        /// </summary>
        virtual public System.Decimal? DigCotr {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.DigCotr);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.DigCotr, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.IND_INTR_EXEC
        /// </summary>
        virtual public System.String IndIntrExec {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.IndIntrExec);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.IndIntrExec, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NOME_USUA_INTR
        /// </summary>
        virtual public System.String NomeUsuaIntr {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.NomeUsuaIntr);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.NomeUsuaIntr, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.NOME_TMNL_INTR
        /// </summary>
        virtual public System.String NomeTmnlIntr {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.NomeTmnlIntr);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.NomeTmnlIntr, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.DATA_SIST_INTR
        /// </summary>
        virtual public System.DateTime? DataSistIntr {
            get {
                return base.GetSystemDateTime(TmfposicMetadata.ColumnNames.DataSistIntr);
            }

            set {
                base.SetSystemDateTime(TmfposicMetadata.ColumnNames.DataSistIntr, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.IND_LIBE
        /// </summary>
        virtual public System.String IndLibe {
            get {
                return base.GetSystemString(TmfposicMetadata.ColumnNames.IndLibe);
            }

            set {
                base.SetSystemString(TmfposicMetadata.ColumnNames.IndLibe, value);
            }
        }

        /// <summary>
        /// Maps to TMFPOSIC.PR_MEDIO_ATU
        /// </summary>
        virtual public System.Decimal? PrMedioAtu {
            get {
                return base.GetSystemDecimal(TmfposicMetadata.ColumnNames.PrMedioAtu);
            }

            set {
                base.SetSystemDecimal(TmfposicMetadata.ColumnNames.PrMedioAtu, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str {
            get {
                if (esstrings == null) {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings {
            public esStrings(esTmfposic entity) {
                this.entity = entity;
            }


            public System.String Id {
                get {
                    System.Decimal? data = entity.Id;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.Id = null;
                    else entity.Id = Convert.ToDecimal(value);
                }
            }

            public System.String DtDatmov {
                get {
                    System.DateTime? data = entity.DtDatmov;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DtDatmov = null;
                    else entity.DtDatmov = Convert.ToDateTime(value);
                }
            }

            public System.String CdCliente {
                get {
                    System.Decimal? data = entity.CdCliente;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CdCliente = null;
                    else entity.CdCliente = Convert.ToDecimal(value);
                }
            }

            public System.String CdCommod {
                get {
                    System.String data = entity.CdCommod;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CdCommod = null;
                    else entity.CdCommod = Convert.ToString(value);
                }
            }

            public System.String CdMercad {
                get {
                    System.String data = entity.CdMercad;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CdMercad = null;
                    else entity.CdMercad = Convert.ToString(value);
                }
            }

            public System.String CdSerie {
                get {
                    System.String data = entity.CdSerie;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CdSerie = null;
                    else entity.CdSerie = Convert.ToString(value);
                }
            }

            public System.String QtDiaAnt {
                get {
                    System.Decimal? data = entity.QtDiaAnt;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtDiaAnt = null;
                    else entity.QtDiaAnt = Convert.ToDecimal(value);
                }
            }

            public System.String QtDiaAtu {
                get {
                    System.Decimal? data = entity.QtDiaAtu;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtDiaAtu = null;
                    else entity.QtDiaAtu = Convert.ToDecimal(value);
                }
            }

            public System.String VlDiaAnt {
                get {
                    System.Decimal? data = entity.VlDiaAnt;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlDiaAnt = null;
                    else entity.VlDiaAnt = Convert.ToDecimal(value);
                }
            }

            public System.String VlDiaAtu {
                get {
                    System.Decimal? data = entity.VlDiaAtu;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlDiaAtu = null;
                    else entity.VlDiaAtu = Convert.ToDecimal(value);
                }
            }

            public System.String VlAjupos {
                get {
                    System.Decimal? data = entity.VlAjupos;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlAjupos = null;
                    else entity.VlAjupos = Convert.ToDecimal(value);
                }
            }

            public System.String VlAjuperAnt {
                get {
                    System.Decimal? data = entity.VlAjuperAnt;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlAjuperAnt = null;
                    else entity.VlAjuperAnt = Convert.ToDecimal(value);
                }
            }

            public System.String VlAjuper {
                get {
                    System.Decimal? data = entity.VlAjuper;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlAjuper = null;
                    else entity.VlAjuper = Convert.ToDecimal(value);
                }
            }

            public System.String VlAjuposAcuAnt {
                get {
                    System.Decimal? data = entity.VlAjuposAcuAnt;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlAjuposAcuAnt = null;
                    else entity.VlAjuposAcuAnt = Convert.ToDecimal(value);
                }
            }

            public System.String VlAjuposAcu {
                get {
                    System.Decimal? data = entity.VlAjuposAcu;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.VlAjuposAcu = null;
                    else entity.VlAjuposAcu = Convert.ToDecimal(value);
                }
            }

            public System.String CodTipoOpc {
                get {
                    System.String data = entity.CodTipoOpc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodTipoOpc = null;
                    else entity.CodTipoOpc = Convert.ToString(value);
                }
            }

            public System.String ValComn {
                get {
                    System.Decimal? data = entity.ValComn;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.ValComn = null;
                    else entity.ValComn = Convert.ToDecimal(value);
                }
            }

            public System.String DataMvto {
                get {
                    System.DateTime? data = entity.DataMvto;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataMvto = null;
                    else entity.DataMvto = Convert.ToDateTime(value);
                }
            }

            public System.String CodCli {
                get {
                    System.Decimal? data = entity.CodCli;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodCli = null;
                    else entity.CodCli = Convert.ToDecimal(value);
                }
            }

            public System.String CodIsin {
                get {
                    System.String data = entity.CodIsin;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodIsin = null;
                    else entity.CodIsin = Convert.ToString(value);
                }
            }

            public System.String NumDist {
                get {
                    System.Decimal? data = entity.NumDist;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NumDist = null;
                    else entity.NumDist = Convert.ToDecimal(value);
                }
            }

            public System.String CodNeg {
                get {
                    System.String data = entity.CodNeg;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodNeg = null;
                    else entity.CodNeg = Convert.ToString(value);
                }
            }

            public System.String CodLoc {
                get {
                    System.Decimal? data = entity.CodLoc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodLoc = null;
                    else entity.CodLoc = Convert.ToDecimal(value);
                }
            }

            public System.String CodCart {
                get {
                    System.Decimal? data = entity.CodCart;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodCart = null;
                    else entity.CodCart = Convert.ToDecimal(value);
                }
            }

            public System.String DataPreg {
                get {
                    System.DateTime? data = entity.DataPreg;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataPreg = null;
                    else entity.DataPreg = Convert.ToDateTime(value);
                }
            }

            public System.String NumNego {
                get {
                    System.Decimal? data = entity.NumNego;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NumNego = null;
                    else entity.NumNego = Convert.ToDecimal(value);
                }
            }

            public System.String ValNego {
                get {
                    System.Decimal? data = entity.ValNego;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.ValNego = null;
                    else entity.ValNego = Convert.ToDecimal(value);
                }
            }

            public System.String CodUsuaConp {
                get {
                    System.Decimal? data = entity.CodUsuaConp;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodUsuaConp = null;
                    else entity.CodUsuaConp = Convert.ToDecimal(value);
                }
            }

            public System.String DataVenc {
                get {
                    System.DateTime? data = entity.DataVenc;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataVenc = null;
                    else entity.DataVenc = Convert.ToDateTime(value);
                }
            }

            public System.String QtdeLqdo {
                get {
                    System.Decimal? data = entity.QtdeLqdo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.QtdeLqdo = null;
                    else entity.QtdeLqdo = Convert.ToDecimal(value);
                }
            }

            public System.String TipoLiqd {
                get {
                    System.Decimal? data = entity.TipoLiqd;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.TipoLiqd = null;
                    else entity.TipoLiqd = Convert.ToDecimal(value);
                }
            }

            public System.String ValLqdo {
                get {
                    System.Decimal? data = entity.ValLqdo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.ValLqdo = null;
                    else entity.ValLqdo = Convert.ToDecimal(value);
                }
            }

            public System.String DataLiqd {
                get {
                    System.DateTime? data = entity.DataLiqd;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataLiqd = null;
                    else entity.DataLiqd = Convert.ToDateTime(value);
                }
            }

            public System.String NomeTmnl {
                get {
                    System.String data = entity.NomeTmnl;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NomeTmnl = null;
                    else entity.NomeTmnl = Convert.ToString(value);
                }
            }

            public System.String NomeUsua {
                get {
                    System.String data = entity.NomeUsua;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NomeUsua = null;
                    else entity.NomeUsua = Convert.ToString(value);
                }
            }

            public System.String DataSist {
                get {
                    System.DateTime? data = entity.DataSist;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataSist = null;
                    else entity.DataSist = Convert.ToDateTime(value);
                }
            }

            public System.String CodIsinLqdo {
                get {
                    System.String data = entity.CodIsinLqdo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.CodIsinLqdo = null;
                    else entity.CodIsinLqdo = Convert.ToString(value);
                }
            }

            public System.String NumDistLqdo {
                get {
                    System.Decimal? data = entity.NumDistLqdo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NumDistLqdo = null;
                    else entity.NumDistLqdo = Convert.ToDecimal(value);
                }
            }

            public System.String IndIntrEftv {
                get {
                    System.String data = entity.IndIntrEftv;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.IndIntrEftv = null;
                    else entity.IndIntrEftv = Convert.ToString(value);
                }
            }

            public System.String NumCotr {
                get {
                    System.Decimal? data = entity.NumCotr;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NumCotr = null;
                    else entity.NumCotr = Convert.ToDecimal(value);
                }
            }

            public System.String DigCotr {
                get {
                    System.Decimal? data = entity.DigCotr;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DigCotr = null;
                    else entity.DigCotr = Convert.ToDecimal(value);
                }
            }

            public System.String IndIntrExec {
                get {
                    System.String data = entity.IndIntrExec;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.IndIntrExec = null;
                    else entity.IndIntrExec = Convert.ToString(value);
                }
            }

            public System.String NomeUsuaIntr {
                get {
                    System.String data = entity.NomeUsuaIntr;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NomeUsuaIntr = null;
                    else entity.NomeUsuaIntr = Convert.ToString(value);
                }
            }

            public System.String NomeTmnlIntr {
                get {
                    System.String data = entity.NomeTmnlIntr;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.NomeTmnlIntr = null;
                    else entity.NomeTmnlIntr = Convert.ToString(value);
                }
            }

            public System.String DataSistIntr {
                get {
                    System.DateTime? data = entity.DataSistIntr;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.DataSistIntr = null;
                    else entity.DataSistIntr = Convert.ToDateTime(value);
                }
            }

            public System.String IndLibe {
                get {
                    System.String data = entity.IndLibe;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.IndLibe = null;
                    else entity.IndLibe = Convert.ToString(value);
                }
            }

            public System.String PrMedioAtu {
                get {
                    System.Decimal? data = entity.PrMedioAtu;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set {
                    if (value == null || value.Length == 0) entity.PrMedioAtu = null;
                    else entity.PrMedioAtu = Convert.ToDecimal(value);
                }
            }


            private esTmfposic entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esTmfposicQuery query) {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table) {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1) {
                throw new Exception("esTmfposic can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class Tmfposic : esTmfposic {


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties() {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys() {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys() {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys() {
        }

    }



    [Serializable]
    abstract public class esTmfposicQuery : esDynamicQuery {
        override protected IMetadata Meta {
            get {
                return TmfposicMetadata.Meta();
            }
        }


        public esQueryItem Id {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.Id, esSystemType.Decimal);
            }
        }

        public esQueryItem DtDatmov {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DtDatmov, esSystemType.DateTime);
            }
        }

        public esQueryItem CdCliente {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
            }
        }

        public esQueryItem CdCommod {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CdCommod, esSystemType.String);
            }
        }

        public esQueryItem CdMercad {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CdMercad, esSystemType.String);
            }
        }

        public esQueryItem CdSerie {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CdSerie, esSystemType.String);
            }
        }

        public esQueryItem QtDiaAnt {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.QtDiaAnt, esSystemType.Decimal);
            }
        }

        public esQueryItem QtDiaAtu {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.QtDiaAtu, esSystemType.Decimal);
            }
        }

        public esQueryItem VlDiaAnt {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlDiaAnt, esSystemType.Decimal);
            }
        }

        public esQueryItem VlDiaAtu {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlDiaAtu, esSystemType.Decimal);
            }
        }

        public esQueryItem VlAjupos {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlAjupos, esSystemType.Decimal);
            }
        }

        public esQueryItem VlAjuperAnt {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlAjuperAnt, esSystemType.Decimal);
            }
        }

        public esQueryItem VlAjuper {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlAjuper, esSystemType.Decimal);
            }
        }

        public esQueryItem VlAjuposAcuAnt {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlAjuposAcuAnt, esSystemType.Decimal);
            }
        }

        public esQueryItem VlAjuposAcu {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.VlAjuposAcu, esSystemType.Decimal);
            }
        }

        public esQueryItem CodTipoOpc {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodTipoOpc, esSystemType.String);
            }
        }

        public esQueryItem ValComn {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.ValComn, esSystemType.Decimal);
            }
        }

        public esQueryItem DataMvto {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataMvto, esSystemType.DateTime);
            }
        }

        public esQueryItem CodCli {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodCli, esSystemType.Decimal);
            }
        }

        public esQueryItem CodIsin {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodIsin, esSystemType.String);
            }
        }

        public esQueryItem NumDist {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NumDist, esSystemType.Decimal);
            }
        }

        public esQueryItem CodNeg {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodNeg, esSystemType.String);
            }
        }

        public esQueryItem CodLoc {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodLoc, esSystemType.Decimal);
            }
        }

        public esQueryItem CodCart {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodCart, esSystemType.Decimal);
            }
        }

        public esQueryItem DataPreg {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataPreg, esSystemType.DateTime);
            }
        }

        public esQueryItem NumNego {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NumNego, esSystemType.Decimal);
            }
        }

        public esQueryItem ValNego {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.ValNego, esSystemType.Decimal);
            }
        }

        public esQueryItem CodUsuaConp {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodUsuaConp, esSystemType.Decimal);
            }
        }

        public esQueryItem DataVenc {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
            }
        }

        public esQueryItem QtdeLqdo {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.QtdeLqdo, esSystemType.Decimal);
            }
        }

        public esQueryItem TipoLiqd {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.TipoLiqd, esSystemType.Decimal);
            }
        }

        public esQueryItem ValLqdo {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.ValLqdo, esSystemType.Decimal);
            }
        }

        public esQueryItem DataLiqd {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataLiqd, esSystemType.DateTime);
            }
        }

        public esQueryItem NomeTmnl {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NomeTmnl, esSystemType.String);
            }
        }

        public esQueryItem NomeUsua {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NomeUsua, esSystemType.String);
            }
        }

        public esQueryItem DataSist {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataSist, esSystemType.DateTime);
            }
        }

        public esQueryItem CodIsinLqdo {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.CodIsinLqdo, esSystemType.String);
            }
        }

        public esQueryItem NumDistLqdo {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NumDistLqdo, esSystemType.Decimal);
            }
        }

        public esQueryItem IndIntrEftv {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.IndIntrEftv, esSystemType.String);
            }
        }

        public esQueryItem NumCotr {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
            }
        }

        public esQueryItem DigCotr {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DigCotr, esSystemType.Decimal);
            }
        }

        public esQueryItem IndIntrExec {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.IndIntrExec, esSystemType.String);
            }
        }

        public esQueryItem NomeUsuaIntr {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NomeUsuaIntr, esSystemType.String);
            }
        }

        public esQueryItem NomeTmnlIntr {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.NomeTmnlIntr, esSystemType.String);
            }
        }

        public esQueryItem DataSistIntr {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.DataSistIntr, esSystemType.DateTime);
            }
        }

        public esQueryItem IndLibe {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.IndLibe, esSystemType.String);
            }
        }

        public esQueryItem PrMedioAtu {
            get {
                return new esQueryItem(this, TmfposicMetadata.ColumnNames.PrMedioAtu, esSystemType.Decimal);
            }
        }

    }



    [Serializable]
    [XmlType("TmfposicCollection")]
    public partial class TmfposicCollection : esTmfposicCollection, IEnumerable<Tmfposic> {
        public TmfposicCollection() {

        }

        public static implicit operator List<Tmfposic>(TmfposicCollection coll) {
            List<Tmfposic> list = new List<Tmfposic>();

            foreach (Tmfposic emp in coll) {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta {
            get {
                return TmfposicMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery() {
            if (this.query == null) {
                this.query = new TmfposicQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row) {
            return new Tmfposic(row);
        }

        override protected esEntity CreateEntity() {
            return new Tmfposic();
        }


        #endregion


        [BrowsableAttribute(false)]
        public TmfposicQuery Query {
            get {
                if (this.query == null) {
                    this.query = new TmfposicQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset() {
            this.query = null;
        }

        public bool Load(TmfposicQuery query) {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public Tmfposic AddNew() {
            Tmfposic entity = base.AddNewEntity() as Tmfposic;

            return entity;
        }

        public Tmfposic FindByPrimaryKey(System.Decimal id) {
            return base.FindByPrimaryKey(id) as Tmfposic;
        }


        #region IEnumerable<Tmfposic> Members

        IEnumerator<Tmfposic> IEnumerable<Tmfposic>.GetEnumerator() {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext()) {
                yield return iterator.Current as Tmfposic;
            }
        }

        #endregion

        private TmfposicQuery query;
    }


    /// <summary>
    /// Encapsulates the 'TMFPOSIC' table
    /// </summary>

    [Serializable]
    public partial class Tmfposic : esTmfposic {
        public Tmfposic() {

        }

        public Tmfposic(DataRow row)
            : base(row) {

        }

        #region Housekeeping methods
        override protected IMetadata Meta {
            get {
                return TmfposicMetadata.Meta();
            }
        }



        override protected esTmfposicQuery GetDynamicQuery() {
            if (this.query == null) {
                this.query = new TmfposicQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public TmfposicQuery Query {
            get {
                if (this.query == null) {
                    this.query = new TmfposicQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset() {
            this.query = null;
        }


        public bool Load(TmfposicQuery query) {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private TmfposicQuery query;
    }



    [Serializable]
    public partial class TmfposicQuery : esTmfposicQuery {
        public TmfposicQuery() {

        }

        public TmfposicQuery(string joinAlias) {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class TmfposicMetadata : esMetadata, IMetadata {
        #region Protected Constructor
        protected TmfposicMetadata() {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.Id;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 20;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DtDatmov, 1, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DtDatmov;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CdCliente, 2, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.CdCliente;
            c.NumericPrecision = 7;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CdCommod, 3, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CdCommod;
            c.CharacterMaxLength = 3;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CdMercad, 4, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CdMercad;
            c.CharacterMaxLength = 3;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CdSerie, 5, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CdSerie;
            c.CharacterMaxLength = 4;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.QtDiaAnt, 6, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.QtDiaAnt;
            c.NumericPrecision = 7;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.QtDiaAtu, 7, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.QtDiaAtu;
            c.NumericPrecision = 7;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlDiaAnt, 8, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlDiaAnt;
            c.NumericPrecision = 15;
            c.NumericScale = 3;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlDiaAtu, 9, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlDiaAtu;
            c.NumericPrecision = 15;
            c.NumericScale = 3;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlAjupos, 10, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlAjupos;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlAjuperAnt, 11, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlAjuperAnt;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlAjuper, 12, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlAjuper;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlAjuposAcuAnt, 13, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlAjuposAcuAnt;
            c.NumericPrecision = 18;
            c.NumericScale = 2;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.VlAjuposAcu, 14, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.VlAjuposAcu;
            c.NumericPrecision = 18;
            c.NumericScale = 2;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodTipoOpc, 15, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodTipoOpc;
            c.CharacterMaxLength = 4;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.ValComn, 16, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.ValComn;
            c.NumericPrecision = 20;
            c.NumericScale = 2;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataMvto, 17, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataMvto;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodCli, 18, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodCli;
            c.NumericPrecision = 7;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodIsin, 19, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodIsin;
            c.CharacterMaxLength = 12;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NumDist, 20, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.NumDist;
            c.NumericPrecision = 3;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodNeg, 21, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodNeg;
            c.CharacterMaxLength = 12;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodLoc, 22, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodLoc;
            c.NumericPrecision = 7;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodCart, 23, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodCart;
            c.NumericPrecision = 5;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataPreg, 24, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataPreg;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NumNego, 25, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.NumNego;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.ValNego, 26, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.ValNego;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodUsuaConp, 27, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodUsuaConp;
            c.NumericPrecision = 38;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataVenc, 28, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataVenc;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.QtdeLqdo, 29, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.QtdeLqdo;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.TipoLiqd, 30, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.TipoLiqd;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.ValLqdo, 31, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.ValLqdo;
            c.NumericPrecision = 38;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataLiqd, 32, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataLiqd;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NomeTmnl, 33, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.NomeTmnl;
            c.CharacterMaxLength = 15;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NomeUsua, 34, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.NomeUsua;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataSist, 35, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataSist;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.CodIsinLqdo, 36, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.CodIsinLqdo;
            c.CharacterMaxLength = 12;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NumDistLqdo, 37, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.NumDistLqdo;
            c.NumericPrecision = 3;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.IndIntrEftv, 38, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.IndIntrEftv;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NumCotr, 39, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.NumCotr;
            c.NumericPrecision = 9;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DigCotr, 40, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.DigCotr;
            c.NumericPrecision = 1;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.IndIntrExec, 41, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.IndIntrExec;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NomeUsuaIntr, 42, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.NomeUsuaIntr;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.NomeTmnlIntr, 43, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.NomeTmnlIntr;
            c.CharacterMaxLength = 15;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.DataSistIntr, 44, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TmfposicMetadata.PropertyNames.DataSistIntr;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.IndLibe, 45, typeof(System.String), esSystemType.String);
            c.PropertyName = TmfposicMetadata.PropertyNames.IndLibe;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TmfposicMetadata.ColumnNames.PrMedioAtu, 46, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TmfposicMetadata.PropertyNames.PrMedioAtu;
            c.NumericPrecision = 16;
            c.NumericScale = 6;
            c.IsNullable = true;
            _columns.Add(c);


        }
        #endregion

        static public TmfposicMetadata Meta() {
            return meta;
        }

        public Guid DataID {
            get { return base._dataID; }
        }

        public bool MultiProviderMode {
            get { return false; }
        }

        public esColumnMetadataCollection Columns {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames {
            public const string Id = "ID";
            public const string DtDatmov = "DT_DATMOV";
            public const string CdCliente = "CD_CLIENTE";
            public const string CdCommod = "CD_COMMOD";
            public const string CdMercad = "CD_MERCAD";
            public const string CdSerie = "CD_SERIE";
            public const string QtDiaAnt = "QT_DIA_ANT";
            public const string QtDiaAtu = "QT_DIA_ATU";
            public const string VlDiaAnt = "VL_DIA_ANT";
            public const string VlDiaAtu = "VL_DIA_ATU";
            public const string VlAjupos = "VL_AJUPOS";
            public const string VlAjuperAnt = "VL_AJUPER_ANT";
            public const string VlAjuper = "VL_AJUPER";
            public const string VlAjuposAcuAnt = "VL_AJUPOS_ACU_ANT";
            public const string VlAjuposAcu = "VL_AJUPOS_ACU";
            public const string CodTipoOpc = "COD_TIPO_OPC";
            public const string ValComn = "VAL_COMN";
            public const string DataMvto = "DATA_MVTO";
            public const string CodCli = "COD_CLI";
            public const string CodIsin = "COD_ISIN";
            public const string NumDist = "NUM_DIST";
            public const string CodNeg = "COD_NEG";
            public const string CodLoc = "COD_LOC";
            public const string CodCart = "COD_CART";
            public const string DataPreg = "DATA_PREG";
            public const string NumNego = "NUM_NEGO";
            public const string ValNego = "VAL_NEGO";
            public const string CodUsuaConp = "COD_USUA_CONP";
            public const string DataVenc = "DATA_VENC";
            public const string QtdeLqdo = "QTDE_LQDO";
            public const string TipoLiqd = "TIPO_LIQD";
            public const string ValLqdo = "VAL_LQDO";
            public const string DataLiqd = "DATA_LIQD";
            public const string NomeTmnl = "NOME_TMNL";
            public const string NomeUsua = "NOME_USUA";
            public const string DataSist = "DATA_SIST";
            public const string CodIsinLqdo = "COD_ISIN_LQDO";
            public const string NumDistLqdo = "NUM_DIST_LQDO";
            public const string IndIntrEftv = "IND_INTR_EFTV";
            public const string NumCotr = "NUM_COTR";
            public const string DigCotr = "DIG_COTR";
            public const string IndIntrExec = "IND_INTR_EXEC";
            public const string NomeUsuaIntr = "NOME_USUA_INTR";
            public const string NomeTmnlIntr = "NOME_TMNL_INTR";
            public const string DataSistIntr = "DATA_SIST_INTR";
            public const string IndLibe = "IND_LIBE";
            public const string PrMedioAtu = "PR_MEDIO_ATU";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames {
            public const string Id = "Id";
            public const string DtDatmov = "DtDatmov";
            public const string CdCliente = "CdCliente";
            public const string CdCommod = "CdCommod";
            public const string CdMercad = "CdMercad";
            public const string CdSerie = "CdSerie";
            public const string QtDiaAnt = "QtDiaAnt";
            public const string QtDiaAtu = "QtDiaAtu";
            public const string VlDiaAnt = "VlDiaAnt";
            public const string VlDiaAtu = "VlDiaAtu";
            public const string VlAjupos = "VlAjupos";
            public const string VlAjuperAnt = "VlAjuperAnt";
            public const string VlAjuper = "VlAjuper";
            public const string VlAjuposAcuAnt = "VlAjuposAcuAnt";
            public const string VlAjuposAcu = "VlAjuposAcu";
            public const string CodTipoOpc = "CodTipoOpc";
            public const string ValComn = "ValComn";
            public const string DataMvto = "DataMvto";
            public const string CodCli = "CodCli";
            public const string CodIsin = "CodIsin";
            public const string NumDist = "NumDist";
            public const string CodNeg = "CodNeg";
            public const string CodLoc = "CodLoc";
            public const string CodCart = "CodCart";
            public const string DataPreg = "DataPreg";
            public const string NumNego = "NumNego";
            public const string ValNego = "ValNego";
            public const string CodUsuaConp = "CodUsuaConp";
            public const string DataVenc = "DataVenc";
            public const string QtdeLqdo = "QtdeLqdo";
            public const string TipoLiqd = "TipoLiqd";
            public const string ValLqdo = "ValLqdo";
            public const string DataLiqd = "DataLiqd";
            public const string NomeTmnl = "NomeTmnl";
            public const string NomeUsua = "NomeUsua";
            public const string DataSist = "DataSist";
            public const string CodIsinLqdo = "CodIsinLqdo";
            public const string NumDistLqdo = "NumDistLqdo";
            public const string IndIntrEftv = "IndIntrEftv";
            public const string NumCotr = "NumCotr";
            public const string DigCotr = "DigCotr";
            public const string IndIntrExec = "IndIntrExec";
            public const string NomeUsuaIntr = "NomeUsuaIntr";
            public const string NomeTmnlIntr = "NomeTmnlIntr";
            public const string DataSistIntr = "DataSistIntr";
            public const string IndLibe = "IndLibe";
            public const string PrMedioAtu = "PrMedioAtu";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName) {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault() {
            // This is only executed once per the life of the application
            lock (typeof(TmfposicMetadata)) {
                if (TmfposicMetadata.mapDelegates == null) {
                    TmfposicMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (TmfposicMetadata.meta == null) {
                    TmfposicMetadata.meta = new TmfposicMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName) {
            if (!_providerMetadataMaps.ContainsKey(mapName)) {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DT_DATMOV", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("CD_COMMOD", new esTypeMap("CHAR", "System.String"));
                meta.AddTypeMap("CD_MERCAD", new esTypeMap("CHAR", "System.String"));
                meta.AddTypeMap("CD_SERIE", new esTypeMap("CHAR", "System.String"));
                meta.AddTypeMap("QT_DIA_ANT", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("QT_DIA_ATU", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_DIA_ANT", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_DIA_ATU", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_AJUPOS", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_AJUPER_ANT", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_AJUPER", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_AJUPOS_ACU_ANT", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VL_AJUPOS_ACU", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_TIPO_OPC", new esTypeMap("CHAR", "System.String"));
                meta.AddTypeMap("VAL_COMN", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DATA_MVTO", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_ISIN", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NUM_DIST", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("COD_LOC", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_CART", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DATA_PREG", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("NUM_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VAL_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("COD_USUA_CONP", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("QTDE_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("TIPO_LIQD", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("VAL_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DATA_LIQD", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("NOME_TMNL", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NOME_USUA", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("DATA_SIST", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("COD_ISIN_LQDO", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NUM_DIST_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("IND_INTR_EFTV", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("DIG_COTR", new esTypeMap("NUMBER", "System.Decimal"));
                meta.AddTypeMap("IND_INTR_EXEC", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NOME_USUA_INTR", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("NOME_TMNL_INTR", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("DATA_SIST_INTR", new esTypeMap("DATE", "System.DateTime"));
                meta.AddTypeMap("IND_LIBE", new esTypeMap("VARCHAR2", "System.String"));
                meta.AddTypeMap("PR_MEDIO_ATU", new esTypeMap("NUMBER", "System.Decimal"));



                meta.Source = "TMFPOSIC";
                meta.Destination = "TMFPOSIC";

                meta.spInsert = "proc_TMFPOSICInsert";
                meta.spUpdate = "proc_TMFPOSICUpdate";
                meta.spDelete = "proc_TMFPOSICDelete";
                meta.spLoadAll = "proc_TMFPOSICLoadAll";
                meta.spLoadByPrimaryKey = "proc_TMFPOSICLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private TmfposicMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
