/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 24/09/2012 14:05:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTcfposiTermCollection : esEntityCollection
	{
		public esTcfposiTermCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TcfposiTermCollection";
		}

		#region Query Logic
		protected void InitQuery(esTcfposiTermQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTcfposiTermQuery);
		}
		#endregion
		
		virtual public TcfposiTerm DetachEntity(TcfposiTerm entity)
		{
			return base.DetachEntity(entity) as TcfposiTerm;
		}
		
		virtual public TcfposiTerm AttachEntity(TcfposiTerm entity)
		{
			return base.AttachEntity(entity) as TcfposiTerm;
		}
		
		virtual public void Combine(TcfposiTermCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TcfposiTerm this[int index]
		{
			get
			{
				return base[index] as TcfposiTerm;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TcfposiTerm);
		}
	}



	[Serializable]
	abstract public class esTcfposiTerm : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTcfposiTermQuery GetDynamicQuery()
		{
			return null;
		}

		public esTcfposiTerm()
		{

		}

		public esTcfposiTerm(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTcfposiTermQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTcfposiTermQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DataAtu": this.str.DataAtu = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "CodIsin": this.str.CodIsin = (string)value; break;							
						case "NumDist": this.str.NumDist = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "CodLoc": this.str.CodLoc = (string)value; break;							
						case "CodCart": this.str.CodCart = (string)value; break;							
						case "DataPreg": this.str.DataPreg = (string)value; break;							
						case "NumNego": this.str.NumNego = (string)value; break;							
						case "DataVenc": this.str.DataVenc = (string)value; break;							
						case "QtdeDisp": this.str.QtdeDisp = (string)value; break;							
						case "ValNego": this.str.ValNego = (string)value; break;							
						case "ValCotr": this.str.ValCotr = (string)value; break;							
						case "CodUsuaConp": this.str.CodUsuaConp = (string)value; break;							
						case "DataUltMoviCotr": this.str.DataUltMoviCotr = (string)value; break;							
						case "CodAgenQlfd": this.str.CodAgenQlfd = (string)value; break;							
						case "CodCliQlfd": this.str.CodCliQlfd = (string)value; break;							
						case "PrecLqdo": this.str.PrecLqdo = (string)value; break;							
						case "PrecBrut": this.str.PrecBrut = (string)value; break;							
						case "DataUltMvto": this.str.DataUltMvto = (string)value; break;							
						case "TipoLiqd": this.str.TipoLiqd = (string)value; break;							
						case "CodIsinAtu": this.str.CodIsinAtu = (string)value; break;							
						case "NumDistAtu": this.str.NumDistAtu = (string)value; break;							
						case "ValFinSlas": this.str.ValFinSlas = (string)value; break;							
						case "QtdeOri": this.str.QtdeOri = (string)value; break;							
						case "NumCotr": this.str.NumCotr = (string)value; break;							
						case "ValDol": this.str.ValDol = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DataAtu":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtu = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "NumDist":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumDist = (System.Decimal?)value;
							break;
						
						case "CodLoc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodLoc = (System.Decimal?)value;
							break;
						
						case "CodCart":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCart = (System.Decimal?)value;
							break;
						
						case "DataPreg":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPreg = (System.DateTime?)value;
							break;
						
						case "NumNego":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumNego = (System.Decimal?)value;
							break;
						
						case "DataVenc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVenc = (System.DateTime?)value;
							break;
						
						case "QtdeDisp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeDisp = (System.Decimal?)value;
							break;
						
						case "ValNego":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValNego = (System.Decimal?)value;
							break;
						
						case "ValCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValCotr = (System.Decimal?)value;
							break;
						
						case "CodUsuaConp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodUsuaConp = (System.Decimal?)value;
							break;
						
						case "DataUltMoviCotr":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltMoviCotr = (System.DateTime?)value;
							break;
						
						case "CodAgenQlfd":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodAgenQlfd = (System.Decimal?)value;
							break;
						
						case "CodCliQlfd":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCliQlfd = (System.Decimal?)value;
							break;
						
						case "PrecLqdo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecLqdo = (System.Decimal?)value;
							break;
						
						case "PrecBrut":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecBrut = (System.Decimal?)value;
							break;
						
						case "DataUltMvto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltMvto = (System.DateTime?)value;
							break;
						
						case "TipoLiqd":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoLiqd = (System.Decimal?)value;
							break;
						
						case "NumDistAtu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumDistAtu = (System.Decimal?)value;
							break;
						
						case "ValFinSlas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValFinSlas = (System.Decimal?)value;
							break;
						
						case "QtdeOri":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeOri = (System.Decimal?)value;
							break;
						
						case "NumCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotr = (System.Decimal?)value;
							break;
						
						case "ValDol":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValDol = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.DATA_ATU
		/// </summary>
		virtual public System.DateTime? DataAtu
		{
			get
			{
				return base.GetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataAtu);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataAtu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_ISIN
		/// </summary>
		virtual public System.String CodIsin
		{
			get
			{
				return base.GetSystemString(TcfposiTermMetadata.ColumnNames.CodIsin);
			}
			
			set
			{
				base.SetSystemString(TcfposiTermMetadata.ColumnNames.CodIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.NUM_DIST
		/// </summary>
		virtual public System.Decimal? NumDist
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumDist);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumDist, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(TcfposiTermMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(TcfposiTermMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_LOC
		/// </summary>
		virtual public System.Decimal? CodLoc
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodLoc);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodLoc, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_CART
		/// </summary>
		virtual public System.Decimal? CodCart
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCart);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCart, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.DATA_PREG
		/// </summary>
		virtual public System.DateTime? DataPreg
		{
			get
			{
				return base.GetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataPreg);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataPreg, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.NUM_NEGO
		/// </summary>
		virtual public System.Decimal? NumNego
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumNego);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumNego, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.DATA_VENC
		/// </summary>
		virtual public System.DateTime? DataVenc
		{
			get
			{
				return base.GetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataVenc);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataVenc, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.QTDE_DISP
		/// </summary>
		virtual public System.Decimal? QtdeDisp
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.QtdeDisp);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.QtdeDisp, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.VAL_NEGO
		/// </summary>
		virtual public System.Decimal? ValNego
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValNego);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValNego, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.VAL_COTR
		/// </summary>
		virtual public System.Decimal? ValCotr
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValCotr);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_USUA_CONP
		/// </summary>
		virtual public System.Decimal? CodUsuaConp
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodUsuaConp);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodUsuaConp, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.DATA_ULT_MOVI_COTR
		/// </summary>
		virtual public System.DateTime? DataUltMoviCotr
		{
			get
			{
				return base.GetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataUltMoviCotr);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataUltMoviCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_AGEN_QLFD
		/// </summary>
		virtual public System.Decimal? CodAgenQlfd
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodAgenQlfd);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodAgenQlfd, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_CLI_QLFD
		/// </summary>
		virtual public System.Decimal? CodCliQlfd
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCliQlfd);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.CodCliQlfd, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.PREC_LQDO
		/// </summary>
		virtual public System.Decimal? PrecLqdo
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.PrecLqdo);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.PrecLqdo, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.PREC_BRUT
		/// </summary>
		virtual public System.Decimal? PrecBrut
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.PrecBrut);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.PrecBrut, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.DATA_ULT_MVTO
		/// </summary>
		virtual public System.DateTime? DataUltMvto
		{
			get
			{
				return base.GetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataUltMvto);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiTermMetadata.ColumnNames.DataUltMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.TIPO_LIQD
		/// </summary>
		virtual public System.Decimal? TipoLiqd
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.TipoLiqd);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.TipoLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.COD_ISIN_ATU
		/// </summary>
		virtual public System.String CodIsinAtu
		{
			get
			{
				return base.GetSystemString(TcfposiTermMetadata.ColumnNames.CodIsinAtu);
			}
			
			set
			{
				base.SetSystemString(TcfposiTermMetadata.ColumnNames.CodIsinAtu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.NUM_DIST_ATU
		/// </summary>
		virtual public System.Decimal? NumDistAtu
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumDistAtu);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumDistAtu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.VAL_FIN_SLAS
		/// </summary>
		virtual public System.Decimal? ValFinSlas
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValFinSlas);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValFinSlas, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.QTDE_ORI
		/// </summary>
		virtual public System.Decimal? QtdeOri
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.QtdeOri);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.QtdeOri, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.NUM_COTR
		/// </summary>
		virtual public System.Decimal? NumCotr
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumCotr);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.NumCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_TERM.VAL_DOL
		/// </summary>
		virtual public System.Decimal? ValDol
		{
			get
			{
				return base.GetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValDol);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiTermMetadata.ColumnNames.ValDol, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTcfposiTerm entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAtu
			{
				get
				{
					System.DateTime? data = entity.DataAtu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtu = null;
					else entity.DataAtu = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodIsin
			{
				get
				{
					System.String data = entity.CodIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsin = null;
					else entity.CodIsin = Convert.ToString(value);
				}
			}
				
			public System.String NumDist
			{
				get
				{
					System.Decimal? data = entity.NumDist;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDist = null;
					else entity.NumDist = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String CodLoc
			{
				get
				{
					System.Decimal? data = entity.CodLoc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodLoc = null;
					else entity.CodLoc = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodCart
			{
				get
				{
					System.Decimal? data = entity.CodCart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCart = null;
					else entity.CodCart = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataPreg
			{
				get
				{
					System.DateTime? data = entity.DataPreg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPreg = null;
					else entity.DataPreg = Convert.ToDateTime(value);
				}
			}
				
			public System.String NumNego
			{
				get
				{
					System.Decimal? data = entity.NumNego;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumNego = null;
					else entity.NumNego = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVenc
			{
				get
				{
					System.DateTime? data = entity.DataVenc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVenc = null;
					else entity.DataVenc = Convert.ToDateTime(value);
				}
			}
				
			public System.String QtdeDisp
			{
				get
				{
					System.Decimal? data = entity.QtdeDisp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeDisp = null;
					else entity.QtdeDisp = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValNego
			{
				get
				{
					System.Decimal? data = entity.ValNego;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValNego = null;
					else entity.ValNego = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValCotr
			{
				get
				{
					System.Decimal? data = entity.ValCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValCotr = null;
					else entity.ValCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodUsuaConp
			{
				get
				{
					System.Decimal? data = entity.CodUsuaConp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodUsuaConp = null;
					else entity.CodUsuaConp = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltMoviCotr
			{
				get
				{
					System.DateTime? data = entity.DataUltMoviCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltMoviCotr = null;
					else entity.DataUltMoviCotr = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodAgenQlfd
			{
				get
				{
					System.Decimal? data = entity.CodAgenQlfd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodAgenQlfd = null;
					else entity.CodAgenQlfd = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodCliQlfd
			{
				get
				{
					System.Decimal? data = entity.CodCliQlfd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCliQlfd = null;
					else entity.CodCliQlfd = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecLqdo
			{
				get
				{
					System.Decimal? data = entity.PrecLqdo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecLqdo = null;
					else entity.PrecLqdo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecBrut
			{
				get
				{
					System.Decimal? data = entity.PrecBrut;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecBrut = null;
					else entity.PrecBrut = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltMvto
			{
				get
				{
					System.DateTime? data = entity.DataUltMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltMvto = null;
					else entity.DataUltMvto = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoLiqd
			{
				get
				{
					System.Decimal? data = entity.TipoLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiqd = null;
					else entity.TipoLiqd = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodIsinAtu
			{
				get
				{
					System.String data = entity.CodIsinAtu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsinAtu = null;
					else entity.CodIsinAtu = Convert.ToString(value);
				}
			}
				
			public System.String NumDistAtu
			{
				get
				{
					System.Decimal? data = entity.NumDistAtu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDistAtu = null;
					else entity.NumDistAtu = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValFinSlas
			{
				get
				{
					System.Decimal? data = entity.ValFinSlas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValFinSlas = null;
					else entity.ValFinSlas = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeOri
			{
				get
				{
					System.Decimal? data = entity.QtdeOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeOri = null;
					else entity.QtdeOri = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumCotr
			{
				get
				{
					System.Decimal? data = entity.NumCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotr = null;
					else entity.NumCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValDol
			{
				get
				{
					System.Decimal? data = entity.ValDol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValDol = null;
					else entity.ValDol = Convert.ToDecimal(value);
				}
			}
			

			private esTcfposiTerm entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTcfposiTermQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTcfposiTerm can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TcfposiTerm : esTcfposiTerm
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTcfposiTermQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TcfposiTermMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAtu
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.DataAtu, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodIsin
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem NumDist
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.NumDist, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem CodLoc
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodLoc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodCart
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodCart, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataPreg
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.DataPreg, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NumNego
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.NumNego, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVenc
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QtdeDisp
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.QtdeDisp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValNego
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.ValNego, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValCotr
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.ValCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodUsuaConp
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodUsuaConp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltMoviCotr
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.DataUltMoviCotr, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodAgenQlfd
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodAgenQlfd, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodCliQlfd
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodCliQlfd, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecLqdo
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.PrecLqdo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecBrut
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.PrecBrut, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltMvto
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.DataUltMvto, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoLiqd
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.TipoLiqd, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodIsinAtu
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.CodIsinAtu, esSystemType.String);
			}
		} 
		
		public esQueryItem NumDistAtu
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.NumDistAtu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValFinSlas
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.ValFinSlas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeOri
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.QtdeOri, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumCotr
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValDol
		{
			get
			{
				return new esQueryItem(this, TcfposiTermMetadata.ColumnNames.ValDol, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TcfposiTermCollection")]
	public partial class TcfposiTermCollection : esTcfposiTermCollection, IEnumerable<TcfposiTerm>
	{
		public TcfposiTermCollection()
		{

		}
		
		public static implicit operator List<TcfposiTerm>(TcfposiTermCollection coll)
		{
			List<TcfposiTerm> list = new List<TcfposiTerm>();
			
			foreach (TcfposiTerm emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TcfposiTermMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfposiTermQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TcfposiTerm(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TcfposiTerm();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TcfposiTermQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfposiTermQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TcfposiTermQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TcfposiTerm AddNew()
		{
			TcfposiTerm entity = base.AddNewEntity() as TcfposiTerm;
			
			return entity;
		}

		public TcfposiTerm FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as TcfposiTerm;
		}


		#region IEnumerable<TcfposiTerm> Members

		IEnumerator<TcfposiTerm> IEnumerable<TcfposiTerm>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TcfposiTerm;
			}
		}

		#endregion
		
		private TcfposiTermQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TCFPOSI_TERM' table
	/// </summary>

	[Serializable]
	public partial class TcfposiTerm : esTcfposiTerm
	{
		public TcfposiTerm()
		{

		}
	
		public TcfposiTerm(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TcfposiTermMetadata.Meta();
			}
		}
		
		
		
		override protected esTcfposiTermQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfposiTermQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TcfposiTermQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfposiTermQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TcfposiTermQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TcfposiTermQuery query;
	}



	[Serializable]
	public partial class TcfposiTermQuery : esTcfposiTermQuery
	{
		public TcfposiTermQuery()
		{

		}		
		
		public TcfposiTermQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TcfposiTermMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TcfposiTermMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.DataAtu, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.DataAtu;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodCli, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodIsin, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.NumDist, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.NumDist;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodNeg, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodLoc, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodLoc;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodCart, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodCart;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.DataPreg, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.DataPreg;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.NumNego, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.NumNego;	
			c.NumericPrecision = 9;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.DataVenc, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.DataVenc;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.QtdeDisp, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.QtdeDisp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.ValNego, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.ValNego;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.ValCotr, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.ValCotr;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodUsuaConp, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodUsuaConp;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.DataUltMoviCotr, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.DataUltMoviCotr;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodAgenQlfd, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodAgenQlfd;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodCliQlfd, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodCliQlfd;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.PrecLqdo, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.PrecLqdo;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.PrecBrut, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.PrecBrut;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.DataUltMvto, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.DataUltMvto;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.TipoLiqd, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.TipoLiqd;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.CodIsinAtu, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.CodIsinAtu;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.NumDistAtu, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.NumDistAtu;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.ValFinSlas, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.ValFinSlas;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.QtdeOri, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.QtdeOri;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.NumCotr, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.NumCotr;	
			c.NumericPrecision = 9;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiTermMetadata.ColumnNames.ValDol, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiTermMetadata.PropertyNames.ValDol;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TcfposiTermMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DataAtu = "DATA_ATU";
			 public const string CodCli = "COD_CLI";
			 public const string CodIsin = "COD_ISIN";
			 public const string NumDist = "NUM_DIST";
			 public const string CodNeg = "COD_NEG";
			 public const string CodLoc = "COD_LOC";
			 public const string CodCart = "COD_CART";
			 public const string DataPreg = "DATA_PREG";
			 public const string NumNego = "NUM_NEGO";
			 public const string DataVenc = "DATA_VENC";
			 public const string QtdeDisp = "QTDE_DISP";
			 public const string ValNego = "VAL_NEGO";
			 public const string ValCotr = "VAL_COTR";
			 public const string CodUsuaConp = "COD_USUA_CONP";
			 public const string DataUltMoviCotr = "DATA_ULT_MOVI_COTR";
			 public const string CodAgenQlfd = "COD_AGEN_QLFD";
			 public const string CodCliQlfd = "COD_CLI_QLFD";
			 public const string PrecLqdo = "PREC_LQDO";
			 public const string PrecBrut = "PREC_BRUT";
			 public const string DataUltMvto = "DATA_ULT_MVTO";
			 public const string TipoLiqd = "TIPO_LIQD";
			 public const string CodIsinAtu = "COD_ISIN_ATU";
			 public const string NumDistAtu = "NUM_DIST_ATU";
			 public const string ValFinSlas = "VAL_FIN_SLAS";
			 public const string QtdeOri = "QTDE_ORI";
			 public const string NumCotr = "NUM_COTR";
			 public const string ValDol = "VAL_DOL";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DataAtu = "DataAtu";
			 public const string CodCli = "CodCli";
			 public const string CodIsin = "CodIsin";
			 public const string NumDist = "NumDist";
			 public const string CodNeg = "CodNeg";
			 public const string CodLoc = "CodLoc";
			 public const string CodCart = "CodCart";
			 public const string DataPreg = "DataPreg";
			 public const string NumNego = "NumNego";
			 public const string DataVenc = "DataVenc";
			 public const string QtdeDisp = "QtdeDisp";
			 public const string ValNego = "ValNego";
			 public const string ValCotr = "ValCotr";
			 public const string CodUsuaConp = "CodUsuaConp";
			 public const string DataUltMoviCotr = "DataUltMoviCotr";
			 public const string CodAgenQlfd = "CodAgenQlfd";
			 public const string CodCliQlfd = "CodCliQlfd";
			 public const string PrecLqdo = "PrecLqdo";
			 public const string PrecBrut = "PrecBrut";
			 public const string DataUltMvto = "DataUltMvto";
			 public const string TipoLiqd = "TipoLiqd";
			 public const string CodIsinAtu = "CodIsinAtu";
			 public const string NumDistAtu = "NumDistAtu";
			 public const string ValFinSlas = "ValFinSlas";
			 public const string QtdeOri = "QtdeOri";
			 public const string NumCotr = "NumCotr";
			 public const string ValDol = "ValDol";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TcfposiTermMetadata))
			{
				if(TcfposiTermMetadata.mapDelegates == null)
				{
					TcfposiTermMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TcfposiTermMetadata.meta == null)
				{
					TcfposiTermMetadata.meta = new TcfposiTermMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_ATU", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_ISIN", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_DIST", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("COD_LOC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_CART", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_PREG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NUM_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("QTDE_DISP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_NEGO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_USUA_CONP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_ULT_MOVI_COTR", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_AGEN_QLFD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_CLI_QLFD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_LQDO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_BRUT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_ULT_MVTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("TIPO_LIQD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_ISIN_ATU", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_DIST_ATU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_FIN_SLAS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_ORI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_DOL", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TCFPOSI_TERM";
				meta.Destination = "TCFPOSI_TERM";
				
				meta.spInsert = "proc_TCFPOSI_TERMInsert";				
				meta.spUpdate = "proc_TCFPOSI_TERMUpdate";		
				meta.spDelete = "proc_TCFPOSI_TERMDelete";
				meta.spLoadAll = "proc_TCFPOSI_TERMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TCFPOSI_TERMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TcfposiTermMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
