/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 12/06/2013 16:44:33
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVcfmoviTediCollection : esEntityCollection
	{
		public esVcfmoviTediCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VcfmoviTediCollection";
		}

		#region Query Logic
		protected void InitQuery(esVcfmoviTediQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVcfmoviTediQuery);
		}
		#endregion
		
		virtual public VcfmoviTedi DetachEntity(VcfmoviTedi entity)
		{
			return base.DetachEntity(entity) as VcfmoviTedi;
		}
		
		virtual public VcfmoviTedi AttachEntity(VcfmoviTedi entity)
		{
			return base.AttachEntity(entity) as VcfmoviTedi;
		}
		
		virtual public void Combine(VcfmoviTediCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VcfmoviTedi this[int index]
		{
			get
			{
				return base[index] as VcfmoviTedi;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VcfmoviTedi);
		}
	}



	[Serializable]
	abstract public class esVcfmoviTedi : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVcfmoviTediQuery GetDynamicQuery()
		{
			return null;
		}

		public esVcfmoviTedi()
		{

		}

		public esVcfmoviTedi(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVcfmoviTediQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVcfmoviTediQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DataMvto": this.str.DataMvto = (string)value; break;							
						case "DataNego": this.str.DataNego = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "TipoTitu": this.str.TipoTitu = (string)value; break;							
						case "DataVenc": this.str.DataVenc = (string)value; break;							
						case "CodSeli": this.str.CodSeli = (string)value; break;							
						case "NatOpe": this.str.NatOpe = (string)value; break;							
						case "QtdeTitu": this.str.QtdeTitu = (string)value; break;							
						case "PrecTrns": this.str.PrecTrns = (string)value; break;							
						case "ValTrns": this.str.ValTrns = (string)value; break;							
						case "ValTaxaAgen": this.str.ValTaxaAgen = (string)value; break;							
						case "ValTot": this.str.ValTot = (string)value; break;							
						case "PrecOrig": this.str.PrecOrig = (string)value; break;							
						case "ValOrig": this.str.ValOrig = (string)value; break;							
						case "DataOrig": this.str.DataOrig = (string)value; break;							
						case "DataLiqd": this.str.DataLiqd = (string)value; break;							
						case "ValRes": this.str.ValRes = (string)value; break;							
						case "ValIr": this.str.ValIr = (string)value; break;							
						case "DescHistMvto": this.str.DescHistMvto = (string)value; break;							
						case "ValIof": this.str.ValIof = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DataMvto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMvto = (System.DateTime?)value;
							break;
						
						case "DataNego":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataNego = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "DataVenc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVenc = (System.DateTime?)value;
							break;
						
						case "CodSeli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodSeli = (System.Decimal?)value;
							break;
						
						case "QtdeTitu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeTitu = (System.Decimal?)value;
							break;
						
						case "PrecTrns":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecTrns = (System.Decimal?)value;
							break;
						
						case "ValTrns":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValTrns = (System.Decimal?)value;
							break;
						
						case "ValTaxaAgen":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValTaxaAgen = (System.Decimal?)value;
							break;
						
						case "ValTot":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValTot = (System.Decimal?)value;
							break;
						
						case "PrecOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecOrig = (System.Decimal?)value;
							break;
						
						case "ValOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValOrig = (System.Decimal?)value;
							break;
						
						case "DataOrig":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOrig = (System.DateTime?)value;
							break;
						
						case "DataLiqd":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiqd = (System.DateTime?)value;
							break;
						
						case "ValRes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValRes = (System.Decimal?)value;
							break;
						
						case "ValIr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValIr = (System.Decimal?)value;
							break;
						
						case "ValIof":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValIof = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DATA_MVTO
		/// </summary>
		virtual public System.DateTime? DataMvto
		{
			get
			{
				return base.GetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataMvto);
			}
			
			set
			{
				base.SetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DATA_NEGO
		/// </summary>
		virtual public System.DateTime? DataNego
		{
			get
			{
				return base.GetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataNego);
			}
			
			set
			{
				base.SetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataNego, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.TIPO_TITU
		/// </summary>
		virtual public System.String TipoTitu
		{
			get
			{
				return base.GetSystemString(VcfmoviTediMetadata.ColumnNames.TipoTitu);
			}
			
			set
			{
				base.SetSystemString(VcfmoviTediMetadata.ColumnNames.TipoTitu, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DATA_VENC
		/// </summary>
		virtual public System.DateTime? DataVenc
		{
			get
			{
				return base.GetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataVenc);
			}
			
			set
			{
				base.SetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataVenc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.COD_SELI
		/// </summary>
		virtual public System.Decimal? CodSeli
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.CodSeli);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.CodSeli, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.NAT_OPE
		/// </summary>
		virtual public System.String NatOpe
		{
			get
			{
				return base.GetSystemString(VcfmoviTediMetadata.ColumnNames.NatOpe);
			}
			
			set
			{
				base.SetSystemString(VcfmoviTediMetadata.ColumnNames.NatOpe, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.QTDE_TITU
		/// </summary>
		virtual public System.Decimal? QtdeTitu
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.QtdeTitu);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.QtdeTitu, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.PREC_TRNS
		/// </summary>
		virtual public System.Decimal? PrecTrns
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.PrecTrns);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.PrecTrns, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_TRNS
		/// </summary>
		virtual public System.Decimal? ValTrns
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTrns);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTrns, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_TAXA_AGEN
		/// </summary>
		virtual public System.Decimal? ValTaxaAgen
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTaxaAgen);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTaxaAgen, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_TOT
		/// </summary>
		virtual public System.Decimal? ValTot
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTot);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValTot, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.PREC_ORIG
		/// </summary>
		virtual public System.Decimal? PrecOrig
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.PrecOrig);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.PrecOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_ORIG
		/// </summary>
		virtual public System.Decimal? ValOrig
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValOrig);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DATA_ORIG
		/// </summary>
		virtual public System.DateTime? DataOrig
		{
			get
			{
				return base.GetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataOrig);
			}
			
			set
			{
				base.SetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DATA_LIQD
		/// </summary>
		virtual public System.DateTime? DataLiqd
		{
			get
			{
				return base.GetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataLiqd);
			}
			
			set
			{
				base.SetSystemDateTime(VcfmoviTediMetadata.ColumnNames.DataLiqd, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_RES
		/// </summary>
		virtual public System.Decimal? ValRes
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValRes);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValRes, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_IR
		/// </summary>
		virtual public System.Decimal? ValIr
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValIr);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValIr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.DESC_HIST_MVTO
		/// </summary>
		virtual public System.String DescHistMvto
		{
			get
			{
				return base.GetSystemString(VcfmoviTediMetadata.ColumnNames.DescHistMvto);
			}
			
			set
			{
				base.SetSystemString(VcfmoviTediMetadata.ColumnNames.DescHistMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFMOVI_TEDI.VAL_IOF
		/// </summary>
		virtual public System.Decimal? ValIof
		{
			get
			{
				return base.GetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValIof);
			}
			
			set
			{
				base.SetSystemDecimal(VcfmoviTediMetadata.ColumnNames.ValIof, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVcfmoviTedi entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataMvto
			{
				get
				{
					System.DateTime? data = entity.DataMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMvto = null;
					else entity.DataMvto = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataNego
			{
				get
				{
					System.DateTime? data = entity.DataNego;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataNego = null;
					else entity.DataNego = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoTitu
			{
				get
				{
					System.String data = entity.TipoTitu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTitu = null;
					else entity.TipoTitu = Convert.ToString(value);
				}
			}
				
			public System.String DataVenc
			{
				get
				{
					System.DateTime? data = entity.DataVenc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVenc = null;
					else entity.DataVenc = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodSeli
			{
				get
				{
					System.Decimal? data = entity.CodSeli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodSeli = null;
					else entity.CodSeli = Convert.ToDecimal(value);
				}
			}
				
			public System.String NatOpe
			{
				get
				{
					System.String data = entity.NatOpe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NatOpe = null;
					else entity.NatOpe = Convert.ToString(value);
				}
			}
				
			public System.String QtdeTitu
			{
				get
				{
					System.Decimal? data = entity.QtdeTitu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeTitu = null;
					else entity.QtdeTitu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecTrns
			{
				get
				{
					System.Decimal? data = entity.PrecTrns;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecTrns = null;
					else entity.PrecTrns = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValTrns
			{
				get
				{
					System.Decimal? data = entity.ValTrns;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValTrns = null;
					else entity.ValTrns = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValTaxaAgen
			{
				get
				{
					System.Decimal? data = entity.ValTaxaAgen;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValTaxaAgen = null;
					else entity.ValTaxaAgen = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValTot
			{
				get
				{
					System.Decimal? data = entity.ValTot;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValTot = null;
					else entity.ValTot = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecOrig
			{
				get
				{
					System.Decimal? data = entity.PrecOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecOrig = null;
					else entity.PrecOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValOrig
			{
				get
				{
					System.Decimal? data = entity.ValOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValOrig = null;
					else entity.ValOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOrig
			{
				get
				{
					System.DateTime? data = entity.DataOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOrig = null;
					else entity.DataOrig = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiqd
			{
				get
				{
					System.DateTime? data = entity.DataLiqd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiqd = null;
					else entity.DataLiqd = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValRes
			{
				get
				{
					System.Decimal? data = entity.ValRes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValRes = null;
					else entity.ValRes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValIr
			{
				get
				{
					System.Decimal? data = entity.ValIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValIr = null;
					else entity.ValIr = Convert.ToDecimal(value);
				}
			}
				
			public System.String DescHistMvto
			{
				get
				{
					System.String data = entity.DescHistMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescHistMvto = null;
					else entity.DescHistMvto = Convert.ToString(value);
				}
			}
				
			public System.String ValIof
			{
				get
				{
					System.Decimal? data = entity.ValIof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValIof = null;
					else entity.ValIof = Convert.ToDecimal(value);
				}
			}
			

			private esVcfmoviTedi entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVcfmoviTediQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVcfmoviTedi can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VcfmoviTedi : esVcfmoviTedi
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVcfmoviTediQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VcfmoviTediMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataMvto
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DataMvto, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataNego
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DataNego, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoTitu
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.TipoTitu, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVenc
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodSeli
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.CodSeli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NatOpe
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.NatOpe, esSystemType.String);
			}
		} 
		
		public esQueryItem QtdeTitu
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.QtdeTitu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecTrns
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.PrecTrns, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValTrns
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValTrns, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValTaxaAgen
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValTaxaAgen, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValTot
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValTot, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecOrig
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.PrecOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValOrig
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOrig
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DataOrig, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiqd
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DataLiqd, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValRes
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValRes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValIr
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValIr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DescHistMvto
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.DescHistMvto, esSystemType.String);
			}
		} 
		
		public esQueryItem ValIof
		{
			get
			{
				return new esQueryItem(this, VcfmoviTediMetadata.ColumnNames.ValIof, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VcfmoviTediCollection")]
	public partial class VcfmoviTediCollection : esVcfmoviTediCollection, IEnumerable<VcfmoviTedi>
	{
		public VcfmoviTediCollection()
		{

		}
		
		public static implicit operator List<VcfmoviTedi>(VcfmoviTediCollection coll)
		{
			List<VcfmoviTedi> list = new List<VcfmoviTedi>();
			
			foreach (VcfmoviTedi emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VcfmoviTediMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfmoviTediQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VcfmoviTedi(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VcfmoviTedi();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VcfmoviTediQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfmoviTediQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VcfmoviTediQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VcfmoviTedi AddNew()
		{
			VcfmoviTedi entity = base.AddNewEntity() as VcfmoviTedi;
			
			return entity;
		}

		public VcfmoviTedi FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as VcfmoviTedi;
		}


		#region IEnumerable<VcfmoviTedi> Members

		IEnumerator<VcfmoviTedi> IEnumerable<VcfmoviTedi>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VcfmoviTedi;
			}
		}

		#endregion
		
		private VcfmoviTediQuery query;
	}


	/// <summary>
	/// Encapsulates the 'VCFMOVI_TEDI' table
	/// </summary>

	[Serializable]
	public partial class VcfmoviTedi : esVcfmoviTedi
	{
		public VcfmoviTedi()
		{

		}
	
		public VcfmoviTedi(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VcfmoviTediMetadata.Meta();
			}
		}
		
		
		
		override protected esVcfmoviTediQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfmoviTediQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VcfmoviTediQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfmoviTediQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VcfmoviTediQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VcfmoviTediQuery query;
	}



	[Serializable]
	public partial class VcfmoviTediQuery : esVcfmoviTediQuery
	{
		public VcfmoviTediQuery()
		{

		}		
		
		public VcfmoviTediQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VcfmoviTediMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VcfmoviTediMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DataMvto, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DataMvto;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DataNego, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DataNego;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.CodCli, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.TipoTitu, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.TipoTitu;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DataVenc, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DataVenc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.CodSeli, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.CodSeli;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.NatOpe, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.NatOpe;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.QtdeTitu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.QtdeTitu;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.PrecTrns, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.PrecTrns;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValTrns, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValTrns;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValTaxaAgen, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValTaxaAgen;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValTot, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValTot;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.PrecOrig, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.PrecOrig;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValOrig, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValOrig;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DataOrig, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DataOrig;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DataLiqd, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DataLiqd;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValRes, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValRes;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValIr, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValIr;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.DescHistMvto, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.DescHistMvto;
			c.CharacterMaxLength = 70;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfmoviTediMetadata.ColumnNames.ValIof, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfmoviTediMetadata.PropertyNames.ValIof;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VcfmoviTediMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DataMvto = "DATA_MVTO";
			 public const string DataNego = "DATA_NEGO";
			 public const string CodCli = "COD_CLI";
			 public const string TipoTitu = "TIPO_TITU";
			 public const string DataVenc = "DATA_VENC";
			 public const string CodSeli = "COD_SELI";
			 public const string NatOpe = "NAT_OPE";
			 public const string QtdeTitu = "QTDE_TITU";
			 public const string PrecTrns = "PREC_TRNS";
			 public const string ValTrns = "VAL_TRNS";
			 public const string ValTaxaAgen = "VAL_TAXA_AGEN";
			 public const string ValTot = "VAL_TOT";
			 public const string PrecOrig = "PREC_ORIG";
			 public const string ValOrig = "VAL_ORIG";
			 public const string DataOrig = "DATA_ORIG";
			 public const string DataLiqd = "DATA_LIQD";
			 public const string ValRes = "VAL_RES";
			 public const string ValIr = "VAL_IR";
			 public const string DescHistMvto = "DESC_HIST_MVTO";
			 public const string ValIof = "VAL_IOF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DataMvto = "DataMvto";
			 public const string DataNego = "DataNego";
			 public const string CodCli = "CodCli";
			 public const string TipoTitu = "TipoTitu";
			 public const string DataVenc = "DataVenc";
			 public const string CodSeli = "CodSeli";
			 public const string NatOpe = "NatOpe";
			 public const string QtdeTitu = "QtdeTitu";
			 public const string PrecTrns = "PrecTrns";
			 public const string ValTrns = "ValTrns";
			 public const string ValTaxaAgen = "ValTaxaAgen";
			 public const string ValTot = "ValTot";
			 public const string PrecOrig = "PrecOrig";
			 public const string ValOrig = "ValOrig";
			 public const string DataOrig = "DataOrig";
			 public const string DataLiqd = "DataLiqd";
			 public const string ValRes = "ValRes";
			 public const string ValIr = "ValIr";
			 public const string DescHistMvto = "DescHistMvto";
			 public const string ValIof = "ValIof";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VcfmoviTediMetadata))
			{
				if(VcfmoviTediMetadata.mapDelegates == null)
				{
					VcfmoviTediMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VcfmoviTediMetadata.meta == null)
				{
					VcfmoviTediMetadata.meta = new VcfmoviTediMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_MVTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_NEGO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_TITU", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_SELI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NAT_OPE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("QTDE_TITU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_TRNS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_TRNS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_TAXA_AGEN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_TOT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_ORIG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_LIQD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("VAL_RES", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_IR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DESC_HIST_MVTO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VAL_IOF", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "VCFMOVI_TEDI";
				meta.Destination = "VCFMOVI_TEDI";
				
				meta.spInsert = "proc_VCFMOVI_TEDIInsert";				
				meta.spUpdate = "proc_VCFMOVI_TEDIUpdate";		
				meta.spDelete = "proc_VCFMOVI_TEDIDelete";
				meta.spLoadAll = "proc_VCFMOVI_TEDILoadAll";
				meta.spLoadByPrimaryKey = "proc_VCFMOVI_TEDILoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VcfmoviTediMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
