/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 14/09/2012 11:22:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVcfproventoCollection : esEntityCollection
	{
		public esVcfproventoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VcfproventoCollection";
		}

		#region Query Logic
		protected void InitQuery(esVcfproventoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVcfproventoQuery);
		}
		#endregion
		
		virtual public Vcfprovento DetachEntity(Vcfprovento entity)
		{
			return base.DetachEntity(entity) as Vcfprovento;
		}
		
		virtual public Vcfprovento AttachEntity(Vcfprovento entity)
		{
			return base.AttachEntity(entity) as Vcfprovento;
		}
		
		virtual public void Combine(VcfproventoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Vcfprovento this[int index]
		{
			get
			{
				return base[index] as Vcfprovento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Vcfprovento);
		}
	}



	[Serializable]
	abstract public class esVcfprovento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVcfproventoQuery GetDynamicQuery()
		{
			return null;
		}

		public esVcfprovento()
		{

		}

		public esVcfprovento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVcfproventoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVcfproventoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "TipoProv": this.str.TipoProv = (string)value; break;							
						case "CodIsin": this.str.CodIsin = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "ValProv": this.str.ValProv = (string)value; break;							
						case "DataIniPgtoProv": this.str.DataIniPgtoProv = (string)value; break;							
						case "CodIsinDest": this.str.CodIsinDest = (string)value; break;							
						case "CodIsinDir": this.str.CodIsinDir = (string)value; break;							
						case "PrecPapSubs": this.str.PrecPapSubs = (string)value; break;							
						case "DataLimSubs": this.str.DataLimSubs = (string)value; break;							
						case "DataPgtoDivi": this.str.DataPgtoDivi = (string)value; break;							
						case "CodIsinOri": this.str.CodIsinOri = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "TipoProv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoProv = (System.Decimal?)value;
							break;
						
						case "ValProv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValProv = (System.Decimal?)value;
							break;
						
						case "DataIniPgtoProv":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataIniPgtoProv = (System.DateTime?)value;
							break;
						
						case "PrecPapSubs":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecPapSubs = (System.Decimal?)value;
							break;
						
						case "DataLimSubs":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLimSubs = (System.DateTime?)value;
							break;
						
						case "DataPgtoDivi":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPgtoDivi = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to VCFPROVENTO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VcfproventoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VcfproventoMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.TIPO_PROV
		/// </summary>
		virtual public System.Decimal? TipoProv
		{
			get
			{
				return base.GetSystemDecimal(VcfproventoMetadata.ColumnNames.TipoProv);
			}
			
			set
			{
				base.SetSystemDecimal(VcfproventoMetadata.ColumnNames.TipoProv, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.COD_ISIN
		/// </summary>
		virtual public System.String CodIsin
		{
			get
			{
				return base.GetSystemString(VcfproventoMetadata.ColumnNames.CodIsin);
			}
			
			set
			{
				base.SetSystemString(VcfproventoMetadata.ColumnNames.CodIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(VcfproventoMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(VcfproventoMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.VAL_PROV
		/// </summary>
		virtual public System.Decimal? ValProv
		{
			get
			{
				return base.GetSystemDecimal(VcfproventoMetadata.ColumnNames.ValProv);
			}
			
			set
			{
				base.SetSystemDecimal(VcfproventoMetadata.ColumnNames.ValProv, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.DATA_INI_PGTO_PROV
		/// </summary>
		virtual public System.DateTime? DataIniPgtoProv
		{
			get
			{
				return base.GetSystemDateTime(VcfproventoMetadata.ColumnNames.DataIniPgtoProv);
			}
			
			set
			{
				base.SetSystemDateTime(VcfproventoMetadata.ColumnNames.DataIniPgtoProv, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.COD_ISIN_DEST
		/// </summary>
		virtual public System.String CodIsinDest
		{
			get
			{
				return base.GetSystemString(VcfproventoMetadata.ColumnNames.CodIsinDest);
			}
			
			set
			{
				base.SetSystemString(VcfproventoMetadata.ColumnNames.CodIsinDest, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.COD_ISIN_DIR
		/// </summary>
		virtual public System.String CodIsinDir
		{
			get
			{
				return base.GetSystemString(VcfproventoMetadata.ColumnNames.CodIsinDir);
			}
			
			set
			{
				base.SetSystemString(VcfproventoMetadata.ColumnNames.CodIsinDir, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.PREC_PAP_SUBS
		/// </summary>
		virtual public System.Decimal? PrecPapSubs
		{
			get
			{
				return base.GetSystemDecimal(VcfproventoMetadata.ColumnNames.PrecPapSubs);
			}
			
			set
			{
				base.SetSystemDecimal(VcfproventoMetadata.ColumnNames.PrecPapSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.DATA_LIM_SUBS
		/// </summary>
		virtual public System.DateTime? DataLimSubs
		{
			get
			{
				return base.GetSystemDateTime(VcfproventoMetadata.ColumnNames.DataLimSubs);
			}
			
			set
			{
				base.SetSystemDateTime(VcfproventoMetadata.ColumnNames.DataLimSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.DATA_PGTO_DIVI
		/// </summary>
		virtual public System.DateTime? DataPgtoDivi
		{
			get
			{
				return base.GetSystemDateTime(VcfproventoMetadata.ColumnNames.DataPgtoDivi);
			}
			
			set
			{
				base.SetSystemDateTime(VcfproventoMetadata.ColumnNames.DataPgtoDivi, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPROVENTO.COD_ISIN_ORI
		/// </summary>
		virtual public System.String CodIsinOri
		{
			get
			{
				return base.GetSystemString(VcfproventoMetadata.ColumnNames.CodIsinOri);
			}
			
			set
			{
				base.SetSystemString(VcfproventoMetadata.ColumnNames.CodIsinOri, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVcfprovento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoProv
			{
				get
				{
					System.Decimal? data = entity.TipoProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProv = null;
					else entity.TipoProv = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodIsin
			{
				get
				{
					System.String data = entity.CodIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsin = null;
					else entity.CodIsin = Convert.ToString(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String ValProv
			{
				get
				{
					System.Decimal? data = entity.ValProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValProv = null;
					else entity.ValProv = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataIniPgtoProv
			{
				get
				{
					System.DateTime? data = entity.DataIniPgtoProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataIniPgtoProv = null;
					else entity.DataIniPgtoProv = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodIsinDest
			{
				get
				{
					System.String data = entity.CodIsinDest;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsinDest = null;
					else entity.CodIsinDest = Convert.ToString(value);
				}
			}
				
			public System.String CodIsinDir
			{
				get
				{
					System.String data = entity.CodIsinDir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsinDir = null;
					else entity.CodIsinDir = Convert.ToString(value);
				}
			}
				
			public System.String PrecPapSubs
			{
				get
				{
					System.Decimal? data = entity.PrecPapSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecPapSubs = null;
					else entity.PrecPapSubs = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataLimSubs
			{
				get
				{
					System.DateTime? data = entity.DataLimSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLimSubs = null;
					else entity.DataLimSubs = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPgtoDivi
			{
				get
				{
					System.DateTime? data = entity.DataPgtoDivi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPgtoDivi = null;
					else entity.DataPgtoDivi = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodIsinOri
			{
				get
				{
					System.String data = entity.CodIsinOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsinOri = null;
					else entity.CodIsinOri = Convert.ToString(value);
				}
			}
			

			private esVcfprovento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVcfproventoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVcfprovento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Vcfprovento : esVcfprovento
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVcfproventoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VcfproventoMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoProv
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.TipoProv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodIsin
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.CodIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem ValProv
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.ValProv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataIniPgtoProv
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.DataIniPgtoProv, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodIsinDest
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.CodIsinDest, esSystemType.String);
			}
		} 
		
		public esQueryItem CodIsinDir
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.CodIsinDir, esSystemType.String);
			}
		} 
		
		public esQueryItem PrecPapSubs
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.PrecPapSubs, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataLimSubs
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.DataLimSubs, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPgtoDivi
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.DataPgtoDivi, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodIsinOri
		{
			get
			{
				return new esQueryItem(this, VcfproventoMetadata.ColumnNames.CodIsinOri, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VcfproventoCollection")]
	public partial class VcfproventoCollection : esVcfproventoCollection, IEnumerable<Vcfprovento>
	{
		public VcfproventoCollection()
		{

		}
		
		public static implicit operator List<Vcfprovento>(VcfproventoCollection coll)
		{
			List<Vcfprovento> list = new List<Vcfprovento>();
			
			foreach (Vcfprovento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VcfproventoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfproventoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Vcfprovento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Vcfprovento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VcfproventoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfproventoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VcfproventoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Vcfprovento AddNew()
		{
			Vcfprovento entity = base.AddNewEntity() as Vcfprovento;
			
			return entity;
		}

		public Vcfprovento FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Vcfprovento;
		}


		#region IEnumerable<Vcfprovento> Members

		IEnumerator<Vcfprovento> IEnumerable<Vcfprovento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Vcfprovento;
			}
		}

		#endregion
		
		private VcfproventoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'VCFPROVENTO' table
	/// </summary>

	[Serializable]
	public partial class Vcfprovento : esVcfprovento
	{
		public Vcfprovento()
		{

		}
	
		public Vcfprovento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VcfproventoMetadata.Meta();
			}
		}
		
		
		
		override protected esVcfproventoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfproventoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VcfproventoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfproventoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VcfproventoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VcfproventoQuery query;
	}



	[Serializable]
	public partial class VcfproventoQuery : esVcfproventoQuery
	{
		public VcfproventoQuery()
		{

		}		
		
		public VcfproventoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VcfproventoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VcfproventoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfproventoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.TipoProv, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfproventoMetadata.PropertyNames.TipoProv;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.CodIsin, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfproventoMetadata.PropertyNames.CodIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.CodNeg, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfproventoMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.ValProv, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfproventoMetadata.PropertyNames.ValProv;	
			c.NumericPrecision = 38;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.DataIniPgtoProv, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfproventoMetadata.PropertyNames.DataIniPgtoProv;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.CodIsinDest, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfproventoMetadata.PropertyNames.CodIsinDest;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.CodIsinDir, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfproventoMetadata.PropertyNames.CodIsinDir;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.PrecPapSubs, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfproventoMetadata.PropertyNames.PrecPapSubs;	
			c.NumericPrecision = 38;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.DataLimSubs, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfproventoMetadata.PropertyNames.DataLimSubs;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.DataPgtoDivi, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfproventoMetadata.PropertyNames.DataPgtoDivi;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfproventoMetadata.ColumnNames.CodIsinOri, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfproventoMetadata.PropertyNames.CodIsinOri;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VcfproventoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string TipoProv = "TIPO_PROV";
			 public const string CodIsin = "COD_ISIN";
			 public const string CodNeg = "COD_NEG";
			 public const string ValProv = "VAL_PROV";
			 public const string DataIniPgtoProv = "DATA_INI_PGTO_PROV";
			 public const string CodIsinDest = "COD_ISIN_DEST";
			 public const string CodIsinDir = "COD_ISIN_DIR";
			 public const string PrecPapSubs = "PREC_PAP_SUBS";
			 public const string DataLimSubs = "DATA_LIM_SUBS";
			 public const string DataPgtoDivi = "DATA_PGTO_DIVI";
			 public const string CodIsinOri = "COD_ISIN_ORI";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string TipoProv = "TipoProv";
			 public const string CodIsin = "CodIsin";
			 public const string CodNeg = "CodNeg";
			 public const string ValProv = "ValProv";
			 public const string DataIniPgtoProv = "DataIniPgtoProv";
			 public const string CodIsinDest = "CodIsinDest";
			 public const string CodIsinDir = "CodIsinDir";
			 public const string PrecPapSubs = "PrecPapSubs";
			 public const string DataLimSubs = "DataLimSubs";
			 public const string DataPgtoDivi = "DataPgtoDivi";
			 public const string CodIsinOri = "CodIsinOri";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VcfproventoMetadata))
			{
				if(VcfproventoMetadata.mapDelegates == null)
				{
					VcfproventoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VcfproventoMetadata.meta == null)
				{
					VcfproventoMetadata.meta = new VcfproventoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_PROV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_ISIN", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("VAL_PROV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_INI_PGTO_PROV", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_ISIN_DEST", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("COD_ISIN_DIR", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("PREC_PAP_SUBS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_LIM_SUBS", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_PGTO_DIVI", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_ISIN_ORI", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "VCFPROVENTO";
				meta.Destination = "VCFPROVENTO";
				
				meta.spInsert = "proc_VCFPROVENTOInsert";				
				meta.spUpdate = "proc_VCFPROVENTOUpdate";		
				meta.spDelete = "proc_VCFPROVENTODelete";
				meta.spLoadAll = "proc_VCFPROVENTOLoadAll";
				meta.spLoadByPrimaryKey = "proc_VCFPROVENTOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VcfproventoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
