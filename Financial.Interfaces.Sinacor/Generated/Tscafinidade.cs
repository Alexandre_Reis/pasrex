/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/10/2013 12:42:18
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscafinidadeCollection : esEntityCollection
	{
		public esTscafinidadeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscafinidadeCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscafinidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscafinidadeQuery);
		}
		#endregion
		
		virtual public Tscafinidade DetachEntity(Tscafinidade entity)
		{
			return base.DetachEntity(entity) as Tscafinidade;
		}
		
		virtual public Tscafinidade AttachEntity(Tscafinidade entity)
		{
			return base.AttachEntity(entity) as Tscafinidade;
		}
		
		virtual public void Combine(TscafinidadeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscafinidade this[int index]
		{
			get
			{
				return base[index] as Tscafinidade;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscafinidade);
		}
	}



	[Serializable]
	abstract public class esTscafinidade : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscafinidadeQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscafinidade()
		{

		}

		public esTscafinidade(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscafinidadeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscafinidadeQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdAfinidade": this.str.CdAfinidade = (string)value; break;							
						case "DsAfinidade": this.str.DsAfinidade = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdAfinidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAfinidade = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCAFINIDADE.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscafinidadeMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscafinidadeMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCAFINIDADE.CD_AFINIDADE
		/// </summary>
		virtual public System.Decimal? CdAfinidade
		{
			get
			{
				return base.GetSystemDecimal(TscafinidadeMetadata.ColumnNames.CdAfinidade);
			}
			
			set
			{
				base.SetSystemDecimal(TscafinidadeMetadata.ColumnNames.CdAfinidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCAFINIDADE.DS_AFINIDADE
		/// </summary>
		virtual public System.String DsAfinidade
		{
			get
			{
				return base.GetSystemString(TscafinidadeMetadata.ColumnNames.DsAfinidade);
			}
			
			set
			{
				base.SetSystemString(TscafinidadeMetadata.ColumnNames.DsAfinidade, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscafinidade entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAfinidade
			{
				get
				{
					System.Decimal? data = entity.CdAfinidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAfinidade = null;
					else entity.CdAfinidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String DsAfinidade
			{
				get
				{
					System.String data = entity.DsAfinidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsAfinidade = null;
					else entity.DsAfinidade = Convert.ToString(value);
				}
			}
			

			private esTscafinidade entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscafinidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscafinidade can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscafinidade : esTscafinidade
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscafinidadeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscafinidadeMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscafinidadeMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAfinidade
		{
			get
			{
				return new esQueryItem(this, TscafinidadeMetadata.ColumnNames.CdAfinidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DsAfinidade
		{
			get
			{
				return new esQueryItem(this, TscafinidadeMetadata.ColumnNames.DsAfinidade, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscafinidadeCollection")]
	public partial class TscafinidadeCollection : esTscafinidadeCollection, IEnumerable<Tscafinidade>
	{
		public TscafinidadeCollection()
		{

		}
		
		public static implicit operator List<Tscafinidade>(TscafinidadeCollection coll)
		{
			List<Tscafinidade> list = new List<Tscafinidade>();
			
			foreach (Tscafinidade emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscafinidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscafinidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscafinidade(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscafinidade();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscafinidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscafinidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscafinidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscafinidade AddNew()
		{
			Tscafinidade entity = base.AddNewEntity() as Tscafinidade;
			
			return entity;
		}

		public Tscafinidade FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscafinidade;
		}


		#region IEnumerable<Tscafinidade> Members

		IEnumerator<Tscafinidade> IEnumerable<Tscafinidade>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscafinidade;
			}
		}

		#endregion
		
		private TscafinidadeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCAFINIDADE' table
	/// </summary>

	[Serializable]
	public partial class Tscafinidade : esTscafinidade
	{
		public Tscafinidade()
		{

		}
	
		public Tscafinidade(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscafinidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esTscafinidadeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscafinidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscafinidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscafinidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscafinidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscafinidadeQuery query;
	}



	[Serializable]
	public partial class TscafinidadeQuery : esTscafinidadeQuery
	{
		public TscafinidadeQuery()
		{

		}		
		
		public TscafinidadeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscafinidadeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscafinidadeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscafinidadeMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscafinidadeMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscafinidadeMetadata.ColumnNames.CdAfinidade, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscafinidadeMetadata.PropertyNames.CdAfinidade;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscafinidadeMetadata.ColumnNames.DsAfinidade, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TscafinidadeMetadata.PropertyNames.DsAfinidade;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscafinidadeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdAfinidade = "CD_AFINIDADE";
			 public const string DsAfinidade = "DS_AFINIDADE";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdAfinidade = "CdAfinidade";
			 public const string DsAfinidade = "DsAfinidade";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscafinidadeMetadata))
			{
				if(TscafinidadeMetadata.mapDelegates == null)
				{
					TscafinidadeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscafinidadeMetadata.meta == null)
				{
					TscafinidadeMetadata.meta = new TscafinidadeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_AFINIDADE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DS_AFINIDADE", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "TSCAFINIDADE";
				meta.Destination = "TSCAFINIDADE";
				
				meta.spInsert = "proc_TSCAFINIDADEInsert";				
				meta.spUpdate = "proc_TSCAFINIDADEUpdate";		
				meta.spDelete = "proc_TSCAFINIDADEDelete";
				meta.spLoadAll = "proc_TSCAFINIDADELoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCAFINIDADELoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscafinidadeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
