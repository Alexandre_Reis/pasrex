/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/05/2012 12:47:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	
















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTbtcontratoCollection : esEntityCollection
	{
		public esTbtcontratoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TbtcontratoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTbtcontratoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTbtcontratoQuery);
		}
		#endregion
		
		virtual public Tbtcontrato DetachEntity(Tbtcontrato entity)
		{
			return base.DetachEntity(entity) as Tbtcontrato;
		}
		
		virtual public Tbtcontrato AttachEntity(Tbtcontrato entity)
		{
			return base.AttachEntity(entity) as Tbtcontrato;
		}
		
		virtual public void Combine(TbtcontratoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tbtcontrato this[int index]
		{
			get
			{
				return base[index] as Tbtcontrato;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tbtcontrato);
		}
	}



	[Serializable]
	abstract public class esTbtcontrato : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTbtcontratoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTbtcontrato()
		{

		}

		public esTbtcontrato(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTbtcontratoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTbtcontratoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "DataMvto": this.str.DataMvto = (string)value; break;							
						case "DataRef": this.str.DataRef = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "DataCblc": this.str.DataCblc = (string)value; break;							
						case "NumCotr": this.str.NumCotr = (string)value; break;							
						case "TipoMerc": this.str.TipoMerc = (string)value; break;							
						case "TipoMercBtc": this.str.TipoMercBtc = (string)value; break;							
						case "TipoOri": this.str.TipoOri = (string)value; break;							
						case "NumCotrOrig": this.str.NumCotrOrig = (string)value; break;							
						case "DataOri": this.str.DataOri = (string)value; break;							
						case "DataAber": this.str.DataAber = (string)value; break;							
						case "QtdeAcoe": this.str.QtdeAcoe = (string)value; break;							
						case "QtdeAcoeSegr": this.str.QtdeAcoeSegr = (string)value; break;							
						case "DataVenc": this.str.DataVenc = (string)value; break;							
						case "PrecMed": this.str.PrecMed = (string)value; break;							
						case "TaxaRemu": this.str.TaxaRemu = (string)value; break;							
						case "TaxaComi": this.str.TaxaComi = (string)value; break;							
						case "QtdeAcoeOrig": this.str.QtdeAcoeOrig = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "PrecDiaAnte": this.str.PrecDiaAnte = (string)value; break;							
						case "IndPosi": this.str.IndPosi = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "DataMvto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMvto = (System.DateTime?)value;
							break;
						
						case "DataRef":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRef = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "DataCblc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCblc = (System.DateTime?)value;
							break;
						
						case "NumCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotr = (System.Decimal?)value;
							break;
						
						case "TipoOri":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoOri = (System.Decimal?)value;
							break;
						
						case "NumCotrOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotrOrig = (System.Decimal?)value;
							break;
						
						case "DataOri":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOri = (System.DateTime?)value;
							break;
						
						case "DataAber":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAber = (System.DateTime?)value;
							break;
						
						case "QtdeAcoe":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoe = (System.Decimal?)value;
							break;
						
						case "QtdeAcoeSegr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoeSegr = (System.Decimal?)value;
							break;
						
						case "DataVenc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVenc = (System.DateTime?)value;
							break;
						
						case "PrecMed":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecMed = (System.Decimal?)value;
							break;
						
						case "TaxaRemu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaRemu = (System.Decimal?)value;
							break;
						
						case "TaxaComi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaComi = (System.Decimal?)value;
							break;
						
						case "QtdeAcoeOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoeOrig = (System.Decimal?)value;
							break;
						
						case "PrecDiaAnte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecDiaAnte = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TBTCONTRATO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_MVTO
		/// </summary>
		virtual public System.DateTime? DataMvto
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataMvto);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataMvto, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_REF
		/// </summary>
		virtual public System.DateTime? DataRef
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataRef);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataRef, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_CBLC
		/// </summary>
		virtual public System.DateTime? DataCblc
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataCblc);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataCblc, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.NUM_COTR
		/// </summary>
		virtual public System.Decimal? NumCotr
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.NumCotr);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.NumCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.TIPO_MERC
		/// </summary>
		virtual public System.String TipoMerc
		{
			get
			{
				return base.GetSystemString(TbtcontratoMetadata.ColumnNames.TipoMerc);
			}
			
			set
			{
				base.SetSystemString(TbtcontratoMetadata.ColumnNames.TipoMerc, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.TIPO_MERC_BTC
		/// </summary>
		virtual public System.String TipoMercBtc
		{
			get
			{
				return base.GetSystemString(TbtcontratoMetadata.ColumnNames.TipoMercBtc);
			}
			
			set
			{
				base.SetSystemString(TbtcontratoMetadata.ColumnNames.TipoMercBtc, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.TIPO_ORI
		/// </summary>
		virtual public System.Decimal? TipoOri
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.TipoOri);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.TipoOri, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.NUM_COTR_ORIG
		/// </summary>
		virtual public System.Decimal? NumCotrOrig
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.NumCotrOrig);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.NumCotrOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_ORI
		/// </summary>
		virtual public System.DateTime? DataOri
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataOri);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataOri, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_ABER
		/// </summary>
		virtual public System.DateTime? DataAber
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataAber);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataAber, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.QTDE_ACOE
		/// </summary>
		virtual public System.Decimal? QtdeAcoe
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoe);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoe, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.QTDE_ACOE_SEGR
		/// </summary>
		virtual public System.Decimal? QtdeAcoeSegr
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoeSegr);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoeSegr, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.DATA_VENC
		/// </summary>
		virtual public System.DateTime? DataVenc
		{
			get
			{
				return base.GetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataVenc);
			}
			
			set
			{
				base.SetSystemDateTime(TbtcontratoMetadata.ColumnNames.DataVenc, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.PREC_MED
		/// </summary>
		virtual public System.Decimal? PrecMed
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.PrecMed);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.PrecMed, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.TAXA_REMU
		/// </summary>
		virtual public System.Decimal? TaxaRemu
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.TaxaRemu);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.TaxaRemu, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.TAXA_COMI
		/// </summary>
		virtual public System.Decimal? TaxaComi
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.TaxaComi);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.TaxaComi, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.QTDE_ACOE_ORIG
		/// </summary>
		virtual public System.Decimal? QtdeAcoeOrig
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoeOrig);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.QtdeAcoeOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(TbtcontratoMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(TbtcontratoMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.PREC_DIA_ANTE
		/// </summary>
		virtual public System.Decimal? PrecDiaAnte
		{
			get
			{
				return base.GetSystemDecimal(TbtcontratoMetadata.ColumnNames.PrecDiaAnte);
			}
			
			set
			{
				base.SetSystemDecimal(TbtcontratoMetadata.ColumnNames.PrecDiaAnte, value);
			}
		}
		
		/// <summary>
		/// Maps to TBTCONTRATO.IND_POSI
		/// </summary>
		virtual public System.String IndPosi
		{
			get
			{
				return base.GetSystemString(TbtcontratoMetadata.ColumnNames.IndPosi);
			}
			
			set
			{
				base.SetSystemString(TbtcontratoMetadata.ColumnNames.IndPosi, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTbtcontrato entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataMvto
			{
				get
				{
					System.DateTime? data = entity.DataMvto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMvto = null;
					else entity.DataMvto = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataRef
			{
				get
				{
					System.DateTime? data = entity.DataRef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRef = null;
					else entity.DataRef = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataCblc
			{
				get
				{
					System.DateTime? data = entity.DataCblc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCblc = null;
					else entity.DataCblc = Convert.ToDateTime(value);
				}
			}
				
			public System.String NumCotr
			{
				get
				{
					System.Decimal? data = entity.NumCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotr = null;
					else entity.NumCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoMerc
			{
				get
				{
					System.String data = entity.TipoMerc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMerc = null;
					else entity.TipoMerc = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercBtc
			{
				get
				{
					System.String data = entity.TipoMercBtc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercBtc = null;
					else entity.TipoMercBtc = Convert.ToString(value);
				}
			}
				
			public System.String TipoOri
			{
				get
				{
					System.Decimal? data = entity.TipoOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOri = null;
					else entity.TipoOri = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumCotrOrig
			{
				get
				{
					System.Decimal? data = entity.NumCotrOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotrOrig = null;
					else entity.NumCotrOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOri
			{
				get
				{
					System.DateTime? data = entity.DataOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOri = null;
					else entity.DataOri = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAber
			{
				get
				{
					System.DateTime? data = entity.DataAber;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAber = null;
					else entity.DataAber = Convert.ToDateTime(value);
				}
			}
				
			public System.String QtdeAcoe
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoe = null;
					else entity.QtdeAcoe = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeAcoeSegr
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoeSegr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoeSegr = null;
					else entity.QtdeAcoeSegr = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVenc
			{
				get
				{
					System.DateTime? data = entity.DataVenc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVenc = null;
					else entity.DataVenc = Convert.ToDateTime(value);
				}
			}
				
			public System.String PrecMed
			{
				get
				{
					System.Decimal? data = entity.PrecMed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecMed = null;
					else entity.PrecMed = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaRemu
			{
				get
				{
					System.Decimal? data = entity.TaxaRemu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaRemu = null;
					else entity.TaxaRemu = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaComi
			{
				get
				{
					System.Decimal? data = entity.TaxaComi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaComi = null;
					else entity.TaxaComi = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeAcoeOrig
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoeOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoeOrig = null;
					else entity.QtdeAcoeOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String PrecDiaAnte
			{
				get
				{
					System.Decimal? data = entity.PrecDiaAnte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecDiaAnte = null;
					else entity.PrecDiaAnte = Convert.ToDecimal(value);
				}
			}
				
			public System.String IndPosi
			{
				get
				{
					System.String data = entity.IndPosi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndPosi = null;
					else entity.IndPosi = Convert.ToString(value);
				}
			}
			

			private esTbtcontrato entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTbtcontratoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTbtcontrato can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tbtcontrato : esTbtcontrato
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTbtcontratoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TbtcontratoMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataMvto
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataMvto, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataRef
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataRef, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataCblc
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataCblc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NumCotr
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoMerc
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.TipoMerc, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercBtc
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.TipoMercBtc, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoOri
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.TipoOri, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumCotrOrig
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.NumCotrOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOri
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataOri, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAber
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataAber, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QtdeAcoe
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.QtdeAcoe, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeAcoeSegr
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.QtdeAcoeSegr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVenc
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PrecMed
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.PrecMed, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaRemu
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.TaxaRemu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaComi
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.TaxaComi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeAcoeOrig
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.QtdeAcoeOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem PrecDiaAnte
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.PrecDiaAnte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IndPosi
		{
			get
			{
				return new esQueryItem(this, TbtcontratoMetadata.ColumnNames.IndPosi, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TbtcontratoCollection")]
	public partial class TbtcontratoCollection : esTbtcontratoCollection, IEnumerable<Tbtcontrato>
	{
		public TbtcontratoCollection()
		{

		}
		
		public static implicit operator List<Tbtcontrato>(TbtcontratoCollection coll)
		{
			List<Tbtcontrato> list = new List<Tbtcontrato>();
			
			foreach (Tbtcontrato emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TbtcontratoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbtcontratoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tbtcontrato(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tbtcontrato();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TbtcontratoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbtcontratoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TbtcontratoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tbtcontrato AddNew()
		{
			Tbtcontrato entity = base.AddNewEntity() as Tbtcontrato;
			
			return entity;
		}

		public Tbtcontrato FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tbtcontrato;
		}


		#region IEnumerable<Tbtcontrato> Members

		IEnumerator<Tbtcontrato> IEnumerable<Tbtcontrato>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tbtcontrato;
			}
		}

		#endregion
		
		private TbtcontratoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TBTCONTRATO' table
	/// </summary>

	[Serializable]
	public partial class Tbtcontrato : esTbtcontrato
	{
		public Tbtcontrato()
		{

		}
	
		public Tbtcontrato(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TbtcontratoMetadata.Meta();
			}
		}
		
		
		
		override protected esTbtcontratoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbtcontratoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TbtcontratoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbtcontratoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TbtcontratoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TbtcontratoQuery query;
	}



	[Serializable]
	public partial class TbtcontratoQuery : esTbtcontratoQuery
	{
		public TbtcontratoQuery()
		{

		}		
		
		public TbtcontratoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TbtcontratoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TbtcontratoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataMvto, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataMvto;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataRef, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataRef;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.CodCli, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataCblc, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataCblc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.NumCotr, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.NumCotr;	
			c.NumericPrecision = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.TipoMerc, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.TipoMerc;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.TipoMercBtc, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.TipoMercBtc;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.TipoOri, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.TipoOri;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.NumCotrOrig, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.NumCotrOrig;	
			c.NumericPrecision = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataOri, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataOri;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataAber, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataAber;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.QtdeAcoe, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.QtdeAcoe;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.QtdeAcoeSegr, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.QtdeAcoeSegr;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.DataVenc, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.DataVenc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.PrecMed, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.PrecMed;	
			c.NumericPrecision = 18;
			c.NumericScale = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.TaxaRemu, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.TaxaRemu;	
			c.NumericPrecision = 10;
			c.NumericScale = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.TaxaComi, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.TaxaComi;	
			c.NumericPrecision = 10;
			c.NumericScale = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.QtdeAcoeOrig, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.QtdeAcoeOrig;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.CodNeg, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.PrecDiaAnte, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.PrecDiaAnte;	
			c.NumericPrecision = 18;
			c.NumericScale = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbtcontratoMetadata.ColumnNames.IndPosi, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = TbtcontratoMetadata.PropertyNames.IndPosi;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TbtcontratoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string DataMvto = "DATA_MVTO";
			 public const string DataRef = "DATA_REF";
			 public const string CodCli = "COD_CLI";
			 public const string DataCblc = "DATA_CBLC";
			 public const string NumCotr = "NUM_COTR";
			 public const string TipoMerc = "TIPO_MERC";
			 public const string TipoMercBtc = "TIPO_MERC_BTC";
			 public const string TipoOri = "TIPO_ORI";
			 public const string NumCotrOrig = "NUM_COTR_ORIG";
			 public const string DataOri = "DATA_ORI";
			 public const string DataAber = "DATA_ABER";
			 public const string QtdeAcoe = "QTDE_ACOE";
			 public const string QtdeAcoeSegr = "QTDE_ACOE_SEGR";
			 public const string DataVenc = "DATA_VENC";
			 public const string PrecMed = "PREC_MED";
			 public const string TaxaRemu = "TAXA_REMU";
			 public const string TaxaComi = "TAXA_COMI";
			 public const string QtdeAcoeOrig = "QTDE_ACOE_ORIG";
			 public const string CodNeg = "COD_NEG";
			 public const string PrecDiaAnte = "PREC_DIA_ANTE";
			 public const string IndPosi = "IND_POSI";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string DataMvto = "DataMvto";
			 public const string DataRef = "DataRef";
			 public const string CodCli = "CodCli";
			 public const string DataCblc = "DataCblc";
			 public const string NumCotr = "NumCotr";
			 public const string TipoMerc = "TipoMerc";
			 public const string TipoMercBtc = "TipoMercBtc";
			 public const string TipoOri = "TipoOri";
			 public const string NumCotrOrig = "NumCotrOrig";
			 public const string DataOri = "DataOri";
			 public const string DataAber = "DataAber";
			 public const string QtdeAcoe = "QtdeAcoe";
			 public const string QtdeAcoeSegr = "QtdeAcoeSegr";
			 public const string DataVenc = "DataVenc";
			 public const string PrecMed = "PrecMed";
			 public const string TaxaRemu = "TaxaRemu";
			 public const string TaxaComi = "TaxaComi";
			 public const string QtdeAcoeOrig = "QtdeAcoeOrig";
			 public const string CodNeg = "CodNeg";
			 public const string PrecDiaAnte = "PrecDiaAnte";
			 public const string IndPosi = "IndPosi";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TbtcontratoMetadata))
			{
				if(TbtcontratoMetadata.mapDelegates == null)
				{
					TbtcontratoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TbtcontratoMetadata.meta == null)
				{
					TbtcontratoMetadata.meta = new TbtcontratoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_MVTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_REF", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_CBLC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_MERC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TIPO_MERC_BTC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TIPO_ORI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NUM_COTR_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_ORI", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_ABER", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("QTDE_ACOE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_ACOE_SEGR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("PREC_MED", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TAXA_REMU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TAXA_COMI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_ACOE_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("PREC_DIA_ANTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IND_POSI", new esTypeMap("VARCHAR2", "System.String"));			
				
				
				
				meta.Source = "TBTCONTRATO";
				meta.Destination = "TBTCONTRATO";
				
				meta.spInsert = "proc_TBTCONTRATOInsert";				
				meta.spUpdate = "proc_TBTCONTRATOUpdate";		
				meta.spDelete = "proc_TBTCONTRATODelete";
				meta.spLoadAll = "proc_TBTCONTRATOLoadAll";
				meta.spLoadByPrimaryKey = "proc_TBTCONTRATOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TbtcontratoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
