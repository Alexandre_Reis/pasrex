/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 18/06/2009 14:04:40
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTormovdCollection : esEntityCollection
	{
		public esTormovdCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TormovdCollection";
		}

		#region Query Logic
		protected void InitQuery(esTormovdQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTormovdQuery);
		}
		#endregion
		
		virtual public Tormovd DetachEntity(Tormovd entity)
		{
			return base.DetachEntity(entity) as Tormovd;
		}
		
		virtual public Tormovd AttachEntity(Tormovd entity)
		{
			return base.AttachEntity(entity) as Tormovd;
		}
		
		virtual public void Combine(TormovdCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tormovd this[int index]
		{
			get
			{
				return base[index] as Tormovd;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tormovd);
		}
	}



	[Serializable]
	abstract public class esTormovd : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTormovdQuery GetDynamicQuery()
		{
			return null;
		}

		public esTormovd()
		{

		}

		public esTormovd(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTormovdQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTormovdQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "InSituac": this.str.InSituac = (string)value; break;							
						case "CdBolsa": this.str.CdBolsa = (string)value; break;							
						case "DtDatord": this.str.DtDatord = (string)value; break;							
						case "NrSeqord": this.str.NrSeqord = (string)value; break;							
						case "NrSubseq": this.str.NrSubseq = (string)value; break;							
						case "DtHorord": this.str.DtHorord = (string)value; break;							
						case "TpOrdem": this.str.TpOrdem = (string)value; break;							
						case "CdMercad": this.str.CdMercad = (string)value; break;							
						case "QtOrdem": this.str.QtOrdem = (string)value; break;							
						case "CdNegocio": this.str.CdNegocio = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "DvCliente": this.str.DvCliente = (string)value; break;							
						case "CdLiqfin": this.str.CdLiqfin = (string)value; break;							
						case "InPesvin": this.str.InPesvin = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "VlPrepap": this.str.VlPrepap = (string)value; break;							
						case "TpDocfax": this.str.TpDocfax = (string)value; break;							
						case "PcRedacr": this.str.PcRedacr = (string)value; break;							
						case "QtOrdexec": this.str.QtOrdexec = (string)value; break;							
						case "CdCarliq": this.str.CdCarliq = (string)value; break;							
						case "DtValord": this.str.DtValord = (string)value; break;							
						case "InDistrib": this.str.InDistrib = (string)value; break;							
						case "CdCodusu": this.str.CdCodusu = (string)value; break;							
						case "NmCliente": this.str.NmCliente = (string)value; break;							
						case "DsObser": this.str.DsObser = (string)value; break;							
						case "InTrigger": this.str.InTrigger = (string)value; break;							
						case "TpVcoter": this.str.TpVcoter = (string)value; break;							
						case "PcFinanc": this.str.PcFinanc = (string)value; break;							
						case "VlFinanc": this.str.VlFinanc = (string)value; break;							
						case "InDessaldo": this.str.InDessaldo = (string)value; break;							
						case "NmEmitOrdem": this.str.NmEmitOrdem = (string)value; break;							
						case "QtAnexo4": this.str.QtAnexo4 = (string)value; break;							
						case "QtAnexo5": this.str.QtAnexo5 = (string)value; break;							
						case "CdCodisi": this.str.CdCodisi = (string)value; break;							
						case "CdCodcas": this.str.CdCodcas = (string)value; break;							
						case "VlDolar": this.str.VlDolar = (string)value; break;							
						case "InLiquida": this.str.InLiquida = (string)value; break;							
						case "NmApelido": this.str.NmApelido = (string)value; break;							
						case "InCasaord": this.str.InCasaord = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "InDistribui": this.str.InDistribui = (string)value; break;							
						case "QtCancofe": this.str.QtCancofe = (string)value; break;							
						case "InAdminCon": this.str.InAdminCon = (string)value; break;							
						case "NrSeqordOri": this.str.NrSeqordOri = (string)value; break;							
						case "InExterno": this.str.InExterno = (string)value; break;							
						case "InFop": this.str.InFop = (string)value; break;							
						case "DtSistema": this.str.DtSistema = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtDatord":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatord = (System.DateTime?)value;
							break;
						
						case "NrSeqord":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqord = (System.Decimal?)value;
							break;
						
						case "NrSubseq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSubseq = (System.Decimal?)value;
							break;
						
						case "DtHorord":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtHorord = (System.DateTime?)value;
							break;
						
						case "TpOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpOrdem = (System.Decimal?)value;
							break;
						
						case "QtOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtOrdem = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DvCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvCliente = (System.Decimal?)value;
							break;
						
						case "CdLiqfin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdLiqfin = (System.Decimal?)value;
							break;
						
						case "VlPrepap":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlPrepap = (System.Decimal?)value;
							break;
						
						case "TpDocfax":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpDocfax = (System.Decimal?)value;
							break;
						
						case "PcRedacr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcRedacr = (System.Decimal?)value;
							break;
						
						case "QtOrdexec":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtOrdexec = (System.Decimal?)value;
							break;
						
						case "CdCarliq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCarliq = (System.Decimal?)value;
							break;
						
						case "DtValord":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtValord = (System.DateTime?)value;
							break;
						
						case "CdCodusu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCodusu = (System.Decimal?)value;
							break;
						
						case "TpVcoter":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpVcoter = (System.Decimal?)value;
							break;
						
						case "PcFinanc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcFinanc = (System.Decimal?)value;
							break;
						
						case "VlFinanc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlFinanc = (System.Decimal?)value;
							break;
						
						case "QtAnexo4":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtAnexo4 = (System.Decimal?)value;
							break;
						
						case "QtAnexo5":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtAnexo5 = (System.Decimal?)value;
							break;
						
						case "VlDolar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDolar = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "QtCancofe":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtCancofe = (System.Decimal?)value;
							break;
						
						case "NrSeqordOri":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrSeqordOri = (System.Decimal?)value;
							break;
						
						case "DtSistema":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtSistema = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TORMOVD.IN_SITUAC
		/// </summary>
		virtual public System.String InSituac
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InSituac);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_BOLSA
		/// </summary>
		virtual public System.String CdBolsa
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdBolsa);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DT_DATORD
		/// </summary>
		virtual public System.DateTime? DtDatord
		{
			get
			{
				return base.GetSystemDateTime(TormovdMetadata.ColumnNames.DtDatord);
			}
			
			set
			{
				base.SetSystemDateTime(TormovdMetadata.ColumnNames.DtDatord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NR_SEQORD
		/// </summary>
		virtual public System.Decimal? NrSeqord
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.NrSeqord);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.NrSeqord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NR_SUBSEQ
		/// </summary>
		virtual public System.Decimal? NrSubseq
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.NrSubseq);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.NrSubseq, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DT_HORORD
		/// </summary>
		virtual public System.DateTime? DtHorord
		{
			get
			{
				return base.GetSystemDateTime(TormovdMetadata.ColumnNames.DtHorord);
			}
			
			set
			{
				base.SetSystemDateTime(TormovdMetadata.ColumnNames.DtHorord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.TP_ORDEM
		/// </summary>
		virtual public System.Decimal? TpOrdem
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.TpOrdem);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.TpOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_MERCAD
		/// </summary>
		virtual public System.String CdMercad
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdMercad);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdMercad, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.QT_ORDEM
		/// </summary>
		virtual public System.Decimal? QtOrdem
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.QtOrdem);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.QtOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_NEGOCIO
		/// </summary>
		virtual public System.String CdNegocio
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdNegocio);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DV_CLIENTE
		/// </summary>
		virtual public System.Decimal? DvCliente
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.DvCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.DvCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_LIQFIN
		/// </summary>
		virtual public System.Decimal? CdLiqfin
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.CdLiqfin);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.CdLiqfin, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_PESVIN
		/// </summary>
		virtual public System.String InPesvin
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InPesvin);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InPesvin, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.VL_PREPAP
		/// </summary>
		virtual public System.Decimal? VlPrepap
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.VlPrepap);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.VlPrepap, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.TP_DOCFAX
		/// </summary>
		virtual public System.Decimal? TpDocfax
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.TpDocfax);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.TpDocfax, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.PC_REDACR
		/// </summary>
		virtual public System.Decimal? PcRedacr
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.PcRedacr);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.PcRedacr, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.QT_ORDEXEC
		/// </summary>
		virtual public System.Decimal? QtOrdexec
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.QtOrdexec);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.QtOrdexec, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_CARLIQ
		/// </summary>
		virtual public System.Decimal? CdCarliq
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.CdCarliq);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.CdCarliq, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DT_VALORD
		/// </summary>
		virtual public System.DateTime? DtValord
		{
			get
			{
				return base.GetSystemDateTime(TormovdMetadata.ColumnNames.DtValord);
			}
			
			set
			{
				base.SetSystemDateTime(TormovdMetadata.ColumnNames.DtValord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_DISTRIB
		/// </summary>
		virtual public System.String InDistrib
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InDistrib);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InDistrib, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_CODUSU
		/// </summary>
		virtual public System.Decimal? CdCodusu
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.CdCodusu);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.CdCodusu, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NM_CLIENTE
		/// </summary>
		virtual public System.String NmCliente
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.NmCliente);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.NmCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DS_OBSER
		/// </summary>
		virtual public System.String DsObser
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.DsObser);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.DsObser, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_TRIGGER
		/// </summary>
		virtual public System.String InTrigger
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InTrigger);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InTrigger, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.TP_VCOTER
		/// </summary>
		virtual public System.Decimal? TpVcoter
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.TpVcoter);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.TpVcoter, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.PC_FINANC
		/// </summary>
		virtual public System.Decimal? PcFinanc
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.PcFinanc);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.PcFinanc, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.VL_FINANC
		/// </summary>
		virtual public System.Decimal? VlFinanc
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.VlFinanc);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.VlFinanc, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_DESSALDO
		/// </summary>
		virtual public System.String InDessaldo
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InDessaldo);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InDessaldo, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NM_EMIT_ORDEM
		/// </summary>
		virtual public System.String NmEmitOrdem
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.NmEmitOrdem);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.NmEmitOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.QT_ANEXO4
		/// </summary>
		virtual public System.Decimal? QtAnexo4
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.QtAnexo4);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.QtAnexo4, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.QT_ANEXO5
		/// </summary>
		virtual public System.Decimal? QtAnexo5
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.QtAnexo5);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.QtAnexo5, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_CODISI
		/// </summary>
		virtual public System.String CdCodisi
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdCodisi);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdCodisi, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_CODCAS
		/// </summary>
		virtual public System.String CdCodcas
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.CdCodcas);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.CdCodcas, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.VL_DOLAR
		/// </summary>
		virtual public System.Decimal? VlDolar
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.VlDolar);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.VlDolar, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_LIQUIDA
		/// </summary>
		virtual public System.String InLiquida
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InLiquida);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InLiquida, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NM_APELIDO
		/// </summary>
		virtual public System.String NmApelido
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.NmApelido);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.NmApelido, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_CASAORD
		/// </summary>
		virtual public System.String InCasaord
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InCasaord);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InCasaord, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_DISTRIBUI
		/// </summary>
		virtual public System.String InDistribui
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InDistribui);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InDistribui, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.QT_CANCOFE
		/// </summary>
		virtual public System.Decimal? QtCancofe
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.QtCancofe);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.QtCancofe, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_ADMIN_CON
		/// </summary>
		virtual public System.String InAdminCon
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InAdminCon);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InAdminCon, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.NR_SEQORD_ORI
		/// </summary>
		virtual public System.Decimal? NrSeqordOri
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.NrSeqordOri);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.NrSeqordOri, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_EXTERNO
		/// </summary>
		virtual public System.String InExterno
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InExterno);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.IN_FOP
		/// </summary>
		virtual public System.String InFop
		{
			get
			{
				return base.GetSystemString(TormovdMetadata.ColumnNames.InFop);
			}
			
			set
			{
				base.SetSystemString(TormovdMetadata.ColumnNames.InFop, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.DT_SISTEMA
		/// </summary>
		virtual public System.DateTime? DtSistema
		{
			get
			{
				return base.GetSystemDateTime(TormovdMetadata.ColumnNames.DtSistema);
			}
			
			set
			{
				base.SetSystemDateTime(TormovdMetadata.ColumnNames.DtSistema, value);
			}
		}
		
		/// <summary>
		/// Maps to TORMOVD.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TormovdMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TormovdMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTormovd entity)
			{
				this.entity = entity;
			}
			
	
			public System.String InSituac
			{
				get
				{
					System.String data = entity.InSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InSituac = null;
					else entity.InSituac = Convert.ToString(value);
				}
			}
				
			public System.String CdBolsa
			{
				get
				{
					System.String data = entity.CdBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdBolsa = null;
					else entity.CdBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DtDatord
			{
				get
				{
					System.DateTime? data = entity.DtDatord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatord = null;
					else entity.DtDatord = Convert.ToDateTime(value);
				}
			}
				
			public System.String NrSeqord
			{
				get
				{
					System.Decimal? data = entity.NrSeqord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqord = null;
					else entity.NrSeqord = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrSubseq
			{
				get
				{
					System.Decimal? data = entity.NrSubseq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSubseq = null;
					else entity.NrSubseq = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtHorord
			{
				get
				{
					System.DateTime? data = entity.DtHorord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtHorord = null;
					else entity.DtHorord = Convert.ToDateTime(value);
				}
			}
				
			public System.String TpOrdem
			{
				get
				{
					System.Decimal? data = entity.TpOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOrdem = null;
					else entity.TpOrdem = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdMercad
			{
				get
				{
					System.String data = entity.CdMercad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdMercad = null;
					else entity.CdMercad = Convert.ToString(value);
				}
			}
				
			public System.String QtOrdem
			{
				get
				{
					System.Decimal? data = entity.QtOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtOrdem = null;
					else entity.QtOrdem = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdNegocio
			{
				get
				{
					System.String data = entity.CdNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNegocio = null;
					else entity.CdNegocio = Convert.ToString(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvCliente
			{
				get
				{
					System.Decimal? data = entity.DvCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvCliente = null;
					else entity.DvCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdLiqfin
			{
				get
				{
					System.Decimal? data = entity.CdLiqfin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdLiqfin = null;
					else entity.CdLiqfin = Convert.ToDecimal(value);
				}
			}
				
			public System.String InPesvin
			{
				get
				{
					System.String data = entity.InPesvin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPesvin = null;
					else entity.InPesvin = Convert.ToString(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String VlPrepap
			{
				get
				{
					System.Decimal? data = entity.VlPrepap;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlPrepap = null;
					else entity.VlPrepap = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpDocfax
			{
				get
				{
					System.Decimal? data = entity.TpDocfax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpDocfax = null;
					else entity.TpDocfax = Convert.ToDecimal(value);
				}
			}
				
			public System.String PcRedacr
			{
				get
				{
					System.Decimal? data = entity.PcRedacr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcRedacr = null;
					else entity.PcRedacr = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtOrdexec
			{
				get
				{
					System.Decimal? data = entity.QtOrdexec;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtOrdexec = null;
					else entity.QtOrdexec = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCarliq
			{
				get
				{
					System.Decimal? data = entity.CdCarliq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCarliq = null;
					else entity.CdCarliq = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtValord
			{
				get
				{
					System.DateTime? data = entity.DtValord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtValord = null;
					else entity.DtValord = Convert.ToDateTime(value);
				}
			}
				
			public System.String InDistrib
			{
				get
				{
					System.String data = entity.InDistrib;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InDistrib = null;
					else entity.InDistrib = Convert.ToString(value);
				}
			}
				
			public System.String CdCodusu
			{
				get
				{
					System.Decimal? data = entity.CdCodusu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodusu = null;
					else entity.CdCodusu = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmCliente
			{
				get
				{
					System.String data = entity.NmCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmCliente = null;
					else entity.NmCliente = Convert.ToString(value);
				}
			}
				
			public System.String DsObser
			{
				get
				{
					System.String data = entity.DsObser;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsObser = null;
					else entity.DsObser = Convert.ToString(value);
				}
			}
				
			public System.String InTrigger
			{
				get
				{
					System.String data = entity.InTrigger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InTrigger = null;
					else entity.InTrigger = Convert.ToString(value);
				}
			}
				
			public System.String TpVcoter
			{
				get
				{
					System.Decimal? data = entity.TpVcoter;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpVcoter = null;
					else entity.TpVcoter = Convert.ToDecimal(value);
				}
			}
				
			public System.String PcFinanc
			{
				get
				{
					System.Decimal? data = entity.PcFinanc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcFinanc = null;
					else entity.PcFinanc = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlFinanc
			{
				get
				{
					System.Decimal? data = entity.VlFinanc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlFinanc = null;
					else entity.VlFinanc = Convert.ToDecimal(value);
				}
			}
				
			public System.String InDessaldo
			{
				get
				{
					System.String data = entity.InDessaldo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InDessaldo = null;
					else entity.InDessaldo = Convert.ToString(value);
				}
			}
				
			public System.String NmEmitOrdem
			{
				get
				{
					System.String data = entity.NmEmitOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEmitOrdem = null;
					else entity.NmEmitOrdem = Convert.ToString(value);
				}
			}
				
			public System.String QtAnexo4
			{
				get
				{
					System.Decimal? data = entity.QtAnexo4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtAnexo4 = null;
					else entity.QtAnexo4 = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtAnexo5
			{
				get
				{
					System.Decimal? data = entity.QtAnexo5;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtAnexo5 = null;
					else entity.QtAnexo5 = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCodisi
			{
				get
				{
					System.String data = entity.CdCodisi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodisi = null;
					else entity.CdCodisi = Convert.ToString(value);
				}
			}
				
			public System.String CdCodcas
			{
				get
				{
					System.String data = entity.CdCodcas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodcas = null;
					else entity.CdCodcas = Convert.ToString(value);
				}
			}
				
			public System.String VlDolar
			{
				get
				{
					System.Decimal? data = entity.VlDolar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDolar = null;
					else entity.VlDolar = Convert.ToDecimal(value);
				}
			}
				
			public System.String InLiquida
			{
				get
				{
					System.String data = entity.InLiquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InLiquida = null;
					else entity.InLiquida = Convert.ToString(value);
				}
			}
				
			public System.String NmApelido
			{
				get
				{
					System.String data = entity.NmApelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmApelido = null;
					else entity.NmApelido = Convert.ToString(value);
				}
			}
				
			public System.String InCasaord
			{
				get
				{
					System.String data = entity.InCasaord;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InCasaord = null;
					else entity.InCasaord = Convert.ToString(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String InDistribui
			{
				get
				{
					System.String data = entity.InDistribui;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InDistribui = null;
					else entity.InDistribui = Convert.ToString(value);
				}
			}
				
			public System.String QtCancofe
			{
				get
				{
					System.Decimal? data = entity.QtCancofe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtCancofe = null;
					else entity.QtCancofe = Convert.ToDecimal(value);
				}
			}
				
			public System.String InAdminCon
			{
				get
				{
					System.String data = entity.InAdminCon;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InAdminCon = null;
					else entity.InAdminCon = Convert.ToString(value);
				}
			}
				
			public System.String NrSeqordOri
			{
				get
				{
					System.Decimal? data = entity.NrSeqordOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrSeqordOri = null;
					else entity.NrSeqordOri = Convert.ToDecimal(value);
				}
			}
				
			public System.String InExterno
			{
				get
				{
					System.String data = entity.InExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InExterno = null;
					else entity.InExterno = Convert.ToString(value);
				}
			}
				
			public System.String InFop
			{
				get
				{
					System.String data = entity.InFop;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InFop = null;
					else entity.InFop = Convert.ToString(value);
				}
			}
				
			public System.String DtSistema
			{
				get
				{
					System.DateTime? data = entity.DtSistema;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtSistema = null;
					else entity.DtSistema = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTormovd entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTormovdQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTormovd can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tormovd : esTormovd
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTormovdQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TormovdMetadata.Meta();
			}
		}	
		

		public esQueryItem InSituac
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InSituac, esSystemType.String);
			}
		} 
		
		public esQueryItem CdBolsa
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DtDatord
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DtDatord, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem NrSeqord
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NrSeqord, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrSubseq
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NrSubseq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtHorord
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DtHorord, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TpOrdem
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.TpOrdem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdMercad
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdMercad, esSystemType.String);
			}
		} 
		
		public esQueryItem QtOrdem
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.QtOrdem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdNegocio
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvCliente
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DvCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdLiqfin
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdLiqfin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InPesvin
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InPesvin, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem VlPrepap
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.VlPrepap, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpDocfax
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.TpDocfax, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PcRedacr
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.PcRedacr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtOrdexec
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.QtOrdexec, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCarliq
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdCarliq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtValord
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DtValord, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem InDistrib
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InDistrib, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCodusu
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdCodusu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmCliente
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NmCliente, esSystemType.String);
			}
		} 
		
		public esQueryItem DsObser
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DsObser, esSystemType.String);
			}
		} 
		
		public esQueryItem InTrigger
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InTrigger, esSystemType.String);
			}
		} 
		
		public esQueryItem TpVcoter
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.TpVcoter, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PcFinanc
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.PcFinanc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlFinanc
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.VlFinanc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InDessaldo
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InDessaldo, esSystemType.String);
			}
		} 
		
		public esQueryItem NmEmitOrdem
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NmEmitOrdem, esSystemType.String);
			}
		} 
		
		public esQueryItem QtAnexo4
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.QtAnexo4, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtAnexo5
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.QtAnexo5, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCodisi
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdCodisi, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCodcas
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdCodcas, esSystemType.String);
			}
		} 
		
		public esQueryItem VlDolar
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.VlDolar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InLiquida
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InLiquida, esSystemType.String);
			}
		} 
		
		public esQueryItem NmApelido
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NmApelido, esSystemType.String);
			}
		} 
		
		public esQueryItem InCasaord
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InCasaord, esSystemType.String);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InDistribui
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InDistribui, esSystemType.String);
			}
		} 
		
		public esQueryItem QtCancofe
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.QtCancofe, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InAdminCon
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InAdminCon, esSystemType.String);
			}
		} 
		
		public esQueryItem NrSeqordOri
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.NrSeqordOri, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InExterno
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem InFop
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.InFop, esSystemType.String);
			}
		} 
		
		public esQueryItem DtSistema
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.DtSistema, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TormovdMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TormovdCollection")]
	public partial class TormovdCollection : esTormovdCollection, IEnumerable<Tormovd>
	{
		public TormovdCollection()
		{

		}
		
		public static implicit operator List<Tormovd>(TormovdCollection coll)
		{
			List<Tormovd> list = new List<Tormovd>();
			
			foreach (Tormovd emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TormovdMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TormovdQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tormovd(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tormovd();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TormovdQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TormovdQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TormovdQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tormovd AddNew()
		{
			Tormovd entity = base.AddNewEntity() as Tormovd;
			
			return entity;
		}

		public Tormovd FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tormovd;
		}


		#region IEnumerable<Tormovd> Members

		IEnumerator<Tormovd> IEnumerable<Tormovd>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tormovd;
			}
		}

		#endregion
		
		private TormovdQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TORMOVD' table
	/// </summary>

	[Serializable]
	public partial class Tormovd : esTormovd
	{
		public Tormovd()
		{

		}
	
		public Tormovd(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TormovdMetadata.Meta();
			}
		}
		
		
		
		override protected esTormovdQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TormovdQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TormovdQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TormovdQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TormovdQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TormovdQuery query;
	}



	[Serializable]
	public partial class TormovdQuery : esTormovdQuery
	{
		public TormovdQuery()
		{

		}		
		
		public TormovdQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TormovdMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TormovdMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InSituac, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InSituac;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdBolsa;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DtDatord, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TormovdMetadata.PropertyNames.DtDatord;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NrSeqord, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.NrSeqord;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NrSubseq, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.NrSubseq;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DtHorord, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TormovdMetadata.PropertyNames.DtHorord;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.TpOrdem, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.TpOrdem;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdMercad, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdMercad;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.QtOrdem, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.QtOrdem;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdNegocio, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdNegocio;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdCliente, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DvCliente, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.DvCliente;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdLiqfin, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.CdLiqfin;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InPesvin, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InPesvin;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdNatope, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.VlPrepap, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.VlPrepap;	
			c.NumericPrecision = 19;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.TpDocfax, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.TpDocfax;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.PcRedacr, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.PcRedacr;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.QtOrdexec, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.QtOrdexec;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdCarliq, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.CdCarliq;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DtValord, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TormovdMetadata.PropertyNames.DtValord;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InDistrib, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InDistrib;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdCodusu, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.CdCodusu;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NmCliente, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.NmCliente;
			c.CharacterMaxLength = 85;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DsObser, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.DsObser;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InTrigger, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InTrigger;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.TpVcoter, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.TpVcoter;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.PcFinanc, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.PcFinanc;	
			c.NumericPrecision = 10;
			c.NumericScale = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.VlFinanc, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.VlFinanc;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InDessaldo, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InDessaldo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NmEmitOrdem, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.NmEmitOrdem;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.QtAnexo4, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.QtAnexo4;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.QtAnexo5, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.QtAnexo5;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdCodisi, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdCodisi;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdCodcas, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.CdCodcas;
			c.CharacterMaxLength = 24;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.VlDolar, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.VlDolar;	
			c.NumericPrecision = 7;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InLiquida, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InLiquida;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NmApelido, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.NmApelido;
			c.CharacterMaxLength = 18;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InCasaord, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InCasaord;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.CdUsuario, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InDistribui, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InDistribui;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.QtCancofe, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.QtCancofe;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InAdminCon, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InAdminCon;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.NrSeqordOri, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.NrSeqordOri;	
			c.NumericPrecision = 9;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InExterno, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InExterno;
			c.CharacterMaxLength = 18;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.InFop, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = TormovdMetadata.PropertyNames.InFop;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.DtSistema, 46, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TormovdMetadata.PropertyNames.DtSistema;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TormovdMetadata.ColumnNames.Id, 47, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TormovdMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TormovdMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string InSituac = "IN_SITUAC";
			 public const string CdBolsa = "CD_BOLSA";
			 public const string DtDatord = "DT_DATORD";
			 public const string NrSeqord = "NR_SEQORD";
			 public const string NrSubseq = "NR_SUBSEQ";
			 public const string DtHorord = "DT_HORORD";
			 public const string TpOrdem = "TP_ORDEM";
			 public const string CdMercad = "CD_MERCAD";
			 public const string QtOrdem = "QT_ORDEM";
			 public const string CdNegocio = "CD_NEGOCIO";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string DvCliente = "DV_CLIENTE";
			 public const string CdLiqfin = "CD_LIQFIN";
			 public const string InPesvin = "IN_PESVIN";
			 public const string CdNatope = "CD_NATOPE";
			 public const string VlPrepap = "VL_PREPAP";
			 public const string TpDocfax = "TP_DOCFAX";
			 public const string PcRedacr = "PC_REDACR";
			 public const string QtOrdexec = "QT_ORDEXEC";
			 public const string CdCarliq = "CD_CARLIQ";
			 public const string DtValord = "DT_VALORD";
			 public const string InDistrib = "IN_DISTRIB";
			 public const string CdCodusu = "CD_CODUSU";
			 public const string NmCliente = "NM_CLIENTE";
			 public const string DsObser = "DS_OBSER";
			 public const string InTrigger = "IN_TRIGGER";
			 public const string TpVcoter = "TP_VCOTER";
			 public const string PcFinanc = "PC_FINANC";
			 public const string VlFinanc = "VL_FINANC";
			 public const string InDessaldo = "IN_DESSALDO";
			 public const string NmEmitOrdem = "NM_EMIT_ORDEM";
			 public const string QtAnexo4 = "QT_ANEXO4";
			 public const string QtAnexo5 = "QT_ANEXO5";
			 public const string CdCodisi = "CD_CODISI";
			 public const string CdCodcas = "CD_CODCAS";
			 public const string VlDolar = "VL_DOLAR";
			 public const string InLiquida = "IN_LIQUIDA";
			 public const string NmApelido = "NM_APELIDO";
			 public const string InCasaord = "IN_CASAORD";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string InDistribui = "IN_DISTRIBUI";
			 public const string QtCancofe = "QT_CANCOFE";
			 public const string InAdminCon = "IN_ADMIN_CON";
			 public const string NrSeqordOri = "NR_SEQORD_ORI";
			 public const string InExterno = "IN_EXTERNO";
			 public const string InFop = "IN_FOP";
			 public const string DtSistema = "DT_SISTEMA";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string InSituac = "InSituac";
			 public const string CdBolsa = "CdBolsa";
			 public const string DtDatord = "DtDatord";
			 public const string NrSeqord = "NrSeqord";
			 public const string NrSubseq = "NrSubseq";
			 public const string DtHorord = "DtHorord";
			 public const string TpOrdem = "TpOrdem";
			 public const string CdMercad = "CdMercad";
			 public const string QtOrdem = "QtOrdem";
			 public const string CdNegocio = "CdNegocio";
			 public const string CdCliente = "CdCliente";
			 public const string DvCliente = "DvCliente";
			 public const string CdLiqfin = "CdLiqfin";
			 public const string InPesvin = "InPesvin";
			 public const string CdNatope = "CdNatope";
			 public const string VlPrepap = "VlPrepap";
			 public const string TpDocfax = "TpDocfax";
			 public const string PcRedacr = "PcRedacr";
			 public const string QtOrdexec = "QtOrdexec";
			 public const string CdCarliq = "CdCarliq";
			 public const string DtValord = "DtValord";
			 public const string InDistrib = "InDistrib";
			 public const string CdCodusu = "CdCodusu";
			 public const string NmCliente = "NmCliente";
			 public const string DsObser = "DsObser";
			 public const string InTrigger = "InTrigger";
			 public const string TpVcoter = "TpVcoter";
			 public const string PcFinanc = "PcFinanc";
			 public const string VlFinanc = "VlFinanc";
			 public const string InDessaldo = "InDessaldo";
			 public const string NmEmitOrdem = "NmEmitOrdem";
			 public const string QtAnexo4 = "QtAnexo4";
			 public const string QtAnexo5 = "QtAnexo5";
			 public const string CdCodisi = "CdCodisi";
			 public const string CdCodcas = "CdCodcas";
			 public const string VlDolar = "VlDolar";
			 public const string InLiquida = "InLiquida";
			 public const string NmApelido = "NmApelido";
			 public const string InCasaord = "InCasaord";
			 public const string CdUsuario = "CdUsuario";
			 public const string InDistribui = "InDistribui";
			 public const string QtCancofe = "QtCancofe";
			 public const string InAdminCon = "InAdminCon";
			 public const string NrSeqordOri = "NrSeqordOri";
			 public const string InExterno = "InExterno";
			 public const string InFop = "InFop";
			 public const string DtSistema = "DtSistema";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TormovdMetadata))
			{
				if(TormovdMetadata.mapDelegates == null)
				{
					TormovdMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TormovdMetadata.meta == null)
				{
					TormovdMetadata.meta = new TormovdMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IN_SITUAC", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_BOLSA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_DATORD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("NR_SEQORD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_SUBSEQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_HORORD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("TP_ORDEM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_MERCAD", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("QT_ORDEM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_LIQFIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_PESVIN", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("VL_PREPAP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_DOCFAX", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PC_REDACR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QT_ORDEXEC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CARLIQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_VALORD", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("IN_DISTRIB", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_CODUSU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_CLIENTE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DS_OBSER", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_TRIGGER", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_VCOTER", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PC_FINANC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_FINANC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_DESSALDO", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NM_EMIT_ORDEM", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("QT_ANEXO4", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QT_ANEXO5", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CODISI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CODCAS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VL_DOLAR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_LIQUIDA", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_APELIDO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_CASAORD", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_DISTRIBUI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("QT_CANCOFE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_ADMIN_CON", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NR_SEQORD_ORI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_EXTERNO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_FOP", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_SISTEMA", new esTypeMap("TIMESTAMP", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TORMOVD";
				meta.Destination = "TORMOVD";
				
				meta.spInsert = "proc_TORMOVDInsert";				
				meta.spUpdate = "proc_TORMOVDUpdate";		
				meta.spDelete = "proc_TORMOVDDelete";
				meta.spLoadAll = "proc_TORMOVDLoadAll";
				meta.spLoadByPrimaryKey = "proc_TORMOVDLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TormovdMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
