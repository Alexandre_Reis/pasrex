/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 17/12/2013 10:03:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTsccligerCollection : esEntityCollection
	{
		public esTsccligerCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TsccligerCollection";
		}

		#region Query Logic
		protected void InitQuery(esTsccligerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTsccligerQuery);
		}
		#endregion
		
		virtual public Tsccliger DetachEntity(Tsccliger entity)
		{
			return base.DetachEntity(entity) as Tsccliger;
		}
		
		virtual public Tsccliger AttachEntity(Tsccliger entity)
		{
			return base.AttachEntity(entity) as Tsccliger;
		}
		
		virtual public void Combine(TsccligerCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tsccliger this[int index]
		{
			get
			{
				return base[index] as Tsccliger;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tsccliger);
		}
	}



	[Serializable]
	abstract public class esTsccliger : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTsccligerQuery GetDynamicQuery()
		{
			return null;
		}

		public esTsccliger()
		{

		}

		public esTsccliger(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTsccligerQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTsccligerQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;							
						case "NmCliente": this.str.NmCliente = (string)value; break;							
						case "TpPessoa": this.str.TpPessoa = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "InSituac": this.str.InSituac = (string)value; break;							
						case "TpCliente": this.str.TpCliente = (string)value; break;							
						case "InPessVinc": this.str.InPessVinc = (string)value; break;							
						case "InPoliticoExp": this.str.InPoliticoExp = (string)value; break;							
						case "DtNascFund": this.str.DtNascFund = (string)value; break;							
						case "CdConDep": this.str.CdConDep = (string)value; break;							
						case "DtCriacao": this.str.DtCriacao = (string)value; break;							
						case "DtAtualiz": this.str.DtAtualiz = (string)value; break;							
						case "TpImpRenda": this.str.TpImpRenda = (string)value; break;							
						case "NmResuClie": this.str.NmResuClie = (string)value; break;							
						case "CdEmpresa": this.str.CdEmpresa = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "TpOcorrencia": this.str.TpOcorrencia = (string)value; break;							
						case "VlOperacional1": this.str.VlOperacional1 = (string)value; break;							
						case "VlOperacional2": this.str.VlOperacional2 = (string)value; break;							
						case "VlOperacional3": this.str.VlOperacional3 = (string)value; break;							
						case "PcSfppeso": this.str.PcSfppeso = (string)value; break;							
						case "InSfpLiberado": this.str.InSfpLiberado = (string)value; break;							
						case "CdOrigem": this.str.CdOrigem = (string)value; break;							
						case "CdSituac": this.str.CdSituac = (string)value; break;							
						case "CdMotivo": this.str.CdMotivo = (string)value; break;							
						case "NrUnico": this.str.NrUnico = (string)value; break;							
						case "DtVlOperacional": this.str.DtVlOperacional = (string)value; break;							
						case "InPartCci": this.str.InPartCci = (string)value; break;							
						case "CdOperacCvm": this.str.CdOperacCvm = (string)value; break;							
						case "CdAssessor": this.str.CdAssessor = (string)value; break;							
						case "TpSituac": this.str.TpSituac = (string)value; break;							
						case "VlCapacidadeFin": this.str.VlCapacidadeFin = (string)value; break;							
						case "CdGrupo": this.str.CdGrupo = (string)value; break;							
						case "InCpmfNaoIncide": this.str.InCpmfNaoIncide = (string)value; break;							
						case "InNaoEmiteEtiq": this.str.InNaoEmiteEtiq = (string)value; break;							
						case "TpInvestidor": this.str.TpInvestidor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCpfcgc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfcgc = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "TpCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpCliente = (System.Decimal?)value;
							break;
						
						case "DtNascFund":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNascFund = (System.DateTime?)value;
							break;
						
						case "CdConDep":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdConDep = (System.Decimal?)value;
							break;
						
						case "DtCriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtCriacao = (System.DateTime?)value;
							break;
						
						case "DtAtualiz":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtAtualiz = (System.DateTime?)value;
							break;
						
						case "TpImpRenda":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpImpRenda = (System.Decimal?)value;
							break;
						
						case "CdEmpresa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdEmpresa = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "VlOperacional1":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlOperacional1 = (System.Decimal?)value;
							break;
						
						case "VlOperacional2":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlOperacional2 = (System.Decimal?)value;
							break;
						
						case "VlOperacional3":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlOperacional3 = (System.Decimal?)value;
							break;
						
						case "PcSfppeso":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcSfppeso = (System.Decimal?)value;
							break;
						
						case "CdOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdOrigem = (System.Decimal?)value;
							break;
						
						case "CdSituac":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdSituac = (System.Decimal?)value;
							break;
						
						case "CdMotivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdMotivo = (System.Decimal?)value;
							break;
						
						case "DtVlOperacional":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtVlOperacional = (System.DateTime?)value;
							break;
						
						case "CdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAssessor = (System.Decimal?)value;
							break;
						
						case "VlCapacidadeFin":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCapacidadeFin = (System.Decimal?)value;
							break;
						
						case "CdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdGrupo = (System.Decimal?)value;
							break;
						
						case "TpInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpInvestidor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_CPFCGC
		/// </summary>
		virtual public System.Decimal? CdCpfcgc
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdCpfcgc);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdCpfcgc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.NM_CLIENTE
		/// </summary>
		virtual public System.String NmCliente
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.NmCliente);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.NmCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_PESSOA
		/// </summary>
		virtual public System.String TpPessoa
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.TpPessoa);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.TpPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_SITUAC
		/// </summary>
		virtual public System.String InSituac
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InSituac);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_CLIENTE
		/// </summary>
		virtual public System.Decimal? TpCliente
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.TpCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.TpCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_PESS_VINC
		/// </summary>
		virtual public System.String InPessVinc
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InPessVinc);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InPessVinc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_POLITICO_EXP
		/// </summary>
		virtual public System.String InPoliticoExp
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InPoliticoExp);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InPoliticoExp, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.DT_NASC_FUND
		/// </summary>
		virtual public System.DateTime? DtNascFund
		{
			get
			{
				return base.GetSystemDateTime(TsccligerMetadata.ColumnNames.DtNascFund);
			}
			
			set
			{
				base.SetSystemDateTime(TsccligerMetadata.ColumnNames.DtNascFund, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_CON_DEP
		/// </summary>
		virtual public System.Decimal? CdConDep
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdConDep);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdConDep, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.DT_CRIACAO
		/// </summary>
		virtual public System.DateTime? DtCriacao
		{
			get
			{
				return base.GetSystemDateTime(TsccligerMetadata.ColumnNames.DtCriacao);
			}
			
			set
			{
				base.SetSystemDateTime(TsccligerMetadata.ColumnNames.DtCriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.DT_ATUALIZ
		/// </summary>
		virtual public System.DateTime? DtAtualiz
		{
			get
			{
				return base.GetSystemDateTime(TsccligerMetadata.ColumnNames.DtAtualiz);
			}
			
			set
			{
				base.SetSystemDateTime(TsccligerMetadata.ColumnNames.DtAtualiz, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_IMP_RENDA
		/// </summary>
		virtual public System.Decimal? TpImpRenda
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.TpImpRenda);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.TpImpRenda, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.NM_RESU_CLIE
		/// </summary>
		virtual public System.String NmResuClie
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.NmResuClie);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.NmResuClie, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_EMPRESA
		/// </summary>
		virtual public System.Decimal? CdEmpresa
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdEmpresa);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdEmpresa, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_OCORRENCIA
		/// </summary>
		virtual public System.String TpOcorrencia
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.TpOcorrencia);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.TpOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.VL_OPERACIONAL1
		/// </summary>
		virtual public System.Decimal? VlOperacional1
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional1);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional1, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.VL_OPERACIONAL2
		/// </summary>
		virtual public System.Decimal? VlOperacional2
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional2);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional2, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.VL_OPERACIONAL3
		/// </summary>
		virtual public System.Decimal? VlOperacional3
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional3);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.VlOperacional3, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.PC_SFPPESO
		/// </summary>
		virtual public System.Decimal? PcSfppeso
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.PcSfppeso);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.PcSfppeso, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_SFP_LIBERADO
		/// </summary>
		virtual public System.String InSfpLiberado
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InSfpLiberado);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InSfpLiberado, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_ORIGEM
		/// </summary>
		virtual public System.Decimal? CdOrigem
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdOrigem);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_SITUAC
		/// </summary>
		virtual public System.Decimal? CdSituac
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdSituac);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_MOTIVO
		/// </summary>
		virtual public System.Decimal? CdMotivo
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdMotivo);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdMotivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.NR_UNICO
		/// </summary>
		virtual public System.String NrUnico
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.NrUnico);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.NrUnico, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.DT_VL_OPERACIONAL
		/// </summary>
		virtual public System.DateTime? DtVlOperacional
		{
			get
			{
				return base.GetSystemDateTime(TsccligerMetadata.ColumnNames.DtVlOperacional);
			}
			
			set
			{
				base.SetSystemDateTime(TsccligerMetadata.ColumnNames.DtVlOperacional, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_PART_CCI
		/// </summary>
		virtual public System.String InPartCci
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InPartCci);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InPartCci, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_OPERAC_CVM
		/// </summary>
		virtual public System.String CdOperacCvm
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.CdOperacCvm);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.CdOperacCvm, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_ASSESSOR
		/// </summary>
		virtual public System.Decimal? CdAssessor
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdAssessor);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_SITUAC
		/// </summary>
		virtual public System.String TpSituac
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.TpSituac);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.TpSituac, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.VL_CAPACIDADE_FIN
		/// </summary>
		virtual public System.Decimal? VlCapacidadeFin
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.VlCapacidadeFin);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.VlCapacidadeFin, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.CD_GRUPO
		/// </summary>
		virtual public System.Decimal? CdGrupo
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.CdGrupo);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.CdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_CPMF_NAO_INCIDE
		/// </summary>
		virtual public System.String InCpmfNaoIncide
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InCpmfNaoIncide);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InCpmfNaoIncide, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.IN_NAO_EMITE_ETIQ
		/// </summary>
		virtual public System.String InNaoEmiteEtiq
		{
			get
			{
				return base.GetSystemString(TsccligerMetadata.ColumnNames.InNaoEmiteEtiq);
			}
			
			set
			{
				base.SetSystemString(TsccligerMetadata.ColumnNames.InNaoEmiteEtiq, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCCLIGER.TP_INVESTIDOR
		/// </summary>
		virtual public System.Decimal? TpInvestidor
		{
			get
			{
				return base.GetSystemDecimal(TsccligerMetadata.ColumnNames.TpInvestidor);
			}
			
			set
			{
				base.SetSystemDecimal(TsccligerMetadata.ColumnNames.TpInvestidor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTsccliger entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCpfcgc
			{
				get
				{
					System.Decimal? data = entity.CdCpfcgc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfcgc = null;
					else entity.CdCpfcgc = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmCliente
			{
				get
				{
					System.String data = entity.NmCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmCliente = null;
					else entity.NmCliente = Convert.ToString(value);
				}
			}
				
			public System.String TpPessoa
			{
				get
				{
					System.String data = entity.TpPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpPessoa = null;
					else entity.TpPessoa = Convert.ToString(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String InSituac
			{
				get
				{
					System.String data = entity.InSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InSituac = null;
					else entity.InSituac = Convert.ToString(value);
				}
			}
				
			public System.String TpCliente
			{
				get
				{
					System.Decimal? data = entity.TpCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpCliente = null;
					else entity.TpCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String InPessVinc
			{
				get
				{
					System.String data = entity.InPessVinc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPessVinc = null;
					else entity.InPessVinc = Convert.ToString(value);
				}
			}
				
			public System.String InPoliticoExp
			{
				get
				{
					System.String data = entity.InPoliticoExp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPoliticoExp = null;
					else entity.InPoliticoExp = Convert.ToString(value);
				}
			}
				
			public System.String DtNascFund
			{
				get
				{
					System.DateTime? data = entity.DtNascFund;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNascFund = null;
					else entity.DtNascFund = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdConDep
			{
				get
				{
					System.Decimal? data = entity.CdConDep;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdConDep = null;
					else entity.CdConDep = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtCriacao
			{
				get
				{
					System.DateTime? data = entity.DtCriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtCriacao = null;
					else entity.DtCriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtAtualiz
			{
				get
				{
					System.DateTime? data = entity.DtAtualiz;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtAtualiz = null;
					else entity.DtAtualiz = Convert.ToDateTime(value);
				}
			}
				
			public System.String TpImpRenda
			{
				get
				{
					System.Decimal? data = entity.TpImpRenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpImpRenda = null;
					else entity.TpImpRenda = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmResuClie
			{
				get
				{
					System.String data = entity.NmResuClie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmResuClie = null;
					else entity.NmResuClie = Convert.ToString(value);
				}
			}
				
			public System.String CdEmpresa
			{
				get
				{
					System.Decimal? data = entity.CdEmpresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEmpresa = null;
					else entity.CdEmpresa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpOcorrencia
			{
				get
				{
					System.String data = entity.TpOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOcorrencia = null;
					else entity.TpOcorrencia = Convert.ToString(value);
				}
			}
				
			public System.String VlOperacional1
			{
				get
				{
					System.Decimal? data = entity.VlOperacional1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlOperacional1 = null;
					else entity.VlOperacional1 = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlOperacional2
			{
				get
				{
					System.Decimal? data = entity.VlOperacional2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlOperacional2 = null;
					else entity.VlOperacional2 = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlOperacional3
			{
				get
				{
					System.Decimal? data = entity.VlOperacional3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlOperacional3 = null;
					else entity.VlOperacional3 = Convert.ToDecimal(value);
				}
			}
				
			public System.String PcSfppeso
			{
				get
				{
					System.Decimal? data = entity.PcSfppeso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcSfppeso = null;
					else entity.PcSfppeso = Convert.ToDecimal(value);
				}
			}
				
			public System.String InSfpLiberado
			{
				get
				{
					System.String data = entity.InSfpLiberado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InSfpLiberado = null;
					else entity.InSfpLiberado = Convert.ToString(value);
				}
			}
				
			public System.String CdOrigem
			{
				get
				{
					System.Decimal? data = entity.CdOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOrigem = null;
					else entity.CdOrigem = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdSituac
			{
				get
				{
					System.Decimal? data = entity.CdSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdSituac = null;
					else entity.CdSituac = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdMotivo
			{
				get
				{
					System.Decimal? data = entity.CdMotivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdMotivo = null;
					else entity.CdMotivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrUnico
			{
				get
				{
					System.String data = entity.NrUnico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrUnico = null;
					else entity.NrUnico = Convert.ToString(value);
				}
			}
				
			public System.String DtVlOperacional
			{
				get
				{
					System.DateTime? data = entity.DtVlOperacional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtVlOperacional = null;
					else entity.DtVlOperacional = Convert.ToDateTime(value);
				}
			}
				
			public System.String InPartCci
			{
				get
				{
					System.String data = entity.InPartCci;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPartCci = null;
					else entity.InPartCci = Convert.ToString(value);
				}
			}
				
			public System.String CdOperacCvm
			{
				get
				{
					System.String data = entity.CdOperacCvm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOperacCvm = null;
					else entity.CdOperacCvm = Convert.ToString(value);
				}
			}
				
			public System.String CdAssessor
			{
				get
				{
					System.Decimal? data = entity.CdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAssessor = null;
					else entity.CdAssessor = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpSituac
			{
				get
				{
					System.String data = entity.TpSituac;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpSituac = null;
					else entity.TpSituac = Convert.ToString(value);
				}
			}
				
			public System.String VlCapacidadeFin
			{
				get
				{
					System.Decimal? data = entity.VlCapacidadeFin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCapacidadeFin = null;
					else entity.VlCapacidadeFin = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdGrupo
			{
				get
				{
					System.Decimal? data = entity.CdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdGrupo = null;
					else entity.CdGrupo = Convert.ToDecimal(value);
				}
			}
				
			public System.String InCpmfNaoIncide
			{
				get
				{
					System.String data = entity.InCpmfNaoIncide;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InCpmfNaoIncide = null;
					else entity.InCpmfNaoIncide = Convert.ToString(value);
				}
			}
				
			public System.String InNaoEmiteEtiq
			{
				get
				{
					System.String data = entity.InNaoEmiteEtiq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InNaoEmiteEtiq = null;
					else entity.InNaoEmiteEtiq = Convert.ToString(value);
				}
			}
				
			public System.String TpInvestidor
			{
				get
				{
					System.Decimal? data = entity.TpInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpInvestidor = null;
					else entity.TpInvestidor = Convert.ToDecimal(value);
				}
			}
			

			private esTsccliger entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTsccligerQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTsccliger can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tsccliger : esTsccliger
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTsccligerQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TsccligerMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCpfcgc
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmCliente
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.NmCliente, esSystemType.String);
			}
		} 
		
		public esQueryItem TpPessoa
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpPessoa, esSystemType.String);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InSituac
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InSituac, esSystemType.String);
			}
		} 
		
		public esQueryItem TpCliente
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InPessVinc
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InPessVinc, esSystemType.String);
			}
		} 
		
		public esQueryItem InPoliticoExp
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InPoliticoExp, esSystemType.String);
			}
		} 
		
		public esQueryItem DtNascFund
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.DtNascFund, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdConDep
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdConDep, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtCriacao
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.DtCriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtAtualiz
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.DtAtualiz, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TpImpRenda
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpImpRenda, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmResuClie
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.NmResuClie, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEmpresa
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdEmpresa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpOcorrencia
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpOcorrencia, esSystemType.String);
			}
		} 
		
		public esQueryItem VlOperacional1
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.VlOperacional1, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlOperacional2
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.VlOperacional2, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlOperacional3
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.VlOperacional3, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PcSfppeso
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.PcSfppeso, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InSfpLiberado
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InSfpLiberado, esSystemType.String);
			}
		} 
		
		public esQueryItem CdOrigem
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdOrigem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdSituac
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdSituac, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdMotivo
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdMotivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrUnico
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.NrUnico, esSystemType.String);
			}
		} 
		
		public esQueryItem DtVlOperacional
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.DtVlOperacional, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem InPartCci
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InPartCci, esSystemType.String);
			}
		} 
		
		public esQueryItem CdOperacCvm
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdOperacCvm, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAssessor
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdAssessor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpSituac
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpSituac, esSystemType.String);
			}
		} 
		
		public esQueryItem VlCapacidadeFin
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.VlCapacidadeFin, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdGrupo
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.CdGrupo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InCpmfNaoIncide
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InCpmfNaoIncide, esSystemType.String);
			}
		} 
		
		public esQueryItem InNaoEmiteEtiq
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.InNaoEmiteEtiq, esSystemType.String);
			}
		} 
		
		public esQueryItem TpInvestidor
		{
			get
			{
				return new esQueryItem(this, TsccligerMetadata.ColumnNames.TpInvestidor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TsccligerCollection")]
	public partial class TsccligerCollection : esTsccligerCollection, IEnumerable<Tsccliger>
	{
		public TsccligerCollection()
		{

		}
		
		public static implicit operator List<Tsccliger>(TsccligerCollection coll)
		{
			List<Tsccliger> list = new List<Tsccliger>();
			
			foreach (Tsccliger emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TsccligerMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TsccligerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tsccliger(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tsccliger();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TsccligerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TsccligerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TsccligerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tsccliger AddNew()
		{
			Tsccliger entity = base.AddNewEntity() as Tsccliger;
			
			return entity;
		}

		public Tsccliger FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tsccliger;
		}


		#region IEnumerable<Tsccliger> Members

		IEnumerator<Tsccliger> IEnumerable<Tsccliger>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tsccliger;
			}
		}

		#endregion
		
		private TsccligerQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCCLIGER' table
	/// </summary>

	[Serializable]
	public partial class Tsccliger : esTsccliger
	{
		public Tsccliger()
		{

		}
	
		public Tsccliger(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TsccligerMetadata.Meta();
			}
		}
		
		
		
		override protected esTsccligerQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TsccligerQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TsccligerQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TsccligerQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TsccligerQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TsccligerQuery query;
	}



	[Serializable]
	public partial class TsccligerQuery : esTsccligerQuery
	{
		public TsccligerQuery()
		{

		}		
		
		public TsccligerQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TsccligerMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TsccligerMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdCpfcgc, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdCpfcgc;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.NmCliente, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.NmCliente;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpPessoa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpPessoa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.Id, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InSituac, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InSituac;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpCliente, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpCliente;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InPessVinc, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InPessVinc;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InPoliticoExp, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InPoliticoExp;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.DtNascFund, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TsccligerMetadata.PropertyNames.DtNascFund;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdConDep, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdConDep;	
			c.NumericPrecision = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.DtCriacao, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TsccligerMetadata.PropertyNames.DtCriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.DtAtualiz, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TsccligerMetadata.PropertyNames.DtAtualiz;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpImpRenda, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpImpRenda;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.NmResuClie, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.NmResuClie;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdEmpresa, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdEmpresa;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdUsuario, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpOcorrencia, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpOcorrencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.VlOperacional1, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.VlOperacional1;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.VlOperacional2, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.VlOperacional2;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.VlOperacional3, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.VlOperacional3;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.PcSfppeso, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.PcSfppeso;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InSfpLiberado, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InSfpLiberado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdOrigem, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdOrigem;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdSituac, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdSituac;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdMotivo, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdMotivo;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.NrUnico, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.NrUnico;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.DtVlOperacional, 26, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TsccligerMetadata.PropertyNames.DtVlOperacional;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InPartCci, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InPartCci;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdOperacCvm, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdOperacCvm;
			c.CharacterMaxLength = 19;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdAssessor, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdAssessor;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpSituac, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpSituac;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.VlCapacidadeFin, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.VlCapacidadeFin;	
			c.NumericPrecision = 17;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.CdGrupo, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.CdGrupo;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InCpmfNaoIncide, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InCpmfNaoIncide;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.InNaoEmiteEtiq, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = TsccligerMetadata.PropertyNames.InNaoEmiteEtiq;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TsccligerMetadata.ColumnNames.TpInvestidor, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TsccligerMetadata.PropertyNames.TpInvestidor;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TsccligerMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCpfcgc = "CD_CPFCGC";
			 public const string NmCliente = "NM_CLIENTE";
			 public const string TpPessoa = "TP_PESSOA";
			 public const string Id = "ID";
			 public const string InSituac = "IN_SITUAC";
			 public const string TpCliente = "TP_CLIENTE";
			 public const string InPessVinc = "IN_PESS_VINC";
			 public const string InPoliticoExp = "IN_POLITICO_EXP";
			 public const string DtNascFund = "DT_NASC_FUND";
			 public const string CdConDep = "CD_CON_DEP";
			 public const string DtCriacao = "DT_CRIACAO";
			 public const string DtAtualiz = "DT_ATUALIZ";
			 public const string TpImpRenda = "TP_IMP_RENDA";
			 public const string NmResuClie = "NM_RESU_CLIE";
			 public const string CdEmpresa = "CD_EMPRESA";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string TpOcorrencia = "TP_OCORRENCIA";
			 public const string VlOperacional1 = "VL_OPERACIONAL1";
			 public const string VlOperacional2 = "VL_OPERACIONAL2";
			 public const string VlOperacional3 = "VL_OPERACIONAL3";
			 public const string PcSfppeso = "PC_SFPPESO";
			 public const string InSfpLiberado = "IN_SFP_LIBERADO";
			 public const string CdOrigem = "CD_ORIGEM";
			 public const string CdSituac = "CD_SITUAC";
			 public const string CdMotivo = "CD_MOTIVO";
			 public const string NrUnico = "NR_UNICO";
			 public const string DtVlOperacional = "DT_VL_OPERACIONAL";
			 public const string InPartCci = "IN_PART_CCI";
			 public const string CdOperacCvm = "CD_OPERAC_CVM";
			 public const string CdAssessor = "CD_ASSESSOR";
			 public const string TpSituac = "TP_SITUAC";
			 public const string VlCapacidadeFin = "VL_CAPACIDADE_FIN";
			 public const string CdGrupo = "CD_GRUPO";
			 public const string InCpmfNaoIncide = "IN_CPMF_NAO_INCIDE";
			 public const string InNaoEmiteEtiq = "IN_NAO_EMITE_ETIQ";
			 public const string TpInvestidor = "TP_INVESTIDOR";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCpfcgc = "CdCpfcgc";
			 public const string NmCliente = "NmCliente";
			 public const string TpPessoa = "TpPessoa";
			 public const string Id = "Id";
			 public const string InSituac = "InSituac";
			 public const string TpCliente = "TpCliente";
			 public const string InPessVinc = "InPessVinc";
			 public const string InPoliticoExp = "InPoliticoExp";
			 public const string DtNascFund = "DtNascFund";
			 public const string CdConDep = "CdConDep";
			 public const string DtCriacao = "DtCriacao";
			 public const string DtAtualiz = "DtAtualiz";
			 public const string TpImpRenda = "TpImpRenda";
			 public const string NmResuClie = "NmResuClie";
			 public const string CdEmpresa = "CdEmpresa";
			 public const string CdUsuario = "CdUsuario";
			 public const string TpOcorrencia = "TpOcorrencia";
			 public const string VlOperacional1 = "VlOperacional1";
			 public const string VlOperacional2 = "VlOperacional2";
			 public const string VlOperacional3 = "VlOperacional3";
			 public const string PcSfppeso = "PcSfppeso";
			 public const string InSfpLiberado = "InSfpLiberado";
			 public const string CdOrigem = "CdOrigem";
			 public const string CdSituac = "CdSituac";
			 public const string CdMotivo = "CdMotivo";
			 public const string NrUnico = "NrUnico";
			 public const string DtVlOperacional = "DtVlOperacional";
			 public const string InPartCci = "InPartCci";
			 public const string CdOperacCvm = "CdOperacCvm";
			 public const string CdAssessor = "CdAssessor";
			 public const string TpSituac = "TpSituac";
			 public const string VlCapacidadeFin = "VlCapacidadeFin";
			 public const string CdGrupo = "CdGrupo";
			 public const string InCpmfNaoIncide = "InCpmfNaoIncide";
			 public const string InNaoEmiteEtiq = "InNaoEmiteEtiq";
			 public const string TpInvestidor = "TpInvestidor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TsccligerMetadata))
			{
				if(TsccligerMetadata.mapDelegates == null)
				{
					TsccligerMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TsccligerMetadata.meta == null)
				{
					TsccligerMetadata.meta = new TsccligerMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_CLIENTE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TP_PESSOA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_SITUAC", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_PESS_VINC", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("IN_POLITICO_EXP", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("DT_NASC_FUND", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_CON_DEP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_CRIACAO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_ATUALIZ", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("TP_IMP_RENDA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_RESU_CLIE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_EMPRESA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_OCORRENCIA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("VL_OPERACIONAL1", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_OPERACIONAL2", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_OPERACIONAL3", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PC_SFPPESO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_SFP_LIBERADO", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_ORIGEM", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_SITUAC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_MOTIVO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_UNICO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_VL_OPERACIONAL", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("IN_PART_CCI", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_OPERAC_CVM", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_ASSESSOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_SITUAC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VL_CAPACIDADE_FIN", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_GRUPO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_CPMF_NAO_INCIDE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("IN_NAO_EMITE_ETIQ", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_INVESTIDOR", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TSCCLIGER";
				meta.Destination = "TSCCLIGER";
				
				meta.spInsert = "proc_TSCCLIGERInsert";				
				meta.spUpdate = "proc_TSCCLIGERUpdate";		
				meta.spDelete = "proc_TSCCLIGERDelete";
				meta.spLoadAll = "proc_TSCCLIGERLoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCCLIGERLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TsccligerMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
