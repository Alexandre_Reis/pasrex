/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 18/06/2009 14:04:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTornegdCollection : esEntityCollection
	{
		public esTornegdCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TornegdCollection";
		}

		#region Query Logic
		protected void InitQuery(esTornegdQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTornegdQuery);
		}
		#endregion
		
		virtual public Tornegd DetachEntity(Tornegd entity)
		{
			return base.DetachEntity(entity) as Tornegd;
		}
		
		virtual public Tornegd AttachEntity(Tornegd entity)
		{
			return base.AttachEntity(entity) as Tornegd;
		}
		
		virtual public void Combine(TornegdCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tornegd this[int index]
		{
			get
			{
				return base[index] as Tornegd;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tornegd);
		}
	}



	[Serializable]
	abstract public class esTornegd : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTornegdQuery GetDynamicQuery()
		{
			return null;
		}

		public esTornegd()
		{

		}

		public esTornegd(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTornegdQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTornegdQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DtMovimento": this.str.DtMovimento = (string)value; break;							
						case "DtPregao": this.str.DtPregao = (string)value; break;							
						case "HhNegocio": this.str.HhNegocio = (string)value; break;							
						case "CdBolsamov": this.str.CdBolsamov = (string)value; break;							
						case "CdCorresp": this.str.CdCorresp = (string)value; break;							
						case "PcComissaoCorresp": this.str.PcComissaoCorresp = (string)value; break;							
						case "NrNegocio": this.str.NrNegocio = (string)value; break;							
						case "DvNegocio": this.str.DvNegocio = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "TpMercado": this.str.TpMercado = (string)value; break;							
						case "CdNegocio": this.str.CdNegocio = (string)value; break;							
						case "CdEspecif": this.str.CdEspecif = (string)value; break;							
						case "NmNomres": this.str.NmNomres = (string)value; break;							
						case "QtNegocio": this.str.QtNegocio = (string)value; break;							
						case "QtEspecificada": this.str.QtEspecificada = (string)value; break;							
						case "VlNegocio": this.str.VlNegocio = (string)value; break;							
						case "TpTermo": this.str.TpTermo = (string)value; break;							
						case "TpOrigem": this.str.TpOrigem = (string)value; break;							
						case "CdContraparte": this.str.CdContraparte = (string)value; break;							
						case "DsObs": this.str.DsObs = (string)value; break;							
						case "FtValorizacao": this.str.FtValorizacao = (string)value; break;							
						case "DtOpcoes": this.str.DtOpcoes = (string)value; break;							
						case "TpGeraoferta": this.str.TpGeraoferta = (string)value; break;							
						case "QtCasada": this.str.QtCasada = (string)value; break;							
						case "TpVcoter": this.str.TpVcoter = (string)value; break;							
						case "InOrineg": this.str.InOrineg = (string)value; break;							
						case "TpLiquid": this.str.TpLiquid = (string)value; break;							
						case "CdTitobj": this.str.CdTitobj = (string)value; break;							
						case "CdCodisi": this.str.CdCodisi = (string)value; break;							
						case "CdCodcas": this.str.CdCodcas = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "InAfterm": this.str.InAfterm = (string)value; break;							
						case "IdLiquid": this.str.IdLiquid = (string)value; break;							
						case "NrOfcpmega": this.str.NrOfcpmega = (string)value; break;							
						case "NrOfvdmega": this.str.NrOfvdmega = (string)value; break;							
						case "NrOfemega": this.str.NrOfemega = (string)value; break;							
						case "CdOperaMega": this.str.CdOperaMega = (string)value; break;							
						case "FtLiqnot": this.str.FtLiqnot = (string)value; break;							
						case "IdTpliq": this.str.IdTpliq = (string)value; break;							
						case "InBrokeragem": this.str.InBrokeragem = (string)value; break;							
						case "CdOfestr1": this.str.CdOfestr1 = (string)value; break;							
						case "CdOfestr2": this.str.CdOfestr2 = (string)value; break;							
						case "DtSistema": this.str.DtSistema = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtMovimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtMovimento = (System.DateTime?)value;
							break;
						
						case "DtPregao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtPregao = (System.DateTime?)value;
							break;
						
						case "CdCorresp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCorresp = (System.Decimal?)value;
							break;
						
						case "PcComissaoCorresp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PcComissaoCorresp = (System.Decimal?)value;
							break;
						
						case "NrNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNegocio = (System.Decimal?)value;
							break;
						
						case "DvNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DvNegocio = (System.Decimal?)value;
							break;
						
						case "QtNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtNegocio = (System.Decimal?)value;
							break;
						
						case "QtEspecificada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtEspecificada = (System.Decimal?)value;
							break;
						
						case "VlNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlNegocio = (System.Decimal?)value;
							break;
						
						case "CdContraparte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdContraparte = (System.Decimal?)value;
							break;
						
						case "FtValorizacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FtValorizacao = (System.Decimal?)value;
							break;
						
						case "DtOpcoes":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtOpcoes = (System.DateTime?)value;
							break;
						
						case "QtCasada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtCasada = (System.Decimal?)value;
							break;
						
						case "TpVcoter":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpVcoter = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "IdLiquid":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IdLiquid = (System.Decimal?)value;
							break;
						
						case "NrOfcpmega":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrOfcpmega = (System.Decimal?)value;
							break;
						
						case "NrOfvdmega":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrOfvdmega = (System.Decimal?)value;
							break;
						
						case "NrOfemega":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrOfemega = (System.Decimal?)value;
							break;
						
						case "CdOperaMega":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdOperaMega = (System.Decimal?)value;
							break;
						
						case "FtLiqnot":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FtLiqnot = (System.Decimal?)value;
							break;
						
						case "IdTpliq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IdTpliq = (System.Decimal?)value;
							break;
						
						case "DtSistema":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtSistema = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TORNEGD.DT_MOVIMENTO
		/// </summary>
		virtual public System.DateTime? DtMovimento
		{
			get
			{
				return base.GetSystemDateTime(TornegdMetadata.ColumnNames.DtMovimento);
			}
			
			set
			{
				base.SetSystemDateTime(TornegdMetadata.ColumnNames.DtMovimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.DT_PREGAO
		/// </summary>
		virtual public System.DateTime? DtPregao
		{
			get
			{
				return base.GetSystemDateTime(TornegdMetadata.ColumnNames.DtPregao);
			}
			
			set
			{
				base.SetSystemDateTime(TornegdMetadata.ColumnNames.DtPregao, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.HH_NEGOCIO
		/// </summary>
		virtual public System.String HhNegocio
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.HhNegocio);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.HhNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_BOLSAMOV
		/// </summary>
		virtual public System.String CdBolsamov
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdBolsamov);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdBolsamov, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_CORRESP
		/// </summary>
		virtual public System.Decimal? CdCorresp
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.CdCorresp);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.CdCorresp, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.PC_COMISSAO_CORRESP
		/// </summary>
		virtual public System.Decimal? PcComissaoCorresp
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.PcComissaoCorresp);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.PcComissaoCorresp, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.NR_NEGOCIO
		/// </summary>
		virtual public System.Decimal? NrNegocio
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.NrNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.NrNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.DV_NEGOCIO
		/// </summary>
		virtual public System.Decimal? DvNegocio
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.DvNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.DvNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_MERCADO
		/// </summary>
		virtual public System.String TpMercado
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.TpMercado);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.TpMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_NEGOCIO
		/// </summary>
		virtual public System.String CdNegocio
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdNegocio);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_ESPECIF
		/// </summary>
		virtual public System.String CdEspecif
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdEspecif);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdEspecif, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.NM_NOMRES
		/// </summary>
		virtual public System.String NmNomres
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.NmNomres);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.NmNomres, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.QT_NEGOCIO
		/// </summary>
		virtual public System.Decimal? QtNegocio
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.QtNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.QtNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.QT_ESPECIFICADA
		/// </summary>
		virtual public System.Decimal? QtEspecificada
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.QtEspecificada);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.QtEspecificada, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.VL_NEGOCIO
		/// </summary>
		virtual public System.Decimal? VlNegocio
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.VlNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.VlNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_TERMO
		/// </summary>
		virtual public System.String TpTermo
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.TpTermo);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.TpTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_ORIGEM
		/// </summary>
		virtual public System.String TpOrigem
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.TpOrigem);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.TpOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_CONTRAPARTE
		/// </summary>
		virtual public System.Decimal? CdContraparte
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.CdContraparte);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.CdContraparte, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.DS_OBS
		/// </summary>
		virtual public System.String DsObs
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.DsObs);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.DsObs, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.FT_VALORIZACAO
		/// </summary>
		virtual public System.Decimal? FtValorizacao
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.FtValorizacao);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.FtValorizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.DT_OPCOES
		/// </summary>
		virtual public System.DateTime? DtOpcoes
		{
			get
			{
				return base.GetSystemDateTime(TornegdMetadata.ColumnNames.DtOpcoes);
			}
			
			set
			{
				base.SetSystemDateTime(TornegdMetadata.ColumnNames.DtOpcoes, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_GERAOFERTA
		/// </summary>
		virtual public System.String TpGeraoferta
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.TpGeraoferta);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.TpGeraoferta, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.QT_CASADA
		/// </summary>
		virtual public System.Decimal? QtCasada
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.QtCasada);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.QtCasada, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_VCOTER
		/// </summary>
		virtual public System.Decimal? TpVcoter
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.TpVcoter);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.TpVcoter, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.IN_ORINEG
		/// </summary>
		virtual public System.String InOrineg
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.InOrineg);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.InOrineg, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.TP_LIQUID
		/// </summary>
		virtual public System.String TpLiquid
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.TpLiquid);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.TpLiquid, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_TITOBJ
		/// </summary>
		virtual public System.String CdTitobj
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdTitobj);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdTitobj, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_CODISI
		/// </summary>
		virtual public System.String CdCodisi
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdCodisi);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdCodisi, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_CODCAS
		/// </summary>
		virtual public System.String CdCodcas
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdCodcas);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdCodcas, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.IN_AFTERM
		/// </summary>
		virtual public System.String InAfterm
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.InAfterm);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.InAfterm, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.ID_LIQUID
		/// </summary>
		virtual public System.Decimal? IdLiquid
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.IdLiquid);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.IdLiquid, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.NR_OFCPMEGA
		/// </summary>
		virtual public System.Decimal? NrOfcpmega
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.NrOfcpmega);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.NrOfcpmega, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.NR_OFVDMEGA
		/// </summary>
		virtual public System.Decimal? NrOfvdmega
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.NrOfvdmega);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.NrOfvdmega, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.NR_OFEMEGA
		/// </summary>
		virtual public System.Decimal? NrOfemega
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.NrOfemega);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.NrOfemega, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_OPERA_MEGA
		/// </summary>
		virtual public System.Decimal? CdOperaMega
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.CdOperaMega);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.CdOperaMega, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.FT_LIQNOT
		/// </summary>
		virtual public System.Decimal? FtLiqnot
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.FtLiqnot);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.FtLiqnot, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.ID_TPLIQ
		/// </summary>
		virtual public System.Decimal? IdTpliq
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.IdTpliq);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.IdTpliq, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.IN_BROKERAGEM
		/// </summary>
		virtual public System.String InBrokeragem
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.InBrokeragem);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.InBrokeragem, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_OFESTR1
		/// </summary>
		virtual public System.String CdOfestr1
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdOfestr1);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdOfestr1, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.CD_OFESTR2
		/// </summary>
		virtual public System.String CdOfestr2
		{
			get
			{
				return base.GetSystemString(TornegdMetadata.ColumnNames.CdOfestr2);
			}
			
			set
			{
				base.SetSystemString(TornegdMetadata.ColumnNames.CdOfestr2, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.DT_SISTEMA
		/// </summary>
		virtual public System.DateTime? DtSistema
		{
			get
			{
				return base.GetSystemDateTime(TornegdMetadata.ColumnNames.DtSistema);
			}
			
			set
			{
				base.SetSystemDateTime(TornegdMetadata.ColumnNames.DtSistema, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGD.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TornegdMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TornegdMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTornegd entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DtMovimento
			{
				get
				{
					System.DateTime? data = entity.DtMovimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtMovimento = null;
					else entity.DtMovimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtPregao
			{
				get
				{
					System.DateTime? data = entity.DtPregao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtPregao = null;
					else entity.DtPregao = Convert.ToDateTime(value);
				}
			}
				
			public System.String HhNegocio
			{
				get
				{
					System.String data = entity.HhNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HhNegocio = null;
					else entity.HhNegocio = Convert.ToString(value);
				}
			}
				
			public System.String CdBolsamov
			{
				get
				{
					System.String data = entity.CdBolsamov;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdBolsamov = null;
					else entity.CdBolsamov = Convert.ToString(value);
				}
			}
				
			public System.String CdCorresp
			{
				get
				{
					System.Decimal? data = entity.CdCorresp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCorresp = null;
					else entity.CdCorresp = Convert.ToDecimal(value);
				}
			}
				
			public System.String PcComissaoCorresp
			{
				get
				{
					System.Decimal? data = entity.PcComissaoCorresp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PcComissaoCorresp = null;
					else entity.PcComissaoCorresp = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrNegocio
			{
				get
				{
					System.Decimal? data = entity.NrNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNegocio = null;
					else entity.NrNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String DvNegocio
			{
				get
				{
					System.Decimal? data = entity.DvNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DvNegocio = null;
					else entity.DvNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String TpMercado
			{
				get
				{
					System.String data = entity.TpMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpMercado = null;
					else entity.TpMercado = Convert.ToString(value);
				}
			}
				
			public System.String CdNegocio
			{
				get
				{
					System.String data = entity.CdNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNegocio = null;
					else entity.CdNegocio = Convert.ToString(value);
				}
			}
				
			public System.String CdEspecif
			{
				get
				{
					System.String data = entity.CdEspecif;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdEspecif = null;
					else entity.CdEspecif = Convert.ToString(value);
				}
			}
				
			public System.String NmNomres
			{
				get
				{
					System.String data = entity.NmNomres;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmNomres = null;
					else entity.NmNomres = Convert.ToString(value);
				}
			}
				
			public System.String QtNegocio
			{
				get
				{
					System.Decimal? data = entity.QtNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtNegocio = null;
					else entity.QtNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtEspecificada
			{
				get
				{
					System.Decimal? data = entity.QtEspecificada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtEspecificada = null;
					else entity.QtEspecificada = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlNegocio
			{
				get
				{
					System.Decimal? data = entity.VlNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlNegocio = null;
					else entity.VlNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpTermo
			{
				get
				{
					System.String data = entity.TpTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpTermo = null;
					else entity.TpTermo = Convert.ToString(value);
				}
			}
				
			public System.String TpOrigem
			{
				get
				{
					System.String data = entity.TpOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpOrigem = null;
					else entity.TpOrigem = Convert.ToString(value);
				}
			}
				
			public System.String CdContraparte
			{
				get
				{
					System.Decimal? data = entity.CdContraparte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdContraparte = null;
					else entity.CdContraparte = Convert.ToDecimal(value);
				}
			}
				
			public System.String DsObs
			{
				get
				{
					System.String data = entity.DsObs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsObs = null;
					else entity.DsObs = Convert.ToString(value);
				}
			}
				
			public System.String FtValorizacao
			{
				get
				{
					System.Decimal? data = entity.FtValorizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FtValorizacao = null;
					else entity.FtValorizacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtOpcoes
			{
				get
				{
					System.DateTime? data = entity.DtOpcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtOpcoes = null;
					else entity.DtOpcoes = Convert.ToDateTime(value);
				}
			}
				
			public System.String TpGeraoferta
			{
				get
				{
					System.String data = entity.TpGeraoferta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpGeraoferta = null;
					else entity.TpGeraoferta = Convert.ToString(value);
				}
			}
				
			public System.String QtCasada
			{
				get
				{
					System.Decimal? data = entity.QtCasada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtCasada = null;
					else entity.QtCasada = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpVcoter
			{
				get
				{
					System.Decimal? data = entity.TpVcoter;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpVcoter = null;
					else entity.TpVcoter = Convert.ToDecimal(value);
				}
			}
				
			public System.String InOrineg
			{
				get
				{
					System.String data = entity.InOrineg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InOrineg = null;
					else entity.InOrineg = Convert.ToString(value);
				}
			}
				
			public System.String TpLiquid
			{
				get
				{
					System.String data = entity.TpLiquid;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpLiquid = null;
					else entity.TpLiquid = Convert.ToString(value);
				}
			}
				
			public System.String CdTitobj
			{
				get
				{
					System.String data = entity.CdTitobj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdTitobj = null;
					else entity.CdTitobj = Convert.ToString(value);
				}
			}
				
			public System.String CdCodisi
			{
				get
				{
					System.String data = entity.CdCodisi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodisi = null;
					else entity.CdCodisi = Convert.ToString(value);
				}
			}
				
			public System.String CdCodcas
			{
				get
				{
					System.String data = entity.CdCodcas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodcas = null;
					else entity.CdCodcas = Convert.ToString(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String InAfterm
			{
				get
				{
					System.String data = entity.InAfterm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InAfterm = null;
					else entity.InAfterm = Convert.ToString(value);
				}
			}
				
			public System.String IdLiquid
			{
				get
				{
					System.Decimal? data = entity.IdLiquid;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquid = null;
					else entity.IdLiquid = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrOfcpmega
			{
				get
				{
					System.Decimal? data = entity.NrOfcpmega;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrOfcpmega = null;
					else entity.NrOfcpmega = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrOfvdmega
			{
				get
				{
					System.Decimal? data = entity.NrOfvdmega;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrOfvdmega = null;
					else entity.NrOfvdmega = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrOfemega
			{
				get
				{
					System.Decimal? data = entity.NrOfemega;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrOfemega = null;
					else entity.NrOfemega = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdOperaMega
			{
				get
				{
					System.Decimal? data = entity.CdOperaMega;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOperaMega = null;
					else entity.CdOperaMega = Convert.ToDecimal(value);
				}
			}
				
			public System.String FtLiqnot
			{
				get
				{
					System.Decimal? data = entity.FtLiqnot;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FtLiqnot = null;
					else entity.FtLiqnot = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTpliq
			{
				get
				{
					System.Decimal? data = entity.IdTpliq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTpliq = null;
					else entity.IdTpliq = Convert.ToDecimal(value);
				}
			}
				
			public System.String InBrokeragem
			{
				get
				{
					System.String data = entity.InBrokeragem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InBrokeragem = null;
					else entity.InBrokeragem = Convert.ToString(value);
				}
			}
				
			public System.String CdOfestr1
			{
				get
				{
					System.String data = entity.CdOfestr1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOfestr1 = null;
					else entity.CdOfestr1 = Convert.ToString(value);
				}
			}
				
			public System.String CdOfestr2
			{
				get
				{
					System.String data = entity.CdOfestr2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdOfestr2 = null;
					else entity.CdOfestr2 = Convert.ToString(value);
				}
			}
				
			public System.String DtSistema
			{
				get
				{
					System.DateTime? data = entity.DtSistema;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtSistema = null;
					else entity.DtSistema = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTornegd entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTornegdQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTornegd can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tornegd : esTornegd
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTornegdQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TornegdMetadata.Meta();
			}
		}	
		

		public esQueryItem DtMovimento
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DtMovimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtPregao
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DtPregao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HhNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.HhNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdBolsamov
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdBolsamov, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCorresp
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdCorresp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PcComissaoCorresp
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.PcComissaoCorresp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.NrNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DvNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DvNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem TpMercado
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdEspecif
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdEspecif, esSystemType.String);
			}
		} 
		
		public esQueryItem NmNomres
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.NmNomres, esSystemType.String);
			}
		} 
		
		public esQueryItem QtNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.QtNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtEspecificada
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.QtEspecificada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlNegocio
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.VlNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpTermo
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpTermo, esSystemType.String);
			}
		} 
		
		public esQueryItem TpOrigem
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpOrigem, esSystemType.String);
			}
		} 
		
		public esQueryItem CdContraparte
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdContraparte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DsObs
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DsObs, esSystemType.String);
			}
		} 
		
		public esQueryItem FtValorizacao
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.FtValorizacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtOpcoes
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DtOpcoes, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TpGeraoferta
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpGeraoferta, esSystemType.String);
			}
		} 
		
		public esQueryItem QtCasada
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.QtCasada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpVcoter
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpVcoter, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InOrineg
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.InOrineg, esSystemType.String);
			}
		} 
		
		public esQueryItem TpLiquid
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.TpLiquid, esSystemType.String);
			}
		} 
		
		public esQueryItem CdTitobj
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdTitobj, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCodisi
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdCodisi, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCodcas
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdCodcas, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InAfterm
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.InAfterm, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLiquid
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.IdLiquid, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrOfcpmega
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.NrOfcpmega, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrOfvdmega
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.NrOfvdmega, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrOfemega
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.NrOfemega, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdOperaMega
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdOperaMega, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FtLiqnot
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.FtLiqnot, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTpliq
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.IdTpliq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem InBrokeragem
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.InBrokeragem, esSystemType.String);
			}
		} 
		
		public esQueryItem CdOfestr1
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdOfestr1, esSystemType.String);
			}
		} 
		
		public esQueryItem CdOfestr2
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.CdOfestr2, esSystemType.String);
			}
		} 
		
		public esQueryItem DtSistema
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.DtSistema, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TornegdMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TornegdCollection")]
	public partial class TornegdCollection : esTornegdCollection, IEnumerable<Tornegd>
	{
		public TornegdCollection()
		{

		}
		
		public static implicit operator List<Tornegd>(TornegdCollection coll)
		{
			List<Tornegd> list = new List<Tornegd>();
			
			foreach (Tornegd emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TornegdMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TornegdQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tornegd(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tornegd();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TornegdQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TornegdQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TornegdQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tornegd AddNew()
		{
			Tornegd entity = base.AddNewEntity() as Tornegd;
			
			return entity;
		}

		public Tornegd FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tornegd;
		}


		#region IEnumerable<Tornegd> Members

		IEnumerator<Tornegd> IEnumerable<Tornegd>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tornegd;
			}
		}

		#endregion
		
		private TornegdQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TORNEGD' table
	/// </summary>

	[Serializable]
	public partial class Tornegd : esTornegd
	{
		public Tornegd()
		{

		}
	
		public Tornegd(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TornegdMetadata.Meta();
			}
		}
		
		
		
		override protected esTornegdQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TornegdQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TornegdQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TornegdQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TornegdQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TornegdQuery query;
	}



	[Serializable]
	public partial class TornegdQuery : esTornegdQuery
	{
		public TornegdQuery()
		{

		}		
		
		public TornegdQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TornegdMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TornegdMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DtMovimento, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TornegdMetadata.PropertyNames.DtMovimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DtPregao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TornegdMetadata.PropertyNames.DtPregao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.HhNegocio, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.HhNegocio;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdBolsamov, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdBolsamov;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdCorresp, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.CdCorresp;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.PcComissaoCorresp, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.PcComissaoCorresp;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.NrNegocio, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.NrNegocio;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DvNegocio, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.DvNegocio;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdNatope, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpMercado, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.TpMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdNegocio, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdNegocio;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdEspecif, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdEspecif;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.NmNomres, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.NmNomres;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.QtNegocio, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.QtNegocio;	
			c.NumericPrecision = 15;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.QtEspecificada, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.QtEspecificada;	
			c.NumericPrecision = 15;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.VlNegocio, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.VlNegocio;	
			c.NumericPrecision = 19;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpTermo, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.TpTermo;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpOrigem, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.TpOrigem;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdContraparte, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.CdContraparte;	
			c.NumericPrecision = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DsObs, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.DsObs;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.FtValorizacao, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.FtValorizacao;	
			c.NumericPrecision = 15;
			c.NumericScale = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DtOpcoes, 21, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TornegdMetadata.PropertyNames.DtOpcoes;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpGeraoferta, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.TpGeraoferta;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.QtCasada, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.QtCasada;	
			c.NumericPrecision = 15;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpVcoter, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.TpVcoter;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.InOrineg, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.InOrineg;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.TpLiquid, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.TpLiquid;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdTitobj, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdTitobj;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdCodisi, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdCodisi;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdCodcas, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdCodcas;
			c.CharacterMaxLength = 24;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdCliente, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.InAfterm, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.InAfterm;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.IdLiquid, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.IdLiquid;	
			c.NumericPrecision = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.NrOfcpmega, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.NrOfcpmega;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.NrOfvdmega, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.NrOfvdmega;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.NrOfemega, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.NrOfemega;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdOperaMega, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.CdOperaMega;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.FtLiqnot, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.FtLiqnot;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.IdTpliq, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.IdTpliq;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.InBrokeragem, 39, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.InBrokeragem;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdOfestr1, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdOfestr1;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.CdOfestr2, 41, typeof(System.String), esSystemType.String);
			c.PropertyName = TornegdMetadata.PropertyNames.CdOfestr2;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.DtSistema, 42, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TornegdMetadata.PropertyNames.DtSistema;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TornegdMetadata.ColumnNames.Id, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TornegdMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TornegdMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DtMovimento = "DT_MOVIMENTO";
			 public const string DtPregao = "DT_PREGAO";
			 public const string HhNegocio = "HH_NEGOCIO";
			 public const string CdBolsamov = "CD_BOLSAMOV";
			 public const string CdCorresp = "CD_CORRESP";
			 public const string PcComissaoCorresp = "PC_COMISSAO_CORRESP";
			 public const string NrNegocio = "NR_NEGOCIO";
			 public const string DvNegocio = "DV_NEGOCIO";
			 public const string CdNatope = "CD_NATOPE";
			 public const string TpMercado = "TP_MERCADO";
			 public const string CdNegocio = "CD_NEGOCIO";
			 public const string CdEspecif = "CD_ESPECIF";
			 public const string NmNomres = "NM_NOMRES";
			 public const string QtNegocio = "QT_NEGOCIO";
			 public const string QtEspecificada = "QT_ESPECIFICADA";
			 public const string VlNegocio = "VL_NEGOCIO";
			 public const string TpTermo = "TP_TERMO";
			 public const string TpOrigem = "TP_ORIGEM";
			 public const string CdContraparte = "CD_CONTRAPARTE";
			 public const string DsObs = "DS_OBS";
			 public const string FtValorizacao = "FT_VALORIZACAO";
			 public const string DtOpcoes = "DT_OPCOES";
			 public const string TpGeraoferta = "TP_GERAOFERTA";
			 public const string QtCasada = "QT_CASADA";
			 public const string TpVcoter = "TP_VCOTER";
			 public const string InOrineg = "IN_ORINEG";
			 public const string TpLiquid = "TP_LIQUID";
			 public const string CdTitobj = "CD_TITOBJ";
			 public const string CdCodisi = "CD_CODISI";
			 public const string CdCodcas = "CD_CODCAS";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string InAfterm = "IN_AFTERM";
			 public const string IdLiquid = "ID_LIQUID";
			 public const string NrOfcpmega = "NR_OFCPMEGA";
			 public const string NrOfvdmega = "NR_OFVDMEGA";
			 public const string NrOfemega = "NR_OFEMEGA";
			 public const string CdOperaMega = "CD_OPERA_MEGA";
			 public const string FtLiqnot = "FT_LIQNOT";
			 public const string IdTpliq = "ID_TPLIQ";
			 public const string InBrokeragem = "IN_BROKERAGEM";
			 public const string CdOfestr1 = "CD_OFESTR1";
			 public const string CdOfestr2 = "CD_OFESTR2";
			 public const string DtSistema = "DT_SISTEMA";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DtMovimento = "DtMovimento";
			 public const string DtPregao = "DtPregao";
			 public const string HhNegocio = "HhNegocio";
			 public const string CdBolsamov = "CdBolsamov";
			 public const string CdCorresp = "CdCorresp";
			 public const string PcComissaoCorresp = "PcComissaoCorresp";
			 public const string NrNegocio = "NrNegocio";
			 public const string DvNegocio = "DvNegocio";
			 public const string CdNatope = "CdNatope";
			 public const string TpMercado = "TpMercado";
			 public const string CdNegocio = "CdNegocio";
			 public const string CdEspecif = "CdEspecif";
			 public const string NmNomres = "NmNomres";
			 public const string QtNegocio = "QtNegocio";
			 public const string QtEspecificada = "QtEspecificada";
			 public const string VlNegocio = "VlNegocio";
			 public const string TpTermo = "TpTermo";
			 public const string TpOrigem = "TpOrigem";
			 public const string CdContraparte = "CdContraparte";
			 public const string DsObs = "DsObs";
			 public const string FtValorizacao = "FtValorizacao";
			 public const string DtOpcoes = "DtOpcoes";
			 public const string TpGeraoferta = "TpGeraoferta";
			 public const string QtCasada = "QtCasada";
			 public const string TpVcoter = "TpVcoter";
			 public const string InOrineg = "InOrineg";
			 public const string TpLiquid = "TpLiquid";
			 public const string CdTitobj = "CdTitobj";
			 public const string CdCodisi = "CdCodisi";
			 public const string CdCodcas = "CdCodcas";
			 public const string CdCliente = "CdCliente";
			 public const string InAfterm = "InAfterm";
			 public const string IdLiquid = "IdLiquid";
			 public const string NrOfcpmega = "NrOfcpmega";
			 public const string NrOfvdmega = "NrOfvdmega";
			 public const string NrOfemega = "NrOfemega";
			 public const string CdOperaMega = "CdOperaMega";
			 public const string FtLiqnot = "FtLiqnot";
			 public const string IdTpliq = "IdTpliq";
			 public const string InBrokeragem = "InBrokeragem";
			 public const string CdOfestr1 = "CdOfestr1";
			 public const string CdOfestr2 = "CdOfestr2";
			 public const string DtSistema = "DtSistema";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TornegdMetadata))
			{
				if(TornegdMetadata.mapDelegates == null)
				{
					TornegdMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TornegdMetadata.meta == null)
				{
					TornegdMetadata.meta = new TornegdMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DT_MOVIMENTO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_PREGAO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("HH_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_BOLSAMOV", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CORRESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PC_COMISSAO_CORRESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DV_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_MERCADO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_ESPECIF", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_NOMRES", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("QT_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QT_ESPECIFICADA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_TERMO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TP_ORIGEM", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_CONTRAPARTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DS_OBS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("FT_VALORIZACAO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_OPCOES", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("TP_GERAOFERTA", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("QT_CASADA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_VCOTER", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_ORINEG", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_LIQUID", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_TITOBJ", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CODISI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CODCAS", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_AFTERM", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("ID_LIQUID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_OFCPMEGA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_OFVDMEGA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_OFEMEGA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_OPERA_MEGA", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("FT_LIQNOT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID_TPLIQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("IN_BROKERAGEM", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_OFESTR1", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_OFESTR2", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_SISTEMA", new esTypeMap("TIMESTAMP", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TORNEGD";
				meta.Destination = "TORNEGD";
				
				meta.spInsert = "proc_TORNEGDInsert";				
				meta.spUpdate = "proc_TORNEGDUpdate";		
				meta.spDelete = "proc_TORNEGDDelete";
				meta.spLoadAll = "proc_TORNEGDLoadAll";
				meta.spLoadByPrimaryKey = "proc_TORNEGDLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TornegdMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
