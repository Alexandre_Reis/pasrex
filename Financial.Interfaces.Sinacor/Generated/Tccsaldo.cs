/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 06/08/2012 10:51:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTccsaldoCollection : esEntityCollection
	{
		public esTccsaldoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TccsaldoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTccsaldoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTccsaldoQuery);
		}
		#endregion
		
		virtual public Tccsaldo DetachEntity(Tccsaldo entity)
		{
			return base.DetachEntity(entity) as Tccsaldo;
		}
		
		virtual public Tccsaldo AttachEntity(Tccsaldo entity)
		{
			return base.AttachEntity(entity) as Tccsaldo;
		}
		
		virtual public void Combine(TccsaldoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tccsaldo this[int index]
		{
			get
			{
				return base[index] as Tccsaldo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tccsaldo);
		}
	}



	[Serializable]
	abstract public class esTccsaldo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTccsaldoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTccsaldo()
		{

		}

		public esTccsaldo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTccsaldoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTccsaldoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "VlDisponivel": this.str.VlDisponivel = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "VlTotal": this.str.VlTotal = (string)value; break;							
						case "VlProjetado": this.str.VlProjetado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "VlDisponivel":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDisponivel = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "VlTotal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTotal = (System.Decimal?)value;
							break;
						
						case "VlProjetado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlProjetado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TCCSALDO.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TccsaldoMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TccsaldoMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCSALDO.VL_DISPONIVEL
		/// </summary>
		virtual public System.Decimal? VlDisponivel
		{
			get
			{
				return base.GetSystemDecimal(TccsaldoMetadata.ColumnNames.VlDisponivel);
			}
			
			set
			{
				base.SetSystemDecimal(TccsaldoMetadata.ColumnNames.VlDisponivel, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCSALDO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TccsaldoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TccsaldoMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCSALDO.VL_TOTAL
		/// </summary>
		virtual public System.Decimal? VlTotal
		{
			get
			{
				return base.GetSystemDecimal(TccsaldoMetadata.ColumnNames.VlTotal);
			}
			
			set
			{
				base.SetSystemDecimal(TccsaldoMetadata.ColumnNames.VlTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to TCCSALDO.VL_PROJETADO
		/// </summary>
		virtual public System.Decimal? VlProjetado
		{
			get
			{
				return base.GetSystemDecimal(TccsaldoMetadata.ColumnNames.VlProjetado);
			}
			
			set
			{
				base.SetSystemDecimal(TccsaldoMetadata.ColumnNames.VlProjetado, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTccsaldo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlDisponivel
			{
				get
				{
					System.Decimal? data = entity.VlDisponivel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDisponivel = null;
					else entity.VlDisponivel = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTotal
			{
				get
				{
					System.Decimal? data = entity.VlTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTotal = null;
					else entity.VlTotal = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlProjetado
			{
				get
				{
					System.Decimal? data = entity.VlProjetado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlProjetado = null;
					else entity.VlProjetado = Convert.ToDecimal(value);
				}
			}
			

			private esTccsaldo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTccsaldoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTccsaldo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tccsaldo : esTccsaldo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTccsaldoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TccsaldoMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TccsaldoMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlDisponivel
		{
			get
			{
				return new esQueryItem(this, TccsaldoMetadata.ColumnNames.VlDisponivel, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TccsaldoMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTotal
		{
			get
			{
				return new esQueryItem(this, TccsaldoMetadata.ColumnNames.VlTotal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlProjetado
		{
			get
			{
				return new esQueryItem(this, TccsaldoMetadata.ColumnNames.VlProjetado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TccsaldoCollection")]
	public partial class TccsaldoCollection : esTccsaldoCollection, IEnumerable<Tccsaldo>
	{
		public TccsaldoCollection()
		{

		}
		
		public static implicit operator List<Tccsaldo>(TccsaldoCollection coll)
		{
			List<Tccsaldo> list = new List<Tccsaldo>();
			
			foreach (Tccsaldo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TccsaldoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TccsaldoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tccsaldo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tccsaldo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TccsaldoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TccsaldoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TccsaldoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tccsaldo AddNew()
		{
			Tccsaldo entity = base.AddNewEntity() as Tccsaldo;
			
			return entity;
		}

		public Tccsaldo FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tccsaldo;
		}


		#region IEnumerable<Tccsaldo> Members

		IEnumerator<Tccsaldo> IEnumerable<Tccsaldo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tccsaldo;
			}
		}

		#endregion
		
		private TccsaldoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TCCSALDO' table
	/// </summary>

	[Serializable]
	public partial class Tccsaldo : esTccsaldo
	{
		public Tccsaldo()
		{

		}
	
		public Tccsaldo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TccsaldoMetadata.Meta();
			}
		}
		
		
		
		override protected esTccsaldoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TccsaldoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TccsaldoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TccsaldoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TccsaldoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TccsaldoQuery query;
	}



	[Serializable]
	public partial class TccsaldoQuery : esTccsaldoQuery
	{
		public TccsaldoQuery()
		{

		}		
		
		public TccsaldoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TccsaldoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TccsaldoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TccsaldoMetadata.ColumnNames.CdCliente, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccsaldoMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccsaldoMetadata.ColumnNames.VlDisponivel, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccsaldoMetadata.PropertyNames.VlDisponivel;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccsaldoMetadata.ColumnNames.Id, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccsaldoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccsaldoMetadata.ColumnNames.VlTotal, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccsaldoMetadata.PropertyNames.VlTotal;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TccsaldoMetadata.ColumnNames.VlProjetado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TccsaldoMetadata.PropertyNames.VlProjetado;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TccsaldoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCliente = "CD_CLIENTE";
			 public const string VlDisponivel = "VL_DISPONIVEL";
			 public const string Id = "ID";
			 public const string VlTotal = "VL_TOTAL";
			 public const string VlProjetado = "VL_PROJETADO";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCliente = "CdCliente";
			 public const string VlDisponivel = "VlDisponivel";
			 public const string Id = "Id";
			 public const string VlTotal = "VlTotal";
			 public const string VlProjetado = "VlProjetado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TccsaldoMetadata))
			{
				if(TccsaldoMetadata.mapDelegates == null)
				{
					TccsaldoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TccsaldoMetadata.meta == null)
				{
					TccsaldoMetadata.meta = new TccsaldoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_DISPONIVEL", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TOTAL", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_PROJETADO", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TCCSALDO";
				meta.Destination = "TCCSALDO";
				
				meta.spInsert = "proc_TCCSALDOInsert";				
				meta.spUpdate = "proc_TCCSALDOUpdate";		
				meta.spDelete = "proc_TCCSALDODelete";
				meta.spLoadAll = "proc_TCCSALDOLoadAll";
				meta.spLoadByPrimaryKey = "proc_TCCSALDOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TccsaldoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
