/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 17/09/2012 10:35:27
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTbotitulosCollection : esEntityCollection
	{
		public esTbotitulosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TbotitulosCollection";
		}

		#region Query Logic
		protected void InitQuery(esTbotitulosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTbotitulosQuery);
		}
		#endregion
		
		virtual public Tbotitulos DetachEntity(Tbotitulos entity)
		{
			return base.DetachEntity(entity) as Tbotitulos;
		}
		
		virtual public Tbotitulos AttachEntity(Tbotitulos entity)
		{
			return base.AttachEntity(entity) as Tbotitulos;
		}
		
		virtual public void Combine(TbotitulosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tbotitulos this[int index]
		{
			get
			{
				return base[index] as Tbotitulos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tbotitulos);
		}
	}



	[Serializable]
	abstract public class esTbotitulos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTbotitulosQuery GetDynamicQuery()
		{
			return null;
		}

		public esTbotitulos()
		{

		}

		public esTbotitulos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTbotitulosQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTbotitulosQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCodneg": this.str.CdCodneg = (string)value; break;							
						case "CdTpmerc": this.str.CdTpmerc = (string)value; break;							
						case "DtDatfch": this.str.DtDatfch = (string)value; break;							
						case "DtDatven": this.str.DtDatven = (string)value; break;							
						case "DtInineg": this.str.DtInineg = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "NmEspeci": this.str.NmEspeci = (string)value; break;							
						case "NmNompre": this.str.NmNompre = (string)value; break;							
						case "VlPreexe": this.str.VlPreexe = (string)value; break;							
						case "CdTitobj": this.str.CdTitobj = (string)value; break;							
						case "CdCodisi": this.str.CdCodisi = (string)value; break;							
						case "NrNserie": this.str.NrNserie = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtDatfch":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatfch = (System.DateTime?)value;
							break;
						
						case "DtDatven":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtDatven = (System.DateTime?)value;
							break;
						
						case "DtInineg":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtInineg = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "VlPreexe":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlPreexe = (System.Decimal?)value;
							break;
						
						case "NrNserie":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNserie = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TBOTITULOS.CD_CODNEG
		/// </summary>
		virtual public System.String CdCodneg
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.CdCodneg);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.CdCodneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.CD_TPMERC
		/// </summary>
		virtual public System.String CdTpmerc
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.CdTpmerc);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.CdTpmerc, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.DT_DATFCH
		/// </summary>
		virtual public System.DateTime? DtDatfch
		{
			get
			{
				return base.GetSystemDateTime(TbotitulosMetadata.ColumnNames.DtDatfch);
			}
			
			set
			{
				base.SetSystemDateTime(TbotitulosMetadata.ColumnNames.DtDatfch, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.DT_DATVEN
		/// </summary>
		virtual public System.DateTime? DtDatven
		{
			get
			{
				return base.GetSystemDateTime(TbotitulosMetadata.ColumnNames.DtDatven);
			}
			
			set
			{
				base.SetSystemDateTime(TbotitulosMetadata.ColumnNames.DtDatven, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.DT_ININEG
		/// </summary>
		virtual public System.DateTime? DtInineg
		{
			get
			{
				return base.GetSystemDateTime(TbotitulosMetadata.ColumnNames.DtInineg);
			}
			
			set
			{
				base.SetSystemDateTime(TbotitulosMetadata.ColumnNames.DtInineg, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TbotitulosMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TbotitulosMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.NM_ESPECI
		/// </summary>
		virtual public System.String NmEspeci
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.NmEspeci);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.NmEspeci, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.NM_NOMPRE
		/// </summary>
		virtual public System.String NmNompre
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.NmNompre);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.NmNompre, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.VL_PREEXE
		/// </summary>
		virtual public System.Decimal? VlPreexe
		{
			get
			{
				return base.GetSystemDecimal(TbotitulosMetadata.ColumnNames.VlPreexe);
			}
			
			set
			{
				base.SetSystemDecimal(TbotitulosMetadata.ColumnNames.VlPreexe, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.CD_TITOBJ
		/// </summary>
		virtual public System.String CdTitobj
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.CdTitobj);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.CdTitobj, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.CD_CODISI
		/// </summary>
		virtual public System.String CdCodisi
		{
			get
			{
				return base.GetSystemString(TbotitulosMetadata.ColumnNames.CdCodisi);
			}
			
			set
			{
				base.SetSystemString(TbotitulosMetadata.ColumnNames.CdCodisi, value);
			}
		}
		
		/// <summary>
		/// Maps to TBOTITULOS.NR_NSERIE
		/// </summary>
		virtual public System.Decimal? NrNserie
		{
			get
			{
				return base.GetSystemDecimal(TbotitulosMetadata.ColumnNames.NrNserie);
			}
			
			set
			{
				base.SetSystemDecimal(TbotitulosMetadata.ColumnNames.NrNserie, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTbotitulos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCodneg
			{
				get
				{
					System.String data = entity.CdCodneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodneg = null;
					else entity.CdCodneg = Convert.ToString(value);
				}
			}
				
			public System.String CdTpmerc
			{
				get
				{
					System.String data = entity.CdTpmerc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdTpmerc = null;
					else entity.CdTpmerc = Convert.ToString(value);
				}
			}
				
			public System.String DtDatfch
			{
				get
				{
					System.DateTime? data = entity.DtDatfch;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatfch = null;
					else entity.DtDatfch = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtDatven
			{
				get
				{
					System.DateTime? data = entity.DtDatven;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtDatven = null;
					else entity.DtDatven = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtInineg
			{
				get
				{
					System.DateTime? data = entity.DtInineg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtInineg = null;
					else entity.DtInineg = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmEspeci
			{
				get
				{
					System.String data = entity.NmEspeci;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEspeci = null;
					else entity.NmEspeci = Convert.ToString(value);
				}
			}
				
			public System.String NmNompre
			{
				get
				{
					System.String data = entity.NmNompre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmNompre = null;
					else entity.NmNompre = Convert.ToString(value);
				}
			}
				
			public System.String VlPreexe
			{
				get
				{
					System.Decimal? data = entity.VlPreexe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlPreexe = null;
					else entity.VlPreexe = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdTitobj
			{
				get
				{
					System.String data = entity.CdTitobj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdTitobj = null;
					else entity.CdTitobj = Convert.ToString(value);
				}
			}
				
			public System.String CdCodisi
			{
				get
				{
					System.String data = entity.CdCodisi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodisi = null;
					else entity.CdCodisi = Convert.ToString(value);
				}
			}
				
			public System.String NrNserie
			{
				get
				{
					System.Decimal? data = entity.NrNserie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNserie = null;
					else entity.NrNserie = Convert.ToDecimal(value);
				}
			}
			

			private esTbotitulos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTbotitulosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTbotitulos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tbotitulos : esTbotitulos
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTbotitulosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TbotitulosMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCodneg
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.CdCodneg, esSystemType.String);
			}
		} 
		
		public esQueryItem CdTpmerc
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.CdTpmerc, esSystemType.String);
			}
		} 
		
		public esQueryItem DtDatfch
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.DtDatfch, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtDatven
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.DtDatven, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtInineg
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.DtInineg, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmEspeci
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.NmEspeci, esSystemType.String);
			}
		} 
		
		public esQueryItem NmNompre
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.NmNompre, esSystemType.String);
			}
		} 
		
		public esQueryItem VlPreexe
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.VlPreexe, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdTitobj
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.CdTitobj, esSystemType.String);
			}
		} 
		
		public esQueryItem CdCodisi
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.CdCodisi, esSystemType.String);
			}
		} 
		
		public esQueryItem NrNserie
		{
			get
			{
				return new esQueryItem(this, TbotitulosMetadata.ColumnNames.NrNserie, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TbotitulosCollection")]
	public partial class TbotitulosCollection : esTbotitulosCollection, IEnumerable<Tbotitulos>
	{
		public TbotitulosCollection()
		{

		}
		
		public static implicit operator List<Tbotitulos>(TbotitulosCollection coll)
		{
			List<Tbotitulos> list = new List<Tbotitulos>();
			
			foreach (Tbotitulos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TbotitulosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbotitulosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tbotitulos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tbotitulos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TbotitulosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbotitulosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TbotitulosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tbotitulos AddNew()
		{
			Tbotitulos entity = base.AddNewEntity() as Tbotitulos;
			
			return entity;
		}

		public Tbotitulos FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tbotitulos;
		}


		#region IEnumerable<Tbotitulos> Members

		IEnumerator<Tbotitulos> IEnumerable<Tbotitulos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tbotitulos;
			}
		}

		#endregion
		
		private TbotitulosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TBOTITULOS' table
	/// </summary>

	[Serializable]
	public partial class Tbotitulos : esTbotitulos
	{
		public Tbotitulos()
		{

		}
	
		public Tbotitulos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TbotitulosMetadata.Meta();
			}
		}
		
		
		
		override protected esTbotitulosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TbotitulosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TbotitulosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TbotitulosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TbotitulosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TbotitulosQuery query;
	}



	[Serializable]
	public partial class TbotitulosQuery : esTbotitulosQuery
	{
		public TbotitulosQuery()
		{

		}		
		
		public TbotitulosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TbotitulosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TbotitulosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.CdCodneg, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.CdCodneg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.CdTpmerc, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.CdTpmerc;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.DtDatfch, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbotitulosMetadata.PropertyNames.DtDatfch;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.DtDatven, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbotitulosMetadata.PropertyNames.DtDatven;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.DtInineg, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TbotitulosMetadata.PropertyNames.DtInineg;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.Id, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbotitulosMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.NmEspeci, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.NmEspeci;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.NmNompre, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.NmNompre;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.VlPreexe, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbotitulosMetadata.PropertyNames.VlPreexe;	
			c.NumericPrecision = 11;
			c.NumericScale = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.CdTitobj, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.CdTitobj;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.CdCodisi, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TbotitulosMetadata.PropertyNames.CdCodisi;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TbotitulosMetadata.ColumnNames.NrNserie, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TbotitulosMetadata.PropertyNames.NrNserie;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TbotitulosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCodneg = "CD_CODNEG";
			 public const string CdTpmerc = "CD_TPMERC";
			 public const string DtDatfch = "DT_DATFCH";
			 public const string DtDatven = "DT_DATVEN";
			 public const string DtInineg = "DT_ININEG";
			 public const string Id = "ID";
			 public const string NmEspeci = "NM_ESPECI";
			 public const string NmNompre = "NM_NOMPRE";
			 public const string VlPreexe = "VL_PREEXE";
			 public const string CdTitobj = "CD_TITOBJ";
			 public const string CdCodisi = "CD_CODISI";
			 public const string NrNserie = "NR_NSERIE";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCodneg = "CdCodneg";
			 public const string CdTpmerc = "CdTpmerc";
			 public const string DtDatfch = "DtDatfch";
			 public const string DtDatven = "DtDatven";
			 public const string DtInineg = "DtInineg";
			 public const string Id = "Id";
			 public const string NmEspeci = "NmEspeci";
			 public const string NmNompre = "NmNompre";
			 public const string VlPreexe = "VlPreexe";
			 public const string CdTitobj = "CdTitobj";
			 public const string CdCodisi = "CdCodisi";
			 public const string NrNserie = "NrNserie";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TbotitulosMetadata))
			{
				if(TbotitulosMetadata.mapDelegates == null)
				{
					TbotitulosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TbotitulosMetadata.meta == null)
				{
					TbotitulosMetadata.meta = new TbotitulosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CODNEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_TPMERC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_DATFCH", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_DATVEN", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_ININEG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_ESPECI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NM_NOMPRE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("VL_PREEXE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_TITOBJ", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_CODISI", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("NR_NSERIE", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TBOTITULOS";
				meta.Destination = "TBOTITULOS";
				
				meta.spInsert = "proc_TBOTITULOSInsert";				
				meta.spUpdate = "proc_TBOTITULOSUpdate";		
				meta.spDelete = "proc_TBOTITULOSDelete";
				meta.spLoadAll = "proc_TBOTITULOSLoadAll";
				meta.spLoadByPrimaryKey = "proc_TBOTITULOSLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TbotitulosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
