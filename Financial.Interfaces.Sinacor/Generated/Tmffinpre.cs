/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 28/01/2010 16:40:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		









		
				
				








	
















					
								
											
	









		





		




				
				








		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTmffinpreCollection : esEntityCollection
	{
		public esTmffinpreCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TmffinpreCollection";
		}

		#region Query Logic
		protected void InitQuery(esTmffinpreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTmffinpreQuery);
		}
		#endregion
		
		virtual public Tmffinpre DetachEntity(Tmffinpre entity)
		{
			return base.DetachEntity(entity) as Tmffinpre;
		}
		
		virtual public Tmffinpre AttachEntity(Tmffinpre entity)
		{
			return base.AttachEntity(entity) as Tmffinpre;
		}
		
		virtual public void Combine(TmffinpreCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tmffinpre this[int index]
		{
			get
			{
				return base[index] as Tmffinpre;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tmffinpre);
		}
	}



	[Serializable]
	abstract public class esTmffinpre : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTmffinpreQuery GetDynamicQuery()
		{
			return null;
		}

		public esTmffinpre()
		{

		}

		public esTmffinpre(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTmffinpreQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTmffinpreQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "DtNegocio": this.str.DtNegocio = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "QtQtdesp": this.str.QtQtdesp = (string)value; break;							
						case "CdCommod": this.str.CdCommod = (string)value; break;							
						case "CdMercad": this.str.CdMercad = (string)value; break;							
						case "CdSerie": this.str.CdSerie = (string)value; break;							
						case "TpNegocio": this.str.TpNegocio = (string)value; break;							
						case "PrNegocio": this.str.PrNegocio = (string)value; break;							
						case "VlNegocio": this.str.VlNegocio = (string)value; break;							
						case "VlAjuneg": this.str.VlAjuneg = (string)value; break;							
						case "VlCorneg": this.str.VlCorneg = (string)value; break;							
						case "VlTaxreg": this.str.VlTaxreg = (string)value; break;							
						case "VlEmoneg": this.str.VlEmoneg = (string)value; break;							
						case "CdSerieOri": this.str.CdSerieOri = (string)value; break;							
						case "PrNegocioAju": this.str.PrNegocioAju = (string)value; break;							
						case "NrNegocioCas": this.str.NrNegocioCas = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "DtNegocio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtNegocio = (System.DateTime?)value;
							break;
						
						case "QtQtdesp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtQtdesp = (System.Decimal?)value;
							break;
						
						case "PrNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrNegocio = (System.Decimal?)value;
							break;
						
						case "VlNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlNegocio = (System.Decimal?)value;
							break;
						
						case "VlAjuneg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlAjuneg = (System.Decimal?)value;
							break;
						
						case "VlCorneg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCorneg = (System.Decimal?)value;
							break;
						
						case "VlTaxreg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlTaxreg = (System.Decimal?)value;
							break;
						
						case "VlEmoneg":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlEmoneg = (System.Decimal?)value;
							break;
						
						case "PrNegocioAju":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrNegocioAju = (System.Decimal?)value;
							break;
						
						case "NrNegocioCas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNegocioCas = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.DT_NEGOCIO
		/// </summary>
		virtual public System.DateTime? DtNegocio
		{
			get
			{
				return base.GetSystemDateTime(TmffinpreMetadata.ColumnNames.DtNegocio);
			}
			
			set
			{
				base.SetSystemDateTime(TmffinpreMetadata.ColumnNames.DtNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.QT_QTDESP
		/// </summary>
		virtual public System.Decimal? QtQtdesp
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.QtQtdesp);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.QtQtdesp, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_COMMOD
		/// </summary>
		virtual public System.String CdCommod
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.CdCommod);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.CdCommod, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_MERCAD
		/// </summary>
		virtual public System.String CdMercad
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.CdMercad);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.CdMercad, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_SERIE
		/// </summary>
		virtual public System.String CdSerie
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.CdSerie);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.CdSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.TP_NEGOCIO
		/// </summary>
		virtual public System.String TpNegocio
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.TpNegocio);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.TpNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.PR_NEGOCIO
		/// </summary>
		virtual public System.Decimal? PrNegocio
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.PrNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.PrNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.VL_NEGOCIO
		/// </summary>
		virtual public System.Decimal? VlNegocio
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.VlNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.VlNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.VL_AJUNEG
		/// </summary>
		virtual public System.Decimal? VlAjuneg
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.VlAjuneg);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.VlAjuneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.VL_CORNEG
		/// </summary>
		virtual public System.Decimal? VlCorneg
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.VlCorneg);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.VlCorneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.VL_TAXREG
		/// </summary>
		virtual public System.Decimal? VlTaxreg
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.VlTaxreg);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.VlTaxreg, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.VL_EMONEG
		/// </summary>
		virtual public System.Decimal? VlEmoneg
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.VlEmoneg);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.VlEmoneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.CD_SERIE_ORI
		/// </summary>
		virtual public System.String CdSerieOri
		{
			get
			{
				return base.GetSystemString(TmffinpreMetadata.ColumnNames.CdSerieOri);
			}
			
			set
			{
				base.SetSystemString(TmffinpreMetadata.ColumnNames.CdSerieOri, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.PR_NEGOCIO_AJU
		/// </summary>
		virtual public System.Decimal? PrNegocioAju
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.PrNegocioAju);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.PrNegocioAju, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.NR_NEGOCIO_CAS
		/// </summary>
		virtual public System.Decimal? NrNegocioCas
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.NrNegocioCas);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.NrNegocioCas, value);
			}
		}
		
		/// <summary>
		/// Maps to TMFFINPRE.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TmffinpreMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TmffinpreMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTmffinpre entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtNegocio
			{
				get
				{
					System.DateTime? data = entity.DtNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtNegocio = null;
					else entity.DtNegocio = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String QtQtdesp
			{
				get
				{
					System.Decimal? data = entity.QtQtdesp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtQtdesp = null;
					else entity.QtQtdesp = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCommod
			{
				get
				{
					System.String data = entity.CdCommod;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCommod = null;
					else entity.CdCommod = Convert.ToString(value);
				}
			}
				
			public System.String CdMercad
			{
				get
				{
					System.String data = entity.CdMercad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdMercad = null;
					else entity.CdMercad = Convert.ToString(value);
				}
			}
				
			public System.String CdSerie
			{
				get
				{
					System.String data = entity.CdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdSerie = null;
					else entity.CdSerie = Convert.ToString(value);
				}
			}
				
			public System.String TpNegocio
			{
				get
				{
					System.String data = entity.TpNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpNegocio = null;
					else entity.TpNegocio = Convert.ToString(value);
				}
			}
				
			public System.String PrNegocio
			{
				get
				{
					System.Decimal? data = entity.PrNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrNegocio = null;
					else entity.PrNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlNegocio
			{
				get
				{
					System.Decimal? data = entity.VlNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlNegocio = null;
					else entity.VlNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlAjuneg
			{
				get
				{
					System.Decimal? data = entity.VlAjuneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlAjuneg = null;
					else entity.VlAjuneg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCorneg
			{
				get
				{
					System.Decimal? data = entity.VlCorneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCorneg = null;
					else entity.VlCorneg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlTaxreg
			{
				get
				{
					System.Decimal? data = entity.VlTaxreg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlTaxreg = null;
					else entity.VlTaxreg = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlEmoneg
			{
				get
				{
					System.Decimal? data = entity.VlEmoneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlEmoneg = null;
					else entity.VlEmoneg = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdSerieOri
			{
				get
				{
					System.String data = entity.CdSerieOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdSerieOri = null;
					else entity.CdSerieOri = Convert.ToString(value);
				}
			}
				
			public System.String PrNegocioAju
			{
				get
				{
					System.Decimal? data = entity.PrNegocioAju;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrNegocioAju = null;
					else entity.PrNegocioAju = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrNegocioCas
			{
				get
				{
					System.Decimal? data = entity.NrNegocioCas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNegocioCas = null;
					else entity.NrNegocioCas = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTmffinpre entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTmffinpreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTmffinpre can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tmffinpre : esTmffinpre
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTmffinpreQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TmffinpreMetadata.Meta();
			}
		}	
		

		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtNegocio
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.DtNegocio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem QtQtdesp
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.QtQtdesp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCommod
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdCommod, esSystemType.String);
			}
		} 
		
		public esQueryItem CdMercad
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdMercad, esSystemType.String);
			}
		} 
		
		public esQueryItem CdSerie
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdSerie, esSystemType.String);
			}
		} 
		
		public esQueryItem TpNegocio
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.TpNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem PrNegocio
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.PrNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlNegocio
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.VlNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlAjuneg
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.VlAjuneg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCorneg
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.VlCorneg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlTaxreg
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.VlTaxreg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlEmoneg
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.VlEmoneg, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdSerieOri
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.CdSerieOri, esSystemType.String);
			}
		} 
		
		public esQueryItem PrNegocioAju
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.PrNegocioAju, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrNegocioCas
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.NrNegocioCas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TmffinpreMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TmffinpreCollection")]
	public partial class TmffinpreCollection : esTmffinpreCollection, IEnumerable<Tmffinpre>
	{
		public TmffinpreCollection()
		{

		}
		
		public static implicit operator List<Tmffinpre>(TmffinpreCollection coll)
		{
			List<Tmffinpre> list = new List<Tmffinpre>();
			
			foreach (Tmffinpre emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TmffinpreMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TmffinpreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tmffinpre(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tmffinpre();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TmffinpreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TmffinpreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TmffinpreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tmffinpre AddNew()
		{
			Tmffinpre entity = base.AddNewEntity() as Tmffinpre;
			
			return entity;
		}

		public Tmffinpre FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tmffinpre;
		}


		#region IEnumerable<Tmffinpre> Members

		IEnumerator<Tmffinpre> IEnumerable<Tmffinpre>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tmffinpre;
			}
		}

		#endregion
		
		private TmffinpreQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TMFFINPRE' table
	/// </summary>

	[Serializable]
	public partial class Tmffinpre : esTmffinpre
	{
		public Tmffinpre()
		{

		}
	
		public Tmffinpre(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TmffinpreMetadata.Meta();
			}
		}
		
		
		
		override protected esTmffinpreQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TmffinpreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TmffinpreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TmffinpreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TmffinpreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TmffinpreQuery query;
	}



	[Serializable]
	public partial class TmffinpreQuery : esTmffinpreQuery
	{
		public TmffinpreQuery()
		{

		}		
		
		public TmffinpreQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TmffinpreMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TmffinpreMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdCliente, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.DtNegocio, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TmffinpreMetadata.PropertyNames.DtNegocio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdNatope, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.QtQtdesp, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.QtQtdesp;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdCommod, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdCommod;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdMercad, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdMercad;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdSerie, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdSerie;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.TpNegocio, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.TpNegocio;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.PrNegocio, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.PrNegocio;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.VlNegocio, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.VlNegocio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.VlAjuneg, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.VlAjuneg;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.VlCorneg, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.VlCorneg;	
			c.NumericPrecision = 14;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.VlTaxreg, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.VlTaxreg;	
			c.NumericPrecision = 10;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.VlEmoneg, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.VlEmoneg;	
			c.NumericPrecision = 14;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.CdSerieOri, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TmffinpreMetadata.PropertyNames.CdSerieOri;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.PrNegocioAju, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.PrNegocioAju;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.NrNegocioCas, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.NrNegocioCas;	
			c.NumericPrecision = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TmffinpreMetadata.ColumnNames.Id, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TmffinpreMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TmffinpreMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdCliente = "CD_CLIENTE";
			 public const string DtNegocio = "DT_NEGOCIO";
			 public const string CdNatope = "CD_NATOPE";
			 public const string QtQtdesp = "QT_QTDESP";
			 public const string CdCommod = "CD_COMMOD";
			 public const string CdMercad = "CD_MERCAD";
			 public const string CdSerie = "CD_SERIE";
			 public const string TpNegocio = "TP_NEGOCIO";
			 public const string PrNegocio = "PR_NEGOCIO";
			 public const string VlNegocio = "VL_NEGOCIO";
			 public const string VlAjuneg = "VL_AJUNEG";
			 public const string VlCorneg = "VL_CORNEG";
			 public const string VlTaxreg = "VL_TAXREG";
			 public const string VlEmoneg = "VL_EMONEG";
			 public const string CdSerieOri = "CD_SERIE_ORI";
			 public const string PrNegocioAju = "PR_NEGOCIO_AJU";
			 public const string NrNegocioCas = "NR_NEGOCIO_CAS";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdCliente = "CdCliente";
			 public const string DtNegocio = "DtNegocio";
			 public const string CdNatope = "CdNatope";
			 public const string QtQtdesp = "QtQtdesp";
			 public const string CdCommod = "CdCommod";
			 public const string CdMercad = "CdMercad";
			 public const string CdSerie = "CdSerie";
			 public const string TpNegocio = "TpNegocio";
			 public const string PrNegocio = "PrNegocio";
			 public const string VlNegocio = "VlNegocio";
			 public const string VlAjuneg = "VlAjuneg";
			 public const string VlCorneg = "VlCorneg";
			 public const string VlTaxreg = "VlTaxreg";
			 public const string VlEmoneg = "VlEmoneg";
			 public const string CdSerieOri = "CdSerieOri";
			 public const string PrNegocioAju = "PrNegocioAju";
			 public const string NrNegocioCas = "NrNegocioCas";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TmffinpreMetadata))
			{
				if(TmffinpreMetadata.mapDelegates == null)
				{
					TmffinpreMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TmffinpreMetadata.meta == null)
				{
					TmffinpreMetadata.meta = new TmffinpreMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_NEGOCIO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("QT_QTDESP", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_COMMOD", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_MERCAD", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("CD_SERIE", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("TP_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("PR_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_AJUNEG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_CORNEG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_TAXREG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VL_EMONEG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_SERIE_ORI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("PR_NEGOCIO_AJU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_NEGOCIO_CAS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TMFFINPRE";
				meta.Destination = "TMFFINPRE";
				
				meta.spInsert = "proc_TMFFINPREInsert";				
				meta.spUpdate = "proc_TMFFINPREUpdate";		
				meta.spDelete = "proc_TMFFINPREDelete";
				meta.spLoadAll = "proc_TMFFINPRELoadAll";
				meta.spLoadByPrimaryKey = "proc_TMFFINPRELoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TmffinpreMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
