/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 02/07/2012 10:49:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTcfposiProvPrevCollection : esEntityCollection
	{
		public esTcfposiProvPrevCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TcfposiProvPrevCollection";
		}

		#region Query Logic
		protected void InitQuery(esTcfposiProvPrevQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTcfposiProvPrevQuery);
		}
		#endregion
		
		virtual public TcfposiProvPrev DetachEntity(TcfposiProvPrev entity)
		{
			return base.DetachEntity(entity) as TcfposiProvPrev;
		}
		
		virtual public TcfposiProvPrev AttachEntity(TcfposiProvPrev entity)
		{
			return base.AttachEntity(entity) as TcfposiProvPrev;
		}
		
		virtual public void Combine(TcfposiProvPrevCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TcfposiProvPrev this[int index]
		{
			get
			{
				return base[index] as TcfposiProvPrev;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TcfposiProvPrev);
		}
	}



	[Serializable]
	abstract public class esTcfposiProvPrev : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTcfposiProvPrevQuery GetDynamicQuery()
		{
			return null;
		}

		public esTcfposiProvPrev()
		{

		}

		public esTcfposiProvPrev(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTcfposiProvPrevQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTcfposiProvPrevQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataAtu": this.str.DataAtu = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "CodIsin": this.str.CodIsin = (string)value; break;							
						case "TipoProv": this.str.TipoProv = (string)value; break;							
						case "QtdeProv": this.str.QtdeProv = (string)value; break;							
						case "ValProv": this.str.ValProv = (string)value; break;							
						case "PrecEmisSubs": this.str.PrecEmisSubs = (string)value; break;							
						case "QtdeParcSubs": this.str.QtdeParcSubs = (string)value; break;							
						case "PercPrimParcSubs": this.str.PercPrimParcSubs = (string)value; break;							
						case "PercIrDivi": this.str.PercIrDivi = (string)value; break;							
						case "DataDebSubs": this.str.DataDebSubs = (string)value; break;							
						case "DataAquiTitu": this.str.DataAquiTitu = (string)value; break;							
						case "ValAquiTitu": this.str.ValAquiTitu = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataAtu":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtu = (System.DateTime?)value;
							break;
						
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "TipoProv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoProv = (System.Decimal?)value;
							break;
						
						case "QtdeProv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeProv = (System.Decimal?)value;
							break;
						
						case "ValProv":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValProv = (System.Decimal?)value;
							break;
						
						case "PrecEmisSubs":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecEmisSubs = (System.Decimal?)value;
							break;
						
						case "QtdeParcSubs":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeParcSubs = (System.Decimal?)value;
							break;
						
						case "PercPrimParcSubs":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercPrimParcSubs = (System.Decimal?)value;
							break;
						
						case "PercIrDivi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercIrDivi = (System.Decimal?)value;
							break;
						
						case "DataDebSubs":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataDebSubs = (System.DateTime?)value;
							break;
						
						case "DataAquiTitu":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAquiTitu = (System.DateTime?)value;
							break;
						
						case "ValAquiTitu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValAquiTitu = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.DATA_ATU
		/// </summary>
		virtual public System.DateTime? DataAtu
		{
			get
			{
				return base.GetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataAtu);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataAtu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.COD_ISIN
		/// </summary>
		virtual public System.String CodIsin
		{
			get
			{
				return base.GetSystemString(TcfposiProvPrevMetadata.ColumnNames.CodIsin);
			}
			
			set
			{
				base.SetSystemString(TcfposiProvPrevMetadata.ColumnNames.CodIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.TIPO_PROV
		/// </summary>
		virtual public System.Decimal? TipoProv
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.TipoProv);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.TipoProv, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.QTDE_PROV
		/// </summary>
		virtual public System.Decimal? QtdeProv
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.QtdeProv);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.QtdeProv, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.VAL_PROV
		/// </summary>
		virtual public System.Decimal? ValProv
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.ValProv);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.ValProv, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.PREC_EMIS_SUBS
		/// </summary>
		virtual public System.Decimal? PrecEmisSubs
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PrecEmisSubs);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PrecEmisSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.QTDE_PARC_SUBS
		/// </summary>
		virtual public System.Decimal? QtdeParcSubs
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.QtdeParcSubs);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.QtdeParcSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.PERC_PRIM_PARC_SUBS
		/// </summary>
		virtual public System.Decimal? PercPrimParcSubs
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PercPrimParcSubs);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PercPrimParcSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.PERC_IR_DIVI
		/// </summary>
		virtual public System.Decimal? PercIrDivi
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PercIrDivi);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.PercIrDivi, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.DATA_DEB_SUBS
		/// </summary>
		virtual public System.DateTime? DataDebSubs
		{
			get
			{
				return base.GetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataDebSubs);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataDebSubs, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.DATA_AQUI_TITU
		/// </summary>
		virtual public System.DateTime? DataAquiTitu
		{
			get
			{
				return base.GetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataAquiTitu);
			}
			
			set
			{
				base.SetSystemDateTime(TcfposiProvPrevMetadata.ColumnNames.DataAquiTitu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.VAL_AQUI_TITU
		/// </summary>
		virtual public System.Decimal? ValAquiTitu
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.ValAquiTitu);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.ValAquiTitu, value);
			}
		}
		
		/// <summary>
		/// Maps to TCFPOSI_PROV_PREV.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TcfposiProvPrevMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTcfposiProvPrev entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataAtu
			{
				get
				{
					System.DateTime? data = entity.DataAtu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtu = null;
					else entity.DataAtu = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodIsin
			{
				get
				{
					System.String data = entity.CodIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsin = null;
					else entity.CodIsin = Convert.ToString(value);
				}
			}
				
			public System.String TipoProv
			{
				get
				{
					System.Decimal? data = entity.TipoProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProv = null;
					else entity.TipoProv = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeProv
			{
				get
				{
					System.Decimal? data = entity.QtdeProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeProv = null;
					else entity.QtdeProv = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValProv
			{
				get
				{
					System.Decimal? data = entity.ValProv;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValProv = null;
					else entity.ValProv = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecEmisSubs
			{
				get
				{
					System.Decimal? data = entity.PrecEmisSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecEmisSubs = null;
					else entity.PrecEmisSubs = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeParcSubs
			{
				get
				{
					System.Decimal? data = entity.QtdeParcSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeParcSubs = null;
					else entity.QtdeParcSubs = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercPrimParcSubs
			{
				get
				{
					System.Decimal? data = entity.PercPrimParcSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercPrimParcSubs = null;
					else entity.PercPrimParcSubs = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercIrDivi
			{
				get
				{
					System.Decimal? data = entity.PercIrDivi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercIrDivi = null;
					else entity.PercIrDivi = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataDebSubs
			{
				get
				{
					System.DateTime? data = entity.DataDebSubs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataDebSubs = null;
					else entity.DataDebSubs = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAquiTitu
			{
				get
				{
					System.DateTime? data = entity.DataAquiTitu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAquiTitu = null;
					else entity.DataAquiTitu = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValAquiTitu
			{
				get
				{
					System.Decimal? data = entity.ValAquiTitu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValAquiTitu = null;
					else entity.ValAquiTitu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esTcfposiProvPrev entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTcfposiProvPrevQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTcfposiProvPrev can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TcfposiProvPrev : esTcfposiProvPrev
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTcfposiProvPrevQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TcfposiProvPrevMetadata.Meta();
			}
		}	
		

		public esQueryItem DataAtu
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.DataAtu, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodIsin
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.CodIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoProv
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.TipoProv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeProv
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.QtdeProv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValProv
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.ValProv, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecEmisSubs
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.PrecEmisSubs, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeParcSubs
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.QtdeParcSubs, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercPrimParcSubs
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.PercPrimParcSubs, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercIrDivi
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.PercIrDivi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataDebSubs
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.DataDebSubs, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAquiTitu
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.DataAquiTitu, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValAquiTitu
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.ValAquiTitu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TcfposiProvPrevMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TcfposiProvPrevCollection")]
	public partial class TcfposiProvPrevCollection : esTcfposiProvPrevCollection, IEnumerable<TcfposiProvPrev>
	{
		public TcfposiProvPrevCollection()
		{

		}
		
		public static implicit operator List<TcfposiProvPrev>(TcfposiProvPrevCollection coll)
		{
			List<TcfposiProvPrev> list = new List<TcfposiProvPrev>();
			
			foreach (TcfposiProvPrev emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TcfposiProvPrevMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfposiProvPrevQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TcfposiProvPrev(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TcfposiProvPrev();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TcfposiProvPrevQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfposiProvPrevQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TcfposiProvPrevQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TcfposiProvPrev AddNew()
		{
			TcfposiProvPrev entity = base.AddNewEntity() as TcfposiProvPrev;
			
			return entity;
		}

		public TcfposiProvPrev FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as TcfposiProvPrev;
		}


		#region IEnumerable<TcfposiProvPrev> Members

		IEnumerator<TcfposiProvPrev> IEnumerable<TcfposiProvPrev>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TcfposiProvPrev;
			}
		}

		#endregion
		
		private TcfposiProvPrevQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TCFPOSI_PROV_PREV' table
	/// </summary>

	[Serializable]
	public partial class TcfposiProvPrev : esTcfposiProvPrev
	{
		public TcfposiProvPrev()
		{

		}
	
		public TcfposiProvPrev(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TcfposiProvPrevMetadata.Meta();
			}
		}
		
		
		
		override protected esTcfposiProvPrevQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TcfposiProvPrevQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TcfposiProvPrevQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TcfposiProvPrevQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TcfposiProvPrevQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TcfposiProvPrevQuery query;
	}



	[Serializable]
	public partial class TcfposiProvPrevQuery : esTcfposiProvPrevQuery
	{
		public TcfposiProvPrevQuery()
		{

		}		
		
		public TcfposiProvPrevQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TcfposiProvPrevMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TcfposiProvPrevMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.DataAtu, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.DataAtu;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.CodCli, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.CodIsin, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.CodIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.TipoProv, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.TipoProv;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.QtdeProv, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.QtdeProv;	
			c.NumericPrecision = 19;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.ValProv, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.ValProv;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.PrecEmisSubs, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.PrecEmisSubs;	
			c.NumericPrecision = 13;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.QtdeParcSubs, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.QtdeParcSubs;	
			c.NumericPrecision = 1;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.PercPrimParcSubs, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.PercPrimParcSubs;	
			c.NumericPrecision = 18;
			c.NumericScale = 11;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.PercIrDivi, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.PercIrDivi;	
			c.NumericPrecision = 7;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.DataDebSubs, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.DataDebSubs;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.DataAquiTitu, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.DataAquiTitu;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.ValAquiTitu, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.ValAquiTitu;	
			c.NumericPrecision = 15;
			c.NumericScale = 6;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TcfposiProvPrevMetadata.ColumnNames.Id, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TcfposiProvPrevMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TcfposiProvPrevMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataAtu = "DATA_ATU";
			 public const string CodCli = "COD_CLI";
			 public const string CodIsin = "COD_ISIN";
			 public const string TipoProv = "TIPO_PROV";
			 public const string QtdeProv = "QTDE_PROV";
			 public const string ValProv = "VAL_PROV";
			 public const string PrecEmisSubs = "PREC_EMIS_SUBS";
			 public const string QtdeParcSubs = "QTDE_PARC_SUBS";
			 public const string PercPrimParcSubs = "PERC_PRIM_PARC_SUBS";
			 public const string PercIrDivi = "PERC_IR_DIVI";
			 public const string DataDebSubs = "DATA_DEB_SUBS";
			 public const string DataAquiTitu = "DATA_AQUI_TITU";
			 public const string ValAquiTitu = "VAL_AQUI_TITU";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataAtu = "DataAtu";
			 public const string CodCli = "CodCli";
			 public const string CodIsin = "CodIsin";
			 public const string TipoProv = "TipoProv";
			 public const string QtdeProv = "QtdeProv";
			 public const string ValProv = "ValProv";
			 public const string PrecEmisSubs = "PrecEmisSubs";
			 public const string QtdeParcSubs = "QtdeParcSubs";
			 public const string PercPrimParcSubs = "PercPrimParcSubs";
			 public const string PercIrDivi = "PercIrDivi";
			 public const string DataDebSubs = "DataDebSubs";
			 public const string DataAquiTitu = "DataAquiTitu";
			 public const string ValAquiTitu = "ValAquiTitu";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TcfposiProvPrevMetadata))
			{
				if(TcfposiProvPrevMetadata.mapDelegates == null)
				{
					TcfposiProvPrevMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TcfposiProvPrevMetadata.meta == null)
				{
					TcfposiProvPrevMetadata.meta = new TcfposiProvPrevMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DATA_ATU", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_ISIN", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TIPO_PROV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_PROV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_PROV", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_EMIS_SUBS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_PARC_SUBS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PERC_PRIM_PARC_SUBS", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PERC_IR_DIVI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_DEB_SUBS", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_AQUI_TITU", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("VAL_AQUI_TITU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TCFPOSI_PROV_PREV";
				meta.Destination = "TCFPOSI_PROV_PREV";
				
				meta.spInsert = "proc_TCFPOSI_PROV_PREVInsert";				
				meta.spUpdate = "proc_TCFPOSI_PROV_PREVUpdate";		
				meta.spDelete = "proc_TCFPOSI_PROV_PREVDelete";
				meta.spLoadAll = "proc_TCFPOSI_PROV_PREVLoadAll";
				meta.spLoadByPrimaryKey = "proc_TCFPOSI_PROV_PREVLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TcfposiProvPrevMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
