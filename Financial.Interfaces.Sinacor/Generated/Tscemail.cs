/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 13/09/2012 10:19:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTscemailCollection : esEntityCollection
	{
		public esTscemailCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TscemailCollection";
		}

		#region Query Logic
		protected void InitQuery(esTscemailQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTscemailQuery);
		}
		#endregion
		
		virtual public Tscemail DetachEntity(Tscemail entity)
		{
			return base.DetachEntity(entity) as Tscemail;
		}
		
		virtual public Tscemail AttachEntity(Tscemail entity)
		{
			return base.AttachEntity(entity) as Tscemail;
		}
		
		virtual public void Combine(TscemailCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tscemail this[int index]
		{
			get
			{
				return base[index] as Tscemail;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tscemail);
		}
	}



	[Serializable]
	abstract public class esTscemail : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTscemailQuery GetDynamicQuery()
		{
			return null;
		}

		public esTscemail()
		{

		}

		public esTscemail(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTscemailQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTscemailQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdCpfcgc": this.str.CdCpfcgc = (string)value; break;							
						case "NmEMail": this.str.NmEMail = (string)value; break;							
						case "InPrincipal": this.str.InPrincipal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdCpfcgc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCpfcgc = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TSCEMAIL.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TscemailMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TscemailMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCEMAIL.CD_CPFCGC
		/// </summary>
		virtual public System.Decimal? CdCpfcgc
		{
			get
			{
				return base.GetSystemDecimal(TscemailMetadata.ColumnNames.CdCpfcgc);
			}
			
			set
			{
				base.SetSystemDecimal(TscemailMetadata.ColumnNames.CdCpfcgc, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCEMAIL.NM_E_MAIL
		/// </summary>
		virtual public System.String NmEMail
		{
			get
			{
				return base.GetSystemString(TscemailMetadata.ColumnNames.NmEMail);
			}
			
			set
			{
				base.SetSystemString(TscemailMetadata.ColumnNames.NmEMail, value);
			}
		}
		
		/// <summary>
		/// Maps to TSCEMAIL.IN_PRINCIPAL
		/// </summary>
		virtual public System.String InPrincipal
		{
			get
			{
				return base.GetSystemString(TscemailMetadata.ColumnNames.InPrincipal);
			}
			
			set
			{
				base.SetSystemString(TscemailMetadata.ColumnNames.InPrincipal, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTscemail entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCpfcgc
			{
				get
				{
					System.Decimal? data = entity.CdCpfcgc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCpfcgc = null;
					else entity.CdCpfcgc = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmEMail
			{
				get
				{
					System.String data = entity.NmEMail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmEMail = null;
					else entity.NmEMail = Convert.ToString(value);
				}
			}
				
			public System.String InPrincipal
			{
				get
				{
					System.String data = entity.InPrincipal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InPrincipal = null;
					else entity.InPrincipal = Convert.ToString(value);
				}
			}
			

			private esTscemail entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTscemailQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTscemail can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tscemail : esTscemail
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTscemailQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TscemailMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TscemailMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCpfcgc
		{
			get
			{
				return new esQueryItem(this, TscemailMetadata.ColumnNames.CdCpfcgc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmEMail
		{
			get
			{
				return new esQueryItem(this, TscemailMetadata.ColumnNames.NmEMail, esSystemType.String);
			}
		} 
		
		public esQueryItem InPrincipal
		{
			get
			{
				return new esQueryItem(this, TscemailMetadata.ColumnNames.InPrincipal, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TscemailCollection")]
	public partial class TscemailCollection : esTscemailCollection, IEnumerable<Tscemail>
	{
		public TscemailCollection()
		{

		}
		
		public static implicit operator List<Tscemail>(TscemailCollection coll)
		{
			List<Tscemail> list = new List<Tscemail>();
			
			foreach (Tscemail emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TscemailMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscemailQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tscemail(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tscemail();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TscemailQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscemailQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TscemailQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tscemail AddNew()
		{
			Tscemail entity = base.AddNewEntity() as Tscemail;
			
			return entity;
		}

		public Tscemail FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tscemail;
		}


		#region IEnumerable<Tscemail> Members

		IEnumerator<Tscemail> IEnumerable<Tscemail>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tscemail;
			}
		}

		#endregion
		
		private TscemailQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TSCEMAIL' table
	/// </summary>

	[Serializable]
	public partial class Tscemail : esTscemail
	{
		public Tscemail()
		{

		}
	
		public Tscemail(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TscemailMetadata.Meta();
			}
		}
		
		
		
		override protected esTscemailQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TscemailQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TscemailQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TscemailQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TscemailQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TscemailQuery query;
	}



	[Serializable]
	public partial class TscemailQuery : esTscemailQuery
	{
		public TscemailQuery()
		{

		}		
		
		public TscemailQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TscemailMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TscemailMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TscemailMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscemailMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscemailMetadata.ColumnNames.CdCpfcgc, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TscemailMetadata.PropertyNames.CdCpfcgc;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscemailMetadata.ColumnNames.NmEMail, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TscemailMetadata.PropertyNames.NmEMail;
			c.CharacterMaxLength = 80;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TscemailMetadata.ColumnNames.InPrincipal, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TscemailMetadata.PropertyNames.InPrincipal;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TscemailMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdCpfcgc = "CD_CPFCGC";
			 public const string NmEMail = "NM_E_MAIL";
			 public const string InPrincipal = "IN_PRINCIPAL";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdCpfcgc = "CdCpfcgc";
			 public const string NmEMail = "NmEMail";
			 public const string InPrincipal = "InPrincipal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TscemailMetadata))
			{
				if(TscemailMetadata.mapDelegates == null)
				{
					TscemailMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TscemailMetadata.meta == null)
				{
					TscemailMetadata.meta = new TscemailMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CPFCGC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_E_MAIL", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("IN_PRINCIPAL", new esTypeMap("CHAR", "System.String"));			
				
				
				
				meta.Source = "TSCEMAIL";
				meta.Destination = "TSCEMAIL";
				
				meta.spInsert = "proc_TSCEMAILInsert";				
				meta.spUpdate = "proc_TSCEMAILUpdate";		
				meta.spDelete = "proc_TSCEMAILDelete";
				meta.spLoadAll = "proc_TSCEMAILLoadAll";
				meta.spLoadByPrimaryKey = "proc_TSCEMAILLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TscemailMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
