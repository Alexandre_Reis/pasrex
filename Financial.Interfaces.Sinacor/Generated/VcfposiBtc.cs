/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 16/05/2012 12:47:24
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	
















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVcfposiBtcCollection : esEntityCollection
	{
		public esVcfposiBtcCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VcfposiBtcCollection";
		}

		#region Query Logic
		protected void InitQuery(esVcfposiBtcQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVcfposiBtcQuery);
		}
		#endregion
		
		virtual public VcfposiBtc DetachEntity(VcfposiBtc entity)
		{
			return base.DetachEntity(entity) as VcfposiBtc;
		}
		
		virtual public VcfposiBtc AttachEntity(VcfposiBtc entity)
		{
			return base.AttachEntity(entity) as VcfposiBtc;
		}
		
		virtual public void Combine(VcfposiBtcCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VcfposiBtc this[int index]
		{
			get
			{
				return base[index] as VcfposiBtc;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VcfposiBtc);
		}
	}



	[Serializable]
	abstract public class esVcfposiBtc : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVcfposiBtcQuery GetDynamicQuery()
		{
			return null;
		}

		public esVcfposiBtc()
		{

		}

		public esVcfposiBtc(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVcfposiBtcQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVcfposiBtcQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TipoCotr": this.str.TipoCotr = (string)value; break;							
						case "CodCli": this.str.CodCli = (string)value; break;							
						case "NumCotr": this.str.NumCotr = (string)value; break;							
						case "TipoMerc": this.str.TipoMerc = (string)value; break;							
						case "NumCotrOrig": this.str.NumCotrOrig = (string)value; break;							
						case "TipoOri": this.str.TipoOri = (string)value; break;							
						case "DescTipoOri": this.str.DescTipoOri = (string)value; break;							
						case "DataOri": this.str.DataOri = (string)value; break;							
						case "DataAber": this.str.DataAber = (string)value; break;							
						case "QtdeAcoe": this.str.QtdeAcoe = (string)value; break;							
						case "QtdeAcoeSegr": this.str.QtdeAcoeSegr = (string)value; break;							
						case "DataVenc": this.str.DataVenc = (string)value; break;							
						case "PrecMed": this.str.PrecMed = (string)value; break;							
						case "TaxaRemu": this.str.TaxaRemu = (string)value; break;							
						case "TaxaComi": this.str.TaxaComi = (string)value; break;							
						case "QtdeAcoeOrig": this.str.QtdeAcoeOrig = (string)value; break;							
						case "PrecDiaAnte": this.str.PrecDiaAnte = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "CodCliOri": this.str.CodCliOri = (string)value; break;							
						case "ValBrut": this.str.ValBrut = (string)value; break;							
						case "ValBrutDoad": this.str.ValBrutDoad = (string)value; break;							
						case "ValComi": this.str.ValComi = (string)value; break;							
						case "ValEmolCblc": this.str.ValEmolCblc = (string)value; break;							
						case "ValIr": this.str.ValIr = (string)value; break;							
						case "ValLiq": this.str.ValLiq = (string)value; break;							
						case "TipoMercBtc": this.str.TipoMercBtc = (string)value; break;							
						case "SituCotr": this.str.SituCotr = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "CodCli":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCli = (System.Decimal?)value;
							break;
						
						case "NumCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotr = (System.Decimal?)value;
							break;
						
						case "NumCotrOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumCotrOrig = (System.Decimal?)value;
							break;
						
						case "TipoOri":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TipoOri = (System.Decimal?)value;
							break;
						
						case "DataOri":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOri = (System.DateTime?)value;
							break;
						
						case "DataAber":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAber = (System.DateTime?)value;
							break;
						
						case "QtdeAcoe":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoe = (System.Decimal?)value;
							break;
						
						case "QtdeAcoeSegr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoeSegr = (System.Decimal?)value;
							break;
						
						case "DataVenc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVenc = (System.DateTime?)value;
							break;
						
						case "PrecMed":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecMed = (System.Decimal?)value;
							break;
						
						case "TaxaRemu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaRemu = (System.Decimal?)value;
							break;
						
						case "TaxaComi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaComi = (System.Decimal?)value;
							break;
						
						case "QtdeAcoeOrig":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeAcoeOrig = (System.Decimal?)value;
							break;
						
						case "PrecDiaAnte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecDiaAnte = (System.Decimal?)value;
							break;
						
						case "CodCliOri":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodCliOri = (System.Decimal?)value;
							break;
						
						case "ValBrut":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValBrut = (System.Decimal?)value;
							break;
						
						case "ValBrutDoad":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValBrutDoad = (System.Decimal?)value;
							break;
						
						case "ValComi":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValComi = (System.Decimal?)value;
							break;
						
						case "ValEmolCblc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValEmolCblc = (System.Decimal?)value;
							break;
						
						case "ValIr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValIr = (System.Decimal?)value;
							break;
						
						case "ValLiq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValLiq = (System.Decimal?)value;
							break;
						
						case "SituCotr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SituCotr = (System.Decimal?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TIPO_COTR
		/// </summary>
		virtual public System.String TipoCotr
		{
			get
			{
				return base.GetSystemString(VcfposiBtcMetadata.ColumnNames.TipoCotr);
			}
			
			set
			{
				base.SetSystemString(VcfposiBtcMetadata.ColumnNames.TipoCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.COD_CLI
		/// </summary>
		virtual public System.Decimal? CodCli
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.CodCli);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.CodCli, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.NUM_COTR
		/// </summary>
		virtual public System.Decimal? NumCotr
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.NumCotr);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.NumCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TIPO_MERC
		/// </summary>
		virtual public System.String TipoMerc
		{
			get
			{
				return base.GetSystemString(VcfposiBtcMetadata.ColumnNames.TipoMerc);
			}
			
			set
			{
				base.SetSystemString(VcfposiBtcMetadata.ColumnNames.TipoMerc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.NUM_COTR_ORIG
		/// </summary>
		virtual public System.Decimal? NumCotrOrig
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.NumCotrOrig);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.NumCotrOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TIPO_ORI
		/// </summary>
		virtual public System.Decimal? TipoOri
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TipoOri);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TipoOri, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.DESC_TIPO_ORI
		/// </summary>
		virtual public System.String DescTipoOri
		{
			get
			{
				return base.GetSystemString(VcfposiBtcMetadata.ColumnNames.DescTipoOri);
			}
			
			set
			{
				base.SetSystemString(VcfposiBtcMetadata.ColumnNames.DescTipoOri, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.DATA_ORI
		/// </summary>
		virtual public System.DateTime? DataOri
		{
			get
			{
				return base.GetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataOri);
			}
			
			set
			{
				base.SetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataOri, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.DATA_ABER
		/// </summary>
		virtual public System.DateTime? DataAber
		{
			get
			{
				return base.GetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataAber);
			}
			
			set
			{
				base.SetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataAber, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.QTDE_ACOE
		/// </summary>
		virtual public System.Decimal? QtdeAcoe
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoe);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoe, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.QTDE_ACOE_SEGR
		/// </summary>
		virtual public System.Decimal? QtdeAcoeSegr
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoeSegr);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoeSegr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.DATA_VENC
		/// </summary>
		virtual public System.DateTime? DataVenc
		{
			get
			{
				return base.GetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataVenc);
			}
			
			set
			{
				base.SetSystemDateTime(VcfposiBtcMetadata.ColumnNames.DataVenc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.PREC_MED
		/// </summary>
		virtual public System.Decimal? PrecMed
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.PrecMed);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.PrecMed, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TAXA_REMU
		/// </summary>
		virtual public System.Decimal? TaxaRemu
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TaxaRemu);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TaxaRemu, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TAXA_COMI
		/// </summary>
		virtual public System.Decimal? TaxaComi
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TaxaComi);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.TaxaComi, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.QTDE_ACOE_ORIG
		/// </summary>
		virtual public System.Decimal? QtdeAcoeOrig
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoeOrig);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.QtdeAcoeOrig, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.PREC_DIA_ANTE
		/// </summary>
		virtual public System.Decimal? PrecDiaAnte
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.PrecDiaAnte);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.PrecDiaAnte, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(VcfposiBtcMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(VcfposiBtcMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.COD_CLI_ORI
		/// </summary>
		virtual public System.Decimal? CodCliOri
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.CodCliOri);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.CodCliOri, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_BRUT
		/// </summary>
		virtual public System.Decimal? ValBrut
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValBrut);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValBrut, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_BRUT_DOAD
		/// </summary>
		virtual public System.Decimal? ValBrutDoad
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValBrutDoad);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValBrutDoad, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_COMI
		/// </summary>
		virtual public System.Decimal? ValComi
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValComi);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValComi, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_EMOL_CBLC
		/// </summary>
		virtual public System.Decimal? ValEmolCblc
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValEmolCblc);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValEmolCblc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_IR
		/// </summary>
		virtual public System.Decimal? ValIr
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValIr);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValIr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.VAL_LIQ
		/// </summary>
		virtual public System.Decimal? ValLiq
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValLiq);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.ValLiq, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.TIPO_MERC_BTC
		/// </summary>
		virtual public System.String TipoMercBtc
		{
			get
			{
				return base.GetSystemString(VcfposiBtcMetadata.ColumnNames.TipoMercBtc);
			}
			
			set
			{
				base.SetSystemString(VcfposiBtcMetadata.ColumnNames.TipoMercBtc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.SITU_COTR
		/// </summary>
		virtual public System.Decimal? SituCotr
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.SituCotr);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.SituCotr, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPOSI_BTC.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VcfposiBtcMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VcfposiBtcMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVcfposiBtc entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TipoCotr
			{
				get
				{
					System.String data = entity.TipoCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCotr = null;
					else entity.TipoCotr = Convert.ToString(value);
				}
			}
				
			public System.String CodCli
			{
				get
				{
					System.Decimal? data = entity.CodCli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCli = null;
					else entity.CodCli = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumCotr
			{
				get
				{
					System.Decimal? data = entity.NumCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotr = null;
					else entity.NumCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoMerc
			{
				get
				{
					System.String data = entity.TipoMerc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMerc = null;
					else entity.TipoMerc = Convert.ToString(value);
				}
			}
				
			public System.String NumCotrOrig
			{
				get
				{
					System.Decimal? data = entity.NumCotrOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumCotrOrig = null;
					else entity.NumCotrOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoOri
			{
				get
				{
					System.Decimal? data = entity.TipoOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOri = null;
					else entity.TipoOri = Convert.ToDecimal(value);
				}
			}
				
			public System.String DescTipoOri
			{
				get
				{
					System.String data = entity.DescTipoOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescTipoOri = null;
					else entity.DescTipoOri = Convert.ToString(value);
				}
			}
				
			public System.String DataOri
			{
				get
				{
					System.DateTime? data = entity.DataOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOri = null;
					else entity.DataOri = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAber
			{
				get
				{
					System.DateTime? data = entity.DataAber;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAber = null;
					else entity.DataAber = Convert.ToDateTime(value);
				}
			}
				
			public System.String QtdeAcoe
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoe = null;
					else entity.QtdeAcoe = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeAcoeSegr
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoeSegr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoeSegr = null;
					else entity.QtdeAcoeSegr = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVenc
			{
				get
				{
					System.DateTime? data = entity.DataVenc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVenc = null;
					else entity.DataVenc = Convert.ToDateTime(value);
				}
			}
				
			public System.String PrecMed
			{
				get
				{
					System.Decimal? data = entity.PrecMed;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecMed = null;
					else entity.PrecMed = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaRemu
			{
				get
				{
					System.Decimal? data = entity.TaxaRemu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaRemu = null;
					else entity.TaxaRemu = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaComi
			{
				get
				{
					System.Decimal? data = entity.TaxaComi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaComi = null;
					else entity.TaxaComi = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdeAcoeOrig
			{
				get
				{
					System.Decimal? data = entity.QtdeAcoeOrig;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeAcoeOrig = null;
					else entity.QtdeAcoeOrig = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrecDiaAnte
			{
				get
				{
					System.Decimal? data = entity.PrecDiaAnte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecDiaAnte = null;
					else entity.PrecDiaAnte = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String CodCliOri
			{
				get
				{
					System.Decimal? data = entity.CodCliOri;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodCliOri = null;
					else entity.CodCliOri = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValBrut
			{
				get
				{
					System.Decimal? data = entity.ValBrut;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValBrut = null;
					else entity.ValBrut = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValBrutDoad
			{
				get
				{
					System.Decimal? data = entity.ValBrutDoad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValBrutDoad = null;
					else entity.ValBrutDoad = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValComi
			{
				get
				{
					System.Decimal? data = entity.ValComi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValComi = null;
					else entity.ValComi = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValEmolCblc
			{
				get
				{
					System.Decimal? data = entity.ValEmolCblc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValEmolCblc = null;
					else entity.ValEmolCblc = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValIr
			{
				get
				{
					System.Decimal? data = entity.ValIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValIr = null;
					else entity.ValIr = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValLiq
			{
				get
				{
					System.Decimal? data = entity.ValLiq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValLiq = null;
					else entity.ValLiq = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoMercBtc
			{
				get
				{
					System.String data = entity.TipoMercBtc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercBtc = null;
					else entity.TipoMercBtc = Convert.ToString(value);
				}
			}
				
			public System.String SituCotr
			{
				get
				{
					System.Decimal? data = entity.SituCotr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SituCotr = null;
					else entity.SituCotr = Convert.ToDecimal(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esVcfposiBtc entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVcfposiBtcQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVcfposiBtc can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VcfposiBtc : esVcfposiBtc
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVcfposiBtcQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VcfposiBtcMetadata.Meta();
			}
		}	
		

		public esQueryItem TipoCotr
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TipoCotr, esSystemType.String);
			}
		} 
		
		public esQueryItem CodCli
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.CodCli, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumCotr
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.NumCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoMerc
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TipoMerc, esSystemType.String);
			}
		} 
		
		public esQueryItem NumCotrOrig
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.NumCotrOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoOri
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TipoOri, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DescTipoOri
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.DescTipoOri, esSystemType.String);
			}
		} 
		
		public esQueryItem DataOri
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.DataOri, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAber
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.DataAber, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QtdeAcoe
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.QtdeAcoe, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeAcoeSegr
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.QtdeAcoeSegr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVenc
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.DataVenc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PrecMed
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.PrecMed, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaRemu
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TaxaRemu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaComi
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TaxaComi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdeAcoeOrig
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.QtdeAcoeOrig, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrecDiaAnte
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.PrecDiaAnte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem CodCliOri
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.CodCliOri, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValBrut
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValBrut, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValBrutDoad
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValBrutDoad, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValComi
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValComi, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValEmolCblc
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValEmolCblc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValIr
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValIr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValLiq
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.ValLiq, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoMercBtc
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.TipoMercBtc, esSystemType.String);
			}
		} 
		
		public esQueryItem SituCotr
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.SituCotr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VcfposiBtcMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VcfposiBtcCollection")]
	public partial class VcfposiBtcCollection : esVcfposiBtcCollection, IEnumerable<VcfposiBtc>
	{
		public VcfposiBtcCollection()
		{

		}
		
		public static implicit operator List<VcfposiBtc>(VcfposiBtcCollection coll)
		{
			List<VcfposiBtc> list = new List<VcfposiBtc>();
			
			foreach (VcfposiBtc emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VcfposiBtcMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfposiBtcQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VcfposiBtc(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VcfposiBtc();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VcfposiBtcQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfposiBtcQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VcfposiBtcQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VcfposiBtc AddNew()
		{
			VcfposiBtc entity = base.AddNewEntity() as VcfposiBtc;
			
			return entity;
		}

		public VcfposiBtc FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as VcfposiBtc;
		}


		#region IEnumerable<VcfposiBtc> Members

		IEnumerator<VcfposiBtc> IEnumerable<VcfposiBtc>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VcfposiBtc;
			}
		}

		#endregion
		
		private VcfposiBtcQuery query;
	}


	/// <summary>
	/// Encapsulates the 'VCFPOSI_BTC' table
	/// </summary>

	[Serializable]
	public partial class VcfposiBtc : esVcfposiBtc
	{
		public VcfposiBtc()
		{

		}
	
		public VcfposiBtc(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VcfposiBtcMetadata.Meta();
			}
		}
		
		
		
		override protected esVcfposiBtcQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfposiBtcQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VcfposiBtcQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfposiBtcQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VcfposiBtcQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VcfposiBtcQuery query;
	}



	[Serializable]
	public partial class VcfposiBtcQuery : esVcfposiBtcQuery
	{
		public VcfposiBtcQuery()
		{

		}		
		
		public VcfposiBtcQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VcfposiBtcMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VcfposiBtcMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TipoCotr, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TipoCotr;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.CodCli, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.CodCli;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.NumCotr, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.NumCotr;	
			c.NumericPrecision = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TipoMerc, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TipoMerc;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.NumCotrOrig, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.NumCotrOrig;	
			c.NumericPrecision = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TipoOri, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TipoOri;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.DescTipoOri, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.DescTipoOri;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.DataOri, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.DataOri;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.DataAber, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.DataAber;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.QtdeAcoe, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.QtdeAcoe;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.QtdeAcoeSegr, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.QtdeAcoeSegr;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.DataVenc, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.DataVenc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.PrecMed, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.PrecMed;	
			c.NumericPrecision = 18;
			c.NumericScale = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TaxaRemu, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TaxaRemu;	
			c.NumericPrecision = 10;
			c.NumericScale = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TaxaComi, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TaxaComi;	
			c.NumericPrecision = 10;
			c.NumericScale = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.QtdeAcoeOrig, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.QtdeAcoeOrig;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.PrecDiaAnte, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.PrecDiaAnte;	
			c.NumericPrecision = 18;
			c.NumericScale = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.CodNeg, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.CodCliOri, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.CodCliOri;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValBrut, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValBrut;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValBrutDoad, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValBrutDoad;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValComi, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValComi;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValEmolCblc, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValEmolCblc;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValIr, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValIr;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.ValLiq, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.ValLiq;	
			c.NumericPrecision = 38;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.TipoMercBtc, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.TipoMercBtc;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.SituCotr, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.SituCotr;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfposiBtcMetadata.ColumnNames.Id, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfposiBtcMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VcfposiBtcMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TipoCotr = "TIPO_COTR";
			 public const string CodCli = "COD_CLI";
			 public const string NumCotr = "NUM_COTR";
			 public const string TipoMerc = "TIPO_MERC";
			 public const string NumCotrOrig = "NUM_COTR_ORIG";
			 public const string TipoOri = "TIPO_ORI";
			 public const string DescTipoOri = "DESC_TIPO_ORI";
			 public const string DataOri = "DATA_ORI";
			 public const string DataAber = "DATA_ABER";
			 public const string QtdeAcoe = "QTDE_ACOE";
			 public const string QtdeAcoeSegr = "QTDE_ACOE_SEGR";
			 public const string DataVenc = "DATA_VENC";
			 public const string PrecMed = "PREC_MED";
			 public const string TaxaRemu = "TAXA_REMU";
			 public const string TaxaComi = "TAXA_COMI";
			 public const string QtdeAcoeOrig = "QTDE_ACOE_ORIG";
			 public const string PrecDiaAnte = "PREC_DIA_ANTE";
			 public const string CodNeg = "COD_NEG";
			 public const string CodCliOri = "COD_CLI_ORI";
			 public const string ValBrut = "VAL_BRUT";
			 public const string ValBrutDoad = "VAL_BRUT_DOAD";
			 public const string ValComi = "VAL_COMI";
			 public const string ValEmolCblc = "VAL_EMOL_CBLC";
			 public const string ValIr = "VAL_IR";
			 public const string ValLiq = "VAL_LIQ";
			 public const string TipoMercBtc = "TIPO_MERC_BTC";
			 public const string SituCotr = "SITU_COTR";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TipoCotr = "TipoCotr";
			 public const string CodCli = "CodCli";
			 public const string NumCotr = "NumCotr";
			 public const string TipoMerc = "TipoMerc";
			 public const string NumCotrOrig = "NumCotrOrig";
			 public const string TipoOri = "TipoOri";
			 public const string DescTipoOri = "DescTipoOri";
			 public const string DataOri = "DataOri";
			 public const string DataAber = "DataAber";
			 public const string QtdeAcoe = "QtdeAcoe";
			 public const string QtdeAcoeSegr = "QtdeAcoeSegr";
			 public const string DataVenc = "DataVenc";
			 public const string PrecMed = "PrecMed";
			 public const string TaxaRemu = "TaxaRemu";
			 public const string TaxaComi = "TaxaComi";
			 public const string QtdeAcoeOrig = "QtdeAcoeOrig";
			 public const string PrecDiaAnte = "PrecDiaAnte";
			 public const string CodNeg = "CodNeg";
			 public const string CodCliOri = "CodCliOri";
			 public const string ValBrut = "ValBrut";
			 public const string ValBrutDoad = "ValBrutDoad";
			 public const string ValComi = "ValComi";
			 public const string ValEmolCblc = "ValEmolCblc";
			 public const string ValIr = "ValIr";
			 public const string ValLiq = "ValLiq";
			 public const string TipoMercBtc = "TipoMercBtc";
			 public const string SituCotr = "SituCotr";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VcfposiBtcMetadata))
			{
				if(VcfposiBtcMetadata.mapDelegates == null)
				{
					VcfposiBtcMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VcfposiBtcMetadata.meta == null)
				{
					VcfposiBtcMetadata.meta = new VcfposiBtcMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("TIPO_COTR", new esTypeMap("CHAR", "System.String"));
				meta.AddTypeMap("COD_CLI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NUM_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_MERC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_COTR_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_ORI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DESC_TIPO_ORI", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATA_ORI", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_ABER", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("QTDE_ACOE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_ACOE_SEGR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DATA_VENC", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("PREC_MED", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TAXA_REMU", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TAXA_COMI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("QTDE_ACOE_ORIG", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("PREC_DIA_ANTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("COD_CLI_ORI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_BRUT", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_BRUT_DOAD", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_COMI", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_EMOL_CBLC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_IR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("VAL_LIQ", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TIPO_MERC_BTC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("SITU_COTR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "VCFPOSI_BTC";
				meta.Destination = "VCFPOSI_BTC";
				
				meta.spInsert = "proc_VCFPOSI_BTCInsert";				
				meta.spUpdate = "proc_VCFPOSI_BTCUpdate";		
				meta.spDelete = "proc_VCFPOSI_BTCDelete";
				meta.spLoadAll = "proc_VCFPOSI_BTCLoadAll";
				meta.spLoadByPrimaryKey = "proc_VCFPOSI_BTCLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VcfposiBtcMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
