/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 29/08/2012 10:22:58
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esVcfpapelCollection : esEntityCollection
	{
		public esVcfpapelCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VcfpapelCollection";
		}

		#region Query Logic
		protected void InitQuery(esVcfpapelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVcfpapelQuery);
		}
		#endregion
		
		virtual public Vcfpapel DetachEntity(Vcfpapel entity)
		{
			return base.DetachEntity(entity) as Vcfpapel;
		}
		
		virtual public Vcfpapel AttachEntity(Vcfpapel entity)
		{
			return base.AttachEntity(entity) as Vcfpapel;
		}
		
		virtual public void Combine(VcfpapelCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Vcfpapel this[int index]
		{
			get
			{
				return base[index] as Vcfpapel;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Vcfpapel);
		}
	}



	[Serializable]
	abstract public class esVcfpapel : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVcfpapelQuery GetDynamicQuery()
		{
			return null;
		}

		public esVcfpapel()
		{

		}

		public esVcfpapel(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esVcfpapelQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esVcfpapelQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CodIsin": this.str.CodIsin = (string)value; break;							
						case "NumDist": this.str.NumDist = (string)value; break;							
						case "CodNeg": this.str.CodNeg = (string)value; break;							
						case "DataIniNeg": this.str.DataIniNeg = (string)value; break;							
						case "DataLimNeg": this.str.DataLimNeg = (string)value; break;							
						case "TipoMerc": this.str.TipoMerc = (string)value; break;							
						case "TipoGrup": this.str.TipoGrup = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "NumDist":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NumDist = (System.Decimal?)value;
							break;
						
						case "DataIniNeg":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataIniNeg = (System.DateTime?)value;
							break;
						
						case "DataLimNeg":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLimNeg = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to VCFPAPEL.COD_ISIN
		/// </summary>
		virtual public System.String CodIsin
		{
			get
			{
				return base.GetSystemString(VcfpapelMetadata.ColumnNames.CodIsin);
			}
			
			set
			{
				base.SetSystemString(VcfpapelMetadata.ColumnNames.CodIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.NUM_DIST
		/// </summary>
		virtual public System.Decimal? NumDist
		{
			get
			{
				return base.GetSystemDecimal(VcfpapelMetadata.ColumnNames.NumDist);
			}
			
			set
			{
				base.SetSystemDecimal(VcfpapelMetadata.ColumnNames.NumDist, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.COD_NEG
		/// </summary>
		virtual public System.String CodNeg
		{
			get
			{
				return base.GetSystemString(VcfpapelMetadata.ColumnNames.CodNeg);
			}
			
			set
			{
				base.SetSystemString(VcfpapelMetadata.ColumnNames.CodNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.DATA_INI_NEG
		/// </summary>
		virtual public System.DateTime? DataIniNeg
		{
			get
			{
				return base.GetSystemDateTime(VcfpapelMetadata.ColumnNames.DataIniNeg);
			}
			
			set
			{
				base.SetSystemDateTime(VcfpapelMetadata.ColumnNames.DataIniNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.DATA_LIM_NEG
		/// </summary>
		virtual public System.DateTime? DataLimNeg
		{
			get
			{
				return base.GetSystemDateTime(VcfpapelMetadata.ColumnNames.DataLimNeg);
			}
			
			set
			{
				base.SetSystemDateTime(VcfpapelMetadata.ColumnNames.DataLimNeg, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.TIPO_MERC
		/// </summary>
		virtual public System.String TipoMerc
		{
			get
			{
				return base.GetSystemString(VcfpapelMetadata.ColumnNames.TipoMerc);
			}
			
			set
			{
				base.SetSystemString(VcfpapelMetadata.ColumnNames.TipoMerc, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.TIPO_GRUP
		/// </summary>
		virtual public System.String TipoGrup
		{
			get
			{
				return base.GetSystemString(VcfpapelMetadata.ColumnNames.TipoGrup);
			}
			
			set
			{
				base.SetSystemString(VcfpapelMetadata.ColumnNames.TipoGrup, value);
			}
		}
		
		/// <summary>
		/// Maps to VCFPAPEL.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(VcfpapelMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(VcfpapelMetadata.ColumnNames.Id, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVcfpapel entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CodIsin
			{
				get
				{
					System.String data = entity.CodIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodIsin = null;
					else entity.CodIsin = Convert.ToString(value);
				}
			}
				
			public System.String NumDist
			{
				get
				{
					System.Decimal? data = entity.NumDist;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDist = null;
					else entity.NumDist = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodNeg
			{
				get
				{
					System.String data = entity.CodNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodNeg = null;
					else entity.CodNeg = Convert.ToString(value);
				}
			}
				
			public System.String DataIniNeg
			{
				get
				{
					System.DateTime? data = entity.DataIniNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataIniNeg = null;
					else entity.DataIniNeg = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLimNeg
			{
				get
				{
					System.DateTime? data = entity.DataLimNeg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLimNeg = null;
					else entity.DataLimNeg = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoMerc
			{
				get
				{
					System.String data = entity.TipoMerc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMerc = null;
					else entity.TipoMerc = Convert.ToString(value);
				}
			}
				
			public System.String TipoGrup
			{
				get
				{
					System.String data = entity.TipoGrup;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoGrup = null;
					else entity.TipoGrup = Convert.ToString(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
			

			private esVcfpapel entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVcfpapelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVcfpapel can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Vcfpapel : esVcfpapel
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVcfpapelQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VcfpapelMetadata.Meta();
			}
		}	
		

		public esQueryItem CodIsin
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.CodIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem NumDist
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.NumDist, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodNeg
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.CodNeg, esSystemType.String);
			}
		} 
		
		public esQueryItem DataIniNeg
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.DataIniNeg, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLimNeg
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.DataLimNeg, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoMerc
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.TipoMerc, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoGrup
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.TipoGrup, esSystemType.String);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, VcfpapelMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VcfpapelCollection")]
	public partial class VcfpapelCollection : esVcfpapelCollection, IEnumerable<Vcfpapel>
	{
		public VcfpapelCollection()
		{

		}
		
		public static implicit operator List<Vcfpapel>(VcfpapelCollection coll)
		{
			List<Vcfpapel> list = new List<Vcfpapel>();
			
			foreach (Vcfpapel emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VcfpapelMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfpapelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Vcfpapel(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Vcfpapel();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VcfpapelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfpapelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VcfpapelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Vcfpapel AddNew()
		{
			Vcfpapel entity = base.AddNewEntity() as Vcfpapel;
			
			return entity;
		}

		public Vcfpapel FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Vcfpapel;
		}


		#region IEnumerable<Vcfpapel> Members

		IEnumerator<Vcfpapel> IEnumerable<Vcfpapel>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Vcfpapel;
			}
		}

		#endregion
		
		private VcfpapelQuery query;
	}


	/// <summary>
	/// Encapsulates the 'VCFPAPEL' table
	/// </summary>

	[Serializable]
	public partial class Vcfpapel : esVcfpapel
	{
		public Vcfpapel()
		{

		}
	
		public Vcfpapel(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VcfpapelMetadata.Meta();
			}
		}
		
		
		
		override protected esVcfpapelQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VcfpapelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VcfpapelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VcfpapelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VcfpapelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VcfpapelQuery query;
	}



	[Serializable]
	public partial class VcfpapelQuery : esVcfpapelQuery
	{
		public VcfpapelQuery()
		{

		}		
		
		public VcfpapelQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VcfpapelMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VcfpapelMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.CodIsin, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfpapelMetadata.PropertyNames.CodIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.NumDist, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfpapelMetadata.PropertyNames.NumDist;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.CodNeg, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfpapelMetadata.PropertyNames.CodNeg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.DataIniNeg, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfpapelMetadata.PropertyNames.DataIniNeg;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.DataLimNeg, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VcfpapelMetadata.PropertyNames.DataLimNeg;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.TipoMerc, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfpapelMetadata.PropertyNames.TipoMerc;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.TipoGrup, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = VcfpapelMetadata.PropertyNames.TipoGrup;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VcfpapelMetadata.ColumnNames.Id, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = VcfpapelMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VcfpapelMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CodIsin = "COD_ISIN";
			 public const string NumDist = "NUM_DIST";
			 public const string CodNeg = "COD_NEG";
			 public const string DataIniNeg = "DATA_INI_NEG";
			 public const string DataLimNeg = "DATA_LIM_NEG";
			 public const string TipoMerc = "TIPO_MERC";
			 public const string TipoGrup = "TIPO_GRUP";
			 public const string Id = "ID";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CodIsin = "CodIsin";
			 public const string NumDist = "NumDist";
			 public const string CodNeg = "CodNeg";
			 public const string DataIniNeg = "DataIniNeg";
			 public const string DataLimNeg = "DataLimNeg";
			 public const string TipoMerc = "TipoMerc";
			 public const string TipoGrup = "TipoGrup";
			 public const string Id = "Id";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VcfpapelMetadata))
			{
				if(VcfpapelMetadata.mapDelegates == null)
				{
					VcfpapelMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VcfpapelMetadata.meta == null)
				{
					VcfpapelMetadata.meta = new VcfpapelMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("COD_ISIN", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("NUM_DIST", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("COD_NEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DATA_INI_NEG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DATA_LIM_NEG", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("TIPO_MERC", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TIPO_GRUP", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "VCFPAPEL";
				meta.Destination = "VCFPAPEL";
				
				meta.spInsert = "proc_VCFPAPELInsert";				
				meta.spUpdate = "proc_VCFPAPELUpdate";		
				meta.spDelete = "proc_VCFPAPELDelete";
				meta.spLoadAll = "proc_VCFPAPELLoadAll";
				meta.spLoadByPrimaryKey = "proc_VCFPAPELLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VcfpapelMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
