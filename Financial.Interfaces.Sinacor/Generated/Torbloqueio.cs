/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 17/11/2014 12:22:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTorbloqueioCollection : esEntityCollection
	{
		public esTorbloqueioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TorbloqueioCollection";
		}

		#region Query Logic
		protected void InitQuery(esTorbloqueioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTorbloqueioQuery);
		}
		#endregion
		
		virtual public Torbloqueio DetachEntity(Torbloqueio entity)
		{
			return base.DetachEntity(entity) as Torbloqueio;
		}
		
		virtual public Torbloqueio AttachEntity(Torbloqueio entity)
		{
			return base.AttachEntity(entity) as Torbloqueio;
		}
		
		virtual public void Combine(TorbloqueioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Torbloqueio this[int index]
		{
			get
			{
				return base[index] as Torbloqueio;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Torbloqueio);
		}
	}



	[Serializable]
	abstract public class esTorbloqueio : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTorbloqueioQuery GetDynamicQuery()
		{
			return null;
		}

		public esTorbloqueio()
		{

		}

		public esTorbloqueio(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTorbloqueioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTorbloqueioQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "CdAssessor": this.str.CdAssessor = (string)value; break;							
						case "CdAfinidade": this.str.CdAfinidade = (string)value; break;							
						case "CdCliente": this.str.CdCliente = (string)value; break;							
						case "TpCliente": this.str.TpCliente = (string)value; break;							
						case "DtInicio": this.str.DtInicio = (string)value; break;							
						case "DtFinal": this.str.DtFinal = (string)value; break;							
						case "CdNrmerc": this.str.CdNrmerc = (string)value; break;							
						case "CdCodneg": this.str.CdCodneg = (string)value; break;							
						case "TpBloqueio": this.str.TpBloqueio = (string)value; break;							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "DsBloqueio": this.str.DsBloqueio = (string)value; break;							
						case "CdUsuario": this.str.CdUsuario = (string)value; break;							
						case "NmTerminal": this.str.NmTerminal = (string)value; break;							
						case "DtSistema": this.str.DtSistema = (string)value; break;							
						case "CodBloq": this.str.CodBloq = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "CdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAssessor = (System.Decimal?)value;
							break;
						
						case "CdAfinidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdAfinidade = (System.Decimal?)value;
							break;
						
						case "CdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdCliente = (System.Decimal?)value;
							break;
						
						case "TpCliente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpCliente = (System.Decimal?)value;
							break;
						
						case "DtInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtInicio = (System.DateTime?)value;
							break;
						
						case "DtFinal":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtFinal = (System.DateTime?)value;
							break;
						
						case "CdNrmerc":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdNrmerc = (System.Decimal?)value;
							break;
						
						case "CdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CdUsuario = (System.Decimal?)value;
							break;
						
						case "DtSistema":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtSistema = (System.DateTime?)value;
							break;
						
						case "CodBloq":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CodBloq = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TORBLOQUEIO.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_ASSESSOR
		/// </summary>
		virtual public System.Decimal? CdAssessor
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdAssessor);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_AFINIDADE
		/// </summary>
		virtual public System.Decimal? CdAfinidade
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdAfinidade);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdAfinidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_CLIENTE
		/// </summary>
		virtual public System.Decimal? CdCliente
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.TP_CLIENTE
		/// </summary>
		virtual public System.Decimal? TpCliente
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.TpCliente);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.TpCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.DT_INICIO
		/// </summary>
		virtual public System.DateTime? DtInicio
		{
			get
			{
				return base.GetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtInicio);
			}
			
			set
			{
				base.SetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.DT_FINAL
		/// </summary>
		virtual public System.DateTime? DtFinal
		{
			get
			{
				return base.GetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtFinal);
			}
			
			set
			{
				base.SetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_NRMERC
		/// </summary>
		virtual public System.Decimal? CdNrmerc
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdNrmerc);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdNrmerc, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_CODNEG
		/// </summary>
		virtual public System.String CdCodneg
		{
			get
			{
				return base.GetSystemString(TorbloqueioMetadata.ColumnNames.CdCodneg);
			}
			
			set
			{
				base.SetSystemString(TorbloqueioMetadata.ColumnNames.CdCodneg, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.TP_BLOQUEIO
		/// </summary>
		virtual public System.String TpBloqueio
		{
			get
			{
				return base.GetSystemString(TorbloqueioMetadata.ColumnNames.TpBloqueio);
			}
			
			set
			{
				base.SetSystemString(TorbloqueioMetadata.ColumnNames.TpBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TorbloqueioMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TorbloqueioMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.DS_BLOQUEIO
		/// </summary>
		virtual public System.String DsBloqueio
		{
			get
			{
				return base.GetSystemString(TorbloqueioMetadata.ColumnNames.DsBloqueio);
			}
			
			set
			{
				base.SetSystemString(TorbloqueioMetadata.ColumnNames.DsBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.CD_USUARIO
		/// </summary>
		virtual public System.Decimal? CdUsuario
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdUsuario);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.NM_TERMINAL
		/// </summary>
		virtual public System.String NmTerminal
		{
			get
			{
				return base.GetSystemString(TorbloqueioMetadata.ColumnNames.NmTerminal);
			}
			
			set
			{
				base.SetSystemString(TorbloqueioMetadata.ColumnNames.NmTerminal, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.DT_SISTEMA
		/// </summary>
		virtual public System.DateTime? DtSistema
		{
			get
			{
				return base.GetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtSistema);
			}
			
			set
			{
				base.SetSystemDateTime(TorbloqueioMetadata.ColumnNames.DtSistema, value);
			}
		}
		
		/// <summary>
		/// Maps to TORBLOQUEIO.COD_BLOQ
		/// </summary>
		virtual public System.Decimal? CodBloq
		{
			get
			{
				return base.GetSystemDecimal(TorbloqueioMetadata.ColumnNames.CodBloq);
			}
			
			set
			{
				base.SetSystemDecimal(TorbloqueioMetadata.ColumnNames.CodBloq, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTorbloqueio entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAssessor
			{
				get
				{
					System.Decimal? data = entity.CdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAssessor = null;
					else entity.CdAssessor = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAfinidade
			{
				get
				{
					System.Decimal? data = entity.CdAfinidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAfinidade = null;
					else entity.CdAfinidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCliente
			{
				get
				{
					System.Decimal? data = entity.CdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCliente = null;
					else entity.CdCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpCliente
			{
				get
				{
					System.Decimal? data = entity.TpCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpCliente = null;
					else entity.TpCliente = Convert.ToDecimal(value);
				}
			}
				
			public System.String DtInicio
			{
				get
				{
					System.DateTime? data = entity.DtInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtInicio = null;
					else entity.DtInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtFinal
			{
				get
				{
					System.DateTime? data = entity.DtFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtFinal = null;
					else entity.DtFinal = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdNrmerc
			{
				get
				{
					System.Decimal? data = entity.CdNrmerc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNrmerc = null;
					else entity.CdNrmerc = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdCodneg
			{
				get
				{
					System.String data = entity.CdCodneg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdCodneg = null;
					else entity.CdCodneg = Convert.ToString(value);
				}
			}
				
			public System.String TpBloqueio
			{
				get
				{
					System.String data = entity.TpBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpBloqueio = null;
					else entity.TpBloqueio = Convert.ToString(value);
				}
			}
				
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String DsBloqueio
			{
				get
				{
					System.String data = entity.DsBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsBloqueio = null;
					else entity.DsBloqueio = Convert.ToString(value);
				}
			}
				
			public System.String CdUsuario
			{
				get
				{
					System.Decimal? data = entity.CdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdUsuario = null;
					else entity.CdUsuario = Convert.ToDecimal(value);
				}
			}
				
			public System.String NmTerminal
			{
				get
				{
					System.String data = entity.NmTerminal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmTerminal = null;
					else entity.NmTerminal = Convert.ToString(value);
				}
			}
				
			public System.String DtSistema
			{
				get
				{
					System.DateTime? data = entity.DtSistema;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtSistema = null;
					else entity.DtSistema = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodBloq
			{
				get
				{
					System.Decimal? data = entity.CodBloq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodBloq = null;
					else entity.CodBloq = Convert.ToDecimal(value);
				}
			}
			

			private esTorbloqueio entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTorbloqueioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTorbloqueio can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Torbloqueio : esTorbloqueio
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTorbloqueioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TorbloqueioMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAssessor
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdAssessor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAfinidade
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdAfinidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCliente
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpCliente
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.TpCliente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DtInicio
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.DtInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DtFinal
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.DtFinal, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdNrmerc
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdNrmerc, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdCodneg
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdCodneg, esSystemType.String);
			}
		} 
		
		public esQueryItem TpBloqueio
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.TpBloqueio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem DsBloqueio
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.DsBloqueio, esSystemType.String);
			}
		} 
		
		public esQueryItem CdUsuario
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CdUsuario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NmTerminal
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.NmTerminal, esSystemType.String);
			}
		} 
		
		public esQueryItem DtSistema
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.DtSistema, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodBloq
		{
			get
			{
				return new esQueryItem(this, TorbloqueioMetadata.ColumnNames.CodBloq, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TorbloqueioCollection")]
	public partial class TorbloqueioCollection : esTorbloqueioCollection, IEnumerable<Torbloqueio>
	{
		public TorbloqueioCollection()
		{

		}
		
		public static implicit operator List<Torbloqueio>(TorbloqueioCollection coll)
		{
			List<Torbloqueio> list = new List<Torbloqueio>();
			
			foreach (Torbloqueio emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TorbloqueioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorbloqueioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Torbloqueio(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Torbloqueio();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TorbloqueioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorbloqueioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TorbloqueioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Torbloqueio AddNew()
		{
			Torbloqueio entity = base.AddNewEntity() as Torbloqueio;
			
			return entity;
		}

		public Torbloqueio FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Torbloqueio;
		}


		#region IEnumerable<Torbloqueio> Members

		IEnumerator<Torbloqueio> IEnumerable<Torbloqueio>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Torbloqueio;
			}
		}

		#endregion
		
		private TorbloqueioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TORBLOQUEIO' table
	/// </summary>

	[Serializable]
	public partial class Torbloqueio : esTorbloqueio
	{
		public Torbloqueio()
		{

		}
	
		public Torbloqueio(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TorbloqueioMetadata.Meta();
			}
		}
		
		
		
		override protected esTorbloqueioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorbloqueioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TorbloqueioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorbloqueioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TorbloqueioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TorbloqueioQuery query;
	}



	[Serializable]
	public partial class TorbloqueioQuery : esTorbloqueioQuery
	{
		public TorbloqueioQuery()
		{

		}		
		
		public TorbloqueioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TorbloqueioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TorbloqueioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.Id, 0, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 38;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdAssessor, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdAssessor;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdAfinidade, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdAfinidade;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdCliente, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdCliente;	
			c.NumericPrecision = 7;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.TpCliente, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.TpCliente;	
			c.NumericPrecision = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.DtInicio, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.DtInicio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.DtFinal, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.DtFinal;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdNrmerc, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdNrmerc;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdCodneg, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdCodneg;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.TpBloqueio, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.TpBloqueio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdNatope, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.DsBloqueio, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.DsBloqueio;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CdUsuario, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CdUsuario;	
			c.NumericPrecision = 6;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.NmTerminal, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.NmTerminal;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.DtSistema, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.DtSistema;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorbloqueioMetadata.ColumnNames.CodBloq, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorbloqueioMetadata.PropertyNames.CodBloq;	
			c.NumericPrecision = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TorbloqueioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string CdAssessor = "CD_ASSESSOR";
			 public const string CdAfinidade = "CD_AFINIDADE";
			 public const string CdCliente = "CD_CLIENTE";
			 public const string TpCliente = "TP_CLIENTE";
			 public const string DtInicio = "DT_INICIO";
			 public const string DtFinal = "DT_FINAL";
			 public const string CdNrmerc = "CD_NRMERC";
			 public const string CdCodneg = "CD_CODNEG";
			 public const string TpBloqueio = "TP_BLOQUEIO";
			 public const string CdNatope = "CD_NATOPE";
			 public const string DsBloqueio = "DS_BLOQUEIO";
			 public const string CdUsuario = "CD_USUARIO";
			 public const string NmTerminal = "NM_TERMINAL";
			 public const string DtSistema = "DT_SISTEMA";
			 public const string CodBloq = "COD_BLOQ";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string CdAssessor = "CdAssessor";
			 public const string CdAfinidade = "CdAfinidade";
			 public const string CdCliente = "CdCliente";
			 public const string TpCliente = "TpCliente";
			 public const string DtInicio = "DtInicio";
			 public const string DtFinal = "DtFinal";
			 public const string CdNrmerc = "CdNrmerc";
			 public const string CdCodneg = "CdCodneg";
			 public const string TpBloqueio = "TpBloqueio";
			 public const string CdNatope = "CdNatope";
			 public const string DsBloqueio = "DsBloqueio";
			 public const string CdUsuario = "CdUsuario";
			 public const string NmTerminal = "NmTerminal";
			 public const string DtSistema = "DtSistema";
			 public const string CodBloq = "CodBloq";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TorbloqueioMetadata))
			{
				if(TorbloqueioMetadata.mapDelegates == null)
				{
					TorbloqueioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TorbloqueioMetadata.meta == null)
				{
					TorbloqueioMetadata.meta = new TorbloqueioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_ASSESSOR", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_AFINIDADE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_CLIENTE", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("DT_INICIO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("DT_FINAL", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("CD_NRMERC", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("CD_CODNEG", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("TP_BLOQUEIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_NATOPE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DS_BLOQUEIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_USUARIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NM_TERMINAL", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_SISTEMA", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("COD_BLOQ", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TORBLOQUEIO";
				meta.Destination = "TORBLOQUEIO";
				
				meta.spInsert = "proc_TORBLOQUEIOInsert";				
				meta.spUpdate = "proc_TORBLOQUEIOUpdate";		
				meta.spDelete = "proc_TORBLOQUEIODelete";
				meta.spLoadAll = "proc_TORBLOQUEIOLoadAll";
				meta.spLoadByPrimaryKey = "proc_TORBLOQUEIOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TorbloqueioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
