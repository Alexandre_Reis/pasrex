/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 18/06/2009 14:04:59
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





namespace Financial.Interfaces.Sinacor
{

	[Serializable]
	abstract public class esTorneghCollection : esEntityCollection
	{
		public esTorneghCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TorneghCollection";
		}

		#region Query Logic
		protected void InitQuery(esTorneghQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTorneghQuery);
		}
		#endregion
		
		virtual public Tornegh DetachEntity(Tornegh entity)
		{
			return base.DetachEntity(entity) as Tornegh;
		}
		
		virtual public Tornegh AttachEntity(Tornegh entity)
		{
			return base.AttachEntity(entity) as Tornegh;
		}
		
		virtual public void Combine(TorneghCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Tornegh this[int index]
		{
			get
			{
				return base[index] as Tornegh;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Tornegh);
		}
	}



	[Serializable]
	abstract public class esTornegh : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTorneghQuery GetDynamicQuery()
		{
			return null;
		}

		public esTornegh()
		{

		}

		public esTornegh(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Decimal id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Decimal id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTorneghQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Decimal id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Decimal id)
		{
			esTorneghQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Decimal id)
		{
			esParameters parms = new esParameters();
			parms.Add("ID",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdNatope": this.str.CdNatope = (string)value; break;							
						case "CdNegocio": this.str.CdNegocio = (string)value; break;							
						case "DtPregao": this.str.DtPregao = (string)value; break;							
						case "Id": this.str.Id = (string)value; break;							
						case "NrNegocio": this.str.NrNegocio = (string)value; break;							
						case "TpVcoter": this.str.TpVcoter = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DtPregao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtPregao = (System.DateTime?)value;
							break;
						
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Id = (System.Decimal?)value;
							break;
						
						case "NrNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.NrNegocio = (System.Decimal?)value;
							break;
						
						case "TpVcoter":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TpVcoter = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TORNEGH.CD_NATOPE
		/// </summary>
		virtual public System.String CdNatope
		{
			get
			{
				return base.GetSystemString(TorneghMetadata.ColumnNames.CdNatope);
			}
			
			set
			{
				base.SetSystemString(TorneghMetadata.ColumnNames.CdNatope, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGH.CD_NEGOCIO
		/// </summary>
		virtual public System.String CdNegocio
		{
			get
			{
				return base.GetSystemString(TorneghMetadata.ColumnNames.CdNegocio);
			}
			
			set
			{
				base.SetSystemString(TorneghMetadata.ColumnNames.CdNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGH.DT_PREGAO
		/// </summary>
		virtual public System.DateTime? DtPregao
		{
			get
			{
				return base.GetSystemDateTime(TorneghMetadata.ColumnNames.DtPregao);
			}
			
			set
			{
				base.SetSystemDateTime(TorneghMetadata.ColumnNames.DtPregao, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGH.ID
		/// </summary>
		virtual public System.Decimal? Id
		{
			get
			{
				return base.GetSystemDecimal(TorneghMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemDecimal(TorneghMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGH.NR_NEGOCIO
		/// </summary>
		virtual public System.Decimal? NrNegocio
		{
			get
			{
				return base.GetSystemDecimal(TorneghMetadata.ColumnNames.NrNegocio);
			}
			
			set
			{
				base.SetSystemDecimal(TorneghMetadata.ColumnNames.NrNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TORNEGH.TP_VCOTER
		/// </summary>
		virtual public System.Decimal? TpVcoter
		{
			get
			{
				return base.GetSystemDecimal(TorneghMetadata.ColumnNames.TpVcoter);
			}
			
			set
			{
				base.SetSystemDecimal(TorneghMetadata.ColumnNames.TpVcoter, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTornegh entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdNatope
			{
				get
				{
					System.String data = entity.CdNatope;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNatope = null;
					else entity.CdNatope = Convert.ToString(value);
				}
			}
				
			public System.String CdNegocio
			{
				get
				{
					System.String data = entity.CdNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdNegocio = null;
					else entity.CdNegocio = Convert.ToString(value);
				}
			}
				
			public System.String DtPregao
			{
				get
				{
					System.DateTime? data = entity.DtPregao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtPregao = null;
					else entity.DtPregao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Id
			{
				get
				{
					System.Decimal? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToDecimal(value);
				}
			}
				
			public System.String NrNegocio
			{
				get
				{
					System.Decimal? data = entity.NrNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrNegocio = null;
					else entity.NrNegocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String TpVcoter
			{
				get
				{
					System.Decimal? data = entity.TpVcoter;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TpVcoter = null;
					else entity.TpVcoter = Convert.ToDecimal(value);
				}
			}
			

			private esTornegh entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTorneghQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTornegh can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Tornegh : esTornegh
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTorneghQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TorneghMetadata.Meta();
			}
		}	
		

		public esQueryItem CdNatope
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.CdNatope, esSystemType.String);
			}
		} 
		
		public esQueryItem CdNegocio
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.CdNegocio, esSystemType.String);
			}
		} 
		
		public esQueryItem DtPregao
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.DtPregao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.Id, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NrNegocio
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.NrNegocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TpVcoter
		{
			get
			{
				return new esQueryItem(this, TorneghMetadata.ColumnNames.TpVcoter, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TorneghCollection")]
	public partial class TorneghCollection : esTorneghCollection, IEnumerable<Tornegh>
	{
		public TorneghCollection()
		{

		}
		
		public static implicit operator List<Tornegh>(TorneghCollection coll)
		{
			List<Tornegh> list = new List<Tornegh>();
			
			foreach (Tornegh emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TorneghMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorneghQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Tornegh(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Tornegh();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TorneghQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorneghQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TorneghQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Tornegh AddNew()
		{
			Tornegh entity = base.AddNewEntity() as Tornegh;
			
			return entity;
		}

		public Tornegh FindByPrimaryKey(System.Decimal id)
		{
			return base.FindByPrimaryKey(id) as Tornegh;
		}


		#region IEnumerable<Tornegh> Members

		IEnumerator<Tornegh> IEnumerable<Tornegh>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Tornegh;
			}
		}

		#endregion
		
		private TorneghQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TORNEGH' table
	/// </summary>

	[Serializable]
	public partial class Tornegh : esTornegh
	{
		public Tornegh()
		{

		}
	
		public Tornegh(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TorneghMetadata.Meta();
			}
		}
		
		
		
		override protected esTorneghQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TorneghQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TorneghQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TorneghQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TorneghQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TorneghQuery query;
	}



	[Serializable]
	public partial class TorneghQuery : esTorneghQuery
	{
		public TorneghQuery()
		{

		}		
		
		public TorneghQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TorneghMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TorneghMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TorneghMetadata.ColumnNames.CdNatope, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TorneghMetadata.PropertyNames.CdNatope;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorneghMetadata.ColumnNames.CdNegocio, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TorneghMetadata.PropertyNames.CdNegocio;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorneghMetadata.ColumnNames.DtPregao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TorneghMetadata.PropertyNames.DtPregao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorneghMetadata.ColumnNames.Id, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorneghMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorneghMetadata.ColumnNames.NrNegocio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorneghMetadata.PropertyNames.NrNegocio;	
			c.NumericPrecision = 9;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TorneghMetadata.ColumnNames.TpVcoter, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TorneghMetadata.PropertyNames.TpVcoter;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TorneghMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdNatope = "CD_NATOPE";
			 public const string CdNegocio = "CD_NEGOCIO";
			 public const string DtPregao = "DT_PREGAO";
			 public const string Id = "ID";
			 public const string NrNegocio = "NR_NEGOCIO";
			 public const string TpVcoter = "TP_VCOTER";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdNatope = "CdNatope";
			 public const string CdNegocio = "CdNegocio";
			 public const string DtPregao = "DtPregao";
			 public const string Id = "Id";
			 public const string NrNegocio = "NrNegocio";
			 public const string TpVcoter = "TpVcoter";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TorneghMetadata))
			{
				if(TorneghMetadata.mapDelegates == null)
				{
					TorneghMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TorneghMetadata.meta == null)
				{
					TorneghMetadata.meta = new TorneghMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CD_NATOPE", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("CD_NEGOCIO", new esTypeMap("VARCHAR2", "System.String"));
				meta.AddTypeMap("DT_PREGAO", new esTypeMap("DATE", "System.DateTime"));
				meta.AddTypeMap("ID", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("NR_NEGOCIO", new esTypeMap("NUMBER", "System.Decimal"));
				meta.AddTypeMap("TP_VCOTER", new esTypeMap("NUMBER", "System.Decimal"));			
				
				
				
				meta.Source = "TORNEGH";
				meta.Destination = "TORNEGH";
				
				meta.spInsert = "proc_TORNEGHInsert";				
				meta.spUpdate = "proc_TORNEGHUpdate";		
				meta.spDelete = "proc_TORNEGHDelete";
				meta.spLoadAll = "proc_TORNEGHLoadAll";
				meta.spLoadByPrimaryKey = "proc_TORNEGHLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TorneghMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
