﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 05/03/2010 11:41:50
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor {
	public partial class TsccligerCollection : esTsccligerCollection {

        public class InfoPessoaSinacor {

            public InfoPessoaSinacor() { }

            // CPF Ou CNPJ com 11 ou 14 caracateres
            public string Cpfcnpj;
            // Tipo Pessoa F ou J
            public string Tipo;
            // Nome Cliente
            public string Nome;
            // TipoCliente
            public decimal? TipoCliente;
            // É Pessoa Vinculada
            public string IsPessoaVinculada;
            // Data Nascimento
            public DateTime? DataNascimentoFundo;
            // Pessoa oPliticamente Exposta
            public string InPoliticoExp;
        }

        /// <summary>
        /// Retorna InfoPessoaSinacor com as informações da Pessoa
        /// </summary>
        /// <param name="cpfCGC">cpf da pessoa</param>
        /// <returns></returns>
        /// <exception cref="Exception">se não encontrou as informações</exception>
        public InfoPessoaSinacor RetornaInfoPessoa(decimal cpfCGC) {
            InfoPessoaSinacor info = new InfoPessoaSinacor();

            // Se Achou no Sinacor                    
            #region TSCCLIGER
            // Pega CD_CPFCGC e TP_PESSOA em tsccliger

            TsccligerCollection collCliGer = new TsccligerCollection();
            collCliGer.es.Connection.Name = "Sinacor";
            collCliGer.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            //
            collCliGer.Query.Select(collCliGer.Query.CdCpfcgc,
                                    collCliGer.Query.TpPessoa,
                                    collCliGer.Query.NmCliente,
                                    collCliGer.Query.TpCliente,
                                    collCliGer.Query.InPessVinc,
                                    collCliGer.Query.DtNascFund,
                                    collCliGer.Query.InPoliticoExp)
                   .Where(collCliGer.Query.CdCpfcgc == cpfCGC);
            //
            collCliGer.Query.Load();

            if (collCliGer.Count == 0) {
                throw new Exception("Não foi possível encontrar informações do cliente na tabela CLIGER: ");
            }

            info.Nome = collCliGer[0].NmCliente;
            if (info.Nome.Length > 255) {
                info.Nome = info.Nome.Substring(0, 255);
            }

            if (collCliGer[0].TpPessoa.ToUpper() == "F" || collCliGer[0].TpPessoa.ToUpper() == "J") {
                info.Tipo = collCliGer[0].TpPessoa.ToUpper() == "F" ? "F" : "J";
            }

            if (info.Tipo == "F") {
                info.Cpfcnpj = cpfCGC.ToString().PadLeft(11, '0');
            }
            else {
                info.Cpfcnpj = cpfCGC.ToString().PadLeft(14, '0');
            }

            info.TipoCliente = collCliGer[0].TpCliente;
            info.IsPessoaVinculada = collCliGer[0].InPessVinc;
            info.DataNascimentoFundo = collCliGer[0].DtNascFund;
            info.InPoliticoExp = collCliGer[0].InPoliticoExp;

            #endregion

            return info;
        }
	}
}