using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TbotitulosCollection : esTbotitulosCollection
	{
        public void BuscaAtivosPorIsin(string codigoIsin, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CdCodisi.Equal(codigoIsin),
                             this.Query.DtInineg.LessThanOrEqual(data),
                             this.Query.CdTpmerc.Equal("VIS"));
            this.Query.Load();
        }

        public void BuscaAtivoPorCodigo(string cdAtivoBolsa)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CdCodneg.Equal(cdAtivoBolsa));
            this.Query.OrderBy(this.Query.DtInineg.Descending);
            this.Query.Load();
        }
	}
}
