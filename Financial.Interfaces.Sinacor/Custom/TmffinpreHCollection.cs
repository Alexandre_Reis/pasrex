﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TmffinpreHCollection : esTmffinpreHCollection
	{
        /// <summary>
        /// Busca operações no Sinacor a serem importadas, mas a partir da TmffinpreH.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesSINACOR(Int32 codigoCliente, DateTime dataOperacao)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();

            this.Query.Where(this.Query.CdCliente.Equal(codigoCliente),
                             this.Query.DtNegocio.Equal(dataOperacao),
                             this.Query.TpNegocio.NotIn("AJ", "LQ"),
                             this.Query.CdCommod.NotLike("VOI%"),
                             this.Query.CdCommod.NotLike("VTC%"),
                             this.Query.CdCommod.NotLike("VID%"),
                             this.Query.CdCommod.NotLike("SDC%"),                             
                             this.Query.CdCommod.NotLike("IR1%"),
                             this.Query.CdCommod.NotLike("DR1%"),
                             this.Query.CdCommod.NotLike("CR1%"),
                             this.Query.CdCommod.NotLike("BR1%"),
                             this.Query.CdCommod.NotLike("SR1%"),
                             this.Query.CdCommod.NotLike("FRC%"),
                             this.Query.CdMercad.NotIn("SWP", "FLX"))
                      .OrderBy(this.Query.CdCommod.Ascending);
            this.Query.Load();
        }
	}
}
