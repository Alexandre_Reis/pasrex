using EntitySpaces.Interfaces;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TbtliquidacaoCollection : esTbtliquidacaoCollection
	{
        // Construtor
        // Cria uma nova TbtliquidacaoCollection com os dados de HbtliquidacaoCollection
        public TbtliquidacaoCollection(HbtliquidacaoCollection hbtliquidacaoCollection)
        {
            for (int i = 0; i < hbtliquidacaoCollection.Count; i++) {
                //
                Tbtliquidacao p = new Tbtliquidacao();

                // Para cada Coluna de TmffinpreH copia para Tmffinpre
                foreach (esColumnMetadata colhbtliquidacao in hbtliquidacaoCollection.es.Meta.Columns) 
                {
                    // Copia todas as colunas
                    esColumnMetadata coltbtliquidacao = p.es.Meta.Columns.FindByPropertyName(colhbtliquidacao.PropertyName);
                    if (hbtliquidacaoCollection[i].GetColumn(colhbtliquidacao.Name) != null)
                    {
                        p.SetColumn(coltbtliquidacao.Name, hbtliquidacaoCollection[i].GetColumn(colhbtliquidacao.Name));
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        public void BuscaContratosLiquidados(List<int> listaCodigos, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";

            if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC != "")
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaBTC;
            }
            else
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            }

            this.QueryReset();
            this.Query.Where(this.Query.CodCli.In(listaCodigos),
                             this.Query.DataLiqd.Equal(data));
            this.Query.Load();
        }
	}
}
