using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class HbtcontratoCollection : esHbtcontratoCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        public void BuscaContratos(List<int> listaCodigos, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";

            if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC != "")
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaBTC;
            }
            else
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            }

            this.QueryReset();
            this.Query.Where(this.Query.CodCli.In(listaCodigos),
                             this.Query.DataAber.Equal(data));
            this.Query.Load();
        }
	}
}
