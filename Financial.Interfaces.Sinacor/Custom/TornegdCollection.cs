﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TornegdCollection : esTornegdCollection
	{
        /// <summary>
        /// Busca operações no Sinacor a serem importadas, relaciona com TORCOMI e TORMOVD.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesSINACOR(Int32 codigoCliente, Int32 codigoCliente2, DateTime dataOperacao)
        {
            TornegdQuery tornegdQuery = new TornegdQuery("N");
            TormovdQuery tormovdQuery = new TormovdQuery("M");
            TorcomiQuery torcomiQuery = new TorcomiQuery("C");

            tornegdQuery.es.Connection.Name = "Sinacor";
            tornegdQuery.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            tornegdQuery.Select(tornegdQuery.CdCliente, tornegdQuery.CdNatope, tornegdQuery.CdNegocio, 
                                tornegdQuery.DtPregao, tornegdQuery.NrNegocio, tornegdQuery.QtNegocio, 
                                tornegdQuery.VlNegocio, tornegdQuery.FtValorizacao);
            tornegdQuery.InnerJoin(tormovdQuery).On(tormovdQuery.DtDatord == torcomiQuery.DtDatord &
                                                    tormovdQuery.NrSeqord == torcomiQuery.NrSeqord);
            tornegdQuery.InnerJoin(torcomiQuery).On(torcomiQuery.DtNegocio == tornegdQuery.DtPregao &
                                                    torcomiQuery.NrNegocio == tornegdQuery.NrNegocio &
                                                    torcomiQuery.CdNegocio == tornegdQuery.CdNegocio &
                                                    torcomiQuery.CdNatope == tornegdQuery.CdNatope);
            tornegdQuery.Where(tornegdQuery.CdCliente.In(codigoCliente, codigoCliente2) &
                               tornegdQuery.DtPregao.Equal(dataOperacao))
                      .OrderBy(tornegdQuery.NrNegocio.Ascending);

            this.Load(tornegdQuery);
        }      

        /// <summary>
        /// Busca operações no Sinacor a serem importadas, relaciona com TORCOMI e TORMOVD.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="tipoMercado"></param>
        public void BuscaOperacoesSINACOR(Int32 codigoCliente, Int32 codigoCliente2, DateTime dataOperacao,
                                          string tipoMercado)
        {
            TornegdQuery tornegdQuery = new TornegdQuery("N");
            TormovdQuery tormovdQuery = new TormovdQuery("M");
            TorcomiQuery torcomiQuery = new TorcomiQuery("C");
            TbotitulosQuery tbotitulosQuery = new TbotitulosQuery("T");

            tornegdQuery.es.Connection.Name = "Sinacor";
            tornegdQuery.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            tornegdQuery.Select(tornegdQuery.CdCliente, tornegdQuery.CdNatope, tornegdQuery.CdNegocio, 
                                tornegdQuery.DtPregao, tornegdQuery.NrNegocio, tornegdQuery.QtNegocio, 
                                tornegdQuery.VlNegocio, tornegdQuery.FtValorizacao);
            tornegdQuery.InnerJoin(tormovdQuery).On(tormovdQuery.DtDatord == torcomiQuery.DtDatord &
                                                    tormovdQuery.NrSeqord == torcomiQuery.NrSeqord);
            tornegdQuery.InnerJoin(torcomiQuery).On(torcomiQuery.DtNegocio == tornegdQuery.DtPregao &
                                                    torcomiQuery.NrNegocio == tornegdQuery.NrNegocio &
                                                    torcomiQuery.CdNegocio == tornegdQuery.CdNegocio &
                                                    torcomiQuery.CdNatope == tornegdQuery.CdNatope);
            tornegdQuery.InnerJoin(tbotitulosQuery).On(tbotitulosQuery.CdCodneg == tornegdQuery.CdNegocio);
            tornegdQuery.Where(tornegdQuery.CdCliente.In(codigoCliente, codigoCliente2) &
                               tornegdQuery.DtPregao.Equal(dataOperacao),
                               tbotitulosQuery.CdTpmerc.Equal(tipoMercado))
                      .OrderBy(tornegdQuery.NrNegocio.Ascending);

            this.Load(tornegdQuery);
        }      
        
	}
}
