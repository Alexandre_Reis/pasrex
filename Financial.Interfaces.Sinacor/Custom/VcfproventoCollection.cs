﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class VcfproventoCollection : esVcfproventoCollection
	{
        /// <summary>
        /// Busca proventos lançados no dia, fisicos e financeiros.
        /// </summary>
        /// <param name="data"></param>
        public void BuscaProventosSinacor(DateTime data)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();

            this.Query.Where(this.Query.DataIniPgtoProv.Equal(data),
                             this.Query.TipoProv.In(10, 11, 12, 13, 14, 20, 30, 40, 60, 70, 80, 81, 90, 52));
            this.Query.Load();
        }
	}
}
