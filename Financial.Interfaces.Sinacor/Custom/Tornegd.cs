﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Sinacor.Exceptions;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class Tornegd : esTornegd
	{
        /// <summary>
        /// Busca operações no Sinacor a serem importadas.
        /// Se não encontrar, faz outra tentativa na TORNEGH.
        /// Exception TORNEGD_DataVencimentoTermoNaoEncontrada se não achar data de vencimento
        /// </summary>
        /// <param name="dataPregao"></param>
        /// <param name="numeroNegocio"></param>
        /// <param name="naturezaOperacao"></param>
        /// <param name="codigoNegocio"></param>
        /// <returns>nr de dias até o vencimento do termo (contado a partir da data da operação)</returns>
        public int RetornaDiasVencimentoTermoSINACOR(DateTime dataPregao, int numeroNegocio, string naturezaOperacao,
                                               string codigoNegocio)
        {
            TornegdCollection tornegdCollection = new TornegdCollection();

            tornegdCollection.es.Connection.Name = "Sinacor";
            tornegdCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            
            tornegdCollection.QueryReset();
            tornegdCollection.Query.Select(tornegdCollection.Query.TpVcoter)
                                   .Where(tornegdCollection.Query.DtPregao.Equal(dataPregao),
                                         tornegdCollection.Query.NrNegocio.Equal(numeroNegocio),
                                         tornegdCollection.Query.CdNatope.Equal(naturezaOperacao),
                                         tornegdCollection.Query.CdNegocio.Equal(codigoNegocio));
            tornegdCollection.Query.Load();

            int numeroDiasVencimento;
            if (tornegdCollection.HasData)
            {
                if (!tornegdCollection[0].TpVcoter.HasValue)
                {
                    throw new TORNEGD_DataVencimentoTermoNaoEncontrada("TORNEGD - Sinacor não encontrado para o ativo " + codigoNegocio + ", nr negocio " + numeroNegocio);
                }
                else
                {
                    numeroDiasVencimento = (int)tornegdCollection[0].TpVcoter.Value;
                }
            }
            else
            {
                TorneghCollection torneghCollection = new TorneghCollection();

                torneghCollection.es.Connection.Name = "Sinacor";
                torneghCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

                torneghCollection.QueryReset();
                torneghCollection.Query.Select(torneghCollection.Query.TpVcoter)
                                       .Where(torneghCollection.Query.DtPregao.Equal(dataPregao),
                                             torneghCollection.Query.NrNegocio.Equal(numeroNegocio),
                                             torneghCollection.Query.CdNatope.Equal(naturezaOperacao),
                                             torneghCollection.Query.CdNegocio.Equal(codigoNegocio));
                torneghCollection.Query.Load();

                if (torneghCollection.HasData)
                {
                    if (!torneghCollection[0].TpVcoter.HasValue)
                    {
                        throw new TORNEGD_DataVencimentoTermoNaoEncontrada("TORNEGD - Sinacor não encontrado para o ativo " + codigoNegocio + ", nr negocio " + numeroNegocio);
                    }
                    else
                    {
                        numeroDiasVencimento = (int)torneghCollection[0].TpVcoter.Value;
                    }
                }
                else
                {
                    throw new TORNEGD_DataVencimentoTermoNaoEncontrada("TORNEGD - Sinacor não encontrado para o ativo " + codigoNegocio + ", nr negocio " + numeroNegocio);
                }                
            }

            return numeroDiasVencimento;
        }
	}
}
