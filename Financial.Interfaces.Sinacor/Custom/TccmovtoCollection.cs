﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TccmovtoCollection : esTccmovtoCollection
	{
        /// <summary>
        /// Busca os lançamentos do cliente liquidando na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>
        /// <returns>bool indicando se achou registro</returns>
        public bool BuscaLiquidacaoData(int idCliente, DateTime dataLiquidacao)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            //
            this.Query.Where(this.Query.CdCliente.Equal(idCliente),
                             this.Query.DtLiquidacao.Equal(dataLiquidacao));

            return this.Query.Load();            
        }

	}
}
