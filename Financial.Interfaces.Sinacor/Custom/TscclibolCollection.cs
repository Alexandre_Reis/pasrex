﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : Oracle
Date Generated       : 05/11/2009 12:23:49
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor {
	public partial class TscclibolCollection : esTscclibolCollection {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigoSinacor"></param>
        /// <returns>Retorna infos de Tscclibol na posicao 0</returns>
        /// <exception>se Codigo não foi achado, se CPF não está presente</exception>
        public void GetTscclibol(int codigoSinacor) {
            // Tentar encontrar codigo no Sinacor
            this.QueryReset();
            //
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            this.Query.Where(this.Query.CdCliente.Equal(codigoSinacor));

            try {
                this.Query.Load();
            }
            catch (Exception e1) {
                string s = this.Query.es.LastQuery;
            }
            
            if (this.Count == 0) {
                //Código não encontrado no Sinacor
                throw new Exception("Código de cliente não encontrado no Sinacor: " + codigoSinacor.ToString());
            }

            if (!this[0].CdCpfcgc.HasValue) {
                //CPF não encontrado no Sinacor
                throw new Exception("CPF de cliente não encontrado no Sinacor: " + codigoSinacor.ToString());
            }            
        }              
	}
}