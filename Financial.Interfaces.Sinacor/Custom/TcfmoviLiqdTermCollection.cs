using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TcfmoviLiqdTermCollection : esTcfmoviLiqdTermCollection
	{
        // Construtor
        // Cria uma nova TcfmoviLiqdTermCollection com os dados de HcfmoviLiqdTermCollection
        public TcfmoviLiqdTermCollection(HcfmoviLiqdTermCollection hcfmoviLiqdTermCollection)
        {
            for (int i = 0; i < hcfmoviLiqdTermCollection.Count; i++)
            {
                //
                TcfmoviLiqdTerm t = new TcfmoviLiqdTerm();

                foreach (esColumnMetadata colhcfmoviLiqdTerm in hcfmoviLiqdTermCollection.es.Meta.Columns) 
                {
                    // Copia todas as colunas
                    esColumnMetadata coltcfmoviLiqdTerm = t.es.Meta.Columns.FindByPropertyName(colhcfmoviLiqdTerm.PropertyName);
                    if (hcfmoviLiqdTermCollection[i].GetColumn(colhcfmoviLiqdTerm.Name) != null)
                    {
                        t.SetColumn(coltcfmoviLiqdTerm.Name, hcfmoviLiqdTermCollection[i].GetColumn(colhcfmoviLiqdTerm.Name));
                    }
                }
                this.AttachEntity(t);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        public void BuscaContratosLiquidados(List<int> listaCodigos, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CodCli.In(listaCodigos),
                             this.Query.DataMvto.Equal(data));
            this.Query.Load();
        }
	}
}
