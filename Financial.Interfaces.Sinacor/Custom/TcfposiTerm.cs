﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TcfposiTerm : esTcfposiTerm
	{
        /// <summary>
        /// Retorna 0 e não achar nr do contrato.
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="numeroNegocio"></param>
        /// <returns></returns>
        public int RetornaNumeroContrato(List<int> listaCodigos, string cdAtivoBolsa, DateTime dataVencimento, int numeroNegocio)
        {
            TcfposiTermCollection tcfposiTermCollection = new TcfposiTermCollection();
            tcfposiTermCollection.es.Connection.Name = "Sinacor";
            tcfposiTermCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            tcfposiTermCollection.Query.Select(tcfposiTermCollection.Query.NumCotr);
            tcfposiTermCollection.Query.Where(tcfposiTermCollection.Query.CodCli.In(listaCodigos),
                                              tcfposiTermCollection.Query.CodNeg.Equal(cdAtivoBolsa),
                                              tcfposiTermCollection.Query.DataVenc.Equal(dataVencimento),
                                              tcfposiTermCollection.Query.NumNego.Equal(numeroNegocio));
            tcfposiTermCollection.Query.Load();

            int numeroContrato = 0;
            if (tcfposiTermCollection.Count > 0)
            {
                numeroContrato = Convert.ToInt32(tcfposiTermCollection[0].NumCotr.Value);
            }

            return numeroContrato;
        }

        public int RetornaNumeroContrato(List<int> listaCodigos, string cdAtivoBolsa, DateTime dataOperacao, DateTime dataVencimento, decimal quantidade)
        {
            TcfposiTermCollection tcfposiTermCollection = new TcfposiTermCollection();
            tcfposiTermCollection.es.Connection.Name = "Sinacor";
            tcfposiTermCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            tcfposiTermCollection.Query.Select(tcfposiTermCollection.Query.NumCotr);
            tcfposiTermCollection.Query.Where(tcfposiTermCollection.Query.CodCli.In(listaCodigos),
                                              tcfposiTermCollection.Query.CodNeg.Equal(cdAtivoBolsa),
                                              tcfposiTermCollection.Query.DataPreg.Equal(dataOperacao),
                                              tcfposiTermCollection.Query.DataVenc.Equal(dataVencimento),
                                              tcfposiTermCollection.Query.QtdeDisp.Equal(quantidade));
            tcfposiTermCollection.Query.Load();

            int numeroContrato = 0;
            if (tcfposiTermCollection.Count > 0)
            {
                numeroContrato = Convert.ToInt32(tcfposiTermCollection[0].NumCotr.Value);
            }

            return numeroContrato;
        }
        
	}
}
