﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class VcfmoviTediCollection : esVcfmoviTediCollection
	{
        /// <summary>
        /// Busca operações no Sinacor a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesSINACOR(List<Int32> listaCodigos, DateTime dataOperacao)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();

            this.Query.Where(this.Query.CodCli.In(listaCodigos),
                             this.Query.DataNego.Equal(dataOperacao),
                             this.Query.DescHistMvto.In("COMPRA", "VENDA"));                      
            this.Query.Load();
        }
	}
}
