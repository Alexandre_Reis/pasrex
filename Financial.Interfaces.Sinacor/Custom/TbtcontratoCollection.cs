using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TbtcontratoCollection : esTbtcontratoCollection
	{
        // Construtor
        // Cria uma nova TbtliquidacaoCollection com os dados de HbtliquidacaoCollection
        public TbtcontratoCollection(HbtcontratoCollection hbtcontratoCollection)
        {
            for (int i = 0; i < hbtcontratoCollection.Count; i++) {
                //
                Tbtcontrato p = new Tbtcontrato();

                // Para cada Coluna de TmffinpreH copia para Tmffinpre
                foreach (esColumnMetadata colhbtcontrato in hbtcontratoCollection.es.Meta.Columns) 
                {
                    // Copia todas as colunas
                    esColumnMetadata coltbtcontrato = p.es.Meta.Columns.FindByPropertyName(colhbtcontrato.PropertyName);
                    if (hbtcontratoCollection[i].GetColumn(colhbtcontrato.Name) != null)
                    {
                        p.SetColumn(coltbtcontrato.Name, hbtcontratoCollection[i].GetColumn(colhbtcontrato.Name));
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        public void BuscaContratos(List<int> listaCodigos, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";

            if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC != "")
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaBTC;
            }
            else
            {
                this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            }

            this.QueryReset();
            this.Query.Where(this.Query.CodCli.In(listaCodigos),
                             this.Query.DataAber.Equal(data));
            this.Query.Load();
        }
	}
}
