using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class VcfpapelCollection : esVcfpapelCollection
	{
        public void BuscaPapel(string cdAtivoBolsa)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CodNeg.Equal(cdAtivoBolsa));
            this.Query.Load();
        }
	}
}
