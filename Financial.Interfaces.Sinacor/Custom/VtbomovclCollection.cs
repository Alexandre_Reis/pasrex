﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor {
    public partial class VTbomovclCollection : esVTbomovclCollection {
        /// <summary>
        /// Busca operações no Sinacor a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesSINACOR(List<Int32> listaCodigos, DateTime dataOperacao) 
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();

            this.Query.Select(this.Query.CdCliente, this.Query.CdNatope, this.Query.CdNegocio, this.Query.DtNegocio,
                              this.Query.InNegocio, this.Query.NrNegocio, this.Query.QtQtdesp, this.Query.VlCortot,
                              this.Query.VlEmolumBv, this.Query.VlEmolumCb, this.Query.VlNegocio, this.Query.VlTaxreg, 
                              this.Query.VlTotneg, this.Query.PzProjCc, this.Query.VlIss);
            this.Query.Where(this.Query.CdCliente.In(listaCodigos),
                             this.Query.DtNegocio.Equal(dataOperacao))
                      .OrderBy(this.Query.CdNegocio.Ascending, this.Query.NrNegocio.Ascending);
            this.Query.Load();
        }

        /// <summary>
        /// Busca operações no Sinacor a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="tpMerc">VIS, OPC, OPV, TER</param>
        public void BuscaOperacoesSINACOR(Int32 codigoCliente, Int32 codigoCliente2, DateTime dataOperacao, string tpMerc) 
        {
            VTbomovclQuery vtbomovclQuery = new VTbomovclQuery("V");
            TbotitulosQuery tbotitulosQuery = new TbotitulosQuery("T");

            vtbomovclQuery.es.Connection.Name = "Sinacor";
            tbotitulosQuery.es.Connection.Name = "Sinacor";
            vtbomovclQuery.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            tbotitulosQuery.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            vtbomovclQuery.Select(vtbomovclQuery.CdCliente, vtbomovclQuery.CdNatope, vtbomovclQuery.CdNegocio,
                              vtbomovclQuery.DtNegocio, vtbomovclQuery.InNegocio, vtbomovclQuery.NrNegocio,
                              vtbomovclQuery.QtQtdesp, vtbomovclQuery.VlCortot,
                              vtbomovclQuery.VlEmolumBv, vtbomovclQuery.VlEmolumCb, vtbomovclQuery.VlNegocio, 
                              vtbomovclQuery.VlTaxreg, vtbomovclQuery.VlTotneg, vtbomovclQuery.PzProjCc, vtbomovclQuery.VlIss);
            vtbomovclQuery.InnerJoin(tbotitulosQuery).On(vtbomovclQuery.CdNegocio == tbotitulosQuery.CdCodneg);
            vtbomovclQuery.Where(tbotitulosQuery.CdTpmerc.Equal(tpMerc));
            vtbomovclQuery.Where(vtbomovclQuery.CdCliente.In(codigoCliente, codigoCliente2),
                                 vtbomovclQuery.DtNegocio.Equal(dataOperacao))
                      .OrderBy(this.Query.CdNegocio.Ascending, vtbomovclQuery.NrNegocio.Ascending);

            VTbomovclCollection vtbomovclCollection = new VTbomovclCollection();
            this.Load(vtbomovclQuery);
        }

        /// <summary>
        /// Busca valor de IR DayTrade e a base de rendimento DT.
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        public void BuscaIRDayTradeRetido(List<Int32> listaCodigos, DateTime data)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.Query.Select(this.Query.VlIrretido.Sum(),
                              this.Query.VlBaseirdt.Sum(),
                              this.Query.PzProjCc);
            this.Query.Where(this.Query.CdCliente.In(listaCodigos),
                             this.Query.DtNegocio.Equal(data));
            this.Query.GroupBy(this.Query.PzProjCc);
            this.Query.Load();
        }
    }
}
