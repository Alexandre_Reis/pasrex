using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class VTccmovtoCollection : esVTccmovtoCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public void BuscaLiquidacoes(List<int> listaCodigos, DateTime? dataInicio, DateTime? dataFim)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            this.Query.Select(this.Query.CdCliente.As("IdCliente"),
                             this.Query.DtLiquidacao.As("DtLiquidacao"),
                             this.Query.DsLancamento.As("Descricao"),
                             this.Query.VlLancamento.As("ValorLancamento"));

            this.Query.Where(this.Query.CdCliente.In(listaCodigos));

            if (dataInicio.HasValue) 
            {
                this.Query.Where(this.Query.DtLiquidacao >= dataInicio.Value);
            }

            if (dataFim.HasValue) 
            {
                this.Query.Where(this.Query.DtLiquidacao <= dataFim.Value);
            }

            //
            this.Query.OrderBy(this.Query.DtLiquidacao.Ascending,
                               this.Query.DsLancamento.Ascending);
            //
            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>        
        public void BuscaLancamentoDataFull(List<int> listaCodigos,
                                            DateTime data, 
                                            List<int> listaEventosNaoImportar)
        {
            this.Query.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CdCliente.In(listaCodigos),
                             this.Query.DtLancamento.Equal(data),
                              this.Query.CdHistorico.NotIn(listaEventosNaoImportar));
            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void BuscaLiquidacaoData(List<int> listaCodigos,
                                        DateTime data,
                                        List<int> listaEventos)
        {
            this.BuscaLiquidacaoData(listaCodigos, data, listaEventos, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaCodigos"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void BuscaLiquidacaoData(List<int> listaCodigos,
                                        DateTime data,
                                        List<int> listaEventos,
                                        bool ordenaValor)
        {
            this.Query.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            this.Query.Where(this.Query.CdCliente.In(listaCodigos),
                                           this.Query.DtLiquidacao.Equal(data),
                                           this.Query.CdHistorico.In(listaEventos));
            if (ordenaValor)
            {
                this.Query.OrderBy(this.Query.VlLancamento.Ascending);
            }

            this.Query.Load();
        }


	}
}
