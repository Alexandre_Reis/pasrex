﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
	public partial class TmffinpreCollection : esTmffinpreCollection
	{
        // Construtor
        // Cria uma nova TmffinpreCollection com os dados de TmffinpreHCollection
        public TmffinpreCollection(TmffinpreHCollection tmffinpreHCollection)
        {
            for (int i = 0; i < tmffinpreHCollection.Count; i++) {
                //
                Tmffinpre p = new Tmffinpre();

                // Para cada Coluna de TmffinpreH copia para Tmffinpre
                foreach (esColumnMetadata coltmffinpreH in tmffinpreHCollection.es.Meta.Columns) 
                {
                    // Copia todas as colunas
                    esColumnMetadata coltmffinpre = p.es.Meta.Columns.FindByPropertyName(coltmffinpreH.PropertyName);
                    if (tmffinpreHCollection[i].GetColumn(coltmffinpreH.Name) != null)
                    {
                        p.SetColumn(coltmffinpre.Name, tmffinpreHCollection[i].GetColumn(coltmffinpreH.Name));
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Busca operações no Sinacor a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesSINACOR(Int32 codigoCliente, DateTime dataOperacao)
        {
            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();

            this.Query.Where(this.Query.CdCliente.Equal(codigoCliente),
                             this.Query.DtNegocio.Equal(dataOperacao),
                             this.Query.TpNegocio.NotIn("AJ", "LQ"),
                             this.Query.CdCommod.NotLike("VOI%"),
                             this.Query.CdCommod.NotLike("VTC%"),
                             this.Query.CdCommod.NotLike("VID%"),
                             this.Query.CdCommod.NotLike("SDC%"),
                             this.Query.CdCommod.NotLike("IR1%"),
                             this.Query.CdCommod.NotLike("DR1%"),
                             this.Query.CdCommod.NotLike("CR1%"),
                             this.Query.CdCommod.NotLike("BR1%"),
                             this.Query.CdCommod.NotLike("SR1%"),
                             this.Query.CdCommod.NotLike("FRC%"),
                             this.Query.CdCommod.NotLike("FRP%"),
                             this.Query.CdMercad.NotIn("SWP", "FLX"))
                      .OrderBy(this.Query.CdCommod.Ascending);
            this.Query.Load();
        }

	}
}
