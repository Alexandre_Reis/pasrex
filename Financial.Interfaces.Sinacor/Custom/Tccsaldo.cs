using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.Interfaces.Sinacor
{
    public partial class Tccsaldo : esTccsaldo
    {
        /// <summary>
        /// Retorna o saldo disponivel do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaSaldo(int idCliente)
        {
            decimal saldo = 0;

            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            //
            this.Query.Select(this.Query.VlDisponivel.Sum());
            this.Query.Where(this.Query.CdCliente == idCliente);

            if (this.Query.Load())
	        {
                if (this.VlDisponivel.HasValue)
                {
                    saldo = this.VlDisponivel.Value;
                }
            }

            return saldo;
        }

        /// <summary>
        /// Retorna o saldo total do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaSaldoTotal(int idCliente)
        {
            decimal saldo = 0;

            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            //
            this.Query.Select(this.Query.VlTotal.Sum());
            this.Query.Where(this.Query.CdCliente == idCliente);

            if (this.Query.Load())
            {
                if (this.VlTotal.HasValue)
                {
                    saldo = this.VlTotal.Value;
                }
            }

            return saldo;
        }

        /// <summary>
        /// Retorna o saldo projetado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaSaldoProjetado(int idCliente)
        {
            decimal saldo = 0;

            this.es.Connection.Name = "Sinacor";
            this.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            this.QueryReset();
            //
            this.Query.Select(this.Query.VlProjetado.Sum());
            this.Query.Where(this.Query.CdCliente == idCliente);

            if (this.Query.Load())
            {
                if (this.VlProjetado.HasValue)
                {
                    saldo = this.VlProjetado.Value;
                }
            }

            return saldo;
        }
    }
}
