﻿using System;

namespace Financial.Interfaces.Sinacor.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente
    /// </summary>
    public class InterfacesSinacorException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesSinacorException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesSinacorException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesSinacorException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de TORNEGD_DataVencimentoTermoNaoEncontrada
    /// </summary>
    public class TORNEGD_DataVencimentoTermoNaoEncontrada : InterfacesSinacorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TORNEGD_DataVencimentoTermoNaoEncontrada() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TORNEGD_DataVencimentoTermoNaoEncontrada(string mensagem) : base(mensagem) { }
    }

}


