﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.ExtratorRiskGrid.ADO;
using System.Configuration;
using Financial.ExtratorRiskGrid.SQL;
using System.IO;

namespace Financial.RiskGridFileHandler
{
    public class ArquivoRiskGrid
    {
        string ArquivoZip = string.Empty;

        private string _linha;
        [DbAttribute(Campo = "LINHA")]
        public String Linha
        {
            get
            {
                return _linha;
            }
            set
            {
                _linha = value;
            }
        }


        MemoryStream readStream()
        {
            MemoryStream memStream = new MemoryStream();

            using (FileStream fileStream = System.IO.File.OpenRead(this.ArquivoZip))
            {

                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }

            return memStream;
        }

        private List<ArquivoRiskGrid> ObterArquivo(string NomeQuery, DateTime dtPosicao, string carteiras)
        {
            ArquivoRiskGrid script = null;
            List<ArquivoRiskGrid> lstScript = null;
            ADOBase ado = new ADOBase();

            string sdt = dtPosicao.ToString("yyyy-MM-dd");

            string query = string.Format(Query.recuperarQuery(NomeQuery), sdt, carteiras);

            //if (ExtratorRiskGrid.Negocio.Classes.ContextParam.SistemaOrigem == ContextParam.SistemaOrigemEnum.SAC)
            //    lstScript = ado.ExecutarQueryPesquisa<ArquivoRiskGrid>(script, query, ADOBase.Projeto.ExtratorRiskGridSAC);
            //else
            //{
            lstScript = ado.ExecutarQueryPesquisa<ArquivoRiskGrid>(script, query, ADOBase.Projeto.FOL);
            //}

            return lstScript;
        }

        public MemoryStream GeraArquivos(string Instituicao, string diretorioGravacao, DateTime DataPosicao, string Clientes)
        {
            string[,] arquivos;

            arquivos = new string[,]
            {
                {"CF", "FOL_SAC_CL_PATRFI"},
                {"RF", "FOL_SAC_RF_POS"},
                {"EV", "FOL_SAC_RF_EVENTO"},
                {"RV", "FOL_SAC_CL_PATRRV"},
                {"SW", "FOL_SAC_SW_POS"},
                {"FU", "FOL_SAC_CL_PATRFUT"},
                {"FX", "FOL_FLUXOCAIXA"},
                {"EA", "FOL_EMPRESTIMOACOES"},
                {"PO", "FOL_PORTFOLIOS"},
            };

            int progresso = 0;
            string[] arquivosRiskGrid = new string[arquivos.GetLength(0)];

            string diretorio = Path.GetDirectoryName(diretorioGravacao);
            for (int i = 0; i < arquivos.GetLength(0); i++)
            {
                try
                {
                    progresso = i;
                    List<ArquivoRiskGrid> lstArq = this.ObterArquivo(arquivos[i, 1], DataPosicao, Clientes);


                    //string arquivo = string.Format("{0}\\{1}_{2}_{3}{4}", diretorio, Instituicao, arquivos[i, 0], DateTime.Now.ToString("yyyy-MM-dd_hhmm"), ".TXT", false);
                    string arquivo = string.Format("{0}_{1}_{2}{3}", Instituicao, arquivos[i, 0], DateTime.Now.ToString("yyyy-MM-dd_hhmm"), ".TXT", false);
                    arquivosRiskGrid[i] = arquivo;

                    System.IO.StreamWriter Writer = new System.IO.StreamWriter(arquivo);
                    StringBuilder strBuild = new StringBuilder();
                    Writer.WriteLine(string.Format("{0};{1};{2}", Instituicao, arquivos[i, 0], DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")));

                    if (lstArq != null)
                    {
                        for (int j = 0; j < lstArq.Count; j++)
                        {
                            strBuild.Append(lstArq[j].Linha);
                            strBuild.AppendLine();
                        }
                    }


                    Writer.Write(strBuild);
                    Writer.Flush();
                    Writer.Close();

                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }
            }

            CompactaArquivos zip = new CompactaArquivos();
            ArquivoZip = string.Format("{0}\\{1}_{2}_{3}{4}", diretorio, "RiskGrid", Instituicao, DateTime.Now.ToString("yyyy-MM-dd_hhmm"), ".ZIP", false);

            try
            {
                zip.compactar(ArquivoZip, arquivosRiskGrid);

                foreach (String arquivoDel in arquivosRiskGrid)
                {
                    File.Delete(arquivoDel);
                }

                return readStream();

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

    }

}
