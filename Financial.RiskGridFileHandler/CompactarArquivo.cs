﻿using System;
using System.Collections.Generic;
using System.Text;
using Ionic.Zip;

namespace Financial.RiskGridFileHandler
{
    public class CompactaArquivos
    {
        public void compactar(String ArquivoZip, string[] Arquivos)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {

                    foreach (String arquivo in Arquivos)
                    {
                        
                        ZipEntry e = zip.AddFile(arquivo);
                        //e.Comment = "Added by Cheeso's CreateZip utility.";
                    }

                    //zip.Comment = "Arquivos de interface RiskGrid";

                    zip.Save(ArquivoZip);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Compactar arquivos " + ex.Message);
            }


        }

    }

}
