﻿namespace ImportaCVM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImportaCotasButton = new System.Windows.Forms.Button();
            this.ImportaFundosButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ImportaCotasButton
            // 
            this.ImportaCotasButton.Location = new System.Drawing.Point(33, 54);
            this.ImportaCotasButton.Name = "ImportaCotasButton";
            this.ImportaCotasButton.Size = new System.Drawing.Size(161, 23);
            this.ImportaCotasButton.TabIndex = 0;
            this.ImportaCotasButton.Text = "Importa Cotas";
            this.ImportaCotasButton.UseVisualStyleBackColor = true;
            this.ImportaCotasButton.Click += new System.EventHandler(this.ImportaCotasButton_Click);
            // 
            // ImportaFundosButton
            // 
            this.ImportaFundosButton.Location = new System.Drawing.Point(33, 107);
            this.ImportaFundosButton.Name = "ImportaFundosButton";
            this.ImportaFundosButton.Size = new System.Drawing.Size(161, 23);
            this.ImportaFundosButton.TabIndex = 1;
            this.ImportaFundosButton.Text = "Importa Fundos";
            this.ImportaFundosButton.UseVisualStyleBackColor = true;
            this.ImportaFundosButton.Click += new System.EventHandler(this.ImportaFundosButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 252);
            this.Controls.Add(this.ImportaFundosButton);
            this.Controls.Add(this.ImportaCotasButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ImportaCotasButton;
        private System.Windows.Forms.Button ImportaFundosButton;
    }
}

