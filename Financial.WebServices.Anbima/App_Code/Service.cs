using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : System.Web.Services.WebService
{
    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public DataSet ExecuteSQLCommand(string connectionString, string commandText, int commandType)
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataSet dsInfo = new DataSet();

        try
        {
            cmd.CommandText = commandText;
            cmd.CommandType = (System.Data.CommandType)commandType;
            cmd.Connection = conn;

            adapter.SelectCommand = cmd;
            adapter.Fill(dsInfo);

            return dsInfo;
        }
        catch (Exception ex)
        {
            System.Web.HttpContext.Current.Response.Write(ex.Message);
            return null;
        }
        finally
        {
            conn.Close();
        }
    }
    
}
