﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;

namespace Financial.BsMappingService
{
    public partial class MainService : ServiceBase
    {
        private static Timer aTimer;

        public MainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("Serviço BsMapping Iniciado");
            IniciaTimer();
        }

        protected override void OnStop()
        {
            FinalizaTimer();
        }

        public void IniciaTimer()
        {
            TimerCallback tcb = new TimerCallback(this.ExecutaProcedure);
            AutoResetEvent autoEvent = new AutoResetEvent(true);
            
            // Create a timer with on hour interval.            
            aTimer = new Timer(tcb,autoEvent,5000,3600000);    
        }      

        public void FinalizaTimer()
        {   
            aTimer.Dispose();
        }

        public void ExecutaProcedure(Object stateInfo)
        {           
            
            if (DateTime.Now.Hour == int.Parse(ConfigurationManager.AppSettings["HorarioDeExecucao"]))
            {

                EventLog.WriteEntry("Inicio da execução da procedure de carregamento do BsMapping");
                //Read the connection string from Web.Config file
                string connectionString = ConfigurationManager.ConnectionStrings["BsMapping"].ConnectionString;
                string nomeDaProcedure = ConfigurationManager.AppSettings["NomeDaProcedure"];


                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    //Create the SqlCommand object
                    SqlCommand cmd = new SqlCommand(nomeDaProcedure, con);

                    //Specify that the SqlCommand is a stored procedure
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@DataRef", DateTime.Now);
                    cmd.CommandTimeout = 60 * 60;

                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();

                    EventLog.WriteEntry("Fim da execução da procedure de carregamento");

                }                
            }
            
        }
    }
}
