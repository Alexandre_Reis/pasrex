using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace Financial.BsMappingService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            
#if DEBUG       
            MainService s = new MainService();
            s.IniciaTimer();
#else

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] { new MainService() };
            ServiceBase.Run(ServicesToRun);     
#endif

                        
        }
    }
}