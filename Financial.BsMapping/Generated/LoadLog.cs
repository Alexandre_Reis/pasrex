/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/06/2015 18:44:38
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.BsMapping
{

    [Serializable]
    abstract public class esLoadLogCollection : esEntityCollection
    {
        public esLoadLogCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "LoadLogCollection";
        }

        #region Query Logic
        protected void InitQuery(esLoadLogQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esLoadLogQuery);
        }
        #endregion

        virtual public LoadLog DetachEntity(LoadLog entity)
        {
            return base.DetachEntity(entity) as LoadLog;
        }

        virtual public LoadLog AttachEntity(LoadLog entity)
        {
            return base.AttachEntity(entity) as LoadLog;
        }

        virtual public void Combine(LoadLogCollection collection)
        {
            base.Combine(collection);
        }

        new public LoadLog this[int index]
        {
            get
            {
                return base[index] as LoadLog;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(LoadLog);
        }
    }



    [Serializable]
    abstract public class esLoadLog : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esLoadLogQuery GetDynamicQuery()
        {
            return null;
        }

        public esLoadLog()
        {

        }

        public esLoadLog(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idLog)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idLog);
            else
                return LoadByPrimaryKeyStoredProcedure(idLog);
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLog)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idLog);
            else
                return LoadByPrimaryKeyStoredProcedure(idLog);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idLog)
        {
            esLoadLogQuery query = this.GetDynamicQuery();
            query.Where(query.IdLog == idLog);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLog)
        {
            esParameters parms = new esParameters();
            parms.Add("IdLog", idLog);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdLog": this.str.IdLog = (string)value; break;
                        case "IdExec": this.str.IdExec = (string)value; break;
                        case "DbUserId": this.str.DbUserId = (string)value; break;
                        case "ProcedureName": this.str.ProcedureName = (string)value; break;
                        case "DataInicioProcesso": this.str.DataInicioProcesso = (string)value; break;
                        case "DataFimProcesso": this.str.DataFimProcesso = (string)value; break;
                        case "Sucesso": this.str.Sucesso = (string)value; break;
                        case "DataRef": this.str.DataRef = (string)value; break;
                        case "Message": this.str.Message = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdLog":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdLog = (System.Int32?)value;
                            break;

                        case "IdExec":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdExec = (System.Int32?)value;
                            break;

                        case "DataInicioProcesso":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataInicioProcesso = (System.DateTime?)value;
                            break;

                        case "DataFimProcesso":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataFimProcesso = (System.DateTime?)value;
                            break;

                        case "Sucesso":

                            if (value == null || value.GetType().ToString() == "System.Boolean")
                                this.Sucesso = (System.Boolean?)value;
                            break;

                        case "DataRef":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataRef = (System.DateTime?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to LOAD_LOG.IdLog
        /// </summary>
        virtual public System.Int32? IdLog
        {
            get
            {
                return base.GetSystemInt32(LoadLogMetadata.ColumnNames.IdLog);
            }

            set
            {
                base.SetSystemInt32(LoadLogMetadata.ColumnNames.IdLog, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.IdExec
        /// </summary>
        virtual public System.Int32? IdExec
        {
            get
            {
                return base.GetSystemInt32(LoadLogMetadata.ColumnNames.IdExec);
            }

            set
            {
                base.SetSystemInt32(LoadLogMetadata.ColumnNames.IdExec, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.DbUserId
        /// </summary>
        virtual public System.String DbUserId
        {
            get
            {
                return base.GetSystemString(LoadLogMetadata.ColumnNames.DbUserId);
            }

            set
            {
                base.SetSystemString(LoadLogMetadata.ColumnNames.DbUserId, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.ProcedureName
        /// </summary>
        virtual public System.String ProcedureName
        {
            get
            {
                return base.GetSystemString(LoadLogMetadata.ColumnNames.ProcedureName);
            }

            set
            {
                base.SetSystemString(LoadLogMetadata.ColumnNames.ProcedureName, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.DataInicioProcesso
        /// </summary>
        virtual public System.DateTime? DataInicioProcesso
        {
            get
            {
                return base.GetSystemDateTime(LoadLogMetadata.ColumnNames.DataInicioProcesso);
            }

            set
            {
                base.SetSystemDateTime(LoadLogMetadata.ColumnNames.DataInicioProcesso, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.DataFimProcesso
        /// </summary>
        virtual public System.DateTime? DataFimProcesso
        {
            get
            {
                return base.GetSystemDateTime(LoadLogMetadata.ColumnNames.DataFimProcesso);
            }

            set
            {
                base.SetSystemDateTime(LoadLogMetadata.ColumnNames.DataFimProcesso, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.Sucesso
        /// </summary>
        virtual public System.Boolean? Sucesso
        {
            get
            {
                return base.GetSystemBoolean(LoadLogMetadata.ColumnNames.Sucesso);
            }

            set
            {
                base.SetSystemBoolean(LoadLogMetadata.ColumnNames.Sucesso, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.DataRef
        /// </summary>
        virtual public System.DateTime? DataRef
        {
            get
            {
                return base.GetSystemDateTime(LoadLogMetadata.ColumnNames.DataRef);
            }

            set
            {
                base.SetSystemDateTime(LoadLogMetadata.ColumnNames.DataRef, value);
            }
        }

        /// <summary>
        /// Maps to LOAD_LOG.Message
        /// </summary>
        virtual public System.String Message
        {
            get
            {
                return base.GetSystemString(LoadLogMetadata.ColumnNames.Message);
            }

            set
            {
                base.SetSystemString(LoadLogMetadata.ColumnNames.Message, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esLoadLog entity)
            {
                this.entity = entity;
            }


            public System.String IdLog
            {
                get
                {
                    System.Int32? data = entity.IdLog;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdLog = null;
                    else entity.IdLog = Convert.ToInt32(value);
                }
            }

            public System.String IdExec
            {
                get
                {
                    System.Int32? data = entity.IdExec;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdExec = null;
                    else entity.IdExec = Convert.ToInt32(value);
                }
            }

            public System.String DbUserId
            {
                get
                {
                    System.String data = entity.DbUserId;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DbUserId = null;
                    else entity.DbUserId = Convert.ToString(value);
                }
            }

            public System.String ProcedureName
            {
                get
                {
                    System.String data = entity.ProcedureName;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ProcedureName = null;
                    else entity.ProcedureName = Convert.ToString(value);
                }
            }

            public System.String DataInicioProcesso
            {
                get
                {
                    System.DateTime? data = entity.DataInicioProcesso;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataInicioProcesso = null;
                    else entity.DataInicioProcesso = Convert.ToDateTime(value);
                }
            }

            public System.String DataFimProcesso
            {
                get
                {
                    System.DateTime? data = entity.DataFimProcesso;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataFimProcesso = null;
                    else entity.DataFimProcesso = Convert.ToDateTime(value);
                }
            }

            public System.String Sucesso
            {
                get
                {
                    System.Boolean? data = entity.Sucesso;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Sucesso = null;
                    else entity.Sucesso = Convert.ToBoolean(value);
                }
            }

            public System.String DataRef
            {
                get
                {
                    System.DateTime? data = entity.DataRef;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataRef = null;
                    else entity.DataRef = Convert.ToDateTime(value);
                }
            }

            public System.String Message
            {
                get
                {
                    System.String data = entity.Message;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Message = null;
                    else entity.Message = Convert.ToString(value);
                }
            }


            private esLoadLog entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esLoadLogQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esLoadLog can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class LoadLog : esLoadLog
    {


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esLoadLogQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return LoadLogMetadata.Meta();
            }
        }


        public esQueryItem IdLog
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.IdLog, esSystemType.Int32);
            }
        }

        public esQueryItem IdExec
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.IdExec, esSystemType.Int32);
            }
        }

        public esQueryItem DbUserId
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.DbUserId, esSystemType.String);
            }
        }

        public esQueryItem ProcedureName
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.ProcedureName, esSystemType.String);
            }
        }

        public esQueryItem DataInicioProcesso
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.DataInicioProcesso, esSystemType.DateTime);
            }
        }

        public esQueryItem DataFimProcesso
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.DataFimProcesso, esSystemType.DateTime);
            }
        }

        public esQueryItem Sucesso
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.Sucesso, esSystemType.Boolean);
            }
        }

        public esQueryItem DataRef
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.DataRef, esSystemType.DateTime);
            }
        }

        public esQueryItem Message
        {
            get
            {
                return new esQueryItem(this, LoadLogMetadata.ColumnNames.Message, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("LoadLogCollection")]
    public partial class LoadLogCollection : esLoadLogCollection, IEnumerable<LoadLog>
    {
        public LoadLogCollection()
        {

        }

        public static implicit operator List<LoadLog>(LoadLogCollection coll)
        {
            List<LoadLog> list = new List<LoadLog>();

            foreach (LoadLog emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return LoadLogMetadata.Meta();
            }
        }


        override protected string GetConnectionName()
        {
            return "Financial";
        }

        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new LoadLogQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new LoadLog(row);
        }

        override protected esEntity CreateEntity()
        {
            return new LoadLog();
        }


        #endregion


        [BrowsableAttribute(false)]
        public LoadLogQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new LoadLogQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(LoadLogQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public LoadLog AddNew()
        {
            LoadLog entity = base.AddNewEntity() as LoadLog;

            return entity;
        }

        public LoadLog FindByPrimaryKey(System.Int32 idLog)
        {
            return base.FindByPrimaryKey(idLog) as LoadLog;
        }


        #region IEnumerable<LoadLog> Members

        IEnumerator<LoadLog> IEnumerable<LoadLog>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as LoadLog;
            }
        }

        #endregion

        private LoadLogQuery query;
    }


    /// <summary>
    /// Encapsulates the 'LOAD_LOG' table
    /// </summary>

    [Serializable]
    public partial class LoadLog : esLoadLog
    {
        public LoadLog()
        {

        }

        public LoadLog(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return LoadLogMetadata.Meta();
            }
        }


        override protected string GetConnectionName()
        {
            return "Financial";
        }

        override protected esLoadLogQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new LoadLogQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public LoadLogQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new LoadLogQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(LoadLogQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private LoadLogQuery query;
    }



    [Serializable]
    public partial class LoadLogQuery : esLoadLogQuery
    {
        public LoadLogQuery()
        {

        }

        public LoadLogQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


        override protected string GetConnectionName()
        {
            return "Financial";
        }
    }



    [Serializable]
    public partial class LoadLogMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected LoadLogMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.IdLog, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = LoadLogMetadata.PropertyNames.IdLog;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.IdExec, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = LoadLogMetadata.PropertyNames.IdExec;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.DbUserId, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = LoadLogMetadata.PropertyNames.DbUserId;
            c.CharacterMaxLength = 2147483647;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.ProcedureName, 3, typeof(System.String), esSystemType.String);
            c.PropertyName = LoadLogMetadata.PropertyNames.ProcedureName;
            c.CharacterMaxLength = 2147483647;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.DataInicioProcesso, 4, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = LoadLogMetadata.PropertyNames.DataInicioProcesso;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.DataFimProcesso, 5, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = LoadLogMetadata.PropertyNames.DataFimProcesso;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.Sucesso, 6, typeof(System.Boolean), esSystemType.Boolean);
            c.PropertyName = LoadLogMetadata.PropertyNames.Sucesso;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.DataRef, 7, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = LoadLogMetadata.PropertyNames.DataRef;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(LoadLogMetadata.ColumnNames.Message, 8, typeof(System.String), esSystemType.String);
            c.PropertyName = LoadLogMetadata.PropertyNames.Message;
            c.CharacterMaxLength = 2147483647;
            c.NumericPrecision = 0;
            _columns.Add(c);


        }
        #endregion

        static public LoadLogMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdLog = "IdLog";
            public const string IdExec = "IdExec";
            public const string DbUserId = "DbUserId";
            public const string ProcedureName = "ProcedureName";
            public const string DataInicioProcesso = "DataInicioProcesso";
            public const string DataFimProcesso = "DataFimProcesso";
            public const string Sucesso = "Sucesso";
            public const string DataRef = "DataRef";
            public const string Message = "Message";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdLog = "IdLog";
            public const string IdExec = "IdExec";
            public const string DbUserId = "DbUserId";
            public const string ProcedureName = "ProcedureName";
            public const string DataInicioProcesso = "DataInicioProcesso";
            public const string DataFimProcesso = "DataFimProcesso";
            public const string Sucesso = "Sucesso";
            public const string DataRef = "DataRef";
            public const string Message = "Message";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(LoadLogMetadata))
            {
                if (LoadLogMetadata.mapDelegates == null)
                {
                    LoadLogMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (LoadLogMetadata.meta == null)
                {
                    LoadLogMetadata.meta = new LoadLogMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdLog", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdExec", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("DbUserId", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("ProcedureName", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("DataInicioProcesso", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("DataFimProcesso", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("Sucesso", new esTypeMap("bit", "System.Boolean"));
                meta.AddTypeMap("DataRef", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("Message", new esTypeMap("varchar", "System.String"));



                meta.Source = "LOAD_LOG";
                meta.Destination = "LOAD_LOG";

                meta.spInsert = "proc_LOAD_LOGInsert";
                meta.spUpdate = "proc_LOAD_LOGUpdate";
                meta.spDelete = "proc_LOAD_LOGDelete";
                meta.spLoadAll = "proc_LOAD_LOGLoadAll";
                meta.spLoadByPrimaryKey = "proc_LOAD_LOGLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private LoadLogMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
