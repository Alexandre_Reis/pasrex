Ext.define('Financial.store.Cliente', {
    extend: 'Ext.data.Store',

    config: {
        model: 'Financial.model.Cliente',
        storeId: 'ClienteStore',
        listeners: [
            {
                fn: 'onBeforeLoad',
                event: 'beforeload'
            }
        ]
    },

    onBeforeLoad: function (store) {
        //define proxy at run time because we need the ticket authentication info
        if (!store.proxySet) {
            this.initProxy();
        }
    },

    initProxy: function () {
        if (this.proxySet === true) {
            return;
        }

        this.proxySet = true;
        this.setProxy({
            type: 'ajax',
            url: Financial.application.hostURL + '/ashx/controllers/clientehandler.ashx',
            extraParams: {
                //ticket: Financial.application.userInfo.Ticket
                ticket: null,
                action: 'list'
            },
            reader: {
                type: 'json',
                rootProperty: 'collection',
                successProperty: 'success'
            }
        });
    }
});