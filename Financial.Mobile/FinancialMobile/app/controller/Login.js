﻿Ext.define('Financial.controller.Login', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            loginView: 'loginview',
            mainMenuView: 'maintab'
        },
        control: {
            loginView: {
                signInCommand: 'onSignInCommand'
            }
        }
    },


    onSignInCommand: function (view, tenantName, username, password) {
        var me = this,
        loginView = me.getLoginView();

        if (username.length === 0 || password.length === 0 || tenantName.length === 0) {

            loginView.showSignInFailedMessage('Por favor, informe o seu login e senha.');
            return;
        }

        //Financial.application.hostURL = "http://" + tenantName + ".financialonline.com.br/fin_" + tenantName;
        //Financial.application.hostURL = "http://" + tenantName + '.financialonline.com.br/financial/financial.web';

        var indexOfMobile = window.location.href.toLowerCase().indexOf('/mobile');
        Financial.application.hostURL = window.location.href.substr(0, indexOfMobile);

        //Marreta para testes em DEV
        if (window.location.href.toLowerCase().indexOf('financial/financial.mobile') >= 0) {
            Financial.application.hostURL = '/financial/financial.web';
        }

        loginView.setMasked({
            xtype: 'loadmask',
            message: 'Autenticando...'
        });

        Ext.Ajax.request({
            url: Financial.application.hostURL + '/login/LoginAjax.ashx',
            method: 'post',
            params: {
                tenantName: tenantName,
                login: username,
                password: password
            },
            success: function (response) {

                var loginResponse = Ext.JSON.decode(response.responseText);

                if (loginResponse.success === true) {
                    //Checar se devemos enviar cliente para site do financial Desktop
                    var queryString = Ext.Object.fromQueryString(window.location.search.replace('?', ''));
                    if (queryString && queryString.mobile && queryString.mobile == 'false') {
                        window.location = Financial.application.hostURL + '/default.aspx?mobile=false';
                    }

                    Financial.application.userInfo = me.userInfo = loginResponse.userInfo;
                    me.signInSuccess();     //Just simulating success.
                } else {
                    me.signInFailure('Nome de usuário ou senha incorretos. Por favor, tente novamente.');
                }
            },
            failure: function (response) {
                me.userInfo = null;
                me.signInFailure('Login failed. Please try again later.');
            }
        });
    },

    signInSuccess: function () {
        var me = this;
        var loginView = this.getLoginView();


        var url = Financial.application.hostURL + '/ashx/appsettings.ashx';


        //Load globals
        Ext.Ajax.request({
            url: url,
            scope: this,
            params: {
                mobile: true
            },
            success: function (response) {

                var obj = Ext.decode(response.responseText);
                if (!obj.success) {
                    if (obj.erros && obj.erros.length) {
                        alert(obj.erros[0]);
                    }
                    return false;
                }

                Financial.application.idCarteira = obj.idCliente;
                Financial.application.idIndice = obj.idIndiceDefault;
                Financial.application.dataReferencia = obj.dataCliente;
                Financial.application.nomeCarteira = obj.nomeCliente;

                Financial.application.initStores(obj);

                var mainMenuView = Ext.Viewport.add([
                    { xtype: 'maintabpanel' }
                ]);
                Ext.Viewport.animateActiveItem(mainMenuView, this.getSlideLeftTransition());
                loginView.setMasked(false);

            }
        });
    },

    getSlideLeftTransition: function () {
        return { type: 'slide', direction: 'left' };
    },

    signInFailure: function (message) {
        var loginView = this.getLoginView();
        loginView.showSignInFailedMessage(message);
        loginView.setMasked(false);
    }
});