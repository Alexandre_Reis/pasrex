Ext.define('Financial.view.ListComposicaoCarteira', {
    extend: 'Financial.view.TouchTreeGrid',
    alias: 'widget.listcomposicaocarteira',

    config: {
        store: 'ComposicaoCarteiraStore',
        columns: [
                    {
                        header: 'Ativo',
                        dataIndex: 'Nome',
                        width: '60%',
                        style: 'text-align: left;',
                        categStyle: 'font-weight: bold; text-align: left; color: #858d91;',
                        headerStyle: 'text-align: left; color: #ccc;'
                    },
                    {
                        header: 'Saldo L&#237;quido',
                        dataIndex: 'SaldoLiquido',
                        width: '30%',
                        style: 'text-align: right;',
                        renderer: 'this.formatNumbers(values.SaldoLiquido, 0, null, null, ".", ",")',
                        categStyle: 'text-align: right;',
                        headerStyle: 'text-align: right; color: #ccc;'
                    }
                ],
        disclosureProperty: 'leaf',
        listItemId: 'composicaocarteiralist',
        includeFooter: false,
        categDepthColors: true,
        categDepthColorsArr: [
                    '#f8f9fa',
                    '#fff',
                    '#fff'
                ],
        applyDefaultCollapseLevel: false
    },

    updateDadosVisualizacao: function () {
        this.setMasked({
            xtype: 'loadmask',
            message: 'Atualizando...'
        });

        var tableStore = Ext.data.StoreManager.lookup('TableStore');
        if (!this.installedMaskOnStore) {
            tableStore.on('load', function () { this.setMasked(false); }, this);
            this.installedMaskOnStore = true;
        }
        if (!tableStore.loadDadosVisualizacao()) {
            this.setMasked(false);
        }
    }
});