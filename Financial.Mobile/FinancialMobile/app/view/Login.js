Ext.define('Financial.view.Login', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loginview',
    //requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img'],
    config: {
        title: 'Login',
        listeners: [{
            delegate: '#logInButton',
            event: 'tap',
            fn: 'onLogInButtonTap'
        }],

        items: [
        /*{
        xtype: 'image',
        src: Ext.Viewport.getOrientation() == 'portrait' ? '../../../img/login.png' : '../../../img/login-small.png',
        style: Ext.Viewport.getOrientation() == 'portrait' ? 'width:80px;height:80px;margin:auto' : 'width:40px;height:40px;margin:auto'
        },*/
                    {
                    xtype: 'label',
                    html: 'O login falhou. Por favor, informe as credenciais corretas.',
                    itemId: 'signInFailedLabel',
                    hidden: true,
                    hideAnimation: 'fadeOut',
                    showAnimation: 'fadeIn',
                    style: 'color:#990000;margin:5px 0px;'
                },
                    {
                        xtype: 'fieldset',
                        title: 'Financial',
                        items: [
                            {
                                xtype: 'textfield',
                                placeHolder: 'Instituição',
                                itemId: 'tenantNameTextField',
                                name: 'tenantNameTextField',
                                listeners: {
                                    initialize: function (field) {
                                        var tenant = window.location.hostname.split('.')[0];
                                        field.setValue(tenant);
                                        field.hide();
                                    }
                                },
                                required: true
                            },
                            {
                                xtype: 'textfield',
                                placeHolder: 'Login',
                                itemId: 'userNameTextField',
                                name: 'userNameTextField',
                                required: true/*,
                                value: 'adminmaster@$'*/
                            },
                            {
                                xtype: 'passwordfield',
                                placeHolder: 'Senha',
                                itemId: 'passwordTextField',
                                name: 'passwordTextField',
                                required: true
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        margin: '10px',
                        text: 'Entrar'
                    }
         ]
    },

    showSignInFailedMessage: function (message) {
        var label = this.down('#signInFailedLabel');
        label.setHtml(message);
        label.show();
    },


    onLogInButtonTap: function () {
        var me = this;

        var usernameField = me.down('#userNameTextField'),
        passwordField = me.down('#passwordTextField'),
        tenantNameField = me.down('#tenantNameTextField'),
        label = me.down('#signInFailedLabel');

        label.hide();

        var username = usernameField.getValue(),
        tenantName = tenantNameField.getValue(),
        password = passwordField.getValue();

        // Using a delayed task in order to give the hide animation above
        // time to finish before executing the next steps.
        var task = Ext.create('Ext.util.DelayedTask', function () {

            label.setHtml('');

            me.fireEvent('signInCommand', me, tenantName, username, password);

            usernameField.setValue('');
            passwordField.setValue('');
        });

        task.delay(500);
    }
});