﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.InvestidorCotista.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de InvestidorCotista
    /// </summary>
    public class InvestidorCotistaException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InvestidorCotistaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InvestidorCotistaException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InvestidorCotistaException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de Cotista
    /// </summary>
    public class CotistaSemPosicaoException : InvestidorCotistaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotistaSemPosicaoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotistaSemPosicaoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de Cotista
    /// </summary>
    public class CotistaNaoCadastradoException : InvestidorCotistaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotistaNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotistaNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de PosicaoCotista
    /// </summary>
    public class PosicaoCotistaNaoCadastradoException : InvestidorCotistaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public PosicaoCotistaNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public PosicaoCotistaNaoCadastradoException(string mensagem) : base(mensagem) { }
    }
}


