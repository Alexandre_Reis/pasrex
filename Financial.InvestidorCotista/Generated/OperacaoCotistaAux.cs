/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/04/2016 17:07:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esOperacaoCotistaAuxCollection : esEntityCollection
	{
		public esOperacaoCotistaAuxCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoCotistaAuxCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoCotistaAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoCotistaAuxQuery);
		}
		#endregion
		
		virtual public OperacaoCotistaAux DetachEntity(OperacaoCotistaAux entity)
		{
			return base.DetachEntity(entity) as OperacaoCotistaAux;
		}
		
		virtual public OperacaoCotistaAux AttachEntity(OperacaoCotistaAux entity)
		{
			return base.AttachEntity(entity) as OperacaoCotistaAux;
		}
		
		virtual public void Combine(OperacaoCotistaAuxCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoCotistaAux this[int index]
		{
			get
			{
				return base[index] as OperacaoCotistaAux;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoCotistaAux);
		}
	}



	[Serializable]
	abstract public class esOperacaoCotistaAux : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoCotistaAuxQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoCotistaAux()
		{

		}

		public esOperacaoCotistaAux(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacaoCotistaAux)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacaoCotistaAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacaoCotistaAux);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacaoCotistaAux)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacaoCotistaAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacaoCotistaAux);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacaoCotistaAux)
		{
			esOperacaoCotistaAuxQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacaoCotistaAux == idOperacaoCotistaAux);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacaoCotistaAux)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacaoCotistaAux",idOperacaoCotistaAux);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacaoCotistaAux": this.str.IdOperacaoCotistaAux = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "DataAgendamento": this.str.DataAgendamento = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "TipoResgate": this.str.TipoResgate = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "DadosBancarios": this.str.DadosBancarios = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "CotaInformada": this.str.CotaInformada = (string)value; break;							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "IdOperacaoAuxiliar": this.str.IdOperacaoAuxiliar = (string)value; break;							
						case "IdOrdem": this.str.IdOrdem = (string)value; break;							
						case "RegistroEditado": this.str.RegistroEditado = (string)value; break;							
						case "ValoresColados": this.str.ValoresColados = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdSeriesOffShore": this.str.IdSeriesOffShore = (string)value; break;							
						case "DepositoRetirada": this.str.DepositoRetirada = (string)value; break;							
						case "FieModalidade": this.str.FieModalidade = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "ValorDespesas": this.str.ValorDespesas = (string)value; break;							
						case "ValorTaxas": this.str.ValorTaxas = (string)value; break;							
						case "ValorTributos": this.str.ValorTributos = (string)value; break;							
						case "PenalidadeLockUp": this.str.PenalidadeLockUp = (string)value; break;							
						case "ValorHoldBack": this.str.ValorHoldBack = (string)value; break;							
						case "IdTransferenciaSerie": this.str.IdTransferenciaSerie = (string)value; break;							
						case "IdContaCorrente": this.str.IdContaCorrente = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DataAplicCautelaResgatada": this.str.DataAplicCautelaResgatada = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "ExclusaoLogica": this.str.ExclusaoLogica = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacaoCotistaAux":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoCotistaAux = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "DataAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAgendamento = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "TipoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoResgate = (System.Byte?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "CotaInformada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaInformada = (System.Decimal?)value;
							break;
						
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "IdOperacaoAuxiliar":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoAuxiliar = (System.Int32?)value;
							break;
						
						case "IdOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdem = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdSeriesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSeriesOffShore = (System.Int32?)value;
							break;
						
						case "DepositoRetirada":
						
							if (value == null || value.GetType().ToString() == "System.Boolean")
								this.DepositoRetirada = (System.Boolean?)value;
							break;
						
						case "FieModalidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieModalidade = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "ValorDespesas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDespesas = (System.Decimal?)value;
							break;
						
						case "ValorTaxas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTaxas = (System.Decimal?)value;
							break;
						
						case "ValorTributos":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTributos = (System.Decimal?)value;
							break;
						
						case "PenalidadeLockUp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PenalidadeLockUp = (System.Decimal?)value;
							break;
						
						case "ValorHoldBack":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorHoldBack = (System.Decimal?)value;
							break;
						
						case "IdTransferenciaSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferenciaSerie = (System.Int32?)value;
							break;
						
						case "IdContaCorrente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaCorrente = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DataAplicCautelaResgatada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicCautelaResgatada = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdOperacaoCotistaAux
		/// </summary>
		virtual public System.Int32? IdOperacaoCotistaAux
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoCotistaAux);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoCotistaAux, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataAgendamento
		/// </summary>
		virtual public System.DateTime? DataAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.TipoResgate
		/// </summary>
		virtual public System.Byte? TipoResgate
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.TipoResgate);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.TipoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.IdFormaLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DadosBancarios
		/// </summary>
		virtual public System.String DadosBancarios
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.DadosBancarios);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.DadosBancarios, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaAuxMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.CotaInformada
		/// </summary>
		virtual public System.Decimal? CotaInformada
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.CotaInformada);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.CotaInformada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdOperacaoAuxiliar
		/// </summary>
		virtual public System.Int32? IdOperacaoAuxiliar
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoAuxiliar);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoAuxiliar, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdOrdem
		/// </summary>
		virtual public System.Int32? IdOrdem
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOrdem);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.RegistroEditado
		/// </summary>
		virtual public System.String RegistroEditado
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.RegistroEditado);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.RegistroEditado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValoresColados
		/// </summary>
		virtual public System.String ValoresColados
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.ValoresColados);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.ValoresColados, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdSeriesOffShore
		/// </summary>
		virtual public System.Int32? IdSeriesOffShore
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdSeriesOffShore);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdSeriesOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DepositoRetirada
		/// </summary>
		virtual public System.Boolean? DepositoRetirada
		{
			get
			{
				return base.GetSystemBoolean(OperacaoCotistaAuxMetadata.ColumnNames.DepositoRetirada);
			}
			
			set
			{
				base.SetSystemBoolean(OperacaoCotistaAuxMetadata.ColumnNames.DepositoRetirada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.FieModalidade
		/// </summary>
		virtual public System.Int32? FieModalidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.FieModalidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.FieModalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorDespesas
		/// </summary>
		virtual public System.Decimal? ValorDespesas
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorDespesas);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorTaxas
		/// </summary>
		virtual public System.Decimal? ValorTaxas
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorTaxas);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorTaxas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorTributos
		/// </summary>
		virtual public System.Decimal? ValorTributos
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorTributos);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorTributos, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.PenalidadeLockUp
		/// </summary>
		virtual public System.Decimal? PenalidadeLockUp
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.PenalidadeLockUp);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.PenalidadeLockUp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ValorHoldBack
		/// </summary>
		virtual public System.Decimal? ValorHoldBack
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorHoldBack);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaAuxMetadata.ColumnNames.ValorHoldBack, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdTransferenciaSerie
		/// </summary>
		virtual public System.Int32? IdTransferenciaSerie
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdTransferenciaSerie);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdTransferenciaSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdContaCorrente
		/// </summary>
		virtual public System.Int32? IdContaCorrente
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdContaCorrente);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdContaCorrente, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.DataAplicCautelaResgatada
		/// </summary>
		virtual public System.DateTime? DataAplicCautelaResgatada
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataAplicCautelaResgatada);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.DataAplicCautelaResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaAuxMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCotistaAuxMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotistaAux.ExclusaoLogica
		/// </summary>
		virtual public System.String ExclusaoLogica
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.ExclusaoLogica);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaAuxMetadata.ColumnNames.ExclusaoLogica, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoCotistaAux entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacaoCotistaAux
			{
				get
				{
					System.Int32? data = entity.IdOperacaoCotistaAux;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoCotistaAux = null;
					else entity.IdOperacaoCotistaAux = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAgendamento
			{
				get
				{
					System.DateTime? data = entity.DataAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAgendamento = null;
					else entity.DataAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String TipoResgate
			{
				get
				{
					System.Byte? data = entity.TipoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoResgate = null;
					else entity.TipoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String DadosBancarios
			{
				get
				{
					System.String data = entity.DadosBancarios;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DadosBancarios = null;
					else entity.DadosBancarios = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String CotaInformada
			{
				get
				{
					System.Decimal? data = entity.CotaInformada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaInformada = null;
					else entity.CotaInformada = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoAuxiliar
			{
				get
				{
					System.Int32? data = entity.IdOperacaoAuxiliar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoAuxiliar = null;
					else entity.IdOperacaoAuxiliar = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOrdem
			{
				get
				{
					System.Int32? data = entity.IdOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdem = null;
					else entity.IdOrdem = Convert.ToInt32(value);
				}
			}
				
			public System.String RegistroEditado
			{
				get
				{
					System.String data = entity.RegistroEditado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroEditado = null;
					else entity.RegistroEditado = Convert.ToString(value);
				}
			}
				
			public System.String ValoresColados
			{
				get
				{
					System.String data = entity.ValoresColados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValoresColados = null;
					else entity.ValoresColados = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSeriesOffShore
			{
				get
				{
					System.Int32? data = entity.IdSeriesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSeriesOffShore = null;
					else entity.IdSeriesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String DepositoRetirada
			{
				get
				{
					System.Boolean? data = entity.DepositoRetirada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DepositoRetirada = null;
					else entity.DepositoRetirada = Convert.ToBoolean(value);
				}
			}
				
			public System.String FieModalidade
			{
				get
				{
					System.Int32? data = entity.FieModalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieModalidade = null;
					else entity.FieModalidade = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDespesas
			{
				get
				{
					System.Decimal? data = entity.ValorDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDespesas = null;
					else entity.ValorDespesas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTaxas
			{
				get
				{
					System.Decimal? data = entity.ValorTaxas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTaxas = null;
					else entity.ValorTaxas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTributos
			{
				get
				{
					System.Decimal? data = entity.ValorTributos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTributos = null;
					else entity.ValorTributos = Convert.ToDecimal(value);
				}
			}
				
			public System.String PenalidadeLockUp
			{
				get
				{
					System.Decimal? data = entity.PenalidadeLockUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PenalidadeLockUp = null;
					else entity.PenalidadeLockUp = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorHoldBack
			{
				get
				{
					System.Decimal? data = entity.ValorHoldBack;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorHoldBack = null;
					else entity.ValorHoldBack = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTransferenciaSerie
			{
				get
				{
					System.Int32? data = entity.IdTransferenciaSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferenciaSerie = null;
					else entity.IdTransferenciaSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String IdContaCorrente
			{
				get
				{
					System.Int32? data = entity.IdContaCorrente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaCorrente = null;
					else entity.IdContaCorrente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAplicCautelaResgatada
			{
				get
				{
					System.DateTime? data = entity.DataAplicCautelaResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicCautelaResgatada = null;
					else entity.DataAplicCautelaResgatada = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String ExclusaoLogica
			{
				get
				{
					System.String data = entity.ExclusaoLogica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExclusaoLogica = null;
					else entity.ExclusaoLogica = Convert.ToString(value);
				}
			}
			

			private esOperacaoCotistaAux entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoCotistaAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoCotistaAux can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoCotistaAux : esOperacaoCotistaAux
	{

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OpCotistaAux_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoCotistaAuxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCotistaAuxMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacaoCotistaAux
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoCotistaAux, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAgendamento
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.TipoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem DadosBancarios
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DadosBancarios, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CotaInformada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.CotaInformada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoAuxiliar
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoAuxiliar, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOrdem
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdOrdem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RegistroEditado
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.RegistroEditado, esSystemType.String);
			}
		} 
		
		public esQueryItem ValoresColados
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValoresColados, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSeriesOffShore
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdSeriesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DepositoRetirada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DepositoRetirada, esSystemType.Boolean);
			}
		} 
		
		public esQueryItem FieModalidade
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.FieModalidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDespesas
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorDespesas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTaxas
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorTaxas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTributos
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorTributos, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PenalidadeLockUp
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.PenalidadeLockUp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorHoldBack
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ValorHoldBack, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTransferenciaSerie
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdTransferenciaSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdContaCorrente
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdContaCorrente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAplicCautelaResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.DataAplicCautelaResgatada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExclusaoLogica
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaAuxMetadata.ColumnNames.ExclusaoLogica, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoCotistaAuxCollection")]
	public partial class OperacaoCotistaAuxCollection : esOperacaoCotistaAuxCollection, IEnumerable<OperacaoCotistaAux>
	{
		public OperacaoCotistaAuxCollection()
		{

		}
		
		public static implicit operator List<OperacaoCotistaAux>(OperacaoCotistaAuxCollection coll)
		{
			List<OperacaoCotistaAux> list = new List<OperacaoCotistaAux>();
			
			foreach (OperacaoCotistaAux emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoCotistaAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCotistaAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoCotistaAux(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoCotistaAux();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoCotistaAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCotistaAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoCotistaAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoCotistaAux AddNew()
		{
			OperacaoCotistaAux entity = base.AddNewEntity() as OperacaoCotistaAux;
			
			return entity;
		}

		public OperacaoCotistaAux FindByPrimaryKey(System.Int32 idOperacaoCotistaAux)
		{
			return base.FindByPrimaryKey(idOperacaoCotistaAux) as OperacaoCotistaAux;
		}


		#region IEnumerable<OperacaoCotistaAux> Members

		IEnumerator<OperacaoCotistaAux> IEnumerable<OperacaoCotistaAux>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoCotistaAux;
			}
		}

		#endregion
		
		private OperacaoCotistaAuxQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoCotistaAux' table
	/// </summary>

	[Serializable]
	public partial class OperacaoCotistaAux : esOperacaoCotistaAux
	{
		public OperacaoCotistaAux()
		{

		}
	
		public OperacaoCotistaAux(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCotistaAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoCotistaAuxQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCotistaAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoCotistaAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCotistaAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoCotistaAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoCotistaAuxQuery query;
	}



	[Serializable]
	public partial class OperacaoCotistaAuxQuery : esOperacaoCotistaAuxQuery
	{
		public OperacaoCotistaAuxQuery()
		{

		}		
		
		public OperacaoCotistaAuxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoCotistaAuxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoCotistaAuxMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoCotistaAux, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdOperacaoCotistaAux;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdCotista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataOperacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataConversao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataLiquidacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataAgendamento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataAgendamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.TipoOperacao, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.TipoResgate, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.TipoResgate;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdFormaLiquidacao, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdFormaLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.Quantidade, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.CotaOperacao, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorBruto, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorLiquido, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorIR, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorCPMF, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorPerformance, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.PrejuizoUsado, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.RendimentoResgate, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.VariacaoResgate, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.Observacao, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DadosBancarios, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DadosBancarios;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.Fonte, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdConta, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.CotaInformada, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.CotaInformada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdAgenda, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdAgenda;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoResgatada, 29, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacaoAuxiliar, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdOperacaoAuxiliar;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdOrdem, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdOrdem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.RegistroEditado, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.RegistroEditado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValoresColados, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValoresColados;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdLocalNegociacao, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('1')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdSeriesOffShore, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdSeriesOffShore;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DepositoRetirada, 36, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DepositoRetirada;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.FieModalidade, 37, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.FieModalidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.FieTabelaIr, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorDespesas, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorDespesas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorTaxas, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorTaxas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorTributos, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorTributos;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.PenalidadeLockUp, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.PenalidadeLockUp;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ValorHoldBack, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ValorHoldBack;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdTransferenciaSerie, 44, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdTransferenciaSerie;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdContaCorrente, 45, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdContaCorrente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataRegistro, 46, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.DataAplicCautelaResgatada, 47, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.DataAplicCautelaResgatada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.UserTimeStamp, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.IdTrader, 49, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaAuxMetadata.ColumnNames.ExclusaoLogica, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaAuxMetadata.PropertyNames.ExclusaoLogica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoCotistaAuxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacaoCotistaAux = "IdOperacaoCotistaAux";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string Observacao = "Observacao";
			 public const string DadosBancarios = "DadosBancarios";
			 public const string Fonte = "Fonte";
			 public const string IdConta = "IdConta";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdAgenda = "IdAgenda";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string IdOperacaoAuxiliar = "IdOperacaoAuxiliar";
			 public const string IdOrdem = "IdOrdem";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string ValorDespesas = "ValorDespesas";
			 public const string ValorTaxas = "ValorTaxas";
			 public const string ValorTributos = "ValorTributos";
			 public const string PenalidadeLockUp = "PenalidadeLockUp";
			 public const string ValorHoldBack = "ValorHoldBack";
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string IdContaCorrente = "IdContaCorrente";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacaoCotistaAux = "IdOperacaoCotistaAux";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string Observacao = "Observacao";
			 public const string DadosBancarios = "DadosBancarios";
			 public const string Fonte = "Fonte";
			 public const string IdConta = "IdConta";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdAgenda = "IdAgenda";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string IdOperacaoAuxiliar = "IdOperacaoAuxiliar";
			 public const string IdOrdem = "IdOrdem";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string ValorDespesas = "ValorDespesas";
			 public const string ValorTaxas = "ValorTaxas";
			 public const string ValorTributos = "ValorTributos";
			 public const string PenalidadeLockUp = "PenalidadeLockUp";
			 public const string ValorHoldBack = "ValorHoldBack";
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string IdContaCorrente = "IdContaCorrente";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoCotistaAuxMetadata))
			{
				if(OperacaoCotistaAuxMetadata.mapDelegates == null)
				{
					OperacaoCotistaAuxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoCotistaAuxMetadata.meta == null)
				{
					OperacaoCotistaAuxMetadata.meta = new OperacaoCotistaAuxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacaoCotistaAux", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataAgendamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DadosBancarios", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CotaInformada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoAuxiliar", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOrdem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RegistroEditado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValoresColados", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSeriesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DepositoRetirada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("FieModalidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDespesas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTaxas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTributos", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PenalidadeLockUp", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorHoldBack", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdTransferenciaSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdContaCorrente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataAplicCautelaResgatada", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExclusaoLogica", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "OperacaoCotistaAux";
				meta.Destination = "OperacaoCotistaAux";
				
				meta.spInsert = "proc_OperacaoCotistaAuxInsert";				
				meta.spUpdate = "proc_OperacaoCotistaAuxUpdate";		
				meta.spDelete = "proc_OperacaoCotistaAuxDelete";
				meta.spLoadAll = "proc_OperacaoCotistaAuxLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoCotistaAuxLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoCotistaAuxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
