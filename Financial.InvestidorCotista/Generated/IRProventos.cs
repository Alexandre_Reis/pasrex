/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/11/2015 19:55:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Fundo;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esIRProventosCollection : esEntityCollection
	{
		public esIRProventosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IRProventosCollection";
		}

		#region Query Logic
		protected void InitQuery(esIRProventosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIRProventosQuery);
		}
		#endregion
		
		virtual public IRProventos DetachEntity(IRProventos entity)
		{
			return base.DetachEntity(entity) as IRProventos;
		}
		
		virtual public IRProventos AttachEntity(IRProventos entity)
		{
			return base.AttachEntity(entity) as IRProventos;
		}
		
		virtual public void Combine(IRProventosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IRProventos this[int index]
		{
			get
			{
				return base[index] as IRProventos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IRProventos);
		}
	}



	[Serializable]
	abstract public class esIRProventos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIRProventosQuery GetDynamicQuery()
		{
			return null;
		}

		public esIRProventos()
		{

		}

		public esIRProventos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idIRProventos)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIRProventos);
			else
				return LoadByPrimaryKeyStoredProcedure(idIRProventos);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idIRProventos)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIRProventos);
			else
				return LoadByPrimaryKeyStoredProcedure(idIRProventos);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idIRProventos)
		{
			esIRProventosQuery query = this.GetDynamicQuery();
			query.Where(query.IdIRProventos == idIRProventos);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idIRProventos)
		{
			esParameters parms = new esParameters();
			parms.Add("IdIRProventos",idIRProventos);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdIRProventos": this.str.IdIRProventos = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "ValorDistribuido": this.str.ValorDistribuido = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdIRProventos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdIRProventos = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "ValorDistribuido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDistribuido = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IRProventos.IdIRProventos
		/// </summary>
		virtual public System.Int32? IdIRProventos
		{
			get
			{
				return base.GetSystemInt32(IRProventosMetadata.ColumnNames.IdIRProventos);
			}
			
			set
			{
				base.SetSystemInt32(IRProventosMetadata.ColumnNames.IdIRProventos, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(IRProventosMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(IRProventosMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.ValorDistribuido
		/// </summary>
		virtual public System.Decimal? ValorDistribuido
		{
			get
			{
				return base.GetSystemDecimal(IRProventosMetadata.ColumnNames.ValorDistribuido);
			}
			
			set
			{
				base.SetSystemDecimal(IRProventosMetadata.ColumnNames.ValorDistribuido, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(IRProventosMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(IRProventosMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(IRProventosMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(IRProventosMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(IRProventosMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(IRProventosMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(IRProventosMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(IRProventosMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(IRProventosMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(IRProventosMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.IsentoIR
		/// </summary>
		virtual public System.String IsentoIR
		{
			get
			{
				return base.GetSystemString(IRProventosMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemString(IRProventosMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to IRProventos.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(IRProventosMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(IRProventosMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIRProventos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdIRProventos
			{
				get
				{
					System.Int32? data = entity.IdIRProventos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIRProventos = null;
					else entity.IdIRProventos = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDistribuido
			{
				get
				{
					System.Decimal? data = entity.ValorDistribuido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDistribuido = null;
					else entity.ValorDistribuido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.String data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToString(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
			

			private esIRProventos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIRProventosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIRProventos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IRProventos : esIRProventos
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - IRProventos_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - IRProventos_Cotista_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIRProventosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IRProventosMetadata.Meta();
			}
		}	
		

		public esQueryItem IdIRProventos
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.IdIRProventos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDistribuido
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.ValorDistribuido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.IsentoIR, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, IRProventosMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IRProventosCollection")]
	public partial class IRProventosCollection : esIRProventosCollection, IEnumerable<IRProventos>
	{
		public IRProventosCollection()
		{

		}
		
		public static implicit operator List<IRProventos>(IRProventosCollection coll)
		{
			List<IRProventos> list = new List<IRProventos>();
			
			foreach (IRProventos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IRProventosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IRProventosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IRProventos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IRProventos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IRProventosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IRProventosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IRProventosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IRProventos AddNew()
		{
			IRProventos entity = base.AddNewEntity() as IRProventos;
			
			return entity;
		}

		public IRProventos FindByPrimaryKey(System.Int32 idIRProventos)
		{
			return base.FindByPrimaryKey(idIRProventos) as IRProventos;
		}


		#region IEnumerable<IRProventos> Members

		IEnumerator<IRProventos> IEnumerable<IRProventos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IRProventos;
			}
		}

		#endregion
		
		private IRProventosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IRProventos' table
	/// </summary>

	[Serializable]
	public partial class IRProventos : esIRProventos
	{
		public IRProventos()
		{

		}
	
		public IRProventos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IRProventosMetadata.Meta();
			}
		}
		
		
		
		override protected esIRProventosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IRProventosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IRProventosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IRProventosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IRProventosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IRProventosQuery query;
	}



	[Serializable]
	public partial class IRProventosQuery : esIRProventosQuery
	{
		public IRProventosQuery()
		{

		}		
		
		public IRProventosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IRProventosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IRProventosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.IdIRProventos, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IRProventosMetadata.PropertyNames.IdIRProventos;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IRProventosMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.ValorDistribuido, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IRProventosMetadata.PropertyNames.ValorDistribuido;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.ValorIR, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IRProventosMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.IdCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IRProventosMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.DataLancamento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IRProventosMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IRProventosMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.Descricao, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = IRProventosMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.IsentoIR, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = IRProventosMetadata.PropertyNames.IsentoIR;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRProventosMetadata.ColumnNames.ValorLiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IRProventosMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IRProventosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdIRProventos = "IdIRProventos";
			 public const string IdCotista = "IdCotista";
			 public const string ValorDistribuido = "ValorDistribuido";
			 public const string ValorIR = "ValorIR";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string IsentoIR = "IsentoIR";
			 public const string ValorLiquido = "ValorLiquido";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdIRProventos = "IdIRProventos";
			 public const string IdCotista = "IdCotista";
			 public const string ValorDistribuido = "ValorDistribuido";
			 public const string ValorIR = "ValorIR";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string IsentoIR = "IsentoIR";
			 public const string ValorLiquido = "ValorLiquido";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IRProventosMetadata))
			{
				if(IRProventosMetadata.mapDelegates == null)
				{
					IRProventosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IRProventosMetadata.meta == null)
				{
					IRProventosMetadata.meta = new IRProventosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdIRProventos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDistribuido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "IRProventos";
				meta.Destination = "IRProventos";
				
				meta.spInsert = "proc_IRProventosInsert";				
				meta.spUpdate = "proc_IRProventosUpdate";		
				meta.spDelete = "proc_IRProventosDelete";
				meta.spLoadAll = "proc_IRProventosLoadAll";
				meta.spLoadByPrimaryKey = "proc_IRProventosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IRProventosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
