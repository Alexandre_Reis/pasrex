/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2016 17:11:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Security;
using Financial.CRM;
using Financial.Fundo;
using Financial.Investidor;



namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esCotistaCollection : esEntityCollection
	{
		public esCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotistaQuery);
		}
		#endregion
		
		virtual public Cotista DetachEntity(Cotista entity)
		{
			return base.DetachEntity(entity) as Cotista;
		}
		
		virtual public Cotista AttachEntity(Cotista entity)
		{
			return base.AttachEntity(entity) as Cotista;
		}
		
		virtual public void Combine(CotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Cotista this[int index]
		{
			get
			{
				return base[index] as Cotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Cotista);
		}
	}



	[Serializable]
	abstract public class esCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotista()
		{

		}

		public esCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCotista);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCotista)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotistaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCotista == idCotista);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCotista)
		{
			esCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCotista == idCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCotista",idCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "IsentoIOF": this.str.IsentoIOF = (string)value; break;							
						case "StatusAtivo": this.str.StatusAtivo = (string)value; break;							
						case "TipoCotistaCVM": this.str.TipoCotistaCVM = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "TipoTributacao": this.str.TipoTributacao = (string)value; break;							
						case "DataExpiracao": this.str.DataExpiracao = (string)value; break;							
						case "PendenciaCadastral": this.str.PendenciaCadastral = (string)value; break;							
						case "IdClienteEspelho": this.str.IdClienteEspelho = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "OffShore": this.str.OffShore = (string)value; break;							
						case "CodigoConsolidacaoExterno": this.str.CodigoConsolidacaoExterno = (string)value; break;
						case "InvestidorProfissional": this.str.InvestidorProfissional = (string)value; break;							
						case "InvestidorQualificado": this.str.InvestidorQualificado = (string)value; break;
						case "TipoCotistaAnbima": this.str.TipoCotistaAnbima = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "StatusAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.StatusAtivo = (System.Byte?)value;
							break;
						
						case "TipoCotistaCVM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCotistaCVM = (System.Int32?)value;
							break;
						
						case "TipoTributacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTributacao = (System.Byte?)value;
							break;
						
						case "DataExpiracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataExpiracao = (System.DateTime?)value;
							break;
						
						case "IdClienteEspelho":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClienteEspelho = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
                            break;

						case "TipoCotistaAnbima":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCotistaAnbima = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Cotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(CotistaMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.IsentoIR
		/// </summary>
		virtual public System.String IsentoIR
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.IsentoIOF
		/// </summary>
		virtual public System.String IsentoIOF
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.IsentoIOF);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.IsentoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.StatusAtivo
		/// </summary>
		virtual public System.Byte? StatusAtivo
		{
			get
			{
				return base.GetSystemByte(CotistaMetadata.ColumnNames.StatusAtivo);
			}
			
			set
			{
				base.SetSystemByte(CotistaMetadata.ColumnNames.StatusAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.TipoCotistaCVM
		/// </summary>
		virtual public System.Int32? TipoCotistaCVM
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.TipoCotistaCVM);
			}
			
			set
			{
				base.SetSystemInt32(CotistaMetadata.ColumnNames.TipoCotistaCVM, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.TipoTributacao
		/// </summary>
		virtual public System.Byte? TipoTributacao
		{
			get
			{
				return base.GetSystemByte(CotistaMetadata.ColumnNames.TipoTributacao);
			}
			
			set
			{
				base.SetSystemByte(CotistaMetadata.ColumnNames.TipoTributacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.DataExpiracao
		/// </summary>
		virtual public System.DateTime? DataExpiracao
		{
			get
			{
				return base.GetSystemDateTime(CotistaMetadata.ColumnNames.DataExpiracao);
			}
			
			set
			{
				base.SetSystemDateTime(CotistaMetadata.ColumnNames.DataExpiracao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.PendenciaCadastral
		/// </summary>
		virtual public System.String PendenciaCadastral
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.PendenciaCadastral);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.PendenciaCadastral, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.IdClienteEspelho
		/// </summary>
		virtual public System.Int32? IdClienteEspelho
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.IdClienteEspelho);
			}
			
			set
			{
				base.SetSystemInt32(CotistaMetadata.ColumnNames.IdClienteEspelho, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{

				if(base.SetSystemInt32(CotistaMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		/// Maps to Cotista.TipoCotistaAnbima
		/// </summary>
		virtual public System.Int32? TipoCotistaAnbima
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.TipoCotistaAnbima);
            }

            set
            {
				base.SetSystemInt32(CotistaMetadata.ColumnNames.TipoCotistaAnbima, value);
			}
		}

		
		/// <summary>
		/// Maps to Cotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CotistaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cotista.OffShore
		/// </summary>
		virtual public System.String OffShore
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.OffShore);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.OffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.CodigoConsolidacaoExterno
		/// </summary>
		virtual public System.String CodigoConsolidacaoExterno
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.CodigoConsolidacaoExterno);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.CodigoConsolidacaoExterno, value);
			}
		}

		/// <summary>
		/// Maps to Cotista.InvestidorProfissional
		/// </summary>
		virtual public System.String InvestidorProfissional
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.InvestidorProfissional);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.InvestidorProfissional, value);
			}
		}
		
		/// <summary>
		/// Maps to Cotista.InvestidorQualificado
		/// </summary>
		virtual public System.String InvestidorQualificado
		{
			get
			{
				return base.GetSystemString(CotistaMetadata.ColumnNames.InvestidorQualificado);
			}
			
			set
			{
				base.SetSystemString(CotistaMetadata.ColumnNames.InvestidorQualificado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;

		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.String data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIOF
			{
				get
				{
					System.String data = entity.IsentoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIOF = null;
					else entity.IsentoIOF = Convert.ToString(value);
				}
			}
				
			public System.String StatusAtivo
			{
				get
				{
					System.Byte? data = entity.StatusAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusAtivo = null;
					else entity.StatusAtivo = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCotistaCVM
			{
				get
				{
					System.Int32? data = entity.TipoCotistaCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCotistaCVM = null;
					else entity.TipoCotistaCVM = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String TipoTributacao
			{
				get
				{
					System.Byte? data = entity.TipoTributacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTributacao = null;
					else entity.TipoTributacao = Convert.ToByte(value);
				}
			}
				
			public System.String DataExpiracao
			{
				get
				{
					System.DateTime? data = entity.DataExpiracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataExpiracao = null;
					else entity.DataExpiracao = Convert.ToDateTime(value);
				}
			}
				
			public System.String PendenciaCadastral
			{
				get
				{
					System.String data = entity.PendenciaCadastral;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PendenciaCadastral = null;
					else entity.PendenciaCadastral = Convert.ToString(value);
				}
			}
				
			public System.String IdClienteEspelho
			{
				get
				{
					System.Int32? data = entity.IdClienteEspelho;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClienteEspelho = null;
					else entity.IdClienteEspelho = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String OffShore
			{
				get
				{
					System.String data = entity.OffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OffShore = null;
					else entity.OffShore = Convert.ToString(value);
				}
			}
				
			public System.String CodigoConsolidacaoExterno
			{
				get
				{
					System.String data = entity.CodigoConsolidacaoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoConsolidacaoExterno = null;
					else entity.CodigoConsolidacaoExterno = Convert.ToString(value);
				}
			}

			public System.String InvestidorProfissional
			{
				get
				{
					System.String data = entity.InvestidorProfissional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestidorProfissional = null;
					else entity.InvestidorProfissional = Convert.ToString(value);
				}
			}
				
			public System.String InvestidorQualificado
			{
				get
				{
					System.String data = entity.InvestidorQualificado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestidorQualificado = null;
					else entity.InvestidorQualificado = Convert.ToString(value);
				}
			}
				
			public System.String TipoCotistaAnbima
			{
				get
				{
					System.Int32? data = entity.TipoCotistaAnbima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCotistaAnbima = null;
					else entity.TipoCotistaAnbima = Convert.ToInt32(value);
				}
			}
			

			private esCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Cotista : esCotista
	{

				
		#region ContaCorrenteCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Pessoa_Cotista_FK
		/// </summary>

		[XmlIgnore]
		public ContaCorrenteCollection ContaCorrenteCollectionByIdCotista
		{
			get
			{
				if(this._ContaCorrenteCollectionByIdCotista == null)
				{
					this._ContaCorrenteCollectionByIdCotista = new ContaCorrenteCollection();
					this._ContaCorrenteCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContaCorrenteCollectionByIdCotista", this._ContaCorrenteCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._ContaCorrenteCollectionByIdCotista.Query.Where(this._ContaCorrenteCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._ContaCorrenteCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContaCorrenteCollectionByIdCotista.fks.Add(ContaCorrenteMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._ContaCorrenteCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContaCorrenteCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("ContaCorrenteCollectionByIdCotista"); 
					this._ContaCorrenteCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private ContaCorrenteCollection _ContaCorrenteCollectionByIdCotista;
		#endregion

				
		#region DetalheResgateCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_DetalheResgateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public DetalheResgateCotistaCollection DetalheResgateCotistaCollectionByIdCotista
		{
			get
			{
				if(this._DetalheResgateCotistaCollectionByIdCotista == null)
				{
					this._DetalheResgateCotistaCollectionByIdCotista = new DetalheResgateCotistaCollection();
					this._DetalheResgateCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DetalheResgateCotistaCollectionByIdCotista", this._DetalheResgateCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._DetalheResgateCotistaCollectionByIdCotista.Query.Where(this._DetalheResgateCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._DetalheResgateCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._DetalheResgateCotistaCollectionByIdCotista.fks.Add(DetalheResgateCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._DetalheResgateCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DetalheResgateCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("DetalheResgateCotistaCollectionByIdCotista"); 
					this._DetalheResgateCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private DetalheResgateCotistaCollection _DetalheResgateCotistaCollectionByIdCotista;
		#endregion

				
		#region OperacaoCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoCotistaCollection OperacaoCotistaCollectionByIdCotista
		{
			get
			{
				if(this._OperacaoCotistaCollectionByIdCotista == null)
				{
					this._OperacaoCotistaCollectionByIdCotista = new OperacaoCotistaCollection();
					this._OperacaoCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoCotistaCollectionByIdCotista", this._OperacaoCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._OperacaoCotistaCollectionByIdCotista.Query.Where(this._OperacaoCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._OperacaoCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoCotistaCollectionByIdCotista.fks.Add(OperacaoCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._OperacaoCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("OperacaoCotistaCollectionByIdCotista"); 
					this._OperacaoCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private OperacaoCotistaCollection _OperacaoCotistaCollectionByIdCotista;
		#endregion

				
		#region OrdemCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_OrdemCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemCotistaCollection OrdemCotistaCollectionByIdCotista
		{
			get
			{
				if(this._OrdemCotistaCollectionByIdCotista == null)
				{
					this._OrdemCotistaCollectionByIdCotista = new OrdemCotistaCollection();
					this._OrdemCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemCotistaCollectionByIdCotista", this._OrdemCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._OrdemCotistaCollectionByIdCotista.Query.Where(this._OrdemCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._OrdemCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemCotistaCollectionByIdCotista.fks.Add(OrdemCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._OrdemCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("OrdemCotistaCollectionByIdCotista"); 
					this._OrdemCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private OrdemCotistaCollection _OrdemCotistaCollectionByIdCotista;
		#endregion

		#region UpToUsuarioCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Cotista_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public UsuarioCollection UpToUsuarioCollection
		{
			get
			{
				if(this._UpToUsuarioCollection == null)
				{
					this._UpToUsuarioCollection = new UsuarioCollection();
					this._UpToUsuarioCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToUsuarioCollection", this._UpToUsuarioCollection);
					this._UpToUsuarioCollection.ManyToManyCotistaCollection(this.IdCotista);
				}

				return this._UpToUsuarioCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToUsuarioCollection != null) 
				{ 
					this.RemovePostSave("UpToUsuarioCollection"); 
					this._UpToUsuarioCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Cotista_PermissaoCotista_FK1
		/// </summary>
		public void AssociateUsuarioCollection(Usuario entity)
		{
			if (this._PermissaoCotistaCollection == null)
			{
				this._PermissaoCotistaCollection = new PermissaoCotistaCollection();
				this._PermissaoCotistaCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoCotistaCollection", this._PermissaoCotistaCollection);
			}

			PermissaoCotista obj = this._PermissaoCotistaCollection.AddNew();
			obj.IdCotista = this.IdCotista;
			obj.IdUsuario = entity.IdUsuario;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Cotista_PermissaoCotista_FK1
		/// </summary>
		public void DissociateUsuarioCollection(Usuario entity)
		{
			if (this._PermissaoCotistaCollection == null)
			{
				this._PermissaoCotistaCollection = new PermissaoCotistaCollection();
				this._PermissaoCotistaCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoCotistaCollection", this._PermissaoCotistaCollection);
			}

			PermissaoCotista obj = this._PermissaoCotistaCollection.AddNew();
			obj.IdCotista = this.IdCotista;
			obj.IdUsuario = entity.IdUsuario;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private UsuarioCollection _UpToUsuarioCollection;
		private PermissaoCotistaCollection _PermissaoCotistaCollection;
		#endregion

				
		#region PermissaoCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoCotistaCollection PermissaoCotistaCollectionByIdCotista
		{
			get
			{
				if(this._PermissaoCotistaCollectionByIdCotista == null)
				{
					this._PermissaoCotistaCollectionByIdCotista = new PermissaoCotistaCollection();
					this._PermissaoCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoCotistaCollectionByIdCotista", this._PermissaoCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PermissaoCotistaCollectionByIdCotista.Query.Where(this._PermissaoCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PermissaoCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoCotistaCollectionByIdCotista.fks.Add(PermissaoCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PermissaoCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PermissaoCotistaCollectionByIdCotista"); 
					this._PermissaoCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PermissaoCotistaCollection _PermissaoCotistaCollectionByIdCotista;
		#endregion

				
		#region PosicaoCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PosicaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaCollection PosicaoCotistaCollectionByIdCotista
		{
			get
			{
				if(this._PosicaoCotistaCollectionByIdCotista == null)
				{
					this._PosicaoCotistaCollectionByIdCotista = new PosicaoCotistaCollection();
					this._PosicaoCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaCollectionByIdCotista", this._PosicaoCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PosicaoCotistaCollectionByIdCotista.Query.Where(this._PosicaoCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PosicaoCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaCollectionByIdCotista.fks.Add(PosicaoCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PosicaoCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaCollectionByIdCotista"); 
					this._PosicaoCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PosicaoCotistaCollection _PosicaoCotistaCollectionByIdCotista;
		#endregion

				
		#region PosicaoCotistaAberturaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PosicaoCotistaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaAberturaCollection PosicaoCotistaAberturaCollectionByIdCotista
		{
			get
			{
				if(this._PosicaoCotistaAberturaCollectionByIdCotista == null)
				{
					this._PosicaoCotistaAberturaCollectionByIdCotista = new PosicaoCotistaAberturaCollection();
					this._PosicaoCotistaAberturaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaAberturaCollectionByIdCotista", this._PosicaoCotistaAberturaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PosicaoCotistaAberturaCollectionByIdCotista.Query.Where(this._PosicaoCotistaAberturaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PosicaoCotistaAberturaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaAberturaCollectionByIdCotista.fks.Add(PosicaoCotistaAberturaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PosicaoCotistaAberturaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaAberturaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaAberturaCollectionByIdCotista"); 
					this._PosicaoCotistaAberturaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PosicaoCotistaAberturaCollection _PosicaoCotistaAberturaCollectionByIdCotista;
		#endregion

				
		#region PosicaoCotistaHistoricoCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PosicaoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaHistoricoCollection PosicaoCotistaHistoricoCollectionByIdCotista
		{
			get
			{
				if(this._PosicaoCotistaHistoricoCollectionByIdCotista == null)
				{
					this._PosicaoCotistaHistoricoCollectionByIdCotista = new PosicaoCotistaHistoricoCollection();
					this._PosicaoCotistaHistoricoCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaHistoricoCollectionByIdCotista", this._PosicaoCotistaHistoricoCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PosicaoCotistaHistoricoCollectionByIdCotista.Query.Where(this._PosicaoCotistaHistoricoCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PosicaoCotistaHistoricoCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaHistoricoCollectionByIdCotista.fks.Add(PosicaoCotistaHistoricoMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PosicaoCotistaHistoricoCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaHistoricoCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaHistoricoCollectionByIdCotista"); 
					this._PosicaoCotistaHistoricoCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PosicaoCotistaHistoricoCollection _PosicaoCotistaHistoricoCollectionByIdCotista;
		#endregion

				
		#region PrejuizoCotistaCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PrejuizoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaCollection PrejuizoCotistaCollectionByIdCotista
		{
			get
			{
				if(this._PrejuizoCotistaCollectionByIdCotista == null)
				{
					this._PrejuizoCotistaCollectionByIdCotista = new PrejuizoCotistaCollection();
					this._PrejuizoCotistaCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaCollectionByIdCotista", this._PrejuizoCotistaCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PrejuizoCotistaCollectionByIdCotista.Query.Where(this._PrejuizoCotistaCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PrejuizoCotistaCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaCollectionByIdCotista.fks.Add(PrejuizoCotistaMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PrejuizoCotistaCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaCollectionByIdCotista"); 
					this._PrejuizoCotistaCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaCollection _PrejuizoCotistaCollectionByIdCotista;
		#endregion

				
		#region PrejuizoCotistaHistoricoCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_PrejuizoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaHistoricoCollection PrejuizoCotistaHistoricoCollectionByIdCotista
		{
			get
			{
				if(this._PrejuizoCotistaHistoricoCollectionByIdCotista == null)
				{
					this._PrejuizoCotistaHistoricoCollectionByIdCotista = new PrejuizoCotistaHistoricoCollection();
					this._PrejuizoCotistaHistoricoCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaHistoricoCollectionByIdCotista", this._PrejuizoCotistaHistoricoCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PrejuizoCotistaHistoricoCollectionByIdCotista.Query.Where(this._PrejuizoCotistaHistoricoCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PrejuizoCotistaHistoricoCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaHistoricoCollectionByIdCotista.fks.Add(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PrejuizoCotistaHistoricoCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaHistoricoCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaHistoricoCollectionByIdCotista"); 
					this._PrejuizoCotistaHistoricoCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaHistoricoCollection _PrejuizoCotistaHistoricoCollectionByIdCotista;
		#endregion

				
		#region PrejuizoCotistaUsadoCollectionByIdCotista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PrejuizoCotistaUsado_Cotista
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaUsadoCollection PrejuizoCotistaUsadoCollectionByIdCotista
		{
			get
			{
				if(this._PrejuizoCotistaUsadoCollectionByIdCotista == null)
				{
					this._PrejuizoCotistaUsadoCollectionByIdCotista = new PrejuizoCotistaUsadoCollection();
					this._PrejuizoCotistaUsadoCollectionByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaUsadoCollectionByIdCotista", this._PrejuizoCotistaUsadoCollectionByIdCotista);
				
					if(this.IdCotista != null)
					{
						this._PrejuizoCotistaUsadoCollectionByIdCotista.Query.Where(this._PrejuizoCotistaUsadoCollectionByIdCotista.Query.IdCotista == this.IdCotista);
						this._PrejuizoCotistaUsadoCollectionByIdCotista.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaUsadoCollectionByIdCotista.fks.Add(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCotista, this.IdCotista);
					}
				}

				return this._PrejuizoCotistaUsadoCollectionByIdCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaUsadoCollectionByIdCotista != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaUsadoCollectionByIdCotista"); 
					this._PrejuizoCotistaUsadoCollectionByIdCotista = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaUsadoCollection _PrejuizoCotistaUsadoCollectionByIdCotista;
		#endregion

				
		#region TemplateCotista - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cotista_TemplateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public TemplateCotista TemplateCotista
		{
			get
			{
				if(this._TemplateCotista == null)
				{
					this._TemplateCotista = new TemplateCotista();
					this._TemplateCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("TemplateCotista", this._TemplateCotista);
				
					if(this.IdCotista != null)
					{
						this._TemplateCotista.Query.Where(this._TemplateCotista.Query.IdCotista == this.IdCotista);
						this._TemplateCotista.Query.Load();
					}
				}

				return this._TemplateCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TemplateCotista != null) 
				{ 
					this.RemovePostOneSave("TemplateCotista"); 
					this._TemplateCotista = null;
					
				} 
			}          			
		}

		private TemplateCotista _TemplateCotista;
		#endregion

				
		#region TransferenciaCotaCollectionByIdCotistaOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_TransferenciaCota_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaCotaCollection TransferenciaCotaCollectionByIdCotistaOrigem
		{
			get
			{
				if(this._TransferenciaCotaCollectionByIdCotistaOrigem == null)
				{
					this._TransferenciaCotaCollectionByIdCotistaOrigem = new TransferenciaCotaCollection();
					this._TransferenciaCotaCollectionByIdCotistaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaCotaCollectionByIdCotistaOrigem", this._TransferenciaCotaCollectionByIdCotistaOrigem);
				
					if(this.IdCotista != null)
					{
						this._TransferenciaCotaCollectionByIdCotistaOrigem.Query.Where(this._TransferenciaCotaCollectionByIdCotistaOrigem.Query.IdCotistaOrigem == this.IdCotista);
						this._TransferenciaCotaCollectionByIdCotistaOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaCotaCollectionByIdCotistaOrigem.fks.Add(TransferenciaCotaMetadata.ColumnNames.IdCotistaOrigem, this.IdCotista);
					}
				}

				return this._TransferenciaCotaCollectionByIdCotistaOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaCotaCollectionByIdCotistaOrigem != null) 
				{ 
					this.RemovePostSave("TransferenciaCotaCollectionByIdCotistaOrigem"); 
					this._TransferenciaCotaCollectionByIdCotistaOrigem = null;
					
				} 
			} 			
		}

		private TransferenciaCotaCollection _TransferenciaCotaCollectionByIdCotistaOrigem;
		#endregion

				
		#region TransferenciaCotaCollectionByIdCotistaDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_TransferenciaCota_FK2
		/// </summary>

		[XmlIgnore]
		public TransferenciaCotaCollection TransferenciaCotaCollectionByIdCotistaDestino
		{
			get
			{
				if(this._TransferenciaCotaCollectionByIdCotistaDestino == null)
				{
					this._TransferenciaCotaCollectionByIdCotistaDestino = new TransferenciaCotaCollection();
					this._TransferenciaCotaCollectionByIdCotistaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaCotaCollectionByIdCotistaDestino", this._TransferenciaCotaCollectionByIdCotistaDestino);
				
					if(this.IdCotista != null)
					{
						this._TransferenciaCotaCollectionByIdCotistaDestino.Query.Where(this._TransferenciaCotaCollectionByIdCotistaDestino.Query.IdCotistaDestino == this.IdCotista);
						this._TransferenciaCotaCollectionByIdCotistaDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaCotaCollectionByIdCotistaDestino.fks.Add(TransferenciaCotaMetadata.ColumnNames.IdCotistaDestino, this.IdCotista);
					}
				}

				return this._TransferenciaCotaCollectionByIdCotistaDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaCotaCollectionByIdCotistaDestino != null) 
				{ 
					this.RemovePostSave("TransferenciaCotaCollectionByIdCotistaDestino"); 
					this._TransferenciaCotaCollectionByIdCotistaDestino = null;
					
				} 
			} 			
		}

		private TransferenciaCotaCollection _TransferenciaCotaCollectionByIdCotistaDestino;
		#endregion

				
		#region TransferenciaEntreCotistaCollectionByIdCotistaOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransEntreCotista_CotOri_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaEntreCotistaCollection TransferenciaEntreCotistaCollectionByIdCotistaOrigem
		{
			get
			{
				if(this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem == null)
				{
					this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem = new TransferenciaEntreCotistaCollection();
					this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaEntreCotistaCollectionByIdCotistaOrigem", this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem);
				
					if(this.IdCotista != null)
					{
						this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem.Query.Where(this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem.Query.IdCotistaOrigem == this.IdCotista);
						this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem.fks.Add(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaOrigem, this.IdCotista);
					}
				}

				return this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem != null) 
				{ 
					this.RemovePostSave("TransferenciaEntreCotistaCollectionByIdCotistaOrigem"); 
					this._TransferenciaEntreCotistaCollectionByIdCotistaOrigem = null;
					
				} 
			} 			
		}

		private TransferenciaEntreCotistaCollection _TransferenciaEntreCotistaCollectionByIdCotistaOrigem;
		#endregion

				
		#region TransferenciaEntreCotistaCollectionByIdCotistaDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransEntreCotista_CotDes_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaEntreCotistaCollection TransferenciaEntreCotistaCollectionByIdCotistaDestino
		{
			get
			{
				if(this._TransferenciaEntreCotistaCollectionByIdCotistaDestino == null)
				{
					this._TransferenciaEntreCotistaCollectionByIdCotistaDestino = new TransferenciaEntreCotistaCollection();
					this._TransferenciaEntreCotistaCollectionByIdCotistaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaEntreCotistaCollectionByIdCotistaDestino", this._TransferenciaEntreCotistaCollectionByIdCotistaDestino);
				
					if(this.IdCotista != null)
					{
						this._TransferenciaEntreCotistaCollectionByIdCotistaDestino.Query.Where(this._TransferenciaEntreCotistaCollectionByIdCotistaDestino.Query.IdCotistaDestino == this.IdCotista);
						this._TransferenciaEntreCotistaCollectionByIdCotistaDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaEntreCotistaCollectionByIdCotistaDestino.fks.Add(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino, this.IdCotista);
					}
				}

				return this._TransferenciaEntreCotistaCollectionByIdCotistaDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaEntreCotistaCollectionByIdCotistaDestino != null) 
				{ 
					this.RemovePostSave("TransferenciaEntreCotistaCollectionByIdCotistaDestino"); 
					this._TransferenciaEntreCotistaCollectionByIdCotistaDestino = null;
					
				} 
			} 			
		}

		private TransferenciaEntreCotistaCollection _TransferenciaEntreCotistaCollectionByIdCotistaDestino;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_IdCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_IdPessoa_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();

            props.Add(new esPropertyDescriptor(this, "ContaCorrenteCollectionByIdCotista", typeof(ContaCorrenteCollection), new Financial.Investidor.ContaCorrente()));
			props.Add(new esPropertyDescriptor(this, "DetalheResgateCotistaCollectionByIdCotista", typeof(DetalheResgateCotistaCollection), new DetalheResgateCotista()));
			props.Add(new esPropertyDescriptor(this, "OperacaoCotistaCollectionByIdCotista", typeof(OperacaoCotistaCollection), new OperacaoCotista()));
			props.Add(new esPropertyDescriptor(this, "OrdemCotistaCollectionByIdCotista", typeof(OrdemCotistaCollection), new OrdemCotista()));
			props.Add(new esPropertyDescriptor(this, "PermissaoCotistaCollectionByIdCotista", typeof(PermissaoCotistaCollection), new PermissaoCotista()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaCollectionByIdCotista", typeof(PosicaoCotistaCollection), new PosicaoCotista()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaAberturaCollectionByIdCotista", typeof(PosicaoCotistaAberturaCollection), new PosicaoCotistaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaHistoricoCollectionByIdCotista", typeof(PosicaoCotistaHistoricoCollection), new PosicaoCotistaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaCollectionByIdCotista", typeof(PrejuizoCotistaCollection), new PrejuizoCotista()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaHistoricoCollectionByIdCotista", typeof(PrejuizoCotistaHistoricoCollection), new PrejuizoCotistaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaUsadoCollectionByIdCotista", typeof(PrejuizoCotistaUsadoCollection), new PrejuizoCotistaUsado()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaCotaCollectionByIdCotistaOrigem", typeof(TransferenciaCotaCollection), new TransferenciaCota()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaCotaCollectionByIdCotistaDestino", typeof(TransferenciaCotaCollection), new TransferenciaCota()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaEntreCotistaCollectionByIdCotistaOrigem", typeof(TransferenciaEntreCotistaCollection), new TransferenciaEntreCotista()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaEntreCotistaCollectionByIdCotistaDestino", typeof(TransferenciaEntreCotistaCollection), new TransferenciaEntreCotista()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class CotistaCollection : esCotistaCollection
	{
		#region ManyToManyUsuarioCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyUsuarioCollection(System.Int32? IdUsuario)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario", IdUsuario);
	
			return base.Load( esQueryType.ManyToMany, 
				"Cotista,PermissaoCotista|IdCotista,IdCotista|IdUsuario",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IsentoIR, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIOF
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IsentoIOF, esSystemType.String);
			}
		} 
		
		public esQueryItem StatusAtivo
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.StatusAtivo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCotistaCVM
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.TipoCotistaCVM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoTributacao
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.TipoTributacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataExpiracao
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.DataExpiracao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PendenciaCadastral
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.PendenciaCadastral, esSystemType.String);
			}
		} 
		
		public esQueryItem IdClienteEspelho
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IdClienteEspelho, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OffShore
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.OffShore, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoConsolidacaoExterno
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.CodigoConsolidacaoExterno, esSystemType.String);
			}
		} 

		public esQueryItem InvestidorProfissional
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.InvestidorProfissional, esSystemType.String);
			}
		} 
		
		public esQueryItem InvestidorQualificado
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.InvestidorQualificado, esSystemType.String);
            }
        }

		public esQueryItem TipoCotistaAnbima
		{
			get
			{
				return new esQueryItem(this, CotistaMetadata.ColumnNames.TipoCotistaAnbima, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotistaCollection")]
	public partial class CotistaCollection : esCotistaCollection, IEnumerable<Cotista>
	{
		public CotistaCollection()
		{

		}
		
		public static implicit operator List<Cotista>(CotistaCollection coll)
		{
			List<Cotista> list = new List<Cotista>();
			
			foreach (Cotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Cotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Cotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Cotista AddNew()
		{
			Cotista entity = base.AddNewEntity() as Cotista;
			
			return entity;
		}

		public Cotista FindByPrimaryKey(System.Int32 idCotista)
		{
			return base.FindByPrimaryKey(idCotista) as Cotista;
		}


		#region IEnumerable<Cotista> Members

		IEnumerator<Cotista> IEnumerable<Cotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Cotista;
			}
		}

		#endregion
		
		private CotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Cotista' table
	/// </summary>

	[Serializable]
	public partial class Cotista : esCotista
	{
		public Cotista()
		{

		}
	
		public Cotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotistaQuery query;
	}



	[Serializable]
	public partial class CotistaQuery : esCotistaQuery
	{
		public CotistaQuery()
		{

		}		
		
		public CotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IdCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IsentoIR, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.IsentoIR;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IsentoIOF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.IsentoIOF;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.StatusAtivo, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CotistaMetadata.PropertyNames.StatusAtivo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.TipoCotistaCVM, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.TipoCotistaCVM;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.CodigoInterface, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.TipoTributacao, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CotistaMetadata.PropertyNames.TipoTributacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.DataExpiracao, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotistaMetadata.PropertyNames.DataExpiracao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.PendenciaCadastral, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.PendenciaCadastral;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IdClienteEspelho, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.IdClienteEspelho;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IdPessoa, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.IdPessoa;	
            c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 

			c = new esColumnMetadata(CotistaMetadata.ColumnNames.TipoCotistaAnbima, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.TipoCotistaAnbima;	
            c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
				
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.IdCarteira, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.OffShore, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.OffShore;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.CodigoConsolidacaoExterno, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.CodigoConsolidacaoExterno;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 

			c = new esColumnMetadata(CotistaMetadata.ColumnNames.InvestidorProfissional, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.InvestidorProfissional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
			c = new esColumnMetadata(CotistaMetadata.ColumnNames.InvestidorQualificado, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = CotistaMetadata.PropertyNames.InvestidorQualificado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
		}
		#endregion
	
		static public CotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string TipoCotistaCVM = "TipoCotistaCVM";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string TipoTributacao = "TipoTributacao";
			 public const string DataExpiracao = "DataExpiracao";
			 public const string PendenciaCadastral = "PendenciaCadastral";
			 public const string IdClienteEspelho = "IdClienteEspelho";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdCarteira = "IdCarteira";
			 public const string OffShore = "OffShore";
			 public const string CodigoConsolidacaoExterno = "CodigoConsolidacaoExterno";
			 public const string InvestidorProfissional = "InvestidorProfissional";
			 public const string InvestidorQualificado = "InvestidorQualificado";
			 public const string TipoCotistaAnbima = "TipoCotistaAnbima";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string TipoCotistaCVM = "TipoCotistaCVM";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string TipoTributacao = "TipoTributacao";
			 public const string DataExpiracao = "DataExpiracao";
			 public const string PendenciaCadastral = "PendenciaCadastral";
			 public const string IdClienteEspelho = "IdClienteEspelho";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdCarteira = "IdCarteira";
			 public const string OffShore = "OffShore";
			 public const string CodigoConsolidacaoExterno = "CodigoConsolidacaoExterno";
			 public const string InvestidorProfissional = "InvestidorProfissional";
			 public const string InvestidorQualificado = "InvestidorQualificado";
			 public const string TipoCotistaAnbima = "TipoCotistaAnbima";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotistaMetadata))
			{
				if(CotistaMetadata.mapDelegates == null)
				{
					CotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotistaMetadata.meta == null)
				{
					CotistaMetadata.meta = new CotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IsentoIOF", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("StatusAtivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCotistaCVM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoTributacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataExpiracao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PendenciaCadastral", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdClienteEspelho", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OffShore", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CodigoConsolidacaoExterno", new esTypeMap("varchar", "System.String"));			
				meta.AddTypeMap("InvestidorProfissional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InvestidorQualificado", new esTypeMap("char", "System.String"));			
				meta.AddTypeMap("TipoCotistaAnbima", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Cotista";
				meta.Destination = "Cotista";
				
				meta.spInsert = "proc_CotistaInsert";				
				meta.spUpdate = "proc_CotistaUpdate";		
				meta.spDelete = "proc_CotistaDelete";
				meta.spLoadAll = "proc_CotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
