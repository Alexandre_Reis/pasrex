/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/14/2015 4:14:59 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esPrejuizoCotistaUsadoCollection : esEntityCollection
	{
		public esPrejuizoCotistaUsadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PrejuizoCotistaUsadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPrejuizoCotistaUsadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPrejuizoCotistaUsadoQuery);
		}
		#endregion
		
		virtual public PrejuizoCotistaUsado DetachEntity(PrejuizoCotistaUsado entity)
		{
			return base.DetachEntity(entity) as PrejuizoCotistaUsado;
		}
		
		virtual public PrejuizoCotistaUsado AttachEntity(PrejuizoCotistaUsado entity)
		{
			return base.AttachEntity(entity) as PrejuizoCotistaUsado;
		}
		
		virtual public void Combine(PrejuizoCotistaUsadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PrejuizoCotistaUsado this[int index]
		{
			get
			{
				return base[index] as PrejuizoCotistaUsado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PrejuizoCotistaUsado);
		}
	}



	[Serializable]
	abstract public class esPrejuizoCotistaUsado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPrejuizoCotistaUsadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPrejuizoCotistaUsado()
		{

		}

		public esPrejuizoCotistaUsado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, idCarteiraCompensada, idCotista, dataOriginal, dataCompensacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, idCarteiraCompensada, idCotista, dataOriginal, dataCompensacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPrejuizoCotistaUsadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.IdCarteiraCompensada == idCarteiraCompensada, query.IdCotista == idCotista, query.DataOriginal == dataOriginal, query.DataCompensacao == dataCompensacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, idCarteiraCompensada, idCotista, dataOriginal, dataCompensacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, idCarteiraCompensada, idCotista, dataOriginal, dataCompensacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			esPrejuizoCotistaUsadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.IdCarteiraCompensada == idCarteiraCompensada, query.IdCotista == idCotista, query.DataOriginal == dataOriginal, query.DataCompensacao == dataCompensacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("IdCarteiraCompensada",idCarteiraCompensada);			parms.Add("IdCotista",idCotista);			parms.Add("DataOriginal",dataOriginal);			parms.Add("DataCompensacao",dataCompensacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCarteiraCompensada": this.str.IdCarteiraCompensada = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "DataOriginal": this.str.DataOriginal = (string)value; break;							
						case "DataCompensacao": this.str.DataCompensacao = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCarteiraCompensada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraCompensada = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "DataOriginal":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOriginal = (System.DateTime?)value;
							break;
						
						case "DataCompensacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCompensacao = (System.DateTime?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.IdCarteiraCompensada
		/// </summary>
		virtual public System.Int32? IdCarteiraCompensada
		{
			get
			{
				return base.GetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteiraCompensada);
			}
			
			set
			{
				base.SetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteiraCompensada, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.DataOriginal
		/// </summary>
		virtual public System.DateTime? DataOriginal
		{
			get
			{
				return base.GetSystemDateTime(PrejuizoCotistaUsadoMetadata.ColumnNames.DataOriginal);
			}
			
			set
			{
				base.SetSystemDateTime(PrejuizoCotistaUsadoMetadata.ColumnNames.DataOriginal, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.DataCompensacao
		/// </summary>
		virtual public System.DateTime? DataCompensacao
		{
			get
			{
				return base.GetSystemDateTime(PrejuizoCotistaUsadoMetadata.ColumnNames.DataCompensacao);
			}
			
			set
			{
				base.SetSystemDateTime(PrejuizoCotistaUsadoMetadata.ColumnNames.DataCompensacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaUsado.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(PrejuizoCotistaUsadoMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(PrejuizoCotistaUsadoMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPrejuizoCotistaUsado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraCompensada
			{
				get
				{
					System.Int32? data = entity.IdCarteiraCompensada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraCompensada = null;
					else entity.IdCarteiraCompensada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOriginal
			{
				get
				{
					System.DateTime? data = entity.DataOriginal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOriginal = null;
					else entity.DataOriginal = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataCompensacao
			{
				get
				{
					System.DateTime? data = entity.DataCompensacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCompensacao = null;
					else entity.DataCompensacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
			

			private esPrejuizoCotistaUsado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPrejuizoCotistaUsadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPrejuizoCotistaUsado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PrejuizoCotistaUsado : esPrejuizoCotistaUsado
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PrejuizoCotistaUsado_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PrejuizoCotistaUsado_Cotista
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPrejuizoCotistaUsadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PrejuizoCotistaUsadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraCompensada
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteiraCompensada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOriginal
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.DataOriginal, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataCompensacao
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.DataCompensacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaUsadoMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PrejuizoCotistaUsadoCollection")]
	public partial class PrejuizoCotistaUsadoCollection : esPrejuizoCotistaUsadoCollection, IEnumerable<PrejuizoCotistaUsado>
	{
		public PrejuizoCotistaUsadoCollection()
		{

		}
		
		public static implicit operator List<PrejuizoCotistaUsado>(PrejuizoCotistaUsadoCollection coll)
		{
			List<PrejuizoCotistaUsado> list = new List<PrejuizoCotistaUsado>();
			
			foreach (PrejuizoCotistaUsado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PrejuizoCotistaUsadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrejuizoCotistaUsadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PrejuizoCotistaUsado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PrejuizoCotistaUsado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PrejuizoCotistaUsadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrejuizoCotistaUsadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PrejuizoCotistaUsadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PrejuizoCotistaUsado AddNew()
		{
			PrejuizoCotistaUsado entity = base.AddNewEntity() as PrejuizoCotistaUsado;
			
			return entity;
		}

		public PrejuizoCotistaUsado FindByPrimaryKey(System.Int32 idCarteira, System.Int32 idCarteiraCompensada, System.Int32 idCotista, System.DateTime dataOriginal, System.DateTime dataCompensacao)
		{
			return base.FindByPrimaryKey(idCarteira, idCarteiraCompensada, idCotista, dataOriginal, dataCompensacao) as PrejuizoCotistaUsado;
		}


		#region IEnumerable<PrejuizoCotistaUsado> Members

		IEnumerator<PrejuizoCotistaUsado> IEnumerable<PrejuizoCotistaUsado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PrejuizoCotistaUsado;
			}
		}

		#endregion
		
		private PrejuizoCotistaUsadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PrejuizoCotistaUsado' table
	/// </summary>

	[Serializable]
	public partial class PrejuizoCotistaUsado : esPrejuizoCotistaUsado
	{
		public PrejuizoCotistaUsado()
		{

		}
	
		public PrejuizoCotistaUsado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PrejuizoCotistaUsadoMetadata.Meta();
			}
		}
		
		
		
		override protected esPrejuizoCotistaUsadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrejuizoCotistaUsadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PrejuizoCotistaUsadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrejuizoCotistaUsadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PrejuizoCotistaUsadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PrejuizoCotistaUsadoQuery query;
	}



	[Serializable]
	public partial class PrejuizoCotistaUsadoQuery : esPrejuizoCotistaUsadoQuery
	{
		public PrejuizoCotistaUsadoQuery()
		{

		}		
		
		public PrejuizoCotistaUsadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PrejuizoCotistaUsadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PrejuizoCotistaUsadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteiraCompensada, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.IdCarteiraCompensada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCotista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.DataOriginal, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.DataOriginal;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.DataCompensacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.DataCompensacao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaUsadoMetadata.ColumnNames.PrejuizoUsado, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PrejuizoCotistaUsadoMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PrejuizoCotistaUsadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCarteiraCompensada = "IdCarteiraCompensada";
			 public const string IdCotista = "IdCotista";
			 public const string DataOriginal = "DataOriginal";
			 public const string DataCompensacao = "DataCompensacao";
			 public const string PrejuizoUsado = "PrejuizoUsado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCarteiraCompensada = "IdCarteiraCompensada";
			 public const string IdCotista = "IdCotista";
			 public const string DataOriginal = "DataOriginal";
			 public const string DataCompensacao = "DataCompensacao";
			 public const string PrejuizoUsado = "PrejuizoUsado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PrejuizoCotistaUsadoMetadata))
			{
				if(PrejuizoCotistaUsadoMetadata.mapDelegates == null)
				{
					PrejuizoCotistaUsadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PrejuizoCotistaUsadoMetadata.meta == null)
				{
					PrejuizoCotistaUsadoMetadata.meta = new PrejuizoCotistaUsadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraCompensada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOriginal", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataCompensacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PrejuizoCotistaUsado";
				meta.Destination = "PrejuizoCotistaUsado";
				
				meta.spInsert = "proc_PrejuizoCotistaUsadoInsert";				
				meta.spUpdate = "proc_PrejuizoCotistaUsadoUpdate";		
				meta.spDelete = "proc_PrejuizoCotistaUsadoDelete";
				meta.spLoadAll = "proc_PrejuizoCotistaUsadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PrejuizoCotistaUsadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PrejuizoCotistaUsadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
