/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/03/2016 13:08:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;



namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esPosicaoCotistaAlteradaCollection : esEntityCollection
	{
		public esPosicaoCotistaAlteradaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoCotistaAlteradaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoCotistaAlteradaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoCotistaAlteradaQuery);
		}
		#endregion
		
		virtual public PosicaoCotistaAlterada DetachEntity(PosicaoCotistaAlterada entity)
		{
			return base.DetachEntity(entity) as PosicaoCotistaAlterada;
		}
		
		virtual public PosicaoCotistaAlterada AttachEntity(PosicaoCotistaAlterada entity)
		{
			return base.AttachEntity(entity) as PosicaoCotistaAlterada;
		}
		
		virtual public void Combine(PosicaoCotistaAlteradaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoCotistaAlterada this[int index]
		{
			get
			{
				return base[index] as PosicaoCotistaAlterada;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoCotistaAlterada);
		}
	}



	[Serializable]
	abstract public class esPosicaoCotistaAlterada : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoCotistaAlteradaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoCotistaAlterada()
		{

		}

		public esPosicaoCotistaAlterada(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idPosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idPosicao)
		{
			esPosicaoCotistaAlteradaQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "ValorIOFAntigo": this.str.ValorIOFAntigo = (string)value; break;							
						case "ValorIOFNovo": this.str.ValorIOFNovo = (string)value; break;							
						case "ValorIRAntigo": this.str.ValorIRAntigo = (string)value; break;							
						case "ValorIRNovo": this.str.ValorIRNovo = (string)value; break;							
						case "QuantidadeAntigo": this.str.QuantidadeAntigo = (string)value; break;							
						case "QuantidadeNovo": this.str.QuantidadeNovo = (string)value; break;							
						case "QuantidadeAntesCortesNovo": this.str.QuantidadeAntesCortesNovo = (string)value; break;							
						case "QuantidadeAntesCortesAntigo": this.str.QuantidadeAntesCortesAntigo = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "ValorIOFAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFAntigo = (System.Decimal?)value;
							break;
						
						case "ValorIOFNovo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFNovo = (System.Decimal?)value;
							break;
						
						case "ValorIRAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIRAntigo = (System.Decimal?)value;
							break;
						
						case "ValorIRNovo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIRNovo = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntigo = (System.Decimal?)value;
							break;
						
						case "QuantidadeNovo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeNovo = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortesNovo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortesNovo = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortesAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortesAntigo = (System.Decimal?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAlteradaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAlteradaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAlteradaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoCotistaAlteradaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.ValorIOFAntigo
		/// </summary>
		virtual public System.Decimal? ValorIOFAntigo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.ValorIOFNovo
		/// </summary>
		virtual public System.Decimal? ValorIOFNovo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFNovo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFNovo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.ValorIRAntigo
		/// </summary>
		virtual public System.Decimal? ValorIRAntigo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.ValorIRNovo
		/// </summary>
		virtual public System.Decimal? ValorIRNovo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRNovo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRNovo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.QuantidadeAntigo
		/// </summary>
		virtual public System.Decimal? QuantidadeAntigo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.QuantidadeNovo
		/// </summary>
		virtual public System.Decimal? QuantidadeNovo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeNovo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeNovo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.QuantidadeAntesCortesNovo
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortesNovo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesNovo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesNovo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.QuantidadeAntesCortesAntigo
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortesAntigo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAlterada.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAlteradaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoCotistaAlteradaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoCotistaAlterada entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorIOFAntigo
			{
				get
				{
					System.Decimal? data = entity.ValorIOFAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFAntigo = null;
					else entity.ValorIOFAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFNovo
			{
				get
				{
					System.Decimal? data = entity.ValorIOFNovo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFNovo = null;
					else entity.ValorIOFNovo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIRAntigo
			{
				get
				{
					System.Decimal? data = entity.ValorIRAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIRAntigo = null;
					else entity.ValorIRAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIRNovo
			{
				get
				{
					System.Decimal? data = entity.ValorIRNovo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIRNovo = null;
					else entity.ValorIRNovo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntigo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntigo = null;
					else entity.QuantidadeAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeNovo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeNovo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeNovo = null;
					else entity.QuantidadeNovo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortesNovo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortesNovo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortesNovo = null;
					else entity.QuantidadeAntesCortesNovo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortesAntigo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortesAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortesAntigo = null;
					else entity.QuantidadeAntesCortesAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
			

			private esPosicaoCotistaAlterada entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoCotistaAlteradaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoCotistaAlterada can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoCotistaAlterada : esPosicaoCotistaAlterada
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PosicaoCotistaAlterada_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoCotistaAlteradaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoCotistaAlteradaMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorIOFAntigo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFNovo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFNovo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIRAntigo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIRNovo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRNovo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntigo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeNovo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeNovo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortesNovo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesNovo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortesAntigo
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAlteradaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoCotistaAlteradaCollection")]
	public partial class PosicaoCotistaAlteradaCollection : esPosicaoCotistaAlteradaCollection, IEnumerable<PosicaoCotistaAlterada>
	{
		public PosicaoCotistaAlteradaCollection()
		{

		}
		
		public static implicit operator List<PosicaoCotistaAlterada>(PosicaoCotistaAlteradaCollection coll)
		{
			List<PosicaoCotistaAlterada> list = new List<PosicaoCotistaAlterada>();
			
			foreach (PosicaoCotistaAlterada emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoCotistaAlteradaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoCotistaAlteradaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoCotistaAlterada(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoCotistaAlterada();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoCotistaAlteradaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoCotistaAlteradaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoCotistaAlteradaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoCotistaAlterada AddNew()
		{
			PosicaoCotistaAlterada entity = base.AddNewEntity() as PosicaoCotistaAlterada;
			
			return entity;
		}

		public PosicaoCotistaAlterada FindByPrimaryKey(System.DateTime data, System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(data, idPosicao) as PosicaoCotistaAlterada;
		}


		#region IEnumerable<PosicaoCotistaAlterada> Members

		IEnumerator<PosicaoCotistaAlterada> IEnumerable<PosicaoCotistaAlterada>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoCotistaAlterada;
			}
		}

		#endregion
		
		private PosicaoCotistaAlteradaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoCotistaAlterada' table
	/// </summary>

	[Serializable]
	public partial class PosicaoCotistaAlterada : esPosicaoCotistaAlterada
	{
		public PosicaoCotistaAlterada()
		{

		}
	
		public PosicaoCotistaAlterada(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoCotistaAlteradaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoCotistaAlteradaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoCotistaAlteradaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoCotistaAlteradaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoCotistaAlteradaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoCotistaAlteradaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoCotistaAlteradaQuery query;
	}



	[Serializable]
	public partial class PosicaoCotistaAlteradaQuery : esPosicaoCotistaAlteradaQuery
	{
		public PosicaoCotistaAlteradaQuery()
		{

		}		
		
		public PosicaoCotistaAlteradaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoCotistaAlteradaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoCotistaAlteradaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFAntigo, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.ValorIOFAntigo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIOFNovo, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.ValorIOFNovo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRAntigo, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.ValorIRAntigo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.ValorIRNovo, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.ValorIRNovo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntigo, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.QuantidadeAntigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeNovo, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.QuantidadeNovo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesNovo, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.QuantidadeAntesCortesNovo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.QuantidadeAntesCortesAntigo, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.QuantidadeAntesCortesAntigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAlteradaMetadata.ColumnNames.IdCarteira, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAlteradaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoCotistaAlteradaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdPosicao = "IdPosicao";
			 public const string ValorIOFAntigo = "ValorIOFAntigo";
			 public const string ValorIOFNovo = "ValorIOFNovo";
			 public const string ValorIRAntigo = "ValorIRAntigo";
			 public const string ValorIRNovo = "ValorIRNovo";
			 public const string QuantidadeAntigo = "QuantidadeAntigo";
			 public const string QuantidadeNovo = "QuantidadeNovo";
			 public const string QuantidadeAntesCortesNovo = "QuantidadeAntesCortesNovo";
			 public const string QuantidadeAntesCortesAntigo = "QuantidadeAntesCortesAntigo";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdPosicao = "IdPosicao";
			 public const string ValorIOFAntigo = "ValorIOFAntigo";
			 public const string ValorIOFNovo = "ValorIOFNovo";
			 public const string ValorIRAntigo = "ValorIRAntigo";
			 public const string ValorIRNovo = "ValorIRNovo";
			 public const string QuantidadeAntigo = "QuantidadeAntigo";
			 public const string QuantidadeNovo = "QuantidadeNovo";
			 public const string QuantidadeAntesCortesNovo = "QuantidadeAntesCortesNovo";
			 public const string QuantidadeAntesCortesAntigo = "QuantidadeAntesCortesAntigo";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoCotistaAlteradaMetadata))
			{
				if(PosicaoCotistaAlteradaMetadata.mapDelegates == null)
				{
					PosicaoCotistaAlteradaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoCotistaAlteradaMetadata.meta == null)
				{
					PosicaoCotistaAlteradaMetadata.meta = new PosicaoCotistaAlteradaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorIOFAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFNovo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIRAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIRNovo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeNovo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortesNovo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortesAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PosicaoCotistaAlterada";
				meta.Destination = "PosicaoCotistaAlterada";
				
				meta.spInsert = "proc_PosicaoCotistaAlteradaInsert";				
				meta.spUpdate = "proc_PosicaoCotistaAlteradaUpdate";		
				meta.spDelete = "proc_PosicaoCotistaAlteradaDelete";
				meta.spLoadAll = "proc_PosicaoCotistaAlteradaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoCotistaAlteradaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoCotistaAlteradaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
