/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 17:28:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	







		
using Financial.Fundo;










					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esPosicaoCotistaAberturaCollection : esEntityCollection
	{
		public esPosicaoCotistaAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoCotistaAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoCotistaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoCotistaAberturaQuery);
		}
		#endregion
		
		virtual public PosicaoCotistaAbertura DetachEntity(PosicaoCotistaAbertura entity)
		{
			return base.DetachEntity(entity) as PosicaoCotistaAbertura;
		}
		
		virtual public PosicaoCotistaAbertura AttachEntity(PosicaoCotistaAbertura entity)
		{
			return base.AttachEntity(entity) as PosicaoCotistaAbertura;
		}
		
		virtual public void Combine(PosicaoCotistaAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoCotistaAbertura this[int index]
		{
			get
			{
				return base[index] as PosicaoCotistaAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoCotistaAbertura);
		}
	}



	[Serializable]
	abstract public class esPosicaoCotistaAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoCotistaAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoCotistaAbertura()
		{

		}

		public esPosicaoCotistaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoCotistaAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoCotistaAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAplicacao": this.str.ValorAplicacao = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "CotaAplicacao": this.str.CotaAplicacao = (string)value; break;							
						case "CotaDia": this.str.CotaDia = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataUltimaCobrancaIR": this.str.DataUltimaCobrancaIR = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "ValorIOFVirtual": this.str.ValorIOFVirtual = (string)value; break;							
						case "QuantidadeAntesCortes": this.str.QuantidadeAntesCortes = (string)value; break;							
						case "ValorRendimento": this.str.ValorRendimento = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;							
						case "PosicaoIncorporada": this.str.PosicaoIncorporada = (string)value; break;							
						case "IdSeriesOffShore": this.str.IdSeriesOffShore = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "QtdePendenteLiquidacao": this.str.QtdePendenteLiquidacao = (string)value; break;							
						case "ValorPendenteLiquidacao": this.str.ValorPendenteLiquidacao = (string)value; break;							
						case "AmortizacaoAcumuladaPorCota": this.str.AmortizacaoAcumuladaPorCota = (string)value; break;							
						case "JurosAcumuladoPorCota": this.str.JurosAcumuladoPorCota = (string)value; break;							
						case "AmortizacaoAcumuladaPorValor": this.str.AmortizacaoAcumuladaPorValor = (string)value; break;							
						case "JurosAcumuladoPorValor": this.str.JurosAcumuladoPorValor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAplicacao = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "CotaAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAplicacao = (System.Decimal?)value;
							break;
						
						case "CotaDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaDia = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataUltimaCobrancaIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaCobrancaIR = (System.DateTime?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtual = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortes = (System.Decimal?)value;
							break;
						
						case "ValorRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorRendimento = (System.Decimal?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
						
						case "IdSeriesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSeriesOffShore = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "QtdePendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdePendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "ValorPendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorCota = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorCota = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorValor = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorValor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorAplicacao
		/// </summary>
		virtual public System.Decimal? ValorAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.CotaAplicacao
		/// </summary>
		virtual public System.Decimal? CotaAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.CotaAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.CotaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.CotaDia
		/// </summary>
		virtual public System.Decimal? CotaDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.CotaDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.CotaDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.DataUltimaCobrancaIR
		/// </summary>
		virtual public System.DateTime? DataUltimaCobrancaIR
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimaCobrancaIR);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimaCobrancaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorIOFVirtual
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOFVirtual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.QuantidadeAntesCortes
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortes
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeAntesCortes);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeAntesCortes, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorRendimento
		/// </summary>
		virtual public System.Decimal? ValorRendimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.PosicaoIncorporada
		/// </summary>
		virtual public System.String PosicaoIncorporada
		{
			get
			{
				return base.GetSystemString(PosicaoCotistaAberturaMetadata.ColumnNames.PosicaoIncorporada);
			}
			
			set
			{
				base.SetSystemString(PosicaoCotistaAberturaMetadata.ColumnNames.PosicaoIncorporada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.IdSeriesOffShore
		/// </summary>
		virtual public System.Int32? IdSeriesOffShore
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdSeriesOffShore);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.IdSeriesOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoCotistaAberturaMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.QtdePendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? QtdePendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QtdePendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.QtdePendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.ValorPendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? ValorPendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.AmortizacaoAcumuladaPorCota
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.JurosAcumuladoPorCota
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.AmortizacaoAcumuladaPorValor
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoCotistaAbertura.JurosAcumuladoPorValor
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorValor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoCotistaAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAplicacao = null;
					else entity.ValorAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CotaAplicacao
			{
				get
				{
					System.Decimal? data = entity.CotaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAplicacao = null;
					else entity.CotaAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaDia
			{
				get
				{
					System.Decimal? data = entity.CotaDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaDia = null;
					else entity.CotaDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimaCobrancaIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimaCobrancaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaCobrancaIR = null;
					else entity.DataUltimaCobrancaIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtual
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtual = null;
					else entity.ValorIOFVirtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortes
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortes = null;
					else entity.QuantidadeAntesCortes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorRendimento
			{
				get
				{
					System.Decimal? data = entity.ValorRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorRendimento = null;
					else entity.ValorRendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
				
			public System.String PosicaoIncorporada
			{
				get
				{
					System.String data = entity.PosicaoIncorporada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PosicaoIncorporada = null;
					else entity.PosicaoIncorporada = Convert.ToString(value);
				}
			}
				
			public System.String IdSeriesOffShore
			{
				get
				{
					System.Int32? data = entity.IdSeriesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSeriesOffShore = null;
					else entity.IdSeriesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdePendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.QtdePendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdePendenteLiquidacao = null;
					else entity.QtdePendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.ValorPendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPendenteLiquidacao = null;
					else entity.ValorPendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorCota
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorCota = null;
					else entity.AmortizacaoAcumuladaPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorCota
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorCota = null;
					else entity.JurosAcumuladoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorValor
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorValor = null;
					else entity.AmortizacaoAcumuladaPorValor = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorValor
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorValor = null;
					else entity.JurosAcumuladoPorValor = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoCotistaAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoCotistaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoCotistaAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoCotistaAbertura : esPosicaoCotistaAbertura
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_PosicaoCotistaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_PosicaoCotistaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoCotistaAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoCotistaAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CotaAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.CotaAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaDia
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.CotaDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimaCobrancaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimaCobrancaIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtual
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOFVirtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortes
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeAntesCortes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorRendimento
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorRendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PosicaoIncorporada
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.PosicaoIncorporada, esSystemType.String);
			}
		} 
		
		public esQueryItem IdSeriesOffShore
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.IdSeriesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdePendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.QtdePendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.ValorPendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorValor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoCotistaAberturaCollection")]
	public partial class PosicaoCotistaAberturaCollection : esPosicaoCotistaAberturaCollection, IEnumerable<PosicaoCotistaAbertura>
	{
		public PosicaoCotistaAberturaCollection()
		{

		}
		
		public static implicit operator List<PosicaoCotistaAbertura>(PosicaoCotistaAberturaCollection coll)
		{
			List<PosicaoCotistaAbertura> list = new List<PosicaoCotistaAbertura>();
			
			foreach (PosicaoCotistaAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoCotistaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoCotistaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoCotistaAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoCotistaAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoCotistaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoCotistaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoCotistaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoCotistaAbertura AddNew()
		{
			PosicaoCotistaAbertura entity = base.AddNewEntity() as PosicaoCotistaAbertura;
			
			return entity;
		}

		public PosicaoCotistaAbertura FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoCotistaAbertura;
		}


		#region IEnumerable<PosicaoCotistaAbertura> Members

		IEnumerator<PosicaoCotistaAbertura> IEnumerable<PosicaoCotistaAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoCotistaAbertura;
			}
		}

		#endregion
		
		private PosicaoCotistaAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoCotistaAbertura' table
	/// </summary>

	[Serializable]
	public partial class PosicaoCotistaAbertura : esPosicaoCotistaAbertura
	{
		public PosicaoCotistaAbertura()
		{

		}
	
		public PosicaoCotistaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoCotistaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoCotistaAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoCotistaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoCotistaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoCotistaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoCotistaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoCotistaAberturaQuery query;
	}



	[Serializable]
	public partial class PosicaoCotistaAberturaQuery : esPosicaoCotistaAberturaQuery
	{
		public PosicaoCotistaAberturaQuery()
		{

		}		
		
		public PosicaoCotistaAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoCotistaAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoCotistaAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.IdCotista, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.IdCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorAplicacao, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.DataAplicacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.DataConversao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.CotaAplicacao, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.CotaAplicacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.CotaDia, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.CotaDia;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorBruto, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeInicial, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.Quantidade, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeBloqueada, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimaCobrancaIR, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.DataUltimaCobrancaIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIR, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPerformance, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorIOFVirtual, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorIOFVirtual;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.QuantidadeAntesCortes, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.QuantidadeAntesCortes;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorRendimento, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorRendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.DataUltimoCortePfee, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.PosicaoIncorporada, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.PosicaoIncorporada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.IdSeriesOffShore, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.IdSeriesOffShore;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.FieTabelaIr, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.QtdePendenteLiquidacao, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.QtdePendenteLiquidacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.ValorPendenteLiquidacao, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.ValorPendenteLiquidacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.AmortizacaoAcumuladaPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorCota, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.JurosAcumuladoPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.AmortizacaoAcumuladaPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoCotistaAberturaMetadata.ColumnNames.JurosAcumuladoPorValor, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoCotistaAberturaMetadata.PropertyNames.JurosAcumuladoPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoCotistaAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoCotistaAberturaMetadata))
			{
				if(PosicaoCotistaAberturaMetadata.mapDelegates == null)
				{
					PosicaoCotistaAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoCotistaAberturaMetadata.meta == null)
				{
					PosicaoCotistaAberturaMetadata.meta = new PosicaoCotistaAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CotaAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimaCobrancaIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorRendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PosicaoIncorporada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdSeriesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdePendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorValor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorValor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoCotistaAbertura";
				meta.Destination = "PosicaoCotistaAbertura";
				
				meta.spInsert = "proc_PosicaoCotistaAberturaInsert";				
				meta.spUpdate = "proc_PosicaoCotistaAberturaUpdate";		
				meta.spDelete = "proc_PosicaoCotistaAberturaDelete";
				meta.spLoadAll = "proc_PosicaoCotistaAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoCotistaAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoCotistaAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
