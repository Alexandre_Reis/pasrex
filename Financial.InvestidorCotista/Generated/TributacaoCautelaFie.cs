/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/05/2015 11:28:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esTributacaoCautelaFieCollection : esEntityCollection
	{
		public esTributacaoCautelaFieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TributacaoCautelaFieCollection";
		}

		#region Query Logic
		protected void InitQuery(esTributacaoCautelaFieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTributacaoCautelaFieQuery);
		}
		#endregion
		
		virtual public TributacaoCautelaFie DetachEntity(TributacaoCautelaFie entity)
		{
			return base.DetachEntity(entity) as TributacaoCautelaFie;
		}
		
		virtual public TributacaoCautelaFie AttachEntity(TributacaoCautelaFie entity)
		{
			return base.AttachEntity(entity) as TributacaoCautelaFie;
		}
		
		virtual public void Combine(TributacaoCautelaFieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TributacaoCautelaFie this[int index]
		{
			get
			{
				return base[index] as TributacaoCautelaFie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TributacaoCautelaFie);
		}
	}



	[Serializable]
	abstract public class esTributacaoCautelaFie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTributacaoCautelaFieQuery GetDynamicQuery()
		{
			return null;
		}

		public esTributacaoCautelaFie()
		{

		}

		public esTributacaoCautelaFie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idOperacao)
		{
			esTributacaoCautelaFieQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TributacaoCautelaFie.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TributacaoCautelaFieMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TributacaoCautelaFieMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TributacaoCautelaFie.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(TributacaoCautelaFieMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(TributacaoCautelaFieMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoCotistaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TributacaoCautelaFie.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(TributacaoCautelaFieMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(TributacaoCautelaFieMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected OperacaoCotista _UpToOperacaoCotistaByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTributacaoCautelaFie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
			

			private esTributacaoCautelaFie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTributacaoCautelaFieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTributacaoCautelaFie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TributacaoCautelaFie : esTributacaoCautelaFie
	{

				
		#region UpToOperacaoCotistaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TributacaoCautelaFie_OperacaoCotista_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoCotista UpToOperacaoCotistaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoCotistaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoCotistaByIdOperacao = new OperacaoCotista();
					this._UpToOperacaoCotistaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoCotistaByIdOperacao", this._UpToOperacaoCotistaByIdOperacao);
					this._UpToOperacaoCotistaByIdOperacao.Query.Where(this._UpToOperacaoCotistaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoCotistaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoCotistaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoCotistaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoCotistaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoCotistaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoCotistaByIdOperacao", this._UpToOperacaoCotistaByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoCotistaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoCotistaByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTributacaoCautelaFieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TributacaoCautelaFieMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TributacaoCautelaFieMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, TributacaoCautelaFieMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, TributacaoCautelaFieMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TributacaoCautelaFieCollection")]
	public partial class TributacaoCautelaFieCollection : esTributacaoCautelaFieCollection, IEnumerable<TributacaoCautelaFie>
	{
		public TributacaoCautelaFieCollection()
		{

		}
		
		public static implicit operator List<TributacaoCautelaFie>(TributacaoCautelaFieCollection coll)
		{
			List<TributacaoCautelaFie> list = new List<TributacaoCautelaFie>();
			
			foreach (TributacaoCautelaFie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TributacaoCautelaFieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TributacaoCautelaFieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TributacaoCautelaFie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TributacaoCautelaFie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TributacaoCautelaFieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TributacaoCautelaFieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TributacaoCautelaFieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TributacaoCautelaFie AddNew()
		{
			TributacaoCautelaFie entity = base.AddNewEntity() as TributacaoCautelaFie;
			
			return entity;
		}

		public TributacaoCautelaFie FindByPrimaryKey(System.DateTime data, System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(data, idOperacao) as TributacaoCautelaFie;
		}


		#region IEnumerable<TributacaoCautelaFie> Members

		IEnumerator<TributacaoCautelaFie> IEnumerable<TributacaoCautelaFie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TributacaoCautelaFie;
			}
		}

		#endregion
		
		private TributacaoCautelaFieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TributacaoCautelaFie' table
	/// </summary>

	[Serializable]
	public partial class TributacaoCautelaFie : esTributacaoCautelaFie
	{
		public TributacaoCautelaFie()
		{

		}
	
		public TributacaoCautelaFie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TributacaoCautelaFieMetadata.Meta();
			}
		}
		
		
		
		override protected esTributacaoCautelaFieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TributacaoCautelaFieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TributacaoCautelaFieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TributacaoCautelaFieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TributacaoCautelaFieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TributacaoCautelaFieQuery query;
	}



	[Serializable]
	public partial class TributacaoCautelaFieQuery : esTributacaoCautelaFieQuery
	{
		public TributacaoCautelaFieQuery()
		{

		}		
		
		public TributacaoCautelaFieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TributacaoCautelaFieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TributacaoCautelaFieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TributacaoCautelaFieMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TributacaoCautelaFieMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TributacaoCautelaFieMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TributacaoCautelaFieMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TributacaoCautelaFieMetadata.ColumnNames.FieTabelaIr, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TributacaoCautelaFieMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TributacaoCautelaFieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdOperacao = "IdOperacao";
			 public const string FieTabelaIr = "FieTabelaIr";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdOperacao = "IdOperacao";
			 public const string FieTabelaIr = "FieTabelaIr";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TributacaoCautelaFieMetadata))
			{
				if(TributacaoCautelaFieMetadata.mapDelegates == null)
				{
					TributacaoCautelaFieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TributacaoCautelaFieMetadata.meta == null)
				{
					TributacaoCautelaFieMetadata.meta = new TributacaoCautelaFieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TributacaoCautelaFie";
				meta.Destination = "TributacaoCautelaFie";
				
				meta.spInsert = "proc_TributacaoCautelaFieInsert";				
				meta.spUpdate = "proc_TributacaoCautelaFieUpdate";		
				meta.spDelete = "proc_TributacaoCautelaFieDelete";
				meta.spLoadAll = "proc_TributacaoCautelaFieLoadAll";
				meta.spLoadByPrimaryKey = "proc_TributacaoCautelaFieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TributacaoCautelaFieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
