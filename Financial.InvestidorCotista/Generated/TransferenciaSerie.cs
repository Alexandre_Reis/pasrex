/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/03/2015 11:33:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esTransferenciaSerieCollection : esEntityCollection
	{
		public esTransferenciaSerieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TransferenciaSerieCollection";
		}

		#region Query Logic
		protected void InitQuery(esTransferenciaSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTransferenciaSerieQuery);
		}
		#endregion
		
		virtual public TransferenciaSerie DetachEntity(TransferenciaSerie entity)
		{
			return base.DetachEntity(entity) as TransferenciaSerie;
		}
		
		virtual public TransferenciaSerie AttachEntity(TransferenciaSerie entity)
		{
			return base.AttachEntity(entity) as TransferenciaSerie;
		}
		
		virtual public void Combine(TransferenciaSerieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TransferenciaSerie this[int index]
		{
			get
			{
				return base[index] as TransferenciaSerie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TransferenciaSerie);
		}
	}



	[Serializable]
	abstract public class esTransferenciaSerie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTransferenciaSerieQuery GetDynamicQuery()
		{
			return null;
		}

		public esTransferenciaSerie()
		{

		}

		public esTransferenciaSerie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTransferenciaSerie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferenciaSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferenciaSerie);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTransferenciaSerie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferenciaSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferenciaSerie);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTransferenciaSerie)
		{
			esTransferenciaSerieQuery query = this.GetDynamicQuery();
			query.Where(query.IdTransferenciaSerie == idTransferenciaSerie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTransferenciaSerie)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTransferenciaSerie",idTransferenciaSerie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTransferenciaSerie": this.str.IdTransferenciaSerie = (string)value; break;							
						case "DataExecucao": this.str.DataExecucao = (string)value; break;							
						case "DataPosicao": this.str.DataPosicao = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdSerieOrigem": this.str.IdSerieOrigem = (string)value; break;							
						case "IdSerieDestino": this.str.IdSerieDestino = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdEventoRollUp": this.str.IdEventoRollUp = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTransferenciaSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferenciaSerie = (System.Int32?)value;
							break;
						
						case "DataExecucao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataExecucao = (System.DateTime?)value;
							break;
						
						case "DataPosicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPosicao = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdSerieOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerieOrigem = (System.Int32?)value;
							break;
						
						case "IdSerieDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerieDestino = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdEventoRollUp":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoRollUp = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdTransferenciaSerie
		/// </summary>
		virtual public System.Int32? IdTransferenciaSerie
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.DataExecucao
		/// </summary>
		virtual public System.DateTime? DataExecucao
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaSerieMetadata.ColumnNames.DataExecucao);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaSerieMetadata.ColumnNames.DataExecucao, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.DataPosicao
		/// </summary>
		virtual public System.DateTime? DataPosicao
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaSerieMetadata.ColumnNames.DataPosicao);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaSerieMetadata.ColumnNames.DataPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdSerieOrigem
		/// </summary>
		virtual public System.Int32? IdSerieOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem, value))
				{
					this._UpToSeriesOffShoreByIdSerieOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdSerieDestino
		/// </summary>
		virtual public System.Int32? IdSerieDestino
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdSerieDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdSerieDestino, value))
				{
					this._UpToSeriesOffShoreByIdSerieDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaSerie.IdEventoRollUp
		/// </summary>
		virtual public System.Int32? IdEventoRollUp
		{
			get
			{
				return base.GetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdEventoRollUp);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaSerieMetadata.ColumnNames.IdEventoRollUp, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SeriesOffShore _UpToSeriesOffShoreByIdSerieOrigem;
		[CLSCompliant(false)]
		internal protected SeriesOffShore _UpToSeriesOffShoreByIdSerieDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTransferenciaSerie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTransferenciaSerie
			{
				get
				{
					System.Int32? data = entity.IdTransferenciaSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferenciaSerie = null;
					else entity.IdTransferenciaSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String DataExecucao
			{
				get
				{
					System.DateTime? data = entity.DataExecucao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataExecucao = null;
					else entity.DataExecucao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPosicao
			{
				get
				{
					System.DateTime? data = entity.DataPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPosicao = null;
					else entity.DataPosicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerieOrigem
			{
				get
				{
					System.Int32? data = entity.IdSerieOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerieOrigem = null;
					else entity.IdSerieOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerieDestino
			{
				get
				{
					System.Int32? data = entity.IdSerieDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerieDestino = null;
					else entity.IdSerieDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoRollUp
			{
				get
				{
					System.Int32? data = entity.IdEventoRollUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoRollUp = null;
					else entity.IdEventoRollUp = Convert.ToInt32(value);
				}
			}
			

			private esTransferenciaSerie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTransferenciaSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTransferenciaSerie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TransferenciaSerie : esTransferenciaSerie
	{

				
		#region UpToSeriesOffShoreByIdSerieOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransSerie_SerieDest_FK
		/// </summary>

		[XmlIgnore]
		public SeriesOffShore UpToSeriesOffShoreByIdSerieOrigem
		{
			get
			{
				if(this._UpToSeriesOffShoreByIdSerieOrigem == null
					&& IdSerieOrigem != null					)
				{
					this._UpToSeriesOffShoreByIdSerieOrigem = new SeriesOffShore();
					this._UpToSeriesOffShoreByIdSerieOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieOrigem", this._UpToSeriesOffShoreByIdSerieOrigem);
					this._UpToSeriesOffShoreByIdSerieOrigem.Query.Where(this._UpToSeriesOffShoreByIdSerieOrigem.Query.IdSeriesOffShore == this.IdSerieOrigem);
					this._UpToSeriesOffShoreByIdSerieOrigem.Query.Load();
				}

				return this._UpToSeriesOffShoreByIdSerieOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToSeriesOffShoreByIdSerieOrigem");
				

				if(value == null)
				{
					this.IdSerieOrigem = null;
					this._UpToSeriesOffShoreByIdSerieOrigem = null;
				}
				else
				{
					this.IdSerieOrigem = value.IdSeriesOffShore;
					this._UpToSeriesOffShoreByIdSerieOrigem = value;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieOrigem", this._UpToSeriesOffShoreByIdSerieOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSeriesOffShoreByIdSerieDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransSerie_SerieOrig_FK
		/// </summary>

		[XmlIgnore]
		public SeriesOffShore UpToSeriesOffShoreByIdSerieDestino
		{
			get
			{
				if(this._UpToSeriesOffShoreByIdSerieDestino == null
					&& IdSerieDestino != null					)
				{
					this._UpToSeriesOffShoreByIdSerieDestino = new SeriesOffShore();
					this._UpToSeriesOffShoreByIdSerieDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieDestino", this._UpToSeriesOffShoreByIdSerieDestino);
					this._UpToSeriesOffShoreByIdSerieDestino.Query.Where(this._UpToSeriesOffShoreByIdSerieDestino.Query.IdSeriesOffShore == this.IdSerieDestino);
					this._UpToSeriesOffShoreByIdSerieDestino.Query.Load();
				}

				return this._UpToSeriesOffShoreByIdSerieDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToSeriesOffShoreByIdSerieDestino");
				

				if(value == null)
				{
					this.IdSerieDestino = null;
					this._UpToSeriesOffShoreByIdSerieDestino = null;
				}
				else
				{
					this.IdSerieDestino = value.IdSeriesOffShore;
					this._UpToSeriesOffShoreByIdSerieDestino = value;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieDestino", this._UpToSeriesOffShoreByIdSerieDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSeriesOffShoreByIdSerieOrigem != null)
			{
				this.IdSerieOrigem = this._UpToSeriesOffShoreByIdSerieOrigem.IdSeriesOffShore;
			}
			if(!this.es.IsDeleted && this._UpToSeriesOffShoreByIdSerieDestino != null)
			{
				this.IdSerieDestino = this._UpToSeriesOffShoreByIdSerieDestino.IdSeriesOffShore;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTransferenciaSerieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaSerieMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTransferenciaSerie
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataExecucao
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.DataExecucao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPosicao
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.DataPosicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerieOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerieDestino
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdSerieDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoRollUp
		{
			get
			{
				return new esQueryItem(this, TransferenciaSerieMetadata.ColumnNames.IdEventoRollUp, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TransferenciaSerieCollection")]
	public partial class TransferenciaSerieCollection : esTransferenciaSerieCollection, IEnumerable<TransferenciaSerie>
	{
		public TransferenciaSerieCollection()
		{

		}
		
		public static implicit operator List<TransferenciaSerie>(TransferenciaSerieCollection coll)
		{
			List<TransferenciaSerie> list = new List<TransferenciaSerie>();
			
			foreach (TransferenciaSerie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TransferenciaSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TransferenciaSerie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TransferenciaSerie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TransferenciaSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TransferenciaSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TransferenciaSerie AddNew()
		{
			TransferenciaSerie entity = base.AddNewEntity() as TransferenciaSerie;
			
			return entity;
		}

		public TransferenciaSerie FindByPrimaryKey(System.Int32 idTransferenciaSerie)
		{
			return base.FindByPrimaryKey(idTransferenciaSerie) as TransferenciaSerie;
		}


		#region IEnumerable<TransferenciaSerie> Members

		IEnumerator<TransferenciaSerie> IEnumerable<TransferenciaSerie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TransferenciaSerie;
			}
		}

		#endregion
		
		private TransferenciaSerieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TransferenciaSerie' table
	/// </summary>

	[Serializable]
	public partial class TransferenciaSerie : esTransferenciaSerie
	{
		public TransferenciaSerie()
		{

		}
	
		public TransferenciaSerie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esTransferenciaSerieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TransferenciaSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TransferenciaSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TransferenciaSerieQuery query;
	}



	[Serializable]
	public partial class TransferenciaSerieQuery : esTransferenciaSerieQuery
	{
		public TransferenciaSerieQuery()
		{

		}		
		
		public TransferenciaSerieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TransferenciaSerieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TransferenciaSerieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdTransferenciaSerie;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.DataExecucao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.DataExecucao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.DataPosicao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.DataPosicao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdSerieOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdSerieDestino, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdSerieDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdPosicao, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaSerieMetadata.ColumnNames.IdEventoRollUp, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaSerieMetadata.PropertyNames.IdEventoRollUp;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TransferenciaSerieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string DataExecucao = "DataExecucao";
			 public const string DataPosicao = "DataPosicao";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdSerieOrigem = "IdSerieOrigem";
			 public const string IdSerieDestino = "IdSerieDestino";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdEventoRollUp = "IdEventoRollUp";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string DataExecucao = "DataExecucao";
			 public const string DataPosicao = "DataPosicao";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdSerieOrigem = "IdSerieOrigem";
			 public const string IdSerieDestino = "IdSerieDestino";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdEventoRollUp = "IdEventoRollUp";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TransferenciaSerieMetadata))
			{
				if(TransferenciaSerieMetadata.mapDelegates == null)
				{
					TransferenciaSerieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TransferenciaSerieMetadata.meta == null)
				{
					TransferenciaSerieMetadata.meta = new TransferenciaSerieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTransferenciaSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataExecucao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPosicao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerieOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerieDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoRollUp", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TransferenciaSerie";
				meta.Destination = "TransferenciaSerie";
				
				meta.spInsert = "proc_TransferenciaSerieInsert";				
				meta.spUpdate = "proc_TransferenciaSerieUpdate";		
				meta.spDelete = "proc_TransferenciaSerieDelete";
				meta.spLoadAll = "proc_TransferenciaSerieLoadAll";
				meta.spLoadByPrimaryKey = "proc_TransferenciaSerieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TransferenciaSerieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
