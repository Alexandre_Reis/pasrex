/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/01/2015 15:05:53
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esDetalheResgateCotistaAuxCollection : esEntityCollection
	{
		public esDetalheResgateCotistaAuxCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DetalheResgateCotistaAuxCollection";
		}

		#region Query Logic
		protected void InitQuery(esDetalheResgateCotistaAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDetalheResgateCotistaAuxQuery);
		}
		#endregion
		
		virtual public DetalheResgateCotistaAux DetachEntity(DetalheResgateCotistaAux entity)
		{
			return base.DetachEntity(entity) as DetalheResgateCotistaAux;
		}
		
		virtual public DetalheResgateCotistaAux AttachEntity(DetalheResgateCotistaAux entity)
		{
			return base.AttachEntity(entity) as DetalheResgateCotistaAux;
		}
		
		virtual public void Combine(DetalheResgateCotistaAuxCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DetalheResgateCotistaAux this[int index]
		{
			get
			{
				return base[index] as DetalheResgateCotistaAux;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DetalheResgateCotistaAux);
		}
	}



	[Serializable]
	abstract public class esDetalheResgateCotistaAux : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDetalheResgateCotistaAuxQuery GetDynamicQuery()
		{
			return null;
		}

		public esDetalheResgateCotistaAux()
		{

		}

		public esDetalheResgateCotistaAux(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esDetalheResgateCotistaAuxQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao, query.IdPosicaoResgatada == idPosicaoResgatada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);			parms.Add("IdPosicaoResgatada",idPosicaoResgatada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(DetalheResgateCotistaAuxMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(DetalheResgateCotistaAuxMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateCotistaAux.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(DetalheResgateCotistaAuxMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(DetalheResgateCotistaAuxMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDetalheResgateCotistaAux entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
			

			private esDetalheResgateCotistaAux entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDetalheResgateCotistaAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDetalheResgateCotistaAux can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DetalheResgateCotistaAux : esDetalheResgateCotistaAux
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDetalheResgateCotistaAuxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateCotistaAuxMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateCotistaAuxMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DetalheResgateCotistaAuxCollection")]
	public partial class DetalheResgateCotistaAuxCollection : esDetalheResgateCotistaAuxCollection, IEnumerable<DetalheResgateCotistaAux>
	{
		public DetalheResgateCotistaAuxCollection()
		{

		}
		
		public static implicit operator List<DetalheResgateCotistaAux>(DetalheResgateCotistaAuxCollection coll)
		{
			List<DetalheResgateCotistaAux> list = new List<DetalheResgateCotistaAux>();
			
			foreach (DetalheResgateCotistaAux emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DetalheResgateCotistaAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateCotistaAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DetalheResgateCotistaAux(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DetalheResgateCotistaAux();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DetalheResgateCotistaAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateCotistaAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DetalheResgateCotistaAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DetalheResgateCotistaAux AddNew()
		{
			DetalheResgateCotistaAux entity = base.AddNewEntity() as DetalheResgateCotistaAux;
			
			return entity;
		}

		public DetalheResgateCotistaAux FindByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			return base.FindByPrimaryKey(idOperacao, idPosicaoResgatada) as DetalheResgateCotistaAux;
		}


		#region IEnumerable<DetalheResgateCotistaAux> Members

		IEnumerator<DetalheResgateCotistaAux> IEnumerable<DetalheResgateCotistaAux>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DetalheResgateCotistaAux;
			}
		}

		#endregion
		
		private DetalheResgateCotistaAuxQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DetalheResgateCotistaAux' table
	/// </summary>

	[Serializable]
	public partial class DetalheResgateCotistaAux : esDetalheResgateCotistaAux
	{
		public DetalheResgateCotistaAux()
		{

		}
	
		public DetalheResgateCotistaAux(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateCotistaAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDetalheResgateCotistaAuxQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateCotistaAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DetalheResgateCotistaAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateCotistaAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DetalheResgateCotistaAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DetalheResgateCotistaAuxQuery query;
	}



	[Serializable]
	public partial class DetalheResgateCotistaAuxQuery : esDetalheResgateCotistaAuxQuery
	{
		public DetalheResgateCotistaAuxQuery()
		{

		}		
		
		public DetalheResgateCotistaAuxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DetalheResgateCotistaAuxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DetalheResgateCotistaAuxMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.IdPosicaoResgatada, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.IdPosicaoResgatada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCotista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.Quantidade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorBruto, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorLiquido, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorCPMF, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorPerformance, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.PrejuizoUsado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.RendimentoResgate, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.VariacaoResgate, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIR, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.ValorIOF, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.DataOperacao, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateCotistaAuxMetadata.ColumnNames.TipoOperacao, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DetalheResgateCotistaAuxMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DetalheResgateCotistaAuxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DetalheResgateCotistaAuxMetadata))
			{
				if(DetalheResgateCotistaAuxMetadata.mapDelegates == null)
				{
					DetalheResgateCotistaAuxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DetalheResgateCotistaAuxMetadata.meta == null)
				{
					DetalheResgateCotistaAuxMetadata.meta = new DetalheResgateCotistaAuxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "DetalheResgateCotistaAux";
				meta.Destination = "DetalheResgateCotistaAux";
				
				meta.spInsert = "proc_DetalheResgateCotistaAuxInsert";				
				meta.spUpdate = "proc_DetalheResgateCotistaAuxUpdate";		
				meta.spDelete = "proc_DetalheResgateCotistaAuxDelete";
				meta.spLoadAll = "proc_DetalheResgateCotistaAuxLoadAll";
				meta.spLoadByPrimaryKey = "proc_DetalheResgateCotistaAuxLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DetalheResgateCotistaAuxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
