/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/08/2015 16:51:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esComplementoProventosCotistaCollection : esEntityCollection
	{
		public esComplementoProventosCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ComplementoProventosCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esComplementoProventosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esComplementoProventosCotistaQuery);
		}
		#endregion
		
		virtual public ComplementoProventosCotista DetachEntity(ComplementoProventosCotista entity)
		{
			return base.DetachEntity(entity) as ComplementoProventosCotista;
		}
		
		virtual public ComplementoProventosCotista AttachEntity(ComplementoProventosCotista entity)
		{
			return base.AttachEntity(entity) as ComplementoProventosCotista;
		}
		
		virtual public void Combine(ComplementoProventosCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ComplementoProventosCotista this[int index]
		{
			get
			{
				return base[index] as ComplementoProventosCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ComplementoProventosCotista);
		}
	}



	[Serializable]
	abstract public class esComplementoProventosCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esComplementoProventosCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esComplementoProventosCotista()
		{

		}

		public esComplementoProventosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esComplementoProventosCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdOperacaoReferencia": this.str.IdOperacaoReferencia = (string)value; break;							
						case "Rendimento": this.str.Rendimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdOperacaoReferencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoReferencia = (System.Int32?)value;
							break;
						
						case "Rendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rendimento = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ComplementoProventosCotista.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ComplementoProventosCotista.IdOperacaoReferencia
		/// </summary>
		virtual public System.Int32? IdOperacaoReferencia
		{
			get
			{
				return base.GetSystemInt32(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacaoReferencia);
			}
			
			set
			{
				base.SetSystemInt32(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacaoReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ComplementoProventosCotista.Rendimento
		/// </summary>
		virtual public System.Decimal? Rendimento
		{
			get
			{
				return base.GetSystemDecimal(ComplementoProventosCotistaMetadata.ColumnNames.Rendimento);
			}
			
			set
			{
				base.SetSystemDecimal(ComplementoProventosCotistaMetadata.ColumnNames.Rendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to ComplementoProventosCotista.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ComplementoProventosCotistaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ComplementoProventosCotistaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ComplementoProventosCotista.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(ComplementoProventosCotistaMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(ComplementoProventosCotistaMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esComplementoProventosCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoReferencia
			{
				get
				{
					System.Int32? data = entity.IdOperacaoReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoReferencia = null;
					else entity.IdOperacaoReferencia = Convert.ToInt32(value);
				}
			}
				
			public System.String Rendimento
			{
				get
				{
					System.Decimal? data = entity.Rendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rendimento = null;
					else entity.Rendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
			

			private esComplementoProventosCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esComplementoProventosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esComplementoProventosCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ComplementoProventosCotista : esComplementoProventosCotista
	{

		#region UpToOperacaoCotista - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - ComplementoProventosCotista_OperacaoCotista_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoCotista UpToOperacaoCotista
		{
			get
			{
				if(this._UpToOperacaoCotista == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoCotista = new OperacaoCotista();
					this._UpToOperacaoCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoCotista", this._UpToOperacaoCotista);
					this._UpToOperacaoCotista.Query.Where(this._UpToOperacaoCotista.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoCotista.Query.Load();
				}

				return this._UpToOperacaoCotista;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToOperacaoCotista");

				if(value == null)
				{
					this._UpToOperacaoCotista = null;
				}
				else
				{
					this._UpToOperacaoCotista = value;
					this.SetPreSave("UpToOperacaoCotista", this._UpToOperacaoCotista);
				}
				
				
			} 
		}

		private OperacaoCotista _UpToOperacaoCotista;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoCotista != null)
			{
				this.IdOperacao = this._UpToOperacaoCotista.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esComplementoProventosCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ComplementoProventosCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, ComplementoProventosCotistaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoReferencia
		{
			get
			{
				return new esQueryItem(this, ComplementoProventosCotistaMetadata.ColumnNames.IdOperacaoReferencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Rendimento
		{
			get
			{
				return new esQueryItem(this, ComplementoProventosCotistaMetadata.ColumnNames.Rendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ComplementoProventosCotistaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, ComplementoProventosCotistaMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ComplementoProventosCotistaCollection")]
	public partial class ComplementoProventosCotistaCollection : esComplementoProventosCotistaCollection, IEnumerable<ComplementoProventosCotista>
	{
		public ComplementoProventosCotistaCollection()
		{

		}
		
		public static implicit operator List<ComplementoProventosCotista>(ComplementoProventosCotistaCollection coll)
		{
			List<ComplementoProventosCotista> list = new List<ComplementoProventosCotista>();
			
			foreach (ComplementoProventosCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ComplementoProventosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ComplementoProventosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ComplementoProventosCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ComplementoProventosCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ComplementoProventosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ComplementoProventosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ComplementoProventosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ComplementoProventosCotista AddNew()
		{
			ComplementoProventosCotista entity = base.AddNewEntity() as ComplementoProventosCotista;
			
			return entity;
		}

		public ComplementoProventosCotista FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as ComplementoProventosCotista;
		}


		#region IEnumerable<ComplementoProventosCotista> Members

		IEnumerator<ComplementoProventosCotista> IEnumerable<ComplementoProventosCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ComplementoProventosCotista;
			}
		}

		#endregion
		
		private ComplementoProventosCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ComplementoProventosCotista' table
	/// </summary>

	[Serializable]
	public partial class ComplementoProventosCotista : esComplementoProventosCotista
	{
		public ComplementoProventosCotista()
		{

		}
	
		public ComplementoProventosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ComplementoProventosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esComplementoProventosCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ComplementoProventosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ComplementoProventosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ComplementoProventosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ComplementoProventosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ComplementoProventosCotistaQuery query;
	}



	[Serializable]
	public partial class ComplementoProventosCotistaQuery : esComplementoProventosCotistaQuery
	{
		public ComplementoProventosCotistaQuery()
		{

		}		
		
		public ComplementoProventosCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ComplementoProventosCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ComplementoProventosCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ComplementoProventosCotistaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ComplementoProventosCotistaMetadata.ColumnNames.IdOperacaoReferencia, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ComplementoProventosCotistaMetadata.PropertyNames.IdOperacaoReferencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ComplementoProventosCotistaMetadata.ColumnNames.Rendimento, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ComplementoProventosCotistaMetadata.PropertyNames.Rendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ComplementoProventosCotistaMetadata.ColumnNames.Quantidade, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ComplementoProventosCotistaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ComplementoProventosCotistaMetadata.ColumnNames.DataAplicacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ComplementoProventosCotistaMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ComplementoProventosCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdOperacaoReferencia = "IdOperacaoReferencia";
			 public const string Rendimento = "Rendimento";
			 public const string Quantidade = "Quantidade";
			 public const string DataAplicacao = "DataAplicacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdOperacaoReferencia = "IdOperacaoReferencia";
			 public const string Rendimento = "Rendimento";
			 public const string Quantidade = "Quantidade";
			 public const string DataAplicacao = "DataAplicacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ComplementoProventosCotistaMetadata))
			{
				if(ComplementoProventosCotistaMetadata.mapDelegates == null)
				{
					ComplementoProventosCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ComplementoProventosCotistaMetadata.meta == null)
				{
					ComplementoProventosCotistaMetadata.meta = new ComplementoProventosCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoReferencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Rendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "ComplementoProventosCotista";
				meta.Destination = "ComplementoProventosCotista";
				
				meta.spInsert = "proc_ComplementoProventosCotistaInsert";				
				meta.spUpdate = "proc_ComplementoProventosCotistaUpdate";		
				meta.spDelete = "proc_ComplementoProventosCotistaDelete";
				meta.spLoadAll = "proc_ComplementoProventosCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ComplementoProventosCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ComplementoProventosCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
