/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/01/2015 11:40:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esColagemOperResgPosCotistaCollection : esEntityCollection
	{
		public esColagemOperResgPosCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemOperResgPosCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemOperResgPosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemOperResgPosCotistaQuery);
		}
		#endregion
		
		virtual public ColagemOperResgPosCotista DetachEntity(ColagemOperResgPosCotista entity)
		{
			return base.DetachEntity(entity) as ColagemOperResgPosCotista;
		}
		
		virtual public ColagemOperResgPosCotista AttachEntity(ColagemOperResgPosCotista entity)
		{
			return base.AttachEntity(entity) as ColagemOperResgPosCotista;
		}
		
		virtual public void Combine(ColagemOperResgPosCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemOperResgPosCotista this[int index]
		{
			get
			{
				return base[index] as ColagemOperResgPosCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemOperResgPosCotista);
		}
	}



	[Serializable]
	abstract public class esColagemOperResgPosCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemOperResgPosCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemOperResgPosCotista()
		{

		}

		public esColagemOperResgPosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemOperResgPosCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemOperResgPosCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemOperResgPosCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemOperResgPosCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemOperResgPosCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemOperResgPosCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemOperResgPosCotista)
		{
			esColagemOperResgPosCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemOperResgPosCotista == idColagemOperResgPosCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemOperResgPosCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemOperResgPosCotista",idColagemOperResgPosCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemOperResgPosCotista": this.str.IdColagemOperResgPosCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;							
						case "IdOperacaoResgateVinculada": this.str.IdOperacaoResgateVinculada = (string)value; break;							
						case "IdPosicaoVinculada": this.str.IdPosicaoVinculada = (string)value; break;							
						case "QuantidadeVinculada": this.str.QuantidadeVinculada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemOperResgPosCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemOperResgPosCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
						
						case "IdOperacaoResgateVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgateVinculada = (System.Int32?)value;
							break;
						
						case "IdPosicaoVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoVinculada = (System.Int32?)value;
							break;
						
						case "QuantidadeVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeVinculada = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdColagemOperResgPosCotista
		/// </summary>
		virtual public System.Int32? IdColagemOperResgPosCotista
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemOperResgPosCotista);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemOperResgPosCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemOperResgPosCotistaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemOperResgPosCotistaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdOperacaoResgateVinculada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgateVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.IdPosicaoVinculada
		/// </summary>
		virtual public System.Int32? IdPosicaoVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdPosicaoVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosCotista.QuantidadeVinculada
		/// </summary>
		virtual public System.Decimal? QuantidadeVinculada
		{
			get
			{
				return base.GetSystemDecimal(ColagemOperResgPosCotistaMetadata.ColumnNames.QuantidadeVinculada);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemOperResgPosCotistaMetadata.ColumnNames.QuantidadeVinculada, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemOperResgPosCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemOperResgPosCotista
			{
				get
				{
					System.Int32? data = entity.IdColagemOperResgPosCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemOperResgPosCotista = null;
					else entity.IdColagemOperResgPosCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoResgateVinculada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgateVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgateVinculada = null;
					else entity.IdOperacaoResgateVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoVinculada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoVinculada = null;
					else entity.IdPosicaoVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeVinculada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeVinculada = null;
					else entity.QuantidadeVinculada = Convert.ToDecimal(value);
				}
			}
			

			private esColagemOperResgPosCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemOperResgPosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemOperResgPosCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemOperResgPosCotista : esColagemOperResgPosCotista
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemOperResgPosCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemOperResgPosCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemOperResgPosCotista
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemOperResgPosCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoResgateVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosCotistaMetadata.ColumnNames.QuantidadeVinculada, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemOperResgPosCotistaCollection")]
	public partial class ColagemOperResgPosCotistaCollection : esColagemOperResgPosCotistaCollection, IEnumerable<ColagemOperResgPosCotista>
	{
		public ColagemOperResgPosCotistaCollection()
		{

		}
		
		public static implicit operator List<ColagemOperResgPosCotista>(ColagemOperResgPosCotistaCollection coll)
		{
			List<ColagemOperResgPosCotista> list = new List<ColagemOperResgPosCotista>();
			
			foreach (ColagemOperResgPosCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemOperResgPosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemOperResgPosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemOperResgPosCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemOperResgPosCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemOperResgPosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemOperResgPosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemOperResgPosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemOperResgPosCotista AddNew()
		{
			ColagemOperResgPosCotista entity = base.AddNewEntity() as ColagemOperResgPosCotista;
			
			return entity;
		}

		public ColagemOperResgPosCotista FindByPrimaryKey(System.Int32 idColagemOperResgPosCotista)
		{
			return base.FindByPrimaryKey(idColagemOperResgPosCotista) as ColagemOperResgPosCotista;
		}


		#region IEnumerable<ColagemOperResgPosCotista> Members

		IEnumerator<ColagemOperResgPosCotista> IEnumerable<ColagemOperResgPosCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemOperResgPosCotista;
			}
		}

		#endregion
		
		private ColagemOperResgPosCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemOperResgPosCotista' table
	/// </summary>

	[Serializable]
	public partial class ColagemOperResgPosCotista : esColagemOperResgPosCotista
	{
		public ColagemOperResgPosCotista()
		{

		}
	
		public ColagemOperResgPosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemOperResgPosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemOperResgPosCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemOperResgPosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemOperResgPosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemOperResgPosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemOperResgPosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemOperResgPosCotistaQuery query;
	}



	[Serializable]
	public partial class ColagemOperResgPosCotistaQuery : esColagemOperResgPosCotistaQuery
	{
		public ColagemOperResgPosCotistaQuery()
		{

		}		
		
		public ColagemOperResgPosCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemOperResgPosCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemOperResgPosCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemOperResgPosCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdColagemOperResgPosCotista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdColagemResgateDetalhe;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdOperacaoResgateVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.IdPosicaoVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosCotistaMetadata.ColumnNames.QuantidadeVinculada, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemOperResgPosCotistaMetadata.PropertyNames.QuantidadeVinculada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemOperResgPosCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemOperResgPosCotista = "IdColagemOperResgPosCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string QuantidadeVinculada = "QuantidadeVinculada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemOperResgPosCotista = "IdColagemOperResgPosCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string QuantidadeVinculada = "QuantidadeVinculada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemOperResgPosCotistaMetadata))
			{
				if(ColagemOperResgPosCotistaMetadata.mapDelegates == null)
				{
					ColagemOperResgPosCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemOperResgPosCotistaMetadata.meta == null)
				{
					ColagemOperResgPosCotistaMetadata.meta = new ColagemOperResgPosCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemOperResgPosCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoResgateVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeVinculada", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemOperResgPosCotista";
				meta.Destination = "ColagemOperResgPosCotista";
				
				meta.spInsert = "proc_ColagemOperResgPosCotistaInsert";				
				meta.spUpdate = "proc_ColagemOperResgPosCotistaUpdate";		
				meta.spDelete = "proc_ColagemOperResgPosCotistaDelete";
				meta.spLoadAll = "proc_ColagemOperResgPosCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemOperResgPosCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemOperResgPosCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
