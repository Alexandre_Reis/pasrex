/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/14/2015 4:14:47 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	


		















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esBloqueioCotaCollection : esEntityCollection
	{
		public esBloqueioCotaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BloqueioCotaCollection";
		}

		#region Query Logic
		protected void InitQuery(esBloqueioCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBloqueioCotaQuery);
		}
		#endregion
		
		virtual public BloqueioCota DetachEntity(BloqueioCota entity)
		{
			return base.DetachEntity(entity) as BloqueioCota;
		}
		
		virtual public BloqueioCota AttachEntity(BloqueioCota entity)
		{
			return base.AttachEntity(entity) as BloqueioCota;
		}
		
		virtual public void Combine(BloqueioCotaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BloqueioCota this[int index]
		{
			get
			{
				return base[index] as BloqueioCota;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BloqueioCota);
		}
	}



	[Serializable]
	abstract public class esBloqueioCota : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBloqueioCotaQuery GetDynamicQuery()
		{
			return null;
		}

		public esBloqueioCota()
		{

		}

		public esBloqueioCota(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBloqueio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBloqueio)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBloqueioCotaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBloqueio == idBloqueio);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBloqueio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBloqueio)
		{
			esBloqueioCotaQuery query = this.GetDynamicQuery();
			query.Where(query.IdBloqueio == idBloqueio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBloqueio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBloqueio",idBloqueio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBloqueio": this.str.IdBloqueio = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "TipoLiberacao": this.str.TipoLiberacao = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBloqueio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBloqueio = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "TipoLiberacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLiberacao = (System.Byte?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BloqueioCota.IdBloqueio
		/// </summary>
		virtual public System.Int32? IdBloqueio
		{
			get
			{
				return base.GetSystemInt32(BloqueioCotaMetadata.ColumnNames.IdBloqueio);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioCotaMetadata.ColumnNames.IdBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(BloqueioCotaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioCotaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(BloqueioCotaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(BloqueioCotaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(BloqueioCotaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(BloqueioCotaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(BloqueioCotaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(BloqueioCotaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.TipoLiberacao
		/// </summary>
		virtual public System.Byte? TipoLiberacao
		{
			get
			{
				return base.GetSystemByte(BloqueioCotaMetadata.ColumnNames.TipoLiberacao);
			}
			
			set
			{
				base.SetSystemByte(BloqueioCotaMetadata.ColumnNames.TipoLiberacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(BloqueioCotaMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(BloqueioCotaMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(BloqueioCotaMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(BloqueioCotaMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioCota.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(BloqueioCotaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(BloqueioCotaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBloqueioCota entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBloqueio
			{
				get
				{
					System.Int32? data = entity.IdBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBloqueio = null;
					else entity.IdBloqueio = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String TipoLiberacao
			{
				get
				{
					System.Byte? data = entity.TipoLiberacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiberacao = null;
					else entity.TipoLiberacao = Convert.ToByte(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
			

			private esBloqueioCota entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBloqueioCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBloqueioCota can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BloqueioCota : esBloqueioCota
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBloqueioCotaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioCotaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBloqueio
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.IdBloqueio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoLiberacao
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.TipoLiberacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, BloqueioCotaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BloqueioCotaCollection")]
	public partial class BloqueioCotaCollection : esBloqueioCotaCollection, IEnumerable<BloqueioCota>
	{
		public BloqueioCotaCollection()
		{

		}
		
		public static implicit operator List<BloqueioCota>(BloqueioCotaCollection coll)
		{
			List<BloqueioCota> list = new List<BloqueioCota>();
			
			foreach (BloqueioCota emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BloqueioCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BloqueioCota(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BloqueioCota();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BloqueioCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BloqueioCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BloqueioCota AddNew()
		{
			BloqueioCota entity = base.AddNewEntity() as BloqueioCota;
			
			return entity;
		}

		public BloqueioCota FindByPrimaryKey(System.Int32 idBloqueio)
		{
			return base.FindByPrimaryKey(idBloqueio) as BloqueioCota;
		}


		#region IEnumerable<BloqueioCota> Members

		IEnumerator<BloqueioCota> IEnumerable<BloqueioCota>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BloqueioCota;
			}
		}

		#endregion
		
		private BloqueioCotaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BloqueioCota' table
	/// </summary>

	[Serializable]
	public partial class BloqueioCota : esBloqueioCota
	{
		public BloqueioCota()
		{

		}
	
		public BloqueioCota(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esBloqueioCotaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BloqueioCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BloqueioCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BloqueioCotaQuery query;
	}



	[Serializable]
	public partial class BloqueioCotaQuery : esBloqueioCotaQuery
	{
		public BloqueioCotaQuery()
		{

		}		
		
		public BloqueioCotaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BloqueioCotaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BloqueioCotaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.IdBloqueio, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.IdBloqueio;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.Quantidade, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.TipoOperacao, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.TipoLiberacao, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.TipoLiberacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.DataInicio, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.DataFim, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioCotaMetadata.ColumnNames.Observacao, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = BloqueioCotaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BloqueioCotaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string IdPosicao = "IdPosicao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoLiberacao = "TipoLiberacao";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Observacao = "Observacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string IdPosicao = "IdPosicao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoLiberacao = "TipoLiberacao";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Observacao = "Observacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BloqueioCotaMetadata))
			{
				if(BloqueioCotaMetadata.mapDelegates == null)
				{
					BloqueioCotaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BloqueioCotaMetadata.meta == null)
				{
					BloqueioCotaMetadata.meta = new BloqueioCotaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBloqueio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoLiberacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "BloqueioCota";
				meta.Destination = "BloqueioCota";
				
				meta.spInsert = "proc_BloqueioCotaInsert";				
				meta.spUpdate = "proc_BloqueioCotaUpdate";		
				meta.spDelete = "proc_BloqueioCotaDelete";
				meta.spLoadAll = "proc_BloqueioCotaLoadAll";
				meta.spLoadByPrimaryKey = "proc_BloqueioCotaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BloqueioCotaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
