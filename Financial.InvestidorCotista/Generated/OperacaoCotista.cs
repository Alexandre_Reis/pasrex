/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/04/2016 17:07:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using refeInvestidor = Financial.Investidor;
using refeContaCorrente = Financial.ContaCorrente;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Common;



namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esOperacaoCotistaCollection : esEntityCollection
	{
		public esOperacaoCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoCotistaQuery);
		}
		#endregion
		
		virtual public OperacaoCotista DetachEntity(OperacaoCotista entity)
		{
			return base.DetachEntity(entity) as OperacaoCotista;
		}
		
		virtual public OperacaoCotista AttachEntity(OperacaoCotista entity)
		{
			return base.AttachEntity(entity) as OperacaoCotista;
		}
		
		virtual public void Combine(OperacaoCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoCotista this[int index]
		{
			get
			{
				return base[index] as OperacaoCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoCotista);
		}
	}



	[Serializable]
	abstract public class esOperacaoCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoCotista()
		{

		}

		public esOperacaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "DataAgendamento": this.str.DataAgendamento = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "TipoResgate": this.str.TipoResgate = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "DadosBancarios": this.str.DadosBancarios = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "CotaInformada": this.str.CotaInformada = (string)value; break;							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "IdOperacaoAuxiliar": this.str.IdOperacaoAuxiliar = (string)value; break;							
						case "IdOrdem": this.str.IdOrdem = (string)value; break;							
						case "RegistroEditado": this.str.RegistroEditado = (string)value; break;							
						case "ValoresColados": this.str.ValoresColados = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdSeriesOffShore": this.str.IdSeriesOffShore = (string)value; break;							
						case "IdTransferenciaSerie": this.str.IdTransferenciaSerie = (string)value; break;							
						case "ValorDespesas": this.str.ValorDespesas = (string)value; break;							
						case "ValorTaxas": this.str.ValorTaxas = (string)value; break;							
						case "ValorTributos": this.str.ValorTributos = (string)value; break;							
						case "PenalidadeLockUp": this.str.PenalidadeLockUp = (string)value; break;							
						case "ValorHoldBack": this.str.ValorHoldBack = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DepositoRetirada": this.str.DepositoRetirada = (string)value; break;							
						case "IdContaCorrente": this.str.IdContaCorrente = (string)value; break;							
						case "IdBoletaExterna": this.str.IdBoletaExterna = (string)value; break;							
						case "FieModalidade": this.str.FieModalidade = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "DataAplicCautelaResgatada": this.str.DataAplicCautelaResgatada = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "ExclusaoLogica": this.str.ExclusaoLogica = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "DataAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAgendamento = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "TipoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoResgate = (System.Byte?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "CotaInformada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaInformada = (System.Decimal?)value;
							break;
						
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "IdOperacaoAuxiliar":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoAuxiliar = (System.Int32?)value;
							break;
						
						case "IdOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdem = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdSeriesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSeriesOffShore = (System.Int32?)value;
							break;
						
						case "IdTransferenciaSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferenciaSerie = (System.Int32?)value;
							break;
						
						case "ValorDespesas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDespesas = (System.Decimal?)value;
							break;
						
						case "ValorTaxas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTaxas = (System.Decimal?)value;
							break;
						
						case "ValorTributos":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTributos = (System.Decimal?)value;
							break;
						
						case "PenalidadeLockUp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PenalidadeLockUp = (System.Decimal?)value;
							break;
						
						case "ValorHoldBack":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorHoldBack = (System.Decimal?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DepositoRetirada":
						
							if (value == null || value.GetType().ToString() == "System.Boolean")
								this.DepositoRetirada = (System.Boolean?)value;
							break;
						
						case "IdContaCorrente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaCorrente = (System.Int32?)value;
							break;
						
						case "IdBoletaExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBoletaExterna = (System.Int32?)value;
							break;
						
						case "FieModalidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieModalidade = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "DataAplicCautelaResgatada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicCautelaResgatada = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoCotista.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataAgendamento
		/// </summary>
		virtual public System.DateTime? DataAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.TipoResgate
		/// </summary>
		virtual public System.Byte? TipoResgate
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaMetadata.ColumnNames.TipoResgate);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaMetadata.ColumnNames.TipoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				if(base.SetSystemByte(OperacaoCotistaMetadata.ColumnNames.IdFormaLiquidacao, value))
				{
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DadosBancarios
		/// </summary>
		virtual public System.String DadosBancarios
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaMetadata.ColumnNames.DadosBancarios);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaMetadata.ColumnNames.DadosBancarios, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoCotistaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoCotistaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.CotaInformada
		/// </summary>
		virtual public System.Decimal? CotaInformada
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.CotaInformada);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.CotaInformada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdOperacaoAuxiliar
		/// </summary>
		virtual public System.Int32? IdOperacaoAuxiliar
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacaoAuxiliar);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOperacaoAuxiliar, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdOrdem
		/// </summary>
		virtual public System.Int32? IdOrdem
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOrdem);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.RegistroEditado
		/// </summary>
		virtual public System.String RegistroEditado
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaMetadata.ColumnNames.RegistroEditado);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaMetadata.ColumnNames.RegistroEditado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValoresColados
		/// </summary>
		virtual public System.String ValoresColados
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaMetadata.ColumnNames.ValoresColados);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaMetadata.ColumnNames.ValoresColados, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdSeriesOffShore
		/// </summary>
		virtual public System.Int32? IdSeriesOffShore
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdSeriesOffShore);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdSeriesOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdTransferenciaSerie
		/// </summary>
		virtual public System.Int32? IdTransferenciaSerie
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdTransferenciaSerie);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdTransferenciaSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorDespesas
		/// </summary>
		virtual public System.Decimal? ValorDespesas
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorDespesas);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorTaxas
		/// </summary>
		virtual public System.Decimal? ValorTaxas
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorTaxas);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorTaxas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorTributos
		/// </summary>
		virtual public System.Decimal? ValorTributos
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorTributos);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorTributos, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.PenalidadeLockUp
		/// </summary>
		virtual public System.Decimal? PenalidadeLockUp
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.PenalidadeLockUp);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.PenalidadeLockUp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ValorHoldBack
		/// </summary>
		virtual public System.Decimal? ValorHoldBack
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorHoldBack);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCotistaMetadata.ColumnNames.ValorHoldBack, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DepositoRetirada
		/// </summary>
		virtual public System.Boolean? DepositoRetirada
		{
			get
			{
				return base.GetSystemBoolean(OperacaoCotistaMetadata.ColumnNames.DepositoRetirada);
			}
			
			set
			{
				base.SetSystemBoolean(OperacaoCotistaMetadata.ColumnNames.DepositoRetirada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdContaCorrente
		/// </summary>
		virtual public System.Int32? IdContaCorrente
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdContaCorrente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdContaCorrente, value))
				{
					this._UpToContaCorrenteByIdContaCorrente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdBoletaExterna
		/// </summary>
		virtual public System.Int32? IdBoletaExterna
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdBoletaExterna);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdBoletaExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.FieModalidade
		/// </summary>
		virtual public System.Int32? FieModalidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.FieModalidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.FieModalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.DataAplicCautelaResgatada
		/// </summary>
		virtual public System.DateTime? DataAplicCautelaResgatada
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataAplicCautelaResgatada);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.DataAplicCautelaResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCotistaMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCotistaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCotista.ExclusaoLogica
		/// </summary>
		virtual public System.String ExclusaoLogica
		{
			get
			{
				return base.GetSystemString(OperacaoCotistaMetadata.ColumnNames.ExclusaoLogica);
			}
			
			set
			{
				base.SetSystemString(OperacaoCotistaMetadata.ColumnNames.ExclusaoLogica, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
        internal protected refeInvestidor.ContaCorrente _UpToContaCorrenteByIdContaCorrente;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		[CLSCompliant(false)]
        internal protected refeContaCorrente.FormaLiquidacao _UpToFormaLiquidacaoByIdFormaLiquidacao;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAgendamento
			{
				get
				{
					System.DateTime? data = entity.DataAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAgendamento = null;
					else entity.DataAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String TipoResgate
			{
				get
				{
					System.Byte? data = entity.TipoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoResgate = null;
					else entity.TipoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String DadosBancarios
			{
				get
				{
					System.String data = entity.DadosBancarios;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DadosBancarios = null;
					else entity.DadosBancarios = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String CotaInformada
			{
				get
				{
					System.Decimal? data = entity.CotaInformada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaInformada = null;
					else entity.CotaInformada = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoAuxiliar
			{
				get
				{
					System.Int32? data = entity.IdOperacaoAuxiliar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoAuxiliar = null;
					else entity.IdOperacaoAuxiliar = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOrdem
			{
				get
				{
					System.Int32? data = entity.IdOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdem = null;
					else entity.IdOrdem = Convert.ToInt32(value);
				}
			}
				
			public System.String RegistroEditado
			{
				get
				{
					System.String data = entity.RegistroEditado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroEditado = null;
					else entity.RegistroEditado = Convert.ToString(value);
				}
			}
				
			public System.String ValoresColados
			{
				get
				{
					System.String data = entity.ValoresColados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValoresColados = null;
					else entity.ValoresColados = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSeriesOffShore
			{
				get
				{
					System.Int32? data = entity.IdSeriesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSeriesOffShore = null;
					else entity.IdSeriesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTransferenciaSerie
			{
				get
				{
					System.Int32? data = entity.IdTransferenciaSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferenciaSerie = null;
					else entity.IdTransferenciaSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDespesas
			{
				get
				{
					System.Decimal? data = entity.ValorDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDespesas = null;
					else entity.ValorDespesas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTaxas
			{
				get
				{
					System.Decimal? data = entity.ValorTaxas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTaxas = null;
					else entity.ValorTaxas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTributos
			{
				get
				{
					System.Decimal? data = entity.ValorTributos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTributos = null;
					else entity.ValorTributos = Convert.ToDecimal(value);
				}
			}
				
			public System.String PenalidadeLockUp
			{
				get
				{
					System.Decimal? data = entity.PenalidadeLockUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PenalidadeLockUp = null;
					else entity.PenalidadeLockUp = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorHoldBack
			{
				get
				{
					System.Decimal? data = entity.ValorHoldBack;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorHoldBack = null;
					else entity.ValorHoldBack = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DepositoRetirada
			{
				get
				{
					System.Boolean? data = entity.DepositoRetirada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DepositoRetirada = null;
					else entity.DepositoRetirada = Convert.ToBoolean(value);
				}
			}
				
			public System.String IdContaCorrente
			{
				get
				{
					System.Int32? data = entity.IdContaCorrente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaCorrente = null;
					else entity.IdContaCorrente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdBoletaExterna
			{
				get
				{
					System.Int32? data = entity.IdBoletaExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBoletaExterna = null;
					else entity.IdBoletaExterna = Convert.ToInt32(value);
				}
			}
				
			public System.String FieModalidade
			{
				get
				{
					System.Int32? data = entity.FieModalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieModalidade = null;
					else entity.FieModalidade = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicCautelaResgatada
			{
				get
				{
					System.DateTime? data = entity.DataAplicCautelaResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicCautelaResgatada = null;
					else entity.DataAplicCautelaResgatada = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String ExclusaoLogica
			{
				get
				{
					System.String data = entity.ExclusaoLogica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExclusaoLogica = null;
					else entity.ExclusaoLogica = Convert.ToString(value);
				}
			}
			

			private esOperacaoCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoCotista : esOperacaoCotista
	{

		#region UpToOfertaDistribuicaoCotasCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OfertaDistribuicaoCotasCollection UpToOfertaDistribuicaoCotasCollection
		{
			get
			{
				if(this._UpToOfertaDistribuicaoCotasCollection == null)
				{
					this._UpToOfertaDistribuicaoCotasCollection = new OfertaDistribuicaoCotasCollection();
					this._UpToOfertaDistribuicaoCotasCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToOfertaDistribuicaoCotasCollection", this._UpToOfertaDistribuicaoCotasCollection);
					this._UpToOfertaDistribuicaoCotasCollection.ManyToManyOperacaoCotistaCollection(this.IdOperacao);
				}

				return this._UpToOfertaDistribuicaoCotasCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToOfertaDistribuicaoCotasCollection != null) 
				{ 
					this.RemovePostSave("UpToOfertaDistribuicaoCotasCollection"); 
					this._UpToOfertaDistribuicaoCotasCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OperacaoCotista_FK1
		/// </summary>
		public void AssociateOfertaDistribuicaoCotasCollection(OfertaDistribuicaoCotas entity)
		{
			if (this._AplicacaoOfertaDistribuicaoCotasCollection == null)
			{
				this._AplicacaoOfertaDistribuicaoCotasCollection = new AplicacaoOfertaDistribuicaoCotasCollection();
				this._AplicacaoOfertaDistribuicaoCotasCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollection", this._AplicacaoOfertaDistribuicaoCotasCollection);
			}

			AplicacaoOfertaDistribuicaoCotas obj = this._AplicacaoOfertaDistribuicaoCotasCollection.AddNew();
			obj.IdOperacao = this.IdOperacao;
			obj.IdOfertaDistribuicaoCotas = entity.IdOfertaDistribuicaoCotas;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OperacaoCotista_FK1
		/// </summary>
		public void DissociateOfertaDistribuicaoCotasCollection(OfertaDistribuicaoCotas entity)
		{
			if (this._AplicacaoOfertaDistribuicaoCotasCollection == null)
			{
				this._AplicacaoOfertaDistribuicaoCotasCollection = new AplicacaoOfertaDistribuicaoCotasCollection();
				this._AplicacaoOfertaDistribuicaoCotasCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollection", this._AplicacaoOfertaDistribuicaoCotasCollection);
			}

			AplicacaoOfertaDistribuicaoCotas obj = this._AplicacaoOfertaDistribuicaoCotasCollection.AddNew();
			obj.IdOperacao = this.IdOperacao;
			obj.IdOfertaDistribuicaoCotas = entity.IdOfertaDistribuicaoCotas;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private OfertaDistribuicaoCotasCollection _UpToOfertaDistribuicaoCotasCollection;
		private AplicacaoOfertaDistribuicaoCotasCollection _AplicacaoOfertaDistribuicaoCotasCollection;
		#endregion

				
		#region AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public AplicacaoOfertaDistribuicaoCotasCollection AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao
		{
			get
			{
				if(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao == null)
				{
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao = new AplicacaoOfertaDistribuicaoCotasCollection();
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao", this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao.Query.Where(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao.fks.Add(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao"); 
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private AplicacaoOfertaDistribuicaoCotasCollection _AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao;
		#endregion

				
		#region ComplementoProventosCotista - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - ComplementoProventosCotista_OperacaoCotista_FK
		/// </summary>

		[XmlIgnore]
		public ComplementoProventosCotista ComplementoProventosCotista
		{
			get
			{
				if(this._ComplementoProventosCotista == null)
				{
					this._ComplementoProventosCotista = new ComplementoProventosCotista();
					this._ComplementoProventosCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ComplementoProventosCotista", this._ComplementoProventosCotista);
				
					if(this.IdOperacao != null)
					{
						this._ComplementoProventosCotista.Query.Where(this._ComplementoProventosCotista.Query.IdOperacao == this.IdOperacao);
						this._ComplementoProventosCotista.Query.Load();
					}
				}

				return this._ComplementoProventosCotista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ComplementoProventosCotista != null) 
				{ 
					this.RemovePostOneSave("ComplementoProventosCotista"); 
					this._ComplementoProventosCotista = null;
					
				} 
			}          			
		}

		private ComplementoProventosCotista _ComplementoProventosCotista;
		#endregion

				
		#region PosicaoCotistaCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Operacao_PosicaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaCollection PosicaoCotistaCollectionByIdOperacao
		{
			get
			{
				if(this._PosicaoCotistaCollectionByIdOperacao == null)
				{
					this._PosicaoCotistaCollectionByIdOperacao = new PosicaoCotistaCollection();
					this._PosicaoCotistaCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaCollectionByIdOperacao", this._PosicaoCotistaCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._PosicaoCotistaCollectionByIdOperacao.Query.Where(this._PosicaoCotistaCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._PosicaoCotistaCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaCollectionByIdOperacao.fks.Add(PosicaoCotistaMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._PosicaoCotistaCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaCollectionByIdOperacao"); 
					this._PosicaoCotistaCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private PosicaoCotistaCollection _PosicaoCotistaCollectionByIdOperacao;
		#endregion

				
		#region TransferenciaEntreCotistaCollectionByIdAplicacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransEntreCotista_Operacao_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaEntreCotistaCollection TransferenciaEntreCotistaCollectionByIdAplicacao
		{
			get
			{
				if(this._TransferenciaEntreCotistaCollectionByIdAplicacao == null)
				{
					this._TransferenciaEntreCotistaCollectionByIdAplicacao = new TransferenciaEntreCotistaCollection();
					this._TransferenciaEntreCotistaCollectionByIdAplicacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaEntreCotistaCollectionByIdAplicacao", this._TransferenciaEntreCotistaCollectionByIdAplicacao);
				
					if(this.IdOperacao != null)
					{
						this._TransferenciaEntreCotistaCollectionByIdAplicacao.Query.Where(this._TransferenciaEntreCotistaCollectionByIdAplicacao.Query.IdAplicacao == this.IdOperacao);
						this._TransferenciaEntreCotistaCollectionByIdAplicacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaEntreCotistaCollectionByIdAplicacao.fks.Add(TransferenciaEntreCotistaMetadata.ColumnNames.IdAplicacao, this.IdOperacao);
					}
				}

				return this._TransferenciaEntreCotistaCollectionByIdAplicacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaEntreCotistaCollectionByIdAplicacao != null) 
				{ 
					this.RemovePostSave("TransferenciaEntreCotistaCollectionByIdAplicacao"); 
					this._TransferenciaEntreCotistaCollectionByIdAplicacao = null;
					
				} 
			} 			
		}

		private TransferenciaEntreCotistaCollection _TransferenciaEntreCotistaCollectionByIdAplicacao;
		#endregion

				
		#region TributacaoCautelaFieCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TributacaoCautelaFie_OperacaoCotista_FK
		/// </summary>

		[XmlIgnore]
		public TributacaoCautelaFieCollection TributacaoCautelaFieCollectionByIdOperacao
		{
			get
			{
				if(this._TributacaoCautelaFieCollectionByIdOperacao == null)
				{
					this._TributacaoCautelaFieCollectionByIdOperacao = new TributacaoCautelaFieCollection();
					this._TributacaoCautelaFieCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TributacaoCautelaFieCollectionByIdOperacao", this._TributacaoCautelaFieCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._TributacaoCautelaFieCollectionByIdOperacao.Query.Where(this._TributacaoCautelaFieCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._TributacaoCautelaFieCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TributacaoCautelaFieCollectionByIdOperacao.fks.Add(TributacaoCautelaFieMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._TributacaoCautelaFieCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TributacaoCautelaFieCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("TributacaoCautelaFieCollectionByIdOperacao"); 
					this._TributacaoCautelaFieCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private TributacaoCautelaFieCollection _TributacaoCautelaFieCollectionByIdOperacao;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdContaCorrente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Operacao_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
        public refeInvestidor.ContaCorrente UpToContaCorrenteByIdContaCorrente
		{
			get
			{
				if(this._UpToContaCorrenteByIdContaCorrente == null
					&& IdContaCorrente != null					)
				{
					this._UpToContaCorrenteByIdContaCorrente = new refeInvestidor.ContaCorrente();
					this._UpToContaCorrenteByIdContaCorrente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdContaCorrente", this._UpToContaCorrenteByIdContaCorrente);
					this._UpToContaCorrenteByIdContaCorrente.Query.Where(this._UpToContaCorrenteByIdContaCorrente.Query.IdConta == this.IdContaCorrente);
					this._UpToContaCorrenteByIdContaCorrente.Query.Load();
				}

				return this._UpToContaCorrenteByIdContaCorrente;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdContaCorrente");
				

				if(value == null)
				{
					this.IdContaCorrente = null;
					this._UpToContaCorrenteByIdContaCorrente = null;
				}
				else
				{
					this.IdContaCorrente = value.IdConta;
					this._UpToContaCorrenteByIdContaCorrente = value;
					this.SetPreSave("UpToContaCorrenteByIdContaCorrente", this._UpToContaCorrenteByIdContaCorrente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

				
		#region UpToFormaLiquidacaoByIdFormaLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FormaLiquidacao_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
        public refeContaCorrente.FormaLiquidacao UpToFormaLiquidacaoByIdFormaLiquidacao
		{
			get
			{
				if(this._UpToFormaLiquidacaoByIdFormaLiquidacao == null
					&& IdFormaLiquidacao != null					)
				{
                    this._UpToFormaLiquidacaoByIdFormaLiquidacao = new refeContaCorrente.FormaLiquidacao();
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Where(this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Load();
				}

				return this._UpToFormaLiquidacaoByIdFormaLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToFormaLiquidacaoByIdFormaLiquidacao");
				

				if(value == null)
				{
					this.IdFormaLiquidacao = null;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
				else
				{
					this.IdFormaLiquidacao = value.IdFormaLiquidacao;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = value;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OpCotista_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao", typeof(AplicacaoOfertaDistribuicaoCotasCollection), new AplicacaoOfertaDistribuicaoCotas()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaCollectionByIdOperacao", typeof(PosicaoCotistaCollection), new PosicaoCotista()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaEntreCotistaCollectionByIdAplicacao", typeof(TransferenciaEntreCotistaCollection), new TransferenciaEntreCotista()));
			props.Add(new esPropertyDescriptor(this, "TributacaoCautelaFieCollectionByIdOperacao", typeof(TributacaoCautelaFieCollection), new TributacaoCautelaFie()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdContaCorrente != null)
			{
				this.IdContaCorrente = this._UpToContaCorrenteByIdContaCorrente.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToFormaLiquidacaoByIdFormaLiquidacao != null)
			{
				this.IdFormaLiquidacao = this._UpToFormaLiquidacaoByIdFormaLiquidacao.IdFormaLiquidacao;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AplicacaoOfertaDistribuicaoCotasCollection != null)
			{
				foreach(AplicacaoOfertaDistribuicaoCotas obj in this._AplicacaoOfertaDistribuicaoCotasCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao != null)
			{
				foreach(AplicacaoOfertaDistribuicaoCotas obj in this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._PosicaoCotistaCollectionByIdOperacao != null)
			{
				foreach(PosicaoCotista obj in this._PosicaoCotistaCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._TransferenciaEntreCotistaCollectionByIdAplicacao != null)
			{
				foreach(TransferenciaEntreCotista obj in this._TransferenciaEntreCotistaCollectionByIdAplicacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAplicacao = this.IdOperacao;
					}
				}
			}
			if(this._TributacaoCautelaFieCollectionByIdOperacao != null)
			{
				foreach(TributacaoCautelaFie obj in this._TributacaoCautelaFieCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._ComplementoProventosCotista != null)
			{
				if(this._ComplementoProventosCotista.es.IsAdded)
				{
					this._ComplementoProventosCotista.IdOperacao = this.IdOperacao;
				}
			}
		}
		
	}
	
	public partial class OperacaoCotistaCollection : esOperacaoCotistaCollection
	{
		#region ManyToManyOfertaDistribuicaoCotasCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyOfertaDistribuicaoCotasCollection(System.Int32? IdOfertaDistribuicaoCotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOfertaDistribuicaoCotas", IdOfertaDistribuicaoCotas);
	
			return base.Load( esQueryType.ManyToMany, 
				"OperacaoCotista,AplicacaoOfertaDistribuicaoCotas|IdOperacao,IdOperacao|IdOfertaDistribuicaoCotas",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esOperacaoCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAgendamento
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.TipoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem DadosBancarios
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DadosBancarios, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CotaInformada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.CotaInformada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoAuxiliar
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdOperacaoAuxiliar, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOrdem
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdOrdem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RegistroEditado
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.RegistroEditado, esSystemType.String);
			}
		} 
		
		public esQueryItem ValoresColados
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValoresColados, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSeriesOffShore
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdSeriesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTransferenciaSerie
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdTransferenciaSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDespesas
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorDespesas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTaxas
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorTaxas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTributos
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorTributos, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PenalidadeLockUp
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.PenalidadeLockUp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorHoldBack
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ValorHoldBack, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DepositoRetirada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DepositoRetirada, esSystemType.Boolean);
			}
		} 
		
		public esQueryItem IdContaCorrente
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdContaCorrente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdBoletaExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdBoletaExterna, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieModalidade
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.FieModalidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicCautelaResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.DataAplicCautelaResgatada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExclusaoLogica
		{
			get
			{
				return new esQueryItem(this, OperacaoCotistaMetadata.ColumnNames.ExclusaoLogica, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoCotistaCollection")]
	public partial class OperacaoCotistaCollection : esOperacaoCotistaCollection, IEnumerable<OperacaoCotista>
	{
		public OperacaoCotistaCollection()
		{

		}
		
		public static implicit operator List<OperacaoCotista>(OperacaoCotistaCollection coll)
		{
			List<OperacaoCotista> list = new List<OperacaoCotista>();
			
			foreach (OperacaoCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoCotista AddNew()
		{
			OperacaoCotista entity = base.AddNewEntity() as OperacaoCotista;
			
			return entity;
		}

		public OperacaoCotista FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoCotista;
		}


		#region IEnumerable<OperacaoCotista> Members

		IEnumerator<OperacaoCotista> IEnumerable<OperacaoCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoCotista;
			}
		}

		#endregion
		
		private OperacaoCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoCotista' table
	/// </summary>

	[Serializable]
	public partial class OperacaoCotista : esOperacaoCotista
	{
		public OperacaoCotista()
		{

		}
	
		public OperacaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoCotistaQuery query;
	}



	[Serializable]
	public partial class OperacaoCotistaQuery : esOperacaoCotistaQuery
	{
		public OperacaoCotistaQuery()
		{

		}		
		
		public OperacaoCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataOperacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataLiquidacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataAgendamento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataAgendamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.TipoOperacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.TipoResgate, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.TipoResgate;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdFormaLiquidacao, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdFormaLiquidacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.Quantidade, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorBruto, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorIR, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorIOF, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorCPMF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.Observacao, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DadosBancarios, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DadosBancarios;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.Fonte, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdConta, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.CotaInformada, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.CotaInformada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdAgenda, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdAgenda;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdOperacaoAuxiliar, 29, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdOperacaoAuxiliar;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdOrdem, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdOrdem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.RegistroEditado, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.RegistroEditado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValoresColados, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValoresColados;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdLocalNegociacao, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('1')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdSeriesOffShore, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdSeriesOffShore;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdTransferenciaSerie, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdTransferenciaSerie;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorDespesas, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorDespesas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorTaxas, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorTaxas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorTributos, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorTributos;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.PenalidadeLockUp, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.PenalidadeLockUp;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ValorHoldBack, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ValorHoldBack;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataRegistro, 41, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DepositoRetirada, 42, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DepositoRetirada;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdContaCorrente, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdContaCorrente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdBoletaExterna, 44, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdBoletaExterna;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.FieModalidade, 45, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.FieModalidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.FieTabelaIr, 46, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.DataAplicCautelaResgatada, 47, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.DataAplicCautelaResgatada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.UserTimeStamp, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.IdTrader, 49, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCotistaMetadata.ColumnNames.ExclusaoLogica, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoCotistaMetadata.PropertyNames.ExclusaoLogica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string Observacao = "Observacao";
			 public const string DadosBancarios = "DadosBancarios";
			 public const string Fonte = "Fonte";
			 public const string IdConta = "IdConta";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdAgenda = "IdAgenda";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string IdOperacaoAuxiliar = "IdOperacaoAuxiliar";
			 public const string IdOrdem = "IdOrdem";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string ValorDespesas = "ValorDespesas";
			 public const string ValorTaxas = "ValorTaxas";
			 public const string ValorTributos = "ValorTributos";
			 public const string PenalidadeLockUp = "PenalidadeLockUp";
			 public const string ValorHoldBack = "ValorHoldBack";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string IdContaCorrente = "IdContaCorrente";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string Observacao = "Observacao";
			 public const string DadosBancarios = "DadosBancarios";
			 public const string Fonte = "Fonte";
			 public const string IdConta = "IdConta";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdAgenda = "IdAgenda";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string IdOperacaoAuxiliar = "IdOperacaoAuxiliar";
			 public const string IdOrdem = "IdOrdem";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string IdTransferenciaSerie = "IdTransferenciaSerie";
			 public const string ValorDespesas = "ValorDespesas";
			 public const string ValorTaxas = "ValorTaxas";
			 public const string ValorTributos = "ValorTributos";
			 public const string PenalidadeLockUp = "PenalidadeLockUp";
			 public const string ValorHoldBack = "ValorHoldBack";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string IdContaCorrente = "IdContaCorrente";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoCotistaMetadata))
			{
				if(OperacaoCotistaMetadata.mapDelegates == null)
				{
					OperacaoCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoCotistaMetadata.meta == null)
				{
					OperacaoCotistaMetadata.meta = new OperacaoCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataAgendamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DadosBancarios", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CotaInformada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoAuxiliar", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOrdem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RegistroEditado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValoresColados", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSeriesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTransferenciaSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDespesas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTaxas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTributos", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PenalidadeLockUp", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorHoldBack", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DepositoRetirada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("IdContaCorrente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdBoletaExterna", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieModalidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicCautelaResgatada", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExclusaoLogica", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "OperacaoCotista";
				meta.Destination = "OperacaoCotista";
				
				meta.spInsert = "proc_OperacaoCotistaInsert";				
				meta.spUpdate = "proc_OperacaoCotistaUpdate";		
				meta.spDelete = "proc_OperacaoCotistaDelete";
				meta.spLoadAll = "proc_OperacaoCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
