/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 26/01/2015 11:09:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esColagemComeCotasPosCotistaCollection : esEntityCollection
	{
		public esColagemComeCotasPosCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemComeCotasPosCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemComeCotasPosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemComeCotasPosCotistaQuery);
		}
		#endregion
		
		virtual public ColagemComeCotasPosCotista DetachEntity(ColagemComeCotasPosCotista entity)
		{
			return base.DetachEntity(entity) as ColagemComeCotasPosCotista;
		}
		
		virtual public ColagemComeCotasPosCotista AttachEntity(ColagemComeCotasPosCotista entity)
		{
			return base.AttachEntity(entity) as ColagemComeCotasPosCotista;
		}
		
		virtual public void Combine(ColagemComeCotasPosCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemComeCotasPosCotista this[int index]
		{
			get
			{
				return base[index] as ColagemComeCotasPosCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemComeCotasPosCotista);
		}
	}



	[Serializable]
	abstract public class esColagemComeCotasPosCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemComeCotasPosCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemComeCotasPosCotista()
		{

		}

		public esColagemComeCotasPosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemComeCotasPosCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasPosCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasPosCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemComeCotasPosCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasPosCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasPosCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemComeCotasPosCotista)
		{
			esColagemComeCotasPosCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemComeCotasPosCotista == idColagemComeCotasPosCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemComeCotasPosCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemComeCotasPosCotista",idColagemComeCotasPosCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemComeCotasPosCotista": this.str.IdColagemComeCotasPosCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdPosicaoVinculada": this.str.IdPosicaoVinculada = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoComeCotas": this.str.RendimentoComeCotas = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "VariacaoComeCotas": this.str.VariacaoComeCotas = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Participacao": this.str.Participacao = (string)value; break;							
						case "DetalheImportado": this.str.DetalheImportado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemComeCotasPosCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotasPosCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdPosicaoVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoVinculada = (System.Int32?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoComeCotas = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "VariacaoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoComeCotas = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Participacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Participacao = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.IdColagemComeCotasPosCotista
		/// </summary>
		virtual public System.Int32? IdColagemComeCotasPosCotista
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdColagemComeCotasPosCotista);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdColagemComeCotasPosCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.IdPosicaoVinculada
		/// </summary>
		virtual public System.Int32? IdPosicaoVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdPosicaoVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.RendimentoComeCotas
		/// </summary>
		virtual public System.Decimal? RendimentoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.RendimentoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.RendimentoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.VariacaoComeCotas
		/// </summary>
		virtual public System.Decimal? VariacaoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.VariacaoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.VariacaoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.Participacao
		/// </summary>
		virtual public System.Decimal? Participacao
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.Participacao);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasPosCotistaMetadata.ColumnNames.Participacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasPosCotista.DetalheImportado
		/// </summary>
		virtual public System.String DetalheImportado
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasPosCotistaMetadata.ColumnNames.DetalheImportado);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasPosCotistaMetadata.ColumnNames.DetalheImportado, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemComeCotasPosCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemComeCotasPosCotista
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotasPosCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotasPosCotista = null;
					else entity.IdColagemComeCotasPosCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPosicaoVinculada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoVinculada = null;
					else entity.IdPosicaoVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoComeCotas
			{
				get
				{
					System.Decimal? data = entity.RendimentoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoComeCotas = null;
					else entity.RendimentoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoComeCotas
			{
				get
				{
					System.Decimal? data = entity.VariacaoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoComeCotas = null;
					else entity.VariacaoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Participacao
			{
				get
				{
					System.Decimal? data = entity.Participacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Participacao = null;
					else entity.Participacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DetalheImportado
			{
				get
				{
					System.String data = entity.DetalheImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DetalheImportado = null;
					else entity.DetalheImportado = Convert.ToString(value);
				}
			}
			

			private esColagemComeCotasPosCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemComeCotasPosCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemComeCotasPosCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemComeCotasPosCotista : esColagemComeCotasPosCotista
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemComeCotasPosCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasPosCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemComeCotasPosCotista
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.IdColagemComeCotasPosCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPosicaoVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.RendimentoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.VariacaoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Participacao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.Participacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DetalheImportado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasPosCotistaMetadata.ColumnNames.DetalheImportado, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemComeCotasPosCotistaCollection")]
	public partial class ColagemComeCotasPosCotistaCollection : esColagemComeCotasPosCotistaCollection, IEnumerable<ColagemComeCotasPosCotista>
	{
		public ColagemComeCotasPosCotistaCollection()
		{

		}
		
		public static implicit operator List<ColagemComeCotasPosCotista>(ColagemComeCotasPosCotistaCollection coll)
		{
			List<ColagemComeCotasPosCotista> list = new List<ColagemComeCotasPosCotista>();
			
			foreach (ColagemComeCotasPosCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemComeCotasPosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasPosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemComeCotasPosCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemComeCotasPosCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemComeCotasPosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasPosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemComeCotasPosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemComeCotasPosCotista AddNew()
		{
			ColagemComeCotasPosCotista entity = base.AddNewEntity() as ColagemComeCotasPosCotista;
			
			return entity;
		}

		public ColagemComeCotasPosCotista FindByPrimaryKey(System.Int32 idColagemComeCotasPosCotista)
		{
			return base.FindByPrimaryKey(idColagemComeCotasPosCotista) as ColagemComeCotasPosCotista;
		}


		#region IEnumerable<ColagemComeCotasPosCotista> Members

		IEnumerator<ColagemComeCotasPosCotista> IEnumerable<ColagemComeCotasPosCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemComeCotasPosCotista;
			}
		}

		#endregion
		
		private ColagemComeCotasPosCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemComeCotasPosCotista' table
	/// </summary>

	[Serializable]
	public partial class ColagemComeCotasPosCotista : esColagemComeCotasPosCotista
	{
		public ColagemComeCotasPosCotista()
		{

		}
	
		public ColagemComeCotasPosCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasPosCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemComeCotasPosCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasPosCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemComeCotasPosCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasPosCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemComeCotasPosCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemComeCotasPosCotistaQuery query;
	}



	[Serializable]
	public partial class ColagemComeCotasPosCotistaQuery : esColagemComeCotasPosCotistaQuery
	{
		public ColagemComeCotasPosCotistaQuery()
		{

		}		
		
		public ColagemComeCotasPosCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemComeCotasPosCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemComeCotasPosCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdColagemComeCotasPosCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.IdColagemComeCotasPosCotista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataConversao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.DataReferencia, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.IdPosicaoVinculada, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.IdPosicaoVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.PrejuizoUsado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.RendimentoComeCotas, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.RendimentoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorBruto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorCPMF, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIOF, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorIR, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorLiquido, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.ValorPerformance, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.VariacaoComeCotas, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.VariacaoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.Quantidade, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.Participacao, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.Participacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasPosCotistaMetadata.ColumnNames.DetalheImportado, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasPosCotistaMetadata.PropertyNames.DetalheImportado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemComeCotasPosCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemComeCotasPosCotista = "IdColagemComeCotasPosCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataConversao = "DataConversao";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorIR = "ValorIR";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string Quantidade = "Quantidade";
			 public const string Participacao = "Participacao";
			 public const string DetalheImportado = "DetalheImportado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemComeCotasPosCotista = "IdColagemComeCotasPosCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataConversao = "DataConversao";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorIR = "ValorIR";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string Quantidade = "Quantidade";
			 public const string Participacao = "Participacao";
			 public const string DetalheImportado = "DetalheImportado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemComeCotasPosCotistaMetadata))
			{
				if(ColagemComeCotasPosCotistaMetadata.mapDelegates == null)
				{
					ColagemComeCotasPosCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemComeCotasPosCotistaMetadata.meta == null)
				{
					ColagemComeCotasPosCotistaMetadata.meta = new ColagemComeCotasPosCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemComeCotasPosCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPosicaoVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Participacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DetalheImportado", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "ColagemComeCotasPosCotista";
				meta.Destination = "ColagemComeCotasPosCotista";
				
				meta.spInsert = "proc_ColagemComeCotasPosCotistaInsert";				
				meta.spUpdate = "proc_ColagemComeCotasPosCotistaUpdate";		
				meta.spDelete = "proc_ColagemComeCotasPosCotistaDelete";
				meta.spLoadAll = "proc_ColagemComeCotasPosCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemComeCotasPosCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemComeCotasPosCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
