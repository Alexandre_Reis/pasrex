/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/14/2015 4:14:59 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	












		
using Financial.Fundo;





					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esTransferenciaCotaCollection : esEntityCollection
	{
		public esTransferenciaCotaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TransferenciaCotaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTransferenciaCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTransferenciaCotaQuery);
		}
		#endregion
		
		virtual public TransferenciaCota DetachEntity(TransferenciaCota entity)
		{
			return base.DetachEntity(entity) as TransferenciaCota;
		}
		
		virtual public TransferenciaCota AttachEntity(TransferenciaCota entity)
		{
			return base.AttachEntity(entity) as TransferenciaCota;
		}
		
		virtual public void Combine(TransferenciaCotaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TransferenciaCota this[int index]
		{
			get
			{
				return base[index] as TransferenciaCota;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TransferenciaCota);
		}
	}



	[Serializable]
	abstract public class esTransferenciaCota : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTransferenciaCotaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTransferenciaCota()
		{

		}

		public esTransferenciaCota(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTransferencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTransferencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTransferenciaCotaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTransferencia == idTransferencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTransferencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTransferencia)
		{
			esTransferenciaCotaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTransferencia == idTransferencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTransferencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTransferencia",idTransferencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTransferencia": this.str.IdTransferencia = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCotistaOrigem": this.str.IdCotistaOrigem = (string)value; break;							
						case "IdCotistaDestino": this.str.IdCotistaDestino = (string)value; break;							
						case "DataTransferencia": this.str.DataTransferencia = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "IdPosicaoOrigem": this.str.IdPosicaoOrigem = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTransferencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferencia = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCotistaOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotistaOrigem = (System.Int32?)value;
							break;
						
						case "IdCotistaDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotistaDestino = (System.Int32?)value;
							break;
						
						case "DataTransferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataTransferencia = (System.DateTime?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
						
						case "IdPosicaoOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoOrigem = (System.Int32?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TransferenciaCota.IdTransferencia
		/// </summary>
		virtual public System.Int32? IdTransferencia
		{
			get
			{
				return base.GetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdTransferencia);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdTransferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.IdCotistaOrigem
		/// </summary>
		virtual public System.Int32? IdCotistaOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCotistaOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCotistaOrigem, value))
				{
					this._UpToCotistaByIdCotistaOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.IdCotistaDestino
		/// </summary>
		virtual public System.Int32? IdCotistaDestino
		{
			get
			{
				return base.GetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCotistaDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdCotistaDestino, value))
				{
					this._UpToCotistaByIdCotistaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.DataTransferencia
		/// </summary>
		virtual public System.DateTime? DataTransferencia
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaCotaMetadata.ColumnNames.DataTransferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaCotaMetadata.ColumnNames.DataTransferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(TransferenciaCotaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(TransferenciaCotaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.IdPosicaoOrigem
		/// </summary>
		virtual public System.Int32? IdPosicaoOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdPosicaoOrigem);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaCotaMetadata.ColumnNames.IdPosicaoOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaCotaMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaCotaMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(TransferenciaCotaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(TransferenciaCotaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaCota.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TransferenciaCotaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TransferenciaCotaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotistaOrigem;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotistaDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTransferenciaCota entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTransferencia
			{
				get
				{
					System.Int32? data = entity.IdTransferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferencia = null;
					else entity.IdTransferencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotistaOrigem
			{
				get
				{
					System.Int32? data = entity.IdCotistaOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotistaOrigem = null;
					else entity.IdCotistaOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotistaDestino
			{
				get
				{
					System.Int32? data = entity.IdCotistaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotistaDestino = null;
					else entity.IdCotistaDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String DataTransferencia
			{
				get
				{
					System.DateTime? data = entity.DataTransferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataTransferencia = null;
					else entity.DataTransferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoOrigem
			{
				get
				{
					System.Int32? data = entity.IdPosicaoOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoOrigem = null;
					else entity.IdPosicaoOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esTransferenciaCota entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTransferenciaCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTransferenciaCota can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TransferenciaCota : esTransferenciaCota
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_TransferenciaCota_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotistaOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_TransferenciaCota_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotistaOrigem
		{
			get
			{
				if(this._UpToCotistaByIdCotistaOrigem == null
					&& IdCotistaOrigem != null					)
				{
					this._UpToCotistaByIdCotistaOrigem = new Cotista();
					this._UpToCotistaByIdCotistaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotistaOrigem", this._UpToCotistaByIdCotistaOrigem);
					this._UpToCotistaByIdCotistaOrigem.Query.Where(this._UpToCotistaByIdCotistaOrigem.Query.IdCotista == this.IdCotistaOrigem);
					this._UpToCotistaByIdCotistaOrigem.Query.Load();
				}

				return this._UpToCotistaByIdCotistaOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotistaOrigem");
				

				if(value == null)
				{
					this.IdCotistaOrigem = null;
					this._UpToCotistaByIdCotistaOrigem = null;
				}
				else
				{
					this.IdCotistaOrigem = value.IdCotista;
					this._UpToCotistaByIdCotistaOrigem = value;
					this.SetPreSave("UpToCotistaByIdCotistaOrigem", this._UpToCotistaByIdCotistaOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotistaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_TransferenciaCota_FK2
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotistaDestino
		{
			get
			{
				if(this._UpToCotistaByIdCotistaDestino == null
					&& IdCotistaDestino != null					)
				{
					this._UpToCotistaByIdCotistaDestino = new Cotista();
					this._UpToCotistaByIdCotistaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotistaDestino", this._UpToCotistaByIdCotistaDestino);
					this._UpToCotistaByIdCotistaDestino.Query.Where(this._UpToCotistaByIdCotistaDestino.Query.IdCotista == this.IdCotistaDestino);
					this._UpToCotistaByIdCotistaDestino.Query.Load();
				}

				return this._UpToCotistaByIdCotistaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotistaDestino");
				

				if(value == null)
				{
					this.IdCotistaDestino = null;
					this._UpToCotistaByIdCotistaDestino = null;
				}
				else
				{
					this.IdCotistaDestino = value.IdCotista;
					this._UpToCotistaByIdCotistaDestino = value;
					this.SetPreSave("UpToCotistaByIdCotistaDestino", this._UpToCotistaByIdCotistaDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTransferenciaCotaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaCotaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTransferencia
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.IdTransferencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotistaOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.IdCotistaOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotistaDestino
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.IdCotistaDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataTransferencia
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.DataTransferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.IdPosicaoOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TransferenciaCotaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TransferenciaCotaCollection")]
	public partial class TransferenciaCotaCollection : esTransferenciaCotaCollection, IEnumerable<TransferenciaCota>
	{
		public TransferenciaCotaCollection()
		{

		}
		
		public static implicit operator List<TransferenciaCota>(TransferenciaCotaCollection coll)
		{
			List<TransferenciaCota> list = new List<TransferenciaCota>();
			
			foreach (TransferenciaCota emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TransferenciaCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TransferenciaCota(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TransferenciaCota();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TransferenciaCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TransferenciaCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TransferenciaCota AddNew()
		{
			TransferenciaCota entity = base.AddNewEntity() as TransferenciaCota;
			
			return entity;
		}

		public TransferenciaCota FindByPrimaryKey(System.Int32 idTransferencia)
		{
			return base.FindByPrimaryKey(idTransferencia) as TransferenciaCota;
		}


		#region IEnumerable<TransferenciaCota> Members

		IEnumerator<TransferenciaCota> IEnumerable<TransferenciaCota>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TransferenciaCota;
			}
		}

		#endregion
		
		private TransferenciaCotaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TransferenciaCota' table
	/// </summary>

	[Serializable]
	public partial class TransferenciaCota : esTransferenciaCota
	{
		public TransferenciaCota()
		{

		}
	
		public TransferenciaCota(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esTransferenciaCotaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TransferenciaCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TransferenciaCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TransferenciaCotaQuery query;
	}



	[Serializable]
	public partial class TransferenciaCotaQuery : esTransferenciaCotaQuery
	{
		public TransferenciaCotaQuery()
		{

		}		
		
		public TransferenciaCotaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TransferenciaCotaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TransferenciaCotaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.IdTransferencia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.IdTransferencia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.IdCotistaOrigem, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.IdCotistaOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.IdCotistaDestino, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.IdCotistaDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.DataTransferencia, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.DataTransferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.Tipo, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.IdPosicaoOrigem, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.IdPosicaoOrigem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.DataAplicacao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.Quantidade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaCotaMetadata.ColumnNames.Taxa, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TransferenciaCotaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TransferenciaCotaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotistaOrigem = "IdCotistaOrigem";
			 public const string IdCotistaDestino = "IdCotistaDestino";
			 public const string DataTransferencia = "DataTransferencia";
			 public const string Tipo = "Tipo";
			 public const string IdPosicaoOrigem = "IdPosicaoOrigem";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string Quantidade = "Quantidade";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotistaOrigem = "IdCotistaOrigem";
			 public const string IdCotistaDestino = "IdCotistaDestino";
			 public const string DataTransferencia = "DataTransferencia";
			 public const string Tipo = "Tipo";
			 public const string IdPosicaoOrigem = "IdPosicaoOrigem";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string Quantidade = "Quantidade";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TransferenciaCotaMetadata))
			{
				if(TransferenciaCotaMetadata.mapDelegates == null)
				{
					TransferenciaCotaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TransferenciaCotaMetadata.meta == null)
				{
					TransferenciaCotaMetadata.meta = new TransferenciaCotaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTransferencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotistaOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotistaDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataTransferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TransferenciaCota";
				meta.Destination = "TransferenciaCota";
				
				meta.spInsert = "proc_TransferenciaCotaInsert";				
				meta.spUpdate = "proc_TransferenciaCotaUpdate";		
				meta.spDelete = "proc_TransferenciaCotaDelete";
				meta.spLoadAll = "proc_TransferenciaCotaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TransferenciaCotaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TransferenciaCotaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
