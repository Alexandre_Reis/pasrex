/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/09/2015 15:18:39
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esColagemResgateMapaCotistaCollection : esEntityCollection
	{
		public esColagemResgateMapaCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemResgateMapaCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemResgateMapaCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemResgateMapaCotistaQuery);
		}
		#endregion
		
		virtual public ColagemResgateMapaCotista DetachEntity(ColagemResgateMapaCotista entity)
		{
			return base.DetachEntity(entity) as ColagemResgateMapaCotista;
		}
		
		virtual public ColagemResgateMapaCotista AttachEntity(ColagemResgateMapaCotista entity)
		{
			return base.AttachEntity(entity) as ColagemResgateMapaCotista;
		}
		
		virtual public void Combine(ColagemResgateMapaCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemResgateMapaCotista this[int index]
		{
			get
			{
				return base[index] as ColagemResgateMapaCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemResgateMapaCotista);
		}
	}



	[Serializable]
	abstract public class esColagemResgateMapaCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemResgateMapaCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemResgateMapaCotista()
		{

		}

		public esColagemResgateMapaCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemResgateMapaCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateMapaCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateMapaCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemResgateMapaCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateMapaCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateMapaCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemResgateMapaCotista)
		{
			esColagemResgateMapaCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemResgateMapaCotista == idColagemResgateMapaCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemResgateMapaCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemResgateMapaCotista",idColagemResgateMapaCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemResgateMapaCotista": this.str.IdColagemResgateMapaCotista = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DetalheImportado": this.str.DetalheImportado = (string)value; break;							
						case "IdOperacaoResgateVinculada": this.str.IdOperacaoResgateVinculada = (string)value; break;							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemResgateMapaCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateMapaCotista = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "IdOperacaoResgateVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgateVinculada = (System.Int32?)value;
							break;
						
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.IdColagemResgateMapaCotista
		/// </summary>
		virtual public System.Int32? IdColagemResgateMapaCotista
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateMapaCotista);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateMapaCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateMapaCotistaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateMapaCotistaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateMapaCotistaMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateMapaCotistaMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.DetalheImportado
		/// </summary>
		virtual public System.String DetalheImportado
		{
			get
			{
				return base.GetSystemString(ColagemResgateMapaCotistaMetadata.ColumnNames.DetalheImportado);
			}
			
			set
			{
				base.SetSystemString(ColagemResgateMapaCotistaMetadata.ColumnNames.DetalheImportado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.IdOperacaoResgateVinculada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgateVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaCotista.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemResgateMapaCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemResgateMapaCotista
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateMapaCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateMapaCotista = null;
					else entity.IdColagemResgateMapaCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DetalheImportado
			{
				get
				{
					System.String data = entity.DetalheImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DetalheImportado = null;
					else entity.DetalheImportado = Convert.ToString(value);
				}
			}
				
			public System.String IdOperacaoResgateVinculada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgateVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgateVinculada = null;
					else entity.IdOperacaoResgateVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
			

			private esColagemResgateMapaCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemResgateMapaCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemResgateMapaCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemResgateMapaCotista : esColagemResgateMapaCotista
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemResgateMapaCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateMapaCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemResgateMapaCotista
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateMapaCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DetalheImportado
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.DetalheImportado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdOperacaoResgateVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemResgateMapaCotistaCollection")]
	public partial class ColagemResgateMapaCotistaCollection : esColagemResgateMapaCotistaCollection, IEnumerable<ColagemResgateMapaCotista>
	{
		public ColagemResgateMapaCotistaCollection()
		{

		}
		
		public static implicit operator List<ColagemResgateMapaCotista>(ColagemResgateMapaCotistaCollection coll)
		{
			List<ColagemResgateMapaCotista> list = new List<ColagemResgateMapaCotista>();
			
			foreach (ColagemResgateMapaCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemResgateMapaCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateMapaCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemResgateMapaCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemResgateMapaCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemResgateMapaCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateMapaCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemResgateMapaCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemResgateMapaCotista AddNew()
		{
			ColagemResgateMapaCotista entity = base.AddNewEntity() as ColagemResgateMapaCotista;
			
			return entity;
		}

		public ColagemResgateMapaCotista FindByPrimaryKey(System.Int32 idColagemResgateMapaCotista)
		{
			return base.FindByPrimaryKey(idColagemResgateMapaCotista) as ColagemResgateMapaCotista;
		}


		#region IEnumerable<ColagemResgateMapaCotista> Members

		IEnumerator<ColagemResgateMapaCotista> IEnumerable<ColagemResgateMapaCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemResgateMapaCotista;
			}
		}

		#endregion
		
		private ColagemResgateMapaCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemResgateMapaCotista' table
	/// </summary>

	[Serializable]
	public partial class ColagemResgateMapaCotista : esColagemResgateMapaCotista
	{
		public ColagemResgateMapaCotista()
		{

		}
	
		public ColagemResgateMapaCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateMapaCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemResgateMapaCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateMapaCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemResgateMapaCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateMapaCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemResgateMapaCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemResgateMapaCotistaQuery query;
	}



	[Serializable]
	public partial class ColagemResgateMapaCotistaQuery : esColagemResgateMapaCotistaQuery
	{
		public ColagemResgateMapaCotistaQuery()
		{

		}		
		
		public ColagemResgateMapaCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemResgateMapaCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemResgateMapaCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateMapaCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.IdColagemResgateMapaCotista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.DetalheImportado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.DetalheImportado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.IdOperacaoResgateVinculada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.IdOperacaoResgateVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaCotistaMetadata.ColumnNames.IdColagemResgateDetalhe, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaCotistaMetadata.PropertyNames.IdColagemResgateDetalhe;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemResgateMapaCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemResgateMapaCotista = "IdColagemResgateMapaCotista";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemResgateMapaCotista = "IdColagemResgateMapaCotista";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemResgateMapaCotistaMetadata))
			{
				if(ColagemResgateMapaCotistaMetadata.mapDelegates == null)
				{
					ColagemResgateMapaCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemResgateMapaCotistaMetadata.meta == null)
				{
					ColagemResgateMapaCotistaMetadata.meta = new ColagemResgateMapaCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemResgateMapaCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DetalheImportado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdOperacaoResgateVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ColagemResgateMapaCotista";
				meta.Destination = "ColagemResgateMapaCotista";
				
				meta.spInsert = "proc_ColagemResgateMapaCotistaInsert";				
				meta.spUpdate = "proc_ColagemResgateMapaCotistaUpdate";		
				meta.spDelete = "proc_ColagemResgateMapaCotistaDelete";
				meta.spLoadAll = "proc_ColagemResgateMapaCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemResgateMapaCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemResgateMapaCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
