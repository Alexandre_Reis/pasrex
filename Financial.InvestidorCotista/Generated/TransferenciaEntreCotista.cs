/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/07/2015 10:29:01
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;

namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esTransferenciaEntreCotistaCollection : esEntityCollection
	{
		public esTransferenciaEntreCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TransferenciaEntreCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTransferenciaEntreCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTransferenciaEntreCotistaQuery);
		}
		#endregion
		
		virtual public TransferenciaEntreCotista DetachEntity(TransferenciaEntreCotista entity)
		{
			return base.DetachEntity(entity) as TransferenciaEntreCotista;
		}
		
		virtual public TransferenciaEntreCotista AttachEntity(TransferenciaEntreCotista entity)
		{
			return base.AttachEntity(entity) as TransferenciaEntreCotista;
		}
		
		virtual public void Combine(TransferenciaEntreCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TransferenciaEntreCotista this[int index]
		{
			get
			{
				return base[index] as TransferenciaEntreCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TransferenciaEntreCotista);
		}
	}



	[Serializable]
	abstract public class esTransferenciaEntreCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTransferenciaEntreCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTransferenciaEntreCotista()
		{

		}

		public esTransferenciaEntreCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTransferenciaEntreCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferenciaEntreCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferenciaEntreCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTransferenciaEntreCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferenciaEntreCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferenciaEntreCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTransferenciaEntreCotista)
		{
			esTransferenciaEntreCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTransferenciaEntreCotista == idTransferenciaEntreCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTransferenciaEntreCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTransferenciaEntreCotista",idTransferenciaEntreCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTransferenciaEntreCotista": this.str.IdTransferenciaEntreCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCotistaOrigem": this.str.IdCotistaOrigem = (string)value; break;							
						case "IdCotistaDestino": this.str.IdCotistaDestino = (string)value; break;							
						case "DataProcessamento": this.str.DataProcessamento = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdOperacaoDeposito": this.str.IdOperacaoDeposito = (string)value; break;							
						case "IdOperacaoRetirada": this.str.IdOperacaoRetirada = (string)value; break;							
						case "IdAplicacao": this.str.IdAplicacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTransferenciaEntreCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferenciaEntreCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCotistaOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotistaOrigem = (System.Int32?)value;
							break;
						
						case "IdCotistaDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotistaDestino = (System.Int32?)value;
							break;
						
						case "DataProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataProcessamento = (System.DateTime?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdOperacaoDeposito":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoDeposito = (System.Int32?)value;
							break;
						
						case "IdOperacaoRetirada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoRetirada = (System.Int32?)value;
							break;
						
						case "IdAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAplicacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdTransferenciaEntreCotista
		/// </summary>
		virtual public System.Int32? IdTransferenciaEntreCotista
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdCotistaOrigem
		/// </summary>
		virtual public System.Int32? IdCotistaOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaOrigem, value))
				{
					this._UpToCotistaByIdCotistaOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdCotistaDestino
		/// </summary>
		virtual public System.Int32? IdCotistaDestino
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino, value))
				{
					this._UpToCotistaByIdCotistaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.DataProcessamento
		/// </summary>
		virtual public System.DateTime? DataProcessamento
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaEntreCotistaMetadata.ColumnNames.DataProcessamento);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaEntreCotistaMetadata.ColumnNames.DataProcessamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdOperacaoDeposito
		/// </summary>
		virtual public System.Int32? IdOperacaoDeposito
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoDeposito);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoDeposito, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdOperacaoRetirada
		/// </summary>
		virtual public System.Int32? IdOperacaoRetirada
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoRetirada);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoRetirada, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaEntreCotista.IdAplicacao
		/// </summary>
		virtual public System.Int32? IdAplicacao
		{
			get
			{
				return base.GetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdAplicacao);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaEntreCotistaMetadata.ColumnNames.IdAplicacao, value))
				{
					this._UpToOperacaoCotistaByIdAplicacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotistaOrigem;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotistaDestino;
		[CLSCompliant(false)]
		internal protected OperacaoCotista _UpToOperacaoCotistaByIdAplicacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTransferenciaEntreCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTransferenciaEntreCotista
			{
				get
				{
					System.Int32? data = entity.IdTransferenciaEntreCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferenciaEntreCotista = null;
					else entity.IdTransferenciaEntreCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotistaOrigem
			{
				get
				{
					System.Int32? data = entity.IdCotistaOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotistaOrigem = null;
					else entity.IdCotistaOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotistaDestino
			{
				get
				{
					System.Int32? data = entity.IdCotistaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotistaDestino = null;
					else entity.IdCotistaDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String DataProcessamento
			{
				get
				{
					System.DateTime? data = entity.DataProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataProcessamento = null;
					else entity.DataProcessamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoDeposito
			{
				get
				{
					System.Int32? data = entity.IdOperacaoDeposito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoDeposito = null;
					else entity.IdOperacaoDeposito = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoRetirada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoRetirada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoRetirada = null;
					else entity.IdOperacaoRetirada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAplicacao
			{
				get
				{
					System.Int32? data = entity.IdAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAplicacao = null;
					else entity.IdAplicacao = Convert.ToInt32(value);
				}
			}
			

			private esTransferenciaEntreCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTransferenciaEntreCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTransferenciaEntreCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TransferenciaEntreCotista : esTransferenciaEntreCotista
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransEntreCotista_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotistaOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransEntreCotista_CotOri_FK
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotistaOrigem
		{
			get
			{
				if(this._UpToCotistaByIdCotistaOrigem == null
					&& IdCotistaOrigem != null					)
				{
					this._UpToCotistaByIdCotistaOrigem = new Cotista();
					this._UpToCotistaByIdCotistaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotistaOrigem", this._UpToCotistaByIdCotistaOrigem);
					this._UpToCotistaByIdCotistaOrigem.Query.Where(this._UpToCotistaByIdCotistaOrigem.Query.IdCotista == this.IdCotistaOrigem);
					this._UpToCotistaByIdCotistaOrigem.Query.Load();
				}

				return this._UpToCotistaByIdCotistaOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotistaOrigem");
				

				if(value == null)
				{
					this.IdCotistaOrigem = null;
					this._UpToCotistaByIdCotistaOrigem = null;
				}
				else
				{
					this.IdCotistaOrigem = value.IdCotista;
					this._UpToCotistaByIdCotistaOrigem = value;
					this.SetPreSave("UpToCotistaByIdCotistaOrigem", this._UpToCotistaByIdCotistaOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotistaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransEntreCotista_CotDes_FK
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotistaDestino
		{
			get
			{
				if(this._UpToCotistaByIdCotistaDestino == null
					&& IdCotistaDestino != null					)
				{
					this._UpToCotistaByIdCotistaDestino = new Cotista();
					this._UpToCotistaByIdCotistaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotistaDestino", this._UpToCotistaByIdCotistaDestino);
					this._UpToCotistaByIdCotistaDestino.Query.Where(this._UpToCotistaByIdCotistaDestino.Query.IdCotista == this.IdCotistaDestino);
					this._UpToCotistaByIdCotistaDestino.Query.Load();
				}

				return this._UpToCotistaByIdCotistaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotistaDestino");
				

				if(value == null)
				{
					this.IdCotistaDestino = null;
					this._UpToCotistaByIdCotistaDestino = null;
				}
				else
				{
					this.IdCotistaDestino = value.IdCotista;
					this._UpToCotistaByIdCotistaDestino = value;
					this.SetPreSave("UpToCotistaByIdCotistaDestino", this._UpToCotistaByIdCotistaDestino);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoCotistaByIdAplicacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TransEntreCotista_Operacao_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoCotista UpToOperacaoCotistaByIdAplicacao
		{
			get
			{
				if(this._UpToOperacaoCotistaByIdAplicacao == null
					&& IdAplicacao != null					)
				{
					this._UpToOperacaoCotistaByIdAplicacao = new OperacaoCotista();
					this._UpToOperacaoCotistaByIdAplicacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoCotistaByIdAplicacao", this._UpToOperacaoCotistaByIdAplicacao);
					this._UpToOperacaoCotistaByIdAplicacao.Query.Where(this._UpToOperacaoCotistaByIdAplicacao.Query.IdOperacao == this.IdAplicacao);
					this._UpToOperacaoCotistaByIdAplicacao.Query.Load();
				}

				return this._UpToOperacaoCotistaByIdAplicacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoCotistaByIdAplicacao");
				

				if(value == null)
				{
					this.IdAplicacao = null;
					this._UpToOperacaoCotistaByIdAplicacao = null;
				}
				else
				{
					this.IdAplicacao = value.IdOperacao;
					this._UpToOperacaoCotistaByIdAplicacao = value;
					this.SetPreSave("UpToOperacaoCotistaByIdAplicacao", this._UpToOperacaoCotistaByIdAplicacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoCotistaByIdAplicacao != null)
			{
				this.IdAplicacao = this._UpToOperacaoCotistaByIdAplicacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTransferenciaEntreCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaEntreCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTransferenciaEntreCotista
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotistaOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotistaDestino
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataProcessamento
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.DataProcessamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoDeposito
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoDeposito, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoRetirada
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoRetirada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAplicacao
		{
			get
			{
				return new esQueryItem(this, TransferenciaEntreCotistaMetadata.ColumnNames.IdAplicacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TransferenciaEntreCotistaCollection")]
	public partial class TransferenciaEntreCotistaCollection : esTransferenciaEntreCotistaCollection, IEnumerable<TransferenciaEntreCotista>
	{
		public TransferenciaEntreCotistaCollection()
		{

		}
		
		public static implicit operator List<TransferenciaEntreCotista>(TransferenciaEntreCotistaCollection coll)
		{
			List<TransferenciaEntreCotista> list = new List<TransferenciaEntreCotista>();
			
			foreach (TransferenciaEntreCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TransferenciaEntreCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaEntreCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TransferenciaEntreCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TransferenciaEntreCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TransferenciaEntreCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaEntreCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TransferenciaEntreCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TransferenciaEntreCotista AddNew()
		{
			TransferenciaEntreCotista entity = base.AddNewEntity() as TransferenciaEntreCotista;
			
			return entity;
		}

		public TransferenciaEntreCotista FindByPrimaryKey(System.Int32 idTransferenciaEntreCotista)
		{
			return base.FindByPrimaryKey(idTransferenciaEntreCotista) as TransferenciaEntreCotista;
		}


		#region IEnumerable<TransferenciaEntreCotista> Members

		IEnumerator<TransferenciaEntreCotista> IEnumerable<TransferenciaEntreCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TransferenciaEntreCotista;
			}
		}

		#endregion
		
		private TransferenciaEntreCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TransferenciaEntreCotista' table
	/// </summary>

	[Serializable]
	public partial class TransferenciaEntreCotista : esTransferenciaEntreCotista
	{
		public TransferenciaEntreCotista()
		{

		}
	
		public TransferenciaEntreCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaEntreCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esTransferenciaEntreCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaEntreCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TransferenciaEntreCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaEntreCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TransferenciaEntreCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TransferenciaEntreCotistaQuery query;
	}



	[Serializable]
	public partial class TransferenciaEntreCotistaQuery : esTransferenciaEntreCotistaQuery
	{
		public TransferenciaEntreCotistaQuery()
		{

		}		
		
		public TransferenciaEntreCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TransferenciaEntreCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TransferenciaEntreCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdTransferenciaEntreCotista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaOrigem, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdCotistaOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdCotistaDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.DataProcessamento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.DataProcessamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdPosicao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoDeposito, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdOperacaoDeposito;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdOperacaoRetirada, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdOperacaoRetirada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaEntreCotistaMetadata.ColumnNames.IdAplicacao, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaEntreCotistaMetadata.PropertyNames.IdAplicacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TransferenciaEntreCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTransferenciaEntreCotista = "IdTransferenciaEntreCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotistaOrigem = "IdCotistaOrigem";
			 public const string IdCotistaDestino = "IdCotistaDestino";
			 public const string DataProcessamento = "DataProcessamento";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacaoDeposito = "IdOperacaoDeposito";
			 public const string IdOperacaoRetirada = "IdOperacaoRetirada";
			 public const string IdAplicacao = "IdAplicacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTransferenciaEntreCotista = "IdTransferenciaEntreCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotistaOrigem = "IdCotistaOrigem";
			 public const string IdCotistaDestino = "IdCotistaDestino";
			 public const string DataProcessamento = "DataProcessamento";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacaoDeposito = "IdOperacaoDeposito";
			 public const string IdOperacaoRetirada = "IdOperacaoRetirada";
			 public const string IdAplicacao = "IdAplicacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TransferenciaEntreCotistaMetadata))
			{
				if(TransferenciaEntreCotistaMetadata.mapDelegates == null)
				{
					TransferenciaEntreCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TransferenciaEntreCotistaMetadata.meta == null)
				{
					TransferenciaEntreCotistaMetadata.meta = new TransferenciaEntreCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTransferenciaEntreCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotistaOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotistaDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataProcessamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoDeposito", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoRetirada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAplicacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TransferenciaEntreCotista";
				meta.Destination = "TransferenciaEntreCotista";
				
				meta.spInsert = "proc_TransferenciaEntreCotistaInsert";				
				meta.spUpdate = "proc_TransferenciaEntreCotistaUpdate";		
				meta.spDelete = "proc_TransferenciaEntreCotistaDelete";
				meta.spLoadAll = "proc_TransferenciaEntreCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TransferenciaEntreCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TransferenciaEntreCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
