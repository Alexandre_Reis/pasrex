/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/14/2015 4:14:58 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	









		
using Financial.Fundo;








					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.InvestidorCotista
{

	[Serializable]
	abstract public class esPrejuizoCotistaHistoricoCollection : esEntityCollection
	{
		public esPrejuizoCotistaHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PrejuizoCotistaHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPrejuizoCotistaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPrejuizoCotistaHistoricoQuery);
		}
		#endregion
		
		virtual public PrejuizoCotistaHistorico DetachEntity(PrejuizoCotistaHistorico entity)
		{
			return base.DetachEntity(entity) as PrejuizoCotistaHistorico;
		}
		
		virtual public PrejuizoCotistaHistorico AttachEntity(PrejuizoCotistaHistorico entity)
		{
			return base.AttachEntity(entity) as PrejuizoCotistaHistorico;
		}
		
		virtual public void Combine(PrejuizoCotistaHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PrejuizoCotistaHistorico this[int index]
		{
			get
			{
				return base[index] as PrejuizoCotistaHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PrejuizoCotistaHistorico);
		}
	}



	[Serializable]
	abstract public class esPrejuizoCotistaHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPrejuizoCotistaHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPrejuizoCotistaHistorico()
		{

		}

		public esPrejuizoCotistaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCotista, idCarteira, data);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCotista, idCarteira, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPrejuizoCotistaHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdCotista == idCotista, query.IdCarteira == idCarteira, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCotista, idCarteira, data);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCotista, idCarteira, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			esPrejuizoCotistaHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdCotista == idCotista, query.IdCarteira == idCarteira, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdCotista",idCotista);			parms.Add("IdCarteira",idCarteira);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "ValorPrejuizo": this.str.ValorPrejuizo = (string)value; break;							
						case "DataLimiteCompensacao": this.str.DataLimiteCompensacao = (string)value; break;							
						case "ValorPrejuizoOriginal": this.str.ValorPrejuizoOriginal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "ValorPrejuizo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPrejuizo = (System.Decimal?)value;
							break;
						
						case "DataLimiteCompensacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLimiteCompensacao = (System.DateTime?)value;
							break;
						
						case "ValorPrejuizoOriginal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPrejuizoOriginal = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.ValorPrejuizo
		/// </summary>
		virtual public System.Decimal? ValorPrejuizo
		{
			get
			{
				return base.GetSystemDecimal(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizo);
			}
			
			set
			{
				base.SetSystemDecimal(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizo, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.DataLimiteCompensacao
		/// </summary>
		virtual public System.DateTime? DataLimiteCompensacao
		{
			get
			{
				return base.GetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataLimiteCompensacao);
			}
			
			set
			{
				base.SetSystemDateTime(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataLimiteCompensacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PrejuizoCotistaHistorico.ValorPrejuizoOriginal
		/// </summary>
		virtual public System.Decimal? ValorPrejuizoOriginal
		{
			get
			{
				return base.GetSystemDecimal(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizoOriginal);
			}
			
			set
			{
				base.SetSystemDecimal(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizoOriginal, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPrejuizoCotistaHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorPrejuizo
			{
				get
				{
					System.Decimal? data = entity.ValorPrejuizo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPrejuizo = null;
					else entity.ValorPrejuizo = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataLimiteCompensacao
			{
				get
				{
					System.DateTime? data = entity.DataLimiteCompensacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLimiteCompensacao = null;
					else entity.DataLimiteCompensacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorPrejuizoOriginal
			{
				get
				{
					System.Decimal? data = entity.ValorPrejuizoOriginal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPrejuizoOriginal = null;
					else entity.ValorPrejuizoOriginal = Convert.ToDecimal(value);
				}
			}
			

			private esPrejuizoCotistaHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPrejuizoCotistaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPrejuizoCotistaHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PrejuizoCotistaHistorico : esPrejuizoCotistaHistorico
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_PrejuizoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_PrejuizoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPrejuizoCotistaHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PrejuizoCotistaHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorPrejuizo
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataLimiteCompensacao
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.DataLimiteCompensacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorPrejuizoOriginal
		{
			get
			{
				return new esQueryItem(this, PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizoOriginal, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PrejuizoCotistaHistoricoCollection")]
	public partial class PrejuizoCotistaHistoricoCollection : esPrejuizoCotistaHistoricoCollection, IEnumerable<PrejuizoCotistaHistorico>
	{
		public PrejuizoCotistaHistoricoCollection()
		{

		}
		
		public static implicit operator List<PrejuizoCotistaHistorico>(PrejuizoCotistaHistoricoCollection coll)
		{
			List<PrejuizoCotistaHistorico> list = new List<PrejuizoCotistaHistorico>();
			
			foreach (PrejuizoCotistaHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PrejuizoCotistaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrejuizoCotistaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PrejuizoCotistaHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PrejuizoCotistaHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PrejuizoCotistaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrejuizoCotistaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PrejuizoCotistaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PrejuizoCotistaHistorico AddNew()
		{
			PrejuizoCotistaHistorico entity = base.AddNewEntity() as PrejuizoCotistaHistorico;
			
			return entity;
		}

		public PrejuizoCotistaHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCotista, System.Int32 idCarteira, System.DateTime data)
		{
			return base.FindByPrimaryKey(dataHistorico, idCotista, idCarteira, data) as PrejuizoCotistaHistorico;
		}


		#region IEnumerable<PrejuizoCotistaHistorico> Members

		IEnumerator<PrejuizoCotistaHistorico> IEnumerable<PrejuizoCotistaHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PrejuizoCotistaHistorico;
			}
		}

		#endregion
		
		private PrejuizoCotistaHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PrejuizoCotistaHistorico' table
	/// </summary>

	[Serializable]
	public partial class PrejuizoCotistaHistorico : esPrejuizoCotistaHistorico
	{
		public PrejuizoCotistaHistorico()
		{

		}
	
		public PrejuizoCotistaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PrejuizoCotistaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPrejuizoCotistaHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrejuizoCotistaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PrejuizoCotistaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrejuizoCotistaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PrejuizoCotistaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PrejuizoCotistaHistoricoQuery query;
	}



	[Serializable]
	public partial class PrejuizoCotistaHistoricoQuery : esPrejuizoCotistaHistoricoQuery
	{
		public PrejuizoCotistaHistoricoQuery()
		{

		}		
		
		public PrejuizoCotistaHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PrejuizoCotistaHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PrejuizoCotistaHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.Data, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizo, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.ValorPrejuizo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.DataLimiteCompensacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.DataLimiteCompensacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrejuizoCotistaHistoricoMetadata.ColumnNames.ValorPrejuizoOriginal, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PrejuizoCotistaHistoricoMetadata.PropertyNames.ValorPrejuizoOriginal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PrejuizoCotistaHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorPrejuizo = "ValorPrejuizo";
			 public const string DataLimiteCompensacao = "DataLimiteCompensacao";
			 public const string ValorPrejuizoOriginal = "ValorPrejuizoOriginal";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorPrejuizo = "ValorPrejuizo";
			 public const string DataLimiteCompensacao = "DataLimiteCompensacao";
			 public const string ValorPrejuizoOriginal = "ValorPrejuizoOriginal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PrejuizoCotistaHistoricoMetadata))
			{
				if(PrejuizoCotistaHistoricoMetadata.mapDelegates == null)
				{
					PrejuizoCotistaHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PrejuizoCotistaHistoricoMetadata.meta == null)
				{
					PrejuizoCotistaHistoricoMetadata.meta = new PrejuizoCotistaHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorPrejuizo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataLimiteCompensacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorPrejuizoOriginal", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PrejuizoCotistaHistorico";
				meta.Destination = "PrejuizoCotistaHistorico";
				
				meta.spInsert = "proc_PrejuizoCotistaHistoricoInsert";				
				meta.spUpdate = "proc_PrejuizoCotistaHistoricoUpdate";		
				meta.spDelete = "proc_PrejuizoCotistaHistoricoDelete";
				meta.spLoadAll = "proc_PrejuizoCotistaHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PrejuizoCotistaHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PrejuizoCotistaHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
