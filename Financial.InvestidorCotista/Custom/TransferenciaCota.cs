﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;

namespace Financial.InvestidorCotista
{
	public partial class TransferenciaCota : esTransferenciaCota
	{
        /// <summary>
        /// Processa transferência entre cotistas. Pode ser parcial ou integral.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaTransferencia(int idCarteira, DateTime data)
        {
            TransferenciaCotaCollection transferenciaCotaCollection = new TransferenciaCotaCollection();
            transferenciaCotaCollection.Query.Where(transferenciaCotaCollection.Query.IdCarteira.Equal(idCarteira),
                                                    transferenciaCotaCollection.Query.DataTransferencia.Equal(data));
            transferenciaCotaCollection.Query.Load();

            if (transferenciaCotaCollection.Count > 0)
            {
                foreach (TransferenciaCota transferenciaCota in transferenciaCotaCollection)
                {
                    int idCotistaDe = transferenciaCota.IdCotistaOrigem.Value;
                    int idCotistaPara = transferenciaCota.IdCotistaDestino.Value;
                    byte tipo = transferenciaCota.Tipo.Value;
                    decimal? taxaTransferencia = transferenciaCota.Taxa;

                    if (tipo == (byte)TipoTransferencia.Integral)
                    {
                        #region Transfere Operacao, Posicao (Abertura e Hist tb), Prejuizo (Hist tb), DetalheResgate
                        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                              operacaoCotistaCollection.Query.IdCotista.Equal(idCotistaDe));
                        operacaoCotistaCollection.Query.Load();

                        foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                        {
                            operacaoCotista.IdCotista = idCotistaPara;
                        }

                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaCollection.Query.IdCotista.Equal(idCotistaDe));
                        posicaoCotistaCollection.Query.Load();

                        foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                        {
                            posicaoCotista.IdCotista = idCotistaPara;
                        }

                        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                        posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaAberturaCollection.Query.IdCotista.Equal(idCotistaDe));
                        posicaoCotistaAberturaCollection.Query.Load();

                        foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                        {
                            posicaoCotistaAbertura.IdCotista = idCotistaPara;
                        }

                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaHistoricoCollection.Query.IdCotista.Equal(idCotistaDe));
                        posicaoCotistaHistoricoCollection.Query.Load();

                        foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
                        {
                            posicaoCotistaHistorico.IdCotista = idCotistaPara;
                        }

                        DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
                        detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                                    detalheResgateCotistaCollection.Query.IdCotista.Equal(idCotistaDe));
                        detalheResgateCotistaCollection.Query.Load();

                        foreach (DetalheResgateCotista detalheResgateCotista in detalheResgateCotistaCollection)
                        {
                            detalheResgateCotista.IdCotista = idCotistaPara;
                        }

                        //As 2 tabelas abaixo não podem sofrer Update direto, precisa ser com Insert + Delete sucessivo
                        PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
                        prejuizoCotistaCollection.Query.Where(prejuizoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             prejuizoCotistaCollection.Query.IdCotista.Equal(idCotistaDe));
                        prejuizoCotistaCollection.Query.Load();

                        foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                        {
                            PrejuizoCotista prejuizoCotistaNovo = new PrejuizoCotista();
                            if (prejuizoCotistaNovo.LoadByPrimaryKey(idCotistaPara, idCarteira, data))
                            {
                                prejuizoCotistaNovo.ValorPrejuizo += prejuizoCotista.ValorPrejuizo.Value;
                            }
                            else
                            {
                                prejuizoCotistaNovo.IdCarteira = idCarteira;
                                prejuizoCotistaNovo.Data = prejuizoCotista.Data;
                                prejuizoCotistaNovo.DataLimiteCompensacao = prejuizoCotista.DataLimiteCompensacao;
                                prejuizoCotistaNovo.IdCotista = idCotistaPara;
                                prejuizoCotistaNovo.ValorPrejuizo = prejuizoCotista.ValorPrejuizo;
                                prejuizoCotistaNovo.ValorPrejuizoOriginal = prejuizoCotista.ValorPrejuizoOriginal;
                            }
                            prejuizoCotistaNovo.Save();
                        }

                        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
                        prejuizoCotistaHistoricoCollection.Query.Where(prejuizoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                             prejuizoCotistaHistoricoCollection.Query.IdCotista.Equal(idCotistaDe));
                        prejuizoCotistaHistoricoCollection.Query.Load();

                        foreach (PrejuizoCotistaHistorico prejuizoCotistaHistorico in prejuizoCotistaHistoricoCollection)
                        {
                            DateTime dataHistorico = prejuizoCotistaHistorico.DataHistorico.Value;
                            PrejuizoCotistaHistorico prejuizoCotistaHistoricoNovo = new PrejuizoCotistaHistorico();
                            if (prejuizoCotistaHistoricoNovo.LoadByPrimaryKey(dataHistorico, idCotistaPara, idCarteira, data))
                            {
                                prejuizoCotistaHistoricoNovo.ValorPrejuizo += prejuizoCotistaHistorico.ValorPrejuizo.Value;
                            }
                            else
                            {
                                prejuizoCotistaHistoricoNovo.DataHistorico = dataHistorico;
                                prejuizoCotistaHistoricoNovo.IdCarteira = idCarteira;
                                prejuizoCotistaHistoricoNovo.Data = prejuizoCotistaHistorico.Data;
                                prejuizoCotistaHistoricoNovo.DataLimiteCompensacao = prejuizoCotistaHistorico.DataLimiteCompensacao;
                                prejuizoCotistaHistoricoNovo.IdCotista = idCotistaPara;
                                prejuizoCotistaHistoricoNovo.ValorPrejuizo = prejuizoCotistaHistorico.ValorPrejuizo;
                                prejuizoCotistaHistoricoNovo.ValorPrejuizoOriginal = prejuizoCotistaHistorico.ValorPrejuizoOriginal;
                            }
                            prejuizoCotistaHistoricoNovo.Save();
                        }
                        

                        operacaoCotistaCollection.Save();
                        posicaoCotistaCollection.Save();
                        posicaoCotistaAberturaCollection.Save();
                        posicaoCotistaHistoricoCollection.Save();
                        detalheResgateCotistaCollection.Save();

                        prejuizoCotistaCollection.MarkAllAsDeleted();
                        prejuizoCotistaCollection.Save();
                        prejuizoCotistaHistoricoCollection.MarkAllAsDeleted();
                        prejuizoCotistaHistoricoCollection.Save();
                        #endregion
                    }
                    else if (tipo == (byte)TipoTransferencia.PosicoesQuantidade)
                    {
                        #region Transfere posições parciais
                        decimal quantidadeTransferencia = transferenciaCota.Quantidade.Value;                        

                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCotista.Equal(idCotistaDe),
                                                             posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaCollection.Query.Quantidade.NotEqual(0));
                        posicaoCotistaCollection.Query.OrderBy(posicaoCotistaCollection.Query.DataConversao.Ascending);
                        if (posicaoCotistaCollection.Query.Load())
                        {
                            decimal quantidadeResidual = quantidadeTransferencia;
                            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                            {
                                decimal quantidadePosicao = posicaoCotista.Quantidade.Value;

                                decimal quantidadeTransferir = 0;
                                if (quantidadeResidual > quantidadePosicao)
                                {
                                    quantidadeTransferir = quantidadePosicao;
                                }
                                else
                                {
                                    quantidadeTransferir = quantidadeResidual;
                                }

                                decimal fatorProporcao = quantidadeTransferir / posicaoCotista.Quantidade.Value;
                                
                                //Salva nova posição referente à quantidade da posição transferida
                                PosicaoCotista posicaoCotistaNova = new PosicaoCotista();
                                posicaoCotistaNova.IdOperacao = posicaoCotista.IdOperacao;
                                posicaoCotistaNova.IdCotista = idCotistaPara;
                                posicaoCotistaNova.IdCarteira = posicaoCotista.IdCarteira;
                                posicaoCotistaNova.ValorAplicacao = Math.Round(posicaoCotista.ValorAplicacao.Value * fatorProporcao, 2);
                                posicaoCotistaNova.DataAplicacao = posicaoCotista.DataAplicacao;
                                posicaoCotistaNova.DataConversao = posicaoCotista.DataConversao;
                                posicaoCotistaNova.DataConversao = posicaoCotista.DataConversao;
                                posicaoCotistaNova.CotaAplicacao = posicaoCotista.CotaAplicacao;
                                posicaoCotistaNova.CotaDia = posicaoCotista.CotaDia;
                                posicaoCotistaNova.ValorBruto = Math.Round(posicaoCotista.ValorBruto.Value * fatorProporcao, 2);
                                posicaoCotistaNova.ValorLiquido = Math.Round(posicaoCotista.ValorLiquido.Value * fatorProporcao, 2);
                                posicaoCotistaNova.QuantidadeInicial = Math.Round(posicaoCotista.QuantidadeInicial.Value * fatorProporcao, 2);
                                posicaoCotistaNova.Quantidade = quantidadeTransferir;
                                posicaoCotistaNova.QuantidadeBloqueada = 0;
                                posicaoCotistaNova.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
                                posicaoCotistaNova.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                                posicaoCotistaNova.ValorIR = Math.Round(posicaoCotista.ValorIR.Value * fatorProporcao, 2);
                                posicaoCotistaNova.ValorIOF = Math.Round(posicaoCotista.ValorIOF.Value * fatorProporcao, 2);
                                posicaoCotistaNova.ValorPerformance = Math.Round(posicaoCotista.ValorPerformance.Value * fatorProporcao, 2);
                                posicaoCotistaNova.ValorIOFVirtual = Math.Round(posicaoCotista.ValorIOFVirtual.Value * fatorProporcao, 2);
                                posicaoCotistaNova.QuantidadeAntesCortes = Math.Round(posicaoCotista.QuantidadeAntesCortes.Value * fatorProporcao, 8);
                                posicaoCotistaNova.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
                                posicaoCotistaNova.ValorRendimento = 0;
                                posicaoCotistaNova.Save();
                                //

                                //Atualiza quantidade e valores na posição de origem
                                posicaoCotista.Quantidade -= quantidadeTransferir;
                                posicaoCotista.QuantidadeInicial -= posicaoCotistaNova.QuantidadeInicial;
                                posicaoCotista.ValorAplicacao -= posicaoCotistaNova.ValorAplicacao;
                                posicaoCotista.ValorBruto -= posicaoCotistaNova.ValorBruto;
                                posicaoCotista.ValorLiquido -= posicaoCotistaNova.ValorLiquido;
                                posicaoCotista.ValorIR -= posicaoCotistaNova.ValorIR;
                                posicaoCotista.ValorIOF -= posicaoCotistaNova.ValorIOF;
                                posicaoCotista.ValorPerformance -= posicaoCotistaNova.ValorPerformance;
                                posicaoCotista.ValorIOFVirtual -= posicaoCotistaNova.ValorIOFVirtual;
                                posicaoCotista.QuantidadeAntesCortes -= posicaoCotistaNova.QuantidadeAntesCortes;
                                
                                quantidadeResidual -= quantidadeTransferir;
                            }

                            posicaoCotistaCollection.Save();
                        }
                        #endregion
                    }
                    else if (tipo == (byte)TipoTransferencia.PosicoesPercentual && taxaTransferencia.HasValue)
                    {
                        #region Transfere posições parciais
                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCotista.Equal(idCotistaDe),
                                                             posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaCollection.Query.Quantidade.NotEqual(0));
                        posicaoCotistaCollection.Query.Load();
                        foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                        {
                            decimal taxa = taxaTransferencia.Value;

                            int idPosicao = posicaoCotista.IdPosicao.Value;
                            decimal quantidadePosicao = posicaoCotista.Quantidade.Value;

                            PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
                            if (posicaoCotistaAbertura.LoadByPrimaryKey(idPosicao, data))
                            {                                
                                decimal quantidadeTransferir = Math.Round(posicaoCotistaAbertura.Quantidade.Value * taxa / 100M, 8);

                                if (quantidadeTransferir > quantidadePosicao)
                                {
                                    quantidadeTransferir = quantidadePosicao;
                                    taxa = 100;
                                }
                                
                                //Salva nova posição referente à quantidade da posição transferida
                                PosicaoCotista posicaoCotistaNova = new PosicaoCotista();
                                posicaoCotistaNova.IdOperacao = posicaoCotista.IdOperacao;
                                posicaoCotistaNova.IdCotista = idCotistaPara;
                                posicaoCotistaNova.IdCarteira = posicaoCotista.IdCarteira;
                                posicaoCotistaNova.ValorAplicacao = Math.Round(posicaoCotista.ValorAplicacao.Value * taxa / 100M, 2);
                                posicaoCotistaNova.DataAplicacao = posicaoCotista.DataAplicacao;
                                posicaoCotistaNova.DataConversao = posicaoCotista.DataConversao;
                                posicaoCotistaNova.DataConversao = posicaoCotista.DataConversao;
                                posicaoCotistaNova.CotaAplicacao = posicaoCotista.CotaAplicacao;
                                posicaoCotistaNova.CotaDia = posicaoCotista.CotaDia;
                                posicaoCotistaNova.ValorBruto = Math.Round(posicaoCotista.ValorBruto.Value * taxa / 100M, 2);
                                posicaoCotistaNova.ValorLiquido = Math.Round(posicaoCotista.ValorLiquido.Value * taxa / 100M, 2);
                                posicaoCotistaNova.QuantidadeInicial = Math.Round(posicaoCotista.QuantidadeInicial.Value * taxa / 100M, 2);
                                posicaoCotistaNova.Quantidade = quantidadeTransferir;
                                posicaoCotistaNova.QuantidadeBloqueada = 0;
                                posicaoCotistaNova.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
                                posicaoCotistaNova.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                                posicaoCotistaNova.ValorIR = Math.Round(posicaoCotista.ValorIR.Value * taxa / 100M, 2);
                                posicaoCotistaNova.ValorIOF = Math.Round(posicaoCotista.ValorIOF.Value * taxa / 100M, 2);
                                posicaoCotistaNova.ValorPerformance = Math.Round(posicaoCotista.ValorPerformance.Value * taxa / 100M, 2);
                                posicaoCotistaNova.ValorIOFVirtual = Math.Round(posicaoCotista.ValorIOFVirtual.Value * taxa / 100M, 2);
                                posicaoCotistaNova.QuantidadeAntesCortes = Math.Round(posicaoCotista.QuantidadeAntesCortes.Value * taxa / 100M, 8);
                                posicaoCotistaNova.ValorRendimento = 0;
                                posicaoCotistaNova.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
                                posicaoCotistaNova.Save();
                                //

                                //Atualiza quantidade e valores na posição de origem
                                posicaoCotista.Quantidade -= quantidadeTransferir;
                                posicaoCotista.QuantidadeInicial -= posicaoCotistaNova.QuantidadeInicial;
                                posicaoCotista.ValorAplicacao -= posicaoCotistaNova.ValorAplicacao;
                                posicaoCotista.ValorBruto -= posicaoCotistaNova.ValorBruto;
                                posicaoCotista.ValorLiquido -= posicaoCotistaNova.ValorLiquido;
                                posicaoCotista.ValorIR -= posicaoCotistaNova.ValorIR;
                                posicaoCotista.ValorIOF -= posicaoCotistaNova.ValorIOF;
                                posicaoCotista.ValorPerformance -= posicaoCotistaNova.ValorPerformance;
                                posicaoCotista.ValorIOFVirtual -= posicaoCotistaNova.ValorIOFVirtual;
                                posicaoCotista.QuantidadeAntesCortes -= posicaoCotistaNova.QuantidadeAntesCortes;
                            }
                                
                        }

                        posicaoCotistaCollection.Save();                    
                        #endregion
                    }
                }
            }
        }
	}
}
