﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/04/2015 16:05:01
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using Financial.Common.Enums;

namespace Financial.InvestidorCotista
{
	public partial class TransferenciaEntreCotistaCollection : esTransferenciaEntreCotistaCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteia"></param>
        /// <param name="data"></param>
        public void ProcessaTransferenciaEntreCotista(int idCarteira, DateTime data)
        {
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();            
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("posicao");
            TransferenciaEntreCotistaQuery transferenciaQuery = new TransferenciaEntreCotistaQuery("transferencia");            
            OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("operacao");

            #region Posição com Transferencia de série
            posicaoCotistaQuery.Select(posicaoCotistaQuery, transferenciaQuery, operacaoQuery.DataAgendamento, operacaoQuery.DataLiquidacao);           
            posicaoCotistaQuery.InnerJoin(transferenciaQuery).On(posicaoCotistaQuery.IdOperacao.Equal(transferenciaQuery.IdAplicacao));
            posicaoCotistaQuery.InnerJoin(operacaoQuery).On(transferenciaQuery.IdAplicacao.Equal(operacaoQuery.IdOperacao));
            posicaoCotistaQuery.Where(transferenciaQuery.DataProcessamento.Equal(data) & transferenciaQuery.IdCarteira.Equal(idCarteira));

            if (posicaoCotistaColl.Load(posicaoCotistaQuery))
            {
                operacaoCotistaColl = new OperacaoCotistaCollection();

                foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
                {
                    OperacaoCotista operacaoRetirada = new OperacaoCotista();
                    OperacaoCotista operacaoDeposito = new OperacaoCotista();

                    int idPosicao = Convert.ToInt32(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao));
                    int idCotistaOrigem = Convert.ToInt32(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdCotista));
                    int idCotistaDestino = Convert.ToInt32(posicaoCotista.GetColumn(TransferenciaEntreCotistaMetadata.ColumnNames.IdCotistaDestino));
                    int idTransferencia = Convert.ToInt32(posicaoCotista.GetColumn(TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista));
                    decimal valorBruto = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.ValorBruto));
                    decimal cotaDia = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaDia));
                    decimal qtde = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade));
                    DateTime dataAplicacao = Convert.ToDateTime(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao));
                    DateTime dataConversao = Convert.ToDateTime(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao));
                    DateTime dataLiquidacao = Convert.ToDateTime(posicaoCotista.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataLiquidacao));
                    DateTime dataAgendamento = Convert.ToDateTime(posicaoCotista.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataAgendamento));

                    #region Campos comuns                    
                    operacaoRetirada.IdCarteira = operacaoDeposito.IdCarteira = idCarteira;                    
                    operacaoRetirada.ValorBruto = operacaoDeposito.ValorBruto = valorBruto;
                    operacaoRetirada.Fonte = operacaoDeposito.Fonte = (int)FonteOperacaoCotista.TransferenciaEntreCotista;
                    operacaoRetirada.IdLocalNegociacao = operacaoDeposito.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
                    operacaoRetirada.DataConversao = operacaoDeposito.DataConversao = dataConversao;
                    operacaoRetirada.DataRegistro = operacaoDeposito.DataRegistro = data;
                    operacaoRetirada.DataOperacao = operacaoDeposito.DataOperacao = dataAplicacao;
                    operacaoRetirada.DataAgendamento = operacaoDeposito.DataLiquidacao = dataAgendamento;
                    operacaoRetirada.DataLiquidacao = operacaoDeposito.DataAgendamento = dataLiquidacao;
                    operacaoRetirada.Quantidade = operacaoDeposito.Quantidade = qtde;
                    #endregion

                    #region Retirada
                    operacaoRetirada.TipoOperacao = (int)TipoOperacaoCotista.ResgateCotasEspecial;
                    operacaoRetirada.IdPosicaoResgatada = idPosicao;
                    operacaoRetirada.TipoResgate = (int)TipoResgateCotista.Especifico;
                    operacaoRetirada.IdSeriesOffShore = null;                    
                    operacaoRetirada.CotaInformada = operacaoRetirada.CotaOperacao = Convert.ToDecimal(posicaoCotista.CotaAplicacao.Value);
                    operacaoRetirada.IdCotista = idCotistaOrigem;
                    #endregion

                    #region Deposito
                    operacaoDeposito.TipoOperacao = (int)TipoOperacaoCotista.AplicacaoCotasEspecial;
                    operacaoDeposito.IdSeriesOffShore = null;
                    operacaoDeposito.CotaInformada = operacaoDeposito.CotaOperacao =  Convert.ToDecimal(posicaoCotista.CotaAplicacao.Value);
                    operacaoDeposito.IdCotista = idCotistaDestino;
                    #endregion

                    operacaoRetirada.Save();
                    operacaoDeposito.Save();

                    TransferenciaEntreCotista transferencia = new TransferenciaEntreCotista();
                    transferencia.LoadByPrimaryKey(idTransferencia);
                    transferencia.IdOperacaoRetirada = operacaoRetirada.IdOperacao.Value;
                    transferencia.IdOperacaoDeposito = operacaoDeposito.IdOperacao.Value;
                    transferencia.IdAplicacao = posicaoCotista.IdOperacao.Value;
                    transferencia.Save();
                }
            }
            #endregion

            operacaoCotistaColl.Save();
        }
	}
}
