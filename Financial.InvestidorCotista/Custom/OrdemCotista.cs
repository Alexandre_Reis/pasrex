﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using Financial.Security;
using System.Configuration;
using Financial.Interfaces.Export;
using System.Web;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo.Enums;
using Financial.CRM;
using System.Net.Mail;

namespace Financial.InvestidorCotista
{
    public partial class OrdemCotista : esOrdemCotista
    {
        /// <summary>
        /// Carrega ordens para OperacaoCotista.
        /// Altera o status de Liberado para Processado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemCotista(int idCarteira, DateTime data)
        {
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
            ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                             ordemCotistaCollection.Query.DataOperacao.Equal(data),
                                             ordemCotistaCollection.Query.Status.Equal((byte)StatusOrdemCotista.Aprovado));
            ordemCotistaCollection.Query.Load();

            foreach (OrdemCotista ordemCotista in ordemCotistaCollection)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();
                operacaoCotista.DataAgendamento = ordemCotista.DataAgendamento;
                operacaoCotista.DataConversao = ordemCotista.DataConversao;
                operacaoCotista.DataLiquidacao = ordemCotista.DataLiquidacao;
                operacaoCotista.DataOperacao = ordemCotista.DataOperacao;
                operacaoCotista.DataRegistro = ordemCotista.DataOperacao;
                operacaoCotista.IdCarteira = ordemCotista.IdCarteira;
                operacaoCotista.IdCotista = ordemCotista.IdCotista;
                operacaoCotista.IdFormaLiquidacao = ordemCotista.IdFormaLiquidacao;
                operacaoCotista.IdPosicaoResgatada = ordemCotista.IdPosicaoResgatada;
                operacaoCotista.Observacao = ordemCotista.Observacao;
                operacaoCotista.Quantidade = ordemCotista.Quantidade;
                operacaoCotista.TipoOperacao = ordemCotista.TipoOperacao;
                operacaoCotista.TipoResgate = ordemCotista.TipoResgate;
                operacaoCotista.ValorBruto = ordemCotista.ValorBruto;
                operacaoCotista.ValorLiquido = ordemCotista.ValorLiquido;
                operacaoCotista.IdConta = ordemCotista.IdConta;
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.OrdemCotista;
                operacaoCotista.IdOrdem = ordemCotista.IdOperacao;
                operacaoCotista.IdTrader = ordemCotista.IdTrader;

                ordemCotista.Status = (byte)StatusOrdemCotista.Processado;
            }

            ordemCotistaCollection.Save();
            operacaoCotistaCollection.Save();
        }

        public bool IntegraItau(int tipoEmail)
        {
            string msgRetorno;
            return IntegraItau(tipoEmail, out msgRetorno);
        }

        public bool IntegraItau(int tipoEmail, out string msgRetorno)
        {
            msgRetorno = "";
            bool operacaoAgendada = this.DataOperacao.Value.Date > DateTime.Today;

            this.Status = (byte)StatusOrdemCotista.Digitado;
            this.StatusIntegracao = (byte)StatusOrdemIntegracao.Agendado;

            this.Save();

            bool success;
            if (operacaoAgendada)
            {
                //Registrar apenas agendamento
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OrdemCotista Agendada - Operacao: Insert OrdemCotista: " + this.IdOperacao,
                                                HttpContext.Current.User.Identity.Name,
                                                "",
                                                "",
                                                HistoricoLogOrigem.Outros);

                success = true;
            }
            else
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(this.IdCarteira.Value);

                int idAdministradorIntegraItau = Convert.ToInt32(ConfigurationManager.AppSettings["IdAdministradorIntegraItau"]);
                int idGestorIntegraItau = Convert.ToInt32(ConfigurationManager.AppSettings["IdGestorIntegraItau"]);

                bool enviarItau = (idAdministradorIntegraItau == carteira.IdAgenteAdministrador) && (idGestorIntegraItau == carteira.IdAgenteGestor);
                bool sucessoEnvioItau = false;
                bool erroSaldo = false;                

                if (enviarItau)
                {
                    Itau itau = new Itau();
                    try
                    {
                        msgRetorno = itau.EnviaEop_XML(this);
                    }
                    catch(Exception e)
                    {
                        string msgErro = "Ocorreu um erro genérico no envio desta operação para o Itaú. Detalhes do erro:" + e.Message +
                            " - " + e.InnerException + " - " + e.Source + " - " + e.StackTrace;
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        msgErro,
                                                        HttpContext.Current.User.Identity.Name,
                                                        "",
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                    }

                    sucessoEnvioItau = msgRetorno.ToUpper().Contains("OPERACAO+EFETUADA");

                    if (!sucessoEnvioItau)
                    {
                        erroSaldo = msgRetorno.ToUpper().Contains("SALDO");
                    }
                }

                if (sucessoEnvioItau || !enviarItau)
                {
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Portal Cliente DEBUG: 2108 sucessoEnvioItau || !enviarItau " + this.IdOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.Outros);

                    this.Status = (byte)StatusOrdemCotista.Aprovado;
                    this.StatusIntegracao = (byte)StatusOrdemIntegracao.ItauOK;

                    //this.CriaOrdemFundo(this);
                    this.CriaOrdemCarteiraAdministrada(this);

                    if (tipoEmail == 1)
                    {
                        this.EnviarEmailVarejo(this, HttpContext.Current, carteira);
                    }
                    else if (tipoEmail == 2)
                    {
                        this.EnviarEmailInstitucional(this, HttpContext.Current, carteira, (byte)SituacaoEnvio.Aprovacao);
                    }
                    success = true;

                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Portal Cliente DEBUG: Cadastro de OrdemCotista - Operacao: Insert OrdemCotista: " + this.IdOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                }
                else
                {
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Portal Cliente DEBUG: !sucessoEnvioItau && enviarItau " + this.IdOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.Outros);

                    this.Status = (byte)StatusOrdemCotista.Digitado;
                    this.StatusIntegracao = erroSaldo ? (byte)StatusOrdemIntegracao.SemSaldo : (byte)StatusOrdemIntegracao.OutroErro;
                    success = false;
                }
                this.Save();
            }

            return success;
        }

        public void CriaOrdemFundo(OrdemCotista ordemCotista)
        {
            #region Faz uma inserção tb em OperacaoFundo se for o caso

            //Operacoes de aplic/resgate em carteira adm não pode entrar!
            if (ordemCotista.IdCarteira.Value != ordemCotista.IdCotista.Value)
            {

                int idCliente = 0;
                Cliente clienteFundo = new Cliente();

                if (clienteFundo.LoadByPrimaryKey(ordemCotista.IdCotista.Value))
                {
                    if (ordemCotista.DataOperacao.Value > clienteFundo.DataDia.Value ||
                       (ordemCotista.DataOperacao.Value == clienteFundo.DataDia.Value && clienteFundo.Status != (byte)StatusCliente.Divulgado))
                    {
                        idCliente = clienteFundo.IdCliente.Value;
                        OperacaoFundo operacaoFundo = new OperacaoFundo();
                        operacaoFundo.DataAgendamento = ordemCotista.DataOperacao;
                        operacaoFundo.DataConversao = ordemCotista.DataConversao;
                        operacaoFundo.DataLiquidacao = ordemCotista.DataLiquidacao;
                        operacaoFundo.DataOperacao = ordemCotista.DataOperacao;
                        operacaoFundo.TipoOperacao = ordemCotista.TipoOperacao;
                        operacaoFundo.IdCarteira = ordemCotista.IdCarteira;
                        operacaoFundo.IdCliente = idCliente;
                        operacaoFundo.IdFormaLiquidacao = ordemCotista.IdFormaLiquidacao;
                        operacaoFundo.Observacao = ordemCotista.Observacao;
                        operacaoFundo.TipoResgate = ordemCotista.TipoResgate;


                        //Salva o valor dependendo do tipo de operação, o resto força zerado
                        //Resgate total força tudo zerado!
                        operacaoFundo.ValorBruto = ordemCotista.ValorBruto;
                        operacaoFundo.ValorLiquido = ordemCotista.ValorLiquido;
                        operacaoFundo.Quantidade = 0;

                        if (ordemCotista.IdPosicaoResgatada.HasValue)
                        {
                            PosicaoCotista posicaoCotista = new PosicaoCotista();
                            posicaoCotista.LoadByPrimaryKey(ordemCotista.IdPosicaoResgatada.Value);

                            DateTime dataConversao = posicaoCotista.DataConversao.Value;

                            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.Equal(ordemCotista.IdCarteira.Value),
                                                               posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.DataConversao.Equal(dataConversao));
                            posicaoFundoCollection.Query.Load();

                            if (posicaoFundoCollection.Count > 0)
                            {
                                operacaoFundo.IdPosicaoResgatada = posicaoFundoCollection[0].IdPosicao.Value;
                            }
                        }

                        operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;

                        operacaoFundo.Save();

                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Portal Cliente: Criacao OperacaoFundo " + this.IdOperacao,
                                                        HttpContext.Current.User.Identity.Name,
                                                        "",
                                                        "",
                                                        HistoricoLogOrigem.Outros);

                    }
                }
            }
            #endregion

        }

        public void CriaOrdemCarteiraAdministrada(OrdemCotista ordemCotista)
        {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Portal Cliente 2117: Início Criação Ordem na carteira administrada " + this.IdOperacao,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.Outros);

            #region Faz uma inserção tb em OperacaoFundo se for o caso

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(ordemCotista.IdCotista.Value);

            if (!cotista.IdClienteEspelho.HasValue)
            {
                return;
            }

            int? idCarteiraAdministrada = cotista.IdClienteEspelho;

            int idCliente = 0;
            Cliente clienteFundo = new Cliente();

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Portal Cliente 2112: Vou tentar carregar a carteira administrada de id: " + idCarteiraAdministrada.Value,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.Outros);

            if (clienteFundo.LoadByPrimaryKey(idCarteiraAdministrada.Value))
            {
                if (ordemCotista.DataOperacao.Value > clienteFundo.DataDia.Value ||
                   (ordemCotista.DataOperacao.Value == clienteFundo.DataDia.Value && clienteFundo.Status != (byte)StatusCliente.Divulgado))
                {
                    idCliente = clienteFundo.IdCliente.Value;
                    OperacaoFundo operacaoFundo = new OperacaoFundo();
                    operacaoFundo.DataAgendamento = ordemCotista.DataOperacao;
                    operacaoFundo.DataConversao = ordemCotista.DataConversao;
                    operacaoFundo.DataLiquidacao = ordemCotista.DataLiquidacao;
                    operacaoFundo.DataOperacao = ordemCotista.DataOperacao;
                    operacaoFundo.TipoOperacao = ordemCotista.TipoOperacao;
                    operacaoFundo.IdCarteira = ordemCotista.IdCarteira;
                    operacaoFundo.IdCliente = idCarteiraAdministrada;
                    operacaoFundo.IdFormaLiquidacao = ordemCotista.IdFormaLiquidacao;
                    operacaoFundo.Observacao = ordemCotista.Observacao;
                    operacaoFundo.TipoResgate = ordemCotista.TipoResgate;


                    //Salva o valor dependendo do tipo de operação, o resto força zerado
                    //Resgate total força tudo zerado!
                    operacaoFundo.ValorBruto = ordemCotista.ValorBruto;
                    operacaoFundo.ValorLiquido = ordemCotista.ValorLiquido;
                    operacaoFundo.Quantidade = 0;

                    if (ordemCotista.IdPosicaoResgatada.HasValue)
                    {
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        posicaoCotista.LoadByPrimaryKey(ordemCotista.IdPosicaoResgatada.Value);

                        DateTime dataConversao = posicaoCotista.DataConversao.Value;

                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.Equal(idCarteiraAdministrada),
                                                           posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.DataConversao.Equal(dataConversao));
                        posicaoFundoCollection.Query.Load();

                        if (posicaoFundoCollection.Count > 0)
                        {
                            operacaoFundo.IdPosicaoResgatada = posicaoFundoCollection[0].IdPosicao.Value;
                        }
                    }

                    operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;

                    operacaoFundo.Save();

                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Portal Cliente 2112: Criada Ordem para carteira administrada " + this.IdOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                }
                else
                {
                    historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Portal Cliente: NÃO foi Criada Ordem para carteira administrada " + this.IdOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                }
            }
            #endregion

        }

        public void EnviarEmailInstitucional(OrdemCotista ordemCotista, HttpContext context, Carteira carteira, byte situacaoEnvio)
        {
            int idCotista = ordemCotista.IdCotista.Value;
            int idCarteira = ordemCotista.IdCarteira.Value;
            DateTime dataOperacao = ordemCotista.DataOperacao.Value;
            byte tipoOperacao = ordemCotista.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;


            decimal valorBruto = ordemCotista.ValorBruto.Value;
            decimal valorLiquido = ordemCotista.ValorLiquido.Value;
            decimal valor = valorBruto > 0 ? valorBruto : valorLiquido;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            #region Avisar por Email
            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";
            PermissaoOperacaoFundo permissaoOperacaoFundo = new PermissaoOperacaoFundo();

            if (permissaoOperacaoFundo.LoadByPrimaryKey(idCotista, idCarteira) &&
                !String.IsNullOrEmpty(permissaoOperacaoFundo.Email) &&
                (permissaoOperacaoFundo.SituacaoEnvio.Value == (byte)SituacaoEnvio.Todas
                    || permissaoOperacaoFundo.SituacaoEnvio.Value == situacaoEnvio))
            {

                envieiEmail = true;

                const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "Boleta - \"{NomeCotista}\"";
                const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezados,<br /><br />Segue movimento do cotista: {NomeCotista} - CNPJ: {CpfCnpjCotista}<br /><br />Data da Solicitação: {Now}<br />Cotista: {NomeCotista}<br />Movimento: {TipoOperacao}<br />Valor: {Valor}<br />{TipoLiquidacao}Fundo: {NomeFundo}<br />Data Liquidação: {DataLiquidacao}<br />{ContaCorrenteLiquidacao}";
                const string FROM_DEFAULT = "suporte@financialonline.com.br";

                string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
                if (String.IsNullOrEmpty(subject))
                {
                    subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
                }

                string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
                if (String.IsNullOrEmpty(body))
                {
                    body = BODY_DEFAULT_CADASTRO_OPERACAO;
                }

                if (isResgateTotal)
                {
                    //Remover info de valor se for um Resgate Total
                    body = body.Replace("Valor: {Valor}<br />", "");
                }

                string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
                if (String.IsNullOrEmpty(from))
                {
                    from = FROM_DEFAULT;
                }

                MailMessage mail = new MailMessage();
                string sendTo = permissaoOperacaoFundo.Email.Replace(';', ',');
                mail.To.Add(sendTo);
                mail.From = new MailAddress(from);

                string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
        ToTitleCase(tipoOperacaoDescricao);

                string tipoLiquidacao = "";
                if (ordemCotista.IdFormaLiquidacao.HasValue)
                {
                    Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = ordemCotista.UpToFormaLiquidacaoByIdFormaLiquidacao;
                    tipoLiquidacao = String.Format("Tipo de Liquidação: {0}<br />", formaLiquidacao.Descricao);
                }



                string contaCorrenteLiquidacao = "";
                if (ordemCotista.IdConta.HasValue)
                {

                    Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                    contaCorrente.LoadByPrimaryKey(ordemCotista.IdConta.Value);

                    string bancoNome = "";
                    if (contaCorrente.IdBanco.HasValue)
                    {
                        Banco banco = new Banco();
                        banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                        bancoNome = banco.Nome;
                    }

                    string agenciaCodigo = "";
                    if (contaCorrente.IdAgencia.HasValue)
                    {
                        Agencia agencia = new Agencia();
                        agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                        agenciaCodigo = agencia.Codigo;
                    }

                    contaCorrenteLiquidacao = String.Format("<br />Banco: {0}<br />Ag.: {1}<br />C/C: {2}-{3}",
                        bancoNome, agenciaCodigo,
                        contaCorrente.Numero, contaCorrente.DigitoConta);
                }



                string[] placeholders = { "{TipoOperacao}", "{NomeFundo}", 
            "{DataOperacao}", "{Valor}", "{NomeCotista}", "{Now}", "{DataLiquidacao}",
                "{TipoLiquidacao}", "{ContaCorrenteLiquidacao}","{CpfCnpjCotista}"};


                string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, carteira.Nome,
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), 
                cotista.Nome, DateTime.Today.ToString("d/MM/yyyy"), 
                ordemCotista.DataLiquidacao.Value.ToString("d/MM/yyyy"), tipoLiquidacao, contaCorrenteLiquidacao, 
                pessoa.Cpfcnpj};


                for (int i = 0; i < placeholders.Length; i++)
                {
                    subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                    body = body.Replace(placeholders[i], placeholdersValues[i]);
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();

                /*if (from.Contains("@financialonline.com.br"))
                {
                    smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                    smtp.EnableSsl = true;
                }*/

                string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

                if (smtpEnableSSL == "true")
                {
                    smtp.EnableSsl = true;
                }

                try
                {
                    smtp.Send(mail);
                }
                catch (Exception e)
                {
                    errosSendMail = e.Message;
                    envieiEmail = false;
                }

            }
            #endregion

            string envieiEmailString = envieiEmail ? "Email enviado" : "Email não enviado - erro: " + errosSendMail;
            string descricaoHistorico = String.Format("Portal Cliente: Envio de email: {0} (Cotista: {1}, Fundo: {2})", envieiEmailString, idCotista, idCarteira);

            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            descricaoHistorico,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.Outros);

        }

        public void EnviarEmailVarejo(OrdemCotista ordemCotista, HttpContext context, Carteira carteira)
        {
            int idCotista = ordemCotista.IdCotista.Value;
            int idCarteira = ordemCotista.IdCarteira.Value;
            DateTime dataOperacao = ordemCotista.DataOperacao.Value;
            byte tipoOperacao = ordemCotista.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;

            string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
            string login = infoCotistaSplitted[2];

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(login);
            decimal valorBruto = ordemCotista.ValorBruto.Value;
            decimal valorLiquido = ordemCotista.ValorLiquido.Value;
            decimal valor = valorBruto > 0 ? valorBruto : valorLiquido;

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";
            PermissaoOperacaoFundo permissaoOperacaoFundo = new PermissaoOperacaoFundo();
            if (permissaoOperacaoFundo.LoadByPrimaryKey(idCotista, idCarteira) &&
                !String.IsNullOrEmpty(permissaoOperacaoFundo.Email))
            {
                envieiEmail = true;

                const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "Boleta - \"{0}\"";
                const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezado Cliente,<br /><br />Sua solicitação foi finalizada no Portal do Investidor. Detalhes da operação:<br /><br /><br />Tipo da Operação: {TipoOperacao}<br />Fundo: {NomeFundo}<br />Valor: {Valor}<br />Data: {DataOperacao}<br /><br />Atenciosamente<br /><br />Porto Seguro Investimento<br /><br />Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).";
                const string FROM_DEFAULT = "suporte@financialonline.com.br";

                string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
                if (String.IsNullOrEmpty(subject))
                {
                    subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
                }

                string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
                if (String.IsNullOrEmpty(body))
                {
                    body = BODY_DEFAULT_CADASTRO_OPERACAO;
                }

                if (isResgateTotal)
                {
                    //Remover info de valor se for um Resgate Total
                    body = body.Replace("Valor: {Valor}<br />", "");
                }

                string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
                if (String.IsNullOrEmpty(from))
                {
                    from = FROM_DEFAULT;
                }

                MailMessage mail = new MailMessage();
                string sendTo = permissaoOperacaoFundo.Email.Replace(';', ',');
                mail.To.Add(sendTo);
                mail.From = new MailAddress(from);

                string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
        ToTitleCase(tipoOperacaoDescricao);

                string[] placeholders = { "{TipoOperacao}", "{NomeUsuario}", "{NomeFundo}", 
            "{DataOperacao}", "{Valor}", "{NomeCotista}"};

                string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, usuario.Nome, carteira.Nome,
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), cotista.Nome};


                for (int i = 0; i < placeholders.Length; i++)
                {
                    subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                    body = body.Replace(placeholders[i], placeholdersValues[i]);
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();

                /*if (from.Contains("@financialonline.com.br"))
                {
                    smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                    smtp.EnableSsl = true;
                }*/

                string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

                if (smtpEnableSSL == "true")
                {
                    smtp.EnableSsl = true;
                }

                try
                {
                    smtp.Send(mail);
                }
                catch (Exception e)
                {
                    errosSendMail = e.Message;
                    envieiEmail = false;
                }

            }


            string envieiEmailString = envieiEmail ? "Email enviado" : "Email não enviado - erro: " + errosSendMail;
            string descricaoHistorico = String.Format("Portal Cliente: Envio de email: {0} (Cotista: {1}, Fundo: {2})", envieiEmailString, idCotista, idCarteira);

            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            descricaoHistorico,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.Outros);

        }

        public void EnviarEmailBackOffice(OrdemCotista ordemCotista, HttpContext context, Carteira carteira, string descricaoTriggerEmail, bool isTesouraria)
        {
            string emailBackOffice = "";
            if (isTesouraria)
            {
                emailBackOffice = Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional;
            }
            else
            {
                emailBackOffice = Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOffice;
            }

            if (String.IsNullOrEmpty(emailBackOffice))
            {
                return;
            }

            string sendTo = emailBackOffice.Replace(';', ',');

            int idCotista = ordemCotista.IdCotista.Value;
            int idCarteira = ordemCotista.IdCarteira.Value;
            DateTime dataOperacao = ordemCotista.DataOperacao.Value;
            byte tipoOperacao = ordemCotista.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;

            decimal valorBruto = ordemCotista.ValorBruto.Value;
            decimal valorLiquido = ordemCotista.ValorLiquido.Value;
            decimal valor = valorBruto > 0 ? valorBruto : valorLiquido;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            #region Avisar por Email
            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";
            PermissaoOperacaoFundo permissaoOperacaoFundo = new PermissaoOperacaoFundo();

            envieiEmail = true;

            const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "{DescricaoTriggerEmail} - \"{NomeCotista}\"";
            const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezados,<br /><br />Segue movimento do cotista: {NomeCotista} - CNPJ: {CpfCnpjCotista}<br /><br />Data da Solicitação: {Now}<br />Cotista: {NomeCotista}<br />Movimento: {TipoOperacao}<br />Valor: {Valor}<br />{TipoLiquidacao}Fundo: {NomeFundo}<br />Data Liquidação: {DataLiquidacao}<br />{ContaCorrenteLiquidacao}";
            const string FROM_DEFAULT = "suporte@financialonline.com.br";

            string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
            if (String.IsNullOrEmpty(subject))
            {
                subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
            }

            string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
            if (String.IsNullOrEmpty(body))
            {
                body = BODY_DEFAULT_CADASTRO_OPERACAO;
            }

            if (isResgateTotal)
            {
                //Remover info de valor se for um Resgate Total
                body = body.Replace("Valor: {Valor}<br />", "");
            }

            string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
            if (String.IsNullOrEmpty(from))
            {
                from = FROM_DEFAULT;
            }

            MailMessage mail = new MailMessage();

            mail.To.Add(sendTo);
            mail.From = new MailAddress(from);

            string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
    ToTitleCase(tipoOperacaoDescricao);

            string tipoLiquidacao = "";
            if (ordemCotista.IdFormaLiquidacao.HasValue)
            {
                Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = ordemCotista.UpToFormaLiquidacaoByIdFormaLiquidacao;
                tipoLiquidacao = String.Format("Tipo de Liquidação: {0}<br />", formaLiquidacao.Descricao);
            }

            string contaCorrenteLiquidacao = "";
            if (ordemCotista.IdConta.HasValue)
            {

                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                contaCorrente.LoadByPrimaryKey(ordemCotista.IdConta.Value);

                string bancoNome = "";
                if (contaCorrente.IdBanco.HasValue)
                {
                    Banco banco = new Banco();
                    banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                    bancoNome = banco.Nome;
                }

                string agenciaCodigo = "";
                if (contaCorrente.IdAgencia.HasValue)
                {
                    Agencia agencia = new Agencia();
                    agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                    agenciaCodigo = agencia.Codigo;
                }

                contaCorrenteLiquidacao = String.Format("<br />Banco: {0}<br />Ag.: {1}<br />C/C: {2}-{3}",
                    bancoNome, agenciaCodigo,
                    contaCorrente.Numero, contaCorrente.DigitoConta);
            }



            string[] placeholders = { "{TipoOperacao}", "{NomeFundo}", 
            "{DataOperacao}", "{Valor}", "{NomeCotista}", "{Now}", "{DataLiquidacao}",
                "{TipoLiquidacao}", "{ContaCorrenteLiquidacao}","{CpfCnpjCotista}", "{DescricaoTriggerEmail}"};


            string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, carteira.Nome,
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), 
                cotista.Nome, DateTime.Today.ToString("d/MM/yyyy"), 
                ordemCotista.DataLiquidacao.Value.ToString("d/MM/yyyy"), tipoLiquidacao, contaCorrenteLiquidacao, 
                pessoa.Cpfcnpj, descricaoTriggerEmail};


            for (int i = 0; i < placeholders.Length; i++)
            {
                subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                body = body.Replace(placeholders[i], placeholdersValues[i]);
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            /*if (from.Contains("@financialonline.com.br"))
            {
                smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                smtp.EnableSsl = true;
            }*/

            string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

            if (smtpEnableSSL == "true")
            {
                smtp.EnableSsl = true;
            }

            try
            {
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                errosSendMail = e.Message;
                envieiEmail = false;
            }
            #endregion

        }
    }
}
