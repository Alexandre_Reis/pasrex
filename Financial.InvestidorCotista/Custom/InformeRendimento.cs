﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo.Enums;
using Financial.CRM;

namespace Financial.InvestidorCotista.Custom
{
    public class InformeRendimento
    {
        public int Ano;
        public string Tipo;
        public int IdFundo;
        public string CodFundo;
        public string NomeFundo;
        public int IdCotista;       
        public string CodCotista;
        public string NomeCotista;
        public DateTime DataSaldo;
        public decimal QuantidadeCotas;
        public decimal SaldoFinanceiro;
        public string CpfcnpjCotista;       
        public string CodigoInterfaceCotista;
        public string CpfcnpjCarteira;
        private DateTime DataReferencia; 

        public List<InformeRendimento> RetornaSaldos(int? idCarteiraParam, int? idCotistaParam, DateTime data)
        {
            DataReferencia = Calendario.RetornaUltimoDiaUtilMes(data);
            return RetornaSaldos(idCarteiraParam, idCotistaParam);
        }
        public List<InformeRendimento> RetornaSaldos(int? idCarteiraParam, int? idCotistaParam, int ano)
        {
            DataReferencia = Calendario.RetornaUltimoDiaUtilAno(new DateTime(ano, 12, 01));
            return RetornaSaldos(idCarteiraParam, idCotistaParam);
        }

        private List<InformeRendimento> RetornaSaldos(int? idCarteiraParam, int? idCotistaParam)
        {
            List<InformeRendimento> lista = new List<InformeRendimento>();

            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            carteiraQuery.Select(carteiraQuery.IdCarteira, 
                                 carteiraQuery.Nome, 
                                 carteiraQuery.TipoTributacao,
                                 carteiraQuery.TipoCota,
                                 clienteQuery.IdTipo);
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.Where(clienteQuery.IdTipo.In(TipoClienteFixo.Fundo, TipoClienteFixo.Clube),
                                clienteQuery.TipoControle.In((byte)TipoControleCliente.Completo, (byte)TipoControleCliente.Cotista));
            if (idCarteiraParam.HasValue)
            {
                carteiraQuery.Where(carteiraQuery.IdCarteira.Equal(idCarteiraParam.Value));
            }

            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);

            foreach (Carteira carteira in carteiraCollection)
            {
                int idCarteira = carteira.IdCarteira.Value;
                string nomeCarteira = carteira.Nome;
                byte tipoTributacao = carteira.TipoTributacao.Value;
                byte tipoCota = carteira.TipoCota.Value;
                int idTipo = Convert.ToInt32(carteira.GetColumn(ClienteMetadata.ColumnNames.IdTipo));

                string descricaoTipo = idTipo == TipoClienteFixo.Clube ? "Clube" : "Fundo";

                string codigoInterfaceCarteira = "";
                ClienteInterface clienteInterface = new ClienteInterface();
                if (clienteInterface.LoadByPrimaryKey(idCarteira) && !String.IsNullOrEmpty(clienteInterface.CodigoYMF))
                {
                    codigoInterfaceCarteira = clienteInterface.CodigoYMF;
                }

                Pessoa pessoaCarteira = new Pessoa();
                pessoaCarteira.LoadByPrimaryKey(carteira.IdCarteira.Value);
                string cpfcnpjCarteira = pessoaCarteira.Cpfcnpj;

                if (tipoTributacao == (byte)TipoTributacaoFundo.Acoes)
                {
                    #region Trata tributação Ações
                    PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                    CotistaQuery cotistaQuery = new CotistaQuery("C");
                    posicaoCotistaHistoricoQuery.Select(cotistaQuery.Nome,
                                                        cotistaQuery.IdCotista,
                                                        cotistaQuery.CodigoInterface,
                                                        posicaoCotistaHistoricoQuery.Quantidade.Sum(),
                                                        (posicaoCotistaHistoricoQuery.Quantidade * posicaoCotistaHistoricoQuery.CotaAplicacao).As("SaldoPosicao").Sum());
                    posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
                    posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira == idCarteira,
                                                       posicaoCotistaHistoricoQuery.DataHistorico == DataReferencia);

                    if (idCotistaParam.HasValue)
                    {
                        posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista == idCotistaParam.Value);
                    }

                    posicaoCotistaHistoricoQuery.GroupBy(cotistaQuery.Nome,
                                                         cotistaQuery.IdCotista,
                                                         cotistaQuery.CodigoInterface);
                    posicaoCotistaHistoricoQuery.OrderBy(cotistaQuery.Nome.Ascending);

                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

                    foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
	                {
                        int idCotista = Convert.ToInt32(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.IdCotista));
                        string nomeCotista = Convert.ToString(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.Nome));
                        string codigoInterfaceCotista = "";
                        if (!Convert.IsDBNull(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface)))
                        {
                            codigoInterfaceCotista = Convert.ToString(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface));
                        }

                        Pessoa pessoaCotista = new Pessoa();
                        pessoaCotista.LoadByPrimaryKey(idCotista);
                        string cpfcnpjCotista = pessoaCotista.Cpfcnpj;

                        decimal quantidadeCotista = posicaoCotistaHistorico.Quantidade.Value;
                        decimal saldoCotista = (decimal)posicaoCotistaHistorico.GetColumn("SaldoPosicao");

                        InformeRendimento informeRendimento = new InformeRendimento();
                        informeRendimento.Ano = DataReferencia.Year;
                        informeRendimento.CodCotista = codigoInterfaceCotista;
                        informeRendimento.CodFundo = codigoInterfaceCarteira;
                        informeRendimento.DataSaldo = DataReferencia;
                        informeRendimento.IdCotista = idCotista;
                        informeRendimento.IdFundo = idCarteira;
                        informeRendimento.NomeCotista = nomeCotista;
                        informeRendimento.NomeFundo = nomeCarteira;
                        informeRendimento.QuantidadeCotas = quantidadeCotista;
                        informeRendimento.SaldoFinanceiro = saldoCotista;
                        informeRendimento.Tipo = descricaoTipo;
                        informeRendimento.CpfcnpjCarteira = cpfcnpjCarteira;
                        informeRendimento.CpfcnpjCotista = cpfcnpjCotista;
                        informeRendimento.CodigoInterfaceCotista = codigoInterfaceCotista;

                        lista.Add(informeRendimento);
                    }
                    #endregion
                }
                else
                {
                    #region Trata tributação Fundos com comecotas
                    PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQueryPrincipal = new PosicaoCotistaHistoricoQuery("P");
                    CotistaQuery cotistaQueryPrincipal = new CotistaQuery("O");

                    posicaoCotistaHistoricoQueryPrincipal.Select(cotistaQueryPrincipal.Nome,
                                                                 cotistaQueryPrincipal.IdCotista,
                                                                 cotistaQueryPrincipal.CodigoInterface);
                    posicaoCotistaHistoricoQueryPrincipal.InnerJoin(cotistaQueryPrincipal).On(cotistaQueryPrincipal.IdCotista == posicaoCotistaHistoricoQueryPrincipal.IdCotista);
                    posicaoCotistaHistoricoQueryPrincipal.Where(posicaoCotistaHistoricoQueryPrincipal.IdCarteira.Equal(idCarteira),
                                                                posicaoCotistaHistoricoQueryPrincipal.DataHistorico.Equal(DataReferencia));

                    if (idCotistaParam.HasValue)
                    {
                        posicaoCotistaHistoricoQueryPrincipal.Where(posicaoCotistaHistoricoQueryPrincipal.IdCotista.Equal(idCotistaParam.Value));
                    }

                    posicaoCotistaHistoricoQueryPrincipal.GroupBy(cotistaQueryPrincipal.Nome,
                                                                 cotistaQueryPrincipal.IdCotista,
                                                                 cotistaQueryPrincipal.CodigoInterface);
                    posicaoCotistaHistoricoQueryPrincipal.OrderBy(cotistaQueryPrincipal.Nome.Ascending);
                                        
                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollectionPrincipal = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollectionPrincipal.Load(posicaoCotistaHistoricoQueryPrincipal);

                    foreach (PosicaoCotistaHistorico posicaoCotistaHistoricoPrincipal in posicaoCotistaHistoricoCollectionPrincipal)
                    {
                        int idCotista = Convert.ToInt32(posicaoCotistaHistoricoPrincipal.GetColumn(CotistaMetadata.ColumnNames.IdCotista));
                        string nomeCotista = Convert.ToString(posicaoCotistaHistoricoPrincipal.GetColumn(CotistaMetadata.ColumnNames.Nome));
                        string codigoInterfaceCotista = "";
                        if (!Convert.IsDBNull(posicaoCotistaHistoricoPrincipal.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface)))
                        {
                            codigoInterfaceCotista = Convert.ToString(posicaoCotistaHistoricoPrincipal.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface));
                        }

                        Pessoa pessoaCotista = new Pessoa();
                        pessoaCotista.LoadByPrimaryKey(idCotista);
                        string cpfcnpjCotista = pessoaCotista.Cpfcnpj;

                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataConversao,
                                                                       posicaoCotistaHistoricoCollection.Query.DataUltimaCobrancaIR,
                                                                       posicaoCotistaHistoricoCollection.Query.Quantidade,
                                                                       posicaoCotistaHistoricoCollection.Query.CotaAplicacao,
                                                                       posicaoCotistaHistoricoCollection.Query.CotaDia);
                        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                                      posicaoCotistaHistoricoCollection.Query.DataHistorico.Equal(DataReferencia),
                                                                      posicaoCotistaHistoricoCollection.Query.IdCotista.Equal(idCotista));
                        posicaoCotistaHistoricoCollection.Query.Load();

                        decimal saldoCotista = 0;
                        decimal quantidadeCotista = 0;
                        foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
                        {
                            #region Computa saldo do cotista
                            DateTime dataConversao = posicaoCotistaHistorico.DataConversao.Value;
                            DateTime dataUltimaCobrancaIR = posicaoCotistaHistorico.DataUltimaCobrancaIR.Value;
                            decimal quantidadePosicao = posicaoCotistaHistorico.Quantidade.Value;
                            decimal cotaAplicacao = posicaoCotistaHistorico.CotaAplicacao.Value;
                            decimal cotaDia = posicaoCotistaHistorico.CotaDia.Value;

                            quantidadeCotista += quantidadePosicao;

                            decimal cotaCusto = 0;
                            decimal saldoPosicao = 0;
                            if (dataUltimaCobrancaIR > dataConversao)
                            {
                                HistoricoCota historicoCota = new HistoricoCota();
                                historicoCota.Query.Select(historicoCota.Query.CotaAbertura,
                                                           historicoCota.Query.CotaFechamento);
                                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                                          historicoCota.Query.Data.Equal(dataUltimaCobrancaIR));
                                historicoCota.Query.Load();

                                if (tipoCota == (byte)TipoCotaFundo.Abertura)
                                {
                                    cotaCusto = historicoCota.CotaAbertura.Value;
                                }
                                else
                                {
                                    if (historicoCota.CotaFechamento.HasValue)
                                    {
                                        cotaCusto = historicoCota.CotaFechamento.Value;
                                    }
                                }
                            }
                            else
                            {
                                cotaCusto = cotaAplicacao;
                            }

                            saldoPosicao = quantidadePosicao * cotaCusto;

                            saldoCotista += saldoPosicao;
                            #endregion
                        }

                        InformeRendimento informeRendimento = new InformeRendimento();
                        informeRendimento.Ano = DataReferencia.Year; 
                        informeRendimento.CodCotista = codigoInterfaceCotista;
                        informeRendimento.CodFundo = codigoInterfaceCarteira;
                        informeRendimento.DataSaldo = DataReferencia;
                        informeRendimento.IdCotista = idCotista;
                        informeRendimento.IdFundo = idCarteira;
                        informeRendimento.NomeCotista = nomeCotista;
                        informeRendimento.NomeFundo = nomeCarteira;
                        informeRendimento.QuantidadeCotas = quantidadeCotista;
                        informeRendimento.SaldoFinanceiro = saldoCotista;
                        informeRendimento.Tipo = descricaoTipo;
                        informeRendimento.CpfcnpjCarteira = cpfcnpjCarteira;
                        informeRendimento.CpfcnpjCotista = cpfcnpjCotista;
                        informeRendimento.CodigoInterfaceCotista = codigoInterfaceCotista;

                        lista.Add(informeRendimento);
                    }                                        
                    #endregion
                }
            }

            return lista;
        }


    }
}
