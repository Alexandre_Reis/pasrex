﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
    public partial class PosicaoCotistaHistoricoCollection : esPosicaoCotistaHistoricoCollection
    {
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Deleta posições de cotista históricas.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoCotistaHistoricoDataHistoricoMaiorIgual(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta posições de cotista históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoCotistaHistoricoDataHistoricoMaior(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoCotistaHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoCotistaHistoricoCompleta(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCarteira == idCarteira,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
        }

        public void ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicaoNotaArray,
            DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF)
        {

            foreach (Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota in posicaoNotaArray)
            {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                if (posicaoCotistaHistorico.ImportaPosicaoNotaYMF(posicaoNota, 
                    dataImplantacao, mappingOperacaoFinancialYMF, mappingPosicaoFinancialYMF))
                {
                    posicaoCotistaHistorico.Save();
                }
            }

        }

        public void ImportaPosicaoNotaHistoricaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicaoNotaArray)
        {

            foreach (Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota in posicaoNotaArray)
            {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                if (posicaoCotistaHistorico.ImportaPosicaoNotaHistoricaYMF(posicaoNota))
                {
                    posicaoCotistaHistorico.Save();
                }
            }

        }

    }
}
