using System;
using System.Collections.Generic;
using System.Text;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.CRM;
using Financial.Util;
using Financial.CRM.Enums;

namespace Financial.InvestidorCotista.Custom
{
    public class SimulacaoResgate
    {
        public int idFundo;
        public int idCotista;
        public string nomeFundo;
        public string nomeCotista;
        public string cnpjFundo;
        public string cpfCnpjCotista;        
        public decimal cotaOperacao;        
        public decimal rendimento;
        public decimal quantidade;
        public decimal valorIR;
        public decimal valorIOF;
        public decimal valorBruto;
        public decimal valorLiquido;

        public SimulacaoResgate(int idFundo, int idCotista, TipoOperacaoCotista tipoOperacaoCotista, TipoResgateCotista tipoResgate, decimal valor)
        {

            this.idCotista = idCotista;
            this.idFundo = idFundo;

            Pessoa pessoaFundo = new Pessoa();
            pessoaFundo.LoadByPrimaryKey(idFundo);

            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(idCotista);

            this.nomeFundo = pessoaFundo.Nome;
            this.cnpjFundo = Utilitario.MascaraCNPJ(pessoaFundo.Cpfcnpj);

            this.nomeCotista = pessoaCotista.Nome;

            if (pessoaCotista.Tipo.Value == (byte)TipoPessoa.Fisica)
            {
                this.cpfCnpjCotista = Utilitario.MascaraCPF(pessoaCotista.Cpfcnpj);
            }
            else
            {
                this.cpfCnpjCotista = Utilitario.MascaraCNPJ(pessoaCotista.Cpfcnpj);
            }


            #region Montagem do OrderBy ->> colunas = "(ValorIR + ValorIOF) / Quantidade)"
            StringBuilder colunas = new StringBuilder();
            string stringValorIR = PosicaoCotistaMetadata.ColumnNames.ValorIR;
            string stringValorIOF = PosicaoCotistaMetadata.ColumnNames.ValorIOF;
            string stringQuantidade = PosicaoCotistaMetadata.ColumnNames.Quantidade;
            colunas = colunas.Append("<( [").Append(stringValorIR).Append("] + [").Append(stringValorIOF).Append("] )").Append("/ [")
                             .Append(stringQuantidade).Append("] As Impostos>");
            #endregion

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.CotaAplicacao, 
                posicaoCotistaCollection.Query.CotaDia,
                posicaoCotistaCollection.Query.DataAplicacao,
                posicaoCotistaCollection.Query.DataConversao,
                posicaoCotistaCollection.Query.DataUltimaCobrancaIR,
                posicaoCotistaCollection.Query.DataUltimoCortePfee,
                posicaoCotistaCollection.Query.IdCarteira,
                posicaoCotistaCollection.Query.IdCotista,
                posicaoCotistaCollection.Query.IdOperacao,
                posicaoCotistaCollection.Query.IdPosicao,
                posicaoCotistaCollection.Query.PosicaoIncorporada,
                posicaoCotistaCollection.Query.Quantidade,
                posicaoCotistaCollection.Query.QuantidadeAntesCortes,
                posicaoCotistaCollection.Query.QuantidadeBloqueada,
                posicaoCotistaCollection.Query.QuantidadeInicial,
                posicaoCotistaCollection.Query.ValorAplicacao,
                posicaoCotistaCollection.Query.ValorBruto,
                posicaoCotistaCollection.Query.ValorIOF,
                posicaoCotistaCollection.Query.ValorIOFVirtual,
                posicaoCotistaCollection.Query.ValorIR,
                posicaoCotistaCollection.Query.ValorLiquido,
                posicaoCotistaCollection.Query.ValorPerformance,
                posicaoCotistaCollection.Query.ValorRendimento,
                colunas.ToString());
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idFundo),
                                                 posicaoCotistaCollection.Query.IdCotista.Equal(idCotista),
                                                 posicaoCotistaCollection.Query.Quantidade.NotEqual(0));

            if (tipoResgate == TipoResgateCotista.FIFO)
            {
                posicaoCotistaCollection.Query.OrderBy(posicaoCotistaCollection.Query.DataConversao.Ascending);
            }
            else if (tipoResgate == TipoResgateCotista.LIFO)
            {
                posicaoCotistaCollection.Query.OrderBy(posicaoCotistaCollection.Query.DataConversao.Ascending);
            }
            else
            {
                posicaoCotistaCollection.Query.OrderBy("Impostos", esOrderByDirection.Ascending);
            }

            posicaoCotistaCollection.Query.Load();

            decimal valorResidual = valor;
            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                decimal saldoBruto = posicaoCotista.ValorBruto.Value;
                decimal saldoLiquido = posicaoCotista.ValorLiquido.Value;
                decimal saldoQuantidade = posicaoCotista.Quantidade.Value;
                decimal saldoIR = posicaoCotista.ValorIR.Value;
                decimal saldoIOF = posicaoCotista.ValorIOF.Value;
                decimal cotaAplicacao = posicaoCotista.CotaAplicacao.Value;
                decimal cotaDia = posicaoCotista.CotaDia.Value;
                
                this.cotaOperacao = cotaDia;
                
                if (tipoOperacaoCotista == TipoOperacaoCotista.ResgateBruto)
                {
                    if (valorResidual > saldoBruto)
                    {
                        valorResidual -= saldoBruto;
                        this.valorBruto += saldoBruto;

                        this.valorIR += saldoIR;
                        this.valorIOF += saldoIOF;
                        this.valorLiquido += saldoLiquido;
                        this.quantidade += saldoQuantidade;
                    }
                    else
                    {
                        this.valorBruto += valorResidual;

                        this.valorIR += valorResidual / saldoBruto * saldoIR;
                        this.valorIOF += valorResidual / saldoBruto * saldoIOF;
                        this.valorLiquido += valorResidual / saldoBruto * saldoLiquido;
                        this.quantidade += valorResidual / saldoBruto * saldoQuantidade;
                        valorResidual = 0;
                    }

                    this.rendimento += this.quantidade * (cotaDia - cotaAplicacao);

                    if (valorResidual == 0)
                    {
                        break;
                    }
                }
                else //Resgate Liquido
                {
                    if (valorResidual > saldoLiquido)
                    {
                        valorResidual -= saldoLiquido;
                        this.valorLiquido += saldoLiquido;

                        this.valorIR += saldoIR;
                        this.valorIOF += saldoIOF;
                        this.valorLiquido += saldoLiquido;
                        this.quantidade += saldoQuantidade;
                    }
                    else
                    {
                        this.valorLiquido += valorResidual;

                        this.valorIR += valorResidual / saldoLiquido * saldoIR;
                        this.valorIOF += valorResidual / saldoLiquido * saldoIOF;
                        this.valorBruto += valorResidual / saldoLiquido * saldoBruto;
                        this.quantidade += valorResidual / saldoLiquido * saldoQuantidade;
                        valorResidual = 0;
                    }
                }

            }

        }
    }
}
