/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/06/2015 16:48:30
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{
	public partial class PosicaoCotistaAlterada : esPosicaoCotistaAlterada
	{
        public List<PosicaoCotistaAlterada> RetornaLista(int idCarteira, DateTime data, int? idPosicao)
        {
            List<PosicaoCotistaAlterada> lstPosicaoCotistaAlterada = new List<PosicaoCotistaAlterada>();
            PosicaoCotistaAlteradaCollection posicaoCotistaAlteradaColl = new PosicaoCotistaAlteradaCollection();
            
            posicaoCotistaAlteradaColl.Query.Where(posicaoCotistaAlteradaColl.Query.Data.Equal(data) & posicaoCotistaAlteradaColl.Query.IdCarteira.Equal(idCarteira));

            if (idPosicao.HasValue)
                posicaoCotistaAlteradaColl.Query.Where(posicaoCotistaAlteradaColl.Query.IdPosicao.Equal(idPosicao.Value));

            if (posicaoCotistaAlteradaColl.Query.Load())
            {
                lstPosicaoCotistaAlterada = (List<PosicaoCotistaAlterada>)posicaoCotistaAlteradaColl;
            }

            return lstPosicaoCotistaAlterada;
        }
	}
}
