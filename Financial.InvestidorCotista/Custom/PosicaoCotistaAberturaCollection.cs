﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
    public partial class PosicaoCotistaAberturaCollection : esPosicaoCotistaAberturaCollection
    {
        /// <summary>
        /// Deleta posições de cotista históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoCotistaAberturaDataHistoricoMaior(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoCotistaAbertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoCotistaAberturaCompleta(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCarteira == idCarteira,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
        }

        public void ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicaoNotaArray,
            DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF )
        {

            foreach (Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota in posicaoNotaArray)
            {
                PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
                if (posicaoCotistaAbertura.ImportaPosicaoNotaYMF(posicaoNota, 
                    dataImplantacao, mappingOperacaoFinancialYMF, mappingPosicaoFinancialYMF))
                {
                    posicaoCotistaAbertura.Save();
                }
            }
        }

        public void ImportaPosicaoNotaHistoricaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicaoNotaArray)
        {

            foreach (Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota in posicaoNotaArray)
            {
                PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
                if (posicaoCotistaAbertura.ImportaPosicaoNotaHistoricaYMF(posicaoNota))
                {
                    posicaoCotistaAbertura.Save();
                }
            }
        }

    }
}
