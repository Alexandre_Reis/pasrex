using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{
    public partial class PrejuizoCotistaUsado : esPrejuizoCotistaUsado
	{
        /// <summary>
        /// Retorna o SUM de PrejuizoUsado, baseado na consulta IdCotista.Equal(idCotista), IdCarteiraCompensada.Equal(idCarteiraCompensada), DataOriginal.Equal(dataOriginal).
        /// </summary>
        /// <param name="idCotista"></param>
        /// <param name="idCarteiraCompensar"></param>
        /// <param name="dataPrejuizo"></param>
        /// <returns></returns>
        public decimal RetornaPrejuizoUsado(int idCotista, int idCarteiraCompensada, DateTime dataOriginal)
        {
            PrejuizoCotistaUsado prejuizoCotistaUsadoHistorico = new PrejuizoCotistaUsado();
            prejuizoCotistaUsadoHistorico.Query.Select(prejuizoCotistaUsadoHistorico.Query.PrejuizoUsado.Sum());
            prejuizoCotistaUsadoHistorico.Query.Where(prejuizoCotistaUsadoHistorico.Query.IdCotista.Equal(idCotista),
                                                     prejuizoCotistaUsadoHistorico.Query.IdCarteiraCompensada.Equal(idCarteiraCompensada),
                                                     prejuizoCotistaUsadoHistorico.Query.DataOriginal.Equal(dataOriginal));
            prejuizoCotistaUsadoHistorico.Query.Load();

            return prejuizoCotistaUsadoHistorico.PrejuizoUsado.HasValue? prejuizoCotistaUsadoHistorico.PrejuizoUsado.Value: 0;
        }
	}
}
