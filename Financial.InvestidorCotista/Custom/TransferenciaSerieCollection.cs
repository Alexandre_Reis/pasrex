﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/03/2015 14:47:16
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using Financial.Common.Enums;
using System.Collections;
using Financial.Fundo;

namespace Financial.InvestidorCotista
{
	public partial class TransferenciaSerieCollection : esTransferenciaSerieCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteia"></param>
        /// <param name="data"></param>
        public void ProcessaTransferenciaSerie(int idCarteia, DateTime data)
        {
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
            SeriesOffShoreCollection serieOffShoreColl = new SeriesOffShoreCollection();
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("posicao");
            TransferenciaSerieQuery transferenciaSerieQuery = new TransferenciaSerieQuery("transferencia");
            List<SeriesOffShore> lstSeriesOffShore = new List<SeriesOffShore>();
            OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("operacao");

            #region Carrega Séries OffShore
            serieOffShoreColl.Query.Where(serieOffShoreColl.Query.IdClassesOffShore.Equal(idCarteia));

            if (serieOffShoreColl.Query.Load())
                lstSeriesOffShore = (List<SeriesOffShore>)serieOffShoreColl;
            #endregion

            #region Posição com Transferencia de série
            posicaoCotistaQuery.Select(posicaoCotistaQuery, transferenciaSerieQuery, operacaoQuery.DataAgendamento, operacaoQuery.DataLiquidacao);
            posicaoCotistaQuery.InnerJoin(operacaoQuery).On(posicaoCotistaQuery.IdOperacao.Equal(operacaoQuery.IdOperacao));
            posicaoCotistaQuery.InnerJoin(transferenciaSerieQuery).On(posicaoCotistaQuery.IdPosicao.Equal(transferenciaSerieQuery.IdPosicao)
                                                                      & transferenciaSerieQuery.IdEventoRollUp.IsNull());
            posicaoCotistaQuery.Where(transferenciaSerieQuery.DataPosicao.Equal(data) & transferenciaSerieQuery.IdCarteira.Equal(idCarteia));

            if (posicaoCotistaColl.Load(posicaoCotistaQuery))
            {
                operacaoCotistaColl = new OperacaoCotistaCollection();

                foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
                {
                    OperacaoCotista operacaoRetirada = new OperacaoCotista();
                    OperacaoCotista operacaoDeposito = new OperacaoCotista();

                    #region Busca Série destino
                    int idSerieDestino = Convert.ToInt32(posicaoCotista.GetColumn(TransferenciaSerieMetadata.ColumnNames.IdSerieDestino));
                    int idSerieOrigem = Convert.ToInt32(posicaoCotista.GetColumn(TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem));
                    int idTransferencia = Convert.ToInt32(posicaoCotista.GetColumn(TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie));
                    SeriesOffShore serieDestino = lstSeriesOffShore.Find(delegate(SeriesOffShore x) { return x.IdSeriesOffShore == idSerieDestino; });
                    #endregion

                    this.PopulaOperacaoDepositoRetirada(idCarteia, data, idTransferencia, idSerieOrigem, idSerieDestino, serieDestino.VlCotaInicial.Value, posicaoCotista, ref operacaoRetirada, ref operacaoDeposito);

                    operacaoCotistaColl.AttachEntity(operacaoRetirada);
                    operacaoCotistaColl.AttachEntity(operacaoDeposito);
                }
            }
            #endregion

            operacaoCotistaColl.Save();
        }

        /// <summary>
        /// Gera registro 
        /// </summary>
        /// <param name="idCarteia"></param>
        /// <param name="data"></param>
        public void GeraTransferenciaRollUp(int idCarteia, DateTime data, EventoRollUp eventoRollUp)
        {
            #region Objetos
            List<int> lstPosicoesAfetadas = new List<int>();
            List<int> lstSeriesAfetadas = new List<int>();
            OperacaoCotista operacaoRetirada = new OperacaoCotista();
            OperacaoCotista operacaoDeposito = new OperacaoCotista();
            PosicaoCotistaAberturaCollection posicaoAberturaColl = new PosicaoCotistaAberturaCollection();
            PosicaoCotistaCollection posicaoColl = new PosicaoCotistaCollection();
            PosicaoCotistaAberturaQuery posicaoAberturaQuery = new PosicaoCotistaAberturaQuery("posicaoAbertura");
            EventoRollUpQuery eventoQuery = new EventoRollUpQuery("rollUp");
            SeriesOffShoreQuery serieQuery = new SeriesOffShoreQuery("serie");
            OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("operacao");
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            #endregion

            posicaoAberturaQuery.Select(posicaoAberturaQuery, operacaoQuery.DataAgendamento, operacaoQuery.DataLiquidacao);
            posicaoAberturaQuery.InnerJoin(operacaoQuery).On(posicaoAberturaQuery.IdOperacao.Equal(operacaoQuery.IdOperacao));
            posicaoAberturaQuery.Where(posicaoAberturaQuery.DataHistorico.Equal(eventoRollUp.DataNav.Value)
                                       & posicaoAberturaQuery.IdSeriesOffShore.Equal(eventoRollUp.IdSerieOrigem.Value));

            if (posicaoAberturaColl.Load(posicaoAberturaQuery))
            {
                decimal vlCotaInicial = eventoRollUp.UpToSeriesOffShoreByIdSerieOrigem.VlCotaInicial.Value;

                #region Gera todos os depositos e retiradas com base nos valores da data NAV
                foreach (PosicaoCotistaAbertura posicaoAbertura in posicaoAberturaColl)
                {
                    lstPosicoesAfetadas.Add(posicaoAbertura.IdPosicao.Value);

                    #region Grava Transferencia de Série
                    TransferenciaSerie transferenciaSerie = new TransferenciaSerie();
                    transferenciaSerie.DataPosicao = eventoRollUp.DataNav;
                    transferenciaSerie.DataExecucao = eventoRollUp.DataExecucao;
                    transferenciaSerie.IdCarteira = idCarteia;
                    transferenciaSerie.IdPosicao = posicaoAbertura.IdPosicao.Value;
                    transferenciaSerie.IdSerieDestino = eventoRollUp.IdSerieDestino.Value;
                    transferenciaSerie.IdSerieOrigem = eventoRollUp.IdSerieOrigem.Value;
                    transferenciaSerie.IdEventoRollUp = eventoRollUp.IdEventoRollUp.Value;
                    transferenciaSerie.Save();
                    #endregion

                    #region Popula Operações 
                    operacaoRetirada = new OperacaoCotista();
                    operacaoDeposito = new OperacaoCotista();

                    this.PopulaOperacaoDepositoRetirada(idCarteia, data, transferenciaSerie.IdTransferenciaSerie.Value, transferenciaSerie.IdSerieOrigem.Value, transferenciaSerie.IdSerieDestino.Value, vlCotaInicial, posicaoAbertura, ref operacaoRetirada, ref operacaoDeposito);

                    operacaoCotistaColl.AttachEntity(operacaoRetirada);
                    operacaoCotistaColl.AttachEntity(operacaoDeposito);
                    #endregion
                }
                #endregion

                #region Gera retiradas de qualquer posição gerada após a data NAV
                posicaoColl.Query.Where(posicaoColl.Query.IdCarteira.Equal(idCarteia)
                                        & posicaoColl.Query.IdSeriesOffShore.In(eventoRollUp.IdSerieOrigem.Value)
                                        & posicaoColl.Query.IdPosicao.NotIn(lstPosicoesAfetadas));

                if (posicaoColl.Query.Load())
                {
                    foreach (PosicaoCotista posicao in posicaoColl)
                    {
                        #region Grava Transferencia de Série
                        TransferenciaSerie transferenciaSerie = new TransferenciaSerie();
                        transferenciaSerie.DataPosicao = eventoRollUp.DataNav;
                        transferenciaSerie.DataExecucao = eventoRollUp.DataExecucao;
                        transferenciaSerie.IdCarteira = idCarteia;
                        transferenciaSerie.IdPosicao = posicao.IdPosicao.Value;
                        transferenciaSerie.IdSerieDestino = eventoRollUp.IdSerieDestino.Value;
                        transferenciaSerie.IdSerieOrigem = eventoRollUp.IdSerieOrigem.Value;
                        transferenciaSerie.IdEventoRollUp = eventoRollUp.IdEventoRollUp.Value;
                        transferenciaSerie.Save();
                        #endregion

                        #region Popula Operações
                        operacaoRetirada = new OperacaoCotista();
                        operacaoDeposito = new OperacaoCotista();

                        this.PopulaOperacaoDepositoRetirada(idCarteia, data, transferenciaSerie.IdTransferenciaSerie.Value, transferenciaSerie.IdSerieOrigem.Value, transferenciaSerie.IdSerieDestino.Value, vlCotaInicial, posicao, ref operacaoRetirada, ref operacaoDeposito);

                        operacaoCotistaColl.AttachEntity(operacaoRetirada);
                        operacaoCotistaColl.AttachEntity(operacaoDeposito);
                        #endregion
                    }
                }
                #endregion
            }

            operacaoCotistaColl.Save();
        }

        private void PopulaOperacaoDepositoRetirada(int idCarteia, DateTime data, int idTransferencia, int idSerieOrigem, int idSerieDestino, decimal valorCotaInicialSerie, esEntity posicaoCotista, ref OperacaoCotista operacaoRetirada, ref OperacaoCotista operacaoDeposito)
        {
            int idPosicao = Convert.ToInt32(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao));
            int idCotista = Convert.ToInt32(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdCotista));
            decimal valorBruto = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.ValorBruto));
            decimal cotaDia = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaDia));
            decimal qtde = Convert.ToDecimal(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade));
            DateTime dataAplicacao = Convert.ToDateTime(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao));
            DateTime dataConversao = Convert.ToDateTime(posicaoCotista.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao));
            DateTime dataLiquidacao = Convert.ToDateTime(posicaoCotista.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataLiquidacao));
            DateTime dataAgendamento = Convert.ToDateTime(posicaoCotista.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataAgendamento));

            #region Campos comuns
            operacaoRetirada.IdTransferenciaSerie = operacaoDeposito.IdTransferenciaSerie = idTransferencia;
            operacaoRetirada.IdCarteira = operacaoDeposito.IdCarteira = idCarteia;
            operacaoRetirada.IdCotista = operacaoDeposito.IdCotista = idCotista;
            operacaoRetirada.ValorBruto = operacaoDeposito.ValorBruto = valorBruto;
            operacaoRetirada.Fonte = operacaoDeposito.Fonte = (int)FonteOperacaoCotista.TransferenciaSerie;
            operacaoRetirada.IdLocalNegociacao = operacaoDeposito.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
            operacaoRetirada.DataConversao = operacaoDeposito.DataConversao = dataConversao;
            operacaoRetirada.DataRegistro = operacaoDeposito.DataRegistro = data;          
            operacaoRetirada.DataOperacao = operacaoDeposito.DataOperacao = dataAplicacao;
            operacaoRetirada.DataAgendamento = operacaoDeposito.DataLiquidacao = dataAgendamento;
            operacaoRetirada.DataLiquidacao = operacaoDeposito.DataAgendamento = dataLiquidacao;
            #endregion

            #region Retirada
            operacaoRetirada.TipoOperacao = (int)TipoOperacaoCotista.ResgateCotasEspecial;
            operacaoRetirada.IdPosicaoResgatada = idPosicao;
            operacaoRetirada.TipoResgate = (int)TipoResgateCotista.Especifico;
            operacaoRetirada.IdSeriesOffShore = idSerieOrigem;
            operacaoRetirada.Quantidade = qtde;
            operacaoRetirada.CotaInformada = operacaoRetirada.CotaOperacao = cotaDia;
            #endregion

            #region Deposito
            operacaoDeposito.TipoOperacao = (int)TipoOperacaoCotista.AplicacaoCotasEspecial;
            operacaoDeposito.IdSeriesOffShore = idSerieDestino;
            if (valorCotaInicialSerie != 0)
            {
                operacaoDeposito.CotaInformada = operacaoDeposito.CotaOperacao = valorCotaInicialSerie;
                operacaoDeposito.Quantidade = operacaoDeposito.ValorBruto / valorCotaInicialSerie;
            }
            #endregion
        }
	}
}
