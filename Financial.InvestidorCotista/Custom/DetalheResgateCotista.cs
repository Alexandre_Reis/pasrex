﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           3/5/2007 13:46:45
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
	public partial class DetalheResgateCotista : esDetalheResgateCotista
	{
        public bool ImportaResgateYMF(Financial.Interfaces.Import.YMF.ResgateYMF resgateYMF,
            MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF, int countResgate)
        {

            if (mappingOperacaoFinancialYMF.ContainsKey(resgateYMF.IdNota))
            {
                this.IdOperacao = mappingOperacaoFinancialYMF[resgateYMF.IdNota];
            }
            else
            {
                //Pode ocorrer da OperacaoCotista nao ter sido importada (por exemplo quando os tipos de Operacao nao sao suportados)
                return false;
            }

            if (mappingPosicaoFinancialYMF != null && mappingPosicaoFinancialYMF.ContainsKey(resgateYMF.IdNotaAplicacao))
            {
                this.IdPosicaoResgatada = mappingPosicaoFinancialYMF[resgateYMF.IdNotaAplicacao];
            }
            else
            {
                if (mappingOperacaoFinancialYMF.ContainsKey(resgateYMF.IdNotaAplicacao))
                {
                    this.IdPosicaoResgatada = mappingOperacaoFinancialYMF[resgateYMF.IdNotaAplicacao];
                }
                else
                {
                    this.IdPosicaoResgatada = countResgate; //colocar qualquer numero, mas nao repetido... usar o count
                }
            }

            OperacaoCotista operacaoCotista = new OperacaoCotista();
            if (!operacaoCotista.LoadByPrimaryKey(this.IdOperacao.Value))
            {
                //Pode ocorrer da OperacaoCotista nao ter sido importada (por exemplo quando os tipos de Operacao nao sao suportados)
                return false;
            }

            //Ajustar cota na operacao
            if (resgateYMF.VlCotaUtilizada.HasValue && operacaoCotista.CotaOperacao == 0)
            {
                operacaoCotista.CotaOperacao = resgateYMF.VlCotaUtilizada.Value;
                operacaoCotista.Save();
            }
            

            /*PosicaoCotista posicaoCotista = new PosicaoCotista();
            if (!posicaoCotista.LoadByPrimaryKey(this.IdPosicaoResgatada.Value))
            {
                //Como iremos importar os saldos atuais, muitos números de nota já terão sido integralmente resgatadas, 
                //portanto várias linhas do arquivo Resgate.txt não terão como ser importados
                return false;
            }*/

            this.IdCarteira = operacaoCotista.IdCarteira;
            this.IdCotista = operacaoCotista.IdCotista;

            this.Quantidade = resgateYMF.QtCotas;
            this.ValorIOF = resgateYMF.VlIof;
            this.ValorBruto = resgateYMF.VlBrutoResgate;
            this.ValorIR = resgateYMF.VlIr;
            this.ValorLiquido = resgateYMF.VlLiquidoResgate;
            this.ValorPerformance = resgateYMF.VlPerformance.HasValue ? resgateYMF.VlPerformance.Value : 0;

            this.VariacaoResgate = 0;
            this.RendimentoResgate = 0;

            if (resgateYMF.VlRendimentoCompensado.HasValue)
            {
                this.VariacaoResgate += resgateYMF.VlRendimentoCompensado.Value;
            }

            if (resgateYMF.VlRendimentoTributado.HasValue)
            {
                this.VariacaoResgate += resgateYMF.VlRendimentoTributado.Value;
                this.RendimentoResgate += resgateYMF.VlRendimentoTributado.Value;
            }

            /*if (resgateYMF.VlRendimentoIsento.HasValue)
            {
                this.VariacaoResgate += resgateYMF.VlRendimentoIsento.Value;
            }*/

            this.PrejuizoUsado = 0;
            this.ValorCPMF = 0;

            return true;
        }

	}
}
