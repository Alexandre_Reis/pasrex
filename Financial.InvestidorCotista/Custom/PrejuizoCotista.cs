﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo;
using Financial.Util;
using Financial.Investidor;
using Financial.Interfaces.Import.YMF;
using Financial.CRM;
using System.Collections.Generic;

namespace Financial.InvestidorCotista {
    public partial class PrejuizoCotista : esPrejuizoCotista 
    {
        /// <summary>
        /// Acumula o prejuizo de cotista calculado no resgate.
        /// </summary>
        /// <param name="idCotista"></param>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="valorRendimento"></param>
        public void AcumulaPrejuizo(int idCotista, int idCarteira, DateTime data, decimal valorRendimento)
        {
            PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
            if (prejuizoCotista.LoadByPrimaryKey(idCotista, idCarteira, data))
            {
                prejuizoCotista.ValorPrejuizoOriginal += Math.Abs(valorRendimento); //Acumula o prejuízo
                prejuizoCotista.ValorPrejuizo += Math.Abs(valorRendimento); //Acumula o prejuízo
                prejuizoCotista.Save();
            }
            else
            {
                //Cria nova linha de prejuízo
                PrejuizoCotista prejuizoCotistaInserir = new PrejuizoCotista();
                prejuizoCotistaInserir.AddNew();
                prejuizoCotistaInserir.Data = data;
                prejuizoCotistaInserir.IdCarteira = idCarteira;
                prejuizoCotistaInserir.IdCotista = idCotista;
                prejuizoCotistaInserir.ValorPrejuizo = Math.Abs(valorRendimento);
                prejuizoCotistaInserir.ValorPrejuizoOriginal = Math.Abs(valorRendimento);
                prejuizoCotistaInserir.Save();
            }            
        }

        /// <summary>
        /// Trata valores de rendimento positivos (a serem compensados com prejuízos em outros fundos de mesma categoria e administrador).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="valorRendimento"></param>
        public void CompensaPrejuizo(DateTime data, int idCarteira, int idCotista, decimal prejuizoCompensado, PrejuizoCotistaCollection prejuizoCotistaCollection)
        {
            if (prejuizoCompensado > 0)
            {
                decimal prejuizoCompensadoRestante = prejuizoCompensado;
                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                {
                    int idCarteiraCompensar = prejuizoCotista.IdCarteira.Value;
                    decimal valorPrejuizoOriginal = prejuizoCotista.ValorPrejuizoOriginal.Value;
                    DateTime dataOriginal = prejuizoCotista.Data.Value;

                    PrejuizoCotistaUsado prejuizoCotistaUsadoHistorico = new PrejuizoCotistaUsado();
                    prejuizoCotistaUsadoHistorico.Query.Select(prejuizoCotistaUsadoHistorico.Query.PrejuizoUsado.Sum());
                    prejuizoCotistaUsadoHistorico.Query.Where(prejuizoCotistaUsadoHistorico.Query.IdCotista.Equal(idCotista),
                                                             prejuizoCotistaUsadoHistorico.Query.IdCarteiraCompensada.Equal(idCarteiraCompensar),
                                                             prejuizoCotistaUsadoHistorico.Query.DataOriginal.Equal(dataOriginal));
                    prejuizoCotistaUsadoHistorico.Query.Load();

                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsadoHistorico.PrejuizoUsado.HasValue ? prejuizoCotistaUsadoHistorico.PrejuizoUsado.Value : 0;

                    decimal prejuizoDisponivel = valorPrejuizoOriginal - prejuizoUsadoHistorico;

                    decimal prejuizoBaixar = 0;
                    if (prejuizoCompensadoRestante <= prejuizoDisponivel)
                    {
                        prejuizoBaixar = prejuizoCompensadoRestante;
                        prejuizoCompensadoRestante = 0;
                    }
                    else
                    {
                        prejuizoBaixar = prejuizoDisponivel;
                        prejuizoCompensadoRestante -= prejuizoDisponivel;
                    }

                    prejuizoDisponivel -= prejuizoBaixar;

                    prejuizoCotista.ValorPrejuizo = prejuizoDisponivel;

                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                    if (prejuizoCotistaUsado.LoadByPrimaryKey(idCarteira, idCarteiraCompensar, idCotista, dataOriginal, data))
                    {
                        prejuizoCotistaUsado.PrejuizoUsado += prejuizoBaixar;
                    }
                    else
                    {
                        prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                        prejuizoCotistaUsado.DataCompensacao = data;
                        prejuizoCotistaUsado.DataOriginal = dataOriginal;
                        prejuizoCotistaUsado.IdCarteira = idCarteira;
                        prejuizoCotistaUsado.IdCarteiraCompensada = idCarteiraCompensar;
                        prejuizoCotistaUsado.IdCotista = idCotista;
                        prejuizoCotistaUsado.PrejuizoUsado = prejuizoBaixar;                        
                    }
                    prejuizoCotistaUsado.Save();

                    if (prejuizoCompensadoRestante == 0)
                    {
                        break;
                    }
                }

                if (prejuizoCompensadoRestante > 0)
                {
                    throw new Exception("Não havia prejuízo suficiente para compensação no resgate do fundo " + idCarteira.ToString() + ", cotista " + idCotista.ToString() + ".");
                }

                prejuizoCotistaCollection.Save(); //Atualiza os valores de prejuizo na PrejuizoCotista
            }
        
        }        

        /// <summary>
        /// Para os casos de resgates totais, a data limite de compensação deve ser atualizad
        /// para o últmo dia útil do ano seguinte.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        public void AtualizaDataLimite(DateTime data, int idCarteira, int idCotista) {
            DateTime ultimoDiaAnoSeguinte = Calendario.RetornaUltimoDiaUtilAno(data, 1);
            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
            prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idCarteira, idCotista);

            for (int i = 0; i < prejuizoCotistaCollection.Count; i++) {
                prejuizoCotistaCollection[i].DataLimiteCompensacao = ultimoDiaAnoSeguinte;
            }
            prejuizoCotistaCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de PrejuizoCotista para PrejuizoCotistaHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCarteira, DateTime data) {
            PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollectionDeletar = new PrejuizoCotistaHistoricoCollection();
            prejuizoCotistaHistoricoCollectionDeletar.Query.Select(prejuizoCotistaHistoricoCollectionDeletar.Query.DataHistorico, 
                prejuizoCotistaHistoricoCollectionDeletar.Query.IdCarteira,prejuizoCotistaHistoricoCollectionDeletar.Query.IdCotista, 
                prejuizoCotistaHistoricoCollectionDeletar.Query.Data);
            prejuizoCotistaHistoricoCollectionDeletar.Query.Where(prejuizoCotistaHistoricoCollectionDeletar.Query.IdCarteira.Equal(idCarteira),
                                                                 prejuizoCotistaHistoricoCollectionDeletar.Query.DataHistorico.Equal(data));
            prejuizoCotistaHistoricoCollectionDeletar.Query.Load();
            prejuizoCotistaHistoricoCollectionDeletar.MarkAllAsDeleted();
            prejuizoCotistaHistoricoCollectionDeletar.Save();


            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
            prejuizoCotistaCollection.BuscaPrejuizoCotistaCompleto(idCarteira);
            //
            PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
            //
            #region Copia de PrejuizoCotista para PrejuizoCotistaHistorico
            for (int i = 0; i < prejuizoCotistaCollection.Count; i++) {
                PrejuizoCotista prejuizoCotista = prejuizoCotistaCollection[i];

                // Copia de PrejuizoCotista para PrejuizoCotistaHistorico
                PrejuizoCotistaHistorico prejuizoCotistaHistorico = prejuizoCotistaHistoricoCollection.AddNew();
                //
                prejuizoCotistaHistorico.DataHistorico = data;
                prejuizoCotistaHistorico.IdCotista = prejuizoCotista.IdCotista;
                prejuizoCotistaHistorico.IdCarteira = prejuizoCotista.IdCarteira;
                prejuizoCotistaHistorico.Data = prejuizoCotista.Data;
                prejuizoCotistaHistorico.ValorPrejuizo = prejuizoCotista.ValorPrejuizo;
                prejuizoCotistaHistorico.ValorPrejuizoOriginal = prejuizoCotista.ValorPrejuizoOriginal;
                prejuizoCotistaHistorico.DataLimiteCompensacao = prejuizoCotista.DataLimiteCompensacao;
            }
            #endregion

            prejuizoCotistaHistoricoCollection.Save();            
        }

        public bool ImportaRendimentoCompensarYMF(Financial.Interfaces.Import.YMF.RendimentoCompensarYMF rendimentoCompensarYMF)
        {
            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(rendimentoCompensarYMF.CdFundo.Trim());
            if (cliente == null)
            {
                throw new Exception("Cliente não encontrado: " + rendimentoCompensarYMF.CdFundo);
            }

            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(rendimentoCompensarYMF.CdCotista.Trim());
            if (cotista == null)
            {
                Pessoa pessoa = new Pessoa();
                ClienteYMF clienteYMF = new ClienteYMF();
                clienteYMF.CdCliente = rendimentoCompensarYMF.CdCotista;
                clienteYMF.IcFjPessoa = "F";
                clienteYMF.IdEstadoCivil = 0;
                clienteYMF.NmCliente = "Prejuízo a Compensar: " + clienteYMF.CdCliente;
                clienteYMF.NmAbreviado = clienteYMF.NmCliente;
                pessoa.ImportaClienteYMF(clienteYMF);

                pessoa.Save();

                cotista = new Cotista();
                CotistaYMF cotistaYMF = new CotistaYMF();

                cotistaYMF.CdCliente = pessoa.CodigoInterface;
                cotistaYMF.CdCotista = rendimentoCompensarYMF.CdCotista;
                cotista.ImportaCotistaYMF(cotistaYMF);
                cotista.Save();

          //      throw new Exception("Cotista não encontrado: " + rendimentoCompensarYMF.CdCotista);
            }

            this.IdCotista = cotista.IdCotista;
            this.IdCarteira = cliente.IdCliente;
            this.Data = rendimentoCompensarYMF.DtPosicao;
            this.ValorPrejuizo = rendimentoCompensarYMF.VlCompensar;
            this.ValorPrejuizoOriginal = this.ValorPrejuizo;
            this.DataLimiteCompensacao = rendimentoCompensarYMF.DtLimite;

            return true;
        }

        /// <summary>
        /// Atualiza a PrejuizoCotista a partir do que tiver já compensado na PrejuizoCotistaUsado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void AtualizaValorPrejuizo(int idCarteira, DateTime data)
        {            
            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
            prejuizoCotistaCollection.Query.Where(prejuizoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  prejuizoCotistaCollection.Query.ValorPrejuizo.NotEqual(0));
            prejuizoCotistaCollection.Query.Load();

            foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
            {
                PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                decimal prejuizoUsado = prejuizoCotistaUsado.RetornaPrejuizoUsado(prejuizoCotista.IdCotista.Value, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                prejuizoCotista.ValorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value - prejuizoUsado;
            }

            prejuizoCotistaCollection.Save();
        }

    }
}
