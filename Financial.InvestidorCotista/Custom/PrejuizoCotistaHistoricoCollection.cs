﻿
/*
===============================================================================
                    EntitySpaces(TM) 2008 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET  
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2008.1.0623.0
                       MyGeneration Version # 1.2.0.7
                       Date Generated       : 11/11/2008 16:13:01
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista {
    public partial class PrejuizoCotistaHistoricoCollection : esPrejuizoCotistaHistoricoCollection {

        public void ImportaRendimentoCompensarYMF(Financial.Interfaces.Import.YMF.RendimentoCompensarYMF[] rendimentosCompensarYMF)
        {

            PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();

            List<int> idCarteiraImportadaList = new List<int>();
            List<int> idCotistaImportadoList = new List<int>();
            List<DateTime> dataImportadaList = new List<DateTime>();
            List<DateTime> dataHistoricoImportadaList = new List<DateTime>();


            foreach (Financial.Interfaces.Import.YMF.RendimentoCompensarYMF rendimentoCompensarYMF in rendimentosCompensarYMF)
            {
                PrejuizoCotistaHistorico prejuizoCotistaHistorico = new PrejuizoCotistaHistorico();
                if (prejuizoCotistaHistorico.ImportaRendimentoCompensarYMF(rendimentoCompensarYMF))
                {
                    idCarteiraImportadaList.Add(prejuizoCotistaHistorico.IdCarteira.Value);
                    idCotistaImportadoList.Add(prejuizoCotistaHistorico.IdCotista.Value);
                    dataImportadaList.Add(prejuizoCotistaHistorico.Data.Value);
                    dataHistoricoImportadaList.Add(prejuizoCotistaHistorico.DataHistorico.Value);

                    //Se já possuimos um prejuizo nesta data para este cotista e fundo, 
                    //vamos apenas somar com o valor importado sem anexar à collection
                    PrejuizoCotistaHistorico prejuizoCotistaHistoricoExistente = prejuizoCotistaHistoricoCollection.FindByPrimaryKey(prejuizoCotistaHistorico.DataHistorico.Value,
                        prejuizoCotistaHistorico.IdCotista.Value, prejuizoCotistaHistorico.IdCarteira.Value, prejuizoCotistaHistorico.Data.Value);
                    if (prejuizoCotistaHistoricoExistente == null)
                    {
                        prejuizoCotistaHistoricoCollection.AttachEntity(prejuizoCotistaHistorico);
                    }
                    else
                    {
                        prejuizoCotistaHistoricoExistente.ValorPrejuizo += prejuizoCotistaHistorico.ValorPrejuizo;
                    }

                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                // Salva PrejuizoCotistas importados
                prejuizoCotistaHistoricoCollection.Save();

                scope.Complete();
            }

        }

    }
}
