﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           3/5/2007 13:46:45
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;

namespace Financial.InvestidorCotista {
    public partial class PrejuizoCotistaHistorico : esPrejuizoCotistaHistorico {

        public bool ImportaRendimentoCompensarYMF(Financial.Interfaces.Import.YMF.RendimentoCompensarYMF rendimentoCompensarYMF)
        {   
            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(rendimentoCompensarYMF.CdFundo.Trim());
            if (cliente == null)
            {
                throw new Exception("Cliente não encontrado: " + rendimentoCompensarYMF.CdFundo);
            }

            //A unica diferenca em relacao ao codigo do PrejuizoCotista é a linha abaixo:
            this.DataHistorico = Util.Calendario.SubtraiDiaUtil(cliente.DataImplantacao.Value, 1);

            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(rendimentoCompensarYMF.CdCotista.Trim());
            if (cotista == null)
            {
                throw new Exception("Cotista não encontrado: " + rendimentoCompensarYMF.CdCotista);
            }

            this.IdCotista = cotista.IdCotista;
            this.IdCarteira = cliente.IdCliente;
            this.Data = rendimentoCompensarYMF.DtPosicao;
            this.ValorPrejuizo = rendimentoCompensarYMF.VlCompensar;
            this.ValorPrejuizoOriginal = this.ValorPrejuizo;
            this.DataLimiteCompensacao = rendimentoCompensarYMF.DtLimite;

            return true;
        }

    }
}
