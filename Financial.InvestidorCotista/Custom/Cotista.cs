﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.InvestidorCotista.Exceptions;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using Financial.Interfaces.Import.YMF;
using Financial.Investidor;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Common;
using Financial.Interfaces.Sinacor;
using Financial.Fundo;
using Financial.InvestidorCotista.Perfil;

namespace Financial.InvestidorCotista
{
    public partial class Cotista : esCotista
    {
        /// <summary>
        /// Cria Cotista referente (isento IR e isento IOF), caso este não exista.
        /// </summary>
        public void CriaCotista(int idCotista, int idCliente)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.IdPessoa);
            cliente.LoadByPrimaryKey(campos, idCliente);

            Cotista c = new Cotista();
            c.Query.Where(c.Query.IdCarteira.Equal(idCliente) & c.Query.IdPessoa.Equal(cliente.IdPessoa));
            if (!c.Query.Load())
            {
                Cotista cotistaAux = new Cotista();
                cotistaAux.IdCotista = idCotista;
                cotistaAux.Nome = cliente.str.Apelido;
                cotistaAux.Apelido = cliente.str.Apelido;
                cotistaAux.IsentoIR = "S";
                cotistaAux.IsentoIOF = "S";
                cotistaAux.TipoTributacao = (byte)TipoTributacaoCotista.Residente;
                cotistaAux.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                cotistaAux.IdPessoa = cliente.IdPessoa.Value;
                cotistaAux.IdCarteira = idCliente;
                cotistaAux.Save();
            }
        }

        /// <summary>
        /// Classe para abarcar as informações geradas no arquivo CCS do Bacen.
        /// </summary>
        public class CotistaCSS
        {
            public int idCotista;
            public string nome;
            public string cpfCNPJ;
            public string tipoPessoa;
            public string cnpjAdministrador;
            public DateTime dataInicioOperacao;
            public DateTime? dataFimOperacao;

        }

        /// <summary>
        /// Retorna lista dos cotistas que aportaram na data, resgataram total, foram transferidos para outra instituição ou entraram por transferência na instituição.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="listaFundos"></param>
        /// <returns></returns>
        public List<CotistaCSS> RetornaListaCCS(DateTime data, List<int> listaFundos)
        {
            List<CotistaCSS> lista = new List<CotistaCSS>();

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            #region Cotistas que aportaram a 1a vez
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdCotista,
                                                   operacaoCotistaCollection.Query.IdCarteira);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.In(listaFundos),
                                                  operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                  operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
            operacaoCotistaCollection.Query.Load();

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                int idCotista = operacaoCotista.IdCotista.Value;
                int idCarteira = operacaoCotista.IdCarteira.Value;

                PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                posicaoCotistaAberturaCollection.Query.Select(posicaoCotistaAberturaCollection.Query.IdCotista);
                posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCotista.Equal(idCotista),
                                                             posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data),
                                                             posicaoCotistaAberturaCollection.Query.Quantidade.NotEqual(0),
                                                             posicaoCotistaAberturaCollection.Query.IdCarteira.In(listaFundos));
                posicaoCotistaAberturaCollection.Query.Load();

                if (posicaoCotistaAberturaCollection.Count == 0)
                {
                    CotistaCSS cotistaCSS = new CotistaCSS();
                    cotistaCSS.idCotista = idCotista;
                    cotistaCSS.dataInicioOperacao = data;
                    //cotistaCSS.dataFimOperacao = data;

                    Pessoa pessoa = new Pessoa();
                    pessoa.LoadByPrimaryKey(idCotista);

                    Carteira carteira = new Carteira();
                    carteira.Query.Select(carteira.Query.IdAgenteAdministrador);
                    carteira.Query.Where(carteira.Query.IdCarteira.Equal(idCarteira));
                    carteira.Query.Load();

                    AgenteMercado agenteMercado = new AgenteMercado();
                    int idAgenteMercado = carteira.IdAgenteAdministrador.Value;
                    agenteMercado.LoadByPrimaryKey(idAgenteMercado);

                    string CNPJ_Adm = Convert.ToString(agenteMercado.Cnpj);

                    if (CNPJ_Adm.Length >= 8)
                    {
                        CNPJ_Adm = CNPJ_Adm.Replace("-", "").Replace(" ", "").Replace(".", "").Replace("/", "").Replace("\n", "").Substring(0, 8);
                    }

                    cotistaCSS.cpfCNPJ = String.IsNullOrEmpty(pessoa.Cpfcnpj) ? "" : pessoa.Cpfcnpj;
                    cotistaCSS.nome = pessoa.Nome;
                    cotistaCSS.tipoPessoa = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "F" : "J";
                    cotistaCSS.cnpjAdministrador = CNPJ_Adm;

                    lista.Add(cotistaCSS);
                }
            }
            #endregion

            #region Cotistas que resgataram total
            operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdCotista,
                                                   operacaoCotistaCollection.Query.IdCarteira);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.In(listaFundos),
                                                  operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                  operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateTotal));
            operacaoCotistaCollection.Query.Load();

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                int idCotista = operacaoCotista.IdCotista.Value;
                int idCarteira = operacaoCotista.IdCarteira.Value;

                CotistaCSS cotistaCSS = new CotistaCSS();
                cotistaCSS.idCotista = idCotista;
                cotistaCSS.dataInicioOperacao = data;
                cotistaCSS.dataFimOperacao = data;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(idCotista);

                Carteira carteira = new Carteira();
                carteira.Query.Select(carteira.Query.IdAgenteAdministrador);
                carteira.Query.Where(carteira.Query.IdCarteira.Equal(idCarteira));
                carteira.Query.Load();

                AgenteMercado agenteMercado = new AgenteMercado();
                int idAgenteMercado = carteira.IdAgenteAdministrador.Value;
                agenteMercado.LoadByPrimaryKey(idAgenteMercado);

                string CNPJ_Adm = Convert.ToString(agenteMercado.Cnpj);

                if (CNPJ_Adm.Length >= 8)
                {
                    CNPJ_Adm = CNPJ_Adm.Replace("-", "").Replace(" ", "").Replace(".", "").Replace("/", "").Replace("\n", "").Substring(0, 8);
                }

                cotistaCSS.cpfCNPJ = String.IsNullOrEmpty(pessoa.Cpfcnpj) ? "" : pessoa.Cpfcnpj;
                cotistaCSS.nome = pessoa.Nome;
                cotistaCSS.tipoPessoa = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "F" : "J";
                cotistaCSS.cnpjAdministrador = CNPJ_Adm;

                lista.Add(cotistaCSS);
            }
            #endregion

            return lista;
        }

        /// <summary>
        /// True se ativo. False se não ativo.
        /// </summary>
        public bool IsAtivo
        {
            get { return this.StatusAtivo == (byte)StatusAtivoCotista.Ativo; }
        }

        /// <summary>
        /// Carrega o objeto InvestidorCotista com os campos IsentoIR, IsentoIOF, TipoTributacao.
        /// CotistaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>        
        public void BuscaTributacaoCotista(int idCotista)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPessoa, 
                         this.Query.IsentoIR, 
                         this.Query.IsentoIOF,
                         this.Query.TipoTributacao)
                 .Where(this.Query.IdCotista == idCotista);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CotistaNaoCadastradoException("Cotista " + IdCotista + " não cadastrado.");
            }
        }

        /// <summary>
        /// Retorna booleano indicando se o cotista é isento de IR.
        /// </summary>
        /// <returns></returns>
        public bool IsIsentoIR()
        {
            return this.IsentoIR == "S";
        }

        /// <summary>
        /// Retorna booleano indicando se o cotista é isento de IOF.
        /// </summary>
        /// <returns></returns>
        public bool IsIsentoIOF()
        {
            return this.IsentoIOF == "S";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCotista"></param>
        /// <param name="idCotistaReplicar"></param>
        /// <returns>Clone do Objeto Cotista com IdCotistaReplicar</returns>
        public Cotista ReplicaCotista(int idCotista, int idCotistaReplicar)
        {
            /*
            Cotista cotistaReplicado = new Cotista();
            cotistaReplicado.LoadByPrimaryKey(idCotistaReplicar);
            //
            Cotista cotista = new Cotista();
            cotista = (Cotista)Utilitario.Clone(cotistaReplicado);
            cotista.IdCotista = idCotista;
            //
            return cotista;
            */

            //
            Cotista c = new Cotista();
            c.LoadByPrimaryKey(idCotistaReplicar);
            c.MarkAllColumnsAsDirty(DataRowState.Added);
            c.IdCotista = idCotista;
            //
            return c;
        }

        public void ImportaCotistaYMF(CotistaYMF cotistaYMF)
        {

            string codigoClienteYMF = cotistaYMF.CdCliente.Trim();

            //Localizar Pessoa por Codigo da YMF
            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCodigoInterface(codigoClienteYMF);

            if (pessoa == null)
            {
                throw new Exception("Não foi possível encontrar a pessoa com código YMF: " + codigoClienteYMF);
            }

            this.IdCotista = pessoa.IdPessoa;
            this.CodigoInterface = cotistaYMF.CdCotista.Trim();

            this.Nome = pessoa.Nome;
            this.Apelido = pessoa.Apelido;
            this.IsentoIR = "S";
            this.IsentoIOF = "S";
            this.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
            this.TipoTributacao = (byte)TipoTributacaoCotista.Residente;

            this.TipoCotistaCVM = pessoa.IsPessoaFisica() ? (int?)TipoCotista.PESSOA_FISICA_VAREJO :
                (int?)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO;

            //TODOIMP: Setar CtTipoEndereco como principal !!O que fazer com: cotistaYMF.CdTipoEndereco;


        }

        public void ImportaTipoCotistaYMF(Dictionary<int, Financial.Interfaces.Import.YMF.TipoCotistaYMF> dictionaryTipoCotistaYMF)
        {
            TipoCotistaYMF tipoCotistaYMF = dictionaryTipoCotistaYMF[(int)this.Table.Rows[0]["TipoCotistaYMF"]];

            //Precisamos salvar o cotista fora da collection pois o da Collection possui o DataTable alterado
            Cotista cotistaForaCollection = new Cotista();
            cotistaForaCollection.LoadByPrimaryKey(this.IdCotista.Value);
            cotistaForaCollection.IsentoIR = tipoCotistaYMF.IcSnIsentoIr;
            cotistaForaCollection.IsentoIOF = (tipoCotistaYMF.IcSnIofResgate == "S") ? "N" : "S";
            cotistaForaCollection.Save();
        }

        public void GuardaIdTipoCotistaYMF(Financial.Interfaces.Import.YMF.CotistaYMF cotistaYMF)
        {
            this.Table.Columns.Add("TipoCotistaYMF", typeof(System.Int32));
            this.Table.Rows[0]["TipoCotistaYMF"] = cotistaYMF.IdTipoCotista;
        }

        /// <summary>
        /// De Acordo com os Codigos Sinacor
        /// Insere novo cotista se não Existir Ou Atualiza se já existir
        /// Insere nova pessoa se não Existir Ou Atualiza se já existir
        /// Deleta e Insere os endereços em PessoaEndereco
        /// Atualiza os Emails em PessoaEndereco
        /// </summary>
        /// <exception cref="Exception">Se codigo sinacor não achado ou cpf não encontrado na base sinacor</exception>
        public void ImportaCotistaSinacor(List<int> codigosSinacor)
        {

            CotistaCollection cotistaCollection = new CotistaCollection();

            for (int i = 0; i < codigosSinacor.Count; i++)
            {

                #region TSCCLIBOL
                TscclibolCollection tscclibolCollection = new TscclibolCollection();

                // Throws Exception
                tscclibolCollection.GetTscclibol(codigosSinacor[i]);

                decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
                #endregion

                #region TSCCLIGER

                TsccligerCollection collCliGer = new TsccligerCollection();
                TsccligerCollection.InfoPessoaSinacor info;
                try
                {
                    info = collCliGer.RetornaInfoPessoa(cpfCGC);
                }
                catch (Exception e1)
                {
                    throw new Exception(e1.Message + codigosSinacor[i].ToString());
                }
                #endregion

                Cotista cotista = new Cotista();
                if (!cotista.LoadByPrimaryKey(codigosSinacor[i]))
                { // Insert
                    cotista = new Cotista();
                }

                #region Update/Insert
                cotista.IdCotista = codigosSinacor[i];
                cotista.Nome = info.Nome.Trim();

                #region Apelido
                string apelido = info.Nome;

                if (apelido.Length > 40)
                {
                    apelido = apelido.Substring(0, 40);
                }

                if (string.IsNullOrEmpty(apelido.Trim()))
                {
                    int maxlength = (int)CotistaMetadata.Meta().Columns.FindByColumnName(CotistaMetadata.ColumnNames.Apelido).CharacterMaxLength;
                    cotista.Apelido = (cotista.Nome.Length <= maxlength) ? cotista.Nome : cotista.Nome.Substring(0, maxlength - 1);
                }
                else
                {
                    cotista.Apelido = apelido;
                }
                #endregion

                cotista.IsentoIOF = "N";
                cotista.IsentoIR = "N";
                cotista.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                cotista.CodigoInterface = cotista.IdCotista.ToString();
                cotista.TipoTributacao = (byte)TipoTributacaoCotista.Residente;
                #endregion

                cotistaCollection.AttachEntity(cotista);
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                new Pessoa().ImportaPessoasSinacor(codigosSinacor);
                //
                cotistaCollection.Save();
                //
                scope.Complete();
            }
        }

        public ContaCorrenteCollection BuscaContaCorrenteCollection()
        {
            ContaCorrenteCollection contas = new ContaCorrenteCollection();
            contas.Query.Where(contas.Query.IdPessoa.Equal(this.IdCotista.Value));

            contas.Load(contas.Query);
            return contas;
        }

        public string Cpfcnpj
        {
            get
            {
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCotista.Value);
                return pessoa.Cpfcnpj;
            }
        }

        public string PerfilInvestidor
        {
            get
            {
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCotista.Value);

                string descricaoPerfilInvestidor = "";
                if (pessoa.IdPerfilInvestidor.HasValue)
                {
                    PerfilInvestidor perfilInvestidor = new PerfilInvestidor();
                    perfilInvestidor.LoadByPrimaryKey(pessoa.IdPerfilInvestidor.Value);
                    descricaoPerfilInvestidor = perfilInvestidor.Descricao;
                }

                return descricaoPerfilInvestidor;
            }
        }

        /// <summary>
        /// max Id cotista + 1
        /// </summary>
        /// <returns></returns>
        public int GeraIdCotista()
        {
            //Rotina para gerar o próximo Id de pessoa disponível
            int idCotistaVago = 1;

            Cotista cotista = new Cotista();
            cotista.Query.Select(cotista.Query.IdCotista.Max());
            if (cotista.Query.Load())
            {
                if (cotista.IdCotista.HasValue)
                {
                    idCotistaVago = cotista.IdCotista.Value + 1;
                }
            }
            return idCotistaVago;
        }

    }
}
