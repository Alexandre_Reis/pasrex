﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
	public partial class PosicaoCotistaAbertura : esPosicaoCotistaAbertura
	{
        /// <summary>
        /// Retorna o total de quantidade de cotas de todos os cotistas da carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns></returns>
        public decimal RetornaTotalCotas(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataHistorico.Equal(data));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        public bool ImportaPosicaoNotaHistoricaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota)
        {
            this.DataHistorico = posicaoNota.DtPosicao;

            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(posicaoNota.CdFundo.Trim());
            if (cliente == null)
            {
                throw new Exception("Não foi possível encontrar o fundo: " + posicaoNota.CdFundo);
            }

            if (posicaoNota.DtPosicao.Date >= cliente.DataImplantacao.Value.Date)
            {
                throw new Exception("Não é permitido importar uma posição posterior à data de implantação do fundo: " + posicaoNota.CdFundo);
            }

            this.IdPosicao = posicaoNota.IdNota;
                        
            if (posicaoNota.QtCotas == 0)
            {
                return false;
            }

            this.MapeiaCamposYMFFinancial(posicaoNota);
            return true;
        }

        private void MapeiaCamposYMFFinancial(PosicaoNotaYMF posicaoNota)
        {
            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(posicaoNota.CdCotista.Trim());
            if (cotista == null)
            {
                throw new Exception("Não foi possível encontrar o cotista: " + posicaoNota.CdCotista);
            }
            this.IdCotista = cotista.IdCotista;

            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(posicaoNota.CdFundo.Trim());
            if (cliente == null)
            {
                throw new Exception("Não foi possível encontrar o fundo: " + posicaoNota.CdFundo);
            }
            this.IdCarteira = cliente.IdCliente;
            this.DataAplicacao = posicaoNota.DtAplicacao;
            this.ValorAplicacao = posicaoNota.VlAplicacao;
            this.CotaAplicacao = posicaoNota.VlCotaAplicacao;

            this.Quantidade = posicaoNota.QtCotas;
            this.ValorBruto = posicaoNota.VlBruto;
            this.ValorIR = posicaoNota.VlIr;
            this.ValorIOF = posicaoNota.VlIof;
            this.ValorPerformance = posicaoNota.VlPerformance;
            this.ValorIOFVirtual = posicaoNota.VlIofVirtual;

            this.DataConversao = this.DataAplicacao;
            this.CotaDia = this.ValorBruto / this.Quantidade;
            this.ValorLiquido = this.ValorBruto - this.ValorIR - this.ValorIOF - this.ValorPerformance;
            this.QuantidadeInicial = this.Quantidade;
            this.QuantidadeBloqueada = 0;
            this.QuantidadeAntesCortes = this.Quantidade;
            this.PosicaoIncorporada = "N";
            this.ValorRendimento = 0;
            this.DataUltimaCobrancaIR = posicaoNota.DtUltimoResgateIr.HasValue ? posicaoNota.DtUltimoResgateIr : cliente.DataDia;
            this.DataUltimoCortePfee = posicaoNota.DtInicioPerformance;
        }

        public bool ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota, DateTime dataImplantacao, 
            MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF, MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF)
        {
            //Estas são as diferenças para o ImportaPosicaoNotaYMF no PosicaoCotista:
            this.DataHistorico = posicaoNota.DtPosicao;
            if (mappingPosicaoFinancialYMF.ContainsKey(posicaoNota.IdNota))
            {
                this.IdPosicao = mappingPosicaoFinancialYMF[posicaoNota.IdNota];
            }
            else
            {
                //Trata-se de uma posição que não importamos (provavelmente posição pós data de implantação)
                return false;
            }

            //O restante do código é idêntico:
            if (posicaoNota.QtCotas == 0)
            {
                return false;
            }

            if (posicaoNota.DtPosicao != dataImplantacao)
            {
                //Só queremos importar posições cujas datas correspondam à data de implantação do fundo
                return false;
            }

            if (mappingOperacaoFinancialYMF.ContainsKey(posicaoNota.IdNota))
            {
                //Vamos tentar encontrar na tabela de operações uma operação com o IdNota desta posição. Na YMF IdOperacao e IdPosicao caminham juntos como IdNota
                this.IdOperacao = mappingOperacaoFinancialYMF[posicaoNota.IdNota];
            }

            this.MapeiaCamposYMFFinancial(posicaoNota);

            return true;
        }
        
    
    }
}
