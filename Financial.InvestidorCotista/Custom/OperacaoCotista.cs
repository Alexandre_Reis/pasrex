﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using log4net;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Tributo.Custom;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using Financial.Fundo.Exceptions;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Tributo.Debug.CalculoTributo;
using Financial.Fundo.Debug.HistoricoCota;
using Financial.InvestidorCotista.Debug.OperacaoCotista;
using Financial.Util.ConfiguracaoSistema;
using Financial.Common;
using System.Configuration;
using System.Linq;
using Financial.Common.Enums;
using Financial.Security;
using System.Collections;

namespace Financial.InvestidorCotista
{
    public partial class OperacaoCotista : esOperacaoCotista
    {
        /// <summary>
        /// Calcula o valor de performance (em cota) para a data e carteira passadas.
        /// Usado exclusivamente em situações de resgate de cotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="dataConversao">Data da conversão da aplicação sendo resgatada</param>
        private decimal RetornaValorPerformanceResgate(int idCarteira, decimal quantidadeCotas, DateTime data, DateTime dataConversao, DateTime? dataUltimoCortePfee)
        {
            //É buscada apenas a 1a linha da tabela de performance, para a situação ResgataPerformance="S"
            TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
            tabelaTaxaPerformanceCollection.BuscaTabelaTaxaPerformanceResgate(idCarteira, data);

            if (tabelaTaxaPerformanceCollection.Count == 0)
            {
                return 0;
            }

            TabelaTaxaPerformance tabelaTaxaPerformance = tabelaTaxaPerformanceCollection[0];
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            decimal valorPerformance = calculoPerformance.RetornaValorPerformanceCotista(idCarteira, data, dataConversao, dataUltimoCortePfee, quantidadeCotas, tabelaTaxaPerformance);

            return valorPerformance;
        }

        /// <summary>
        /// Processa resgates líquidos (específico, FIFO, LIFO, menor imposto) para Fundos de Ações.
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// </param>
        /// <param name="data"></param>
        /// <param name="carteira"></param>
        /// <param name="operacao">Operacao Cotista ou Operacao Fundo</param>
        /// <param name="posicaoCollection">PosicaoCotista Ou PosicaoFundo</param>
        /// <param name="cotaDia"></param>
        /// <param name="projecaoCalculo">
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="ArgumentException">
        /// Se operacao nao for do tipo OperacaoCotista ou OperacaoFundo
        /// Se posicaoCollection nao for do tipo PosicaoCotistaCollection Ou PosicaoFundoCollection
        /// </exception>
        private void ProcessaResgateLiquidoAcoesCompensacaoDireta(int idCarteiraCliente, DateTime data, Carteira carteira,
                                          esEntity operacao, esEntityCollection posicaoCollection,
                                          decimal cotaDia, bool calculaPfee, params bool[] projecaoCalculo)
        {
            bool pegaCotaAplicacaoPosicao = ParametrosConfiguracaoSistema.Fundo.UsaCotaInicialPosicaoIR == "S";

            #region Trata Exception
            if ((operacao.GetType() != typeof(OperacaoCotista) &&
                   operacao.GetType() != typeof(OperacaoFundo)) ||

                   (posicaoCollection.GetType() != typeof(PosicaoCotistaCollection) &&
                    posicaoCollection.GetType() != typeof(PosicaoFundoCollection))
               )
            {

                throw new ArgumentException("Parâmetro Incorreto: Tipo de Objeto Inválido");
            }
            #endregion

            #region Carrega os parâmetros da carteira, para cálculo dos tributos
            int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            #region Carrega as variáveis principais (da operação), inclusive o valor Liquido
            int? idCotista = null;
            int? idCarteira = null;
            int? idCliente = null;

            int idOperacao = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
            decimal valorLiquidoOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido);
            //
            if (operacao is OperacaoCotista)
            {
                idCarteira = idCarteiraCliente;
                idCotista = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCotista);

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada));
                }
            }
            else if (operacao is OperacaoFundo)
            {
                idCarteira = (int)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdCarteira);
                idCliente = idCarteiraCliente;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada));
                }
            }
            #endregion

            #region Busca isenção em tributos
            Cotista cotista = null;
            Cliente cliente = null;
            Pessoa pessoa = new Pessoa();

            if (operacao is OperacaoCotista)
            {
                cotista = new Cotista();
                cotista.BuscaTributacaoCotista(idCotista.Value);

                pessoa = cotista.UpToPessoaByIdPessoa;

                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idCarteiraCliente);

                //Para os casos do cotista fora do controle de fundos e clubes, não há calculo de tributos
                if (cliente.IdTipo.Value != TipoClienteFixo.Clube &&
                    cliente.IdTipo.Value != TipoClienteFixo.Fundo &&
                    cliente.IdTipo.Value != TipoClienteFixo.FDIC)
                {
                    cotista.IsentoIR = "S";
                    cotista.IsentoIOF = "S";
                }
            }
            else
            {
                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IsentoIR);
                campos.Add(cliente.Query.IsentoIOF);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);

                pessoa = cliente.UpToPessoaByIdPessoa;
            }
            #endregion

            //A ser computado dentro do loop
            decimal quantidadeResgate = 0, valorBrutoResgate = 0, valorLiquidoResgate = 0,
                    valorIRResgate = 0, variacaoResgate = 0, rendimentoCompensadoResgate = 0, prejuizoUsadoResgate = 0, valorPerformanceResgate = 0;
            //
            decimal saldoLiquidoRestante = valorLiquidoOperacao;
            //
            bool posicaoSuficiente = false;
            //
            CalculoTributo calculoTributo = new CalculoTributo();

            #region Loop de posições sendo resgatadas
            int i = 0;
            while (i < posicaoCollection.Count)
            {
                int id = operacao is OperacaoCotista ? idCotista.Value : idCliente.Value;
                TipoProcessaResgate t = operacao is OperacaoCotista ? TipoProcessaResgate.OperacaoCotista : TipoProcessaResgate.OperacaoFundo;
                bool projecao = operacao is OperacaoCotista ? projecaoCalculo[0] : false;

                #region Dados da posicao, cotaAplicacao, valorPerformanceEmCota
                int idPosicaoResgatada = (int)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao);
                decimal quantidadePosicao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade) -
                                            (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeBloqueada);
                DateTime dataAplicacao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao);
                DateTime dataConversao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao);

                DateTime? dataUltimoCortePfee = null;
                if (!Convert.IsDBNull((((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee))))
                {
                    dataUltimoCortePfee = Convert.ToDateTime(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee));
                }


                bool posicaoIncorporada = (string)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.PosicaoIncorporada) == "S";

                decimal cotaAplicacao = 0;
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = operacao is OperacaoCotista
                                    ? ((PosicaoCotista)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, idCotista.Value)
                                    : ((PosicaoFundo)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, idCliente.Value);
                }
                else
                {
                    if (!pegaCotaAplicacaoPosicao)
                    {
                        HistoricoCota historicoCotaAplicacao = new HistoricoCota();

                        if (posicaoIncorporada)
                        {
                            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.LessThan(data));
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraDestino.Equal(idCarteira));
                            eventoFundoCollection.Query.Load();
                            if (eventoFundoCollection.Count > 0)
                            {
                                historicoCotaAplicacao.CotaFechamento = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                                historicoCotaAplicacao.CotaAbertura = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                            }
                            else
                            {
                                historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao, data); //Trata posição incorporada
                            }
                        }
                        else
                        {
                            historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao);
                        }

                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                        }
                    }
                    else
                    {
                        cotaAplicacao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                    }
                }

                //Calcula performance em valor de cota (a ser usado em 2 situações adiante)
                decimal valorPerformanceEmCota = 0;
                if (calculaPfee)
                {
                    valorPerformanceEmCota = this.RetornaValorPerformanceResgate(idCarteira.Value, 1, data, dataConversao, dataUltimoCortePfee);
                }
                #endregion

                #region Calcula IR e Saldos bruto e liquido da posição (NÃO leva em conta prejuízo a compensar) -> valorIRPosicao, saldoBrutoPosicao, saldoLiquidoPosicao
                bool isentoIR = operacao is OperacaoCotista ? cotista.IsIsentoIR() : cliente.IsIsentoIR();
                bool tributaNaoResidente = operacao is OperacaoCotista ? cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente :
                                                                         cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;

                decimal valorIRPosicao = 0;
                if (!isentoIR)
                {
                    CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                    calculoIRFundoAcoes.Quantidade = quantidadePosicao;
                    calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoAcoes.CotaCalculo = cotaDia;
                    calculoIRFundoAcoes.PrejuizoCompensar = 0;
                    calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                    calculoIRFundoAcoes.DataCalculo = data;
                    calculoIRFundoAcoes.DataAplicacao = dataAplicacao;
                    calculoIRFundoAcoes.IdCarteira = idCarteira.Value;

                    ListaTipoInvestidor tipoInvestidor;
                    if (pessoa.Tipo.HasValue)
                    {
                        if (pessoa.Tipo.Value == 1)
                            tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                        else
                            tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
                    }
                    else
                        tipoInvestidor = ListaTipoInvestidor.OffShore;

                    calculoIRFundoAcoes.TipoInvestidor = Convert.ToInt16(tipoInvestidor);
                    calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);
                    valorIRPosicao = calculoTributo.IR;
                }

                decimal saldoBrutoPosicao = truncaFinanceiro
                                            ? Utilitario.Truncate(quantidadePosicao * cotaDia, 2)
                                            : Math.Round(quantidadePosicao * cotaDia, 2);

                decimal saldoLiquidoPosicao = saldoBrutoPosicao - valorIRPosicao;
                #endregion

                bool resgateComLucro = cotaDia > cotaAplicacao;

                decimal valorLiquidoResgatar = 0;
                if ((saldoLiquidoRestante > saldoLiquidoPosicao))
                {
                    valorLiquidoResgatar = saldoLiquidoPosicao;
                }
                else
                {
                    valorLiquidoResgatar = saldoLiquidoRestante;
                }

                decimal prejuizoCompensar = 0;
                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();

                if (!isentoIR)
                {
                    #region Busca prejuízo a compensar
                    if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                        {
                            if (!projecao)
                            {
                                #region Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                                prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id);

                                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                                {
                                    decimal valorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value;

                                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsado.RetornaPrejuizoUsado(id, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                                    valorPrejuizo -= prejuizoUsadoHistorico;

                                    if (valorPrejuizo < 0)
                                    {
                                        valorPrejuizo = 0;
                                    }

                                    prejuizoCompensar += valorPrejuizo;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoCompensar = prejuizoFundo.RetornaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id);
                        }
                    }
                    #endregion
                }

                decimal valorLiquidoCompensar = resgateComLucro ? Math.Round(prejuizoCompensar / (cotaDia - cotaAplicacao) * cotaDia, 2) : 0;

                decimal valorLiquidoCompensado = 0;
                decimal valorLiquidoNaoCompensado = 0;
                if (valorLiquidoResgatar > valorLiquidoCompensar)
                {
                    valorLiquidoCompensado = valorLiquidoCompensar;
                    valorLiquidoNaoCompensado = valorLiquidoResgatar - valorLiquidoCompensado;
                }
                else
                {
                    valorLiquidoCompensado = valorLiquidoResgatar;
                    valorLiquidoNaoCompensado = 0;
                }
                decimal prejuizoUsado = resgateComLucro ? Math.Round(valorLiquidoCompensado / cotaDia * (cotaDia - cotaAplicacao), 2, MidpointRounding.AwayFromZero) : 0;

                decimal valorIR = 0;
                if (resgateComLucro && saldoLiquidoPosicao > 0)
                {
                    valorIR = Math.Round(valorLiquidoNaoCompensado * valorIRPosicao / saldoLiquidoPosicao, 2, MidpointRounding.AwayFromZero);
                }

                decimal valorBruto = valorLiquidoResgatar + valorIR;


                decimal quantidadeBaixa = 0;
                if ((saldoLiquidoRestante > saldoLiquidoPosicao))
                {
                    quantidadeBaixa = quantidadePosicao;
                }
                else
                {
                    quantidadeBaixa = truncaQuantidade
                                      ? Utilitario.Truncate(valorBruto / cotaDia, casasDecimaisQuantidade)
                                      : Math.Round(valorBruto / cotaDia, casasDecimaisQuantidade, MidpointRounding.AwayFromZero);

                    //Se baixar a quantidade restante inteira, prepara para sair do loop de posições
                    if (quantidadeBaixa < quantidadePosicao)
                    {
                        posicaoSuficiente = true;
                    }
                    else
                    { //Garante só resgatar o máximo da posição
                        quantidadeBaixa = quantidadePosicao;
                    }
                }

                //Recalcula valor bruto e valor liquido pela quantidade
                valorBruto = truncaFinanceiro ? Utilitario.Truncate(quantidadeBaixa * cotaDia, 2) : Math.Round(quantidadeBaixa * cotaDia, 2);
                valorLiquidoResgatar = valorBruto - valorIR;

                decimal rendimento = Math.Round(quantidadeBaixa * (cotaDia - cotaAplicacao), 2, MidpointRounding.AwayFromZero);

                if (!isentoIR)
                {
                    #region Trata compensação de prejuízo
                    if (operacao is OperacaoCotista) //Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                    {
                        if (!projecao)
                        {
                            PrejuizoCotista prejuizoCotista = new PrejuizoCotista();

                            if (rendimento < 0)
                            {
                                prejuizoCotista.AcumulaPrejuizo(id, idCarteira.Value, data, rendimento);
                            }

                            prejuizoCotista.CompensaPrejuizo(data, idCarteira.Value, id, prejuizoUsado, prejuizoCotistaCollection);
                        }
                    }
                    else
                    {
                        PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                        prejuizoFundo.TrataCompensacaoPrejuizo(data, idAgenteAdministrador, tipoTributacao, idCarteira.Value, id, rendimento, prejuizoUsado);
                    }
                    #endregion
                }

                decimal rendimentoCompensado = rendimento - prejuizoUsado;

                decimal valorPerformance = Math.Round(valorPerformanceEmCota * quantidadeBaixa, 2, MidpointRounding.AwayFromZero);

                #region Calcula valores (bruto, líquido) da operação e adiciona as variáveis gerais do resgate
                decimal variacao = Math.Round(quantidadeBaixa * (cotaDia - cotaAplicacao), 2);

                quantidadeResgate += quantidadeBaixa;
                valorBrutoResgate += valorBruto;
                valorLiquidoResgate += valorLiquidoResgatar;
                valorIRResgate += valorIR;
                variacaoResgate += variacao;
                rendimentoCompensadoResgate += rendimentoCompensado;
                prejuizoUsadoResgate += prejuizoUsado;
                valorPerformanceResgate += valorPerformance;
                #endregion

                #region Baixa a posição em quantidade e garante zeragem de valores, IR etc caso tenha baixado tudo da posição
                if (operacao is OperacaoCotista)
                {
                    if (!projecaoCalculo[0])
                    {
                        PosicaoCotista p = (PosicaoCotista)posicaoCollection[i];
                        p.Quantidade -= quantidadeBaixa;
                        p.QuantidadeAntesCortes -= quantidadeBaixa;

                        if (p.Quantidade == 0)
                        {
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;
                            i++;
                        }
                        else
                        {
                            p.ValorPerformance -= valorPerformance;

                            //Checa eventuais resíduos na qtde por ter compensado prejuizo.
                            //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                            decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                            if (prejuizoUsado == 0 || posicaoSuficiente)
                            {
                                i++;
                            }
                            else if (valorPosicaoResidual < 0.01M)
                            {
                                p.Quantidade = 0;
                                p.ValorBruto = 0;
                                p.ValorLiquido = 0;
                                p.ValorIR = 0;
                                p.ValorIOF = 0;
                                p.ValorPerformance = 0;
                                p.ValorIOFVirtual = 0;

                                i++;
                            }
                        }
                    }
                }
                else
                {
                    PosicaoFundo p = (PosicaoFundo)posicaoCollection[i];
                    p.Quantidade -= quantidadeBaixa;
                    p.QuantidadeAntesCortes -= quantidadeBaixa;

                    if (p.Quantidade == 0)
                    {
                        p.ValorBruto = 0;
                        p.ValorLiquido = 0;
                        p.ValorIR = 0;
                        p.ValorIOF = 0;
                        p.ValorPerformance = 0;
                        p.ValorIOFVirtual = 0;
                        i++;
                    }
                    else
                    {
                        //Checa eventuais resíduos na qtde por ter compensado prejuizo. 
                        //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                        decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                        if (prejuizoUsado == 0 || posicaoSuficiente)
                        {
                            i++;
                        }
                        else if (valorPosicaoResidual < 0.01M)
                        {
                            p.Quantidade = 0;
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;

                            i++;
                        }
                    }
                }
                #endregion

                #region DetalheResgateCotista ou DetalheResgateFundo
                if (operacao is OperacaoCotista)
                {
                    #region Insert em DetalheResgateCotista
                    DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                    try
                    {
                        detalheResgateCotista.IdOperacao = idOperacao;
                        detalheResgateCotista.IdPosicaoResgatada = idPosicaoResgatada;
                        detalheResgateCotista.IdCotista = idCotista;
                        detalheResgateCotista.IdCarteira = idCarteira;
                        detalheResgateCotista.Quantidade = quantidadeBaixa;
                        detalheResgateCotista.ValorBruto = valorBruto;
                        detalheResgateCotista.ValorLiquido = valorLiquidoResgatar;
                        detalheResgateCotista.PrejuizoUsado = prejuizoUsado;
                        detalheResgateCotista.RendimentoResgate = rendimentoCompensado;
                        detalheResgateCotista.VariacaoResgate = variacao;
                        detalheResgateCotista.ValorIR = valorIR;
                        detalheResgateCotista.ValorIOF = 0;
                        detalheResgateCotista.Save();
                    }
                    catch (Exception e)
                    {
                        detalheResgateCotista = new DetalheResgateCotista();
                        if (detalheResgateCotista.LoadByPrimaryKey(idOperacao, idPosicaoResgatada))
                        {
                            detalheResgateCotista.Quantidade += quantidadeBaixa;
                            detalheResgateCotista.ValorBruto += valorBruto;
                            detalheResgateCotista.ValorLiquido += valorLiquidoResgatar;
                            detalheResgateCotista.PrejuizoUsado += prejuizoUsado;
                            detalheResgateCotista.RendimentoResgate += rendimentoCompensado;
                            detalheResgateCotista.VariacaoResgate += variacao;
                            detalheResgateCotista.ValorIR += valorIR;
                            detalheResgateCotista.Save();
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Insert em DetalheResgateFundo
                    DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                    try
                    {
                        detalheResgateFundo.IdOperacao = idOperacao;
                        detalheResgateFundo.IdPosicaoResgatada = idPosicaoResgatada;
                        detalheResgateFundo.IdCliente = idCliente;
                        detalheResgateFundo.IdCarteira = idCarteira;
                        detalheResgateFundo.Quantidade = quantidadeBaixa;
                        detalheResgateFundo.ValorBruto = valorBruto;
                        detalheResgateFundo.ValorLiquido = valorLiquidoResgatar;
                        detalheResgateFundo.PrejuizoUsado = prejuizoUsado;
                        detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                        detalheResgateFundo.VariacaoResgate = variacao;
                        detalheResgateFundo.Save();
                    }
                    catch (Exception e)
                    {
                        detalheResgateFundo = new DetalheResgateFundo();
                        detalheResgateFundo.LoadByPrimaryKey(idOperacao, idPosicaoResgatada);
                        detalheResgateFundo.Quantidade += quantidadeBaixa;
                        detalheResgateFundo.ValorBruto += valorBruto;
                        detalheResgateFundo.ValorLiquido += valorLiquidoResgatar;
                        detalheResgateFundo.PrejuizoUsado += prejuizoUsado;
                        detalheResgateFundo.RendimentoResgate += rendimentoCompensado;
                        detalheResgateFundo.VariacaoResgate += variacao;
                        detalheResgateFundo.Save();
                    }

                    #endregion
                }
                #endregion

                if (!posicaoSuficiente)
                {
                    saldoLiquidoRestante -= valorLiquidoResgatar;
                }
                else
                {
                    break;
                }
            }
            #endregion

            //Se valor/qtde solicitada do resgate for acima do q tem em posição, usa os valores computados no loop
            //Caso contrário, usa os valores da própria operação
            if (!posicaoSuficiente)
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoResgate);
            }
            else
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorLiquidoOperacao + valorIRResgate + valorPerformanceResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoOperacao);
            }

            #region Atualiza OperacaoCotista/OperacaoFundo (cota, tributos, valor bruto/liquido, qtde) -> Save() no escopo superior
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade, quantidadeResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, cotaDia);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIR, valorIRResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIOF, 0);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, prejuizoUsadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, rendimentoCompensadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, variacaoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, valorPerformanceResgate);
            #endregion

            //Atualiza qtde de cotas das posições baixadas
            posicaoCollection.Save();
        }

        /// <summary>
        /// Processa resgates líquidos (específico, FIFO, LIFO, menor imposto) para Fundos de Ações.
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// </param>
        /// <param name="data"></param>
        /// <param name="carteira"></param>
        /// <param name="operacao">Operacao Cotista ou Operacao Fundo</param>
        /// <param name="posicaoCollection">PosicaoCotista Ou PosicaoFundo</param>
        /// <param name="cotaDia"></param>
        /// <param name="projecaoCalculo">
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="ArgumentException">
        /// Se operacao nao for do tipo OperacaoCotista ou OperacaoFundo
        /// Se posicaoCollection nao for do tipo PosicaoCotistaCollection Ou PosicaoFundoCollection
        /// </exception>
        private void ProcessaResgateLiquidoAcoes(int idCarteiraCliente, DateTime data, Carteira carteira,
                                          esEntity operacao, esEntityCollection posicaoCollection,
                                          decimal cotaDia, bool calculaPfee, params bool[] projecaoCalculo)
        {
            bool pegaCotaAplicacaoPosicao = ParametrosConfiguracaoSistema.Fundo.UsaCotaInicialPosicaoIR == "S";

            #region Trata Exception
            if ((operacao.GetType() != typeof(OperacaoCotista) &&
                   operacao.GetType() != typeof(OperacaoFundo)) ||

                   (posicaoCollection.GetType() != typeof(PosicaoCotistaCollection) &&
                    posicaoCollection.GetType() != typeof(PosicaoFundoCollection))
               )
            {

                throw new ArgumentException("Parâmetro Incorreto: Tipo de Objeto Inválido");
            }
            #endregion

            #region Carrega os parâmetros da carteira, para cálculo dos tributos
            int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            #region Carrega as variáveis principais (da operação), inclusive o valor Liquido
            int? idCotista = null;
            int? idCarteira = null;
            int? idCliente = null;

            int idOperacao = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
            DateTime dataOperacao = (DateTime)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataOperacao);

            int tipoOperacao = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoOperacao));

            int? tipoResgate = null;
            if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate)))
            {
                tipoResgate = Convert.ToInt16(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate));
            }
            //                                   
            decimal valorLiquidoOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido);

            //
            if (operacao is OperacaoCotista)
            {
                idCarteira = idCarteiraCliente;
                idCotista = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCotista);

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada));
                }
            }
            else if (operacao is OperacaoFundo)
            {
                idCarteira = (int)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdCarteira);
                idCliente = idCarteiraCliente;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada));
                }
            }
            #endregion

            #region Busca isenção em tributos
            Cotista cotista = null;
            Cliente cliente = null;
            Pessoa pessoa = new Pessoa();

            if (operacao is OperacaoCotista)
            {
                cotista = new Cotista();
                cotista.BuscaTributacaoCotista(idCotista.Value);

                pessoa = cotista.UpToPessoaByIdPessoa;

                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdPessoa);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                cliente.LoadByPrimaryKey(campos, idCarteiraCliente);

                //Para os casos do cotista fora do controle de fundos e clubes, não há calculo de tributos
                if (cliente.IdTipo.Value != TipoClienteFixo.Clube &&
                    cliente.IdTipo.Value != TipoClienteFixo.Fundo &&
                    cliente.IdTipo.Value != TipoClienteFixo.FDIC)
                {
                    cotista.IsentoIR = "S";
                    cotista.IsentoIOF = "S";
                }
            }
            else
            {
                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IsentoIR);
                campos.Add(cliente.Query.IsentoIOF);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);

                pessoa = cotista.UpToPessoaByIdPessoa;
            }
            #endregion

            ListaTipoInvestidor tipoInvestidor;
            if (pessoa.Tipo.HasValue)
            {
                if (pessoa.Tipo.Value == 1)
                    tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                else
                    tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
            }
            else
                tipoInvestidor = ListaTipoInvestidor.OffShore;

            //A ser computado dentro do loop
            decimal quantidadeResgate = 0, valorBrutoResgate = 0, valorLiquidoResgate = 0,
                    valorIRResgate = 0, variacaoResgate = 0, rendimentoCompensadoResgate = 0, prejuizoUsadoResgate = 0, valorPerformanceResgate = 0;
            //
            decimal saldoLiquidoRestante = valorLiquidoOperacao;
            //
            bool posicaoSuficiente = false;
            //
            CalculoTributo calculoTributo = new CalculoTributo();

            #region Loop de posições sendo resgatadas
            int i = 0;
            while (i < posicaoCollection.Count)
            {
                int id = operacao is OperacaoCotista ? idCotista.Value : idCliente.Value;
                TipoProcessaResgate t = operacao is OperacaoCotista ? TipoProcessaResgate.OperacaoCotista : TipoProcessaResgate.OperacaoFundo;
                bool projecao = operacao is OperacaoCotista ? projecaoCalculo[0] : false;

                #region Dados da posicao, cotaAplicacao, valorPerformanceEmCota
                int idPosicaoResgatada = (int)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao);
                decimal quantidadePosicao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade) -
                                            (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeBloqueada);
                DateTime dataConversao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao);

                DateTime? dataUltimoCortePfee = null;
                if (!Convert.IsDBNull((((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee))))
                {
                    dataUltimoCortePfee = Convert.ToDateTime(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee));
                }

                bool posicaoIncorporada = (string)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.PosicaoIncorporada) == "S";

                decimal cotaAplicacao = 0;
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = operacao is OperacaoCotista
                                    ? ((PosicaoCotista)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, idCotista.Value)
                                    : ((PosicaoFundo)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, idCliente.Value);
                }
                else
                {
                    if (!pegaCotaAplicacaoPosicao)
                    {
                        HistoricoCota historicoCotaAplicacao = new HistoricoCota();

                        if (posicaoIncorporada)
                        {
                            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.LessThan(data));
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraDestino.Equal(idCarteira));
                            eventoFundoCollection.Query.Load();
                            if (eventoFundoCollection.Count > 0)
                            {
                                historicoCotaAplicacao.CotaFechamento = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                                historicoCotaAplicacao.CotaAbertura = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                            }
                            else
                            {
                                historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao, data); //Trata posição incorporada
                            }

                        }
                        else
                        {
                            historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao);
                        }

                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                        }
                    }
                    else
                    {
                        cotaAplicacao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                    }
                }

                //Calcula performance em valor de cota (a ser usado em 2 situações adiante)
                decimal valorPerformanceEmCota = 0;
                if (calculaPfee)
                {
                    valorPerformanceEmCota = this.RetornaValorPerformanceResgate(idCarteira.Value, 1, data, dataConversao, dataUltimoCortePfee);
                }
                #endregion
                #region Calcula a quantidade de baixa (provisória) da posição - não leva em conta prej a compensar

                #region Calcula IR da quantidade da posição (NÃO leva em conta prejuízo a compensar) -> valorIRPosicao
                decimal valorIRPosicao = 0;

                bool isentoIR = operacao is OperacaoCotista ? cotista.IsIsentoIR() : cliente.IsIsentoIR();

                bool tributaNaoResidente = operacao is OperacaoCotista ? cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente :
                                                                         cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;

                if (!isentoIR)
                {
                    CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                    calculoIRFundoAcoes.Quantidade = quantidadePosicao;
                    calculoIRFundoAcoes.PrejuizoCompensar = 0;
                    calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoAcoes.CotaCalculo = cotaDia;
                    calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                    calculoIRFundoAcoes.DataCalculo = data;
                    calculoIRFundoAcoes.DataAplicacao = dataConversao;
                    calculoIRFundoAcoes.IdCarteira = idCarteira.Value;
                    calculoIRFundoAcoes.IdPosicao = idPosicaoResgatada;
                    calculoIRFundoAcoes.TipoInvestidor = Convert.ToInt16(tipoInvestidor);
                    calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);
                    valorIRPosicao = calculoTributo.IR;
                }
                #endregion

                //Calcula performance da posição
                decimal valorPerformancePosicao = Math.Round(valorPerformanceEmCota * quantidadePosicao, 2);

                decimal saldoBrutoPosicao = truncaFinanceiro
                                            ? Utilitario.Truncate(quantidadePosicao * cotaDia, 2)
                                            : Math.Round(quantidadePosicao * cotaDia, 2);

                decimal saldoLiquidoPosicao = saldoBrutoPosicao - valorIRPosicao - valorPerformancePosicao;

                decimal valorIRAux = 0;
                decimal valorPerformanceAux = 0;
                if (saldoLiquidoPosicao > 0)
                {
                    valorIRAux = saldoLiquidoRestante * valorIRPosicao / saldoLiquidoPosicao;
                    valorPerformanceAux = saldoLiquidoRestante * valorPerformancePosicao / saldoLiquidoPosicao;
                }

                decimal valorBrutoAux = Math.Round(saldoLiquidoRestante + valorIRAux + valorPerformanceAux, 2);

                decimal quantidadeBaixa;
                if ((saldoLiquidoRestante > saldoLiquidoPosicao))
                {
                    quantidadeBaixa = quantidadePosicao;
                }
                else
                {
                    quantidadeBaixa = truncaQuantidade
                                      ? Utilitario.Truncate(valorBrutoAux / cotaDia, casasDecimaisQuantidade)
                                      : Math.Round(valorBrutoAux / cotaDia, casasDecimaisQuantidade);

                    //Se baixar a quantidade restante inteira, prepara para sair do loop de posições
                    if (quantidadeBaixa < quantidadePosicao)
                    {
                        posicaoSuficiente = true;
                    }
                    else
                    { //Garante só resgatar o máximo da posição
                        quantidadeBaixa = quantidadePosicao;
                    }
                }
                #endregion

                #region Calcula rendimentos e IR (leva em conta eventual prejuízo a compensar)
                decimal prejuizoCompensar = 0;

                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();

                if (!isentoIR)
                {
                    #region Busca prejuízo a compensar
                    if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                        {
                            if (!projecao)
                            {
                                #region Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                                prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id);

                                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                                {
                                    decimal valorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value;

                                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsado.RetornaPrejuizoUsado(id, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                                    valorPrejuizo -= prejuizoUsadoHistorico;

                                    if (valorPrejuizo < 0)
                                    {
                                        valorPrejuizo = 0;
                                    }

                                    prejuizoCompensar += valorPrejuizo;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoCompensar = prejuizoFundo.RetornaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id);
                        }
                    }
                    #endregion
                }

                decimal rendimento = 0;
                decimal rendimentoCompensado = 0;
                decimal prejuizoUsado = 0;
                decimal valorIR = 0;

                if (!isentoIR)
                {
                    CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                    calculoIRFundoAcoes.Quantidade = quantidadeBaixa;
                    calculoIRFundoAcoes.PrejuizoCompensar = prejuizoCompensar;
                    calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoAcoes.CotaCalculo = cotaDia;
                    calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                    calculoIRFundoAcoes.DataCalculo = data;
                    calculoIRFundoAcoes.DataAplicacao = dataConversao;
                    calculoIRFundoAcoes.IdCarteira = idCarteira.Value;
                    calculoIRFundoAcoes.IdPosicao = idPosicaoResgatada;
                    calculoIRFundoAcoes.TipoInvestidor = Convert.ToInt16(tipoInvestidor);
                    calculoIRFundoAcoes.DataAplicacao = dataConversao;
                    calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);
                    valorIR = calculoTributo.IR;
                    rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                    rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                    prejuizoUsado = calculoTributo.PrejuizoUsado;

                    #region Trata compensação de prejuízo
                    if (operacao is OperacaoCotista) //Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                    {
                        if (!projecao)
                        {
                            PrejuizoCotista prejuizoCotista = new PrejuizoCotista();

                            if (rendimento < 0)
                            {
                                prejuizoCotista.AcumulaPrejuizo(id, idCarteira.Value, data, rendimento);
                            }

                            prejuizoCotista.CompensaPrejuizo(data, idCarteira.Value, id, prejuizoUsado, prejuizoCotistaCollection);
                        }
                    }
                    else
                    {
                        PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                        prejuizoFundo.TrataCompensacaoPrejuizo(data, idAgenteAdministrador, tipoTributacao, idCarteira.Value, id, rendimento, prejuizoUsado);
                    }
                    #endregion
                }
                #endregion

                #region Calcula a qtde final de baixa da posição
                if (prejuizoUsado != 0) //tem que ajustar a qtde, dado que o prejuizo usado diminui a qtde resgatada da posição
                {
                    #region Calcula IR da quantidade baixada (NÃO leva em conta prejuízo a compensar)
                    CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                    calculoIRFundoAcoes.Quantidade = quantidadeBaixa;
                    calculoIRFundoAcoes.PrejuizoCompensar = 0;
                    calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoAcoes.CotaCalculo = cotaDia;
                    calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                    calculoIRFundoAcoes.DataCalculo = data;
                    calculoIRFundoAcoes.DataAplicacao = dataConversao;
                    calculoIRFundoAcoes.TipoInvestidor = Convert.ToInt16(tipoInvestidor);
                    calculoIRFundoAcoes.IdCarteira = idCarteira.Value;
                    calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);

                    decimal valorIRPosicaoAux = calculoTributo.IR;
                    #endregion

                    //Calcula o valor bruto levando em conta IR sem abater prejuizo
                    decimal valorBrutoPosicaoAux = Math.Round(quantidadeBaixa * cotaDia, 2);

                    quantidadeBaixa = truncaQuantidade
                                  ? Utilitario.Truncate((valorBrutoPosicaoAux - valorIRPosicaoAux + valorIR) / cotaDia, casasDecimaisQuantidade)
                                  : Math.Round((valorBrutoPosicaoAux - valorIRPosicaoAux + valorIR) / cotaDia, casasDecimaisQuantidade);
                }
                #endregion

                decimal valorPerformance = Math.Round(valorPerformanceEmCota * quantidadeBaixa, 2);

                quantidadeResgate += quantidadeBaixa;

                #region Calcula valores (bruto, líquido) da operação e adiciona as variáveis gerais do resgate!
                decimal valorBruto = truncaFinanceiro
                                     ? Utilitario.Truncate(quantidadeBaixa * cotaDia, 2)
                                     : Math.Round(quantidadeBaixa * cotaDia, 2);

                decimal valorLiquido = valorBruto - valorIR;

                decimal variacao = Math.Round(quantidadeBaixa * (cotaDia - cotaAplicacao), 2);

                valorBrutoResgate += valorBruto;
                valorLiquidoResgate += valorLiquido;
                valorIRResgate += valorIR;
                variacaoResgate += variacao;
                rendimentoCompensadoResgate += rendimentoCompensado;
                prejuizoUsadoResgate += prejuizoUsado;
                valorPerformanceResgate += valorPerformance;
                #endregion

                //Baixa a posição em quantidade
                if (operacao is OperacaoCotista)
                {
                    if (!projecaoCalculo[0])
                    {
                        PosicaoCotista p = (PosicaoCotista)posicaoCollection[i];
                        p.Quantidade -= quantidadeBaixa;
                        p.QuantidadeAntesCortes -= quantidadeBaixa;

                        if (p.Quantidade == 0)
                        {
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;
                            i++;
                        }
                        else
                        {
                            p.ValorPerformance -= valorPerformance;

                            //Checa eventuais resíduos na qtde por ter compensado prejuizo. 
                            //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                            decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                            if (prejuizoUsado == 0 || posicaoSuficiente)
                            {
                                i++;
                            }
                            else if (valorPosicaoResidual < 0.01M)
                            {
                                p.Quantidade = 0;
                                p.ValorBruto = 0;
                                p.ValorLiquido = 0;
                                p.ValorIR = 0;
                                p.ValorIOF = 0;
                                p.ValorPerformance = 0;
                                p.ValorIOFVirtual = 0;

                                i++;
                            }
                        }
                    }
                }
                else
                {
                    PosicaoFundo p = (PosicaoFundo)posicaoCollection[i];
                    p.Quantidade -= quantidadeBaixa;
                    p.QuantidadeAntesCortes -= quantidadeBaixa;

                    if (p.Quantidade == 0)
                    {
                        p.ValorBruto = 0;
                        p.ValorLiquido = 0;
                        p.ValorIR = 0;
                        p.ValorIOF = 0;
                        p.ValorPerformance = 0;
                        p.ValorIOFVirtual = 0;
                        i++;
                    }
                    else
                    {
                        //Checa eventuais resíduos na qtde por ter compensado prejuizo. 
                        //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                        decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                        if (prejuizoUsado == 0 || posicaoSuficiente)
                        {
                            i++;
                        }
                        else if (valorPosicaoResidual < 0.01M)
                        {
                            p.Quantidade = 0;
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;

                            i++;
                        }
                    }
                }


                if (operacao is OperacaoCotista)
                {
                    #region Insert em DetalheResgateCotista
                    DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                    try
                    {
                        detalheResgateCotista.IdOperacao = idOperacao;
                        detalheResgateCotista.IdPosicaoResgatada = idPosicaoResgatada;
                        detalheResgateCotista.IdCotista = idCotista;
                        detalheResgateCotista.IdCarteira = idCarteira;
                        detalheResgateCotista.Quantidade = quantidadeBaixa;
                        detalheResgateCotista.ValorBruto = valorBruto;
                        detalheResgateCotista.ValorLiquido = valorLiquido;
                        detalheResgateCotista.PrejuizoUsado = prejuizoUsado;
                        detalheResgateCotista.RendimentoResgate = rendimentoCompensado;
                        detalheResgateCotista.VariacaoResgate = variacao;
                        detalheResgateCotista.ValorIR = valorIR;
                        detalheResgateCotista.ValorIOF = 0;
                        detalheResgateCotista.Save();
                    }
                    catch (Exception e)
                    {
                        detalheResgateCotista = new DetalheResgateCotista();
                        if (detalheResgateCotista.LoadByPrimaryKey(idOperacao, idPosicaoResgatada))
                        {
                            detalheResgateCotista.Quantidade += quantidadeBaixa;
                            detalheResgateCotista.ValorBruto += valorBruto;
                            detalheResgateCotista.ValorLiquido += valorLiquido;
                            detalheResgateCotista.PrejuizoUsado += prejuizoUsado;
                            detalheResgateCotista.RendimentoResgate += rendimentoCompensado;
                            detalheResgateCotista.VariacaoResgate += variacao;
                            detalheResgateCotista.ValorIR += valorIR;
                            detalheResgateCotista.Save();
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Insert em DetalheResgateFundo
                    DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                    try
                    {
                        detalheResgateFundo.IdOperacao = idOperacao;
                        detalheResgateFundo.IdPosicaoResgatada = idPosicaoResgatada;
                        detalheResgateFundo.IdCliente = idCliente;
                        detalheResgateFundo.IdCarteira = idCarteira;
                        detalheResgateFundo.Quantidade = quantidadeBaixa;
                        detalheResgateFundo.ValorBruto = valorBruto;
                        detalheResgateFundo.ValorLiquido = valorLiquido;
                        detalheResgateFundo.PrejuizoUsado = prejuizoUsado;
                        detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                        detalheResgateFundo.VariacaoResgate = variacao;
                        detalheResgateFundo.Save();
                    }
                    catch (Exception e)
                    {
                        detalheResgateFundo = new DetalheResgateFundo();
                        if (detalheResgateFundo.LoadByPrimaryKey(idOperacao, idPosicaoResgatada))
                        {
                            detalheResgateFundo.Quantidade += quantidadeBaixa;
                            detalheResgateFundo.ValorBruto += valorBruto;
                            detalheResgateFundo.ValorLiquido += valorLiquido;
                            detalheResgateFundo.PrejuizoUsado += prejuizoUsado;
                            detalheResgateFundo.RendimentoResgate += rendimentoCompensado;
                            detalheResgateFundo.VariacaoResgate += variacao;
                            detalheResgateFundo.Save();
                        }
                    }

                    #endregion
                }

                if (!posicaoSuficiente)
                {
                    saldoLiquidoRestante -= valorLiquido;
                }
                else
                {
                    break;
                }

            }
            #endregion

            //Se valor/qtde solicitada do resgate for acima do q tem em posição, usa os valores computados no loop
            //Caso contrário, usa os valores da própria operação
            if (!posicaoSuficiente)
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoResgate);
            }
            else
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorLiquidoOperacao + valorIRResgate + valorPerformanceResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoOperacao);
            }

            #region Atualiza OperacaoCotista/OperacaoFundo (cota, tributos, valor bruto/liquido, qtde) -> Save() no escopo superior
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade, quantidadeResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, cotaDia);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIR, valorIRResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIOF, 0);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, prejuizoUsadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, rendimentoCompensadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, variacaoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, valorPerformanceResgate);
            #endregion

            //Atualiza qtde de cotas das posições baixadas
            posicaoCollection.Save();
        }

        /// <summary>
        /// Processa resgates líquidos (específico, FIFO, LIFO, menor imposto) para Fundos de Ações.
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// </param>
        /// <param name="data"></param>
        /// <param name="carteira"></param>
        /// <param name="operacao">Operacao Cotista ou Operacao Fundo</param>
        /// <param name="posicaoCollection">PosicaoCotista Ou PosicaoFundo</param>
        /// <param name="cotaDia"></param>
        /// <param name="projecaoCalculo">
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="ArgumentException">
        /// Se operacao nao for do tipo OperacaoCotista ou OperacaoFundo
        /// Se posicaoCollection nao for do tipo PosicaoCotistaCollection Ou PosicaoFundoCollection
        /// </exception>
        private void ProcessaResgateLiquidoRF(int idCarteiraCliente, DateTime data, Carteira carteira,
                                                 esEntity operacao, esEntityCollection posicaoCollection,
                                                 decimal cotaDia, bool calculaPfee, params bool[] projecaoCalculo)
        {
            bool pegaCotaAplicacaoPosicao = ParametrosConfiguracaoSistema.Fundo.UsaCotaInicialPosicaoIR == "S";

            #region Trata Exception
            if ((operacao.GetType() != typeof(OperacaoCotista) &&
                   operacao.GetType() != typeof(OperacaoFundo)) ||

                   (posicaoCollection.GetType() != typeof(PosicaoCotistaCollection) &&
                    posicaoCollection.GetType() != typeof(PosicaoFundoCollection))
               )
            {

                throw new ArgumentException("Parâmetro Incorreto: Tipo de Objeto Inválido");
            }
            #endregion

            #region Carrega os parâmetros da carteira, para cálculo dos tributos
            //int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoTributacao = Carteira.RetornaTipoTributacao(carteira.IdCarteira.Value, data);
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            #region Carrega as variáveis principais (da operação), inclusive o valor Liquido
            int? idCarteira = null;
            int? id = null;
            TipoProcessaResgate? tipoProcessaResgate = null;
            //          
            int idOperacao = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
            DateTime dataOperacao = (DateTime)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataOperacao);
            //
            int tipoOperacao = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoOperacao));

            int? tipoResgate = null;
            if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate)))
            {
                tipoResgate = Convert.ToInt16(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate));
            }
            //                     
            decimal valorLiquidoOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido);
            //
            if (operacao is OperacaoCotista)
            {
                idCarteira = idCarteiraCliente;
                id = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCotista);
                tipoProcessaResgate = TipoProcessaResgate.OperacaoCotista;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada));
                }
            }
            else if (operacao is OperacaoFundo)
            {
                idCarteira = (int)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdCarteira);
                id = idCarteiraCliente;
                tipoProcessaResgate = TipoProcessaResgate.OperacaoFundo;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada));
                }
            }
            #endregion

            #region Busca isenção de cotista em IR e IOF
            Pessoa pessoa = new Pessoa();
            Cotista cotista = null;
            Cliente cliente = null;
            //
            if (operacao is OperacaoCotista)
            {
                cotista = new Cotista();
                cotista.BuscaTributacaoCotista(id.Value);

                pessoa = cotista.UpToPessoaByIdPessoa;

                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IsentoIR);
                campos.Add(cliente.Query.IsentoIOF);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idCarteiraCliente);

                //Para os casos do cotista fora do controle de fundos e clubes, não há calculo de tributos
                if (cliente.IdTipo.Value != TipoClienteFixo.Clube &&
                    cliente.IdTipo.Value != TipoClienteFixo.Fundo &&
                    cliente.IdTipo.Value != TipoClienteFixo.FDIC)
                {
                    cotista.IsentoIR = "S";
                    cotista.IsentoIOF = "S";
                }
            }
            else
            {
                cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IsentoIR);
                campos.Add(cliente.Query.IsentoIOF);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, id.Value);

                pessoa = cliente.UpToPessoaByIdPessoa;
            }
            #endregion

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoResgate(idCarteira.Value, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);
            int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira.Value, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            //A ser computado dentro do loop
            decimal quantidadeResgate = 0;
            decimal valorBrutoResgate = 0;
            decimal valorLiquidoResgate = 0;
            decimal valorIRResgate = 0;
            decimal valorIOFResgate = 0;
            decimal variacaoResgate = 0;
            decimal rendimentoCompensadoResgate = 0;
            decimal prejuizoUsadoResgate = 0;
            decimal valorPerformanceResgate = 0;
            //
            decimal saldoLiquidoRestante = valorLiquidoOperacao;
            //
            bool posicaoSuficiente = false;
            //
            CalculoTributo calculoTributo = new CalculoTributo();
            //
            #region Loop de posições sendo resgatadas
            int i = 0;
            while (i < posicaoCollection.Count)
            {
                bool projecao = operacao is OperacaoCotista ? projecaoCalculo[0] : false;

                #region Dados da Posicao, cotaAplicacao, valorPerformanceEmCota
                int idPosicaoResgatada = (int)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao);
                decimal quantidadePosicao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade) -
                                            (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeBloqueada);
                decimal quantidadeAntesCortes = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeAntesCortes);
                DateTime dataAplicacao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao);
                DateTime dataConversao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao);
                DateTime dataLiquidacao = this.RetornaDataLiquidacao((esEntity)posicaoCollection[i]);
                decimal iofVirtual = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.ValorIOFVirtual);
                DateTime? dataUltimoCortePfee = null;
                if (!Convert.IsDBNull((((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee))))
                {
                    dataUltimoCortePfee = Convert.ToDateTime(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee));
                }

                bool posicaoIncorporada = (string)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.PosicaoIncorporada) == "S";

                decimal cotaAplicacao = 0;
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = operacao is OperacaoCotista
                                    ? ((PosicaoCotista)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, id.Value)
                                    : ((PosicaoFundo)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, id.Value);
                }
                else
                {
                    if (!pegaCotaAplicacaoPosicao)
                    {
                        HistoricoCota historicoCotaAplicacao = new HistoricoCota();

                        if (posicaoIncorporada)
                        {
                            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.LessThan(data));
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraDestino.Equal(idCarteira));
                            eventoFundoCollection.Query.Load();
                            if (eventoFundoCollection.Count > 0)
                            {
                                historicoCotaAplicacao.CotaFechamento = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                                historicoCotaAplicacao.CotaAbertura = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                            }
                            else
                            {
                                historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao, data); //Trata posição incorporada
                            }
                        }
                        else
                        {
                            historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao);
                        }

                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                        }
                    }
                    else
                    {
                        cotaAplicacao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                    }
                }

                //Calcula performance em valor de cota (a ser usado em 2 situações adiante)
                decimal valorPerformanceEmCota = 0;
                if (calculaPfee)
                {
                    valorPerformanceEmCota = this.RetornaValorPerformanceResgate(idCarteira.Value, 1, data, dataConversao, dataUltimoCortePfee);
                }

                bool aliquotaDiferenciada = true;

                //Avalia se deve usar a quantidade antes de cortes ou a quantidade real da posição!
                int diasTotal = Calendario.NumeroDias(dataConversao, data);
                if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo && diasTotal > 180 ||
                    tipoTributacao == (int)TipoTributacaoFundo.LongoPrazo && diasTotal > 720)
                {
                    aliquotaDiferenciada = false;
                }
                #endregion

                #region Calcula a quantidade de baixa (provisória) da posição e o valor liquido - não leva em conta prej a compensar

                #region Calcula IR e IOF da quantidade da posição (NÃO leva em conta prejuízo a compensar) -> valorIRPosicao, valorIOFPosicao
                decimal valorIRPosicao = 0;
                decimal valorIOFPosicao = 0;

                bool isentoIR = operacao is OperacaoCotista ? cotista.IsIsentoIR() : cliente.IsIsentoIR();
                bool isentoIOF = operacao is OperacaoCotista ? cotista.IsIsentoIOF() : cliente.IsIsentoIOF();
                bool tributaNaoResidente = operacao is OperacaoCotista ? cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente :
                                                                         cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;

                if (!isentoIR && tipoTributacao != (int)TipoTributacaoFundo.Isento && !carteira.Fie.Equals("S"))
                {
                    ListaTipoInvestidor tipoInvestidor;
                    if (pessoa.Tipo.HasValue)
                    {
                        if (pessoa.Tipo == 1)
                            tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                        else
                            tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
                    }
                    else
                    {
                        tipoInvestidor = ListaTipoInvestidor.OffShore;
                    }

                    if (!calculoTributo.calculoExcecaoIR(idCarteira.Value, dataAplicacao, data, quantidadeResgate, isentoIOF, tipoInvestidor))
                    {
                        if (tipoTributacao == (int)TipoTributacaoFundo.AliquotaEspecifica)
                        {
                            calculoTributo.calculoAliquotaEspecifica(idCarteira.Value,
                                                                     dataAplicacao,
                                                                     data,
                                                                     aliquotaDiferenciada ? quantidadeAntesCortes : quantidadePosicao,
                                                                     isentoIOF);
                        }
                        else
                        {
                            CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                            calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                            calculoIRFundoRendaFixa.IdFundo = idCarteira;

                            calculoIRFundoRendaFixa.Quantidade = aliquotaDiferenciada ? quantidadeAntesCortes : quantidadePosicao;

                            calculoIRFundoRendaFixa.TipoCota = tipoCota;
                            calculoIRFundoRendaFixa.PrejuizoCompensar = 0;
                            calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                            calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                            calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                            calculoIRFundoRendaFixa.DataConversao = dataConversao;
                            calculoIRFundoRendaFixa.DataCalculo = data;
                            calculoIRFundoRendaFixa.DataUltimaCobrancaIR = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR);
                            calculoIRFundoRendaFixa.IdPosicao = idPosicaoResgatada;
                            calculoIRFundoRendaFixa.TipoProcessamentoResgate = tipoProcessaResgate;
                            calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                            calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                            calculoIRFundoRendaFixa.IsentoIOF = isentoIOF;
                            calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;
                            calculoIRFundoRendaFixa.IOFVirtual = iofVirtual;
                            calculoIRFundoRendaFixa.QuantidadePosicao = Convert.ToDecimal(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade));
                            calculoIRFundoRendaFixa.QuantidadeAntesCortes = Convert.ToDecimal(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeAntesCortes));
                            calculoIRFundoRendaFixa.DataLiquidacao = dataLiquidacao;
                            calculoIRFundoRendaFixa.DataLiquidacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataLiquidacao);
                            calculoIRFundoRendaFixa.DataConversaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataConversao);
                            calculoIRFundoRendaFixa.DataOperacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataOperacao);
                            //
                            DebugCalculaIRFundoRendaFixa debugCalculaIRFundoRendaFixa = new DebugCalculaIRFundoRendaFixa();

                            calculoTributo.CalculaIRFundoRendaFixa(calculoIRFundoRendaFixa, true,
                                out debugCalculaIRFundoRendaFixa);

                            valorIRPosicao = calculoTributo.IR;
                            valorIOFPosicao = calculoTributo.IOF;
                        }
                    }
                }
                else if (!isentoIOF)
                {
                    CalculoIOF calculoIOF = new CalculoIOF();
                    calculoIOF.IdFundo = idCarteira;
                    calculoIOF.Quantidade = quantidadeAntesCortes;
                    calculoIOF.CotaAplicacao = cotaAplicacao;
                    calculoIOF.CotaCalculo = cotaDia;
                    calculoIOF.DataAplicacao = dataAplicacao;
                    calculoIOF.DataCalculo = data;
                    calculoIOF.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoTributo.CalculaIOFFundo(calculoIOF);
                    valorIOFPosicao = calculoTributo.IOF;
                }
                #endregion

                //Calcula performance da posição
                decimal valorPerformancePosicao = Math.Round(valorPerformanceEmCota * quantidadePosicao, 2);

                decimal saldoBrutoPosicao = truncaFinanceiro
                            ? Utilitario.Truncate(quantidadePosicao * cotaDia, 2)
                            : Math.Round(quantidadePosicao * cotaDia, 2);

                decimal saldoLiquidoPosicao = saldoBrutoPosicao - valorIRPosicao - valorIOFPosicao - valorPerformancePosicao;

                byte RESGATE_TOTAL = operacao is OperacaoCotista ? (byte)TipoOperacaoCotista.ResgateTotal : (byte)TipoOperacaoFundo.ResgateTotal;
                decimal quantidadeBruta;
                decimal valorLiquido;
                if ((saldoLiquidoRestante > saldoLiquidoPosicao))
                {
                    valorLiquido = saldoLiquidoPosicao;

                    quantidadeBruta = aliquotaDiferenciada ? quantidadeAntesCortes : quantidadePosicao;
                }
                else
                {
                    valorLiquido = saldoLiquidoRestante;

                    quantidadeBruta = aliquotaDiferenciada
                              ? saldoLiquidoRestante * quantidadeAntesCortes / saldoLiquidoPosicao
                              : saldoLiquidoRestante * quantidadePosicao / saldoLiquidoPosicao;

                }
                #endregion

                #region Calcula rendimentos e IR (leva em conta prejuízo a compensar), pela proporcao entre o vl liquido e o saldo liquido da posicao
                decimal prejuizoCompensar = 0;
                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection(); //Usado apenas no caso de ser uma chamada da parte do Cotista

                if (!isentoIR)
                {
                    #region Busca prejuízo a compensar
                    if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                        {
                            if (!projecao)
                            {
                                #region Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                                prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id.Value);

                                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                                {
                                    decimal valorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value;

                                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsado.RetornaPrejuizoUsado(id.Value, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                                    valorPrejuizo -= prejuizoUsadoHistorico;

                                    if (valorPrejuizo < 0)
                                    {
                                        valorPrejuizo = 0;
                                    }

                                    prejuizoCompensar += valorPrejuizo;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoCompensar = prejuizoFundo.RetornaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id.Value);
                        }
                    }
                    #endregion
                }

                decimal rendimento = 0;
                decimal rendimentoCompensado = 0;
                decimal prejuizoUsado = 0;
                decimal valorIR = 0;
                decimal valorIOF = 0;

                if (!isentoIR && tipoTributacao != (int)TipoTributacaoFundo.Isento)
                {
                    ListaTipoInvestidor tipoInvestidor;
                    if (pessoa.Tipo.HasValue)
                    {
                        if (pessoa.Tipo.Value == 1)
                            tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                        else
                            tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
                    }
                    else
                        tipoInvestidor = ListaTipoInvestidor.OffShore;

                    if (calculoTributo.calculoExcecaoIR(idCarteira.Value, dataAplicacao, data, quantidadeResgate, isentoIOF, tipoInvestidor))
                    {
                        valorIR = calculoTributo.IR;
                        valorIOF = calculoTributo.IOF;
                    }
                    else if (carteira.Fie.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                        {
                            calculoTributo.calculoIRFieCotista(idPosicaoResgatada,
                                                               cotaDia,
                                                               quantidadePosicao,
                                                               isentoIOF,
                                                               diasLiquidacaoResgate,
                                                               data);
                        }
                        else
                        {
                            calculoTributo.calculoIRFieFundo(idPosicaoResgatada,
                                                               cotaDia,
                                                               quantidadePosicao,
                                                               isentoIOF,
                                                               diasLiquidacaoResgate,
                                                               data);
                        }

                        valorIR = calculoTributo.IR;
                        valorIOF = calculoTributo.IOF;

                    }
                    else
                    {
                        CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                        calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                        calculoIRFundoRendaFixa.TipoCota = tipoCota;
                        calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                        calculoIRFundoRendaFixa.DataCalculo = data;
                        calculoIRFundoRendaFixa.TipoProcessamentoResgate = tipoProcessaResgate;
                        calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;

                        calculoTributo.CalculaIRFundoRendaFixaLiquido(calculoIRFundoRendaFixa, prejuizoCompensar, valorIRPosicao, saldoLiquidoPosicao, valorLiquido);
                        valorIR = calculoTributo.IR;

                        if (valorIR == 0)
                        {
                            calculoIRFundoRendaFixa.IdFundo = idCarteira;
                            calculoIRFundoRendaFixa.Quantidade = quantidadeBruta;
                            calculoIRFundoRendaFixa.PrejuizoCompensar = prejuizoCompensar;
                            calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                            calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                            calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                            calculoIRFundoRendaFixa.DataConversao = dataConversao;
                            calculoIRFundoRendaFixa.DataCalculo = data;
                            calculoIRFundoRendaFixa.IdPosicao = idPosicaoResgatada;
                            calculoIRFundoRendaFixa.DataUltimaCobrancaIR = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR);
                            calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                            calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                            calculoIRFundoRendaFixa.IsentoIOF = isentoIOF;
                            calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;
                            calculoIRFundoRendaFixa.QuantidadeAntesCortes = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeAntesCortes);
                            calculoIRFundoRendaFixa.QuantidadePosicao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade);
                            calculoIRFundoRendaFixa.DataLiquidacao = dataLiquidacao;
                            calculoIRFundoRendaFixa.DataLiquidacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataLiquidacao);
                            calculoIRFundoRendaFixa.DataConversaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataConversao);
                            calculoIRFundoRendaFixa.DataOperacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataOperacao);

                            DebugCalculaIRFundoRendaFixa debugCalculaIRFundoRendaFixa = new DebugCalculaIRFundoRendaFixa();
                            calculoTributo.CalculaIRFundoRendaFixa(calculoIRFundoRendaFixa, true,
                                out debugCalculaIRFundoRendaFixa);
                            rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                            rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                        }
                        else
                        {
                            rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                            rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                        }

                        prejuizoUsado = calculoTributo.PrejuizoUsado;

                        #region Trata compensação de prejuízo
                        if (operacao is OperacaoCotista)
                        {
                            if (!projecao)
                            {
                                PrejuizoCotista prejuizoCotista = new PrejuizoCotista();

                                if (rendimento < 0)
                                {
                                    prejuizoCotista.AcumulaPrejuizo(id.Value, idCarteira.Value, data, rendimento);
                                }

                                prejuizoCotista.CompensaPrejuizo(data, idCarteira.Value, id.Value, prejuizoUsado, prejuizoCotistaCollection);
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoFundo.TrataCompensacaoPrejuizo(data, idAgenteAdministrador, tipoTributacao, idCarteira.Value, id.Value, rendimento, prejuizoUsado);
                        }
                        #endregion
                    }
                }

                if (!isentoIOF)
                {
                    CalculoIOF calculoIOF = new CalculoIOF();
                    calculoIOF.IdFundo = idCarteira;
                    calculoIOF.Quantidade = quantidadeBruta;
                    calculoIOF.CotaAplicacao = retornaCotaCalculoIOF(carteira, data, idPosicaoResgatada, cotaAplicacao);
                    calculoIOF.CotaCalculo = cotaDia;
                    calculoIOF.DataAplicacao = dataAplicacao;
                    calculoIOF.DataCalculo = data;
                    calculoIOF.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoTributo.CalculaIOFFundo(calculoIOF);
                    valorIOF = calculoTributo.IOF;
                }
                #endregion

                decimal valorPerformance = 0;
                if (saldoLiquidoPosicao > 0)
                {
                    valorPerformance = Math.Round(valorLiquido * valorPerformancePosicao / saldoLiquidoPosicao, 2);
                }

                #region Calcula valor bruto/qtde da operação e adiciona as variáveis gerais do resgate!
                decimal valorBruto = valorLiquido + valorIR + valorIOF + valorPerformance;
                decimal quantidadeBaixa;

                if (truncaQuantidade)
                {
                    quantidadeBaixa = Utilitario.Truncate(valorBruto / cotaDia, casasDecimaisQuantidade);
                }
                else
                {
                    quantidadeBaixa = Math.Round(valorBruto / cotaDia, casasDecimaisQuantidade);
                }

                //Se o saldo líquido restante (a resgatar) é 100% abatido pelo saldo líquido em posição, resgata tudo
                if ((saldoLiquidoRestante >= saldoLiquidoPosicao))
                {
                    quantidadeBaixa = quantidadePosicao;
                }
                //Se tem saldoLiquidoPosicao = 0, é pq existe qtde mínima residual (q deve ser resgatada)
                else if (saldoLiquidoPosicao == 0)
                {
                    quantidadeBaixa = quantidadePosicao;
                }
                //Garante só resgatar o máximo da posição
                else if (quantidadeBaixa > quantidadePosicao)
                {
                    quantidadeBaixa = quantidadePosicao;
                }

                decimal variacao = Math.Round(quantidadeBaixa * (cotaDia - cotaAplicacao), 2);

                quantidadeResgate += quantidadeBaixa;
                valorBrutoResgate += valorBruto;
                valorLiquidoResgate += valorLiquido;
                valorIRResgate += valorIR;
                valorIOFResgate += valorIOF;
                variacaoResgate += variacao;
                rendimentoCompensadoResgate += rendimentoCompensado;
                prejuizoUsadoResgate += prejuizoUsado;
                valorPerformanceResgate += valorPerformance;
                #endregion

                if (saldoLiquidoRestante > valorLiquido)
                {
                    saldoLiquidoRestante -= valorLiquido;
                }
                else
                {
                    posicaoSuficiente = true;
                }

                //Baixa a posição em quantidade
                if (operacao is OperacaoCotista)
                {
                    if (!projecaoCalculo[0])
                    {
                        PosicaoCotista p = (PosicaoCotista)posicaoCollection[i];
                        //
                        p.Quantidade -= quantidadeBaixa;
                        p.QuantidadeAntesCortes -= quantidadeBruta;

                        if (p.Quantidade == 0)
                        {
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;
                            i++;
                        }
                        else
                        {
                            p.ValorPerformance -= valorPerformance;

                            //Checa eventuais resíduos na qtde por ter compensado prejuizo. 
                            //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                            decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                            if (prejuizoUsado == 0 || posicaoSuficiente)
                            {
                                i++;
                            }
                            else if (valorPosicaoResidual < 0.01M)
                            {
                                p.Quantidade = 0;
                                p.ValorBruto = 0;
                                p.ValorLiquido = 0;
                                p.ValorIR = 0;
                                p.ValorIOF = 0;
                                p.ValorPerformance = 0;
                                p.ValorIOFVirtual = 0;
                                i++;
                            }
                        }
                    }
                }
                else
                {
                    PosicaoFundo p = (PosicaoFundo)posicaoCollection[i];
                    p.Quantidade -= quantidadeBaixa;
                    p.QuantidadeAntesCortes -= quantidadeBruta;

                    if (p.Quantidade == 0)
                    {
                        p.ValorBruto = 0;
                        p.ValorLiquido = 0;
                        p.ValorIR = 0;
                        p.ValorIOF = 0;
                        p.ValorPerformance = 0;
                        p.ValorIOFVirtual = 0;
                        i++;
                    }
                    else
                    {
                        //Checa eventuais resíduos na qtde por ter compensado prejuizo. 
                        //Se tiver, ou calcula novamente na mesma posição, ou zera forçada a posição.
                        decimal valorPosicaoResidual = Math.Round(p.Quantidade.Value * cotaDia);
                        if (prejuizoUsado == 0 || posicaoSuficiente)
                        {
                            i++;
                        }
                        else if (valorPosicaoResidual < 0.01M)
                        {
                            p.Quantidade = 0;
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;
                            i++;
                        }
                    }

                }

                if (operacao is OperacaoCotista)
                {
                    #region Insert em DetalheResgateCotista
                    if (operacao is OperacaoCotista)
                    {
                        DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                        try
                        {
                            detalheResgateCotista.IdOperacao = idOperacao;
                            detalheResgateCotista.IdPosicaoResgatada = idPosicaoResgatada;
                            detalheResgateCotista.IdCotista = id.Value;
                            detalheResgateCotista.IdCarteira = idCarteira;
                            detalheResgateCotista.Quantidade = quantidadeBaixa;
                            detalheResgateCotista.ValorBruto = valorBruto;
                            detalheResgateCotista.ValorLiquido = valorLiquido;
                            detalheResgateCotista.PrejuizoUsado = prejuizoUsado;
                            detalheResgateCotista.RendimentoResgate = rendimentoCompensado;
                            detalheResgateCotista.VariacaoResgate = variacao;
                            detalheResgateCotista.ValorIR = valorIR;
                            detalheResgateCotista.ValorIOF = valorIOF;
                            detalheResgateCotista.Save();
                        }
                        catch (Exception e)
                        {
                            detalheResgateCotista = new DetalheResgateCotista();
                            if (detalheResgateCotista.LoadByPrimaryKey(idOperacao, idPosicaoResgatada))
                            {
                                detalheResgateCotista.Quantidade += quantidadeBaixa;
                                detalheResgateCotista.ValorBruto += valorBruto;
                                detalheResgateCotista.ValorLiquido += valorLiquido;
                                detalheResgateCotista.PrejuizoUsado += prejuizoUsado;
                                detalheResgateCotista.RendimentoResgate += rendimentoCompensado;
                                detalheResgateCotista.VariacaoResgate += variacao;
                                detalheResgateCotista.ValorIR += valorIR;
                                detalheResgateCotista.ValorIOF += valorIOF;
                                detalheResgateCotista.Save();
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Insert em DetalheResgateFundo
                    DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                    try
                    {
                        detalheResgateFundo.IdOperacao = idOperacao;
                        detalheResgateFundo.IdPosicaoResgatada = idPosicaoResgatada;
                        detalheResgateFundo.IdCliente = id.Value;
                        detalheResgateFundo.IdCarteira = idCarteira;
                        detalheResgateFundo.Quantidade = quantidadeBaixa;
                        detalheResgateFundo.ValorBruto = valorBruto;
                        detalheResgateFundo.ValorLiquido = valorLiquido;
                        detalheResgateFundo.PrejuizoUsado = prejuizoUsado;
                        detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                        detalheResgateFundo.VariacaoResgate = variacao;
                        detalheResgateFundo.Save();
                    }
                    catch (Exception e)
                    {
                        detalheResgateFundo = new DetalheResgateFundo();
                        if (detalheResgateFundo.LoadByPrimaryKey(idOperacao, idPosicaoResgatada))
                        {
                            detalheResgateFundo.Quantidade += quantidadeBaixa;
                            detalheResgateFundo.ValorBruto += valorBruto;
                            detalheResgateFundo.ValorLiquido += valorLiquido;
                            detalheResgateFundo.PrejuizoUsado += prejuizoUsado;
                            detalheResgateFundo.RendimentoResgate += rendimentoCompensado;
                            detalheResgateFundo.VariacaoResgate += variacao;
                            detalheResgateFundo.Save();
                        }
                    }

                    #endregion
                }

                if (posicaoSuficiente)
                {
                    break;
                }
            }
            #endregion

            //Se valor/qtde solicitada do resgate for acima do q tem em posição, usa os valores computados no loop
            //Caso contrário, usa os valores da própria operação
            if (!posicaoSuficiente)
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoResgate);
            }
            else
            {
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorLiquidoOperacao + valorIRResgate + valorIOFResgate + valorPerformanceResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoOperacao);
            }

            #region Atualiza OperacaoCotista/OperacaoFundo (cota, tributos, valor bruto/liquido, qtde) -> Save() no escopo superior
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade, quantidadeResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, cotaDia);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIR, valorIRResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIOF, valorIOFResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, prejuizoUsadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, rendimentoCompensadoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, variacaoResgate);
            operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, valorPerformanceResgate);
            #endregion

            //Atualiza qtde de cotas das posições baixadas
            posicaoCollection.Save();

        }

        /// <summary>
        /// Verifica cota que deverá ser utilizada devido a troca de classificacacao tributária de RV para outro tipo
        /// </summary>
        /// <param name="carteira"></param>
        /// <param name="data"></param>
        /// <param name="idPosicao"></param>
        /// <param name="cotaAplicacao"></param>
        /// <returns></returns>
        protected decimal retornaCotaCalculoIOF(Carteira carteira, DateTime data, int idPosicao, decimal cotaAplicacao)
        {
            decimal valorCota = cotaAplicacao;

            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(data),
                                                  agendaComeCotasCollection.Query.TipoEvento.Equal(FonteOperacaoCotista.MudancaClassificacao));
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;

            if (agendaComeCotasCollection.Query.Load() && agendaComeCotasCollection.Count > 0)
            {
                DateTime dataPesquisa = agendaComeCotasCollection[0].DataLancamento.Value;

                int tipoTributacao = Carteira.RetornaTipoTributacao(carteira.IdCarteira.Value, dataPesquisa);
                if (tipoTributacao == (int)ClassificacaoTributaria.RendaVariavels)
                {
                    HistoricoCota historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(carteira.IdCarteira.Value, dataPesquisa);
                    if (carteira.TipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        valorCota = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        valorCota = historicoCota.CotaFechamento.Value;
                    }
                }

            }

            return valorCota;
        }

        /// <summary>
        /// Processa todos os resgate (Ações e Renda Fixa), exceto resgate líquido.
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// </param>
        /// <param name="data"></param>
        /// <param name="carteira"></param>
        /// <param name="operacao">Operacao Cotista ou Operacao Fundo</param>
        /// <param name="posicaoCollection">PosicaoCotista Ou PosicaoFundo</param>
        /// <param name="cotaDia"></param>
        /// <param name="projecaoCalculo">
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="ArgumentException">
        /// Se operacao nao for do tipo OperacaoCotista ou OperacaoFundo
        /// Se posicaoCollection nao for do tipo PosicaoCotistaCollection Ou PosicaoFundoCollection
        /// </exception>
        private void ProcessaResgateCotas(int idCarteiraCliente, DateTime data, Carteira carteira,
                                          esEntity operacao, esEntityCollection posicaoCollection,
                                          decimal cotaDia, bool calculaPfee, bool debug, out DebugProcessaResgateCotas debugProcessaResgateCotas,
                                          params bool[] projecaoCalculo)
        {
            debugProcessaResgateCotas = new DebugProcessaResgateCotas();

            bool pegaCotaAplicacaoPosicao = ParametrosConfiguracaoSistema.Fundo.UsaCotaInicialPosicaoIR == "S";
            byte RESGATE_TOTAL = operacao is OperacaoCotista ? (byte)TipoOperacaoCotista.ResgateTotal : (byte)TipoOperacaoFundo.ResgateTotal;
            //

            #region Trata Exception
            if ((operacao.GetType() != typeof(OperacaoCotista) &&
                   operacao.GetType() != typeof(OperacaoFundo)) ||

                   (posicaoCollection.GetType() != typeof(PosicaoCotistaCollection) &&
                    posicaoCollection.GetType() != typeof(PosicaoFundoCollection))
               )
            {

                throw new ArgumentException("Parâmetro Incorreto: Tipo de Objeto Inválido");
            }
            #endregion

            #region Carrega os parâmetros da carteira, para cálculo dos tributos
            int tipoTributacao = Carteira.RetornaTipoTributacao(carteira.IdCarteira.Value, data);
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            bool calculaIOF = carteira.IsCalculaIOF();
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            #region Carrega as variáveis principais (da operação)
            int? idCarteira = null;
            int? id = null;
            TipoProcessaResgate? tipoProcessaResgate = null;
            //          
            int idOperacao = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao);
            DateTime dataOperacao = (DateTime)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataOperacao);
            int tipoOperacao = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoOperacao));

            int? tipoResgate = null;
            if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate)))
            {
                tipoResgate = Convert.ToInt16(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate));
            }
            //
            if (operacao is OperacaoCotista)
            {
                idCarteira = idCarteiraCliente;
                id = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCotista);
                tipoProcessaResgate = TipoProcessaResgate.OperacaoCotista;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.CotaInformada));
                }
            }
            else if (operacao is OperacaoFundo)
            {
                idCarteira = (int)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdCarteira);
                id = idCarteiraCliente;
                tipoProcessaResgate = TipoProcessaResgate.OperacaoFundo;

                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada)))
                {
                    cotaDia = Convert.ToDecimal(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.CotaInformada));
                }
            }

            #endregion

            #region Busca isenção de cotista em IR e IOF
            Cotista cotista = null;
            Cliente cliente = null;
            Pessoa pessoa = new Pessoa();
            //
            if (operacao is OperacaoCotista)
            {
                cotista = new Cotista();
                cotista.BuscaTributacaoCotista(id.Value);

                pessoa = cotista.UpToPessoaByIdPessoa;

                cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteiraCliente);

                //Para os casos do cotista fora do controle de fundos e clubes, não há calculo de tributos
                if (cliente.IdTipo.Value != TipoClienteFixo.Clube &&
                    cliente.IdTipo.Value != TipoClienteFixo.Fundo &&
                    cliente.IdTipo.Value != TipoClienteFixo.FDIC)
                {
                    cotista.IsentoIR = "S";
                    cotista.IsentoIOF = "S";
                }
            }
            else
            {
                cliente = new Cliente();
                cliente.LoadByPrimaryKey(id.Value);

                pessoa = cliente.UpToPessoaByIdPessoa;
            }
            #endregion

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoResgate(idCarteira.Value, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);
            int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira.Value, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Calcula a quantidade da operação
            /*
            int RESGATE_BRUTO = operacao is OperacaoCotista ? (int)TipoOperacaoCotista.ResgateBruto : (int)TipoOperacaoFundo.ResgateBruto;
            int RESGATE_COTAS = operacao is OperacaoCotista ? (int)TipoOperacaoCotista.ResgateCotas : (int)TipoOperacaoFundo.ResgateCotas;
            int RESGATE_COTAS_ESPECIAL = operacao is OperacaoCotista ? (int)TipoOperacaoCotista.ResgateCotasEspecial : (int)TipoOperacaoFundo.ResgateCotasEspecial;
            int RESGATE_TOTAL = operacao is OperacaoCotista ? (int)TipoOperacaoCotista.ResgateTotal : (int)TipoOperacaoFundo.ResgateTotal;
             */

            decimal quantidadeOperacao = 0;
            decimal valorBrutoOperacao = 0;

            if (operacao is OperacaoCotista)
            {
                switch (tipoOperacao)
                {
                    case (int)TipoOperacaoCotista.ResgateBruto:
                        valorBrutoOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto);

                        quantidadeOperacao = truncaQuantidade
                                             ? Utilitario.Truncate(valorBrutoOperacao / cotaDia, casasDecimaisQuantidade)
                                             : Math.Round(valorBrutoOperacao / cotaDia, casasDecimaisQuantidade);
                        break;
                    case (int)TipoOperacaoCotista.ResgateCotas:
                    case (int)TipoOperacaoCotista.ResgateCotasEspecial:
                    case (int)TipoOperacaoCotista.ResgateTotal:
                        quantidadeOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade);
                        valorBrutoOperacao = truncaFinanceiro
                                             ? Utilitario.Truncate(quantidadeOperacao * cotaDia, 2)
                                             : Math.Round(quantidadeOperacao * cotaDia, 2);
                        break;
                }
            }
            else
            {
                switch (tipoOperacao)
                {
                    case (int)TipoOperacaoFundo.ResgateBruto:
                        valorBrutoOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto);

                        quantidadeOperacao = truncaQuantidade
                                             ? Utilitario.Truncate(valorBrutoOperacao / cotaDia, casasDecimaisQuantidade)
                                             : Math.Round(valorBrutoOperacao / cotaDia, casasDecimaisQuantidade);
                        break;
                    case (int)TipoOperacaoFundo.ResgateCotas:
                    case (int)TipoOperacaoFundo.ResgateCotasEspecial:
                    case (int)TipoOperacaoFundo.ResgateTotal:
                    case (int)TipoOperacaoFundo.RetiradaAtivoImpactoCota:
                    case (int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde:
                        quantidadeOperacao = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade);
                        valorBrutoOperacao = truncaFinanceiro
                                             ? Utilitario.Truncate(quantidadeOperacao * cotaDia, 2)
                                             : Math.Round(quantidadeOperacao * cotaDia, 2);
                        break;
                }
            }
            #endregion

            //A ser computado dentro do loop
            decimal quantidadeResgate = 0;
            decimal valorBrutoResgate = 0;
            decimal valorLiquidoResgate = 0;
            decimal valorIRResgate = 0;
            decimal valorIOFResgate = 0;
            decimal variacaoResgate = 0;
            decimal rendimentoCompensadoResgate = 0;
            decimal prejuizoUsadoResgate = 0;
            decimal valorPerformanceResgate = 0;

            decimal quantidadeRestante = quantidadeOperacao;

            bool posicaoSuficiente = false;
            //
            CalculoTributo calculoTributo = new CalculoTributo();

            #region Loop de posições sendo resgatadas
            for (int i = 0; i < posicaoCollection.Count; i++)
            {
                DebugProcessaResgateCotasPosicaoResgatada debugPosicaoResgatada = new DebugProcessaResgateCotasPosicaoResgatada();

                #region Dados de posicao, cotaAplicacao
                int idPosicaoResgatada = (int)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao);

                decimal quantidadePosicao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade) -
                                            (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeBloqueada);
                decimal quantidadeAntesCortes = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.QuantidadeAntesCortes);
                DateTime dataAplicacao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao);
                DateTime dataConversao = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao);
                DateTime dataLiquidacao = this.RetornaDataLiquidacao((esEntity)posicaoCollection[i]);

                DateTime? dataUltimoCortePfee = null;
                if (!Convert.IsDBNull((((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee))))
                {
                    dataUltimoCortePfee = Convert.ToDateTime(((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee));
                }
                //                                                           
                decimal cotaAplicacao = 0;
                //

                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = operacao is OperacaoCotista
                                    ? ((PosicaoCotista)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, id.Value)
                                    : ((PosicaoFundo)posicaoCollection[i]).RetornaCotaMedia(idCarteira.Value, id.Value);
                }
                else
                {
                    if (!pegaCotaAplicacaoPosicao)
                    {
                        bool posicaoIncorporada = (string)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.PosicaoIncorporada) == "S";

                        HistoricoCota historicoCotaAplicacao = new HistoricoCota();

                        if (posicaoIncorporada)
                        {
                            //TODO: Alterar buscaValorCotaDia para contemplar nova tabela de incorporacao
                            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.LessThan(data));
                            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraDestino.Equal(idCarteira));
                            eventoFundoCollection.Query.Load();
                            if (eventoFundoCollection.Count > 0)
                            {
                                historicoCotaAplicacao.CotaFechamento = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                                historicoCotaAplicacao.CotaAbertura = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                            }
                            else
                            {
                                historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao, data); //Trata posição incorporada
                            }
                        }
                        else
                        {
                            historicoCotaAplicacao.BuscaValorCotaDia(idCarteira.Value, dataConversao);
                        }

                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                        }
                    }
                    else
                    {
                        cotaAplicacao = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
                    }
                }
                #endregion

                //bool aliquotaDiferenciada = true;
                #region Checa aliquota diferenciada
                //Avalia se deve usar a quantidade antes de cortes ou a quantidade real da posição!
                //int diasTotal = Calendario.NumeroDias(dataConversao, data);
                //if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo && diasTotal > 180 ||
                //    tipoTributacao == (int)TipoTributacaoFundo.LongoPrazo && diasTotal > 720)
                //{
                //    aliquotaDiferenciada = false;
                //}
                //bool aliquotaDiferenciada = false; //calculoTributo.AliquotaDiferenciada(dataConversao, data, idCarteira.Value, idPosicaoResgatada, false);

                #endregion

                decimal quantidadeBruta;
                decimal quantidadeBaixa;

                decimal quantidadeColada = 0;
                #region Verifica quantidade colada
                if (operacao is OperacaoCotista)
                {
                    ColagemOperResgPosCotista colaOperRegPosAfetada = new ColagemOperResgPosCotista();
                    colaOperRegPosAfetada.Query.Select(colaOperRegPosAfetada.Query.QuantidadeVinculada.Sum());
                    colaOperRegPosAfetada.Query.Where(colaOperRegPosAfetada.Query.IdPosicaoVinculada.Equal(idPosicaoResgatada)
                                                      & colaOperRegPosAfetada.Query.DataReferencia.Equal(data)
                                                      & colaOperRegPosAfetada.Query.IdOperacaoResgateVinculada.Equal(idOperacao));

                    if (colaOperRegPosAfetada.Query.Load())
                        quantidadeColada = colaOperRegPosAfetada.QuantidadeVinculada.GetValueOrDefault(0);
                }
                else if (operacao is OperacaoFundo)
                {
                    ColagemOperResgPosFundo colaOperRegPosAfetada = new ColagemOperResgPosFundo();
                    colaOperRegPosAfetada.Query.Select(colaOperRegPosAfetada.Query.QuantidadeVinculada.Sum());
                    colaOperRegPosAfetada.Query.Where(colaOperRegPosAfetada.Query.IdPosicaoVinculada.Equal(idPosicaoResgatada)
                                                      & colaOperRegPosAfetada.Query.DataReferencia.Equal(data)
                                                      & colaOperRegPosAfetada.Query.IdOperacaoResgateVinculada.Equal(idOperacao));

                    if (colaOperRegPosAfetada.Query.Load())
                        quantidadeColada = colaOperRegPosAfetada.QuantidadeVinculada.GetValueOrDefault(0);
                }
                #endregion

                quantidadeBruta = truncaQuantidade
                                  ? Utilitario.Truncate(quantidadeRestante * quantidadeAntesCortes / quantidadePosicao, casasDecimaisQuantidade)
                                  : Math.Round(quantidadeRestante * quantidadeAntesCortes / quantidadePosicao, casasDecimaisQuantidade);

                if (quantidadeColada != 0)
                {
                    quantidadeBaixa = quantidadeColada;
                }
                else
                {
                    if ((quantidadeRestante > quantidadePosicao) || tipoOperacao == RESGATE_TOTAL)
                    {
                        //quantidadeBruta = quantidadePosicao;
                        quantidadeBaixa = quantidadePosicao;
                    }
                    else
                    {
                        quantidadeBaixa = quantidadeRestante;
                    }
                }

                #region Calcula rendimentos e IR/IOF (leva em conta eventual prejuízo a compensar)

                decimal prejuizoCompensar = 0;
                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection(); //Usado apenas no caso de ser uma chamada da parte do Cotista
                //
                bool isentoIR = operacao is OperacaoCotista ? cotista.IsIsentoIR() : cliente.IsIsentoIR();
                bool isentoIOF = operacao is OperacaoCotista ? cotista.IsIsentoIOF() : cliente.IsIsentoIOF();

                bool tributaNaoResidente = operacao is OperacaoCotista ? cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente :
                                                                         cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;
                //

                decimal rendimento = 0, rendimentoCompensado = 0, prejuizoUsado = 0,
                        valorIR = 0, valorIOF = 0;

                //if (!isentoIOF && calculaIOF)
                //{
                //    CalculoIOF calculoIOF = new CalculoIOF();
                //    calculoIOF.IdFundo = idCarteira;
                //    calculoIOF.Quantidade = quantidadeBaixa;
                //    calculoIOF.CotaAplicacao = cotaAplicacao;
                //    calculoIOF.CotaCalculo = cotaDia;
                //    calculoIOF.DataAplicacao = dataAplicacao;
                //    calculoIOF.DataCalculo = data;
                //    calculoIOF.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                //    calculoTributo.CalculaIOFFundo(calculoIOF);
                //    valorIOF = calculoTributo.IOF;
                //}

                if (!isentoIR && tipoTributacao != (int)TipoTributacaoFundo.Isento)
                {
                    bool projecao = operacao is OperacaoCotista ? projecaoCalculo[0] : false;
                    //
                    if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                        {
                            if (!projecao)
                            {
                                #region Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                                prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id.Value);

                                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                                {
                                    decimal valorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value;

                                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsado.RetornaPrejuizoUsado(id.Value, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                                    valorPrejuizo -= prejuizoUsadoHistorico;

                                    if (valorPrejuizo < 0)
                                    {
                                        valorPrejuizo = 0;
                                    }

                                    prejuizoCompensar += valorPrejuizo;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoCompensar = prejuizoFundo.RetornaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, id.Value);
                        }
                    }

                    #region Não Isento
                    ListaTipoInvestidor tipoInvestidor;
                    if (pessoa.Tipo.HasValue)
                    {
                        if (pessoa.Tipo == 1)
                            tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                        else
                            tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
                    }
                    else
                    {
                        tipoInvestidor = ListaTipoInvestidor.OffShore;
                    }

                    if (calculoTributo.calculoExcecaoIR(idCarteira.Value, dataAplicacao, data, quantidadeOperacao, isentoIOF, tipoInvestidor))
                    {
                        valorIOF = calculoTributo.IOF;
                        valorIR = calculoTributo.IR;
                        rendimento = calculoTributo.RendimentoPosterior;
                    }
                    else if (carteira.Fie.Equals("S"))
                    {
                        if (operacao is OperacaoCotista)
                            calculoTributo.calculoIRFieCotista(idPosicaoResgatada,
                                                               cotaDia,
                                                               quantidadePosicao,
                                                               isentoIOF,
                                                               diasLiquidacaoResgate,
                                                               data);
                        else
                            calculoTributo.calculoIRFieFundo(idPosicaoResgatada,
                                                             cotaDia,
                                                             quantidadePosicao,
                                                             isentoIOF,
                                                             diasLiquidacaoResgate,
                                                             data);
                        valorIR = calculoTributo.IR;
                        valorIOF = calculoTributo.IOF;
                    }
                    else
                    {

                        if (tipoTributacao == (int)TipoTributacaoFundo.AliquotaEspecifica)
                        {
                            calculoTributo.calculoAliquotaEspecifica(idCarteira.Value, dataAplicacao, data, quantidadeOperacao, isentoIOF);
                            valorIOF = calculoTributo.IOF;
                            valorIR = calculoTributo.IR;
                            rendimento = calculoTributo.RendimentoPosterior;
                        }
                        else if (tipoTributacao != (int)TipoTributacaoFundo.Isento)
                        {

                            CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                            calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                            calculoIRFundoRendaFixa.IdFundo = idCarteira;
                            calculoIRFundoRendaFixa.Quantidade = (tipoOperacao == RESGATE_TOTAL ? quantidadePosicao : quantidadeBruta);
                            calculoIRFundoRendaFixa.QuantidadeAntesCortes = quantidadeAntesCortes;
                            calculoIRFundoRendaFixa.QuantidadePosicao = quantidadePosicao;
                            calculoIRFundoRendaFixa.TipoCota = tipoCota;
                            calculoIRFundoRendaFixa.PrejuizoCompensar = prejuizoCompensar;
                            calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                            calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                            calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                            calculoIRFundoRendaFixa.DataConversao = dataConversao;
                            calculoIRFundoRendaFixa.DataCalculo = data;
                            calculoIRFundoRendaFixa.TipoProcessamentoResgate = tipoProcessaResgate.Value;
                            calculoIRFundoRendaFixa.DataUltimaCobrancaIR = (DateTime)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR);
                            calculoIRFundoRendaFixa.IOFVirtual = (decimal)((esEntity)posicaoCollection[i]).GetColumn(PosicaoCotistaMetadata.ColumnNames.ValorIOFVirtual); //0; //Obsoleto... 
                            calculoIRFundoRendaFixa.IdPosicao = idPosicaoResgatada;
                            calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                            calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                            calculoIRFundoRendaFixa.IsentoIOF = isentoIOF;
                            calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;
                            calculoIRFundoRendaFixa.DataLiquidacao = dataLiquidacao;
                            calculoIRFundoRendaFixa.DataLiquidacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataLiquidacao);
                            calculoIRFundoRendaFixa.DataConversaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataConversao);
                            calculoIRFundoRendaFixa.DataOperacaoResgate = (DateTime)operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataOperacao);

                            DebugCalculaIRFundoRendaFixa debugCalculaIRFundoRendaFixa = new DebugCalculaIRFundoRendaFixa();
                            calculoTributo.CalculaIRFundoRendaFixa(calculoIRFundoRendaFixa, true,
                                out debugCalculaIRFundoRendaFixa);
                            valorIR = calculoTributo.IR;
                            valorIOF = calculoTributo.IOF;
                            rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                            rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                            prejuizoUsado = calculoTributo.PrejuizoUsado;
                        }
                    }


                    #region Trata compensação de prejuízo
                    if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                    {
                        if (operacao is OperacaoCotista) //Precisa ser tratado de forma separada, pois no Cotista os fundos compensáveis podem estar em datas distintas
                        {
                            if (!projecao)
                            {
                                PrejuizoCotista prejuizoCotista = new PrejuizoCotista();

                                if (rendimento < 0)
                                {
                                    prejuizoCotista.AcumulaPrejuizo(id.Value, idCarteira.Value, data, rendimento);
                                }

                                prejuizoCotista.CompensaPrejuizo(data, idCarteira.Value, id.Value, prejuizoUsado, prejuizoCotistaCollection);
                            }
                        }
                        else
                        {
                            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                            prejuizoFundo.TrataCompensacaoPrejuizo(data, idAgenteAdministrador, tipoTributacao, idCarteira.Value, id.Value, rendimento, prejuizoUsado);
                        }
                    }
                    #endregion

                    #endregion
                }
                #endregion

                // Calcula performance (valor e quantidade)
                decimal valorPerformanceEmCota = 0;
                if (calculaPfee)
                {
                    valorPerformanceEmCota = this.RetornaValorPerformanceResgate(idCarteira.Value, 1, data, dataConversao, dataUltimoCortePfee);
                }

                decimal valorPerformance = Math.Round(valorPerformanceEmCota * quantidadeBaixa, 2);

                #region Calcula valores (bruto, líquido) da operação e adiciona as variáveis gerais do resgate!
                quantidadeResgate += quantidadeBaixa;

                decimal valorBruto = truncaFinanceiro
                                     ? Utilitario.Truncate(quantidadeBaixa * cotaDia, 2)
                                     : Math.Round(quantidadeBaixa * cotaDia, 2);

                decimal valorLiquido = valorBruto - valorIR - valorIOF - valorPerformance;

                decimal variacao = Math.Round(quantidadeBaixa * (cotaDia - cotaAplicacao), 2);

                valorBrutoResgate += valorBruto;
                valorLiquidoResgate += valorLiquido;
                valorIRResgate += valorIR;
                valorIOFResgate += valorIOF;
                variacaoResgate += variacao;
                rendimentoCompensadoResgate += rendimentoCompensado;
                prejuizoUsadoResgate += prejuizoUsado;
                valorPerformanceResgate += valorPerformance;
                #endregion

                //Baixa a posição em quantidade
                if (operacao is OperacaoCotista)
                {
                    if (!projecaoCalculo[0])
                    {
                        PosicaoCotista p = (PosicaoCotista)posicaoCollection[i];

                        p.Quantidade -= quantidadeBaixa;
                        p.QuantidadeAntesCortes -= quantidadeBruta;

                        if (p.Quantidade == 0)
                        {
                            p.ValorBruto = 0;
                            p.ValorLiquido = 0;
                            p.ValorIR = 0;
                            p.ValorIOF = 0;
                            p.ValorPerformance = 0;
                            p.ValorIOFVirtual = 0;
                        }
                        else
                        {
                            p.ValorPerformance -= valorPerformance;
                        }
                    }
                }
                else
                {
                    PosicaoFundo p = (PosicaoFundo)posicaoCollection[i];
                    //
                    p.Quantidade -= quantidadeBaixa;
                    p.QuantidadeAntesCortes -= quantidadeBruta;

                    if (p.Quantidade == 0)
                    {
                        p.ValorBruto = 0;
                        p.ValorLiquido = 0;
                        p.ValorIR = 0;
                        p.ValorIOF = 0;
                        p.ValorPerformance = 0;
                        p.ValorIOFVirtual = 0;
                    }
                }

                if (operacao is OperacaoCotista)
                {
                    #region Insert em DetalheResgateCotista
                    DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();

                    detalheResgateCotista.IdOperacao = idOperacao;
                    detalheResgateCotista.IdPosicaoResgatada = idPosicaoResgatada;
                    detalheResgateCotista.IdCotista = id;
                    detalheResgateCotista.IdCarteira = idCarteira;
                    detalheResgateCotista.Quantidade = quantidadeBaixa;
                    detalheResgateCotista.ValorBruto = valorBruto;
                    detalheResgateCotista.ValorLiquido = valorLiquido;
                    detalheResgateCotista.PrejuizoUsado = prejuizoUsado;
                    detalheResgateCotista.RendimentoResgate = rendimentoCompensado;
                    detalheResgateCotista.VariacaoResgate = variacao;
                    detalheResgateCotista.ValorIR = valorIR;
                    detalheResgateCotista.ValorIOF = valorIOF;
                    if (!debug)
                    {
                        detalheResgateCotista.Save();
                    }
                    #endregion
                }
                else
                {
                    #region Insert em DetalheResgateFundo
                    DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();

                    detalheResgateFundo.IdOperacao = idOperacao;
                    detalheResgateFundo.IdPosicaoResgatada = idPosicaoResgatada;
                    detalheResgateFundo.IdCliente = id;
                    detalheResgateFundo.IdCarteira = idCarteira;
                    detalheResgateFundo.Quantidade = quantidadeBaixa;
                    detalheResgateFundo.ValorBruto = valorBruto;
                    detalheResgateFundo.ValorLiquido = valorLiquido;
                    detalheResgateFundo.PrejuizoUsado = prejuizoUsado;
                    detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                    detalheResgateFundo.VariacaoResgate = variacao;
                    if (!debug)
                    {
                        detalheResgateFundo.Save();
                    }
                    #endregion
                }

                if (tipoOperacao != (byte)TipoOperacaoFundo.ResgateTotal) //Resgate total não precisa perguntar
                {
                    if (quantidadeRestante - quantidadeBaixa > 0 && posicaoCollection.Count - 1 == i) //resgate não foi satisfeito por completo
                    {
                        if (ParametrosConfiguracaoSistema.Outras.ValidaQtdeResgateProcessamento.Equals("S"))
                        {
                            string mensagem = "Qtde indisponível para cotizar o Resgate";
                            if (operacao is OperacaoCotista)
                                mensagem += " (Cotista) ";
                            else
                                mensagem += " (Fundo) ";

                            mensagem += "Id.Operação: " + operacao.GetColumn("IdOperacao");

                            mensagem += "- Ativo: " + carteira.Nome.Trim();

                            throw new Exception(mensagem);
                        }
                    }
                    else if (quantidadeRestante > quantidadeBaixa)
                    {
                        quantidadeRestante -= quantidadeBaixa;
                    }
                    else
                    {
                        posicaoSuficiente = true;
                        break;
                    }
                }

                #region Preenchimento de Infos de Debug
                debugPosicaoResgatada.IdPosicao = idPosicaoResgatada;
                debugProcessaResgateCotas.PosicoesResgatas.Add(debugPosicaoResgatada);

                #endregion
            }
            #endregion

            string valorColado = Convert.ToString(operacao is OperacaoCotista ? operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValoresColados) : operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.ValoresColados));

            //Para operações off shore, Utiliza valor de Taxas, tributos, despesas, lockup e holdback para cálculo de valor líquido
            decimal despesasOffShore = 0;
            if (operacao is OperacaoCotista)
                despesasOffShore = (this.debitosOffShoreValorBruto(carteira, (OperacaoCotista)operacao));


            //Se for processamento de operações coladas, não sobreescreve os campos da operação
            if (valorColado.Equals("N"))
            {
                //Se valor/qtde solicitada do resgate for acima do q tem em posição, usa os valores computados no loop
                //Caso contrário, usa os valores da própria operação
                if (!posicaoSuficiente || tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
                {
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade, quantidadeResgate);
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoResgate);
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorLiquidoResgate - despesasOffShore);
                }
                else
                {
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade, quantidadeOperacao);
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoOperacao);
                    operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorBrutoOperacao - valorIRResgate - valorIOFResgate - despesasOffShore);
                }

                #region Checa valor bruto final da operação em relação ao calculado (p/ não dar divergência no PL final)
                if (operacao is OperacaoCotista)
                {
                    decimal valorBrutoChecagem = 0;

                    decimal quantidade = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.Quantidade);
                    decimal valorBruto = (decimal)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto);

                    valorBrutoChecagem = truncaFinanceiro
                                         ? Utilitario.Truncate(quantidade * cotaDia, 2)
                                         : Math.Round(quantidade * cotaDia, 2);

                    if (valorBrutoChecagem != valorBruto)
                    {
                        //Joga a Diferença no IR, no IOF ou no valor liquido (nessa ordem)
                        if (valorIRResgate != 0 && (valorIRResgate - (valorBruto - valorBrutoChecagem)) > 0)
                        {
                            valorIRResgate -= valorBruto - valorBrutoChecagem;
                        }
                        if (valorIOFResgate != 0 && (valorIOFResgate - (valorBruto - valorBrutoChecagem)) > 0)
                        {
                            valorIOFResgate -= valorBruto - valorBrutoChecagem;
                        }

                        operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorBruto, valorBrutoChecagem);

                        // Precisa ReCalcular o Liquido
                        operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorLiquido, valorBrutoChecagem - valorIRResgate - valorIOFResgate);
                    }
                }
                #endregion

                #region Atualiza OperacaoCotista/OperacaoFundo (cota, tributos, valor bruto/liquido, qtde) -> Save() no escopo superior
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.CotaOperacao, cotaDia);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIR, valorIRResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorIOF, valorIOFResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.PrejuizoUsado, prejuizoUsadoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.RendimentoResgate, rendimentoCompensadoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.VariacaoResgate, variacaoResgate);
                operacao.SetColumn(OperacaoCotistaMetadata.ColumnNames.ValorPerformance, valorPerformanceResgate);
                #endregion
            }
            //Atualiza qtde de cotas das posições baixadas
            if (!debug)
            {
                posicaoCollection.Save();
            }

            //Atualiza data limite para o último dia do ano seguinte, em caso de resgate total
            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                this.AtualizaDataLimite(data, idCarteira.Value, id.Value, tipoProcessaResgate.Value);
            }
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotista com os campos Quantidade, CotaOperacao, ValorIR, ValorIOF.
        /// Filtra por: TipoOperacao.Equal((byte)TipoOperacaoCotista.ComeCotas).
        /// </summary>
        /// <param name="idPosicaoResgatada"></param>
        /// <param name="dataConversao"></param>
        public bool BuscaComeCotasPorNota(int idPosicaoResgatada, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.CotaOperacao, this.Query.ValorIR, this.Query.ValorIOF)
                 .Where(this.Query.IdPosicaoResgatada == idPosicaoResgatada,
                        this.Query.DataConversao.Equal(dataConversao),
                        this.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ComeCotas));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Soma valores para debito nas operações de resgate de offshore
        /// </summary>
        /// <param name="carteira"></param>
        /// <param name="operacaoCotista"></param>
        /// <returns></returns>
        protected decimal debitosOffShoreValorBruto(Carteira carteira, OperacaoCotista operacaoCotista)
        {
            decimal valorDebitos = 0;

            int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
            if (idTipoCliente == TipoClienteFixo.OffShore_PF || idTipoCliente == TipoClienteFixo.OffShore_PJ)
            {
                valorDebitos = operacaoCotista.ValorDespesas.Value +
                               operacaoCotista.ValorTaxas.Value +
                               operacaoCotista.ValorTributos.Value +
                               operacaoCotista.ValorHoldBack.Value +
                               operacaoCotista.PenalidadeLockUp.Value;
            }

            return valorDebitos;
        }

        /// <summary>
        /// Soma quantidade para debito nas operações de resgate de offshore
        /// </summary>
        /// <param name="carteira"></param>
        /// <param name="operacaoCotista"></param>
        /// <returns></returns>
        protected decimal debitosOffShoreQuantidade(Carteira carteira, OperacaoCotista operacaoCotista)
        {
            decimal qtdDebito = 0;

            int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
            if (idTipoCliente == TipoClienteFixo.OffShore_PF || idTipoCliente == TipoClienteFixo.OffShore_PJ)
            {
                qtdDebito += operacaoCotista.ValorHoldBack.Value +
                               operacaoCotista.PenalidadeLockUp.Value;

                qtdDebito += (operacaoCotista.ValorDespesas.Value + operacaoCotista.ValorTaxas.Value + operacaoCotista.ValorTributos.Value) / operacaoCotista.CotaOperacao.Value;

            }

            return qtdDebito;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto de aplicações na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>         
        public decimal RetornaSumValorBrutoAplicacao(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                   (int)TipoOperacaoCotista.AplicacaoAcoesEspecial),
                        this.Query.DataConversao.Equal(dataConversao));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto de resgates (todos os tipos) na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>        
        public decimal RetornaSumValorBrutoResgate(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.ResgateBruto,
                                                   (int)TipoOperacaoCotista.ResgateCotas,
                                                   (int)TipoOperacaoCotista.ResgateLiquido,
                                                   (int)TipoOperacaoCotista.ResgateTotal),
                        this.Query.DataConversao.Equal(dataConversao));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a quantidade total de cotas de operações por resgate especial.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>         
        public decimal RetornaTotalCotasResgateEspecial(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ResgateCotasEspecial),
                        this.Query.DataConversao.Equal(dataConversao));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total de operações por resgate especial.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>         
        public decimal RetornaTotalValorResgateEspecial(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ResgateCotasEspecial),
                        this.Query.DataConversao.Equal(dataConversao));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor comecotas na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>        
        public decimal RetornaSumValorComeCotas(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ComeCotas),
                        this.Query.DataConversao.Equal(data));

            this.Query.Load();

            return this.ValorIR.HasValue ? this.ValorIR.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto de aplicações na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicio">Data de inicio da operação</param>
        /// <param name="dataFim">Data fim da operação</param>        
        /// <returns></returns>        
        public decimal RetornaSumValorBrutoAplicacao(int idCarteira, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                   (int)TipoOperacaoCotista.AplicacaoAcoesEspecial),
                        this.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                        this.Query.DataOperacao.LessThanOrEqual(dataFim));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto de resgates (todos os tipos) na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicio">Data de inicio da operação</param>
        /// <param name="dataFim">Data fim da operação</param>        
        /// <returns></returns>        
        public decimal RetornaSumValorBrutoResgate(int idCarteira, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.ResgateBruto,
                                                   (int)TipoOperacaoCotista.ResgateCotas,
                                                   (int)TipoOperacaoCotista.ResgateLiquido,
                                                   (int)TipoOperacaoCotista.ResgateTotal),
                        this.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                        this.Query.DataOperacao.LessThanOrEqual(dataFim));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto de aplicações na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>        
        public decimal RetornaValorAplicacaoLiquidar(int idCarteira, DateTime dataLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao),
                        this.Query.DataLiquidacao.Equal(dataLiquidacao));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Realiza o processo de conversão do valor aplicado em quantidade de cotas (ou vice-versa)
        /// para as aplicações que estiverem cotizando na data.        
        /// Atualiza a OperacaoCotista e cria uma nova PosicaoCotista.        
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaAplicacao(int idCarteira, DateTime data)
        {
            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            List<esQueryItem> fieldsToReturn = new List<esQueryItem>();
            fieldsToReturn.Add(carteira.Query.IdCarteira);
            fieldsToReturn.Add(carteira.Query.TipoCota);
            fieldsToReturn.Add(carteira.Query.TruncaQuantidade);
            fieldsToReturn.Add(carteira.Query.TruncaFinanceiro);
            fieldsToReturn.Add(carteira.Query.CasasDecimaisQuantidade);
            fieldsToReturn.Add(carteira.Query.CotaInicial);
            fieldsToReturn.Add(carteira.Query.TipoTributacao);
            fieldsToReturn.Add(carteira.Query.DataInicioCota);

            carteira.LoadByPrimaryKey(fieldsToReturn, idCarteira);

            int tipoCota = carteira.TipoCota.Value;
            string truncaQuantidade = carteira.TruncaQuantidade;
            string truncaFinanceiro = carteira.TruncaFinanceiro;
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            byte tipoTributacao = carteira.TipoTributacao.Value;
            #endregion

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();

            operacaoCotistaCollection.BuscaAplicacao(idCarteira, data);

            #region Busca o valor da cota na data (abertura ou fechamento), ou cota informada
            decimal cotaAplicacao;
            int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
            if (idTipoCliente == TipoClienteFixo.OffShore_PF || idTipoCliente == TipoClienteFixo.OffShore_PJ)
                cotaAplicacao = 1;
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, data))
                {
                    cotaAplicacao = carteira.CotaInicial.Value;
                }
                else
                {
                    if (tipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        cotaAplicacao = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        if (!historicoCota.CotaFechamento.HasValue)
                        {
                            cotaAplicacao = carteira.CotaInicial.Value;
                        }
                        else
                        {
                            cotaAplicacao = historicoCota.CotaFechamento.Value;
                        }
                    }
                }
            }
            #endregion

            for (int i = 0; i < operacaoCotistaCollection.Count; i++)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection[i];

                if (operacaoCotista.CotaInformada.HasValue)
                {
                    cotaAplicacao = operacaoCotista.CotaInformada.Value;
                }

                int tipoOperacao = operacaoCotista.TipoOperacao.Value;

                #region Converte financeiro em quantidade ou quantidade em financeiro
                decimal valorBruto;
                decimal quantidade;
                if (tipoOperacao == (int)TipoOperacaoCotista.Aplicacao ||
                    tipoOperacao == (int)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                {
                    valorBruto = operacaoCotista.ValorBruto.Value;

                    if (truncaQuantidade == "S")
                    {
                        quantidade = Utilitario.Truncate(valorBruto / cotaAplicacao, casasDecimaisQuantidade);
                    }
                    else
                    {
                        quantidade = Math.Round(valorBruto / cotaAplicacao, casasDecimaisQuantidade);
                    }
                }
                else
                {
                    quantidade = operacaoCotista.Quantidade.Value;

                    if (truncaFinanceiro == "S")
                    {
                        valorBruto = Utilitario.Truncate(quantidade * cotaAplicacao, 2);
                    }
                    else
                    {
                        valorBruto = Math.Round(quantidade * cotaAplicacao, 2);
                    }
                }
                #endregion

                DateTime dataUltimaCobrancaIR = data;
                if (tipoTributacao == (byte)TipoTributacaoFundo.CurtoPrazo ||
                    tipoTributacao == (byte)TipoTributacaoFundo.LongoPrazo)
                {
                    dataUltimaCobrancaIR = CalculoTributo.RetornaDataComeCotasAnterior(data);
                }

                if (valorBruto + ValorLiquido.GetValueOrDefault(0) + quantidade == 0)
                    continue;

                #region Insert em PosicaoCotista
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                posicaoCotista.AddNew();
                posicaoCotista.IdOperacao = operacaoCotista.IdOperacao;
                posicaoCotista.IdCotista = operacaoCotista.IdCotista;
                posicaoCotista.IdCarteira = operacaoCotista.IdCarteira;
                posicaoCotista.ValorAplicacao = valorBruto;
                posicaoCotista.DataAplicacao = operacaoCotista.DataOperacao;
                posicaoCotista.DataConversao = operacaoCotista.DataConversao;
                posicaoCotista.CotaAplicacao = cotaAplicacao;
                posicaoCotista.CotaDia = cotaAplicacao;
                posicaoCotista.ValorBruto = valorBruto;
                posicaoCotista.ValorLiquido = valorBruto - (operacaoCotista.ValorTaxas.GetValueOrDefault(0) + operacaoCotista.ValorDespesas.GetValueOrDefault(0) + operacaoCotista.ValorTributos.GetValueOrDefault(0));
                posicaoCotista.QuantidadeInicial = quantidade;
                posicaoCotista.Quantidade = quantidade;
                posicaoCotista.QuantidadeAntesCortes = quantidade;
                posicaoCotista.DataUltimaCobrancaIR = operacaoCotista.DataConversao; //Artificio para início de posição                
                posicaoCotista.ValorRendimento = 0;
                posicaoCotista.PosicaoIncorporada = "N";
                posicaoCotista.IdSeriesOffShore = operacaoCotista.IdSeriesOffShore;
                posicaoCotista.FieTabelaIr = operacaoCotista.FieTabelaIr;
                posicaoCotistaCollection.AttachEntity(posicaoCotista);
                #endregion

                operacaoCotista.Quantidade = quantidade;
                operacaoCotista.ValorBruto = valorBruto;
                operacaoCotista.ValorLiquido = valorBruto - (operacaoCotista.ValorTaxas.GetValueOrDefault(0) + operacaoCotista.ValorDespesas.GetValueOrDefault(0) + operacaoCotista.ValorTributos.GetValueOrDefault(0));
                operacaoCotista.CotaOperacao = cotaAplicacao;
            }

            //Salva as collections
            operacaoCotistaCollection.Save();
            posicaoCotistaCollection.Save();

            if (operacaoCotistaCollection.Count > 0)
            {
                Cliente cliente = new Cliente();
                fieldsToReturn = new List<esQueryItem>();
                fieldsToReturn.Add(cliente.Query.IdTipo);
                fieldsToReturn.Add(cliente.Query.DataImplantacao);
                fieldsToReturn.Add(cliente.Query.IdCliente);
                cliente.LoadByPrimaryKey(fieldsToReturn, idCarteira);
                if (cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo && cliente.IdTipo.Value != (int)TipoClienteFixo.Clube &&
                    cliente.DataImplantacao.Value != data && ParametrosConfiguracaoSistema.Outras.ResetDataInicio)
                {
                    HistoricoCota historicoCotaQtde = new HistoricoCota();
                    historicoCotaQtde.Query.Select(historicoCotaQtde.Query.QuantidadeFechamento);
                    historicoCotaQtde.Query.Where(historicoCotaQtde.Query.IdCarteira.Equal(idCarteira),
                                              historicoCotaQtde.Query.Data.Equal(data));
                    historicoCotaQtde.Query.Load();

                    if (!historicoCotaQtde.QuantidadeFechamento.HasValue || historicoCotaQtde.QuantidadeFechamento.Value == 0)
                    {
                        carteira.DataInicioCota = data;
                        carteira.Save();
                    }
                }
            }

        }

        /// <summary>
        /// Lança em Liquidacao todas as aplicações consolidadas liquidando no dia.
        /// Lança eventuais contrapartidas (compensação) como "Valores a converter em cotas".
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void LancaCCAplicacao(int idCarteira, DateTime data)
        {
            string nomeClienteConfig = ConfigurationManager.AppSettings["Cliente"];

            if (!String.IsNullOrEmpty(nomeClienteConfig))
            {
                nomeClienteConfig = nomeClienteConfig.ToUpper();
            }

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.IdCliente);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.DiasCotizacaoAplicacao);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            int diasCotizacaoAplicacao = carteira.DiasCotizacaoAplicacao.Value;

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
            {
                OperacaoCotistaCollection operacaoCotistaCollectionDia = new OperacaoCotistaCollection();
                operacaoCotistaCollectionDia.BuscaAplicacaoLiquidaDiaAberto(idCarteira, data);

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollectionDia)
                {
                    decimal valor = operacaoCotista.ValorBruto.Value;
                    DateTime dataLiquidacao = operacaoCotista.DataLiquidacao.Value;

                    #region Insert em Liquidacao
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);

                    if (operacaoCotista.IdConta != null && nomeClienteConfig != "PORTOPAR")
                    {
                        idContaDefault = (int)operacaoCotista.IdConta;
                    }

                    string descricao = "Aplicações do dia";

                    if (!String.IsNullOrEmpty(operacaoCotista.Observacao) && operacaoCotista.Observacao != "")
                    {
                        descricao = descricao + " - " + operacaoCotista.Observacao;
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion
                }
            }
            else
            {
                OperacaoCotistaCollection operacaoCotistaCollectionDia = new OperacaoCotistaCollection();
                operacaoCotistaCollectionDia.BuscaAplicacaoLiquidar(idCarteira, data);

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollectionDia)
                {
                    decimal valor = operacaoCotista.ValorBruto.Value;

                    #region Insert em Liquidacao
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);

                    if (operacaoCotista.IdConta != null && nomeClienteConfig != "PORTOPAR")
                    {
                        idContaDefault = (int)operacaoCotista.IdConta;
                    }


                    #region Insert em Liquidacao
                    string descricao = "Aplicações do dia";

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion
                    #endregion
                }

            }

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            //Forço a contrapartida contábil (valores a converter em cotas)
            //para evitar o impacto na cota (pela entrada do $ da aplicação).
            if (tipoCota == (byte)TipoCotaFundo.Fechamento && diasCotizacaoAplicacao == 0)
            {
                operacaoCotistaCollection.BuscaAplicacaoConverterDia(idCarteira, data);
            }
            else
            {
                operacaoCotistaCollection.BuscaAplicacaoConverter(idCarteira, data);
            }

            for (int i = 0; i < operacaoCotistaCollection.Count; i++)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection[i];
                decimal valor = operacaoCotista.ValorBruto.Value;
                DateTime dataConversao = operacaoCotista.DataConversao.Value;

                #region Insert em Liquidacao (Valores a converter em cotas) - contrapartida contábil
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //

                string descricao = "Valores a converter em cotas";

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCarteira;
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataConversao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valor * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idContaDefault;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion
            }

            //Salva a collection de Liquidacao
            liquidacaoCollection.Save();

        }

        /// <summary>
        /// Lança em Liquidacao o projetado de todos os resgates (valor resgate, IR, IOF, Pfee) com conversão na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>
        public void LancaCCResgate(int idCarteira, DateTime dataConversao)
        {
            string nomeClienteConfig = ConfigurationManager.AppSettings["Cliente"];

            if (!String.IsNullOrEmpty(nomeClienteConfig))
            {
                nomeClienteConfig = nomeClienteConfig.ToUpper();
            }

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.IdCliente);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.ProjecaoIRResgate);
            campos.Add(carteira.Query.DiasAposResgateIR);
            campos.Add(carteira.Query.ProjecaoIOFResgate);
            campos.Add(carteira.Query.DiasAposResgateIOF);
            campos.Add(carteira.Query.TipoVisualizacaoResgCotista);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            bool analitico = false;
            if (!string.IsNullOrEmpty(carteira.TipoVisualizacaoResgCotista))
                analitico = carteira.TipoVisualizacaoResgCotista.Equals("A");

            DateTime dataPagamentoTributoIR = new DateTime();
            CalculoTributo.ProjecaoIRCotista projecaoIRCotista = (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRResgate.Value;
            DateTime dataPagamentoTributoIOF = new DateTime();
            CalculoTributo.ProjecaoIRCotista projecaoIOF = (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIOFResgate.Value;
            if (!analitico)
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.LoadByPrimaryKey(cliente.IdLocalNegociacao.GetValueOrDefault((int)LocalNegociacaoFixo.Brasil));

                int idLocalFeriado = localNegociacao.IdLocalFeriado.Value;

                #region Calcula data de pagamento de IR / IOF
                if (cliente.IsTipoClienteFundo() || cliente.IdTipo.Value == TipoClienteFixo.Clube)
                {
                    CalculoTributo calculoTributo = new CalculoTributo();
                    CalculoTributo.ProjecaoIRCotista projecaoIR = (carteira.ProjecaoIRResgate.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRResgate.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
                    int diasAposResgate = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposResgateIR.Value : 0);

                    dataPagamentoTributoIR = calculoTributo.RetornaDiaPagamentoIR(dataConversao, projecaoIRCotista, diasAposResgate, idLocalFeriado);

                    projecaoIR = (carteira.ProjecaoIOFResgate.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIOFResgate.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
                    diasAposResgate = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposResgateIOF.Value : 0);

                    dataPagamentoTributoIOF = calculoTributo.RetornaDiaPagamentoIR(dataConversao, projecaoIRCotista, diasAposResgate, idLocalFeriado);
                }
                #endregion
            }

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            if (analitico)
            {
                operacaoCotistaCollection.BuscaResgateProjetaLiquidacaoAberto(idCarteira, dataConversao);
            }
            else
            {
                operacaoCotistaCollection.BuscaResgateProjetaLiquidacao(idCarteira, dataConversao);
            }

            List<LocalNegociacao> lstLocalNegociacao = new List<LocalNegociacao>();
            LocalNegociacaoCollection localNegociacaoCollection = new LocalNegociacaoCollection();

            if (localNegociacaoCollection.LoadAll())
                lstLocalNegociacao = (List<LocalNegociacao>)localNegociacaoCollection;

            for (int i = 0; i < operacaoCotistaCollection.Count; i++)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection[i];
                decimal valor = operacaoCotista.ValorLiquido.Value;
                DateTime dataLiquidacao = operacaoCotista.DataLiquidacao.Value;

                if (analitico)
                {
                    int idLocalFeriado = LocalFeriadoFixo.Brasil;
                    #region Busca local de Feriado
                    LocalNegociacao localNegociacao = lstLocalNegociacao.Find(delegate(LocalNegociacao x) { return x.IdLocalNegociacao == operacaoCotista.IdLocalNegociacao.Value; });
                    if (localNegociacao != null && localNegociacao.IdLocalNegociacao > 0)
                        idLocalFeriado = localNegociacao.IdLocalFeriado.Value;
                    #endregion

                    #region Calcula data de pagamento de IR / IOF
                    dataPagamentoTributoIR = new DateTime();
                    projecaoIRCotista = (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRResgate.Value;
                    if (cliente.IsTipoClienteFundo() || cliente.IdTipo.Value == TipoClienteFixo.Clube)
                    {
                        CalculoTributo calculoTributo = new CalculoTributo();
                        CalculoTributo.ProjecaoIRCotista projecaoIR = (carteira.ProjecaoIRResgate.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRResgate.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
                        int diasAposResgate = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposResgateIR.Value : 0);

                        dataPagamentoTributoIR = calculoTributo.RetornaDiaPagamentoIR(dataConversao, projecaoIRCotista, diasAposResgate, idLocalFeriado);

                        projecaoIR = (carteira.ProjecaoIOFResgate.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIOFResgate.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
                        diasAposResgate = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposResgateIOF.Value : 0);

                        dataPagamentoTributoIOF = calculoTributo.RetornaDiaPagamentoIR(dataConversao, projecaoIRCotista, diasAposResgate, idLocalFeriado);
                    }
                    #endregion
                }

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);

                if (operacaoCotista.IdConta != null && nomeClienteConfig != "PORTOPAR")
                {
                    idContaDefault = (int)operacaoCotista.IdConta;
                }

                #region Insert em Liquidacao (Valor Líquido)
                string descricao = "Resgates no dia";

                if (operacaoCotista.IdOperacao.HasValue)
                    descricao += " (Id.Operação - " + operacaoCotista.IdOperacao.Value + ") ";

                if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                    cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
                {
                    if (!String.IsNullOrEmpty(operacaoCotista.Observacao) && operacaoCotista.Observacao != "")
                    {
                        descricao = descricao + " - " + operacaoCotista.Observacao;
                    }
                }

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCarteira;
                liquidacao.DataLancamento = dataConversao;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao + " (Qtde - " + operacaoCotista.Quantidade.Value + ")"; ;
                liquidacao.Valor = valor * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.Resgate;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idContaDefault;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion

                decimal valorIR = operacaoCotista.ValorIR.Value;
                decimal valorIOF = operacaoCotista.ValorIOF.Value;
                decimal valorPerformance = operacaoCotista.ValorPerformance.Value;
                if (cliente.IsTipoClienteFundo() || cliente.IdTipo.Value == TipoClienteFixo.Clube)
                {
                    if (projecaoIRCotista == CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate)
                    {
                        dataPagamentoTributoIR = dataLiquidacao;
                    }

                    #region Insert em Liquidacao (Valor IR)
                    if (valorIR != 0)
                    {
                        descricao = "IR s/ Resgates no dia";

                        if (operacaoCotista.IdOperacao.HasValue)
                            descricao += " (Id.Operação - " + operacaoCotista.IdOperacao.Value + ") ";

                        liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.DataLancamento = dataConversao;
                        liquidacao.DataVencimento = dataPagamentoTributoIR;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorIR * -1;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.IRResgate;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    #endregion

                    #region Insert em Liquidacao (Valor IOF)
                    if (valorIOF != 0)
                    {
                        descricao = "IOF s/ Resgates no dia";

                        if (operacaoCotista.IdOperacao.HasValue)
                            descricao += " (Id.Operação - " + operacaoCotista.IdOperacao.Value + ") ";

                        liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.DataLancamento = dataConversao;
                        liquidacao.DataVencimento = dataPagamentoTributoIOF;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorIOF * -1;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.IOFResgate;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    #endregion

                    #region Insert em Liquidacao (Valor Performance)
                    if (valorPerformance != 0)
                    {
                        descricao = "Performance s/ Resgates no dia";

                        if (operacaoCotista.IdOperacao.HasValue)
                            descricao += " (Id.Operação - " + operacaoCotista.IdOperacao.Value + ") ";

                        liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.DataLancamento = dataConversao;
                        liquidacao.DataVencimento = dataLiquidacao;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorPerformance * -1;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.PfeeResgate;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    #endregion
                }

                #region OffShore
                if (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ))
                {
                    ClassesOffShore classesOffShore = new ClassesOffShore();
                    classesOffShore.LoadByPrimaryKey(idCarteira);

                    //Verifica se ocorreu LockUp
                    if (operacaoCotista.PenalidadeLockUp.HasValue && operacaoCotista.PenalidadeLockUp.Value > 0)
                    {
                        descricao = "LockUp de " + classesOffShore.PenalidadeResgate.Value + "% sobre resgate.";

                        if (operacaoCotista.IdOperacao.HasValue)
                            descricao += " (Id.Operação - " + operacaoCotista.IdOperacao.Value + ") ";

                        liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.DataLancamento = dataConversao;
                        liquidacao.DataVencimento = dataLiquidacao;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = operacaoCotista.PenalidadeLockUp;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cotista.PfeeResgate;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                }
                #endregion

                //Salva a collection de Liquidacao
                liquidacaoCollection.Save();
            }
        }

        /// <summary>
        /// Método principal para processamento de resgates com conversão na data. 
        /// Calcula os tributos (IR, IOF) e eventual pfee sobre os resgates.        
        /// Baixa as posições de cotas de cotistas.        
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// <param name="data"></param>
        /// <param name="tipoProcessaResgate">Indica se a origem chamadora é OperacaoFundo ou OperacaoCotista</param>
        /// <param name="projecaoCalculo">Parametro Opcional
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="HistoricoCotaNaoCadastradoException">
        /// Se operacao for do tipo OperacaoFundo e a Cota não estiver Cadastrada na data Passada   
        /// </exception>
        /// 
        /// <exception cref="ArgumentException">
        /// Se operacao for do tipo OperacaoCotista projecaoCalculo é obrigatorio
        /// </exception>
        public void ProcessaResgate(int idCarteiraCliente, DateTime data, TipoProcessaResgate tipoProcessaResgate, bool debug, out DebugProcessaResgate debugProcessaResgate, params bool[] projecaoCalculo)
        {
            #region Exception
            if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
            {
                if (projecaoCalculo == null)
                {
                    throw new ArgumentException("Parâmetro Incorreto: projecaoCalculo Obrigatório");
                }
            }
            #endregion

            debugProcessaResgate = new DebugProcessaResgate();

            esEntityCollection operacaoCollection;
            if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
            {
                operacaoCollection = new OperacaoCotistaCollection();
            }
            else
            {
                operacaoCollection = new OperacaoFundoCollection();
            }

            int? idCarteira = null, idCliente = null;
            Cliente cliente = new Cliente();
            Carteira carteira = new Carteira();
            HistoricoCota historicoCota = new HistoricoCota();
            decimal? cotaDia = null;
            int? tipoTributacao = null;

            if (!debug)
            {
                if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                {
                    #region Deleta DetalheResgateCotista >= data
                    DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                    OperacaoCotistaQuery opCotistaQuery = new OperacaoCotistaQuery("O");

                    detalheResgateCotistaQuery.InnerJoin(opCotistaQuery).On(opCotistaQuery.IdOperacao.Equal(detalheResgateCotistaQuery.IdOperacao));
                    detalheResgateCotistaQuery.Where(opCotistaQuery.IdCarteira.Equal(idCarteiraCliente) &
                                                    ((opCotistaQuery.DataConversao.Equal(data) & opCotistaQuery.DataConversao.GreaterThanOrEqual(opCotistaQuery.DataRegistro)) | (opCotistaQuery.DataRegistro.Equal(data) & opCotistaQuery.DataConversao.LessThanOrEqual(opCotistaQuery.DataRegistro))));
                    DetalheResgateCotistaCollection detalheResgateCotistaCollectionDeletar = new DetalheResgateCotistaCollection();
                    detalheResgateCotistaCollectionDeletar.Load(detalheResgateCotistaQuery);
                    detalheResgateCotistaCollectionDeletar.MarkAllAsDeleted();
                    detalheResgateCotistaCollectionDeletar.Save();
                    #endregion
                }
                else
                {
                    #region Deleta DetalheResgateFundo >= data
                    DetalheResgateFundoQuery detalheResgateFundoQuery = new DetalheResgateFundoQuery("D");
                    OperacaoFundoQuery opFundoQuery = new OperacaoFundoQuery("O");

                    detalheResgateFundoQuery.InnerJoin(opFundoQuery).On(opFundoQuery.IdOperacao.Equal(detalheResgateFundoQuery.IdOperacao));
                    detalheResgateFundoQuery.Where(opFundoQuery.IdCliente.Equal(idCarteiraCliente) &
                                                   ((opFundoQuery.DataConversao.Equal(data) & opFundoQuery.DataConversao.GreaterThanOrEqual(opFundoQuery.DataRegistro)) | (opFundoQuery.DataRegistro.Equal(data) & opFundoQuery.DataConversao.LessThanOrEqual(opFundoQuery.DataRegistro))) &
                                                   opFundoQuery.Fonte.NotIn((byte)FonteOperacaoFundo.SMA,
                                                                                  (byte)FonteOperacaoFundo.YMFSemCalculo,
                                                                                  (byte)FonteOperacaoFundo.Ajustado));
                    DetalheResgateFundoCollection detalheResgateFundoCollectionDeletar = new DetalheResgateFundoCollection();
                    detalheResgateFundoCollectionDeletar.Load(detalheResgateFundoQuery);
                    detalheResgateFundoCollectionDeletar.MarkAllAsDeleted();
                    detalheResgateFundoCollectionDeletar.Save();
                    #endregion
                }
            }

            if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
            {
                idCarteira = idCarteiraCliente;
                ((OperacaoCotistaCollection)operacaoCollection).BuscaResgate(idCarteira.Value, data);
                //
                #region Busca os parâmetros da carteira, para cálculo dos tributos
                carteira.LoadByPrimaryKey(idCarteira.Value);
                //tipoTributacao = carteira.TipoTributacao.Value;
                tipoTributacao = Carteira.RetornaTipoTributacao(idCarteira.Value, data);
                int tipoCusto = carteira.TipoCusto.Value;
                int tipoCota = carteira.TipoCota.Value;
                bool truncaQuantidade = carteira.IsTruncaQuantidade();
                bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
                int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
                bool calculaIOF = carteira.IsCalculaIOF();
                int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
                #endregion

                #region Busca o valor da cota na data
                if (operacaoCollection.Count > 0)
                {
                    historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(idCarteira.Value, data);

                    cotaDia = tipoCota == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;
                }
                #endregion
            }
            else
            {
                idCliente = idCarteiraCliente;
                List<byte> lista = new List<byte>();
                lista.Add((byte)FonteOperacaoFundo.Manual);
                lista.Add((byte)FonteOperacaoFundo.OrdemFundo);
                lista.Add((byte)FonteOperacaoFundo.YMFComCalculo);
                ((OperacaoFundoCollection)operacaoCollection).BuscaResgate(idCliente.Value, data, lista);

                #region Busca isenção IR/IOF do cliente
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IsentoIR);
                campos.Add(cliente.Query.IsentoIOF);
                campos.Add(cliente.Query.IdCliente);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                #endregion
            }

            for (int i = 0; i < operacaoCollection.Count; i++)
            {
                esEntity operacao = (esEntity)operacaoCollection[i];

                int tipoOperacao = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoOperacao));
                if (tipoOperacao == (int)TipoOperacaoCotista.ResgateCotasEspecial)
                    continue;
                //                                                             
                int? tipoResgate = null;
                if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                {
                    if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate)) &&
                         Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate)) != 0)
                    {
                        tipoResgate = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.TipoResgate));
                    }
                    else
                    {
                        tipoResgate = (int)TipoResgateCotista.FIFO;
                    }
                }
                else
                {
                    if (!Convert.IsDBNull(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.TipoResgate)) &&
                         Convert.ToInt32(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.TipoResgate)) != 0)
                    {
                        tipoResgate = Convert.ToInt32(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.TipoResgate));
                    }
                    else
                    {
                        tipoResgate = (int)TipoResgateFundo.FIFO;
                    }
                }

                int idPosicaoResgatada = 0;
                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada)))
                {
                    idPosicaoResgatada = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdPosicaoResgatada));
                }

                int idOperacaoResgatada = 0;
                if (!Convert.IsDBNull(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada)))
                {
                    idOperacaoResgatada = Convert.ToInt32(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacaoResgatada));
                }

                int? idCotista = null;
                //
                if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                {
                    idCotista = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCotista);
                }
                else
                {
                    idCarteira = (int)operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdCarteira);

                    #region Busca os parâmetros da carteira, para cálculo dos tributos
                    carteira = new Carteira();
                    carteira.LoadByPrimaryKey(idCarteira.Value);
                    tipoTributacao = carteira.TipoTributacao.Value;
                    int tipoCusto = carteira.TipoCusto.Value;
                    int tipoCota = carteira.TipoCota.Value;
                    bool truncaQuantidade = carteira.IsTruncaQuantidade();
                    bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
                    int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
                    bool calculaIOF = carteira.IsCalculaIOF();
                    int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
                    #endregion

                    #region Busca o valor da cota na data
                    historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(idCarteira.Value, data);

                    cotaDia = tipoCota == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;

                    if (cotaDia == 0)
                    {
                        throw new HistoricoCotaNaoCadastradoException("Cota zerada na data " + data + " para a carteira " + idCarteira);
                    }
                    #endregion
                }

                esEntityCollection posicaoCollection = null;
                //
                if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                {
                    #region Carrega PosicaoCotistaCollection, conforme o tipo de resgate (específico, FIFO, LIFO, Menor Imp.)
                    posicaoCollection = new PosicaoCotistaCollection();
                    switch (tipoResgate)
                    {
                        case (byte)TipoResgateCotista.Especifico:

                            DateTime dataAplicCautelaResgatada = new DateTime();
                            DateTime dataConvCautelaResgatada = new DateTime();
                            if (DateTime.TryParse(operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.DataAplicCautelaResgatada).ToString(), out dataAplicCautelaResgatada))
                            {
                                PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
                                posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdCarteira.Equal(idCarteira.Value) &
                                                               posicaoCotistaColl.Query.IdCotista.Equal(idCotista.Value) &
                                                               posicaoCotistaColl.Query.DataConversao.Equal(dataAplicCautelaResgatada) &
                                                               posicaoCotistaColl.Query.Quantidade.NotEqual(0));

                                if (posicaoCotistaColl.Query.Load())
                                {
                                    ((PosicaoCotistaCollection)posicaoCollection).Combine(posicaoCotistaColl);
                                }
                                else
                                {
                                    throw new ArgumentException("Não existe posição com data de aplicação especificada! Id.Operação Cotista - " + operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao));
                                }
                            }
                            else
                            {
                                PosicaoCotista posicaoCotista = new PosicaoCotista();
                                if (!posicaoCotista.LoadByPrimaryKey(idPosicaoResgatada))
                                {
                                    posicaoCotista = new PosicaoCotista();
                                    posicaoCotista.Query.Where(posicaoCotista.Query.IdOperacao.Equal(idOperacaoResgatada));
                                    posicaoCotista.Query.Load();
                                }

                                if (posicaoCotista.Quantidade.GetValueOrDefault(0) == 0)
                                {
                                    throw new ArgumentException("Posição Específica não tem quantidade disponível! Id.Operação Cotista - " + operacao.GetColumn(OperacaoCotistaMetadata.ColumnNames.IdOperacao));
                                }

                                ((PosicaoCotistaCollection)posicaoCollection).AttachEntity(posicaoCotista);

                                #region Seleciona a cota do dia para operações offshore
                                int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
                                if (idTipoCliente == TipoClienteFixo.OffShore_PF || idTipoCliente == TipoClienteFixo.OffShore_PJ)
                                {
                                    historicoCota.BuscaValorCotaDia(idCarteira.Value, posicaoCotista.DataConversao.Value, data);
                                    cotaDia = carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;
                                }
                                #endregion
                            }
                            break;
                        case (byte)TipoResgateCotista.FIFO:
                            ((PosicaoCotistaCollection)posicaoCollection).BuscaPosicaoCotistaFIFO(idCarteira.Value, idCotista.Value);
                            break;
                        case (byte)TipoResgateCotista.LIFO:
                            ((PosicaoCotistaCollection)posicaoCollection).BuscaPosicaoCotistaLIFO(idCarteira.Value, idCotista.Value);
                            break;
                        case (byte)TipoResgateCotista.MenorImposto:
                            ((PosicaoCotistaCollection)posicaoCollection).BuscaPosicaoCotistaMenorImposto(idCarteira.Value, idCotista.Value);
                            break;
                        case (byte)TipoResgateCotista.Colagem:
                            {
                                ColagemResgate colagemResgate = new ColagemResgate();
                                ((PosicaoCotistaCollection)posicaoCollection).Combine(colagemResgate.BuscaPosicoesParaResgateCotista(idCarteira.Value, data, Convert.ToInt32(operacao.GetColumn(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao))));
                                break;
                            }
                    }
                    #endregion
                }
                else
                {
                    #region Carrega PosicaoFundoCollection, conforme o tipo de resgate (específico, FIFO, LIFO, Menor Imp.)
                    posicaoCollection = new PosicaoFundoCollection();
                    switch (tipoResgate)
                    {
                        case (byte)TipoResgateFundo.Especifico:
                            DateTime dataAplicCautelaResgatada = new DateTime();
                            if (DateTime.TryParse(operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.DataAplicCautelaResgatada).ToString(), out dataAplicCautelaResgatada))
                            {
                                PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
                                posicaoFundoColl.Query.Where(posicaoFundoColl.Query.IdCarteira.Equal(idCarteira.Value) &
                                                               posicaoFundoColl.Query.IdCliente.Equal(idCliente.Value) &
                                                               posicaoFundoColl.Query.DataConversao.Equal(dataAplicCautelaResgatada) &
                                                               posicaoFundoColl.Query.Quantidade.NotEqual(0));

                                if (posicaoFundoColl.Query.Load())
                                {
                                    ((PosicaoFundoCollection)posicaoCollection).Combine(posicaoFundoColl);
                                }
                                else
                                {
                                    throw new ArgumentException("Não existe posição com data de aplicação especificada! Id.Operação Fundo - " + operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdOperacao));
                                }
                            }
                            else
                            {
                                PosicaoFundo posicaoFundo = new PosicaoFundo();
                                if (!posicaoFundo.LoadByPrimaryKey(idPosicaoResgatada))
                                {
                                    posicaoFundo = new PosicaoFundo();
                                    posicaoFundo.Query.Where(posicaoFundo.Query.IdOperacao.Equal(idOperacaoResgatada));
                                    posicaoFundo.Query.Load();
                                }

                                if (posicaoFundo.Quantidade.GetValueOrDefault(0) == 0)
                                {
                                    throw new ArgumentException("Posição Específica não tem quantidade disponível! Id.Operação Fundo - " + operacao.GetColumn(OperacaoFundoMetadata.ColumnNames.IdOperacao));
                                }

                                ((PosicaoFundoCollection)posicaoCollection).AttachEntity(posicaoFundo);
                            }
                            break;
                        case (byte)TipoResgateFundo.FIFO:
                            ((PosicaoFundoCollection)posicaoCollection).BuscaPosicaoFundoFIFO(idCliente.Value, idCarteira.Value);
                            break;
                        case (byte)TipoResgateFundo.LIFO:
                            ((PosicaoFundoCollection)posicaoCollection).BuscaPosicaoFundoLIFO(idCliente.Value, idCarteira.Value);
                            break;
                        case (byte)TipoResgateFundo.MenorImposto:
                            ((PosicaoFundoCollection)posicaoCollection).BuscaPosicaoFundoMenorImposto(idCliente.Value, idCarteira.Value);
                            break;
                        case (byte)TipoResgateFundo.Colagem:
                            {
                                ColagemResgate colagemResgate = new ColagemResgate();
                                ((PosicaoFundoCollection)posicaoCollection).Combine(colagemResgate.BuscaPosicoesParaResgateFundo(idCliente.Value, data, Convert.ToInt32(operacao.GetColumn(OperacaoCotistaAuxMetadata.ColumnNames.IdOperacao))));
                                break;
                            }
                    }
                    #endregion
                }

                int TIPO_OPERACAO_RESGATELIQUIDO = this is OperacaoCotista ? (int)TipoOperacaoCotista.ResgateLiquido : (int)TipoOperacaoFundo.ResgateLiquido;

                bool calculaPfee = true;
                if (tipoProcessaResgate == TipoProcessaResgate.OperacaoFundo)
                {
                    calculaPfee = ParametrosConfiguracaoSistema.Fundo.PfeeCotaFundos;
                }

                if (tipoOperacao != TIPO_OPERACAO_RESGATELIQUIDO)
                {
                    if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                    {
                        this.ProcessaResgateCotas(idCarteira.Value, data, carteira, operacao,
                            posicaoCollection, cotaDia.Value, calculaPfee, debug, out debugProcessaResgate.DebugProcessaResgateCotas,
                            projecaoCalculo);
                    }
                    else
                    {
                        this.ProcessaResgateCotas(idCliente.Value, data, carteira, operacao,
                            posicaoCollection, cotaDia.Value, calculaPfee, debug, out debugProcessaResgate.DebugProcessaResgateCotas);
                    }
                }
                else
                {
                    if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                    {
                        if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                        {
                            if (carteira.CompensacaoPrejuizo.Value == (byte)TipoCompensacaoPrejuizo.Padrao)
                            {
                                this.ProcessaResgateLiquidoAcoes(idCarteira.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee, projecaoCalculo);
                            }
                            else
                            {
                                this.ProcessaResgateLiquidoAcoesCompensacaoDireta(idCarteira.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee, projecaoCalculo);
                            }
                        }
                        else
                        {
                            if (carteira.CompensacaoPrejuizo.Value == (byte)TipoCompensacaoPrejuizo.Padrao)
                            {
                                this.ProcessaResgateLiquidoAcoes(idCliente.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee);
                            }
                            else
                            {
                                this.ProcessaResgateLiquidoAcoesCompensacaoDireta(idCliente.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee);
                            }
                        }
                    }
                    else
                    {
                        if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
                        {
                            this.ProcessaResgateLiquidoRF(idCarteira.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee, projecaoCalculo);
                        }
                        else
                        {
                            this.ProcessaResgateLiquidoRF(idCliente.Value, data, carteira, operacao, posicaoCollection, cotaDia.Value, calculaPfee);
                        }
                    }
                }
            }

            if (!debug)
            {
                operacaoCollection.Save();
            }


        }

        /// <summary>
        /// Método principal para processamento de resgates com conversão na data. 
        /// Calcula os tributos (IR, IOF) e eventual pfee sobre os resgates.        
        /// Baixa as posições de cotas de cotistas.        
        /// </summary>
        /// <param name="idCarteiraCliente">
        /// Se for chamado de OperacaoCotista o parametro é o idCarteira
        /// Se for chamado de OperacaoFundo o parametro é o idCliente
        /// <param name="data"></param>
        /// <param name="tipoProcessaResgate">Indica se a origem chamadora é OperacaoFundo ou OperacaoCotista</param>
        /// <param name="projecaoCalculo">Parametro Opcional
        /// Indica se está apenas projetando os tributos na OperacaoCotista
        /// Se for passado OperacaoFundo esse parametro é desconsiderado
        /// </param>
        /// <exception cref="HistoricoCotaNaoCadastradoException">
        /// Se operacao for do tipo OperacaoFundo e a Cota não estiver Cadastrada na data Passada   
        /// </exception>
        /// 
        /// <exception cref="ArgumentException">
        /// Se operacao for do tipo OperacaoCotista projecaoCalculo é obrigatorio
        /// </exception>
        public void ProcessaResgate(int idCarteiraCliente, DateTime data, TipoProcessaResgate tipoProcessaResgate, params bool[] projecaoCalculo)
        {
            DebugProcessaResgate debugProcessaResgate;
            this.ProcessaResgate(idCarteiraCliente, data, tipoProcessaResgate, false, out debugProcessaResgate, projecaoCalculo);
        }

        /// <summary>
        /// Método principal para processamento de resgates com conversão na data. 
        /// Calcula os tributos (IR, IOF) e eventual pfee sobre os resgates.        
        /// Baixa as posições de cotas de cotistas.    
        /// </summary>
        /// <param name="idCarteiraCliente"></param>
        /// <param name="data"></param>
        /// <param name="projecaoCalculo"></param>
        /// <see cref="Financial.InvestidorCotista.ProcessaResgate(int idCarteiraCliente, DateTime data, TipoProcessaResgate tipoProcessaResgate, params bool[] projecaoCalculo)"/>
        public void ProcessaResgate(int idCarteiraCliente, DateTime data, bool projecaoCalculo)
        {
            this.ProcessaResgate(idCarteiraCliente, data, TipoProcessaResgate.OperacaoCotista, projecaoCalculo);
        }

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Dsitribui dividendos em forma de aplicação para os cotistas.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DistribuiDividendos(int idCarteira, DateTime data)
        {
            int diasCotizacao = 0;
            byte distribuicaoDividendos = 0;
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DiasCotizacaoAplicacao);
            campos.Add(carteira.Query.DistribuicaoDividendo);
            if (carteira.LoadByPrimaryKey(campos, idCarteira))
            {
                diasCotizacao = carteira.DiasCotizacaoAplicacao.Value;
                distribuicaoDividendos = carteira.DistribuicaoDividendo.Value;

                if (distribuicaoDividendos != (byte)DistribuicaoDividendosCarteira.DistribuiCotasCaixa
                    && distribuicaoDividendos != (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                {
                    return;
                }
            }

            #region Delete Operação Cotista
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCarteira),
                                                         operacaoCotistaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoCotista.DistribuicaoDividendo));
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.DataAgendamento.Equal(data));
            operacaoCotistaCollectionDeletar.Query.Load();
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();
            #endregion


            #region delete IRProventos
            IRProventosCollection iRProventosCollection = new IRProventosCollection();
            iRProventosCollection.Query.Where(iRProventosCollection.Query.IdCarteira.Equal(idCarteira));

            if (distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                iRProventosCollection.Query.Where(iRProventosCollection.Query.DataLancamento.Equal(data));
            else
                iRProventosCollection.Query.Where(iRProventosCollection.Query.DataVencimento.Equal(data));


            iRProventosCollection.Query.Load();
            iRProventosCollection.MarkAllAsDeleted();
            iRProventosCollection.Save();
            #endregion

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Descricao,
                                              liquidacaoCollection.Query.DataLancamento,
                                              liquidacaoCollection.Query.DataVencimento,
                                              liquidacaoCollection.Query.Valor.Sum());
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteira),
                                             liquidacaoCollection.Query.Origem.Equal((byte)OrigemLancamentoLiquidacao.Bolsa.Dividendo),
                                             liquidacaoCollection.Query.Valor.GreaterThan(0));

            if (distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataLancamento.Equal(data));
            else
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataVencimento.Equal(data));

            liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.Descricao,
                                               liquidacaoCollection.Query.DataLancamento,
                                               liquidacaoCollection.Query.DataVencimento);

            if (liquidacaoCollection.Query.Load())
            {

                foreach (Liquidacao liquidacao in liquidacaoCollection)
                {

                    decimal valorTotalDistribuir = liquidacao.Valor.Value;
                    string descricaoLancamento = liquidacao.Descricao;
                    DateTime dataLancamento = liquidacao.DataLancamento.Value;
                    DateTime dataVencimento = liquidacao.DataVencimento.Value;
                    DateTime dataConversao = Calendario.AdicionaDiaUtil(data, diasCotizacao);
                    if (distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                        dataConversao = Calendario.AdicionaDiaUtil(dataVencimento, diasCotizacao);

                    decimal valorCota = 0;
                    HistoricoCota historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(idCarteira, data);
                    valorCota = historicoCota.CotaFechamento.Value;

                    PosicaoCotistaAbertura posicaoCotistaAberturaTotal = new PosicaoCotistaAbertura();
                    decimal totalCotas = posicaoCotistaAberturaTotal.RetornaTotalCotas(idCarteira, data);

                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                    posicaoCotistaAberturaCollection.Query.Select(posicaoCotistaAberturaCollection.Query.IdCotista,
                                                                  posicaoCotistaAberturaCollection.Query.Quantidade.Sum());
                    posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                                 posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data),
                                                                 posicaoCotistaAberturaCollection.Query.Quantidade.GreaterThan(0));
                    posicaoCotistaAberturaCollection.Query.GroupBy(posicaoCotistaAberturaCollection.Query.IdCotista);
                    posicaoCotistaAberturaCollection.Query.Load();

                    decimal totalIR = 0;
                    decimal valorResidual = valorTotalDistribuir;
                    int cont = 1;
                    foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                    {
                        int idCotista = posicaoCotistaAbertura.IdCotista.Value;
                        decimal quantidade = posicaoCotistaAbertura.Quantidade.Value;
                        decimal fatorDistribuicao = quantidade / totalCotas;

                        decimal valorDistribuir = Utilitario.Truncate(valorTotalDistribuir * fatorDistribuicao, 2);

                        if (cont == posicaoCotistaAberturaCollection.Count)
                        {
                            valorDistribuir = valorResidual;
                        }

                        Cotista cotista = new Cotista();
                        cotista.LoadByPrimaryKey(idCotista);
                        decimal valorIr = 0;
                        DateTime dataCorteRFB1585 = new DateTime(2015, 09, 02);
                        if (cotista.IsentoIR.Equals("N") && DateTime.Compare(dataLancamento, dataCorteRFB1585) >= 0)
                        {
                            valorIr = Utilitario.Truncate(valorDistribuir * 0.15M, 2);
                            valorDistribuir -= valorIr;
                            totalIR += valorIr;
                        }

                        valorResidual -= (valorDistribuir + valorIr);

                        OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();
                        operacaoCotista.CotaOperacao = valorCota;
                        operacaoCotista.DataAgendamento = data;
                        operacaoCotista.DataConversao = dataConversao;
                        operacaoCotista.DataLiquidacao = data;
                        operacaoCotista.DataOperacao = data;
                        if (distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                        {
                            operacaoCotista.DataLiquidacao = dataVencimento;
                            operacaoCotista.DataOperacao = dataVencimento;
                        }
                        operacaoCotista.IdCarteira = idCarteira;
                        operacaoCotista.IdCotista = idCotista;
                        operacaoCotista.IdFormaLiquidacao = 1; //TODO AJUSTAR!!!!!
                        operacaoCotista.Quantidade = 0;
                        operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.Aplicacao;
                        operacaoCotista.ValorBruto = valorDistribuir;
                        operacaoCotista.ValorLiquido = valorDistribuir;
                        operacaoCotista.Fonte = (byte)FonteOperacaoCotista.DistribuicaoDividendo;

                        #region insere Tabela para consulta da distribuicao
                        IRProventos irProventos = new IRProventos();
                        irProventos.IdCarteira = idCarteira;
                        irProventos.IdCotista = idCotista;
                        irProventos.ValorDistribuido = valorDistribuir + valorIr;
                        irProventos.ValorIR = valorIr;
                        irProventos.ValorLiquido = valorDistribuir;
                        irProventos.Descricao = descricaoLancamento;
                        irProventos.DataLancamento = dataLancamento;
                        irProventos.DataVencimento = dataVencimento;
                        irProventos.IsentoIR = cotista.IsentoIR;
                        irProventos.Save();
                        #endregion

                        cont++;

                    }

                    operacaoCotistaCollection.Save();

                    //Lanca em CC um estorno do total de dividendos pagos na data
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //

                    string descricao = "Distribuição de dividendos pagos na data";

                    Liquidacao liquidacaoDistribuicao = new Liquidacao();
                    liquidacaoDistribuicao.IdCliente = idCarteira;
                    liquidacaoDistribuicao.DataLancamento = data;
                    liquidacaoDistribuicao.DataVencimento = data;
                    liquidacaoDistribuicao.Descricao = descricao;
                    liquidacaoDistribuicao.Valor = (valorTotalDistribuir - totalIR) * -1;
                    liquidacaoDistribuicao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacaoDistribuicao.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.Outros;
                    liquidacaoDistribuicao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacaoDistribuicao.IdConta = idContaDefault;
                    liquidacaoDistribuicao.Save();


                    #region RFB 1585
                    if (totalIR > 0)
                    {
                        //Lanca em CC um estorno do total de dividendos pagos na data
                        //Busca o idContaDefault do cliente
                        contaCorrente = new Investidor.ContaCorrente();
                        idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                        //

                        descricao = "IR - Distribuição de dividendos";

                        Liquidacao liquidacaoIR = new Liquidacao();
                        liquidacaoIR.IdCliente = idCarteira;
                        liquidacaoIR.DataLancamento = data;
                        liquidacaoIR.DataVencimento = data;
                        liquidacaoIR.Descricao = descricao;
                        liquidacaoIR.Valor = totalIR * -1;
                        liquidacaoIR.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacaoIR.Origem = OrigemLancamentoLiquidacao.Bolsa.IRProventos;
                        liquidacaoIR.Fonte = (int)FonteLancamentoLiquidacao.Manual;
                        liquidacaoIR.IdConta = idContaDefault;
                        liquidacaoIR.Save();

                    }
                    #endregion
                }
            }

        }

        /// <summary>
        /// Baseado no Tipo de Processamento Atualiza a Data Limite do Prejuizo considerando
        /// PrejuizoCotista e PrejuizoFundo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idClienteCotista">Se Tipo de Processamento de Resgate for OperacaoCotista
        /// esse parametro é o idCotista. Se for OperacaoFundo é o IdCliente
        /// <param name="tipo"></param>
        private void AtualizaDataLimite(DateTime data, int idCarteira, int idCotistaCliente, TipoProcessaResgate tipo)
        {
            if (tipo == TipoProcessaResgate.OperacaoCotista)
            {
                PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
                prejuizoCotista.AtualizaDataLimite(data, idCarteira, idCotistaCliente);
            }
            else
            {
                PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                prejuizoFundo.AtualizaDataLimite(data, idCarteira, idCotistaCliente);
            }
        }

        public bool ImportaMovimentoYMF(Financial.Interfaces.Import.YMF.MovimentoYMF movimento, bool deferSave)
        {
            const int ID_TEMPLATE_IMPORTACAO_YMF = 7777;//jogar no web.config

            this.DataOperacao = movimento.DtMovimento;

            if ((movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateDividendos) ||
                (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateJuros) ||
                (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.TR) ||
                (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateLiberado) ||
                (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.VR))
            {
                //Nao importar
                return false;
            }

            if (!movimento.VlBruto.HasValue || !movimento.QtCotas.HasValue)
            {
                //Nao importar. Em geral sao lançamentos futuros em relacao à data do fundo que ainda nao foram cotizados
                return false;
            }

            this.TipoOperacao = MapTipoMovimentoYMF(movimento.CdTipo);

            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(movimento.CdCotista.Trim());
            if (cotista == null)
            {
                throw new Exception("Cotista não cadastrado: " + movimento.CdCotista);
            }
            this.IdCotista = cotista.IdCotista;

            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(movimento.CdFundo);
            if (cliente == null)
            {
                throw new Exception("Não foi possível encontrar o cliente/fundo com códigoYMF: " + movimento.CdFundo);

                /*
                //Código de importação automatica:
                //Nao vamos dar erro. Neste caso precisamos criar o Fundo/Cliente

                //Tentar localizar pessoa pelo Codigo Interface importado da YMF
                Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCodigoInterface(movimento.CdFundo);
                if (pessoa == null)
                {
                    throw new Exception("Não foi possível encontrar a pessoa com código de interface: " + movimento.CdFundo);
                }

                cliente = new Cliente();
                cliente = cliente.CriaClienteViaTemplate(pessoa.IdPessoa.Value, ID_TEMPLATE_IMPORTACAO_YMF, pessoa.Nome, pessoa.Apelido, movimento.CdFundo);
                */
            }

            this.IdCarteira = cliente.IdCliente;

            if (!string.IsNullOrEmpty(movimento.CdCriterioResgate) &&
                this.TipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
            {
                this.TipoResgate = MapCriterioResgateMovimentoYMF(movimento.CdCriterioResgate);
            }

            this.DataLiquidacao = movimento.DtLiquidacaoFinanceira.HasValue ? movimento.DtLiquidacaoFinanceira : this.DataOperacao;
            this.DataConversao = movimento.DtLiquidacaoFisica.HasValue ? movimento.DtLiquidacaoFisica.Value : this.DataLiquidacao;
            if (this.TipoOperacao == (byte)TipoOperacaoCotista.ComeCotas)
            {
                this.ValorBruto = 0;
                this.ValorLiquido = 0;
            }
            else
            {
                this.ValorBruto = movimento.VlBruto;
                this.ValorLiquido = movimento.VlLiquido;
            }

            this.ValorIR = movimento.VlIr;
            this.ValorIOF = movimento.VlIof;
            this.ValorPerformance = movimento.VlPerformance.HasValue ? movimento.VlPerformance.Value : 0;
            this.Quantidade = movimento.QtCotas;
            this.CotaOperacao = movimento.VlCotaMovimentacao.HasValue ? movimento.VlCotaMovimentacao.Value : 0;
            this.DataAgendamento = movimento.DtMovimento;

            //this.IdPosicaoResgatada = movimento.IdNotaDigitada; //Codigo antigo... Por ora setar null
            this.IdPosicaoResgatada = null;

            this.IdFormaLiquidacao = 3; //Dinheiro - valor proveniente de tabela populada na criação do banco de dados

            this.ValorCPMF = 0;

            this.PrejuizoUsado = 0;
            this.RendimentoResgate = 0;
            this.VariacaoResgate = 0;
            this.Observacao = string.Empty;
            this.IdConta = null;
            this.Fonte = (byte)FonteOperacaoCotista.Manual;

            if (!deferSave)
            {
                this.Save();
            }

            return true;
        }

        /*       public bool ImportaMovimentoAnaliticoYMF(Financial.Interfaces.Import.Fundo.MovimentoAnaliticoYMF movimento, bool deferSave, bool comCalculo)
               {
                   this.IdOperacao = movimento.IdNota;

                   this.IdCotista = Convert.ToInt32(movimento.CdCotista);

                   Cotista cotista = new Cotista();
                   if (!cotista.LoadByPrimaryKey(this.IdCotista.Value))
                   {
                       throw new Exception("Cotista não encontrado: " + this.IdCotista.Value);
                   }

                   this.IdCarteira = Convert.ToInt32(movimento.CdFundo);
                   Carteira carteira = new Carteira();
                   if (!carteira.LoadByPrimaryKey(this.IdCarteira.Value))
                   {
                       throw new Exception("Carteira não encontrada: " + this.IdCarteira.Value);
                   }

                   this.DataOperacao = movimento.DtMovimento;
                   this.DataConversao = movimento.DtLiquidacaoFisica;
                   this.DataLiquidacao = movimento.DtLiquidacaoFinanceira;
                   this.DataAgendamento = movimento.DtMovimento;

                   if ((movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.TR) ||
                       (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.VR))
                   {
                       //Nao importar
                       return false;
                   }

                   if (comCalculo) //Se a opção for com cálculo os comecotas serão calculados pelo Financial
                   {
                       if (movimento.CdTipo == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateIR)
                       {
                           //Nao importar
                           return false;
                       }
                   }

                   this.TipoOperacao = MapTipoMovimentoYMF(movimento.CdTipo);

                   this.TipoOperacao = MapTipoMovimentoYMF(movimento.CdTipo);
                   if (this.TipoOperacao != (byte)TipoOperacaoFundo.Aplicacao)
                   {
                       this.TipoResgate = (byte)TipoResgateFundo.Especifico;
                       this.Quantidade = movimento.QtCotas;
                       this.ValorBruto = movimento.VlBruto;
                       this.ValorLiquido = movimento.VlLiquido;
                       this.ValorIR = movimento.VlIR;
                       this.ValorIOF = movimento.VlIOF;
                   }
                   else
                   {
                       this.Quantidade = movimento.QtCotasTotal;
                       this.ValorBruto = movimento.VlBrutoTotal;
                       this.ValorLiquido = movimento.VlLiquidoTotal;
                       this.ValorIR = 0;
                       this.ValorIOF = 0;
                   }

                   this.IdPosicaoResgatada = movimento.IdNotaAplicacao;

                   this.IdFormaLiquidacao = 1; //Checar se vai ser DOC mesmo

                   this.CotaOperacao = this.ValorBruto.Value / this.Quantidade.Value;
                   this.ValorCPMF = 0;
                   this.ValorPerformance = 0;

                   this.PrejuizoUsado = 0;
                   this.RendimentoResgate = 0;
                   this.VariacaoResgate = 0;
                   this.Observacao = string.Empty;
                   if (!deferSave)
                   {
                       this.Save();
                   }

                   return true;
               }
               */

        public byte MapTipoMovimentoYMF(string tipoMovimentoYMF)
        {
            //Faz o mapeamento entro e ENUM TipoMovimentoYMF definido em Interfaces com o ENUM TipoOperacaoCotista
            byte tipoOperacaoCotista;
            if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.Aplicacao ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.AG)
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.Aplicacao;
            }
            else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateBruto) ||
                ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaBruto)))
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateBruto;
            }
            else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateCotas) ||
      ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaCotas)))
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateCotas;
            }
            else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateLiquido) ||
      ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaLiquido)))
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateLiquido;
            }
            else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateTotal) ||
      ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaTotal)))
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateTotal;
            }
            else if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateIR)
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.ComeCotas;
            }
            else if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.RM)
            {
                tipoOperacaoCotista = (byte)TipoOperacaoCotista.Amortizacao;
            }

            else
            {
                throw new Exception("Formato de Tipo de Operação Cotista não suportado: " + tipoMovimentoYMF);
            }
            return tipoOperacaoCotista;
        }

        public byte MapCriterioResgateMovimentoYMF(string criterioResgateMovimentoYMF)
        {
            //Faz o mapeamento entro e ENUM CriterioResgateMovimentoYMF definido em Interfaces com o ENUM TipoResgateCotista
            byte tipoResgateCotista;
            if (criterioResgateMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.CriterioResgateMovimentoYMF.FIFO)
            {
                tipoResgateCotista = (byte)TipoResgateCotista.FIFO;
            }
            else if (criterioResgateMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.CriterioResgateMovimentoYMF.ConservacaoCotas)
            {
                tipoResgateCotista = (byte)TipoResgateCotista.FIFO;
            }
            else if (criterioResgateMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.CriterioResgateMovimentoYMF.MelhorAniversario)
            {
                tipoResgateCotista = (byte)TipoResgateCotista.MenorImposto;
            }
            else if (criterioResgateMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.CriterioResgateMovimentoYMF.I)
            {
                tipoResgateCotista = (byte)TipoResgateCotista.MenorImposto;
            }
            else
            {
                throw new Exception("Formato de Tipo de Resgate Cotista não suportado: " + criterioResgateMovimentoYMF);
            }
            return tipoResgateCotista;
        }

        public void ProcessaEventos(int idCarteira, DateTime data)
        {
            ComplementoProventosCotistaCollection complementoProventosCotistaColl = new ComplementoProventosCotistaCollection();
            LiquidacaoCollection liquidacaoColl = new LiquidacaoCollection();
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            Hashtable hsCotista = new Hashtable();            

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                operacaoCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Amortizacao,
                                                                                                (byte)TipoOperacaoCotista.Juros,
                                                                                                (byte)TipoOperacaoCotista.AmortizacaoJuros,
                                                                                                (byte)TipoOperacaoCotista.Dividendo),
                                                operacaoCotistaCollection.Query.DataOperacao.Equal(data));
            operacaoCotistaCollection.Query.Load();
            operacaoCotistaCollection.MarkAllAsDeleted();
            operacaoCotistaCollection.Save();

            AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
            agendaFundoCollection.Query.Where(agendaFundoCollection.Query.DataEvento.Equal(data),
                                              agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                              agendaFundoCollection.Query.TipoEvento.In((byte)TipoEventoFundo.Amortizacao,
                                                                                        (byte)TipoEventoFundo.AmortizacaoJuros,
                                                                                        (byte)TipoEventoFundo.Juros,
                                                                                        (byte)TipoEventoFundo.Dividendo));
            if (!agendaFundoCollection.Query.Load())
                return;

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdPosicao,
                                                posicaoCotistaCollection.Query.IdCotista,
                                                posicaoCotistaCollection.Query.Quantidade,
                                                posicaoCotistaCollection.Query.DataAplicacao,
                                                posicaoCotistaCollection.Query.DataConversao,
                                                posicaoCotistaCollection.Query.CotaAplicacao,
                                                posicaoCotistaCollection.Query.IdOperacao,
                                                posicaoCotistaCollection.Query.AmortizacaoAcumuladaPorCota,
                                                posicaoCotistaCollection.Query.AmortizacaoAcumuladaPorValor,
                                                posicaoCotistaCollection.Query.JurosAcumuladoPorCota,
                                                posicaoCotistaCollection.Query.JurosAcumuladoPorValor,
                                                posicaoCotistaCollection.Query.ValorBruto,
                                                posicaoCotistaCollection.Query.ValorAplicacao);
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));

            if (!posicaoCotistaCollection.Query.Load())
                return;

            decimal valor = 0;
            decimal cotaFechamento = 0;
            int tipoCota = carteira.TipoCota.Value;
            int idLocalFeriado = cliente.IdLocal.Value;
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idCarteira, data);

            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);

            //Busca valor da cota no dia
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaFechamento = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaFechamento = historicoCota.CotaFechamento.Value;
            }

            decimal valorEventoJuros = 0;
            decimal valorAmortizacao = 0;
            decimal valorDividendo = 0;
            decimal valorAmortzJuros = 0;

            decimal taxaJuros = 0;
            decimal taxaAmortizacao = 0;
            decimal taxaDividendo = 0;
            decimal taxaAmortzJuros = 0;

            CalculoTributo calculoTributo = new CalculoTributo();
            CalculoTributo.ProjecaoIRCotista projecaoIR = (carteira.ProjecaoIRResgate.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRResgate.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
            int diasAposResgate = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposResgateIR.Value : 0);

            DateTime dataPagamentoTributoIR = calculoTributo.RetornaDiaPagamentoIR(data, projecaoIR, diasAposResgate, idLocalFeriado);
            DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, carteira.DiasLiquidacaoResgate.GetValueOrDefault(0));

            foreach (AgendaFundo agendaFundo in agendaFundoCollection)
            {
                byte tipoEvento = agendaFundo.TipoEvento.Value;
                decimal taxa = agendaFundo.Taxa.Value;

                if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.ValorporCota))
                {
                    valor = agendaFundo.Valor.Value;

                    agendaFundo.CotaFechamentoCheia = cotaFechamento;
                }
                else
                {
                    if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.TaxaCotaEx))
                    {
                        valor = cotaFechamento * (taxa / (100 - taxa));
                        agendaFundo.Valor = valor;

                        agendaFundo.CotaFechamentoCheia = cotaFechamento;
                    }

                    if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.TaxaFixa))
                    {
                        valor = cotaFechamento * taxa / 100;
                        agendaFundo.Valor = valor;

                        agendaFundo.CotaFechamentoCheia = cotaFechamento;
                    }
                }

                if (tipoEvento == (byte)TipoEventoFundo.Amortizacao)
                {
                    valorAmortizacao += valor;
                    taxaAmortizacao = agendaFundo.Taxa.Value;
                }
                else if (tipoEvento == (byte)TipoEventoFundo.Juros)
                {
                    valorEventoJuros += valor;
                    taxaJuros = agendaFundo.Taxa.Value;
                }
                else if (tipoEvento == (byte)TipoEventoFundo.Dividendo)
                {
                    valorDividendo += valor;
                    taxaDividendo = agendaFundo.Taxa.Value;
                }
                else if (tipoEvento == (byte)TipoEventoFundo.AmortizacaoJuros)
                {
                    valorAmortzJuros += valor;
                    taxaAmortzJuros = agendaFundo.Taxa.Value;
                }
            }

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                int idPosicao = posicaoCotista.IdPosicao.Value;
                int idCotista = posicaoCotista.IdCotista.Value;
                decimal quantidade = posicaoCotista.Quantidade.Value;
                DateTime dataAplicacao = posicaoCotista.DataAplicacao.Value;
                DateTime dataConversao = posicaoCotista.DataConversao.Value;
                decimal cotaAplicacao = posicaoCotista.CotaAplicacao.Value;
                decimal rendimento = 0;
                decimal valorIR = 0;
                decimal aliquotaIR = 0;                
                decimal cotaFechamentoEX = historicoCota.CotaFechamento.Value;
                string codigoAtivo = idCarteira.ToString();

                Cotista cotista = new Cotista();
                Pessoa pessoa = new Pessoa();
                if (hsCotista.Contains(idCotista))
                {
                    cotista = (Cotista)hsCotista[idCotista];
                    pessoa = cotista.UpToPessoaByIdPessoa;
                }
                else
                {
                    cotista.LoadByPrimaryKey(idCotista);
                    pessoa = cotista.UpToPessoaByIdPessoa;
                    hsCotista.Add(idCotista, cotista);
                }
                ListaTipoInvestidor tipoInvestidor;
                if (pessoa.Tipo.HasValue)
                {
                    if (pessoa.Tipo == 1)
                        tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                    else
                        tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
                }
                else
                {
                    tipoInvestidor = ListaTipoInvestidor.OffShore;
                }
                if (!cotista.IsIsentoIR())
                {
                    aliquotaIR = (new ExcecoesTributacaoIR()).RetornaAliquotaFundo(carteira, carteira.IdCategoria.Value, data, dataAplicacao, tipoInvestidor);
                }
                //Evento de Juros
                if (valorEventoJuros != 0)
                {
                    rendimento = valorEventoJuros * quantidade;
                    if (aliquotaIR != 0)                                                                    
                        valorIR = Math.Round(rendimento * aliquotaIR / 100M, 2, MidpointRounding.AwayFromZero); //Juros aplica direto o % do IR no valor dos juros                    
                    byte tipoOperacao = (byte)TipoOperacaoCotista.Juros;
                    //Gera Operação
                    OperacaoCotista operacaoCotista = GeraOperacaoEvento(idCarteira, idCotista, data, dataLiquidacao, tipoOperacao, rendimento, valorIR);
                    operacaoCotista.Save();
                    //Gera Complemento
                    complementoProventosCotistaColl.AttachEntity(GeraComplementoProventosCotista(operacaoCotista.IdOperacao.Value, posicaoCotista.IdOperacao.Value, rendimento, quantidade, dataLiquidacao));

                    //Gera Liquidação
                    liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Juros, -rendimento, valorIR, idPosicao, codigoAtivo));
                    liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataPagamentoTributoIR, idContaDefault, (byte)TipoEventoFundo.Juros, 0, -valorIR, idPosicao, codigoAtivo));

                    //Soma valor de juros por Cota e valor      
                    posicaoCotista.JurosAcumuladoPorCota += valorEventoJuros;
                    posicaoCotista.JurosAcumuladoPorValor += rendimento;
                }

                //Evento de Amortização
                if(valorAmortizacao != 0)
                {
                    rendimento = valorAmortizacao * quantidade;                    

                    byte tipoOperacao = (byte)TipoOperacaoCotista.Amortizacao;
                    //Gera Operação
                    OperacaoCotista operacaoCotista = GeraOperacaoEvento(idCarteira, idCotista, data, dataLiquidacao, tipoOperacao, rendimento, 0);
                    operacaoCotista.Save();

                    //Gera Complemento
                    complementoProventosCotistaColl.AttachEntity(GeraComplementoProventosCotista(operacaoCotista.IdOperacao.Value, posicaoCotista.IdOperacao.Value, rendimento, quantidade, dataLiquidacao));

                    //Gera Liquidação
                    liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Amortizacao, -rendimento, 0, idPosicao, codigoAtivo));

                    //Soma valor de juros por Cota e valor      
                    posicaoCotista.AmortizacaoAcumuladaPorCota += valorAmortizacao;
                    posicaoCotista.AmortizacaoAcumuladaPorValor += rendimento;
                }

                //Evento de Dividendo
                if (valorDividendo != 0)
                {
                    rendimento = valorDividendo * quantidade;

                    byte tipoOperacao = (byte)TipoOperacaoCotista.Dividendo;
                    //Gera Operação
                    OperacaoCotista operacaoCotista = GeraOperacaoEvento(idCarteira, idCotista, data, dataLiquidacao, tipoOperacao, rendimento, 0);
                    operacaoCotista.Save();

                    //Gera Complemento
                    complementoProventosCotistaColl.AttachEntity(GeraComplementoProventosCotista(operacaoCotista.IdOperacao.Value, posicaoCotista.IdOperacao.Value, rendimento, quantidade, dataLiquidacao));

                    //Gera Liquidação
                    liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Dividendo, -rendimento, 0, idPosicao, codigoAtivo));                    
                }

                //Amortização + Juros
                if (valorAmortzJuros != 0)
                {
                    decimal valorAplicacaoEX = posicaoCotista.ValorAplicacao.Value - posicaoCotista.AmortizacaoAcumuladaPorValor.Value;
                    decimal valorDiaEX = posicaoCotista.Quantidade.Value * cotaFechamentoEX;

                    if (truncaFinanceiro)
                    {
                        valorDiaEX = Utilitario.Truncate(posicaoCotista.Quantidade.Value * cotaFechamentoEX, 2);
                    }
                    else
                    {
                        valorDiaEX = Utilitario.RoundMax(posicaoCotista.Quantidade.Value * cotaFechamentoEX, 2);
                    }


                    decimal valorAmortizado = valorAplicacaoEX * (taxaAmortzJuros / 100);
                    decimal valorJurosBruto = (valorDiaEX - valorAplicacaoEX) * (taxaAmortzJuros / 100);
                                        
                    #region Juros
                    byte tipoOperacao = (byte)TipoOperacaoCotista.Juros;
                    decimal valorJurosIR = 0;
                    if (valorJurosBruto > 0)
                    {
                        if (aliquotaIR != 0)
                            valorJurosIR = valorJurosBruto * (aliquotaIR / 100);
                        
                        //Gera Operação
                        OperacaoCotista operacaoJurosCotista = GeraOperacaoEvento(idCarteira, idCotista, data, dataLiquidacao, tipoOperacao, valorJurosBruto, valorJurosIR);
                        operacaoJurosCotista.Save();

                        //Gera Complemento
                        complementoProventosCotistaColl.AttachEntity(GeraComplementoProventosCotista(operacaoJurosCotista.IdOperacao.Value, posicaoCotista.IdOperacao.Value, valorJurosBruto, quantidade, dataLiquidacao));

                        //Gera Liquidação
                        liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Juros, -valorJurosBruto, valorJurosIR, idPosicao, codigoAtivo));
                        liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataPagamentoTributoIR, idContaDefault, (byte)TipoEventoFundo.Juros, 0, -valorJurosIR, idPosicao, codigoAtivo));

                        //Soma valor de juros por Cota e valor      
                        posicaoCotista.JurosAcumuladoPorCota += (valorJurosBruto / quantidade);
                        posicaoCotista.JurosAcumuladoPorValor += valorJurosBruto;
                    }
                    #endregion

                    #region Amortização

                    tipoOperacao = (byte)TipoOperacaoCotista.Amortizacao;
                    //Gera Operação
                    OperacaoCotista operacaoAmortzCotista = GeraOperacaoEvento(idCarteira, idCotista, data, dataLiquidacao, tipoOperacao, valorAmortizado, 0);
                    operacaoAmortzCotista.Save();                    

                    //Gera Complemento
                    complementoProventosCotistaColl.AttachEntity(GeraComplementoProventosCotista(operacaoAmortzCotista.IdOperacao.Value, posicaoCotista.IdOperacao.Value, valorAmortizado, quantidade, dataLiquidacao));

                    //Gera Liquidação
                    liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCarteira, cotista.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Amortizacao, -valorAmortizado, 0, idPosicao, codigoAtivo));

                    //Soma valor de amortização por Cota e valor
                    posicaoCotista.AmortizacaoAcumuladaPorCota += (valorAmortizado / quantidade);
                    posicaoCotista.AmortizacaoAcumuladaPorValor += valorAmortizado;

                    #endregion                    
                }                               
            }

            operacaoCotistaColl.Save();
            agendaFundoCollection.Save();
            complementoProventosCotistaColl.Save();
            posicaoCotistaCollection.Save();
            liquidacaoColl.Save();
        }

        private ComplementoProventosCotista GeraComplementoProventosCotista(int idOperacao, int idOperacaoReferencia, decimal rendimento, decimal quantidade, DateTime dataAplicacao)
        {
            #region Atualiza tabela de complemento de proventos
            ComplementoProventosCotista complementoProventosCotista= new ComplementoProventosCotista();
            complementoProventosCotista.IdOperacao = idOperacao;
            complementoProventosCotista.IdOperacaoReferencia = idOperacaoReferencia;
            complementoProventosCotista.Rendimento = rendimento;
            complementoProventosCotista.Quantidade = quantidade;
            complementoProventosCotista.DataAplicacao = dataAplicacao;
            #endregion

            return complementoProventosCotista;
        }

        private OperacaoCotista GeraOperacaoEvento(int idCarteira, int idCotista, DateTime data, DateTime dataLiquidacao, byte tipoOperacao, decimal rendimento, decimal valorIR)
        {
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            operacaoCotista.TipoOperacao = tipoOperacao;
            operacaoCotista.DataAgendamento = data;
            operacaoCotista.DataConversao = data;
            operacaoCotista.DataLiquidacao = dataLiquidacao;
            operacaoCotista.DataOperacao = data;
            operacaoCotista.IdCarteira = idCarteira;
            operacaoCotista.IdCotista = idCotista;
            operacaoCotista.IdFormaLiquidacao = 1; //FORÇADO
            operacaoCotista.Quantidade = 0;
            operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
            operacaoCotista.ValorIR = valorIR;
            operacaoCotista.ValorBruto = rendimento;
            operacaoCotista.ValorLiquido = rendimento - valorIR;
            operacaoCotista.DataRegistro = data;

            return operacaoCotista;
        }

        private LiquidacaoCollection GeraLiquidacaoEvento(int idCarteira, string apelido_ContraParte, DateTime data, DateTime dataLiquidacao, int idContaDefault, byte tipoEvento, decimal valorBruto, decimal valorIR, int idPosicao, string codigoAtivo)
        {
            LiquidacaoCollection liqColl = new LiquidacaoCollection();
            int origemValorBruto = OrigemLancamentoLiquidacao.Fundo.None;
            int origemValorIR = OrigemLancamentoLiquidacao.Fundo.None;

            string descricao = "Id.Posi��o " + idPosicao + " (Cotista) - ";

            if (tipoEvento == (byte)TipoEventoFundo.Amortizacao)
            {
                descricao += "Amortiza��o ";
                origemValorBruto = OrigemLancamentoLiquidacao.Cotista.Amortizacao;
            }
            else if (tipoEvento == (byte)TipoEventoFundo.Dividendo)
            {
                descricao += "Dividendo ";
                origemValorBruto = OrigemLancamentoLiquidacao.Cotista.Dividendo;
            }
            else if (tipoEvento == (byte)TipoEventoFundo.ComeCotas)
            {
                descricao += "ComeCotas ";
                origemValorBruto = OrigemLancamentoLiquidacao.Cotista.ComeCotas;
            }
            else
            {
                descricao += "Juros ";
                origemValorBruto = OrigemLancamentoLiquidacao.Cotista.Juros;
                origemValorIR = OrigemLancamentoLiquidacao.Cotista.JurosIR;
            }

            descricao = descricao + "'" + apelido_ContraParte + "'";

            if (valorBruto != 0)
            {
                Liquidacao liquidacao = liqColl.AddNew();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCarteira;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Origem = origemValorBruto;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Valor = valorBruto;
                liquidacao.CodigoAtivo = codigoAtivo;
            }

            if (valorIR != 0)
            {
                Liquidacao liquidacaoIR = liqColl.AddNew();
                liquidacaoIR.DataLancamento = data;
                liquidacaoIR.DataVencimento = dataLiquidacao;
                liquidacaoIR.Descricao = descricao.Replace("Juros ", "IR / Juros ");
                liquidacaoIR.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacaoIR.IdCliente = idCarteira;
                liquidacaoIR.IdConta = idContaDefault;
                liquidacaoIR.Origem = origemValorIR;
                liquidacaoIR.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacaoIR.Valor = valorIR;
                liquidacaoIR.CodigoAtivo = codigoAtivo;
            }

            return liqColl;
        }

        public void ConsolidaDetalheResgateCotista()
        {
            DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
            detalheResgateCotista.Query.Select(detalheResgateCotista.Query.RendimentoResgate.Sum(),
                detalheResgateCotista.Query.VariacaoResgate.Sum());
            detalheResgateCotista.Query.Where(detalheResgateCotista.Query.IdOperacao.Equal(this.IdOperacao.Value));
            detalheResgateCotista.Query.Load();

            this.RendimentoResgate = detalheResgateCotista.RendimentoResgate;
            this.VariacaoResgate = detalheResgateCotista.VariacaoResgate;
        }

        public decimal RetornaIRDescontarFDIC(DateTime dataCalculo, DateTime dataConversao, int idCarteira, decimal renda)
        {
            decimal totalPercentualEventos = 0;
            AgendaFundo agendaFundo = new AgendaFundo();
            agendaFundo.Query.Select(agendaFundo.Query.Taxa.Sum());
            agendaFundo.Query.Where(agendaFundo.Query.IdCarteira.Equal(idCarteira),
                                    agendaFundo.Query.DataEvento.Equal(dataCalculo),
                                    agendaFundo.Query.TipoEvento.NotEqual((byte)TipoEventoFundo.Amortizacao));
            agendaFundo.Query.Load();

            if (agendaFundo.Taxa.HasValue)
            {
                totalPercentualEventos = agendaFundo.Taxa.Value;
            }

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoTributacao);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            decimal aliquotaIR = 0;
            CalculoTributo calculoTributo = new CalculoTributo();
            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.CPrazo_SemComeCotas ||
                carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.CurtoPrazo)
            {
                aliquotaIR = calculoTributo.RetornaAliquotaIRCurtoPrazo(dataConversao, dataCalculo);
            }
            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.LPrazo_SemComeCotas ||
                carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.LongoPrazo)
            {
                aliquotaIR = calculoTributo.RetornaAliquotaIRLongoPrazo(dataConversao, dataCalculo);
            }

            decimal valorIR = 0;

            if (totalPercentualEventos != 0)
            {
                valorIR = Math.Round(renda * (totalPercentualEventos / 100M) * (aliquotaIR / 100M), 2);
            }

            return valorIR;
        }

        private DateTime RetornaDataLiquidacao(esEntity posicao)
        {
            DateTime dataLiquidacao = new DateTime();
            if (posicao is PosicaoFundo)
            {
                OperacaoFundo operacaoFundo = new OperacaoFundo();
                if (operacaoFundo.LoadByPrimaryKey(Convert.ToInt32(posicao.GetColumn(PosicaoFundoMetadata.ColumnNames.IdOperacao))))
                    dataLiquidacao = operacaoFundo.DataLiquidacao.Value;
            }
            else if (posicao is PosicaoCotista)
            {
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                if (operacaoCotista.LoadByPrimaryKey(Convert.ToInt32(posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdOperacao))))
                    dataLiquidacao = operacaoCotista.DataLiquidacao.Value;
            }

            return dataLiquidacao;
        }

        public void AlteraTipoResgate(int idCliente, DateTime data, TipoProcessaResgate tipoProcessaResgate)
        {
            #region Cotista 
            if (tipoProcessaResgate == TipoProcessaResgate.OperacaoCotista)
            {
                int idCarteira = idCliente;
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCotaDia(idCarteira, data);

                decimal cotaDia = carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;

                List<int> lstTipoOperacao = new List<int>();
                lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateBruto);
                lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateCotas);
                lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateCotasEspecial);
                lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateLiquido);
                lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateTotal);
                lstTipoOperacao.Add((int)TipoOperacaoCotista.Retirada);

                if (carteira.PossuiResgateAutomatico == "S" && (carteira.ValorMinimoSaldo.HasValue && carteira.ValorMinimoSaldo.Value > 0))
                {
                    PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
                    posicaoCotistaColl.Query.Select(posicaoCotistaColl.Query.IdCotista, 
                                                    posicaoCotistaColl.Query.ValorIR.Sum(),
                                                    posicaoCotistaColl.Query.ValorLiquido.Sum(),
                                                    posicaoCotistaColl.Query.ValorBruto.Sum());
                    posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdCarteira.Equal(idCliente));
                    posicaoCotistaColl.Query.GroupBy(posicaoCotistaColl.Query.IdCotista);

                    if (!posicaoCotistaColl.Query.Load())
                        return;

                    foreach (PosicaoCotista pos in posicaoCotistaColl)
                    {
                        int idCotista = pos.IdCotista.Value;
                        decimal saldoCotista = pos.ValorBruto.Value;
                        decimal valorIR = pos.ValorIR.Value;
                        decimal saldoLiquido = pos.ValorLiquido.Value;
                        decimal valorResgateTotal = 0;

                        OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
                        operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira) &
                                                        operacaoCotistaColl.Query.IdCotista.Equal(idCotista) &
                                                        operacaoCotistaColl.Query.DataConversao.GreaterThanOrEqual(data) &
                                                        operacaoCotistaColl.Query.TipoOperacao.In(lstTipoOperacao));
                        operacaoCotistaColl.Query.OrderBy(operacaoCotistaColl.Query.DataConversao.Ascending,
                                                          operacaoCotistaColl.Query.IdOperacao.Ascending);

                        if (!operacaoCotistaColl.Query.Load())
                            continue;

                        Cotista cotista = new Cotista();
                        cotista.LoadByPrimaryKey(idCotista);

                        bool truncaFinanceiro = carteira.TruncaCota.Equals("S");
                        bool isentoIR = cotista.IsIsentoIR();
                        bool isentoIOF = cotista.IsIsentoIOF();
                        bool tributaNaoResidente = cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente;
                        int tipoCota = carteira.TipoCota.Value;
                        int tipoTributacao = carteira.TipoTributacao.Value;

                        bool exclusaoLogica = false;
                        foreach (OperacaoCotista operacaoCotista in operacaoCotistaColl)
                        {
                            if (operacaoCotista.TipoOperacao.Value == (int)TipoOperacaoCotista.ResgateLiquido)
                            {
                                decimal valorLiquidoPosicao = operacaoCotista.ValorLiquido.Value;                                
                                valorResgateTotal += valorLiquidoPosicao;

                                if (!isentoIR)
                                {
                                    CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                                    calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                                    calculoIRFundoRendaFixa.TipoCota = tipoCota;
                                    calculoIRFundoRendaFixa.DataAplicacao = operacaoCotista.DataOperacao.Value;
                                    calculoIRFundoRendaFixa.DataCalculo = data;
                                    calculoIRFundoRendaFixa.TipoProcessamentoResgate = tipoProcessaResgate;
                                    calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;

                                    CalculoTributo calculoTributo = new CalculoTributo();

                                    calculoTributo.CalculaIRFundoRendaFixaLiquido(calculoIRFundoRendaFixa, 0, valorIR, saldoLiquido, valorLiquidoPosicao);
                                    valorResgateTotal += calculoTributo.IR;
                                }
                            }
                            else if (operacaoCotista.TipoOperacao.Value == (int)TipoOperacaoCotista.ResgateBruto)
                                valorResgateTotal += operacaoCotista.ValorBruto.Value;
                            else
                            {
                                decimal quantidade = operacaoCotista.Quantidade.Value;

                                valorResgateTotal += truncaFinanceiro ? Utilitario.Truncate(quantidade * cotaDia, 2) : Math.Round(quantidade * cotaDia, 2);                            

                            }

                            if (carteira.ValorMinimoSaldo.Value > (saldoCotista - valorResgateTotal))
                            {
                                if (!exclusaoLogica) // a partir da segunda operação, as operações são descartadas (não fisicamente do BD)
                                {
                                    string mensagem = String.Format("Fundo: {0} - Resgate Parcial de {1} convertido para Resgate Total por atingir o saldo mínimo cadastrado para o fundo de {2}.", carteira.Apelido, saldoCotista.ToString("###,##0.00"), carteira.ValorMinimoSaldo.Value.ToString("###,##0.00"));
                                    exclusaoLogica = true;

                                    operacaoCotista.Observacao = mensagem;
                                    operacaoCotista.TipoOperacao = (int)TipoOperacaoFundo.ResgateTotal;

                                    //Grava log
                                    HistoricoLog historicoLog = new HistoricoLog();
                                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                                    DateTime.Now,
                                                                    mensagem,
                                                                    "admin",
                                                                    "",
                                                                    "",
                                                                    Financial.Security.Enums.HistoricoLogOrigem.Processamento);
                                }
                                else
                                {
                                    string mensagem = String.Format("Fundo: {0} - Resgate Parcial de {1} foi excluido logicamente por atingir o saldo mínimo cadastrado para o fundo de {2}.", carteira.Apelido, saldoCotista.ToString("###,##0.00"), carteira.ValorMinimoSaldo.Value.ToString("###,##0.00"));

                                    operacaoCotista.ExclusaoLogica = "S";
                                    operacaoCotista.Observacao = mensagem;
                                    operacaoCotista.ValorBruto = operacaoCotista.ValorLiquido = operacaoCotista.Quantidade = operacaoCotista.CotaInformada = operacaoCotista.CotaOperacao = 0;

                                    //Grava log
                                    HistoricoLog historicoLog = new HistoricoLog();
                                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                                    DateTime.Now,
                                                                    mensagem,
                                                                    "admin",
                                                                    "",
                                                                    "",
                                                                    Financial.Security.Enums.HistoricoLogOrigem.Processamento);
                                }
                            }
                        }

                        operacaoCotistaColl.Save();
                    }
                }
            }
            #endregion

            #region
            if (tipoProcessaResgate == TipoProcessaResgate.OperacaoFundo)
            {
                PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
                posicaoFundoColl.Query.Select(posicaoFundoColl.Query.IdCarteira,
                                              posicaoFundoColl.Query.ValorBruto.Sum(),
                                              posicaoFundoColl.Query.ValorIR.Sum(),
                                              posicaoFundoColl.Query.ValorLiquido.Sum());
                posicaoFundoColl.Query.Where(posicaoFundoColl.Query.IdCliente.Equal(idCliente));
                posicaoFundoColl.Query.GroupBy(posicaoFundoColl.Query.IdCarteira);

                if (posicaoFundoColl.Query.Load())
                    return;

                List<int> lstTipoOperacao = new List<int>();
                lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateBruto);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotas);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotasEspecial);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateLiquido);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateLiquido);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
                lstTipoOperacao.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);

                foreach (PosicaoFundo pos in posicaoFundoColl)
                {
                    int idCarteira = pos.IdCarteira.Value;
                    decimal saldoBruto = pos.ValorBruto.Value;
                    decimal saldoLiquido = pos.ValorLiquido.Value;
                    decimal valorIR = pos.ValorIR.Value;
                    decimal valorResgateTotal = 0;

                    OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
                    operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCarteira.Equal(idCarteira) &
                                                  operacaoFundoColl.Query.IdCliente.Equal(idCliente) &
                                                  operacaoFundoColl.Query.DataConversao.GreaterThanOrEqual(data) &
                                                  operacaoFundoColl.Query.TipoOperacao.In(lstTipoOperacao));
                    operacaoFundoColl.Query.OrderBy(operacaoFundoColl.Query.DataConversao.Ascending,
                                                   operacaoFundoColl.Query.IdOperacao.Ascending);

                    if (!operacaoFundoColl.Query.Load())
                        continue;

                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(idCarteira);

                    if (!carteira.PossuiResgateAutomatico.Equals("S"))
                        continue;

                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCliente);

                    bool truncaFinanceiro = carteira.TruncaCota.Equals("S");
                    bool isentoIR = cliente.IsIsentoIR();
                    bool isentoIOF = cliente.IsIsentoIOF();
                    bool tributaNaoResidente = cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;
                    int tipoCota = carteira.TipoCota.Value;
                    int tipoTributacao = carteira.TipoTributacao.Value;

                    bool exclusaoLogica = false;
                    foreach (OperacaoFundo operacaoFundo in operacaoFundoColl)
                    {
                        if (operacaoFundo.TipoOperacao.Value == (int)TipoOperacaoFundo.ResgateLiquido)
                        {
                            decimal valorLiquidoPosicao = operacaoFundo.ValorLiquido.Value;
                            valorResgateTotal += valorLiquidoPosicao;
                            
                            if (!isentoIR)
                            {
                                CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                                calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                                calculoIRFundoRendaFixa.TipoCota = tipoCota;
                                calculoIRFundoRendaFixa.DataAplicacao = operacaoFundo.DataOperacao.Value;
                                calculoIRFundoRendaFixa.DataCalculo = data;
                                calculoIRFundoRendaFixa.TipoProcessamentoResgate = tipoProcessaResgate;
                                calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;

                                CalculoTributo calculoTributo = new CalculoTributo();

                                calculoTributo.CalculaIRFundoRendaFixaLiquido(calculoIRFundoRendaFixa, 0, valorIR, saldoLiquido, valorLiquidoPosicao);
                                valorResgateTotal += calculoTributo.IR;
                            }
                        }
                        else if (operacaoFundo.TipoOperacao.Value == (int)TipoOperacaoFundo.ResgateBruto)
                            valorResgateTotal += operacaoFundo.ValorBruto.Value;
                        else
                        {
                            HistoricoCota historicoCota = new HistoricoCota();
                            historicoCota.BuscaValorCota(idCarteira, data);

                            decimal cotaDia = carteira.TipoCota == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;
                            decimal quantidade = operacaoFundo.Quantidade.Value;

                            valorResgateTotal += truncaFinanceiro ? Utilitario.Truncate(quantidade * cotaDia, 2): Math.Round(quantidade * cotaDia, 2);                            
                        }

                        if (carteira.ValorMinimoSaldo.Value > (saldoBruto - valorResgateTotal))
                        {
                            if (!exclusaoLogica)
                            {
                                string mensagem = String.Format("Fundo: {0} - Resgate Parcial de {1} convertido para Resgate Total por atingir o saldo mínimo cadastrado para o fundo de {2}.", carteira.Apelido, saldoBruto.ToString("###,##0.00"), carteira.ValorMinimoSaldo.Value.ToString("###,##0.00"));

                                exclusaoLogica = true;
                                operacaoFundo.Observacao = mensagem;
                                operacaoFundo.TipoOperacao = (int)TipoOperacaoFundo.ResgateTotal;

                                //Grava log
                                HistoricoLog historicoLog = new HistoricoLog();
                                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                                DateTime.Now,
                                                                mensagem,
                                                                "admin",
                                                                "",
                                                                "",
                                                                Financial.Security.Enums.HistoricoLogOrigem.Processamento);
                            }
                            else
                            {
                                string mensagem = String.Format("Fundo: {0} - Resgate Parcial de {1} foi excluido logicamente por atingir o saldo mínimo cadastrado para o fundo de {2}.", carteira.Apelido, saldoBruto.ToString("###,##0.00"), carteira.ValorMinimoSaldo.Value.ToString("###,##0.00"));

                                operacaoFundo.ExclusaoLogica = "S";
                                operacaoFundo.Observacao = mensagem;
                                operacaoFundo.ValorBruto = operacaoFundo.ValorLiquido = operacaoFundo.Quantidade = operacaoFundo.CotaInformada = operacaoFundo.CotaOperacao = 0;

                                //Grava log
                                HistoricoLog historicoLog = new HistoricoLog();
                                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                                DateTime.Now,
                                                                mensagem,
                                                                "admin",
                                                                "",
                                                                "",
                                                                Financial.Security.Enums.HistoricoLogOrigem.Processamento);
                            }
                        }
                    }

                    operacaoFundoColl.Save();
                }
            }
            #endregion
        }
    }
}