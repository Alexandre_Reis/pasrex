using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista.Debug.OperacaoCotista
{
    public class DebugProcessaResgate
	{
        public int? IdCarteira;
        public DebugProcessaResgateCotas DebugProcessaResgateCotas;

        public DebugProcessaResgate()
        {
            
        }

        public string GetFormattedCSV(){
            StringBuilder csv = new StringBuilder();
            if (this.DebugProcessaResgateCotas.PosicoesResgatas.Count > 0)
            {
                csv.Append("Processa Resgate Cotas: ");
                csv.AppendLine();
                foreach (DebugProcessaResgateCotasPosicaoResgatada posicaoResgatada in
                    this.DebugProcessaResgateCotas.PosicoesResgatas)
                {
                    csv.Append(posicaoResgatada.IdPosicao.Value);
                    csv.AppendLine();
                }
            }
            return csv.ToString();
        }
	}

    public class DebugProcessaResgateCotas
    {
        /*diasLiquidacaoResgate
            diasLiquidacaoComeCotas*/

        public List<DebugProcessaResgateCotasPosicaoResgatada> PosicoesResgatas;
        public DebugProcessaResgateCotas()
        {
            this.PosicoesResgatas = new List<DebugProcessaResgateCotasPosicaoResgatada>();
        }
    }

    public class DebugProcessaResgateCotasPosicaoResgatada
    {
        public int? IdPosicao;
        /*
       
        decimal quantidadePosicao
            decimal quantidadeAntesCortes
                DateTime dataAplicacao
                     DateTime dataConversao
                         decimal cotaAplicacao
                             diasTotal
                                 aliquotaDiferenciada 
                                 decimal quantidadeBruta;
        decimal quantidadeBaixa;
        decimal prejuizoCompensar = 0;
        bool isentoIR
            bool isentoIOF
                valorIOF
                    prejuizoCompensar
                    tipoTributacao

                        decimal quantidadeResgate = 0;
            decimal valorBrutoResgate = 0;
            decimal valorLiquidoResgate = 0;
            decimal valorIRResgate = 0;
            decimal valorIOFResgate = 0;
            decimal variacaoResgate = 0;
            decimal rendimentoCompensadoResgate = 0;
            decimal prejuizoUsadoResgate = 0;
            decimal valorPerformanceResgate = 0;

        valorIR = calculoTributo.IR;
                        rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                        rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                        prejuizoUsado = calculoTributo.PrejuizoUsado;

        valorIR = calculoTributo.IR;
                        valorIOF = calculoTributo.IOF;
                        rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                        rendimentoCompensado = calculoTributo.RendimentoAnteriorCompensado + calculoTributo.RendimentoPosteriorCompensado;
                        prejuizoUsado = calculoTributo.PrejuizoUsado;
        */

    }
}
