using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Tributo.Debug.CalculoTributo;
using Financial.Fundo.Debug.HistoricoCota;

namespace Financial.InvestidorCotista.Debug.PosicaoCotista
{
    public class DebugAtualizaProjecaoTributoRendaFixa
	{
        public int? IdCarteira;
        public int? IdPosicao;
        public int? IdCotista;
        public byte? TipoTributacao;
        public bool? AliquotaDiferenciada;
        public byte? TipoCota;
        public bool? CalculaIOF;
        public bool? IsIsentoIR;
        public bool? IsIsentoIOF;
        public DateTime? DataAplicacao;
        public DateTime? DataConversao;
        public int? DiasTotal;
        public int? DiasLiquidResgate;
        public int? DiasLiquidComeCotas;
        public decimal? CotaDia;
        public decimal? CotaAplicacao;
        public decimal? CotaDiaCalculo;
        public decimal? CotaAplicacaoCalculo;
        public decimal? QuantidadeAntesCorte;
        public decimal? QuantidadePosicao;
        public decimal? QuantidadeCalcular;
        public decimal? ValorRendimentoComeCotas;
        public decimal? IRDescontarFDIC=0;
        public decimal? ValorBruto;
        public decimal? ValorIR;
        public decimal? ValorIOF;
        public decimal? ValorLiquido;

        public DebugCalculaIRFundoRendaFixa DebugCalculaIRFundoRendaFixa;
        public List<DebugCotaRecomposta> DebugCotaAplicacaoRecompostaList;
        public List<DebugCotaRecomposta> DebugCotaDiaRecompostaList;

        public DebugAtualizaProjecaoTributoRendaFixa()
        {
            this.DebugCalculaIRFundoRendaFixa = new DebugCalculaIRFundoRendaFixa();
            this.DebugCotaAplicacaoRecompostaList = new List<DebugCotaRecomposta>();
            this.DebugCotaDiaRecompostaList = new List<DebugCotaRecomposta>();
        }
	}
}
