﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Exceptions;
using Financial.InvestidorCotista.Enums;

namespace Financial.InvestidorCotista
{
	public partial class BloqueioCota : esBloqueioCota
	{
        /// <summary>
        /// Processa bloqueios de cota feitos na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaBloqueioCota(int idCarteira, DateTime data)
        {
            BloqueioCotaQuery bloqueioCotaQuery = new BloqueioCotaQuery("B");
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
            bloqueioCotaQuery.Select(bloqueioCotaQuery.IdPosicao,
                                     bloqueioCotaQuery.Quantidade,
                                     bloqueioCotaQuery.TipoOperacao);
            bloqueioCotaQuery.InnerJoin(posicaoCotistaQuery).On(posicaoCotistaQuery.IdPosicao == bloqueioCotaQuery.IdPosicao);
            bloqueioCotaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                                    bloqueioCotaQuery.Data.Equal(data));
            BloqueioCotaCollection bloqueioCotaCollection = new BloqueioCotaCollection();
            bloqueioCotaCollection.Load(bloqueioCotaQuery);

            for (int i = 0; i < bloqueioCotaCollection.Count; i++)
            {
                BloqueioCota bloqueioCota = bloqueioCotaCollection[i];
                int idPosicao = bloqueioCota.IdPosicao.Value;
                decimal quantidade = bloqueioCota.Quantidade.Value;
                byte tipoOperacao = bloqueioCota.TipoOperacao.Value;

                PosicaoCotista posicaoCotista = new PosicaoCotista();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(posicaoCotista.Query.IdPosicao);
                campos.Add(posicaoCotista.Query.Quantidade);
                campos.Add(posicaoCotista.Query.QuantidadeBloqueada);
                if (!posicaoCotista.LoadByPrimaryKey(campos, idPosicao))
                {
                    throw new PosicaoCotistaNaoCadastradoException("A posiçao a ser bloqueada/desbloqueada perdeu o vínculo original! Carteira precisa ser retroagida para a data original do bloqueio/desbloqueio.");
                }

                decimal quantidadeTotal = posicaoCotista.Quantidade.Value;

                if (tipoOperacao == (byte)TipoOperacaoBloqueio.Bloqueio)
                {
                    decimal quantidadeBloqueadaFinal = posicaoCotista.QuantidadeBloqueada.Value + quantidade;
                    if (quantidadeBloqueadaFinal > quantidadeTotal)
                    {
                        quantidadeBloqueadaFinal = quantidadeTotal;                        
                    }
                    posicaoCotista.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                    
                }
                else if (tipoOperacao == (byte)TipoOperacaoBloqueio.Desbloqueio)
                {
                    decimal quantidadeBloqueadaFinal = posicaoCotista.QuantidadeBloqueada.Value - quantidade;
                    if (quantidadeBloqueadaFinal < 0)
                    {
                        quantidadeBloqueadaFinal = 0;                    
                    }

                    posicaoCotista.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                }
                posicaoCotista.Save();
            }
        }

        /// <summary>
        /// Processa desbloqueio automático de cotas, para bloqueios feitos com liberação automática.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaDesbloqueioAutomatico(int idCarteira, DateTime data)
        {
            BloqueioCotaQuery bloqueioCotaQuery = new BloqueioCotaQuery("B");
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
            bloqueioCotaQuery.Select(bloqueioCotaQuery.IdPosicao);
            bloqueioCotaQuery.InnerJoin(posicaoCotistaQuery).On(bloqueioCotaQuery.IdPosicao == posicaoCotistaQuery.IdPosicao);
            bloqueioCotaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                                    bloqueioCotaQuery.TipoOperacao.Equal((byte)TipoOperacaoBloqueio.Bloqueio),
                                    bloqueioCotaQuery.TipoLiberacao.Equal((byte)TipoLiberacaoBloqueio.Automatico),
                                    bloqueioCotaQuery.DataFim.Equal(data));

            BloqueioCotaCollection bloqueioCotaCollection = new BloqueioCotaCollection();
            bloqueioCotaCollection.Load(bloqueioCotaQuery);

            foreach (BloqueioCota bloqueioCota in bloqueioCotaCollection)
            {
                int idPosicao = bloqueioCota.IdPosicao.Value;

                PosicaoCotista posicaoCotista = new PosicaoCotista();
                if (posicaoCotista.LoadByPrimaryKey(idPosicao))
                {
                    posicaoCotista.QuantidadeBloqueada = 0;
                    posicaoCotista.Save();
                }                
            }
        }
	}
}
