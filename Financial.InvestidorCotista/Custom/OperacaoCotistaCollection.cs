﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using log4net;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
    public partial class OperacaoCotistaCollection : esOperacaoCotistaCollection
    {
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoCotistaCollection.
        /// Filtra por:
        /// TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
        ///                 (int)TipoOperacaoCotista.AplicacaoAcoesEspecial,
        ///                 (int)TipoOperacaoCotista.AplicacaoCotasEspecial)
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataConversao"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaAplicacao(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                   (int)TipoOperacaoCotista.AplicacaoAcoesEspecial,
                                                   (int)TipoOperacaoCotista.AplicacaoCotasEspecial,
                                                   (int)TipoOperacaoCotista.AplicacaoCotas,
                                                   (int)TipoOperacaoCotista.Deposito));
            this.Query.Where((this.Query.DataConversao.Equal(dataConversao) & this.Query.DataConversao.GreaterThanOrEqual(this.Query.DataRegistro))
                | (this.Query.DataRegistro.Equal(dataConversao) & this.Query.DataConversao.LessThanOrEqual(this.Query.DataRegistro)));
            return this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com o campo ValorBruto.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// OBS: Possível de ser estendido para algum agrupamento futuro. Por isso está na Collection.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataLiquidacao"></param>        
        public void BuscaAplicacaoLiquidaDia(int idCarteira, DateTime dataLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.DataLiquidacao.Equal(dataLiquidacao),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                    (int)TipoOperacaoCotista.AplicacaoAcoesEspecial));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com o campo ValorBruto, Observacao, DataLiquidacao, IdConta.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// OBS: Possível de ser estendido para algum agrupamento futuro. Por isso está na Collection.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataLiquidacao"></param>        
        public void BuscaAplicacaoLiquidaDiaAberto(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto, this.Query.Observacao, this.Query.DataLiquidacao, this.Query.IdConta)
                 .Where(this.Query.DataOperacao.Equal(data),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                    (int)TipoOperacaoCotista.AplicacaoAcoesEspecial));

            this.Query.Load();
        }

        public void BuscaAplicacaoLiquidar(int idCarteira, DateTime dataLiquidacao)
        {
            this.QueryReset();
            this.Query.Select(this.Query.ValorBruto.Sum(),
                              this.Query.IdConta);
            this.Query.Where(this.Query.DataLiquidacao.Equal(dataLiquidacao),
                             this.Query.IdCarteira == idCarteira,
                             this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao));
            this.Query.GroupBy(this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com o campo ValorBruto.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// Filtra por: DataConversao.GreaterThan(dataConversao),
        /// DataOperacao.Equal(data).
        /// GroupBy(this.Query.DataConversao)
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        public void BuscaAplicacaoConverter(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.DataConversao, this.Query.ValorBruto.Sum())
                 .Where(this.Query.DataConversao.GreaterThanOrEqual(data),
                        this.Query.DataOperacao.Equal(data),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                   (int)TipoOperacaoCotista.AplicacaoAcoesEspecial))
                .GroupBy(this.Query.DataConversao);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com o campo ValorBruto.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// GroupBy(this.Query.DataConversao)
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>        
        public void BuscaAplicacaoConverterDia(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.DataConversao, this.Query.ValorBruto.Sum())
                 .Where(this.Query.DataConversao.Equal(dataConversao),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.Aplicacao,
                                                   (int)TipoOperacaoCotista.AplicacaoAcoesEspecial))
            .GroupBy(this.Query.DataConversao);

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as operações de ComeCotas da carteira na data.        
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void DeletaComeCotas(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataOperacao.Equal(data),
                        this.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ComeCotas));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com os campos ValorLiquido.Sum(), ValorIR.Sum(), ValorIOF.Sum(),
        /// ValorPerformance.Sum(), DataLiquidacao.
        /// Busca operações com o TipoOperacao = "ResgateBruto", "ResgateLiquido", "ResgateCotas", "ResgateTotal".
        /// OBS: Possível de ser estendido para algum agrupamento futuro. Por isso está na Collection.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>        
        public void BuscaResgateProjetaLiquidacao(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum(), this.Query.ValorIR.Sum(), this.Query.ValorIOF.Sum(), this.Query.Quantidade.Sum(),
                         this.Query.ValorPerformance.Sum(), this.Query.PenalidadeLockUp.Sum(), this.Query.DataLiquidacao)
                 .Where(this.Query.DataConversao.Equal(dataConversao),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.ResgateBruto,
                                                   (int)TipoOperacaoCotista.ResgateLiquido,
                                                   (int)TipoOperacaoCotista.ResgateCotas,
                                                   (int)TipoOperacaoCotista.ResgateTotal))
                .GroupBy(this.Query.DataLiquidacao, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com os campos ValorLiquido, DataLiquidacao, Observacao.
        /// Busca operações com o TipoOperacao = "ResgateBruto", "ResgateLiquido", "ResgateCotas", "ResgateTotal".
        /// OBS: Possível de ser estendido para algum agrupamento futuro. Por isso está na Collection.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataConversao"></param>        
        public void BuscaResgateProjetaLiquidacaoAberto(int idCarteira, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido, this.Query.Quantidade, this.Query.ValorIR, this.Query.ValorIOF, this.Query.ValorPerformance,
                         this.Query.DataLiquidacao, this.Query.Observacao, this.Query.IdConta, this.Query.IdLocalNegociacao, this.Query.IdOperacao)
                 .Where(this.Query.DataConversao.Equal(dataConversao),
                        this.Query.IdCarteira == idCarteira,
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.ResgateBruto,
                                                   (int)TipoOperacaoCotista.ResgateLiquido,
                                                   (int)TipoOperacaoCotista.ResgateCotas,
                                                   (int)TipoOperacaoCotista.ResgateTotal));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoCotistaCollection com todos os campos de OperacaoCotista.
        /// Filtra por:
        /// TipoOperacao.In(TipoOperacaoCotista.ResgateBruto, 
        ///                 TipoOperacaoCotista.ResgateCotas,
        ///                 TipoOperacaoCotista.ResgateCotasEspecial,
        ///                 TipoOperacaoCotista.ResgateLiquido,
        ///                 TipoOperacaoCotista.ResgateTotal)
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataConversao"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaResgate(int idCarteira, DateTime dataCalculo)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.ExclusaoLogica.Equal("N"),
                        this.Query.TipoOperacao.In((int)TipoOperacaoCotista.ResgateBruto,
                                                   (int)TipoOperacaoCotista.ResgateCotas,
                                                   (int)TipoOperacaoCotista.ResgateCotasEspecial,
                                                   (int)TipoOperacaoCotista.ResgateLiquido,
                                                   (int)TipoOperacaoCotista.ResgateTotal,
                                                   (int)TipoOperacaoCotista.Retirada));

            this.Query.Where((this.Query.DataConversao.Equal(dataCalculo) & this.Query.DataConversao.GreaterThanOrEqual(this.Query.DataRegistro)) |
                (this.Query.DataRegistro.Equal(dataCalculo) & this.Query.DataConversao.LessThanOrEqual(this.Query.DataRegistro)));

            bool retorno = this.Query.Load();

            return retorno;
        }

        public MappingOperacaoFinancialYMF ImportaMovimentoYMF(Financial.Interfaces.Import.YMF.MovimentoYMF[] movimentos)
        {
            StringBuilder sql = new StringBuilder();
            MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF = new MappingOperacaoFinancialYMF();

            foreach (Financial.Interfaces.Import.YMF.MovimentoYMF movimento in movimentos)
            {
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                if (operacaoCotista.ImportaMovimentoYMF(movimento, false))
                {
                    //Inserir novo IdOperacao do Financial na tabela de mapping com Id antigo da Operacao na YMF
                    mappingOperacaoFinancialYMF.Add(movimento.IdNota, operacaoCotista.IdOperacao.Value);
                }
            }

            return mappingOperacaoFinancialYMF;

        }

        /*public void ImportaMovimentoAnaliticoYMF(Financial.Interfaces.Import.Fundo.MovimentoAnaliticoYMF[] movimentos, bool comCalculo)
        {
            const string TABLE_NAME = "OperacaoCotista";

            StringBuilder sql = new StringBuilder();

            sql.AppendLine(String.Format("SET IDENTITY_INSERT [{0}] ON", TABLE_NAME));
            sql.AppendLine("SET NOCOUNT ON");

            List<string> listColumnNames = new List<string>();
            List<string> listColumnParameters = new List<string>();
            foreach (esColumnMetadata col in this.Meta.Columns)
            {
                listColumnNames.Add(col.Name);
                listColumnParameters.Add("@" + col.Name);
            }

            string columnNames = String.Join(",", listColumnNames.ToArray());
            string columnParameters = String.Join(",", listColumnParameters.ToArray());

            sql.AppendLine("DELETE FROM " + TABLE_NAME + " WHERE IdOperacao = {0}");

            sql.AppendLine(String.Format("INSERT INTO {0}({1}) VALUES({2})", TABLE_NAME, columnNames, columnParameters));

            sql.AppendLine(String.Format("SET IDENTITY_INSERT [{0}] OFF", TABLE_NAME));

            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    foreach (Financial.Interfaces.Import.Fundo.MovimentoAnaliticoYMF movimento in movimentos)
                    {
                        OperacaoCotista operacaoCotista = new OperacaoCotista();
                        if (operacaoCotista.ImportaMovimentoAnaliticoYMF(movimento, true, comCalculo))
                        {
                            esParameters esParams = new esParameters();

                            foreach (string columnName in listColumnNames)
                            {
                                esParams.Add(columnName, operacaoCotista.GetColumn(columnName));
                            }

                            //Adicionar clausula de DELETE ao SQL Fixo
                            string sqlDynamic = String.Format(sql.ToString(), operacaoCotista.IdOperacao.Value);

                            esUtility u = new esUtility();
                            u.ExecuteNonQuery(esQueryType.Text, sqlDynamic, esParams);
                        }
                    }
                    scope.Complete();
                }
            }

        }
        */
    }
}
