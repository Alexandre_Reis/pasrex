﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
	public partial class PosicaoCotistaHistorico : esPosicaoCotistaHistorico
	{
        /// <summary>
        /// Retorna a Soma do Valor Liquido dado o idCarteira e uma data
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaSumValorLiquido(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCarteira == idCarteira &
                        this.Query.DataHistorico == data &
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira e uma data
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaSumValorBruto(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira &
                        this.Query.DataHistorico == data &
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de quantidade de cotas do cotista na carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="idCotista"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaTotalCotas(int idCarteira, int idCotista, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.IdCotista.Equal(idCotista),
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o total de quantidade de cotas de todos os cotistas da carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaTotalCotas(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o número de cotistas com posição (não zerada) na carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>    
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public int RetornaCountNumeroCotistas(int idCarteira, DateTime dataHistorico)
        {
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("p");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoSubQuery = new PosicaoCotistaHistoricoQuery("p1");

            string column = "<count(distinct sub." + PosicaoCotistaHistoricoMetadata.ColumnNames.IdCotista + ") as IdCotista>";

            posicaoCotistaHistoricoQuery.Select("" + column + "");
            //
            posicaoCotistaHistoricoQuery.From
                (
                    posicaoCotistaHistoricoSubQuery.Select(posicaoCotistaHistoricoSubQuery.IdCarteira,
                                                           posicaoCotistaHistoricoSubQuery.IdCotista,
                                                           (posicaoCotistaHistoricoSubQuery.Quantidade + posicaoCotistaHistoricoSubQuery.QuantidadeBloqueada).As("Quantidade"))
                                          .Where(posicaoCotistaHistoricoSubQuery.IdCarteira == idCarteira &
                                                 posicaoCotistaHistoricoSubQuery.DataHistorico == dataHistorico &
                                                 posicaoCotistaHistoricoSubQuery.Quantidade != 0)
                ).As("sub");

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            return (posicaoCotistaHistoricoCollection != null && posicaoCotistaHistoricoCollection[0].IdCotista.HasValue) ? posicaoCotistaHistoricoCollection[0].IdCotista.Value : 0;            
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaHistorico com os campos CotaDia, QuantidadeInicial, QuantidadeCotas.
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="dataHistorico"></param>
        public bool BuscaPosicaoHistoricoPorNota(int idPosicao, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CotaDia, this.Query.Quantidade, this.Query.QuantidadeInicial)
                 .Where(this.Query.IdPosicao == idPosicao,
                        this.Query.DataHistorico.Equal(dataHistorico));

            bool retorno = this.Query.Load();

            return retorno;

        }

        public bool ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota, 
            DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF)
        {
            //Estas são as diferenças para o ImportaPosicaoNotaYMF no PosicaoCotista:
            this.DataHistorico = posicaoNota.DtPosicao;
            if (mappingPosicaoFinancialYMF.ContainsKey(posicaoNota.IdNota))
            {
                this.IdPosicao = mappingPosicaoFinancialYMF[posicaoNota.IdNota];
            }
            else
            {
                //Trata-se de uma posição que não importamos (provavelmente posição pós data de implantação)
                return false;
            }

            //O restante do código é idêntico:
            if (posicaoNota.QtCotas == 0)
            {
                return false;
            }

            if (posicaoNota.DtPosicao != dataImplantacao)
            {
                //Só queremos importar posições cujas datas correspondam à data de implantação do fundo
                return false;
            }

            if (mappingOperacaoFinancialYMF.ContainsKey(posicaoNota.IdNota))
            {
                //Vamos tentar encontrar na tabela de operações uma operação com o IdNota desta posição. Na YMF IdOperacao e IdPosicao caminham juntos como IdNota
                this.IdOperacao = mappingOperacaoFinancialYMF[posicaoNota.IdNota];
            }

            this.MapeiaCamposYMFFinancial(posicaoNota);

            return true;
        }

        private void MapeiaCamposYMFFinancial(PosicaoNotaYMF posicaoNota)
        {
            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(posicaoNota.CdCotista.Trim());
            this.IdCotista = cotista.IdCotista;

            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(posicaoNota.CdFundo.Trim());
            this.IdCarteira = cliente.IdCliente;
            this.DataAplicacao = posicaoNota.DtAplicacao;
            this.ValorAplicacao = posicaoNota.VlAplicacao;
            this.CotaAplicacao = posicaoNota.VlCotaAplicacao;

            this.Quantidade = posicaoNota.QtCotas;
            this.ValorBruto = posicaoNota.VlBruto;
            this.ValorIR = posicaoNota.VlIr;
            this.ValorIOF = posicaoNota.VlIof;
            this.ValorPerformance = posicaoNota.VlPerformance;
            this.ValorIOFVirtual = posicaoNota.VlIofVirtual;

            this.DataConversao = this.DataAplicacao;
            this.CotaDia = this.ValorBruto / this.Quantidade;
            this.ValorLiquido = this.ValorBruto - this.ValorIR - this.ValorIOF - this.ValorPerformance;
            this.QuantidadeInicial = this.Quantidade;
            this.QuantidadeBloqueada = 0;
            this.QuantidadeAntesCortes = this.Quantidade;
            this.PosicaoIncorporada = "N";
            this.ValorRendimento = 0;
            this.DataUltimaCobrancaIR = posicaoNota.DtUltimoResgateIr.HasValue ? posicaoNota.DtUltimoResgateIr : cliente.DataDia;
            this.DataUltimoCortePfee = posicaoNota.DtInicioPerformance;
        }

        public bool ImportaPosicaoNotaHistoricaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota)
        {
            this.DataHistorico = posicaoNota.DtPosicao;
            this.IdPosicao = posicaoNota.IdNota;

            if (posicaoNota.QtCotas == 0)
            {
                return false;
            }

            this.MapeiaCamposYMFFinancial(posicaoNota);
            return true;
        }

	}
}
