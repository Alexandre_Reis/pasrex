﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.InvestidorCotista.Exceptions;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Tributo.Custom;
using Financial.Util;
using Financial.Common.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.ContaCorrente.Enums;
using System.IO;
using Financial.Interfaces.Import.Cotista;
using System.Globalization;
using Financial.Investidor.Exceptions;
using Financial.Investidor;
using Financial.Interfaces.Import.Cotista.Exceptions;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.YMF;
using Financial.InvestidorCotista.Debug.PosicaoCotista;
using Financial.Fundo.Debug.HistoricoCota;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Util.Enums;

namespace Financial.InvestidorCotista
{
    public partial class PosicaoCotista : esPosicaoCotista
    {
        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCarteira, DateTime data)
        {
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollectionDeletar = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollectionDeletar.Query.Select(posicaoCotistaHistoricoCollectionDeletar.Query.IdPosicao,
                                                                  posicaoCotistaHistoricoCollectionDeletar.Query.DataHistorico);
            posicaoCotistaHistoricoCollectionDeletar.Query.Where(posicaoCotistaHistoricoCollectionDeletar.Query.IdCarteira.Equal(idCarteira),
                                                                 posicaoCotistaHistoricoCollectionDeletar.Query.DataHistorico.Equal(data));
            posicaoCotistaHistoricoCollectionDeletar.Query.Load();
            posicaoCotistaHistoricoCollectionDeletar.MarkAllAsDeleted();
            posicaoCotistaHistoricoCollectionDeletar.Save();
            
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoCotistaHistorico (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoCotista WHERE IdCarteira = " + idCarteira;
            int count = 0;

            PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
            int columnCount = posicaoCotistaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoCotista in posicaoCotistaHistorico.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoCotista.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoCotista.Name == PosicaoCotistaHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoCotista.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCarteira, DateTime data)
        {
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoCotistaAbertura (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoCotista WHERE IdCarteira = " + idCarteira;
            int count = 0;

            PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
            int columnCount = posicaoCotistaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoCotista in posicaoCotistaAbertura.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoCotista.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoCotista.Name == PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoCotista.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Atualiza a cota (abertura ou fechamento) de todas as posições de cotistas da carteira passada.
        /// Atualiza o saldo bruto de todas as posições de cotistas.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void AtualizaValoresPosicao(int idCarteira, DateTime data)
        {
            #region Busca tipo de cota e parâmetro de truncagem de financeiro
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            int tipoCota = carteira.TipoCota.Value;
            bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
            #endregion

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaCotaDia(idCarteira);

            if (posicaoCotistaCollection.Count > 0)
            {
                decimal cotaDia;

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCotaDia(idCarteira, data);

                //Busca valor da cota no dia
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaDia = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaDia = historicoCota.CotaFechamento.Value;
                }


                #region Carrega operações de Aplicação Especial, gerada por transferencia de série
                List<OperacaoCotista> lstOperacaoCotista = new List<OperacaoCotista>();
                OperacaoCotistaCollection operacaoCotistacoll = new OperacaoCotistaCollection();
                operacaoCotistacoll.Query.Where(operacaoCotistacoll.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.AplicacaoCotasEspecial)
                                                & operacaoCotistacoll.Query.DataOperacao.Equal(data)
                                                & operacaoCotistacoll.Query.Fonte.Equal(FonteOperacaoCotista.TransferenciaSerie));

                if (operacaoCotistacoll.Query.Load())
                    lstOperacaoCotista = (List<OperacaoCotista>)operacaoCotistacoll;
                #endregion

                for (int i = 0; i < posicaoCotistaCollection.Count; i++)
                {
                    PosicaoCotista posicaoCotista = (PosicaoCotista)posicaoCotistaCollection[i];

                    //Se a posição foi gerada por uma transferencia de série, no processamento do dia, não atualizar posição
                    if (posicaoCotista.IdOperacao.HasValue)
                    {
                        OperacaoCotista operacaoCotista = lstOperacaoCotista.Find(delegate(OperacaoCotista x) { return x.IdOperacao.Value == posicaoCotista.IdOperacao.Value; });
                        if (operacaoCotista != null && operacaoCotista.IdOperacao.HasValue && operacaoCotista.IdOperacao.Value != 0)
                            continue;
                    }

                    #region Se for carteira offshore, calcula cota do dia
                    int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
                    if (idTipoCliente == (int)TipoClienteFixo.OffShore_PF || idTipoCliente == (int)TipoClienteFixo.OffShore_PJ)
                    {
                        historicoCota.BuscaValorCotaDia(idCarteira, posicaoCotista.DataConversao.Value, data);
                        //Busca valor da cota no dia
                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaDia = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaDia = historicoCota.CotaFechamento.Value;
                        }
                    }
                    #endregion

                    decimal quantidade = posicaoCotista.Quantidade.Value;

                    decimal saldoBruto;
                    if (truncaFinanceiro)
                    {
                        saldoBruto = Utilitario.Truncate(quantidade * cotaDia, 2);
                    }
                    else
                    {
                        saldoBruto = Utilitario.RoundMax(quantidade * cotaDia, 2);
                    }

                    posicaoCotista.ValorBruto = saldoBruto;
                    posicaoCotista.CotaDia = cotaDia;
                }
            }
            //Atualiza a collection
            posicaoCotistaCollection.Save();
        }

        public void LancaLiquidacaoLockUp(int idCarteira, DateTime data)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
            if (idTipoCliente == TipoClienteFixo.OffShore_PF || idTipoCliente == TipoClienteFixo.OffShore_PJ)
            {
                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.PenalidadeLockUp.GreaterThan(0));
                operacaoCotistaCollection.Query.Load();

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                {

                }
            }
        }

        /// <summary>
        /// Atualiza os valores projetados de IR, IOF, ValorLiquido das posições de cotistas.
        /// Não leva em conta o prejuízo a compensar.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void AtualizaProjecaoTributo(int idCarteira, DateTime data)
        {
            //Carrega parâmetros da Carteira
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            //

            #region Busca o valor da cota na data
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idCarteira, data);

            decimal cotaDia;
            if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
            {
                cotaDia = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaDia = historicoCota.CotaFechamento.Value;
            }
            #endregion

            #region Deleta AgendaComeCotas
            AgendaComeCotasCollection agendaComeCotasColl = new AgendaComeCotasCollection();
            agendaComeCotasColl.Query.Where(agendaComeCotasColl.Query.IdCarteira.Equal(idCarteira));

            if (agendaComeCotasColl.Query.Load())
            {
                agendaComeCotasColl.MarkAllAsDeleted();
                agendaComeCotasColl.Save();
            }

            #endregion


            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotista(idCarteira);

            PosicaoCotistaAlterada posicaoCotistaAlterada = new PosicaoCotistaAlterada();
            List<PosicaoCotistaAlterada> lstPosicaoAlterada = posicaoCotistaAlterada.RetornaLista(idCarteira, data, null);

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                if (Carteira.RetornaTipoTributacao(idCarteira, data) == (byte)TipoTributacaoFundo.Acoes)
                {
                    AtualizaProjecaoTributoAcoes(posicaoCotista, carteira, cotaDia, data);
                }
                else
                {
                    DebugAtualizaProjecaoTributoRendaFixa debugInfo;
                    AtualizaProjecaoTributoRendaFixa(posicaoCotista, carteira, cotaDia, data, lstPosicaoAlterada, out debugInfo);
                }
            }

            //Atualiza a collection de posicaoCotistaCollection
            posicaoCotistaCollection.Save();
        }

        /// <summary>
        /// Atualiza os valores projetados de IR, IOF, ValorLiquido das posições de cotistas em fundos/clubes de ações.
        /// Não leva em conta o prejuízo a compensar.
        /// </summary>
        /// <param name="posicaoCotista"></param>  
        /// <param name="carteira"></param>  
        /// <param name="cotaDia"></param>
        /// <param name="data"></param>
        private void AtualizaProjecaoTributoAcoes(PosicaoCotista posicaoCotista,
                                                 Carteira carteira, decimal cotaDia, DateTime data)
        {
            //Inicializa variáveis da carteira
            int idCarteira = carteira.IdCarteira.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            //

            #region Inicializa as variáveis da posição
            int idCotista = posicaoCotista.IdCotista.Value;
            DateTime dataAplicacao = posicaoCotista.DataAplicacao.Value;
            DateTime dataConversao = posicaoCotista.DataConversao.Value;

            decimal cotaAplicacao;
            if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
            {
                cotaAplicacao = this.RetornaCotaMedia(idCarteira, idCotista);
            }
            else
            {
                cotaAplicacao = posicaoCotista.CotaAplicacao.Value;
            }

            decimal quantidade = posicaoCotista.Quantidade.Value;
            #endregion

            CalculoTributo calculoTributo = new CalculoTributo();
            Cotista cotista = new Cotista();
            cotista.BuscaTributacaoCotista(idCotista);

            bool tributaNaoResidente = cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente;

            #region Calcula IR
            decimal valorIR = 0;
            if (!cotista.IsIsentoIR())
            {
                CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                calculoIRFundoAcoes.Quantidade = quantidade;
                calculoIRFundoAcoes.PrejuizoCompensar = 0; // Não usa prejuízo a compensar na projeção
                calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                calculoIRFundoAcoes.CotaCalculo = cotaDia;
                calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                calculoIRFundoAcoes.DataCalculo = data;
                calculoIRFundoAcoes.IdPosicao = posicaoCotista.IdPosicao.Value;
                calculoIRFundoAcoes.DataAplicacao = dataAplicacao;
                calculoIRFundoAcoes.IdCarteira = idCarteira;
                calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);
                valorIR = calculoTributo.IR;
            }
            #endregion

            posicaoCotista.ValorIOF = 0;
            posicaoCotista.ValorIR = valorIR;

            decimal saldoBruto = posicaoCotista.ValorBruto.Value;
            decimal saldoLiquido = saldoBruto - valorIR;
            posicaoCotista.ValorLiquido = saldoLiquido;
        }


        public List<DebugAtualizaProjecaoTributoRendaFixa> DebugAtualizaProjecaoTributoRendaFixa(PosicaoCotista posicaoCotista, DateTime data)
        {
            List<DebugAtualizaProjecaoTributoRendaFixa> debugList = new List<DebugAtualizaProjecaoTributoRendaFixa>();

            int idCarteira = posicaoCotista.IdCarteira.Value;
            int idPosicao = posicaoCotista.IdPosicao.Value;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);
            

            #region Busca o valor da cota na data
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(carteira.IdCarteira.Value, data);

            decimal cotaDia;
            if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
            {
                cotaDia = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaDia = historicoCota.CotaFechamento.Value;
            }
            #endregion

            PosicaoCotistaAlterada posicaoCotistaAlterada = new PosicaoCotistaAlterada();
            List<PosicaoCotistaAlterada> lstPosicaoAlterada = posicaoCotistaAlterada.RetornaLista(idCarteira, data, idPosicao);

            if (carteira.TipoTributacao == (byte)TipoTributacaoFundo.Acoes)
            {
                throw new Exception("Carteira possui tipo de tributação como ações");
            }
            else
            {
                DebugAtualizaProjecaoTributoRendaFixa debugInfo;
                posicaoCotista.AtualizaProjecaoTributoRendaFixa(posicaoCotista, carteira, cotaDia, data, lstPosicaoAlterada, out debugInfo);
                debugList.Add(debugInfo);
            }

            return debugList;
        }

        /// <summary>
        /// Atualiza os valores projetados de IR, IOF, ValorLiquido das posições de cotistas em fundos de RF.
        /// Não leva em conta o prejuízo a compensar.
        /// </summary>
        /// <param name="posicaoCotista"></param>  
        /// <param name="carteira"></param>  
        /// <param name="cotaDia"></param>
        /// <param name="data"></param>
        /// <param name="lstPosicaoCotistaAlterada">Lista com as posições alteradas guardadas em memória</param>
        /// <param name="debugAtualizaProjecaoTributoRendaFixa"></param>

        private void AtualizaProjecaoTributoRendaFixa(PosicaoCotista posicaoCotista,
                                                      Carteira carteira, decimal cotaDia, DateTime data,
                                                      List<PosicaoCotistaAlterada> lstPosicaoCotistaAlterada,
                                                      out DebugAtualizaProjecaoTributoRendaFixa debugInfo)
        {
            debugInfo = new DebugAtualizaProjecaoTributoRendaFixa();

            //Inicializa variáveis da carteira
            int idCarteira = carteira.IdCarteira.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool calculaIOF = carteira.IsCalculaIOF();
            //int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoTributacao = Carteira.RetornaTipoTributacao(idCarteira, data);
            //

            #region Inicializa as variáveis da posição
            int idPosicao = posicaoCotista.IdPosicao.Value;
            int idCotista = posicaoCotista.IdCotista.Value;
            DateTime dataAplicacao = posicaoCotista.DataAplicacao.Value;
            DateTime dataConversao = posicaoCotista.DataConversao.Value;

            //Avalia se deve usar a quantidade antes de cortes ou a quantidade real da posição!
            bool aliquotaDiferenciada = true;
            int diasTotal = Calendario.NumeroDias(dataConversao, data);
            if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo && diasTotal > 180 ||
                tipoTributacao == (int)TipoTributacaoFundo.LPrazo_SemComeCotas && diasTotal > 720)
            {
                aliquotaDiferenciada = false;
            }
            
            decimal cotaAplicacao;
            decimal? cotaInformada = null;
            decimal? cotaInformadaAtual = null;

            if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
            {
                cotaAplicacao = this.RetornaCotaMedia(idCarteira, idCotista);
            }
            else
            {
                //Tentar encontrar operacao e utilizar a cotaInformada, se for o caso
                if (posicaoCotista.IdOperacao.HasValue)
                {
                    OperacaoCotista operacaoCotista = new OperacaoCotista();
                    if (operacaoCotista.LoadByPrimaryKey(posicaoCotista.IdOperacao.Value))
                    {
                        cotaInformada = operacaoCotista.CotaInformada;
                        
                        //O RetornaCotaRecompostaAtual só deve utilizar a cotaInformada se estamos 
                        //calculando a posição no próprio dia da conversão da operação !!! 
                        if(operacaoCotista.DataConversao.Value.Date == data.Date){
                            cotaInformadaAtual = cotaInformada;
                        }
                    }
                }

                cotaAplicacao = cotaInformada.HasValue ? cotaInformada.Value : posicaoCotista.CotaAplicacao.Value;
            }

            decimal quantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
            decimal quantidadePosicao = posicaoCotista.Quantidade.Value;
            decimal valorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
            #endregion

            #region Verifica data de ultima cobrança de IR
            decimal valorIrComeCotas = 0;
            DateTime dataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;            
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.Equal(data),
                                                  agendaComeCotasCollection.Query.TipoPosicao.Equal(TipoComeCotas.Cotista.GetHashCode()));
            agendaComeCotasCollection.Query.Load();
            if (agendaComeCotasCollection.Count > 0)
            {
                dataUltimaCobrancaIR = agendaComeCotasCollection[0].DataUltimoIR.Value;
                valorIrComeCotas = agendaComeCotasCollection[0].ValorIRPago.Value;
            }
            #endregion

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            //ToDo: Rever
            if (cliente.IdTipo.Value == (int)TipoClienteFixo.FDIC)
            {
                HistoricoCota historicoCota = new HistoricoCota();
                cotaAplicacao = historicoCota.RetornaCotaRecompostaHistorica(idCarteira, dataConversao, data,
                    out debugInfo.DebugCotaAplicacaoRecompostaList, cotaInformada );

                cotaDia = historicoCota.RetornaCotaRecompostaAtual(idCarteira, data, false,
                    out debugInfo.DebugCotaDiaRecompostaList, cotaInformadaAtual);
            }

            CalculoTributo calculoTributo = new CalculoTributo();
            Cotista cotista = new Cotista();
            cotista.BuscaTributacaoCotista(idCotista);

            decimal quantidadeCalcular = 0;
            if (aliquotaDiferenciada == true)
            {
                quantidadeCalcular = quantidadeAntesCortes;
            }
            else
            {
                quantidadeCalcular = quantidadePosicao;
            }

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoResgate(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);
            int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Calcula IR e IOF
            bool tributaNaoResidente = cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente;

            decimal valorIR = 0;
            decimal valorIOF = 0;
            decimal valorRendimentoComeCotas = 0;
            if (!cotista.IsIsentoIR() && tipoTributacao != (int)TipoTributacaoFundo.Isento)             
            {
                if (carteira.Fie.Equals("S"))
                {
                    calculoTributo.calculoIRFieCotista(posicaoCotista.IdPosicao.Value,
                                                       cotaDia,
                                                       quantidadePosicao,
                                                       cotista.IsIsentoIOF(),
                                                       diasLiquidacaoResgate,
                                                       data);
                }
                else
                {

                    CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();

                    #region Seleciona data de liquidacao da operacao para calculo de IOF
                    OperacaoCotista operacao = new OperacaoCotista();
                    operacao.LoadByPrimaryKey(posicaoCotista.IdOperacao.Value);
                    calculoIRFundoRendaFixa.DataLiquidacao = operacao.DataLiquidacao;
                    calculoIRFundoRendaFixa.DataOperacaoResgate = data;
                    calculoIRFundoRendaFixa.DataConversaoResgate = data;
                    calculoIRFundoRendaFixa.DataLiquidacaoResgate = data;
                    #endregion

                    calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                    calculoIRFundoRendaFixa.IdFundo = idCarteira;
                    calculoIRFundoRendaFixa.Quantidade = quantidadeCalcular;
                    calculoIRFundoRendaFixa.QuantidadeAntesCortes = quantidadeAntesCortes;
                    calculoIRFundoRendaFixa.QuantidadePosicao = quantidadePosicao;
                    calculoIRFundoRendaFixa.TipoCota = tipoCota;
                    calculoIRFundoRendaFixa.PrejuizoCompensar = 0; // Não usa prejuízo a compensar na projeção
                    calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                    calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                    calculoIRFundoRendaFixa.DataConversao = dataConversao;
                    calculoIRFundoRendaFixa.DataCalculo = data;
                    calculoIRFundoRendaFixa.DataUltimaCobrancaIR = dataUltimaCobrancaIR;
                    calculoIRFundoRendaFixa.IOFVirtual = 0; //Não usa IOF Virtual na projeção                
                    calculoIRFundoRendaFixa.IdPosicao = idPosicao;
                    calculoIRFundoRendaFixa.TipoProcessamentoResgate = TipoProcessaResgate.OperacaoCotista;
                    calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                    calculoIRFundoRendaFixa.IsentoIOF = cotista.IsIsentoIOF();
                    calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;
                    calculoIRFundoRendaFixa.ProjecaoIR = true;

                    calculoTributo.CalculaIRFundoRendaFixa(calculoIRFundoRendaFixa, true, out debugInfo.DebugCalculaIRFundoRendaFixa);
                }

                #region Verifica se foi cadastrado valor de IR e IOF para a cautela, senão utiliza valor calculado
                PosicaoCotistaAlterada posicaoCotistaAlterada = lstPosicaoCotistaAlterada.Find(delegate(PosicaoCotistaAlterada x) { return x.IdPosicao == idPosicao; });
                if (posicaoCotistaAlterada != null && posicaoCotistaAlterada.IdPosicao.HasValue)
                {
                    valorIR = posicaoCotistaAlterada.ValorIRNovo.Value;
                    valorIOF = posicaoCotistaAlterada.ValorIOFNovo.Value;

                    if (dataUltimaCobrancaIR < data)
                    {
                        valorRendimentoComeCotas = calculoTributo.RendimentoComeCotas;
                    }
                    else
                    {
                        valorRendimentoComeCotas = posicaoCotista.ValorRendimento.Value;
                    }
                }
                else
                {
                    valorIR = calculoTributo.IR - valorIrComeCotas;
                    valorIOF = calculoTributo.IOF;
                    valorRendimentoComeCotas = calculoTributo.RendimentoComeCotas;
                }
                #endregion

                valorIR = calculoTributo.IR - valorIrComeCotas;
                valorIOF = calculoTributo.IOF;
                valorRendimentoComeCotas = calculoTributo.RendimentoComeCotas;

                OperacaoCotista operacaoCotista = new OperacaoCotista();
                decimal irDescontar = operacaoCotista.RetornaIRDescontarFDIC(data, dataConversao, idCarteira, calculoTributo.RendimentoComeCotas);
                debugInfo.IRDescontarFDIC = irDescontar;

                valorIR -= irDescontar;

            }
            else if (!cotista.IsIsentoIOF() && calculaIOF) //PARA O CASO DE SER ISENTO EM IR, MAS NÃO EM IOF
            {
                CalculoIOF calculoIOF = new CalculoIOF();
                calculoIOF.IdFundo = idCarteira;
                calculoIOF.Quantidade = quantidadeCalcular;
                calculoIOF.CotaAplicacao = cotaAplicacao;
                calculoIOF.CotaCalculo = cotaDia;
                calculoIOF.DataAplicacao = dataAplicacao;
                calculoIOF.DataCalculo = data;
                calculoIOF.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                calculoTributo.CalculaIOFFundo(calculoIOF);
                valorIOF = calculoTributo.IOF;
            }
            #endregion

            posicaoCotista.ValorIOF = valorIOF;
            posicaoCotista.ValorIR = valorIR;

            decimal saldoBruto = posicaoCotista.ValorBruto.Value;
            decimal saldoLiquido = saldoBruto - valorIOF - valorIR;
            posicaoCotista.ValorLiquido = saldoLiquido;
            posicaoCotista.ValorRendimento = valorRendimentoComeCotas;

            #region Preenchimento Informacoes de Debug
            debugInfo.AliquotaDiferenciada = aliquotaDiferenciada;
            debugInfo.CalculaIOF = calculaIOF;
            debugInfo.CotaAplicacao = posicaoCotista.CotaAplicacao;
            debugInfo.CotaDia = posicaoCotista.CotaDia;

            debugInfo.CotaAplicacaoCalculo = cotaAplicacao;
            debugInfo.CotaDiaCalculo = cotaDia;

            debugInfo.DataAplicacao = dataAplicacao;

            debugInfo.DataConversao = dataConversao;
            debugInfo.DiasLiquidComeCotas = diasLiquidacaoComeCotas;
            debugInfo.DiasLiquidResgate = diasLiquidacaoResgate;
            debugInfo.DiasTotal = diasTotal;
            debugInfo.IdCarteira = idCarteira;
            debugInfo.IdCotista = idCotista;
            debugInfo.IdPosicao = idPosicao;
            debugInfo.IsIsentoIOF = cotista.IsIsentoIOF();
            debugInfo.IsIsentoIR = cotista.IsIsentoIR();

            debugInfo.QuantidadeAntesCorte = quantidadeAntesCortes;
            debugInfo.QuantidadeCalcular = quantidadeCalcular;

            debugInfo.QuantidadePosicao = quantidadePosicao;
            debugInfo.TipoCota = (byte)tipoCota;
            debugInfo.TipoTributacao = (byte)tipoTributacao;
            debugInfo.ValorBruto = saldoBruto;
            debugInfo.ValorIOF = valorIOF;

            debugInfo.ValorIR = valorIR;
            debugInfo.ValorLiquido = saldoLiquido;

            debugInfo.ValorRendimentoComeCotas = valorRendimentoComeCotas;


            #endregion
        }

        /// <summary>
        /// Calcula a cota média de aplicação, ponderada pelas quantidades aplicadas em cada nota.
        /// Leva em conta a quantidade inicial de cada posição, mesmo que esta esteja zerada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns>cota média de aplicação</returns>
        public decimal RetornaCotaMedia(int idCarteira, int idCotista)
        {
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();

            posicaoCotistaCollection.Query
                 .Select(posicaoCotistaCollection.Query.CotaAplicacao, posicaoCotistaCollection.Query.QuantidadeInicial)
                 .Where(posicaoCotistaCollection.Query.IdCarteira == idCarteira,
                        posicaoCotistaCollection.Query.IdCotista == idCotista);

            posicaoCotistaCollection.Query.Load();

            decimal cotaMedia;
            if (posicaoCotistaCollection.HasData)
            {
                decimal valorAplicado = 0;
                decimal quantidadeAplicada = 0;
                for (int i = 0; i < posicaoCotistaCollection.Count; i++)
                {
                    decimal cotaAplicacao = ((PosicaoCotista)posicaoCotistaCollection[i]).CotaAplicacao.Value;
                    decimal quantidadeInicial = ((PosicaoCotista)posicaoCotistaCollection[i]).QuantidadeInicial.Value;
                    valorAplicado += cotaAplicacao * quantidadeInicial; //Notar que não é truncado este valor
                    quantidadeAplicada += quantidadeInicial;
                }
                cotaMedia = valorAplicado / quantidadeAplicada;

            }
            else
            {
                throw new CotistaSemPosicaoException("Cotista " + idCotista + " sem posição na carteira " + idCarteira);
            }

            return cotaMedia;
        }

        /// <summary>
        /// Calcula o IR e a quantidade do ComeCotas semestral.
        /// Lança operações de ComeCotas em OperacaoCotista.
        /// Lança valores dos débitos de IR (ComeCotas) em Liquidacao.
        /// Leva em conta o prejuízo a compensar em fundos de mesma categoria e administrador.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void ProcessaComeCotas(int idCarteira, DateTime data, bool checaDatas)
        {

            if (checaDatas)
            {
                #region Checa se está no último dia de maio ou novembro
                DateTime ultimoDiaMaio = new DateTime(data.Year, 05, 31);
                if (!Calendario.IsDiaUtil(ultimoDiaMaio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    ultimoDiaMaio = Calendario.SubtraiDiaUtil(ultimoDiaMaio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                if (data != ultimoDiaMaio)
                {
                    DateTime ultimoDiaNovembro = new DateTime(data.Year, 11, 30);
                    if (!Calendario.IsDiaUtil(ultimoDiaNovembro, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                    {
                        ultimoDiaNovembro = Calendario.SubtraiDiaUtil(ultimoDiaNovembro, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    if (data != ultimoDiaNovembro)
                    {
                        return;
                    }
                }
                #endregion
            }

            #region Busca os parâmetros da carteira, para cálculo dos tributos
            Carteira carteira = new Carteira();
            carteira.BuscaCarteiraCalculoTributo(idCarteira);
            int tipoTributacao = Carteira.RetornaTipoTributacao(idCarteira, data);
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes ||
                tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas ||
                tipoTributacao == (int)TipoTributacaoFundo.LPrazo_SemComeCotas ||
                tipoTributacao == (int)TipoTributacaoFundo.Isento ||
                Carteira.RetornaCondominioCarteira(idCarteira, data) == (int)TipoFundo.Fechado ||
                carteira.Fie.Equals("S"))
            {
                return;
            }

            #region Deleta todas as operações de ComeCotas do dia na OperacaoCotista
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.DeletaComeCotas(idCarteira, data);
            #endregion

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Busca o valor da cota na data
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idCarteira, data);

            decimal cotaDia;
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaDia = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaDia = historicoCota.CotaFechamento.Value;
            }
            #endregion

            //TODO: Efetuar tratamento para ComeCotasEntreRegatesConversao
            //Caso 'N', o PAS não deve calcular come cotas, caso o cotista tenha algum resgate total agendado
            //Caso 'S', o PAS calcula o Come cotas normalmente
            //bool ValidaResgateTotalAgendado = !carteira.ComeCotasEntreRegatesConversao.Equals("S");
            bool ValidaResgateTotalAgendado = false;

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotistaRendimentoOrdenado(idCarteira);

            int idContaDefault = 0;
            if (posicaoCotistaCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //
            }

            decimal totalValorIR = 0;
            List<int> lstCotistasResgatesTotaisAgendados = new List<int>();
            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                #region Inicializa as variáveis principais
                PosicaoCotista posicaoCotista = (PosicaoCotista)posicaoCotistaCollection[i];
                int idPosicao = posicaoCotista.IdPosicao.Value;
                int idCotista = posicaoCotista.IdCotista.Value;
                DateTime dataAplicacao = posicaoCotista.DataAplicacao.Value;
                DateTime dataConversao = posicaoCotista.DataConversao.Value;

                decimal cotaAplicacao;
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = this.RetornaCotaMedia(idCarteira, idCotista);
                }
                else
                {
                    cotaAplicacao = posicaoCotista.CotaAplicacao.Value;
                }

                decimal quantidade = posicaoCotista.Quantidade.Value;
                decimal quantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
                DateTime dataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;
                #endregion

                if (ValidaResgateTotalAgendado)
                {
                    if (lstCotistasResgatesTotaisAgendados.Contains(idCotista))
                        continue;
                    else
                    {
                        OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
                        operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira) &
                                                        operacaoCotistaColl.Query.IdCotista.Equal(idCotista) &
                                                        operacaoCotistaColl.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ResgateTotal) &
                                                        operacaoCotistaColl.Query.DataConversao.GreaterThanOrEqual(data));

                        if (operacaoCotistaColl.Query.Load())
                        {
                            lstCotistasResgatesTotaisAgendados.Add(idCotista);
                            continue;
                        }
                    }
                }

                CalculoTributo calculoTributo = new CalculoTributo();
                Cotista cotista = new Cotista();
                cotista.BuscaTributacaoCotista(idCotista);

                if (cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente)
                {
                    continue; //Investidor não residente não tem comecotas
                }

                decimal valorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
                #region Calcula IOF Virtual proporcional à qtde de cotas deste comecotas

                if (valorIOFVirtual != 0)
                {
                    decimal quantidadeHistorica = 0;
                    DateTime dataPosicaoHistorica = CalculoTributo.RetornaDataComeCotasPosterior(dataConversao);
                    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                    posicaoCotistaHistorico.Query.Select(posicaoCotistaHistorico.Query.Quantidade);
                    posicaoCotistaHistorico.Query.Where(posicaoCotistaHistorico.Query.IdPosicao.Equal(idPosicao),
                                                        posicaoCotistaHistorico.Query.DataHistorico.Equal(dataPosicaoHistorica));
                    if (posicaoCotistaHistorico.Query.Load())
                    {
                        quantidadeHistorica = posicaoCotistaHistorico.Quantidade.Value;

                        valorIOFVirtual = Math.Round(quantidade / quantidadeHistorica * valorIOFVirtual, 2);
                    }
                }
                #endregion

                #region Calcula rendimentos e IR (leva em conta eventual prejuízo a compensar)

                #region Busca Prejuizo a compensar
                decimal prejuizoCompensar = 0;

                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
                if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                {
                    prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, idCotista);

                    foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                    {
                        decimal valorPrejuizo = prejuizoCotista.ValorPrejuizo.Value;

                        if (valorPrejuizo < 0)
                        {
                            valorPrejuizo = 0;
                        }

                        prejuizoCompensar += valorPrejuizo;
                    }
                }
                #endregion

                decimal rendimento = 0;
                decimal rendimentoCompensado = 0;
                decimal prejuizoUsado = 0;
                decimal valorIR = 0;
                decimal valorIOF = 0;
                if (!cotista.IsIsentoIR())
                {
                    CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                    calculoIRFundoRendaFixa.IdFundo = idCarteira;
                    calculoIRFundoRendaFixa.Quantidade = quantidade;
                    calculoIRFundoRendaFixa.QuantidadeAntesCortes = quantidadeAntesCortes;
                    calculoIRFundoRendaFixa.TipoCota = tipoCota;
                    calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                    calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                    calculoIRFundoRendaFixa.DataConversao = dataConversao;
                    calculoIRFundoRendaFixa.DataCalculo = data;
                    calculoIRFundoRendaFixa.DataUltimaCobrancaIR = dataUltimaCobrancaIR;
                    calculoIRFundoRendaFixa.IOFVirtual = valorIOFVirtual;
                    calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                    calculoIRFundoRendaFixa.PrejuizoCompensar = prejuizoCompensar;
                    calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                    calculoIRFundoRendaFixa.IsentoIOF = cotista.IsIsentoIOF();
                    calculoIRFundoRendaFixa.IdPosicao = idPosicao;

                    calculoTributo.CalculaIRComeCotasEvento(calculoIRFundoRendaFixa, FonteOperacaoCotista.ComeCotas, TipoComeCotas.Cotista, null);

                    //Valor do IOR calculado
                    valorIOF = calculoTributo.IOF;
                    //Valor do IR calculado
                    valorIR = calculoTributo.IR;
                    //Rendimento sem abater o prejuízo 
                    rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                    //Rendimento com abatimento do prejuízo 
                    rendimentoCompensado = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosteriorCompensado;
                    //Valor do prejuízo usado para compensar rendimentos
                    prejuizoUsado = calculoTributo.PrejuizoUsado;
                }
                #endregion

                #region Trata compensação de prejuízo (somente em caso de rendimento positivo)
                if (prejuizoCompensar != 0 && rendimento > 0)
                {
                    PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
                    prejuizoCotista.CompensaPrejuizo(data, idCarteira, idCotista, prejuizoUsado, prejuizoCotistaCollection);
                }
                #endregion

                if (rendimento > 0)
                {
                    posicaoCotista.DataUltimaCobrancaIR = data;
                }
            }

            //Atualiza as collections
            posicaoCotistaCollection.Save();
        }

        /// <summary>
        /// Efetua cálculo de imposto de fundo onde tenha ocorrido algum evento
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaImpostoEvento(int idCarteira, DateTime data, FonteOperacaoCotista fonteOperacaoCotista, int idEvento)
        {
            #region Busca os parâmetros da carteira, para cálculo dos tributos
            Carteira carteira = new Carteira();
            carteira.BuscaCarteiraCalculoTributo(idCarteira);
            int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            if (tipoTributacao == (int)TipoTributacaoFundo.Isento)
            {
                return;
            }

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Busca o valor da cota na data
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idCarteira, data);

            decimal cotaDia;
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaDia = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaDia = historicoCota.CotaFechamento.Value;
            }
            #endregion

            OperacaoCotistaCollection operacaoCotistaCollectionInserir = new OperacaoCotistaCollection();
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotistaRendimentoOrdenado(idCarteira);

            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {

                #region Inicializa as variáveis principais
                PosicaoCotista posicaoCotista = (PosicaoCotista)posicaoCotistaCollection[i];
                int idPosicao = posicaoCotista.IdPosicao.Value;
                int idCotista = posicaoCotista.IdCotista.Value;
                DateTime dataAplicacao = posicaoCotista.DataAplicacao.Value;
                DateTime dataConversao = posicaoCotista.DataConversao.Value;
                decimal quantidadeIOF = posicaoCotista.Quantidade.Value;

                decimal cotaAplicacao;
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = this.RetornaCotaMedia(idCarteira, idCotista);
                }
                else
                {
                    cotaAplicacao = posicaoCotista.CotaAplicacao.Value;
                }

                CalculoTributo calculoTributo = new CalculoTributo();

                //Seleciona a quantidade do evento
                decimal quantidade = posicaoCotista.Quantidade.Value;
                decimal quantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
                DateTime dataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;
                #endregion

                
                Cotista cotista = new Cotista();
                cotista.BuscaTributacaoCotista(idCotista);

                if (cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente)
                {
                    continue; //Investidor não residente não tem comecotas
                }

                //decimal valorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
                //#region Calcula IOF Virtual proporcional à qtde de cotas deste comecotas

                //if (valorIOFVirtual != 0)
                //{
                //    decimal quantidadeHistorica = 0;
                //    DateTime dataPosicaoHistorica = CalculoTributo.RetornaDataComeCotasPosterior(dataConversao);
                //    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                //    posicaoCotistaHistorico.Query.Select(posicaoCotistaHistorico.Query.Quantidade);
                //    posicaoCotistaHistorico.Query.Where(posicaoCotistaHistorico.Query.IdPosicao.Equal(idPosicao),
                //                                        posicaoCotistaHistorico.Query.DataHistorico.Equal(dataPosicaoHistorica));
                //    if (posicaoCotistaHistorico.Query.Load())
                //    {
                //        quantidadeHistorica = posicaoCotistaHistorico.Quantidade.Value;

                //        valorIOFVirtual = Math.Round(quantidadeIOF / quantidadeHistorica * valorIOFVirtual, 2);
                //    }
                //}
                //#endregion

                #region Calcula rendimentos e IR (leva em conta eventual prejuízo a compensar)

                #region Busca Prejuizo a compensar
                decimal prejuizoCompensar = 0;

                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
                prejuizoCotistaCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, idCotista);

                foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                {
                    decimal valorPrejuizo = prejuizoCotista.ValorPrejuizoOriginal.Value;

                    PrejuizoCotistaUsado prejuizoCotistaUsado = new PrejuizoCotistaUsado();
                    decimal prejuizoUsadoHistorico = prejuizoCotistaUsado.RetornaPrejuizoUsado(idCotista, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);

                    valorPrejuizo -= prejuizoUsadoHistorico;

                    if (valorPrejuizo < 0)
                    {
                        valorPrejuizo = 0;
                    }

                    prejuizoCompensar += valorPrejuizo;
                }
                #endregion

                decimal rendimento = 0;
                decimal rendimentoCompensado = 0;
                decimal prejuizoUsado = 0;
                decimal valorIR = 0;
                decimal valorIOF = 0;

                bool tributaNaoResidente = cotista.TipoTributacao.Value == (byte)TipoTributacaoCotista.NaoResidente;

                if (!cotista.IsIsentoIR())
                {
                    CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                    calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                    calculoIRFundoRendaFixa.IdFundo = idCarteira;
                    calculoIRFundoRendaFixa.Quantidade = quantidade;
                    calculoIRFundoRendaFixa.QuantidadeAntesCortes = quantidadeAntesCortes;
                    calculoIRFundoRendaFixa.TipoCota = tipoCota;
                    calculoIRFundoRendaFixa.PrejuizoCompensar = prejuizoCompensar;
                    calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                    calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                    calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                    calculoIRFundoRendaFixa.DataConversao = dataConversao;
                    calculoIRFundoRendaFixa.DataCalculo = data;
                    calculoIRFundoRendaFixa.TipoProcessamentoResgate = TipoProcessaResgate.OperacaoCotista;
                    calculoIRFundoRendaFixa.DataUltimaCobrancaIR = dataUltimaCobrancaIR;
                    //calculoIRFundoRendaFixa.IOFVirtual = 0; //Obsoleto...
                    calculoIRFundoRendaFixa.IdPosicao = idPosicao;
                    calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                    calculoIRFundoRendaFixa.IsentoIOF = cotista.IsIsentoIOF();
                    calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;

                    calculoTributo.CalculaIRComeCotasEvento(calculoIRFundoRendaFixa, fonteOperacaoCotista, TipoComeCotas.Cotista, idEvento);

                    //Valor do IOR calculado
                    valorIOF = calculoTributo.IOF;
                    //Valor do IR calculado
                    valorIR = calculoTributo.IR;
                    //Rendimento sem abater o prejuízo 
                    rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                    //Rendimento com abatimento do prejuízo 
                    rendimentoCompensado = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosteriorCompensado;
                    //Valor do prejuízo usado para compensar rendimentos
                    prejuizoUsado = calculoTributo.PrejuizoUsado;
                }
                #endregion

                #region Trata compensação de prejuízo (somente em caso de rendimento positivo)
                if (prejuizoCompensar != 0 && rendimento > 0)
                {
                    PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
                    prejuizoCotista.CompensaPrejuizo(data, idCarteira, idCotista, prejuizoUsado, prejuizoCotistaCollection);
                }
                #endregion

                if (rendimento > 0)
                {
                    posicaoCotista.DataUltimaCobrancaIR = data;
                }
            }

            //Atualiza as collections
            posicaoCotistaCollection.Save();

        }

        public void ProcessaComeCotasEvento(int idCarteira, DateTime data)
        {
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.DataLancamento.Equal(data),
                                                  agendaComeCotasCollection.Query.IdCarteira.Equal(idCarteira));

            if (agendaComeCotasCollection.Query.Load())
            {
                decimal totalValorIR = 0;

                foreach (AgendaComeCotas agendaComeCotas in agendaComeCotasCollection)
                {

                    //Verifica se agenda foi criada pelo cotista ou fundo
                    if (agendaComeCotas.TipoPosicao.Equals((int)TipoComeCotas.Cotista))
                    {
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        if (posicaoCotista.LoadByPrimaryKey(agendaComeCotas.IdPosicao.Value))
                        {
                            #region Verifica se existem valores colados para a posicao processada
                            ColagemComeCotasPosCotista colagemComeCotasPosCotista = new ColagemComeCotasPosCotista();
                            colagemComeCotasPosCotista.Query.Where(colagemComeCotasPosCotista.Query.IdPosicaoVinculada.Equal(posicaoCotista.IdPosicao.Value)
                                                                    & colagemComeCotasPosCotista.Query.DataReferencia.Equal(data));
                            #endregion

                            //Verifica se possui quantidade de come cotas
                            if (colagemComeCotasPosCotista.Query.Load() || agendaComeCotas.QuantidadeComida.Value > 0)
                            {
                                bool valoresColados = false;
                                decimal quantidade = agendaComeCotas.QuantidadeComida.Value;
                                decimal valorIRPago = agendaComeCotas.ValorIRPago.Value;
                                decimal prejuizoUsado = agendaComeCotas.PrejuizoUsado.Value;
                                decimal rendimentoResgate = agendaComeCotas.RendimentoCompensado.Value;
                                decimal variacaoResgate = agendaComeCotas.RendimentoCompensado.Value;
                                decimal valorIOF = 0;
                                if (colagemComeCotasPosCotista.IdCarteira != null && colagemComeCotasPosCotista.IdCarteira.Value > 0)
                                {
                                    quantidade = colagemComeCotasPosCotista.Quantidade.Value;
                                    valorIRPago = colagemComeCotasPosCotista.ValorIR.Value;
                                    prejuizoUsado = colagemComeCotasPosCotista.PrejuizoUsado.Value;
                                    rendimentoResgate = colagemComeCotasPosCotista.RendimentoComeCotas.Value;
                                    variacaoResgate = colagemComeCotasPosCotista.VariacaoComeCotas.Value;
                                    valorIOF = colagemComeCotasPosCotista.ValorIOF.Value;
                                    valoresColados = true;
                                }

                                posicaoCotista.Quantidade -= quantidade;
                                posicaoCotista.ValorBruto = Utilitario.Truncate(posicaoCotista.Quantidade.Value * posicaoCotista.CotaDia.Value, 2);
                                posicaoCotista.ValorLiquido = posicaoCotista.ValorBruto.Value - posicaoCotista.ValorIR.Value - posicaoCotista.ValorIOF.Value;

                                totalValorIR += valorIRPago;

                                #region Lanca operacao em OperacaoCotista
                                OperacaoCotista operacaoCotista = new OperacaoCotista();
                                operacaoCotista.AddNew();
                                operacaoCotista.IdCotista = posicaoCotista.IdCotista;
                                operacaoCotista.IdCarteira = posicaoCotista.IdCarteira;
                                operacaoCotista.DataOperacao = data;
                                operacaoCotista.DataConversao = data;
                                operacaoCotista.DataLiquidacao = agendaComeCotas.DataVencimento.Value;
                                operacaoCotista.DataAgendamento = data;
                                operacaoCotista.TipoOperacao = (int)TipoOperacaoCotista.ComeCotas;
                                operacaoCotista.IdPosicaoResgatada = posicaoCotista.IdPosicao;
                                operacaoCotista.Quantidade = quantidade;
                                operacaoCotista.CotaOperacao = posicaoCotista.CotaDia;
                                operacaoCotista.ValorIR = valorIRPago;
                                operacaoCotista.ValorIOF = valorIOF;
                                operacaoCotista.PrejuizoUsado = prejuizoUsado;
                                operacaoCotista.RendimentoResgate = rendimentoResgate;
                                operacaoCotista.VariacaoResgate = variacaoResgate;
                                operacaoCotista.IdFormaLiquidacao = 1; //AJUSTAR DEPOIS!!!
                                operacaoCotista.Fonte = (byte)agendaComeCotas.TipoEvento.Value;
                                operacaoCotista.ValoresColados = valoresColados ? "S" : "N";
                                operacaoCotista.DataRegistro = data;

                                operacaoCotista.Save();

                                #endregion

                                //Este IOF abate do rendimento, portanto deve ser somado a rendimentos posteriores
                                posicaoCotista.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value - agendaComeCotas.ValorIOFVirtual + agendaComeCotas.ValorIOF;

                            }

                            posicaoCotista.Save();
                        }
                    }
                    else
                    {
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        if (posicaoFundo.LoadByPrimaryKey(agendaComeCotas.IdPosicao.Value))
                        {

                            #region Verifica se existem valores colados para a posicao processada
                            ColagemComeCotasPosFundo colagemComeCotasPosFundo = new ColagemComeCotasPosFundo();
                            colagemComeCotasPosFundo.Query.Where(colagemComeCotasPosFundo.Query.IdPosicaoVinculada.Equal(posicaoFundo.IdPosicao.Value)
                                                                & colagemComeCotasPosFundo.Query.DataReferencia.Equal(data));
                            #endregion

                            if (colagemComeCotasPosFundo.Query.Load() || agendaComeCotas.QuantidadeComida.Value > 0)
                            {
                                decimal quantidadeComeCotas = agendaComeCotas.QuantidadeComida.Value;
                                decimal cotaDia = agendaComeCotas.ValorCota.Value;
                                decimal valorIR = agendaComeCotas.ValorIRPago.Value;
                                decimal valorIOF = 0;
                                decimal prejuizoUsado = agendaComeCotas.PrejuizoUsado.Value;
                                decimal rendimentoCompensado = agendaComeCotas.RendimentoCompensado.Value;
                                decimal rendimento = agendaComeCotas.RendimentoCompensado.Value;
                                bool existeColagem = false;

                                if (colagemComeCotasPosFundo.IdCarteira != null && colagemComeCotasPosFundo.IdCarteira.Value > 0)
                                {
                                    ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
                                    cotaDia = colagemComeCotasPosFundo.ValorBruto.Value / colagemComeCotasPosFundo.Quantidade.Value;
                                    quantidadeComeCotas = colagemComeCotasPosFundo.Quantidade.Value;
                                    valorIR = colagemComeCotasPosFundo.ValorIR.Value;
                                    valorIOF = colagemComeCotasPosFundo.ValorIOF.Value;
                                    prejuizoUsado = colagemComeCotasPosFundo.PrejuizoUsado.Value;
                                    rendimentoCompensado = colagemComeCotasPosFundo.RendimentoComeCotas.Value;
                                    rendimento = colagemComeCotasPosFundo.VariacaoComeCotas.Value;
                                    existeColagem = true;
                                }

                                posicaoFundo.Quantidade -= quantidadeComeCotas;

                                posicaoFundo.ValorBruto = Utilitario.Truncate(posicaoFundo.Quantidade.Value * posicaoFundo.CotaDia.Value, 2);
                                posicaoFundo.ValorLiquido = posicaoFundo.ValorBruto.Value - posicaoFundo.ValorIR.Value - posicaoFundo.ValorIOF.Value;

                                #region Lanca operacao em OperacaoFundo (tipo de operacao = ComeCotas)
                                OperacaoFundo operacaoFundo = new OperacaoFundo();
                                operacaoFundo.IdCliente = posicaoFundo.IdCliente;
                                operacaoFundo.IdCarteira = posicaoFundo.IdCarteira;
                                operacaoFundo.DataOperacao = data;
                                operacaoFundo.DataConversao = data;
                                operacaoFundo.DataLiquidacao = agendaComeCotas.DataVencimento.Value;
                                operacaoFundo.DataAgendamento = data;
                                operacaoFundo.TipoOperacao = (int)TipoOperacaoFundo.ComeCotas;
                                operacaoFundo.IdPosicaoResgatada = posicaoFundo.IdPosicao;
                                operacaoFundo.Quantidade = quantidadeComeCotas;
                                operacaoFundo.CotaOperacao = posicaoFundo.CotaDia;
                                operacaoFundo.ValorIR = valorIR;
                                operacaoFundo.ValorIOF = valorIOF;
                                operacaoFundo.PrejuizoUsado = prejuizoUsado;
                                operacaoFundo.RendimentoResgate = rendimentoCompensado;
                                operacaoFundo.VariacaoResgate = rendimento;
                                operacaoFundo.IdFormaLiquidacao = 1; //AJUSTAR DEPOIS!!!
                                operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Interno;
                                operacaoFundo.ValoresColados = (existeColagem ? "S" : "N");
                                operacaoFundo.DataRegistro = data;

                                operacaoFundo.Save();
                                #endregion

                                #region Lanca em DetalheResgateFundo
                                DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                                detalheResgateFundo.IdCarteira = posicaoFundo.IdCarteira.Value;
                                detalheResgateFundo.IdCliente = posicaoFundo.IdCliente.Value;
                                detalheResgateFundo.IdOperacao = operacaoFundo.IdOperacao.Value;
                                detalheResgateFundo.IdPosicaoResgatada = posicaoFundo.IdPosicao.Value;
                                detalheResgateFundo.Quantidade = quantidadeComeCotas;
                                detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                                detalheResgateFundo.ValorIR = valorIR;
                                detalheResgateFundo.ValorIOF = valorIOF;
                                detalheResgateFundo.VariacaoResgate = rendimento;
                                detalheResgateFundo.ValorBruto = 0;
                                detalheResgateFundo.ValorLiquido = 0;

                                detalheResgateFundo.Save();
                                #endregion
                            }

                            //Este IOF abate do rendimento, portanto deve ser somado a rendimentos posteriores
                            posicaoFundo.ValorIOFVirtual = posicaoFundo.ValorIOFVirtual.Value - agendaComeCotas.ValorIOFVirtual + agendaComeCotas.ValorIOF;
                            
                            posicaoFundo.Save();
                        }
                    }
                }

                if (totalValorIR > 0)
                {
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //

                    #region Insert em Liquidacao do valor total de IR do ComeCotas
                    Liquidacao liquidacao = new Liquidacao();

                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(idCarteira);

                    CalculoTributo calculoDataIR = new CalculoTributo();
                    CalculoTributo.ProjecaoIRCotista projecaoIR = (carteira.ProjecaoIRComeCotas.HasValue ? (CalculoTributo.ProjecaoIRCotista)carteira.ProjecaoIRComeCotas.Value : CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate);
                    int diasAposComeCotas = (projecaoIR == CalculoTributo.ProjecaoIRCotista.AlgunsDiasAposResgate ? carteira.DiasAposComeCotasIR.Value : 0);
                    DateTime dataVencimento = calculoDataIR.RetornaDiaPagamentoIR(data, projecaoIR, diasAposComeCotas);
                    string descricao = "Débito de IR - Lei 9532";

                    liquidacao.AddNew();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataVencimento;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = totalValorIR * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Fundo.ComeCotas;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();
                    #endregion
                }
            }
        }

        /// <summary>
        /// Calcula o IR e a quantidade do ComeCotas semestral.
        /// Lança operações de ComeCotas em OperacaoCotista.
        /// Lança valores dos débitos de IR (ComeCotas) em Liquidacao.
        /// Leva em conta o prejuízo a compensar em fundos de mesma categoria e administrador.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void ProcessaComeCotas(int idCarteira, DateTime data)
        {
            this.ProcessaComeCotas(idCarteira, data, true);
        }

        /// <summary>
        /// Retorna o total de quantidade de cotas de todos os cotistas da carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns></returns>
        public decimal RetornaTotalCotas(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.Quantidade.Sum())
                 .Where(this.query.IdCarteira == idCarteira);

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o total de quantidade de cotas de uma série (OffShore)
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idSerie"></param>    
        /// <param name="idCotista">Se diferente de null, retorna as qtde de cotista dentro de uma série</param>    
        /// <returns></returns>
        public decimal RetornaTotalCotasPorSerie(int idCarteira, int idSerie, int? idCotista)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.Quantidade.Sum())
                 .Where(this.query.IdCarteira.Equal(idCarteira) & this.query.IdSeriesOffShore.Equal(idSerie));

            if (idCotista.HasValue)
            {
                this.Query.Where(this.query.IdCotista.Equal(idCotista.Value));
            }
             
            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o total de quantidade de cotas do cotista na carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="idCotista"></param>
        /// <returns></returns>
        public decimal RetornaTotalCotas(int idCarteira, int idCotista)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.IdCotista.Equal(idCotista));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Liquido dado o idCarteira e o IdCotista
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSumValorLiquido(int idCarteira, int idCotista)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.ValorLiquido.Sum())
                 .Where(this.query.IdCarteira == idCarteira,
                        this.query.IdCotista == idCotista,
                        this.query.Quantidade != 0);

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Liquido dado o idCarteira
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSumValorLiquido(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.ValorLiquido.Sum())
                 .Where(this.query.IdCarteira == idCarteira &&
                        this.query.Quantidade != 0);

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSumValorBruto(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira &&
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Carrega o objeto IdOperacao, PosicaoCotista com os campos Quantidade, DataAplicacao, CotaAplicacao, 
        /// DataUltimaCobranca, ValorIOFVirtual.
        /// PosicaoCotistaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idOperacao"></param>        
        /// <returns></returns>
        public decimal BuscaPosicaoCotista(int idOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, 
                         this.Query.Quantidade, 
                         this.Query.DataAplicacao,
                         this.Query.CotaAplicacao, 
                         this.Query.DataUltimaCobrancaIR, 
                         this.Query.ValorIOFVirtual)
                 .Where(this.Query.IdOperacao == idOperacao);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new PosicaoCotistaNaoCadastradoException("Posição " + idOperacao + " não encontrada.");
            }

            return this.CotaAplicacao.Value;
        }

        /// <summary>
        /// Retorna o número de cotistas com posição (não zerada) na carteira.        
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns></returns>
        public int RetornaCountNumeroCotistas(int idCarteira)
        {
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("p");
            PosicaoCotistaQuery posicaoCotistaSubQuery = new PosicaoCotistaQuery("p1");

            string column = "<count(distinct sub." + PosicaoCotistaMetadata.ColumnNames.IdCotista + ") as IdCotista>";

            posicaoCotistaQuery.Select("" + column + "");
            //
            posicaoCotistaQuery.From
                (
                    posicaoCotistaSubQuery.Select(posicaoCotistaSubQuery.IdCarteira,
                                                  posicaoCotistaSubQuery.IdCotista,
                                                  (posicaoCotistaSubQuery.Quantidade + posicaoCotistaSubQuery.QuantidadeBloqueada).As("Quantidade"))
                                          .Where(posicaoCotistaSubQuery.IdCarteira == idCarteira &
                                                 posicaoCotistaSubQuery.Quantidade != 0)
                ).As("sub");

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Load(posicaoCotistaQuery);

            return (posicaoCotistaCollection != null && posicaoCotistaCollection[0].IdCotista.HasValue) ? posicaoCotistaCollection[0].IdCotista.Value : 0;
        }

        /// <summary>
        /// Atualiza a coluna QuantidadeAntesCortes para garantir sua integridade no 1o dia de processamento da carteira.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAtualizacaoCotasPorCalculo(int idCarteira, DateTime data)
        {
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                               posicaoCotistaCollection.Query.Quantidade.GreaterThan(0));
            posicaoCotistaCollection.Query.Load();

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                CalculoTributo calculoTributo = new CalculoTributo();
                decimal totalCotasCortes = calculoTributo.CalculaQuantidadeCortesHistoricos(data, posicaoCotista);

                posicaoCotista.QuantidadeAntesCortes = posicaoCotista.Quantidade.Value + totalCotasCortes;
            }

            posicaoCotistaCollection.Save();

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                       posicaoCotistaAberturaCollection.Query.Quantidade.GreaterThan(0),
                                                       posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data));
            posicaoCotistaAberturaCollection.Query.Load();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
            {
                CalculoTributo calculoTributo = new CalculoTributo();
                decimal totalCotasCortes = calculoTributo.CalculaQuantidadeCortesHistoricos(data, posicaoCotistaAbertura);

                posicaoCotistaAbertura.QuantidadeAntesCortes = posicaoCotistaAbertura.Quantidade.Value + totalCotasCortes;
            }

            posicaoCotistaAberturaCollection.Save();
        }

        /// <summary>
        /// Atualiza a coluna QuantidadeAntesCortes a partir do historico de comecotas na DetalheResgateCotista, para garantir sua integridade no 1o dia de processamento da carteira.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAtualizacaoCotasPorComeCotas(int idCarteira, DateTime data)
        {
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                 posicaoCotistaCollection.Query.Quantidade.GreaterThan(0));
            posicaoCotistaCollection.Query.Load();

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                int idPosicao = posicaoCotista.IdPosicao.Value;

                DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                detalheResgateCotistaQuery.Select(detalheResgateCotistaQuery.Quantidade.Sum());
                detalheResgateCotistaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao == detalheResgateCotistaQuery.IdOperacao);
                detalheResgateCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao.Equal((byte)TipoOperacaoCotista.ComeCotas),
                                                 operacaoCotistaQuery.DataConversao.LessThanOrEqual(data),
                                                 detalheResgateCotistaQuery.IdPosicaoResgatada.Equal(idPosicao));
                detalheResgateCotista.Load(detalheResgateCotistaQuery);

                decimal totalCotasCortes = detalheResgateCotista.Quantidade.HasValue ? detalheResgateCotista.Quantidade.Value : 0;

                posicaoCotista.QuantidadeAntesCortes = posicaoCotista.Quantidade.Value + totalCotasCortes;
            }

            posicaoCotistaCollection.Save();

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                         posicaoCotistaAberturaCollection.Query.Quantidade.GreaterThan(0),
                                                         posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data));
            posicaoCotistaAberturaCollection.Query.Load();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
            {
                int idPosicao = posicaoCotistaAbertura.IdPosicao.Value;

                DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                DetalheResgateCotistaQuery detalheResgateCotistaQuery = new DetalheResgateCotistaQuery("D");
                OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
                detalheResgateCotistaQuery.Select(detalheResgateCotistaQuery.Quantidade.Sum());
                detalheResgateCotistaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao == detalheResgateCotistaQuery.IdOperacao);
                detalheResgateCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao.Equal((byte)TipoOperacaoCotista.ComeCotas),
                                                 operacaoCotistaQuery.DataConversao.LessThanOrEqual(data),
                                                 detalheResgateCotistaQuery.IdPosicaoResgatada.Equal(idPosicao));
                detalheResgateCotista.Load(detalheResgateCotistaQuery);

                decimal totalCotasCortes = detalheResgateCotista.Quantidade.HasValue ? detalheResgateCotista.Quantidade.Value : 0;

                posicaoCotistaAbertura.QuantidadeAntesCortes = posicaoCotistaAbertura.Quantidade.Value + totalCotasCortes;
            }

            posicaoCotistaAberturaCollection.Save();
        }

        public bool ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota, DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF)
        {
            if (posicaoNota.QtCotas == 0)
            {
                return false;
            }

            if (posicaoNota.DtPosicao != dataImplantacao)
            {
                //Só queremos importar posições cujas datas correspondam à data de implantação do fundo
                return false;
            }

            OperacaoCotista operacaoCotista = null;
            if (mappingOperacaoFinancialYMF.ContainsKey(posicaoNota.IdNota))
            {
                //Vamos tentar encontrar na tabela de operações uma operação com o IdNota desta posição. Na YMF IdOperacao e IdPosicao caminham juntos como IdNota
                this.IdOperacao = mappingOperacaoFinancialYMF[posicaoNota.IdNota];
                operacaoCotista = new OperacaoCotista();
                operacaoCotista.LoadByPrimaryKey(this.IdOperacao.Value);
            }

            Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(posicaoNota.CdCotista.Trim());
            this.IdCotista = cotista.IdCotista;

            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(posicaoNota.CdFundo.Trim());
            this.IdCarteira = cliente.IdCliente;
            this.DataAplicacao = posicaoNota.DtAplicacao;
            this.ValorAplicacao = posicaoNota.VlAplicacao;
            this.CotaAplicacao = posicaoNota.VlCotaAplicacao;

            this.Quantidade = posicaoNota.QtCotas;
            this.ValorBruto = posicaoNota.VlBruto;
            this.ValorIR = posicaoNota.VlIr;
            this.ValorIOF = posicaoNota.VlIof;
            this.ValorPerformance = posicaoNota.VlPerformance;
            this.ValorIOFVirtual = posicaoNota.VlIofVirtual;

            //Ajustar data de conversao pela operacao
            if (operacaoCotista != null)
            {
                this.DataConversao = operacaoCotista.DataConversao;
            }
            else
            {
                this.DataConversao = this.DataAplicacao;
            }
            this.CotaDia = this.ValorBruto / this.Quantidade;
            this.ValorLiquido = this.ValorBruto - this.ValorIR - this.ValorIOF - this.ValorPerformance;
            this.QuantidadeInicial = this.Quantidade;
            this.QuantidadeBloqueada = 0;
            this.QuantidadeAntesCortes = this.Quantidade;
            this.ValorRendimento = 0;
            this.PosicaoIncorporada = "N";
            this.DataUltimaCobrancaIR = posicaoNota.DtUltimoResgateIr.HasValue ? posicaoNota.DtUltimoResgateIr : this.DataConversao;
            this.DataUltimoCortePfee = posicaoNota.DtInicioPerformance;

            return true;
        }

        /// <summary>
        /// Processa eventos de comecotas listados na tabela AgendaEventos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaComeCotasAgendaEventos(int idCarteira, DateTime data)
        {
            AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
            agendaFundoCollection.Query.Select(agendaFundoCollection.Query.IdCarteira);
            agendaFundoCollection.Query.Where(agendaFundoCollection.Query.DataEvento.Equal(data),
                                              agendaFundoCollection.Query.TipoEvento.Equal((byte)TipoEventoFundo.ComeCotas),
                                              agendaFundoCollection.Query.IdCarteira.Equal(idCarteira));
            agendaFundoCollection.Query.Load();

            if (agendaFundoCollection.Count > 0)
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                posicaoCotista.ProcessaComeCotas(idCarteira, data);
            }
        }

        /// <summary>
        /// Checa pelos parâmetros passados se o cotista tem saldo livre suficiente dada ou a qtde ou valor passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="idPosicao"></param>
        /// <param name="quantidade"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public bool TemSaldoLivre(int idCarteira, int idCotista, int? idPosicao, decimal? quantidade, decimal? valor)
        {
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Query.Select(posicaoCotista.Query.Quantidade.Sum(),
                                        posicaoCotista.Query.QuantidadeBloqueada.Sum(),
                                        posicaoCotista.Query.CotaDia.Avg());
            posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(idCarteira),
                                       posicaoCotista.Query.IdCotista.Equal(idCotista));

            if (idPosicao.HasValue)
            {
                posicaoCotista.Query.Where(posicaoCotista.Query.IdPosicao.Equal(idPosicao.Value));
            }

            posicaoCotista.Query.Load();

            decimal quantidadeLivre = 0;
            decimal valorLivre = 0;
            if (posicaoCotista.Quantidade.HasValue)
            {
                quantidadeLivre = posicaoCotista.Quantidade.Value - posicaoCotista.QuantidadeBloqueada.Value;
                valorLivre = quantidadeLivre * posicaoCotista.CotaDia.Value;
            }

            bool temSaldoLivre = true;
            if (quantidade.HasValue)
            {
                if (quantidade.Value < quantidadeLivre)
                {
                    temSaldoLivre = false;
                }
            }

            if (valor.HasValue)
            {
                if (valor.Value < valorLivre)
                {
                    temSaldoLivre = false;
                }
            }

            return temSaldoLivre;
        }

        /*public PosicaoCotista RetornaPosicaoLivre(int idCarteira, int idCotista)
        {
            //Descobrir data mais recente da abertura
            DateTime dataHistorico = DateTime.MinValue;
            PosicaoCotistaAbertura posicaoMaisRecente = new PosicaoCotistaAbertura();
            posicaoMaisRecente.Query.Select(posicaoMaisRecente.Query.DataHistorico.Max());
            posicaoMaisRecente.Query.Where(posicaoMaisRecente.Query.IdCarteira.Equal(idCarteira),
                    posicaoMaisRecente.Query.IdCotista.Equal(idCotista));

            if (posicaoMaisRecente.Query.Load())
            {
                if (posicaoMaisRecente.DataHistorico.HasValue)
                {
                    dataHistorico = posicaoMaisRecente.DataHistorico.Value;
                }
            }

            if (dataHistorico == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return new PosicaoCotista().RetornaPosicaoLivre(idCarteira, idCotista, dataHistorico, null);
            }
            
        }*/

        public PosicaoCotista RetornaPosicaoLivreHistorico(int idCarteira, int idCotista, DateTime? data, int? idPosicao)
        {
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.Quantidade.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.QuantidadeBloqueada.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorBruto.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorIR.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorIOF.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorPerformance.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorLiquido.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.ValorRendimento.Sum(),
                                                        posicaoCotistaHistoricoCollection.Query.CotaDia.Avg(),
                                                        posicaoCotistaHistoricoCollection.Query.DataHistorico);
            posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                       posicaoCotistaHistoricoCollection.Query.IdCotista.Equal(idCotista));
            posicaoCotistaHistoricoCollection.Query.GroupBy(posicaoCotistaHistoricoCollection.Query.DataHistorico);
            posicaoCotistaHistoricoCollection.Query.OrderBy(posicaoCotistaHistoricoCollection.Query.DataHistorico.Descending);

            if (idPosicao.HasValue)
            {
                posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdPosicao.Equal(idPosicao.Value));
            }

            if (data.HasValue)
            {
                posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.DataHistorico.Equal(data));
            }

            PosicaoCotista posicaoCotistaRetorno = new PosicaoCotista();
            if (posicaoCotistaHistoricoCollection.Query.Load())
            {

                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                if (posicaoCotistaHistoricoCollection.Count > 0)
                {
                    posicaoCotistaHistorico = posicaoCotistaHistoricoCollection[0];
                }

                decimal valorCota = 0;
                decimal quantidade = 0;
                decimal quantidadeBloqueada = 0;
                decimal valorBruto = 0;
                decimal valorLiquido = 0;
                decimal valorIR = 0;
                decimal valorIOF = 0;
                decimal valorPerformance = 0;
                decimal valorRendimento = 0;
                if (posicaoCotistaHistorico.es.HasData && posicaoCotistaHistorico.Quantidade.HasValue)
                {
                    valorCota = posicaoCotistaHistorico.CotaDia.Value;
                    quantidade = posicaoCotistaHistorico.Quantidade.Value;
                    quantidadeBloqueada = posicaoCotistaHistorico.QuantidadeBloqueada.Value;
                    valorBruto = posicaoCotistaHistorico.ValorBruto.Value;
                    valorLiquido = posicaoCotistaHistorico.ValorLiquido.Value;
                    valorIR = posicaoCotistaHistorico.ValorIR.Value;
                    valorIOF = posicaoCotistaHistorico.ValorIOF.Value;
                    valorPerformance = posicaoCotistaHistorico.ValorPerformance.Value;
                    valorRendimento = posicaoCotistaHistorico.ValorRendimento.Value;
                }

                posicaoCotistaRetorno.IdCarteira = idCarteira;
                posicaoCotistaRetorno.CotaDia = valorCota;
                posicaoCotistaRetorno.IdCotista = idCotista;
                posicaoCotistaRetorno.Quantidade = quantidade;
                posicaoCotistaRetorno.QuantidadeBloqueada = quantidadeBloqueada;
                posicaoCotistaRetorno.ValorBruto = valorBruto;
                posicaoCotistaRetorno.ValorIOF = valorIOF;
                posicaoCotistaRetorno.ValorIR = valorIR;
                posicaoCotistaRetorno.ValorLiquido = valorLiquido;
                posicaoCotistaRetorno.ValorPerformance = valorPerformance;
                posicaoCotistaRetorno.ValorRendimento = valorRendimento;
            }

            return posicaoCotistaRetorno;
        }


        /// <summary>
        /// Retorna o saldo livre do cotista, leva em conta resgates a serem convertidos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="data"></param>
        /// <param name="idPosicao"></param>                
        /// <returns></returns>
        public PosicaoCotista RetornaPosicaoLivre(int idCarteira, int idCotista, DateTime? data, int? idPosicao)
        {
            return RetornaPosicaoLivre(idCarteira, idCotista, data, idPosicao, true);
        }

        /// <summary>
        /// Retorna o saldo livre do cotista, leva em conta resgates a serem convertidos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="data"></param>
        /// <param name="idPosicao"></param>        
        /// <param name="reconstroiPosicao">Reconstroi a posi��o a partir da abertura, sensibilizando com as movimenta��es do dia</param>  
        /// <returns></returns>
        public PosicaoCotista RetornaPosicaoLivre(int idCarteira, int idCotista, DateTime? data, int? idPosicao, bool reconstroiPosicao)
        {            
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("M1 Carteira: {0} Cotista: {1} - DataHistorico: {2}", idCarteira, idCotista, data),
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);

            Carteira carteira = new Carteira();
            carteira.Query.Select(carteira.Query.TipoCota);
            carteira.Query.Where(carteira.Query.IdCarteira.Equal(idCarteira));
            carteira.Query.Load();

            Cliente cliente = new Cliente();
            cliente.Query.Select(cliente.Query.DataDia);
            cliente.Query.Where(cliente.Query.IdCliente.Equal(idCarteira));
            cliente.Query.Load();

            PosicaoCotista posicaoCotistaRetorno = new PosicaoCotista();

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.Query.Select(posicaoCotistaAberturaCollection.Query.Quantidade.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.QuantidadeBloqueada.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorBruto.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorIR.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorIOF.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorPerformance.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorLiquido.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.ValorRendimento.Sum(),
                                                        posicaoCotistaAberturaCollection.Query.CotaDia.Avg(),
                                                        posicaoCotistaAberturaCollection.Query.DataHistorico);
            posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCarteira),
                                                       posicaoCotistaAberturaCollection.Query.IdCotista.Equal(idCotista));
            posicaoCotistaAberturaCollection.Query.GroupBy(posicaoCotistaAberturaCollection.Query.DataHistorico);
            posicaoCotistaAberturaCollection.Query.OrderBy(posicaoCotistaAberturaCollection.Query.DataHistorico.Descending);

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M2",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);

            if (idPosicao.HasValue)
            {
                posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdPosicao.Equal(idPosicao.Value));
            }

            if (data.HasValue)
            {
                posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data));
            }

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M3",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);

            posicaoCotistaAberturaCollection.Query.Load();

            if (!posicaoCotistaAberturaCollection.Query.Load())
            {
                if (!reconstroiPosicao)
                    return new PosicaoCotista();
            }

            PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
            if (posicaoCotistaAberturaCollection.Count > 0)
            {
                posicaoCotistaAbertura = posicaoCotistaAberturaCollection[0];
            }

            decimal valorCota = 0;
            decimal quantidade = 0;
            decimal quantidadeBloqueada = 0;
            decimal valorBruto = 0;
            decimal valorLiquido = 0;
            decimal valorIR = 0;
            decimal valorIOF = 0;
            decimal valorPerformance = 0;
            decimal valorRendimento = 0;
            if (posicaoCotistaAbertura.es.HasData && posicaoCotistaAbertura.Quantidade.HasValue)
            {
                valorCota = posicaoCotistaAbertura.CotaDia.Value;
                quantidade = posicaoCotistaAbertura.Quantidade.Value;
                quantidadeBloqueada = posicaoCotistaAbertura.QuantidadeBloqueada.Value;
                valorBruto = posicaoCotistaAbertura.ValorBruto.Value;
                valorLiquido = posicaoCotistaAbertura.ValorLiquido.Value;                
                valorIR = posicaoCotistaAbertura.ValorIR.Value;
                valorIOF = posicaoCotistaAbertura.ValorIOF.Value;
                valorPerformance = posicaoCotistaAbertura.ValorPerformance.Value;
                valorRendimento = posicaoCotistaAbertura.ValorRendimento.Value;
            }

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M4",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            if (valorCota == 0)
            {
                HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                historicoCotaCollection.Query.Select(historicoCotaCollection.Query.CotaAbertura,
                                                     historicoCotaCollection.Query.CotaFechamento);
                historicoCotaCollection.Query.Where(historicoCotaCollection.Query.IdCarteira.Equal(idCarteira));

                if (data.HasValue)
                {
                    historicoCotaCollection.Query.Where(historicoCotaCollection.Query.Data.Equal(data.Value));
                }

                historicoCotaCollection.Query.OrderBy(historicoCotaCollection.Query.Data.Descending);
                historicoCotaCollection.Query.Load();

                if (historicoCotaCollection.Count > 0)
                {
                    if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                    {
                        valorCota = historicoCotaCollection[0].CotaAbertura.Value;
                    }
                    else if (historicoCotaCollection[0].CotaFechamento.HasValue)
                    {
                        valorCota = historicoCotaCollection[0].CotaFechamento.Value;
                    }
                }
            }

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M5",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.TipoOperacao,
                                                   operacaoCotistaCollection.Query.Quantidade,
                                                   operacaoCotistaCollection.Query.ValorBruto,
                                                   operacaoCotistaCollection.Query.ValorLiquido);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  operacaoCotistaCollection.Query.IdCotista.Equal(idCotista),
                                                  operacaoCotistaCollection.Query.DataConversao.GreaterThanOrEqual(cliente.DataDia.Value),
                                                  operacaoCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao,
                                                                                                  (byte)TipoOperacaoCotista.ResgateBruto,
                                                                                                  (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                                  (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                                  (byte)TipoOperacaoCotista.ResgateTotal));
            operacaoCotistaCollection.Query.Load();

             historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M6",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            bool resgateTotal = false;
            decimal valorAplicado = 0;
            decimal quantidadeCotasResgatadas = 0;
            decimal valorBrutoResgatado = 0;
            decimal valorLiquidoResgatado = 0;            
            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                byte tipoOperacao = operacaoCotista.TipoOperacao.Value;
                decimal quantidadeOperacao = operacaoCotista.Quantidade.Value;
                decimal valorBrutoOperacao = operacaoCotista.ValorBruto.Value;
                decimal valorLiquidoOperacao = operacaoCotista.ValorLiquido.Value;

                if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
                {
                    resgateTotal = true;
                    break;
                }
                else if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    valorAplicado += valorBrutoOperacao;
                }
                else
                {
                    if (quantidadeOperacao != 0)
                    {
                        quantidadeCotasResgatadas += quantidadeOperacao;
                    }
                    else if (valorBrutoOperacao != 0)
                    {
                        valorBrutoResgatado += valorBrutoOperacao;
                    }
                    else if (valorLiquidoOperacao != 0)
                    {
                        valorLiquidoResgatado += valorLiquidoOperacao;
                    }
                }
            }

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M7",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            //Fazer mesma coisa pra Ordem
            OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
            ordemCotistaCollection.Query.Select(ordemCotistaCollection.Query.TipoOperacao,
                                                   ordemCotistaCollection.Query.Quantidade,
                                                   ordemCotistaCollection.Query.ValorBruto,
                                                   ordemCotistaCollection.Query.ValorLiquido);
            ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  ordemCotistaCollection.Query.IdCotista.Equal(idCotista),
                                                  ordemCotistaCollection.Query.Status.In((byte)StatusOrdemCotista.Aprovado, (byte)StatusOrdemCotista.Digitado),
                                                  ordemCotistaCollection.Query.DataConversao.GreaterThanOrEqual(cliente.DataDia.Value),
                                                  ordemCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao,
                                                                                                  (byte)TipoOperacaoCotista.ResgateBruto,
                                                                                                  (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                                  (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                                  (byte)TipoOperacaoCotista.ResgateTotal));
            ordemCotistaCollection.Query.Load();
             historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M8",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);

            foreach (OrdemCotista ordemCotista in ordemCotistaCollection)
            {
                byte tipoOrdem = ordemCotista.TipoOperacao.Value;
                decimal quantidadeOrdem = ordemCotista.Quantidade.Value;
                decimal valorBrutoOrdem = ordemCotista.ValorBruto.Value;
                decimal valorLiquidoOrdem = ordemCotista.ValorLiquido.Value;

                if (tipoOrdem == (byte)TipoOperacaoCotista.ResgateTotal)
                {
                    resgateTotal = true;
                    break;
                }
                else if (tipoOrdem == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    valorAplicado += valorBrutoOrdem;
                }
                else
                {
                    if (quantidadeOrdem != 0)
                    {
                        quantidadeCotasResgatadas += quantidadeOrdem;
                    }
                    else if (valorBrutoOrdem != 0)
                    {
                        valorBrutoResgatado += valorBrutoOrdem;
                    }
                    else if (valorLiquidoOrdem != 0)
                    {
                        valorLiquidoResgatado += valorLiquidoOrdem;
                    }
                }
            }

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M9",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            decimal saldoFinal = 0;
            if (!resgateTotal)
            {
                saldoFinal = valorBruto + valorAplicado - valorBrutoResgatado - valorLiquidoResgatado - (quantidadeCotasResgatadas * valorCota);                
            }

            if (valorCota != 0)
            {
                if (valorBruto == 0)
                {
                    valorLiquido = saldoFinal;
                    quantidade = saldoFinal / valorCota;
                }
                else
                {
                    valorLiquido = (saldoFinal / valorBruto) * valorLiquido;

                    if (valorLiquido == 0)
                    {
                        valorLiquido = valorBruto;
                    }

                    valorIR = (saldoFinal / valorBruto) * valorIR;
                    valorIOF = (saldoFinal / valorBruto) * valorIOF;
                    valorPerformance = (saldoFinal / valorBruto) * valorPerformance;
                    valorRendimento = (saldoFinal / valorBruto) * valorRendimento;
                    quantidade = (saldoFinal / valorBruto) * quantidade;
                }
            }
            
            valorBruto = saldoFinal;

            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "M10",
                                            "Admin",
                                            "",
                                            "",
                                            Financial.Security.Enums.HistoricoLogOrigem.IntegracaoItau);


            posicaoCotistaRetorno.IdCarteira = idCarteira;
            posicaoCotistaRetorno.CotaDia = valorCota;
            posicaoCotistaRetorno.IdCotista = idCotista;
            posicaoCotistaRetorno.Quantidade = quantidade;
            posicaoCotistaRetorno.QuantidadeBloqueada = quantidadeBloqueada;
            posicaoCotistaRetorno.ValorBruto = valorBruto;
            posicaoCotistaRetorno.ValorIOF = valorIOF;
            posicaoCotistaRetorno.ValorIR = valorIR;
            posicaoCotistaRetorno.ValorLiquido = valorLiquido;
            posicaoCotistaRetorno.ValorPerformance = valorPerformance;
            posicaoCotistaRetorno.ValorRendimento = valorRendimento;

            return posicaoCotistaRetorno;
        }

        /// <summary>
        /// Processa a distribuição das qtdes calculadas pós operações de Ingresso e Retirada com impacto na quantidade de Cotas
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DistribuiQtdeCotasPosIngRet(int idCarteira, DateTime data, HistoricoCota historicoCota)
        {
            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            Carteira carteira = new Carteira();
            
            #region Busca as cotas por Cotista
            posicaoCotistaColl.Query.Select(posicaoCotistaColl.Query.IdCarteira,
                                            posicaoCotistaColl.Query.IdCotista,
                                            posicaoCotistaColl.Query.Quantidade.Sum());
            posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdCarteira.Equal(idCarteira));
            posicaoCotistaColl.Query.GroupBy(posicaoCotistaColl.Query.IdCarteira,
                                             posicaoCotistaColl.Query.IdCotista);
            #endregion

            if (posicaoCotistaColl.Query.Load())
            {
                carteira.LoadByPrimaryKey(idCarteira);

                decimal valorCota = (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value);
                decimal qtdeTotal = 0;
                foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
                {
                    qtdeTotal += posicaoCotista.Quantidade.Value;
                }

                foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
                {
                    decimal participacao = (posicaoCotista.Quantidade.Value / qtdeTotal);
                    decimal qtdeParticipacao = participacao * historicoCota.QuantidadeFechamento.Value;
                    decimal qtdeMovimentada = qtdeParticipacao - posicaoCotista.Quantidade.Value;

                    OperacaoCotista operacaoCotista = operacaoCotistaColl.AddNew();
                    operacaoCotista.IdCarteira = posicaoCotista.IdCarteira.Value;
                    operacaoCotista.IdCotista = posicaoCotista.IdCotista.Value;
                    operacaoCotista.Quantidade = Math.Abs(qtdeMovimentada);
                    operacaoCotista.TipoOperacao = (qtdeMovimentada < 0 ? (byte)TipoOperacaoCotista.ResgateCotasEspecial : (byte)TipoOperacaoCotista.AplicacaoCotasEspecial);
                    operacaoCotista.ValorBruto = operacaoCotista.Quantidade.Value * valorCota;
                    operacaoCotista.CotaOperacao = valorCota;
                    operacaoCotista.DataAgendamento = data;
                    operacaoCotista.DataConversao = data;
                    operacaoCotista.DataLiquidacao = data;
                    operacaoCotista.DataOperacao = data;
                    operacaoCotista.DataRegistro = data;
                    operacaoCotista.Fonte = (byte)FonteOperacaoCotista.InclusaoRetirada;
                }

                operacaoCotistaColl.Save();
            }
        }
    }
}