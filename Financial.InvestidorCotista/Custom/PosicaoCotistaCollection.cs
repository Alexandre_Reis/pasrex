﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.YMF;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.InvestidorCotista
{
    public partial class PosicaoCotistaCollection : esPosicaoCotistaCollection
    {
        // Construtor
        // Cria uma nova PosicaoCotistaCollection com os dados de PosicaoCotistaHistoricoCollection
        // Todos do dados de PosicaoCotistaHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoCotistaCollection(PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection)
        {
            for (int i = 0; i < posicaoCotistaHistoricoCollection.Count; i++)
            {
                //
                PosicaoCotista p = new PosicaoCotista();

                // Para cada Coluna de PosicaoCotistaHistorico copia para PosicaoECotista
                foreach (esColumnMetadata colPosicaoHistorico in posicaoCotistaHistoricoCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoCotistaHistoricoMetadata.ColumnNames.DataHistorico)
                    {
                        esColumnMetadata colPosicaoCotista = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoCotistaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null)
                        {
                            p.SetColumn(colPosicaoCotista.Name, posicaoCotistaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Remove uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        public void RemoveColumn(string columnName)
        {
            if (this.Table != null && this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Remove(columnName);
            }
        }

        /// <summary>
        /// Verifica se a coluna existe na collection
        /// </summary>
        /// <param name="columnName"></param>
        public bool ExistColumn(string columnName)
        {
            if (this.Table != null)
            {
                return this.Table.Columns.Contains(columnName);
            }
            return false;
        }

        /// <summary>
        /// Deleta todas as posições do idCarteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        public void DeletaPosicaoCotista(int idCarteira)
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoCotista, a partir de PosicaoCotistaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoCotistaHistorico(int idCliente, DateTime dataHistorico)
        {
            #region Copia de posicaoCotistaHistorico para PosicaoCotista
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoCotista ON ");
            sqlBuilder.Append("INSERT INTO PosicaoCotista (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoCotistaHistorico WHERE DataHistorico = " + "'" + dataHistorico.ToString("yyyyMMdd") + "' AND IdCarteira = " + idCliente;
            int count = 0;

            PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
            int columnCount = posicaoCotistaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoCotista in posicaoCotistaHistorico.es.Meta.Columns)
            {
                count++;

                if (colPosicaoCotista.Name == PosicaoCotistaHistoricoMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoCotista.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoCotista.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoCotista OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }


        /// <summary>
        /// Método de inserção em PosicaoCotista, a partir de PosicaoCotistaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoCotistaAbertura(int idCliente, DateTime dataAbertura)
        {
            #region Copia de posicaoCotistaHistorico para PosicaoCotista
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoCotista ON ");
            sqlBuilder.Append("INSERT INTO PosicaoCotista (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoCotistaAbertura WHERE DataHistorico = " + "'" + dataAbertura.ToString("yyyyMMdd") + "' AND IdCarteira = " + idCliente;
            int count = 0;

            PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
            int columnCount = posicaoCotistaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoCotista in posicaoCotistaAbertura.es.Meta.Columns)
            {
                count++;

                if (colPosicaoCotista.Name == PosicaoCotistaAberturaMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoCotista.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoCotista.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoCotista OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com todos os campos de PosicaoCotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        public void BuscaPosicaoCotistaCompleta(int idCarteira)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdCotista, ValorBruto.Sum().
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaAgrupadoEnquadra(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCotista, this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0))
                 .GroupBy(this.Query.IdCotista);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, CotaDia, Quantidade, ValorBruto.
        /// Query.Quantidade.GreaterThan(0).
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaCotaDia(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.CotaDia, this.Query.Quantidade, this.Query.ValorBruto, this.Query.DataConversao)
                 .Where(this.Query.IdCarteira == idCarteira, this.Query.Quantidade.GreaterThan(0));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoCotistaCollection.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotista(int idCarteira)
        {
            return this.BuscaPosicaoCotista(idCarteira, false);
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoCotistaCollection.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotista(int idCarteira, bool incluirZerada)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira == idCarteira);

            if (!incluirZerada)
                this.Query.Where(this.Query.Quantidade.NotEqual(0));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoCotistaCollection.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaRendimentoOrdenado(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy(this.Query.IdCotista.Ascending, this.Query.ValorRendimento.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela data de conversão.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idCotista"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaFIFO(int idCarteira, int idCotista)
        {
            return BuscaPosicaoCotistaFIFO(idCarteira, idCotista, null);        
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela data de conversão.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idCotista"></param>   
        /// <param name="lstPosicoesJaCarregadas"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaFIFO(int idCarteira, int idCotista, List<int> lstPosicoesJaCarregadas)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade, this.Query.QuantidadeBloqueada,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.DataUltimoCortePfee,
                         this.Query.PosicaoIncorporada)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.IdCotista == idCotista,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy(this.Query.DataConversao.Ascending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena descendente pela data de conversão.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idCotista"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaLIFO(int idCarteira, int idCotista)
        {
            return BuscaPosicaoCotistaLIFO(idCarteira, idCotista, null);
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena descendente pela data de conversão.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <param name="idCotista"></param>   
        /// <param name="lstPosicoesJaCarregadas"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaLIFO(int idCarteira, int idCotista, List<int> lstPosicoesJaCarregadas)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade, this.Query.QuantidadeBloqueada,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.DataUltimoCortePfee,
                         this.Query.PosicaoIncorporada)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.IdCotista == idCotista,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy(this.Query.DataConversao.Descending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela soma de IR + IOF.
        /// </summary>
        /// <param name="idCarteira"></param>       
        /// <param name="idCotista"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaMenorImposto(int idCarteira, int idCotista)
        {
            return BuscaPosicaoCotistaMenorImposto(idCarteira, idCotista, null);
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela soma de IR + IOF.
        /// </summary>
        /// <param name="idCarteira"></param>       
        /// <param name="idCotista"></param>   
        /// <param name="lstPosicoesJaCarregadas"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaMenorImposto(int idCarteira, int idCotista, List<int> lstPosicoesJaCarregadas)
        {
            #region Montagem do OrderBy ->> colunas = "(ValorIR + ValorIOF) / Quantidade)"
            StringBuilder colunas = new StringBuilder();
            string valorIr = PosicaoCotistaMetadata.ColumnNames.ValorIR;
            string valorIOF = PosicaoCotistaMetadata.ColumnNames.ValorIOF;
            string quantidade = PosicaoCotistaMetadata.ColumnNames.Quantidade;
            colunas = colunas.Append("<( [").Append(valorIr).Append("] + [").Append(valorIOF).Append("] )").Append("/ [")
                             .Append(quantidade).Append("] As Impostos>");
            #endregion

            this.QueryReset();
            this.Query
                 .Select(colunas.ToString(), this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade, this.Query.QuantidadeBloqueada,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.DataUltimoCortePfee,
                         this.Query.PosicaoIncorporada)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.IdCotista == idCotista,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy("Impostos", esOrderByDirection.Ascending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoCotistaCollection com os campos IdPosicao, IdOperacao, Quantidade, ValorPerformance, DataUltimoCortePfee.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCarteira"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoCotistaPerformance(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, 
                         this.Query.IdOperacao, 
                         this.Query.Quantidade, 
                         this.Query.ValorPerformance,
                         this.Query.DataUltimoCortePfee)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0));

            bool retorno = this.Query.Load();

            return retorno;
        }

        public MappingPosicaoFinancialYMF ImportaPosicaoNotaYMF(Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicaoNotaArray,
            DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF)
        {
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF = new MappingPosicaoFinancialYMF();

            foreach (Financial.Interfaces.Import.YMF.PosicaoNotaYMF posicaoNota in posicaoNotaArray)
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                if (posicaoCotista.ImportaPosicaoNotaYMF(posicaoNota, dataImplantacao, mappingOperacaoFinancialYMF))
                {
                    posicaoCotista.Save();

                    //A linha abaixo não existe pro Histórico e pra Abertura:
                    mappingPosicaoFinancialYMF.Add(posicaoNota.IdNota, posicaoCotista.IdPosicao.Value);
                }
            }
            return mappingPosicaoFinancialYMF;
        }


        /// <summary>
        /// /// Calcula quantidade que foi convertida, porém não houve liquidação financeira (Resgate)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaPendenciaLiquidacao(int idCliente, DateTime data)
        {
            DetalheResgateCotistaCollection detalheColl = new DetalheResgateCotistaCollection();
            DetalheResgateCotistaQuery detalheQuery = new DetalheResgateCotistaQuery("detalhe");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("operacao");

            detalheQuery.Select(detalheQuery.Quantidade.Sum(), detalheQuery.ValorLiquido.Sum(), detalheQuery.IdPosicaoResgatada);
            detalheQuery.InnerJoin(operacaoCotistaQuery).On(detalheQuery.IdOperacao.Equal(operacaoCotistaQuery.IdOperacao));
            detalheQuery.Where(operacaoCotistaQuery.DataLiquidacao.GreaterThan(data));
            detalheQuery.GroupBy(detalheQuery.IdPosicaoResgatada);

            List<DetalheResgateCotista> lstDetalheResgateCotista = new List<DetalheResgateCotista>();
            if (detalheColl.Load(detalheQuery) && detalheColl.Count > 0)
                lstDetalheResgateCotista = (List<DetalheResgateCotista>)detalheColl;
            else
                return;

            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
            if (!posicaoCotistaColl.BuscaPosicaoCotista(idCliente, true))
                return;

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
            {
                decimal qtdePendente = 0;
                decimal valorPendente = 0;
                int idPosicao = posicaoCotista.IdPosicao.Value;

                DetalheResgateCotista detalheResgateCotista = lstDetalheResgateCotista.Find(delegate(DetalheResgateCotista x) { return x.IdPosicaoResgatada == idPosicao; });
                if (detalheResgateCotista != null && posicaoCotista.IdPosicao > 0)
                {
                    qtdePendente = Convert.ToDecimal(detalheResgateCotista.GetColumn("Quantidade"));
                    valorPendente = Convert.ToDecimal(detalheResgateCotista.GetColumn("ValorLiquido"));
                }

                posicaoCotista.QtdePendenteLiquidacao = qtdePendente;
                posicaoCotista.ValorPendenteLiquidacao = valorPendente;
            }

            posicaoCotistaColl.Save();
        }

        public SuitabilityPerfilInvestidor SelecionaNovoPerfilSuitability(int idCotista, int idCarteira, decimal valorOperacao)
        {
            return this.SelecionaNovoPerfilSuitability(idCotista, idCarteira, valorOperacao, null);
        }

        public SuitabilityPerfilInvestidor SelecionaNovoPerfilSuitability(int idCotista, int idCarteira, decimal valorOperacao, DateTime? dataPosicao)
        {
            SuitabilityPerfilInvestidor suitabilityPerfilInvestidorRetorno  = new SuitabilityPerfilInvestidor();

            List<string> listaPerfilApurado = new List<string>();

            decimal valorTotalCarteira = this.valorTotalPosicao(idCotista) + valorOperacao;

            if (valorTotalCarteira == 0)
                return suitabilityPerfilInvestidorRetorno;

            //Seleciona o perfil da carteira da operacao
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            int idPerfilCarteira = carteira.PerfilRisco.GetValueOrDefault(0);

            List<PerfilPosicao> listaPerfilPosicao;
            //Seleciona totais por perfil - na posicao
            if(dataPosicao.HasValue)
                listaPerfilPosicao = this.definePerfilPosicao(idCotista, dataPosicao.Value);
            else
                listaPerfilPosicao = this.definePerfilPosicao(idCotista, idPerfilCarteira, valorOperacao);

            //Seleciona Regras de apuração
            SuitabilityApuracaoRiscoQuery suitabilityApuracaoRiscoQuery = new SuitabilityApuracaoRiscoQuery("SAR");
            SuitabilityPerfilInvestidorQuery suitabilityPerfilInvestidorQuery = new SuitabilityPerfilInvestidorQuery("SPI");
            suitabilityApuracaoRiscoQuery.Select(suitabilityApuracaoRiscoQuery);
            suitabilityApuracaoRiscoQuery.InnerJoin(suitabilityPerfilInvestidorQuery).On(suitabilityApuracaoRiscoQuery.PerfilCarteira.Equal(suitabilityPerfilInvestidorQuery.IdPerfilInvestidor));
            suitabilityApuracaoRiscoQuery.Where(suitabilityPerfilInvestidorQuery.Nivel.GreaterThan(0));
            suitabilityApuracaoRiscoQuery.OrderBy(suitabilityPerfilInvestidorQuery.Nivel.Ascending);

            SuitabilityApuracaoRiscoCollection suitabilityApuracaoRiscoCollection = new SuitabilityApuracaoRiscoCollection();
            suitabilityApuracaoRiscoCollection.Load(suitabilityApuracaoRiscoQuery);

            if (suitabilityApuracaoRiscoCollection.Count == 0)
                return suitabilityPerfilInvestidorRetorno;

            //Verifica em qual regra a posicao consolidada por perfil se encaixa
            int? totalRegrasEnquadradas = null;
            int idPerfilPesquisa = 0;
            bool regra = false;
            foreach (SuitabilityApuracaoRisco suitabilityApuracaoRisco in suitabilityApuracaoRiscoCollection)
            {

                if (!suitabilityApuracaoRisco.PerfilCarteira.Equals(idPerfilPesquisa))
                {
                    if (totalRegrasEnquadradas.HasValue && totalRegrasEnquadradas.Equals(this.totalRegrasPerfil(idPerfilPesquisa)))
                    {
                        if (suitabilityPerfilInvestidorRetorno.Nivel.HasValue)
                        {
                            SuitabilityPerfilInvestidor suitabilityPerfilInvestidorNivel = new SuitabilityPerfilInvestidor();
                            suitabilityPerfilInvestidorNivel.LoadByPrimaryKey(idPerfilPesquisa);
                            if (suitabilityPerfilInvestidorRetorno.Nivel < suitabilityPerfilInvestidorNivel.Nivel)
                                suitabilityPerfilInvestidorRetorno = suitabilityPerfilInvestidorNivel;
                        }
                        else
                        {
                            suitabilityPerfilInvestidorRetorno.LoadByPrimaryKey(idPerfilPesquisa);
                        }
                        break;
                    }
                    idPerfilPesquisa = suitabilityApuracaoRisco.PerfilCarteira.Value;
                    totalRegrasEnquadradas = 0;
                }

                decimal valorPosicao = 0;

                List<SuitabilityPerfilInvestidor> listaPerfilFundo = suitabilityApuracaoRisco.ListaPerfilRiscoFundo();

                //Verifica se perfil da carteira se encaixa na regra
                foreach (SuitabilityPerfilInvestidor suitabilityPerfilInvestidor in listaPerfilFundo)
                {
                    foreach (PerfilPosicao perfilPosicao in listaPerfilPosicao)
                    {
                        if (perfilPosicao.idPerfil.Equals(suitabilityPerfilInvestidor.IdPerfilInvestidor))
                            valorPosicao += perfilPosicao.valor;
                    }
                }

                decimal percentualPosicao = (valorPosicao / valorTotalCarteira) * 100;

                if (suitabilityApuracaoRisco.Criterio.Equals((int)SuitabilityCriterio.Igual))
                {
                    if (percentualPosicao == suitabilityApuracaoRisco.Percentual)
                    {
                        listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        totalRegrasEnquadradas += 1;
                    }
                }
                if (suitabilityApuracaoRisco.Criterio.Equals((int)SuitabilityCriterio.MaiorIgual))
                {
                    if (percentualPosicao >= suitabilityApuracaoRisco.Percentual)
                    {
                        listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        totalRegrasEnquadradas += 1;
                    }
                }
                if (suitabilityApuracaoRisco.Criterio.Equals((int)SuitabilityCriterio.MenorIgual))
                {
                    if (percentualPosicao <= suitabilityApuracaoRisco.Percentual)
                    {
                        listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        totalRegrasEnquadradas += 1;
                    }
                }
            }

            if (totalRegrasEnquadradas.Equals(this.totalRegrasPerfil(idPerfilPesquisa)))
            {
                if (suitabilityPerfilInvestidorRetorno.Nivel.HasValue)
                {
                    SuitabilityPerfilInvestidor suitabilityPerfilInvestidorNivel = new SuitabilityPerfilInvestidor();
                    suitabilityPerfilInvestidorNivel.LoadByPrimaryKey(idPerfilPesquisa);
                    if (suitabilityPerfilInvestidorRetorno.Nivel < suitabilityPerfilInvestidorNivel.Nivel)
                        suitabilityPerfilInvestidorRetorno = suitabilityPerfilInvestidorNivel;
                }
                else
                {
                    suitabilityPerfilInvestidorRetorno.LoadByPrimaryKey(idPerfilPesquisa);
                }
            }

            return suitabilityPerfilInvestidorRetorno;
        }

        protected int totalRegrasPerfil(int idPerfil)
        {
            SuitabilityApuracaoRiscoCollection suitabilityApuracaoRiscoRegras = new SuitabilityApuracaoRiscoCollection();
            suitabilityApuracaoRiscoRegras.Query.Select(suitabilityApuracaoRiscoRegras.Query.PerfilCarteira.Count());
            suitabilityApuracaoRiscoRegras.Query.Where(suitabilityApuracaoRiscoRegras.Query.PerfilCarteira.Equal(idPerfil));
            suitabilityApuracaoRiscoRegras.Query.Load();

            return suitabilityApuracaoRiscoRegras[0].PerfilCarteira.GetValueOrDefault(0);

        }

        protected decimal valorTotalPosicao(int idCotista)
        {
            decimal valorPosicao = 0;

            //Seleciona total do fundo na posicao
            this.QueryReset();
            this.Query.Select(this.Query.ValorBruto.Sum());
            this.Query.Where(this.Query.IdCotista.Equal(idCotista));
            this.Query.Load();

            valorPosicao = this[0].ValorBruto.GetValueOrDefault(0);
            this.QueryReset();

            //Seleciona total do fundo de operações que ainda não são posições.
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.ValorBruto.Sum());
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdOperacao.NotIn(this.Query.Select(this.Query.IdOperacao).Where(this.Query.IdCotista.Equal(idCotista))));
            operacaoCotistaCollection.Query.Load();

            valorPosicao += operacaoCotistaCollection[0].ValorBruto.GetValueOrDefault(0);

            return valorPosicao;

        }

        protected List<PerfilPosicao> definePerfilPosicao(int idCotista, int idPerfilCarteiraOperacao, decimal valorOperacao)
        {
            List<PerfilPosicao> listaPerfilPosicao = new List<PerfilPosicao>();

            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PC");
            posicaoCotistaQuery.Select(carteiraQuery.PerfilRisco.As("idPerfil"), posicaoCotistaQuery.ValorBruto.Sum());
            posicaoCotistaQuery.InnerJoin(carteiraQuery).On(posicaoCotistaQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCotista.Equal(idCotista));
            posicaoCotistaQuery.GroupBy(carteiraQuery.PerfilRisco);
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Load(posicaoCotistaQuery);

            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                if (posicaoCotista.GetColumn("idPerfil").ToString() != "")
                {
                    PerfilPosicao perfilPosicao = new PerfilPosicao();
                    perfilPosicao.idPerfil = Convert.ToInt16(posicaoCotista.GetColumn("idPerfil"));
                    perfilPosicao.valor = posicaoCotista.ValorBruto.GetValueOrDefault(0);
                    listaPerfilPosicao.Add(perfilPosicao);
                }
            }

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");
            operacaoCotistaQuery.Select(carteiraQuery.PerfilRisco.As("idPerfil"), operacaoCotistaQuery.ValorBruto.Sum());
            operacaoCotistaQuery.InnerJoin(carteiraQuery).On(operacaoCotistaQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.Equal(idCotista));
            operacaoCotistaQuery.GroupBy(carteiraQuery.PerfilRisco);
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                if (operacaoCotista.GetColumn("idPerfil").ToString() != "")
                {
                    bool find = false;
                    foreach (PerfilPosicao perfilPosicao in listaPerfilPosicao)
                    {
                        if (perfilPosicao.idPerfil.Equals(Convert.ToInt16(operacaoCotista.GetColumn("idPerfil"))))
                        {
                            perfilPosicao.valor += operacaoCotista.ValorBruto.Value;
                            find = true;
                            break;
                        }
                    }

                    if (!find)
                    {
                        PerfilPosicao perfilPosicao = new PerfilPosicao();
                        perfilPosicao.idPerfil = Convert.ToInt16(operacaoCotista.GetColumn("idPerfil"));
                        perfilPosicao.valor = operacaoCotista.ValorBruto.GetValueOrDefault(0);
                        listaPerfilPosicao.Add(perfilPosicao);
                    }
                }
            }

            if (idPerfilCarteiraOperacao > 0)
            {
                bool find = false;
                foreach (PerfilPosicao perfilPosicao in listaPerfilPosicao)
                {
                    if (perfilPosicao.idPerfil.Equals(idPerfilCarteiraOperacao))
                    {
                        perfilPosicao.valor += valorOperacao;
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    PerfilPosicao perfilPosicao = new PerfilPosicao();
                    perfilPosicao.idPerfil = idPerfilCarteiraOperacao;
                    perfilPosicao.valor = valorOperacao;
                    listaPerfilPosicao.Add(perfilPosicao);
                }
            }

            return listaPerfilPosicao;

        }

        protected List<PerfilPosicao> definePerfilPosicao(int idCotista, DateTime dataPosicao)
        {
            List<PerfilPosicao> listaPerfilPosicao = new List<PerfilPosicao>();

            CarteiraSuitabilityHistoricoQuery carteiraSuitabilityHistorico = new CarteiraSuitabilityHistoricoQuery("CSH");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("PCH");
            posicaoCotistaHistoricoQuery.Select(carteiraSuitabilityHistorico.IdPerfilRisco.As("idPerfil"), posicaoCotistaHistoricoQuery.ValorBruto.Sum());
            posicaoCotistaHistoricoQuery.InnerJoin(carteiraSuitabilityHistorico).On(posicaoCotistaHistoricoQuery.IdCarteira.Equal(carteiraSuitabilityHistorico.IdCarteira));
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.Equal(idCotista));
            posicaoCotistaHistoricoQuery.GroupBy(carteiraSuitabilityHistorico.IdPerfilRisco);
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                if (posicaoCotistaHistorico.GetColumn("idPerfil").ToString() != "")
                {
                    PerfilPosicao perfilPosicao = new PerfilPosicao();
                    perfilPosicao.idPerfil = Convert.ToInt16(posicaoCotistaHistorico.GetColumn("idPerfil"));
                    perfilPosicao.valor = posicaoCotistaHistorico.ValorBruto.GetValueOrDefault(0);
                    listaPerfilPosicao.Add(perfilPosicao);
                }
            }

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");
            operacaoCotistaQuery.Select(carteiraSuitabilityHistorico.IdPerfilRisco.As("idPerfil"), operacaoCotistaQuery.ValorBruto.Sum());
            operacaoCotistaQuery.InnerJoin(carteiraSuitabilityHistorico).On(operacaoCotistaQuery.IdCarteira.Equal(carteiraSuitabilityHistorico.IdCarteira));
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.Equal(idCotista));
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.LessThanOrEqual(dataPosicao));
            operacaoCotistaQuery.GroupBy(carteiraSuitabilityHistorico.IdPerfilRisco);
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                if (operacaoCotista.GetColumn("idPerfil").ToString() != "")
                {
                    bool find = false;
                    foreach (PerfilPosicao perfilPosicao in listaPerfilPosicao)
                    {
                        if (perfilPosicao.idPerfil.Equals(Convert.ToInt16(operacaoCotista.GetColumn("idPerfil"))))
                        {
                            perfilPosicao.valor += operacaoCotista.ValorBruto.Value;
                            find = true;
                            break;
                        }
                    }

                    if (!find)
                    {
                        PerfilPosicao perfilPosicao = new PerfilPosicao();
                        perfilPosicao.idPerfil = Convert.ToInt16(operacaoCotista.GetColumn("idPerfil"));
                        perfilPosicao.valor = operacaoCotista.ValorBruto.GetValueOrDefault(0);
                        listaPerfilPosicao.Add(perfilPosicao);
                    }
                }
            }

            return listaPerfilPosicao;

        }


        protected class PerfilPosicao
        {
            public int idPerfil;
            public decimal valor;
        }

    }
}
