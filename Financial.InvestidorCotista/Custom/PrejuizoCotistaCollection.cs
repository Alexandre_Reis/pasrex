﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo;
using System.Collections;

namespace Financial.InvestidorCotista
{
	public partial class PrejuizoCotistaCollection : esPrejuizoCotistaCollection
	{
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Construtor
        /// Cria uma nova PrejuizoCotistaCollection com os dados de PrejuizoCotistaHistoricoCollection
        /// Todos os dados de PrejuizoCotistaHistoricoCollection são copiados exceto a dataHistorico
        /// </summary>
        /// <param name="prejuizoCotistaHistoricoCollection"></param>
        public PrejuizoCotistaCollection(PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection) 
        {
            for (int i = 0; i < prejuizoCotistaHistoricoCollection.Count; i++) {
                //
                PrejuizoCotista c = new PrejuizoCotista();

                // Para cada Coluna de PrejuizoCotistaHistorico copia para PrejuizoCotista
                foreach (esColumnMetadata colPrejuizoCotistaHistorico in prejuizoCotistaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPrejuizoCotistaHistorico.PropertyName != PrejuizoCotistaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPrejuizoCotista = c.es.Meta.Columns.FindByPropertyName(colPrejuizoCotistaHistorico.PropertyName);
                        if (prejuizoCotistaHistoricoCollection[i].GetColumn(colPrejuizoCotistaHistorico.Name) != null) {
                            c.SetColumn(colPrejuizoCotista.Name, prejuizoCotistaHistoricoCollection[i].GetColumn(colPrejuizoCotistaHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(c);
            }
        }

        /// <summary>
        /// Carrega o objeto PrejuizoCotistaCollection com os campos IdCarteira, IdCotista, Data, ValorPrejuizo.
        /// </summary>        
        /// <param name="data"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>   
        /// <param name="idCotista"></param>   
        public void BuscaPrejuizoCompensar(DateTime data, int idAgenteAdministrador, int tipoTributacao, int idCotista)
        {
            #region Carrega em <ListaCarteira> os fundos que sejam do mesmo administrador e mesma categoria tributária
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.BuscaCarteira(idAgenteAdministrador, tipoTributacao);
            ArrayList listaCarteira = new ArrayList();
            for (int i = 0; i < carteiraCollection.Count; i++)
            {
                Carteira carteira = (Carteira)carteiraCollection[i];
                listaCarteira.Add(carteira.IdCarteira.Value);
            }
            #endregion

            if (carteiraCollection.Count == 0)
            {
                return;
            }

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.In(listaCarteira.ToArray()) &
                        this.Query.IdCotista.Equal(idCotista) &
                        this.Query.ValorPrejuizo.NotEqual(0) &
                        this.query.Data.LessThanOrEqual(data) &
                        (this.Query.DataLimiteCompensacao.GreaterThanOrEqual(data) | this.Query.DataLimiteCompensacao.IsNull())); 

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>        
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>   
        public void BuscaPrejuizoCompensar(DateTime data, int idCarteira, int idCotista)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.IdCotista.Equal(idCotista),
                        this.Query.ValorPrejuizo.NotEqual(0));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PrejuizoCotistaCollection pelo idCarteira
        /// </summary>
        /// <param name="idCarteira"></param>
        public void BuscaPrejuizoCotistaCompleto(int idCarteira) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }

        public void ImportaRendimentoCompensarYMF(Financial.Interfaces.Import.YMF.RendimentoCompensarYMF[] rendimentosCompensarYMF)
        {

            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();

            List<int> idCarteiraImportadaList = new List<int>();
            List<int> idCotistaImportadoList = new List<int>();
            List<DateTime> dataImportadaList = new List<DateTime>();

            foreach (Financial.Interfaces.Import.YMF.RendimentoCompensarYMF rendimentoCompensarYMF in rendimentosCompensarYMF)
            {
                PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
                if (prejuizoCotista.ImportaRendimentoCompensarYMF(rendimentoCompensarYMF))
                {
                    idCarteiraImportadaList.Add(prejuizoCotista.IdCarteira.Value);
                    idCotistaImportadoList.Add(prejuizoCotista.IdCotista.Value);
                    dataImportadaList.Add(prejuizoCotista.Data.Value);

                    //Se já possuimos um prejuizo nesta data para este cotista e fundo, 
                    //vamos apenas somar com o valor importado sem anexar à collection
                    PrejuizoCotista prejuizoCotistaExistente = prejuizoCotistaCollection.FindByPrimaryKey(
                        prejuizoCotista.IdCotista.Value, prejuizoCotista.IdCarteira.Value, prejuizoCotista.Data.Value);
                    if (prejuizoCotistaExistente == null)
                    {
                        prejuizoCotistaCollection.AttachEntity(prejuizoCotista);
                    }
                    else
                    {
                        prejuizoCotistaExistente.ValorPrejuizo += prejuizoCotista.ValorPrejuizo;
                    }
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                #region Deleta PrejuizoCotista
                PrejuizoCotistaCollection prejuizoCotistaCollectionDeletar;

                for (int i = 0; i < idCarteiraImportadaList.Count; i++)
                {
                    prejuizoCotistaCollectionDeletar = new PrejuizoCotistaCollection();

                    prejuizoCotistaCollectionDeletar.Query
                        .Where(prejuizoCotistaCollectionDeletar.Query.IdCarteira == idCarteiraImportadaList[i],
                          prejuizoCotistaCollectionDeletar.Query.IdCotista == idCotistaImportadoList[i],
                          prejuizoCotistaCollectionDeletar.Query.Data == dataImportadaList[i]);

                    prejuizoCotistaCollectionDeletar.Query.Load();
                    prejuizoCotistaCollectionDeletar.MarkAllAsDeleted();
                    prejuizoCotistaCollectionDeletar.Save();
                }
                #endregion

                // Salva PrejuizoCotistas importados
                prejuizoCotistaCollection.Save();

                scope.Complete();
            }

        }

    }
}
