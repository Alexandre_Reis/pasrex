/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/01/2015 11:40:28
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InvestidorCotista
{
	public partial class ColagemOperResgPosCotistaCollection : esColagemOperResgPosCotistaCollection
	{
        public void DeletaColagemOperResgPosAfetada(int idCarteira, DateTime data)
        {
            #region Objetos
            ColagemOperResgPosCotistaCollection colaOperResgPosAfetadaColl = new ColagemOperResgPosCotistaCollection();
            #endregion

            #region Carrega Posicao dos detalhes colados
            colaOperResgPosAfetadaColl.Query.Where(colaOperResgPosAfetadaColl.Query.IdCarteira.Equal(idCarteira)
                                             & colaOperResgPosAfetadaColl.Query.DataReferencia.Equal(data));

            if (colaOperResgPosAfetadaColl.Query.Load())
            {
                colaOperResgPosAfetadaColl.MarkAllAsDeleted();
                colaOperResgPosAfetadaColl.Save();
            }
            #endregion
        }
	}
}
