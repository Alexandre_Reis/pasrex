﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           3/5/2007 13:46:44
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.YMF;

namespace Financial.InvestidorCotista
{
    public partial class DetalheResgateCotistaCollection : esDetalheResgateCotistaCollection
    {
        public void ImportaResgateYMF(Financial.Interfaces.Import.YMF.ResgateYMF[] resgatesYMF, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF)
        {

            DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
            OperacaoCotistaCollection operacaoCotistaConsolidadaCollection = new OperacaoCotistaCollection();

            List<int> idOperacaoImportadaList = new List<int>();
            List<int> idPosicaoResgatadaImportadaList = new List<int>();

            int countResgate = 0;
            foreach (Financial.Interfaces.Import.YMF.ResgateYMF resgateYMF in resgatesYMF)
            {
                DetalheResgateCotista detalheResgateCotista = new DetalheResgateCotista();
                if (detalheResgateCotista.ImportaResgateYMF(resgateYMF, mappingOperacaoFinancialYMF, mappingPosicaoFinancialYMF, countResgate))
                {
                    idOperacaoImportadaList.Add(detalheResgateCotista.IdOperacao.Value);
                    idPosicaoResgatadaImportadaList.Add(detalheResgateCotista.IdPosicaoResgatada.Value);

                    detalheResgateCotistaCollection.AttachEntity(detalheResgateCotista);
                }
                countResgate++;
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                // Salva DetalheResgateCotistas importados
                detalheResgateCotistaCollection.Save();


                //É necessário realizar a consolidação (somatoria) de determinados campos para o OperacaoCotista
                for (int i = 0; i < idOperacaoImportadaList.Count; i++)
                {
                    OperacaoCotista operacaoCotistaConsolidada = new OperacaoCotista();
                    operacaoCotistaConsolidada.LoadByPrimaryKey(idOperacaoImportadaList[i]);
                    operacaoCotistaConsolidada.ConsolidaDetalheResgateCotista();
                    operacaoCotistaConsolidadaCollection.AttachEntity(operacaoCotistaConsolidada);
                }

                //Salva operações de cotista com os valores consolidados
                operacaoCotistaConsolidadaCollection.Save();

                scope.Complete();
            }

        }
    }
}
