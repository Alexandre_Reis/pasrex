﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista.Enums;
using Financial.Security;
using log4net;
using Financial.CRM.Enums;
using Financial.CRM;
using Financial.Fundo;

namespace Financial.InvestidorCotista
{
    public partial class CotistaCollection : esCotistaCollection
    {
        /// <summary>
        /// Carrega o objeto CotistaCollection com os campos IdCotista, Nome, Apelido.
        /// Filtra por StatusAtivo.Equal(StatusAtivoCotista.Ativo).
        /// this.Load(CotistaQuery);
        /// </summary>
        /// <param name="login"></param>  
        public void BuscaCotistasComAcesso(string login)
        {
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");

            cotistaQuery.Select(cotistaQuery, cotistaQuery.Apelido);
            cotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            cotistaQuery.InnerJoin(usuarioQuery).On(permissaoCotistaQuery.IdUsuario == usuarioQuery.IdUsuario);
            cotistaQuery.Where(usuarioQuery.Login == login,
                               cotistaQuery.StatusAtivo.Equal(StatusAtivoCotista.Ativo));
            cotistaQuery.OrderBy(cotistaQuery.Apelido.Ascending);

            this.Load(cotistaQuery);
        }

        /// <summary>
        /// Carrega o objeto CotistaCollection com os campos IdCotista, Nome, Apelido.
        /// Filtra por StatusAtivo.Equal(StatusAtivoCotista.Ativo).        
        /// </summary>
        /// <param name="login"></param>
        /// <param name="tipoPessoa"></param>
        public void BuscaCotistasComAcesso(string login, TipoPessoa tipoPessoa)
        {
            //
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PessoaQuery pessoaQuery = new PessoaQuery("Pe");
            PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");

            cotistaQuery.Select(cotistaQuery, cotistaQuery.Apelido);
            cotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            cotistaQuery.InnerJoin(usuarioQuery).On(permissaoCotistaQuery.IdUsuario == usuarioQuery.IdUsuario);
            cotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            cotistaQuery.Where(usuarioQuery.Login == login,
                               cotistaQuery.StatusAtivo.Equal(StatusAtivoCotista.Ativo),
                               pessoaQuery.Tipo == (int)tipoPessoa);

            cotistaQuery.OrderBy(cotistaQuery.Apelido.Ascending);

            this.Load(cotistaQuery);
        }

        /// <summary>
        /// Busca Todos os Cotistas de uma determinada Carteira que 
        /// tiveram/tem Posicao ou que Movimentaram algo em um determinado intervalo de tempo
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicioPosicao"></param>
        /// <param name="dataFimPosicao"></param>
        /// <param name="dataInicioOperacao">Data Inicio a ser considerado nas movimentações</param>
        /// <param name="dataFimOperacao">Data Fim a ser considerado nas movimentações</param>
        /// <returns>Lista com os idCotistas pertencentes a uma Determinada Carteira</returns>
        public List<int> BuscaCotistasDaCarteira(int idCarteira, DateTime dataInicioPosicao, DateTime dataFimPosicao,
                                                        DateTime dataInicioOperacao, DateTime dataFimOperacao, string login)
        {

            Usuario u = new Usuario();
            if (!u.BuscaUsuario(login))
            {
                return new List<int>();
            }

            int idUsuario = u.IdUsuario.Value;

            PosicaoCotistaAberturaCollection p = new PosicaoCotistaAberturaCollection();
            p.Query.es.Distinct = true;
            p.Query.Select(p.Query.IdCotista)
                   .Where(p.Query.IdCarteira == idCarteira &
                          p.Query.DataHistorico.Between(dataInicioPosicao, dataFimPosicao) &
                          p.Query.Quantidade != 0);
            //
            p.Query.Load();
            //
            OperacaoCotistaCollection o = new OperacaoCotistaCollection();
            o.Query.es.Distinct = true;
            o.Query.Select(o.Query.IdCotista)
                   .Where(o.Query.IdCarteira == idCarteira &
                          o.Query.DataOperacao.Between(dataInicioOperacao, dataFimOperacao) &
                           o.Query.Quantidade != 0);
            //
            o.Query.Load();
            //
            List<int> listaIdCotistas = new List<int>();

            PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
            permissaoCotistaCollection.Query.Where(permissaoCotistaCollection.Query.IdUsuario.Equal(idUsuario));
            permissaoCotistaCollection.Query.Load();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in p)
            {
                PermissaoCotista permissao = permissaoCotistaCollection.FindByPrimaryKey(idUsuario, posicaoCotistaAbertura.IdCotista.Value);

                if (permissao != null)
                {
                    listaIdCotistas.Add(posicaoCotistaAbertura.IdCotista.Value);
                }
            }

            foreach (OperacaoCotista operacaoCotista in o)
            {
                PermissaoCotista permissao = permissaoCotistaCollection.FindByPrimaryKey(idUsuario, operacaoCotista.IdCotista.Value);
                if (permissao != null)
                {
                    if (!listaIdCotistas.Contains(operacaoCotista.IdCotista.Value))
                    {
                        listaIdCotistas.Add(operacaoCotista.IdCotista.Value);
                    }
                }
            }

            // Ordena em Ordem Crescente
            listaIdCotistas.Sort(delegate(int x, int y)
            {
                return x.CompareTo(y);
            }
                        );

            return listaIdCotistas;
        }

        /// <summary>
        /// Busca Todas as Carteiras de um determinado Cotista em que ele 
        /// tinha/teve Posicao ou onde Movimentou algo
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <returns>Lista com os idCarteiras em que um cotista Aplica</returns>
        public List<int> BuscaCarteirasDoCotista(int idCotista)
        {
            //
            PosicaoCotistaAberturaCollection p = new PosicaoCotistaAberturaCollection();
            p.Query.es.Distinct = true;
            p.Query.Select(p.Query.IdCarteira)
                   .Where(p.Query.IdCotista == idCotista);
            //
            p.Query.Load();
            //
            OperacaoCotistaCollection o = new OperacaoCotistaCollection();
            o.Query.es.Distinct = true;
            o.Query.Select(o.Query.IdCarteira)
                   .Where(o.Query.IdCotista == idCotista);
            //
            o.Query.Load();
            //
            List<int> listaIdCarteiras = new List<int>();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in p)
            {
                if (posicaoCotistaAbertura.IdCarteira.Value != idCotista)
                {
                    listaIdCarteiras.Add(posicaoCotistaAbertura.IdCarteira.Value);
                }
            }

            foreach (OperacaoCotista operacaoCotista in o)
            {
                if (!listaIdCarteiras.Contains(operacaoCotista.IdCarteira.Value) && operacaoCotista.IdCarteira.Value != idCotista)
                {
                    listaIdCarteiras.Add(operacaoCotista.IdCarteira.Value);
                }
            }

            // Ordena em Ordem Crescente
            listaIdCarteiras.Sort(delegate(int x, int y)
            {
                return x.CompareTo(y);
            }
                        );

            return listaIdCarteiras;
        }

        /// <summary>
        /// Busca Todas as Carteiras de um determinado Cotista em que ele 
        /// tinha/teve Posicao ou onde Movimentou algo considerando um intervalo de tempo
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicioPosicao"></param>
        /// <param name="dataFimPosicao"></param>
        /// <param name="dataInicioOperacao">Data Inicio a ser considerado nas movimentações</param>
        /// <param name="dataFimOperacao">Data Fim a ser considerado nas movimentações</param>
        /// <returns>Lista com os idCarteiras em que um cotista Aplica/Aplicou</returns>
        public List<int> BuscaCarteirasDoCotista(int idCotista, DateTime dataInicioPosicao, DateTime dataFimPosicao,
                                                        DateTime dataInicioOperacao, DateTime dataFimOperacao)
        {
            //
            PosicaoCotistaAberturaCollection p = new PosicaoCotistaAberturaCollection();
            p.Query.es.Distinct = true;
            p.Query.Select(p.Query.IdCarteira)
                   .Where(p.Query.IdCotista == idCotista &&
                          p.Query.DataHistorico.Between(dataInicioPosicao, dataFimPosicao));
            //
            p.Query.Load();
            //
            OperacaoCotistaCollection o = new OperacaoCotistaCollection();
            o.Query.es.Distinct = true;
            o.Query.Select(o.Query.IdCarteira)
                   .Where(o.Query.IdCotista == idCotista &&
                          o.Query.DataOperacao.Between(dataInicioOperacao, dataFimOperacao));
            //
            o.Query.Load();
            //
            List<int> listaIdCarteiras = new List<int>();

            foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in p)
            {
                listaIdCarteiras.Add(posicaoCotistaAbertura.IdCarteira.Value);
            }

            foreach (OperacaoCotista operacaoCotista in o)
            {
                if (!listaIdCarteiras.Contains(operacaoCotista.IdCarteira.Value))
                {
                    listaIdCarteiras.Add(operacaoCotista.IdCarteira.Value);
                }
            }

            // Ordena em Ordem Crescente
            listaIdCarteiras.Sort(delegate(int x, int y)
            {
                return x.CompareTo(y);
            }
                        );

            return listaIdCarteiras;
        }

        public Cotista BuscaCotistaPorCodigoInterface(string codigoInterface)
        {
            Cotista cotista = null;
            this.QueryReset();
            this.Query.Where(this.Query.CodigoInterface == codigoInterface);
            if (this.Load(this.Query))
            {
                cotista = this[0];
            }

            return cotista;

        }

        public void ImportaCotistaYMF(Financial.Interfaces.Import.YMF.CotistaYMF[] cotistasYMF)
        {
            foreach (Financial.Interfaces.Import.YMF.CotistaYMF cotistaYMF in cotistasYMF)
            {
                string codigoCotistaYMF = cotistaYMF.CdCotista; 
                Cotista cotistaExistente = new CotistaCollection().BuscaCotistaPorCodigoInterface(cotistaYMF.CdCotista);
                Cotista cotista = new Cotista();
                if (cotistaExistente == null)
                {
                    cotista.ImportaCotistaYMF(cotistaYMF);
                    try
                    {
                        cotista.Save();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Cotista relacionado à pessoa existente: " + cotistaYMF.CdCotista + ": " + cotista.IdCotista + " " + cotista.Nome);
                    }
                    //Armazenar no cotista as infos de tipo cotista em coluna virtual para ser utilizada pela importacao de TipoCotista
                    //Essa operacao tem que ser apos o Save senao ocorre erro no ES
                    cotista.GuardaIdTipoCotistaYMF(cotistaYMF);

                    this.AttachEntity(cotista);
                }
            }
            /*
            List<string> codigoCotistaYMFImportadoList = new List<string>();

            foreach (Financial.Interfaces.Import.YMF.CotistaYMF cotistaYMF in cotistasYMF)
            {
                Cotista cotista = new Cotista();
                cotista.ImportaCotistaYMF(cotistaYMF);

                codigoCotistaYMFImportadoList.Add(cotistaYMF.CdCotista.Trim());
                this.AttachEntity(cotista);

            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                #region Deleta Cotista por CodigoCotistaYMF
                CotistaCollection cotistaCollectionDeletar = new CotistaCollection();
                CotistaCollection cotistaRepository = new CotistaCollection();

                for (int i = 0; i < codigoCotistaYMFImportadoList.Count; i++)
                {
                    Cotista cotista = cotistaRepository.BuscaCotistaPorCodigoInterface(codigoCotistaYMFImportadoList[i]);
                    if (cotista != null)
                    {
                        cotista.MarkAsDeleted();
                        cotista.Save();
                    }
                }
                #endregion

                // Salva Cotistas importados
                this.Save();

                scope.Complete();
            }*/

        }

        public void ImportaTipoCotistaYMF(Financial.Interfaces.Import.YMF.TipoCotistaYMF[] tiposCotistaYMF)
        {
            //Vamos ajustar alguns campos da colecao de cotistas importados com base na tabela de tipos de cotistas da YMF
            Dictionary<int, Financial.Interfaces.Import.YMF.TipoCotistaYMF> dictionaryTiposCotistaYMF = new Dictionary<int, Financial.Interfaces.Import.YMF.TipoCotistaYMF>();
            foreach (Financial.Interfaces.Import.YMF.TipoCotistaYMF tipoCotistaYMF in tiposCotistaYMF)
            {
                dictionaryTiposCotistaYMF.Add(tipoCotistaYMF.IdTipoCotista, tipoCotistaYMF);
            }

            foreach (Cotista cotista in this)
            {
                cotista.ImportaTipoCotistaYMF(dictionaryTiposCotistaYMF);
            }
        }

        public Cotista BuscaCotistaPorCPFCNPJ(string cpfcnpj)
        {
            Cotista cotista = null;

            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCPFCNPJ(cpfcnpj);

            if (pessoa != null)
            {
                cotista = new Cotista();
                if (!cotista.LoadByPrimaryKey(pessoa.IdPessoa.Value))
                {
                    cotista = null;
                }
            }

            return cotista;
        }

        public CotistaCollection BuscaCotistasPorCPFCNPJ(string cpfcnpj)
        {
            CotistaCollection cotistas = null;

            PessoaCollection pessoas = new PessoaCollection().BuscaPessoasPorCPFCNPJ(cpfcnpj);

            if (pessoas != null && pessoas.Count > 0)
            {
                cotistas = new CotistaCollection();
                foreach (Pessoa pessoa in pessoas)
                {
                    Cotista cotista = new Cotista();
                    cotista.LoadByPrimaryKey(pessoa.IdPessoa.Value);
                    cotistas.AttachEntity(cotista);
                }
            }

            return cotistas;
        }
    }
}
