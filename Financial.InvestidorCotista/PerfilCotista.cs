﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Net.Configuration;

using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Util;
using Financial.Fundo;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Common.Enums;
using Financial.InvestidorCotista.Enums;

namespace Financial.InvestidorCotista.Perfil
{
    /// <summary>
    /// Classe para controle de Perfil do Cotista
    /// </summary>
    public class ControllerPerfilCotista
    {

        public void recusaPerfil(int idCotista)
        {
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            Pessoa pessoa = cotista.UpToPessoaByIdPessoa;

            PessoaSuitability pessoaSuitability = new PessoaSuitability();
            pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value);
            pessoaSuitability.UltimaAlteracao = DateTime.Now;
            pessoaSuitability.Recusa = "S";
            pessoaSuitability.Perfil = null;
            pessoaSuitability.Save();

            string login = HttpContext.Current.User.Identity.Name;

            //Atualiza Historico
            PessoaSuitabilityHistorico.createPessoaSuitabilityHistorico(pessoaSuitability, TipoOperacaoBanco.Alteração, login);

            #region Envio de Email de Recusa
            string mensagem = selecionaMensagemEmail(8);

            string subject = "Suitability - Termo de Recusa";

            string body = "Nome do Cotista:" + cotista.Nome + "\nCPF/CNPJ:" + pessoa.Cpfcnpj + "\n \n";
            body += mensagem;

            List<string> toList = new List<string>();

            if (!String.IsNullOrEmpty(pessoa.Email))
            {
                toList.Add(pessoa.Email);
            }

            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            if (suitabilityParametrosWorkflow.LoadByPrimaryKey(1) && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.ResponsavelEmail))
            {
                toList.Add(suitabilityParametrosWorkflow.ResponsavelEmail);
            }

            Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            string from = settings.Smtp.From.ToString();
            string to = toList.Count > 0 ? String.Join(",", toList.ToArray()) : "";
            if (!string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from))
            {
                enviaEmail(body, subject, from, to);
            }
            #endregion

        }

        public string updatePerfil(int idCotista)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(carregaPerfil(idCotista));
        }

        public PerfilCotista carregaPerfil(int idCotista)
        {

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            PerfilCotista perfilCotista = new PerfilCotista();
            perfilCotista.Validade = "";
            perfilCotista.DtUltAtualizacao = "";
            perfilCotista.Perfil = "";
            perfilCotista.DentroValidade = false;
            perfilCotista.Definicao = "";

            PessoaSuitability pessoaSuitability = new PessoaSuitability();
            bool possuiPessoaSuitability = pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value);

            bool dispensado = pessoaSuitability.Dispensado == "S";
            perfilCotista.Dispensado = dispensado ? "Sim" : "Não";


            if (possuiPessoaSuitability)
            {
                //Encontrar um fundo em que o cotista tenha posicao
                try
                {
                    PosicaoCotistaCollection posicaoCotistaCol = new PosicaoCotistaCollection();
                    posicaoCotistaCol.Query.Where(posicaoCotistaCol.Query.IdCotista.Equal(idCotista));
                    posicaoCotistaCol.Query.es.Top = 1;
                    if (posicaoCotistaCol.Load(posicaoCotistaCol.Query) && posicaoCotistaCol != null && posicaoCotistaCol.Count > 0)
                    {
                        SuitabilityPerfilInvestidor suitabilityNovoPerfilSuitability = new PosicaoCotistaCollection().SelecionaNovoPerfilSuitability(idCotista, posicaoCotistaCol[0].IdCarteira.Value, 0);
                        perfilCotista.PerfilInvestimentos = suitabilityNovoPerfilSuitability.Perfil;
                    }

                }
                catch
                {
                }

                if (pessoaSuitability.UltimaAlteracao.HasValue)
                {
                    perfilCotista.DtUltAtualizacao = pessoaSuitability.UltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm");
                }

                if (dispensado)
                {
                    if (pessoaSuitability.Dispensa.HasValue)
                    {
                        SuitabilityTipoDispensa tipoDispensa = new SuitabilityTipoDispensa();
                        tipoDispensa.LoadByPrimaryKey(pessoaSuitability.Dispensa.Value);
                        perfilCotista.TipoDispensa = tipoDispensa.Descricao;
                    }

                    if (pessoaSuitability.UltimaAlteracao.HasValue)
                    {
                        perfilCotista.DtUltAtualizacao = pessoaSuitability.UltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm");
                        DateTime validade = this.retornaValidade(pessoaSuitability.UltimaAlteracao.Value);
                        perfilCotista.DentroValidade = DateTime.Now <= validade;
                        perfilCotista.Validade = validade.ToString("dd/MM/yyyy");
                    }

                    return perfilCotista;
                }

                if (pessoaSuitability.Recusa != null && pessoaSuitability.Recusa.Equals("S"))
                {
                    perfilCotista.Status = SuitabilityStatusResposta.Recusado.ToString();
                    perfilCotista.Perfil = "Cliente recusou preenchimento";
                }
                else
                {
                    if (pessoaSuitability.Perfil.HasValue)
                    {
                        DateTime validade = this.retornaValidade(pessoaSuitability.UltimaAlteracao.Value);
                        perfilCotista.DentroValidade = DateTime.Now <= validade;
                        perfilCotista.Validade = validade.ToString("dd/MM/yyyy");

                        SuitabilityParametroPerfilCollection suitabilityParametroPerfilCollection = new SuitabilityParametroPerfilCollection();
                        suitabilityParametroPerfilCollection.Query.Where(suitabilityParametroPerfilCollection.Query.IdValidacao.Equal(pessoaSuitability.IdValidacao));
                        suitabilityParametroPerfilCollection.Query.Where(suitabilityParametroPerfilCollection.Query.PerfilInvestidor.Equal(pessoaSuitability.Perfil.Value));
                        suitabilityParametroPerfilCollection.Query.Load();
                        if (suitabilityParametroPerfilCollection.Count > 0)
                        {
                            SuitabilityParametroPerfil suitabilityParametroPerfil = suitabilityParametroPerfilCollection[0];

                            SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                            suitabilityPerfilInvestidor.LoadByPrimaryKey(suitabilityParametroPerfil.PerfilInvestidor.Value);

                            perfilCotista.Perfil = suitabilityPerfilInvestidor.Perfil;
                            perfilCotista.Definicao = suitabilityParametroPerfil.Definicao;
                        }
                        else
                        {
                            perfilCotista.Perfil = "Indefinido.";
                            perfilCotista.Definicao = "Indefinido.";
                        }
                    }
                    else
                    {
                        perfilCotista.Perfil = "Indefinido.";
                        perfilCotista.Definicao = "Indefinido.";
                    }
                }
            }

            return perfilCotista;

        }

        //public void aceitePerfil(int idCotista, List<Resposta> respostas)
        //{
        //    DateTime data = DateTime.Now;

        //    foreach (Resposta resposta in respostas)
        //    {
        //        SuitabilityRespostaCotistaDetalhe suitabilityRespostaCotistaDetalhe = new SuitabilityRespostaCotistaDetalhe();
        //        suitabilityRespostaCotistaDetalhe.Data = data;
        //        suitabilityRespostaCotistaDetalhe.IdCotista = Convert.ToInt32(resposta.idCotista);
        //        suitabilityRespostaCotistaDetalhe.IdOpcao = Convert.ToInt32(resposta.idOpcao);
        //        suitabilityRespostaCotistaDetalhe.IdQuestao = Convert.ToInt32(resposta.idQuestao);
        //        suitabilityRespostaCotistaDetalhe.Save();
        //    }

        //    PerfilCotista perfilCotista = apuraPerfil(respostas);

        //    //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
        //    //para selecao das informações de suitability do cadastro de cliente
        //    Cotista cotista = new Cotista();
        //    cotista.LoadByPrimaryKey(idCotista);

        //    PessoaSuitability pessoaSuitability = new PessoaSuitability();
        //    pessoaSuitability.LoadByPrimaryKey(cotista.UpToPessoa.IdPessoa.Value);

        //    pessoaSuitability.Perfil = Convert.ToInt32(perfilCotista.IdPerfil);
        //    pessoaSuitability.UltimaAlteracao = data;
        //    pessoaSuitability.Recusa = "N";
        //    pessoaSuitability.Save();

        //    //Atualiza Historico
        //    PessoaSuitabilityHistorico.createPessoaSuitabilityHistorico(pessoaSuitability, TipoOperacaoBanco.Alteração);

        //    #region Envio de Email de Recusa
        //    string subject = "Suitability - Novo Perfil de Investidor";

        //    string body = "Nome do Cotista:" + cotista.Nome +
        //                  "\nCPF/CNPJ:" + cotista.UpToPessoa.Cpfcnpj +
        //                  "\nPerfil do Investidor:" + perfilCotista.Perfil +
        //                  "\nData e Hora do registro:" + perfilCotista.DtUltAtualizacao;

        //    List<string> toList = new List<string>();

        //    if (!String.IsNullOrEmpty(cotista.UpToPessoa.Email))
        //    {
        //        toList.Add(cotista.UpToPessoa.Email);
        //    }

        //    SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
        //    if (suitabilityParametrosWorkflow.LoadByPrimaryKey(1) && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.ResponsavelEmail))
        //    {
        //        toList.Add(suitabilityParametrosWorkflow.ResponsavelEmail);
        //    }

        //    Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        //    MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        //    string from = settings.Smtp.From.ToString();
        //    string to = toList.Count > 0 ? String.Join(",", toList.ToArray()) : "";
        //    if (!string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from))
        //    {
        //        enviaEmail(body, subject, from, to);
        //    }
        //    #endregion

        //}

        /// <summary>
        /// Overload que tem o usuário
        /// </summary>
        /// <param name="idCotista"></param>
        /// <param name="respostas"></param>
        /// <param name="login"></param>
        public void aceitePerfil(int idCotista, List<Resposta> respostas, string login) {
            DateTime data = DateTime.Now;

            foreach (Resposta resposta in respostas) {
                SuitabilityRespostaCotistaDetalhe suitabilityRespostaCotistaDetalhe = new SuitabilityRespostaCotistaDetalhe();
                suitabilityRespostaCotistaDetalhe.Data = data;
                suitabilityRespostaCotistaDetalhe.IdCotista = Convert.ToInt32(resposta.idCotista);
                suitabilityRespostaCotistaDetalhe.IdOpcao = Convert.ToInt32(resposta.idOpcao);
                suitabilityRespostaCotistaDetalhe.IdQuestao = Convert.ToInt32(resposta.idQuestao);
                suitabilityRespostaCotistaDetalhe.Save();
            }

            PerfilCotista perfilCotista = apuraPerfil(respostas);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            PessoaSuitability pessoaSuitability = new PessoaSuitability();
            pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value);

            pessoaSuitability.Perfil = Convert.ToInt32(perfilCotista.IdPerfil);
            pessoaSuitability.UltimaAlteracao = data;
            pessoaSuitability.Recusa = "N";
            pessoaSuitability.Save();

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);

            //Atualiza Historico com o usuario
            PessoaSuitabilityHistorico.createPessoaSuitabilityHistorico(pessoaSuitability, TipoOperacaoBanco.Alteração, login);

            #region Envio de Email de Recusa
            string subject = "Suitability - Novo Perfil de Investidor";

            string body = "Nome do Cotista:" + cotista.Nome +
                          "\nCPF/CNPJ:" + pessoa.Cpfcnpj +
                          "\nPerfil do Investidor:" + perfilCotista.Perfil +
                          "\nData e Hora do registro:" + perfilCotista.DtUltAtualizacao;

            List<string> toList = new List<string>();

            if (!String.IsNullOrEmpty(cotista.UpToPessoaByIdPessoa.Email)) 
            {
                toList.Add(cotista.UpToPessoaByIdPessoa.Email);
            }

            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            if (suitabilityParametrosWorkflow.LoadByPrimaryKey(1) && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.ResponsavelEmail)) {
                toList.Add(suitabilityParametrosWorkflow.ResponsavelEmail);
            }

            Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            string from = settings.Smtp.From.ToString();
            string to = toList.Count > 0 ? String.Join(",", toList.ToArray()) : "";
            if (!string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from)) {
                enviaEmail(body, subject, from, to);
            }
            #endregion

        }

        public void dispensaPerfil(int idCotista, int idDispensa)
        {

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            PessoaSuitability pessoaSuitability = new PessoaSuitability();
            pessoaSuitability.LoadByPrimaryKey(idCotista);

            bool temDispensa = idDispensa > 0;
            pessoaSuitability.Dispensado = temDispensa ? "S" : "N";
            pessoaSuitability.Dispensa = temDispensa ? idDispensa : (int?)null;
            pessoaSuitability.UltimaAlteracao = DateTime.Now;
            pessoaSuitability.IdPessoa = idCotista;

            pessoaSuitability.Save();

            SuitabilityTipoDispensa tipoDispensa = pessoaSuitability.UpToSuitabilityTipoDispensaByDispensa;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);

            //ENVIAR EMAIL
            #region Envio de Email de Dispensa
            string mensagem = selecionaMensagemEmail(6);

            string subject = "Suitability - Novo status de dispensa de controle de suitability";

            string body = "Nome do Cotista: " + cotista.Nome +
                          "\nCPF/CNPJ: " + pessoa.Cpfcnpj +
                          ("\n\nStatus de Dispensa: " + (temDispensa ? "Sim" : "Não")) +
                          (temDispensa ? ("\nTipo de Dispensa: " + tipoDispensa.Descricao) : "") +
                          "\n\n" + mensagem +
                          "\n\nData e Hora do registro: " + pessoaSuitability.UltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm");

            List<string> toList = new List<string>();

            if (!String.IsNullOrEmpty(pessoa.Email))
            {
                toList.Add(pessoa.Email);
            }
           
            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            if (suitabilityParametrosWorkflow.LoadByPrimaryKey(1) && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.ResponsavelEmail))
            {
                toList.Add(suitabilityParametrosWorkflow.ResponsavelEmail);
            }

            Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            string from = settings.Smtp.From.ToString();
            string to = toList.Count > 0 ? String.Join(",", toList.ToArray()) : "";
            if (!string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from))
            {
                enviaEmail(body, subject, from, to);
            }

            #endregion


        }

        public PerfilCotista apuraPerfil(List<Resposta> respostas)
        {
            PerfilCotista perfilCotista = new PerfilCotista(SuitabilityStatusResposta.Efetuado.ToString());

            SuitabilityQuestao suitabilityQuestao = new SuitabilityQuestao();
            SuitabilityOpcao suitabilityOpcao = new SuitabilityOpcao();

            int ponto = 0;
            int fator = 0;
            int total = 0;

            foreach (Resposta resposta in respostas)
            {
                suitabilityQuestao.QueryReset();
                suitabilityOpcao.QueryReset();

                if (suitabilityQuestao.LoadByPrimaryKey(Convert.ToInt32(resposta.idQuestao)))
                {
                    fator = suitabilityQuestao.Fator.GetValueOrDefault(0);
                }
                else
                {
                    fator = 0;
                }

                if (suitabilityOpcao.LoadByPrimaryKey(Convert.ToInt32(resposta.idOpcao)))
                {
                    ponto = suitabilityOpcao.Pontos.GetValueOrDefault(0);
                }
                else
                {
                    ponto = 0;
                }

                total += fator * ponto;
            }

            SuitabilityParametroPerfilCollection suitabilityParametroPerfilCollection = new SuitabilityParametroPerfilCollection();
            suitabilityParametroPerfilCollection.Query.Where(suitabilityParametroPerfilCollection.Query.FaixaPontuacao.LessThanOrEqual(total));
            suitabilityParametroPerfilCollection.Query.OrderBy(suitabilityParametroPerfilCollection.Query.FaixaPontuacao.Descending);
            suitabilityParametroPerfilCollection.Query.es.Top = 1;
            if (suitabilityParametroPerfilCollection.Query.Load())
            {
                if (suitabilityParametroPerfilCollection.Count > 0)
                {
                    SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                    suitabilityPerfilInvestidor.LoadByPrimaryKey(suitabilityParametroPerfilCollection[0].PerfilInvestidor.Value);

                    perfilCotista.Definicao = suitabilityParametroPerfilCollection[0].Definicao;
                    perfilCotista.Perfil = suitabilityPerfilInvestidor.Perfil;
                    perfilCotista.IdPerfil = suitabilityPerfilInvestidor.IdPerfilInvestidor.Value.ToString();
                    perfilCotista.Status = SuitabilityStatusResposta.Efetuado.ToString();
                    perfilCotista.DtUltAtualizacao = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    perfilCotista.Validade = this.retornaValidade(DateTime.Now).ToString("dd/MM/yyyy");

                }
            }

            return perfilCotista;

        }

        protected void enviaEmail(string body, string subject, string from, string to)
        {
            EmailUtil emailUtil = new EmailUtil();
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                emailUtil.SendMail(body, subject, from, to);
            }
        }

        public DateTime retornaValidade(DateTime data)
        {
            DateTime validade = data;

            SuitabilityParametrosWorkflowCollection suitabilityParametrosWorkflowCollection = new SuitabilityParametrosWorkflowCollection();
            suitabilityParametrosWorkflowCollection.LoadAll();
            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = suitabilityParametrosWorkflowCollection[0];

            if (suitabilityParametrosWorkflow.ValorPeriodo.HasValue && suitabilityParametrosWorkflow.GrandezaPeriodo.HasValue)
            {
                int valorPeriodo = suitabilityParametrosWorkflow.ValorPeriodo.Value;
                if (suitabilityParametrosWorkflow.GrandezaPeriodo.Equals((int)SuitabilityGrandezaTempo.Ano))
                    validade = data.AddYears(valorPeriodo);
                if (suitabilityParametrosWorkflow.GrandezaPeriodo.Equals((int)SuitabilityGrandezaTempo.Mes))
                    validade = data.AddMonths(valorPeriodo);
                if (suitabilityParametrosWorkflow.GrandezaPeriodo.Equals((int)SuitabilityGrandezaTempo.Dia))
                    validade = data.AddDays(valorPeriodo);

            }

            return validade;
        }

        protected string selecionaMensagemEmail(int idMensagem)
        {
            string retorno = "";

            SuitabilityEventos suitabilityEventos = new SuitabilityEventos();
            suitabilityEventos.LoadByPrimaryKey(idMensagem);

            if (suitabilityEventos.IdMensagem.HasValue)
            {
                SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
                suitabilityMensagens.LoadByPrimaryKey(suitabilityEventos.IdMensagem.Value);

                retorno = suitabilityMensagens.Descricao;

            }

            return retorno;
        }

        public string selecionaMensagemEvento(int idEvento, int idCotista)
        {
            SuitabilityEventos suitabilityEventos = new SuitabilityEventos();
            suitabilityEventos.LoadByPrimaryKey(idEvento);

            string output = "";

            if (suitabilityEventos.IdMensagem.HasValue)
            {
                SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
                suitabilityMensagens.LoadByPrimaryKey(suitabilityEventos.IdMensagem.Value);

                output = suitabilityMensagens.Descricao + "|" + suitabilityMensagens.IdMensagem + "|" + suitabilityMensagens.ControlaConcordancia + "|" + idEvento;

                //Se for mensagem do evento - Termo de Ciência de Risco e Adesão ao Fundo - Verifica se já foi respondido
                if (idEvento == 16)
                {
                    //Verifica se cliente já respondeu sobreo evento - Termo de Ciência de Risco e Adesão ao Fundo
                    SuitabilityLogMensagensCollection suitabilityLogMensagensCollection = new SuitabilityLogMensagensCollection();
                    suitabilityLogMensagensCollection.Query.Where(suitabilityLogMensagensCollection.Query.IdCotista.Equal(idCotista));
                    suitabilityLogMensagensCollection.Query.Where(suitabilityLogMensagensCollection.Query.IdMensagem.Equal(suitabilityEventos.IdMensagem.Value));
                    suitabilityLogMensagensCollection.Query.Load();
                    if (suitabilityLogMensagensCollection.Count > 0)
                        output = "";
                }

            }
            return output;
        }

        public void enviaTermoInadequacao(int idCotista, int idCarteira, SuitabilityPerfilInvestidor suitabilityPerfilCotista, SuitabilityPerfilInvestidor suitabilityPerfilPosicao)
        {
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);

            string mensagem = this.selecionaMensagemEvento(19, idCotista).Split('|')[0];

            //TODO Colocar tratamento para selecionar pessoa quando efetuar Merge para 1.1.N(GPS)

            #region Envio de Email de Recusa
            string subject = "Suitability - Termo de Inadequação, Ausência e Desatualização";

            string body = "Nome do Cotista:" + cotista.Nome +
                          "\nCPF/CNPJ:" + pessoa.Cpfcnpj +
                          "\nNome do Fundo:" + carteira.Nome +
                          "\nCNPJ do Fundo:" + pessoa.Cpfcnpj +
                          "\nPerfil do Cotista:" + suitabilityPerfilCotista.Perfil +
                          "\nPerfil da Posição:" + suitabilityPerfilPosicao.Perfil +
                          "\nStatus de Enquadramento: Desenquadrado" +
                          "\n" + mensagem;

            List<string> toList = new List<string>();

            if (!String.IsNullOrEmpty(pessoa.Email))
            {
                toList.Add(pessoa.Email);
            }

            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            if (suitabilityParametrosWorkflow.LoadByPrimaryKey(1) && !String.IsNullOrEmpty(suitabilityParametrosWorkflow.ResponsavelEmail))
            {
                toList.Add(suitabilityParametrosWorkflow.ResponsavelEmail);
            }

            Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            string from = settings.Smtp.From.ToString();
            string to = toList.Count > 0 ? String.Join(",", toList.ToArray()) : "";
            if (!string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from))
            {
                enviaEmail(body, subject, from, to);
            }
            #endregion
        }

        public string verificaEnquadramentoSuitability(int idCotista, int idCarteira, byte tipoOperacao, decimal valorOperacaoParam, 
            out SuitabilityPerfilInvestidor suitabilityPerfilPosicao, out SuitabilityPerfilInvestidor suitabilityPerfilCotista, out bool desenquadrado)
        {
            desenquadrado = false;
            string retorno = "";
            suitabilityPerfilPosicao = new SuitabilityPerfilInvestidor();
            suitabilityPerfilCotista = new SuitabilityPerfilInvestidor();

            ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

            //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
            //para selecao das informações de suitability do cadastro de cliente
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            PessoaSuitability pessoaSuitability = new PessoaSuitability();
            if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
            {
                //Se cliente dispensado, não faz nada
                if (pessoaSuitability.Dispensado.Equals("S") || pessoaSuitability.Recusa.Equals("S"))
                    return retorno;

                //Define o valo da operacao
                decimal valorOperacao = defineValorOperacao(idCotista, idCarteira, tipoOperacao, valorOperacaoParam);

                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                suitabilityPerfilPosicao = posicaoCotistaCollection.SelecionaNovoPerfilSuitability(idCotista, idCarteira, valorOperacao);

                if (!suitabilityPerfilPosicao.Nivel.HasValue)
                {
                    retorno = "Não foi possível definir perfil da posição. Verifique se existem regras definidas para o cotista.";
                    return retorno;
                }

                suitabilityPerfilCotista.LoadByPrimaryKey(pessoaSuitability.Perfil.Value);

                SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
                suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

                if (suitabilityParametrosWorkflow.VerificaEnquadramento.Equals(SuitabilityVerificaEnquadramento.Exato))
                {
                    if (!suitabilityPerfilCotista.Nivel.Equals(suitabilityPerfilPosicao.Nivel))
                    {
                        desenquadrado = true;
                        retorno = controllerPerfilCotista.selecionaMensagemEvento(13, idCotista);

                        if (retorno == "")
                        {
                            retorno = "Não existe mensagem cadastrada para evento - Desenquadramento - Alteração de perfil. Favor cadastrar mensagem para o evento.";
                        }
                        else
                        {
                            retorno += "|" + suitabilityPerfilCotista.IdPerfilInvestidor + "|" + suitabilityPerfilPosicao.IdPerfilInvestidor + "|" + VirtualPathUtility.ToAbsolute("~/");
                        }
                    }
                }
                else
                {
                    if (suitabilityPerfilCotista.Nivel.Value < suitabilityPerfilPosicao.Nivel.Value)
                    {
                        desenquadrado = true;
                        retorno = controllerPerfilCotista.selecionaMensagemEvento(13, idCotista);

                        if (retorno == "")
                        {
                            retorno = "Não existe mensagem cadastrada para evento - Desenquadramento - Alteração de perfil. Favor cadastrar mensagem para o evento.";
                        }
                        else
                        {
                            retorno += "|" + suitabilityPerfilCotista.IdPerfilInvestidor + "|" + suitabilityPerfilPosicao.IdPerfilInvestidor + "|" + VirtualPathUtility.ToAbsolute("~/");
                        }
                    }
                }

            }
            return retorno;

        }

        protected decimal defineValorOperacao(int idCotista, int idCarteira, byte tipoOperacao, decimal valorOperacaoParam)
        {
            
            decimal valorOperacao = 0;

            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.Quantidade.Sum(),
                                                 posicaoCotistaCheck.Query.QuantidadeBloqueada.Sum(),
                                                 posicaoCotistaCheck.Query.CotaDia.Avg());
                posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                posicaoCotistaCheck.Query.IdCotista.Equal(idCotista),
                                                posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                posicaoCotistaCheck.Query.Load();

                decimal quantidadePosicao = posicaoCotistaCheck.Quantidade.Value;
                decimal quantidadeBloqueadaPosicao = posicaoCotistaCheck.QuantidadeBloqueada.Value;
                decimal valorCota = posicaoCotistaCheck.CotaDia.Value;
                decimal valorCheck = Math.Round(quantidadePosicao * valorCota, 2) - Math.Round(quantidadeBloqueadaPosicao * valorCota, 2);

                if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto || tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                {
                    valorOperacao = valorOperacaoParam * (-1);
                }
                else if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas || tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial)
                {
                    valorOperacao = valorOperacaoParam * valorCota * (-1);
                }
                else
                {
                    valorOperacao = valorCheck * (-1);
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao ||
                tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                valorOperacao = valorOperacaoParam;
            }

            return valorOperacao;
        }

        public class Resposta
        {
            public string idCotista;
            public string idQuestao;
            public string idOpcao;
        }

        public class PerfilCotista
        {
            public string Status;
            public string DtUltAtualizacao;
            public string Validade;
            public string IdPerfil;
            public string Perfil;
            public string Definicao;
            public string Dispensado;
            public string TipoDispensa;
            public string Alertas;
            public bool DentroValidade;
            public string PerfilInvestimentos;

            public PerfilCotista()
            {
                this.Dispensado = "Não";
            }

            public PerfilCotista(string Status, string DtUltAtualizacao, string Validade, string IdPerfil, string Perfil, string Definicao, string Dispensado, string TipoDispensa, string Alertas)
            {
                this.Status = Status;
                this.DtUltAtualizacao = DtUltAtualizacao;
                this.Validade = Validade;
                this.IdPerfil = IdPerfil;
                this.Perfil = Perfil;
                this.Definicao = Definicao;
                this.Dispensado = Dispensado;
                this.TipoDispensa = TipoDispensa;
                this.Alertas = Alertas;
            }

            public PerfilCotista(string Status)
            {
                this.Dispensado = "Não";
                this.Status = Status;
            }

        }

        public class LogMensagem
        {
            public string IdCotista;
            public string IdMensagem;
            public string Mensagem;
            public string Resposta;
        }

        public class Mensagem
        {
            public string IdMensagem;
            public string Descricao;
            public string ControlaConcordancia;
        }

    }
}


