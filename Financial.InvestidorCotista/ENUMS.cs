﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using System.Reflection;

namespace Financial.InvestidorCotista.Enums
{
public enum StatusAtivoCotista
    {
        Ativo = 1,
        Inativo = 2
    }

    public enum TipoOperacaoCotista
    {
        [StringValue("Aplicação")]
        Aplicacao = 1,

        [StringValue("Resgate Bruto")]
        ResgateBruto = 2,

        [StringValue("Resgate Líquido")]
        ResgateLiquido = 3,

        [StringValue("Resgate Cotas")]
        ResgateCotas = 4,

        [StringValue("Resgate Total")]
        ResgateTotal = 5,

        [StringValue("Aplicacão Cotas Especial")]
        AplicacaoCotasEspecial = 10,

        [StringValue("Aplicação Ações Especial")]
        AplicacaoAcoesEspecial = 11,

        [StringValue("Resgate Cotas Especial")]
        ResgateCotasEspecial = 12,

        [StringValue("Come Cotas")]
        ComeCotas = 20,

        [StringValue("Amortização")]
        Amortizacao = 80,

        [StringValue("Juros")]
        Juros = 82,

        [StringValue("Amort.+ Juros")]
        AmortizacaoJuros = 84,

        [StringValue("Incorporação Resgate")]
        IncorporacaoResgate = 100,

        [StringValue("Incorp. Aplicação")]
        IncorporacaoAplicacao = 101,

        [StringValue("Retirada")]
        Retirada = 102,

        [StringValue("Depósito")]
        Deposito = 103,

        [StringValue("Aplicação Cotas")]
        AplicacaoCotas = 110,

        [StringValue("Dividendo")]
        Dividendo = 112
    }

    public enum TipoResgateCotista
    {
        [StringValue("Específico")]
        Especifico = 1, //Direcionado a uma nota de aplicação específica

        [StringValue("FIFO")]
        FIFO = 2, //First In First Out (da mais antiga para a mais recente)

        [StringValue("LIFO")]
        LIFO = 3, //Last In First Out (da mais recente para a mais antiga)

        [StringValue("Menor Imposto")]
        MenorImposto = 4, //Busca antes as que projetam menos tributos (IR + IOF)

        [StringValue("Colagem")]
        Colagem = 5 //Busca por posições coladas
    }

    public enum TipoTransferencia
    {
        Integral = 1,
        PosicoesQuantidade = 2,
        PosicoesPercentual = 3

    }

    public enum FonteOperacaoCotista
    {
        Manual = 1,
        DistribuicaoDividendo = 2,
        ResgateTributos = 3,
        ZeraCaixa = 4,
        Incorporacao = 5,
        OperacaoMae = 6, //Operações de aporte/resgate lançadas por carteira mãe
        OrdemCotista = 7,
        CisaoIncorporacaoFusao = 8, 
        MudancaCondominio = 9,
        MudancaClassificacao = 10,
        ComeCotas = 11,
        InclusaoRetirada = 12,
        TransferenciaSerie = 13,
        TransferenciaEntreCotista = 14,
        DiferencaAliquotaGrossUp = 15
    }

    public enum TipoOperacaoBloqueio
    {
        Bloqueio = 1,
        Desbloqueio = 2
    }

    public enum TipoLiberacaoBloqueio
    {
        Manual = 1,
        Automatico = 2
    }

    /// <summary>
    /// Indica o Tipo de Processamento do Resgate
    /// </summary>
    public enum TipoProcessaResgate {
        OperacaoFundo = 0,  // Origem OperaçãoFundo
        OperacaoCotista = 1 // Origem OperaçãoCotista
    }

    public enum StatusOrdemCotista
    {
        Digitado = 1,
        Aprovado = 2,
        Processado = 3,
        Cancelado = 4
    }

    public enum TipoCompensacaoPrejuizo
    {
        Padrao = 1,
        Direto = 2,
        NaoRealiza = 3
    }

    public enum TipoCotistaAnbima
    {
        [StringValue("EFPC Emp. Pública")]
        EFPCEmpPublica = 1,

        [StringValue("EFPC Emp. Privada")]
        EFPCEmpPrivada = 2,

        [StringValue("Seguradora")]
        Seguradora = 3,

        [StringValue("Entidade Aberta de Previdência Complementar")]
        EntidadeAbertadePrevidênciaComplementar = 4,

        [StringValue("Capitalização")]
        Capitalizacao = 5,

        [StringValue("Corporate")]
        Corporate = 6,

        [StringValue("Middle Market")]
        MiddleMarket = 7,

        [StringValue("Private")]
        Private = 8,

        [StringValue("Varejo Alta Renda")]
        VarejoAltaRenda = 9,

        [StringValue("Varejo")]
        Varejo = 10,

        [StringValue("Poder Público")]
        PoderPublico = 11,

        [StringValue("Regime Próprio de Previdência Social")]
        RegimePropriodePrevidenciaSocial = 12,

        [StringValue("Fundos de Investimento")]
        FundosdeInvestimento = 13,

        [StringValue("Estrangeiros")]
        Estrangeiros = 14,

        [StringValue("Outros")]
        Outros = 15
    }

    public enum TipoCotista
    {
        PESSOA_FISICA_PRIVATE_BANKING = 1,
        PESSOA_FISICA_VAREJO = 2,
        PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING = 3,
        PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO = 4,
        BANCO_COMERCIAL = 5,
        CORRETORA_DISTRIBUIDORA = 6,
        OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS = 7,
        INVESTIDORES_NAO_RESIDENTES = 8,
        ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR = 9,
        ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR = 10,
        REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS = 11,
        SOCIEDADE_SEGURADORA_RESSEGURADORA = 12,
        SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL = 13,
        FUNDOS_CLUBES_INVESTIMENTOS = 14,
        COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM = 15,
        OUTROS_TIPOS_COTISTAS = 16
    }

    public enum TipoTributacaoCotista
    {
        Residente = 1,
        NaoResidente = 2
    }

    public enum StatusOrdemIntegracao
    {
        Agendado = 110, //Escopo Integração Itau
        Cancelado = 111, //Escopo Integração Itau
        ItauOK = 112, //Escopo Integração Itau
        SemSaldo = 113, //Escopo Integração Itau
        OutroErro = 114, //Ocorreu erro na integração (diferente do erro de saldo insuficiente)
        Processado = 250 //Manter esta entrada apesar da Ordem nao assumir este status. Sinaliza que deve ser procurada a operacao correspondente
    }

    public enum TipoCotistaCvm
    {
        [StringValue("Pessoa física private banking")]
        PessoaFicaPrivateBanking = 1,
        [StringValue("Pessoa física varejo")]
        PessoaFicaVarejo = 2,
        [StringValue("Pessoa jurídica não financeira private banking")]
        PessoaJuricaNaoFinanceiraPrivateBanking = 3,
        [StringValue("Pessoa jurídica não-financeira varejo")]
        PessoaJuricaNaoFinanceiraVarejo = 4,
        [StringValue("Banco comercial")]
        BancoComercial = 5,
        [StringValue("Corretora ou distribuidora")]
        CorretoraDistribuidora = 6,
        [StringValue("Outras pessoas jurídicas financeiras")]
        OutrasPessoasJuricasJinanceiras = 7,
        [StringValue("Investidores não residentes")]
        InvestidoresNaoResidentes = 8,
        [StringValue("Entidade aberta de previdência complementar")]
        EntidadeAbertaPrevidenciaComplementar = 9,
        [StringValue("Entidade fechada de previdência complementar")]
        EntidadeFechadaPrevidenciaComplementar = 10,
        [StringValue("Regime prõprio de previdência dos servidores públicos")]
        RegimeProprioPrevidenciaServidoresPublicos = 11,
        [StringValue("Sociedade seguradora ou resseguradora")]
        SociedadeSeguradoraResseguradora = 12,
        [StringValue("Sociedade de capitalização e de arrendamento mercantil")]
        SociedadeDeCapitalizacaoArrendamentoMercantil = 13,
        [StringValue("Fundos e clubes de Investimento")]
        FundosClubesInvestimento = 14,
        [StringValue("Cotistas de distribuidores do fundo (distribuidores por conta e ordem)")]
        CotistasDistribuidoresFundoContaOrdem = 15,
        [StringValue("Outros tipos de cotistas não relacionados")]
        OutrosTiposCotistasRelacionados = 16
    }

    public static class CotEnumUtil
    {
        /// <summary>
        /// Gets the enum description.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static String GetEnumDescription(Enum e)
        {
            FieldInfo fieldInfo = e.GetType().GetField(e.ToString());

            StringValueAttribute[] enumAttributes = (StringValueAttribute[])fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false);

            if (enumAttributes.Length > 0)
            {
                return enumAttributes[0].Value;
            }
            return e.ToString();
        }

        public static object GetEnumDescriptionValue(string value, Type enumType, bool ignoreCase)
        {
            object result = null;
            string enumStringValue = null;
            if (!enumType.IsEnum)
                throw new ArgumentException("enumType should be a valid enum");

            foreach (FieldInfo fieldInfo in enumType.GetFields())
            {
                //var attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false)  as StringValueAttribute[];
                var attribs = (StringValueAttribute[])fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false);
                //Get the StringValueAttribute for each enum member 
                if (attribs.Length > 0)
                    enumStringValue = attribs[0].Value;

                if (string.Compare(enumStringValue, value, ignoreCase) == 0)
                    result = Enum.Parse(enumType, fieldInfo.Name);
            }
            return result;
        }
    }

}
