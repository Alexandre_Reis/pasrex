﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using RoboImporta.Properties;
using Financial.Util;
using Financial.Bolsa;
using Financial.Common;
using Financial.BMF;
using EntitySpaces.Interfaces;
using log4net;
using log4net.Config;
using Financial.RendaFixa;
using Financial.Common.Enums;
using Financial.Interfaces.Import.RendaFixa;
using System.Net.Mail;
using System.Net.Configuration;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace RoboImporta
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string timing = "D0";
            if (!String.IsNullOrEmpty(Settings.Default.Timing))
            {
                timing = Settings.Default.Timing;
            }

            DateTime dataAtual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime data = dataAtual;
            
            string BDs = Settings.Default.BDs;
            string[] listaBDs = BDs.Split(new Char[] { ';' });
            string server = Settings.Default.Server;
            string login = Settings.Default.Login;
            string senha = Settings.Default.Senha;

            
            bool erro = false;
            int i = 0;
            foreach (string BD in listaBDs)
            {                
                int posicao = BD.IndexOf("(");
                string imports = "";
                string BDAjustado = BD;
                BDAjustado = BDAjustado.Replace("\r\n", string.Empty);
                if (posicao != -1)
                {
                    imports = BD.Substring(posicao, BD.Length - posicao).Replace("(", "").Replace(")", "");
                    BDAjustado = BD.Replace(imports, "").Trim().Replace("(", "").Replace(")", "");
                }                
                if (BDAjustado.Trim() == "")
                {
                    break;
                }

                
                esConfigSettings.ConnectionInfo.Connections.Clear();

                esConnectionElement connSQL = new esConnectionElement();
                connSQL.Name = "Financial"+i;
                connSQL.ProviderMetadataKey = "esDefault";
                connSQL.SqlAccessType = esSqlAccessType.DynamicSQL;
                connSQL.Provider = "EntitySpaces.SqlClientProvider";
                connSQL.ProviderClass = "DataProvider";
                connSQL.ConnectionString = @"" + "Data Source=" + server + ";Initial Catalog=" + BDAjustado + ";User ID=" + login + ";Password=" + senha + ";" + "";
                connSQL.DatabaseVersion = "2005";
                esConfigSettings.ConnectionInfo.Connections.Add(connSQL);

                esConfigSettings.ConnectionInfo.Default = "Financial"+i;
                i++;

                DateTime dataReferencia = data;
                if (timing != "D0")
                {
                    int numeroDias = Convert.ToInt32(timing.Replace("D", "").Trim());
                    dataReferencia = Calendario.SubtraiDiaUtil(data, numeroDias);                    
                }

                if (!Calendario.IsDiaUtil(data))
                {
                    return;
                }

                if (!ProcessaCarga(BDAjustado, imports, dataReferencia))
                {
                    erro = true;
                }                
            }

            #region Carga do robô - Proventos
            if (Convert.ToBoolean(Settings.Default.Proventos))
            {
                try
                {
                    DateTime dataReferencia = data;
                    if (timing == "D1")
                    {
                        dataReferencia = Calendario.SubtraiDiaUtil(data, 1);
                    }
                    ExportaProventos(dataReferencia);
                }
                catch (Exception e)
                {
                    log.Debug("Carga do robô (Proventos) com problemas. \n" + e.Message);
                    EnviaEmail("Carga do robô (Proventos) com problemas. \n" + e.Message);
                    return;
                }

                EnviaEmail("Carga do robô (Proventos) executada com sucesso.");
            }
            #endregion

            if (!erro)
            {
                EnviaEmail("Carga do robô executada com sucesso!");
            }
            
        }

        private static void EnviaEmail(string mensagemErro)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
            
            string in_from = settings.Smtp.From.ToString();
            string stringEmail = Settings.Default.EmailTo;

            if (stringEmail != "")
            {
                stringEmail = stringEmail.Replace(";", ",");
                string[] emails = stringEmail.Split(new Char[] { ',' });

                foreach (string email in emails)
                {
                    MailAddress from = new MailAddress(in_from);
                    MailAddress to = new MailAddress(email);

                    MailMessage msg = new MailMessage(from, to);
                    msg.From = from;

                    msg.IsBodyHtml = false;

                    msg.Subject = "Carga diária Robô Financial";
                    msg.Body = mensagemErro;

                    SmtpClient smtp = new SmtpClient();

                    if (settings.Smtp.From.Contains("@gmail"))
                    {
                        smtp.EnableSsl = true;
                    }

                    smtp.Send(msg);
                }
            }

            
        }

        private static bool ProcessaCarga(string BD, string imports, DateTime data)
        {
            bool importaBolsa = true;
            bool importaBMF = true;
            bool importaRendaFixa = true;
            bool importaFundos = true;
            bool importaOffshore = true;
            bool importaProventos = true;
            bool importaBloomberg = true;
            
            if (imports != "")
            {
                string[] listaImports = imports.Split(new Char[] { ',' });

                foreach (string itemImport in listaImports)
                {
                    if (itemImport.Trim().ToUpper() == "BOLSA")
                    {
                        importaBolsa = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "BMF")
                    {
                        importaBMF = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "RENDAFIXA")
                    {
                        importaRendaFixa = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "FUNDOS")
                    {
                        importaFundos = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "OFFSHORE")
                    {
                        importaOffshore = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "PROVENTOS")
                    {
                        importaProventos = false;
                    }
                    else if (itemImport.Trim().ToUpper() == "BLOOMBERG")
                    {
                        importaBloomberg = false;
                    }
                }
            }            

            esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string path = Settings.Default.Downloads;            

            bool isFeriadoBovespa = Calendario.IsFeriado(data, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            bool indicaErro = false;
            StringBuilder mensagemErro = new StringBuilder();
            
            if (Calendario.IsDiaUtil(data))
            {
                if (log.IsDebugEnabled)
                {
                    log.Debug("Status de Importação: " + data.ToShortDateString());
                    log.Debug("BD: " + BD + "\n");
                }
                                    
                mensagemErro.AppendLine();

                if (!isFeriadoBovespa)
                {
                    #region BDIN
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    //                
                    string mensagemErroBdin = "";

                    if (importaBolsa)
                    {
                        try
                        {
                            cotacaoBolsa.CarregaCotacaoBdin(data, path);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("BDIN (Bolsa) Importado: " + data.ToShortDateString());
                            }
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;
                            mensagemErroBdin = "Bdin com problemas \n";
                            mensagemErroBdin += " - Cotações não Importadas: " + e1.Message;
                        }
                    }
                    //
                    try
                    {
                        cotacaoIndice.CarregaCotacaoIndiceBdin(data, path);
                        if (log.IsDebugEnabled)
                        {
                            log.Debug("BDIN (Índices) Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e2)
                    {
                        indicaErro = true;
                        if (String.IsNullOrEmpty(mensagemErroBdin))
                        {
                            mensagemErroBdin = "Bdin com problemas \n";
                            mensagemErroBdin += " - Cotações Índices não Importadas: " + e2.Message;
                        }
                        else
                        {
                            mensagemErroBdin += " - Cotações Índices não Importadas: " + e2.Message;
                        }
                    }
                    if (!String.IsNullOrEmpty(mensagemErroBdin))
                    {
                        mensagemErro.AppendLine("\t" + mensagemErroBdin);
                    }
                    #endregion

                    #region Proventos Financial
                    string mensagemErroProventos = "";
                    if (importaProventos)
                    {
                        try
                        {
                            ProventoBolsa proventoBolsa = new ProventoBolsa();
                            proventoBolsa.ImportaProventos(data);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("Proventos Importado: " + data.ToShortDateString());
                            }
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;
                            mensagemErroProventos = "Proventos com problemas \n";
                            mensagemErroProventos += " - Proventos não Importados: " + e1.Message;
                        }
                        
                    }
                    #endregion

                    #region BdPregao
                    AtivoBMF ativoBMF = new AtivoBMF();
                    cotacaoIndice = new CotacaoIndice();
                    CotacaoBMF cotacaoBMF = new CotacaoBMF();
                    //
                    string mensagemErroBDPregao = "";
                    if (importaBMF)
                    {
                        try
                        {
                            ativoBMF.CarregaAtivoBdPregao(data, path);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("BDPregao (BMF) Importado: " + data.ToShortDateString());
                            }
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;
                            mensagemErroBDPregao = "BDPregão com problemas \n";
                            mensagemErroBDPregao += " - Ativos BMF não Importados: " + e1.Message;
                        }
                    }
                    //
                    try
                    {
                        cotacaoIndice.CarregaCotacaoIndiceBdPregao(data, path);
                        if (log.IsDebugEnabled)
                        {
                            log.Debug("BDPregao (Índices) Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e2)
                    {
                        indicaErro = true;
                        if (String.IsNullOrEmpty(mensagemErroBDPregao))
                        {
                            mensagemErroBDPregao = "BDPregão com problemas \n";
                            mensagemErroBDPregao += " - Cotações Índices não Importadas: " + e2.Message;
                        }
                        else
                        {
                            mensagemErroBDPregao += " - Cotações Índices não Importadas: " + e2.Message;
                        }
                    }
                    //
                    if (importaBMF)
                    {
                        try
                        {
                            cotacaoBMF.CarregaCotacaoBdPregao(data, path);
                        }
                        catch (Exception e3)
                        {
                            indicaErro = true;
                            if (String.IsNullOrEmpty(mensagemErroBDPregao))
                            {
                                mensagemErroBDPregao = "BDPregão com problemas \n";
                                mensagemErroBDPregao += " - Cotações BMF não Importadas: " + e3.Message;
                            }
                            else
                            {
                                mensagemErroBDPregao += " - Cotações BMF não Importadas: " + e3.Message;
                            }
                        }
                    }
                    //
                    if (!String.IsNullOrEmpty(mensagemErroBDPregao))
                    {
                        mensagemErro.AppendLine("\t" + mensagemErroBDPregao);
                    }
                    #endregion

                    #region TarPar
                    TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
                    cotacaoIndice = new CotacaoIndice();
                    //
                    string mensagemErroTarpar = "";
                    if (importaBMF)
                    {
                        try
                        {
                            tabelaCalculoBMF.CarregaTabelaCalculoBMFTarPar(data, path);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("TarPar (Tabela Cálculo BMF) Importado: " + data.ToShortDateString());
                            }
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;
                            mensagemErroTarpar = "TarPar com problemas \n";
                            mensagemErroTarpar += " - Tabela Cálculo BMF não Importada: " + e1.Message;
                        }
                    }
                    //
                    try
                    {
                        cotacaoIndice.CarregaCotacaoIndiceTarPar(data, path);
                        if (log.IsDebugEnabled)
                        {
                            log.Debug("TarPar (Cotações Índices) Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e2)
                    {
                        indicaErro = true;
                        if (String.IsNullOrEmpty(mensagemErroTarpar))
                        {
                            mensagemErroTarpar = "TarPar com problemas \n";
                            mensagemErroTarpar += " - Cotações Índices não Importadas: " + e2.Message;
                        }
                    }
                    if (!String.IsNullOrEmpty(mensagemErroTarpar))
                    {
                        mensagemErro.AppendLine("\t" + mensagemErroTarpar);
                    }
                    #endregion

                    //#region TarPreg
                    //TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();
                    ////
                    //if (importaBMF)
                    //{
                    //    try
                    //    {
                    //        tabelaCustosBMF.CarregaTarPreg(data, path);
                    //        if (log.IsDebugEnabled)
                    //        {
                    //            log.Debug("TarPreg Importado: " + data.ToShortDateString());
                    //        }
                    //    }
                    //    catch (Exception e1)
                    //    {
                    //        indicaErro = true;
                    //        string msg = "TarPreg com problemas \n";
                    //        msg += " - Dados não Importados: " + e1.Message;
                    //        mensagemErro.AppendLine("\t" + msg);
                    //    }
                    //}
                    //#endregion
                }

                #region Indic
                CotacaoIndice cotacaoIndiceIndic = new CotacaoIndice();
                //
                try
                {
                    cotacaoIndiceIndic.CarregaCotacaoIndiceIndic(data, path);
                    if (log.IsDebugEnabled)
                    {
                        log.Debug("INDIC Importado: " + data.ToShortDateString());
                    }
                }
                catch (Exception e1)
                {
                    indicaErro = true;

                    if (isFeriadoBovespa)
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                        #region Caso não tenha conseguido carregar INDIC, força Dólar do dia anterior
                        decimal cotacaoDolar = 0;
                        CotacaoIndice cotacaoIndiceDolar = new CotacaoIndice();
                        cotacaoIndiceDolar.Query.Where(cotacaoIndiceDolar.Query.IdIndice.Equal((byte)ListaIndiceFixo.PTAX_800VENDA),
                                                       cotacaoIndiceDolar.Query.Data.Equal(dataAnterior));
                        if (cotacaoIndiceDolar.Query.Load())
                        {
                            cotacaoDolar = cotacaoIndiceDolar.Valor.Value;
                        }

                        CotacaoIndice cotacaoIndiceDolarDeletar = new CotacaoIndice();
                        if (cotacaoIndiceDolarDeletar.LoadByPrimaryKey(data, (byte)ListaIndiceFixo.PTAX_800VENDA))
                        {
                            cotacaoIndiceDolarDeletar.MarkAsDeleted();
                            cotacaoIndiceDolarDeletar.Save();
                        }

                        if (cotacaoDolar != 0)
                        {
                            CotacaoIndice cotacaoIndiceDolarNovo = new CotacaoIndice();
                            cotacaoIndiceDolarNovo.Data = data;
                            cotacaoIndiceDolarNovo.IdIndice = (byte)ListaIndiceFixo.PTAX_800VENDA;
                            cotacaoIndiceDolarNovo.Valor = cotacaoDolar;
                            cotacaoIndiceDolarNovo.Save();
                        }
                        #endregion

                        #region Caso não tenha conseguido carregar INDIC, força CDI do dia anterior
                        decimal cotacaoCDI = 0;
                        CotacaoIndice cotacaoIndiceCDI = new CotacaoIndice();
                        cotacaoIndiceCDI.Query.Where(cotacaoIndiceCDI.Query.IdIndice.Equal((byte)ListaIndiceFixo.CDI),
                                                       cotacaoIndiceCDI.Query.Data.Equal(dataAnterior));
                        if (cotacaoIndiceCDI.Query.Load())
                        {
                            cotacaoCDI = cotacaoIndiceCDI.Valor.Value;
                        }

                        CotacaoIndice cotacaoIndiceCDIDeletar = new CotacaoIndice();
                        if (cotacaoIndiceCDIDeletar.LoadByPrimaryKey(data, (byte)ListaIndiceFixo.CDI))
                        {
                            cotacaoIndiceCDIDeletar.MarkAsDeleted();
                            cotacaoIndiceCDIDeletar.Save();
                        }

                        if (cotacaoCDI != 0)
                        {
                            CotacaoIndice cotacaoIndiceCDINovo = new CotacaoIndice();
                            cotacaoIndiceCDINovo.Data = data;
                            cotacaoIndiceCDINovo.IdIndice = (byte)ListaIndiceFixo.CDI;
                            cotacaoIndiceCDINovo.Valor = cotacaoCDI;
                            cotacaoIndiceCDINovo.Save();
                        }
                        #endregion

                        #region Caso não tenha conseguido carregar INDIC, força SELIC do dia anterior
                        decimal cotacaoSELIC = 0;
                        CotacaoIndice cotacaoIndiceSELIC = new CotacaoIndice();
                        cotacaoIndiceSELIC.Query.Where(cotacaoIndiceSELIC.Query.IdIndice.Equal((byte)ListaIndiceFixo.SELIC),
                                                       cotacaoIndiceSELIC.Query.Data.Equal(dataAnterior));
                        if (cotacaoIndiceSELIC.Query.Load())
                        {
                            cotacaoSELIC = cotacaoIndiceSELIC.Valor.Value;
                        }

                        CotacaoIndice cotacaoIndiceSELICDeletar = new CotacaoIndice();
                        if (cotacaoIndiceSELICDeletar.LoadByPrimaryKey(data, (byte)ListaIndiceFixo.SELIC))
                        {
                            cotacaoIndiceSELICDeletar.MarkAsDeleted();
                            cotacaoIndiceSELICDeletar.Save();
                        }

                        if (cotacaoSELIC != 0)
                        {
                            CotacaoIndice cotacaoIndiceSELICNovo = new CotacaoIndice();
                            cotacaoIndiceSELICNovo.Data = data;
                            cotacaoIndiceSELICNovo.IdIndice = (byte)ListaIndiceFixo.SELIC;
                            cotacaoIndiceSELICNovo.Valor = cotacaoSELIC;
                            cotacaoIndiceSELICNovo.Save();
                        }
                        #endregion

                        string msg = "Indic com problemas \n";
                        msg += " - Cotações Índices não Importadas: " + e1.Message;
                        msg += " - Foram atualizados a partir do dia anterior (Dólar, CDI, SELIC)";
                        mensagemErro.AppendLine("\t" + msg);
                    }
                    else
                    {
                        string msg = "Indic com problemas \n";
                        msg += " - Cotações Índices não Importadas: " + e1.Message;
                        mensagemErro.AppendLine("\t" + msg);
                    }
                }
                #endregion

                #region IMA
                CotacaoIndice cotacaoIndiceIMA = new CotacaoIndice();
                //
                try
                {
                    cotacaoIndiceIMA.CarregaCotacaoIndiceIMA(data);
                    if (log.IsDebugEnabled)
                    {
                        log.Debug("IMA Importado: " + data.ToShortDateString());
                    }
                }
                catch (Exception e1)
                {
                    indicaErro = true;
                    
                    string msg = "IMA com problemas \n";
                    msg += " - Cotações Índices não Importadas: " + e1.Message;
                    mensagemErro.AppendLine("\t" + msg);
                    
                }
                #endregion

                #region Bloomberg para cotações de indices
                string mensagemErroOffShore = "";
                try
                {
                    CotacaoIndice cotacaoIndiceBloomberg = new CotacaoIndice();
                    cotacaoIndiceBloomberg.CarregaCotacaoIndiceBloomberg(data, data);
                }
                catch (Exception eb)
                {
                    indicaErro = true;

                    mensagemErroOffShore = "Cotações Indice Offshore (EUR) com problemas \n";
                    mensagemErroOffShore += " - Cotações não Importadas: " + eb.Message;
                }

                if (!String.IsNullOrEmpty(mensagemErroOffShore))
                {
                    mensagemErro.AppendLine("\t" + mensagemErroOffShore);
                }                
                #endregion

                if (importaRendaFixa)
                {
                    #region AndimaMercado
                    CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
                    //
                    try
                    {
                        cotacaoMercadoAndima.CarregaAndima(data, path);
                        if (log.IsDebugEnabled)
                        {
                            log.Debug("AnbimaMercado Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e1)
                    {
                        indicaErro = true;
                        //
                        string msg = "AnbimaMercado com problemas \n";
                        msg += " - Dados não Importados: " + e1.Message;
                        //
                        mensagemErro.AppendLine("\t" + msg);
                    }
                    #endregion

                    #region CotacaoMercadoDebenture
                    CotacaoMercadoDebenture cotacaoMercadoDebenture = new CotacaoMercadoDebenture();                    
                    //
                    try {
                        cotacaoMercadoDebenture.CarregaDebenture(data, path);                        

                        if (log.IsDebugEnabled) 
                        {
                            log.Debug("CotacaoMercadoDebenture Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e1) {
                        indicaErro = true;
                        //
                        string msg = "CotacaoMercadoDebenture com problemas \n";
                        msg += " - Dados não Importados: " + e1.Message;
                        //
                        mensagemErro.AppendLine("\t" + msg);
                    }
                   
                    #endregion

                    #region CotacaoParDebenture
                    CotacaoDebenture cotacaoParDebenture = new CotacaoDebenture();
                    //
                    try
                    {
                        cotacaoParDebenture.CarregaDebenture(data, path);

                        if (log.IsDebugEnabled)
                        {
                            log.Debug("CotacaoParDebenture Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e1)
                    {
                        indicaErro = true;
                        //
                        string msg = "CotacaoParDebenture com problemas \n";
                        msg += " - Dados não Importados: " + e1.Message;
                        //
                        mensagemErro.AppendLine("\t" + msg);
                    }

                    #endregion

                    #region Resolução 238
                    CotacaoResolucao238 cotacaoResolucao238 = new CotacaoResolucao238();
                    //
                    try
                    {
                        cotacaoResolucao238.CarregaRes238(data, path);
                        if (log.IsDebugEnabled)
                        {
                            log.Debug("Resolucao 238 Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e1)
                    {
                        indicaErro = true;
                        //
                        string msg = "Resolucao 238 com problemas \n";
                        msg += " - Dados não Importados: " + e1.Message;
                        //
                        mensagemErro.AppendLine("\t" + msg);
                    }
                    #endregion
                }

                #region Caso seja feriado BovespaBMF, carrega Ibovespa do dia anterior para o dia
                if (isFeriadoBovespa)
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    decimal cotacaoIbovespa = 0;
                    CotacaoIndice cotacaoIndiceIbov = new CotacaoIndice();
                    cotacaoIndiceIbov.Query.Where(cotacaoIndiceIbov.Query.IdIndice.Equal((byte)ListaIndiceFixo.IBOVESPA_FECHA),
                                                  cotacaoIndiceIbov.Query.Data.Equal(dataAnterior));
                    if (cotacaoIndiceIbov.Query.Load())
                    {
                        cotacaoIbovespa = cotacaoIndiceIbov.Valor.Value;
                    }

                    CotacaoIndiceCollection cotacaoIndiceCollectionDeletar = new CotacaoIndiceCollection();
                    cotacaoIndiceCollectionDeletar.Query.Where(cotacaoIndiceCollectionDeletar.Query.IdIndice.In((byte)ListaIndiceFixo.IBOVESPA_FECHA),
                                                               cotacaoIndiceCollectionDeletar.Query.Data.Equal(data));
                    cotacaoIndiceCollectionDeletar.Query.Load();
                    cotacaoIndiceCollectionDeletar.MarkAllAsDeleted();
                    cotacaoIndiceCollectionDeletar.Save();

                    if (cotacaoIbovespa != 0)
                    {
                        CotacaoIndice cotacaoIndiceNovo = new CotacaoIndice();
                        cotacaoIndiceNovo.Data = data;
                        cotacaoIndiceNovo.IdIndice = (byte)ListaIndiceFixo.IBOVESPA_FECHA;
                        cotacaoIndiceNovo.Valor = cotacaoIbovespa;
                        cotacaoIndiceNovo.Save();
                    }
                }
                #endregion

                if (importaOffshore)
                {
                    #region Cotações Offshore
                    bool isFeriadoNY = Calendario.IsFeriado(data, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.NovaYorkBMF);

                    if (!isFeriadoNY)
                    {
                        //Para offshore, busca do Yahoo, só traz última data disponível
                        DateTime dataHoje = DateTime.Now;
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, 1, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
                        //
                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                        CotacaoBMF cotacaoBMF = new CotacaoBMF();
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();

                        mensagemErroOffShore = "";
                        try
                        {
                            //cotacaoBolsa.CarregaCotacaoOffShore(dataAnterior, path);
                            cotacaoBolsa.CarregaCotacaoBloomberg(dataAnterior, dataAnterior);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("OffShore (Cotação Bolsa Bloomberg) Importado: " + data.ToShortDateString());
                            }
                            cotacaoBMF.CarregaCotacaoOffShoreBloomberg(dataAnterior, dataAnterior);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("OffShore (Cotação BMF Bloomberg) Importado: " + data.ToShortDateString());
                            }
                            cotacaoIndice.CarregaCotacaoIndiceBloomberg(dataAnterior, dataAnterior);
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("OffShore (Cotação Índice Bloomberg) Importado: " + data.ToShortDateString());
                            }
                           
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;

                            mensagemErroOffShore = "Cotações Offshore com problemas \n";
                            mensagemErroOffShore += " - Cotações não Importadas: " + e1.Message;
                        }

                        if (!String.IsNullOrEmpty(mensagemErroOffShore))
                        {
                            mensagemErro.AppendLine("\t" + mensagemErroOffShore);
                        }
                    }
                    #endregion
                }

                if (importaBloomberg)
                {
                    #region Cotações Onshore Bloomberg
                    
                    if (!isFeriadoBovespa)
                    {                        
                        //traz as cotações do dia anterior
                        DateTime dataHoje = DateTime.Now;
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, 1, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
                        //
                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                        CotacaoBMF cotacaoBMF = new CotacaoBMF();

                        mensagemErroOffShore = "";
                        try
                        {
                            cotacaoBolsa.CarregaCotacaoBloomberg(dataAnterior, dataAnterior);
                            cotacaoBMF.CarregaCotacaoOffShoreBloomberg(dataAnterior, dataAnterior);
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;

                            mensagemErroOffShore = "Cotações Offshore com problemas \n";
                            mensagemErroOffShore += " - Cotações não Importadas: " + e1.Message;
                        }

                        if (!String.IsNullOrEmpty(mensagemErroOffShore))
                        {
                            mensagemErro.AppendLine("\t" + mensagemErroOffShore);
                        }
                    }
                    #endregion
                }
                if (importaFundos)
                {
                    #region Cotações fundos SIAnbid
                    bool anbidOK = true;
                    Financial.Fundo.CarteiraCollection fundos = new Financial.Fundo.CarteiraCollection();
                    fundos.BuscaCarteirasFundosAnbima();
                    string mensagemErroSIAnbid = "";
                    foreach (Financial.Fundo.Carteira fundo in fundos)
                    {
                        try
                        {
                            fundo.LoadByPrimaryKey(fundo.IdCarteira.Value);
                            fundo.ImportaCotasSIAnbid();
                        }
                        catch (Exception e1)
                        {
                            indicaErro = true;

                            if (e1.Message == "")
                            {
                                mensagemErroSIAnbid = mensagemErroSIAnbid + "Fundo " + fundo.IdCarteira.Value.ToString() + "\n";
                            }
                            else
                            {
                                mensagemErroSIAnbid = mensagemErroSIAnbid + e1.Message + "\n";
                            }
                        }
                    }

                    HistoricoCotaCollection historicoCotaCollectionExiste = new HistoricoCotaCollection();
                    HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("H");
                    ClienteQuery clienteQuery = new ClienteQuery("C");

                    historicoCotaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == historicoCotaQuery.IdCarteira);
                    historicoCotaQuery.Where(historicoCotaQuery.Data.Equal(data),
                                             clienteQuery.TipoControle.In((byte)TipoControleCliente.ApenasCotacao,
                                                                          (byte)TipoControleCliente.Cotista));
                    historicoCotaCollectionExiste.Load(historicoCotaQuery);

                    anbidOK = historicoCotaCollectionExiste.Count > 0;

                    if (anbidOK)
                    {
                        if (mensagemErroSIAnbid != "")
                        {
                            mensagemErro.AppendLine("Cotações SIAnbid importadas parcialmente: " + mensagemErroSIAnbid);

                            if (log.IsDebugEnabled)
                            {
                                log.Debug("Cotações SIAnbid importadas parcialmente: " + mensagemErroSIAnbid);
                            }
                        }
                        else
                        {
                            if (log.IsDebugEnabled)
                            {
                                log.Debug("Cotações SIAnbid Importadas: " + data.ToShortDateString());
                            }
                        }
                    }
                    else
                    {
                        mensagemErroSIAnbid = "Cotações SIAnbid não importadas: " + mensagemErroSIAnbid;
                        mensagemErro.AppendLine("\t" + mensagemErroSIAnbid);
                    }                                        
                    #endregion
                }
            }
            
            #region Cotacao IGPM/IPCA projetados (Andima)
            CotacaoIndice cotacaoIndiceAndima = new CotacaoIndice();
            //
            try
            {
                cotacaoIndiceAndima.CarregaCotacaoIndiceAndima(data, path);
                if (log.IsDebugEnabled)
                {
                    log.Debug("Cotacao Anbima Indicadores Importado: " + data.ToShortDateString());
                }
            }
            catch (Exception e1)
            {
                indicaErro = true;
                //
                string msg = "Cotacao Anbima Indicadores com problemas \n";
                msg += " - Dados não Importados: " + e1.Message;
                //
                mensagemErro.AppendLine("\t" + msg);
            }
            #endregion

            #region IBGE (IPCA/INPC)
            CotacaoIndice cotacaoIndiceIBGE = new CotacaoIndice();
            //
            try
            {
                DateTime dataMesAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, -1);
                cotacaoIndiceIBGE.CarregaCotacaoIndiceIBGE(dataMesAnterior, path);
                if (log.IsDebugEnabled)
                {
                    log.Debug("Cotacao IBGE Importado: " + dataMesAnterior.ToShortDateString());
                }
            }
            catch (Exception e1)
            {
                if (data.Day >= 15) //Assume tolerância em não ter IPCA/INPC até o dia 15
                {
                    indicaErro = true;
                    //
                    string msg = "Cotacao IBGE (IPCA/INPC) com problemas \n";
                    msg += " - Dados não Importados: " + e1.Message;
                    //
                    mensagemErro.AppendLine("\t" + msg);
                }
            }
            #endregion

            if (indicaErro)
            {
                try
                {
                    log.Debug(mensagemErro);
                    EnviaEmail("BD: " + BD + "\r\n" + mensagemErro.ToString());
                }
                catch (Exception e)
                {
                    //Erro no envio de email e no log - continuar mesmo assim
                }
            }
            else
            {
                log.Debug("\n");
            }

            bool ok = !indicaErro;

            return ok;

        }        

        private static void ExportaProventos(DateTime data)
        {
            esConfigSettings.ConnectionInfo.Connections.Clear();

            string connStringOra = Settings.Default.ConnStringOra;
            
            esConnectionElement conn = new esConnectionElement();
            conn.Name = "Sinacor";
            conn.ProviderMetadataKey = "esDefault";
            conn.SqlAccessType = esSqlAccessType.DynamicSQL;
            conn.Provider = "EntitySpaces.OracleClientProvider";
            conn.ProviderClass = "DataProvider";
            //conn.ConnectionString = @"" + connStringOra;
            conn.ConnectionString = @"" + "Data Source=sinacor;User ID=financial;Password=financial;Persist Security Info=True;";
            esConfigSettings.ConnectionInfo.Connections.Add(conn);

            esConnectionElement connSQL = new esConnectionElement();
            connSQL.Name = "Financial";
            connSQL.ProviderMetadataKey = "esDefault";
            connSQL.SqlAccessType = esSqlAccessType.DynamicSQL;
            connSQL.Provider = "EntitySpaces.SqlClientProvider";
            connSQL.ProviderClass = "DataProvider";
            connSQL.ConnectionString = "Data Source=108.60.206.26\\SQL2008;Initial Catalog=BOLSA;User ID=bolsa;Password=7xpt678_34;";
            esConfigSettings.ConnectionInfo.Connections.Add(connSQL);

            esConfigSettings.ConnectionInfo.Default = "Financial";
                        
            ProventoBolsa proventoBolsa = new ProventoBolsa();
            proventoBolsa.CarregaProventosSinacor(data, false);

            //proventoBolsa.AtualizaFatorPU(data);
        }   
    }
}
