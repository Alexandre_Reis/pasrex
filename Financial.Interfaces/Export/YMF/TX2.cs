﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Export.YMF
{

    [DelimitedRecord(";")]
    public class TX2
    {
        [FieldIgnored]
        public string CdAtivo;
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime DtPosicao;
        public decimal VlCotaLiquida;
        public decimal VlPerformance;
        [FieldIgnored]
        public DateTime DtFimTX2;
        
        [FieldConverter(ConverterKind.Date, "hh:mm:ss")]
        public DateTime HrAtual;
                       
    }

}