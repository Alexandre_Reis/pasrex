﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Export.YMF
{

    [DelimitedRecord(";")]
    public class TX3
    {
        public string CdVersao;
        public string CdAtivo;
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        public DateTime DtPosicao;
        public decimal VlAplicacoesPF;
        public decimal QtCotasEmitidasPF;
        public decimal VlAplicacoesPJ;
        public decimal QtCotasEmitidasPJ;
        public decimal VlCustoResgatesPF;
        public decimal VlResgatesPF;
        public decimal VlIRPF;
        public decimal QtCotasResgatadasPF;
        public decimal VlCustoResgatesPJ;
        public decimal VlResgatesPJ;
        public decimal VlIRPJ;
        public decimal QtCotasResgatadasPJ;
        public decimal VlSaqueCarencia;
        public decimal VlPerformance;
        public int NoCotistas;
        public decimal VlPIP;
        public decimal VlIRPIP;
        public decimal VlPosicaoPFD1;
        public decimal QtCotasEmitidasPFD1;
        public decimal VlPosicaoPJD1;
        public decimal QtCotasEmitidasPJD1;

        [FieldConverter(ConverterKind.Date, "hh:mm:ss")]
        public DateTime HrAtual;
        public decimal VlIOF;
        public decimal VlPerformanceLDagua;

        public decimal VlIOFAplicacoes;
        public decimal VlCustoResgatesIRPF;
        public decimal VlResgatesIRPF;
        public decimal QtCotasResgatesIRPF;
        public decimal VlCustoResgatesIRPJ;
        public decimal VlResgatesIRPJ;
        public decimal QtCotasResgatesIRPJ;

        public decimal VlCustoPatrimonioPF;
        public decimal QtCotasFechamentoPF;
        public decimal VlCustoPatrimonioPJ;
        public decimal QtCotasFechamentoPJ;
        public decimal VlCotaFechamento;

        public string CdTipoMovimento;

        public decimal VlFDPenaltyPF;
        public decimal VlFDPenaltyPJ;
        public decimal VlADPenaltyPF;
        public decimal VlADPenaltyPJ;
        public decimal VlIngresso;

        public decimal VlRepasseDiv;
        public decimal VlIRRFRepasseDiv;
        public decimal VlRepasseJuros;
        public decimal VlIRRFRepasseJuros;
        public decimal VlRepasseRend;
        public decimal VlIRRFRepasseRend;

        public decimal VlAmortizacaoPrincPF;
        public decimal VlAmortizacaoPrincPJ;
        public decimal VlAmortizacaoJurosPF;
        public decimal VlAmortizacaoJurosPJ;
        public decimal VlProvFinanceiroSaida;

        public string DataConversaoFisicaFiller;
        public string DataLiquidacaoFinanceiraFiller;

        //Campos abaixo nao sao usados no arquivo, mas estao presentes na tabela
        public decimal VlPatrimonioFechamentoPF;
        public decimal VlPatrimonioFechamentoPJ;

        public DateTime DtFimTX3;
        public decimal VlAmortizacaoTotal;
        public decimal VlAmortizacaoPJ;
        public decimal VlAmortizacaoPF;
        
        public decimal VlPerformaceLDagua;//SEMPRE 0 no COT ATUAL - Atenção: O nome desta coluna está errado na VIEW... 
        //Existem 2 colunas, uma chamada VlPerformanceLDagua (a qual possui valores) e outra com o nome errado (Performace sem o N) sempre zerada

        public decimal VlAplicacoesPFTotal;//SEMPRE 0 no COT atual
        public decimal QtCotasEmitidasPFTotal;//SEMPRE 0 no COT atual
        public decimal VlAplicacoesPJTotal;//SEMPRE 0 no COT atual
        public decimal QtCotasEmitidasPJTotal;//SEMPRE 0 no COT atual

    }

}