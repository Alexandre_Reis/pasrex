﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using System.Web.Configuration;
using Financial.CRM.Enums;
using System.Net;
using Financial.Util;
using Financial.Security;
using System.Web;
using Financial.Security.Enums;
using System.Configuration;
using Financial.RendaFixa;


namespace Financial.Interfaces.Export
{
    public class Itau
    {
        public class CAC
        {
            private string eBusinessId;
            public string EBusinessId
            {
                get { return eBusinessId; }
                set { eBusinessId = value; }
            }

            private string senha;
            public string Senha
            {
                get { return senha; }
                set { senha = value; }
            }

            private int cdbAnCli;
            public int CdbAnCli
            {
                get { return cdbAnCli; }
                set { cdbAnCli = value; }
            }

            private int agencia;
            public int Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private int conta;
            public int Conta
            {
                get { return conta; }
                set { conta = value; }
            }

            private int dac;
            public int Dac
            {
                get { return dac; }
                set { dac = value; }
            }

            private string nmCli;
            public string NmCli
            {
                get { return nmCli; }
                set { nmCli = value; }
            }

            private string dtNascL8;
            public string DtNascL8
            {
                get { return dtNascL8; }
                set { dtNascL8 = value; }
            }

            private string pes;
            public string Pes
            {
                get { return pes; }
                set { pes = value; }
            }

            private string idCgcCpf;
            public string IdCgcCpf
            {
                get { return idCgcCpf; }
                set { idCgcCpf = value; }
            }

            private string tpDocIde;
            public string TpDocIde
            {
                get { return tpDocIde; }
                set { tpDocIde = value; }
            }

            private string idDoCli;
            public string IdDoCli
            {
                get { return idDoCli; }
                set { idDoCli = value; }
            }

            private string dtDocExp;
            public string DtDocExp
            {
                get { return dtDocExp; }
                set { dtDocExp = value; }
            }

            private string idOrgEmi;
            public string IdOrgEmi
            {
                get { return idOrgEmi; }
                set { idOrgEmi = value; }
            }

            private string idEstEmi;
            public string IdEstEmi
            {
                get { return idEstEmi; }
                set { idEstEmi = value; }
            }

            private string icimp;
            public string Icimp
            {
                get { return icimp; }
                set { icimp = value; }
            }

            private string sitLeg;
            public string SitLeg
            {
                get { return sitLeg; }
                set { sitLeg = value; }
            }

            private string codSex;
            public string CodSex
            {
                get { return codSex; }
                set { codSex = value; }
            }

            private int cdEstCiv;
            public int CdEstCiv
            {
                get { return cdEstCiv; }
                set { cdEstCiv = value; }
            }

            private string cdRamoAt;
            public string CdRamoAt
            {
                get { return cdRamoAt; }
                set { cdRamoAt = value; }
            }

            private int cdProAti;
            public int CdProAti
            {
                get { return cdProAti; }
                set { cdProAti = value; }
            }

            private string cdfConst;
            public string CdfConst
            {
                get { return cdfConst; }
                set { cdfConst = value; }
            }

            private string icLec;
            public string IcLec
            {
                get { return icLec; }
                set { icLec = value; }
            }

            private string cdRemLec;
            public string CdRemLec
            {
                get { return cdRemLec; }
                set { cdRemLec = value; }
            }

            private int ddd;
            public int Ddd
            {
                get { return ddd; }
                set { ddd = value; }
            }

            private int tel8;
            public int Tel8
            {
                get { return tel8; }
                set { tel8 = value; }
            }

            private int ramal;
            public int Ramal
            {
                get { return ramal; }
                set { ramal = value; }
            }

            private string email;
            public string Email
            {
                get { return email; }
                set { email = value; }
            }

            private int idAssess;
            public int IdAssess
            {
                get { return idAssess; }
                set { idAssess = value; }
            }

            private string idCliTer;
            public string IdCliTer
            {
                get { return idCliTer; }
                set { idCliTer = value; }
            }

            private string icExtM;
            public string IcExtM
            {
                get { return icExtM; }
                set { icExtM = value; }
            }

            private string icCfm;
            public string IcCfm
            {
                get { return icCfm; }
                set { icCfm = value; }
            }

            private int vRendFam;
            public int VRendFam
            {
                get { return vRendFam; }
                set { vRendFam = value; }
            }

            private int vlPatrim;
            public int VlPatrim
            {
                get { return vlPatrim; }
                set { vlPatrim = value; }
            }

            private string cdClsCli;
            public string CdClsCli
            {
                get { return cdClsCli; }
                set { cdClsCli = value; }
            }

            private string cdCetip;
            public string CdCetip
            {
                get { return cdCetip; }
                set { cdCetip = value; }
            }

            private string cdDistrib;
            public string CdDistrib
            {
                get { return cdDistrib; }
                set { cdDistrib = value; }
            }

            private string nomAdmin;
            public string NomAdmin
            {
                get { return nomAdmin; }
                set { nomAdmin = value; }
            }

            private string nmGest;
            public string NmGest
            {
                get { return nmGest; }
                set { nmGest = value; }
            }

            private string nmCustod;
            public string NmCustod
            {
                get { return nmCustod; }
                set { nmCustod = value; }
            }

            private string nmConta1;
            public string NmConta1
            {
                get { return nmConta1; }
                set { nmConta1 = value; }
            }

            private string ddd1Cust;
            public string Ddd1Cust
            {
                get { return ddd1Cust; }
                set { ddd1Cust = value; }
            }

            private string tel1Cust;
            public string Tel1Cust
            {
                get { return tel1Cust; }
                set { tel1Cust = value; }
            }

            private string rml1Cust;
            public string Rml1Cust
            {
                get { return rml1Cust; }
                set { rml1Cust = value; }
            }

            private string eml1Cust;
            public string Eml1Cust
            {
                get { return eml1Cust; }
                set { eml1Cust = value; }
            }

            private string nmConta2;
            public string Nmconta2
            {
                get { return nmConta2; }
                set { nmConta2 = value; }
            }

            private string ddd2Cust;
            public string Ddd2Cust
            {
                get { return ddd2Cust; }
                set { ddd2Cust = value; }
            }

            private string tel2Cust;
            public string Tel2Cust
            {
                get { return tel2Cust; }
                set { tel2Cust = value; }
            }

            private string rml2Cust;
            public string Rml2Cust
            {
                get { return rml2Cust; }
                set { rml2Cust = value; }
            }

            private string eml2Cust;
            public string Eml2Cust
            {
                get { return eml2Cust; }
                set { eml2Cust = value; }
            }

            private string noLogRes;
            public string NoLogRes
            {
                get { return noLogRes; }
                set { noLogRes = value; }
            }

            private string nuLogRes;
            public string NuLogRes
            {
                get { return nuLogRes; }
                set { nuLogRes = value; }
            }

            private string cpLogRes;
            public string CpLogRes
            {
                get { return cpLogRes; }
                set { cpLogRes = value; }
            }

            private string bairroRes;
            public string BairroRes
            {
                get { return bairroRes; }
                set { bairroRes = value; }
            }

            private string cepResid;
            public string CepResid
            {
                get { return cepResid; }
                set { cepResid = value; }
            }

            private string cidAdRes;
            public string CidAdRes
            {
                get { return cidAdRes; }
                set { cidAdRes = value; }
            }

            private string ufResid;
            public string UfResid
            {
                get { return ufResid; }
                set { ufResid = value; }
            }

            private string noLogCom;
            public string NoLogCom
            {
                get { return noLogCom; }
                set { noLogCom = value; }
            }

            private string nuLogCom;
            public string NuLogCom
            {
                get { return nuLogCom; }
                set { nuLogCom = value; }
            }

            private string cpLogCom;
            public string CpLogCom
            {
                get { return cpLogCom; }
                set { cpLogCom = value; }
            }

            private string bairrCom;
            public string BairrCom
            {
                get { return bairrCom; }
                set { bairrCom = value; }
            }

            private string cepComer;
            public string CepComer
            {
                get { return cepComer; }
                set { cepComer = value; }
            }

            private string cidAdCml;
            public string CidAdCml
            {
                get { return cidAdCml; }
                set { cidAdCml = value; }
            }

            private string ufComerc;
            public string UfComerc
            {
                get { return ufComerc; }
                set { ufComerc = value; }
            }

            private string noLogAlt;
            public string NoLogAlt
            {
                get { return noLogAlt; }
                set { noLogAlt = value; }
            }

            private string nuLogAlt;
            public string NuLogAlt
            {
                get { return nuLogAlt; }
                set { nuLogAlt = value; }
            }

            private string cpLogAlt;
            public string CpLogAlt
            {
                get { return cpLogAlt; }
                set { cpLogAlt = value; }
            }

            private string bairrAlt;
            public string BairrAlt
            {
                get { return bairrAlt; }
                set { bairrAlt = value; }
            }

            private string cepAlter;
            public string CepAlter
            {
                get { return cepAlter; }
                set { cepAlter = value; }
            }

            private string cidadAlt;
            public string CidadAlt
            {
                get { return cidadAlt; }
                set { cidadAlt = value; }
            }

            private string ufAltern;
            public string UfAltern
            {
                get { return ufAltern; }
                set { ufAltern = value; }
            }

            private string idOpemac;
            public string IdOpemac
            {
                get { return idOpemac; }
                set { idOpemac = value; }
            }

            private string indCtsavicd;
            public string IndCtsavicd
            {
                get { return indCtsavicd; }
                set { indCtsavicd = value; }
            }

            private string cdNatCap;
            public string CdNatCap
            {
                get { return cdNatCap; }
                set { cdNatCap = value; }
            }

            private string tipoRend;
            public string TipoRend
            {
                get { return tipoRend; }
                set { tipoRend = value; }
            }

            private string idDclFat;
            public string IdDclFat
            {
                get { return idDclFat; }
                set { idDclFat = value; }
            }

            private string idResFis;
            public string IdResFis
            {
                get { return idResFis; }
                set { idResFis = value; }
            }

            private string paiResDf;
            public string PaiResDf
            {
                get { return paiResDf; }
                set { paiResDf = value; }
            }

            private string giinTin;
            public string GiinTin
            {
                get { return giinTin; }
                set { giinTin = value; }
            }

            private string cdJusAgi;
            public string CdJusAgi
            {
                get { return cdJusAgi; }
                set { cdJusAgi = value; }
            }

            private string cnaeDivi;
            public string CnaeDivi
            {
                get { return cnaeDivi; }
                set { cnaeDivi = value; }
            }

            private string cnaeGrup;
            public string CnaeGrup
            {
                get { return cnaeGrup; }
                set { cnaeGrup = value; }
            }

            private string cnaeClas;
            public string CnaeClas
            {
                get { return cnaeClas; }
                set { cnaeClas = value; }
            }

            private string cnaeSubc;
            public string CnaeSubc
            {
                get { return cnaeSubc; }
                set { cnaeSubc = value; }
            }

            private string codPaiNa;
            public string CodPaiNa
            {
                get { return codPaiNa; }
                set { codPaiNa = value; }
            }
        }

        public class ARQEOP
        {
            private string campo0; //eBusinessId;
            public string Campo0 //EBusinessId
            {
                get { return campo0; }
                set { campo0 = value; }
            }

            private string campo1;//senha;
            public string Campo1 //Senha
            {
                get { return campo1; }
                set { campo1 = value; }
            }

            private int campo2; //cdBanc;
            public int Campo2 //CdBanc
            {
                get { return campo2; }
                set { campo2 = value; }
            }

            private int campo3; //cdFdo;
            public int Campo3
            {
                get { return campo3; }
                set { campo3 = value; }
            }

            private int campo4;//cdBanCli;
            public int Campo4 //CdBanCli
            {
                get { return campo4; }
                set { campo4 = value; }
            }

            private int campo5; //agencia;
            public int Campo5 //Agencia
            {
                get { return campo5; }
                set { campo5 = value; }
            }

            private int campo6; //cdCta;
            public int Campo6 //CdCta
            {
                get { return campo6; }
                set { campo6 = value; }
            }

            private int campo7;// dac10;
            public int Campo7//Dac10
            {
                get { return campo7; }
                set { campo7 = value; }
            }

            private int campo8; //subCont;
            public int Campo8 //SubCont
            {
                get { return campo8; }
                set { campo8 = value; }
            }

            private int campo9; //opeMov;
            public int Campo9 //OpeMov
            {
                get { return campo9; }
                set { campo9 = value; }
            }

            private decimal campo10; // vLiqSol;
            public decimal Campo10 //VLiqSol
            {
                get { return campo10; }
                set { campo10 = value; }
            }

            private string campo11;//bcoAgCt1;
            public string Campo11//BcoAgCt1
            {
                get { return campo11; }
                set { campo11 = value; }
            }

            private int campo12;//idOpemac;
            public int Campo12//IdOpemac
            {
                get { return campo12; }
                set { campo12 = value; }
            }

            private string campo13;//cdTipLiq;
            public string Campo13//CdTipLiq
            {
                get { return campo13; }
                set { campo13 = value; }
            }

            private int campo14;//cdApl;
            public int Campo14//CdApl
            {
                get { return campo14; }
                set { campo14 = value; }
            }

            private string campo15;//idTipCt1;
            public string Campo15//IdTipCt1
            {
                get { return campo15; }
                set { campo15 = value; }
            }

            private string campo16;//dataAgend;
            public string Campo16//DataAgend
            {
                get { return campo16; }
                set { campo16 = value; }
            }

            private string campo17;//dtLanct;
            public string Campo17//DtLanct
            {
                get { return campo17; }
                set { campo17 = value; }
            }

            private string campo18;//isbp;
            public string Campo18//ISPB
            {
                get { return campo18; }
                set { campo18 = value; }
            }
        }

        public class CCI
        {
            private string eBusinessId;
            public string EBusinessId
            {
                get { return eBusinessId; }
                set { eBusinessId = value; }
            }

            private string senha;
            public string Senha
            {
                get { return senha; }
                set { senha = value; }
            }

            private int cdBanCli;
            public int CdBanCli
            {
                get { return cdBanCli; }
                set { cdBanCli = value; }
            }

            private int agencia;
            public int Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private int cdCta;
            public int CdCta
            {
                get { return cdCta; }
                set { cdCta = value; }
            }

            private int dac10;
            public int Dac10
            {
                get { return dac10; }
                set { dac10 = value; }
            }

            private string codMov;
            public string CodMov
            {
                get { return codMov; }
                set { codMov = value; }
            }

            private string idTipCt1;
            public string IdTipCt1
            {
                get { return idTipCt1; }
                set { idTipCt1 = value; }
            }

            private string bcoAgCt1;
            public string BcoAgCt1
            {
                get { return bcoAgCt1; }
                set { bcoAgCt1 = value; }
            }

            private string idTpCt01;
            public string IdTpCt01
            {
                get { return idTpCt01; }
                set { idTpCt01 = value; }
            }

            private string idCgcCpf;
            public string IdCgcCpf
            {
                get { return idCgcCpf; }
                set { idCgcCpf = value; }
            }

            private string idTtCi01;
            public string IdTtCi01
            {
                get { return idTtCi01; }
                set { idTtCi01 = value; }
            }

            private string idClit01;
            public string IdClit01
            {
                get { return idClit01; }
                set { idClit01 = value; }
            }

            private int idOpemac;
            public int IdOpemac
            {
                get { return idOpemac; }
                set { idOpemac = value; }
            }
        }

        public class CCO
        {
            private string eBusinessId;
            public string EBusinessId
            {
                get { return eBusinessId; }
                set { eBusinessId = value; }
            }

            private string senha;
            public string Senha
            {
                get { return senha; }
                set { senha = value; }
            }

            private int cdBanCli;
            public int CdBanCli
            {
                get { return cdBanCli; }
                set { cdBanCli = value; }
            }

            private int agencia;
            public int Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private int cdCta;
            public int CdCta
            {
                get { return cdCta; }
                set { cdCta = value; }
            }

            private int dac10;
            public int Dac10
            {
                get { return dac10; }
                set { dac10 = value; }
            }

            private string codMov;
            public string CodMov
            {
                get { return codMov; }
                set { codMov = value; }
            }

            private string nmCli;
            public string NmCli
            {
                get { return nmCli; }
                set { nmCli = value; }
            }

            private string dtNascL8;
            public string DtNascL8
            {
                get { return dtNascL8; }
                set { dtNascL8 = value; }
            }

            private string pes;
            public string Pes
            {
                get { return pes; }
                set { pes = value; }
            }

            private string cpf;
            public string Cpf
            {
                get { return cpf; }
                set { cpf = value; }
            }

            private string tpDocIde;
            public string TpDocIde
            {
                get { return tpDocIde; }
                set { tpDocIde = value; }
            }

            private string idDoCli;
            public string IdDoCli
            {
                get { return idDoCli; }
                set { idDoCli = value; }
            }

            private string dtDocExp;
            public string DtDocExp
            {
                get { return dtDocExp; }
                set { dtDocExp = value; }
            }

            private string idOrgEmi;
            public string IdOrgEmi
            {
                get { return idOrgEmi; }
                set { idOrgEmi = value; }
            }

            private string idEstEmi;
            public string IdEstEmi
            {
                get { return idEstEmi; }
                set { idEstEmi = value; }
            }

            private string sitLeg;
            public string SitLeg
            {
                get { return sitLeg; }
                set { sitLeg = value; }
            }

            private string codSex;
            public string CodSex
            {
                get { return codSex; }
                set { codSex = value; }
            }

            private string cdProAti;
            public string CdProAti
            {
                get { return cdProAti; }
                set { cdProAti = value; }
            }

            private string cdEstCiv;
            public string CdEstCiv
            {
                get { return cdEstCiv; }
                set { cdEstCiv = value; }
            }

            private int vRendFam;
            public int VRendFam
            {
                get { return vRendFam; }
                set { vRendFam = value; }
            }

            private int vlPatrim;
            public int VlPatrim
            {
                get { return vlPatrim; }
                set { vlPatrim = value; }
            }
        }

        public class UPC
        {
            private string eBusinessId;
            public string EBusinessId
            {
                get { return eBusinessId; }
                set { eBusinessId = value; }
            }

            private string senha;
            public string Senha
            {
                get { return senha; }
                set { senha = value; }
            }

            private int cdBanCli;
            public int CdBanCli
            {
                get { return cdBanCli; }
                set { cdBanCli = value; }
            }

            private int agencia;
            public int Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private int cdCta;
            public int CdCta
            {
                get { return cdCta; }
                set { cdCta = value; }
            }

            private int dac10;
            public int Dac10
            {
                get { return dac10; }
                set { dac10 = value; }
            }

            private string codMov;
            public string CodMov
            {
                get { return codMov; }
                set { codMov = value; }
            }

            private string cdPesqEx;
            public string CdPesqEx
            {
                get { return cdPesqEx; }
                set { cdPesqEx = value; }
            }

            private string idClasRf;
            public string IdClasRf
            {
                get { return idClasRf; }
                set { idClasRf = value; }
            }

            private string nmPesPad;
            public string NmPesPad
            {
                get { return nmPesPad; }
                set { nmPesPad = value; }
            }

            private string inConPep;
            public string InConPep
            {
                get { return inConPep; }
                set { inConPep = value; }
            }

            private string pes;
            public string Pes
            {
                get { return pes; }
                set { pes = value; }
            }

            private string codSex;
            public string CodSex
            {
                get { return codSex; }
                set { codSex = value; }
            }

            private string idCgcCpf;
            public string IdCgcCpf
            {
                get { return idCgcCpf; }
                set { idCgcCpf = value; }
            }

            private string tpDocIde;
            public string TpDocIde
            {
                get { return tpDocIde; }
                set { tpDocIde = value; }
            }

            private string idDoCli;
            public string IdDoCli
            {
                get { return idDoCli; }
                set { idDoCli = value; }
            }

            private string dtDocExp;
            public string DtDocExp
            {
                get { return dtDocExp; }
                set { dtDocExp = value; }
            }

            private string idOrgEmi;
            public string IdOrgEmi
            {
                get { return idOrgEmi; }
                set { idOrgEmi = value; }
            }

            private string idEstEmi;
            public string IdEstEmi
            {
                get { return idEstEmi; }
                set { idEstEmi = value; }
            }

            private string lograd;
            public string Lograd
            {
                get { return lograd; }
                set { lograd = value; }
            }

            private string numLec;
            public string NumLec
            {
                get { return numLec; }
                set { numLec = value; }
            }

            private string compl;
            public string Compl
            {
                get { return compl; }
                set { compl = value; }
            }

            private string bairro;
            public string Bairro
            {
                get { return bairro; }
                set { bairro = value; }
            }

            private string cidade;
            public string Cidade
            {
                get { return cidade; }
                set { cidade = value; }
            }

            private string estado;
            public string Estado
            {
                get { return estado; }
                set { estado = value; }
            }

            private string cep8;
            public string Cep8
            {
                get { return cep8; }
                set { cep8 = value; }
            }

            private string percPar1;
            public string PercPar1
            {
                get { return percPar1; }
                set { percPar1 = value; }
            }

            private string nmConta1;
            public string NmConta1
            {
                get { return nmConta1; }
                set { nmConta1 = value; }
            }

            private string ddd1Cust;
            public string Ddd1Cust
            {
                get { return ddd1Cust; }
                set { ddd1Cust = value; }
            }

            private string tel1Cust;
            public string Tel1Cust
            {
                get { return tel1Cust; }
                set { tel1Cust = value; }
            }

            private string rml1Cust;
            public string Rml1Cust
            {
                get { return rml1Cust; }
                set { rml1Cust = value; }
            }

            private string eml1Cust;
            public string Eml1Cust
            {
                get { return eml1Cust; }
                set { eml1Cust = value; }
            }

            private string nmConta2;
            public string NmConta2
            {
                get { return nmConta2; }
                set { nmConta2 = value; }
            }

            private string ddd2Cust;
            public string Ddd2Cust
            {
                get { return ddd2Cust; }
                set { ddd2Cust = value; }
            }

            private string tel2Cust;
            public string Tel2Cust
            {
                get { return tel1Cust; }
                set { tel1Cust = value; }
            }

            private string rml2Cust;
            public string Rml2Cust
            {
                get { return rml2Cust; }
                set { rml2Cust = value; }
            }

            private string eml2Cust;
            public string Eml2Cust
            {
                get { return eml2Cust; }
                set { eml2Cust = value; }
            }

            private int idOpemac;
            public int IdOpemac
            {
                get { return idOpemac; }
                set { idOpemac = value; }
            }
        }

        public void apendParam(string nomecampo, string valor, int tamanho, char tipo, XmlNode Node, XmlDocument xmlDocument)
        {
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");
            XmlNode paramNode = xmlDocument.CreateElement("param");
            XmlAttribute attribute = xmlDocument.CreateAttribute("id");
            attribute.Value = nomecampo.PadRight(11, espaco);
            paramNode.Attributes.Append(attribute);
            attribute = xmlDocument.CreateAttribute("value");
            if (valor == null)
            {
                valor = "";
            }
            if (tipo == espaco)
            {
                attribute.Value = Utilitario.RemoveAcentos(valor).Trim().PadRight(tamanho, espaco).Substring(0, tamanho);
            }
            if (tipo == zero)
            {
                attribute.Value = valor.Trim().PadLeft(tamanho, zero).Substring(0, tamanho);
            }
            paramNode.Attributes.Append(attribute);
            Node.AppendChild(paramNode);
        }

        /// <summary>
        /// retorna arquivo CAC
        /// </summary>
        /// <param name="idPessoa"></param>
        public XmlDocument RetornaCac_Xml(int idPessoa)
        {
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");

            XmlDocument xmlDocument = new XmlDocument();

            Pessoa pessoa = new Pessoa();

            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                XmlNode rootNode = xmlDocument.CreateElement("itaumsg");
                xmlDocument.AppendChild(rootNode);

                XmlNode parameterNode = xmlDocument.CreateElement("parameter");
                rootNode.AppendChild(parameterNode);

                #region Config
                string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
                string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;
                string configCdBanCli = WebConfigurationManager.AppSettings["ConfigCdBanCli"];

                apendParam("EBUSINESSID", configUsuarioIntegracao, 33, espaco, parameterNode, xmlDocument);
                apendParam("SENHA", configSenhaIntegracao, 8, espaco, parameterNode, xmlDocument);
                apendParam("CDBANCLI", configCdBanCli, 6, zero, parameterNode, xmlDocument);
                #endregion

                #region Conta Corrente
                /*ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
                BancoQuery bancoQuery = new BancoQuery("B");
                contaCorrenteQuery.InnerJoin(bancoQuery).On(bancoQuery.IdBanco == contaCorrenteQuery.IdBanco);
                contaCorrenteQuery.Where(bancoQuery.CodigoCompensacao.Like("%341%"),
                                         contaCorrenteQuery.IdPessoa.Equal(idPessoa));
                ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                contaCorrenteCollection.Load(contaCorrenteQuery);*/

                string strAgencia = "";
                string conta = "";
                string dac = "";
                /*if (contaCorrenteCollection.Count > 0)
                {
                    Agencia agencia = new Agencia();
                    if (agencia.LoadByPrimaryKey(Convert.ToInt32(contaCorrenteCollection[0].IdAgencia)))
                    {
                        strAgencia = agencia.Codigo.ToString();
                    }
                    if (contaCorrenteCollection[0].Numero != null)
                    {
                        conta = contaCorrenteCollection[0].Numero;
                    }
                    if (contaCorrenteCollection[0].DigitoConta != null)
                    {
                        dac = contaCorrenteCollection[0].DigitoConta;
                    }
                }*/
                //aqui


                Cotista cotista = new Cotista();
                if (cotista.LoadByPrimaryKey(idPessoa))
                {
                    if (!String.IsNullOrEmpty(cotista.CodigoInterface))
                    {
                        strAgencia = cotista.CodigoInterface.Substring(0, 4);
                        conta = cotista.CodigoInterface.Substring(4, 7);
                        dac = cotista.CodigoInterface.Substring(11, 1);
                    }
                }
                else
                {
                    cotista = null;
                }

                apendParam("AGENCIA", strAgencia, 4, zero, parameterNode, xmlDocument);
                apendParam("CONTA", conta, 9, zero, parameterNode, xmlDocument);
                apendParam("DAC", dac.ToString(), 1, zero, parameterNode, xmlDocument);
                #endregion

                #region Pessoa
                string nomePessoa = "";

                if (pessoa.Nome != null)
                {
                    nomePessoa = pessoa.Nome;
                }
                else
                {
                    nomePessoa = "-";
                }

                apendParam("NMCLI", nomePessoa, 30, espaco, parameterNode, xmlDocument);

                string dtNasc8 = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica && pessoa.DataNascimento != null)
                {
                    dtNasc8 = pessoa.DataNascimento.Value.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica && pessoa.DataNascimento == null)
                    {
                        dtNasc8 = "01.01.1900";
                    }
                }

                apendParam("DTNASCL8", dtNasc8, 10, espaco, parameterNode, xmlDocument);

                string pes = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    pes = "F";
                }
                else
                {
                    pes = "J";
                }

                apendParam("PES", pes, 1, espaco, parameterNode, xmlDocument);

                string cpfCnpj = "";
                if (pessoa.Cpfcnpj != null)
                {
                    cpfCnpj = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "");
                }

                apendParam("IDCGCCPF", cpfCnpj, 15, zero, parameterNode, xmlDocument);

                #region PessoaDocumento
                string tpDocIde = "";
                string idDocLi = "";
                string dtDocExp = "";
                string idOrgEmi = "";
                string idEstEmi = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();
                    pessoaDocumentoCollection.Query.Where(pessoaDocumentoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
                    pessoaDocumentoCollection.Query.Load();

                    if (pessoaDocumentoCollection.Count > 0)
                    {
                        bool temDoc = true;
                        PessoaDocumento pessoaDocumento = pessoaDocumentoCollection[0];
                        if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_CI)
                        {
                            tpDocIde = "CI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_CIE)
                        {
                            tpDocIde = "CIE";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeMilitar)
                        {
                            tpDocIde = "CIM";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissional)
                        {
                            tpDocIde = "CIP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissionalOrgaoPublico)
                        {
                            tpDocIde = "CIPF";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CertidaoNascimento)
                        {
                            tpDocIde = "CN";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraHabilitacao)
                        {
                            tpDocIde = "CNH";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraTrabalhoProfissional)
                        {
                            tpDocIde = "CTPS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.IdentificaçãoPrisional)
                        {
                            tpDocIde = "IP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.LaissezPasser)
                        {
                            tpDocIde = "LPASS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.Passaporte)
                        {
                            tpDocIde = "PASSP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_RG)
                        {
                            tpDocIde = "RG";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.DocumentoIdentidade)
                        {
                            tpDocIde = "RI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_RNE)
                        {
                            tpDocIde = "RNE";
                        }

                        if (temDoc)
                        {
                            if (pessoaDocumento.NumeroDocumento != null)
                            {
                                idDocLi = pessoaDocumento.NumeroDocumento;
                            }
                            else
                            {
                                idDocLi = "9999999999999";
                            }

                            if (pessoaDocumento.DataExpedicao != null)
                            {
                                dtDocExp = pessoaDocumento.DataExpedicao.Value.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            if (pessoaDocumento.OrgaoEmissor != null)
                            {
                                idOrgEmi = pessoaDocumento.OrgaoEmissor;
                            }
                            if (pessoaDocumento.UFEmissor != null)
                            {
                                idEstEmi = pessoaDocumento.UFEmissor;
                            }
                        }
                    }

                }

                apendParam("TPDOCIDE", tpDocIde, 5, espaco, parameterNode, xmlDocument);
                apendParam("IDDOCLI", idDocLi, 13, zero, parameterNode, xmlDocument);
                apendParam("DTDOCEXP", dtDocExp, 10, espaco, parameterNode, xmlDocument);
                apendParam("IDORGEMI", idOrgEmi, 7, espaco, parameterNode, xmlDocument);
                apendParam("IDESTEMI", idEstEmi, 2, espaco, parameterNode, xmlDocument);
                #endregion

                #region Cotista Isencao
                string icimp = "";
                if (cotista != null)
                {
                    //F	Isentos = ISENTO IR + ISENTO IOF
                    if (cotista.IsentoIR == "S" && cotista.IsentoIOF == "S")
                    {
                        icimp = "F";
                    }
                    //I	Tributados = NAO ISENTO IR + NAO ISENTO IOF
                    if (cotista.IsentoIR == "N" && cotista.IsentoIOF == "N")
                    {
                        icimp = "I";
                    }
                    //M	Incide apenas IOF(IN 341) = ISENTO IR + NAO ISENTO IOF
                    if (cotista.IsentoIR == "S" && cotista.IsentoIOF == "N")
                    {
                        icimp = "M";
                    }
                    //R	Incide apenas IR = NAO ISENTO IR + ISENTO IOF
                    if (cotista.IsentoIR == "N" && cotista.IsentoIOF == "S")
                    {
                        icimp = "R";
                    }

                }
                apendParam("ICIMP", icimp, 1, espaco, parameterNode, xmlDocument);
                #endregion

                string sitLeg = "L";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    if (pessoa.SituacaoLegal != null)
                    {
                        if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Espolio)
                        {
                            sitLeg = "E";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Interdito)
                        {
                            sitLeg = "I";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Maior)
                        {
                            sitLeg = "L";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Menor)
                        {
                            sitLeg = "M";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Emancipado)
                        {
                            sitLeg = "N";
                        }
                    }
                }
                apendParam("SITLEG", sitLeg, 1, espaco, parameterNode, xmlDocument);

                string codSex = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    codSex = "M";
                    if (pessoa.Sexo != null)
                    {
                        codSex = pessoa.Sexo.ToString();
                    }
                }

                apendParam("CODSEX", codSex, 1, espaco, parameterNode, xmlDocument);

                string cdEstCiv = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    cdEstCiv = "1";
                    if (pessoa.EstadoCivil != null)
                    {
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Casado)
                        {
                            cdEstCiv = "1";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Divorciado)
                        {
                            cdEstCiv = "5";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Solteiro)
                        {
                            cdEstCiv = "4";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Viuvo)
                        {
                            cdEstCiv = "6";
                        }
                    }
                }
                apendParam("CDESTCIV", cdEstCiv, 1, espaco, parameterNode, xmlDocument);

                string cdRamoAt = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Juridica)
                {
                    cdRamoAt = "91119999000";
                }
                apendParam("CDRAMOAT", cdRamoAt, 11, espaco, parameterNode, xmlDocument);

                string cdProAti = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    cdProAti = "999";
                }
                apendParam("CDPROATI", cdProAti, 3, espaco, parameterNode, xmlDocument);

                string cdfConst = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Juridica)
                {
                    cdfConst = "2";
                }
                apendParam("CDFCONST", cdfConst, 2, espaco, parameterNode, xmlDocument);
                #endregion

                #region PessoaEndereco
                string icLec = "";
                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                    pessoaEnderecoCollection.Query.RecebeCorrespondencia == "S");
                pessoaEnderecoCollection.Query.Load();

                if (pessoaEnderecoCollection.Count > 0)
                {
                    if (pessoaEnderecoCollection[0].TipoEndereco == TipoEnderecoPessoa.Residencial)
                    {
                        icLec = "R";
                    }
                    if (pessoaEnderecoCollection[0].TipoEndereco == TipoEnderecoPessoa.Comercial)
                    {
                        icLec = "C";
                    }
                }
                apendParam("ICLEC", icLec, 1, espaco, parameterNode, xmlDocument);

                string cdRemLec = "C";
                apendParam("CDREMLEC", cdRemLec, 1, espaco, parameterNode, xmlDocument);
                #endregion

                #region PessoaTelefone
                string ddd = "";
                string numero = "";
                string ramal = "";
                PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
                pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                     pessoaTelefoneCollection.Query.TipoTelefone.Equal(TipoTelefonePessoa.Residencial));
                if (pessoaTelefoneCollection.Query.Load())
                {
                    if (pessoaTelefoneCollection[0].Ddd != null)
                    {
                        ddd = pessoaTelefoneCollection[0].Ddd;
                    }
                    if (pessoaTelefoneCollection[0].Numero != null)
                    {
                        numero = pessoaTelefoneCollection[0].Numero;
                    }
                    if (pessoaTelefoneCollection[0].Ramal != null)
                    {
                        ramal = pessoaTelefoneCollection[0].Ramal;
                    }
                }

                apendParam("DDD", ddd, 4, zero, parameterNode, xmlDocument);
                apendParam("TEL8", numero, 10, zero, parameterNode, xmlDocument);
                apendParam("RAMAL", ramal, 6, zero, parameterNode, xmlDocument);
                #endregion

                #region PessoaEmail
                string email = "";
                PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
                pessoaEmailCollection.Query.Where(pessoaEmailCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
                if (pessoaEmailCollection.Query.Load())
                {
                    email = pessoaEmailCollection[0].Email;
                }
                apendParam("EMAIL", email, 80, espaco, parameterNode, xmlDocument);
                #endregion

                #region Valores Contantes

                apendParam("IDASSESS", "", 5, espaco, parameterNode, xmlDocument);

                apendParam("IDCLITER", "", 25, espaco, parameterNode, xmlDocument);

                apendParam("ICEXTM", "S", 1, espaco, parameterNode, xmlDocument);

                apendParam("ICCFM", "S", 1, espaco, parameterNode, xmlDocument);

                apendParam("VRENDFAM", "0", 11, zero, parameterNode, xmlDocument);

                apendParam("VLPATRIM", "0", 15, zero, parameterNode, xmlDocument);

                //apendParam("CDCLSCLI", "99", 3, zero, parameterNode, xmlDocument);
                apendParam("CDCLSCLI", "3", 3, zero, parameterNode, xmlDocument); //3 = cliente varejo

                apendParam("CDCETIP", "", 8, espaco, parameterNode, xmlDocument);
                //apendParam("CDCETIP", "", 0, espaco, parameterNode, xmlDocument);

                apendParam("CDDISTRB", "", 10, espaco, parameterNode, xmlDocument);

                string NOMEPAI = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? pessoa.FiliacaoNomePai : "";
                apendParam("NOMEPAI", NOMEPAI, 30, espaco, parameterNode, xmlDocument);

                string NOMEMAE = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? pessoa.FiliacaoNomeMae : "";
                apendParam("NOMEMAE", NOMEMAE, 30, espaco, parameterNode, xmlDocument);

                apendParam("NOMECONJ", "", 30, espaco, parameterNode, xmlDocument);

                //apendParam("NOMADMIN", "", 45, espaco, parameterNode, xmlDocument);

                //apendParam("NMGEST", "", 40, espaco, parameterNode, xmlDocument);
                #endregion

                #region Custodiante (opcional)

                /*apendParam("NMCUSTOD", "", 40, espaco, parameterNode, xmlDocument);

                apendParam("NMCONTA1", "", 30, espaco, parameterNode, xmlDocument);

                apendParam("DDD1CUST", "", 3, espaco, parameterNode, xmlDocument);

                apendParam("TEL1CUST", "", 8, espaco, parameterNode, xmlDocument);

                apendParam("RML1CUST", "", 4, espaco, parameterNode, xmlDocument);

                apendParam("EML1CUST", "", 80, espaco, parameterNode, xmlDocument);

                apendParam("NMCONTA2", "", 30, espaco, parameterNode, xmlDocument);

                apendParam("DDD2CUST", "", 4, espaco, parameterNode, xmlDocument);

                apendParam("TEL2CUST", "", 10, espaco, parameterNode, xmlDocument);

                apendParam("RML2CUST", "", 6, espaco, parameterNode, xmlDocument);

                apendParam("EML2CUST", "", 80, espaco, parameterNode, xmlDocument);*/

                #endregion

                #region Endereco Residencial
                string noLogRes = "";
                string nuLogRes = "";
                string cpLogRes = "";
                string bairrRes = "";
                string cepResid = "";
                string cidadRes = "";
                string ufResid = "";
                int idEndRes = 0;
                pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                    pessoaEnderecoCollection.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Residencial));
                if (pessoaEnderecoCollection.Query.Load())
                {
                    idEndRes = Convert.ToInt32(pessoaEnderecoCollection[0].IdEndereco);
                    if (pessoaEnderecoCollection[0].Endereco != null)
                    {
                        noLogRes = pessoaEnderecoCollection[0].Endereco;
                    }
                    if (pessoaEnderecoCollection[0].Numero != null)
                    {
                        nuLogRes = pessoaEnderecoCollection[0].Numero;
                    }
                    if (pessoaEnderecoCollection[0].Complemento != null)
                    {
                        cpLogRes = pessoaEnderecoCollection[0].Complemento;
                    }
                    if (pessoaEnderecoCollection[0].Bairro != null)
                    {
                        bairrRes = pessoaEnderecoCollection[0].Bairro;
                    }
                    if (pessoaEnderecoCollection[0].Cep != null)
                    {
                        cepResid = pessoaEnderecoCollection[0].Cep;
                    }
                    if (pessoaEnderecoCollection[0].Cidade != null)
                    {
                        cidadRes = pessoaEnderecoCollection[0].Cidade;
                    }
                    if (pessoaEnderecoCollection[0].Uf != null)
                    {
                        ufResid = pessoaEnderecoCollection[0].Uf;
                    }
                }

                apendParam("NOLOGRES", noLogRes, 30, espaco, parameterNode, xmlDocument);

                apendParam("NULOGRES", nuLogRes, 5, espaco, parameterNode, xmlDocument);

                apendParam("CPLOGRES", cpLogRes, 15, espaco, parameterNode, xmlDocument);

                apendParam("BAIRRRES", bairrRes, 15, espaco, parameterNode, xmlDocument);

                apendParam("CEPRESID", cepResid.Trim().Replace("-", ""), 8, zero, parameterNode, xmlDocument);

                apendParam("CIDADRES", cidadRes, 20, espaco, parameterNode, xmlDocument);

                apendParam("UFRESID", ufResid, 2, espaco, parameterNode, xmlDocument);
                #endregion

                #region Endereco Comercial
                string noLogCom = "";
                string nuLogCom = "";
                string cpLogCom = "";
                string bairrCom = "";
                string cepComer = "";
                string cidadCml = "";
                string ufComerc = "";
                int idEndCom = 0;
                pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                    pessoaEnderecoCollection.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Comercial));
                if (pessoaEnderecoCollection.Query.Load())
                {
                    idEndCom = Convert.ToInt32(pessoaEnderecoCollection[0].IdEndereco);
                    if (pessoaEnderecoCollection[0].Endereco != null)
                    {
                        noLogCom = pessoaEnderecoCollection[0].Endereco;
                    }
                    if (pessoaEnderecoCollection[0].Numero != null)
                    {
                        nuLogCom = pessoaEnderecoCollection[0].Numero;
                    }
                    if (pessoaEnderecoCollection[0].Complemento != null)
                    {
                        cpLogCom = pessoaEnderecoCollection[0].Complemento;
                    }
                    if (pessoaEnderecoCollection[0].Bairro != null)
                    {
                        bairrCom = pessoaEnderecoCollection[0].Bairro;
                    }
                    if (pessoaEnderecoCollection[0].Cep != null)
                    {
                        cepComer = pessoaEnderecoCollection[0].Cep;
                    }
                    if (pessoaEnderecoCollection[0].Cidade != null)
                    {
                        cidadCml = pessoaEnderecoCollection[0].Cidade;
                    }
                    if (pessoaEnderecoCollection[0].Uf != null)
                    {
                        ufComerc = pessoaEnderecoCollection[0].Uf;
                    }
                }

                apendParam("NOLOGCOM", noLogCom, 30, espaco, parameterNode, xmlDocument);

                apendParam("NULOGCOM", nuLogCom, 5, espaco, parameterNode, xmlDocument);

                apendParam("CPLOGCOM", cpLogCom, 15, espaco, parameterNode, xmlDocument);

                apendParam("BAIRRCOM", bairrCom, 15, espaco, parameterNode, xmlDocument);

                if (String.IsNullOrEmpty(cepComer))
                {
                    apendParam("CEPCOMER", "", 8, espaco, parameterNode, xmlDocument);
                }
                else
                {
                    apendParam("CEPCOMER", cepComer.Trim().Replace("-", ""), 8, zero, parameterNode, xmlDocument);
                }

                apendParam("CIDADCML", cidadCml, 20, espaco, parameterNode, xmlDocument);

                apendParam("UFCOMERC", ufComerc, 2, espaco, parameterNode, xmlDocument);
                #endregion

                #region Endereco Alternativo
                string noLogAlt = "";
                string nuLogAlt = "";
                string cpLogAlt = "";
                string bairrAlt = "";
                string cepAlter = "";
                string cidadAlt = "";
                string ufAltern = "";
                pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                    pessoaEnderecoCollection.Query.IdEndereco.NotEqual(idEndRes),
                                                    pessoaEnderecoCollection.Query.IdEndereco.NotEqual(idEndCom));
                if (pessoaEnderecoCollection.Query.Load())
                {
                    if (pessoaEnderecoCollection[0].Endereco != null)
                    {
                        noLogAlt = pessoaEnderecoCollection[0].Endereco;
                    }
                    if (pessoaEnderecoCollection[0].Numero != null)
                    {
                        nuLogAlt = pessoaEnderecoCollection[0].Numero;
                    }
                    if (pessoaEnderecoCollection[0].Complemento != null)
                    {
                        cpLogAlt = pessoaEnderecoCollection[0].Complemento;
                    }
                    if (pessoaEnderecoCollection[0].Bairro != null)
                    {
                        bairrAlt = pessoaEnderecoCollection[0].Bairro;
                    }
                    if (pessoaEnderecoCollection[0].Cep != null)
                    {
                        cepAlter = pessoaEnderecoCollection[0].Cep;
                    }
                    if (pessoaEnderecoCollection[0].Cidade != null)
                    {
                        cidadAlt = pessoaEnderecoCollection[0].Cidade;
                    }
                    if (pessoaEnderecoCollection[0].Uf != null)
                    {
                        ufAltern = pessoaEnderecoCollection[0].Uf;
                    }
                }

                apendParam("NOLOGALT", noLogAlt, 30, espaco, parameterNode, xmlDocument);

                apendParam("NULOGALT", nuLogAlt, 5, espaco, parameterNode, xmlDocument);

                apendParam("CPLOGALT", cpLogAlt, 15, espaco, parameterNode, xmlDocument);

                apendParam("BAIRRALT", bairrAlt, 15, espaco, parameterNode, xmlDocument);

                apendParam("CEPALTER", cepAlter, 8, espaco, parameterNode, xmlDocument);

                apendParam("CIDADALT", cidadAlt, 20, espaco, parameterNode, xmlDocument);

                apendParam("UFALTERN", ufAltern, 2, espaco, parameterNode, xmlDocument);

                #endregion

                apendParam("IDOPEMAC", "", 6, zero, parameterNode, xmlDocument);

                string pessoaVinculada = "N";
                if (pessoa.PessoaVinculada != null)
                {
                    pessoaVinculada = pessoa.PessoaVinculada;
                }
                apendParam("INDCTSAVICD", pessoaVinculada, 1, espaco, parameterNode, xmlDocument);

                string cdNatCap = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Juridica)
                {
                    if (pessoa.NaturezaJuridica != null)
                    {
                        cdNatCap = pessoa.NaturezaJuridica.ToString();
                    }
                }
                apendParam("CDNATCAP", cdNatCap, 3, zero, parameterNode, xmlDocument);

                string tipoRend = "";
                if (pessoa.TipoRenda != null)
                {
                    tipoRend = pessoa.TipoRenda;
                }
                apendParam("TIPOREND", tipoRend, 1, espaco, parameterNode, xmlDocument);

                string idDclFat = "N";
                if (pessoa.IndicadorFatca != null)
                {
                    idDclFat = pessoa.IndicadorFatca;
                }
                apendParam("IDDCLFAT", idDclFat, 1, espaco, parameterNode, xmlDocument);

                string idResFis = "N";
                string pais = "";
                pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                    pessoaEnderecoCollection.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Residencial));
                if (pessoaEnderecoCollection.Query.Load())
                {
                    pais = pessoaEnderecoCollection[0].Pais;
                    if (pais != "Brasil")
                    {
                        idResFis = "S";
                    }
                    else
                    {
                        idResFis = "N";
                    }
                }
                apendParam("IDRESFIS", idResFis, 1, espaco, parameterNode, xmlDocument);

                string paiResDf = "";
                //criar opções segundo tabela CC11
                switch (pais)
                {
                    case "Afeganistao": paiResDf = "013"; break;
                    case "Antilhas Holandesas": paiResDf = "047"; break;
                    case "Argentina": paiResDf = "063"; break;
                    case "Austria": paiResDf = "072"; break;
                    case "Burundi": paiResDf = "031"; break;
                    case "Belgica": paiResDf = "087"; break;
                    case "Benin": paiResDf = "229"; break;
                    case "Bangladesh": paiResDf = "081"; break;
                    case "Bahrein, Ilhas": paiResDf = "080"; break;
                    case "Belize": paiResDf = "088"; break;
                    case "Bermudas": paiResDf = "090"; break;
                    case "Barbados": paiResDf = "083"; break;
                    case "Botsuana": paiResDf = "101"; break;
                    case "Chile": paiResDf = "158"; break;
                    case "China, Republica Popular": paiResDf = "160"; break;
                    case "Ucrania": paiResDf = "831"; break;
                    case "Costa Do Marfim": paiResDf = "193"; break;
                    case "Camaroes": paiResDf = "145"; break;
                    case "Congo": paiResDf = "177"; break;
                    case "Colombia": paiResDf = "169"; break;
                    case "Saara Ocidental": paiResDf = "685"; break;
                    case "Cuba": paiResDf = "199"; break;
                    case "Chipre": paiResDf = "163"; break;
                    case "Dinamarca": paiResDf = "232"; break;
                    case "Dominica, Ilha": paiResDf = "235"; break;
                    case "Republica Dominicana": paiResDf = "647"; break;
                    case "Argelia": paiResDf = "059"; break;
                    case "Equador": paiResDf = "239"; break;
                    case "Costa Rica": paiResDf = "196"; break;
                    case "Egito": paiResDf = "240"; break;
                    case "Djibouti": paiResDf = "783"; break;
                    case "Espanha": paiResDf = "245"; break;
                    case "Etiopia": paiResDf = "253"; break;
                    case "Emirados Arabes Unidos": paiResDf = "244"; break;
                    case "Fidji": paiResDf = "870"; break;
                    case "Franca": paiResDf = "275"; break;
                    case "Gana": paiResDf = "289"; break;
                    case "Gambia": paiResDf = "285"; break;
                    case "Grecia": paiResDf = "301"; break;
                    case "Guatemala": paiResDf = "317"; break;
                    case "Guiana": paiResDf = "337"; break;
                    case "Hong Kong": paiResDf = "351"; break;
                    case "Formosa (Taiwan)": paiResDf = "161"; break;
                    case "Burkina Faso": paiResDf = "115"; break;
                    case "Canarias, Ilhas": paiResDf = "151"; break;
                    case "Georgia, Republica Da": paiResDf = "291"; break;
                    case "Granada": paiResDf = "297"; break;
                    case "Indonesia": paiResDf = "365"; break;
                    case "India": paiResDf = "361"; break;
                    case "Ira, Republica Islamica Do": paiResDf = "372"; break;
                    case "Irlanda": paiResDf = "375"; break;
                    case "Samoa Ocidental": paiResDf = "690"; break;
                    case "Islandia": paiResDf = "379"; break;
                    case "Italia": paiResDf = "386"; break;
                    case "Estonia, Republica Da": paiResDf = "251"; break;
                    case "Virgens Americanas, Ilhas": paiResDf = "866"; break;
                    case "Jamaica": paiResDf = "391"; break;
                    case "Japao": paiResDf = "399"; break;
                    case "Finlandia": paiResDf = "271"; break;
                    case "Coreia, Republica Da": paiResDf = "190"; break;
                    case "Laos, Republica Popular Democratica": paiResDf = "420"; break;
                    case "Liberia": paiResDf = "434"; break;
                    case "Libia": paiResDf = "438"; break;
                    case "Lituania, Republica Da": paiResDf = "442"; break;
                    case "Sri Lanka": paiResDf = "750"; break;
                    case "Macau": paiResDf = "447"; break;
                    case "Marrocos": paiResDf = "474"; break;
                    case "Madagascar": paiResDf = "450"; break;
                    case "Malta": paiResDf = "467"; break;
                    case "Monaco": paiResDf = "495"; break;
                    case "Mauricio": paiResDf = "485"; break;
                    case "Malavi": paiResDf = "458"; break;
                    case "Nauru": paiResDf = "508"; break;
                    case "Niger": paiResDf = "525"; break;
                    case "Nicaragua": paiResDf = "521"; break;
                    case "Groelandia": paiResDf = "305"; break;
                    case "Noruega": paiResDf = "538"; break;
                    case "Panama": paiResDf = "580"; break;
                    case "Paraguai": paiResDf = "586"; break;
                    case "Peru": paiResDf = "589"; break;
                    case "Filipinas": paiResDf = "267"; break;
                    case "Polonia, Republica Da": paiResDf = "603"; break;
                    case "Portugal": paiResDf = "607"; break;
                    case "Gabao": paiResDf = "281"; break;
                    case "Reuniao, Ilha": paiResDf = "660"; break;
                    case "Ruanda": paiResDf = "675"; break;
                    case "Arabia Saudita": paiResDf = "053"; break;
                    case "Belarus, Republica Da": paiResDf = "085"; break;
                    case "Senegal": paiResDf = "728"; break;
                    case "Cingapura": paiResDf = "741"; break;
                    case "Siria, Republica Arabe Da": paiResDf = "744"; break;
                    case "Serra Leoa": paiResDf = "735"; break;
                    case "El Salvador": paiResDf = "687"; break;
                    case "San Marino": paiResDf = "697"; break;
                    case "Santa Lucia": paiResDf = "715"; break;
                    case "Suecia": paiResDf = "764"; break;
                    case "Togo": paiResDf = "800"; break;
                    case "Tailandia": paiResDf = "776"; break;
                    case "Tonga": paiResDf = "810"; break;
                    case "Trinidad E Tobago": paiResDf = "815"; break;
                    case "Turquia": paiResDf = "827"; break;
                    case "Tanzania, Republica Unida Da": paiResDf = "780"; break;
                    case "Uruguai": paiResDf = "845"; break;
                    case "Estados Unidos": paiResDf = "249"; break;
                    case "Venezuela": paiResDf = "850"; break;
                    case "Virgens Britanicas, Ilhas": paiResDf = "863"; break;
                    case "Zambia": paiResDf = "890"; break;
                    case "Alemanha": paiResDf = "023"; break;
                    case "Australia": paiResDf = "069"; break;
                    case "Bahamas, Ilhas": paiResDf = "077"; break;
                    case "Bolivia": paiResDf = "097"; break;
                    case "Brasil": paiResDf = "105"; break;
                    case "Canada": paiResDf = "149"; break;
                    case "Gibraltar": paiResDf = "293"; break;
                    case "Guadalupe": paiResDf = "309"; break;
                    case "Haiti": paiResDf = "341"; break;
                    case "Hungria, Republica Da": paiResDf = "355"; break;
                    case "Inglaterra": paiResDf = "367"; break;
                    case "Iraque": paiResDf = "369"; break;
                    case "Israel": paiResDf = "383"; break;
                    case "Iugoslavia, Republica Federativa": paiResDf = "388"; break;
                    case "Jordania": paiResDf = "403"; break;
                    case "Libano": paiResDf = "431"; break;
                    case "Liechtenstein": paiResDf = "440"; break;
                    case "Luxemburgo": paiResDf = "445"; break;
                    case "Malasia": paiResDf = "455"; break;
                    case "Mali": paiResDf = "464"; break;
                    case "Martinica": paiResDf = "477"; break;
                    case "Mexico": paiResDf = "493"; break;
                    case "Nepal": paiResDf = "517"; break;
                    case "Niue, Ilha": paiResDf = "531"; break;
                    case "Nova Zelandia": paiResDf = "548"; break;
                    case "Paquistao": paiResDf = "576"; break;
                    case "Porto Rico": paiResDf = "611"; break;
                    case "Quenia": paiResDf = "623"; break;
                    case "Romenia": paiResDf = "670"; break;
                    case "Russia, Federacao Da": paiResDf = "676"; break;
                    case "Sudao": paiResDf = "759"; break;
                    case "Suica": paiResDf = "767"; break;
                    case "Tcheca, Republica": paiResDf = "791"; break;
                    case "Tunisia": paiResDf = "820"; break;
                    case "Uganda": paiResDf = "833"; break;
                    case "Vanuatu  Pacific": paiResDf = "551"; break;
                    case "Zaire": paiResDf = "888"; break;
                    case "Zimbabue": paiResDf = "665"; break;
                    case "Republica Centro Africana": paiResDf = "640"; break;
                    case "Africa Do Sul": paiResDf = "756"; break;
                    case "Albania, Republica Da": paiResDf = "017"; break;
                    case "Andorra": paiResDf = "037"; break;
                    case "Angola": paiResDf = "040"; break;
                    case "Anguilla": paiResDf = "041"; break;
                    case "Antigua Barbuda": paiResDf = "043"; break;
                    case "Armenia, Republica Da": paiResDf = "064"; break;
                    case "Aruba": paiResDf = "065"; break;
                    case "Arzebaijao, Republica Do": paiResDf = "073"; break;
                    case "Bosnia-Herzegovinia": paiResDf = "098"; break;
                    case "Brunei": paiResDf = "108"; break;
                    case "Bulgaria, Republica Da": paiResDf = "111"; break;
                    case "Butao": paiResDf = "119"; break;
                    case "Cabo Verde, Republica De": paiResDf = "127"; break;
                    case "Camboja": paiResDf = "141"; break;
                    case "Casaquistao, Republica Do": paiResDf = "153"; break;
                    case "Catar": paiResDf = "154"; break;
                    case "Cayman, Ilhas": paiResDf = "137"; break;
                    case "Chade": paiResDf = "788"; break;
                    case "Christmas, Ilhas": paiResDf = "511"; break;
                    case "Cocos-Keeling, Ilhas": paiResDf = "165"; break;
                    case "Comores, Ilhas": paiResDf = "173"; break;
                    case "Cook, Ilhas": paiResDf = "183"; break;
                    case "Coreia, Republica Popular Democratica": paiResDf = "187"; break;
                    case "Coveite": paiResDf = "198"; break;
                    case "Croacia, Republica Da": paiResDf = "195"; break;
                    case "Dubai": paiResDf = "237"; break;
                    case "Eslovaca, Republica": paiResDf = "247"; break;
                    case "Eslovenia, Republica": paiResDf = "246"; break;
                    case "Falkland (Malvinas)": paiResDf = "255"; break;
                    case "Feroe, Ilhas": paiResDf = "259"; break;
                    case "Fezzan": paiResDf = "263"; break;
                    case "Guam": paiResDf = "313"; break;
                    case "Guiana Francesa": paiResDf = "325"; break;
                    case "Guine": paiResDf = "329"; break;
                    case "Guine-Bissau": paiResDf = "334"; break;
                    case "Guine-Equatorial": paiResDf = "331"; break;
                    case "Honduras": paiResDf = "345"; break;
                    case "Iemen": paiResDf = "357"; break;
                    case "Johnston, Ilhas": paiResDf = "396"; break;
                    case "Kiribati": paiResDf = "411"; break;
                    case "Lebuan, Ilhas": paiResDf = "423"; break;
                    case "Lesoto": paiResDf = "426"; break;
                    case "Letonia, Republica Da": paiResDf = "427"; break;
                    case "Macedonia": paiResDf = "449"; break;
                    case "Maldivas": paiResDf = "461"; break;
                    case "Marianas Do Norte": paiResDf = "472"; break;
                    case "Marshall, Ilhas": paiResDf = "476"; break;
                    case "Mauritania": paiResDf = "488"; break;
                    case "Mianmar (Birmania)": paiResDf = "093"; break;
                    case "Micronesia": paiResDf = "499"; break;
                    case "Midway, Ilhas": paiResDf = "490"; break;
                    case "Mocambique": paiResDf = "505"; break;
                    case "Moldova, Republica Da": paiResDf = "494"; break;
                    case "Mongolia": paiResDf = "497"; break;
                    case "Montserrat, Ilhas": paiResDf = "501"; break;
                    case "Namibia": paiResDf = "507"; break;
                    case "Norfolk, Ilha": paiResDf = "535"; break;
                    case "Nigeria": paiResDf = "528"; break;
                    case "Nova Caledonia": paiResDf = "542"; break;
                    case "Oma": paiResDf = "556"; break;
                    case "Pacifico(Adm Eua), Ilhas Do": paiResDf = "563"; break;
                    case "Pacifico(Posse Eua), Ilhas Do": paiResDf = "566"; break;
                    case "Paises Baixos(Holanda)": paiResDf = "573"; break;
                    case "Palau": paiResDf = "575"; break;
                    case "Papua Nova Guine": paiResDf = "545"; break;
                    case "Pitcaim, Ilha De": paiResDf = "593"; break;
                    case "Polinesia Francesa": paiResDf = "599"; break;
                    case "Quirguiz, Republica Da": paiResDf = "625"; break;
                    case "Reino Unido": paiResDf = "628"; break;
                    case "Salomao, Ilhas": paiResDf = "677"; break;
                    case "Samoa Americana": paiResDf = "691"; break;
                    case "Santa Helena": paiResDf = "710"; break;
                    case "Sao Cristovao E Neves, Ilhas": paiResDf = "695"; break;
                    case "Sao Pedro E Miquelon": paiResDf = "700"; break;
                    case "Sao Tome E Principe, Ilhas": paiResDf = "720"; break;
                    case "Sao Vicente E Granadinas": paiResDf = "705"; break;
                    case "Seychelles": paiResDf = "731"; break;
                    case "Somalia": paiResDf = "748"; break;
                    case "Suazilandia": paiResDf = "754"; break;
                    case "Suriname": paiResDf = "770"; break;
                    case "Tadjiquistao": paiResDf = "772"; break;
                    case "Territorio Britanico Oceano Indico": paiResDf = "782"; break;
                    case "Timor Leste": paiResDf = "795"; break;
                    case "Toquelau, Ilhas": paiResDf = "805"; break;
                    case "Turcas E Caicos": paiResDf = "823"; break;
                    case "Turcomenistao, Republica Do": paiResDf = "824"; break;
                    case "Tuvalu": paiResDf = "828"; break;
                    case "Uzbequistao, Republica Do": paiResDf = "847"; break;
                    case "Vaticano, Estado Da Cidade Do": paiResDf = "848"; break;
                    case "Vietna": paiResDf = "858"; break;
                    case "Wallis E Futuna, Ilhas": paiResDf = "875"; break;
                }
                apendParam("PAIRESDF", paiResDf, 3, espaco, parameterNode, xmlDocument);

                string giinTin = "";
                if (pessoa.GiinTin != null)
                {
                    giinTin = pessoa.GiinTin;
                }
                apendParam("GIINTIN", giinTin, 16, espaco, parameterNode, xmlDocument);

                string cdJusAgi = "";
                if (giinTin == "")
                {
                    if (pessoa.JustificativaGiin != null)
                    {
                        cdJusAgi = pessoa.JustificativaGiin;
                    }
                }
                apendParam("CDJUSAGI", cdJusAgi, 2, espaco, parameterNode, xmlDocument);

                string cnaeDivi = "";
                if (pessoa.CnaeDivisao != null)
                {
                    cnaeDivi = pessoa.CnaeDivisao.ToString();
                }
                apendParam("CNAEDIVI", cnaeDivi, 2, zero, parameterNode, xmlDocument);

                string cnaeGrup = "";
                if (pessoa.CnaeGrupo != null)
                {
                    cnaeGrup = pessoa.CnaeGrupo.ToString();
                }
                apendParam("CNAEGRUP", cnaeGrup, 2, zero, parameterNode, xmlDocument);

                string cnaeClas = "";
                if (pessoa.CnaeClasse != null)
                {
                    cnaeClas = pessoa.CnaeClasse.ToString();
                }
                apendParam("CNAECLAS", cnaeClas, 2, zero, parameterNode, xmlDocument);

                string cnaeSubc = "";
                if (pessoa.CnaeSubClasse != null)
                {
                    cnaeSubc = pessoa.CnaeSubClasse.ToString();
                }
                apendParam("CNAESUBC", cnaeSubc, 2, zero, parameterNode, xmlDocument);

                string codPaiNa = "";
                if (pessoa.Nacionalidade != null)
                {
                    codPaiNa = pessoa.Nacionalidade.ToString();
                }
                apendParam("CODPAINA", codPaiNa, 3, zero, parameterNode, xmlDocument);

                string nomeFant = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "" : pessoa.Apelido;
                apendParam("NOMEFANT", nomeFant, 30, espaco, parameterNode, xmlDocument);

                string rankAnbi = cotista.TipoCotistaAnbima.ToString();
                apendParam("RANKANBI", rankAnbi, 9, zero, parameterNode, xmlDocument);

                string emprTrab = "";
                apendParam("EMPRTRAB", emprTrab, 30, espaco, parameterNode, xmlDocument);

                string DTINIEMP = "";
                apendParam("DTINIEMP", DTINIEMP, 10, espaco, parameterNode, xmlDocument);

                string NIRE = "";
                apendParam("NIRE", NIRE, 11, zero, parameterNode, xmlDocument);

                string CADEMP = "";
                apendParam("CADEMP", CADEMP, 6, espaco, parameterNode, xmlDocument);

                string CODNAC = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? pessoa.Nacionalidade.ToString() : "";
                apendParam("CODNAC", CODNAC, 3, espaco, parameterNode, xmlDocument);

                string CODCCID = "";
                apendParam("CODCCID", CODCCID, 6, espaco, parameterNode, xmlDocument);

                string DTCONSTI = "";
                apendParam("DTCONSTI", DTCONSTI, 10, espaco, parameterNode, xmlDocument);

                string INCONPEP = String.IsNullOrEmpty(pessoa.PessoaPoliticamenteExposta) ? "N" : pessoa.PessoaPoliticamenteExposta;
                apendParam("INCONPEP", INCONPEP, 1, espaco, parameterNode, xmlDocument);

                string INDCORD = "N";
                apendParam("INDCORD", INDCORD, 1, espaco, parameterNode, xmlDocument);

                string DDD2 = "";
                apendParam("DDD2", DDD2, 4, zero, parameterNode, xmlDocument);

                string TEL2 = "";
                apendParam("TEL2", TEL2, 10, zero, parameterNode, xmlDocument);

                string RAMAL2 = "";
                apendParam("RAMAL2", RAMAL2, 6, zero, parameterNode, xmlDocument);

                string DDD3 = "";
                apendParam("DDD3", DDD3, 4, zero, parameterNode, xmlDocument);

                string TEL3 = "";
                apendParam("TEL3", TEL3, 10, zero, parameterNode, xmlDocument);

                string RAMAL3 = "";
                apendParam("RAMAL3", RAMAL3, 6, zero, parameterNode, xmlDocument);

            }

            return xmlDocument;
        }

        /// <summary>
        /// retorna arqEOP
        /// </summary>
        /// <param name="ordemCotista"></param>
        /// <returns></returns>
        public XmlDocument RetornaEop_Xml(OrdemCotista ordemCotista)
        {
            const int ID_FORMA_LIQUIDACAO_CETIP = 4;
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");

            XmlDocument xmlDocument = new XmlDocument();

            XmlNode rootNode = xmlDocument.CreateElement("itaumsg");
            xmlDocument.AppendChild(rootNode);

            XmlNode parameterNode = xmlDocument.CreateElement("parameter");
            rootNode.AppendChild(parameterNode);

            #region Config
            string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
            string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;
            string configCdBanCli = WebConfigurationManager.AppSettings["ConfigCdBanCli"];
            string ConfigCdBanc = WebConfigurationManager.AppSettings["ConfigCdBanc"];
            string configConta = ParametrosConfiguracaoSistema.Outras.ContaIntegracao;

            apendParam("campo0", configUsuarioIntegracao, 33, espaco, parameterNode, xmlDocument);
            apendParam("campo1", configSenhaIntegracao, 8, espaco, parameterNode, xmlDocument);
            apendParam("campo2", ConfigCdBanc, 6, zero, parameterNode, xmlDocument);

            string cdFdo = "";
            if (ordemCotista.IdCarteira != null)
            {
                int idFundo = (int)ordemCotista.IdCarteira.Value;
                ClienteInterface clienteInterface = new ClienteInterface();
                if (clienteInterface.LoadByPrimaryKey(idFundo))
                {
                    cdFdo = clienteInterface.CodigoYMF != null ? clienteInterface.CodigoYMF : "";
                }
            }
            apendParam("campo3", cdFdo, 5, zero, parameterNode, xmlDocument);

            apendParam("campo4", configCdBanCli, 6, zero, parameterNode, xmlDocument);
            #endregion

            int idCotista = ordemCotista.IdCotista.Value;

            #region Conta Corrente
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            BancoQuery bancoQuery = new BancoQuery("B");
            contaCorrenteQuery.InnerJoin(bancoQuery).On(bancoQuery.IdBanco == contaCorrenteQuery.IdBanco);
            contaCorrenteQuery.Where(bancoQuery.CodigoCompensacao.Like("%341%"),
                                     contaCorrenteQuery.IdPessoa.Equal(idCotista));
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
            contaCorrenteCollection.Load(contaCorrenteQuery);

            string strAgencia = "";
            string conta = "";
            string dac = "";

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);
            if (!String.IsNullOrEmpty(cotista.CodigoInterface))
            {
                strAgencia = cotista.CodigoInterface.Substring(0, 4);
                conta = cotista.CodigoInterface.Substring(4, 7);
                dac = cotista.CodigoInterface.Substring(11, 1);
            }

            apendParam("campo5", strAgencia, 4, zero, parameterNode, xmlDocument);
            apendParam("campo6", conta, 9, zero, parameterNode, xmlDocument);
            apendParam("campo7", dac.ToString(), 1, zero, parameterNode, xmlDocument);
            #endregion

            apendParam("campo8", configConta, 3, zero, parameterNode, xmlDocument);

            string opeMov = "";
            bool isAplicacao = false;
            if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
            {
                opeMov = "030";
                isAplicacao = true;
            }
            else if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                opeMov = "115";
            }
            else if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotas)
            {
                opeMov = "121";
            }
            else
            {
                opeMov = "105";
            }

            apendParam("campo9", opeMov, 3, zero, parameterNode, xmlDocument);

            decimal vLiqSol = 0;
            if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
            {
                vLiqSol = ordemCotista.ValorBruto.Value;
            }
            else if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto)
            {
                vLiqSol = ordemCotista.ValorBruto.Value;
            }
            else if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateLiquido)
            {
                vLiqSol = ordemCotista.ValorLiquido.Value;
            }
            else if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotas || ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                vLiqSol = ordemCotista.Quantidade.Value;
            }

            //string strVLigSol = String.Format("{0:0.00}", vLiqSol).Replace(",", "").Replace(".","");
            string strVLigSol = String.Format("{0:g}", Decimal.Round(vLiqSol * 100, 0));

            apendParam("campo10", strVLigSol, 15, zero, parameterNode, xmlDocument);

            string bcoAgct1 = "";
            if (!isAplicacao && ordemCotista.IdFormaLiquidacao.Value != ID_FORMA_LIQUIDACAO_CETIP)
            {
                #region Concatenação do campo bcoAgct1

                string stringBanco = "";
                string stringAgencia = "";
                string stringDACAgencia = "";
                string stringConta = "";
                string stringDACContaExterna = "";
                string stringDACContaItau = "";

                Financial.Investidor.ContaCorrente contaCorrenteCredito = new Financial.Investidor.ContaCorrente();
                if (ordemCotista.IdConta.HasValue)
                {
                    contaCorrenteCredito.LoadByPrimaryKey(ordemCotista.IdConta.Value);
                }
                Agencia agenciaCredito = new Agencia();

                if (contaCorrenteCredito.IdAgencia.HasValue)
                {
                    if (agenciaCredito.LoadByPrimaryKey(contaCorrenteCredito.IdAgencia.Value))
                    {
                        Banco bancoCredito = new Banco();
                        bancoCredito.LoadByPrimaryKey(agenciaCredito.IdBanco.Value);

                        if (!String.IsNullOrEmpty(bancoCredito.CodigoCompensacao))
                        {
                            stringBanco = bancoCredito.CodigoCompensacao.Trim().PadLeft(4, '0');
                            stringAgencia = agenciaCredito.Codigo.Trim().PadLeft(5, '0');

                            if (!String.IsNullOrEmpty(agenciaCredito.DigitoAgencia))
                            {
                                stringDACAgencia = agenciaCredito.DigitoAgencia.Trim();
                            }

                            stringConta = contaCorrenteCredito.Numero.Trim().PadLeft(11, '0');

                            bool contaItau = bancoCredito.CodigoCompensacao.Contains("341");
                            if (contaItau)
                            {
                                stringDACContaItau = contaCorrenteCredito.DigitoConta != null ? contaCorrenteCredito.DigitoConta.PadLeft(1, ' ') : " ";
                            }
                            else
                            {
                                stringDACContaExterna = contaCorrenteCredito.DigitoConta != null ? contaCorrenteCredito.DigitoConta.PadLeft(2, ' ') : "  ";
                            }
                        }
                    }
                }
                stringBanco = stringBanco.PadLeft(4, zero);
                stringAgencia = stringAgencia.PadLeft(5, zero);
                stringDACAgencia = stringDACAgencia.PadRight(1, espaco);
                stringConta = stringConta.PadLeft(11, zero);
                stringDACContaExterna = stringDACContaExterna.PadRight(2, espaco);
                stringDACContaItau = stringDACContaItau.PadRight(1, espaco);

                bcoAgct1 = stringBanco + stringAgencia + stringDACAgencia + stringConta + stringDACContaExterna + stringDACContaItau;
                #endregion
            }

            //********AJUSTAR DEPOIS***************************************************
            string cdTipoLiq = "";

            bool isResgate = !isAplicacao;

            Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
            if (ordemCotista.IdFormaLiquidacao.HasValue && formaLiquidacao.LoadByPrimaryKey(ordemCotista.IdFormaLiquidacao.Value))
            {
                string descricao = formaLiquidacao.Descricao.ToUpper();
                if (descricao == "DOC" && isResgate)
                {
                    cdTipoLiq = "D";
                }
                else if (descricao == "TED" && isResgate)
                {
                    cdTipoLiq = "S";
                }
                else if (descricao == "CETIP")
                {
                    cdTipoLiq = "C";
                }
            }

            if (String.IsNullOrEmpty(cdTipoLiq))
            {
                if (isAplicacao)
                {
                    cdTipoLiq = "R"; //Disponível
                }
                else
                {
                    cdTipoLiq = "F"; //Crédito em Conta Corrente Itaú
                }
            }

            if (cdTipoLiq == "C" && cotista.IdClienteEspelho.HasValue && isResgate)
            {
                //Encontrar carteira
                ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
                if (clienteRendaFixa.LoadByPrimaryKey(cotista.IdClienteEspelho.Value))
                {
                    bcoAgct1 = clienteRendaFixa.CodigoCetip;
                }
                bcoAgct1 = "";
            }

            apendParam("campo11", bcoAgct1, 24, espaco, parameterNode, xmlDocument);

            apendParam("campo12", "0", 6, zero, parameterNode, xmlDocument);

            //*************************************************************************

            apendParam("campo13", cdTipoLiq, 1, espaco, parameterNode, xmlDocument);

            apendParam("campo14", "0", 10, zero, parameterNode, xmlDocument);

            apendParam("campo15", "C", 1, espaco, parameterNode, xmlDocument);

            string datAgend = "";
            /*if (opeMov == "105" || opeMov == "106" || opeMov == "115" || opeMov == "116" ||
                opeMov == "121" || opeMov == "123" || opeMov == "125" || opeMov == "127")
            {
                if (ordemCotista.DataOperacao.Value.Date > DateTime.Today.Date)
                {
                    datAgend = ordemCotista.DataAgendamento.Value.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
            }*/

            apendParam("campo16", datAgend, 8, espaco, parameterNode, xmlDocument);

            //if (isResgate)
            if (true)//por enquanto sempre fazer... Parece que esta dando erro ateh pra aplicacao quando a liquidacao eh cetip !
            {
                apendParam("campo17", "", 8, espaco, parameterNode, xmlDocument);
            }
            else
            {
                string datLanct = "";
                if (ordemCotista.DataOperacao.HasValue)
                {
                    datLanct = ordemCotista.DataOperacao.Value.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
                }

                apendParam("campo17", datLanct, 8, espaco, parameterNode, xmlDocument);
            }

            apendParam("campo18", "", 8, espaco, parameterNode, xmlDocument);

            return xmlDocument;
        }

        /// <summary>
        /// retorna CCI
        /// </summary>
        /// <param name="idPessoa"></param>
        /// <param name="contaCorrenteCredito"></param>
        /// <param name="codMov"></param>
        /// <returns></returns>
        public XmlDocument RetornaCci_XML(int idPessoa, Financial.Investidor.ContaCorrente contaCorrenteCredito, string codMov)
        {
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");

            XmlDocument xmlDocument = new XmlDocument();

            XmlNode rootNode = xmlDocument.CreateElement("itaumsg");
            xmlDocument.AppendChild(rootNode);

            XmlNode parameterNode = xmlDocument.CreateElement("parameter");
            rootNode.AppendChild(parameterNode);

            #region Config
            string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
            string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;
            string configCdBanCli = WebConfigurationManager.AppSettings["ConfigCdBanCli"];

            apendParam("EBUSINESSID", configUsuarioIntegracao, 33, espaco, parameterNode, xmlDocument);
            apendParam("SENHA", configSenhaIntegracao, 8, espaco, parameterNode, xmlDocument);
            apendParam("CDBANCLI", configCdBanCli, 6, zero, parameterNode, xmlDocument);
            #endregion

            #region Conta Corrente

            string strAgencia = "";
            string conta = "";
            string dac = "";
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idPessoa);
            if (!String.IsNullOrEmpty(cotista.CodigoInterface))
            {
                strAgencia = cotista.CodigoInterface.Substring(0, 4);
                conta = cotista.CodigoInterface.Substring(4, 7);
                dac = cotista.CodigoInterface.Substring(11, 1);
            }

            apendParam("AGENCIA", strAgencia, 4, zero, parameterNode, xmlDocument);
            apendParam("CDCTA", conta, 9, zero, parameterNode, xmlDocument);
            apendParam("DAC10", dac.ToString(), 1, zero, parameterNode, xmlDocument);
            #endregion

            apendParam("CODMOV", codMov, 1, espaco, parameterNode, xmlDocument);

            apendParam("IDTIPCT1", "C", 1, espaco, parameterNode, xmlDocument);

            #region Concatenação do campo bcoAgct1
            string bcoAgct1 = "";
            string stringBanco = "";
            string stringAgencia = "";
            string stringDACAgencia = "";
            string stringConta = "";
            string stringDACContaExterna = "";
            string stringDACContaItau = "";

            Agencia agenciaCredito = new Agencia();
            if (contaCorrenteCredito.IdAgencia.HasValue)
            {
                if (agenciaCredito.LoadByPrimaryKey(contaCorrenteCredito.IdAgencia.Value))
                {
                    Banco bancoCredito = new Banco();
                    bancoCredito.LoadByPrimaryKey(agenciaCredito.IdBanco.Value);

                    if (!String.IsNullOrEmpty(bancoCredito.CodigoCompensacao))
                    {
                        stringBanco = bancoCredito.CodigoCompensacao.Trim().PadLeft(4, '0');
                        stringAgencia = agenciaCredito.Codigo.Trim().PadLeft(5, '0');

                        if (!String.IsNullOrEmpty(agenciaCredito.DigitoAgencia))
                        {
                            stringDACAgencia = agenciaCredito.DigitoAgencia.Trim();
                        }

                        stringConta = contaCorrenteCredito.Numero.Trim().PadLeft(11, '0');

                        bool contaItau = bancoCredito.CodigoCompensacao.Contains("341");
                        if (contaItau)
                        {
                            stringDACContaItau = contaCorrenteCredito.DigitoConta != null ? contaCorrenteCredito.DigitoConta.PadLeft(1, ' ') : " ";
                        }
                        else
                        {
                            stringDACContaExterna = contaCorrenteCredito.DigitoConta != null ? contaCorrenteCredito.DigitoConta.PadLeft(2, ' ') : "  ";
                        }
                    }
                }
            }
            stringBanco = stringBanco.PadLeft(4, zero);
            stringAgencia = stringAgencia.PadLeft(5, zero);
            stringDACAgencia = stringDACAgencia.PadRight(1, espaco);
            stringConta = stringConta.PadLeft(11, zero);
            stringDACContaExterna = stringDACContaExterna.PadRight(2, espaco);
            stringDACContaItau = stringDACContaItau.PadRight(1, espaco);

            bcoAgct1 = stringBanco + stringAgencia + stringDACAgencia + stringConta + stringDACContaExterna + stringDACContaItau;
            #endregion

            apendParam("BCOAGCT1", bcoAgct1, 24, espaco, parameterNode, xmlDocument);

            apendParam("IDTPCT01", "P", 1, espaco, parameterNode, xmlDocument);

            apendParam("IDCGCCPF", "", 15, espaco, parameterNode, xmlDocument);

            apendParam("IDTTCI01", "1", 1, espaco, parameterNode, xmlDocument);

            apendParam("IDCLIT01", "", 25, espaco, parameterNode, xmlDocument);

            apendParam("IDOPEMAC", "0", 6, zero, parameterNode, xmlDocument);

            return xmlDocument;
        }

        /// <summary>
        /// retorna CCO
        /// </summary>
        /// <param name="idPessoa"></param>
        /// <param name="contaCorrenteCredito"></param>
        /// <param name="codMov"></param>
        /// <returns></returns>
        public XmlDocument RetornaCco_XML(int idPessoa, string codMov)
        {
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");

            XmlDocument xmlDocument = new XmlDocument();

            XmlNode rootNode = xmlDocument.CreateElement("itaumsg");
            xmlDocument.AppendChild(rootNode);

            XmlNode parameterNode = xmlDocument.CreateElement("parameter");
            rootNode.AppendChild(parameterNode);

            #region Config

            string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
            string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;
            string configCdBanCli = WebConfigurationManager.AppSettings["ConfigCdBanCli"];

            apendParam("EBUSINESSID", configUsuarioIntegracao, 33, espaco, parameterNode, xmlDocument);
            apendParam("SENHA", configSenhaIntegracao, 8, espaco, parameterNode, xmlDocument);
            apendParam("CODMOV", codMov, 1, espaco, parameterNode, xmlDocument);
            apendParam("CDBANCLI", configCdBanCli, 6, zero, parameterNode, xmlDocument);
            #endregion

            #region Conta Corrente


            string strAgencia = "";
            string conta = "";
            string dac = "";
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idPessoa);
            if (!String.IsNullOrEmpty(cotista.CodigoInterface))
            {
                strAgencia = cotista.CodigoInterface.Substring(0, 4);
                conta = cotista.CodigoInterface.Substring(4, 7);
                dac = cotista.CodigoInterface.Substring(11, 1);
            }

            apendParam("AGENCIA", strAgencia, 4, zero, parameterNode, xmlDocument);
            apendParam("CDCTA", conta, 9, zero, parameterNode, xmlDocument);
            apendParam("DAC10", dac.ToString(), 1, zero, parameterNode, xmlDocument);
            #endregion

            

            #region Pessoa
            Pessoa pessoa = new Pessoa();

            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                string nmCli = "";

                if (pessoa.Nome != null)
                {
                    nmCli = pessoa.Nome;
                }
                else
                {
                    nmCli = "-";
                }

                apendParam("NMCLI", nmCli, 30, espaco, parameterNode, xmlDocument);

                string dtNasc8 = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica && pessoa.DataNascimento != null)
                {
                    dtNasc8 = pessoa.DataNascimento.Value.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica && pessoa.DataNascimento == null)
                    {
                        dtNasc8 = "01011900";
                    }
                }

                apendParam("DTNASCL8", dtNasc8, 8, espaco, parameterNode, xmlDocument);

                string pes = "F";

                apendParam("PES", pes, 1, espaco, parameterNode, xmlDocument);

                string cpf = "";
                if (pessoa.Cpfcnpj != null)
                {
                    cpf = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "");
                }

                apendParam("CPF", cpf, 11, zero, parameterNode, xmlDocument);

                #region PessoaDocumento
                string tpDocIde = "";
                string idDocLi = "";
                string dtDocExp = "";
                string idOrgEmi = "";
                string idEstEmi = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();
                    pessoaDocumentoCollection.Query.Where(pessoaDocumentoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
                    pessoaDocumentoCollection.Query.Load();

                    if (pessoaDocumentoCollection.Count > 0)
                    {
                        bool temDoc = true;
                        PessoaDocumento pessoaDocumento = pessoaDocumentoCollection[0];
                        if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_CI)
                        {
                            tpDocIde = "CI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_CIE)
                        {
                            tpDocIde = "CIE";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeMilitar)
                        {
                            tpDocIde = "CIM";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissional)
                        {
                            tpDocIde = "CIP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissionalOrgaoPublico)
                        {
                            tpDocIde = "CIPF";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CertidaoNascimento)
                        {
                            tpDocIde = "CN";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraHabilitacao)
                        {
                            tpDocIde = "CNH";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraTrabalhoProfissional)
                        {
                            tpDocIde = "CTPS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.IdentificaçãoPrisional)
                        {
                            tpDocIde = "IP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.LaissezPasser)
                        {
                            tpDocIde = "LPASS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.Passaporte)
                        {
                            tpDocIde = "PASSP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_RG)
                        {
                            tpDocIde = "RG";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.DocumentoIdentidade)
                        {
                            tpDocIde = "RI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_RNE)
                        {
                            tpDocIde = "RNE";
                        }

                        if (temDoc)
                        {
                            if (pessoaDocumento.NumeroDocumento != null)
                            {
                                idDocLi = pessoaDocumento.NumeroDocumento;
                            }

                            if (pessoaDocumento.DataExpedicao != null)
                            {
                                dtDocExp = pessoaDocumento.DataExpedicao.Value.ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            if (pessoaDocumento.OrgaoEmissor != null)
                            {
                                idOrgEmi = pessoaDocumento.OrgaoEmissor;
                            }
                            if (pessoaDocumento.UFEmissor != null)
                            {
                                idEstEmi = pessoaDocumento.UFEmissor;
                            }
                        }
                    }

                }

                apendParam("TPDOCIDE", tpDocIde, 5, espaco, parameterNode, xmlDocument);
                apendParam("IDDOCLI", idDocLi, 13, espaco, parameterNode, xmlDocument);
                apendParam("DTDOCEXP", dtDocExp, 8, espaco, parameterNode, xmlDocument);
                apendParam("IDORGEMI", idOrgEmi, 7, espaco, parameterNode, xmlDocument);
                apendParam("IDESTEMI", idEstEmi, 2, espaco, parameterNode, xmlDocument);
                #endregion

                string sitLeg = "L";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    if (pessoa.SituacaoLegal != null)
                    {
                        if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Espolio)
                        {
                            sitLeg = "E";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Interdito)
                        {
                            sitLeg = "I";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Maior)
                        {
                            sitLeg = "L";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Menor)
                        {
                            sitLeg = "M";
                        }
                        else if (pessoa.SituacaoLegal.Value == (byte)SituacaoLegalPessoa.Emancipado)
                        {
                            sitLeg = "N";
                        }
                    }
                }
                apendParam("SITLEG", sitLeg, 1, espaco, parameterNode, xmlDocument);

                string codSex = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    codSex = "M";
                    if (pessoa.Sexo != null)
                    {
                        codSex = pessoa.Sexo.ToString();
                    }
                }
                apendParam("CODSEX", codSex, 1, espaco, parameterNode, xmlDocument);

                string cdProAti = "999";
                apendParam("CDPROATI", cdProAti, 3, espaco, parameterNode, xmlDocument);

                string cdEstCiv = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    cdEstCiv = "1";
                    if (pessoa.EstadoCivil != null)
                    {
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Casado)
                        {
                            cdEstCiv = "1";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Divorciado)
                        {
                            cdEstCiv = "5";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Solteiro)
                        {
                            cdEstCiv = "4";
                        }
                        if (pessoa.EstadoCivil == (byte)EstadoCivilPessoa.Viuvo)
                        {
                            cdEstCiv = "6";
                        }
                    }
                }
                apendParam("CDESTCIV", cdEstCiv, 1, espaco, parameterNode, xmlDocument);





            }
            #endregion

            apendParam("VRENDFAM", "0", 11, zero, parameterNode, xmlDocument);

            apendParam("VLPATRIM", "0", 15, zero, parameterNode, xmlDocument);


            //INI
            string CODNACL = "";
            apendParam("CODNACL", CODNACL, 3, zero, parameterNode, xmlDocument);

            string CDPAINAS = "";
            apendParam("CDPAINAS", CDPAINAS, 3, zero, parameterNode, xmlDocument);

            #region Endereco
            string noLog = "";
            string nuLog = "";
            string cpLog = "";
            string bairr = "";
            string cep = "";
            string cidad = "";
            string uf = "";
            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
            if (pessoaEnderecoCollection.Query.Load())
            {
                if (pessoaEnderecoCollection[0].Endereco != null)
                {
                    noLog = pessoaEnderecoCollection[0].Endereco;
                }
                if (pessoaEnderecoCollection[0].Numero != null)
                {
                    nuLog = pessoaEnderecoCollection[0].Numero;
                }
                if (pessoaEnderecoCollection[0].Complemento != null)
                {
                    cpLog = pessoaEnderecoCollection[0].Complemento;
                }
                if (pessoaEnderecoCollection[0].Bairro != null)
                {
                    bairr = pessoaEnderecoCollection[0].Bairro;
                }
                if (pessoaEnderecoCollection[0].Cidade != null)
                {
                    cidad = pessoaEnderecoCollection[0].Cidade;
                }
                if (pessoaEnderecoCollection[0].Uf != null)
                {
                    uf = pessoaEnderecoCollection[0].Uf;
                }
                if (pessoaEnderecoCollection[0].Cep != null)
                {
                    cep = pessoaEnderecoCollection[0].Cep;
                }
            }

            apendParam("LOGRAD", noLog, 60, espaco, parameterNode, xmlDocument);

            apendParam("NUMLEC", nuLog, 6, espaco, parameterNode, xmlDocument);

            apendParam("COMPL", cpLog, 25, espaco, parameterNode, xmlDocument);

            apendParam("BAIRRO", bairr, 20, espaco, parameterNode, xmlDocument);

            apendParam("CIDADE", cidad, 40, espaco, parameterNode, xmlDocument);

            apendParam("ESTADO", uf, 40, espaco, parameterNode, xmlDocument);

            apendParam("CEP10", cep, 10, espaco, parameterNode, xmlDocument);

            #endregion

            apendParam("PAIRESD", "", 3, espaco, parameterNode, xmlDocument);

            string idResFis = "N";
            string pais = "";
            pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                pessoaEnderecoCollection.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Residencial));
            if (pessoaEnderecoCollection.Query.Load())
            {
                pais = pessoaEnderecoCollection[0].Pais;
                if (pais != "Brasil")
                {
                    idResFis = "S";
                }
                else
                {
                    idResFis = "N";
                }
            }
            apendParam("IDRESFIS", idResFis, 1, espaco, parameterNode, xmlDocument);

            string paiResD = "";
            switch (pais)
            {
                case "Afeganistao": paiResD = "013"; break;
                case "Antilhas Holandesas": paiResD = "047"; break;
                case "Argentina": paiResD = "063"; break;
                case "Austria": paiResD = "072"; break;
                case "Burundi": paiResD = "031"; break;
                case "Belgica": paiResD = "087"; break;
                case "Benin": paiResD = "229"; break;
                case "Bangladesh": paiResD = "081"; break;
                case "Bahrein, Ilhas": paiResD = "080"; break;
                case "Belize": paiResD = "088"; break;
                case "Bermudas": paiResD = "090"; break;
                case "Barbados": paiResD = "083"; break;
                case "Botsuana": paiResD = "101"; break;
                case "Chile": paiResD = "158"; break;
                case "China, Republica Popular": paiResD = "160"; break;
                case "Ucrania": paiResD = "831"; break;
                case "Costa Do Marfim": paiResD = "193"; break;
                case "Camaroes": paiResD = "145"; break;
                case "Congo": paiResD = "177"; break;
                case "Colombia": paiResD = "169"; break;
                case "Saara Ocidental": paiResD = "685"; break;
                case "Cuba": paiResD = "199"; break;
                case "Chipre": paiResD = "163"; break;
                case "Dinamarca": paiResD = "232"; break;
                case "Dominica, Ilha": paiResD = "235"; break;
                case "Republica Dominicana": paiResD = "647"; break;
                case "Argelia": paiResD = "059"; break;
                case "Equador": paiResD = "239"; break;
                case "Costa Rica": paiResD = "196"; break;
                case "Egito": paiResD = "240"; break;
                case "Djibouti": paiResD = "783"; break;
                case "Espanha": paiResD = "245"; break;
                case "Etiopia": paiResD = "253"; break;
                case "Emirados Arabes Unidos": paiResD = "244"; break;
                case "Fidji": paiResD = "870"; break;
                case "Franca": paiResD = "275"; break;
                case "Gana": paiResD = "289"; break;
                case "Gambia": paiResD = "285"; break;
                case "Grecia": paiResD = "301"; break;
                case "Guatemala": paiResD = "317"; break;
                case "Guiana": paiResD = "337"; break;
                case "Hong Kong": paiResD = "351"; break;
                case "Formosa (Taiwan)": paiResD = "161"; break;
                case "Burkina Faso": paiResD = "115"; break;
                case "Canarias, Ilhas": paiResD = "151"; break;
                case "Georgia, Republica Da": paiResD = "291"; break;
                case "Granada": paiResD = "297"; break;
                case "Indonesia": paiResD = "365"; break;
                case "India": paiResD = "361"; break;
                case "Ira, Republica Islamica Do": paiResD = "372"; break;
                case "Irlanda": paiResD = "375"; break;
                case "Samoa Ocidental": paiResD = "690"; break;
                case "Islandia": paiResD = "379"; break;
                case "Italia": paiResD = "386"; break;
                case "Estonia, Republica Da": paiResD = "251"; break;
                case "Virgens Americanas, Ilhas": paiResD = "866"; break;
                case "Jamaica": paiResD = "391"; break;
                case "Japao": paiResD = "399"; break;
                case "Finlandia": paiResD = "271"; break;
                case "Coreia, Republica Da": paiResD = "190"; break;
                case "Laos, Republica Popular Democratica": paiResD = "420"; break;
                case "Liberia": paiResD = "434"; break;
                case "Libia": paiResD = "438"; break;
                case "Lituania, Republica Da": paiResD = "442"; break;
                case "Sri Lanka": paiResD = "750"; break;
                case "Macau": paiResD = "447"; break;
                case "Marrocos": paiResD = "474"; break;
                case "Madagascar": paiResD = "450"; break;
                case "Malta": paiResD = "467"; break;
                case "Monaco": paiResD = "495"; break;
                case "Mauricio": paiResD = "485"; break;
                case "Malavi": paiResD = "458"; break;
                case "Nauru": paiResD = "508"; break;
                case "Niger": paiResD = "525"; break;
                case "Nicaragua": paiResD = "521"; break;
                case "Groelandia": paiResD = "305"; break;
                case "Noruega": paiResD = "538"; break;
                case "Panama": paiResD = "580"; break;
                case "Paraguai": paiResD = "586"; break;
                case "Peru": paiResD = "589"; break;
                case "Filipinas": paiResD = "267"; break;
                case "Polonia, Republica Da": paiResD = "603"; break;
                case "Portugal": paiResD = "607"; break;
                case "Gabao": paiResD = "281"; break;
                case "Reuniao, Ilha": paiResD = "660"; break;
                case "Ruanda": paiResD = "675"; break;
                case "Arabia Saudita": paiResD = "053"; break;
                case "Belarus, Republica Da": paiResD = "085"; break;
                case "Senegal": paiResD = "728"; break;
                case "Cingapura": paiResD = "741"; break;
                case "Siria, Republica Arabe Da": paiResD = "744"; break;
                case "Serra Leoa": paiResD = "735"; break;
                case "El Salvador": paiResD = "687"; break;
                case "San Marino": paiResD = "697"; break;
                case "Santa Lucia": paiResD = "715"; break;
                case "Suecia": paiResD = "764"; break;
                case "Togo": paiResD = "800"; break;
                case "Tailandia": paiResD = "776"; break;
                case "Tonga": paiResD = "810"; break;
                case "Trinidad E Tobago": paiResD = "815"; break;
                case "Turquia": paiResD = "827"; break;
                case "Tanzania, Republica Unida Da": paiResD = "780"; break;
                case "Uruguai": paiResD = "845"; break;
                case "Estados Unidos": paiResD = "249"; break;
                case "Venezuela": paiResD = "850"; break;
                case "Virgens Britanicas, Ilhas": paiResD = "863"; break;
                case "Zambia": paiResD = "890"; break;
                case "Alemanha": paiResD = "023"; break;
                case "Australia": paiResD = "069"; break;
                case "Bahamas, Ilhas": paiResD = "077"; break;
                case "Bolivia": paiResD = "097"; break;
                case "Brasil": paiResD = "105"; break;
                case "Canada": paiResD = "149"; break;
                case "Gibraltar": paiResD = "293"; break;
                case "Guadalupe": paiResD = "309"; break;
                case "Haiti": paiResD = "341"; break;
                case "Hungria, Republica Da": paiResD = "355"; break;
                case "Inglaterra": paiResD = "367"; break;
                case "Iraque": paiResD = "369"; break;
                case "Israel": paiResD = "383"; break;
                case "Iugoslavia, Republica Federativa": paiResD = "388"; break;
                case "Jordania": paiResD = "403"; break;
                case "Libano": paiResD = "431"; break;
                case "Liechtenstein": paiResD = "440"; break;
                case "Luxemburgo": paiResD = "445"; break;
                case "Malasia": paiResD = "455"; break;
                case "Mali": paiResD = "464"; break;
                case "Martinica": paiResD = "477"; break;
                case "Mexico": paiResD = "493"; break;
                case "Nepal": paiResD = "517"; break;
                case "Niue, Ilha": paiResD = "531"; break;
                case "Nova Zelandia": paiResD = "548"; break;
                case "Paquistao": paiResD = "576"; break;
                case "Porto Rico": paiResD = "611"; break;
                case "Quenia": paiResD = "623"; break;
                case "Romenia": paiResD = "670"; break;
                case "Russia, Federacao Da": paiResD = "676"; break;
                case "Sudao": paiResD = "759"; break;
                case "Suica": paiResD = "767"; break;
                case "Tcheca, Republica": paiResD = "791"; break;
                case "Tunisia": paiResD = "820"; break;
                case "Uganda": paiResD = "833"; break;
                case "Vanuatu  Pacific": paiResD = "551"; break;
                case "Zaire": paiResD = "888"; break;
                case "Zimbabue": paiResD = "665"; break;
                case "Republica Centro Africana": paiResD = "640"; break;
                case "Africa Do Sul": paiResD = "756"; break;
                case "Albania, Republica Da": paiResD = "017"; break;
                case "Andorra": paiResD = "037"; break;
                case "Angola": paiResD = "040"; break;
                case "Anguilla": paiResD = "041"; break;
                case "Antigua Barbuda": paiResD = "043"; break;
                case "Armenia, Republica Da": paiResD = "064"; break;
                case "Aruba": paiResD = "065"; break;
                case "Arzebaijao, Republica Do": paiResD = "073"; break;
                case "Bosnia-Herzegovinia": paiResD = "098"; break;
                case "Brunei": paiResD = "108"; break;
                case "Bulgaria, Republica Da": paiResD = "111"; break;
                case "Butao": paiResD = "119"; break;
                case "Cabo Verde, Republica De": paiResD = "127"; break;
                case "Camboja": paiResD = "141"; break;
                case "Casaquistao, Republica Do": paiResD = "153"; break;
                case "Catar": paiResD = "154"; break;
                case "Cayman, Ilhas": paiResD = "137"; break;
                case "Chade": paiResD = "788"; break;
                case "Christmas, Ilhas": paiResD = "511"; break;
                case "Cocos-Keeling, Ilhas": paiResD = "165"; break;
                case "Comores, Ilhas": paiResD = "173"; break;
                case "Cook, Ilhas": paiResD = "183"; break;
                case "Coreia, Republica Popular Democratica": paiResD = "187"; break;
                case "Coveite": paiResD = "198"; break;
                case "Croacia, Republica Da": paiResD = "195"; break;
                case "Dubai": paiResD = "237"; break;
                case "Eslovaca, Republica": paiResD = "247"; break;
                case "Eslovenia, Republica": paiResD = "246"; break;
                case "Falkland (Malvinas)": paiResD = "255"; break;
                case "Feroe, Ilhas": paiResD = "259"; break;
                case "Fezzan": paiResD = "263"; break;
                case "Guam": paiResD = "313"; break;
                case "Guiana Francesa": paiResD = "325"; break;
                case "Guine": paiResD = "329"; break;
                case "Guine-Bissau": paiResD = "334"; break;
                case "Guine-Equatorial": paiResD = "331"; break;
                case "Honduras": paiResD = "345"; break;
                case "Iemen": paiResD = "357"; break;
                case "Johnston, Ilhas": paiResD = "396"; break;
                case "Kiribati": paiResD = "411"; break;
                case "Lebuan, Ilhas": paiResD = "423"; break;
                case "Lesoto": paiResD = "426"; break;
                case "Letonia, Republica Da": paiResD = "427"; break;
                case "Macedonia": paiResD = "449"; break;
                case "Maldivas": paiResD = "461"; break;
                case "Marianas Do Norte": paiResD = "472"; break;
                case "Marshall, Ilhas": paiResD = "476"; break;
                case "Mauritania": paiResD = "488"; break;
                case "Mianmar (Birmania)": paiResD = "093"; break;
                case "Micronesia": paiResD = "499"; break;
                case "Midway, Ilhas": paiResD = "490"; break;
                case "Mocambique": paiResD = "505"; break;
                case "Moldova, Republica Da": paiResD = "494"; break;
                case "Mongolia": paiResD = "497"; break;
                case "Montserrat, Ilhas": paiResD = "501"; break;
                case "Namibia": paiResD = "507"; break;
                case "Norfolk, Ilha": paiResD = "535"; break;
                case "Nigeria": paiResD = "528"; break;
                case "Nova Caledonia": paiResD = "542"; break;
                case "Oma": paiResD = "556"; break;
                case "Pacifico(Adm Eua), Ilhas Do": paiResD = "563"; break;
                case "Pacifico(Posse Eua), Ilhas Do": paiResD = "566"; break;
                case "Paises Baixos(Holanda)": paiResD = "573"; break;
                case "Palau": paiResD = "575"; break;
                case "Papua Nova Guine": paiResD = "545"; break;
                case "Pitcaim, Ilha De": paiResD = "593"; break;
                case "Polinesia Francesa": paiResD = "599"; break;
                case "Quirguiz, Republica Da": paiResD = "625"; break;
                case "Reino Unido": paiResD = "628"; break;
                case "Salomao, Ilhas": paiResD = "677"; break;
                case "Samoa Americana": paiResD = "691"; break;
                case "Santa Helena": paiResD = "710"; break;
                case "Sao Cristovao E Neves, Ilhas": paiResD = "695"; break;
                case "Sao Pedro E Miquelon": paiResD = "700"; break;
                case "Sao Tome E Principe, Ilhas": paiResD = "720"; break;
                case "Sao Vicente E Granadinas": paiResD = "705"; break;
                case "Seychelles": paiResD = "731"; break;
                case "Somalia": paiResD = "748"; break;
                case "Suazilandia": paiResD = "754"; break;
                case "Suriname": paiResD = "770"; break;
                case "Tadjiquistao": paiResD = "772"; break;
                case "Territorio Britanico Oceano Indico": paiResD = "782"; break;
                case "Timor Leste": paiResD = "795"; break;
                case "Toquelau, Ilhas": paiResD = "805"; break;
                case "Turcas E Caicos": paiResD = "823"; break;
                case "Turcomenistao, Republica Do": paiResD = "824"; break;
                case "Tuvalu": paiResD = "828"; break;
                case "Uzbequistao, Republica Do": paiResD = "847"; break;
                case "Vaticano, Estado Da Cidade Do": paiResD = "848"; break;
                case "Vietna": paiResD = "858"; break;
                case "Wallis E Futuna, Ilhas": paiResD = "875"; break;
            }

            apendParam("PAIRESD", paiResD, 3, espaco, parameterNode, xmlDocument);

            string giinTin = "";
            if (pessoa.GiinTin != null)
            {
                giinTin = pessoa.GiinTin;
            }
            apendParam("GIINTIN", giinTin, 16, espaco, parameterNode, xmlDocument);

            #region PessoaEmail
            string email = "";
            PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
            pessoaEmailCollection.Query.Where(pessoaEmailCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
            if (pessoaEmailCollection.Query.Load())
            {
                email = pessoaEmailCollection[0].Email;
            }
            apendParam("EMAIL", email, 80, espaco, parameterNode, xmlDocument);
            #endregion

            #region PessoaTelefone
            string ddd = "";
            string numero = "";
            string ramal = "";
            PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
            pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa.Equal(pessoa.IdPessoa),
                                                 pessoaTelefoneCollection.Query.TipoTelefone.Equal(TipoTelefonePessoa.Residencial));
            if (pessoaTelefoneCollection.Query.Load())
            {
                if (pessoaTelefoneCollection[0].Ddd != null)
                {
                    ddd = pessoaTelefoneCollection[0].Ddd;
                }
                if (pessoaTelefoneCollection[0].Numero != null)
                {
                    numero = pessoaTelefoneCollection[0].Numero;
                }
                if (pessoaTelefoneCollection[0].Ramal != null)
                {
                    ramal = pessoaTelefoneCollection[0].Ramal;
                }
            }



            apendParam("DDD1", ddd, 4, zero, parameterNode, xmlDocument);
            apendParam("TEL1", numero, 10, zero, parameterNode, xmlDocument);
            apendParam("RAMAL1", ramal, 6, zero, parameterNode, xmlDocument);

            apendParam("DDD2", "", 4, zero, parameterNode, xmlDocument);
            apendParam("TEL2", "", 10, zero, parameterNode, xmlDocument);
            apendParam("RAMAL2", "", 6, zero, parameterNode, xmlDocument);



            #endregion

            string emprTrab = "";
            apendParam("EMPRTRAB", emprTrab, 30, espaco, parameterNode, xmlDocument);

            string DTINIEMP = "";
            apendParam("DTINIEMP", DTINIEMP, 10, espaco, parameterNode, xmlDocument);

            string NOMEPAI = pessoa.FiliacaoNomePai;
            apendParam("NOMEPAI", NOMEPAI, 30, espaco, parameterNode, xmlDocument);

            string NOMEMAE = pessoa.FiliacaoNomeMae;
            apendParam("NOMEMAE", NOMEMAE, 30, espaco, parameterNode, xmlDocument);

            string NOMECONJ = "";
            apendParam("NOMECONJ", NOMECONJ, 30, espaco, parameterNode, xmlDocument);

            string CIDANASC = "";
            apendParam("CIDANASC", CIDANASC, 6, espaco, parameterNode, xmlDocument);

            return xmlDocument;
        }

        /// <summary>
        /// retorna UPC
        /// </summary>
        /// <param name="idPessoa"></param>
        /// <param name="codMov"></param>
        /// <returns></returns>
        public XmlDocument RetornaUpc_XML(int idPessoa, string codMov)
        {
            char espaco = Convert.ToChar(" ");
            char zero = Convert.ToChar("0");

            XmlDocument xmlDocument = new XmlDocument();

            XmlNode rootNode = xmlDocument.CreateElement("itaumsg");
            xmlDocument.AppendChild(rootNode);

            XmlNode parameterNode = xmlDocument.CreateElement("parameter");
            rootNode.AppendChild(parameterNode);

            #region Config
            string configUsuarioIntegracao = ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao;
            string configSenhaIntegracao = ParametrosConfiguracaoSistema.Outras.SenhaIntegracao;
            string configCdBanCli = WebConfigurationManager.AppSettings["ConfigCdBanCli"];

            apendParam("EBUSINESSID", configUsuarioIntegracao, 33, espaco, parameterNode, xmlDocument);
            apendParam("SENHA", configSenhaIntegracao, 8, espaco, parameterNode, xmlDocument);
            apendParam("CDBANCLI", configCdBanCli, 6, zero, parameterNode, xmlDocument);
            #endregion

            #region Conta Corrente
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            BancoQuery bancoQuery = new BancoQuery("B");
            contaCorrenteQuery.InnerJoin(bancoQuery).On(bancoQuery.IdBanco.Equal(contaCorrenteQuery.IdBanco));
            contaCorrenteQuery.Where(bancoQuery.CodigoCompensacao.Like("%341%"),
                                     contaCorrenteQuery.IdPessoa.Equal(idPessoa));
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
            contaCorrenteCollection.Load(contaCorrenteQuery);

            string strAgencia = "";
            string conta = "";
            string dac = "";
            if (contaCorrenteCollection.Count > 0)
            {
                Agencia agencia = new Agencia();
                if (agencia.LoadByPrimaryKey(Convert.ToInt32(contaCorrenteCollection[0].IdAgencia)))
                {
                    strAgencia = agencia.Codigo.ToString();
                }
                if (contaCorrenteCollection[0].Numero != null)
                {
                    conta = contaCorrenteCollection[0].Numero;
                }
                if (contaCorrenteCollection[0].DigitoConta != null)
                {
                    dac = contaCorrenteCollection[0].DigitoConta;
                }
            }

            apendParam("AGENCIA", strAgencia, 4, zero, parameterNode, xmlDocument);
            apendParam("CDCTA", conta, 9, zero, parameterNode, xmlDocument);
            apendParam("DAC10", dac.ToString(), 1, zero, parameterNode, xmlDocument);
            #endregion

            apendParam("CODMOV", codMov, 1, espaco, parameterNode, xmlDocument);

            string cdPesqEx = idPessoa.ToString();

            apendParam("CDPESQEX", cdPesqEx, 5, espaco, parameterNode, xmlDocument);

            #region Pessoa

            string idClasRf = "OUT"; //ver tabela 09

            apendParam("IDCLASRF", idClasRf, 4, espaco, parameterNode, xmlDocument);

            Pessoa pessoa = new Pessoa();

            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                string nmPesPad = "";

                if (pessoa.Nome != null)
                {
                    nmPesPad = pessoa.Nome;
                }
                else
                {
                    nmPesPad = "-";
                }

                apendParam("NMPESPAD", nmPesPad, 60, espaco, parameterNode, xmlDocument);

                apendParam("INCONPEP", " ", 1, espaco, parameterNode, xmlDocument);

                string pes = "F";

                apendParam("PES", pes, 1, espaco, parameterNode, xmlDocument);

                string codSex = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    codSex = "M";
                    if (pessoa.Sexo != null)
                    {
                        codSex = pessoa.Sexo.ToString();
                    }
                }
                apendParam("CODSEX", codSex, 1, espaco, parameterNode, xmlDocument);

                string cgccpf = "";
                if (pessoa.Cpfcnpj != null)
                {
                    cgccpf = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "");
                }

                apendParam("IDCGCCPF", cgccpf, 15, zero, parameterNode, xmlDocument);

                #region PessoaDocumento
                string tpDocIde = "";
                string idDocLi = "";
                string dtDocExp = "";
                string idOrgEmi = "";
                string idEstEmi = "";
                if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();
                    pessoaDocumentoCollection.Query.Where(pessoaDocumentoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
                    pessoaDocumentoCollection.Query.Load();

                    if (pessoaDocumentoCollection.Count > 0)
                    {
                        bool temDoc = true;
                        PessoaDocumento pessoaDocumento = pessoaDocumentoCollection[0];
                        if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_CI)
                        {
                            tpDocIde = "CI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_CIE)
                        {
                            tpDocIde = "CIE";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeMilitar)
                        {
                            tpDocIde = "CIM";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissional)
                        {
                            tpDocIde = "CIP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissionalOrgaoPublico)
                        {
                            tpDocIde = "CIPF";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CertidaoNascimento)
                        {
                            tpDocIde = "CN";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraHabilitacao)
                        {
                            tpDocIde = "CNH";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraTrabalhoProfissional)
                        {
                            tpDocIde = "CTPS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.IdentificaçãoPrisional)
                        {
                            tpDocIde = "IP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.LaissezPasser)
                        {
                            tpDocIde = "LPASS";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.Passaporte)
                        {
                            tpDocIde = "PASSP";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_RG)
                        {
                            tpDocIde = "RG";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.DocumentoIdentidade)
                        {
                            tpDocIde = "RI";
                        }
                        else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_RNE)
                        {
                            tpDocIde = "RNE";
                        }

                        if (temDoc)
                        {
                            if (pessoaDocumento.NumeroDocumento != null)
                            {
                                idDocLi = pessoaDocumento.NumeroDocumento;
                            }

                            if (pessoaDocumento.DataExpedicao != null)
                            {
                                dtDocExp = pessoaDocumento.DataExpedicao.Value.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            if (pessoaDocumento.OrgaoEmissor != null)
                            {
                                idOrgEmi = pessoaDocumento.OrgaoEmissor;
                            }
                            if (pessoaDocumento.UFEmissor != null)
                            {
                                idEstEmi = pessoaDocumento.UFEmissor;
                            }
                        }
                    }

                }

                apendParam("TPDOCIDE", tpDocIde, 5, espaco, parameterNode, xmlDocument);
                apendParam("IDDOCLI", idDocLi, 13, espaco, parameterNode, xmlDocument);
                apendParam("DTDOCEXP", dtDocExp, 8, espaco, parameterNode, xmlDocument);
                apendParam("IDORGEMI", idOrgEmi, 7, espaco, parameterNode, xmlDocument);
                apendParam("IDESTEMI", idEstEmi, 2, espaco, parameterNode, xmlDocument);
                #endregion

                #region Endereco
                string noLog = "";
                string nuLog = "";
                string cpLog = "";
                string bairr = "";
                string cep = "";
                string cidad = "";
                string uf = "";
                PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(pessoa.IdPessoa));
                if (pessoaEnderecoCollection.Query.Load())
                {
                    if (pessoaEnderecoCollection[0].Endereco != null)
                    {
                        noLog = pessoaEnderecoCollection[0].Endereco;
                    }
                    if (pessoaEnderecoCollection[0].Numero != null)
                    {
                        nuLog = pessoaEnderecoCollection[0].Numero;
                    }
                    if (pessoaEnderecoCollection[0].Complemento != null)
                    {
                        cpLog = pessoaEnderecoCollection[0].Complemento;
                    }
                    if (pessoaEnderecoCollection[0].Bairro != null)
                    {
                        bairr = pessoaEnderecoCollection[0].Bairro;
                    }
                    if (pessoaEnderecoCollection[0].Cidade != null)
                    {
                        cidad = pessoaEnderecoCollection[0].Cidade;
                    }
                    if (pessoaEnderecoCollection[0].Uf != null)
                    {
                        uf = pessoaEnderecoCollection[0].Uf;
                    }
                    if (pessoaEnderecoCollection[0].Cep != null)
                    {
                        cep = pessoaEnderecoCollection[0].Cep;
                    }
                }

                apendParam("LOGRAD", noLog, 30, espaco, parameterNode, xmlDocument);

                apendParam("NUMLEC", nuLog, 5, espaco, parameterNode, xmlDocument);

                apendParam("COMPL", cpLog, 15, espaco, parameterNode, xmlDocument);

                apendParam("BAIRRO", bairr, 15, espaco, parameterNode, xmlDocument);

                apendParam("CIDADE", cidad, 20, espaco, parameterNode, xmlDocument);

                apendParam("ESTADO", uf, 2, espaco, parameterNode, xmlDocument);

                apendParam("CEP8", cep, 8, espaco, parameterNode, xmlDocument);

                #endregion
            }

            #endregion

            string percPar = "0";

            apendParam("PERCPAR1", percPar, 3, zero, parameterNode, xmlDocument);

            apendParam("NMCONTA1", "", 30, espaco, parameterNode, xmlDocument);

            apendParam("DDD1CUST", "", 4, espaco, parameterNode, xmlDocument);

            apendParam("TEL1CUST", "", 10, espaco, parameterNode, xmlDocument);

            apendParam("RML1CUST", "", 6, espaco, parameterNode, xmlDocument);

            apendParam("EML1CUST", "", 80, espaco, parameterNode, xmlDocument);

            apendParam("NMCONTA2", "", 30, espaco, parameterNode, xmlDocument);

            apendParam("DDD2CUST", "", 4, espaco, parameterNode, xmlDocument);

            apendParam("TEL2CUST", "", 10, espaco, parameterNode, xmlDocument);

            apendParam("RML2CUST", "", 6, espaco, parameterNode, xmlDocument);

            apendParam("EML1CUST", "", 80, espaco, parameterNode, xmlDocument);

            apendParam("IDOPEMAC", "", 6, zero, parameterNode, xmlDocument);

            return xmlDocument;
        }

        public string EnviaCac_XML(int idCotista)
        {
            return "";//TIRAR
            XmlDocument xmlDocument = this.RetornaCac_Xml(idCotista);
            ItauMsg retornoItau = this.EnviaXMLItau(xmlDocument, "xmlcac.jsp", null);

            string msgRetorno = "";
            string codCotista = "";
            string msgQuery = "";
            foreach (Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno = param.value;
                }
                if (param.id.ToUpper() == "CODCOTISTA")
                {
                    codCotista = param.value;
                }
                if (param.id.ToUpper() == "MSGQUERY")
                {
                    msgQuery += param.value;
                }
            }

            //Logar
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("Envio XmlCac para o cotista {0}. Msg retorno: {1} - Cód. Cotista Itaú: {2} - Msg Query: {3}", idCotista, msgRetorno, codCotista, msgQuery),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.IntegracaoItau);

            return codCotista;
        }

        public string EnviaCci_XML(int idPessoa, Financial.Investidor.ContaCorrente contaCorrente, string codMov)
        {
            XmlDocument xmlDocument = this.RetornaCci_XML(idPessoa, contaCorrente, codMov);
            ItauMsg retornoItau = this.EnviaXMLItau(xmlDocument, "xmlcci.jsp", null);

            string msgRetorno = "";
            foreach (Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno = param.value;
                }
            }

            //Logar
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("Envio XmlCci para a pessoa {0}. Msg retorno: {1} - Id Conta: {2}", idPessoa, msgRetorno, contaCorrente.IdConta.Value),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.IntegracaoItau);

            return msgRetorno;
        }

        public string EnviaCco_XML(int idPessoa, string codMov)
        {
            XmlDocument xmlDocument = this.RetornaCco_XML(idPessoa, codMov);
            ItauMsg retornoItau = this.EnviaXMLItau(xmlDocument, "xmlcco.jsp", null);

            string msgRetorno = "";
            foreach (Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno = param.value;
                }
            }

            //Logar
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("Envio XmlCco para a pessoa {0}. Msg retorno: {1}", idPessoa, msgRetorno),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.IntegracaoItau);

            return msgRetorno;
        }


        public string EnviaEop_XML(OrdemCotista ordemCotista)
        {
            XmlDocument xmlDocument = this.RetornaEop_Xml(ordemCotista);

            string pattern = "(campo\\d+)\\s+"; //transformar id="campo 1     " em id="campo1"
            string postData = "strXML=<strXML>" + System.Text.RegularExpressions.Regex.Replace(xmlDocument.OuterXml, pattern, "$1") + "</strXML>";

            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                           DateTime.Now,
                                           String.Format("Envio XmlMva para a ordem cotista {0}. XmlEnviado: {1}",
                                            ordemCotista.IdOperacao.Value, postData),
                                           HttpContext.Current.User.Identity.Name,
                                           "",
                                           "",
                                           HistoricoLogOrigem.IntegracaoItau);

            //Thread lock
            //System.Threading.Thread.Sleep(60000);
            //throw new Exception("asdasd");

            //return "OPERACAO+EFETUADA+MODO+DEBUG+SEM+ENVIO";
            //return "SALDO+INSUFICIENTE_MODO_DEBUG";

            ItauMsg retornoItau = this.EnviaXMLItau(xmlDocument, "xmlmva.jsp", postData);

            string msgRetorno = "";
            string codCotista = "";
            foreach (Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno += (param.value + "|");
                }
            }

            //Logar
            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("Envio XmlMva para a ordem cotista {0}. Msg retorno: {1}", ordemCotista.IdOperacao.Value, msgRetorno),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.IntegracaoItau);

            return msgRetorno;
        }

        private ItauMsg EnviaXMLItau(XmlDocument xmlDocument, string paginaJSP, string customPostData)
        {
            string urlSiteItau = "https://www.itaucustodia.com.br/PassivoWebServices/" + paginaJSP;

            string postData;
            if (String.IsNullOrEmpty(customPostData))
            {
                postData = "strXML=" + xmlDocument.OuterXml;
            }
            else
            {
                postData = customPostData;
            }

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlSiteItau);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postData.Length;

            string proxyInfo = ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo))
            {
                //Utilizar padrao de proxy definido no Windows
                webRequest.Proxy = System.Net.WebProxy.GetDefaultProxy();
                webRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default")
                {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    webRequest.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1)
                    {
                        //Foram passadas tambem as credenciais de rede
                        webRequest.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }



            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(postData);
            }

            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

            string responseData = string.Empty;

            using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
            {
                responseData = responseReader.ReadToEnd();
            }

            ItauMsg retornoItau = null;
            XmlSerializer serializer = new XmlSerializer(typeof(ItauMsg));

            using (StringReader reader = new StringReader(responseData))
            {
                retornoItau = (ItauMsg)serializer.Deserialize(reader);
            }

            return retornoItau;
        }


        [XmlRoot(ElementName = "itaumsg")]
        public class ItauMsg
        {
            public List<Param> parameter = new List<Param>();
            public ItauMsg()
            {

            }
        }

        public enum ParamTypes
        {
            number,
            text
        }

        [XmlType(TypeName = "param")]
        public class Param
        {
            [XmlAttribute]
            public string id;

            [XmlAttribute]
            public string value;
            public Param()
            {
            }
            public Param(string idParam, string valueParam, ParamTypes paramType, int paramLength)
            {
                this.id = idParam.PadRight(11, ' ');

                string formattedValue = "";
                if (valueParam.Length > paramLength)
                {
                    //Nao eh necessario preencher com fillers, apenas capar no tamanho passado
                    formattedValue = valueParam.Substring(0, paramLength);
                }
                else
                {
                    //Precisamos preencher com 0s ou brancos
                    if (paramType == ParamTypes.number)
                    {
                        formattedValue = valueParam.PadLeft(paramLength, '0');
                    }
                    else if (paramType == ParamTypes.text)
                    {
                        formattedValue = valueParam.PadRight(paramLength, ' ');
                    }
                    else
                    {
                        throw new Exception("Tipo de parâmetro não suportado");
                    }
                }

                this.value = formattedValue;
            }
        }

    }

}
