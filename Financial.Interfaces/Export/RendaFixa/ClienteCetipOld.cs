/*using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Export.RendaFixa
{
    [Serializable()]
    public class ClienteCetipViewModel
    {
        public HeaderClienteCetip header;
        public List<RegistroClienteCetip> registros;
        public FooterClienteCetip footer;

        public ClienteCetipViewModel()
        {
            this.header = new HeaderClienteCetip();
            this.footer = new FooterClienteCetip();
            this.registros = new List<RegistroClienteCetip>();
        }
    }

    [FixedLengthRecord()]
    public class HeaderClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;
        [FieldFixedLength(1)]
        public int IdTipoLinha;
        [FieldFixedLength(20)]
        public string EntidadeGerouArquivo;
        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Data;
        [FieldFixedLength(449)]
        public string Filler;
    }

    [FixedLengthRecord()]
    public class RegistroClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;
        
        [FieldFixedLength(1)]
        public string Acao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int TitularContaCliente;

        [FieldFixedLength(1)]
        public string TipoPessoa;

        [FieldFixedLength(14)]
        public string CPFCNPJ;
        
        [FieldFixedLength(100)]
        public string Nome;

        [FieldFixedLength(1)]
        public string TipoEndereco;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int CEP;

        [FieldFixedLength(40)]
        public string Bairro;

        [FieldFixedLength(40)]
        public string Endereco;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(20)]
        public int Numero;

        [FieldFixedLength(40)]
        public string Complemento;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(6)]
        public int CodigoMunicipio;

        [FieldFixedLength(2)]
        public string UF;

        [FieldFixedLength(40)]
        public string NomeMunicipio;

        [FieldFixedLength(40)]
        public string Pais;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(5)]
        public int DDDTelefone;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(10)]
        public int NumeroTelefone;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(1)]
        public int TipoEmail;

        [FieldFixedLength(100)]
        public string Email;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(9)]
        public int ContaClienteSELIC;
    }

    [FixedLengthRecord()]
    public class FooterClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;
        [FieldFixedLength(1)]
        public int IdTipoLinha;
        [FieldFixedLength(10)]
        public int QuantidadeRegistros;
        [FieldFixedLength(476)]
        public string Filler;
    }

}
*/