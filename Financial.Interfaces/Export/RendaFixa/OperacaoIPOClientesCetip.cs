using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Export.RendaFixa
{
    [Serializable()]
    public class OperacaoIPOClientesCetipViewModel
    {
        public HeaderOperacaoIPOClientesCetip header;
        public List<RegistroOperacaoIPOClientesCetip> registros;

        public OperacaoIPOClientesCetipViewModel()
        {
            this.header = new HeaderOperacaoIPOClientesCetip();
            this.registros = new List<RegistroOperacaoIPOClientesCetip>();
        }
    }

    [FixedLengthRecord()]
    public class HeaderOperacaoIPOClientesCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldFixedLength(4)]
        public string CodigoAcao;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Data;

    }

    [FixedLengthRecord()]
    public class RegistroOperacaoIPOClientesCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldFixedLength(4)]
        public string CodigoAcao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(16)]
        public long CodigoOperacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(12)]
        public decimal QuantidadeOperacaoInteiro;

        [FieldAlign(AlignMode.Left, '0')]
        [FieldFixedLength(8)]
        public decimal QuantidadeOperacaoDecimal;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int ContaCliente;

        [FieldFixedLength(2)]
        public string TipoComitente;

        [FieldFixedLength(14)]
        public string CpfcnpjCliente;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int DestinoColocacao;

        [FieldFixedLength(16)]
        public string CodigoAgendamento;

    }


}

