using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Export.RendaFixa
{
    [Serializable()]
    public class ClienteCetipViewModel
    {
        public HeaderClienteCetip header;
        public List<RegistroClienteCetip> registros;
        
        public ClienteCetipViewModel()
        {
            this.header = new HeaderClienteCetip();
            this.registros = new List<RegistroClienteCetip>();
        }
    }

    public class RegistroClienteCetip
    {
        public DadosIdentificacaoClienteCetip dadosIdentificacao;
        public DadosBasicosClienteCetip dadosBasicos;
        public EnderecoClienteCetip endereco;
        public DetalhesPessoaFisicaClienteCetip detalhesPF;
        public DetalhesPessoaJuridicaClienteCetip detalhesPJ;

        public RegistroClienteCetip()
        {
        }

        public RegistroClienteCetip(byte tipoPessoa)
        {
            this.dadosIdentificacao = new DadosIdentificacaoClienteCetip();
            this.dadosBasicos = new DadosBasicosClienteCetip();
            this.endereco = new EnderecoClienteCetip();
            if (tipoPessoa == 1)
            {
                this.detalhesPF = new DetalhesPessoaFisicaClienteCetip();
            }
            else
            {
                this.detalhesPJ = new DetalhesPessoaJuridicaClienteCetip();
            }
        }
    }

    [FixedLengthRecord()]
    public class HeaderClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldFixedLength(20)]
        public string EntidadeGerouArquivo;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Data;
    }

    [FixedLengthRecord()]
    public class DadosIdentificacaoClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int TitularContaCliente;

        [FieldFixedLength(1)]
        public string TipoPessoa;

        [FieldFixedLength(14)]
        public string CPFCNPJ;

        [FieldFixedLength(9)]
        public string ContaDeposito;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(9)]
        public int ContaClienteSELIC;

        [FieldFixedLength(1)]
        public string NaturezaFiscal;

        [FieldFixedLength(2)]
        public string Acao;

        [FieldFixedLength(1)]
        public string ConfirmacaoInstituicao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int QuantidadeLinhas;



    }

    [FixedLengthRecord()]
    public class DadosBasicosClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;
                
        [FieldFixedLength(100)]
        public string Nome;

        [FieldFixedLength(1)]
        public string InvestidorEstrangeiro;

        [FieldFixedLength(3)]
        public string Nacionalidade;

    }

    [FixedLengthRecord()]
    public class EnderecoClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldFixedLength(1)]
        public string TipoEndereco;

        [FieldFixedLength(40)]
        public string Endereco;

        [FieldFixedLength(20)]
        public string Numero;

        [FieldFixedLength(40)]
        public string Complemento;

        [FieldFixedLength(40)]
        public string Bairro;

        [FieldFixedLength(8)]
        public string CEP;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(6)]
        public int CodigoMunicipio;

        [FieldFixedLength(40)]
        public string NomeMunicipio;

        [FieldFixedLength(2)]
        public string UF;

        [FieldFixedLength(3)]
        public string Pais;

        [FieldFixedLength(1)]
        public string TipoTelefone;

        [FieldFixedLength(2)]
        public string FILLER;

        [FieldFixedLength(3)]
        public string DDDTelefone;

        [FieldFixedLength(10)]
        public string NumeroTelefone;

        [FieldFixedLength(4)]
        public string Ramal;

        [FieldFixedLength(1)]
        public string TipoEmail;

        [FieldFixedLength(100)]
        public string Email;

    }

    [FixedLengthRecord()]
    public class DetalhesPessoaFisicaClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime DataNascimento;

        [FieldFixedLength(1)]
        public string Sexo;

        [FieldFixedLength(100)]
        public string FiliacaoPai;

        [FieldFixedLength(100)]
        public string FiliacaoMae;

        [FieldFixedLength(6)]
        public string NaturalidadeMunicipio;

        [FieldFixedLength(2)]
        public string NaturalidadeUF;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int EstadoCivil;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int TipoDocumentoIdentificacao;

        [FieldFixedLength(20)]
        public string NumeroDocumentoIdentificacao;

        [FieldFixedLength(10)]
        public string OrgaoExpedidor;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime DataExpedicao;

        [FieldFixedLength(4)]
        public string OcupacaoProfissionalPrincipal;

        [FieldFixedLength(1)]
        public string PessoaPoliticamenteExposta;

    }

    [FixedLengthRecord()]
    public class DetalhesPessoaJuridicaClienteCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime DataConstituicao;

        [FieldFixedLength(9)]
        public string AtividadeEconomica;

        [FieldFixedLength(20)]
        public string GrupoEconomico;
    }

}
