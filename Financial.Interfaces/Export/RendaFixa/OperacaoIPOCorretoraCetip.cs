using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Export.RendaFixa
{
    [Serializable()]
    public class OperacaoIPOCorretoraCetipViewModel
    {
        public HeaderOperacaoIPOCorretoraCetip header;
        public List<RegistroOperacaoIPOCorretoraCetip> registros;

        public OperacaoIPOCorretoraCetipViewModel()
        {
            this.header = new HeaderOperacaoIPOCorretoraCetip();
            this.registros = new List<RegistroOperacaoIPOCorretoraCetip>();
        }
    }

    [FixedLengthRecord()]
    public class HeaderOperacaoIPOCorretoraCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldFixedLength(4)]
        public string CodigoTipoOperacao;

        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Data;
    }

    [FixedLengthRecord()]
    public class RegistroOperacaoIPOCorretoraCetip
    {
        [FieldFixedLength(5)]
        public string IdSistema;

        [FieldFixedLength(1)]
        public int IdTipoLinha;

        [FieldFixedLength(4)]
        public string CodigoTipoOperacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(10)]
        public int MeuNumero;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int PapelParticipante;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int TipoAtivo;

        [FieldFixedLength(11)]
        public string CodigoAtivo;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int Parte;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int ContraParte;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(12)]
        public decimal QuantidadeOperacaoInteiro;

        [FieldAlign(AlignMode.Left, '0')]
        [FieldFixedLength(8)]
        public decimal QuantidadeOperacaoDecimal;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(10)]
        public decimal PUOperacaoInteiro;

        [FieldAlign(AlignMode.Left, '0')]
        [FieldFixedLength(8)]
        public decimal PUOperacaoDecimal;

        [FieldFixedLength(3)]
        public string TaxaDistribuidorInteiro;

        [FieldFixedLength(5)]
        public string TaxaDistribuidorDecimal;

        [FieldAlign(AlignMode.Left, '0')]
        [FieldFixedLength(2)]
        public decimal ModalidadeLiquidacao;

        [FieldAlign(AlignMode.Left, '0')]
        [FieldFixedLength(8)]
        public decimal BancoLiquidante;

    }


}

