﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Export.RendaFixa
{
    [Serializable()]
    public class OperacaoCetipViewModel
    {
        public HeaderOperacaoCetip header;
        public List<RegistroOperacaoCetip> registros;

        public OperacaoCetipViewModel()
        {
            this.header = new HeaderOperacaoCetip();
            this.registros = new List<RegistroOperacaoCetip>();
        }
    }

    [FixedLengthRecord()]
    public class HeaderOperacaoCetip
    {
        [FieldFixedLength(5)]
        public string TipoIF;
        [FieldFixedLength(1)]
        public string TipoRegistro;
        [FieldFixedLength(4)]
        public string CodigoOperacao;
        [FieldFixedLength(20)]
        public string NomeSimplificadoParticipante;
        [FieldFixedLength(8)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Data;
        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(5)]
        public int VersaoLayout;
        [FieldFixedLength(1)]
        public string Delimitador;
    }

    [FixedLengthRecord()]
    public class RegistroOperacaoCetip
    {
        [FieldFixedLength(5)]
        public string TipoIF;

        [FieldFixedLength(1)]
        public string TipoRegistro;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(4)]
        public int CodigoOperacao;

        [FieldFixedLength(14)]
        public string CodigoIF;
        
        [FieldFixedLength(1)]
        public string IFComRestricao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(2)]
        public int TipoCompraVenda;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int ContaParte;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(10)]
        public int MeuNumero;

        //Tratar como string pois pode ser NULL !!! Não podemos enviar zero se não for preenchido... Dá erro no FileHelper se tentamos serializar um int nullable
        [FieldFixedLength(16)]
        public string NumeroOperacaoCetip;

        //Tratar como string pois pode ser NULL !!! Não podemos enviar zero se não for preenchido... Dá erro no FileHelper se tentamos serializar um int nullable
        [FieldFixedLength(6)]
        public string NumeroAssociacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public int ContaContraparte;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(14)]
        public decimal QuantidadeOperacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(15)]
        public decimal ValorOperacao;
        
        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(18)]
        public decimal PrecoUnitarioOperacao;

        [FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(1)]
        public int CodigoModalidadeLiquidacao;

        //[FieldAlign(AlignMode.Right, '0')]
        [FieldFixedLength(8)]
        public string ContaBancoLiquidante;
        
        [FieldFixedLength(61)]
        public string Filler1;

        [FieldFixedLength(18)]
        public string CpfcnpjCliente;

        [FieldFixedLength(2)]
        public string NaturezaEmitente;

        [FieldFixedLength(249)]
        public string Filler2;

        [FieldFixedLength(8)]
        public string DataLiquidacao;//String pois eh opcional e se for nullable ele preenche com o Min date

        [FieldFixedLength(1)]
        public string CienciaLiquidacaoAntecipada;

        [FieldFixedLength(1)]
        public string Delimitador;

    }
}
