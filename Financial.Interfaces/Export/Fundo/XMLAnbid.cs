﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;

namespace Financial.Interfaces.Export.Fundo
{
    [Serializable()]
    public class XMLAnbidViewModel
    {
        public ArquivoPosicao arquivoposicao_4_01;
    }

    public class ArquivoPosicao
    {
        public Fundo fundo;
        public Carteira carteira;
    }

    public class Carteira
    {
        public Header header;
        public List<TitPublico> titpublico;
        public Caixa caixa;
        public Despesas despesas;
        public List<OutrasDespesas> outrasdespesas;
        public List<Provisao> provisao;
    }

    public class Fundo
    {
        public Header header;
        public List<TitPublico> titpublico;
        public Caixa caixa;
        public Despesas despesas;
        public List<OutrasDespesas> outrasdespesas;
        public List<Provisao> provisao;
    }

    public class Header
    {
        public string isin;
        public string cnpj;
        public string nome;
        public string dtposicao;
        public string nomeadm;
        public string cnpjadm;
        public string nomegestor;
        public string cnpjgestor;
        public string nomecustodiante;
        public string cnpjcustodiante;
        public decimal valorcota;
        public decimal quantidade;
        public decimal patliq;
        public decimal valorativos;
        public decimal valorreceber;
        public decimal valorpagar;
        public decimal vlcotasemitir;
        public decimal vlcotasresgatar;
        public string codanbid;
        public int tipofundo;
        public string nivelrsc;
    }

    public class TitPublico
    {
        public string isin;
        public string codativo;
        public string cusip;
        public string dtemissao;
        public string dtoperacao;
        public string dtvencimento;
        public decimal qtdisponivel;
        public decimal qtgarantia;
        public decimal depgar;
        public decimal pucompra;
        public decimal puvencimento;
        public decimal puposicao;
        public decimal puemissao;
        public decimal principal;
        public decimal tributos;
        public decimal valorfindisp;
        public decimal valorfinemgar;
        public decimal coupom;
        public string indexador;
        public decimal percindex;
        public string caracteristica;
        public decimal percprovcred;
        public Compromisso compromisso;
        public string classeoperacao;
        public int idinternoativo;
        public string nivelrsc;

    }

    public class Compromisso
    {
        public string dtretorno;
        public decimal puretorno;
        public string indexadorcomp;
        public decimal perindexcomp;
        public decimal txoperacao;
        public string classecomp;
    }

    public class Swap
    {
        public string cetipbmf;
        public string isin;
        public string dtoperacao;
        public string dtregistro;
        public string dtvencimento;
        public string cnpjcontraparte;
        public int garantia;
        public decimal vlnotional;
        public decimal tributos;
        public decimal vlmercadoativo;
        public decimal taxaativo;
        public string indexadorativo;
        public decimal percindexativo;
        public decimal vlmercadopassivo;
        public decimal taxapassivo;
        public string indexadorpassivo;
        public decimal percindexpassivo;
        public string hedge;
        public int tphedge;
        public string idinternoativo;
    }

    public class Caixa
    {
        public string isininstituicao;
        public string tpconta;
        public decimal saldo;
        public string nivelrsc;
    }

    public class Despesas
    {
        public decimal txadm;
        public decimal tributos;
        public decimal perctaxaadm;
        public string txperf;
        public decimal vltxperf;
        public decimal perctxperf;
        public decimal percindex;
        public decimal outtax;
        public string indexador;
    }

    public class OutrasDespesas
    {
        public int coddesp;
        public decimal valor;
    }

    public class Provisao
    {
        public int codprov;
        public string credeb;
        public string dt;
        public decimal valor;
    }

    public class XMLAnbid
    {
        public XMLAnbidViewModel XMLAnbidViewModel;

        public void ExportaArquivoXML()
        {
            // Create a new XmlSerializer instance with the type of the test class
            XmlSerializer SerializerObj = new XmlSerializer(typeof(XMLAnbidViewModel));

            // Create a new file stream to write the serialized object to a file
            TextWriter WriteFileStream = new StreamWriter(@"C:\temp\test.xml");
            SerializerObj.Serialize(WriteFileStream, this.XMLAnbidViewModel);

            // Cleanup
            WriteFileStream.Close();

        }

        public string RetornaIndice(short idIndice)
        {
            //INDICES NAO MAPEADOS
            //IEE	IEE
            //IGP	Índice de preços
            //IMA	Índice de Mercado Andima Geral
            //IMA-B5	Índice de Mercado Andima NTN-B até 5 anos
            //IMA-B5+	Índice de Mercado Andima NTN-B mais de 5 anos
            //IMA-C5	Índice de Mercado Andima NTN-C até 5 anos
            //IMA-C5+	Índice de Mercado Andima NTN-C mais de 5 anos
            //IPC	Índice de Preços ao Consumidor (IPC/FIPE)
            //ITE	ITEL
            //PIB	Cota de PIBB
            //SB1	Carteira de ações
            //SB2	Carteira de ações
            //TBF	Taxa Básica Financeira
            //

            switch (idIndice)
            {
                case ListaIndiceFixo.ANBID:
                    return "ANB";
                case ListaIndiceFixo.CDI:
                    return "DI1";
                case ListaIndiceFixo.PTAX_800VENDA:
                    return "DOL";
                case ListaIndiceFixo.IBRX50_FECHA:
                    return "I50";
                case ListaIndiceFixo.IPCA:
                    return "IAP";
                case ListaIndiceFixo.IBRX_FECHA:
                    return "IBX";
                case ListaIndiceFixo.IGPDI:
                    return "IGD";
                case ListaIndiceFixo.IGPM:
                    return "IGM";
                case ListaIndiceFixo.IMA_B:
                    return "IMA-B";
                case ListaIndiceFixo.IMA_C:
                    return "IMA-C";
                case ListaIndiceFixo.IMA_S:
                    return "IMA-S";
                case ListaIndiceFixo.IBOVESPA_FECHA:
                    return "IND";
                case ListaIndiceFixo.INPC:
                    return "INP";
                case ListaIndiceFixo.IRFM:
                    return "IRF";
                case ListaIndiceFixo.YENE:
                    return "JPY";
                case ListaIndiceFixo.OUROBMF_FECHA:
                    return "OZ1";
                case ListaIndiceFixo.EURO:
                    return "REU";
                case ListaIndiceFixo.SELIC:
                    return "SEL";
                case ListaIndiceFixo.TJLP:
                    return "TJL";
                case ListaIndiceFixo.TR:
                    return "TR";
                default:
                    throw new Exception("IdIndice não suportado: " + idIndice);
            }

        }
    }
}
