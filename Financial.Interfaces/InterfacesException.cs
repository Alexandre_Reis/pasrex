﻿using System;

namespace Financial.Interfaces.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de Bolsa
    /// </summary>
    public class InterfacesException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de TORNEGD_DataVencimentoTermoNaoEncontrada
    /// </summary>
    public class TORNEGD_DataVencimentoTermoNaoEncontrada : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TORNEGD_DataVencimentoTermoNaoEncontrada() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TORNEGD_DataVencimentoTermoNaoEncontrada(string mensagem) : base(mensagem) { }
    }

}


