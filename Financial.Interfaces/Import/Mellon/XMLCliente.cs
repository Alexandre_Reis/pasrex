﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using Financial.CRM;
using Financial.Investidor;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.BMF.Enums;
using Financial.Tributo;
using Financial.Captacao;
using Financial.Captacao.Enums;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Bolsa.Enums;

namespace Financial.Interfaces.Import.Mellon {

    public class ExportacaoClientes
    {
        [XmlElement(ElementName = "Titularidades")]
        public Titularidades titularidades;
    }
    
    public class Titularidades
    {
         [XmlElement(ElementName = "Titularidade")]
         public List<Titularidade> titularidade = new List<Titularidade>();
    }

    public class Titularidade
    {
         private string classificacaoTributaria;
        [XmlElement(ElementName = "classificacaoTributaria")]
        public string ClassificacaoTributaria
         {
             get { return classificacaoTributaria; }
             set { classificacaoTributaria = value; }
         }

        private string tipoTitularidade;
        [XmlElement(ElementName = "tipoTitularidade")]
        public string TipoTitularidade
         {
             get { return tipoTitularidade; }
             set { tipoTitularidade = value; }
         }

        private string transmitirOrdemVerbal;
        [XmlElement(ElementName = "transmitirOrdemVerbal")]
        public string TransmitirOrdemVerbal
         {
             get { return transmitirOrdemVerbal; }
             set { transmitirOrdemVerbal = value; }
         }

        private string transmitirOrdemEscritaFaxEmail;
        [XmlElement(ElementName = "transmitirOrdemEscritaFaxEmail")]
        public string TransmitirOrdemEscritaFaxEmail
         {
             get { return transmitirOrdemEscritaFaxEmail; }
             set { transmitirOrdemEscritaFaxEmail = value; }
         }

        private string dataInicio;
        [XmlElement(ElementName = "dataInicio")]
        public string DataInicio
         {
             get { return dataInicio; }
             set { dataInicio = value; }
         }

        private string dataFim;
        [XmlElement(ElementName = "dataFim")]
        public string DataFim
         {
             get { return dataFim; }
             set { dataFim = value; }
         }

        private string faixaPatrimonial;
        [XmlElement(ElementName = "faixaPatrimonial")]
        public string FaixaPatrimonial
         {
             get { return faixaPatrimonial; }
             set { faixaPatrimonial = value; }
         }

        private string faixaRemuneracao;
        [XmlElement(ElementName = "faixaRemuneracao")]
        public string FaixaRemuneracao
         {
             get { return faixaRemuneracao; }
             set { faixaRemuneracao = value; }
         }

        [XmlElement(ElementName = "Titular")]
        public Titular titular;

        [XmlElement(ElementName = "ContasExternas")]
        public ContasExternas contasExternas;
        
        [XmlElement(ElementName = "Conta")]
        public List<Conta> conta;
    }

    public class Titular
    {
        private string idPessoa;
        [XmlElement(ElementName = "idPessoa")]
        public string IdPessoa
        {
            get { return idPessoa; }
            set { idPessoa = value; }
        }

        private string nome;
        [XmlElement(ElementName = "nome")]
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string apelido;
        [XmlElement(ElementName = "apelido")]
        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        private string tipoPessoa;
        [XmlElement(ElementName = "tipoPessoa")]
        public string TipoPessoa
        {
            get { return tipoPessoa; }
            set { tipoPessoa = value; }
        }

        private string nomeAbreviado;
        [XmlElement(ElementName = "nomeAbreviado")]
        public string NomeAbreviado
        {
            get { return nomeAbreviado; }
            set { nomeAbreviado = value; }
        }

        private string cpf;
        [XmlElement(ElementName = "CPF")]
        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        
        private string cnpjEmpresa;
        [XmlElement(ElementName = "cnpjEmpresa")]
        public string CnpjEmpresa
        {
            get { return cnpjEmpresa; }
            set { cnpjEmpresa = value; }
        }

        [XmlElement(ElementName = "Atividade")]
        public List<Atividade> atividade;

        private string inscricaoEstadual;
        [XmlElement(ElementName = "inscricaoEstadual")]
        public string InscricaoEstadual
        {
            get { return inscricaoEstadual; }
            set { inscricaoEstadual = value; }
        }

        private string capital;
        [XmlElement(ElementName = "capital")]
        public string Capital
        {
            get { return capital; }
            set { capital = value; }
        }

        private string formaConstituicao;
        [XmlElement(ElementName = "formaConstituicao")]
        public string FormaConstituicao
        {
            get { return formaConstituicao; }
            set { formaConstituicao = value; }
        }

        private string dataConstituicao;
        [XmlElement(ElementName = "dataConstituicao")]
        public string DataConstituicao
        {
            get { return dataConstituicao; }
            set { dataConstituicao = value; }
        }

        private string naturezaJuridica;
        [XmlElement(ElementName = "naturezaJuridica")]
        public string NaturezaJuridica
        {
            get { return naturezaJuridica; }
            set { naturezaJuridica = value; }
        }

        private string porteEmpresa;
        [XmlElement(ElementName = "porteEmpresa")]
        public string PorteEmpresa
        {
            get { return porteEmpresa; }
            set { porteEmpresa = value; }
        }

        private string paisAcionario;
        [XmlElement(ElementName = "paisAcionario")]
        public string PaisAcionario
        {
            get { return paisAcionario; }
            set { paisAcionario = value; }
        }

        private string logo;
        [XmlElement(ElementName = "logo")]
        public string Logo
        {
            get { return logo; }
            set { logo = value; }
        }

        private string webSite;
        [XmlElement(ElementName = "webSite")]
        public string WebSite
        {
            get { return webSite; }
            set { webSite = value; }
        }

        private string paisSede;
        [XmlElement(ElementName = "PaisSede")]
        public string PaisSede
        {
            get { return paisSede; }
            set { paisSede = value; }
        }

        private string eInstituicaoFinanceira;
        [XmlElement(ElementName = "eInstituicaoFinanceira")]
        public string EInstituicaoFinanceira
        {
            get { return eInstituicaoFinanceira; }
            set { eInstituicaoFinanceira = value; }
        }

        private string sexo;
        [XmlElement(ElementName = "sexo")]
        public string Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        private string tipoRegistro;
        [XmlElement(ElementName = "tipoRegistro")]
        public string TipoRegistro
        {
            get { return tipoRegistro; }
            set { tipoRegistro = value; }
        }

        private string estadoCivil;
        [XmlElement(ElementName = "estadoCivil")]
        public string EstadoCivil
        {
            get { return estadoCivil; }
            set { estadoCivil = value; }
        }

        private string regimeCasamento;
        [XmlElement(ElementName = "regimeCasamento")]
        public string RegimeCasamento
        {
            get { return regimeCasamento; }
            set { regimeCasamento = value; }
        }

        private string paisNacionalidade;
        [XmlElement(ElementName = "paisNacionalidade")]
        public string PaisNacionalidade
        {
            get { return paisNacionalidade; }
            set { paisNacionalidade = value; }
        }

        private string paisDomicilioPF;
        [XmlElement(ElementName = "paisDomicilioPF")]
        public string PaisDomicilioPF
        {
            get { return paisDomicilioPF; }
            set { paisDomicilioPF = value; }
        }

        private string naturalidade;
        [XmlElement(ElementName = "naturalidade")]
        public string Naturalidade
        {
            get { return naturalidade; }
            set { naturalidade = value; }
        }

        private string dataNascimento;
        [XmlElement(ElementName = "dataNascimento")]
        public string DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        private string filiacaoPai;
        [XmlElement(ElementName = "filiacaoPai")]
        public string FiliacaoPai
        {
            get { return filiacaoPai; }
            set { filiacaoPai = value; }
        }

        private string filiacaoMae;
        [XmlElement(ElementName = "filiacaoMae")]
        public string FiliacaoMae
        {
            get { return filiacaoMae; }
            set { filiacaoMae = value; }
        }

        private string numeroRG;
        [XmlElement(ElementName = "numeroRG")]
        public string NumeroRG
        {
            get { return numeroRG; }
            set { numeroRG = value; }
        }

        private string emissorRG;
        [XmlElement(ElementName = "emissorRG")]
        public string EmissorRG
        {
            get { return emissorRG; }
            set { emissorRG = value; }
        }

        private string dataEmissaoRG;
        [XmlElement(ElementName = "dataEmissaoRG")]
        public string DataEmissaoRG
        {
            get { return dataEmissaoRG; }
            set { dataEmissaoRG = value; }
        }

        private string ufRg;
        [XmlElement(ElementName = "ufrg")]
        public string UfRg
        {
            get { return ufRg; }
            set { ufRg = value; }
        }

        private string ocupacao;
        [XmlElement(ElementName = "ocupacao")]
        public string Ocupacao
        {
            get { return ocupacao; }
            set { ocupacao = value; }
        }

        private string profissao;
        [XmlElement(ElementName = "profissao")]
        public string Profissao
        {
            get { return profissao; }
            set { profissao = value; }
        }

        [XmlElement(ElementName = "Emails")]
        public Emails emails;
        
        [XmlElement(ElementName = "Telefones")]
        public Telefones telefones;

        [XmlElement(ElementName = "Enderecos")]
        public Enderecos enderecos;
    }

    public class ContasExternas
    {
        [XmlElement(ElementName = "contaExterna")]
        public List<ContaExterna> contaExterna = new List<ContaExterna>();
    }

    public class ContaExterna
    {
        private string tipoContaExterna;
        [XmlElement(ElementName = "tipoContaExterna")]
        public string TipoContaExterna
        {
            get { return tipoContaExterna; }
            set { tipoContaExterna = value; }
        }

        private string eTributada;
        [XmlElement(ElementName = "eTributada")]
        public string ETributada
        {
            get { return eTributada; }
            set { eTributada = value; }
        }

        private string numeroBanco;
        [XmlElement(ElementName = "numeroBanco")]
        public string NumeroBanco
        {
            get { return numeroBanco; }
            set { numeroBanco = value; }
        }

        private string nomeBanco;
        [XmlElement(ElementName = "nomeBanco")]
        public string NomeBanco
        {
            get { return nomeBanco; }
            set { nomeBanco = value; }
        }

        private string numeroAgencia;
        [XmlElement(ElementName = "numeroAgencia")]
        public string NumeroAgencia
        {
            get { return numeroAgencia; }
            set { numeroAgencia = value; }
        }

        private string numero;
        [XmlElement(ElementName = "numero")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string dvContaExterna;
        [XmlElement(ElementName = "dvContaExterna")]
        public string DvContaExterna
        {
            get { return dvContaExterna; }
            set { dvContaExterna = value; }
        }

        private string ativa;
        [XmlElement(ElementName = "ativa")]
        public string Ativa
        {
            get { return ativa; }
            set { ativa = value; }
        }

        private string dataAprovacao;
        [XmlElement(ElementName = "dataAprovacao")]
        public string DataAprovacao
        {
            get { return dataAprovacao; }
            set { dataAprovacao = value; }
        }
    }

    public class Conta
    {

        private string idConta;
        [XmlElement(ElementName = "idConta")]
        public string IdConta
        {
            get { return idConta; }
            set { idConta = value; }
        }

        private string nomeCotista;
        [XmlElement(ElementName = "NomeCotista")]
        public string NomeCotista
        {
            get { return nomeCotista; }
            set { nomeCotista = value; }
        }

        private string ativa;
        [XmlElement(ElementName = "ativa")]
        public string Ativa
        {
            get { return ativa; }
            set { ativa = value; }
        }

        private string classificacaoCVM;
        [XmlElement(ElementName = "classificacaoCVM")]
        public string ClassificacaoCVM
        {
            get { return classificacaoCVM; }
            set { classificacaoCVM = value; }
        }

        private string idOperador;
        [XmlElement(ElementName = "idOperador")]
        public string IdOperador
        {
            get { return idOperador; }
            set { idOperador = value; }
        }

        private string cdOperador;
        [XmlElement(ElementName = "cdOperador")]
        public string CdOperador
        {
            get { return cdOperador; }
            set { cdOperador = value; }
        }

        private string nomeOperador;
        [XmlElement(ElementName = "nomeOperador")]
        public string NomeOperador
        {
            get { return nomeOperador; }
            set { nomeOperador = value; }
        }

        private string idDistribuidor;
        [XmlElement(ElementName = "idDistribuidor")]
        public string IdDistribuidor
        {
            get { return idDistribuidor; }
            set { idDistribuidor = value; }
        }

        private string cdDistribuidor;
        [XmlElement(ElementName = "cdDistribuidor")]
        public string CdDistribuidor
        {
            get { return cdDistribuidor; }
            set { cdDistribuidor = value; }
        }

        private string nomeDistribuidor;
        [XmlElement(ElementName = "nomeDistribuidor")]
        public string NomeDistribuidor
        {
            get { return nomeDistribuidor; }
            set { nomeDistribuidor = value; }
        }

        private string eCorrespondenciaExtrato;
        [XmlElement(ElementName = "eCorrespondenciaExtrato")]
        public string ECorrespondenciaExtrato
        {
            get { return eCorrespondenciaExtrato; }
            set { eCorrespondenciaExtrato = value; }
        }

        private string eCorrespondenciaInforme;
        [XmlElement(ElementName = "eCorrespondenciaInforme")]
        public string ECorrespondenciaInforme
        {
            get { return eCorrespondenciaInforme; }
            set { eCorrespondenciaInforme = value; }
        }

        private string eBloqueado;
        [XmlElement(ElementName = "eBloqueado")]
        public string EBloqueado
        {
            get { return eBloqueado; }
            set { eBloqueado = value; }
        }

        private string eCorrespondenciaMovimentacao;
        [XmlElement(ElementName = "eCorrespondenciaMovimentacao")]
        public string ECorrespondenciaMovimentacao
        {
            get { return eCorrespondenciaMovimentacao; }
            set { eCorrespondenciaMovimentacao = value; }
        }

        private string dataInicio;
        [XmlElement(ElementName = "dataInicio")]
        public string DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        [XmlElement(ElementName = "EnderecoConta")]
        public List<EnderecoConta> enderecoConta;   
    }

    public class Atividade
    {
        private string ocupacao;
        [XmlElement(ElementName = "ocupacao")]
        public string Ocupacao
        {
            get { return ocupacao; }
            set { ocupacao = value; }
        }
    }
    
    public class Emails
    {
        [XmlElement(ElementName = "email")]
        public List<string> email = new List<string>();
    }

    public class Telefones
    {
        [XmlElement(ElementName = "telefone")]
        public List<Telefone> telefone = new List<Telefone>();
    }

    public class Telefone
    {
        private string ddi;
        [XmlElement(ElementName = "ddi")]
        public string Ddi
        {
            get { return ddi; }
            set { ddi = value; }
        }

        private string ddd;
        [XmlElement(ElementName = "ddd")]
        public string Ddd
        {
            get { return ddd; }
            set { ddd = value; }
        }

        private string numero;
        [XmlElement(ElementName = "numero")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string ramal;
        [XmlElement(ElementName = "ramal")]
        public string Ramal
        {
            get { return ramal; }
            set { ramal = value; }
        }

        private string tipoTelefone;
        [XmlElement(ElementName = "tipoTelefone")]
        public string TipoTelefone
        {
            get { return tipoTelefone; }
            set { tipoTelefone = value; }
        }
	}


    public class Enderecos
    {
        [XmlElement(ElementName = "endereco")]
        public List<Endereco> endereco = new List<Endereco>();
    }

    public class Endereco
    {
        private string tipoEndereco;
        [XmlElement(ElementName = "tipoEndereco")]
        public string TipoEndereco
        {
            get { return tipoEndereco; }
            set { tipoEndereco = value; }
        }


        private string logradouro;
        [XmlElement(ElementName = "logradouro")]
        public string Logradouro
        {
            get { return logradouro; }
            set { logradouro = value; }
        }

        private string numero;
        [XmlElement(ElementName = "numero")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string complemento;
        [XmlElement(ElementName = "complemento")]
        public string Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }

        private string bairro;
        [XmlElement(ElementName = "bairro")]
        public string Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }

        private string uf;
        [XmlElement(ElementName = "uf")]
        public string Uf
        {
            get { return uf; }
            set { uf = value; }
        }

        private string cep;
        [XmlElement(ElementName = "cep")]
        public string Cep
        {
            get { return cep; }
            set { cep = value; }
        }

        private string caixaPostal;
        [XmlElement(ElementName = "caixaPostal")]
        public string CaixaPostal
        {
            get { return caixaPostal; }
            set { caixaPostal = value; }
        }

        private string cidade;
        [XmlElement(ElementName = "cidade")]
        public string Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private string pais;
        [XmlElement(ElementName = "pais")]
        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string dataAtualizacao;
        [XmlElement(ElementName = "dataAtualizacao")]
        public string DataAtualizacao
        {
            get { return dataAtualizacao; }
            set { dataAtualizacao = value; }
        }

        private string dataInclusao;
        [XmlElement(ElementName = "dataInclusao")]
        public string DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }

        private string pessoaResponsavel;
        [XmlElement(ElementName = "pessoaResponsavel")]
        public string PessoaResponsavel
        {
            get { return pessoaResponsavel; }
            set { pessoaResponsavel = value; }
        }
    }

    public class EnderecoConta
    {

        private string tipoEndereco;
        [XmlElement(ElementName = "tipoEndereco")]
        public string TipoEndereco
        {
            get { return tipoEndereco; }
            set { tipoEndereco = value; }
        }

        private string logradouro;
        [XmlElement(ElementName = "logradouro")]
        public string Logradouro
        {
            get { return logradouro; }
            set { logradouro = value; }
        }

        private string numero;
        [XmlElement(ElementName = "numero")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string complemento;
        [XmlElement(ElementName = "complemento")]
        public string Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }

        private string bairro;
        [XmlElement(ElementName = "bairro")]
        public string Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }

        private string uf;
        [XmlElement(ElementName = "uf")]
        public string Uf
        {
            get { return uf; }
            set { uf = value; }
        }

        private string cep;
        [XmlElement(ElementName = "cep")]
        public string Cep
        {
            get { return cep; }
            set { cep = value; }
        }


        private string caixaPostal;
        [XmlElement(ElementName = "caixaPostal")]
        public string CaixaPostal
        {
            get { return caixaPostal; }
            set { caixaPostal = value; }
        }

        private string cidade;
        [XmlElement(ElementName = "cidade")]
        public string Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        private string pais;
        [XmlElement(ElementName = "pais")]
        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string dataAtualizacao;
        [XmlElement(ElementName = "dataAtualizacao")]
        public string DataAtualizacao
        {
            get { return dataAtualizacao; }
            set { dataAtualizacao = value; }
        }

        private string dataInclusao;
        [XmlElement(ElementName = "dataInclusao")]
        public string DataInclusao
        {
            get { return dataInclusao; }
            set { dataInclusao = value; }
        }

        private string pessoaResponsavel;
        [XmlElement(ElementName = "pessoaResponsavel")]
        public string PessoaResponsavel
        {
            get { return pessoaResponsavel; }
            set { pessoaResponsavel = value; }
        }
    }

    

    

    public class XMLCliente
    {
        public ExportacaoClientes XMLClienteViewModel;

        public void ImportaArquivoXML(TextReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ExportacaoClientes));

            ExportacaoClientes xmlClientes = (ExportacaoClientes)serializer.Deserialize(reader);

            List<string> clientesAtualizados = new List<string>();

            int i = 0;

            while (i < xmlClientes.titularidades.titularidade.Count)
            {
                #region Preenche Pessoa
                Pessoa pessoa = new Pessoa();
                if (pessoa.LoadByPrimaryKey(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa)))
                {
                    clientesAtualizados.Add(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                }

                pessoa.Nome = xmlClientes.titularidades.titularidade[i].titular.Nome;
                pessoa.Apelido = xmlClientes.titularidades.titularidade[i].titular.Apelido;
                pessoa.IdPessoa = Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                pessoa.DataCadatro = Convert.ToDateTime(xmlClientes.titularidades.titularidade[i].DataInicio);

                if (xmlClientes.titularidades.titularidade[i].ClassificacaoTributaria == "PESSOA FISICA")
                {
                    pessoa.Tipo = (byte)TipoPessoa.Fisica;
                    pessoa.Cpfcnpj = xmlClientes.titularidades.titularidade[i].titular.Cpf;
                    switch (xmlClientes.titularidades.titularidade[i].titular.EstadoCivil)
                    {
                        case "Casado":
                            pessoa.EstadoCivil = (byte?)EstadoCivilPessoa.Casado;
                            break;
                        case "Solteiro":
                            pessoa.EstadoCivil = (byte?)EstadoCivilPessoa.Solteiro;
                            break;
                        case "Viúvo":
                            pessoa.EstadoCivil = (byte?)EstadoCivilPessoa.Viuvo;
                            break;
                        case "Divorciado":
                            pessoa.EstadoCivil = (byte?)EstadoCivilPessoa.Divorciado;
                            break;
                        case "Outro":
                            pessoa.EstadoCivil = (byte?)EstadoCivilPessoa.Outros;
                            break;
                    }
                    pessoa.Sexo = xmlClientes.titularidades.titularidade[i].titular.Sexo;

                    PessoaDocumento pessoaDocumento = new PessoaDocumento();
                    pessoaDocumento.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaDocumento.DataExpedicao = Convert.ToDateTime(xmlClientes.titularidades.titularidade[i].titular.DataEmissaoRG);
                    pessoaDocumento.NumeroDocumento = xmlClientes.titularidades.titularidade[i].titular.NumeroRG;
                    pessoaDocumento.OrgaoEmissor = xmlClientes.titularidades.titularidade[i].titular.EmissorRG;
                    pessoaDocumento.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;
                    pessoa.DataNascimento = Convert.ToDateTime(xmlClientes.titularidades.titularidade[i].titular.DataNascimento);
                    pessoa.FiliacaoNomeMae = xmlClientes.titularidades.titularidade[i].titular.FiliacaoMae;
                    pessoa.FiliacaoNomePai = xmlClientes.titularidades.titularidade[i].titular.FiliacaoPai;
                    string profissao = xmlClientes.titularidades.titularidade[i].titular.Profissao.Substring(xmlClientes.titularidades.titularidade[i].titular.Profissao.IndexOf("-") + 1);
                    if (profissao.Length > 50)
                    {
                        profissao = profissao.Substring(0, 50);
                    }
                    pessoa.Profissao = profissao;
                }
                else
                {
                    pessoa.Tipo = (byte?)TipoPessoa.Juridica;
                    pessoa.Cpfcnpj = xmlClientes.titularidades.titularidade[i].titular.CnpjEmpresa;
                }

                pessoa.DataUltimaAlteracao = DateTime.Now;

                pessoa.Save();
                #endregion

                #region Preenche Telefones
                int j = 0;

                while (j < xmlClientes.titularidades.titularidade[i].titular.telefones.telefone.Count)
                {
                    PessoaTelefone pessoaTelefone = new PessoaTelefone();
                    PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
                    pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa)),
                                                        pessoaTelefoneCollection.Query.Numero.Equal(xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Numero));
                    pessoaTelefoneCollection.Query.Load();
                    if (pessoaTelefoneCollection.Count > 0)
                    {
                        pessoaTelefone = pessoaTelefoneCollection[0];
                    }
                    else
                    {
                        pessoaTelefoneCollection.QueryReset();
                        pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    }

                    pessoaTelefone.IdPessoa = Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                    if (xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ddd != null)
                    {
                        pessoaTelefone.Ddd = xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ddd;
                    }
                    if (xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ddi != null)
                    {
                        pessoaTelefone.Ddi = xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ddi;
                    }
                    if (xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Numero != null)
                    {
                        pessoaTelefone.Numero = xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Numero;
                    }
                    if (xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ramal != null)
                    {
                        pessoaTelefone.Ramal = xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].Ramal;
                    }
                    switch (xmlClientes.titularidades.titularidade[i].titular.telefones.telefone[j].TipoTelefone)
                    {
                        case "Residencial":
                            pessoaTelefone.TipoTelefone = (byte?)TipoTelefonePessoa.Residencial;
                            break;
                        case "Celular":
                            pessoaTelefone.TipoTelefone = (byte?)TipoTelefonePessoa.Celular;
                            break;
                        case "Comercial":
                            pessoaTelefone.TipoTelefone = (byte?)TipoTelefonePessoa.Comercial;
                            break;
                        default:
                            pessoaTelefone.TipoTelefone = (byte?)TipoTelefonePessoa.Outros;
                            break;
                    }
                    pessoaTelefoneCollection.Save();
                    j++;
                }
                #endregion

                #region Preenche Endereços
                j = 0;

                while (j < xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco.Count)
                {
                    PessoaEndereco pessoaEndereco = new PessoaEndereco();
                    PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                    pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa)),
                                                        pessoaEnderecoCollection.Query.Endereco.Equal(xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Logradouro),
                                                        pessoaEnderecoCollection.Query.Numero.Equal(xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Numero),
                                                        pessoaEnderecoCollection.Query.Complemento.Equal(xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Complemento));
                    pessoaEnderecoCollection.Query.Load();
                    if (pessoaEnderecoCollection.Count > 0)
                    {
                        pessoaEndereco = pessoaEnderecoCollection[0];
                    }
                    else
                    {
                        pessoaEnderecoCollection.QueryReset();
                        pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    }

                    pessoaEndereco.IdPessoa = Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                    pessoaEndereco.Endereco = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Logradouro;
                    pessoaEndereco.Numero = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Numero;
                    pessoaEndereco.Complemento = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Complemento;
                    pessoaEndereco.Bairro = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Bairro;
                    pessoaEndereco.Cidade = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Cidade;
                    pessoaEndereco.Cep = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Cep;
                    pessoaEndereco.Uf = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Uf;
                    pessoaEndereco.Pais = xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].Pais;
                    switch (xmlClientes.titularidades.titularidade[i].titular.enderecos.endereco[j].TipoEndereco)
                    {
                        case "Comercial":
                            pessoaEndereco.TipoEndereco = TipoEnderecoPessoa.Comercial;
                            break;
                        case "Residencial":
                            pessoaEndereco.TipoEndereco = TipoEnderecoPessoa.Residencial;
                            break;
                        default:
                            pessoaEndereco.TipoEndereco = TipoEnderecoPessoa.Residencial;
                            break;
                    }
                    pessoaEndereco.RecebeCorrespondencia = RecebeCorrespondenciaEnderecoPessoa.Sim;

                    pessoaEnderecoCollection.Save();
                    j++;
                }
                #endregion

                #region Preenche Emails
                j = 0;

                while (j < xmlClientes.titularidades.titularidade[i].titular.emails.email.Count)
                {
                    if (xmlClientes.titularidades.titularidade[i].titular.emails.email[j] != null)
                    {
                        PessoaEmail pessoaEmail = new PessoaEmail();
                        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
                        pessoaEmailCollection.Query.Where(pessoaEmailCollection.Query.IdPessoa.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa)),
                                                            pessoaEmailCollection.Query.Email.Equal(xmlClientes.titularidades.titularidade[i].titular.emails.email[j]));
                        pessoaEmailCollection.Query.Load();
                        if (pessoaEmailCollection.Count > 0)
                        {
                            pessoaEmail = pessoaEmailCollection[0];
                        }
                        else
                        {
                            pessoaEmailCollection.QueryReset();
                            pessoaEmail = pessoaEmailCollection.AddNew();
                        }

                        pessoaEmail.IdPessoa = Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                        pessoaEmail.Email = xmlClientes.titularidades.titularidade[i].titular.emails.email[j];

                        pessoaEmailCollection.Save();
                    }
                    j++;
                }
                #endregion

                #region Preenche Banco Agencia e Conta
                j = 0;

                while (j < xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna.Count)
                {
                    int idbanco = 0;
                    BancoCollection bancoCollection = new BancoCollection();
                    bancoCollection.Query.Select(bancoCollection.Query.IdBanco);
                    bancoCollection.Query.Where(bancoCollection.Query.CodigoCompensacao.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NumeroBanco)));
                    bancoCollection.Query.es.Distinct = true;
                    bancoCollection.Query.Load();
                    if (bancoCollection.Count > 0)
                    {
                        idbanco = (int)bancoCollection[0].IdBanco;
                    }
                    else
                    {
                        Banco banco = new Banco();
                        banco.Nome = xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NomeBanco;
                        banco.CodigoCompensacao = xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NumeroBanco;
                        banco.Save();
                        idbanco = (int)banco.IdBanco;
                    }
                    int idagencia = 0;
                    AgenciaCollection agenciaCollection = new AgenciaCollection();
                    agenciaCollection.Query.Select(agenciaCollection.Query.IdAgencia);
                    agenciaCollection.Query.Where(agenciaCollection.Query.Codigo.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NumeroAgencia)));
                    agenciaCollection.Query.Where(agenciaCollection.Query.IdBanco.Equal(idbanco));
                    agenciaCollection.Query.es.Distinct = true;
                    agenciaCollection.Query.Load();
                    if (agenciaCollection.Count > 0)
                    {
                        idagencia = (int)agenciaCollection[0].IdAgencia;
                    }
                    else
                    {
                        Agencia agencia = new Agencia();
                        agencia.IdBanco = idbanco;
                        agencia.Codigo = xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NumeroAgencia;
                        agencia.Nome = xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].NumeroAgencia;
                        agencia.Save();
                        idagencia = (int)agencia.IdAgencia;
                    }
                    Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                    ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                    contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa)),
                                                        contaCorrenteCollection.Query.IdBanco.Equal(idbanco),
                                                        contaCorrenteCollection.Query.IdAgencia.Equal(idagencia),
                                                        contaCorrenteCollection.Query.Numero.Equal(xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].Numero));
                    contaCorrenteCollection.Query.Load();
                    if (contaCorrenteCollection.Count > 0)
                    {
                        contaCorrente = contaCorrenteCollection[0];
                    }
                    else
                    {
                        contaCorrenteCollection.QueryReset();
                        contaCorrente = contaCorrenteCollection.AddNew();
                    }

                    contaCorrente.IdPessoa = Convert.ToInt32(xmlClientes.titularidades.titularidade[i].titular.IdPessoa);
                    contaCorrente.IdBanco = idbanco;
                    contaCorrente.IdAgencia = idagencia;
                    contaCorrente.Numero = xmlClientes.titularidades.titularidade[i].contasExternas.contaExterna[j].Numero;
                    contaCorrente.IdMoeda = (int?)ListaMoedaFixo.Real;
                    contaCorrente.ContaDefault = "N";


                    TipoConta tipoConta = new TipoConta();
                    tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                    tipoConta.Query.Load();

                    if (!tipoConta.IdTipoConta.HasValue)
                    {
                        tipoConta = new TipoConta();
                        tipoConta.Descricao = "Conta Depósito";
                        tipoConta.Save();
                    }
                    contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

                    contaCorrenteCollection.Save();
                    j++;
                }
                #endregion

                #region Preenche dados de Cliente
                Cliente cliente = new Cliente();
                ClienteCollection clienteCollection = new ClienteCollection();

                clienteCollection.Query.Where(clienteCollection.Query.IdCliente.Equal(pessoa.IdPessoa));
                clienteCollection.Query.Load();
                if (clienteCollection.Count > 0)
                {
                    cliente = clienteCollection[0];
                }
                else
                {
                    clienteCollection.QueryReset();
                    cliente = clienteCollection.AddNew();
                }
                                
                cliente.IdCliente = pessoa.IdPessoa;
                cliente.Nome = pessoa.Nome;
                cliente.Apelido = pessoa.Apelido;

                cliente.ApuraGanhoRV = "N";
                cliente.CalculaContabil = "N";
                cliente.CalculaGerencial = "N";
                cliente.CalculaRealTime = "N";
                if (pessoa.DataCadatro.HasValue)
                {
                    cliente.DataImplantacao = pessoa.DataCadatro.Value;
                }
                else
                {
                    cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }

                cliente.DataDia = cliente.DataImplantacao.Value;
                cliente.DataInicio = cliente.DataImplantacao.Value;

                GrupoProcessamentoCollection grupoProcessamentoCollection = new GrupoProcessamentoCollection();
                grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.Descricao.Like("%Carteira%"));
                grupoProcessamentoCollection.Query.Load();

                if (grupoProcessamentoCollection.Count == 0)
                {
                    grupoProcessamentoCollection.QueryReset();
                    grupoProcessamentoCollection.LoadAll();
                }

                cliente.IdGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
                cliente.IdMoeda = (byte)ListaMoedaFixo.Real;

                if (pessoa.Tipo == (byte)TipoPessoa.Fisica)
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaFisica;
                }
                else
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaJuridica;
                }

                cliente.IsentoIOF = "N";// = valoresExcel.IsentoIOF.ToUpper();
                cliente.IsentoIR = "N";// = valoresExcel.IsentoIR.ToUpper(); 

                cliente.IsProcessando = "N";
                cliente.Status = (byte)StatusCliente.Aberto;
                cliente.StatusAtivo = (byte)StatusAtivoPessoa.Sim;
                cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                cliente.TipoControle = (byte)TipoControleCliente.Completo;
                cliente.ZeraCaixa = "N";
                cliente.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
                cliente.GrossUP = (byte)GrossupCliente.NaoFaz;

                clienteCollection.Save();
                #endregion

                #region Cadastra ClienteBolsa

                ClienteBolsa clienteBolsa = new ClienteBolsa();
                ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();

                clienteBolsaCollection.Query.Where(clienteBolsaCollection.Query.IdCliente.Equal(pessoa.IdPessoa));
                clienteBolsaCollection.Query.Load();
                if (clienteBolsaCollection.Count > 0)
                {
                    clienteBolsa = clienteBolsaCollection[0];
                }
                else
                {
                    clienteBolsaCollection.QueryReset();
                    clienteBolsa = clienteBolsaCollection.AddNew();
                }

                clienteBolsa.CodigoSinacor = cliente.IdCliente.Value.ToString();
                clienteBolsa.IdCliente = cliente.IdCliente.Value;
                clienteBolsa.IsentoIR = "N";
                clienteBolsa.TipoCotacao = (byte?)TipoCotacaoBolsa.Fechamento;
                clienteBolsa.TipoCusto = 1;

                clienteBolsaCollection.Save();
                #endregion

                #region Cadastra ClienteBMF

                ClienteBMF clienteBMF = new ClienteBMF();
                ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();

                clienteBMFCollection.Query.Where(clienteBMFCollection.Query.IdCliente.Equal(pessoa.IdPessoa));
                clienteBMFCollection.Query.Load();
                if (clienteBMFCollection.Count > 0)
                {
                    clienteBMF = clienteBMFCollection[0];
                }
                else
                {
                    clienteBMFCollection.QueryReset();
                    clienteBMF = clienteBMFCollection.AddNew();
                }

                clienteBMF.CodigoSinacor = cliente.IdCliente.Value.ToString();
                clienteBMF.IdCliente = cliente.IdCliente.Value;
                clienteBMF.InvestidorInstitucional = "N";
                clienteBMF.TipoCotacao = (byte?)TipoCotacaoBMF.Fechamento;
                clienteBMF.Socio = "N";
                clienteBMF.TipoPlataforma = (byte)TipoPlataformaOperacional.Normal;

                clienteBMFCollection.Save();
                #endregion

                #region Cadastra ClienteRendaFixa

                ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
                ClienteRendaFixaCollection clienteRendaFixaCollection = new ClienteRendaFixaCollection();

                clienteRendaFixaCollection.Query.Where(clienteRendaFixaCollection.Query.IdCliente.Equal(pessoa.IdPessoa));
                clienteRendaFixaCollection.Query.Load();
                if (clienteRendaFixaCollection.Count > 0)
                {
                    clienteRendaFixa = clienteRendaFixaCollection[0];
                }
                else
                {
                    clienteRendaFixaCollection.QueryReset();
                    clienteRendaFixa = clienteRendaFixaCollection.AddNew();
                }

                clienteRendaFixa.IdCliente = cliente.IdCliente.Value;
                clienteRendaFixa.IsentoIR = "N";
                clienteRendaFixa.IsentoIOF = "N";
                clienteRendaFixa.CodigoInterface = cliente.IdCliente.Value.ToString();
                clienteRendaFixa.UsaCustoMedio = "N";

                clienteRendaFixaCollection.Save();
                #endregion

                #region tabPage - ClienteInterface
                ClienteInterface clienteInterface = new ClienteInterface();
                ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();

                clienteInterfaceCollection.Query.Where(clienteInterfaceCollection.Query.IdCliente.Equal(pessoa.IdPessoa));
                clienteInterfaceCollection.Query.Load();
                if (clienteInterfaceCollection.Count > 0)
                {
                    clienteInterface = clienteInterfaceCollection[0];
                }
                else
                {
                    clienteInterfaceCollection.QueryReset();
                    clienteInterface = clienteInterfaceCollection.AddNew();
                }

                clienteInterface.IdCliente = pessoa.IdPessoa;

                clienteInterfaceCollection.Save();
                #endregion

                #region Cadastra Carteira
                Carteira carteira = new Carteira();
                CarteiraCollection carteiraCollection = new CarteiraCollection();

                carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(pessoa.IdPessoa));
                carteiraCollection.Query.Load();
                if (carteiraCollection.Count > 0)
                {
                    carteira = carteiraCollection[0];
                }
                else
                {
                    carteiraCollection.QueryReset();
                    carteira = carteiraCollection.AddNew();
                }

                carteira.IdCarteira = pessoa.IdPessoa;
                carteira.Nome = pessoa.Nome;
                carteira.Apelido = pessoa.Apelido;
                carteira.CalculaEnquadra = "N";
                carteira.CalculaIOF = "N";
                carteira.CasasDecimaisCota = 8;
                carteira.CasasDecimaisQuantidade = 8;
                carteira.CobraTaxaFiscalizacaoCVM = "N";
                carteira.ContagemDiasConversaoResgate = (byte)ContagemDiasLiquidacaoResgate.DiasUteis;
                carteira.CotaInicial = 1;
                carteira.TipoAniversario = 0;
                carteira.DataInicioCota = cliente.DataDia.Value;
                carteira.DiasCotizacaoAplicacao = 0;
                carteira.DiasCotizacaoResgate = 0;
                carteira.DiasLiquidacaoAplicacao = 0;
                carteira.DiasLiquidacaoResgate = 0;
                carteira.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;

                AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.Nome.Like("%Padrão%"),
                                                    agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                    agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
                agenteMercadoCollection.Query.Load();

                if (agenteMercadoCollection.Count == 0)
                {
                    agenteMercadoCollection.QueryReset();
                    agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                        agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
                    agenteMercadoCollection.Query.Load();
                }

                carteira.IdAgenteAdministrador = agenteMercadoCollection[0].IdAgente.Value;
                carteira.IdAgenteGestor = agenteMercadoCollection[0].IdAgente.Value;

                CategoriaFundoCollection categoriaFundoCollection = new CategoriaFundoCollection();
                categoriaFundoCollection.Query.Where(categoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));
                categoriaFundoCollection.Query.Load();

                if (categoriaFundoCollection.Count == 0)
                {
                    categoriaFundoCollection.QueryReset();
                    categoriaFundoCollection.LoadAll();
                }

                carteira.IdCategoria = categoriaFundoCollection[0].IdCategoria.Value;

                SubCategoriaFundoCollection subCategoriaFundoCollection = new SubCategoriaFundoCollection();
                subCategoriaFundoCollection.Query.Where(subCategoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));
                subCategoriaFundoCollection.Query.Load();

                if (subCategoriaFundoCollection.Count == 0)
                {
                    subCategoriaFundoCollection.QueryReset();
                    subCategoriaFundoCollection.LoadAll();
                }
                carteira.IdSubCategoria = subCategoriaFundoCollection[0].IdSubCategoria.Value;

                IndiceCollection indiceCollection = new IndiceCollection();
                indiceCollection.Query.Where(indiceCollection.Query.Descricao.Like("%CDI%"));
                indiceCollection.Query.Load();

                if (indiceCollection.Count == 0)
                {
                    indiceCollection.QueryReset();
                    indiceCollection.LoadAll();
                }
                carteira.IdIndiceBenchmark = indiceCollection[0].IdIndice.Value;
                carteira.ProjecaoIRResgate = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;
                carteira.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
                carteira.TipoCarteira = (byte)TipoCarteiraFundo.RendaVariavel;
                carteira.TipoCota = (byte)TipoCotaFundo.Fechamento;
                carteira.TipoCusto = (byte)TipoCustoFundo.Aplicacao;
                carteira.TipoRentabilidade = (byte)TipoRentabilidadeFundo.FinalFinal;
                carteira.TipoTributacao = (byte)TipoTributacaoFundo.Acoes;
                carteira.TipoVisaoFundo = (byte)TipoVisaoFundoRebate.NaoTrata;
                carteira.TruncaCota = "N";
                carteira.TruncaQuantidade = "N";
                carteira.TruncaFinanceiro = "N";
                carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Direta;
                carteira.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;
                carteira.CompensacaoPrejuizo = (byte)TipoCompensacaoPrejuizo.Padrao;
                carteira.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;

                carteiraCollection.Save();
                #endregion

                #region cria permissão para todos os usuários (internos) do sistema
                PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();

                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);

                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCliente permissaoCliente = new PermissaoCliente();

                    permissaoClienteCollection.QueryReset();
                    permissaoClienteCollection.Query.Where(permissaoClienteCollection.Query.IdCliente.Equal(cliente.IdCliente),
                                                            permissaoClienteCollection.Query.IdUsuario.Equal(idUsuario));
                    permissaoClienteCollection.Query.Load();
                    if (permissaoClienteCollection.Count > 0)
                    {
                        permissaoCliente = permissaoClienteCollection[0];
                    }
                    else
                    {
                        permissaoClienteCollection.QueryReset();
                        permissaoCliente = permissaoClienteCollection.AddNew();
                    }

                    permissaoCliente.IdCliente = cliente.IdCliente;
                    permissaoCliente.IdUsuario = idUsuario;
                    permissaoClienteCollection.Save();
                }
                #endregion

                i++;
            }
            
            reader.Close();

            //mensagem em caso de haver atualização dos dados
            if (clientesAtualizados.Count > 0)
            {
                string message = "Clientes Atualizados: ";
                foreach (string cliente in clientesAtualizados)
                {
                    message += cliente + ", ";
                }
                message = message.Substring(0, message.Length - 2);
                
                throw new Exception(message);
            }
        }
    }
}
