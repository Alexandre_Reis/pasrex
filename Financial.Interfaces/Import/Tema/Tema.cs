﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Tema.Enums;
using Financial.Common;

namespace Financial.Interfaces.Import.Tema {
    public class Tema {

        public Tema() { }

        #region Estrutura dos arquivos
        [FixedLengthRecord()]
        [IgnoreEmptyLines()]
        public class Saldos {
            [FieldFixedLength(10)]
            public string codCliente;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataSaldo;

            [FieldFixedLength(3)]
            public string mercado; /* VIS,TER,OPC */

            [FieldFixedLength(6)]
            public string codAcao;

            [FieldConverter(ConverterKind.Decimal, ".")]
            [FieldFixedLength(16)]
            public decimal qtdAcao;

            [FieldConverter(ConverterKind.Decimal, ".")]
            [FieldFixedLength(16)]
            public decimal valCusAcao;

            [FieldFixedLength(1)]
            public TipoSaldo tipoSaldo;            
        }

        [FixedLengthRecord()]
        [IgnoreEmptyLines()]
        public class Apuracao {
            [FieldFixedLength(10)]
            [FieldTrim(TrimMode.Both)]
            public string codCliente;

            [FieldFixedLength(3)]
            public string mercado; /* VIS,TER,OPC */

            [FieldFixedLength(6)]
            [FieldTrim(TrimMode.Both)]
            public string codAcao;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataMovto;

            [FieldFixedLength(30)]
            [FieldTrim(TrimMode.Both)]
            public string operacao;

            [FieldConverter(typeof(Apuracao.TheeDecimalConverter))]
            [FieldFixedLength(15)]
            public decimal qtdAcao;

            [FieldConverter(typeof(Apuracao.FourDecimalConverter))]
            [FieldFixedLength(15)]
            public decimal precoCusto;

            [FieldConverter(typeof(Apuracao.TwoDecimalConverter))]
            [FieldFixedLength(16)]
            public decimal valCusAcao;

            [FieldConverter(typeof(Apuracao.TwoDecimalConverter))]
            [FieldFixedLength(16)]
            public decimal valBaixa;

            [FieldConverter(typeof(Apuracao.TwoDecimalConverter))]
            [FieldFixedLength(16)]
            public decimal valDayTrade;

            [FieldConverter(typeof(Apuracao.TwoDecimalConverter))]
            [FieldFixedLength(16)]
            public decimal valNormal;

            [FieldConverter(typeof(Apuracao.TwoDecimalConverter))]
            [FieldFixedLength(16)]
            public decimal valOutros;

            #region Converter 2 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //
                    return res / 100;
                }
            }
            #endregion

            #region Converter 3 Digitos
            internal class TheeDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //
                    return res / 1000;
                }
            }
            #endregion

            #region Converter 4 Digitos
            internal class FourDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //
                    return res / 10000;
                }
            }
            #endregion
        }
        
        [FixedLengthRecord(FixedMode.AllowLessChars)]
        [IgnoreEmptyLines()]
        public class Movimento {
            [FieldFixedLength(6)]
            public string codCliente;

            public string CodCliente {
                get { return codCliente; }
                set { codCliente = value; }
            }

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataMovto;

            public DateTime DataMovto {
                get { return dataMovto; }
                set { dataMovto = value; }
            }

            [FieldFixedLength(15)]
            public string codCorretora;

            public string CodCorretora {
                get { return codCorretora; }
                set { codCorretora = value; }
            }

            [FieldFixedLength(3)]
            public string codMercado; /* VIS, TER, OPC */

            public string CodMercado {
                get { return codMercado; }
                set { codMercado = value; }
            }

            [FieldFixedLength(15)]
            [FieldTrim(TrimMode.Both)]
            public string codAcao;

            public string CodAcao {
                get { return codAcao; }
                set { codAcao = value; }
            }

            [FieldFixedLength(25)]
            [FieldTrim(TrimMode.Both)]
            public string tipoMovto;

            public string TipoMovto {
                get { return tipoMovto; }
                set { tipoMovto = value; }
            }

            [FieldFixedLength(30)]
            [FieldTrim(TrimMode.Both)]
            public string descMvto;

            public string DescMvto {
                get { return descMvto; }
                set { descMvto = value; }
            }

            [FieldFixedLength(13)]
            public int quantidade;

            public int Quantidade {
                get { return quantidade; }
                set { quantidade = value; }
            }
            
            [FieldFixedLength(13)]
            [FieldConverter(typeof(Movimento.TwoDecimalConverter))]
            public decimal valor;

            public decimal Valor {
                get { return valor; }
                set { valor = value; }
            }
            
            [FieldFixedLength(13)]
            [FieldConverter(typeof(Movimento.TwoDecimalConverter))]
            public decimal valorMulta;

            public decimal ValorMulta {
                get { return valorMulta; }
                set { valorMulta = value; }
            }

            //[FieldConverter(ConverterKind.Decimal, ".")]
            [FieldFixedLength(13)]
            [FieldConverter(typeof(Movimento.TwoDecimalConverter))]
            public decimal valorJuros;

            public decimal ValorJuros {
                get { return valorJuros; }
                set { valorJuros = value; }
            }

            [FieldFixedLength(6)]
            [FieldOptional]
            public string mesAnoRef;

            public string MesAnoRef {
                get { return mesAnoRef; }
                set { mesAnoRef = value; }
            }

            [FieldIgnored()]
            public int id;

            public int Id {
                get { return id; }
                set { id = value; }
            }

            #region Converter 2 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);
                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }            
            #endregion
        }

        //public class NotasCorretagemViewModel {
        //    public HeaderNotasCorretagem header;
        //    public List<DetailNotasCorretagem> detail;

        //    public NotasCorretagemViewModel() {
        //        this.header = new HeaderNotasCorretagem();
        //        this.detail = new List<DetailNotasCorretagem>();
        //    }
        //}
        
        [IgnoreEmptyLines()]
        [FixedLengthRecord(FixedMode.AllowLessChars)]
        public class HeaderNotasCorretagem {
            [FieldFixedLength(1)]
            public string tipoRegistro;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataNota;

            [FieldFixedLength(15)]
            [FieldTrim(TrimMode.Both)]
            public string numNota;

            [FieldFixedLength(15)]
            public string codBovespa;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal taxCBLCeCLC;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal taxNegocicacao;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal taxCorretagem;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal taxTermoOpcao;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal IRRFNormal;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal IRRFDaytrade;

            [FieldFixedLength(1)]
            public string cobraISS;

            [FieldConverter(typeof(HeaderNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal valorISS;

            [FieldFixedLength(10)]
            public string codCliente;

            [FieldIgnored()]
            public int idAgenteMercado;

            #region Converter 2 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //
                    return res / 100;
                }
            }
            #endregion
        }
        
        [IgnoreEmptyLines()]
        [FixedLengthRecord(FixedMode.AllowLessChars)]
        public class DetailNotasCorretagem {
            [FieldFixedLength(1)]
            public string tipoRegistro;

            [FieldFixedLength(1)]
            public string tipoOperacao;

            [FieldFixedLength(3)]
            public string mercado;

            [FieldFixedLength(12)]
            [FieldTrim(TrimMode.Both)]
            public string codAcao;

            [FieldFixedLength(13)]
            public int quantidade;

            [FieldConverter(typeof(DetailNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal preco;

            [FieldConverter(typeof(DetailNotasCorretagem.TwoDecimalConverter))]
            [FieldFixedLength(13)]
            public decimal valor;

            [FieldFixedLength(6)]
            public string numeroNegocio;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            [FieldOptional]
            [FieldTrim(TrimMode.Both)]
            public DateTime? dtLiquidacaoTermo;

            #region Converter 2 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //
                    return res / 100;
                }
            }
            #endregion
        }

        public class DataHolderNotaCorretagem {

            // Construtor
            public DataHolderNotaCorretagem() { }
            
            /**/
            public DetailNotasCorretagem detail = new DetailNotasCorretagem();
            public HeaderNotasCorretagem header = new HeaderNotasCorretagem();
        }

        #endregion
        
        /// <summary>
        /// Armazena arquivo Saldos da Tema
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        public Saldos[] ImportaSaldosTema(StreamReader streamReader) {
            FileHelperEngine engine = new FileHelperEngine(typeof(Saldos));

            Saldos[] saldos = engine.ReadStream(streamReader) as Saldos[];

            return saldos;
        }

        /// <summary>
        /// Armazena arquivo Apuração da Tema
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        public Apuracao[] ImportaApuracaoTema(StreamReader streamReader) {
            FileHelperEngine engine = new FileHelperEngine(typeof(Apuracao));

            Apuracao[] apuracao = engine.ReadStream(streamReader) as Apuracao[];

            return apuracao;
        }

        /// <summary>
        /// Armazena arquivo Movimento da Tema
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        public Movimento[] ImportaMovimentoTema(StreamReader streamReader) {
            FileHelperEngine engine = new FileHelperEngine(typeof(Movimento));
             
            Movimento[] movimento = engine.ReadStream(streamReader) as Movimento[];

            return movimento;
        }

        /// <summary>
        /// Armazena arquivo NotaCorretagem da Tema
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns>
        /// [0] = HeaderNotasCorretagem
        /// [1] = DetailNotasCorretagem
        /// </returns>
        public object[] ImportaNotaCorretagemTema(StreamReader streamReader) {
            MultiRecordEngine engine = new MultiRecordEngine(typeof(HeaderNotasCorretagem), typeof(DetailNotasCorretagem));
            engine.RecordSelector = new RecordTypeSelector(CustomSelector);

            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue; 

            object[] notaCorretagem = null;
            //
            try {
                notaCorretagem = engine.ReadStream(streamReader) as object[];

                if (engine.ErrorManager.HasErrors) {
                    //foreach (ErrorInfo err in engine.ErrorManager.Errors) {
                    //    Console.WriteLine(err.LineNumber);
                    //    Console.WriteLine(err.RecordString);
                    //    Console.WriteLine(err.ExceptionInfo.ToString());
                    //}

                    string error = "Linha: " + engine.ErrorManager.Errors[0].LineNumber + " formato incorreto. ";
                    error += engine.ErrorManager.Errors[0].ExceptionInfo.Message;
                    //
                    throw new Exception(error);
                }
            }
            catch (Exception e) {
                throw new Exception(e.Message);
            }                        

            return notaCorretagem;
        }

        
        /// <summary>
        /// Transforma object de HeaderNotasCorretagem e DetailNotasCorretagem em DataHolderNotaCorretagem
        /// </summary>
        /// <param name="nota"></param>
        /// <returns></returns>
        public List<DataHolderNotaCorretagem> TransformaNotaCorretagem(object[] nota) {
            HeaderNotasCorretagem h = new HeaderNotasCorretagem();

            List<DataHolderNotaCorretagem> lista = new List<DataHolderNotaCorretagem>();

            for (int i = 0; i < nota.Length; i++) {

                if (nota[i] is HeaderNotasCorretagem) {                    
                    h = (HeaderNotasCorretagem)nota[i]; // Salva o Header
                    //

                    //int codigoBovespa;
                    //try {
                    //    codigoBovespa = Convert.ToInt32(h.codBovespa);
                    //}
                    //catch (Exception e) {                        
                    //    throw new Exception("Código Bovespa não Numérico");
                    //}

                    //int idAgenteMercado = new AgenteMercado().BuscaIdAgenteMercadoBovespa(codigoBovespa);

                    //// Seta o idAgenteMercado pelo codigoBovespa
                    //h.idAgenteMercado = idAgenteMercado;
                }
                else {
                    DataHolderNotaCorretagem d = new DataHolderNotaCorretagem();
                    //
                    d.detail = (DetailNotasCorretagem)nota[i];

                    d.header = h;
                    //
                    lista.Add(d);
                }
            }

            return lista;
        }


        /// <summary>
        /// Retorna Somente os Headers das Nota de Corretagem
        /// </summary>
        /// <param name="nota"></param>
        /// <returns></returns>
        public List<HeaderNotasCorretagem> GetHeaderNotaCorretagem(object[] nota) {
            HeaderNotasCorretagem h = new HeaderNotasCorretagem();

            List<HeaderNotasCorretagem> lista = new List<HeaderNotasCorretagem>();

            for (int i = 0; i < nota.Length; i++) {

                if (nota[i] is HeaderNotasCorretagem) {
                    lista.Add((HeaderNotasCorretagem)nota[i]);
                }
            }

            return lista;
        }

        /// <summary>
        /// Para a Importação de Notas de Corretagem
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="record"></param>
        /// <returns></returns>
        private Type CustomSelector(MultiRecordEngine engine, string record) {
            //if (engine.LineNumber == 1) {
            //    return typeof(HeaderNotasCorretagem);
            //}
            //else {
            //    return typeof(DetailNotasCorretagem);
            //}                                                                                

            if (record[0] == '1') {
                return typeof(HeaderNotasCorretagem);
            }
            else { // if (record[0] == '2') {
                return typeof(DetailNotasCorretagem);
            }         
        }
    }
}