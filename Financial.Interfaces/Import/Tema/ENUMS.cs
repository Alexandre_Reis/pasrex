using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.Tema.Enums {

    public enum TipoSaldo { 
        Informado = 1,
        Apurado = 2
    }

    //public static class TipoMovtoTema {
    //    public const string AjustePositivo = "Ajuste Positivo";
    //    public const string AjusteNegativo = "Ajuste Negativo";
    //    public const string TaxaCustodia = "Taxa Custodia";
    //    public const string Grupamento = "Grupamento";
    //    public const string Desdobramento = "Desdobramento";
    //    public const string Bonificacao = "Bonificacao";
    //    public const string DireitoSubscricao = "Direito de Subscricao";
    //    public const string ExercicioSubscricao = "Exercicio de Subscricao";
    //    public const string Dividendos = "Dividendos";
    //    public const string JurosCapitalProprio = "Juros s/ Capital Proprio";
    //    public const string IPO = "IPO";
    //    public const string TermoLiquidado = "Termo Liquidado";
    //    public const string PagamentoDarf = "Pagamento de Darf";        
    //}
}
