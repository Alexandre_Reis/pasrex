﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;

namespace Financial.Interfaces.Import {
    public class Emissor {

        public Emissor() { }

        #region Estrutura do arquivo
        [DelimitedRecord(",")]
        //[IgnoreLast(1)]
        [IgnoreEmptyLines()]
        public class EmissorStructure {

            [FieldQuoted()]
            public string codigo;

            [FieldQuoted()]
            public string emissor;

            [FieldQuoted()]
            public string cnpj;

            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()]
            public DateTime? dataCriacaoEmissor;            
        }
        #endregion


        /// <summary>
        /// Lê um arquivo txt e armazena numa estrutura
        /// </summary>
        /// <param name="arquivo">path completo do arquivo</param>
        /// <returns></returns>
        public EmissorStructure[] ImportaEmissor(string arquivo) {

            FileHelperEngine<EmissorStructure> engine = new FileHelperEngine<EmissorStructure>();

            EmissorStructure[] emissor = engine.ReadFile(arquivo);

            return emissor;
        }        
    }
}
