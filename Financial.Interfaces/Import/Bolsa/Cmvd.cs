﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Util;
using System.Text;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Cmvd {
   
        /// <summary>
        /// A partir de um Arquivo Cmvd Existente Gera um novo Arquivo Cmvd somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoCmvd">Caminho Completo do Arquivo Cmvd</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Cmvd será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Cmvd</param>
        /// <returns>true se novo arquivo Cmvd foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoCmvdNaoEncontradoException">Se Arquivo Cmvd de entrada não existe</exception>
        public bool GeraArquivoCmvd(string nomeArquivoCompletoCmvd, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoCmvd)) {
                throw new ArquivoCmvdNaoEncontradoException("Arquivo Cmvd não Encontrado: " + nomeArquivoCompletoCmvd);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo
            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "cmvd_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoCmvd)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se Dado Pertence a Algum dos Clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Cmvd pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaConl">linha do arquivo Cmvd</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Cmvd pertence a algum dos Clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaCmvd, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaCmvd.Substring(0, 2)) {
                case EstruturaArquivoCMVD.Header:
                case EstruturaArquivoCMVD.Trailer:
                    break;
                case EstruturaArquivoCMVD.IdentificacaoCliente:
                    int codigoCliente = Convert.ToInt32(linhaCmvd.Substring(8, 7)); // Campo 04
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
                case EstruturaArquivoCMVD.IdentificacaoMovimentacao:
                    int cdCliente = Convert.ToInt32(linhaCmvd.Substring(7, 7)); // Campo 03
                    retorno = listaIdCliente.Contains(cdCliente);
                    break;
            }

            return retorno;
        }
    }
}