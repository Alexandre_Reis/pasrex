using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class BdinCollection {
        private List<Bdin> collectionBdin;

        public List<Bdin> CollectionBdin {
            get { return collectionBdin; }
            set { collectionBdin = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>        
        public BdinCollection() {
            this.collectionBdin = new List<Bdin>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de Bdins</param>        
        public void Add(Bdin bdin) {
            collectionBdin.Add(bdin);
        }
    }
}
