using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class DesagioCollection {
        private List<Desagio> collectionDesagio;

        public List<Desagio> CollectionDesagio
        {
            get { return collectionDesagio; }
            set { collectionDesagio = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public DesagioCollection()
        {
            this.collectionDesagio = new List<Desagio>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prod">Elemento a inserir na lista de Prod</param>
        public void Add(Desagio desagio)
        {
            collectionDesagio.Add(desagio);
        }
    }
}
