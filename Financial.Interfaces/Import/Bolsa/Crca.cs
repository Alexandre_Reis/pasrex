﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Crca
    {
        // Informação sobre o tipo registro da linha do arquivo Crca
        private string tipoRegistro;

        // bool indicando se crca deve ser salvo na colecao de Crcas
        // Somente será salvo o Tipo de Registro do Crca que for necessario
        private bool crcaProcessado;

        #region Properties dos Valores do Registro00 - Header

        int? codigoAgente; // Campo 2.2 - Codigo do usuario
        DateTime? dataMovimento; // Campo 07

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }

        #endregion

        #region Properties dos Valores do Registro01 - Movimento do Dia
        Int64 cnpjInstituicao; // Campo 02

        public Int64 CnpjInstituicao
        {
            get { return cnpjInstituicao; }
            set { cnpjInstituicao = value; }
        } 

        string modalidadeOferta; // Campo 03

        public string ModalidadeOferta
        {
            get { return modalidadeOferta; }
            set { modalidadeOferta = value; }
        }

        Int64 cpfCnpj; // Campo 04

        public Int64 CpfCnpj
        {
            get { return cpfCnpj; }
            set { cpfCnpj = value; }
        }

        Int64 codigoCustodiante; // Campo 05.1

        public Int64 CodigoCustodiante
        {
            get { return codigoCustodiante; }
            set { codigoCustodiante = value; }
        }

        Int64 codigoCliente; // Campo 05.2

        public Int64 CodigoCliente
        {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        Int64 quantidadeEfetiva; // Campo 07

        public Int64 QuantidadeEfetiva
        {
            get { return quantidadeEfetiva; }
            set { quantidadeEfetiva = value; }
        }

        decimal valorEfetivo; // Campo 08

        public decimal ValorEfetivo
        {
            get { return valorEfetivo; }
            set { valorEfetivo = value; }
        }
        
        string codigoIsin; // Campo 10

        public string CodigoIsin
        {
            get { return codigoIsin; }
            set { codigoIsin = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Crca devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.crcaProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            //
        }

        /// <summary>
        ///  Indica se Crca possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return crcaProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <exception cref="ArquivoCrcaNaoEncontradoException">Se Arquivo Crca não foi encontrado</exception>
        /// <exception cref="ProcessaCrcaException">Se ocorreu erro no Processamento</exception>
        public CrcaCollection ProcessaCrca(string nomeArquivoCompleto, DateTime data) {
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoCrcaNaoEncontradoException("Arquivo Crca não encontrado: " + nomeArquivoCompleto);
            }

            CrcaCollection crcaCollection = new CrcaCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataCrca(linha, data);
                            // Somente é salvo Crca que possui dados relevantes
                            Crca crca = (Crca)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                crcaCollection.Add(crca);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaCrcaException(e.Message);
            }

            return crcaCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <exception cref="ProcessaCrcaException">Se ocorreu erro no Processamento</exception>
        public CrcaCollection ProcessaCrca(StreamReader sr, DateTime data)
        {
            CrcaCollection crcaCollection = new CrcaCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataCrca(linha, data);
                        // Somente é salvo Crca que possui dados relevantes
                        Crca crca = (Crca)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            crcaCollection.Add(crca);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaCrcaException(e.Message);
            }

            return crcaCollection;
        }

        /// <summary>
        /// A partir de um Arquivo Crca Existente Gera um novo Arquivo Crca somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoCrca">Caminho Completo do Arquivo Crca</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Crca será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Crca</param>
        /// <returns>true se novo arquivo Crca foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoCrcaNaoEncontradoException">Se Arquivo Crca de entrada não existe</exception>
        public bool GeraArquivoCrca(string nomeArquivoCompletoCrca, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoCrca)) {
                throw new ArquivoCrcaNaoEncontradoException("Arquivo Crca não Encontrado: " + nomeArquivoCompletoCrca);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo

            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "crca_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoCrca)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se Dado pertence a algum dos clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Crca pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaCrca">linha do arquivo Crca</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Crca pertence a algum dos clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaCrca, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaCrca.Substring(0, 2)) {
                case EstruturaArquivoCRCA.Header:
                case EstruturaArquivoCRCA.Trailer:
                    break;
                case EstruturaArquivoCRCA.DetalheNegocioRealizado:
                    int codigoCliente = Convert.ToInt32(linhaCrca.Substring(190, 7)); // Campo 13
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoCRCA
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoCRCA");

            List<string> valoresPossiveis = EstruturaArquivoCRCA.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo Crca
            switch (tipoRegistro) {
                case EstruturaArquivoCRCA.Header:
                    this.tipoRegistro = EstruturaArquivoCRCA.Header;
                    break;
                case EstruturaArquivoCRCA.DetalheNegocioRealizado:
                    this.tipoRegistro = EstruturaArquivoCRCA.DetalheNegocioRealizado;
                    break;
                case EstruturaArquivoCRCA.Trailer:
                    this.tipoRegistro = EstruturaArquivoCRCA.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Crca de acordo com o tipo
        private void TrataCrca(string linha, DateTime data) {
            // Limpa os valores do Crca
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCRCA.Header) {
                this.codigoAgente = Convert.ToInt32(linha.Substring(6, 4)); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(31, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
                int ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoCrcaIncorretoException("Arquivo Crca com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));
                }

                // Indica que Crca deve ser salvo                
                this.crcaProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCRCA.DetalheNegocioRealizado) {
                #region MovimentoDia
                this.cnpjInstituicao = Convert.ToInt64(linha.Substring(02, 15)); // Campo 02
                this.modalidadeOferta = Convert.ToString(linha.Substring(17, 1)); // Campo 03
                this.cpfCnpj = Convert.ToInt64(linha.Substring(18, 15)); // Campo 04
                this.codigoCustodiante = Convert.ToInt64(linha.Substring(33, 5)); // Campo 05.1
                this.codigoCliente = Convert.ToInt64(linha.Substring(38, 7)); // Campo 05.2
                this.quantidadeEfetiva = Convert.ToInt64(linha.Substring(62, 18)); // Campo 07                
                this.valorEfetivo = Convert.ToDecimal(linha.Substring(80, 17)) / 100; // Campo 05
                this.codigoIsin = linha.Substring(114, 12).TrimEnd(); // Campo 08
                #endregion

                // Indica que Crca deve ser salvo
                this.crcaProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCRCA.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoDividendo</returns>
        public bool IsTipoRegistroMovimentoDia() {
            return this.tipoRegistro == EstruturaArquivoCRCA.DetalheNegocioRealizado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoCRCA.Header;
        }

    }
}
