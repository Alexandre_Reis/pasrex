using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class ProdCollection {
        private List<Prod> collectionProd;

        public List<Prod> CollectionProd {
            get { return collectionProd; }
            set { collectionProd = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ProdCollection() {
            this.collectionProd = new List<Prod>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prod">Elemento a inserir na lista de Prod</param>
        public void Add(Prod prod) {
            collectionProd.Add(prod);
        }
    }
}
