﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using System.Collections.Generic;
using Financial.Util;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Dbtc {
        // Informação sobre o tipo registro da linha do arquivo DBTC
        private string tipoRegistro;
        
        // bool indicando se dbtc deve ser salvo na colecao de Dbtcs
        // Somente será salvo o Tipo de Registro do Dbtc que for necessario
        private bool dbtcProcessado;

        #region Properties dos Valores do Registro00 - Header
        
        int? codigoAgente; // Campo 02 - Codigo do usuario
        DateTime? dataMovimento; // Campo 06
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Relação de Contratos por Usuários Doadores

        int? numeroContratoDoador; // Campo 02
        /* Para saber se o emprestimo é voluntario ou compulsorio */
        string tipoOrigemDoador; // Campo 08
        /* Pode ser diferente da data de inicio do contrato,pois ele pode estar sendo renovado */
        DateTime? dataRegistroDoador; // Campo 10
        int? codigoClienteDoador; // Campo 11
        Int64? quantidadeDoador; // Campo 16
        DateTime? dataVencimentoDoador; // Campo 17    
        decimal? puDoador; // Campo 18
        int? fatorCotacaoDoador; // Campo 19
        decimal? taxaOperacaoDoador; // Campo 21
        decimal? taxaComissaoDoador; // Campo 22
        string cdAtivoBolsaDoador; // Campo 30

        /// <summary>
        /// 
        /// </summary>
        public int? NumeroContratoDoador {
            get { return numeroContratoDoador; }
            set { numeroContratoDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TipoOrigemDoador {
            get { return tipoOrigemDoador; }
            set { tipoOrigemDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataRegistroDoador {
            get { return dataRegistroDoador; }
            set { dataRegistroDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoClienteDoador {
            get { return codigoClienteDoador; }
            set { codigoClienteDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64? QuantidadeDoador {
            get { return quantidadeDoador; }
            set { quantidadeDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataVencimentoDoador {
            get { return dataVencimentoDoador; }
            set { dataVencimentoDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? PuDoador {
            get { return puDoador; }
            set { puDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? FatorCotacaoDoador {
            get { return fatorCotacaoDoador; }
            set { fatorCotacaoDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? TaxaOperacaoDoador {
            get { return taxaOperacaoDoador; }
            set { taxaOperacaoDoador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? TaxaComissaoDoador {
            get { return taxaComissaoDoador; }
            set { taxaComissaoDoador = value; }
        }
       
        /// <summary>
        /// 
        /// </summary>
        public string CdAtivoBolsaDoador {
            get { return cdAtivoBolsaDoador; }
            set { cdAtivoBolsaDoador = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro02 - Relação de Contratos por Usuários Tomadores

        int? numeroContratoTomador; // Campo 02
        /* Para saber se o emprestimo é voluntario ou compulsorio */
        string tipoOrigemTomador; // Campo 08
        /* Pode ser diferente da data de inicio do contrato,pois ele pode estar sendo renovado */
        DateTime? dataRegistroTomador; // Campo 10 
        int? codigoClienteTomador; // Campo 11
        /* Pode ir para carteira disponivel ou qualquer outra de garantia */
        int? codigoCarteiraDestino; // Campo 13
        Int64? quantidadeTomador; // Campo 16
        DateTime? dataVencimentoTomador; // Campo 18    
        /* Preco medio do papel na abertura do contrato */
        decimal? puTomador; // Campo 19
        /* Usado para calcular o valor do emprestimo */
        int? fatorCotacaoTomador; // Campo 20
        decimal? taxaOperacaoTomador; // Campo 22
        decimal? taxaComissaoTomador; // Campo 23
        string cdAtivoBolsaTomador; // Campo 29

        /// <summary>
        /// 
        /// </summary>
        public int? NumeroContratoTomador {
            get { return numeroContratoTomador; }
            set { numeroContratoTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TipoOrigemTomador {
            get { return tipoOrigemTomador; }
            set { tipoOrigemTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataRegistroTomador {
            get { return dataRegistroTomador; }
            set { dataRegistroTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoClienteTomador {
            get { return codigoClienteTomador; }
            set { codigoClienteTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoCarteiraDestino {
            get { return codigoCarteiraDestino; }
            set { codigoCarteiraDestino = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64? QuantidadeTomador {
            get { return quantidadeTomador; }
            set { quantidadeTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataVencimentoTomador {
            get { return dataVencimentoTomador; }
            set { dataVencimentoTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? PuTomador {
            get { return puTomador; }
            set { puTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? FatorCotacaoTomador {
            get { return fatorCotacaoTomador; }
            set { fatorCotacaoTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? TaxaOperacaoTomador {
            get { return taxaOperacaoTomador; }
            set { taxaOperacaoTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? TaxaComissaoTomador {
            get { return taxaComissaoTomador; }
            set { taxaComissaoTomador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CdAtivoBolsaTomador {
            get { return cdAtivoBolsaTomador; }
            set { cdAtivoBolsaTomador = value; }
        }

        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo ConL devem ser inicializadas
        /// </summary>
        private void Initialize() {                                                                                    
            this.dbtcProcessado = false;
            /* registro00 */
            #region registro00
            this.codigoAgente = null;
            this.dataMovimento = null;
            #endregion
            /* registro01 */
            #region registro01
            this.numeroContratoDoador = null;
            this.tipoOrigemDoador = null;
            this.dataRegistroDoador = null;
            this.codigoClienteDoador = null;
            this.quantidadeDoador = null;
            this.dataVencimentoDoador = null;
            this.puDoador = null;
            this.fatorCotacaoDoador = null;
            this.taxaOperacaoDoador = null;
            this.taxaComissaoDoador = null;
            this.cdAtivoBolsaDoador = null;
            #endregion
            /* registro02 */
            #region registro02
            this.numeroContratoTomador = null;
            this.tipoOrigemTomador = null;
            this.dataRegistroTomador = null;
            this.codigoClienteTomador = null;
            this.codigoCarteiraDestino = null;
            this.quantidadeTomador = null;
            this.dataVencimentoTomador = null;
            this.puTomador = null;
            this.fatorCotacaoTomador = null;
            this.taxaOperacaoTomador = null;
            this.taxaComissaoTomador = null;
            this.cdAtivoBolsaTomador = null;
            #endregion
        }

        /// <summary>
        ///  Indica se DBTC possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return dbtcProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoDbtcNaoEncontradoException
        /// throws ArquivoDBTCIncorretoException se DataMovimento do Header do arquivo Dbtc for diferente de data
        public DbtcCollection ProcessaDbtc(string nomeArquivoCompleto, DateTime data) {            

            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoDbtcNaoEncontradoException("Arquivo DBTC "+ nomeArquivoCompleto + " não encontrado.");
            }
                        
            DbtcCollection dbtcCollection = new DbtcCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataDbtc(linha, data);
                            // Somente é salvo DBTC que possui dados relevantes
                            Dbtc dbtc = (Dbtc)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                dbtcCollection.Add(dbtc);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaDbtcException(e.Message);
            }

            return dbtcCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>        
        /// throws ArquivoDBTCIncorretoException se DataMovimento do Header do arquivo Dbtc for diferente de data
        public DbtcCollection ProcessaDbtc(StreamReader sr, DateTime data)
        {
            DbtcCollection dbtcCollection = new DbtcCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataDbtc(linha, data);
                        // Somente é salvo DBTC que possui dados relevantes
                        Dbtc dbtc = (Dbtc)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            dbtcCollection.Add(dbtc);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaDbtcException(e.Message);
            }

            return dbtcCollection;
        }

        /// <summary>
        /// A partir de um Arquivo DBTC Existente Gera um novo Arquivo DBTC somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoDBTC">Caminho Completo do Arquivo DBTC</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo DBTC será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o DBTC</param>
        /// <returns>true se novo arquivo DBTC foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoDBTCNaoEncontradoException">Se Arquivo DBTC de entrada não existe</exception>
        public bool GeraArquivoDBTC(string nomeArquivoCompletoDBTC, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoDBTC)) {
                throw new ArquivoDbtcNaoEncontradoException("Arquivo DBTC não Encontrado: " + nomeArquivoCompletoDBTC);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de Novo Arquivo
            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "dbtc_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoDBTC)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se dado pertence a algum dos clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo DBTC pertence a algum dos Clientes da Lista
        /// </summary>
        /// <param name="linhaDBTC">linha do arquivo DBTC</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo DBTC pertence a algum dos Clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaDBTC, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaDBTC.Substring(0, 2)) {
                case EstruturaArquivoDBTC.Header:
                case EstruturaArquivoDBTC.Trailer:
                    break;
                case EstruturaArquivoDBTC.RelacaoContratoUsuariosDoadores:
                case EstruturaArquivoDBTC.RelacaoContratoUsuariosTomadores:
                    int codigoCliente = Convert.ToInt32(linhaDBTC.Substring(177, 7)); // Campo 11
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// <exception cref="ArgumentException">Se tipoRegistro não está dentro dos valores possiveis do enum EstruturaArquivoDBTC</exception>
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoDBTC");

            List<string> valoresPossiveis = EstruturaArquivoDBTC.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo DBTC
            switch (tipoRegistro) {
                case EstruturaArquivoDBTC.Header:
                    this.tipoRegistro = EstruturaArquivoDBTC.Header;
                    break;
                case EstruturaArquivoDBTC.RelacaoContratoUsuariosDoadores:
                    this.tipoRegistro = EstruturaArquivoDBTC.RelacaoContratoUsuariosDoadores;
                    break;
                case EstruturaArquivoDBTC.RelacaoContratoUsuariosTomadores:
                    this.tipoRegistro = EstruturaArquivoDBTC.RelacaoContratoUsuariosTomadores;
                    break;
                case EstruturaArquivoDBTC.Trailer:
                    this.tipoRegistro = EstruturaArquivoDBTC.Trailer;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// Salva os atributos do arquivo Dbtc de acordo com o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataDbtc(string linha, DateTime data) {
            // Limpa os valores do Dbtc
            this.Initialize();
            //                        
            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoDBTC.Header) {
                #region Header
                this.codigoAgente = Convert.ToInt32( linha.Substring(6, 4) ); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(30, 8); // Campo 07                                
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if( this.dataMovimento.Value.CompareTo(data) != 0 ) {
                    throw new ArquivoDbtcIncorretoException("Arquivo DBTC com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                                        
                }

                // Indica que DBTC deve ser salvo                
                this.dbtcProcessado = true;
                #endregion
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoDBTC.RelacaoContratoUsuariosDoadores) {
                #region Doador
                //
                this.numeroContratoDoador = Convert.ToInt32(linha.Substring(2, 8)); // Campo 02
                this.tipoOrigemDoador = linha.Substring(101, 60).Trim(); // Campo 08
                // Formato = "AAAAMMDD"
                string dataRegistroDoador = linha.Substring(169, 8); // Campo 10
                int dia = Convert.ToInt32(dataRegistroDoador.Substring(6, 2));
                int mes = Convert.ToInt32(dataRegistroDoador.Substring(4, 2));
                int ano = Convert.ToInt32(dataRegistroDoador.Substring(0, 4));
                this.dataRegistroDoador = new DateTime(ano, mes, dia);
                //
                this.codigoClienteDoador = Convert.ToInt32(linha.Substring(177, 7)); // Campo 11
                this.quantidadeDoador = Convert.ToInt64(linha.Substring(212, 18)); // Campo 16
                // Formato = "AAAAMMDD"
                string dataVencimentoDoador = linha.Substring(230, 8); // Campo 17
                dia = Convert.ToInt32(dataVencimentoDoador.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimentoDoador.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimentoDoador.Substring(0, 4));
                this.dataVencimentoDoador = new DateTime(ano, mes, dia);
                //
                int puDoadorInteiro = Convert.ToInt32(linha.Substring(238, 11)); // Campo 18
                int puDoadorFracionario = Convert.ToInt32(linha.Substring(249, 7));
                decimal puDoadorFracionarioDecimal = puDoadorFracionario / 10000000M;
                this.puDoador = puDoadorInteiro + puDoadorFracionarioDecimal;
                //
                this.fatorCotacaoDoador = Convert.ToInt32(linha.Substring(256, 7)); // Campo 19
                //
                int taxaOperacaoDoadorInteiro = Convert.ToInt32(linha.Substring(266,5)); // Campo 21
                int taxaOperacaoDoadorFracionario = Convert.ToInt32(linha.Substring(271, 5));
                decimal taxaOperacaoDoadorFracionarioDecimal = taxaOperacaoDoadorFracionario / 100000M;
                this.taxaOperacaoDoador = taxaOperacaoDoadorInteiro + taxaOperacaoDoadorFracionarioDecimal;
                //
                int taxaComissaoDoadorInteiro = Convert.ToInt32(linha.Substring(276, 5)); // Campo 22
                int taxaComissaoDoadorFracionario = Convert.ToInt32(linha.Substring(281, 5));
                decimal taxaComissaoDoadorFracionarioDecimal = taxaComissaoDoadorFracionario / 100000M;
                this.taxaComissaoDoador = taxaComissaoDoadorInteiro + taxaComissaoDoadorFracionarioDecimal;
                //
                this.cdAtivoBolsaDoador = linha.Substring(395, 12).Trim(); // Campo 30
                //

                // Se Codigo Cliente não está cadastrado na Base não salva DBTC                                
                /*
                Cliente cliente = new Cliente();
                if (!cliente.LoadByPrimaryKey(this.codigoClienteDoador.Value) ) {
                    this.dbtcProcessado = false;
                }
                else {
                    // Indica que DBTC deve ser salvo
                    this.dbtcProcessado = true;
                }*/
                this.dbtcProcessado = true;
                #endregion
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoDBTC.RelacaoContratoUsuariosTomadores) {
                #region Tomador
                //
                this.numeroContratoTomador = Convert.ToInt32(linha.Substring(2, 8)); // Campo 02
                this.tipoOrigemTomador = linha.Substring(101, 60).Trim(); // Campo 08
                // Formato = "AAAAMMDD"
                string dataRegistroTomador = linha.Substring(169, 8); // Campo 10
                int dia = Convert.ToInt32(dataRegistroTomador.Substring(6, 2));
                int mes = Convert.ToInt32(dataRegistroTomador.Substring(4, 2));
                int ano = Convert.ToInt32(dataRegistroTomador.Substring(0, 4));
                this.dataRegistroTomador = new DateTime(ano, mes, dia);
                //
                this.codigoClienteTomador = Convert.ToInt32(linha.Substring(177, 7)); // Campo 11
                this.codigoCarteiraDestino = Convert.ToInt32(linha.Substring(190, 5)); // Campo 13
                this.quantidadeTomador = Convert.ToInt64(linha.Substring(212, 18)); // Campo 16
                // Formato = "AAAAMMDD"
                string dataVencimentoTomador = linha.Substring(248, 8); // Campo 18
                dia = Convert.ToInt32(dataVencimentoTomador.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimentoTomador.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimentoTomador.Substring(0, 4));
                this.dataVencimentoTomador = new DateTime(ano, mes, dia);
                //
                int puTomadorInteiro = Convert.ToInt32(linha.Substring(256, 11)); // Campo 19
                int puTomadorFracionario = Convert.ToInt32(linha.Substring(267, 7));
                decimal puTomadorFracionarioDecimal = puTomadorFracionario / 10000000M;
                this.puTomador = puTomadorInteiro + puTomadorFracionarioDecimal;
                //
                this.fatorCotacaoTomador = Convert.ToInt32(linha.Substring(274, 7)); // Campo 20
                //
                int taxaOperacaoTomadorInteiro = Convert.ToInt32(linha.Substring(284, 5)); // Campo 22
                int taxaOperacaoTomadorFracionario = Convert.ToInt32(linha.Substring(289, 5));
                decimal taxaOperacaoTomadorFracionarioDecimal = taxaOperacaoTomadorFracionario / 100000M;
                this.taxaOperacaoTomador = taxaOperacaoTomadorInteiro + taxaOperacaoTomadorFracionarioDecimal;
                //
                int taxaComissaoTomadorInteiro = Convert.ToInt32(linha.Substring(294, 5)); // Campo 23
                int taxaComissaoTomadorFracionario = Convert.ToInt32(linha.Substring(299, 5));
                decimal taxaComissaoTomadorFracionarioDecimal = taxaComissaoTomadorFracionario / 100000M;
                this.taxaComissaoTomador = taxaComissaoTomadorInteiro + taxaComissaoTomadorFracionarioDecimal;
                //
                this.cdAtivoBolsaTomador = linha.Substring(350, 12).Trim(); // Campo 29
                //            

                // Se Codigo Cliente não está cadastrado na Base não salva DBTC                
                /*
                Cliente cliente = new Cliente();
                if (!cliente.LoadByPrimaryKey(this.codigoClienteTomador.Value)) {
                    this.dbtcProcessado = false;
                }
                else {
                    // Indica que DBTC deve ser salvo
                    this.dbtcProcessado = true;
                } */
                this.dbtcProcessado = true;
                #endregion
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoDBTC.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Doador</returns>
        public bool IsTipoRegistroDoador() {
            return this.tipoRegistro == EstruturaArquivoDBTC.RelacaoContratoUsuariosDoadores;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Tomador</returns>
        public bool IsTipoRegistroTomador() {
            return this.tipoRegistro == EstruturaArquivoDBTC.RelacaoContratoUsuariosTomadores;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoDBTC.Header;
        }
    }
}
