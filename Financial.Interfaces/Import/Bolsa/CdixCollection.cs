using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class CdixCollection {
        private List<Cdix> collectionCdix;

        public List<Cdix> CollectionCdix {
            get { return collectionCdix; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CdixCollection() {
            this.collectionCdix = new List<Cdix>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de Cdixs</param>
        public void Add(Cdix cdix) {
            collectionCdix.Add(cdix);
        }
    }
}
