﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Negs {

        // Informação sobre o tipo registro da linha do arquivo Negs
        private string tipoRegistro;
        
        // bool indicando se Negs deve ser salvo na colecao de Negss
        // Somente será salvo o Tipo de Registro do Negs que for necessario
        private bool negsProcessado;

        #region Properties dos Valores do Registro00 - Header
        
        int? codigoAgenteCorretora; // Campo 2.2 - Codigo do usuario
        DateTime? dataPregao; // Campo 06
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgenteCorretora {
            get { return codigoAgenteCorretora; }
            set { codigoAgenteCorretora = value; }
        }

        public DateTime? DataPregao {
            get { return dataPregao; }
            set { dataPregao = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Detalhe do Negocio Realizado

        int? numeroNegocio; // Campo 02

        public int? NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        string tipoOperacao; // Campo 03

        public string TipoOperacao {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        string cdAtivoBolsa; // Campo 04

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        string tipoMercado; // Campo 05

        public string TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        Int64? quantidade; // Campo 09

        public Int64? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        decimal? pu; // Campo 10

        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        /* Usado somente para termo - prazo em dias */
        int? prazoVencimento; // Campo 12

        public int? PrazoVencimento {
            get { return prazoVencimento; }
            set { prazoVencimento = value; }
        }

        /* ' ' -  INCLUSÃO; 'A' - ANULAÇÃO */
        char? situacaoNegocio; // Campo 15

        public char? SituacaoNegocio {
            get { return situacaoNegocio; }
            set { situacaoNegocio = value; }
        }
        
        /* Somente usado para EOC(EXERCÍCIO DE OPÇÃO DE COMPRA), 
         *                    EOV(EXERCÍCIO DE OPÇÃO DE VENDA), 
         *                    OPC(OPÇÃO DE COMPRA)
         *                    OPV(OPÇÃO DE VENDA)
         */
        string cdAtivoBolsaObjeto; // Campo 16

        public string CdAtivoBolsaObjeto {
            get { return cdAtivoBolsaObjeto; }
            set { cdAtivoBolsaObjeto = value; }
        }

        int? fatorCotacao; // Campo 21

        public int? FatorCotacao {
            get { return fatorCotacao; }
            set { fatorCotacao = value; }
        }

        /* '*' - SIM    -   ' ' - Não */
        char? indicadorAfterMarket; // Campo 23

        public char? IndicadorAfterMarket {
            get { return indicadorAfterMarket; }
            set { indicadorAfterMarket = value; }
        }      
        #endregion

        #region Atributos Extras do Negs
        /// <summary>
        /// Classe de Atributos Extras do Negs
        /// Esses Atributos não Aparecem no Arquivo Negs
        /// </summary>
        [Serializable]
        public class ExtendedAtributos {
            private int? idCliente;
            private decimal? valor;
            
            // Campo CdAtivo + TipoOperacao Usado para Ordenação do Negs
            private string campoOrdenacao;

            public int? IdCliente {
                get { return idCliente; }
                set { idCliente = value; }
            }

            public decimal? Valor {
                get { return valor; }
                set { valor = value; }
            }

            public string CampoOrdenacao {
                get { return campoOrdenacao; }
                set { campoOrdenacao = value; }
            }
        }
        #endregion

        private ExtendedAtributos extendAtributos = new ExtendedAtributos();

        public ExtendedAtributos ExtendAtributos {
            get { return extendAtributos; }
        }

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Negs devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.negsProcessado = false;
            this.codigoAgenteCorretora = null;
            this.dataPregao = null;
            //
            this.numeroNegocio = null; 
            this.tipoOperacao = null;
            this.cdAtivoBolsa = null;
            this.tipoMercado = null;
            this.quantidade = null;
            this.pu = null;
            this.prazoVencimento = null;
            this.situacaoNegocio = null;
            this.cdAtivoBolsaObjeto = null;
            this.fatorCotacao = null;
            this.indicadorAfterMarket = null;
        }

        /// <summary>
        ///  Indica se Negs possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return negsProcessado;
        }

        /// <summary>
        /// Realiza a Leitura do Arquivo Negs
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <exception cref="ArquivoNegsNaoEncontradoException">Se Path do Arquivo não Existe</exception>
        /// <exception cref="ArquivoNegsIncorretoException">Se DataPregao do Header do arquivo Negs for diferente de data de Entrada</exception>
        public NegsCollection ProcessaNegs(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoNegsNaoEncontradoException("Arquivo NEGS não encontrado: " + nomeArquivoCompleto);
            }
                               
            NegsCollection negsCollection = new NegsCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {                    
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataNegs(linha, data);
                            // Somente é salvo Negs que possui dados relevantes
                            Negs negs = (Negs)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                negsCollection.Add(negs);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Negs com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha "+i+":"+linha);
                //
                //throw new ProcessaNegsException(mensagem.ToString());
                throw new ProcessaNegsException(e.Message);
            }

            return negsCollection;
        }

        /// <summary>
        /// Realiza a Leitura do Arquivo Negs
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>        
        /// <exception cref="ArquivoNegsIncorretoException">Se DataPregao do Header do arquivo Negs for diferente de data de Entrada</exception>
        public NegsCollection ProcessaNegs(StreamReader sr, DateTime data)
        {
            NegsCollection negsCollection = new NegsCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataNegs(linha, data);
                        // Somente é salvo Negs que possui dados relevantes
                        Negs negs = (Negs)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            negsCollection.Add(negs);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Negs com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                //
                //throw new ProcessaNegsException(mensagem.ToString());
                throw new ProcessaNegsException(e.Message);
            }

            return negsCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// <exception cref="ArgumentException">se tipoRegistro não está dentro dos Valores possiveis do enum EstruturaArquivoNEGS</exception>
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoNEGS");

            List<string> valoresPossiveis = EstruturaArquivoNEGS.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo NEGS
            switch (tipoRegistro) {
                case EstruturaArquivoNEGS.Header:
                    this.tipoRegistro = EstruturaArquivoNEGS.Header;
                    break;
                case EstruturaArquivoNEGS.DetalheNegocioRealizado:
                    this.tipoRegistro = EstruturaArquivoNEGS.DetalheNegocioRealizado;
                    break;
                case EstruturaArquivoNEGS.Trailer:
                    this.tipoRegistro = EstruturaArquivoNEGS.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Negs de acordo com o tipo
        private void TrataNegs(string linha, DateTime data) {
            // Limpa os valores do Negs
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoNEGS.Header) {
                //
                this.codigoAgenteCorretora = Convert.ToInt32( linha.Substring(6, 4) ); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataPregao = linha.Substring(30, 8); // Campo 06
                int dia = Convert.ToInt32( dataPregao.Substring(6, 2));
                int mes = Convert.ToInt32( dataPregao.Substring(4, 2));
                int ano = Convert.ToInt32( dataPregao.Substring(0, 4));
                //
                this.dataPregao = new DateTime(ano, mes, dia);

                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataPregao.Value.CompareTo(data) != 0) {
                    throw new ArquivoNegsNaoEncontradoException("Arquivo NEGS com data incorreta: " + this.dataPregao.Value.ToString("dd/MM/yyyy"));                    
                }

                // Indica que Negs deve ser salvo                
                this.negsProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoNEGS.DetalheNegocioRealizado) {
                //
                this.numeroNegocio = Convert.ToInt32(linha.Substring(2, 7)); // Campo 02
                this.tipoOperacao = linha.Substring(9, 1).Trim(); // Campo 03
                this.cdAtivoBolsa = linha.Substring(10, 12).Trim(); // Campo 04
                this.tipoMercado = linha.Substring(22, 3).Trim(); // Campo 05
                this.quantidade = Convert.ToInt64(linha.Substring(50, 11)); // Campo 09
                //
                int puInteiro = Convert.ToInt32(linha.Substring(61, 9)); // Campo 10
                int puFracionario = Convert.ToInt32(linha.Substring(70, 2));
                decimal puFracionarioDecimal = puFracionario / 100M;
                this.pu = puInteiro + puFracionarioDecimal;
                //
                if (!linha.Substring(77, 3).Trim().Equals("")) {
                    this.prazoVencimento = Convert.ToInt32(linha.Substring(77, 3)); // Campo 12
                }
                else {
                    this.prazoVencimento = 0;
                }
                this.situacaoNegocio = Convert.ToChar(linha.Substring(86, 1)); // Campo 15
                //
                if (!linha.Substring(87, 12).Trim().Equals("")) {
                    this.cdAtivoBolsaObjeto = linha.Substring(87, 12).Trim(); // Campo 16                
                }                               
                //
                this.fatorCotacao = Convert.ToInt32(linha.Substring(122, 7)); // Campo 21
                //
                this.indicadorAfterMarket = Convert.ToChar(linha.Substring(140, 1)); // Campo 23
                //                
                // Preenche o Valor Extendido Valor
                this.extendAtributos.Valor = (this.quantidade.Value * this.pu.Value) / this.fatorCotacao;

                // Preenche o Campo Extendido CampoOrdenacao
                this.extendAtributos.CampoOrdenacao = this.cdAtivoBolsa.Trim() + this.TipoOperacao.Trim();


                // Indica que Negs deve ser salvo
                this.negsProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoNEGS.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é DetalheNegocioRealizado</returns>
        public bool IsTipoRegistroDetalheNegocioRealizado() {
            return this.tipoRegistro == EstruturaArquivoNEGS.DetalheNegocioRealizado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Vista</returns>
        public bool IsTipoMercadoVista() {
            return this.TipoMercado == TipoDoMercadoNegs.Vista;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é ExercicioOpcao Compra</returns>
        public bool IsTipoMercadoExercicioCompra() {
            return this.TipoMercado == TipoDoMercadoNegs.ExercicioCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é ExercicioOpcao Venda</returns>
        public bool IsTipoMercadoExercicioVenda() {
            return this.TipoMercado == TipoDoMercadoNegs.ExercicioVenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é LeilaoTitulosNaoCotados</returns>
        public bool IsTipoMercadoLeilaoTitulosNaoCotados() {
            return this.TipoMercado == TipoDoMercadoNegs.LeilaoTitulosNaoCotados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Leilao</returns>
        public bool IsTipoMercadoLeilao() {
            return this.TipoMercado == TipoDoMercadoNegs.Leilao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Fracionario</returns>
        public bool IsTipoMercadoFracionario() {
            return this.TipoMercado == TipoDoMercadoNegs.Fracionario;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Termo</returns>
        public bool IsTipoMercadoTermo() {
            return this.TipoMercado == TipoDoMercadoNegs.Termo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é OpcaoCompra</returns>
        public bool IsTipoMercadoOpcaoCompra() {
            return this.TipoMercado == TipoDoMercadoNegs.OpcaoCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é OpcaoVenda</returns>
        public bool IsTipoMercadoOpcaoVenda() {
            return this.TipoMercado == TipoDoMercadoNegs.OpcaoVenda;
        }

        /// <summary>
        ///  Função delegate que Serve para ordenar a collection de Negs pelo numero do negocio
        /// </summary>
        /// <param name="negs1">NumeroNegocio1</param>
        /// <param name="negs2">NumeroNegocio2</param>
        /// <returns> 0 se x igual y
        ///           1 se x maior y  
        ///          -1 se x menor y 
        /// </returns>
        public int OrderNegsByNumeroNegocio(Negs negs1, Negs negs2) {
            int retorno = 0; // x = y (default)
            if (negs1.numeroNegocio != null && negs2.numeroNegocio != null) {
                if (negs1.numeroNegocio < negs2.numeroNegocio) {
                    retorno = -1;
                }
                else {
                    retorno = 1;
                }
            }
            return retorno;
        }

        /// <summary>
        ///  Função Delegate que Serve para Ordenar a Collection de negs pelo CdativoBolsa + TipoOperacao
        /// </summary>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <returns> 0 se x igual y
        ///           1 se x maior y  
        ///          -1 se x menor y
        /// </returns>
        public int OrderNegsByCdAtivoBolsaTipoOperacao(Negs n1, Negs n2) {
            if (String.IsNullOrEmpty(n1.extendAtributos.CampoOrdenacao) || String.IsNullOrEmpty(n2.extendAtributos.CampoOrdenacao)) {
                return 0;
            }
            else {
                return n1.extendAtributos.CampoOrdenacao.CompareTo(n2.extendAtributos.CampoOrdenacao);
            }
        }

        /// <summary>
        /// Codigo Bovespa é Retirado do Nome do Arquivo Negs
        /// Se Nome do arquivo não começar com Numero retorna a string vazia
        /// </summary>
        /// <param name="pathArquivo"></param>
        /// <returns></returns>
        /// <exception cref="ArquivoNegsNaoEncontradoException">Se Path do Arquivo não Existe</exception>
        public string RetornaCodigoBovespa(string pathArquivo) {
            if (!File.Exists(pathArquivo)) {
                throw new ArquivoNegsNaoEncontradoException("Arquivo NEGS não encontrado: " + pathArquivo);
            }

            #region Trata IdCliente do Arquivo
            string pathAux = pathArquivo;
            //
            pathAux = pathAux.Replace("\\", "/");
            int indice = pathAux.LastIndexOf("/") + 1;
            string arquivo = pathAux.Substring(indice, pathAux.Length - indice).Trim();
            char[] arquivoCliente = arquivo.ToCharArray();

            string idClienteString = "";
            for (int i = 0; char.IsDigit(arquivoCliente[i]); i++) {
                idClienteString += arquivoCliente[i];
            }
            #endregion

            return idClienteString;
        }

        /// <summary>
        /// Agrupa a Collection por CdAtivo E Tipo Operacao fazendo a somatoria de valor e quantidade
        /// </summary>
        /// <param name="negsCollection">collection de negs que se quer Agrupar</param>
        /// <returns>Nova Collection de Negs Agrupada por CdAtivo E Tipo Operacao</returns>
        public NegsCollection GroupBy(NegsCollection negsCollection) {
            NegsCollection negsAgrupada = new NegsCollection();

            // Ordena por CdAtivo concatenado com Tipo Operacao
            Negs n = new Negs();
            negsCollection.CollectionNegs.Sort(n.OrderNegsByCdAtivoBolsaTipoOperacao);

            Int64 quantidade = 0;
            decimal valor = 0;
            //
            for (int i = 0; i < negsCollection.CollectionNegs.Count; i++) {
                quantidade += negsCollection.CollectionNegs[i].Quantidade.Value;
                valor += negsCollection.CollectionNegs[i].extendAtributos.Valor.Value;

                int j = i;
                for ( ; j < negsCollection.CollectionNegs.Count; j++) {
                    
                    // Se tem Próximo
                    if (j + 1 <= negsCollection.CollectionNegs.Count - 1) {
                        // Se próximo é igual ao Anterior
                        if (negsCollection.CollectionNegs[j].ExtendAtributos.CampoOrdenacao == negsCollection.CollectionNegs[j + 1].ExtendAtributos.CampoOrdenacao) {
                            quantidade += negsCollection.CollectionNegs[j + 1].Quantidade.Value;
                            valor += negsCollection.CollectionNegs[j + 1].extendAtributos.Valor.Value;
                        }
                        else {
                            // Não é igual
                            break;
                        }
                    }
                    else {
                        // Não tem Próximo
                        break;
                    }
                }

                i = j;
                // Cria o elemento da nova collection
                Negs negs = new Negs();
                negs.CdAtivoBolsa = negsCollection.CollectionNegs[i].CdAtivoBolsa.Trim();
                negs.Quantidade = quantidade;
                negs.ExtendAtributos.Valor = valor;
                negs.TipoOperacao = negsCollection.CollectionNegs[i].TipoOperacao.Trim();
                //
                negs.FatorCotacao = negsCollection.CollectionNegs[i].FatorCotacao;
                negs.Pu = negsCollection.CollectionNegs[i].Pu;
                negs.ExtendAtributos.IdCliente = negsCollection.CollectionNegs[i].ExtendAtributos.IdCliente;
                //
                negsAgrupada.Add(negs);
                //
                // Soma por CdativoBolsa
                quantidade = 0;
                valor = 0;                
            }

            return negsAgrupada;
        }
    }
}
