﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class GVDat
    {
        // Informação sobre o tipo registro da linha do arquivo Negs
        private string tipoRegistro;

        // bool indicando se GVDat deve ser salvo na colecao de GVDats
        // Somente será salvo o Tipo de Registro do GVDat que for necessario
        private bool GVDatProcessado;

        #region Properties dos Valores do Registro01 - Detalhe dos papeis

        string cdAtivoBolsa; // Campo 01

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        decimal? desagio; // Campo 2

        public decimal? Desagio {
            get { return desagio; }
            set { desagio = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo GVDat devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.GVDatProcessado = false;
            //
            this.cdAtivoBolsa = null;
            this.desagio = null;            
        }

        /// <summary>
        ///  Indica se GVDat possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return GVDatProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// TODO: Diretorio Servidor
        /// throws ArquivoNegsNaoEncontradoException
        /// throws ArquivoNegsIncorretoException se DataPregao do Header do arquivo Negs for diferente de data
        public GVDatCollection ProcessaGVDat(DateTime data, string path) 
        {
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path).Append("GV.dat");

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadGVDat(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoGVDatIncorretoException("Arquivo GV.dat não encontrado: " + nomeArquivo);
                }
            }

            
            GVDatCollection gVDatCollection = new GVDatCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivo.ToString()))
                {                    
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataGVDat(linha, data);
                            // Somente é salvo GVDat que possui dados relevantes
                            GVDat gVDat = (GVDat)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                gVDatCollection.Add(gVDat);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento GVDat com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha "+i+":"+linha);                                                                
                throw new ProcessaGVDatException(mensagem.ToString());
            }

            return gVDatCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoNEGS
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoGVDat");

            List<string> valoresPossiveis = EstruturaArquivoGVDAT.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo GVDat
            switch (tipoRegistro) {
                case EstruturaArquivoGVDAT.Header:
                    this.tipoRegistro = EstruturaArquivoGVDAT.Header;
                    break;
                case EstruturaArquivoGVDAT.DadosDosPapeis:
                    this.tipoRegistro = EstruturaArquivoGVDAT.DadosDosPapeis;
                    break;
                case EstruturaArquivoGVDAT.Trailer:
                    this.tipoRegistro = EstruturaArquivoGVDAT.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo GVDat de acordo com o tipo
        private void TrataGVDat(string linha, DateTime data) {
            // Limpa os valores do Negs
            this.Initialize();

            if (this.tipoRegistro == EstruturaArquivoNEGS.DetalheNegocioRealizado) {
                //
                this.cdAtivoBolsa = linha.Substring(2, 20).Trim(); // Campo 01
                this.desagio = Convert.ToDecimal(linha.Substring(65, 23).Replace("0", "")); // Campo 02                

                // Indica que GVDat deve ser salvo
                this.GVDatProcessado = true;
            }                        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é DetalheNegocioRealizado</returns>
        public bool IsTipoRegistroDetalheDadoPapel() {
            return this.tipoRegistro == EstruturaArquivoGVDAT.DadosDosPapeis;
        }

        /// <summary>
        /// Baixa o arquivo GVDat da Internet e Descompacta o arquivo Zip 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool DownloadGVDat(DateTime data, string path)
        {
            string nomeArquivoDestino = "GV.dat";
            string endereco = "http://www.cblc.com.br/cblc/ControleRisco/Limgar/Download/GV.dat";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            
            // Se arquivo GV.dat existe retorna true
            return File.Exists(pathArquivoDestino);
        }
    }
}
