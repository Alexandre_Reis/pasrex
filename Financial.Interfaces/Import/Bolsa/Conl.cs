﻿using System;
using System.Text;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Conl {
        // Informação sobre o tipo registro da linha do arquivo CONL
        private string tipoRegistro;
        
        // bool indicando se conl deve ser salvo na colecao de ConLs
        // Somente será salvo o Tipo de Registro do ConL que for necessario
        private bool conlProcessado;

        #region Properties dos Valores do Registro00 - Header

        int? codigoAgente; // Campo 04 - Codigo do usuario
        DateTime? dataMovimento; // Campo 07
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Identificação do Contrato Liquidado

        DateTime? dataVencimento; // Campo 03
        int? codigoCliente; // Campo 06
        int? numeroContrato; // Campo 10
        int? numeroNegocio; // Campo 12
        int? quantidade; // Campo 16
        decimal? pu; // Campo 17
        decimal? valor; // Campo 18
        int? tipoLiquidacao; // Campo 20
        string indicadorPosicao; //Campo 22
        string cdAtivoBolsa; // Campo 26
        DateTime? dataLiquidacao; // Campo 27

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? NumeroContrato {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? Valor {
            get { return valor; }
            set { valor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? TipoLiquidacao {
            get { return tipoLiquidacao; }
            set { tipoLiquidacao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IndicadorPosicao
        {
            get { return indicadorPosicao; }
            set { indicadorPosicao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataLiquidacao {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Conl devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.conlProcessado = false;
            this.codigoAgente = null;
            this.dataMovimento = null;
            this.DataVencimento = null;
            this.codigoCliente = null;
            this.numeroContrato = null;
            this.numeroNegocio = null;
            this.quantidade = null;
            this.pu = null;
            this.valor = null;
            this.tipoLiquidacao = null;
            this.indicadorPosicao = null;
            this.cdAtivoBolsa = null;
            this.dataLiquidacao = null;
        }

        /// <summary>
        ///  Indica se Conl possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return conlProcessado;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <exception cref="ArquivoConlNaoEncontradoException">Se arquivo Conl não Encontrado</exception>
        /// <exception cref="ProcessaConlException">Se ocorrer Erro no Processamento do Arquivo</exception> 
        public ConlCollection ProcessaConl(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoConlNaoEncontradoException("Arquivo CONL " + nomeArquivoCompleto + " não encontrado.");
            }
                               
            ConlCollection conLCollection = new ConlCollection();
            int i = 1;
            string linha = "";                
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataConL(linha, data);
                            // Somente é salvo CONL que possui dados relevantes
                            Conl conl = (Conl)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                conLCollection.Add(conl);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaConlException(e.Message);
            }

            return conLCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <exception cref="ProcessaConlException">Se ocorrer Erro no Processamento do Arquivo</exception> 
        public ConlCollection ProcessaConl(StreamReader sr, DateTime data)
        {
            ConlCollection conLCollection = new ConlCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataConL(linha, data);
                        // Somente é salvo CONL que possui dados relevantes
                        Conl conl = (Conl)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            conLCollection.Add(conl);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaConlException(e.Message);
            }

            return conLCollection;
        }

        /// <summary>
        /// A partir de um Arquivo Conl Existente Gera um novo Arquivo Conl somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoConl">Caminho Completo do Arquivo Conl</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Conl será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Conl</param>
        /// <returns>true se novo arquivo Conl foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoConlNaoEncontradoException">Se Arquivo Conl de entrada não existe</exception>
        public bool GeraArquivoConl(string nomeArquivoCompletoConl, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoConl)) {
                throw new ArquivoConlNaoEncontradoException("Arquivo Conl não Encontrado: " + nomeArquivoCompletoConl);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo
            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "conl_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoConl)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se dado pertence a algum dos clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Conl pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaConl">linha do arquivo Conl</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Conl pertence a algum dos Clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaConl, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaConl.Substring(0, 2)) {
                case EstruturaArquivoCONL.Header:
                case EstruturaArquivoCONL.Trailer:
                    break;
                case EstruturaArquivoCONL.IdentificacaoContratoLiquidado:
                    int codigoCliente = Convert.ToInt32(linhaConl.Substring(24, 7)); // Campo 06
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// <exception cref="ArgumentException"> Se tipoRegistro não está dentro dos valores possiveis do enum EstruturaArquivoCONL</exception>
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoCONL");

            List<string> valoresPossiveis = EstruturaArquivoCONL.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo CONL
            switch (tipoRegistro) {
                case EstruturaArquivoCONL.Header:
                    this.tipoRegistro = EstruturaArquivoCONL.Header;
                    break;
                case EstruturaArquivoCONL.IdentificacaoContratoLiquidado:
                    this.tipoRegistro = EstruturaArquivoCONL.IdentificacaoContratoLiquidado;
                    break;
                case EstruturaArquivoCONL.Trailer:
                    this.tipoRegistro = EstruturaArquivoCONL.Trailer;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// Salva os atributos do arquivo Conl de acordo com o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataConL(string linha, DateTime data) {
            // Limpa os valores do ConL
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCONL.Header) {
                this.codigoAgente = Convert.ToInt32( linha.Substring(18, 4) ); // Campo 04
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(39, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);

                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoConlIncorretoException("Arquivo CONL com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                    
                }

                // Indica que ConL deve ser salvo                
                this.conlProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCONL.IdentificacaoContratoLiquidado) {
                // Formato = "AAAAMMDD"
                string dataVencimento = linha.Substring(10, 8); // Campo 03
                int dia = Convert.ToInt32(dataVencimento.Substring(6, 2));
                int mes = Convert.ToInt32(dataVencimento.Substring(4, 2));
                int ano = Convert.ToInt32(dataVencimento.Substring(0, 4));
                this.dataVencimento = new DateTime(ano, mes, dia);
                //                
                this.codigoCliente = Convert.ToInt32(linha.Substring(24, 7)); // Campo 06
                this.numeroContrato = Convert.ToInt32(linha.Substring(38, 9)); // Campo 10
                this.numeroNegocio = Convert.ToInt32(linha.Substring(48, 7)); // Campo 12
                this.quantidade = Convert.ToInt32(linha.Substring(82, 15)); // Campo 16
                //
                int puInteiro = Convert.ToInt32(linha.Substring(97, 11)); // Campo 17
                int puFracionario = Convert.ToInt32(linha.Substring(108, 2));
                decimal puFracionarioDecimal = puFracionario / 100M;
                this.pu = puInteiro + puFracionarioDecimal;
                //
                int valorInteiro = Convert.ToInt32(linha.Substring(110, 11)); // Campo 18
                int valorFracionario = Convert.ToInt32(linha.Substring(121, 2));
                decimal valorFracionarioDecimal = valorFracionario / 100M;
                this.valor = valorInteiro + valorFracionarioDecimal;
                //
                /* 1- Antecipada, 2- Por Diferença, 3- Por Decurso, 4- Por Diferença Especifica */
                this.tipoLiquidacao = Convert.ToInt32(linha.Substring(131, 1)); // Campo 20
                //

                this.indicadorPosicao = linha.Substring(133, 1).Trim(); // Campo 22

                // OBS: no arquivo aparece tamanho 12 mas deve ser tamanho 11
                this.cdAtivoBolsa = linha.Substring(156, 11).Trim(); // Campo 26
                //
                // Formato = "AAAA-MM-DD"
                string dataLiquidacao = linha.Substring(168, 10); // Campo 27
                dia = Convert.ToInt32(dataLiquidacao.Substring(8, 2));
                mes = Convert.ToInt32(dataLiquidacao.Substring(5, 2));
                ano = Convert.ToInt32(dataLiquidacao.Substring(0, 4));
                this.dataLiquidacao = new DateTime(ano, mes, dia);
                //
                // Indica que ConL deve ser salvo
                this.conlProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCONL.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoContratoLiquidado</returns>
        public bool IsTipoRegistroIdentificacaoContratoLiquidado() {
            return this.tipoRegistro == EstruturaArquivoCONL.IdentificacaoContratoLiquidado;
        }
    }
}
