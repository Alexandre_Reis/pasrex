using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.Bolsa
{
    public class PosicaoNotas
    {
        //campos do cabecalho e do rodape
        public java.awt.Rectangle recDataPregao;
        public java.awt.Rectangle recCodigoBovespa;
        public java.awt.Rectangle recCodigoCliente;
        public java.awt.Rectangle recCorretagem;
        public java.awt.Rectangle recIss;
        public java.awt.Rectangle recTaxaLiquidacao;
        public java.awt.Rectangle recRegistroCBLC;
        public java.awt.Rectangle recRegistroBolsa;
        public java.awt.Rectangle recEmolumento;
        public java.awt.Rectangle recIsDayTrade;
        public java.awt.Rectangle recIsDayTrade2;

        //campos da lista de detalhe
        public java.awt.Rectangle recTipoOperacao;
        public java.awt.Rectangle recTipoMercado;
        public java.awt.Rectangle recPrazo;
        public java.awt.Rectangle recAtivo;
        public java.awt.Rectangle recObs;
        public java.awt.Rectangle recQuantidade;
        public java.awt.Rectangle recPu;
        public java.awt.Rectangle recValor;

    }

    public class DetalheNotaBolsaPDF
    {
        private String _tipoOperacao;
        private String _tipoMercado;
        private String _prazo;
        private String _ativo;
        private String _obs;
        private Decimal _quantidade;
        private Decimal _pu;
        private Decimal _valor;

        public Decimal valor
        {
            set
            {
                _valor = value;
            }
            get
            {
                return _valor;
            }
        }

        public Decimal pu
        {
            set
            {
                _pu = value;
            }
            get
            {
                return _pu;
            }
        }

        public Decimal quantidade
        {
            set
            {
                _quantidade = value;
            }
            get
            {
                return _quantidade;
            }
        }

        public String obs
        {
            set
            {
                _obs = value;
            }
            get
            {
                return _obs;
            }
        }

        public String ativo
        {
            set
            {
                _ativo = value;
            }
            get
            {
                return _ativo;
            }
        }

        public String prazo
        {
            set
            {
                _prazo = value;
            }
            get
            {
                return _prazo;
            }
        }

        public String tipoMercado
        {
            set
            {
                _tipoMercado = value;
            }
            get
            {
                return _tipoMercado;
            }
        }

        public String tipoOperacao
        {
            set
            {
                _tipoOperacao = value;
            }
            get
            {
                return _tipoOperacao;
            }
        }
    }

    public class NotaCorretagemBolsaPDF
    {
        private DateTime _dataPregao;
        private int _codigoBovespa;
        private int _codigoCliente;
        private List<DetalheNotaBolsaPDF> _listaDetalheNotaBolsaPDF;
        private Decimal _corretagem;
        private Decimal _iss;
        private Decimal _taxaLiquidacao;
        private Decimal _registroCBLC;
        private Decimal _registroBolsa;
        private Decimal _emolumento;
        private Decimal _irDayTrade;
        private bool _indicaDT;

        public Decimal irDayTrade
        {
            set
            {
                _irDayTrade = value;
            }
            get
            {
                return _irDayTrade;
            }
        }

        public Decimal emolumento
        {
            set
            {
                _emolumento = value;
            }
            get
            {
                return _emolumento;
            }
        }

        public Decimal registroBolsa
        {
            set
            {
                _registroBolsa = value;
            }
            get
            {
                return _registroBolsa;
            }
        }

        public Decimal registroCBLC
        {
            set
            {
                _registroCBLC = value;
            }
            get
            {
                return _registroCBLC;
            }
        }

        public Decimal taxaLiquidacao
        {
            set
            {
                _taxaLiquidacao = value;
            }
            get
            {
                return _taxaLiquidacao;
            }
        }

        public Decimal iss
        {
            set
            {
                _iss = value;
            }
            get
            {
                return _iss;
            }
        }

        public Decimal corretagem
        {
            set
            {
                _corretagem = value;
            }
            get
            {
                return _corretagem;
            }
        }

        public List<DetalheNotaBolsaPDF> listaDetalheNotaBolsaPDF
        {
            set
            {
                _listaDetalheNotaBolsaPDF = value;
            }
            get
            {
                return _listaDetalheNotaBolsaPDF;
            }
        }

        public int codigoCliente
        {
            set
            {
                _codigoCliente = value;
            }
            get
            {
                return _codigoCliente;
            }
        }

        public int codigoBovespa
        {
            set
            {
                _codigoBovespa = value;
            }
            get
            {
                return _codigoBovespa;
            }
        }

        public DateTime dataPregao
        {
            set
            {
                _dataPregao = value;
            }
            get
            {
                return _dataPregao;
            }
        }

        public bool indicaDT
        {
            set
            {
                _indicaDT = value;
            }
            get
            {
                return _indicaDT;
            }
        }
    }
}
