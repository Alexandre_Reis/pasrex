using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class GVDatCollection {
        private List<GVDat> collectionGVDat;

        public List<GVDat> CollectionGVDat {
            get { return collectionGVDat; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public GVDatCollection() {
            this.collectionGVDat = new List<GVDat>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="negs">Elemento a inserir na lista de GVDats</param>
        public void Add(GVDat gVDat) {
            collectionGVDat.Add(gVDat);
        }
    }
}
