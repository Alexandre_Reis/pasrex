using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class ConlCollection {
        private List<Conl> collectionConl;

        public List<Conl> CollectionConl {
            get { return collectionConl; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConlCollection() {
            this.collectionConl = new List<Conl>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de ConLs</param>
        public void Add(Conl conl) {
            collectionConl.Add(conl);
        }
    }
}
