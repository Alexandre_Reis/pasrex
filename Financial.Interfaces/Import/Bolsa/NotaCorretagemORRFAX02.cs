﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Reflection;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class NotaCorretagemORRFAX02 : NotaCorretagem
    {

        const string TOKEN_CODIGO = "Código: ";
        const string TOKEN_TAXASOPERACIONAIS = "TAXAS OPERACIONAIS";
        const string TOKEN_TOTALCORRETAGEM = "CORRETAGEM";
        const string TOKEN_IRSEMDAYTRADE = "IR S/ DAY-TRADE";
        const string TOKEN_IRSEMOPERACOES = "IR S/ OPERAÇÕES";

        #region Estruturas auxiliares para fazer a leitura do XML
        [XmlRoot("XML")]
        public class XMLCabec {
            [XmlElement("CORPO")]
            public CorpoCabec corpoCabec;
        }

        public class CorpoCabec {
            [XmlElement("DETALHE")]
            public DetalheCabec detalheCabec;
        }

        public class DetalheCabec {
            [XmlElement("NM_PARA")]
            public String nmPara;

            [XmlElement("NM_EMPPARA")]
            public String nmEmpPara;

            [XmlElement("FAX_PARA")]
            public String faxEmpPara;

            [XmlElement("NM_DE")]
            public String nmDe;

            [XmlElement("NM_EMPRESA")]
            public String nmEmpresa;

            [XmlElement("TEL_EMPRESA")]
            public String telEmpresa;

            [XmlElement("FAX_EMPRESA")]
            public String faxEmpresa;

            [XmlElement("CURRENT_DATE")]
            public String currentDate;

            [XmlElement("DT_PREGAO")]
            public String dtPregao;

            [XmlElement("NM_CLIENTE")]
            public String nmCliente;

            [XmlElement("CD_CVM")]
            public String cdCVM;

            [XmlElement("CD_BOVESPACODE")]
            public String cdBOVESPACode;

            [XmlElement("DS_OBSERVACAO")]
            public String dsObservacao;

            [XmlElement("VL_DOLAR")]
            public String vlDolar;

            [XmlElement("sNomeLogoHTML")]
            public String sNomeLogoHTML;

        }

        [XmlRoot("XML")]
        public class XMLDetalhe {
            [XmlElement("CORPO")]
            public CorpoDetalhe corpoDetalhe;
        }

        public class CorpoDetalhe {
            public CorpoDetalhe() {
                detalheList = new List<Detalhe>();
            }

            [XmlElement("DETALHE")]
            public List<Detalhe> detalheList;
        }

        public class Detalhe {
            [XmlElement("DS_TIT1")]
            public String dsTit1;

            [XmlElement("DS_TIT2")]
            public String dsTit2;

            [XmlElement("DS_TIT3")]
            public String dsTit3;

            [XmlElement("DS_TIT4")]
            public String dsTit4;

            [XmlElement("DS_TIT5")]
            public String dsTit5;

            [XmlElement("DS_TIT6")]
            public String dsTit6;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataNotaCorretagem"></param>
        /// <param name="arquivoHtml">Conteudo do Arquivo HTML a processar</param>
        /// <returns></returns>
        public ResumoOperacoes ProcessNotaCorretagem(DateTime dataNotaCorretagem, InfoArquivoHTML infoArquivoHTML) {
            string[] TOKEN_TOTAIS = new string[4] { TOKEN_TAXASOPERACIONAIS, TOKEN_TOTALCORRETAGEM, TOKEN_IRSEMDAYTRADE, TOKEN_IRSEMOPERACOES };
            string[] TOKEN_TOTAIS_PROPERTIES = new string[4] { "TaxasOperacionais", "TotalCorretagem", "IrSemDayTrade", "IrSemOperacoes" };

            Regex regex = new Regex("(<xml\\b[^>]*>.*?</xml>)", RegexOptions.IgnoreCase);
            Match match = regex.Match(infoArquivoHTML.conteudo);

            #region Parsing do Cabeçalho (tudo que vem no HTML antes da tabela).
            string xml = match.Value;

            XmlSerializer serializer = new XmlSerializer(typeof(XMLCabec));

            StringReader stringReader = new StringReader(xml);
            XMLCabec xmlCabec = (XMLCabec)serializer.Deserialize(stringReader);
            #endregion

            xml = match.NextMatch().Value;
            serializer = new XmlSerializer(typeof(XMLDetalhe));
            stringReader = new StringReader(xml);

            XMLDetalhe xmlDetalhe = (XMLDetalhe)serializer.Deserialize(stringReader);

            ResumoOperacoes resumoOperacoes = new ResumoOperacoes();
            
            resumoOperacoes.DataPregao = DateTime.ParseExact(xmlCabec.corpoCabec.detalheCabec.dtPregao, "dd/MM/yyyy", null);
            //
            resumoOperacoes.AgenteMercado = (string)xmlCabec.corpoCabec.detalheCabec.nmEmpresa.Trim();

            //Loop por cada uma das linhas da tabela de detalhes
            int j = 0;
            foreach (Detalhe linha in xmlDetalhe.corpoDetalhe.detalheList) {
                j++;
                //Caso mais comum é ser uma linha de operação - Critério de identificação: Coluna 6 preenchida
                if (linha.dsTit6 != null) {

                    if (linha.dsTit2.Trim() != "Soma") { // Exclui coluna de Somatório
                        Operacao operacao = new Operacao();
                        operacao.Ativo = linha.dsTit2.Trim();
                        operacao.Quantidade = Int32.Parse(linha.dsTit3, NumberStyles.AllowThousands);
                        operacao.Preco = Decimal.Parse(linha.dsTit4);
                        Decimal valor = Decimal.Parse(linha.dsTit5);
                        operacao.Valor = Math.Abs(valor);
                        operacao.Corretagem = Decimal.Parse(linha.dsTit6);
                        operacao.Tipo = valor > 0 ? TIPO_VENDA : TIPO_COMPRA;
                        resumoOperacoes.Operacoes.Add(operacao);
                    }
                }
                //Testar se é um dos totais procurados
                else if (linha.dsTit3 != null && linha.dsTit4 != null && linha.dsTit1 == null) {
                    Type resumoOperacoesType = resumoOperacoes.GetType();
                    int i = 0;
                    foreach (string token in TOKEN_TOTAIS) {
                        if (linha.dsTit3 == token && !String.IsNullOrEmpty(linha.dsTit4)) {
                            PropertyInfo property = resumoOperacoesType.GetProperty(TOKEN_TOTAIS_PROPERTIES[i]);
                            property.SetValue(resumoOperacoes, Decimal.Parse(linha.dsTit4), null);
                        }
                        i++;
                    }
                }
                
                //Testar se é código 
                else if (linha.dsTit1 != null && linha.dsTit1.StartsWith(TOKEN_CODIGO)) {
                    resumoOperacoes.Codigo = linha.dsTit1.Replace(TOKEN_CODIGO, "");
                }
            }

            return resumoOperacoes;
        }
    }
}