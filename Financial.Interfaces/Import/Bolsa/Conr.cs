﻿using System;
using System.Text;
using Financial.Interfaces.Import.Bolsa.Enums;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Conr {
        // Informação sobre o tipo registro da linha do arquivo Conr
        private string tipoRegistro;
        
        // bool indicando se Conr deve ser salvo na colecao de Conrs
        // Somente será salvo o Tipo de Registro do Conr que for necessario
        private bool conrProcessado;

        #region Properties dos Valores do Registro00 - Header
        
        int? codigoAgente; // Campo 2.2 - Codigo do usuario
        DateTime? dataMovimento; // Campo 07
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Identificação do Contrato Registrado

        DateTime? dataRegistro; // Campo 03

        public DateTime? DataRegistro {
            get { return dataRegistro; }
            set { dataRegistro = value; }
        }
        DateTime? dataVencimento; // Campo 04

        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        int? codigoCliente; // Campo 07

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }
        
        /* Numero do negocio que gerou o contrato */
        int? numeroContrato; // Campo 11

        public int? NumeroContrato {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        int? numeroNegocio; // Campo 13

        public int? NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        Int64? quantidade; // Campo 19

        public Int64? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        decimal? pu; // Campo 21

        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        decimal? valor; // Campo 22

        public decimal? Valor {
            get { return valor; }
            set { valor = value; }
        }

        /* 'C' = Comprador  'V' = Vendedor */
        char? tipo; // Campo 25

        public char? Tipo {
            get { return tipo; }
            set { tipo = value; }
        }

        string cdAtivoBolsa; // Campo 29

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        /* Termo normal, Termo em dolar, Termo em pontos, Termo flexivel */
        int? tipoContrato; // Campo 30

        public int? TipoContrato {
            get { return tipoContrato; }
            set { tipoContrato = value; }
        }

        /* Dolar, TJLP, Moeda corrente (Real) */
        int? indicadorCorrecaoContrato; // Campo 31

        public int? IndicadorCorrecaoContrato {
            get { return indicadorCorrecaoContrato; }
            set { indicadorCorrecaoContrato = value; }
        }

        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Conr devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.conrProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            this.dataRegistro = null;
            this.dataVencimento = null;
            this.codigoCliente = null;
            this.numeroContrato = null;
            this.numeroNegocio = null;
            this.quantidade = null;
            this.pu = null;
            this.valor = null;
            this.tipo = null;
            this.cdAtivoBolsa = null;
            this.tipoContrato = null;
            this.indicadorCorrecaoContrato = null;
            //
        }

        /// <summary>
        ///  Indica se Conr possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return conrProcessado;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="ArquivoConrNaoEncontradoException">Se arquivo Conr não Encontrado</exception>
        /// <exception cref="ProcessaConrException">Se ocorrer Erro no Processamento do Arquivo</exception> 
        public ConrCollection ProcessaConr(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoConrNaoEncontradoException("Arquivo CONR " + nomeArquivoCompleto + " não encontrado.");
            }
                               
            ConrCollection conrCollection = new ConrCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataConr(linha, data);
                            // Somente é salvo Conr que possui dados relevantes
                            Conr conr = (Conr)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                conrCollection.Add(conr);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaConrException(e.Message);
            }

            return conrCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="ProcessaConrException">Se ocorrer Erro no Processamento do Arquivo</exception> 
        public ConrCollection ProcessaConr(StreamReader sr, DateTime data)
        {
            ConrCollection conrCollection = new ConrCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataConr(linha, data);
                        // Somente é salvo Conr que possui dados relevantes
                        Conr conr = (Conr)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            conrCollection.Add(conr);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaConrException(e.Message);
            }

            return conrCollection;
        }

        /// <summary>
        /// A partir de um Arquivo Conr Existente Gera um novo Arquivo Conr somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoConr">Caminho Completo do Arquivo Conr</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Conr será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Conr</param>
        /// <returns>true se novo arquivo Conr foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoConrNaoEncontradoException">Se Arquivo Conr de entrada não existe</exception>
        public bool GeraArquivoConr(string nomeArquivoCompletoConr, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoConr)) {
                throw new ArquivoConrNaoEncontradoException("Arquivo Conr não Encontrado: " + nomeArquivoCompletoConr);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo
            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "conr_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoConr)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se dado pertence a algum dos clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Conr pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaConr">linha do arquivo Conr</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Conr pertence a algum dos Clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaConr, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaConr.Substring(0, 2)) {
                case EstruturaArquivoCONR.Header:
                case EstruturaArquivoCONR.Trailer:
                    break;
                case EstruturaArquivoCONR.IdentificacaoContratoRegistrado:
                    int codigoCliente = Convert.ToInt32(linhaConr.Substring(32, 7)); // Campo 07
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// <exception cref="ArgumentException"> Se tipoRegistro não está dentro dos valores possiveis do enum EstruturaArquivoConr</exception>
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoConr");

            List<string> valoresPossiveis = EstruturaArquivoCONR.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo CONR
            switch (tipoRegistro) {
                case EstruturaArquivoCONR.Header:
                    this.tipoRegistro = EstruturaArquivoCONR.Header;
                    break;
                case EstruturaArquivoCONR.IdentificacaoContratoRegistrado:
                    this.tipoRegistro = EstruturaArquivoCONR.IdentificacaoContratoRegistrado;
                    break;
                case EstruturaArquivoCONR.Trailer:
                    this.tipoRegistro = EstruturaArquivoCONR.Trailer;
                    break;
            }
            #endregion
        }

        
        /// <summary>
        /// Salva os atributos do arquivo Conr de acordo com o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataConr(string linha, DateTime data) {
            // Limpa os valores do Conr
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCONR.Header) {
                //
                this.codigoAgente = Convert.ToInt32( linha.Substring(6, 4) ); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(39, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoConrIncorretoException("Arquivo CONR com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                    
                }

                // Indica que Conr deve ser salvo                
                this.conrProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCONR.IdentificacaoContratoRegistrado) {
                #region IdentificacaoContratoRegistrado
                // Formato = "AAAAMMDD"
                string dataRegistro = linha.Substring(10, 8); // Campo 03
                int dia = Convert.ToInt32(dataRegistro.Substring(6, 2));
                int mes = Convert.ToInt32(dataRegistro.Substring(4, 2));
                int ano = Convert.ToInt32(dataRegistro.Substring(0, 4));
                this.dataRegistro = new DateTime(ano, mes, dia);
                //                
                // Formato = "AAAAMMDD"
                string dataVencimento = linha.Substring(18, 8); // Campo 04
                dia = Convert.ToInt32(dataVencimento.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimento.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimento.Substring(0, 4));
                this.dataVencimento = new DateTime(ano, mes, dia);
                //                
                this.codigoCliente = Convert.ToInt32(linha.Substring(32, 7)); // Campo 07
                this.numeroContrato = Convert.ToInt32(linha.Substring(46, 9)); // Campo 11
                this.numeroNegocio = Convert.ToInt32(linha.Substring(56, 7)); // Campo 13
                this.quantidade = Convert.ToInt64(linha.Substring(106, 15)); // Campo 19
                //
                int puInteiro = Convert.ToInt32(linha.Substring(136, 11)); // Campo 21
                int puFracionario = Convert.ToInt32(linha.Substring(147, 2));
                decimal puFracionarioDecimal = puFracionario / 100M;
                this.pu = puInteiro + puFracionarioDecimal;
                //
                int valorInteiro = Convert.ToInt32(linha.Substring(149, 11)); // Campo 22
                int valorFracionario = Convert.ToInt32(linha.Substring(160, 2));
                decimal valorFracionarioDecimal = valorFracionario / 100M;
                this.valor = valorInteiro + valorFracionarioDecimal;
                //
                /* 'C' - Comprador, 'V'- Vendedor */
                this.tipo = Convert.ToChar(linha.Substring(164, 1)); // Campo 25
                //                
                this.cdAtivoBolsa = linha.Substring(187, 11).Trim(); // Campo 29
                /* Termo normal, Termo em dolar, Termo em pontos, Termo flexivel */
                this.tipoContrato = Convert.ToInt32(linha.Substring(199, 5)); // Campo 30
                /* Dolar, TJLP, Moeda corrente (Real) */
                this.indicadorCorrecaoContrato = Convert.ToInt32(linha.Substring(204, 5)); // Campo 31
                #endregion

                // Indica que Conr deve ser salvo
                this.conrProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCONR.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoSaldo</returns>
        public bool IsTipoRegistroIdentificacaoContratoRegistrado() {
            return this.tipoRegistro == EstruturaArquivoCONR.IdentificacaoContratoRegistrado;
        }
    }
}