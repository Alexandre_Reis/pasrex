﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Reflection;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class NotaCorretagem {

        protected const char TIPO_VENDA = 'V';
        protected const char TIPO_COMPRA = 'C';

        #region Composicao NotaCorretagem
        public class ResumoOperacoes {
            //
            private string codigo;
            private DateTime dataPregao;
            private decimal taxasOperacionais;
            private decimal totalCorretagem;
            private decimal irSemDayTrade;
            private decimal irSemOperacoes;
            private string agenteMercado;
            private int codigoAgenteMercado;
            private int tipoNotaCorretagem;

            private List<Operacao> operacoes = new List<Operacao>();

            public List<Operacao> Operacoes {
                get { return operacoes; }
                set { operacoes = value; }
            }

            public string Codigo {
                get { return codigo; }
                set { codigo = value; }
            }

            public string AgenteMercado {
                get { return agenteMercado; }
                set { agenteMercado = value; }
            }

            public int CodigoAgenteMercado
            {
                get { return codigoAgenteMercado; }
                set { codigoAgenteMercado = value; }
            }

            public DateTime DataPregao {
                get { return dataPregao; }
                set { dataPregao = value; }
            }

            public int TipoNotaCorretagem
            {
                get { return tipoNotaCorretagem; }
                set { tipoNotaCorretagem = value; }
            }

            public decimal TaxasOperacionais {
                get { return taxasOperacionais; }
                set { taxasOperacionais = value; }
            }

            public decimal TotalCorretagem {
                get { return totalCorretagem; }
                set { totalCorretagem = value; }
            }

            public decimal IrSemDayTrade {
                get { return irSemDayTrade; }
                set { irSemDayTrade = value; }
            }

            public decimal IrSemOperacoes {
                get { return irSemOperacoes; }
                set { irSemOperacoes = value; }
            }
            //
        }

        public class Operacao {
            private char tipo; //C=Compra, V=Venda
            private string ativo;
            private string ativoDescricao;
            private string ativoEspecificacao;
            private int quantidade;
            private int prazo;
            private decimal preco;
            private decimal corretagem;
            private decimal valor;
            private string tipoMercado;

            public char Tipo {
                get { return tipo; }
                set { tipo = value; }
            }

            public string TipoMercado
            {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            public string Ativo {
                get { return ativo; }
                set { ativo = value; }
            }

            public string AtivoDescricao
            {
                get { return ativoDescricao; }
                set { ativoDescricao = value; }
            }

            public string AtivoEspecificacao
            {
                get { return ativoEspecificacao; }
                set { ativoEspecificacao = value; }
            }

            public int Quantidade {
                get { return quantidade; }
                set { quantidade = value; }
            }

            public int Prazo
            {
                get { return prazo; }
                set { prazo = value; }
            }

            public decimal Preco {
                get { return preco; }
                set { preco = value; }
            }

            public decimal Corretagem {
                get { return corretagem; }
                set { corretagem = value; }
            }

            public decimal Valor {
                get { return valor; }
                set { valor = value; }
            }

            public bool IsTipoMercadoExercicioCompra()
            {
                return this.TipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns>bool indicando se Tipo Mercado é ExercicioOpcao Venda</returns>
            public bool IsTipoMercadoExercicioVenda()
            {
                return this.TipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda;
            }

            public bool IsTipoMercadoTermo()
            {
                return this.TipoMercado == TipoDoMercadoNotaCorretagem.Termo;
            }
        }
        #endregion

        /// <summary>
        /// Processa o arquivo HTML em uma string
        /// </summary>
        /// <param name="dataNotaCorretagem"></param>
        /// <param name="listaConteudoArquivoHtml">Conteudo das notas de Corretagem</param>
        /// <returns>Lista com as Informação das notas de Corretagem</returns>
        /// <exception cref="Exception">Se houve algum erro de leitura do html devido a formato inválido</exception> 
        public List<ResumoOperacoes> ProcessNotaCorretagem(DateTime dataReferencia, List<InfoArquivoHTML> infoArquivosHTML) {
            List<ResumoOperacoes> retorno = new List<ResumoOperacoes>();
            try {
                foreach (InfoArquivoHTML infoArquivoHTML in infoArquivosHTML) {
                    ResumoOperacoes r = this.ProcessNotaCorretagem(dataReferencia, infoArquivoHTML);
                    retorno.Add(r);
                }
            }
            catch (Exception e) {
                throw new Exception("Nota de Corretagem não processada. Formato Incorreto: " + e.Message);
            }

            return retorno;
        }

        private ResumoOperacoes ProcessNotaCorretagem(DateTime dataReferencia, InfoArquivoHTML infoArquivoHTML)
        {
            ResumoOperacoes resumoOperacoes = new ResumoOperacoes();

            //Corrigir perolas do arquivo .mht
            if (infoArquivoHTML.indicaMht)
            {
                infoArquivoHTML.conteudo = this.CorrigeArquivoMht(infoArquivoHTML.conteudo);
            }

            string fileName = infoArquivoHTML.fileInfo.Name.ToLower();
            if (fileName.Contains("orrfax02"))
            {
                resumoOperacoes = new NotaCorretagemORRFAX02().ProcessNotaCorretagem(dataReferencia, infoArquivoHTML);
                resumoOperacoes.TipoNotaCorretagem = (int)TipoNotaCorretagem.NotaHTML_ORRFAX02;
            }
            else if (fileName.Contains("orrlnota"))
            {

                resumoOperacoes = new NotaCorretagemORRLNOTA().ProcessNotaCorretagem(dataReferencia, infoArquivoHTML);

                if (fileName.Contains("_fut"))
                {
                    resumoOperacoes.TipoNotaCorretagem = (int)TipoNotaCorretagem.NotaHTML_ORRLNOTA_FUT;
                }
                else if (fileName.Contains("_nor"))
                {
                    resumoOperacoes.TipoNotaCorretagem = (int)TipoNotaCorretagem.NotaHTML_ORRLNOTA_NOR;
                }
                else
                {
                    throw new Exception("Nota de Corretagem não processada. Formato ORRLNOTA deve conter no nome do arquivo o sufixo do tipo de mercado operado (ex. _FUT, _NOR");
                }
            }
            else
            {
                throw new Exception("Nota de Corretagem não processada. Formato não suportado: " + fileName);
            }

            return resumoOperacoes;
        }

        public string CorrigeArquivoMht(string conteudoArquivoMht)
        {
            /*
             * //Pular 11 linhas do cabecalho MHT
                for (int i = 0; i < 11; i++)
                {
                    string removido = srReader.ReadLine();
                }
                
             * */
            conteudoArquivoMht = conteudoArquivoMht.Replace("3D", "'");
            conteudoArquivoMht = conteudoArquivoMht.Replace("=\r\n", "");
            conteudoArquivoMht = conteudoArquivoMht.Replace("ID='xmlCabec", "id='xmlCabec'");
            conteudoArquivoMht = conteudoArquivoMht.Replace("id='xmlCabec", "id='xmlCabec'");
            //
            conteudoArquivoMht = conteudoArquivoMht.Replace("ID='xmlDetalhe", "id='xmlDetalhe'");
            conteudoArquivoMht = conteudoArquivoMht.Replace("id='xmlDetalhe", "id='xmlDetalhe'");
            //
            conteudoArquivoMht = conteudoArquivoMht.Replace("=F3", "ó'");
            conteudoArquivoMht = conteudoArquivoMht.Replace("Có'digo:", "Código:");

            return conteudoArquivoMht;
        }

        public class InfoArquivoHTML
        {
            public FileInfo fileInfo;
            public string conteudo;
            public bool indicaMht;
            public InfoArquivoHTML(FileInfo fileInfo)
            {
                this.fileInfo = fileInfo;
                this.indicaMht = fileInfo.Extension.ToLower() == ".mht";
            }
        }
    }
}