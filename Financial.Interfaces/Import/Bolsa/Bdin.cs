﻿using System;
using System.Text;
using Financial.Interfaces.Properties;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Bdin
    {

        // Informação sobre o tipo registro da linha do arquivo BDIN
        private string tipoRegistro;

        // bool indicando se bdin deve ser salvo na colecao de Bdins
        // Somente será salvo o Tipo de Registro do Bdin que for necessario
        private bool bdinProcessado;

        #region Properties dos Valores do Registro01 - Resumo Diario dos Indices

        int? identificacaoIndice; // Campo 02
        int? indiceAbertura; // Campo 04
        int? indiceMedio; // Campo 07
        int? indiceFechamento; // Campo 13

        /// <summary>
        /// 
        /// </summary>
        public int? IdentificacaoIndice
        {
            get { return identificacaoIndice; }
            set { identificacaoIndice = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? IndiceAbertura
        {
            get { return indiceAbertura; }
            set { indiceAbertura = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? IndiceMedio
        {
            get { return indiceMedio; }
            set { indiceMedio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? IndiceFechamento
        {
            get { return indiceFechamento; }
            set { indiceFechamento = value; }
        }

        #endregion

        #region Properties dos Valores do Registro02 - Resumo Diario de Negociações por Papel - Mercado

        string codigoBDI; // Campo 02
        string codigoNegociacao; // Campo 07
        int? tipoMercado; // Campo 08 
        decimal? puAbertura; // Campo 11
        decimal? puMedio; // Campo 14
        decimal? puFechamento; // Campo 15

        /// <summary>
        /// 
        /// </summary>
        public string CodigoBDI
        {
            get { return codigoBDI; }
            set { codigoBDI = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CodigoNegociacao
        {
            get { return codigoNegociacao; }
            set { codigoNegociacao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? TipoMercado
        {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? PuAbertura
        {
            get { return puAbertura; }
            set { puAbertura = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? PuMedio
        {
            get { return puMedio; }
            set { puMedio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? PuFechamento
        {
            get { return puFechamento; }
            set { puFechamento = value; }
        }

        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser inicializado
        /// Somente as variaveis que armazenam informacoes do arquivo Bdin devem ser inicializadas
        /// </summary>
        private void Initialize()
        {
            this.bdinProcessado = false;
            this.identificacaoIndice = null;
            this.indiceAbertura = null;
            this.indiceMedio = null;
            this.indiceFechamento = null;
            this.codigoBDI = "";
            this.codigoNegociacao = "";
            this.tipoMercado = null;
            this.puAbertura = null;
            this.PuMedio = null;
            this.puFechamento = null;
        }

        /// <summary>
        ///  Indica se Bdin possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue()
        {
            return bdinProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Bdin será procurado</param>
        /// throws ArquivoBDinNaoEncontradoException
        public BdinCollection ProcessaBdin(DateTime data, string path)
        {

            // Verifica se arquivo está no diretorio
            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (data.Month < 10)
            {
                mesString = "0" + mesString;
            }
            if (data.Day < 10)
            {
                diaString = "0" + diaString;
            }


            StringBuilder nomeArquivo = new StringBuilder();

            nomeArquivo.Append(path)
             .Append("Bdin").Append(diaString).Append(mesString).Append(anoString).Append(".txt");

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadBdin(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoBDinNaoEncontradoException("Arquivo Bdin: " + nomeArquivo.ToString() + " não encontrado");
                }
            }

            BdinCollection bdinCollection = new BdinCollection();
            int i = 1;
            string linha = "";
            try
            {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivo.ToString()))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        if (!String.IsNullOrEmpty(linha))
                        {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataBdin(linha);
                            // Somente é salvo Bdin que possui dados relevantes
                            Bdin bdin = (Bdin)Utilitario.Clone(this);
                            if (this.HasValue())
                            {
                                bdinCollection.Add(bdin);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Bdin com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);

                //if (log.IsDebugEnabled) {
                //    log.Debug(mensagem);
                //}
                Console.WriteLine(e.Message);
                Console.WriteLine(mensagem);
                throw new ProcessaBDinException(e.Message);
            }

            return bdinCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoBDIN
        private void GetTipoRegistro(string tipoRegistro)
        {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoBDIN");

            List<string> valoresPossiveis = EstruturaArquivoBDIN.Values();
            if (!valoresPossiveis.Contains(tipoRegistro))
            {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo BDIN
            switch (tipoRegistro)
            {
                case EstruturaArquivoBDIN.Header:
                    this.tipoRegistro = EstruturaArquivoBDIN.Header;
                    break;
                case EstruturaArquivoBDIN.ResumoDiarioIndice:
                    this.tipoRegistro = EstruturaArquivoBDIN.ResumoDiarioIndice;
                    break;
                case EstruturaArquivoBDIN.ResumoDiarioNegociacaoPapelMercado:
                    this.tipoRegistro = EstruturaArquivoBDIN.ResumoDiarioNegociacaoPapelMercado;
                    break;
                case EstruturaArquivoBDIN.ResumoDiarioNegociacaoCodigo:
                    this.tipoRegistro = EstruturaArquivoBDIN.ResumoDiarioNegociacaoCodigo;
                    break;
                case EstruturaArquivoBDIN.MaioresOscilacoesMercadoVista:
                    this.tipoRegistro = EstruturaArquivoBDIN.MaioresOscilacoesMercadoVista;
                    break;
                case EstruturaArquivoBDIN.MaioresOscilacoesIbovespa:
                    this.tipoRegistro = EstruturaArquivoBDIN.MaioresOscilacoesIbovespa;
                    break;
                case EstruturaArquivoBDIN.MaisNegociadasMercadoVista:
                    this.tipoRegistro = EstruturaArquivoBDIN.MaisNegociadasMercadoVista;
                    break;
                case EstruturaArquivoBDIN.MaisNegociadas:
                    this.tipoRegistro = EstruturaArquivoBDIN.MaisNegociadas;
                    break;
                case EstruturaArquivoBDIN.IOPV:
                    this.tipoRegistro = EstruturaArquivoBDIN.IOPV;
                    break;
                case EstruturaArquivoBDIN.BDRNaoPatrocinado:
                    this.tipoRegistro = EstruturaArquivoBDIN.BDRNaoPatrocinado;
                    break;
                case EstruturaArquivoBDIN.Trailer:
                    this.tipoRegistro = EstruturaArquivoBDIN.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo BDIn de acordo com o tipo
        private void TrataBdin(string linha)
        {
            // Limpa os valores do Bdin
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoBDIN.Header) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice)
            {
                this.identificacaoIndice = Convert.ToInt32(linha.Substring(2, 2)); // Campo 02
                this.indiceAbertura = Convert.ToInt32(linha.Substring(34, 6)); // Campo 04
                this.indiceMedio = Convert.ToInt32(linha.Substring(52, 6)); // Campo 07
                this.indiceFechamento = Convert.ToInt32(linha.Substring(92, 6)); // Campo 13
                // Indica que Bdin deve ser salvo
                this.bdinProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioNegociacaoPapelMercado)
            {
                this.codigoBDI = linha.Substring(2, 2); // Campo 02
                this.codigoNegociacao = linha.Substring(57, 12).Trim(); // Campo 07
                this.tipoMercado = Convert.ToInt32(linha.Substring(69, 3)); // Campo 08
                //
                int puAberturaInteiro = Convert.ToInt32(linha.Substring(90, 9)); // Campo 11                
                int puAberturaFracionario = Convert.ToInt32(linha.Substring(99, 2));
                decimal puAberturaFracionarioDecimal = puAberturaFracionario / 100M;
                this.puAbertura = puAberturaInteiro + puAberturaFracionarioDecimal;
                //
                int puMedioInteiro = Convert.ToInt32(linha.Substring(123, 9)); // Campo 14 
                int puMedioFracionario = Convert.ToInt32(linha.Substring(132, 2));
                decimal puMedioFracionarioDecimal = puMedioFracionario / 100M;
                this.puMedio = puMedioInteiro + puMedioFracionarioDecimal;
                //
                int puFechamentoInteiro = Convert.ToInt32(linha.Substring(134, 9)); // Campo 15                
                int puFechamentoFracionario = Convert.ToInt32(linha.Substring(143, 2));
                decimal puFechamentoFracionarioDecimal = puFechamentoFracionario / 100M;
                this.puFechamento = puFechamentoInteiro + puFechamentoFracionarioDecimal;

                // Indica que Bdin deve ser salvo
                this.bdinProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoBDIN.BDRNaoPatrocinado)
            {
                this.codigoBDI = "02"; //Não tem a coluna codigo BDI para BDR
                this.codigoNegociacao = linha.Substring(2, 12).Trim(); // Campo 07
                this.tipoMercado = 10; //Não tem a coluna tipo mercado para BDR
                //
                int puFechamentoInteiro = Convert.ToInt32(linha.Substring(39, 9)); // Campo 15                
                int puFechamentoFracionario = Convert.ToInt32(linha.Substring(48, 2));
                decimal puFechamentoFracionarioDecimal = puFechamentoFracionario / 100M;
                this.puFechamento = puFechamentoInteiro + puFechamentoFracionarioDecimal;
                //
                this.puAbertura = this.puFechamento;
                this.puMedio = this.puFechamento;
                //
                

                // Indica que Bdin deve ser salvo
                this.bdinProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioNegociacaoCodigo) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.MaioresOscilacoesMercadoVista) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.MaioresOscilacoesIbovespa) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.MaisNegociadasMercadoVista) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.MaisNegociadas) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.IOPV) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.BDRNaoPatrocinado) { }
            //
            else if (this.tipoRegistro == EstruturaArquivoBDIN.Trailer) { }
        }

        /* Baixa o arquivo Bdin da Internet
           Descompacta o arquivo Zip e renomeia o arquivo para a data correta
        */
        private bool DownloadBdin(DateTime data, string path)
        {

            #region Download

            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();

            string endereco = "";
            DateTime dataMudancaSite = new DateTime(2016, 02, 25);
            if (DateTime.Compare(data, dataMudancaSite) >= 0)
            {
                dataString.Append(mesString).Append(diaString);
                endereco = "http://bvmf.bmfbovespa.com.br/fechamento-pregao/bdi/bdi" + dataString.ToString() + ".zip";
            }
            else
            {
                dataMudancaSite = new DateTime(2009, 11, 16);
                if (DateTime.Compare(data, dataMudancaSite) >= 0)
                {
                    dataString.Append(mesString).Append(diaString);
                    endereco = "http://www.bmfbovespa.com.br/fechamento-pregao/bdi/bdi" + dataString.ToString() + ".zip";
                }
                else
                {
                    dataString.Append(diaString).Append(mesString);
                    endereco = "http://www.bovespa.com.br/bdi/bdi" + dataString.ToString() + ".zip";
                }
            }

            string nomeArquivoDestino = "BDin" + diaString + mesString + anoString + ".zip";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            #region Descompacta
            // Se foi feito o Download, descompacta o arquivo

            string pathArquivoNovo = pathArquivoDestino.Replace(".zip", ".txt");
            
            if (download)
            {
                bool descompacta = Utilitario.UnzipFile(nomeArquivoDestino, path);
                //

                #region Rename
                // Se foi feito o Unzip, renomeia o arquivo de BDIN para BDin+Data
                if (descompacta)
                {
                    StringBuilder nomeArquivoAntigo = new StringBuilder();
                    nomeArquivoAntigo.Append(path).Append("BDIN");
                    //
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), pathArquivoNovo);
                }
                #endregion
            }
            #endregion

            // Se arquivo Bdin+data.txt existe retorna true            
            return File.Exists(pathArquivoNovo);
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = 01 - Todos os Indices
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoRegistro = 01
        ///           false se TipoRegistro != 01
        /// </returns>
        public bool FilterBdinByIndice(Bdin bdin)
        {
            bool retorno = bdin.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é Ibovespa</returns>
        public bool IsIndiceIBOVESPA()
        {
            return this.identificacaoIndice == (int)IndicesBdin.IBOVESPA &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é IBRX_BRASIL</returns>
        public bool IsIndiceIBRX_BRASIL()
        {
            return this.identificacaoIndice == (int)IndicesBdin.IBRX_BRASIL &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é IBRX_50</returns>
        public bool IsIndiceIBRX_50()
        {
            return this.identificacaoIndice == (int)IndicesBdin.IBRX_50 &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é IBRA</returns>
        public bool IsIndiceIBRA()
        {
            return this.identificacaoIndice == (int)IndicesBdin.IBRASIL &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é IFIX</returns>
        public bool IsIndiceIFIX()
        {
            return this.identificacaoIndice == (int)IndicesBdin.INDFDOIMOB &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se indice é IDIVIDENDOS</returns>
        public bool IsIndiceIDIVIDENDOS()
        {
            return this.identificacaoIndice == (int)IndicesBdin.IDIVIDENDOS &&
                   this.tipoRegistro == EstruturaArquivoBDIN.ResumoDiarioIndice;
        }
    }
}
