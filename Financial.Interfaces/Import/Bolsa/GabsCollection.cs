using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class GabsCollection {
        private List<Gabs> collectionGabs;

        public List<Gabs> CollectionGabs
        {
            get { return collectionGabs; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public GabsCollection()
        {
            this.collectionGabs = new List<Gabs>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conr">Elemento a inserir na lista de Gabss</param>
        public void Add(Gabs gabs)
        {
            collectionGabs.Add(gabs);
        }
    }
}
