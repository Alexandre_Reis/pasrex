﻿using System;
using System.Text;
using System.IO;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using System.Collections.Generic;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Posr {
        // Informação sobre o tipo registro da linha do arquivo Posr
        private string tipoRegistro;

        // bool indicando se prod deve ser salvo na colecao de Posr
        // Somente será salvo o Tipo de Registro do Posr que for necessario
        private bool posrProcessado;

        #region Properties dos Valores do Registro00 - Header

        int? codigoAgente; // Campo 2.2
        DateTime? dataMovimento; // Campo 07

        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - IdentificacaoPosicaoRegistrada
        DateTime? dataVencimento; // Campo 02

        public DateTime? DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        int? codigoCliente; // Campo 05

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        string cdAtivoBolsa; // Campo 11

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }
        
        Int64? quantidade; // Campo 15

        public Int64? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        /* "L" = Lançador (posição vendida) / "T" Titular (posição comprada) */
        char? indicadorPosicao; // Campo 20

        public char? IndicadorPosicao {
            get { return indicadorPosicao; }
            set { indicadorPosicao = value; }
        }

        int? fatorCotacao; // Campo 21

        public int? FatorCotacao {
            get { return fatorCotacao; }
            set { fatorCotacao = value; }
        }

        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Posr devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.posrProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            this.codigoCliente = null;
            this.cdAtivoBolsa = null;
            this.quantidade = null;
            this.indicadorPosicao = null;
            this.fatorCotacao = null;
            this.dataVencimento = null;
            //
        }

        /// <summary>
        ///  Indica se Posr possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return posrProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// throws ArquivoPosrNaoEncontradoException
        public PosrCollection ProcessaPosr(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoPosrNaoEncontradoException("Arquivo POSR não encontrado: " + nomeArquivoCompleto);
            }

            PosrCollection posrCollection = new PosrCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataPosr(linha, data);
                            // Somente é salvo Posr que possui dados relevantes
                            Posr posr = (Posr)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                posrCollection.Add(posr);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Posr com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaPosrException(mensagem.ToString());
            }

            return posrCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// throws ArquivoPosrNaoEncontradoException
        public PosrCollection ProcessaPosr(StreamReader sr, DateTime data)
        {
            PosrCollection posrCollection = new PosrCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataPosr(linha, data);
                        // Somente é salvo Posr que possui dados relevantes
                        Posr posr = (Posr)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            posrCollection.Add(posr);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Posr com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaPosrException(e.Message);
            }

            return posrCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoPosr
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoPOSR");

            List<string> valoresPossiveis = EstruturaArquivoPOSR.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo POSR
            switch (tipoRegistro) {
                case EstruturaArquivoPOSR.Header:
                    this.tipoRegistro = EstruturaArquivoPOSR.Header;
                    break;
                case EstruturaArquivoPOSR.IdentificacaoPosicaoRegistrada:
                    this.tipoRegistro = EstruturaArquivoPOSR.IdentificacaoPosicaoRegistrada;
                    break;
                case EstruturaArquivoPOSR.Trailer:
                    this.tipoRegistro = EstruturaArquivoPOSR.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Posr de acordo com o tipo
        private void TrataPosr(string linha, DateTime data) {
            // Limpa os valores do Posr
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoPOSR.Header) {
                this.codigoAgente = Convert.ToInt32(linha.Substring(6, 4)); // Campo 2.2                                               
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(39, 8); // Campo 07
                int dia = Convert.ToInt32( dataMovimento.Substring(6, 2) );
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoPosrIncorretoException("Arquivo POSR com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                                        
                }

                // Indica que Posr deve ser salvo                
                this.posrProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoPOSR.IdentificacaoPosicaoRegistrada) {
                // Formato = "AAAAMMDD"
                string dataVencimento = linha.Substring(2, 8); // Campo 02
                int dia = Convert.ToInt32(dataVencimento.Substring(6, 2));
                int mes = Convert.ToInt32(dataVencimento.Substring(4, 2));
                int ano = Convert.ToInt32(dataVencimento.Substring(0, 4));
                //
                this.dataVencimento = new DateTime(ano, mes, dia);
                this.codigoCliente = Convert.ToInt32(linha.Substring(16, 7)); // Campo 05
                this.cdAtivoBolsa = linha.Substring(49, 12).Trim(); // Campo 12
                this.quantidade = Convert.ToInt64(linha.Substring(96, 15)); // Campo 15
                
                /* "L" = Lançador (posição vendida) / "T" Titular (posição comprada) */ 
                this.indicadorPosicao = Convert.ToChar(linha.Substring(157, 1).Trim()); // Campo 20
                this.fatorCotacao = Convert.ToInt32(linha.Substring(158, 7)); // Campo 21
                
                // Indica que Posr deve ser salvo
                this.posrProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoPOSR.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoPOSR.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroIdentificacaoPosicaoRegistrada() {
            return this.tipoRegistro == EstruturaArquivoPOSR.IdentificacaoPosicaoRegistrada;
        }        
    }
}
