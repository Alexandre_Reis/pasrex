using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class PapdCollection {
        private List<Papd> collectionPapd;

        public List<Papd> CollectionPapd {
            get { return collectionPapd; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public PapdCollection() {
            this.collectionPapd = new List<Papd>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prod">Elemento a inserir na lista de Papd</param>
        public void Add(Papd papd) {
            collectionPapd.Add(papd);
        }
    }
}
