using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class DbtcCollection {
        private List<Dbtc> collectionDbtc;

        public List<Dbtc> CollectionDbtc {
            get { return collectionDbtc; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public DbtcCollection() {
            this.collectionDbtc = new List<Dbtc>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de Dbtcs</param>
        public void Add(Dbtc dbtc) {
            collectionDbtc.Add(dbtc);
        }
    }
}
