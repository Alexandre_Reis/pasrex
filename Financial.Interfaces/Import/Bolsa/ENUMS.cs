﻿using System.Collections.Generic;

namespace Financial.Interfaces.Import.Bolsa.Enums {
    
    public enum IndicesBdin 
    {
        IBOVESPA = 01,
        IEELETRICA = 02,
        IVBX2 = 03,
        IBRX_BRASIL = 04,
        ITELECOM = 05,
        IBRX_50 = 06,
        IGOVERNANCA = 08,
        ISUSTENTABIL = 09,
        ITAG_ALONG = 10,
        INDUSTRIAL = 11,
        SMALLCAP = 12,
        MIDLARGECAP = 13,
        ICONSUMO = 14,
        IMOBILIARIO = 15,
        IFINANCEIRO = 16,
        ICO2 = 17,
        IGCTRAD = 18,
        UTILITIES = 19,
        IDIVIDENDOS = 20,
        IMATBASICOS = 21,
        IBRASIL = 22,
        BDRX = 23,
        INDFDOIMOB = 24,
        IGC_NM = 25
    }

    public enum IndiceCorrecaoContrato {
        Dolar = 00001,
        TJLP = 00002,
        MoedaCorrente = 09000
    }

    public static class TipoDoMercadoPesc {
        /* À VISTA */
        public const string Vista = "VIS";
        /* EXERCÍCIO DE OPÇÃO DE COMPRA */
        public const string ExercicioCompra = "EOC";
        /* EXERCÍCIO DE OPÇÃO DE VENDA */
        public const string ExercicioVenda = "EOV";
        /* LEILÃO DE TÍTULOS NÃO COTADOS */
        public const string LeilaoTitulosNaoCotados = "LNC";
        /* LEILÃO */
        public const string Leilao = "LEI";
        /* FRACIONÁRIO */
        public const string Fracionario = "FRA";
        /* TERMO */
        public const string Termo = "TER";
        /* OPÇÕES DE COMPRA */
        public const string OpcaoCompra = "OPC";
        /* OPÇÕES DE VENDA */
        public const string OpcaoVenda = "OPV"; 
    }

    public static class TipoDoMercadoNegs {
        /* À VISTA */
        public const string Vista = "VIS";
        /* EXERCÍCIO DE OPÇÃO DE COMPRA */
        public const string ExercicioCompra = "EOC";
        /* EXERCÍCIO DE OPÇÃO DE VENDA */
        public const string ExercicioVenda = "EOV";
        /* LEILÃO DE TÍTULOS NÃO COTADOS */
        public const string LeilaoTitulosNaoCotados = "LNC";
        /* LEILÃO */
        public const string Leilao = "LEI";
        /* FRACIONÁRIO */
        public const string Fracionario = "FRA";
        /* TERMO */
        public const string Termo = "TER";
        /* OPÇÕES DE COMPRA */
        public const string OpcaoCompra = "OPC";
        /* OPÇÕES DE VENDA */
        public const string OpcaoVenda = "OPV"; 
    }

    public static class TipoDoMercadoNotaCorretagem
    {
        /* À VISTA */
        public const string Vista = "VISTA";

        /* FRACIONARIO */
        public const string Fracionario = "FRACIONARIO";

        /* OPÇÕES DE COMPRA */
        public const string OpcaoCompra = "OPCAO DE COMPRA";
        
        /* OPÇÕES DE VENDA */
        public const string OpcaoVenda = "OPCAO DE VENDA";

        /* EXERCICIO OPÇÃO DE COMPRA */
        public const string ExercicioCompra = "EXERC OPC COMPRA";
        
        /* EXERCICIO OPÇÃO DE VENDA */
        public const string ExercicioVenda = "EXERC OPC VENDA";

        /* TERMO */
        public const string Termo = "TERMO";
    }

    public enum TipoNotaCorretagem
    {
        NotaHTML_ORRFAX02 = 1,
        NotaHTML_ORRLNOTA_FUT = 2,
        NotaHTML_ORRLNOTA_NOR = 3
    }

    public static class TipoDoProvento {
        public const int Dividendo = 10;
        public const int RestituicaoCapital = 11;
        public const int BonificacaoDinheiro = 12;
        public const int JurosSobreCapital = 13;
        public const int Rendimento = 14;
        public const int Juros = 16;
        public const int Amortizacao = 17;
        public const int Premio = 18;
        public const int Bonificacao = 20;
        public const int RestituicaoCapitalEmAcoes = 21;
        public const int RestituicaoCapitalComReducaoAcoes = 22;
        public const int Desdobramento = 30;
        public const int Grupamento = 40;
        public const int Subscricao = 50;
        public const int PrioridadeSubscricao = 51;
        public const int ExercicioSubscricao = 52;
        public const int SubscricaoComRenuncia = 53;
        public const int CreditoFracoesAcoes = 72;
        public const int RendimentoLiquido = 98;

        public static class Conversao {
            public const int Incorporacao = 60;
            public const int Fusao = 70;
            public const int Cisao = 80;
            public const int CisaoComReducaoQuantidade = 81;
            public const int Atualizacao = 90;            
        }

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Dividendo);
            valoresPossiveis.Add(RestituicaoCapital);
            valoresPossiveis.Add(BonificacaoDinheiro);
            valoresPossiveis.Add(JurosSobreCapital);
            valoresPossiveis.Add(Rendimento);
            valoresPossiveis.Add(Bonificacao);
            valoresPossiveis.Add(RestituicaoCapitalEmAcoes);
            valoresPossiveis.Add(RestituicaoCapitalComReducaoAcoes);
            valoresPossiveis.Add(Desdobramento);
            valoresPossiveis.Add(Grupamento);
            valoresPossiveis.Add(Subscricao);
            valoresPossiveis.Add(PrioridadeSubscricao);
            valoresPossiveis.Add(ExercicioSubscricao);
            valoresPossiveis.Add(SubscricaoComRenuncia);
            valoresPossiveis.Add(CreditoFracoesAcoes);
            valoresPossiveis.Add(RendimentoLiquido);
            valoresPossiveis.Add(Conversao.Incorporacao);
            valoresPossiveis.Add(Conversao.Fusao);
            valoresPossiveis.Add(Conversao.Cisao);
            valoresPossiveis.Add(Conversao.CisaoComReducaoQuantidade);
            valoresPossiveis.Add(Conversao.Atualizacao);
            return valoresPossiveis;
        }
    }
     
    public enum OpcaoIndice {
       MoedaCorrente = 0,
       CorrecaoTaxaDolar = 1,
       CorrecaoTJLP = 2,
       CorrecaoTR = 3,
       CorrecaoIPCR = 4,
       OpcoesTrocaSwoptions = 5,
       OpcoesPontosIndice = 6,
       CorrecaoTaxaDolarProtegido = 7,
       CorrecaoIGPMProtegido = 8,
       CorrecaoURV = 9
    }

    public enum IndicadorDoMercado {
        CustodiaInfungivel = 19,
        FundoInvestimentoDireitoCreditorio = 21,
        FundoInvestimentoParticipacao = 22,
        Precatorio = 12,
        CertificadoInvestimentoFundoImobilario = 4,
        CertificadoInvestimentoFundoAcoes = 5,
        CertificadoAudioVisual = 6,
        FINAM = 9,
        FINOR = 10,
        FISET = 11,
        FUNRES = 15
    }

    public static class EspecificacaoAtivo {
        public const string ON = "3";
        public const string PN = "4";
        public const string PNA = "5";
        public const string PNB = "6";
        public const string PNC = "7";
        public const string PND = "8";
        public const string EspecificacaoInvalida = "EspecificacaoInvalida";
    }

    public static class TipoAtivo {        
        public const string CRI = "CRI";
        public const string DBM = "DBM";
        public const string DBO = "DBO";
        public const string DBP = "DBP";
        public const string DBS = "DBS";
        public const string NPM = "NPM";
    }

    /// <summary>
    /// Hoje só temos mapeados Dividendos e Juros Capital.
    /// </summary>
    public enum CodigoLancamentoCMDF
    {
        Dividendo = 1856,
        JurosCapital = 1957
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoBDIN {
        public const string Header = "00";
        public const string ResumoDiarioIndice = "01";
        public const string ResumoDiarioNegociacaoPapelMercado = "02";
        public const string ResumoDiarioNegociacaoCodigo = "03";
        public const string MaioresOscilacoesMercadoVista = "04";
        public const string MaioresOscilacoesIbovespa = "05";
        public const string MaisNegociadasMercadoVista = "06";
        public const string MaisNegociadas = "07";
        public const string IOPV = "08"; // Novo Tipo Surgido em 28/11/2008 para acomodar indices SmallCaps
        public const string BDRNaoPatrocinado = "09"; // Novo Tipo Surgido em 07/04/2011 para acomodar BDRs nao patrocinados
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(ResumoDiarioIndice);
            valoresPossiveis.Add(ResumoDiarioNegociacaoPapelMercado);
            valoresPossiveis.Add(ResumoDiarioNegociacaoCodigo);
            valoresPossiveis.Add(MaioresOscilacoesMercadoVista);
            valoresPossiveis.Add(MaioresOscilacoesIbovespa);
            valoresPossiveis.Add(MaisNegociadasMercadoVista);
            valoresPossiveis.Add(MaisNegociadas);
            valoresPossiveis.Add(IOPV);
            valoresPossiveis.Add(BDRNaoPatrocinado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCONL {
        public const string Header = "00";
        public const string IdentificacaoContratoLiquidado = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(IdentificacaoContratoLiquidado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCONR {
        public const string Header = "00";
        public const string IdentificacaoContratoRegistrado = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(IdentificacaoContratoRegistrado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoDBTC {
        public const string Header = "00";
        public const string RelacaoContratoUsuariosDoadores = "01";
        public const string RelacaoContratoUsuariosTomadores = "02";        
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(RelacaoContratoUsuariosDoadores);
            valoresPossiveis.Add(RelacaoContratoUsuariosTomadores);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoDBTL {
        public const string Header = "00";
        public const string RelacaoLiquidacoesClienteContrato = "01";
        public const string RelacaoLancamentosTotalContratoCliente = "02";
        public const string RelacaoLancamentosDetalhadoContratoCliente = "03";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(RelacaoLiquidacoesClienteContrato);
            valoresPossiveis.Add(RelacaoLancamentosTotalContratoCliente);
            valoresPossiveis.Add(RelacaoLancamentosDetalhadoContratoCliente);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoPROD {
        public const string Header = "00";
        public const string DadosProventos = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DadosProventos);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCSGD {
        public const string Header = "00";
        public const string IdentificacaoCliente = "01";
        public const string IdentificacaoSaldo = "02";
        public const string IdentificacaoSaldoBloqueado = "03";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(IdentificacaoCliente);
            valoresPossiveis.Add(IdentificacaoSaldo);
            valoresPossiveis.Add(IdentificacaoSaldoBloqueado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCDIX {
        public const string Header = "00";
        public const string IdentificacaoCliente = "01";
        public const string IdentificacaoDividendo = "02";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(IdentificacaoCliente);
            valoresPossiveis.Add(IdentificacaoDividendo);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoPOSR {
        public const string Header = "00";
        public const string IdentificacaoPosicaoRegistrada = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(IdentificacaoPosicaoRegistrada);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoPAPD {
        public const string Header = "00";
        public const string DadosDosPapeis = "01";
        public const string CodigosPapeisOutrasPracasBolsas = "02";        
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DadosDosPapeis);
            valoresPossiveis.Add(CodigosPapeisOutrasPracasBolsas);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoNEGS {
        public const string Header = "00";
        public const string DetalheNegocioRealizado = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DetalheNegocioRealizado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCMDF
    {
        public const string Header = "00";
        public const string MovimentoDia = "01";
        public const string MovimentoFuturo = "02";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values()
        {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(MovimentoDia);
            valoresPossiveis.Add(MovimentoFuturo);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// GV.Dat, arquivo de deságios da CBLC
    /// </summary>
    public static class EstruturaArquivoGVDAT
    {
        public const string Header = "00";
        public const string DadosDosPapeis = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values()
        {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DadosDosPapeis);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }


    /// <summary>
    /// CMVD - Movimentação de Posições
    /// </summary>
    public static class EstruturaArquivoCMVD {
        public const string Header = "00";
        public const string IdentificacaoCliente = "01";
        public const string IdentificacaoMovimentacao = "02";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>(
                new string[] { Header, IdentificacaoCliente, IdentificacaoMovimentacao, Trailer }
            );

            return valoresPossiveis;
        }
    }

    /// <summary>
    /// PESC
    /// </summary>
    public static class EstruturaArquivoPESC {
        public const string Header = "00";
        public const string Especificacao = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>(
                new string[] { Header, Especificacao, Trailer }
            );

            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoCRCA
    {
        public const string Header = "00";
        public const string DetalheNegocioRealizado = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values()
        {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DetalheNegocioRealizado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoDesagio
    {
        public const string Header = "00";
        public const string DetalheContratoRegistrado = "01";
        public const string Trailer = "99";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values()
        {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(DetalheContratoRegistrado);
            valoresPossiveis.Add(Trailer);
            return valoresPossiveis;
        }
    }
}

