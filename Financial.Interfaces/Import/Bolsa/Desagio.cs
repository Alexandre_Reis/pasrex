﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Enums;
using System.Net;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.Bolsa
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Desagio
    {

        //Define se trata-se do arquivo GF-renda fixa ou da planilha de limites e desagios de acoes
        #region Header - Registro00
        
        /// <summary>
        /// 
        /// </summary>

        string tipoRegistro; // Campo 01

        public string TipoRegistro
        {
            get { return tipoRegistro; }
            set { tipoRegistro = value; }
        }

        DateTime? dataCriacaoArquivo; // Campo 02

        public DateTime? DataCriacaoArquivo
        {
            get { return dataCriacaoArquivo; }
            set { dataCriacaoArquivo = value; }
        }

        string nomeArquivo;

        public string NomeArquivo
        {
            get { return nomeArquivo; }
            set { nomeArquivo = value; }
        }

        #endregion

        #region Relação de Contratos Registrados - Registro01

        int? codigoSelic;

        public int? CodigoSelic
        {
            get { return codigoSelic; }
            set { codigoSelic = value; }
        }

        DateTime? dataVencimento;

        public DateTime? DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        decimal? fator;

        public decimal? Fator
        {
            get { return fator; }
            set { fator = value; }
        }

        string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        decimal? puReferenciaGarantia;

        public decimal? PuReferenciaGarantia
        {
            get { return puReferenciaGarantia; }
            set { puReferenciaGarantia = value; }
        }
        #endregion

        #region Trailer - Registro99

        int? numeroRegistros;

        public int? NumeroRegistros
        {
            get { return numeroRegistros; }
            set { numeroRegistros = value; }
        }
        
        #endregion

        #region Arquivo Excell

            string codigo;

            public string Codigo
            {
                get { return codigo; }
                set { codigo = value; }
            }

            string isin;

            public string Isin
            {
                get { return isin; }
                set { isin = value; }
            }

            int? limiteI;

            public int? LimiteI
            {
                get { return limiteI; }
                set { limiteI = value; }
            }

            decimal? desagioI;

            public decimal? DesagioI
            {
                get { return desagioI; }
                set { desagioI = value; }
            }

            int? limiteIIDe;

            public int? LimiteIIDe
            {
                get { return limiteIIDe; }
                set { limiteIIDe = value; }
            }

            int? limiteIIAte;

            public int? LimiteIIAte
            {
                get { return limiteIIAte; }
                set { limiteIIAte = value; }
            }

            decimal? desagioII;

            public decimal? DesagioII
            {
                get { return desagioII; }
                set { desagioII = value; }
            }

            string arquivo;

            public string Arquivo
            {
                get { return arquivo; }
                set { arquivo = value; }
            }
        #endregion
        
        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo devem ser inicializadas
        /// </summary>
        private void Initialize()
        {
            this.dataCriacaoArquivo = null;
            this.nomeArquivo = null;
            //
            this.codigoSelic = null;
            this.dataVencimento = null;
            this.fator = null;
            this.tipo = null;
            this.puReferenciaGarantia = null;

            this.codigo = null;
            this.isin = null;
            this.limiteI = null;
            this.desagioI = null;
            this.limiteIIDe = null;
            this.limiteIIAte = null;
            this.desagioII = null;
        }

        /// <summary>
        /// Realiza a Leitura do Arquivo
        /// </summary>
        public DesagioCollection ImportaDesagioGfDat()
        {

            #region Download

            string postData, url, eventTarget;

            url = "http://www.bmfbovespa.com.br/garantias/garantias.aspx?idioma=pt-br";

            eventTarget = "ctl00$contentPlaceHolderConteudo$limitesFatorDesRendaFixa$lblListaFatorDesagio";

            postData = String.Format(@"__EVENTTARGET={0}", eventTarget);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.KeepAlive = false;
            httpWebRequest.ProtocolVersion = HttpVersion.Version11;
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentLength = 0;

            byte[] postBytes = Encoding.ASCII.GetBytes(postData);

            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = postBytes.Length;
            Stream requestStream = httpWebRequest.GetRequestStream();

            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            
            #endregion
            
            DesagioCollection desagioCollection = new DesagioCollection();

            int i = 1;
            string linha = "";
            try
            {
                using (streamReader)
                {
                    while ((linha = streamReader.ReadLine()) != null)
                    {
                        if (!String.IsNullOrEmpty(linha))
                        {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataDesagio(linha);
                            
                            Desagio desagio = (Desagio)Utilitario.Clone(this);
                            desagioCollection.Add(desagio);
                        }
                        i++;
                    }
                }            
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Desagio com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                
                throw new InterfacesException(e.Message);
            }

            return desagioCollection;
        }

        private void GetTipoRegistro(string tipoRegistro)
        {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto.");

            List<string> valoresPossiveis = EstruturaArquivoDesagio.Values();
            if (!valoresPossiveis.Contains(tipoRegistro))
            {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo
            switch (tipoRegistro)
            {
                case EstruturaArquivoDesagio.Header:
                    this.tipoRegistro = EstruturaArquivoDesagio.Header;
                    break;
                case EstruturaArquivoDesagio.DetalheContratoRegistrado:
                    this.tipoRegistro = EstruturaArquivoDesagio.DetalheContratoRegistrado;
                    break;
                case EstruturaArquivoDesagio.Trailer:
                    this.tipoRegistro = EstruturaArquivoDesagio.Trailer;
                    break;
            }
            #endregion
        }


        // Salva os atributos do arquivo de acordo com o tipo
        private void TrataDesagio(string linha)
        {

            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoDesagio.Header)
            {
                //
                // Formato = "AAAAMMDD"
                string dataArquivo = linha.Substring(2, 8); // Campo 02
                int dia = Convert.ToInt32(dataArquivo.Substring(6, 2));
                int mes = Convert.ToInt32(dataArquivo.Substring(4, 2));
                int ano = Convert.ToInt32(dataArquivo.Substring(0, 4));
                //
                this.dataCriacaoArquivo = new DateTime(ano, mes, dia);
                                
            }
            else if (this.tipoRegistro == EstruturaArquivoDesagio.DetalheContratoRegistrado)
            {
                this.CodigoSelic = Convert.ToInt32(linha.Substring(2, 20)); // Campo 02

                string dataVenc = linha.Substring(22, 8).Trim(); // Campo 03
                int dia = Convert.ToInt32(dataVenc.Substring(6, 2));
                int mes = Convert.ToInt32(dataVenc.Substring(4, 2));
                int ano = Convert.ToInt32(dataVenc.Substring(0, 4));

                this.dataVencimento = new DateTime(ano, mes, dia); // Campo 04

                int fatorInteiro = Convert.ToInt32(linha.Substring(30, 15));
                int fatorFracionarioInteiro = Convert.ToInt32(linha.Substring(45, 8));
                decimal fatorFracionarioDecimal = fatorFracionarioInteiro / 100000000M;
                this.fator = fatorInteiro + fatorFracionarioDecimal; // Campo 05

                this.tipo = linha.Substring(53, 20).Trim(); // Campo 06

                int puReferenciaGarantiaInteiro = Convert.ToInt32(linha.Substring(73, 15));
                int puReferenciaGarantiaFracionarioInteiro = Convert.ToInt32(linha.Substring(88, 8));
                decimal puReferenciaGarantiaFracionarioDecimal = puReferenciaGarantiaFracionarioInteiro / 100000000M;
                this.puReferenciaGarantia = puReferenciaGarantiaInteiro + puReferenciaGarantiaFracionarioDecimal; // Campo 07
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoDesagio.Trailer)
            {
                this.numeroRegistros = Convert.ToInt32(linha.Substring(2, 10)); // Campo 02
            }
        }

        /// <summary>
        /// baixa descompacta e importa planilha
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public DesagioCollection ImportaPlanilhasDesagio(string path)
        {

            #region Download
            string endereco = "http://www.bmfbovespa.com.br/pt-br/mercados/download/Limites-e-Desagios-Acoes.zip";

            string pathArquivoDestino = path + "\\Limites-e-Desagios-Acoes.zip";
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            #region Unzip

            string unzipDir = path + "\\Unzip";
            
            Archive arquivo = new Archive();

            arquivo.QuickUnzip(pathArquivoDestino, unzipDir);

            #endregion

            #region Processaarqs

            DirectoryInfo directoryInfo = new DirectoryInfo(unzipDir);

            FileInfo[] fileInfo = directoryInfo.GetFiles("*.xlsx");

            DesagioCollection desagioCollection = new DesagioCollection();

            foreach(FileInfo file in fileInfo)
            {
                Stream stream = File.Open(unzipDir + "\\" + file.Name, FileMode.Open, FileAccess.Read);
                DesagioCollection desagioCol = ImportaDesagioLimitesDesagiosAcoesXls(stream);
                for (int i = 0; i < desagioCol.CollectionDesagio.Count-1; i++ )
                {
                    desagioCol.CollectionDesagio[i].arquivo = file.Name;
                    desagioCollection.Add(desagioCol.CollectionDesagio[i]);
                }
            }

            #endregion

            return desagioCollection;
        }

        /// <summary>
        /// importa uma planilha e devolve a collection
        /// </summary>
        /// <param name="streamExcel"></param>
        /// <returns></returns>
        private DesagioCollection ImportaDesagioLimitesDesagiosAcoesXls(Stream streamExcel)
        {
            DesagioCollection desagioCollection = new DesagioCollection();
            #region Leitura do Arquivo Excel

            // Open Spreadsheet
            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            document.LoadFromStream(streamExcel);

            Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

            #region Confere Formato Arquivo
            string[] colunasConferencias = new string[] 
                                {"Código","ISIN","Limite I (quantidade) até","Deságio I %","De",
                                 "Até","Deságio II %"};

            bool formato = isExcelFormatoValido(0,colunasConferencias, workSheet);

            if (!formato)
            {
                string mensagem = "Formato Interno do Arquivo Inválido.\n";
                mensagem += "Linha 1 do Arquivo deve ser: \n";
                for (int i = 0; i < colunasConferencias.Length; i++)
                {
                    int coluna = i + 1;
                    mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
                }

                document.Close();
                document.Dispose();
                throw new Exception(mensagem);
            }
            #endregion

            int index = 0;
            //This row,column index should be changed as per your need.
            // i.e. which cell in the excel you are interesting to read.

            /* Formato: 1)IdCliente  - 2)CdAtivoBolsa    - 3)CodigoBovespa   - 4)TipoOrdem           - 5)Data
             *          6)PU         - 7)Valor           - 8)Quantidade      - 9)PercentualDesconto  - 10) Trader
             *          11)CalculaDespesas         
             */

            int linha = 2;
            int coluna1 = 0, coluna2 = 1, coluna3 = 2,
                coluna4 = 3, coluna5 = 4, coluna6 = 5,
                coluna7 = 6;

            try
            {
                // Enquanto Tiver valor
                while (workSheet.Cell(linha, coluna1).Value != null)
                {
                    Desagio desagio = new Desagio();

                    desagio.Initialize();

                    desagio.codigo = workSheet.Cell(linha, coluna1).ValueAsString;
                    desagio.isin = workSheet.Cell(linha, coluna2).ValueAsString;
                    desagio.limiteI = workSheet.Cell(linha, coluna3).ValueAsInteger;

                    if (workSheet.Cell(linha, coluna4).Value != null &&
                         workSheet.Cell(linha, coluna4).Value.ToString().Trim() != "")
                    {
                        desagio.desagioI = Convert.ToDecimal(workSheet.Cell(linha, coluna4).ValueAsDouble);
                    }
                    desagio.limiteIIDe = workSheet.Cell(linha, coluna5).ValueAsInteger;
                    desagio.limiteIIAte = workSheet.Cell(linha, coluna6).ValueAsInteger;

                    if (workSheet.Cell(linha, coluna7).Value != null &&
                         workSheet.Cell(linha, coluna7).Value.ToString().Trim() != "")
                    {
                        desagio.desagioII = Convert.ToDecimal(workSheet.Cell(linha, coluna7).ValueAsDouble);
                    }

                    desagioCollection.Add(desagio);

                    index++;
                    linha = 2 + index;
                }
            }
            catch (Exception ex)
            {
                document.Close();
                document.Dispose();
                throw new Exception(ex.Message);
            }
            #endregion

            document.Close();
            document.Dispose();
            return desagioCollection;
        }

        /// <summary>
        /// valida se a planilha esta no formato correto
        /// </summary>
        /// <param name="linhaInicial"></param>
        /// <param name="colunasConferencias"></param>
        /// <param name="workSheet"></param>
        /// <returns></returns>
        private static bool isExcelFormatoValido(int linhaInicial, string[] colunasConferencias, Bytescout.Spreadsheet.Worksheet workSheet)
        {
            bool formato = true;

            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                string valoresColuna = null;

                if (i == 4 || i == 5) // colunas Limite II (quantidade) 'de' e 'até' desposicionadas e mescladas. tratadas as parte
                {
                    valoresColuna = workSheet.Cell(linhaInicial + 1, i).ValueAsString;
                }
                else
                {
                    valoresColuna = workSheet.Cell(linhaInicial, i).ValueAsString;
                }

                if (!String.IsNullOrEmpty(valoresColuna))
                {
                    if (colunasConferencias[i].Trim().ToLower() != valoresColuna.Trim().ToLower())
                    {
                        formato = false;
                        break;
                    }
                }
                else
                {
                    formato = false;
                    break;
                }
            }

            return formato;
        }
    }
}
