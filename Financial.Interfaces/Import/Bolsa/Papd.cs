﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using System.Collections.Generic;
using Financial.Util;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Papd {
        // Informação sobre o tipo registro da linha do arquivo Papd
        private string tipoRegistro;

        // bool indicando se papd deve ser salvo na colecao de Papd
        // Somente será salvo o Tipo de Registro do Papd que for necessario
        private bool papdProcessado;

        /* Em alguns casos o valor dos campos data será 40001231 
          Isso poderá acontecer em duas situações:  
        * - A data ainda será estabelecida
        * - A data não tem significado para o papel
        * para cada Campo tipo Data deve existir um atributo indicando se a data é valida
        */
        private static readonly DateTime dataInvalida = new DateTime(4000, 12, 31);

        #region Properties dos Valores do Registro00 - Header
       
        bool isValidoDataPregao;

        public bool IsValidoDataPregao {
            get { return isValidoDataPregao; }
            set { isValidoDataPregao = value; }
        }

        DateTime? dataPregao; // Campo 06

        public DateTime? DataPregao {
            get { return dataPregao; }
            set { dataPregao = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Dados dos Papeis
        
        string codigoIsin; // Campo 02

        public string CodigoIsin {
            get { return codigoIsin; }
            set { codigoIsin = value; }
        }

        string codigoIsinTipoAtivo; // Campo 02

        public string CodigoIsinTipoAtivo {
            get { return codigoIsinTipoAtivo; }
            set { codigoIsinTipoAtivo = value; }
        }

        int? distribuicao; // Campo 03

        public int? Distribuicao
        {
            get { return distribuicao; }
            set { distribuicao = value; }
        }

        string cdAtivoBolsa; // Campo 04

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        string nomeEmpresa; // Campo 05

        public string NomeEmpresa {
            get { return nomeEmpresa; }
            set { nomeEmpresa = value; }
        }
        
        /* Para papeis pertencentes ao novo mercado nas posições 9 e 10 está indicado N1, N2, N10  */
        string especificacao; // Campo 06

        public string Especificacao {
            get { return especificacao; }
            set { especificacao = value; }
        }

        /* Codigo do Mercado em que o papel está cadastrado, ver tabela PA003 */
        int? tipoMercado; // Campo 07

        public int? TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        //Número de sequência que identifica uma série p/ opções, termos e futuros
        int? numeroSerie; // Campo 09

        public int? NumeroSerie
        {
            get { return numeroSerie; }
            set { numeroSerie = value; }
        }

        /*Relativo à última data que o ativo teve proventos, vinculado diretamente ao nr de distribuição vigente*/
        DateTime? dataInicioNegociacao; // Campo 10

        public DateTime? DataInicioNegociacao
        {
            get { return dataInicioNegociacao; }
            set { dataInicioNegociacao = value; }
        }

        /* Por ora temos apenas a descrição, se se fizer necessário mais tarde importaremos tb o código do setor de atividade */
        string descricaoSetor; // Campo 22

        public string DescricaoSetor
        {
            get { return descricaoSetor; }
            set { descricaoSetor = value; }
        }
        
        /* Preço de Exercicio ou Valor de Contrato para o mercado de opções */
        decimal? precoExercicio; // Campo 24

        public decimal? PrecoExercicio {
            get { return precoExercicio; }
            set { precoExercicio = value; }
        }

        bool isValidoDataVencimento;

        public bool IsValidoDataVencimento {
            get { return isValidoDataVencimento; }
            set { isValidoDataVencimento = value; }
        }

        DateTime? dataVencimento; // Campo 25

        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        /* Indica se a opcao tem PU exercício corrigido ou se eh Opcao por Pontos  */
        int? indicadorOpcao; // Campo 26

        public int? IndicadorOpcao {
            get { return indicadorOpcao; }
            set { indicadorOpcao = value; }
        }

        decimal? precoFechamentoUltimo; // Campo 28

        public decimal? PrecoFechamentoUltimo
        {
            get { return precoFechamentoUltimo; }
            set { precoFechamentoUltimo = value; }
        }

        decimal? precoMedioUltimo; // Campo 30

        public decimal? PrecoMedioUltimo
        {
            get { return precoMedioUltimo; }
            set { precoMedioUltimo = value; }
        }

        int? fatorCotacao; // Campo 31

        public int? FatorCotacao {
            get { return fatorCotacao; }
            set { fatorCotacao = value; }
        }

        /* S=exige cobrança / N= nao exige cobrança */
        char? indicadorCobrancaCustodia; // Campo 39

        public char? IndicadorCobrancaCustodia {
            get { return indicadorCobrancaCustodia; }
            set { indicadorCobrancaCustodia = value; }
        }

        /* Para ações: o codigo isin do papel que o originou; 
         * Para recibos de subscrição indica o codigo isin do direito que o originou; 
         * Para demais tipos de titulos ou titulos novos estará em branco
        */
        string codigoIsinOriginal; // Campo 40

        public string CodigoIsinOriginal {
            get { return codigoIsinOriginal; }
            set { codigoIsinOriginal = value; }
        }

        bool isValidoDataInicioFatorCotacao;

        public bool IsValidoDataInicioFatorCotacao {
            get { return isValidoDataInicioFatorCotacao; }
            set { isValidoDataInicioFatorCotacao = value; }
        }

        DateTime? dataInicioFatorCotacao; // Campo 47

        public DateTime? DataInicioFatorCotacao {
            get { return dataInicioFatorCotacao; }
            set { dataInicioFatorCotacao = value; }
        }

        int? fatorCotacaoAnterior; // Campo 48

        public int? FatorCotacaoAnterior {
            get { return fatorCotacaoAnterior; }
            set { fatorCotacaoAnterior = value; }
        }

        bool isValidoDataInicioFatorCotacaoAnterior;

        public bool IsValidoDataInicioFatorCotacaoAnterior {
            get { return isValidoDataInicioFatorCotacaoAnterior; }
            set { isValidoDataInicioFatorCotacaoAnterior = value; }
        }

        DateTime? dataInicioFatorCotacaoAnterior; // Campo 49

        public DateTime? DataInicioFatorCotacaoAnterior {
            get { return dataInicioFatorCotacaoAnterior; }
            set { dataInicioFatorCotacaoAnterior = value; }
        }

        /* Preço de Exercicio em pontos para o mercado de opções ou valor do contrato em pontos para 
         * o mercado de termo secundario. Para os referenciado em dolar, cada ponto equivale ao valor, 
         * na moeda corrente de um centesimo da taxa media do dolar comercial interbancario de 
         * fechamento do dia anterior, ou seja 1 ponto = 1/100 US$
        */
        decimal? precoExercicioPontos; // Campo 50

        public decimal? PrecoExercicioPontos {
            get { return precoExercicioPontos; }
            set { precoExercicioPontos = value; }
        }

        string cdAtivoObjeto; // Campo 51

        public string CdAtivoObjeto {
            get { return cdAtivoObjeto; }
            set { cdAtivoObjeto = value; }
        }

        /* Serve para eliminar alguns como fundos FDIC por ex */
        int? indicadorMercado; // Campo 55

        public int? IndicadorMercado {
            get { return indicadorMercado; }
            set { indicadorMercado = value; }
        }
        #endregion

        #region Properties dos Valores do Registro02 - Codigos dos Papeis em outras Praças/Bolsas
        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Papd devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.papdProcessado = false;
            //
            this.dataPregao = null;
            //
            this.codigoIsin = null;
            this.cdAtivoBolsa = null;
            this.nomeEmpresa = null;
            this.especificacao = null;
            this.tipoMercado = null;
            this.dataInicioNegociacao = null;
            this.precoExercicio = null;
            this.dataVencimento = null;
            this.indicadorOpcao = null;
            this.fatorCotacao = null;
            this.indicadorCobrancaCustodia = null;
            this.codigoIsinOriginal = null;
            this.dataInicioFatorCotacao = null;
            this.fatorCotacaoAnterior = null;
            this.dataInicioFatorCotacaoAnterior = null;
            this.precoExercicioPontos = null;
            this.cdAtivoObjeto = null;
            this.indicadorMercado = null;
            //
            this.isValidoDataPregao = true;
            this.isValidoDataVencimento = true;
            this.isValidoDataInicioFatorCotacao = true;
            this.isValidoDataInicioFatorCotacaoAnterior = true;
        }

        /// <summary>
        ///  Indica se Papd possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return papdProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoPapdNaoEncontradoException
        public PapdCollection ProcessaPapd(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoPapdNaoEncontradoException("Arquivo PAPD não encontrado: " + nomeArquivoCompleto);
            }

            PapdCollection papdCollection = new PapdCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataPapd(linha, data);
                            // Somente é salvo Papd que possui dados relevantes
                            Papd papd = (Papd)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                papdCollection.Add(papd);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Papd com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaPapdException(mensagem.ToString());
            }

            return papdCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoPapdNaoEncontradoException
        public PapdCollection ProcessaPapd(StreamReader sr, DateTime data)
        {
            PapdCollection papdCollection = new PapdCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataPapd(linha, data);
                        // Somente é salvo Papd que possui dados relevantes
                        Papd papd = (Papd)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            papdCollection.Add(papd);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Papd com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaPapdException(e.Message);
            }

            return papdCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoPapd
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoPapd");

            List<string> valoresPossiveis = EstruturaArquivoPAPD.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo Papd
            switch (tipoRegistro) {
                case EstruturaArquivoPAPD.Header:
                    this.tipoRegistro = EstruturaArquivoPAPD.Header;
                    break;
                case EstruturaArquivoPAPD.DadosDosPapeis:
                    this.tipoRegistro = EstruturaArquivoPAPD.DadosDosPapeis;
                    break;
                case EstruturaArquivoPAPD.CodigosPapeisOutrasPracasBolsas:
                    this.tipoRegistro = EstruturaArquivoPAPD.CodigosPapeisOutrasPracasBolsas;
                    break;
                case EstruturaArquivoPAPD.Trailer:
                    this.tipoRegistro = EstruturaArquivoPAPD.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Papd de acordo com o tipo
        private void TrataPapd(string linha, DateTime data) {
            // Limpa os valores do Papd
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoPAPD.Header) {
                // Formato = "AAAAMMDD"
                string dataPregao = linha.Substring(30, 8); // Campo 06
                int dia = Convert.ToInt32(dataPregao.Substring(6, 2));
                int mes = Convert.ToInt32(dataPregao.Substring(4, 2));
                int ano = Convert.ToInt32(dataPregao.Substring(0, 4));
                //
                this.dataPregao = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataPregao.Value.CompareTo(data) != 0) {
                    throw new ArquivoPapdIncorretoException("Arquivo PAPD com data incorreta: " + this.dataPregao.Value.ToString("dd/MM/yyyy"));                    
                }

                this.isValidoDataPregao = this.DataValida(this.dataPregao.Value);
                
                // Indica que Papd deve ser salvo                
                this.papdProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoPAPD.DadosDosPapeis) {
                
                #region EstruturaArquivoPAPD.DadosDosPapeis
                this.codigoIsin = linha.Substring(2, 12).Trim(); // Campo 02
                //
                this.codigoIsinTipoAtivo = codigoIsin.Substring(6, 3);
                //
                string distribuicaoString = linha.Substring(14, 3); // Campo 3
                this.distribuicao = Convert.ToInt32(distribuicaoString);
                //
                this.cdAtivoBolsa = linha.Substring(17, 12).Trim(); // Campo 04
                this.nomeEmpresa = linha.Substring(29, 12).Trim(); // Campo 05                
                this.especificacao = linha.Substring(41, 10).Trim(); // Campo 06
                this.tipoMercado = Convert.ToInt32(linha.Substring(51, 3)); // Campo 07
                //
                string numeroSerieString = linha.Substring(69, 7); // Campo 3
                this.numeroSerie = Convert.ToInt32(numeroSerieString);

                string dataInicioNegociacao = linha.Substring(76, 8); // Campo 10

                int dia = 0;
                int mes = 0;
                int ano = 0;

                try
                {
                    dia = Convert.ToInt32(dataInicioNegociacao.Substring(6, 2));
                    mes = Convert.ToInt32(dataInicioNegociacao.Substring(4, 2));
                    ano = Convert.ToInt32(dataInicioNegociacao.Substring(0, 4));
                    this.dataInicioNegociacao = new DateTime(ano, mes, dia);
                }
                catch (Exception)
                {
                }
                
                //                                        
                this.descricaoSetor = linha.Substring(164, 60).Trim(); // Campo 22

                int precoExercicio = Convert.ToInt32(linha.Substring(225, 11)); // Campo 24
                int precoExercicioFracionario = Convert.ToInt32(linha.Substring(236, 6));
                decimal precoExercicioFracionarioDecimal = precoExercicioFracionario / 1000000M;
                this.precoExercicio = precoExercicio + precoExercicioFracionarioDecimal;
                //
                string dataVencimento = linha.Substring(242, 8); // Campo 25
                dia = Convert.ToInt32(dataVencimento.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimento.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimento.Substring(0, 4));
                //
                this.dataVencimento = new DateTime(ano, mes, dia);
                this.isValidoDataVencimento = this.DataValida(this.dataVencimento.Value);
                //
                this.indicadorOpcao = Convert.ToInt32(linha.Substring(250, 1)); // Campo 26

                int precoFechamento = Convert.ToInt32(linha.Substring(266, 11)); // Campo 28
                int precoFechamentoFracionario = Convert.ToInt32(linha.Substring(277, 2));
                decimal precoFechamentoFracionarioDecimal = precoFechamentoFracionario / 100M;
                this.precoFechamentoUltimo = precoFechamento + precoFechamentoFracionarioDecimal;

                int precoMedio = Convert.ToInt32(linha.Substring(287, 11)); // Campo 30
                int precoMedioFracionario = Convert.ToInt32(linha.Substring(298, 6));
                decimal precoMedioFracionarioDecimal = precoFechamentoFracionario / 1000000M;
                this.precoMedioUltimo = precoMedio + precoMedioFracionarioDecimal;

                this.fatorCotacao = Convert.ToInt32(linha.Substring(304, 7)); // Campo 31
                this.IndicadorCobrancaCustodia = Convert.ToChar(linha.Substring(347, 1)); // Campo 39
                this.codigoIsinOriginal = linha.Substring(348, 12).Trim(); // Campo 40
                //
                string dataInicioFatorCotacao = linha.Substring(397, 8); // Campo 47
                dia = Convert.ToInt32(dataInicioFatorCotacao.Substring(6, 2));
                mes = Convert.ToInt32(dataInicioFatorCotacao.Substring(4, 2));
                ano = Convert.ToInt32(dataInicioFatorCotacao.Substring(0, 4));
                //
                this.dataInicioFatorCotacao = new DateTime(ano, mes, dia);
                this.isValidoDataInicioFatorCotacao = this.DataValida(this.dataInicioFatorCotacao.Value);
                //
                this.fatorCotacaoAnterior = Convert.ToInt32(linha.Substring(405, 7)); // Campo 48
                //
                string dataInicioFatorCotacaoAnterior = linha.Substring(412, 8); // Campo 49
                dia = Convert.ToInt32(dataInicioFatorCotacaoAnterior.Substring(6, 2));
                mes = Convert.ToInt32(dataInicioFatorCotacaoAnterior.Substring(4, 2));
                ano = Convert.ToInt32(dataInicioFatorCotacaoAnterior.Substring(0, 4));
                //
                this.dataInicioFatorCotacaoAnterior = new DateTime(ano, mes, dia);
                this.isValidoDataInicioFatorCotacaoAnterior = this.DataValida(this.dataInicioFatorCotacaoAnterior.Value);
                //
                int precoExercicioPontos = Convert.ToInt32(linha.Substring(420, 11)); // Campo 50
                int precoExercicioPontosFracionario = Convert.ToInt32(linha.Substring(431, 6));
                decimal precoExercicioPontosFracionarioDecimal = precoExercicioPontosFracionario / 1000000M;
                this.precoExercicioPontos = precoExercicioPontos + precoExercicioPontosFracionarioDecimal;
                //
                if (linha.Length >= 450) { // Ultima linhas vezes não tem a informação e nao tem o tamanho adequado
                    this.cdAtivoObjeto = linha.Substring(437, 12).Trim(); // Campo 51
                }

                if (linha.Length >= 495) { // Ultima linhas vezes não tem a informação e nao tem o tamanho adequado
                    this.indicadorMercado = Convert.ToInt32(linha.Substring(491, 3)); // Campo 55
                }
                #endregion

                // Indica que Papd deve ser salvo
                this.papdProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoPAPD.CodigosPapeisOutrasPracasBolsas) {
            }
            else if (this.tipoRegistro == EstruturaArquivoPAPD.Trailer) {
            }
        }

        /// <summary>
        /// Se data = 31/12/4000 - data invalida
        /// </summary>
        /// <returns>bool indicando se data é valida</returns>
        private bool DataValida(DateTime data) {
            return data.CompareTo(dataInvalida) == 0 ? false : true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é DadosDosPapeis</returns>
        public bool isTipoRegistroDadosPapeis() {
            return this.tipoRegistro == EstruturaArquivoPAPD.DadosDosPapeis;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool isTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoPAPD.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é CustodiaInfungivel</returns>
        public bool isIndicadorMercadoCustodiaInfungivel() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.CustodiaInfungivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FundoInvestimentoDireitoCreditorio</returns>
        public bool isIndicadorMercadoFundoInvestimentoDireitoCreditorio() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FundoInvestimentoDireitoCreditorio;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FundoInvestimentoParticipacao</returns>
        public bool isIndicadorMercadoFundoInvestimentoParticipacao() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FundoInvestimentoParticipacao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é Precatorio</returns>
        public bool isIndicadorMercadoPrecatorio() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.Precatorio;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é CertificadoInvestimentoFundoImobilario</returns>
        public bool isIndicadorMercadoCertificadoInvestimentoFundoImobilario() {
            return (this.IndicadorMercado == (int)IndicadorDoMercado.CertificadoInvestimentoFundoImobilario || this.descricaoSetor.Trim() == "FINANC E OUTROS/FUNDOS/FDO IMOBILIARIO");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é CertificadoInvestimentoFundoAcoes</returns>
        public bool isIndicadorMercadoCertificadoInvestimentoFundoAcoes() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.CertificadoInvestimentoFundoAcoes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é CertificadoAudioVisual</returns>
        public bool isIndicadorMercadoCertificadoAudioVisual() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.CertificadoAudioVisual;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FINAM</returns>
        public bool isIndicadorMercadoFINAM() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FINAM;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FINOR</returns>
        public bool isIndicadorMercadoFINOR() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FINOR;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FISET</returns>
        public bool isIndicadorMercadoFISET() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FISET;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se IndicadorMercado é FUNRES</returns>
        public bool isIndicadorMercadoFUNRES() {
            return this.IndicadorMercado == (int)IndicadorDoMercado.FUNRES;
        }

        /// <summary>
        /// Conversão do tipo Mercado em formato inteiro para o formato String
        /// </summary>
        /// <param name="tipoMercado">Tipo Mercado em formato inteiro</param>
        /// <returns>String representando o Tipo Mercado
        /// TipoMercado	001	VIS(VIS para FUT)
        ///             010	VIS
        ///             017	VIS(Leilao)
        ///             020	VIS(Fracionario)
        ///             030	TER
        ///             050	FUT
        ///             060	FUT
        ///             070	OPC
        ///             080	OPV
        /// </returns>
        /// <exception cref="Exception">Tipo Mercado Não Mapeado</exception>
        public string ConverteTipoMercadoPapd(int tipoMercado) {
            //
            const string MercadoVista = "VIS";
            const string OpcaoCompra = "OPC";
            const string OpcaoVenda = "OPV";
            const string Termo = "TER";
            const string Futuro = "FUT";

            string tipoMercadoRetorno = "";

            #region Define TipoMercado
            switch (tipoMercado) {
                case 001:
                case 010:
                case 017:
                case 020: tipoMercadoRetorno = MercadoVista;
                    break;
                case 030: tipoMercadoRetorno = Termo;
                    break;
                case 050:
                case 060: tipoMercadoRetorno = Futuro;
                    break;
                case 070: tipoMercadoRetorno = OpcaoCompra;
                    break;
                case 080: tipoMercadoRetorno = OpcaoVenda;
                    break;
                default: throw new Exception("Mapeamento Tipo Mercado do Papd Inválido");
            }
            #endregion

            return tipoMercadoRetorno;
        }

        /// <summary>
        /// Dada a especificação do ativo, converte a especificação para um numero que será usado 
        /// no codigo do ativo
        /// </summary>
        /// <param name="especificacao"></param>
        /// <returns>String representando a especificacao
        /// Especificacao	ON	3
        ///                 PN	4
        ///                 PNA	5
        ///                 PNB	6
        ///                 PNC	7
        ///                 PND	8
        /// </returns>
        public string ConverteEspecificacao(string especificacao) {

            string especificacaoRetorno = "";

            #region Define a especificacao
            switch (especificacao) {
                case "ON": especificacaoRetorno = EspecificacaoAtivo.ON;
                    break;
                case "PN": especificacaoRetorno = EspecificacaoAtivo.PN;
                    break;
                case "PNA": especificacaoRetorno = EspecificacaoAtivo.PNA;
                    break;
                case "PNB": especificacaoRetorno = EspecificacaoAtivo.PNB;
                    break;
                case "PNC": especificacaoRetorno = EspecificacaoAtivo.PNC;
                    break;
                case "PND": especificacaoRetorno = EspecificacaoAtivo.PND;
                    break;
                //default: throw new Exception("Mapeamento Especificação Inválido");
                //    break;
            }
            #endregion
           
            if (String.IsNullOrEmpty(especificacaoRetorno)) {
                especificacaoRetorno = EspecificacaoAtivo.EspecificacaoInvalida;
            }

            return especificacaoRetorno;
        }
    }
}
