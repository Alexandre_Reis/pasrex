using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa
{
    public class CmdfCollection
    {
        private List<Cmdf> collectionCmdf;

        public List<Cmdf> CollectionCmdf
        {
            get { return collectionCmdf; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CmdfCollection()
        {
            this.collectionCmdf = new List<Cmdf>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de Cmdfs</param>
        public void Add(Cmdf cmdf)
        {
            collectionCmdf.Add(cmdf);
        }
    }
}
