﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class PosrCollection {
        private List<Posr> collectionPosr;

        public List<Posr> CollectionPosr {
            get { return collectionPosr; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public PosrCollection() {
            this.collectionPosr = new List<Posr>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posr">Elemento a inserir na lista de Posr</param>
        public void Add(Posr posr) {
            collectionPosr.Add(posr);
        }

        /// <summary>
        /// Procura na collection pelo idCliente
        /// </summary>
        /// <param name="codigoBovespaCliente"></param>
        /// <returns></returns>
        public PosrCollection FilterByIdCliente(int codigoBovespaCliente) {
            PosrCollection posrCollectionAux = new PosrCollection();
            for (int i = 0; i < this.CollectionPosr.Count; i++) {
                Posr posr = this.CollectionPosr[i];
                // o tipoRegistro = Header tambem é colocado na collection 
                if (posr.IsTipoRegistroHeader()) {
                    posrCollectionAux.Add(posr);
                }

                if (posr.CodigoCliente.HasValue && posr.CodigoCliente.Value == codigoBovespaCliente) {
                    posrCollectionAux.Add(posr);
                }
            }

            return posrCollectionAux;
        }
    }
}
