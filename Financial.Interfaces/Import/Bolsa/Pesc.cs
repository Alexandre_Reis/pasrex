﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Pesc {
        // Informação sobre o tipo registro da linha do arquivo Pesc
        private string tipoRegistro;

        // bool indicando se Pesc deve ser salvo na colecao de Pesc
        // Somente será salvo o Tipo de Registro do Pesc que for necessario
        private bool pescProcessado;

        #region Properties dos Valores do Registro00 - Header
        int? codigoAgenteCorretora; // Campo 2.2 - Codigo do usuario
        DateTime? dataMovimento;       // Campo 06

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgenteCorretora {
            get { return codigoAgenteCorretora; }
            set { codigoAgenteCorretora = value; }
        }

        #endregion

        #region Properties dos Valores do Registro01
        DateTime dataPregao;       // Campo 02
        string cdAtivoBolsa;       // Campo 03
        string numeroNegocio;      // Campo 04
        string natureza;           // Campo 05
        int codigoCliente;         // Campo 06
        decimal quantidade;        // Campo 08
        string tipoMercado;        // Campo 15
        decimal pu;                // Campo 16 
        decimal fatorCotacao;      // Campo 17
        string cdAtivoBolsaObjeto; // Campo 19

        public DateTime DataPregao {
            get { return dataPregao; }
            set { dataPregao = value; }
        }

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public string NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        public string Natureza {
            get { return natureza; }
            set { natureza = value; }
        }

        public int CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        public decimal FatorCotacao {
            get { return fatorCotacao; }
            set { fatorCotacao = value; }
        }

        public string TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        public string CdAtivoBolsaObjeto {
            get { return cdAtivoBolsaObjeto; }
            set { cdAtivoBolsaObjeto = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Pesc devem ser inicializadas
        /// </summary>
        private void Initialize() {
            //this.dataMovimento = null;
        }

        /// <summary>
        ///  Indica se Pesc possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return pescProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// throws ArquivoPescIncorretoException se a dataMovimento do arquivo for diferente da data passada
        public PescCollection ProcessaPesc(StreamReader sr, DateTime data) {
            PescCollection pescCollection = new PescCollection();
            int i = 1;
            string linha = "";
            try {
                while ((linha = sr.ReadLine()) != null) {
                    //Console.WriteLine(linha.Length);
                    if (!String.IsNullOrEmpty(linha)) {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataPesc(linha, data);                        
                        //
                        Pesc pesc = (Pesc)Utilitario.Clone(this);
                        //
                        if (this.HasValue()) {
                            pescCollection.Add(pesc);
                        }                        
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Pesc com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaPescException(e.Message);
            }

            return pescCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoPESC
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoPesc");

            List<string> valoresPossiveis = EstruturaArquivoPESC.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo Pesc
            switch (tipoRegistro) {
                case EstruturaArquivoPESC.Header:
                    this.tipoRegistro = EstruturaArquivoPESC.Header;
                    break;
                case EstruturaArquivoPESC.Especificacao:
                    this.tipoRegistro = EstruturaArquivoPESC.Especificacao;
                    break;
                case EstruturaArquivoPESC.Trailer:
                    this.tipoRegistro = EstruturaArquivoPESC.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Pesc de acordo com o tipo
        private void TrataPesc(string linha, DateTime data) {
            // Limpa os valores do Pesc
            //this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoPESC.Header) {
                //
                this.codigoAgenteCorretora = Convert.ToInt32(linha.Substring(6, 4)); // Campo 2.2
                
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(30, 8); // Campo 06
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
                int ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
                ////
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoPescIncorretoException("Arquivo Pesc com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));
                }

                // Indica que Pesc deve ser salvo                
                this.pescProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoPESC.Especificacao) {
                // Formato = "AAAAMMDD"
                string dataAux = linha.Substring(2, 8); // Campo 02
                int dia = Convert.ToInt32(dataAux.Substring(6, 2));
                int mes = Convert.ToInt32(dataAux.Substring(4, 2));
                int ano = Convert.ToInt32(dataAux.Substring(0, 4));
                //
                this.dataPregao = new DateTime(ano, mes, dia);
                //
                this.cdAtivoBolsa = linha.Substring(10, 12).Trim(); // Campo 3
                this.natureza = linha.Substring(29, 1).Trim(); // Campo 5                
                this.codigoCliente = Convert.ToInt32(linha.Substring(30, 7)); // Campo 06
                //
                Int64 quantidadeAux = Convert.ToInt64(linha.Substring(38, 15)); // Campo 8
                this.quantidade = Convert.ToDecimal(quantidadeAux);
                //
                this.tipoMercado = linha.Substring(72, 3).Trim(); // Campo 15
                //
                int pu = Convert.ToInt32(linha.Substring(75, 9)); // Campo 16
                int puFracionario = Convert.ToInt32(linha.Substring(84, 2));
                decimal puFracionarioDecimal = puFracionario / 100M;
                this.pu = pu + puFracionarioDecimal;
                //
                this.fatorCotacao = Convert.ToInt32(linha.Substring(86, 7)); // Campo 17

                if (!linha.Substring(94, 12).Trim().Equals("")) {
                    this.cdAtivoBolsaObjeto = linha.Substring(94, 12).Trim(); // Campo 19                
                }

                // Indica que Pesc deve ser salvo
                this.pescProcessado = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoPESC.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Especificação</returns>
        public bool IsTipoRegistroEspecificacao() {
            return this.tipoRegistro == EstruturaArquivoPESC.Especificacao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Vista</returns>
        public bool IsTipoMercadoVista() {
            return this.TipoMercado == TipoDoMercadoPesc.Vista;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é ExercicioOpcao Compra</returns>
        public bool IsTipoMercadoExercicioCompra() {
            return this.TipoMercado == TipoDoMercadoPesc.ExercicioCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é ExercicioOpcao Venda</returns>
        public bool IsTipoMercadoExercicioVenda() {
            return this.TipoMercado == TipoDoMercadoPesc.ExercicioVenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é LeilaoTitulosNaoCotados</returns>
        public bool IsTipoMercadoLeilaoTitulosNaoCotados() {
            return this.TipoMercado == TipoDoMercadoPesc.LeilaoTitulosNaoCotados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Leilao</returns>
        public bool IsTipoMercadoLeilao() {
            return this.TipoMercado == TipoDoMercadoPesc.Leilao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Fracionario</returns>
        public bool IsTipoMercadoFracionario() {
            return this.TipoMercado == TipoDoMercadoPesc.Fracionario;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é Termo</returns>
        public bool IsTipoMercadoTermo() {
            return this.TipoMercado == TipoDoMercadoPesc.Termo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é OpcaoCompra</returns>
        public bool IsTipoMercadoOpcaoCompra() {
            return this.TipoMercado == TipoDoMercadoPesc.OpcaoCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Mercado é OpcaoVenda</returns>
        public bool IsTipoMercadoOpcaoVenda() {
            return this.TipoMercado == TipoDoMercadoPesc.OpcaoVenda;
        }
    }
}