﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using Financial.Util;

namespace Financial.Interfaces.Import.Bolsa {
    public class NegsCollection {
        private List<Negs> collectionNegs;

        public List<Negs> CollectionNegs {
            get { return collectionNegs; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public NegsCollection() {
            this.collectionNegs = new List<Negs>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="negs">Elemento a inserir na lista de Negss</param>
        public void Add(Negs negs) {
            this.collectionNegs.Add(negs);
        }

        /// <summary>
        /// Preenche o Atributo Extenso idCliente em todos os Itens da Collection
        /// </summary>
        /// <param name="idCliente"></param>
        public void SetaIdCliente(int idCliente) {
            for (int i = 0; i < this.collectionNegs.Count; i++) {
                this.collectionNegs[i].ExtendAtributos.IdCliente = idCliente;
            }
        }

        /// <summary>
        /// Preenche o Atributo Extenso CampoOrdenação em todos os Itens da Collection
        /// </summary>
        /// <param name="idCliente"></param>
        public void SetaCampoOrdenacao() {
            for (int i = 0; i < this.collectionNegs.Count; i++) {
                string cdAtivo = this.collectionNegs[i].CdAtivoBolsa.Trim();
                string tipoOperacao = this.collectionNegs[i].TipoOperacao.Trim();
                //
                this.collectionNegs[i].ExtendAtributos.CampoOrdenacao = cdAtivo + tipoOperacao;
            }
        }

        /// <summary>
        /// Remove o "F" dos CdAtivoBolsa que são Fracionários
        /// CampoOrdenacao também é modificado
        /// </summary>
        /// <param name="idCliente"></param>
        public void CorrigeAtivosFracionarios() {
            for (int i = 0; i < this.collectionNegs.Count; i++) {
                if ( this.IsCdAtivoBolsaFracionario(this.collectionNegs[i].CdAtivoBolsa) ) {
                    int posicao = this.collectionNegs[i].CdAtivoBolsa.Length - 1;
                    this.collectionNegs[i].CdAtivoBolsa = this.collectionNegs[i].CdAtivoBolsa.Remove(posicao);
                    this.collectionNegs[i].ExtendAtributos.CampoOrdenacao = this.collectionNegs[i].CdAtivoBolsa + this.collectionNegs[i].TipoOperacao;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>bool indicando se codigo do ativo é fracionario</returns>
        private bool IsCdAtivoBolsaFracionario(string cdAtivoBolsa) {
            return Utilitario.Right(cdAtivoBolsa, 1).Equals("F");
        }
    }
}