﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using System.Collections.Generic;
using Financial.Util;

namespace Financial.Interfaces.Import.Bolsa {

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Prod {
        // Informação sobre o tipo registro da linha do arquivo PROD
        private string tipoRegistro;

        // bool indicando se prod deve ser salvo na colecao de Prod
        // Somente será salvo o Tipo de Registro do Prod que for necessario
        private bool prodProcessado;

        #region Properties dos Valores do Registro00 - Header

        DateTime? dataMovimento; // Campo 06
        
        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Dados dos Proventos
        
        /* Codigo do papel no sistema isin sobre o qual foram cadastrados proventos */
        string codigoIsinBase; // Campo 02 

        public string CodigoIsinBase {
            get { return codigoIsinBase; }
            set { codigoIsinBase = value; }
        }
        
        /* Número de sequencia do papel correspondente ao estado de direito sob o qual foram 
         * cadastrados proventos */
        int? distribuicaoBase; // Campo 03

        public int? DistribuicaoBase {
            get { return distribuicaoBase; }
            set { distribuicaoBase = value; }
        }

        string cdAtivoBolsa; // Campo 04

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        string especificacao; // Campo 06

        public string Especificacao {
            get { return especificacao; }
            set { especificacao = value; }
        }

        int? tipoProvento; // Campo 07

        public int? TipoProvento {
            get { return tipoProvento; }
            set { tipoProvento = value; }
        }

        DateTime? dataAssembleia; // Campo 09

        public DateTime? DataAssembleia {
            get { return dataAssembleia; }
            set { dataAssembleia = value; }
        }

        decimal? valorProvento; // Campo 10 - Fator

        public decimal? ValorProvento {
            get { return valorProvento; }
            set { valorProvento = value; }
        }
        
        /* Codigo do Papel no Sistema Isin sobre o qual incide o provento */
        string codigoIsinDestino; // Campo 12 

        public string CodigoIsinDestino {
            get { return codigoIsinDestino; }
            set { codigoIsinDestino = value; }
        }
        
        /* Número de sequencia do papel correspondente ao estado de direito sob o qual incide o provento */
        int? distribuicaoDestino; // Campo 13

        public int? DistribuicaoDestino {
            get { return distribuicaoDestino; }
            set { distribuicaoDestino = value; }
        }

        /* Codigo do papel no sistema Isin sob o qual foi cadastrada uma subscrição originando um direito (só existira para tipo de provento = 52) */
        string codigoIsinBaseSubscricao; // Campo 14 

        public string CodigoIsinBaseSubscricao
        {
            get { return codigoIsinBaseSubscricao; }
            set { codigoIsinBaseSubscricao = value; }
        }

        /* Codigo de um direito de subscricao no sistema Isin (só existira para tipo de provento = 50,51) */
        string codigoIsinDireitoSubscricao; // Campo 16 

        public string CodigoIsinDireitoSubscricao {
            get { return codigoIsinDireitoSubscricao; }
            set { codigoIsinDireitoSubscricao = value; }
        }
        
        /* Número de sequencia de um direito de subscricao (só existira para tipo de provento = 50,51) */
        int? distribuicaoSubscricao; // Campo 17

        public int? DistribuicaoSubscricao {
            get { return distribuicaoSubscricao; }
            set { distribuicaoSubscricao = value; }
        }

        decimal? precoEmissaoSubscricao; // Campo 18

        public decimal? PrecoEmissaoSubscricao
        {
            get { return precoEmissaoSubscricao; }
            set { precoEmissaoSubscricao = value; }
        }

        /*Data limite para subscrição */
        DateTime? dataFinalSubscricao; // Campo 19

        public DateTime? DataFinalSubscricao
        {
            get { return dataFinalSubscricao; }
            set { dataFinalSubscricao = value; }
        }

        DateTime? dataPagamento; // Campo 20

        public DateTime? DataPagamento {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }

        /* 
        * 1 COMPRA/VENDA   COMPRA/VENDA PELA EMPRESA                                                		
        * 2 COMPRA PELA EMPRESA                                                      		
        * 3 VENDA PELA EMPRESA                                                       		
        * 5 COMPLEMENTO (ABONO OU ARREDONDAMENTO)                                    		
        * 6 A EMPRESA REALIZA LEILAO DAS FRACOES                                     		
        * 7 A EMPRESA EMITENTE ADMINISTRA AS FRACOES                                 		
        * 9 A SER DESPREZADO 
        */
        string indicadorTratamentoFracao; // Campo 24

        public string IndicadorTratamentoFracao {
            get { return indicadorTratamentoFracao; }
            set { indicadorTratamentoFracao = value; }
        }

        DateTime? dataEx; // Campo 39

        public DateTime? DataEx {
            get { return dataEx; }
            set { dataEx = value; }
        }
        
        /* T = incluido, A = Alterado, E = Excluido, H = Historico se Arquivo = ProH 
           " " Em Atualizacao se Arquivo = ProT ou ProH 
         */
        char? situacaoProvento; // Campo 41 

        public char? SituacaoProvento {
            get { return situacaoProvento; }
            set { situacaoProvento = value; }
        }        
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Prod devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.prodProcessado = false;
            //
            this.codigoIsinBase = null;
            this.distribuicaoBase = null;
            this.cdAtivoBolsa = null;
            this.especificacao = null;
            this.tipoProvento = null;
            this.dataAssembleia = null;
            this.valorProvento = null;
            this.codigoIsinDestino = null;
            this.distribuicaoDestino = null;
            this.codigoIsinBaseSubscricao = null;
            this.codigoIsinDireitoSubscricao = null;
            this.distribuicaoSubscricao = null;
            this.precoEmissaoSubscricao = null;
            this.dataFinalSubscricao = null;
            this.dataPagamento = null;
            this.indicadorTratamentoFracao = null;
            this.dataEx = null;
            this.situacaoProvento = null;
            //
        }

        /// <summary>
        ///  Indica se Prod possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return prodProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoProdNaoEncontradoException
        /// throws ArquivoProdIncorretoException se a data passada e a data do arquivo forem diferentes
        public ProdCollection ProcessaProd(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoProdNaoEncontradoException("Arquivo PROD não encontrado: " + nomeArquivoCompleto);
            }
                               
            ProdCollection prodCollection = new ProdCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataProd(linha, data);
                            // Somente é salvo PROD que possui dados relevantes
                            Prod prod = (Prod)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                prodCollection.Add(prod);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaProdException(e.Message);
            }

            return prodCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoProdNaoEncontradoException
        /// throws ArquivoProdIncorretoException se a data passada e a data do arquivo forem diferentes
        public ProdCollection ProcessaProd(StreamReader sr, DateTime data)
        {
            ProdCollection prodCollection = new ProdCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataProd(linha, data);
                        // Somente é salvo PROD que possui dados relevantes
                        Prod prod = (Prod)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            prodCollection.Add(prod);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaProdException(e.Message);
            }

            return prodCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoProd
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoProd");

            List<string> valoresPossiveis = EstruturaArquivoPROD.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo Prod
            switch (tipoRegistro) {
                case EstruturaArquivoPROD.Header:
                    this.tipoRegistro = EstruturaArquivoPROD.Header;
                    break;
                case EstruturaArquivoPROD.DadosProventos:
                    this.tipoRegistro = EstruturaArquivoPROD.DadosProventos;
                    break;
                case EstruturaArquivoPROD.Trailer:
                    this.tipoRegistro = EstruturaArquivoPROD.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Prod de acordo com o tipo
        private void TrataProd(string linha, DateTime data) {
            // Limpa os valores do Prod
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoPROD.Header) {
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(30, 8); // Campo 06
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoProdIncorretoException("Arquivo PROD com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));
                }

                // Indica que Prod deve ser salvo                
                this.prodProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoPROD.DadosProventos) {
                this.codigoIsinBase = linha.Substring(2, 12).Trim(); // Campo 02
                this.distribuicaoBase = Convert.ToInt32(linha.Substring(14, 3)); // Campo 3
                this.cdAtivoBolsa = linha.Substring(17, 12).Trim(); // Campo 04
                this.especificacao = linha.Substring(41, 10).Trim(); // Campo 06
                this.tipoProvento = Convert.ToInt32(linha.Substring(51, 3)); // Campo 07
                //
                // Formato = "AAAAMMDD"                              
                string dataAssembleia = linha.Substring(69, 8); // Campo 09
                int dia = Convert.ToInt32(dataAssembleia.Substring(6, 2));
                int mes = Convert.ToInt32(dataAssembleia.Substring(4, 2));
                int ano = Convert.ToInt32(dataAssembleia.Substring(0, 4));
                this.dataAssembleia = new DateTime(ano, mes, dia);
                //                
                //
                int valorProvento = Convert.ToInt32(linha.Substring(77, 7)); // Campo 10
                Int64 valorProventoFracionario = Convert.ToInt64(linha.Substring(84, 11));
                decimal valorProventoFracionarioDecimal = valorProventoFracionario / 100000000000M;
                this.valorProvento = valorProvento + valorProventoFracionarioDecimal;
                //
                this.codigoIsinDestino = linha.Substring(113, 12).Trim(); // Campo 12
                this.distribuicaoDestino = Convert.ToInt32(linha.Substring(125, 2)); // Campo 13
                this.codigoIsinBaseSubscricao = Convert.ToString(linha.Substring(128, 12)); // Campo 14
                this.codigoIsinDireitoSubscricao = linha.Substring(143, 12).Trim(); // Campo 16
                this.distribuicaoSubscricao = Convert.ToInt32(linha.Substring(155, 3)); // Campo 17
                //
                int precoEmissao = Convert.ToInt32(linha.Substring(158, 7)); // Campo 10
                Int64 precoEmissaoFracionario = Convert.ToInt64(linha.Substring(165, 11));
                decimal precoEmissaoFracionarioDecimal = valorProventoFracionario / 100000000000M;
                this.precoEmissaoSubscricao = precoEmissao + precoEmissaoFracionarioDecimal;
                // Formato = "AAAAMMDD"                              
                string dataFinalSubscricao = linha.Substring(176, 8); // Campo 19
                dia = Convert.ToInt32(dataFinalSubscricao.Substring(6, 2));
                mes = Convert.ToInt32(dataFinalSubscricao.Substring(4, 2));
                ano = Convert.ToInt32(dataFinalSubscricao.Substring(0, 4));
                this.dataFinalSubscricao = new DateTime(ano, mes, dia);
                // Formato = "AAAAMMDD"                              
                string dataPagamento = linha.Substring(184, 8); // Campo 20
                dia = Convert.ToInt32(dataPagamento.Substring(6, 2));
                mes = Convert.ToInt32(dataPagamento.Substring(4, 2));
                ano = Convert.ToInt32(dataPagamento.Substring(0, 4));
                this.dataPagamento = new DateTime(ano, mes, dia);
                
                //                
                this.indicadorTratamentoFracao = linha.Substring(201, 2); // Campo 24

                // Formato = "AAAAMMDD"                              
                string dataEx = linha.Substring(333, 8); // Campo 39
                dia = Convert.ToInt32(dataEx.Substring(6, 2));
                mes = Convert.ToInt32(dataEx.Substring(4, 2));
                ano = Convert.ToInt32(dataEx.Substring(0, 4));
                this.dataEx = new DateTime(ano, mes, dia);
                //
                /* T = incluido, A = Alterado, E = Excluido, H = Historico se Arquivo = ProH 
                " " Em Atualizacao se Arquivo = ProT ou ProH */
                this.situacaoProvento = Convert.ToChar(linha.Substring(348, 1)); // Campo 41
                //                                
                // Indica que Prod deve ser salvo
                this.prodProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoPROD.Trailer) {
            }
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoProvento
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoProventoBonificacao está na collection        
        ///           false se TipoProvento não está na collection 
        /// </returns>
        public bool FilterProdByTipoProventoBonificacao(Prod prod) {            
            bool retorno = prod.TipoProvento == TipoDoProvento.Bonificacao;
            return retorno;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoProvento
        /// </summary>
        /// <param name="prod"></param>
        /// <returns> true se TipoProventoConversao
        ///           false se TipoProvento não está na collection 
        /// </returns>
        public bool FilterProdByTipoProventoConversao(Prod prod) {
            return prod.TipoProvento == TipoDoProvento.Conversao.Incorporacao ||
                   prod.TipoProvento == TipoDoProvento.Conversao.Fusao ||
                   prod.TipoProvento == TipoDoProvento.Conversao.Atualizacao ||
                   prod.TipoProvento == TipoDoProvento.Conversao.Cisao ||
                   prod.TipoProvento == TipoDoProvento.Conversao.CisaoComReducaoQuantidade;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoProvento
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoProventoGrupamento ou TipoProventoDesdobramento está na collection        
        ///           false se TipoProvento não está na collection 
        /// </returns>
        public bool FilterProdByTipoProventoDesdobramentoGrupamento(Prod prod) {
            return prod.TipoProvento == TipoDoProvento.Desdobramento ||
                   prod.TipoProvento == TipoDoProvento.Grupamento;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoProvento
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoProventoSubscricao está na collection        
        ///           false se TipoProvento não está na collection 
        /// </returns>
        public bool FilterProdByTipoProventoSubscricao(Prod prod)
        {
            bool retorno = (prod.TipoProvento == TipoDoProvento.ExercicioSubscricao);
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Provento é Desdobramento</returns>
        public bool IsTipoProventoDesdobramento() {
            return this.tipoProvento == TipoDoProvento.Desdobramento;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Provento é Grupamento</returns>
        public bool IsTipoProventoGrupamento() {
            return this.tipoProvento == TipoDoProvento.Grupamento;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoProvento
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoProventoBonificacao está na collection        
        ///           false se TipoProvento não está na collection 
        /// </returns>
        public bool FilterProdByTipoProventoEmDinheiro(Prod prod)
        {
            bool retorno = (prod.TipoProvento == TipoDoProvento.BonificacaoDinheiro ||
                            prod.TipoProvento == TipoDoProvento.Dividendo ||
                            prod.TipoProvento == TipoDoProvento.JurosSobreCapital ||
                            prod.TipoProvento == TipoDoProvento.Rendimento ||
                            prod.TipoProvento == TipoDoProvento.RendimentoLiquido ||
                            prod.TipoProvento == TipoDoProvento.RestituicaoCapital ||
                            prod.TipoProvento == TipoDoProvento.RestituicaoCapitalComReducaoAcoes ||
                            prod.TipoProvento == TipoDoProvento.Amortizacao ||
                            prod.TipoProvento == TipoDoProvento.Juros);
            return retorno;
        }

        /// <summary>
        /// Dada a especificação do ativo, converte a especificação para um numero que será usado 
        /// no codigo do ativo
        /// </summary>
        /// <param name="especificacao"></param>
        /// <returns>String representando a especificacao
        /// Especificacao	ON	3
        ///                 PN	4
        ///                 PNA	5
        ///                 PNB	6
        ///                 PNC	7
        ///                 PND	8
        /// </returns>
        public string ConverteEspecificacao(string especificacao) {

            string especificacaoRetorno = "";

            #region Define a especificacao
            switch (especificacao) {
                case "ON": especificacaoRetorno = EspecificacaoAtivo.ON;
                    break;
                case "PN": especificacaoRetorno = EspecificacaoAtivo.PN;
                    break;
                case "PNA": especificacaoRetorno = EspecificacaoAtivo.PNA;
                    break;
                case "PNB": especificacaoRetorno = EspecificacaoAtivo.PNB;
                    break;
                case "PNC": especificacaoRetorno = EspecificacaoAtivo.PNC;
                    break;
                case "PND": especificacaoRetorno = EspecificacaoAtivo.PND;
                    break;
            }
            #endregion

            if (String.IsNullOrEmpty(especificacaoRetorno)) {
                especificacaoRetorno = EspecificacaoAtivo.EspecificacaoInvalida;
            }

            return especificacaoRetorno;
        }
    }
}
