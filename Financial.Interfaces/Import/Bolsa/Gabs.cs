﻿using System;
using System.Text;
using Financial.Interfaces.Import.Bolsa.Enums;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Gabs {
        
        // bool indicando se Gabs deve ser salvo na colecao de Gabs
        // Somente será salvo o Tipo de Registro do Gabs que for necessario
        private bool gabsProcessado;
        
        #region Properties dos Valores de detalhe (Não tem Header nem Trailer)
       
        int? codigoCliente; // Campo 01
        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        string cdAtivoBolsa; // Campo 02
        public string CdAtivoBolsa
        {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        DateTime? dataPregao; // Campo 03
        public DateTime? DataPregao
        {
            get { return dataPregao; }
            set { dataPregao = value; }
        }

        decimal? valorLiquido; // Campo 04
        public decimal? ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }
        
        /* Numero do negocio que gerou o contrato */
        int? numeroContrato; // Campo 11
        public int? NumeroContrato {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        int? numeroNegocio; // Campo 13
        public int? NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        Int64? quantidade; // Campo 19
        public Int64? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        decimal? pu; // Campo 21
        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        decimal? valor; // Campo 22
        public decimal? Valor {
            get { return valor; }
            set { valor = value; }
        }

        string tipo; // Campo 25
        public string Tipo {
            get { return tipo; }
            set { tipo = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Conr devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.gabsProcessado = false;
            //
            this.codigoCliente = null;
            this.cdAtivoBolsa = null;
            this.dataPregao = null;
            this.valorLiquido = null;
            this.numeroContrato = null;
            this.numeroNegocio = null;
            this.quantidade = null;
            this.pu = null;
            this.valor = null;
            this.tipo = null;                        
            //
        }

        /// <summary>
        ///  Indica se Conr possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return gabsProcessado;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// throws ArquivoConrNaoEncontradoException
        public GabsCollection ProcessaGabs(string nomeArquivoCompleto, DateTime data)
        {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoConrNaoEncontradoException("Arquivo Gabs " + nomeArquivoCompleto + " não encontrado.");
            }

            GabsCollection gabsCollection = new GabsCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.TrataGabs(linha, data);
                            // Somente é salvo Gabs que possui dados relevantes
                            Gabs gabs = (Gabs)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                gabsCollection.Add(gabs);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Conr com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaConrException(e.Message);
            }

            return gabsCollection;
        }

        // Salva os atributos do arquivo Gabs
        private void TrataGabs(string linha, DateTime data)
        {
            // Limpa os valores do Conr
            this.Initialize();

            this.codigoCliente = Convert.ToInt32(linha.Substring(0, 7)); // Campo 01
            this.cdAtivoBolsa = linha.Substring(7, 12).Trim(); // Campo 02            

            // Formato = "AAAAMMDD"
            string dataPregao = linha.Substring(19, 8); // Campo 03
            int dia = Convert.ToInt32(dataPregao.Substring(6, 2));
            int mes = Convert.ToInt32(dataPregao.Substring(4, 2));
            int ano = Convert.ToInt32(dataPregao.Substring(0, 4));
            this.dataPregao = new DateTime(ano, mes, dia);

            this.valorLiquido = Convert.ToDecimal(linha.Substring(28, 19)) / 100M; // Campo 04
            /* 'C' - Compra, 'V'- Venda */
            this.tipo = linha.Substring(47, 2).Replace("N", ""); // Campo 05
            
            this.quantidade = Convert.ToInt64(Convert.ToInt64(linha.Substring(49, 11)) / 10000M); // Campo 06
            this.pu = Convert.ToDecimal(linha.Substring(60, 13)) / 100M; // Campo 07

            this.numeroContrato = Convert.ToInt32(linha.Substring(46, 9)); // Campo 11
            this.numeroNegocio = Convert.ToInt32(linha.Substring(56, 7)); // Campo 13
            
            // Indica que Conr deve ser salvo
            this.gabsProcessado = true;            
        }
    }
}
