﻿

using System;
using System.Text;
using Financial.Interfaces.Import.Bolsa.Enums;
using System.IO;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using System.Collections.Generic;
namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Csgd {
        // Informação sobre o tipo registro da linha do arquivo Csgd
        private string tipoRegistro;

        // bool indicando se prod deve ser salvo na colecao de Csgd
        // Somente será salvo o Tipo de Registro do Csgd que for necessario
        private bool csgdProcessado;
        
        #region Properties dos Valores do Registro00 - Header

        int? codigoAgente; // Campo 2.2
        DateTime? dataMovimento; // Campo 07

        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - IdentificacaoCliente       
        #endregion

        #region Properties dos Valores do Registro02 - IdentificacaoSaldo

        int? codigoCliente; // Campo03

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        int? codigoCarteira; // Campo04

        public int? CodigoCarteira {
            get { return codigoCarteira; }
            set { codigoCarteira = value; }
        }

        /* Quantidade de ações em custodia inclusive aações bloqueadas */
        decimal? quantidade; // Campo10 

        public decimal? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        string cdAtivoBolsa; // Campo 12

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        #endregion

        #region Properties dos Valores do Registro03 - IdentificacaoSaldoBloqueado
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Csgd devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.csgdProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            //
            this.codigoCliente = null;
            this.codigoCarteira = null;
            this.quantidade = null;
            this.cdAtivoBolsa = null;
            //
        }

        /// <summary>
        ///  Indica se Csgd possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return csgdProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// throws ArquivoCsgdNaoEncontradoException
        /// throws ArquivoCSGDIncorretoException se a dataMovimento do arquivo for diferente da data passada
        public CsgdCollection ProcessaCsgd(string nomeArquivoCompleto, DateTime data) {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoCsgdNaoEncontradoException("Arquivo CSGD não encontrado: " + nomeArquivoCompleto);
            }

            CsgdCollection csgdCollection = new CsgdCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        //Console.WriteLine(linha.Length);
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataCsgd(linha, data);
                            // Somente é salvo CSGD que possui dados relevantes
                            Csgd csgd = (Csgd)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                csgdCollection.Add(csgd);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Csgd com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaCsgdException(mensagem.ToString());
            }

            return csgdCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// throws ArquivoCSGDIncorretoException se a dataMovimento do arquivo for diferente da data passada
        public CsgdCollection ProcessaCsgd(StreamReader sr, DateTime data)
        {
            CsgdCollection csgdCollection = new CsgdCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    //Console.WriteLine(linha.Length);
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataCsgd(linha, data);
                        // Somente é salvo CSGD que possui dados relevantes
                        Csgd csgd = (Csgd)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            csgdCollection.Add(csgd);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Csgd com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaCsgdException(e.Message);
            }

            return csgdCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoCSGD
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoCSGD");

            List<string> valoresPossiveis = EstruturaArquivoCSGD.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo CSGD
            switch (tipoRegistro) {
                case EstruturaArquivoCSGD.Header:
                    this.tipoRegistro = EstruturaArquivoCSGD.Header;
                    break;
                case EstruturaArquivoCSGD.IdentificacaoCliente:
                    this.tipoRegistro = EstruturaArquivoCSGD.IdentificacaoCliente;
                    break;
                case EstruturaArquivoCSGD.IdentificacaoSaldo:
                    this.tipoRegistro = EstruturaArquivoCSGD.IdentificacaoSaldo;
                    break;
                case EstruturaArquivoCSGD.IdentificacaoSaldoBloqueado:
                    this.tipoRegistro = EstruturaArquivoCSGD.IdentificacaoSaldoBloqueado;
                    break;
                case EstruturaArquivoCSGD.Trailer:
                    this.tipoRegistro = EstruturaArquivoCSGD.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo CSGD de acordo com o tipo
        private void TrataCsgd(string linha, DateTime data) {
            // Limpa os valores do CSGD
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCSGD.Header) {
                this.codigoAgente = Convert.ToInt32(linha.Substring(6, 4)); // Campo 2.2                                               
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(39, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoCsgdIncorretoException("Arquivo CSGD com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                    
                }

                // Indica que Csgd deve ser salvo                
                this.csgdProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoCSGD.IdentificacaoCliente) {
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCSGD.IdentificacaoSaldo) {
                this.codigoCliente = Convert.ToInt32(linha.Substring(7, 7)); // Campo 03
                this.codigoCarteira = Convert.ToInt32(linha.Substring(14, 5)); // Campo 04
                //
                Int64 quantidade = Convert.ToInt64(linha.Substring(71, 15)); // Campo 10
                int quantidadeFracionario = Convert.ToInt32(linha.Substring(86, 3));
                decimal quantidadeFracionarioDecimal = quantidadeFracionario / 1000M;
                this.quantidade = quantidade + quantidadeFracionarioDecimal;
                //
                this.cdAtivoBolsa = linha.Substring(104, 12).Trim(); // Campo 12

                // Indica que Csgd deve ser salvo
                this.csgdProcessado = true;
            }
            else if (this.tipoRegistro == EstruturaArquivoCSGD.IdentificacaoSaldoBloqueado) {
            }
            else if (this.tipoRegistro == EstruturaArquivoCSGD.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoCSGD.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoSaldo</returns>
        public bool IsTipoRegistroIdentificacaoSaldo() {
            return this.tipoRegistro == EstruturaArquivoCSGD.IdentificacaoSaldo;
        }
    }
}
