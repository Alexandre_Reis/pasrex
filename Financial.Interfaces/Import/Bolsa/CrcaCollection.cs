using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa
{
    public class CrcaCollection
    {
        private List<Crca> collectionCrca;

        public List<Crca> CollectionCrca
        {
            get { return collectionCrca; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CrcaCollection()
        {
            this.collectionCrca = new List<Crca>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="crca">Elemento a inserir na lista de Crcas</param>
        public void Add(Crca crca)
        {
            collectionCrca.Add(crca);
        }
    }
}
