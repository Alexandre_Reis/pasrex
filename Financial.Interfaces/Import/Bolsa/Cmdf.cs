﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Cmdf {
        // Informação sobre o tipo registro da linha do arquivo Cmdf
        private string tipoRegistro;

        // bool indicando se cmdf deve ser salvo na colecao de Cmdfs
        // Somente será salvo o Tipo de Registro do Cmdf que for necessario
        private bool cmdfProcessado;

        #region Properties dos Valores do Registro00 - Header

        int? codigoAgente; // Campo 2.2 - Codigo do usuario
        DateTime? dataMovimento; // Campo 07

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }

        #endregion

        #region Properties dos Valores do Registro01 - Movimento do Dia
        DateTime? dataEfetivacao; // Campo 02

        public DateTime? DataEfetivacao {
            get { return dataEfetivacao; }
            set { dataEfetivacao = value; }
        }


        int? codigoGrupo; // Campo 05

        public int? CodigoGrupo {
            get { return codigoGrupo; }
            set { codigoGrupo = value; }
        }

        int? codigoLancamento; // Campo 07

        public int? CodigoLancamento
        {
            get { return codigoLancamento; }
            set { codigoLancamento = value; }
        }


        string descricaoLancamento; // Campo 08

        public string DescricaoLancamento {
            get { return descricaoLancamento; }
            set { descricaoLancamento = value; }
        }


        //C = Crédito  D = Débito
        string tipoLancamento; // Campo 09

        public string TipoLancamento {
            get { return tipoLancamento; }
            set { tipoLancamento = value; }
        }


        int? codigoCliente; // Campo 13

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }


        decimal? valor; // Campo 16

        public decimal? Valor {
            get { return valor; }
            set { valor = value; }
        }


        decimal? quantidade; // Campo 23

        public decimal? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }


        //CdAtivoBolsa
        string codigoNegocio; // Campo 26

        public string CodigoNegocio {
            get { return codigoNegocio; }
            set { codigoNegocio = value; }
        }


        decimal? valorIR; // Campo 28

        public decimal? ValorIR {
            get { return valorIR; }
            set { valorIR = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Cmdf devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.cmdfProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            //
            this.dataEfetivacao = null;

            this.codigoGrupo = null;
            this.descricaoLancamento = null;
            this.tipoLancamento = null;
            this.codigoCliente = null;
            this.valor = null;
            this.quantidade = null;
            this.codigoNegocio = null;
            this.valorIR = null;
            this.codigoLancamento = null;
            //
        }

        /// <summary>
        ///  Indica se Cmdf possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return cmdfProcessado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <exception cref="ArquivoCmdfNaoEncontradoException">Se Arquivo CMDF não foi encontrado</exception>
        /// <exception cref="ProcessaCmdfException">Se ocorreu erro no Processamento</exception>
        public CmdfCollection ProcessaCmdf(string nomeArquivoCompleto, DateTime data) {
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoCmdfNaoEncontradoException("Arquivo CMDF não encontrado: " + nomeArquivoCompleto);
            }

            CmdfCollection cmdfCollection = new CmdfCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataCmdf(linha, data);
                            // Somente é salvo CMDF que possui dados relevantes
                            Cmdf cmdf = (Cmdf)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                cmdfCollection.Add(cmdf);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaCmdfException(e.Message);
            }

            return cmdfCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <exception cref="ProcessaCmdfException">Se ocorreu erro no Processamento</exception>
        public CmdfCollection ProcessaCmdf(StreamReader sr, DateTime data)
        {
            CmdfCollection cmdfCollection = new CmdfCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataCmdf(linha, data);
                        // Somente é salvo CMDF que possui dados relevantes
                        Cmdf cmdf = (Cmdf)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            cmdfCollection.Add(cmdf);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ProcessaCmdfException(e.Message);
            }

            return cmdfCollection;
        }

        /// <summary>
        /// A partir de um Arquivo Cmdf Existente Gera um novo Arquivo Cmdf somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoCmdf">Caminho Completo do Arquivo Cmdf</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Cmdf será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Cmdf</param>
        /// <returns>true se novo arquivo Cmdf foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoCmdfNaoEncontradoException">Se Arquivo Cmdf de entrada não existe</exception>
        public bool GeraArquivoCmdf(string nomeArquivoCompletoCmdf, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoCmdf)) {
                throw new ArquivoCmdfNaoEncontradoException("Arquivo Cmdf não Encontrado: " + nomeArquivoCompletoCmdf);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo

            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "cmdf_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoCmdf)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se Dado pertence a algum dos clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Cmdf pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaCmdf">linha do arquivo Cmdf</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Cmdf pertence a algum dos clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaCmdf, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaCmdf.Substring(0, 2)) {
                case EstruturaArquivoCMDF.Header:
                case EstruturaArquivoCMDF.Trailer:
                    break;
                case EstruturaArquivoCMDF.MovimentoDia:
                case EstruturaArquivoCMDF.MovimentoFuturo:
                    int codigoCliente = Convert.ToInt32(linhaCmdf.Substring(190, 7)); // Campo 13
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoCmdf
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoCMDF");

            List<string> valoresPossiveis = EstruturaArquivoCMDF.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo Cmdf
            switch (tipoRegistro) {
                case EstruturaArquivoCMDF.Header:
                    this.tipoRegistro = EstruturaArquivoCMDF.Header;
                    break;
                case EstruturaArquivoCMDF.MovimentoDia:
                    this.tipoRegistro = EstruturaArquivoCMDF.MovimentoDia;
                    break;
                case EstruturaArquivoCMDF.MovimentoFuturo:
                    this.tipoRegistro = EstruturaArquivoCMDF.MovimentoFuturo;
                    break;
                case EstruturaArquivoCMDF.Trailer:
                    this.tipoRegistro = EstruturaArquivoCMDF.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Cmdf de acordo com o tipo
        private void TrataCmdf(string linha, DateTime data) {
            // Limpa os valores do Cmdf
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCMDF.Header) {
                this.codigoAgente = Convert.ToInt32(linha.Substring(6, 4)); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(31, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
                int ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                
                // Indica que Cmdf deve ser salvo                
                this.cmdfProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCMDF.MovimentoFuturo) {
            }
            else if (this.tipoRegistro == EstruturaArquivoCMDF.MovimentoDia) {
                #region MovimentoDia
                // Formato = "AAAAMMDD"
                string dataEfetivacao = linha.Substring(02, 8); // Campo 02
                int dia = Convert.ToInt32(dataEfetivacao.Substring(6, 2));
                int mes = Convert.ToInt32(dataEfetivacao.Substring(4, 2));
                int ano = Convert.ToInt32(dataEfetivacao.Substring(0, 4));
                this.dataEfetivacao = new DateTime(ano, mes, dia);
                this.codigoGrupo = Convert.ToInt32(linha.Substring(29, 5)); // Campo 05
                this.codigoLancamento = Convert.ToInt32(linha.Substring(49, 5)); // Campo 05
                this.descricaoLancamento = linha.Substring(54, 60).TrimEnd(); // Campo 08
                this.tipoLancamento = linha.Substring(114, 1); // Campo 09
                this.codigoCliente = Convert.ToInt32(linha.Substring(190, 7)); // Campo 13
                this.valor = Convert.ToDecimal(linha.Substring(217, 18)) / 100M; // Campo 16
                this.quantidade = Convert.ToDecimal(linha.Substring(280, 15)); // Campo 23
                this.codigoNegocio = linha.Substring(308, 12).TrimEnd(); // Campo 26
                this.valorIR = Convert.ToDecimal(linha.Substring(333, 18)) / 100M; // Campo 28
                #endregion

                // Indica que Cmdf deve ser salvo
                this.cmdfProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCMDF.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoDividendo</returns>
        public bool IsTipoRegistroMovimentoDia() {
            return this.tipoRegistro == EstruturaArquivoCMDF.MovimentoDia;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoCMDF.Header;
        }

    }
}
