using System.Collections.Generic;

namespace Financial.Interfaces.Import.Bolsa {
    public class CmvdCollection {
        private List<Cmvd> collectionCmvd;

        public List<Cmvd> CollectionCmvd {
            get { return collectionCmvd; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CmvdCollection() {
            this.collectionCmvd = new List<Cmvd>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdin">Elemento a inserir na lista de Cmvds</param>
        public void Add(Cmvd cmvd) {
            collectionCmvd.Add(cmvd);
        }
    }
}