﻿using System;
using System.Collections.Generic;
using System.Text;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System.IO;
using java.io;
using Financial.Util;
using System.Globalization;
using java.awt.geom;
using org.apache.pdfbox.pdfparser;
using org.apache.pdfbox.cos;

namespace Financial.Interfaces.Import.Bolsa
{
    public class NotaCorretagemBolsaPDFCollection
    {

        /// <summary>
        /// Faz a leitura do pdf e carrega a lista de NotaCorretagemBolsaPDF 
        /// </summary>
        /// <param name="s">Vetor de bytes representativo do Arquivo PDF</param>
        /// <returns></returns>
        public List<NotaCorretagemBolsaPDF> RetornaOperacoes(Stream s)
        {
            byte[] streamBytes = Utilitario.ConvertStreamToByteArray(s);

            InputStream input = new ByteArrayInputStream(streamBytes);
            //
            //PDDocument docPdf = PDDocument.load(input);

            //PDFParser parser = new PDFParser((new java.io.FileInputStream(new java.io.File(opnSelArquivo.FileName)));
            PDFParser parser = new PDFParser(input);
            parser.parse();

            COSDocument cos = parser.getDocument();

            // Carrega o arquivo pdf
            PDDocument docPdf = new PDDocument(cos);

            //cria objeto que sera usado para pegar o texto por regiao no documento
            PDFTextStripperByArea docStr = null;

            //cria campos pegados via pedaços de texto
            PDPage pag = null;
            string pagina;
            string recCorretagem;
            string recIss;
            string recTaxaLiquidacao;
            string recRegistroCBLC;
            string recRegistroBolsa;
            string recRegistroBolsa2;
            string recEmolumento;
            string recIsDayTrade;
            string recIsDayTrade2;

            //cria objetos que serao usados para preencher com os valores copiados da nota
            List<NotaCorretagemBolsaPDF> listaOperacao = new List<NotaCorretagemBolsaPDF>();
            NotaCorretagemBolsaPDF notaCorretagemBolsaPDF = null;
            DetalheNotaBolsaPDF detalhe = null;
            String[] arr = { };

            int codigoBovespa = 0;
            string dataPregao = "";
            string codigoCliente = "";

            PosicaoNotas posicoes = new PosicaoNotas();

            //for que inicia a leitura em todos as paginas do pdf
            for (int i = 0; i < docPdf.getDocumentCatalog().getAllPages().size(); i++)
            {
                docStr = new PDFTextStripperByArea();
                docStr.setSortByPosition(true);

                notaCorretagemBolsaPDF = new NotaCorretagemBolsaPDF();
                notaCorretagemBolsaPDF.listaDetalheNotaBolsaPDF = new List<DetalheNotaBolsaPDF>();

                /**
                 * MAPEAMENTO E EXTRACAO DOS VALORES DO CAMPOS DE HEADER E FOOTER
                 **/

                recCorretagem = "Corretagem";
                recIss = "ISS";
                recTaxaLiquidacao = "Taxa de liquidação";
                recRegistroCBLC = "Taxa de Registro";
                recRegistroBolsa = "Taxa de termo/opções";
                recRegistroBolsa2 = "Taxa de opções/futuro";
                recEmolumento = "Emolumentos";
                recIsDayTrade = "I.R.R.F.";
                recIsDayTrade2 = "Day-Trade";

                pag = (PDPage)docPdf.getDocumentCatalog().getAllPages().get(i);

                docStr.addRegion("recCodigoBovespa1", new java.awt.Rectangle(458, 152, 22, 8));
                docStr.addRegion("recCodigoBovespa2", new java.awt.Rectangle(459, 154, 22, 8));
                docStr.addRegion("recDataPregao", new java.awt.Rectangle(530, 49, 40, 9));
                docStr.addRegion("recCodigoCliente1", new java.awt.Rectangle(494, 152, 31, 9));
                docStr.addRegion("recCodigoCliente2", new java.awt.Rectangle(494, 154, 31, 9));
                docStr.addRegion("recPagina", new java.awt.Rectangle(0, 0, Convert.ToInt32(pag.findMediaBox().getWidth()), Convert.ToInt32(pag.findMediaBox().getHeight())));
                docStr.extractRegions(pag);

                //verifica o codigo do cliente e aciona o mapeamento da nota em questao
                codigoBovespa = ConverteInt(LimpaString(docStr.getTextForRegion("recCodigoBovespa1")));
                codigoCliente = LimpaString(docStr.getTextForRegion("recCodigoCliente1"));

                if (codigoBovespa == 0)
                {
                    codigoBovespa = ConverteInt(LimpaString(docStr.getTextForRegion("recCodigoBovespa2")));
                    codigoCliente = LimpaString(docStr.getTextForRegion("recCodigoCliente2"));
                }

                //PEGANDO VALORES DO RODAPE
                pagina = docStr.getTextForRegion("recPagina");

                //remover literias 'corretagem' não referentes ao campo
                pagina = pagina.Replace("NOTA DE CORRETAGEM", "").Replace("Corretagem / Despesas", "").Replace("Total corretagem", "");

                recCorretagem = obtemValor(pagina, recCorretagem).Trim();
                recIss = obtemValor(pagina, recIss).Trim();
                recTaxaLiquidacao = obtemValor(pagina, recTaxaLiquidacao).Trim();
                recRegistroCBLC = obtemValor(pagina, recRegistroCBLC).Trim();
                recRegistroBolsa = obtemValor(pagina, recRegistroBolsa).Trim();
                recEmolumento = obtemValor(pagina, recEmolumento).Trim();
                recIsDayTrade = verificaDayTrade(obtemValor(pagina, recIsDayTrade).Trim());

                //se o registro bolsa estiver vazio, pode ser que a nota tenha o campo com outro nome
                if (recRegistroBolsa.Equals(""))
                    recRegistroBolsa = obtemValor(pagina, recRegistroBolsa2).Trim();

                //se o primeiro campo do day-trade veio vazio, procurar pelo segundo
                if (recIsDayTrade.Equals(""))
                    recIsDayTrade = verificaDayTrade(obtemValor(pagina, recIsDayTrade2).Trim());

                //resgatando os campos
                notaCorretagemBolsaPDF.dataPregao = ConverteDate(LimpaString(docStr.getTextForRegion("recDataPregao")));
                notaCorretagemBolsaPDF.codigoBovespa = codigoBovespa;
                notaCorretagemBolsaPDF.codigoCliente = ConverteInt(codigoCliente);
                notaCorretagemBolsaPDF.corretagem = ConverteDecimal(LimpaString(recCorretagem));
                notaCorretagemBolsaPDF.iss = ConverteDecimal(LimpaString(recIss));
                notaCorretagemBolsaPDF.taxaLiquidacao = ConverteDecimal(LimpaString(recTaxaLiquidacao));
                notaCorretagemBolsaPDF.registroCBLC = ConverteDecimal(LimpaString(recRegistroCBLC));
                notaCorretagemBolsaPDF.registroBolsa = ConverteDecimal(LimpaString(recRegistroBolsa));
                notaCorretagemBolsaPDF.emolumento = ConverteDecimal(LimpaString(recEmolumento));
                notaCorretagemBolsaPDF.irDayTrade = ConverteDecimal(recIsDayTrade);
                /**
                 * FIM - MAPEAMENTO E EXTRACAO DOS VALORES DO CAMPOS DE HEADER E FOOTER
                 **/

                /**
                 * MAPEAMENTO DA LISTA DE DETALHE DA NOTA
                 **/
                //for que percorre as linhas da lista da nota. começa em 215 pois é o inicio da posicao de lista
                for (int l = 220; l <= 508; l = l + 8)
                {
                    docStr = new PDFTextStripperByArea();
                    docStr.setSortByPosition(true);

                    //novo detalhe
                    detalhe = new DetalheNotaBolsaPDF();

                    //mapeamento dos campos da lista da nota
                    docStr.addRegion("recTipoOperacao", new java.awt.Rectangle(92, l, 10, 8));
                    docStr.addRegion("recTipoMercado", new java.awt.Rectangle(104, l, 79, 8));
                    docStr.addRegion("recPrazo", new java.awt.Rectangle(184, l, 22, 8));
                    docStr.addRegion("recAtivo", new java.awt.Rectangle(205, l, 134, 8));
                    docStr.addRegion("recObs", new java.awt.Rectangle(341, l, 29, 8));
                    docStr.addRegion("recQuantidade", new java.awt.Rectangle(372, l, 62, 8));
                    docStr.addRegion("recPu", new java.awt.Rectangle(435, l, 57, 8));
                    docStr.addRegion("recValor", new java.awt.Rectangle(492, l, 73, 8));

                    docStr.extractRegions(pag);

                    detalhe.tipoOperacao = LimpaString(docStr.getTextForRegion("recTipoOperacao"));
                    detalhe.tipoMercado = LimpaString(docStr.getTextForRegion("recTipoMercado"));
                    detalhe.prazo = LimpaString(docStr.getTextForRegion("recPrazo"));
                    detalhe.ativo = LimpaString(docStr.getTextForRegion("recAtivo"));
                    detalhe.obs = LimpaString(docStr.getTextForRegion("recObs"));
                    detalhe.quantidade = ConverteDecimal(docStr.getTextForRegion("recQuantidade"));
                    detalhe.pu = ConverteDecimal(docStr.getTextForRegion("recPu"));
                    detalhe.valor = ConverteDecimal(docStr.getTextForRegion("recValor"));

                    //se o campo tipo de operacao estiver vazio quer dizer que a linha estara em branco
                    if (!detalhe.tipoOperacao.Equals(""))
                    {
                        //se o campo ativo tiver a literal resumo quer dizer que passou do ponto da lista de detalhe
                        if (detalhe.ativo.Contains("Resumo"))
                        {
                            break;
                        }
                        else
                        {
                            //adicionando detalha a operacao
                            notaCorretagemBolsaPDF.listaDetalheNotaBolsaPDF.Add(detalhe);
                        }
                    }
                }

                listaOperacao.Add(notaCorretagemBolsaPDF);
            }

            return listaOperacao;
        }

        protected string obtemValor(string pagina, string campo)
        {
            string ret = pagina.Substring((pagina.IndexOf(campo) + campo.Length), 50).Split('\r')[0];

            string ret2 = "";

            foreach (char a in ret)
            {
                try
                {
                    ret2 = ret2 + Convert.ToInt32(a.ToString());
                }
                catch
                {
                    if (a.ToString().Equals(".") || a.ToString().Equals(",") || a.ToString().Equals(" "))
                        ret2 = ret2 + a.ToString();
                }
            }

            return ret2;
        }

        protected bool VerificaLinhaVazia(String recTipoOperacao, String recTipoMercado, String recPrazo, String recAtivo, String recObs, String recQuantidade, String recPu, String recValor)
        {
            bool vazia = false;

            if (recTipoOperacao.Equals("") &&
                recTipoMercado.Equals("") &&
                    recPrazo.Equals("") &&
                        recAtivo.Equals("") &&
                            recObs.Equals("") &&
                                recQuantidade.Equals("") &&
                                    recPu.Equals("") &&
                                        recValor.Equals(""))
                vazia = true;

            return vazia;
        }

        protected String[] PegaValorDuplicado(String[] arr)
        {
            String[] arrNovo = { "", "" };
            int c = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (!LimpaString(arr[i]).Trim().Equals(""))
                {
                    arrNovo[c] = arr[i];
                    c = c + 1;

                    if (c > 1)
                    {
                        break;
                    }
                }
            }

            return arrNovo;
        }

        protected Boolean VerificaLinhaDuplicada(String[] arr)
        {
            int dupli = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (!LimpaString(arr[i]).Trim().Equals(""))
                {
                    dupli = dupli + 1;
                }

            }

            if (dupli > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //funcao que verifica se o primeiro campo do DayTrade esta preenchido, caso contrario sera procurado na
        //proxima posicao e dentro do retorno sera procurado pelo valor. a string sera lida ao contrario, pois
        //o campo de valor sera na ultima posicao
        protected String verificaDayTrade(String texto)
        {
            char[] arrTexto = texto.ToCharArray();
            int tam = arrTexto.Length;
            int teste = 0;
            String ret = "";
            Boolean para = false;

            if (!texto.Trim().Equals(""))
            {
                //percorrer a string ao contrario procurando pelo valor
                for (int i = 1; i <= tam; i++)
                {
                    try
                    {
                        //se o caracter conseguir converter pra numero, colocar na string de retorno
                        teste = Convert.ToInt32(arrTexto[tam - i].ToString());

                        ret = arrTexto[tam - i].ToString() + ret;
                    }
                    catch (Exception)
                    {
                        //se nao converter, verificar se e "." ou "," e colocar no retorno
                        if (arrTexto[tam - i].ToString().Trim().Equals(".") || arrTexto[tam - i].ToString().Trim().Equals(","))
                        {
                            ret = arrTexto[tam - i].ToString() + ret;
                        }
                        else
                        {
                            //se o campo for diferente de ".", "," ou "" quer dizer que acabou o valor e pode encerrar a procura
                            if (arrTexto[tam - i].ToString().Trim().Equals(""))
                            {
                                para = true;
                            }
                        }
                    }

                    if (para)
                    {
                        break;
                    }
                }
            }

            return ret;
        }

        protected String LimpaString(String texto)
        {
            try
            {
                texto = texto.Replace("\n", "").Replace("\r", "");
            }
            catch (Exception)
            {
                texto = "";
            }

            return texto;
        }

        protected Decimal ConverteDecimal(String texto)
        {
            System.Globalization.CultureInfo i = new System.Globalization.CultureInfo("pt-BR");
            Decimal ret;
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSizes = new int[] { 3 };

            try
            {
                ret = decimal.Parse(texto, i);
            }
            catch (Exception)
            {
                ret = 0m;
            }

            return ret;
        }

        protected int ConverteInt(String texto)
        {
            int ret;

            try
            {
                ret = Convert.ToInt32(texto);
            }
            catch (Exception)
            {
                ret = 0;
            }

            return ret;
        }

        protected DateTime ConverteDate(String texto)
        {
            DateTime ret;

            try
            {
                //ret = DateTime.Parse(texto);
                ret = DateTime.Parse(texto, new System.Globalization.CultureInfo("pt-BR"));
            }
            catch (Exception)
            {
                ret = new DateTime();
            }

            return ret;
        }
    }
}