using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class DbtlCollection {
        private List<Dbtl> collectionDbtl;

        public List<Dbtl> CollectionDbtl {
            get { return collectionDbtl; }
            set { collectionDbtl = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public DbtlCollection() {
            this.collectionDbtl = new List<Dbtl>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbtl">Elemento a inserir na lista de Dbtls</param>
        public void Add(Dbtl dbtl) {
            collectionDbtl.Add(dbtl);
        }
    }
}
