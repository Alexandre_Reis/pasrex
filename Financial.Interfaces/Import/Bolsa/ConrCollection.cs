using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class ConrCollection {
        private List<Conr> collectionConr;

        public List<Conr> CollectionConr {
            get { return collectionConr; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConrCollection() {
            this.collectionConr = new List<Conr>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conr">Elemento a inserir na lista de Conrs</param>
        public void Add(Conr conr) {
            collectionConr.Add(conr);
        }
    }
}
