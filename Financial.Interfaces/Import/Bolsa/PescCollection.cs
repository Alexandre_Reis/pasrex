﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class PescCollection {
        private List<Pesc> collectionPesc;

        public List<Pesc> CollectionPesc {
            get { return collectionPesc; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public PescCollection() {
            this.collectionPesc = new List<Pesc>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Pesc">Elemento a inserir na lista de Pesc</param>
        public void Add(Pesc Pesc) {
            collectionPesc.Add(Pesc);
        }

        /// <summary>
        /// Procura na collection pelo idCliente
        /// </summary>
        /// <param name="codigoBovespaCliente"></param>
        /// <returns></returns>
        //public PescCollection FilterByIdCliente(int codigoBovespaCliente) {
        //    PescCollection PescCollectionAux = new PescCollection();
        //    for (int i = 0; i < this.CollectionPesc.Count; i++) {
        //        Pesc Pesc = this.CollectionPesc[i];
        //        // o tipoRegistro = Header tambem é colocado na collection 
        //        if (Pesc.IsTipoRegistroHeader()) {
        //            PescCollectionAux.Add(Pesc);
        //        }
        //        if (Pesc.CodigoCliente.HasValue && Pesc.CodigoCliente.Value == codigoBovespaCliente) {
        //            PescCollectionAux.Add(Pesc);
        //        }
        //    }

        //    return PescCollectionAux;
        //}
    }
}
