﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Reflection;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class NotaCorretagemORRLNOTA : NotaCorretagem
    {
        #region Estruturas auxiliares para fazer a leitura do XML
        [XmlRoot("XML")]
        public class XMLCabec
        {
            [XmlElement("CORPO")]
            public CorpoCabec corpoCabec;
        }

        public class CorpoCabec
        {
            [XmlElement("DETALHE")]
            public DetalheCabec detalheCabec;
        }

        public class DetalheCabec
        {

            [XmlElement("TP_NOTA")]
            public String tpNota;

            [XmlElement("NR_NOTA")]
            public String nrNota;

            [XmlElement("DT_PREGAO")]
            public String dtPregao;

            [XmlElement("NM_CORRET")]
            public String nmCorret;

            [XmlElement("NM_ENDE_COR")]
            public String nmEndeCor;

            [XmlElement("NR_TELEFONE_COR")]
            public String nrTelefoneCor;

            [XmlElement("CD_INTERNET")]
            public String cdInternet;

            [XmlElement("CD_EMAIL")]
            public String cdEmail;

            [XmlElement("CD_CPFCGC_COR")]
            public String cdCpfCgcCor;

            [XmlElement("CD_CARPAT")]
            public String cdCarPat;

            [XmlElement("CD_CLIENTE")]
            public String cdCliente;

            [XmlElement("DV_CLIENTE")]
            public String dvCliente;

            [XmlElement("NM_CLIENTE")]
            public String nmCliente;

            [XmlElement("CD_CPFCGC_CLI")]
            public String cdCpfCgcCliente;

            [XmlElement("NM_LOGRADOURO")]
            public String nmLogradouro;

            [XmlElement("NR_TELEFONE")]
            public String nrTelefone;

            [XmlElement("NM_LOCAL")]
            public String nmLocal;

            [XmlElement("CD_MEMBRO")]
            public String cdMembro;

            [XmlElement("DV_MEMBRO")]
            public String dvMembro;

            [XmlElement("CD_ASSESSOR")]
            public String cdAssessor;

            [XmlElement("CD_AGECOMP")]
            public String cdAgeComp;

            [XmlElement("DS_AGECOMP")]
            public String dsAgeComp;

            [XmlElement("CD_CLICOMP")]
            public String cdCliComp;

            [XmlElement("VL_LIQNOT_AG")]
            public String vlLiqNotAg;

            [XmlElement("IN_LIQNOT_AG")]
            public String inLiqNotAg;

            [XmlElement("CD_USUA_INST")]
            public String cdUsuaInst;

            [XmlElement("DV_USUA_INST")]
            public String dvUsuaInst;

            [XmlElement("CD_CLIE_INST")]
            public String cdClieInst;

            [XmlElement("DV_CLIE_INST")]
            public String dvClientInst;

            [XmlElement("IN_CONTA_INV")]
            public String inContaInv;

            [XmlElement("CD_BANCO")]
            public String cdBanco;

            [XmlElement("NR_AGENCIA")]
            public String nrAgencia;

            [XmlElement("NR_CTACORR")]
            public String nrCtaCorr;

            [XmlElement("CD_ACIONISTA")]
            public String cdAcionista;

            [XmlElement("CD_ADMIN_CVM")]
            public String cdAdminCVM;

            [XmlElement("NM_COMPL_NOME")]
            public String nmComplNome;

            [XmlElement("sNomeLogoHTML")]
            public String sNomeLogoHTML;

            [XmlElement("sInPessVinc")]
            public String sInPessVinc;

            [XmlElement("NR_TELEFONE_OUVIDORIA")]
            public String nrTelefoneOuvidoria;

            [XmlElement("NM_EMAIL_OUVIDORIA")]
            public String nmEmailOuvidoria;

        }

        [XmlRoot("XML")]
        public class XMLDetalhe
        {
            [XmlElement("CORPO")]
            public CorpoDetalhe corpoDetalhe;
        }

        public class CorpoDetalhe
        {
            public CorpoDetalhe()
            {
                detalheList = new List<Detalhe>();
            }

            [XmlElement("DETALHE")]
            public List<Detalhe> detalheList;
        }

        public class Detalhe
        {
            [XmlElement("IN_LIQUIDA")]
            public String inLiquida;

            [XmlElement("CD_BOLSA")]
            public String cdBolsa;

            [XmlElement("CD_NATOPE")]
            public String cdNatOpe;

            [XmlElement("DS_MERCADO")]
            public String dsMercado;

            [XmlElement("PZ_NEGOCIO")]
            public String pzNegocio;

            [XmlElement("NM_EMPRESA")]
            public String nmEmpresa;

            [XmlElement("NM_ESPECI")]
            public String nmEspeci;

            [XmlElement("DS_OBS")]
            public String dsObs;

            [XmlElement("QT_ORDEXEC")]
            public String qtOrdExec;

            [XmlElement("VL_NEGOCIO")]
            public String vlNegocio;

            [XmlElement("VL_VOLUME")]
            public String vlVolume;

            [XmlElement("IN_VOLUME_DC")]
            public String inVolumeDc;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataNotaCorretagem"></param>
        /// <param name="arquivoHtml">Conteudo do Arquivo HTML a processar</param>
        /// <returns></returns>
        public ResumoOperacoes ProcessNotaCorretagem(DateTime dataNotaCorretagem, InfoArquivoHTML infoArquivoHTML)
        {
            Regex regex = new Regex("(<xml\\b[^>]*>.*?</xml>)", RegexOptions.IgnoreCase);
            Match match = regex.Match(infoArquivoHTML.conteudo);

            #region Parsing do Cabeçalho (tudo que vem no HTML antes da tabela).
            string xml = match.Value;

            XmlSerializer serializer = new XmlSerializer(typeof(XMLCabec));

            StringReader stringReader = new StringReader(xml);
            XMLCabec xmlCabec = (XMLCabec)serializer.Deserialize(stringReader);
            #endregion

            xml = match.NextMatch().Value;
            serializer = new XmlSerializer(typeof(XMLDetalhe));
            stringReader = new StringReader(xml);

            XMLDetalhe xmlDetalhe = (XMLDetalhe)serializer.Deserialize(stringReader);

            ResumoOperacoes resumoOperacoes = new ResumoOperacoes();

            resumoOperacoes.DataPregao = DateTime.ParseExact(xmlCabec.corpoCabec.detalheCabec.dtPregao, "dd/MM/yyyy", null);
            //resumoOperacoes.AgenteMercado = (string)xmlCabec.corpoCabec.detalheCabec.nmCorret.Trim();
            //resumoOperacoes.CodigoAgenteMercado = Int32.Parse(Regex.Match(xmlCabec.corpoCabec.detalheCabec.cdCarPat, @"\d+").Value, NumberStyles.AllowThousands);
            resumoOperacoes.CodigoAgenteMercado = Int32.Parse(xmlCabec.corpoCabec.detalheCabec.cdMembro, NumberStyles.AllowThousands);
                          
            resumoOperacoes.Codigo = String.Format("{0}-{1}", xmlCabec.corpoCabec.detalheCabec.cdCliente.Trim(), xmlCabec.corpoCabec.detalheCabec.dvCliente.Trim());

            //Loop por cada uma das linhas da tabela de detalhes
            int j = 0;
            foreach (Detalhe linha in xmlDetalhe.corpoDetalhe.detalheList)
            {
                j++;
                Operacao operacao = new Operacao();

                string tipoMercado = linha.dsMercado;
                if (tipoMercado == TipoDoMercadoNotaCorretagem.Vista 
                    || tipoMercado == TipoDoMercadoNotaCorretagem.Fracionario
                    || tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra
                    || tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda
                    || tipoMercado == TipoDoMercadoNotaCorretagem.Termo)
                {
                    //Quando mercado a vista o ativo é identificado por sua descrição e não por seu código
                    operacao.AtivoDescricao = linha.nmEmpresa.Trim();
                    string ativoEspecificacao = linha.nmEspeci.Trim();
                    if (ativoEspecificacao.Contains(" "))
                    {
                        ativoEspecificacao = (ativoEspecificacao.Split(' '))[0];
                    }
                    operacao.AtivoEspecificacao = ativoEspecificacao;
                }
                else if (tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoCompra || tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoVenda)
                {
                    operacao.Ativo = linha.nmEmpresa.Trim();
                }
                else
                {
                    throw new Exception("\nNota Corretagem não Processada. Tipo de mercado não suportado: " + tipoMercado);
                }

                operacao.Quantidade = Int32.Parse(linha.qtOrdExec, NumberStyles.AllowThousands);
                operacao.Preco = Decimal.Parse(linha.vlNegocio);
                operacao.Valor = Math.Abs(Decimal.Parse(linha.vlVolume));
                operacao.Tipo = linha.cdNatOpe.ToUpper()[0];
                operacao.TipoMercado = linha.dsMercado;

                int prazo;
                if(Int32.TryParse(linha.pzNegocio, out prazo)){
                    operacao.Prazo = prazo;
                }

                resumoOperacoes.Operacoes.Add(operacao);
            }

            return resumoOperacoes;
        }
    }
}