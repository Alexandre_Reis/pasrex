﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Cdix {
        // Informação sobre o tipo registro da linha do arquivo Cdix
        private string tipoRegistro;
        
        // bool indicando se cdix deve ser salvo na colecao de Cdixs
        // Somente será salvo o Tipo de Registro do Cdix que for necessario
        private bool cdixProcessado;

        #region Properties dos Valores do Registro00 - Header
        
        int? codigoAgente; // Campo 2.2 - Codigo do usuario
        DateTime? dataMovimento; // Campo 07
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores do Registro01 - Identificação dos Dividendos

        int? codigoCliente; // Campo 03

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        string codigoIsin; // Campo 06

        public string CodigoIsin {
            get { return codigoIsin; }
            set { codigoIsin = value; }
        }

        int? distribuicao; // Campo 07

        public int? Distribuicao {
            get { return distribuicao; }
            set { distribuicao = value; }
        }

        decimal? quantidadeBase; // Campo 10

        public decimal? QuantidadeBase {
            get { return quantidadeBase; }
            set { quantidadeBase = value; }
        }

        DateTime? dataAssembleia; // Campo 11

        public DateTime? DataAssembleia {
            get { return dataAssembleia; }
            set { dataAssembleia = value; }
        }

        DateTime? dataInicioProvento; // Campo 12

        public DateTime? DataInicioProvento {
            get { return dataInicioProvento; }
            set { dataInicioProvento = value; }
        }

        DateTime? dataPagamento; // Campo 15

        public DateTime? DataPagamento {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }

        decimal? valor; // Campo 16

        public decimal? Valor {
            get { return valor; }
            set { valor = value; }
        }

        decimal? percentualIR; // Campo 17

        public decimal? PercentualIR {
            get { return percentualIR; }
            set { percentualIR = value; }
        }

        /* Indica se é Dividendo, Juro s/ Capital, Rendimento ou qualquer outro provento em dinheiro */
        int? tipoProvento; // Campo 20

        public int? TipoProvento {
            get { return tipoProvento; }
            set { tipoProvento = value; }
        }

        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser incializado
        /// Somente as variaveis que armazenam informacoes do arquivo Cdix devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.cdixProcessado = false;
            //
            this.codigoAgente = null;
            this.dataMovimento = null;
            //
            this.codigoCliente = null;
            this.codigoIsin = null;
            this.distribuicao = null;
            this.quantidadeBase = null;
            this.dataAssembleia = null;
            this.dataInicioProvento = null;
            this.dataPagamento = null;
            this.valor = null;
            this.percentualIR = null;
            this.tipoProvento = null;
            //
        }

        /// <summary>
        ///  Indica se Cdix possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return cdixProcessado;
        }

        /// <summary>
        /// A partir de um Arquivo Cdix Existente Gera um novo Arquivo Cdix somente 
        /// com os clientes definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoCdix">Caminho Completo do Arquivo Cdix</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo Cdix será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o Cdix</param>
        /// <returns>true se novo arquivo Cdix foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoCdixNaoEncontradoException">Se Arquivo Cdix de entrada não existe</exception>
        public bool GeraArquivoCdix(string nomeArquivoCompletoCdix, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoCdix)) {
                throw new ArquivoCdixNaoEncontradoException("Arquivo Cdix não Encontrado: " + nomeArquivoCompletoCdix);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo

            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();
            
            string pathSaida = diretorioSaidaArquivo + "cdix_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);
            
            using (StreamReader sr = new StreamReader(nomeArquivoCompletoCdix)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {
                        
                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se dado pertence a algum dos clientes
                        if(this.IsDadoCliente(linha.Trim(),listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion
           
            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo Cdix pertence a algum dos clientes da lista
        /// </summary>
        /// <param name="linhaCdix">linha do arquivo Cdix</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo Cdix pertence a algum dos clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaCdix, List<int> listaIdCliente) {
            bool retorno = true;
                        
            switch (linhaCdix.Substring(0,2)) {
                case EstruturaArquivoCDIX.Header:
                case EstruturaArquivoCDIX.Trailer:
                    break;
                case EstruturaArquivoCDIX.IdentificacaoCliente:
                    int codigoCliente = Convert.ToInt32(linhaCdix.Substring(8, 7)); // Campo 04
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
                case EstruturaArquivoCDIX.IdentificacaoDividendo:
                    int cdCliente = Convert.ToInt32(linhaCdix.Substring(7, 7)); // Campo 03
                    retorno = listaIdCliente.Contains(cdCliente);
                    break;
            }
            
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <exception cref="ProcessaCdixException">Se ocorrer Erro no Processamento do Arquivo</exception>
        public CdixCollection ProcessaCdix(StreamReader sr, DateTime data)
        {
            CdixCollection cdixCollection = new CdixCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataCdix(linha, data);
                        // Somente é salvo Cdix que possui dados relevantes
                        Cdix cdix = (Cdix)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            cdixCollection.Add(cdix);
                        }
                    }
                    i++;
                    System.Console.WriteLine(i);
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                throw new ProcessaCdixException(e.Message);
            }

            return cdixCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <exception cref="ArquivoCdixNaoEncontradoException">Se arquivo Cdix não Encontrado</exception>
        /// <exception cref="ProcessaCdixException">Se ocorrer Erro no Processamento do Arquivo</exception>
        public CdixCollection ProcessaCdix(string nomeArquivoCompleto, DateTime data) 
        {            
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoCdixNaoEncontradoException("Arquivo CDIX " + nomeArquivoCompleto + " não encontrado");
            }
                               
            CdixCollection cdixCollection = new CdixCollection();
            int i = 1;
            string linha = "";                
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {                    
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataCdix(linha, data);
                            // Somente é salvo Cdix que possui dados relevantes
                            Cdix cdix = (Cdix)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                cdixCollection.Add(cdix);
                            }
                        }
                        i++;
                        System.Console.WriteLine(i); 
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                throw new ProcessaCdixException(e.Message);                
            }

            return cdixCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// throws ArgumentException se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoCDIX
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoCDIX");

            List<string> valoresPossiveis = EstruturaArquivoCDIX.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo CDIX
            switch (tipoRegistro) {
                case EstruturaArquivoCDIX.Header:
                    this.tipoRegistro = EstruturaArquivoCDIX.Header;
                    break;
                case EstruturaArquivoCDIX.IdentificacaoCliente:
                    this.tipoRegistro = EstruturaArquivoCDIX.IdentificacaoCliente;
                    break;
                case EstruturaArquivoCDIX.IdentificacaoDividendo:
                    this.tipoRegistro = EstruturaArquivoCDIX.IdentificacaoDividendo;
                    break;
                case EstruturaArquivoCDIX.Trailer:
                    this.tipoRegistro = EstruturaArquivoCDIX.Trailer;
                    break;
            }
            #endregion
        }

        // Salva os atributos do arquivo Cdix de acordo com o tipo
        private void TrataCdix(string linha, DateTime data) {
            // Limpa os valores do Cdix
            this.Initialize();

            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoCDIX.Header) {
                this.codigoAgente = Convert.ToInt32( linha.Substring(6, 4) ); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(39, 8); // Campo 07
                int dia = Convert.ToInt32( dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoCdixIncorretoException("Arquivo CDIX com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));
                }
                         
                // Indica que Cdix deve ser salvo                
                this.cdixProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCDIX.IdentificacaoCliente) {
            }
            else if (this.tipoRegistro == EstruturaArquivoCDIX.IdentificacaoDividendo) {
                #region IdentificacaoDividendo
                this.codigoCliente = Convert.ToInt32(linha.Substring(7, 7)); // Campo 03    
                
                /* OBS: Está indicado posição 25 mas deve ser posição 35 */
                this.codigoIsin = linha.Substring(34, 12).Trim(); // Campo 06
                //
                this.distribuicao = Convert.ToInt32(linha.Substring(46, 3)); // Campo 07
                //
                Int64 quantidadeBaseInteiro = Convert.ToInt64(linha.Substring(71, 15)); // Campo 10
                int quantidadeBaseFracionario = Convert.ToInt32(linha.Substring(86, 3));
                decimal quantidadeBaseFracionarioDecimal = quantidadeBaseFracionario / 1000M;
                this.quantidadeBase = quantidadeBaseInteiro + quantidadeBaseFracionarioDecimal;
                //
                // Formato = "AAAAMMDD"
                string dataAssembleia = linha.Substring(89,8); // Campo 11
                int dia = Convert.ToInt32(dataAssembleia.Substring(6, 2));
                int mes = Convert.ToInt32(dataAssembleia.Substring(4, 2));
                int ano = Convert.ToInt32(dataAssembleia.Substring(0, 4));
                this.dataAssembleia = new DateTime(ano, mes, dia);
                //              
                // Formato = "AAAAMMDD"
                string dataInicioProvento = linha.Substring(97, 8); // Campo 12
                dia = Convert.ToInt32(dataInicioProvento.Substring(6, 2));
                mes = Convert.ToInt32(dataInicioProvento.Substring(4, 2));
                ano = Convert.ToInt32(dataInicioProvento.Substring(0, 4));
                this.dataInicioProvento = new DateTime(ano, mes, dia);
                //              
                // Formato = "AAAAMMDD"
                string dataPagamento = linha.Substring(118, 8); // Campo 15
                dia = Convert.ToInt32(dataPagamento.Substring(6, 2));
                mes = Convert.ToInt32(dataPagamento.Substring(4, 2));
                ano = Convert.ToInt32(dataPagamento.Substring(0, 4));
                // Data Invalida - 31/12/4000
                if(dia == 0 && mes == 0 && ano == 0) {
                    this.dataPagamento = new DateTime(4000,12,31);
                }
                else {
                    this.dataPagamento = new DateTime(ano, mes, dia);
                }                                
                //
                Int64 valorInteiro = Convert.ToInt64(linha.Substring(126, 16)); // Campo 16
                int valorFracionario = Convert.ToInt32(linha.Substring(142, 2));
                decimal valorFracionarioDecimal = valorFracionario / 100M;
                this.valor = valorInteiro + valorFracionarioDecimal;
                // 
                int percentualIRInteiro = Convert.ToInt32(linha.Substring(144, 3)); // Campo 17
                int percentualIRFracionario = Convert.ToInt32(linha.Substring(147, 2));
                decimal percentualIRFracionarioDecimal = percentualIRFracionario / 100M;
                this.percentualIR = percentualIRInteiro + percentualIRFracionarioDecimal;
                //
                /* Indica se é Dividendo, Juro s/ Capital, Rendimento ou qq outro provento em dinheiro */
                this.tipoProvento = Convert.ToInt32(linha.Substring(151, 3)); // Campo 20
                //
                #endregion

                // Indica que Cdix deve ser salvo
                this.cdixProcessado = true;
            }
            //
            else if (this.tipoRegistro == EstruturaArquivoCDIX.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é IdentificacaoDividendo</returns>
        public bool IsTipoRegistroIdentificacaoDividendo() {
            return this.tipoRegistro == EstruturaArquivoCDIX.IdentificacaoDividendo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoCDIX.Header;
        }

    }
}
