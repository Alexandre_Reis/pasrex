﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace Financial.Interfaces.Import.Bolsa {
    public class CsgdCollection {
        private List<Csgd> collectionCsgd;

        public List<Csgd> CollectionCsgd {
            get { return collectionCsgd; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CsgdCollection() {
            this.collectionCsgd = new List<Csgd>();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csgd">Elemento a inserir na lista de Csgd</param>
        public void Add(Csgd csgd) {
            collectionCsgd.Add(csgd);
        }

        /// <summary>
        /// Procura na collection pelo idCliente
        /// </summary>
        /// <param name="codigoBovespaCliente"></param>
        /// <returns></returns>
        public CsgdCollection FilterByIdCliente(int codigoBovespaCliente) {
            CsgdCollection csgdCollectionAux = new CsgdCollection();
            for (int i = 0; i < this.CollectionCsgd.Count; i++) {
                Csgd csgd = this.CollectionCsgd[i];
                // o tipoRegistro = Header tambem é colocado na collection 
                if (csgd.IsTipoRegistroHeader()) {
                    csgdCollectionAux.Add(csgd);
                }
                if (csgd.CodigoCliente.HasValue && csgd.CodigoCliente.Value == codigoBovespaCliente) {
                    csgdCollectionAux.Add(csgd);
                }
            }

            return csgdCollectionAux;
        }
    }
}
