﻿using System;

namespace Financial.Interfaces.Import.Bolsa.Exceptions {            
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException: Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção ArquivoBDinNaoEncontrado
    /// </summary>
    public class ArquivoBDinNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoBDinNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoBDinNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoBDinIncorretoException
    /// </summary>
    public class ArquivoBDinIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoBDinIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoBDinIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaBDinException
    /// </summary>
    public class ProcessaBDinException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaBDinException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaBDinException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoConlNaoEncontrado
    /// </summary>
    public class ArquivoConlNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoConlNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoConlNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoConlIncorretoException
    /// </summary>
    public class ArquivoConlIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoConlIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoConlIncorretoException(string mensagem) : base(mensagem) { }
    }
    
    /// <summary>
    /// Exceção ProcessaConlException
    /// </summary>
    public class ProcessaConlException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaConlException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaConlException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDbtcNaoEncontradoException
    /// </summary>
    public class ArquivoDbtcNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDbtcNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDbtcNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDBTCIncorretoException
    /// </summary>
    public class ArquivoDbtcIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDbtcIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDbtcIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaDbtcException
    /// </summary>
    public class ProcessaDbtcException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaDbtcException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaDbtcException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDbtlNaoEncontradoException
    /// </summary>
    public class ArquivoDbtlNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDbtlNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDbtlNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDbtlIncorretoException
    /// </summary>
    public class ArquivoDbtlIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDbtlIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDbtlIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaDbtlException
    /// </summary>
    public class ProcessaDbtlException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaDbtlException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaDbtlException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoProdNaoEncontradoException
    /// </summary>
    public class ArquivoProdNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoProdNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoProdNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoProdIncorretoException
    /// </summary>
    public class ArquivoProdIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoProdIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoProdIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaProdException
    /// </summary>
    public class ProcessaProdException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaProdException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaProdException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCsgdNaoEncontradoException
    /// </summary>
    public class ArquivoCsgdNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCsgdNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCsgdNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCSGDIncorretoException
    /// </summary>
    public class ArquivoCsgdIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCsgdIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCsgdIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCsgdException
    /// </summary>
    public class ProcessaCsgdException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCsgdException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCsgdException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoPosrNaoEncontradoException
    /// </summary>
    public class ArquivoPosrNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPosrNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPosrNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCSGDIncorretoException
    /// </summary>
    public class ArquivoPosrIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPosrIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPosrIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaPosrException
    /// </summary>
    public class ProcessaPosrException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPosrException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPosrException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoPapdNaoEncontradoException
    /// </summary>
    public class ArquivoPapdNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPapdNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPapdNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoPapdIncorretoException
    /// </summary>
    public class ArquivoPapdIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPapdIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPapdIncorretoException(string mensagem) : base(mensagem) { }
    }
    
    /// <summary>
    /// Exceção ProcessaPapdException
    /// </summary>
    public class ProcessaPapdException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPapdException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPapdException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoConrNaoEncontradoException
    /// </summary>
    public class ArquivoConrNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoConrNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoConrNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoConrIncorretoException
    /// </summary>
    public class ArquivoConrIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoConrIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoConrIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaConrException
    /// </summary>
    public class ProcessaConrException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaConrException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaConrException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCdixNaoEncontradoException
    /// </summary>
    public class ArquivoCdixNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCdixNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCdixNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCdixIncorretoException
    /// </summary>
    public class ArquivoCdixIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCdixIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCdixIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCdixException
    /// </summary>
    public class ProcessaCdixException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCdixException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCdixException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoNegsNaoEncontradoException
    /// </summary>
    public class ArquivoNegsNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoNegsNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoNegsNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoNegsIncorretoException
    /// </summary>
    public class ArquivoNegsIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoNegsIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoNegsIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCdixException
    /// </summary>
    public class ProcessaNegsException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaNegsException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaNegsException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCmdfNaoEncontradoException
    /// </summary>
    public class ArquivoCmdfNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCmdfNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCmdfNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCmdfIncorretoException
    /// </summary>
    public class ArquivoCmdfIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCmdfIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCmdfIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCmdfException
    /// </summary>
    public class ProcessaCmdfException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCmdfException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCmdfException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoGVDatIncorretoException
    /// </summary>
    public class ArquivoGVDatIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoGVDatIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoGVDatIncorretoException(string mensagem) : base(mensagem) { }
    }
    
    /// <summary>
    /// Exceção ProcessaGVDatException
    /// </summary>
    public class ProcessaGVDatException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaGVDatException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaGVDatException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCmvdNaoEncontradoException
    /// </summary>
    public class ArquivoCmvdNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCmvdNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCmvdNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCmvdIncorretoException
    /// </summary>
    public class ArquivoCmvdIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCmvdIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCmvdIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCvmdException
    /// </summary>
    public class ProcessaCvmdException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCvmdException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCvmdException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCrcaNaoEncontradoException
    /// </summary>
    public class ArquivoCrcaNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCrcaNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCrcaNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCrcaIncorretoException
    /// </summary>
    public class ArquivoCrcaIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCrcaIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCrcaIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCrcaException
    /// </summary>
    public class ProcessaCrcaException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCrcaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCrcaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoPescNaoEncontradoException
    /// </summary>
    public class ArquivoPescNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPescNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPescNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoPescIncorretoException
    /// </summary>
    public class ArquivoPescIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoPescIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoPescIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaPescException
    /// </summary>
    public class ProcessaPescException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPescException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPescException(string mensagem) : base(mensagem) { }
    }
}


