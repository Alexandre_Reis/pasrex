﻿using System;
using System.Text;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Util;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa.Enums;

namespace Financial.Interfaces.Import.Bolsa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Dbtl {
        // Informação sobre o tipo registro da linha do arquivo DBTL
        private string tipoRegistro;
        
        // bool indicando se dbtl deve ser salvo na colecao de Dbtls
        // Somente será salvo o Tipo de Registro do Dbtl que for necessario
        private bool dbtlProcessado;

        #region Properties dos Valores do Registro00 - Header
        
        int? codigoAgente; // Campo 02 - Codigo do usuario
        DateTime? dataMovimento; // Campo 06
        
        /// <summary>
        /// 
        /// </summary>
        public int? CodigoAgente {
            get { return codigoAgente; }
            set { codigoAgente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataMovimento {
            get { return dataMovimento; }
            set { dataMovimento = value; }
        }
        
        #endregion

        #region Properties dos Valores Comuns do Registro01 e Registro 03

        int? numeroContrato; // Campo 02
        string pontaEmprestimo; //  Campo 03 - D = Doador, T = Tomador
        int? codigoCliente; // Campo 04        
        DateTime? dataLiquidacao; // Campo 05
        
        /// <summary>
        /// 
        /// </summary>
        public int? NumeroContrato
        {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PontaEmprestimo
        {
            get { return pontaEmprestimo; }
            set { pontaEmprestimo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoCliente
        {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }
        #endregion

        #region Properties dos Valores do Registro01 - Relação das Liquidações por Clientes e Contratos

        Int64? quantidade; // Campo 06
        string codigoCarteira; // Campo 09
        Int64? quantidadeInadimplente; // Campo 10
              
        /// <summary>
        /// 
        /// </summary>
        public Int64? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CodigoCarteira {
            get { return codigoCarteira; }
            set { codigoCarteira = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int64? QuantidadeInadimplente {
            get { return quantidadeInadimplente; }
            set { quantidadeInadimplente = value; }
        }
                            
        #endregion

        #region Properties dos Valores do Registro03 - Relação de Lançamentos - Detalhado por Contratos do Cliente

        string descricao; // Campo 07
        decimal? valorLancamento; // Campo 08
                
        /// <summary>
        /// 
        /// </summary>
        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? ValorLancamento {
            get { return valorLancamento; }
            set { valorLancamento = value; }
        }
        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser inicializado
        /// Somente as variaveis que armazenam informacoes do arquivo ConL devem ser inicializadas
        /// </summary>
        private void Initialize() {                                                                                    
            this.dbtlProcessado = false;
            /* registro00 */
            #region registro00
            this.codigoAgente = null;
            this.dataMovimento = null;            
            #endregion
            /* registro01 */
            #region registro01
            this.quantidade = null;
            this.codigoCarteira = null;
            this.quantidadeInadimplente = null;
            #endregion
            /* registro03 */
            #region registro03
            this.descricao = null;
            this.valorLancamento = null;
            #endregion
            /* registro01 e registro03 */
            #region registro01 e registro03
            this.numeroContrato = null;
            this.pontaEmprestimo = null;
            this.codigoCliente = null;
            this.dataLiquidacao = null;
            #endregion
        }

        /// <summary>
        ///  Indica se DBTL possui dados que devem ser salvos
        /// </summary>
        /// <returns></returns>
        private bool HasValue() {
            return dbtlProcessado;
        }

        /// <summary>
        /// A partir de um Arquivo DBTL Existente Gera um novo Arquivo DBTL somente 
        /// com os Clientes Definidos
        /// </summary>
        /// <param name="nomeArquivoCompletoDBTL">Caminho Completo do Arquivo DBTL</param>
        /// <param name="diretorioSaidaArquivo">Diretório de Saida onde o arquivo DBTL será Gerado</param>
        /// <param name="listaIdCliente">Lista de Clientes Desejados para o DBTL</param>
        /// <returns>true se novo arquivo DBTL foi Gerado
        ///          false caso ocorra algum erro
        /// </returns>
        /// <exception cref="ArquivoDBTLNaoEncontradoException">Se Arquivo DBTL de entrada não existe</exception>
        public bool GeraArquivoDBTL(string nomeArquivoCompletoDBTL, string diretorioSaidaArquivo, List<int> listaIdCliente) {
            if (!File.Exists(nomeArquivoCompletoDBTL)) {
                throw new ArquivoDbtlNaoEncontradoException("Arquivo DBTL não Encontrado: " + nomeArquivoCompletoDBTL);
            }

            // Coloca a barra no Diretório se não Tiver
            if (Utilitario.Right(diretorioSaidaArquivo, 1) != "/" && Utilitario.Right(diretorioSaidaArquivo, 1) != "\\") {
                diretorioSaidaArquivo += "/";
            }

            #region Leitura do Arquivo Original e Geração de novo Arquivo
            string dataHoje = (DateTime.Now.Day < 10) ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            dataHoje += (DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            dataHoje += DateTime.Now.Year.ToString();

            string pathSaida = diretorioSaidaArquivo + "dbtl_" + dataHoje + "_out.txt";

            string linha = "";
            StreamWriter arquivoSaida = new StreamWriter(pathSaida, false, Encoding.ASCII);

            using (StreamReader sr = new StreamReader(nomeArquivoCompletoDBTL)) {
                while ((linha = sr.ReadLine()) != null) {
                    if (!String.IsNullOrEmpty(linha)) {

                        #region Escreve no Arquivo - Filtro por Clientes
                        // Se Dado Pertence a algum dos Clientes
                        if (this.IsDadoCliente(linha.Trim(), listaIdCliente)) {
                            arquivoSaida.WriteLine(linha.Trim());
                        }
                        #endregion
                    }
                }
            }
            //Fecha o arquivo
            arquivoSaida.Close();

            #endregion

            return File.Exists(pathSaida);
        }

        /// <summary>
        /// Determina se Linha do Arquivo DBTL pertence a algum dos Clientes da Lista
        /// </summary>
        /// <param name="linhaDBTL">linha do arquivo DBTL</param>
        /// <param name="listaIdCliente"></param>
        /// <returns>true se linha do Arquivo DBTL pertence a algum dos Clientes da lista
        ///          false caso contrário
        /// </returns>
        private bool IsDadoCliente(string linhaDBTL, List<int> listaIdCliente) {
            bool retorno = true;

            switch (linhaDBTL.Substring(0, 2)) {
                case EstruturaArquivoDBTL.Header:
                case EstruturaArquivoDBTL.Trailer:
                    break;
                case EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato:
                case EstruturaArquivoDBTL.RelacaoLancamentosTotalContratoCliente:                    
                    int codigoCliente = Convert.ToInt32(linhaDBTL.Substring(11, 7)); // Campo 04
                    retorno = listaIdCliente.Contains(codigoCliente);
                    break;
                case EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente:
                    // No arquivo Bovespa está 10,7
                    int cdCliente = Convert.ToInt32(linhaDBTL.Substring(11, 7)); // Campo 04
                    retorno = listaIdCliente.Contains(cdCliente);
                    break;
            }

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="ArquivoDbtlNaoEncontradoException">Se Arquivo DBTL não existir</exception>
        /// <exception cref="ArquivoDbtlIncorretoException">Se arquivo DBTL está incorreto</exception>
        public DbtlCollection ProcessaDbtl(string nomeArquivoCompleto, DateTime data) {            

            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoDbtlNaoEncontradoException("Arquivo DBTL não encontrado: " + nomeArquivoCompleto);
            }
                        
            DbtlCollection dbtlCollection = new DbtlCollection();
            int i = 1;
            string linha = "";  
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.GetTipoRegistro(linha.Substring(0, 2));
                            this.TrataDbtl(linha, data);
                            // Somente é Salvo DBTL que possui dados relevantes
                            Dbtl dbtl = (Dbtl)Utilitario.Clone(this);
                            if (this.HasValue()) {
                                dbtlCollection.Add(dbtl);
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Dbtl com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaDbtlException(mensagem.ToString());
            }
        
            return dbtlCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="ArquivoDbtlIncorretoException">Se arquivo DBTL está incorreto</exception>
        public DbtlCollection ProcessaDbtl(StreamReader sr, DateTime data)
        {
            DbtlCollection dbtlCollection = new DbtlCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(linha))
                    {
                        this.GetTipoRegistro(linha.Substring(0, 2));
                        this.TrataDbtl(linha, data);
                        // Somente é Salvo DBTL que possui dados relevantes
                        Dbtl dbtl = (Dbtl)Utilitario.Clone(this);
                        if (this.HasValue())
                        {
                            dbtlCollection.Add(dbtl);
                        }
                    }
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Dbtl com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaDbtlException(e.Message);
            }

            return dbtlCollection;
        }
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoRegistro"></param>
        /// <exception cref="ArgumentException">Se tipoRegistro não está dentro dos valores possiveis do enum 
        /// EstruturaArquivoDBTL</exception>
        private void GetTipoRegistro(string tipoRegistro) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoRegistro incorreto. Os valores possiveis estão no enum EstruturaArquivoDBTL");

            List<string> valoresPossiveis = EstruturaArquivoDBTL.Values();
            if (!valoresPossiveis.Contains(tipoRegistro)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            #region Prenche o TipoRegistro do Arquivo DBTL
            switch (tipoRegistro) {
                case EstruturaArquivoDBTL.Header:
                    this.tipoRegistro = EstruturaArquivoDBTL.Header;
                    break;
                case EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato:
                    this.tipoRegistro = EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato;
                    break;
                case EstruturaArquivoDBTL.RelacaoLancamentosTotalContratoCliente:
                    this.tipoRegistro = EstruturaArquivoDBTL.RelacaoLancamentosTotalContratoCliente;
                    break;
                case EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente:
                    this.tipoRegistro = EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente;
                    break;
                case EstruturaArquivoDBTL.Trailer:
                    this.tipoRegistro = EstruturaArquivoDBTL.Trailer;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// Salva os Atributos do arquivo Dbtl de acordo com o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataDbtl(string linha, DateTime data) {
            // Limpa os valores do Dbtl
            this.Initialize();

            #region Tratamento do Header
            // Tratamento por tipo de registro - Salva somente alguns campos 
            if (this.tipoRegistro == EstruturaArquivoDBTL.Header) {
                
                this.codigoAgente = Convert.ToInt32( linha.Substring(6, 4) ); // Campo 2.2
                // Formato = "AAAAMMDD"
                string dataMovimento = linha.Substring(30, 8); // Campo 07
                int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
                int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
                int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
                //
                this.dataMovimento = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataMovimento.Value.CompareTo(data) != 0) {
                    throw new ArquivoDbtlIncorretoException("Arquivo DBTL com data incorreta: " + this.dataMovimento.Value.ToString("dd/MM/yyyy"));                                                            
                }

                // Indica que DBTL deve ser salvo                
                this.dbtlProcessado = true;
            }
            #endregion
            
            #region Tratamento de RelacaoLiquidacoesClienteContrato
            else if (this.tipoRegistro == EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato) {
                //
                this.numeroContrato = Convert.ToInt32(linha.Substring(2, 8)); // Campo 02
                this.pontaEmprestimo = linha.Substring(10, 1); // Campo 03
                this.codigoCliente = Convert.ToInt32(linha.Substring(11, 7)); // Campo 04
                //
                // Formato = "AAAAMMDD"
                string dataLiquidacao = linha.Substring(18, 8); // Campo 05
                int dia = Convert.ToInt32(dataLiquidacao.Substring(6, 2));
                int mes = Convert.ToInt32(dataLiquidacao.Substring(4, 2));
                int ano = Convert.ToInt32(dataLiquidacao.Substring(0, 4));
                this.dataLiquidacao = new DateTime(ano, mes, dia);
                //
                this.quantidade = Convert.ToInt64(linha.Substring(26, 18)); // Campo 06
                this.codigoCarteira = linha.Substring(57, 5).Trim(); // Campo 09
                this.quantidadeInadimplente = Convert.ToInt64(linha.Substring(62, 18)); // Campo 10
                
                // Indica que DBTL deve ser salvo
                this.dbtlProcessado = true;
            }
            #endregion
            
            else if (this.tipoRegistro == EstruturaArquivoDBTL.RelacaoLancamentosTotalContratoCliente) {
            }

            #region Tratamento de RelacaoLancamentosDetalhadoContratoCliente
            else if (this.tipoRegistro == EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente) {
                this.numeroContrato = Convert.ToInt32(linha.Substring(2, 8)); // Campo 02
                this.pontaEmprestimo = linha.Substring(10, 1); // Campo 03
                this.codigoCliente = Convert.ToInt32(linha.Substring(11, 7)); // Campo 04
                //
                // Formato = "AAAAMMDD"
                string dataLiquidacao = linha.Substring(18, 8); // Campo 05
                int dia = Convert.ToInt32(dataLiquidacao.Substring(6, 2));
                int mes = Convert.ToInt32(dataLiquidacao.Substring(4, 2));
                int ano = Convert.ToInt32(dataLiquidacao.Substring(0, 4));
                this.dataLiquidacao = new DateTime(ano, mes, dia);
                //                
                this.descricao = linha.Substring(31, 60).Trim(); // Campo 07
                //
                int valorLancamentoInteiro = Convert.ToInt32(linha.Substring(91, 16)); // Campo 08
                int valorLancamentoFracionario = Convert.ToInt32(linha.Substring(107, 2));
                decimal valorLancamentoFracionarioDecimal = valorLancamentoFracionario / 100M;
                this.valorLancamento = valorLancamentoInteiro + valorLancamentoFracionarioDecimal;
                //
                // Indica que DBTL deve ser salvo
                /* Se descricao = "REMUNERACAO BRUTA DO DOADOR" ou			
                                = "VALOR DO IR SOBRE REMUNERACAO DO DOADOR"
                                = "BRUTO TOMADOR" registro é salvo, caso contrario não
                */
                List<string> listaDescricaoAceita = new List<string>();
                listaDescricaoAceita.Add("REMUNERACAO BRUTA DO DOADOR");
                listaDescricaoAceita.Add("VALOR DO IR SOBRE REMUNERACAO DO DOADOR");
                listaDescricaoAceita.Add("BRUTO TOMADOR");

                if (!listaDescricaoAceita.Contains(this.descricao))
                    this.dbtlProcessado = false;
                else {
                    this.dbtlProcessado = true;
                }
            }
            #endregion
            
            else if (this.tipoRegistro == EstruturaArquivoDBTL.Trailer) {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoDBTL.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é LiquidacaoClienteContrato</returns>
        public bool IsTipoRegistroLiquidacaoClienteContrato() {
            return this.tipoRegistro == EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é LancamentosDetalhadoContratoCliente</returns>
        public bool IsTipoRegistroLancamentosDetalhadoContratoCliente() {
            return this.tipoRegistro == EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection
        /// </summary>
        /// <param name="dbtl"></param>        
        /// <returns> Filtra pelo tipo registro 3 </returns>
        public bool FilterDbtlByTipoRegistroDetalhado(Dbtl dbtl) {
            return dbtl.tipoRegistro == EstruturaArquivoDBTL.RelacaoLancamentosDetalhadoContratoCliente;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection
        /// </summary>
        /// <param name="dbtl"></param>        
        /// <returns> Filtra pelo tipo registro 1 </returns>
        public bool FilterDbtlByTipoRegistroRelacaoLiquidacoesClienteContrato(Dbtl dbtl) {
            return dbtl.tipoRegistro == EstruturaArquivoDBTL.RelacaoLiquidacoesClienteContrato;
        }
    }
}
