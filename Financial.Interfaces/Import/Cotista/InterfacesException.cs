﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.Cotista.Exceptions {
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    #region PosicaoCotistaYMF
    /// <summary>
    /// Exceção ProcessaPosicaoCotistaYMFException
    /// </summary>
    public class ProcessaPosicaoCotistaYMFException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPosicaoCotistaYMFException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPosicaoCotistaYMFException(string mensagem) : base(mensagem) { }
    }
    #endregion
}