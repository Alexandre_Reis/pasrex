﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;

namespace Financial.Interfaces.Import {
    public class Numerca {

        public Numerca() { }


        #region Enums
        /*
     * PRE = Taxa Pré-Fixada
     * DOL = Cupom de Dólar (Dólar Comercial) 
     * TR = Taxa Referencial 
     * SEL = Taxa Selic 
     * ANB = Taxa ANBID 
     * IGPD = Índice Geral de Preços – DI 
     * TMS = Taxa TMS 
     * DI1 = Taxa CDI CETIP 
     * DOF = Cupom de Dólar (Dólar Flutuante) 
     * IGP = Índice Geral de Preços 
     * TBF = Taxa Básica de Juros 
     * TJL = Taxa TJLP 
     * INPC = Taxa INPC 
     * DI F = Contrato Futuro Taxa DI 
     * INCC = Índice Nacional de Custo da Construção 
     * IP = Índice de Poupança 
     * IPC = Índice de Preços ao Consumidor - FIPE
     * IPCA = Índice Nacional de Preços ao Consumidor Amplo - IBGE 
     * JPY = IENE
    */
        public enum Indexador {
            PRE = 0,
            DOL = 1,
            TR = 2,
            SEL = 3,
            ANB = 4,
            IGPD = 5,
            TMS = 6,
            DI1 = 7,
            DOF = 8,
            IGP = 9,
            TBF = 10,
            TJL = 11,
            INPC = 12,
            DIF = 13,
            INCC = 14,
            IP = 15,
            IPC = 16,
            IPCA = 17,
            JPY = 18,
            NONE = 99
        }

        public enum Acao {
            N = 0, //Novo
            D = 1, //Inativado
            A = 2, //Alterado
            //
            NONE = 99
        }

        public enum TipoEmissaoEnum {
            D = 0, //Doméstica
            I = 1, //Internacional
            //
            NONE = 99
        }

        public enum TipoAtivoObjetoEnum {
            S = 0, //AÇÃO
            T = 1, //COMMODITIES
            I = 2, //ÍNDICES
            F = 3, //FUTURO
            B = 4, //CESTA
            D = 5, //TAXA DE JUROS
            C = 6, //MOEDA
            O = 7, //OPÇÃO
            W = 8, //SWAP
            M = 9, //OUTROS
            // Desconhecidos
            A = 10, // Desconhecido
            E = 11, // Desconhecido
            //
            NONE = 99
        }

        public enum TipoEntregaEnum {
            P = 0, //FÍSICA
            C = 1, //FINANCEIRA
            //
            NONE = 99
        }

        public enum TipoFundoEnum {
            C = 0, //Fechado
            O = 1, //Aberto
            //
            NONE = 99
        }

        public enum TipoGarantiaEnum {
            T = 0,  //GARANTIDO PELO TESOURO
            G = 1,  //GARANTIA P/ TERCEIROS (FIANÇA)
            S = 2,  //GARANTIA REAL
            UF = 3, //FLUTUANTE
            US = 4, //SUBORDINADA
            UQ = 5, //QUIROGRAFÁRIA
            //
            NONE = 99
        }

        public enum TipoJurosEnum {
            Z = 0, //ZERO (TAXA DE DESCONTO)
            F = 1, //FIXO
            V = 1, //VARIÁVEL (DEFAULT)
            //
            NONE = 99
        }

        public enum TipoMercadoEnum {
            S = 0, //PADRONIZADA
            N = 1, //NÃO PADRONIZADA
            //
            NONE = 99
        }

        
        public enum TipoStatusEnum {
            N = 0, //ISIN NOVO 
            R = 1, //ISIN REUTILIZADO
            //
            NONE = 99
        }

        public enum TipoVencimentoEnum {
            A = 0,  //PLANO DE AMORTIZAÇÃO
            B = 1,  //PLANO AMORTIZAÇÃO C/ RESGATE ANTECIPADO
            Q = 2,  //PERPETUO COM OPÇÃO DE RESGATE
            P = 3,  //PERPETUO
            F = 4,  //VENCIMENTO FIXO
            G = 5,  //RESGATE ANTECIPADO
            //
            NONE = 99
        }

        public enum TipoProtecaoEnum {
            N = 0, //PROTEGIDO
            S = 1, //NAO PROTEGIDO
            //
            NONE = 99
        }

        public enum TipoPoliticaDistribuicaoJurosEnum {
            I = 0, //Fundo de Renda
            G = 1, //Fundo de Crescimento
            M = 2, //Fundo Misto
            //
            NONE = 99
        }

        public enum TipoAtivoInvestidoFundoEnum {
            R = 0, //Fundo de Investimento Imobiliário
            S = 1, //Fundo de Investimento em Títulos e Valores Mobiliários e Instrumentos Financeiros
            M = 2, //Fundo de Investimentos em Ativos Diferentes - Misto
            C = 2, //Fundo de Investimentos em Commodities
            D = 2, //Fundo de Investimentos em Derivativos 
            //
            NONE = 99
        }

        public enum TipoFormaEnum {
            B = 0, //PORTADOR
            S = 1, //PORTADOR OU NOMINATIVO
            O = 2, //OUTROS
            R = 3, //NOMINATIVO (DEFAULT)
            //
            NONE = 99
        }

        public enum TipoEstiloOpcaoEnum {
            A = 0, //AMERICANA
            E = 1, //EUROPÉIA
            //
            NONE = 99
        }

        public enum CodigoFrequenciaJurosEnum {
            A = 0, //ANUAL
            B = 1, //BI-ANUAL
            M = 2, //MENSAL
            N = 3, //NÃO APLICÁVEL
            Q = 4, //TRIMESTRAL
            S = 5, //SEMESTRAL 
            W = 6, //SEMANAL
            X = 7, //OUTROS 
            //
            NONE = 99
        }

        public enum SituacaoIsinEnum {
            A = 0, //ATIVO (DEFAULT)
            I = 1, //INATIVO
            //
            NONE = 99
        }
        #endregion


        #region Estrutura do arquivo
        [DelimitedRecord(",")]
        //[IgnoreLast(1)]
        [IgnoreEmptyLines()]
        public class NumercaStructure {

            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()] // Quoted with "
            public DateTime? dataGeracaoArquivo;

            //[FieldQuoted()]
            //[FieldNullValue(Acao.NONE)]
            //public Acao acaoSofrida;
            
            [FieldQuoted()]
            public string acaoSofrida;

            [FieldQuoted()]
            public string codigoIsin;

            [FieldQuoted()]
            public string codigoEmissor;

            [FieldQuoted()]
            public string codigoCFI;

            [FieldQuoted()]
            public string descricao;

            [FieldQuoted()]
            public int? anoEmissao;

            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()]
            public DateTime? dataEmissao;

            [FieldQuoted()]
            public int? anoExpiracao;

            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()]
            public DateTime? dataExpiracao;

            [FieldQuoted()]
            public string taxaJuros;

            [FieldQuoted()]
            public string moedas;

            [FieldQuoted()]
            public decimal? valorNominal;

            [FieldQuoted()]
            public decimal? precoExercicio;

            //[FieldQuoted()]
            //[FieldNullValue(Indexador.NONE)]
            //[FieldConverter(typeof(ConvertEnum))] /* di1 e DI F */
            //public Indexador indexador;

            [FieldQuoted()]                       
            public string indexador;

            [FieldQuoted()]
            public decimal? percentualIndexador;

            [FieldQuoted()]
            public string dataAcao;

            [FieldQuoted()]
            public string codigoCetip;

            [FieldQuoted()]
            public string codigoSelic;

            [FieldQuoted()]
            public string codigoPais;

            [FieldQuoted()]
            public string tipoAtivo;

            [FieldQuoted()]
            public string codigoCategoria;

            [FieldQuoted()]
            public string codigoEspecie;

            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()]
            public DateTime? dataBase;

            [FieldQuoted()]
            public string numeroEmissao;

            [FieldQuoted()]
            public string numeroSerie;

            //[FieldQuoted()]
            //[FieldNullValue(TipoEmissaoEnum.NONE)]
            //public TipoEmissaoEnum tipoEmissao;

            [FieldQuoted()]            
            public string tipoEmissao;

            //[FieldQuoted()]
            //[FieldNullValue(TipoAtivoObjetoEnum.NONE)]
            //public TipoAtivoObjetoEnum tipoAtivoObjeto;

            [FieldQuoted()]
            public string tipoAtivoObjeto;

            //[FieldQuoted()]
            //[FieldNullValue(TipoEntregaEnum.NONE)]
            //public TipoEntregaEnum tipoEntrega;

            [FieldQuoted()]            
            public string tipoEntrega;

            //[FieldQuoted()]
            //[FieldNullValue(TipoFundoEnum.NONE)]
            //public TipoFundoEnum tipoFundo;

            [FieldQuoted()]
            public string tipoFundo;

            //[FieldQuoted()]
            //[FieldNullValue(TipoGarantiaEnum.NONE)]
            //public TipoGarantiaEnum tipoGarantia;

            [FieldQuoted()]            
            public string tipoGarantia;

            //[FieldQuoted()]
            //[FieldNullValue(TipoJurosEnum.NONE)]
            //public TipoJurosEnum tipoJuros;

            [FieldQuoted()]
            public string tipoJuros;


            //[FieldQuoted()]
            //[FieldNullValue(TipoMercadoEnum.NONE)]
            //public TipoMercadoEnum tipoMercado;

            [FieldQuoted()]            
            public string tipoMercado;


            //[FieldQuoted()]
            //[FieldNullValue(TipoStatusEnum.NONE)]
            //public TipoStatusEnum tipoStatusIsin;

            [FieldQuoted()]            
            public string tipoStatusIsin;


            //[FieldQuoted()]
            //[FieldNullValue(TipoVencimentoEnum.NONE)]
            //public TipoVencimentoEnum tipoVencimento;

            [FieldQuoted()]            
            public string tipoVencimento;

            //[FieldQuoted()]
            //[FieldNullValue(TipoProtecaoEnum.NONE)]
            //public TipoProtecaoEnum tipoProtecao;

            [FieldQuoted()]            
            public string tipoProtecao;

            
            //[FieldQuoted()]
            //[FieldNullValue(TipoPoliticaDistribuicaoJurosEnum.NONE)]
            //public TipoPoliticaDistribuicaoJurosEnum tipoPoliticaDistribuicaoJuros;

            [FieldQuoted()]            
            public string tipoPoliticaDistribuicaoJuros;

            //[FieldQuoted()]
            //[FieldNullValue(TipoAtivoInvestidoFundoEnum.NONE)]
            //public TipoAtivoInvestidoFundoEnum tipoAtivoInvestidoFundo;

            [FieldQuoted()]            
            public string tipoAtivoInvestidoFundo;


            //[FieldQuoted()]
            //[FieldNullValue(TipoFormaEnum.NONE)]
            //public TipoFormaEnum tipoForma;

            [FieldQuoted()]            
            public string tipoForma;


            //[FieldQuoted()]
            //[FieldNullValue(TipoEstiloOpcaoEnum.NONE)]
            //public TipoEstiloOpcaoEnum tipoEstiloOpcao;

            [FieldQuoted()]            
            public string tipoEstiloOpcao;


            [FieldQuoted()]
            public string numSerieOpcao;

            //[FieldQuoted()]
            //[FieldNullValue(CodigoFrequenciaJurosEnum.NONE)]
            //public CodigoFrequenciaJurosEnum codigoFrequenciaJuros;

            [FieldQuoted()]            
            public string codigoFrequenciaJuros;

            //[FieldQuoted()]
            //[FieldNullValue(SituacaoIsinEnum.NONE)]
            //public SituacaoIsinEnum situacaoIsin;

            
            [FieldQuoted()]            
            public string situacaoIsin;
            
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            [FieldQuoted()]
            public DateTime? dataPrimeiroPagamentoJuros;
        }

        internal class ConvertEnum : ConverterBase {

            /// <summary>
            /// Tratamento para DI F e di1
            /// </summary>
            /// <param name="from"></param>
            /// <returns></returns>
            public override object StringToField(string from) {
                from = from.ToUpper();

                if (from == "DI F") {
                    from = "DIF";
                }

                Indexador tipo = (Indexador)Enum.Parse(typeof(Indexador), from);

                return tipo;
            }
        } 
        #endregion


        /// <summary>
        /// Lê um arquivo txt e armazena numa estrutura
        /// </summary>
        /// <param name="arquivo">path completo do arquivo</param>
        /// <returns></returns>
        public NumercaStructure[] ImportaNumerca(string arquivo) {
            
            FileHelperEngine<NumercaStructure> engine = new FileHelperEngine<NumercaStructure>();

            NumercaStructure[] numerca = engine.ReadFile(arquivo);

            return numerca;
        }        
    }
}
