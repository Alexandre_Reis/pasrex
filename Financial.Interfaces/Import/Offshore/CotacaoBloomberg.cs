using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.OffShore.Exceptions;
using Mommosoft.Bloomberg;
using System.Net;
using System.Configuration;
using System.Globalization;

namespace Financial.Interfaces.Import.Offshore 
{
    public class CotacaoBloomberg
    {
        private DateTime data;
        private string cdAtivo;
        private decimal pu;

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public string CdAtivo
        {
            get { return cdAtivo; }
            set { cdAtivo = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }

        public List<CotacaoBloomberg> CarregaCotacaoBloomberg(List<string> ativos, List<string> espec, DateTime dataInicio, DateTime dataFim)
        {
            List<CotacaoBloomberg> lista = new List<CotacaoBloomberg>();

            BloombergDataProvider provider = new BloombergDataProvider();

            int i = 0;
            foreach (string cdAtivo in ativos)
            {
                string ativoFormatado = cdAtivo;
                if (espec[i] != "")
                {
                    ativoFormatado = ativoFormatado + ":" + espec[i];
                }
                List<Bar> bars = provider.GetMarketData(ativoFormatado, dataInicio, dataFim);

                foreach (Bar bar in bars)
                {
                    CotacaoBloomberg cotacaoBloomberg = new CotacaoBloomberg();
                    cotacaoBloomberg.CdAtivo = cdAtivo;
                    cotacaoBloomberg.Data = bar.TimeStamp;
                    cotacaoBloomberg.Pu = Convert.ToDecimal(bar.Close);
                    lista.Add(cotacaoBloomberg);
                }

                i++;
            }

            return lista;            
        }
    }
    
}


namespace Mommosoft.Bloomberg
{
    public class Bar
    {
        public Bar(DateTime timeStamp, double close)
        {
            TimeStamp = timeStamp;
            Close = close;
        }


        public DateTime TimeStamp;
        public double Close;

        public override string ToString()
        {
            return string.Format("{0}:\t{1}", TimeStamp.ToShortDateString(), Close);
        }
    }

    public class BloombergDataProvider
    {
        private static string UrlTemplate = @"http://www.bloomberg.com/apps/data?pid=webpxta&Securities=[symbol]&TimePeriod=[period]&Outfields=HDATE,PR005-H";


        public List<Bar> GetMarketData(string symbol, DateTime start, DateTime end)
        {
            List<Bar> bars = new List<Bar>();
            string period = GetPeriod(start);
            string nSymbol = NormalizeSymbol(symbol);
            string url = UrlTemplate;
            url = url.Replace("[period]", period);
            url = url.Replace("[symbol]", nSymbol);
            string dataFeed = String.Empty;
            //Debug.WriteLine(url);

            using (WebClient wc = new WebClient())
            {
                string proxyInfo = ConfigurationManager.AppSettings["WebServiceProxy"];

                if (!string.IsNullOrEmpty(proxyInfo))
                {
                    //Utilizar padrao de proxy definido no Windows
                    wc.Proxy = System.Net.WebProxy.GetDefaultProxy();
                    wc.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    if (proxyInfo != "default")
                    {
                        //Um proxy especifico foi configurado
                        string[] proxyInfos = proxyInfo.Split(';');

                        //Setar url do proxy
                        wc.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                        if (proxyInfos.Length > 1)
                        {
                            //Foram passadas tambem as credenciais de rede
                            wc.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                        }
                    }
                }

                dataFeed = wc.DownloadString(url);

            }

            // trim off unused characters from end of line
            dataFeed = dataFeed.Replace("\r", "");
            // split to array on end of line
            string[] rows = dataFeed.Split('\n');
            string[] rowValues;

            // first row is a header
            for (int i = rows.Length - 1; i > 1; i--)
            {
                rowValues = rows[i].Split('"');


                if (rowValues != null && rowValues.Length >= 2)
                {
                    DateTime timeStamp = DateTime.ParseExact(rowValues[0], "yyyyMMdd", CultureInfo.InvariantCulture);

                    if ((start <= timeStamp) && (timeStamp <= end))
                    {
                        double close = double.Parse(rowValues[1], CultureInfo.InvariantCulture);
                        bars.Add(new Bar(timeStamp, close));
                    }
                    if (timeStamp < start) break;
                }
            }
            return bars;
        }

        private string NormalizeSymbol(string symbol)
        {
            if (symbol.Contains(":")) return symbol;
            return string.Format("{0}:US", symbol);
        }
        private string GetPeriod(DateTime timeStamp)
        {
            double days = (DateTime.Today - timeStamp).TotalDays;
            if (days < 30)
            {
                //int months = (int)(days / 30) + 1;
                //months = (months > 0) ? months : 1;
                return string.Format("{0}M", 1);
            }
            int years = (int)(days / 365);
            years = (years > 6) ? 6 : (years < 1) ? 1 : years;
            return string.Format("{0}Y", years);
        }
    }
}
