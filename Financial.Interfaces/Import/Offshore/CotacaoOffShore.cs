﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.OffShore.Exceptions;

namespace Financial.Interfaces.Import.Offshore {

    /// <summary>
    /// Armazena os Valores de Cotação Bolsa presentes num arquivo .csv proveniente do yahoo
    /// </summary>
    [Serializable]
    public class CotacaoOffShore {
        private DateTime data;
        private string cdAtivo;
        private decimal pu;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string CdAtivo {
            get { return cdAtivo; }
            set { cdAtivo = value; }
        }

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        /// <summary>
        /// Faz o Download do yahoo dos PUs dos Ativos e carrega na Lista de Cotações
        /// </summary>
        /// <param name="ativosOffshore">Cada elemento da lista tem até 100 ativos separados por Virgula</param>
        /// <param name="data"></param>
        /// <param name="path">Path Completo do Diretorio onde o Arquivo com as Cotações Offshore será Procurado</param>
        /// <returns>Lista com os ativos Offshore e seus Pu</returns>
        /// <exception cref="ArquivoOffShoreNaoEncontradoException">Se download falhou e arquivo quotes_1.csv não existe</exception>
        /// <exception cref="ProcessaOffShoreException">Se Processamento de algum arquivo offshore falhou</exception>
        public List<CotacaoOffShore> ProcessaCotacaoOffShore(List<string> ativosOffshore, DateTime data, string path) 
        {
            try
            {
                // Apaga Arquivos Gerados
                #region Apaga Arquivos Gerados
                for (int j = 0; j < ativosOffshore.Count; j++) {
                    int numeroAux = j + 1;
                    string nomeArquivo = path + "quotes_" + numeroAux + ".csv";

                    if (File.Exists(nomeArquivo))
                    {
                        File.Delete(nomeArquivo);
                    }
                }
                #endregion
            }
            catch (Exception)
            {                
                
            }  

            // Download Files - tamanho de ativosOffshore determina o numero de Downloads
            this.DownloadCotacaoOffShore(ativosOffshore, path);

            // Se arquivo não existe dá exceção                                                                    
            if (!File.Exists( path + "quotes_1.csv")) {
                throw new ArquivoOffShoreNaoEncontradoException("Download falhou. Arquivo OffShore não encontrado");
            }

            // Processa N arquivos onde tamanho N = ativosOffshore.Count
            List<CotacaoOffShore> listaRetorno = new List<CotacaoOffShore>();

            for (int i = 0; i < ativosOffshore.Count; i++) {
                int numero = i + 1;
                string nomeArquivo = path + "quotes_" + numero + ".csv";
                string linha = "";
                try {
                    // Using fecha o StreamReader
                    using (StreamReader sr = new StreamReader(nomeArquivo)) {
                        while ((linha = sr.ReadLine()) != null)
                        {
                            if (!String.IsNullOrEmpty(linha)) 
                            {                            
                                this.TrataOffShore(linha, data);
                                CotacaoOffShore offshore = (CotacaoOffShore)Utilitario.Clone(this);
                                // Adiciona na lista de retorno o objeto CotacaoOffshore
                                listaRetorno.Add(offshore);
                            }
                        }
                    }
                }
                catch (Exception e) {

                    // Apaga Arquivos Gerados
                    #region Apaga Arquivos Gerados
                    for (int j = 0; j < ativosOffshore.Count; j++) {
                        int numeroAux = j + 1;
                        string nomeArquivoAux = path + "quotes_" + numeroAux + ".csv";

                        if (File.Exists(nomeArquivoAux)) {
                            File.Delete(nomeArquivo);
                        }
                    }
                    #endregion

                    throw new ProcessaOffShoreException(e.Message);
                }
            }
                        
            return listaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Data Cotação</param>
        /// <param name="linha">Linha do Arquivo OffShore</param>
        private void TrataOffShore(string linha, DateTime data) {
            string[] dados = linha.Split(new Char[] { ',' });
            //       
            this.cdAtivo = dados[0];

            if (dados[1] == "N/A")
            {
                this.pu = 0;
            }
            else
            {
                this.pu = Convert.ToDecimal(dados[1].Replace(".", ","));
            }
            //
            this.data = data;
        }

        /// <summary>
        /// Baixa o Arquivo OffShore da Internet do Site do yahoo
        /// </summary>
        /// <param name="ativosOffshore"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool DownloadCotacaoOffShore(List<string> ativosOffshore, string path) 
        {            
            #region Url
            // Tamanho da Lista ativosOffshore = numero de Downloads Necessário
            List<string> url = new List<string>(ativosOffshore.Count);
            
            for (int i = 0; i < ativosOffshore.Count; i++) {
                url.Add(@"http://finance.yahoo.com/d/quotes.csv?s=" + ativosOffshore[i] + "&f=sp");                
            }            
            #endregion

            #region Download
            for (int i = 0; i < url.Count; i++) {
                int numero = i + 1;
                string nomeArquivoDestino = "quotes_" + numero + ".csv";
                string pathArquivoDestino = path + nomeArquivoDestino;
                //
                bool download = Utilitario.DownloadFile(url[i], pathArquivoDestino);
            }
            #endregion

            // Se arquivo quotes_1.csv existe retorna true
            return File.Exists(path + "quotes_1.csv");
        }
    }
}