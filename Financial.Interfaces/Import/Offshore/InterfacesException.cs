﻿using System;

namespace Financial.Interfaces.Import.OffShore.Exceptions {            
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException: Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção ArquivoOffShoreNaoEncontrado
    /// </summary>
    public class ArquivoOffShoreNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoOffShoreNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoOffShoreNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaOffShoreException
    /// </summary>
    public class ProcessaOffShoreException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaOffShoreException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaOffShoreException(string mensagem) : base(mensagem) { }
    }
}