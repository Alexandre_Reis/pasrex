﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using Financial.Interfaces.Import.BMF.Exceptions;
using Financial.Interfaces.Import.BMF.Enums;

namespace Financial.Interfaces.Import.BMF {
    
    [Serializable]
    public class RComiesp {
        // Informação sobre o tipo registro do arquivoRComiesp
        private string tipoRegistro;

        #region Properties dos registros

        DateTime? data;

        public DateTime? Data {
            get { return data; }
            set { data = value; }
        }
        
        int? codigoAgenteCorretora; // Campo 1

        public int? CodigoAgenteCorretora {
            get { return codigoAgenteCorretora; }
            set { codigoAgenteCorretora = value; }
        }

        int? codigoCliente; // Campo 2

        public int? CodigoCliente {
            get { return codigoCliente; }
            set { codigoCliente = value; }
        }

        int? numeroNegocio; // Campo 3

        public int? NumeroNegocio {
            get { return numeroNegocio; }
            set { numeroNegocio = value; }
        }

        string codigoMercadoria; // Campo 6

        public string CodigoMercadoria {
            get { return codigoMercadoria; }
            set { codigoMercadoria = value; }
        }

        int? tipoMercado; // Campo 7

        public int? TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        string serie; // Campo 8

        public string Serie {
            get { return serie; }
            set { serie = value; }
        }

        string tipoOperacao; // Campo 9

        public string TipoOperacao {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        decimal? preco; // Campo 11

        public decimal? Preco {
            get { return preco; }
            set { preco = value; }
        }

        int? quantidade; // Campo 12

        public int? Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        #endregion

        /// <summary>
        /// TipoRegistro nao deve ser inicializado
        /// Somente as variaveis que armazenam informacoes do arquivo RComiesp devem ser inicializadas
        /// </summary>
        private void Initialize() {
            this.data = null;
            this.codigoAgenteCorretora = null;
            this.codigoCliente = null;
            this.numeroNegocio = null;
            this.codigoMercadoria = null;
            this.tipoMercado = null;
            this.serie = null;
            this.tipoOperacao = null;
            this.preco = null;
            this.quantidade = null;            
        }

        /// <summary>
        /// Carrega a collection de RComiesp a partir do arquivo RComiesp. 
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="ArquivoRComiespNaoEncontradoException">Se Arquivo RComiesp não for encontrado</exception>
        public RComiespCollection ProcessaRComiesp(string nomeArquivoCompleto, DateTime data) {
            if (!File.Exists(nomeArquivoCompleto)) {
                throw new ArquivoRComiespNaoEncontradoException("Arquivo RComiesp: {0} não encontrado" + nomeArquivoCompleto);
            }

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere DataArquivo
            StreamReader srAux = new StreamReader(nomeArquivoCompleto.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataReferencia = linhaAux.Substring(0, 8); // Campo 01
            int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
            int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
            int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0) {
                throw new ArquivoRComiespIncorretoException("Arquivo ArquivoRComiesp {0} incorreto " + dataMovimentoAux);
            }            
            #endregion

            RComiespCollection rcomiespCollection = new RComiespCollection();
            int i = 1;
            string linha = "";
            try {
                using (StreamReader sr = new StreamReader(nomeArquivoCompleto.ToString(), Encoding.Default))
                {
                    while ((linha = sr.ReadLine()) != null) {
                        this.SetTipoRegistro(i);
                        this.TrataRComiesp(linha);
                        RComiesp rcomiesp = (RComiesp)Utilitario.Clone(this);
                        rcomiespCollection.Add(rcomiesp); ;
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento RComiesp com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaRComiespException(mensagem.ToString());
            }

            return rcomiespCollection;
        }

        /// <summary>
        /// Carrega a collection de RComiesp a partir do arquivo RComiesp. 
        /// </summary>
        /// <param name="sr"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public RComiespCollection ProcessaRComiesp(StreamReader sr, DateTime data)
        {
            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere DataArquivo           
            StreamReader srAux = new StreamReader(sr.BaseStream);
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataReferencia = linhaAux.Substring(0, 8); // Campo 01
            int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
            int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
            int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0) {
                throw new ArquivoRComiespIncorretoException("Arquivo ArquivoRComiesp {0} incorreto " + dataMovimentoAux);
            }
            #endregion

            RComiespCollection rcomiespCollection = new RComiespCollection();
            int i = 1;
            string linha = "";
            try
            {
                while ((linha = sr.ReadLine()) != null)
                {
                    this.SetTipoRegistro(i);
                    this.TrataRComiesp(linha);
                    RComiesp rcomiesp = (RComiesp)Utilitario.Clone(this);
                    rcomiespCollection.Add(rcomiesp); ;
                    i++;
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento RComiesp com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaRComiespException(e.Message);
            }

            return rcomiespCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo RComiesp. 
        /// </summary>
        /// <param name="linha"></param>
        private void TrataRComiesp(string linha) {
            // Limpa os valores do RComiesp
            this.Initialize();           

            #region EstruturaArquivoRComiesp
            if (this.IsTipoRegistroHeader()) {
                // Formato = "AAAAMMDD"
                string dataReferencia = linha.Substring(0, 8); // Campo 01
                int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
                int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
                int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
                //
                this.data = new DateTime(ano, mes, dia);
            }
            else if (this.IsTipoRegistroDetalhes()) {
                this.codigoAgenteCorretora = Convert.ToInt32(linha.Substring(0, 6));
                this.codigoCliente = Convert.ToInt32(linha.Substring(6, 6));
                this.numeroNegocio = Convert.ToInt32(linha.Substring(12, 5));
                this.codigoMercadoria = linha.Substring(25, 3).Trim();
                this.tipoMercado = Convert.ToInt32(linha.Substring(28, 1));
                this.serie = linha.Substring(29, 4).Trim();
                this.tipoOperacao = linha.Substring(33, 1).Trim();
                //
                string sinalCotacao = linha.Substring(34, 1);
                //if-else
                decimal preco = sinalCotacao == "-"
                                ? -1 * (Convert.ToDecimal(linha.Substring(35, 15)) / 1000)
                                : Convert.ToDecimal(linha.Substring(35, 15)) / 1000;
                this.preco = preco;
                this.quantidade = Convert.ToInt32(linha.Substring(50, 5));
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha">numero da linha do arquivo</param>
        /// Se linha = 1 tipoRegistro = Header
        /// Caso Contrario tipoRegistro = Detalhes
        private void SetTipoRegistro(int linha) {
            this.tipoRegistro = linha == 1 ? EstruturaArquivoRComiesp.Header : EstruturaArquivoRComiesp.Detalhes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoRComiesp.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Detalhes</returns>
        public bool IsTipoRegistroDetalhes() {
            return this.tipoRegistro == EstruturaArquivoRComiesp.Detalhes;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Detalhes
        /// </summary>
        /// <param name="prod"></param>
        /// 
        /// <returns> true se TipoRegistro = Detalhes está na collection        
        ///           false se TipoRegistro = Detalhes não está na collection 
        /// </returns>
        public bool FilterRComiespByTipoRegistroDetalhes(RComiesp rcomiesp) {
            bool retorno = rcomiesp.tipoRegistro == EstruturaArquivoRComiesp.Detalhes;
            return retorno;
        }
    }
}
