using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF
{
    public class TarPregCollection
    {
        private List<TarPreg> collectionTarPreg;

        public List<TarPreg> CollectionTarPreg
        {
            get { return collectionTarPreg; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public TarPregCollection()
        {
            this.collectionTarPreg = new List<TarPreg>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tarPreg">Elemento a inserir na lista de TarPreg</param>
        public void Add(TarPreg tarPreg)
        {
            collectionTarPreg.Add(tarPreg);
        }
    }
}
