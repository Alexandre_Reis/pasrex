﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Financial.Interfaces.Import.BMF.Enums;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.BMF
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class CodISIND
    {
        #region Properties dos registros
        DateTime? data;

        public DateTime? Data
        {
            get { return data; }
            set { data = value; }
        }

        string cdAtivoBMF;

        public string CdAtivoBMF
        {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        string serie;

        public string Serie
        {
            get { return serie; }
            set { serie = value; }
        }
        #endregion

        private void Initialize()
        {
            this.cdAtivoBMF = null;            
            this.serie = null;
            this.data = null;            
        }

        /// <summary>
        /// Carrega a collection de CodISIND a partir do arquivo CodISIND.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo CodISIND será procurado</param>
        /// throws ArquivoCodISINDNaoEncontradoException
        public CodISINDCollection ProcessaCodISIND(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);            
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("CodISIND").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadCodISIND(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoCodISINDNaoEncontradoException("Arquivo CodISIND: " + nomeArquivo.ToString() + " não encontrado\n");
                }
            }

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere dataArquivo
            StreamReader srAux = new StreamReader(nomeArquivo.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataMovimento = linhaAux.Substring(11, 8); // Campo 01
            dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
            ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0)
            {
                throw new ArquivoCodISINDIncorretoException("Arquivo CodISIND "+dataMovimentoAux.ToString("dd/MM/yyyy")+" incorreto \n");
            }            
            #endregion

            CodISINDCollection codISINDCollection = new CodISINDCollection();
            int i = 1;
            string linha = "";
            try
            {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivo.ToString()))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        this.TrataCodISIND(linha);
                        CodISIND codISIND = (CodISIND)Utilitario.Clone(this);
                        codISINDCollection.Add(codISIND);
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento CodISIND com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaCodISINDException(e.Message);
            }

            return codISINDCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo CodISIND de acordo com o tipo.
        /// </summary>
        /// <param name="linha"></param>
        private void TrataCodISIND(string linha)
        {
            // Limpa os valores do CodISIND
            this.Initialize();

            #region EstruturaArquivoCodISIND
            // Formato = "AAAAMMDD"
            string dataMovimento = linha.Substring(11, 8); // Campo 01
            int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            int mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
            int ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
            //
            this.data = new DateTime(ano, mes, dia);
            this.cdAtivoBMF = linha.Substring(21, 3).Trim();
            this.serie = linha.Substring(26, 4).Trim();            
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo CodISIND da Internet. Descompacta o arquivo.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo BDPregao</param>
        /// <returns></returns>
        private bool DownloadCodISIND(DateTime data, string path)
        {
            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            string endereco = "http://www.bmf.com.br/FTP/ContratosPregaoFinal/BF" + dataString.ToString() + ".ex_";
            string nomeArquivoDestino = "BF" + dataString.ToString() + ".exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path)
                       .Append("CodISIND").Append(dataString).Append(".txt");

            #region Unzip do Arquivo
            if (download) {
                // Deleta o arquivo codISIND.txt se existir
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("CodISIND.txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion

                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try
                //{
                //    p.Start();
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine("Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion

                Archive arquivo = new Archive();
                //
                //arquivo.Comment = "";
                //arquivo.DestinationDirectory = ".";
                //arquivo.ExcludePattern = "";
                arquivo.PreservePath = true;
                //arquivo.SynchronizingObject = this;
                //arquivo.TempFileDirectory = "";
                //arquivo.VolumeLabel = "";
                //arquivo.Exception += new Dart.PowerTCP.Zip.ExceptionEventHandler(this.archive1_Exception);
                //arquivo.BusyChanged += new System.EventHandler(this.archive1_BusyChanged);
                //arquivo.Progress += new Dart.PowerTCP.Zip.ProgressEventHandler(this.archive1_Progress);
                //arquivo.EndUnzip += new Dart.PowerTCP.Zip.EndEventHandler(this.archive1_EndUnzip);

                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;

                //arquivo.Open(pathArquivoDestino);
                //arquivo.BeginQuickUnzip(pathArquivoDestino, path, null);
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion

            // Se arquivo CodISIND+data.txt existe retorna true            
            return File.Exists(nomeArquivoNovo.ToString());
        }
        
    }
}