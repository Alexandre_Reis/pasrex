﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Financial.Interfaces.Import.BMF.Enums;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.BMF
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class BdPregao
    {
        #region Properties dos registros
        DateTime? data;

        public DateTime? Data
        {
            get { return data; }
            set { data = value; }
        }

        string cdAtivoBMF;

        public string CdAtivoBMF
        {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        string serie;

        public string Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        string tipoSerie;

        public string TipoSerie
        {
            get { return tipoSerie; }
            set { tipoSerie = value; }
        }

        int tipoMercado;

        public int TipoMercado
        {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        decimal? precoExercicio;

        public decimal? PrecoExercicio
        {
            get { return precoExercicio; }
            set { precoExercicio = value; }
        }

        decimal peso;

        public decimal Peso
        {
            get { return peso; }
            set { peso = value; }
        }

        DateTime? dataVencimento;

        public DateTime? DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        int? numeroDiasUteis;

        public int? NumeroDiasUteis
        {
            get { return numeroDiasUteis; }
            set { numeroDiasUteis = value; }
        }

        int? numeroDiasCorridos;

        public int? NumeroDiasCorridos
        {
            get { return numeroDiasCorridos; }
            set { numeroDiasCorridos = value; }
        }

        decimal? cotacaoMedio;

        public decimal? CotacaoMedio
        {
            get { return cotacaoMedio; }
            set { cotacaoMedio = value; }
        }

        decimal? cotacaoFechamento; //Aplica-se tanto à cotação de fech para OPC, quanto ao ajuste de fech para futuros

        public decimal? CotacaoFechamento
        {
            get { return cotacaoFechamento; }
            set { cotacaoFechamento = value; }
        }

        decimal? ajusteAnterior; // Aplica-se aos ajustes corrigidos para DI1, DDI

        public decimal? AjusteAnterior
        {
            get { return ajusteAnterior; }
            set { ajusteAnterior = value; }
        }

        decimal? ptax;

        public decimal? Ptax
        {
            get { return ptax; }
            set { ptax = value; }
        }
        #endregion

        private void Initialize()
        {
            this.cdAtivoBMF = null;            
            this.serie = null;
            this.data = null;
            this.dataVencimento = null;
        }

        /// <summary>
        /// Carrega a collection de BdPregao a partir do arquivo BDPregao.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo BdPregao será procurado</param>
        /// throws ArquivoBdPregaoNaoEncontradoException
        public BdPregaoCollection ProcessaBdPregao(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);            
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("Bd_Final").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadBdPregao(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoBdPregaoNaoEncontradoException("Arquivo BdPregão: "+nomeArquivo.ToString()+" não encontrado\n");
                }
            }

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere dataArquivo
            StreamReader srAux = new StreamReader(nomeArquivo.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataMovimento = linhaAux.Substring(11, 8); // Campo 01
            dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
            ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0)
            {
                throw new ArquivoBdPregaoIncorretoException("Arquivo BdPregao "+dataMovimentoAux.ToString("dd/MM/yyyy")+" incorreto \n");
            }            
            #endregion

            BdPregaoCollection bdPregaoCollection = new BdPregaoCollection();
            int i = 1;
            string linha = "";
            try
            {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivo.ToString()))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        this.TrataBdPregao(linha);
                        BdPregao bdPregao = (BdPregao)Utilitario.Clone(this);
                        bdPregaoCollection.Add(bdPregao);
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento BdPregao com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaBdPregaoException(e.Message);
            }

            return bdPregaoCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo BdPregao de acordo com o tipo.
        /// </summary>
        /// <param name="linha"></param>
        private void TrataBdPregao(string linha)
        {
            // Limpa os valores do BdPregao
            this.Initialize();

            #region EstruturaArquivoBdPregao
            // Formato = "AAAAMMDD"
            string dataMovimento = linha.Substring(11, 8); // Campo 01
            int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            int mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
            int ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
            //
            this.data = new DateTime(ano, mes, dia);
            this.cdAtivoBMF = linha.Substring(21, 3).Trim();
            this.serie = linha.Substring(26, 4).Trim();
            this.tipoSerie = linha.Substring(25, 1).Trim();
            this.tipoMercado = Convert.ToInt32(linha.Substring(24, 1));
            
            decimal basePrecoExercicio = 10;            
            decimal expoentePrecoExercicio = Convert.ToDecimal(linha.Substring(315, 1).Trim());
            decimal precoExercicioAux = (decimal)Math.Pow((double)basePrecoExercicio, (double)expoentePrecoExercicio);
            this.precoExercicio = Convert.ToDecimal(linha.Substring(44, 13).Trim()) / precoExercicioAux;
            
            this.peso = Convert.ToDecimal(linha.Substring(57, 13).Trim()) / 10000000;

            if (this.tipoMercado != (int)TipoMercadoBMF.Vista)
            {
                string dataVencimentoAux = linha.Substring(36, 8).Trim();

                dia = Convert.ToInt32(dataVencimentoAux.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimentoAux.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimentoAux.Substring(0, 4));

                dataVencimentoAux = dia + "/" + mes + "/" + ano;

                if (Utilitario.IsDate(dataVencimentoAux))
                {
                    this.dataVencimento = new DateTime(ano, mes, dia);
                }
            }

            this.numeroDiasCorridos = Convert.ToInt32(linha.Substring(383, 5).Trim());
            this.numeroDiasUteis = Convert.ToInt32(linha.Substring(378, 5).Trim()); 

            decimal decimaisCotacao;
            decimal parteInteiraCotacao;
            decimal cotacaoAux;
            decimal decimaisAjusteAnterior;
            decimal parteInteiraAjusteAnterior;
            decimal ajusteAnteriorAux;
            switch (this.tipoMercado)
            {
                case (int)TipoMercadoBMF.Vista:
                case (int)TipoMercadoBMF.OpcaoDisponivel:
                case (int)TipoMercadoBMF.OpcaoFuturo:
                    decimaisCotacao = Convert.ToInt32(linha.Substring(315, 1).Trim());
                    parteInteiraCotacao = Convert.ToInt32(linha.Substring(221, 9).Trim());
                    cotacaoAux = (decimal)Math.Pow(10, (double)decimaisCotacao);
                    this.cotacaoFechamento = parteInteiraCotacao / cotacaoAux;
                    this.ajusteAnterior = 0;
                    break;
                case (int)TipoMercadoBMF.Futuro:
                    decimaisCotacao = Convert.ToInt32(linha.Substring(316, 1).Trim());
                    parteInteiraCotacao = Convert.ToInt32(linha.Substring(230, 14).Trim());                    
                    cotacaoAux = (decimal) Math.Pow (10, (double)decimaisCotacao);
                    this.cotacaoFechamento = parteInteiraCotacao / cotacaoAux;

                    //Se aplica apenas a DI1, DDI e DIL (ativos vinculados a juros)
                    if (this.cdAtivoBMF == "DI1" || this.cdAtivoBMF == "DDI" || this.cdAtivoBMF == "DIL")
                    {
                        decimaisAjusteAnterior = Convert.ToInt32(linha.Substring(316, 1).Trim());
                        parteInteiraAjusteAnterior = Convert.ToInt32(linha.Substring(245, 14).Trim());
                        ajusteAnteriorAux = (decimal)Math.Pow(10, (double)decimaisAjusteAnterior);
                        this.ajusteAnterior = parteInteiraAjusteAnterior / ajusteAnteriorAux;
                    }
                    else {
                        this.ajusteAnterior = 0;
                    }
                    break;
                default:
                    this.cotacaoFechamento = 0;
                    this.cotacaoMedio = 0;
                    this.ajusteAnterior = 0;
                    break;
            }

            if (this.tipoMercado != (int)TipoMercadoBMF.Futuro)
            {
                decimaisCotacao = Convert.ToInt32(linha.Substring(315, 1).Trim());
                parteInteiraCotacao = Convert.ToInt32(linha.Substring(175, 9).Trim());
                cotacaoAux = (decimal)Math.Pow(10, (double)decimaisCotacao);
                this.cotacaoMedio = parteInteiraCotacao / cotacaoAux;

                decimal parteInteiraCotacaoFechamento = Convert.ToInt32(linha.Substring(189, 9).Trim());
                this.cotacaoFechamento = parteInteiraCotacaoFechamento / cotacaoAux;
            }
            else {
                this.cotacaoMedio = 0;
            }

                        
            this.ptax = Convert.ToDecimal(linha.Substring(356, 13).Trim()) / 10000000;
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo BdPregao da Internet. Descompacta o arquivo.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo BDPregao</param>
        /// <returns></returns>
        private bool DownloadBdPregao(DateTime data, string path)
        {
            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            string endereco = "http://www.bmf.com.br/FTP/ContratosPregaoFinal/BF" + dataString.ToString() + ".ex_";
            string nomeArquivoDestino = "BF" + dataString.ToString() + ".exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path)
                       .Append("Bd_Final").Append(dataString).Append(".txt");

            #region Unzip do Arquivo
            if (download) {
                // Deleta o arquivo bdPregao.txt se existir
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("Bd_Final.txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion

                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try
                //{
                //    p.Start();
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine("Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion

                Archive arquivo = new Archive();
                //
                //arquivo.Comment = "";
                //arquivo.DestinationDirectory = ".";
                //arquivo.ExcludePattern = "";
                arquivo.PreservePath = true;
                //arquivo.SynchronizingObject = this;
                //arquivo.TempFileDirectory = "";
                //arquivo.VolumeLabel = "";
                //arquivo.Exception += new Dart.PowerTCP.Zip.ExceptionEventHandler(this.archive1_Exception);
                //arquivo.BusyChanged += new System.EventHandler(this.archive1_BusyChanged);
                //arquivo.Progress += new Dart.PowerTCP.Zip.ProgressEventHandler(this.archive1_Progress);
                //arquivo.EndUnzip += new Dart.PowerTCP.Zip.EndEventHandler(this.archive1_EndUnzip);

                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;

                //arquivo.Open(pathArquivoDestino);
                //arquivo.BeginQuickUnzip(pathArquivoDestino, path, null);
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion

            // Se arquivo BdPregao+data.txt existe retorna true            
            return File.Exists(nomeArquivoNovo.ToString());
        }
        
    }
}