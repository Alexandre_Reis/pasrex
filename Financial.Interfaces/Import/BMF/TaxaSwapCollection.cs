using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF
{
    public class TaxaSwapCollection
    {
        private List<TaxaSwap> collectionTaxaSwap;

        public List<TaxaSwap> CollectionTaxaSwap
        {
            get { return collectionTaxaSwap; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public TaxaSwapCollection()
        {
            this.collectionTaxaSwap = new List<TaxaSwap>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxaSwap">Elemento a inserir na lista de TaxaSwap</param>
        public void Add(TaxaSwap taxaSwap)
        {
            collectionTaxaSwap.Add(taxaSwap);
        }
    }
}
