using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.BMF
{
    public class DetalheNotaBMFPDF
    {
        private String cv;
        private String mercadoria;
        private Decimal precoAjuste;
        private String tipoNegocio;
        private Decimal taxaOperacional;
        private Decimal quantidade;

        public String CV
        {
            set
            {
                this.cv = value;
            }
            get
            {
                return this.cv;
            }
        }

        public String Mercadoria
        {
            set
            {
                this.mercadoria = value;
            }
            get
            {
                return this.mercadoria;
            }
        }
        public Decimal PrecoAjuste
        {
            set
            {
                this.precoAjuste = value;
            }
            get
            {
                return this.precoAjuste;
            }
        }
        public String TipoNegocio
        {
            set
            {
                this.tipoNegocio = value;
            }
            get
            {
                return this.tipoNegocio;
            }
        }
        public Decimal TaxaOperacional
        {
            set
            {
                this.taxaOperacional = value;
            }
            get
            {
                return this.taxaOperacional;
            }
        }
        public Decimal Quantidade
        {
            set
            {
                this.quantidade = value;
            }
            get
            {
                return this.quantidade;
            }
        }
    }

    public class NotaCorretagemBMFPDF
    {
        private DateTime dataPregao;
        private int codigoBovespa;
        private int codigoCliente;
        private List<DetalheNotaBMFPDF> listaDetalheNotaBMFPDF;
        private Decimal taxaRegistroBMF;
        private Decimal taxasBMF;

        public Decimal TaxaRegistroBMF
        {
            set
            {
                this.taxaRegistroBMF = value;
            }
            get
            {
                return this.taxaRegistroBMF;
            }
        }

        public Decimal TaxasBMF
        {
            set
            {
                this.taxasBMF = value;
            }
            get
            {
                return this.taxasBMF;
            }
        }

        public List<DetalheNotaBMFPDF> ListaDetalheNotaBMFPDF
        {
            set
            {
                listaDetalheNotaBMFPDF = value;
            }
            get
            {
                return listaDetalheNotaBMFPDF;
            }
        }

        public int CodigoCliente
        {
            set
            {
                codigoCliente = value;
            }
            get
            {
                return codigoCliente;
            }
        }

        public int CodigoBovespa
        {
            set
            {
                codigoBovespa = value;
            }
            get
            {
                return codigoBovespa;
            }
        }

        public DateTime DataPregao
        {
            set
            {
                dataPregao = value;
            }
            get
            {
                return dataPregao;
            }
        }
    }
}
