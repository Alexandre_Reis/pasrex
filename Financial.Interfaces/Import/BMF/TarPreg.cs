﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Dart.PowerTCP.Zip;
using System.Net;

namespace Financial.Interfaces.Import.BMF
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TarPreg
    {
        #region Properties dos registros
        string cdAtivoBMF;

        public string CdAtivoBMF
        {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        string serie;

        public string Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        DateTime? data;

        public DateTime? Data {
            get { return data; }
            set { data = value; }
        }

        decimal emolumento;

        public decimal Emolumento
        {
            get { return emolumento; }
            set { emolumento = value; }
        }

        decimal emolumentoDT;

        public decimal EmolumentoDT
        {
            get { return emolumentoDT; }
            set { emolumentoDT = value; }
        }

        decimal emolumentoSocio;

        public decimal EmolumentoSocio
        {
            get { return emolumentoSocio; }
            set { emolumentoSocio = value; }
        }

        decimal emolumentoDTSocio;

        public decimal EmolumentoDTSocio
        {
            get { return emolumentoDTSocio; }
            set { emolumentoDTSocio = value; }
        }

        decimal registro;

        public decimal Registro
        {
            get { return registro; }
            set { registro = value; }
        }

        decimal registroDT;

        public decimal RegistroDT
        {
            get { return registroDT; }
            set { registroDT = value; }
        }

        decimal registroSocio;

        public decimal RegistroSocio
        {
            get { return registroSocio; }
            set { registroSocio = value; }
        }

        decimal registroDTSocio;

        public decimal RegistroDTSocio
        {
            get { return registroDTSocio; }
            set { registroDTSocio = value; }
        }

        decimal fatorReducao;

        public decimal FatorReducao
        {
            get { return fatorReducao; }
            set { fatorReducao = value; }
        }

        decimal permanencia;

        public decimal Permanencia
        {
            get { return permanencia; }
            set { permanencia = value; }
        }
        #endregion

        /// <summary>
        /// Carrega a collection de TarPreg a partir do arquivo TarPreg.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo TarPreg será procurado</param>
        /// throws ArquivoTarPregNaoEncontradoException
        public TarPregCollection ProcessaTarPreg(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("TarPreg").Append(dataString).Append(".txt");

            #endregion
            
            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadTarPreg(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoTarPregNaoEncontradoException("Arquivo TarPreg: " + nomeArquivo.ToString() + " não encontrado");
                }
            }

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere dataArquivo
            StreamReader srAux = new StreamReader(nomeArquivo.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataMovimento = linhaAux.Substring(0, 8); // Campo 01
            dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
            ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0) {
                throw new ArquivoTarPregIncorretoException("Arquivo TarPreg " + dataMovimentoAux.ToString("dd/MM/yyyy") + " incorreto " + dataMovimentoAux);
            }
            #endregion

            TarPregCollection tarPregCollection = new TarPregCollection();
            int i = 1;
            string linha = "";
            try {
                // Using fecha o StreamReader
                using (StreamReader sr = new StreamReader(nomeArquivo.ToString())) {
                    while ((linha = sr.ReadLine()) != null) {
                        this.TrataTarPreg(linha);
                        TarPreg tarPreg = (TarPreg)Utilitario.Clone(this);
                        tarPregCollection.Add(tarPreg); ;
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento TarPreg com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaTarPregException(e.Message);
            }

            return tarPregCollection;
        }
                
        /// <summary>
        /// Salva os atributos do arquivo TarPreg de acordo com o tipo.
        /// </summary>
        /// <param name="linha"></param>
        private void TrataTarPreg(string linha)
        {
            #region EstruturaArquivoTarPreg
            // Formato = "AAAAMMDD"
            string dataMovimento = linha.Substring(0, 8); // Campo 01
            int dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            int mes = Convert.ToInt32( dataMovimento.Substring(4,2) );
            int ano = Convert.ToInt32( dataMovimento.Substring(0,4) );
            //
            this.data = new DateTime(ano, mes, dia);
            this.cdAtivoBMF = linha.Substring(8, 3).Trim();
            this.serie = linha.Substring(12, 4).Trim();                        
            this.emolumento = Convert.ToDecimal(linha.Substring(16, 20)) / 10000000;
            this.emolumentoDT = Convert.ToDecimal(linha.Substring(56, 20)) / 10000000;
            this.emolumentoSocio = Convert.ToDecimal(linha.Substring(36, 20)) / 10000000;
            this.emolumentoDTSocio = Convert.ToDecimal(linha.Substring(76, 20)) / 10000000;
            this.registro = Convert.ToDecimal(linha.Substring(96, 20)) / 10000000;
            this.registroDT = Convert.ToDecimal(linha.Substring(136, 20)) / 10000000;
            this.registroSocio = Convert.ToDecimal(linha.Substring(116, 20)) / 10000000;
            this.registroDTSocio = Convert.ToDecimal(linha.Substring(156, 20)) / 10000000;
            this.fatorReducao = Convert.ToDecimal(linha.Substring(256, 20)) / 10000000;
            this.permanencia = Convert.ToDecimal(linha.Substring(276, 20)) / 10000000;
            #endregion            
        }

        /// <summary>
        /// Baixa o arquivo TarPreg da Internet. Descompacta o arquivo Zip.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo TarPreg será procurado</param>
        /// <returns></returns>
        private bool DownloadTarPreg(DateTime data, string path) {
            
            //#region Download

            //int ano = data.Year;
            //int mes = data.Month;
            //int dia = data.Day;

            //string anoString = Utilitario.Right(Convert.ToString(data.Year),2);
            //string mesString = Convert.ToString(data.Month);
            //string diaString = Convert.ToString(data.Day);
            //if (mes < 10)
            //{
            //    mesString = "0" + mesString;
            //}
            //if (dia < 10)
            //{
            //    diaString = "0" + diaString;
            //}

            //StringBuilder dataString = new StringBuilder();
            //dataString.Append(anoString).Append(mesString).Append(diaString);

            //string endereco = "http://www.bmf.com.br/FTP/TarifacaoPregao/TP" + dataString.ToString() + ".ex_";
            //string nomeArquivoDestino = "TP" + dataString.ToString() + ".exe";
            //string pathArquivoDestino = path + nomeArquivoDestino;
            ////
            //bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            //#endregion

            //StringBuilder nomeArquivoNovo = new StringBuilder();
            //nomeArquivoNovo.Append(path)
            //           .Append("TarPreg").Append(dataString).Append(".txt");

            //#region Unzip do Arquivo
            //if (download) {

            //    // Deleta o arquivo tarPreg.txt se existir
            //    #region Deleta
            //    StringBuilder nomeArquivoAntigo = new StringBuilder();
            //    nomeArquivoAntigo.Append(path).Append("TarPreg.txt");

            //    if (File.Exists(nomeArquivoAntigo.ToString())) {
            //        File.Delete(nomeArquivoAntigo.ToString());
            //    }
            //    #endregion

            //    //#region Executa AutoExtrator
            //    //Process p = new Process();
            //    //string targetDir = string.Format(@path);
            //    //p.StartInfo.WorkingDirectory = targetDir;
            //    //p.StartInfo.FileName = nomeArquivoDestino;
            //    ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
            //    //p.StartInfo.CreateNoWindow = false;
            //    //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //    //try {
            //    //    p.Start();
            //    //}
            //    //catch (Exception e) {
            //    //    Console.WriteLine("Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
            //    //}
            //    //p.WaitForExit();
            //    //#endregion

            //    Archive arquivo = new Archive();
            //    //
            //    arquivo.PreservePath = true;
            //    arquivo.Clear();
            //    arquivo.Overwrite = Overwrite.Always;
            //    //
            //    arquivo.QuickUnzip(pathArquivoDestino, path);
            //    //
            //    #region Rename
            //    if (File.Exists(nomeArquivoAntigo.ToString())) {
            //        Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
            //    }
            //    #endregion                        
            //}
            //#endregion

            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            //string endereco = "http://britservices.com.br/tarpreg/TarPreg.txt";

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path).Append("TarPreg").Append(dataString).Append(".txt");
            //
            //bool download = Utilitario.DownloadFile(endereco, nomeArquivoNovo.ToString());

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://187.45.240.64/public_html/tarpreg/TarPreg.txt");
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("britservices", "Ftp963852");
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            FileStream file = File.Create(nomeArquivoNovo.ToString());
            byte[] buffer = new byte[32 * 1024];
            int read;
            //reader.Read(

            while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                file.Write(buffer, 0, read);
            }

            file.Close();
            responseStream.Close();
            response.Close();
            #endregion

            // Se arquivo TarPreg+data.txt existe retorna true            
            return File.Exists(nomeArquivoNovo.ToString());
        }
    }
}