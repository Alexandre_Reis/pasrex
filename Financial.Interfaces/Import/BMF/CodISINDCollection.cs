using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF
{
    public class CodISINDCollection
    {
        private List<CodISIND> collectionCodISIND;

        public List<CodISIND> CollectionCodISIND
        {
            get { return collectionCodISIND; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CodISINDCollection()
        {
            this.collectionCodISIND = new List<CodISIND>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codISIND">Elemento a inserir na lista de CodISIND</param>
        public void Add(CodISIND codISIND)
        {
            collectionCodISIND.Add(codISIND);
        }
    }
}
