﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Dart.PowerTCP.Zip;
using System.Net;


namespace Financial.Interfaces.Import.BMF
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TarPar
    {
        #region Properties dos registros
        string parametro;

        public string Parametro
        {
            get { return parametro; }
            set { parametro = value; }
        }

        DateTime dataReferencia;

        public DateTime DataReferencia
        {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        decimal valor;

        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        #endregion

        /// <summary>
        /// Carrega a collection de TarPar a partir do arquivo TarPar.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo TarPar será procurado</param>
        /// throws ArquivoTarParNaoEncontradoException
        public TarParCollection ProcessaTarPar(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("TarPar").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadTarPar(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoTarParNaoEncontradoException("Arquivo TarPar: "+ nomeArquivo.ToString()+" não encontrado.");
                }
            }

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere dataArquivo
            StreamReader srAux = new StreamReader(nomeArquivo.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataMovimento = linhaAux.Substring(100, 8); // Campo 01
            dia = Convert.ToInt32(dataMovimento.Substring(6, 2));
            mes = Convert.ToInt32(dataMovimento.Substring(4, 2));
            ano = Convert.ToInt32(dataMovimento.Substring(0, 4));
            
            //
            srAux.Close();
            DateTime dataMovimentoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataMovimentoAux.CompareTo(data) != 0)
            {
                throw new ArquivoTarParIncorretoException("Arquivo TarPar " + dataMovimentoAux.ToString("dd/MM/yyyy")+ " incorreto.");
            }
            #endregion

            TarParCollection tarParCollection = new TarParCollection();
            int i = 1;
            string linha = "";
            try
            {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        this.TrataTarPar(linha);
                        TarPar tarPar = (TarPar)Utilitario.Clone(this);
                        tarParCollection.Add(tarPar); ;
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento TarPar com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaTarParException(e.Message);
            }

            return tarParCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo TarPar. 
        /// </summary>
        /// <param name="linha"></param>
        private void TrataTarPar(string linha)
        {
            #region EstruturaArquivoTarPar
            // Formato = "AAAAMMDD"

            linha = ConverteLinhaErrada(linha);

            string dataReferencia = linha.Substring(100, 8); // Campo 01
            int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
            int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
            int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
            //
            this.dataReferencia = new DateTime(ano, mes, dia);
            this.parametro = linha.Substring(0, 30).Trim();
            this.valor = Convert.ToDecimal(linha.Substring(108, 20)) / 10000000;            
            #endregion
        }

        /// <summary>
        /// Função para ajustar a linha que vem com problemas no arquivo (caracteres estranhos).
        /// </summary>
        /// <param name="linha"></param>
        /// <returns></returns>
        private string ConverteLinhaErrada(string linha)
        {
            if (linha.Contains("ì")) 
            {
                linha = linha.Replace("ì", "C");
            }

            if (linha.Contains(""))
            {
                linha = linha.Replace("", "A");
            }

            if (linha.Contains("Õ"))
            {
                linha = linha.Replace("Õ", "O");
            }

            if (linha.Contains("Ø"))
            {
                linha = linha.Replace("Ø", "A");
            }

            return linha;
        }              
         
        /// <summary>
        /// Baixa o arquivo TarPar da Internet. Descompacta o arquivo Zip.          
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo BDPregao</param>
        /// <returns></returns>
        private bool DownloadTarPar(DateTime data, string path)
        {
            //#region Download

            //int ano = data.Year;
            //int mes = data.Month;
            //int dia = data.Day;

            //string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            //string mesString = Convert.ToString(data.Month);
            //string diaString = Convert.ToString(data.Day);
            //if (mes < 10)
            //{
            //    mesString = "0" + mesString;
            //}
            //if (dia < 10)
            //{
            //    diaString = "0" + diaString;
            //}

            //StringBuilder dataString = new StringBuilder();
            //dataString.Append(anoString).Append(mesString).Append(diaString);

            //string endereco = "http://www.bmf.com.br/FTP/TarifacaoParametros/TM" + dataString.ToString() + ".ex_";
            //string nomeArquivoDestino = "TM" + dataString.ToString() + ".exe";
            //string pathArquivoDestino = path + nomeArquivoDestino;
            ////
            //bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            //#endregion
            
            //StringBuilder nomeArquivoNovo = new StringBuilder();
            //nomeArquivoNovo.Append(path)
            //           .Append("TarPar").Append(dataString).Append(".txt");

            //#region Unzip do Arquivo
            //if (download)
            //{
            //    // Deleta o arquivo tarPar.txt se existir
            //    #region Deleta
            //    StringBuilder nomeArquivoAntigo = new StringBuilder();
            //    nomeArquivoAntigo.Append(path).Append("TarPar.txt");

            //    if (File.Exists(nomeArquivoAntigo.ToString()))
            //    {
            //        File.Delete(nomeArquivoAntigo.ToString());
            //    }
            //    #endregion

            //    //#region Executa AutoExtrator
            //    //Process p = new Process();
            //    //string targetDir = string.Format(@path);
            //    //p.StartInfo.WorkingDirectory = targetDir;
            //    //p.StartInfo.FileName = nomeArquivoDestino;
            //    ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
            //    //p.StartInfo.CreateNoWindow = false;
            //    //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //    //try
            //    //{
            //    //    p.Start();
            //    //}
            //    //catch (Exception e)
            //    //{
            //    //    Console.WriteLine("Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
            //    //}
            //    //p.WaitForExit();
            //    //#endregion

            //    Archive arquivo = new Archive();
            //    //
            //    arquivo.PreservePath = true;
            //    arquivo.Clear();
            //    arquivo.Overwrite = Overwrite.Always;
            //    //
            //    arquivo.QuickUnzip(pathArquivoDestino, path);
            //    //
            //    //
            //    #region Rename
            //    if (File.Exists(nomeArquivoAntigo.ToString()))
            //    {
            //        Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
            //    }
            //    #endregion
            //}
            //#endregion

            //// Se arquivo TarPar+data.txt existe retorna true            
            //return File.Exists(nomeArquivoNovo.ToString());


            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            //string endereco = "http://britservices.com.br/tarpar/TarPar.txt";

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path).Append("TarPar").Append(dataString).Append(".txt");
            //
            //bool download = Utilitario.DownloadFile(endereco, nomeArquivoNovo.ToString());

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://187.45.240.64/public_html/tarpar/TarPar" + dataString.ToString() + ".txt");
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("britservices", "Ftp963852");
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            FileStream file = File.Create(nomeArquivoNovo.ToString());
            byte[] buffer = new byte[32 * 1024];
            int read;
            //reader.Read(

            while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                file.Write(buffer, 0, read);
            }

            file.Close();
            responseStream.Close();
            response.Close();
            #endregion

            // Se arquivo TarPar+data.txt existe retorna true            
            return File.Exists(nomeArquivoNovo.ToString());
        }
    }
}
