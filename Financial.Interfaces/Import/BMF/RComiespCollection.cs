using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF {
    public class RComiespCollection {
        private List<RComiesp> collectionRComiesp;

        public List<RComiesp> CollectionRComiesp {
            get { return collectionRComiesp; }
            set { collectionRComiesp = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public RComiespCollection() {
            this.collectionRComiesp = new List<RComiesp>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tarPar">Elemento a inserir na lista de RComiesp</param>
        public void Add(RComiesp rcomiesp) {
            collectionRComiesp.Add(rcomiesp);
        }
    }
}
