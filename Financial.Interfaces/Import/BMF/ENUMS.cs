﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.BMF.Enums {
    /// <summary>
    /// 
    /// </summary>
    public static class CDISIndic {
        public const string DI1 = "DI1";
        public const string DI1_RE = "DI1-RE";
        public const string DOL_T1 = "DOL-T1";
        public const string DOL_T2 = "DOL-T2";
        public const string DOL_T3 = "DOL-T3";
        public const string DOL_D1 = "DOL-D1";
        public const string DOL_D2 = "DOL-D2";
        public const string SEL = "SEL";
        public const string TR = "TR";
        public const string IBOVESPA = "IBOVESPA";
        public const string EURO = "EUR";
    }

    /// <summary>
    /// 
    /// </summary>
    public static class GrupoIndic {
        /* Indicadores Agrícolas */
        public const string IA = "IA";
        /* Títulos da Dívida Externa */
        public const string DE = "DE";
        /* Indicadores Gerais */
        public const string RT = "RT";
        /* Indice BOVESPA */
        public const string BV = "BV";
        /* Moeda Estrangeira */
        public const string ME = "ME";
        /* IDI a Vista */
        public const string ID = "ID";
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TipoMercadoBMF {
        Vista = 1,
        Futuro = 2,
        OpcaoDisponivel = 3,
        OpcaoFuturo = 4,
        Termo = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoRComiesp {
        public const string Header = "00";
        public const string Detalhes = "01";

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(Detalhes);
            return valoresPossiveis;
        }
    }
}