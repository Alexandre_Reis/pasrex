﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Financial.Interfaces.Import.BMF.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção ProcessaTarPregException
    /// </summary>
    public class ProcessaTarPregException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaTarPregException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaTarPregException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTarPregNaoEncontradoException
    /// </summary>
    public class ArquivoTarPregNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTarPregNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTarPregNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTarPregIncorretoException
    /// </summary>
    public class ArquivoTarPregIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTarPregIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTarPregIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoBdPregaoNaoEncontradoException
    /// </summary>
    public class ArquivoBdPregaoNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoBdPregaoNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoBdPregaoNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoBdPregaoIncorretoException
    /// </summary>
    public class ArquivoBdPregaoIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoBdPregaoIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoBdPregaoIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaBdPregaoException
    /// </summary>
    public class ProcessaBdPregaoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaBdPregaoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaBdPregaoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCodISINDNaoEncontradoException
    /// </summary>
    public class ArquivoCodISINDNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCodISINDNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCodISINDNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoCodISINDIncorretoException
    /// </summary>
    public class ArquivoCodISINDIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCodISINDIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCodISINDIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaCodISINDException
    /// </summary>
    public class ProcessaCodISINDException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCodISINDException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCodISINDException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaTarParException
    /// </summary>
    public class ProcessaTarParException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaTarParException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaTarParException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTarParNaoEncontradoException
    /// </summary>
    public class ArquivoTarParNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTarParNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTarParNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTarPregIncorretoException
    /// </summary>
    public class ArquivoTarParIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTarParIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTarParIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoIndicNaoEncontradoException
    /// </summary>
    public class ArquivoIndicNaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoIndicNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoIndicNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoIndicIncorretoException
    /// </summary>
    public class ArquivoIndicIncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoIndicIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoIndicIncorretoException(string mensagem) : base(mensagem) { }
    }
    
    /// <summary>
    /// Exceção ProcessaIndicException
    /// </summary>
    public class ProcessaIndicException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaIndicException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaIndicException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRComiespNaoEncontradoException
    /// </summary>
    public class ArquivoRComiespNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRComiespNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRComiespNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRComiespIncorretoException
    /// </summary>
    public class ArquivoRComiespIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRComiespIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRComiespIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaIndicException
    /// </summary>
    public class ProcessaRComiespException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaRComiespException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaRComiespException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTaxaSwapNaoEncontradoException
    /// </summary>
    public class ArquivoTaxaSwapNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTaxaSwapNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTaxaSwapNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoTaxaSwapIncorretoException
    /// </summary>
    public class ArquivoTaxaSwapIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoTaxaSwapIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoTaxaSwapIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaTaxaSwapException
    /// </summary>
    public class ProcessaTaxaSwapException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaTaxaSwapException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaTaxaSwapException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoContratoVolatilidadeNaoEncontradoException
    /// </summary>
    public class ArquivoRefVolNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRefVolNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRefVolNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRefVolIncorretoException
    /// </summary>
    public class ArquivoRefVolIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRefVolIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRefVolIncorretoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaRefVolException
    /// </summary>
    public class ProcessaRefVolException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaRefVolException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaRefVolException(string mensagem) : base(mensagem) { }
    } 
}


