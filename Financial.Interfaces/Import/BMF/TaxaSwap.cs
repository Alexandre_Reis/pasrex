﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.BMF {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TaxaSwap {
        
        #region Properties dos registros

        DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        string codigoTaxa;

        public string CodigoTaxa {
            get { return codigoTaxa; }
            set { codigoTaxa = value; }
        }

        int numeroDiasCorridosTaxaJuros;

        public int NumeroDiasCorridosTaxaJuros {
            get { return numeroDiasCorridosTaxaJuros; }
            set { numeroDiasCorridosTaxaJuros = value; }
        }

        int numeroDiasSaquesTaxaJuros;

        public int NumeroDiasSaquesTaxaJuros {
            get { return numeroDiasSaquesTaxaJuros; }
            set { numeroDiasSaquesTaxaJuros = value; }
        }

        /* 7 decimais */
        decimal taxaTeorica;

        public decimal TaxaTeorica {
            get { return taxaTeorica; }
            set { taxaTeorica = value; }
        }

        #endregion

        /// <summary>
        /// Processa arquivo de Taxa De Swap
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Indic será procurado</param>
        /// throws ArquivoTaxaSwapNaoEncontradoException
        public TaxaSwapCollection ProcessaTaxaSwap(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("TaxaSwap").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                this.DownloadTaxaSwap(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString())) {
                    throw new ArquivoTaxaSwapNaoEncontradoException("\nArquivo TaxaSwap: "+nomeArquivo.ToString()+" não encontrado\n");
                }
            }

            TaxaSwapCollection taxaSwapCollection = new TaxaSwapCollection();
            int i = 1;
            string linha = "";
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {
                        this.TrataSwap(linha, data);
                        TaxaSwap taxaSwap = (TaxaSwap)Utilitario.Clone(this);
                        taxaSwapCollection.Add(taxaSwap); ;
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento TaxaSwap com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaTaxaSwapException(e.Message);
            }

            return taxaSwapCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo TaxaSwap 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataSwap(string linha, DateTime data) {
            #region EstruturaArquivoTaxaSwap
            // Formato = "AAAAMMDD"
            string dataReferencia = linha.Substring(11, 8);
            int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
            int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
            int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
            //
            this.dataReferencia = new DateTime(ano, mes, dia);
            if (this.dataReferencia.CompareTo(data) != 0) {
                throw new ArquivoTaxaSwapIncorretoException("Arquivo TaxaSwap " + this.dataReferencia.ToString("dd/MM/yyyy") + " incorreto ");
            }
            
            //
            this.codigoTaxa = linha.Substring(21, 5).Trim();
            this.numeroDiasCorridosTaxaJuros = Convert.ToInt32(linha.Substring(41, 5));
            this.numeroDiasSaquesTaxaJuros = Convert.ToInt32(linha.Substring(46, 5));
            //
            /* "+", "-" */
            string sinalTaxaTeorica = linha.Substring(51, 1);
            //
            //if-else
            decimal taxaTeorica = sinalTaxaTeorica == "-"
                            ? -1 * (Convert.ToDecimal(linha.Substring(52, 14)) / 10000000)
                            : Convert.ToDecimal(linha.Substring(52, 14)) / 10000000;
            this.taxaTeorica = taxaTeorica;                                  
            //           
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo TaxaSwap da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Indic</param>
        /// <returns></returns>
        private bool DownloadTaxaSwap(DateTime data, string path) {
            
            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            string endereco = "http://www.bmf.com.br/FTP/TaxasSwap/TS" + dataString.ToString() + ".ex_";
            string nomeArquivoDestino = "TaxaSwap" + dataString.ToString() + ".exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path)
                       .Append("TaxaSwap").Append(dataString).Append(".txt");

            #region Unzip do Arquivo
            if (download) {

                // Deleta o arquivo TaxaSwap.txt se existir
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("TaxaSwap.txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion

                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("TaxasSwap: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion

                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion
           
            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou deleta o zip
            if (arquivoDescompactado) {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString())) {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo TaxaSwap+data.txt existe retorna true            
            return arquivoDescompactado;
        }

    }
}
