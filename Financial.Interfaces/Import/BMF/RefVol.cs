﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.BMF {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class RefVol {

        #region Properties dos registros

        DateTime dataPregao;

        public DateTime DataPregao {
            get { return dataPregao; }
            set { dataPregao = value; }
        }

        string codigoMercadoria;

        public string CodigoMercadoria {
            get { return codigoMercadoria; }
            set { codigoMercadoria = value; }
        }

        string tipoMercado;

        public string TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }

        string vencimento;

        public string Vencimento {
            get { return vencimento; }
            set { vencimento = value; }
        }

        decimal precoFuturoLongo;

        public decimal PrecoFuturoLongo {
            get { return precoFuturoLongo; }
            set { precoFuturoLongo = value; }
        }

        decimal precoFuturo;

        public decimal PrecoFuturo {
            get { return precoFuturo; }
            set { precoFuturo = value; }
        }

        decimal delta;

        public decimal Delta {
            get { return delta; }
            set { delta = value; }
        }
        
        #endregion

        /// <summary>
        /// Carrega a collection de RefVol a partir do arquivo RefVol.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Indic será procurado</param>
        /// throws ArquivoRefVolNaoEncontradoException
        /// throws ArquivoRefVolIncorretoException
        public RefVolCollection ProcessaRefVol(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Convert.ToString(data.Year);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("RefVol").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            #region Download
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                this.DownloadRefVol(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString())) {
                    throw new ArquivoRefVolNaoEncontradoException("Arquivo RefVol: " + nomeArquivo.ToString() + " não encontrado");
                }
            }           
            #endregion

            // Pega a Data Antecipadamente para verificar se é o arquivo Correto
            #region Confere DataArquivo
            StreamReader srAux = new StreamReader(nomeArquivo.ToString());
            string linhaAux = srAux.ReadLine();
            // Formato = "AAAAMMDD"
            string dataPregao = linhaAux.Substring(11, 8);
            dia = Convert.ToInt32(dataPregao.Substring(6, 2));
            mes = Convert.ToInt32(dataPregao.Substring(4, 2));
            ano = Convert.ToInt32(dataPregao.Substring(0, 4));
            
            //
            srAux.Close();
            DateTime dataPregaoAux = new DateTime(ano, mes, dia);
            // Se as datas não forem iguais arquivo sendo lido está incorreto
            if (dataPregaoAux.CompareTo(data) != 0) {
                throw new ArquivoRefVolIncorretoException("Arquivo RefVol " + dataPregaoAux.ToString("dd/MM/yyyy") + " incorreto. ");
            }
            #endregion

            RefVolCollection refVolCollection = new RefVolCollection();
            int i = 1;
            string linha = "";
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {
                        this.TrataRefVol(linha, data);
                        RefVol refVol = (RefVol)Utilitario.Clone(this);

                        /* Somente adiciona se registro não for repetido 
                         * Elemento a inserir deve ser diferente do elemento anterior
                         * Isso ocorre pois o arquivo RefVol está ordenado
                         */
                        #region Adiciona na Collection
                        if (refVolCollection.CollectionRefVol.Count == 0) {
                            refVolCollection.Add(refVol);
                        }
                        else {
                            // Compara com o ultimo elemento
                            int ultimoElemento = refVolCollection.CollectionRefVol.Count - 1;
                            RefVol refVolAux = refVolCollection.CollectionRefVol[ultimoElemento];
                            if (!CompareByPrimaryKey(refVol, refVolAux)) {
                                refVolCollection.Add(refVol);
                            }
                        }
                        #endregion
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento RefVol com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaRefVolException(e.Message);
            }

            return refVolCollection;
        }
     
        /// <summary>
        /// Salva os atributos do arquivo RefVol 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataRefVol(string linha, DateTime data) {
            #region EstruturaArquivoRefVol
            // Formato = "AAAAMMDD"
            string dataPregao = linha.Substring(11, 8);
            int dia = Convert.ToInt32(dataPregao.Substring(6, 2));
            int mes = Convert.ToInt32(dataPregao.Substring(4, 2));
            int ano = Convert.ToInt32(dataPregao.Substring(0, 4));
            //
            this.dataPregao = new DateTime(ano, mes, dia);            
            //
            this.codigoMercadoria = linha.Substring(19, 3).Trim();
            this.tipoMercado = linha.Substring(22, 1);
            this.vencimento = linha.Substring(23, 4).Trim();
            this.precoFuturoLongo = Convert.ToDecimal(linha.Substring(36, 15)) / 1000;
            this.precoFuturo = Convert.ToDecimal(linha.Substring(52, 15)) / 1000;
            this.delta = Convert.ToDecimal(linha.Substring(70, 19)) / 10000000;
            //           
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo RefVol da Internet. Descompacta o arquivo Zip.          
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Indic</param>
        /// <returns></returns>
        private bool DownloadRefVol(DateTime data, string path) {
            
            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string anoStringCompleto = Convert.ToString(data.Year);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            StringBuilder dataStringCompleta = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            dataStringCompleta.Append(anoStringCompleto).Append(mesString).Append(diaString);

            string endereco = "http://www.bmf.com.br/FTP/ReferenciaContratosVolatilidade/RV" + dataString.ToString() + ".ex_";
            string nomeArquivoDestino = "RefVol" + dataStringCompleta.ToString() + ".exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path)
                       .Append("RefVol").Append(dataStringCompleta).Append(".txt");

            #region Unzip do Arquivo
            if (download) {

                // Deleta o arquivo Ref_Vol.txt se existir
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("Ref_Vol.txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion

                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("RefVol: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion

                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion

            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou Deleta o zip
            if (arquivoDescompactado) {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString())) {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo RefVol+data.txt existe retorna true            
            return arquivoDescompactado;
        }

        /// <summary>
        /// Compara dois Objetos refVol pela chave: DataPregao, CodigoMercadoria, Vencimento
        /// </summary>
        /// <param name="refVol1"></param>
        /// <param name="refVol2"></param>
        /// <returns>Retorna true se DataPregao, CodigoMercadoria, Vencimento de refVol1 e refVol2 forem iguais</returns>
        private bool CompareByPrimaryKey(RefVol refVol1, RefVol refVol2) {
            return ( refVol1.dataPregao.Equals(refVol2.DataPregao) &&
                     refVol1.codigoMercadoria == refVol2.codigoMercadoria &&
                     refVol1.vencimento == refVol2.vencimento);
        }
    }
}
