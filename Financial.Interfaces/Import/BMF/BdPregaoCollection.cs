using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF
{
    public class BdPregaoCollection
    {
        private List<BdPregao> collectionBdPregao;

        public List<BdPregao> CollectionBdPregao
        {
            get { return collectionBdPregao; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public BdPregaoCollection()
        {
            this.collectionBdPregao = new List<BdPregao>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bdPregao">Elemento a inserir na lista de BdPregao</param>
        public void Add(BdPregao bdPregao)
        {
            collectionBdPregao.Add(bdPregao);
        }
    }
}
