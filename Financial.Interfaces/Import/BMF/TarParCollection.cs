using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF {
    public class TarParCollection {
        private List<TarPar> collectionTarPar;

        public List<TarPar> CollectionTarPar {
            get { return collectionTarPar; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public TarParCollection() {
            this.collectionTarPar = new List<TarPar>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tarPar">Elemento a inserir na lista de TarPar</param>
        public void Add(TarPar tarPar) {
            collectionTarPar.Add(tarPar);
        }
    }
}
