using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF {
    public class RefVolCollection {
        private List<RefVol> collectionRefVol;

        public List<RefVol> CollectionRefVol {
            get { return collectionRefVol; }            
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public RefVolCollection() {
            this.collectionRefVol = new List<RefVol>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="refVol">Elemento a inserir na lista de RefVol</param>
        public void Add(RefVol refVol) {
            collectionRefVol.Add(refVol);
        }
    }
}
