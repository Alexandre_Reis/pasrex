﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Financial.Interfaces.Import.BMF.Enums;
using Dart.PowerTCP.Zip;
using SevenZip;

namespace Financial.Interfaces.Import.BMF
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Indic
    {
        #region Properties dos registros
        string codigoIndicador;

        public string CodigoIndicador
        {
            get { return codigoIndicador; }
            set { codigoIndicador = value; }
        }

        DateTime dataReferencia;

        public DateTime DataReferencia
        {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        decimal valor;

        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        string grupoIndicador;

        public string GrupoIndicador
        {
            get { return grupoIndicador; }
            set { grupoIndicador = value; }
        }

        #endregion

        /// <summary>
        /// Carrega a collection de Indic a partir do arquivo Indic.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Indic será procurado</param>
        /// throws ArquivoIndicNaoEncontradoException
        public IndicCollection ProcessaIndic(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("Indic").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File
                this.DownloadIndic(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoIndicNaoEncontradoException("\nArquivo Indic: " + nomeArquivo.ToString() + " não encontrado\n");
                }
            }

            IndicCollection indicCollection = new IndicCollection();
            int i = 1;
            string linha = "";
            try
            {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        this.TrataIndic(linha);
                        Indic indic = (Indic)Utilitario.Clone(this);
                        indicCollection.Add(indic); ;
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Indic com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaIndicException(e.Message);
            }

            return indicCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo Indic. 
        /// </summary>
        /// <param name="linha"></param>
        private void TrataIndic(string linha)
        {
            #region EstruturaArquivoIndic
            // Formato = "AAAAMMDD"

            string dataReferencia = linha.Substring(11, 8); // Campo 04
            int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
            int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
            int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
            //
            this.dataReferencia = new DateTime(ano, mes, dia);
            this.codigoIndicador = linha.Substring(21, 25).Trim();
            //
            int numeroDecimais = Convert.ToInt32(linha.Substring(71, 2).Trim());

            if (numeroDecimais == 2)
            {
                this.valor = Convert.ToDecimal(linha.Substring(46, 25)) / 100;
            }
            else if (numeroDecimais == 4)
            {
                this.valor = Convert.ToDecimal(linha.Substring(46, 25)) / 10000;
            }
            else
            {
                this.valor = Convert.ToDecimal(linha.Substring(46, 25)) / 100;
            }
            //
            this.grupoIndicador = linha.Substring(19, 2);
            // 
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo Indic da Internet. Descompacta o arquivo Zip.          
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Indic</param>
        /// <returns></returns>
        private bool DownloadIndic(DateTime data, string path)
        {
            string binPath = path.Replace("\\Downloads\\", "\\bin\\");
            
            #region Download

            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Utilitario.Right(Convert.ToString(data.Year), 2);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);

            string endereco = "http://www.bmf.com.br/FTP/IndicadoresEconomicos/ID" + dataString.ToString() + ".ex_";
            string nomeArquivoDestino = "ID" + dataString.ToString() + ".exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();
            nomeArquivoNovo.Append(path)
                       .Append("Indic").Append(dataString).Append(".txt");

            #region Unzip do Arquivo
            if (download)
            {
                // Deleta o arquivo indic.txt se existir
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("Indic.txt");

                if (File.Exists(nomeArquivoAntigo.ToString()))
                {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion

                /*
                #region Executa AutoExtrator
                Process p = new Process();
                string targetDir = string.Format(@path);
                p.StartInfo.WorkingDirectory = targetDir;
                p.StartInfo.FileName = nomeArquivoDestino;
                p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                p.StartInfo.CreateNoWindow = false;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                try
                {
                    p.Start();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                }
                p.WaitForExit();
                #endregion
                */

                Archive arquivo = new Archive();
                
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                

                arquivo.QuickUnzip(pathArquivoDestino, path);
                
                /*
                SevenZipExtractor.SetLibraryPath(binPath + "7z.dll");
                using (SevenZipExtractor tmp = new SevenZipExtractor(pathArquivoDestino))
                {
                    for (int i = 0; i < tmp.ArchiveFileData.Count; i++)
                    {
                        tmp.ExtractFiles(path, tmp.ArchiveFileData[i].Index);
                    }
                }
                */
                 
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString()))
                {
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion

            // Se arquivo Indic+data.txt existe retorna true            
            return File.Exists(nomeArquivoNovo.ToString());
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection.
        /// </summary>
        /// <param name="indic"></param>
        /// 
        /// <returns> true para alguns CDIs.
        /// </returns>
        public bool FilterIndicByCodigoGrupo(Indic indic)
        {
            return indic.codigoIndicador == CDISIndic.DI1 ||
                   indic.codigoIndicador == CDISIndic.DI1_RE ||
                   (indic.codigoIndicador == CDISIndic.DOL_D1 && indic.grupoIndicador == GrupoIndic.RT) ||
                   (indic.codigoIndicador == CDISIndic.DOL_D2 && indic.grupoIndicador == GrupoIndic.RT) ||
                   (indic.codigoIndicador == CDISIndic.DOL_T1 && indic.grupoIndicador == GrupoIndic.RT) ||
                   (indic.codigoIndicador == CDISIndic.DOL_T2 && indic.grupoIndicador == GrupoIndic.RT) ||
                   (indic.codigoIndicador == CDISIndic.DOL_T3 && indic.grupoIndicador == GrupoIndic.RT) ||
                   indic.codigoIndicador == CDISIndic.EURO ||
                   indic.codigoIndicador == CDISIndic.SEL ||
                   indic.codigoIndicador.Contains("TR 0") || indic.codigoIndicador.Contains("TR 1") ||
                   indic.codigoIndicador.Contains("TR 2") || indic.codigoIndicador.Contains("TR 3") ||
                   indic.codigoIndicador == CDISIndic.IBOVESPA;
        }
    }
}
