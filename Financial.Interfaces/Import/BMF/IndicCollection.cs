using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.BMF
{
    public class IndicCollection
    {
        private List<Indic> collectionIndic;

        public List<Indic> CollectionIndic
        {
            get { return collectionIndic; }
            set { collectionIndic = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public IndicCollection()
        {
            this.collectionIndic = new List<Indic>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indic">Elemento a inserir na lista de Indic</param>
        public void Add(Indic indic)
        {
            collectionIndic.Add(indic);
        }
    }
}
