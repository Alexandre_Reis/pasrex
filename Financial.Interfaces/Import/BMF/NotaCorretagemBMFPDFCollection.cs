﻿using System;
using System.Collections.Generic;
using System.Text;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System.IO;
using java.io;
using Financial.Util;
using System.Globalization;
using java.awt.geom;
using org.apache.pdfbox.pdfparser;
using org.apache.pdfbox.cos;

namespace Financial.Interfaces.Import.BMF
{
    public class NotaCorretagemBMFPDFCollection
    {
        /// <summary>
        /// Faz a leitura do pdf e carrega a lista de NotaCorretagemBMFPDF para notas BMF
        /// </summary>
        /// <param name="s">Vetor de bytes representativo do Arquivo PDF</param>
        /// <returns></returns>
        public List<NotaCorretagemBMFPDF> RetornaOperacoesBMF(Stream s)
        {
            byte[] streamBytes = Utilitario.ConvertStreamToByteArray(s);

            InputStream input = new ByteArrayInputStream(streamBytes);

            PDFParser parser = new PDFParser(input);
            parser.parse();

            COSDocument cos = parser.getDocument();

            // Carrega o arquivo pdf
            PDDocument docPdf = new PDDocument(cos);

            //cria objeto que sera usado para pegar o texto por regiao no documento
            PDFTextStripperByArea docStr = null;

            //cria objetos que serao usados para preencher com os valores copiados da nota
            List<NotaCorretagemBMFPDF> listaOperacao = new List<NotaCorretagemBMFPDF>();
            NotaCorretagemBMFPDF notaCorretagemBMFPDF = null;
            DetalheNotaBMFPDF detalhe = null;

            int codigoBovespa = 0;
            string dataPregao = "";
            string codigoCliente = "";

            //for que inicia a leitura em todos as paginas do pdf
            for (int i = 0; i < docPdf.getDocumentCatalog().getAllPages().size(); i++)
            {
                docStr = new PDFTextStripperByArea();
                docStr.setSortByPosition(true);

                notaCorretagemBMFPDF = new NotaCorretagemBMFPDF();
                notaCorretagemBMFPDF.ListaDetalheNotaBMFPDF = new List<DetalheNotaBMFPDF>();

                /**
                 * MAPEAMENTO E EXTRACAO DOS VALORES DO CAMPOS DE HEADER E FOOTER
                 **/
                PDPage pag = (PDPage)docPdf.getDocumentCatalog().getAllPages().get(i);

                docStr.addRegion("recDataPregao", new java.awt.Rectangle(517, 55, 40, 9));
                //docStr.addRegion("recCorretagem", new java.awt.Rectangle(480, 97, 50, 9));
                docStr.addRegion("recCodigBovespa", new java.awt.Rectangle(480, 97, 50, 9));
                docStr.addRegion("recCodigoCliente", new java.awt.Rectangle(480, 159, 50, 9));
                docStr.addRegion("recTaxaRegistroBMF", new java.awt.Rectangle(353, 616, 98, 9));
                docStr.addRegion("recTaxasBMF", new java.awt.Rectangle(455, 616, 98, 9));
                docStr.extractRegions(pag);

                notaCorretagemBMFPDF.DataPregao = ConverteDate(docStr.getTextForRegion("recDataPregao"));

                string[] stringCodigoBovespa = this.LimpaString(docStr.getTextForRegion("recCodigBovespa")).Split(new Char[] { '-' });

                notaCorretagemBMFPDF.CodigoBovespa = Convert.ToInt32(stringCodigoBovespa[0]);
                notaCorretagemBMFPDF.CodigoCliente = Convert.ToInt32(this.LimpaString(docStr.getTextForRegion("recCodigoCliente")));
                notaCorretagemBMFPDF.TaxaRegistroBMF = ConverteDecimal(docStr.getTextForRegion("recTaxaRegistroBMF"));
                notaCorretagemBMFPDF.TaxasBMF = ConverteDecimal(docStr.getTextForRegion("recTaxasBMF"));

                //verifica se ignora a linha dependendo das regras:
                //Quando houver no campo "Tipo do Negócio" a informação AJUPOS, a linha deverá ser ignorada, não deverá ser levada em consideração, deverá passar para a linha subsequente, caso contrário o sistema não reconhecerá esse tipo de operação.
                //Quando houver no campo "Mercadoria" a informação PPPP logo após os 3 primeiros dígitos que referem-se ao CdAtivoBMF, a linha deverá ser ignorada, não deverá ser levada em consideração, deverá passar para a linha subsequente, caso contrário o sistema não reconhecerá a Série PPPP no cadastro de ativos Bmf.

                /**
                 * FIM - MAPEAMENTO E EXTRACAO DOS VALORES DO CAMPOS DE HEADER E FOOTER
                 **/

                /**
                 * MAPEAMENTO DA LISTA DE DETALHE DA NOTA
                 **/
                //for que percorre as linhas da lista da nota. começa em 215 pois é o inicio da posicao de lista
                for (int l = 197; l <= 580; l = l + 8)
                {
                    bool ignoraLinha = false;
                    docStr = new PDFTextStripperByArea();
                    docStr.setSortByPosition(true);

                    //novo detalhe
                    detalhe = new DetalheNotaBMFPDF();

                    //mapeamento dos campos da lista da nota
                    docStr.addRegion("recLinhaCV", new java.awt.Rectangle(22, l, 15, 8));
                    docStr.addRegion("recLinhaMercadoria", new java.awt.Rectangle(38, l, 60, 8));
                    docStr.addRegion("recLinhaQuantidade", new java.awt.Rectangle(178, l, 55, 8));
                    docStr.addRegion("recLinhaPreco", new java.awt.Rectangle(234, l, 68, 8));
                    docStr.addRegion("recLinhaTipoNegocio", new java.awt.Rectangle(303, l, 110, 8));
                    docStr.addRegion("recLinhaTaxaOperacional", new java.awt.Rectangle(500, l, 65, 8));

                    docStr.extractRegions(pag);

                    detalhe.CV = this.LimpaString(docStr.getTextForRegion("recLinhaCV"));
                    detalhe.Mercadoria = this.LimpaString(docStr.getTextForRegion("recLinhaMercadoria"));
                    detalhe.Quantidade = this.ConverteDecimal(docStr.getTextForRegion("recLinhaQuantidade"));
                    detalhe.PrecoAjuste = this.ConverteDecimal(docStr.getTextForRegion("recLinhaPreco"));
                    detalhe.TipoNegocio = this.LimpaString(docStr.getTextForRegion("recLinhaTipoNegocio"));
                    detalhe.TaxaOperacional = this.ConverteDecimal(docStr.getTextForRegion("recLinhaTaxaOperacional"));

                    //verificando se deve ignorar
                    //se o campo tipo de operacao estiver vazio quer dizer que a linha estara em branco
                    ignoraLinha = detalhe.TipoNegocio.Contains("AJUPOS") || detalhe.Mercadoria.Contains("PPPP") || detalhe.CV.Equals("") || detalhe.Mercadoria.Trim().Equals("disponível");
                                        
                    if (!ignoraLinha)
                    {
                        //adicionando detalha a operacao
                        notaCorretagemBMFPDF.ListaDetalheNotaBMFPDF.Add(detalhe);
                    }
                }

                if (notaCorretagemBMFPDF.ListaDetalheNotaBMFPDF.Count > 0)
                {
                    listaOperacao.Add(notaCorretagemBMFPDF);
                }

            }

            return listaOperacao;
        }

        protected string obtemValor(string pagina, string campo)
        {
            string ret = pagina.Substring((pagina.IndexOf(campo) + campo.Length), 50).Split('\r')[0];

            string ret2 = "";

            foreach (char a in ret)
            {
                try
                {
                    ret2 = ret2 + Convert.ToInt32(a.ToString());
                }
                catch
                {
                    if (a.ToString().Equals(".") || a.ToString().Equals(",") || a.ToString().Equals(" "))
                        ret2 = ret2 + a.ToString();
                }
            }

            return ret2;
        }

        protected bool VerificaLinhaVazia(String recTipoOperacao, String recTipoMercado, String recPrazo, String recAtivo, String recObs, String recQuantidade, String recPu, String recValor)
        {
            bool vazia = false;

            if (recTipoOperacao.Equals("") &&
                recTipoMercado.Equals("") &&
                    recPrazo.Equals("") &&
                        recAtivo.Equals("") &&
                            recObs.Equals("") &&
                                recQuantidade.Equals("") &&
                                    recPu.Equals("") &&
                                        recValor.Equals(""))
                vazia = true;

            return vazia;
        }

        protected String[] PegaValorDuplicado(String[] arr)
        {
            String[] arrNovo = { "", "" };
            int c = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (!LimpaString(arr[i]).Trim().Equals(""))
                {
                    arrNovo[c] = arr[i];
                    c = c + 1;

                    if (c > 1)
                    {
                        break;
                    }
                }
            }

            return arrNovo;
        }

        protected Boolean VerificaLinhaDuplicada(String[] arr)
        {
            int dupli = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (!LimpaString(arr[i]).Trim().Equals(""))
                {
                    dupli = dupli + 1;
                }

            }

            if (dupli > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //funcao que verifica se o primeiro campo do DayTrade esta preenchido, caso contrario sera procurado na
        //proxima posicao e dentro do retorno sera procurado pelo valor. a string sera lida ao contrario, pois
        //o campo de valor sera na ultima posicao
        protected String verificaDayTrade(String texto)
        {
            char[] arrTexto = texto.ToCharArray();
            int tam = arrTexto.Length;
            int teste = 0;
            String ret = "";
            Boolean para = false;

            if (!texto.Trim().Equals(""))
            {
                //percorrer a string ao contrario procurando pelo valor
                for (int i = 1; i <= tam; i++)
                {
                    try
                    {
                        //se o caracter conseguir converter pra numero, colocar na string de retorno
                        teste = Convert.ToInt32(arrTexto[tam - i].ToString());

                        ret = arrTexto[tam - i].ToString() + ret;
                    }
                    catch (Exception)
                    {
                        //se nao converter, verificar se e "." ou "," e colocar no retorno
                        if (arrTexto[tam - i].ToString().Trim().Equals(".") || arrTexto[tam - i].ToString().Trim().Equals(","))
                        {
                            ret = arrTexto[tam - i].ToString() + ret;
                        }
                        else
                        {
                            //se o campo for diferente de ".", "," ou "" quer dizer que acabou o valor e pode encerrar a procura
                            if (arrTexto[tam - i].ToString().Trim().Equals(""))
                            {
                                para = true;
                            }
                        }
                    }

                    if (para)
                    {
                        break;
                    }
                }
            }

            return ret;
        }

        protected String LimpaString(String texto)
        {
            try
            {
                texto = texto.Replace("\n", "").Replace("\r", "");
            }
            catch (Exception)
            {
                texto = "";
            }

            return texto;
        }

        protected Decimal ConverteDecimal(String texto)
        {
            System.Globalization.CultureInfo i = new System.Globalization.CultureInfo("pt-BR");
            Decimal ret;
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSizes = new int[] { 3 };

            try
            {
                ret = decimal.Parse(texto, i);
            }
            catch (Exception)
            {
                ret = 0m;
            }

            return ret;
        }

        protected int ConverteInt(String texto)
        {
            int ret;

            try
            {
                ret = Convert.ToInt32(texto);
            }
            catch (Exception)
            {
                ret = 0;
            }

            return ret;
        }

        protected DateTime ConverteDate(String texto)
        {
            DateTime ret;

            try
            {
                //ret = DateTime.Parse(texto);
                ret = DateTime.Parse(texto, new System.Globalization.CultureInfo("pt-BR"));
            }
            catch (Exception)
            {
                ret = new DateTime();
            }

            return ret;
        }
    }
}