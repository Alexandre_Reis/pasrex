﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.RendaFixa.Exceptions {
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    #region Res238
    /// <summary>
    /// Exceção ProcessaRes238PregException
    /// </summary>
    public class ProcessaRes238Exception : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaRes238Exception() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaRes238Exception(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes238NaoEncontradoException
    /// </summary>
    public class ArquivoRes238NaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRes238NaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRes238NaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes238IncorretoException
    /// </summary>
    public class ArquivoRes238IncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRes238IncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRes238IncorretoException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region Res550
    /// <summary>
    /// Exceção ProcessaRes550Exception
    /// </summary>
    public class ProcessaRes550Exception : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaRes550Exception() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaRes550Exception(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes250NaoEncontradoException
    /// </summary>
    public class ArquivoRes550NaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRes550NaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRes550NaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes550IncorretoException
    /// </summary>
    public class ArquivoRes550IncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoRes550IncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoRes550IncorretoException(string mensagem) : base(mensagem) { }
    }

    #endregion

    #region Debenture
    /// <summary>
    /// Exceção ProcessaDebentureException
    /// </summary>
    public class ProcessaDebentureException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaDebentureException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaDebentureException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDebentureNaoEncontradoException
    /// </summary>
    public class ArquivoDebentureNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDebentureNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDebentureNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDebentureIncorretoException
    /// </summary>
    public class ArquivoDebentureIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDebentureIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDebentureIncorretoException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region DebentureSiteDebentures
    /// <summary>
    /// Exceção ProcessaDebentureSiteDebenturesException
    /// </summary>
    public class ProcessaDebentureSiteDebenturesException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaDebentureSiteDebenturesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaDebentureSiteDebenturesException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoDebentureSiteDebenturesNaoEncontradoException
    /// </summary>
    public class ArquivoDebentureSiteDebenturesNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoDebentureSiteDebenturesNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoDebentureSiteDebenturesNaoEncontradoException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region Andima
    /// <summary>
    /// Exceção ProcessaAndimaException
    /// </summary>
    public class ProcessaAndimaException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaAndimaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaAndimaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoAndimaNaoEncontradoException
    /// </summary>
    public class ArquivoAndimaNaoEncontradoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoAndimaNaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoAndimaNaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoAndimaIncorretoException
    /// </summary>
    public class ArquivoAndimaIncorretoException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoAndimaIncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoAndimaIncorretoException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region CotacaoTesouro
    /// <summary>
    /// Exceção ProcessaCotacaoTesouroException
    /// </summary>
    public class ProcessaCotacaoTesouroException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCotacaoTesouroException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCotacaoTesouroException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region CotacaoIBGE
    /// <summary>
    /// Exceção ProcessaCotacaoIBGE
    /// </summary>
    public class ProcessaCotacaoIBGEException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaCotacaoIBGEException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaCotacaoIBGEException(string mensagem) : base(mensagem) { }
    }

    public class ArquivoCotacaoIBGENaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoCotacaoIBGENaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoCotacaoIBGENaoEncontradoException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region ASEL007
    /// <summary>
    /// Exceção ProcessaASEL007PregException
    /// </summary>
    public class ProcessaASEL007Exception : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaASEL007Exception() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaASEL007Exception(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes238NaoEncontradoException
    /// </summary>
    public class ArquivoASEL007NaoEncontradoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoASEL007NaoEncontradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoASEL007NaoEncontradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ArquivoRes238IncorretoException
    /// </summary>
    public class ArquivoASEL007IncorretoException : InterfacesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ArquivoASEL007IncorretoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ArquivoASEL007IncorretoException(string mensagem) : base(mensagem) { }
    }
    #endregion  
}