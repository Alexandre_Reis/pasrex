using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;
using System.Xml.Serialization;
using FileHelpers;

namespace Financial.Interfaces.Import.RendaFixa
{
    [DelimitedRecord(";")]
    [IgnoreFirst(1)] 
    public class OperacaoProcessadaCetipViewModel
    {
        public int NumeroLinhaOriginal;

        /*[FieldTrim(TrimMode.Both)]
        public string CodigoIF;*/

        [FieldTrim(TrimMode.Both)]
        public string CodigoOperacao;

        [FieldTrim(TrimMode.Both)]
        public string DescricaoMensagem;

        [FieldTrim(TrimMode.Both)]
        public string TextoLinhaOriginal;
                
    }
}
