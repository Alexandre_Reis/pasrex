using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa {
    public class Res238Collection {
        private List<Res238> collectionRes238;

        public List<Res238> CollectionRes238 {
            get { return collectionRes238; }
            set { collectionRes238 = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public Res238Collection() {
            this.collectionRes238 = new List<Res238>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="res238">Elemento a inserir na lista de Res238</param>
        public void Add(Res238 res238) {
            collectionRes238.Add(res238);
        }
    }
}
