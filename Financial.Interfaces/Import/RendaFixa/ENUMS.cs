using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.RendaFixa.Enums {

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoRes238 {
        public const int Header = 1;
        public const int SubHeader = 2;
        public const int Dados = 3;

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(SubHeader);
            valoresPossiveis.Add(Dados);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoRes550 {
        public const int Header = 1;
        public const int SubHeader = 2;
        public const int Dados = 3;
        public const int Footer = 4;

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(SubHeader);
            valoresPossiveis.Add(Dados);
            valoresPossiveis.Add(Footer);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoDebenture {
        public const int Header = 1;
        public const int SubHeader = 2;
        public const int Dados = 3;

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(SubHeader);
            valoresPossiveis.Add(Dados);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoAndima {
        public const int Header = 1;
        public const int SubHeader = 2;
        public const int Dados = 3;

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(SubHeader);
            valoresPossiveis.Add(Dados);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ArquivoTesouro {
        LFT = 0,
        LTN = 1,
        NTNC = 2,
        NTNB = 3,
        NTNBPrincipal = 4,
        NTNF = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EstruturaArquivoASEL007
    {
        public const int Header = 0;
        public const int Dados = 1;
        public const int Rodape = 9;

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        public static List<int> Values()
        {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Header);
            valoresPossiveis.Add(Dados);
            valoresPossiveis.Add(Rodape);
            return valoresPossiveis;
        }
    }
}
