﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.RendaFixa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Res550 {
        // Informação sobre o tipo registro da linha do arquivo Res550
        private int tipoRegistro;

        // Indica se regiao do arquivo é footer
        private bool isFooter = false;

        #region Properties dos registros

        #region Header
        DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }
        #endregion

        #region SubHeader
        string coluna1;

        public string Coluna1 {
            get { return coluna1; }
            set { coluna1 = value; }
        }
        
        string coluna2;

        public string Coluna2 {
            get { return coluna2; }
            set { coluna2 = value; }
        }
        
        string coluna3;

        public string Coluna3 {
            get { return coluna3; }
            set { coluna3 = value; }
        }

        string coluna4;

        public string Coluna4 {
            get { return coluna4; }
            set { coluna4 = value; }
        }

        string coluna5;

        public string Coluna5 {
            get { return coluna5; }
            set { coluna5 = value; }
        }

        #endregion

        #region Dados
        int? codigoSelic;

        // Titulo
        public int? CodigoSelic {
            get { return codigoSelic; }
            set { codigoSelic = value; }
        }

        DateTime? dataVencimento;

        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        decimal? pu;

        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        #endregion

        #endregion

        /// <summary>        
        /// TipoRegistro nao deve ser inicializado
        /// Somente as variaveis da area de Dados deve ser inicilaizado
        /// </summary>
        private void Initialize() {
            this.codigoSelic = null;
            this.dataVencimento = null;
            this.pu = null;
        }

        /// <summary>
        /// Processa o arquivo Resolução 550
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Res550 será procurado</param>
        /// throws ArquivoRes550NaoEncontradoException
        public Res550Collection ProcessaRes550(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato4);
           
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path).Append("Res550_").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                this.DownloadRes550(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString())) {
                    throw new ArquivoRes550NaoEncontradoException("\nArquivo Res550: "+nomeArquivo.ToString()+" não encontrado\n");
                }
            }

            Res550Collection res550Collection = new Res550Collection();
            int i = 1;
            string linha = "";            
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {                                                                                                
                        bool isFooter = this.IsTipoRegistroFooter(linha); 
                        this.SetTipoRegistro(i, isFooter);
                        this.TrataRes550(linha, data);
                        Res550 res550 = (Res550)Utilitario.Clone(this);
                        res550Collection.Add(res550); ;
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Res550 com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaRes550Exception(e.Message);
            }

            return res550Collection;
        }

        /// <summary>
        /// Salva os atributos do arquivo Res550 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataRes550(string linha, DateTime data) {
            // Inicializa area de Dados
            this.Initialize();

            #region EstruturaArquivoRes550
            if (this.IsTipoRegistroHeader()) {                
                #region Header
                // Formato = "DD/MM/AAAA"
                string dataReferencia = linha.Substring(65, 10);
                int dia = Convert.ToInt32(dataReferencia.Substring(0, 2));
                int mes = Convert.ToInt32(dataReferencia.Substring(3, 2));
                int ano = Convert.ToInt32(dataReferencia.Substring(6, 4));
                //
                this.dataReferencia = new DateTime(ano, mes, dia);
                if (this.dataReferencia.CompareTo(data) != 0) {

                    #region Deleta arquivo incorreto
                    // Deleta o arquivo Incorreto
                    StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato3);
                    //  
                    string diretorioBase = Settings.Default.DiretorioBase;
                    StringBuilder nomeArquivo = new StringBuilder();
                    nomeArquivo.Append(diretorioBase)
                                   .Append("Res550_").Append(dataString).Append(".txt");

                    if (File.Exists(nomeArquivo.ToString())) {
                        File.Delete(nomeArquivo.ToString());
                    }
                    #endregion
                    //
                    throw new ArquivoRes550IncorretoException("Arquivo Res550 " + this.dataReferencia.ToString("dd/MM/yyyy") + " incorreto ");
                }
                #endregion                               
            }
            else if (this.IsTipoRegistroSubHeader()) {
                #region SubHeader
                this.coluna1 = linha.Substring(3, 6);
                this.coluna2 = linha.Substring(14, 10);
                this.coluna3 = linha.Substring(30, 14);
                this.coluna4 = linha.Substring(47, 17);
                this.coluna5 = linha.Substring(65, 21);
                #endregion
            }
            //
            else if (this.IsTipoRegistroDados()) {
                #region Dados

                #region Monta Lista com os valores do arquivo
                //
                // Remove espaços a esquerda e a direita
                string linhaAux = linha.TrimStart().TrimEnd();
                linhaAux = linhaAux.Replace(" ", "@"); //Troca espaços por @ 

                // Pega Tokens com delimitador @
                char[] caracter = new char[] {'@'};
                StringTokenizer token = new StringTokenizer(linhaAux, caracter);
                
                // Monta uma Lista de string somente com os valores que interessam                
                List<string> valores = new List<string>();
                /* Posicao 0: Codigo Selic 
                 * Posicao 1: DataVencimento
                 * Posicao 2: PU
                 * Posicao 3: Preço Retorno
                 * Posicao 4: Preço Custodia
                */
                foreach (string item in token) {
                    if (!String.IsNullOrEmpty(item)){
                        valores.Add(item);
                    }
                }
                #endregion

                this.codigoSelic = Convert.ToInt32(valores[0]);

                // Formato = "DD/MM/AAAA"
                string dataVencimento = valores[1];
                int dia = Convert.ToInt32(dataVencimento.Substring(0, 2));
                int mes = Convert.ToInt32(dataVencimento.Substring(3, 2));
                int ano = Convert.ToInt32(dataVencimento.Substring(6, 4));
                //
                this.DataVencimento = new DateTime(ano, mes, dia);
                //
                string pu = valores[2];
                pu = pu.Replace(".", "");
                pu = pu.Replace(",", "");
                this.Pu = Convert.ToDecimal(pu) / 100000000;
                #endregion                
            }                                 
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo Res550 da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Res550 será procurado</param>
        /// <returns></returns>
        private bool DownloadRes550(DateTime data, string path) {
            
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato4);

            //string endereco = "http://www.andima.com.br/res_550/arqs/"+dataString.ToString()+"_550.exe";
            string endereco = "http://www.anbima.com.br/res_550/arqs/" + dataString.ToString() + "_550.tex";

            string nomeArquivoDestino = "Res550_" + dataString.ToString() + ".txt";
            
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);

            // Se arquivo Res existe retorna true
            return File.Exists(pathArquivoDestino);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="numeroLinha"></param>
        /// <returns>bool indicando se Tipo Registro é Footer - Para uso Interno da classe</returns>
        private bool IsTipoRegistroFooter(string linha) {
            if (linha.Substring(0, 3) == "(*)") {
                this.isFooter = true;
            }
            return this.isFooter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha">numero da linha de Processamento</param>
        /// <param name="isFooter">Indica se é footer ou não</param>
        private void SetTipoRegistro(int linha, bool isFooter) {
            
            #region Prenche o TipoRegistro do Arquivo Res550
            if (isFooter) {
                this.tipoRegistro = EstruturaArquivoRes550.Footer;
            }
            else {
                switch (linha) {
                    case 1:
                        this.tipoRegistro = EstruturaArquivoRes550.Header;
                        break;
                    case 2:
                        this.tipoRegistro = EstruturaArquivoRes550.SubHeader;
                        break;
                    default:
                        this.tipoRegistro = EstruturaArquivoRes550.Dados;
                        break;
                }
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoRes550.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é SubHeader</returns>
        public bool IsTipoRegistroSubHeader() {
            return this.tipoRegistro == EstruturaArquivoRes550.SubHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Dados</returns>
        public bool IsTipoRegistroDados() {
            return this.tipoRegistro == EstruturaArquivoRes550.Dados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Footer</returns>
        public bool IsTipoRegistroFooter() {
            return this.tipoRegistro == EstruturaArquivoRes550.Footer;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Dados
        /// </summary>
        /// <param name="res550"></param>
        /// 
        /// <returns> true se TipoRegistro = Dados
        ///           false se TipoRegistro != Dados
        /// </returns>
        public bool FilterRes550ByDados(Res550 res550) {
            return res550.tipoRegistro == EstruturaArquivoRes550.Dados;
        }

    }
}
