﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.RendaFixa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Res238 {

        // Informação sobre o tipo registro da linha do arquivo Res238
        private int tipoRegistro;

        #region Properties dos registros

        #region Header
        DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }
        #endregion

        #region SubHeader
        string coluna1;

        public string Coluna1 {
            get { return coluna1; }
            set { coluna1 = value; }
        }
        string coluna2;

        public string Coluna2 {
            get { return coluna2; }
            set { coluna2 = value; }
        }
        string coluna3;

        public string Coluna3 {
            get { return coluna3; }
            set { coluna3 = value; }
        }
        #endregion

        #region Dados
        int? codigoSelic;

        public int? CodigoSelic {
            get { return codigoSelic; }
            set { codigoSelic = value; }
        }

        DateTime? dataVencimento;

        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        decimal? pu;

        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }

        #endregion

        #endregion

        // Contem a Data que está no arquivo Res caso o download tenha sido feito com sucesso
        private DateTime dataResArquivoDownload;

        /// <summary>        
        /// TipoRegistro nao deve ser inicializado
        /// Somente as variaveis da area de Dados deve ser inicilaizado
        /// </summary>
        private void Initialize() {
            this.codigoSelic = null;
            this.dataVencimento = null;
            this.pu = null;
        }

        /// <summary>
        /// Processa o Arquivo Resolução 238
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Bdin será procurado</param>
        /// throws ArquivoRes238NaoEncontradoException
        public Res238Collection ProcessaRes238(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Convert.ToString(data.Year);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10) {
                mesString = "0" + mesString;
            }
            if (dia < 10) {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("Res238_").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                bool download = this.DownloadRes238(data, path);

                // Se foi feito Download e Se arquivo não existe dá exceção
                if (!File.Exists(nomeArquivo.ToString())) {
                    //throw new ArquivoRes238NaoEncontradoException("Arquivo Res238: " + nomeArquivo.ToString() + " não encontrado\n");

                    if (download) {
                        throw new Exception("Você deve executar o Arquivo Andima238 no dia: " + this.dataResArquivoDownload.ToString("d") + "\n");
                    }
                }
            }

            Res238Collection res238Collection = new Res238Collection();
            int i = 1;
            string linha = "";
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {
                        this.SetTipoRegistro(i);
                        this.TrataRes238(linha, data);
                        Res238 res238 = (Res238)Utilitario.Clone(this);
                        res238Collection.Add(res238); ;
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Res238 com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaRes238Exception(e.Message);
            }

            return res238Collection;
        }
        
        /// <summary>
        /// Salva os atributos do arquivo Res238 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataRes238(string linha, DateTime data) {
            // Inicializa area de Dados
            this.Initialize();

            #region EstruturaArquivoRes238
            if (this.IsTipoRegistroHeader()) {                
                #region Header
                // Formato = "DD/MM/AAAA"
                string dataReferencia = linha.Substring(7, 10);
                int dia = Convert.ToInt32(dataReferencia.Substring(0, 2));
                int mes = Convert.ToInt32(dataReferencia.Substring(3, 2));
                int ano = Convert.ToInt32(dataReferencia.Substring(6, 4));
                //
                this.dataReferencia = new DateTime(ano, mes, dia);
                if (this.dataReferencia.CompareTo(data) != 0) {

                    #region Deleta arquivo incorreto
                    // Deleta o arquivo Incorreto
                    string anoString = Convert.ToString(data.Year);                    
                    string mesString = (data.Month < 10) ? "0" + Convert.ToString(data.Month) : Convert.ToString(data.Month);
                    string diaString = (data.Day < 10) ? "0" + Convert.ToString(data.Day) : Convert.ToString(data.Day);

                    StringBuilder dataString = new StringBuilder();
                    dataString.Append(anoString).Append(mesString).Append(diaString);
                    //  
                    string diretorioBase = Settings.Default.DiretorioBase;
                    StringBuilder nomeArquivo = new StringBuilder();
                    nomeArquivo.Append(diretorioBase)
                                   .Append("Res238_").Append(dataString).Append(".txt");

                    if (File.Exists(nomeArquivo.ToString())) {
                        File.Delete(nomeArquivo.ToString());
                    }
                    #endregion
                    //
                    throw new ArquivoRes238IncorretoException("Arquivo Res238 "+this.dataReferencia.ToString("dd/MM/yyyy")+" incorreto ");
                }
                #endregion                               
            }
            else if (this.IsTipoRegistroSubHeader()) {
                #region SubHeader
                /*
                string[] colunas = linha.Split(new char[] { ' ' });

                for (int i = 0; i < colunas.Length; i++) {                    
                    switch (i) {
                        case 1: this.coluna1 = colunas[i];
                                break;
                        case 2: this.coluna2 = colunas[i];
                                break;
                        case 3: this.coluna2 = colunas[i];
                                break;
                    }
                }*/

                this.coluna1 = linha.Substring(8, 6);
                this.coluna2 = linha.Substring(28, 10);
                this.coluna3 = linha.Substring(57, 6);
                #endregion
            }
            //
            else if (this.IsTipoRegistroDados()) {
                #region Dados
                //
                this.codigoSelic = Convert.ToInt32(linha.Substring(8, 6));

                // Formato = "DD/MM/AAAA"
                string dataVencimento = linha.Substring(28, 10);
                int dia = Convert.ToInt32(dataVencimento.Substring(0, 2));
                int mes = Convert.ToInt32(dataVencimento.Substring(3, 2));
                int ano = Convert.ToInt32(dataVencimento.Substring(6, 4));
                //
                this.DataVencimento = new DateTime(ano, mes, dia);
                //
                string indice = linha.Substring(51, 14);
                // Indice sera dividido em duas partes:
                /* 4.658,09396304: 4.658 - 09396304 */
                string[] tokensIndice = indice.Split(new char[] {','});
                string indiceAux = indice.Replace("," , "");
                indiceAux = indiceAux.Replace("." , "");
                decimal indiceAuxDecimal = Convert.ToDecimal(indiceAux);
                int tamanhoCasasDecimais = tokensIndice[1].Length;
                //                
                decimal divisao  = (decimal)Math.Pow(10, tamanhoCasasDecimais);
                this.Pu = Convert.ToDecimal(indiceAuxDecimal / divisao);
                #endregion                
            }                                 
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo Res238 da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Res238 será procurado</param>
        /// <returns>retorna true Se arquivo Res238+data.txt existe</returns>
        private bool DownloadRes238(DateTime data, string path)
        {
            if (data >= new DateTime(2012, 07, 09))
            {
                return DownloadAnbima238(data, path);
            }
            else
            {
                return DownloadRes238_Antigo(data, path);
            }
        }

        /// <summary>
        /// Baixa o arquivo Res238 da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Res238 será procurado</param>
        /// <returns>retorna true Se arquivo Res238+data.txt existe</returns>
        private bool DownloadRes238_Antigo(DateTime data, string path) {
            
            #region Download
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato4);
            string endereco = "http://www.andima.com.br/res_238/arqs/" + dataString.ToString() + "_238.exe";
            string nomeArquivoDestino = "Res238.exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();

            #region Unzip do Arquivo
            if (download) {

                // Deleta o arquivo RES238.TEX se existir para que não apareça mensagem de sobreposição
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("RES238.TEX");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion
                //
                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("Res238: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion
                //
                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {                    
                    
                    // Pega a Data do Arquivo para colocar no nome do arquivo
                    #region DataArquivo
                    StreamReader srAux = new StreamReader(nomeArquivoAntigo.ToString());
                    // Formato = "DD/MM/AAAA"
                    string linhaAux = srAux.ReadLine();
                    string dataAux = linhaAux.Substring(7, 10);
                    string dataArquivo = dataAux.Substring(6, 4) + dataAux.Substring(3, 2) + dataAux.Substring(0, 2);
                    #endregion

                    //
                    this.dataResArquivoDownload = new DateTime(Convert.ToInt32(dataAux.Substring(6, 4)),
                                                               Convert.ToInt32(dataAux.Substring(3, 2)),
                                                               Convert.ToInt32(dataAux.Substring(0, 2)));

                    nomeArquivoNovo.Append(path)
                               .Append("Res238_").Append(dataArquivo).Append(".txt");

                    srAux.Close();
                    // Se arquivo já existe deleta o arquivo antigo para poder renomear o do download
                    if (File.Exists(nomeArquivoNovo.ToString())) {
                        File.Delete(nomeArquivoNovo.ToString());
                    }
                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());                    
                }
                #endregion
            }
            #endregion
           
            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou deleta o zip
            if (arquivoDescompactado) {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString())) {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo Res238+data.txt existe retorna true            
            return arquivoDescompactado;
        }

        /// <summary>
        /// Baixa o arquivo Andima da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Andima será procurado</param>
        /// <returns></returns>
        private bool DownloadAnbima238(DateTime data, string path)
        {
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato4);

            string endereco = "http://www.anbima.com.br/res_238/arqs/" + dataString.ToString() + "_238.tex";
            string nomeArquivoDestino = "Res238_" + dataString.ToString() + ".txt";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);

            bool arquivoBaixado = File.Exists(pathArquivoDestino);
                        
            // Se arquivo existe retorna true            
            return arquivoBaixado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha"></param>
        private void SetTipoRegistro(int linha) {

            #region Prenche o TipoRegistro do Arquivo Res238
            switch (linha) {
                case 1:
                    this.tipoRegistro = EstruturaArquivoRes238.Header;
                    break;
                case 2:
                    this.tipoRegistro = EstruturaArquivoRes238.SubHeader;
                    break;
                default:
                    this.tipoRegistro = EstruturaArquivoRes238.Dados;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoRes238.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é SubHeader</returns>
        public bool IsTipoRegistroSubHeader() {
            return this.tipoRegistro == EstruturaArquivoRes238.SubHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Dados</returns>
        public bool IsTipoRegistroDados() {
            return this.tipoRegistro == EstruturaArquivoRes238.Dados;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Dados
        /// </summary>
        /// <param name="res238"></param>
        /// 
        /// <returns> true se TipoRegistro = Dados
        ///           false se TipoRegistro != Dados
        /// </returns>
        public bool FilterRes238ByDados(Res238 res238) {
            return res238.tipoRegistro == EstruturaArquivoRes238.Dados;
        }     
    }
}
