using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using Financial.Util;

namespace Financial.Interfaces.Import.RendaFixa {

    public class SIAnbid {
        [Serializable]
        public class Fundo {
            #region Properties
            private bool loaded;
            public bool Loaded {
                get {
                    return loaded;
                }
            }

            private string codigoFundo;
            public string CodigoFundo {
                get {
                    return codigoFundo;
                }
                set {
                    this.codigoFundo = value;
                }
            }

            private string cnpj;
            public string CNPJ {
                get {
                    return cnpj;
                }
                set {
                    this.cnpj = value;
                }
            }

            private string nomeFantasia;
            public string NomeFantasia {
                get {
                    return nomeFantasia;
                }
                set {
                    this.nomeFantasia = value;
                }
            }

            private byte? diasCotizacaoAplicacao, diasLiquidacaoAplicacao, diasCotizacaoResgate, diasLiquidacaoResgate;
            public byte? DiasCotizacaoAplicacao {
                get {
                    return diasCotizacaoAplicacao;
                }
                set {
                    this.diasCotizacaoAplicacao = value;
                }
            }

            public byte? DiasLiquidacaoAplicacao {
                get {
                    return diasLiquidacaoAplicacao;
                }
                set {
                    this.diasLiquidacaoAplicacao = value;
                }
            }

            public byte? DiasCotizacaoResgate {
                get {
                    return diasCotizacaoResgate;
                }
                set {
                    this.diasCotizacaoResgate = value;
                }
            }

            public byte? DiasLiquidacaoResgate {
                get {
                    return diasLiquidacaoResgate;
                }
                set {
                    this.diasLiquidacaoResgate = value;
                }
            }

            private DateTime dataInicio;
            public DateTime DataInicio {
                get {
                    return dataInicio;
                }
                set {
                    this.dataInicio = value;
                }
            }

            private string codigoAdministrador;
            public string CodigoAdministrador {
                get {
                    return codigoAdministrador;
                }
                set {
                    this.codigoAdministrador = value;
                }
            }

            private string nomeAdministrador;
            public string NomeAdministrador {
                get {
                    return nomeAdministrador;
                }
                set {
                    this.nomeAdministrador = value;
                }
            }

            private string codigoGestor;
            public string CodigoGestor {
                get {
                    return codigoGestor;
                }
                set {
                    this.codigoGestor = value;
                }
            }

            private string nomeGestor;
            public string NomeGestor {
                get {
                    return nomeGestor;
                }
                set {
                    this.nomeGestor = value;
                }
            }

            private string cotaAbertura;
            public string CotaAbertura {
                get {
                    return cotaAbertura;
                }
                set {
                    this.cotaAbertura = value;
                }
            }

            private byte? tipoCarteira;
            public byte? TipoCarteira {
                get {
                    return tipoCarteira;
                }
                set {
                    this.tipoCarteira = value;
                }
            }

            private short? idIndiceBenchmark;
            public short? IdIndiceBenchmark {
                get {
                    return idIndiceBenchmark;
                }
                set {
                    this.idIndiceBenchmark = value;
                }
            }

            private decimal? cotaInicial;
            public decimal? CotaInicial {
                get {
                    return cotaInicial;
                }
                set {
                    this.cotaInicial = value;
                }
            }

            private DateTime? dataInicioCota;
            public DateTime? DataInicioCota {
                get {
                    return dataInicioCota;
                }
                set {
                    this.dataInicioCota = value;
                }
            }

            private decimal? valorMinimoAplicacao;
            public decimal? ValorMinimoAplicacao {
                get {
                    return valorMinimoAplicacao;
                }
                set {
                    this.valorMinimoAplicacao = value;
                }
            }

            private decimal? valorMinimoResgate;
            public decimal? ValorMinimoResgate {
                get {
                    return valorMinimoResgate;
                }
                set {
                    this.valorMinimoResgate = value;
                }
            }

            private decimal? valorMinimoSaldo;
            public decimal? ValorMinimoSaldo {
                get {
                    return valorMinimoSaldo;
                }
                set {
                    this.valorMinimoSaldo = value;
                }
            }

            private decimal? valorMinimoInicial;
            public decimal? ValorMinimoInicial {
                get {
                    return valorMinimoInicial;
                }
                set {
                    this.valorMinimoInicial = value;
                }
            }

            private byte? tipoTributacao;
            public byte? TipoTributacao {
                get {
                    return tipoTributacao;
                }
                set {
                    this.tipoTributacao = value;
                }
            }

            private string calculaIOF;
            public string CalculaIOF {
                get {
                    return calculaIOF;
                }
                set {
                    this.calculaIOF = value;
                }
            }

            private int? idCategoria;
            public int? IdCategoria {
                get {
                    return idCategoria;
                }
                set {
                    this.idCategoria = value;
                }
            }

            private string estrategia;
            public string Estrategia {
                get {
                    return estrategia;
                }
                set {
                    this.estrategia = value;
                }
            }

            private string custodiante;
            public string Custodiante {
                get {
                    return custodiante;
                }
                set {
                    this.custodiante = value;
                }
            }

            private string auditoria;
            public string Auditoria {
                get {
                    return auditoria;
                }
                set {
                    this.auditoria = value;
                }
            }

            private decimal? taxaAdministracao;
            public decimal? TaxaAdministracao {
                get {
                    return taxaAdministracao;
                }
                set {
                    this.taxaAdministracao = value;
                }
            }

            private string taxaPerformance;
            public string TaxaPerformance {
                get {
                    return taxaPerformance;
                }
                set {
                    this.taxaPerformance = value;
                }
            }

            private string classificacaoAnbima;
            public string ClassificacaoAnbima {
                get {
                    return classificacaoAnbima;
                }
                set {
                    this.classificacaoAnbima = value;
                }
            }

            private string publicoAlvo;
            public string PublicoAlvo {
                get {
                    return publicoAlvo;
                }
                set {
                    this.publicoAlvo = value;
                }
            }

            private string objetivo;
            public string Objetivo {
                get {
                    return objetivo;
                }
                set {
                    this.objetivo = value;
                }
            }

            private string politicaInvest;
            public string PoliticaInvest {
                get {
                    return politicaInvest;
                }
                set {
                    this.politicaInvest = value;
                }
            }

            private string contato;
            public string Contato {
                get {
                    return contato;
                }
                set {
                    this.contato = value;
                }
            }
            #endregion

            public Fundo() {

            }

            public bool LoadByCodFundo(string codFundo, string selectColumns, bool loadExtendedProperties)
            {
                this.loaded = false;

                if (String.IsNullOrEmpty(codFundo))
                {
                    return this.loaded;
                }

                SIAnbid sianbid = new SIAnbid();
                sianbid.InitializeService();

                DataTable dataTable = sianbid.service.CarregaFundo(codFundo, selectColumns, loadExtendedProperties);
                if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
                {
                    DataRow dataRow = dataTable.Rows[0];
                    this.CodigoFundo = (string)dataRow["codfundo"];
                    this.CNPJ = (string)dataRow["cnpj"];
                    this.NomeFantasia = (string)dataRow["fantasia"];

                    if (loadExtendedProperties)
                    {
                        this.DiasCotizacaoAplicacao = this.DiasLiquidacaoAplicacao = sianbid.ParseInfoDiasFundo(dataRow["prazo_emis_cotas"]);
                        this.DiasCotizacaoResgate = sianbid.ParseInfoDiasFundo(dataRow["prazo_conv_resg"]);
                        this.DiasLiquidacaoResgate = sianbid.ParseInfoDiasFundo(dataRow["prazo_pgto_resg"]);
                        //
                        DateTime? dataAux = Utilitario.CastColumnValueToDateTime(dataRow["dataini"]);
                        this.DataInicio = dataAux.HasValue ? dataAux.Value : new DateTime(1900, 1, 1);
                        //
                        this.DataInicioCota = Utilitario.CastColumnValueToDateTime(dataRow["DataInicioCota"]);
                        if (this.DataInicioCota == null) { this.DataInicioCota = new DateTime(1900, 1, 1); }
                        //
                        this.CodigoAdministrador = sianbid.ConvertNullToVazio( Utilitario.CastColumnValueToStringTrim(dataRow["codinst"]) );
                        this.NomeAdministrador = sianbid.ConvertNullToVazio( Utilitario.CastColumnValueToStringTrim(dataRow["admin_fantasia"]) );
                        this.CodigoGestor = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["gestor"]));
                        this.NomeGestor = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["gestor_fantasia"]));
                        this.CotaAbertura = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["cota_abertura"]));
                        //                        
                        this.CalculaIOF = Utilitario.CastColumnValueToStringTrim(dataRow["CalculaIOF"]); 
                        if(this.CalculaIOF == null) { this.CalculaIOF = "S"; }
                        //
                        this.Estrategia = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["Estrategia"]));
                        this.Custodiante = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["custodiante"]));
                        this.Auditoria = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["auditoria"]));
                        this.TaxaPerformance = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["taxa_perform"]));
                        this.ClassificacaoAnbima = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["descricao"]));
                        this.PublicoAlvo = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["publicoalvo"]));
                        this.PoliticaInvest = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["politicainvest"]));
                        this.Objetivo = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["objetivo"]));
                        this.Contato = sianbid.ConvertNullToVazio(Utilitario.CastColumnValueToStringTrim(dataRow["contato"]));
                        //
                        this.TipoCarteira = Utilitario.CastColumnValueToByte(dataRow["TipoCarteira"]);
                        if (this.TipoCarteira == null) { this.TipoCarteira = (byte)1; }
                        //
                        this.TipoTributacao = Utilitario.CastColumnValueToByte(dataRow["TipoTributacao"]);
                        if(this.TipoTributacao == null) { this.TipoTributacao = (byte)1; }
                        //
                        this.IdIndiceBenchmark = Utilitario.CastColumnValueToShort(dataRow["IdIndiceBenchmark"]);
                        if (this.IdIndiceBenchmark == null) { this.IdIndiceBenchmark = 0; }
                        //
                        this.CotaInicial = Utilitario.CastColumnValueToDecimal(dataRow["CotaInicial"]);
                        if(this.CotaInicial == null) { this.CotaInicial = 1; }
                        //
                        this.ValorMinimoAplicacao = sianbid.ConvertNullToZero(Utilitario.CastColumnValueToDecimal(dataRow["vlr_min_aplic_adic"]));
                        this.ValorMinimoResgate = sianbid.ConvertNullToZero(Utilitario.CastColumnValueToDecimal(dataRow["vlr_min_resgate"]));
                        this.ValorMinimoSaldo = sianbid.ConvertNullToZero(Utilitario.CastColumnValueToDecimal(dataRow["vlr_min_aplic"]));
                        this.ValorMinimoInicial = sianbid.ConvertNullToZero(Utilitario.CastColumnValueToDecimal(dataRow["vlr_min_aplic_ini"]));
                        this.TaxaAdministracao = sianbid.ConvertNullToZero(Utilitario.CastColumnValueToDecimal(dataRow["taxa_fixa"]));
                        //                        
                        this.IdCategoria = sianbid.ConvertIntNullToZero(Utilitario.CastColumnValueToInt(dataRow["codtipo"]));
                    }
                    this.loaded = true;
                }

                return this.loaded;
            }
        }

        public class Cota {
            public Cota(DateTime data, decimal valor, decimal pl) {
                this.data = data;
                this.valor = valor;
                this.pl = pl;
            }
            private decimal valor, pl;
            public decimal Valor {
                get {
                    return valor;
                }
            }

            public decimal PL {
                get {
                    return pl;
                }
            }

            private DateTime data;
            public DateTime Data {
                get {
                    return data;
                }
            }

        }

        private SIAnbidWebService.SIAnbid service;

        private void InitializeService() {
            this.service = new SIAnbidWebService.SIAnbid();
            const string SIANBID_WEBSERVICE_URL = "SIAnbid.asmx";

            string serviceUrl = ConfigurationManager.AppSettings["WebServicesProviderUrl"];
            if (!string.IsNullOrEmpty(serviceUrl)) {
                this.service.Url = serviceUrl + "/" + SIANBID_WEBSERVICE_URL;
            }
            else {
                this.service.Url = "http://atatika.financialonline.com.br/Financial.WebServices.Provider/" + SIANBID_WEBSERVICE_URL;
            }

            string proxyInfo = ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo)) {
                //Utilizar padrao de proxy definido no Windows
                service.Proxy = System.Net.WebProxy.GetDefaultProxy();
                service.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default") {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    service.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1) {
                        //Foram passadas tambem as credenciais de rede
                        service.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }
        }

        private byte? ParseInfoDiasFundo(object columnValue) {
            byte? returnValue;
            if (columnValue == System.DBNull.Value) {
                returnValue = null;
            }
            else {
                byte number;
                if (!byte.TryParse(string.Join(null, System.Text.RegularExpressions.Regex.Split((string)columnValue, "[^\\d]")), out number)) {
                    returnValue = null;
                }
                else {
                    returnValue = number;
                }
            }

            if (returnValue == null) {
                returnValue = 0;
            }

            return returnValue;
        }

        private string ConvertNullToVazio(string valor) {
            return valor == null ? "" : (string)valor;
        }

        private decimal ConvertNullToZero(object valor) {
            //if (!valor is decimal) {
            //    throw new ArgumentException("Tipo deve Decimal");
            //}

            return valor == null ? 0 : Convert.ToDecimal(valor);
        }

        private int ConvertIntNullToZero(object valor) {
            return valor == null ? 0 : Convert.ToInt32(valor);
        }

        public List<Cota> RetornaCotas(string codFundo, DateTime? dataInicio) {
            List<Cota> cotas = new List<Cota>();

            this.InitializeService();
            DataTable dataTable = service.RetornaCotas(codFundo, dataInicio);

            if (dataTable != null) {
                foreach (DataRow dataRow in dataTable.Rows) {
                    object pl = dataRow["pl"];
                    if (pl == System.DBNull.Value) {
                        pl = 0;
                    }

                    decimal valorPL = 0;
                    try
                    {
                        valorPL = Convert.ToDecimal(pl);
                    }
                    catch (Exception e)
                    {
                        valorPL = 0;
                    }


                    Cota cota = new Cota(Convert.ToDateTime(dataRow["data"]), (Decimal)dataRow["valcota"], valorPL);
                    cotas.Add(cota);
                }
            }
            return cotas;
        }
       
        public List<Fundo> BuscaFundos(string cnpj, string nomeFantasia, string codFundo) {
            string selectColumns = "codfundo,cnpj,fantasia";
            List<Fundo> fundos = new List<Fundo>();
            this.InitializeService();
            DataTable dataTable = service.BuscaFundos(cnpj, nomeFantasia, codFundo, selectColumns);
            if (dataTable != null) {
                foreach (DataRow dataRow in dataTable.Rows) {

                    Fundo fundo = new Fundo();
                    fundo.CodigoFundo = (string)dataRow["codfundo"];
                    fundo.CNPJ = Financial.Util.Utilitario.CastColumnValueToStringTrim(dataRow["cnpj"]);
                    fundo.NomeFantasia = Financial.Util.Utilitario.CastColumnValueToStringTrim(dataRow["fantasia"]);
                    fundos.Add(fundo);
                }
            }

            return fundos;
        }
    }
}