﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Globalization;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;
using Financial.Util.Exceptions;

namespace Financial.Interfaces.Import.RendaFixa
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class AnbimaIndicadores
    {
        #region Properties dos Registros
        string indice;
        public string Indice
        {
            get { return indice; }
            set { indice = value; }
        }

        DateTime data;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        decimal valor;
        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        #endregion

        /// <summary>
        /// Processa o Arquivo de AnbimaIndicadores
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">Path Completo do Diretorio de Destino do Download do arquivo xls</param>
        /// Se Houve erro na leitura das planilhas Excel
        /// Se 0 registros foram lidos                                                
        /// </exception>
        public List<AnbimaIndicadores> ProcessaAnbimaIndicadores(DateTime data, string path)
        {
            string nomeArquivoDestino = "indicadores.xls";
            path = path + nomeArquivoDestino;
            List<AnbimaIndicadores> lista = new List<AnbimaIndicadores>();

            if (this.DownloadAnbimaIndicadores(data, path))
            {
                lista = this.LerPlanilhaExcel(data, path);
            }

            return lista;
        }

        /// <summary>
        /// Baixa os Arquivos do site da Anbima, Caso o arquivo já esteja no Diretorio, não baixa o arquivo
        /// Para ano = Atual sempre baixa o arquivo pois os dados estão sempre mudando
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">Path Completo do Diretorio onde o arquivo será procurado</param>
        /// <returns>true se o arquivo com a determinada chave está no diretorio, false caso contrario</returns>
        private bool DownloadAnbimaIndicadores(DateTime data, string path)
        {
            string url = @"http://www.anbima.com.br/indicadores/arqs/indicadores.xls";

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            bool download = Utilitario.DownloadFile(url, path);
                            
            // Se arquivo existe retorna true
            return download;
        }

        /// <summary>
        /// Leitura dos arquivo Excel - (LFT, LTN, NTN-C, NTN-B, NTN-B Principal, NTN-F)
        /// </summary>
        /// <param name="data">Filtro da Data para Pegar o PuBase</param>
        /// <param name="arquivo">Path completo do arquivo Excel que será lido</param>
        /// <exception cref="Exception">Se ocorreu problemas</exception>
        private List<AnbimaIndicadores> LerPlanilhaExcel(DateTime data, string arquivo)
        {
            List<AnbimaIndicadores> lista = new List<AnbimaIndicadores>();

            #region Leitura do Arquivo Excel
            // Open Spreadsheet
            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            document.LoadFromFile(arquivo);

            for (int i = 0; i < document.Workbook.Worksheets.Count; i++)
            {
                Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[i];
                string descricao = workSheet.Name;

                // This row,column index should be changed as per your need.
                // i.e. which cell in the excel you are interesting to read.
                //
                /* Formato: 1)Indice -  2)Descricao/Data - 3) Valor
                 */
                int linha = 1;
                int coluna1 = 0, coluna2 = 1, coluna3 = 2;

                try
                {
                    // Roda o loop 30 iterações
                    for (int cont = 0; cont <= 30; cont++)
                    {
                        if (workSheet.Cell(linha, coluna2).ValueAsString.Contains("Projeção") ||
                            workSheet.Cell(linha, coluna1).ValueAsString.Contains("Euro Venda"))
                        {
                            AnbimaIndicadores item = new AnbimaIndicadores();

                            item.Indice = workSheet.Cell(linha, coluna1).ValueAsString;
                            item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                            
                            DateTime dataIndice = new DateTime();
                            if (workSheet.Cell(linha, coluna2).ValueAsString.Contains("Projeção"))
                            {
                                dataIndice = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
                            }
                            else
                            {
                                dataIndice = Convert.ToDateTime(workSheet.Cell(linha, coluna2).ValueAsDateTime);
                            }

                            item.Data = dataIndice;

                            lista.Add(item);
                        }
                        
                        linha ++;                        
                    }
                }
                catch (Exception ex)
                {
                    document.Close();
                    document.Dispose();

                    throw new Exception(ex.Message);
                }

                document.Close();
                document.Dispose();
            }
            #endregion

            return lista;
        }
    }

    
}