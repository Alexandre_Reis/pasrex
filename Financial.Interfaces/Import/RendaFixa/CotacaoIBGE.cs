﻿using System;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common;
using System.IO;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using Dart.PowerTCP.Zip;
using System.Text;
using System.Diagnostics;
using Financial.Util.Exceptions;

namespace Financial.Interfaces.Import.RendaFixa {

    /// <summary>
    /// Armazena os Valores de CotacaoIBGE presentes num arquivo .xls proveniente do site IBGE.gov.br
    /// </summary>
    [Serializable]
    public class CotacaoIBGE {

        // Guarda os nomes dos arquivos baixados [0] = arquivoIPCA, [1] = arquivoINPC
        private string[] nomeArquivosUnzip = new string[2] { "", "" };
        
        private DateTime data;
        private int indice;
        private decimal cotacao;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public int Indice {
            get { return indice; }
            set { indice = value; }
        }

        public decimal Cotacao {
            get { return cotacao; }
            set { cotacao = value; }
        }

        /// <summary>
        /// Faz o Download do yahoo dos PUs dos Ativos e carrega na Lista de Cotações
        /// </summary>
        /// <param name="ativosOffshore">Cada elemento da lista tem até 100 ativos separados por Virgula</param>
        /// <param name="data"></param>
        /// <param name="path">Path Completo do Diretorio onde o Arquivo com as Cotações Offshore será Procurado</param>
        /// <returns>Lista com os ativos Offshore e seus Pu</returns>
        /// <exception cref="ArquivoOffShoreNaoEncontradoException">Se download falhou e arquivo quotes_1.csv não existe</exception>
        /// <exception cref="ProcessaOffShoreException">Se Processamento de algum arquivo offshore falhou</exception>
        public List<CotacaoIBGE> ProcessaCotacaoIBGE(DateTime data, string path) {
            // Zera vetor de unzip de arquivo baixado
            nomeArquivosUnzip[0] = ""; nomeArquivosUnzip[1] = "";
            
            // Se arquivo não existe dá exceção                                                                    
            if (!this.DownloadCotacaoIBGE(data, path)) {
                throw new ArquivoCotacaoIBGENaoEncontradoException("Download falhou. Arquivo não encontrado");
            }

            List<CotacaoIBGE> listaRetorno = new List<CotacaoIBGE>();

            if (!String.IsNullOrEmpty(this.nomeArquivosUnzip[0])) {
                CotacaoIBGE cotacaoIBGEIPCA = this.LerPlanilhaExcel(data, this.nomeArquivosUnzip[0]);

                if (cotacaoIBGEIPCA != null) {
                    cotacaoIBGEIPCA.Indice = Financial.Common.Enums.ListaIndiceFixo.IPCA;
                    listaRetorno.Add(cotacaoIBGEIPCA);
                }
            }

            if (!String.IsNullOrEmpty(this.nomeArquivosUnzip[1])) {

                CotacaoIBGE cotacaoIBGEINPC = this.LerPlanilhaExcel(data, this.nomeArquivosUnzip[1]);

                if (cotacaoIBGEINPC != null) {
                    cotacaoIBGEINPC.Indice = Financial.Common.Enums.ListaIndiceFixo.INPC;
                    listaRetorno.Add(cotacaoIBGEINPC);
                }
            }

            return listaRetorno;
        }

        /// <summary>
        /// Baixa o Arquivo da Internet do Site do IBGE
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool DownloadCotacaoIBGE(DateTime data, string path) {

            // Sort pela data do arquivo - para FTP não é possivel construir DirectoryInfo
            //string diretorioIPCA = @"ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica/";

            //DirectoryInfo dir = new DirectoryInfo(diretorioIPCA);
            //FileSystemInfo[] files = dir.GetFileSystemInfos("*.zip");

            //// sort them by creation time
            //Array.Sort<FileSystemInfo>(files, delegate(FileSystemInfo a, FileSystemInfo b) {
            //    int numero1 = Convert.ToInt32(a.Name.Substring(5, 6));
            //    int numero2 = Convert.ToInt32(b.Name.Substring(5, 6));

            //    return numero1.CompareTo(numero2);
            //});

            DateTime hoje = DateTime.Today;
            string mes = hoje.Month.ToString();
            if (mes.Length == 1) { mes = "0" + mes; }
                        
            //
            DateTime mesPassado = hoje.AddMonths(-1);
            string mes1 = mesPassado.Month.ToString();
            if (mes1.Length == 1) { mes1 = "0" + mes1; }

            #region Url
            List<string> url = new List<string>();
            List<string> urlSegundaTentativa = new List<string>();

            url.Add(@"ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica/ipca" + "_" + hoje.Year.ToString() + mes + "SerieHist.zip");
            url.Add(@"ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/INPC/Serie_Historica/inpc" + "_" + hoje.Year.ToString() + mes + "SerieHist.zip");
            //
            // Segunda Tentativa
            urlSegundaTentativa.Add(@"ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica/ipca" + "_" + mesPassado.Year.ToString() + mes1 + "SerieHist.zip");
            urlSegundaTentativa.Add(@"ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/INPC/Serie_Historica/inpc" + "_" + mesPassado.Year.ToString() + mes1 + "SerieHist.zip");
            //
            #endregion

            bool temArquivo = false;

            #region Download
            for (int i = 0; i < url.Count; i++) {
                string arq = i == 0 ? "ipca_" : "inpc_";
                //
                string nomeArquivoDestino = url[i].Contains("ipca") ? "SerieHistIPCA.zip" : "SerieHistINPC.zip";
                               
                string pathArquivoDestino = path + nomeArquivoDestino;
                //

                if (File.Exists(pathArquivoDestino)) {
                    File.Delete(pathArquivoDestino);
                }
                
                bool download = false;
                try {
                    download = Utilitario.DownloadFile(url[i], pathArquivoDestino);
                    if (download) {
                        this.nomeArquivosUnzip[i] = path + arq + hoje.Year.ToString() + mes + "SerieHist.xls";
                    }
                }
                // Se endereço falhou tenta segundo endereço2 - 1 mes antes
                catch (EnderecoHttpInvalido e1) {
                    bool download1 = Utilitario.DownloadFile(urlSegundaTentativa[i], pathArquivoDestino);
                    if (download1) {
                        this.nomeArquivosUnzip[i] = path + arq + mesPassado.Year.ToString() + mes1 + "SerieHist.xls";
                    }                                       
                }
                
                bool descompacta = Utilitario.UnzipFile(nomeArquivoDestino, path);
                
                if (descompacta) {
                    temArquivo = true;
                }
            }
            #endregion

            // Se arquivo existe retorna true
            return temArquivo;
        }

        /// <summary>
        /// Leitura dos arquivo Excel - CotacaoIBGE
        /// </summary>
        /// <param name="data">Filtro da Data para Pegar o PuBase</param>
        /// <param name="arquivo">Path completo do arquivo Excel que será lido</param>
        /// <return>null se cotacaoIBGE da data não foi encontrada</return>
        /// <exception cref="Exception">Se ocorreu problemas</exception>
        private CotacaoIBGE LerPlanilhaExcel(DateTime data, string arquivo) {
            CotacaoIBGE cotacaoIBGE = null;

            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            document.LoadFromFile(arquivo);

            Debug.WriteLine("Arquivo: " + arquivo);
            try {
                
                Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
                string descricao = workSheet.Name;
                
                //Debug.WriteLine("Planilha: " + descricao);

                int linhaAno = 191; //Linha do arquivo em que vamos comecar a procurar o ano
                int linhaMax = 2000; //Indice maximo de linha ate onde vamos procurar o ano
                //int colunaAno = 1, colunaCotacao = 3;
                
                int colunaAno = 0, colunaCotacao = 2;
                bool anoEncontrado = false;

                for (; linhaAno < linhaMax; linhaAno++) {
                    int ano;
                    object cellValue = workSheet.Cell(linhaAno, colunaAno).Value;
                    if (cellValue != null && int.TryParse(cellValue.ToString(), out ano)) {
                        if (ano == data.Year) {
                            anoEncontrado = true;
                            //
                            int linhaMes = linhaAno + data.Month - 1;
                            //
                            object val = workSheet.Cell(linhaMes, colunaCotacao).Value;
                            decimal valor;
                            if (val != null) {
                                if (decimal.TryParse(val.ToString(), out valor)) {
                                    cotacaoIBGE = new CotacaoIBGE();
                                    cotacaoIBGE.Data = data;
                                    cotacaoIBGE.Cotacao = Convert.ToDecimal(workSheet.Cell(linhaMes, colunaCotacao).Value);    
                                }                                            
                            }                                                                        
                            break;
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            finally {
                document.Close();
                document.Dispose();
            }

            return cotacaoIBGE;
        }
    }
}