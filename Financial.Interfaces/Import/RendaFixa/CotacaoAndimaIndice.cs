﻿using System;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common;
using System.IO;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Text;
using System.Diagnostics;
using System.Net;
using HtmlAgilityPack;
using System.Web;
using System.Text.RegularExpressions;

namespace Financial.Interfaces.Import.RendaFixa
{

    ///// <summary> FUNÇÕES DEPRECATED APÓS A MUDANÇA DO SITE DA ANBIMA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ///// Armazena os Valores Projetados de IPCA/IGPM a partir do HTML do site http://www.andima.com.br/
    ///// </summary>
    //[Serializable]
    //public class CotacaoAndimaIndice
    //{
    //    private DateTime data;
    //    private short indice;
    //    private decimal cotacao;

    //    public DateTime Data
    //    {
    //        get { return data; }
    //        set { data = value; }
    //    }

    //    public short Indice
    //    {
    //        get { return indice; }
    //        set { indice = value; }
    //    }

    //    public decimal Cotacao
    //    {
    //        get { return cotacao; }
    //        set { cotacao = value; }
    //    }

    //    /// <summary>
    //    /// Faz o Download do de IPCA/IGPM a partir do HTML do site http://www.andima.com.br/ e carrega na lista de indices
    //    /// </summary>
    //    /// <param name="data"></param>
    //    public List<CotacaoAndimaIndice> ProcessaCotacao(DateTime data)
    //    {
    //        List<CotacaoAndimaIndice> listaRetorno = new List<CotacaoAndimaIndice>();

    //        CotacaoAndimaIndice cotacaoAndimaIGPM = new CotacaoAndimaIndice();
    //        cotacaoAndimaIGPM.Indice = Financial.Common.Enums.ListaIndiceFixo.IGPM;
    //        if (cotacaoAndimaIGPM.BuscaCotacaoSite(data))
    //        {
    //            listaRetorno.Add(cotacaoAndimaIGPM);
    //        }


    //        CotacaoAndimaIndice cotacaoAndimaIPCA = new CotacaoAndimaIndice();
    //        cotacaoAndimaIPCA.Indice = Financial.Common.Enums.ListaIndiceFixo.IPCA;
    //        if (cotacaoAndimaIPCA.BuscaCotacaoSite(data))
    //        {
    //            listaRetorno.Add(cotacaoAndimaIPCA);
    //        }

    //        return listaRetorno;
    //    }

    //    /// <summary>
    //    /// Ler as cotacoes de INPC e IGPM da ANDIMA (projetado e efetivo)
    //    /// </summary>
    //    /// <param name="path"></param>
    //    /// <returns></returns>
    //    private bool BuscaCotacaoSite(DateTime data)
    //    {
    //        const string URL_ANDIMA = "http://portal.anbima.com.br/informacoes-tecnicas/precos/indices-de-precos/Pages/{0}.aspx";
    //        const string URL_ANDIMA_IGPM = "igp-m";
    //        const string URL_ANDIMA_IPCA = "ipca";

    //        string urlIndice = this.Indice == Financial.Common.Enums.ListaIndiceFixo.IGPM ? URL_ANDIMA_IGPM : URL_ANDIMA_IPCA;
    //        string urlSite = String.Format(URL_ANDIMA, urlIndice);

    //        //            WebClient webClient = new WebClient();
    //        //            String htmlSite = webClient.DownloadString(urlSite);
    //        //            HtmlWeb htmlWeb = new HtmlWeb();

    //        string htmlSite = GetPageAsString(new Uri(urlSite));

    //        //HtmlDocument htmlDocument = htmlWeb.Load(htmlSite);
    //        HtmlDocument htmlDocument = new HtmlDocument();
    //        htmlDocument.LoadHtml(htmlSite);

    //        HtmlNodeCollection htmlTableNodeCollection = htmlDocument.DocumentNode.SelectNodes("//table[@class='tabela']");

    //        bool temCotacao = false;
    //        //Processar duas primeiras tabelas que são as de projetado
    //        for (int indexTable = 0; indexTable <= 1; indexTable++)
    //        {
    //            HtmlNode htmlTableNode = htmlTableNodeCollection[indexTable];

    //            HtmlNode htmlBoldNode = htmlTableNode.SelectSingleNode("tr[1]/td[1]/b");
    //            string decodedInnerHtml = HttpUtility.HtmlDecode(htmlBoldNode.InnerHtml);

    //            //Exemplo de string a ser processada:Abril de 2011 ou Abril 2011

    //            Regex regex = new Regex(@"(?<MES_EXTENSO>\w+)\s(?:de\s)?(?<ANO>\d{4})$", RegexOptions.IgnoreCase);
    //            Match match;

    //            if ((match = regex.Match(decodedInnerHtml)).Success)
    //            {
    //                int mes = Financial.Util.Calendario.ConverteMesExtensoNumerico
    //                    (match.Groups["MES_EXTENSO"].Value, System.Globalization.CultureInfo.GetCultureInfo("pt-BR"));
    //                int ano = Int16.Parse(match.Groups["ANO"].Value);

    //                if (mes == data.Month && ano == data.Year)
    //                {
    //                    this.Data = data;

    //                    //Descobrir a cotacao mais recente nas demais rows
    //                    HtmlNodeCollection htmlTRNodeCollection = htmlTableNode.SelectNodes("tr");
    //                    foreach (HtmlNode htmlTRNode in htmlTRNodeCollection)
    //                    {
    //                        HtmlNodeCollection htmlTDNodeCollection = htmlTRNode.SelectNodes("td");
    //                        if (htmlTDNodeCollection.Count >= 2)
    //                        {
    //                            //Exisem tabelas com 4 e 3 colunas. Alem disso, a primeira coluna da terceira linha tem colspan, ou seja, essa linha tem o numero maximo de colunas e as demais tem numero maximo - 1
    //                            int maxColumns = indexTable == 0 ? 4 : 3;
    //                            HtmlNode htmlTDCotacaoNode = htmlTDNodeCollection.Count == maxColumns ? htmlTDNodeCollection[2] : htmlTDNodeCollection[1];
    //                            Decimal cotacaoAux;
    //                            if (htmlTDCotacaoNode != null && Decimal.TryParse(htmlTDCotacaoNode.InnerHtml, out cotacaoAux))
    //                            {
    //                                this.Cotacao = cotacaoAux;
    //                                temCotacao = true;
    //                            }
    //                        }
    //                    }

    //                    break;
    //                }

    //            }
    //        }

    //        //Se não encontramos cotação no projetado, tentar encontrar na tabela de efetivos (apenas procurando na primeira linha)
    //        if (!temCotacao)
    //        {
    //            HtmlNode htmlTableNode = htmlTableNodeCollection[2];

    //            HtmlNode htmlTRNode = htmlTableNode.SelectSingleNode("tr[3]");
    //            if (htmlTRNode != null)
    //            {
    //                HtmlNode htmlFirstTDNode = htmlTRNode.SelectSingleNode("td[1]/b");
    //                string mesString = HttpUtility.HtmlDecode(htmlFirstTDNode.InnerHtml);

    //                if (mesString.ToUpper().Contains("MAR"))
    //                {
    //                    mesString = "Março";
    //                }

    //                int mes = Financial.Util.Calendario.ConverteMesExtensoNumerico
    //                        (mesString, System.Globalization.CultureInfo.GetCultureInfo("pt-BR"));

    //                if (mes == data.Month)
    //                {
    //                    HtmlNodeCollection htmlTDNodeCollection = htmlTRNode.SelectNodes("td");
    //                    HtmlNode htmlTDCotacaoNode = htmlTDNodeCollection[4];
    //                    Decimal cotacaoAux;
    //                    if (htmlTDCotacaoNode != null && Decimal.TryParse(htmlTDCotacaoNode.InnerHtml, out cotacaoAux))
    //                    {
    //                        this.Data = data;
    //                        this.Cotacao = cotacaoAux;
    //                        temCotacao = true;
    //                    }
    //                }
    //            }
    //        }

    //        return temCotacao;
    //    }

    //    public static string GetPageAsString(Uri address)
    //    {
    //        string result = "";

    //        // Create the web request  
    //        HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

    //        // Get response  
    //        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
    //        {
    //            // Get the response stream  
    //            StreamReader reader = new StreamReader(response.GetResponseStream());

    //            // Read the whole contents and return as a string  
    //            result = reader.ReadToEnd();
    //        }

    //        return result;
    //    }  
    //}
}
