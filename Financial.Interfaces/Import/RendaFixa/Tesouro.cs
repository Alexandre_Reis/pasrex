﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Globalization;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;
using Financial.Util.Exceptions;

namespace Financial.Interfaces.Import.RendaFixa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Tesouro {

        #region Properties dos Registros
    
        #region Dados

        DateTime dataReferencia; // Campo 1

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        DateTime dataVencimento; // Campo 2

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        string descricao; // Campo3

        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }

        private decimal? pu;   // Campo4

        public decimal? Pu {
            get { return pu; }
            set { pu = value; }
        }       
        #endregion

        #endregion

        /// <summary>
        /// Processa o Arquivo de Tesouro
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">Path Completo do Diretorio de Destino do Download do arquivo xls do tesouro</param>
        /// <exception cref="ProcessaCotacaoTesouroException">
        /// Se Houve erro na leitura das planilhas Excel
        /// Se 0 registros foram lidos                                                
        /// </exception>
        public TesouroCollection ProcessaCotacaoTesouro(DateTime data, string path) {
            TesouroCollection retorno = new TesouroCollection();

            // Download Files - Status = true se arquivo está no diretorio, false caso contrario
            Dictionary<string, bool> status = this.DownloadTesouroXLS(data, path);
            //
            foreach (KeyValuePair<string, bool> pair in status) {
                if (pair.Value == true) {
                                        
                    string arquivo = path + pair.Key;

                    try {
                        // Ler Todas as Planilhas e Pegar pela Data e Inserir em TesouroCollection
                        TesouroCollection aux = this.LerPlanilhaExcel(data, arquivo);
                        retorno.Add(aux.CollectionTesouro); 
                    }
                    catch (Exception e) {
                        StringBuilder mensagem = new StringBuilder();
                        mensagem.Append("Processamento Cotação Tesouro com problemas: ")
                                .AppendLine(e.Message)
                                .AppendLine(e.StackTrace);
                        throw new ProcessaCotacaoTesouroException(e.Message);
                    }        
                }
            }

            if (retorno.CollectionTesouro.Count == 0) {
                throw new ProcessaCotacaoTesouroException("Nenhum Registro Importado na data " + data.ToString("d"));
            }

            return retorno;
        }

        /// <summary>
        /// Baixa os Arquivos do site do Tesouro, Caso o arquivo já esteja no Diretorio, não baixa o arquivo
        /// Para ano = Atual sempre baixa o arquivo pois os dados estão sempre mudando
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">Path Completo do Diretorio onde o arquivo Tesouro será procurado</param>
        /// <returns>true se o arquivo com a determinada chave está no diretorio, false caso contrario</returns>
        private Dictionary<string, bool> DownloadTesouroXLS(DateTime data, string path) {
            //
            int ano = data.Year;

            int anoAtual = DateTime.Today.Year;

            string prefixoDiretorio = ano < 2012 ? "" : "historico";
            string ntnbPrincipal = ano < 2012 ? "NTNB_Principal_" : "historicoNTNBPrincipal_";

            string ntnC = ano >= 2012 ? "NTN-C_" : "NTNC_";
            string ntnB = ano >= 2012 ? "NTN-B_" : "NTNB_";
            string ntnF = ano >= 2012 ? "NTN-F_" : "NTNF_";

            if (ano >= 2012)
            {
                ntnbPrincipal = ntnbPrincipal.Replace("NTNB", "NTN-B");
            }

            string diaAtual = DateTime.Now.ToString("ddMMyy");// ex.: "131112" - arquivo em 13 de novembro de 2012
            
            // path arquivos LFT/LTN/NTNC/NTNB/NTNBPrincipal/NTNF
            List<string> enderecoHttp;
            if (ano >= 2012)
            {
                enderecoHttp = new List<string>(new string[] {
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/LFT_" + ano + ".xls",
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/LTN_" + ano + ".xls",
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/NTN-C_" + ano + ".xls",
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/NTN-B_" + ano + ".xls",
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/NTN-B_Principal_" + ano + ".xls",
                    @"https://www.tesouro.fazenda.gov.br/documents/10180/137713/NTN-F_" + ano + ".xls"
                });
            }
            else
            {
                enderecoHttp = new List<string>(new string[] {
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/" + ano + "/" + prefixoDiretorio + "LFT_" + ano +".xls",
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/"+ano+"/"+prefixoDiretorio+"LTN_"+ano+".xls",
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/"+ano+"/"+prefixoDiretorio+ntnC+ano+".xls",
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/"+ano+"/"+prefixoDiretorio+ntnB+ano+".xls",
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/"+ano+"/"+ntnbPrincipal+ano+".xls",
                    @"http://www3.tesouro.gov.br/tesouro_direto/download/historico/"+ano+"/"+prefixoDiretorio+ntnF+ano+".xls"
                });
            }

            List<string> nomeArquivos = new List<string>(new string[] { 
              "LFT_"+ano+".xls", "LTN_"+ano+".xls", ntnC+ano+".xls",
              ntnB+ano+".xls", ntnbPrincipal+ano+".xls", ntnF+ano+".xls"
            });
            
            // Indica o Status do Download de Cada Arquivo
            Dictionary<string, bool> statusDownload = new Dictionary<string, bool>();
            statusDownload.Add("LFT_" + ano + ".xls", false);
            statusDownload.Add("LTN_" + ano + ".xls", false);
            statusDownload.Add(ntnC + ano + ".xls", false);
            statusDownload.Add(ntnB + ano + ".xls", false);
            statusDownload.Add(ntnbPrincipal + ano + ".xls", false);
            statusDownload.Add(ntnF + ano + ".xls", false);
            //

            #region Download

            for (int i = 0; i < enderecoHttp.Count; i++) {
                string pathArquivoDestino = path + nomeArquivos[i];

                // Se for Ano Atual sempre faz Download
                if (ano >= 2012) {
                    try {
                        bool download = Utilitario.DownloadFileUsingHttpWebRequest(enderecoHttp[i], pathArquivoDestino);
                        //bool download = Utilitario.DownloadFile(enderecoHttp[i], pathArquivoDestino);
                        statusDownload[nomeArquivos[i]] = download;
                    }
                    catch (EnderecoHttpInvalido e) {  // Se endereço http não existe
                        statusDownload[nomeArquivos[i]] = false;
                    }
                }
                else {
                    #region Se nao for Ano Atual
                    if (!File.Exists(pathArquivoDestino)) {

                        /* Não tenta fazer download de alguns arquivos nos anos 2002/2003/2004 pois não existe o endereço
                            Ano 2002 - Não tem NTN-B Principal/NTN-F/NTN-B
                            Ano 2003 - Não tem NTN-B Principal/NTN-F                    
                            Ano 2004 - Não tem NTN-B Principal
                        */
                        try {
                            bool download = Utilitario.DownloadFile(enderecoHttp[i], pathArquivoDestino);
                            statusDownload[nomeArquivos[i]] = download;
                        }
                        catch (EnderecoHttpInvalido e) {  // Se endereço http não existe captura a exceção e passa reto
                            statusDownload[nomeArquivos[i]] = false;
                        }
                    }
                    else {
                        statusDownload[nomeArquivos[i]] = true;
                    }
                    #endregion
                }
            }
            #endregion

            return statusDownload;
        }

        /// <summary>
        /// Leitura dos arquivo Excel - (LFT, LTN, NTN-C, NTN-B, NTN-B Principal, NTN-F)
        /// </summary>
        /// <param name="data">Filtro da Data para Pegar o PuBase</param>
        /// <param name="arquivo">Path completo do arquivo Excel que será lido</param>
        /// <exception cref="Exception">Se ocorreu problemas</exception>
        private TesouroCollection LerPlanilhaExcel(DateTime data, string arquivo) {
            TesouroCollection tesouroCollection = new TesouroCollection();

            #region Leitura do Arquivo Excel
            // Open Spreadsheet
            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            document.LoadFromFile(arquivo);

            Debug.WriteLine("Arquivo: "+arquivo);

            for (int i = 0; i < document.Workbook.Worksheets.Count; i++) {
                Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[i];
                string descricao = workSheet.Name;

                Debug.WriteLine("Planilha: " + descricao);

                //
                int index = 0;
                // This row,column index should be changed as per your need.
                // i.e. which cell in the excel you are interesting to read.
                //
                /* Formato: 1)Data -  6)PuBase
                 */
                int linha = 2;
                int coluna1 = 0, coluna6 = 5;
               
                try {
                    // Enquanto Data tiver valor
                    while (workSheet.Cell(linha, coluna1).Value != null) {
                        Debug.WriteLine("linha: " + linha);
                        //
                        Tesouro item = new Tesouro();
                        //
                        item.DataReferencia = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                        if (workSheet.Cell(linha, coluna6).Value != null && !String.IsNullOrEmpty(workSheet.Cell(linha, coluna6).ValueAsString) ) {
                            item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);                  
                        }
                       
                        // Achei o item
                        if (item.DataReferencia == data && item.Pu.HasValue) {
                            //
                            item.Descricao = descricao;
                            item.DataVencimento = workSheet.Cell(0, 1).ValueAsDateTime;
                            //
                            tesouroCollection.Add(item);
                            break; // Stop da Leitura
                        }                        
                        //
                        index++;
                        linha = 1 + index;
                        //                
                        Console.WriteLine("{0},{1}", item.DataReferencia, item.Pu);
                        //
                    } 
                }
                catch (Exception ex) {
                    document.Close();
                    document.Dispose();

                    throw new Exception(ex.Message);
                }

                document.Close();
                document.Dispose();
            }
            #endregion

            return tesouroCollection;
        }
    }
}