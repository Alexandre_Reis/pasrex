using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa {
    public class AndimaCollection {
        private List<Andima> collectionAndima;

        public List<Andima> CollectionAndima {
            get { return collectionAndima; }
            set { collectionAndima = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public AndimaCollection() {
            this.collectionAndima = new List<Andima>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="andima">Elemento a inserir na lista de Andima</param>
        public void Add(Andima andima) {
            collectionAndima.Add(andima);
        }
    }
}
