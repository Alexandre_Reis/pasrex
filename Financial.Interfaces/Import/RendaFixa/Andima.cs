﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Globalization;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;

namespace Financial.Interfaces.Import.RendaFixa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Andima {

        // Informação sobre o tipo registro da linha do arquivo Andima
        private int tipoRegistro;

        #region Properties dos registros

        #region Header
        string header;

        public string Header {
            get { return header; }
            set { header = value; }
        }
        #endregion

        #region SubHeader
        string[] subHeader;

        public string[] SubHeader {
            get { return subHeader; }
            set { subHeader = value; }
        }       
        #endregion

        #region Dados

        string descricao; // Campo1

        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        } 

        DateTime dataReferencia; // Campo 2

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        string codigoSelic; // Campo 3

        public string CodigoSelic {
            get { return codigoSelic; }
            set { codigoSelic = value; }
        }

        DateTime dataEmissao; // Campo 4

        public DateTime DataEmissao {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }

        DateTime dataVencimento; // Campo 5

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        decimal taxaIndicativa; // Campo 8

        public decimal TaxaIndicativa {
            get { return taxaIndicativa; }
            set { taxaIndicativa = value; }
        }

        decimal pu; // Campo 10

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        #endregion

        #endregion
       
        /// <summary>
        /// Processa o Arquivo de Andima
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Bdin será procurado</param>
        /// throws ArquivoAndimaNaoEncontradoException
        public AndimaCollection ProcessaAndima(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato3);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("MercadoAndima").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                this.DownloadAndima(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString())) {
                    throw new ArquivoAndimaNaoEncontradoException("\nArquivo Andima: " + nomeArquivo.ToString() + " não encontrado\n");
                }
            }

            AndimaCollection andimaCollection = new AndimaCollection();
            int i = 1;
            string linha = "";            
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.SetTipoRegistro(i);
                            this.TrataAndima(linha, data);
                            Andima andima = (Andima)Utilitario.Clone(this);                            
                            andimaCollection.Add(andima);                           
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Processamento Andima com problema: ")
                        .AppendLine(e.Message)
                        .AppendLine(e.StackTrace)
                        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaAndimaException(e.Message);
            }

            return andimaCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo Andima 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataAndima(string linha, DateTime data) {
            
            #region EstruturaArquivoAndima
            if (this.IsTipoRegistroHeader()) {                
                #region Header
                this.header = linha.TrimStart().TrimEnd();
                #endregion                               
            }
            //
            else if (this.IsTipoRegistroSubHeader()) {
                #region SubHeader
                this.subHeader = linha.Split(new char[] { '@' });
                #endregion
            }
            //
            else if (this.IsTipoRegistroDados()) {
                #region Dados
                //
                string[] dados = linha.Split(new char[] { '@' });
                /* Campo 1 - posicao 0 */
                this.descricao = dados[0];
                                
                /* Campo 2 - posicao 1 */
                string dataReferencia = dados[1];
                // Formato = "AAAAMMdd"                
                int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
                int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
                int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
                //
                this.dataReferencia = new DateTime(ano, mes, dia);
                // Se as datas não forem iguais arquivo sendo lido está incorreto
                if (this.dataReferencia.CompareTo(data) != 0) {
                    throw new ArquivoAndimaIncorretoException("Arquivo Andima {0} incorreto " + dataReferencia);
                }
                                            
                /* Campo 3 - posicao 2 */
                this.codigoSelic = dados[2];

                /* Campo 4 - posicao 3 */
                string dataEmissao = dados[3];
                // Formato = "AAAAMMdd"                
                dia = Convert.ToInt32(dataEmissao.Substring(6, 2));
                mes = Convert.ToInt32(dataEmissao.Substring(4, 2));
                ano = Convert.ToInt32(dataEmissao.Substring(0, 4));
                //
                this.dataEmissao = new DateTime(ano, mes, dia);

                /* Campo 5 - posicao 4 */
                string dataVencimento = dados[4];
                // Formato = "AAAAMMdd"                
                dia = Convert.ToInt32(dataVencimento.Substring(6, 2));
                mes = Convert.ToInt32(dataVencimento.Substring(4, 2));
                ano = Convert.ToInt32(dataVencimento.Substring(0, 4));
                //
                this.dataVencimento = new DateTime(ano, mes, dia);
                //
                // Campo 8 - posicao 7
                NumberFormatInfo formato = new NumberFormatInfo();
                formato.CurrencyDecimalSeparator = ".";

                string taxaIndicativaAux = dados[7];
                taxaIndicativaAux = taxaIndicativaAux.Replace("," , ".");
                //
                this.taxaIndicativa = !String.IsNullOrEmpty(taxaIndicativaAux) ? Convert.ToDecimal(taxaIndicativaAux, formato) : 0;

                // Campo 9 - posicao 8
                string puAux = dados[8];
                
                // Se o numero está em notação Cientifica faz o tratamento
                if (puAux.ToUpper().Trim().Contains("E")) {
                    this.pu = Utilitario.ConvertTipos.NotacaoCientificaToDecimal(puAux);
                    //this.pu = Utilitario.Truncate(this.pu, 10);
                }
                else {
                    puAux = puAux.Replace(",", ".");
                    this.pu = !String.IsNullOrEmpty(puAux) ? Convert.ToDecimal(puAux, formato) : 0;    
                }                                                          
                #endregion                
            }                                 
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo Andima da Internet.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Andima será procurado</param>
        /// <returns></returns>
        private bool DownloadAndima(DateTime data, string path)
        {
            if (data >= new DateTime(2012, 07, 09))
            {
                return DownloadAnbima(data, path);
            }
            else
            {
                return DownloadAndimaAntigo(data, path);
            }
        }

        /// <summary>
        /// Baixa o arquivo Andima da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Andima será procurado</param>
        /// <returns></returns>
        private bool DownloadAndimaAntigo(DateTime data, string path) {
            
            #region Download
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);

            string endereco = "http://www.andima.com.br/merc_sec/arqs/ms" + dataString.ToString() + ".exe";
            string nomeArquivoDestino = "Andima.exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();

            #region Unzip do Arquivo
            if (download) {

                // Deleta o arquivo MS+data.txt se existir para que não apareça mensagem de sobreposição
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("MS" + dataString.ToString()+".txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion
                //
                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("Andima: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion
                //
                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);
                //
                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    StringBuilder dataStringAux = Utilitario.FormataData(data, PatternData.formato3);
                    nomeArquivoNovo.Append(path)
                               .Append("MercadoAndima").Append(dataStringAux.ToString()).Append(".txt");

                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());                    
                }
                #endregion
            }
            #endregion
           
            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou deleta o zip
            if (arquivoDescompactado) {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString())) {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo Andima+data.txt existe retorna true            
            return arquivoDescompactado;
        }

        /// <summary>
        /// Baixa o arquivo Andima da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Andima será procurado</param>
        /// <returns></returns>
        private bool DownloadAnbima(DateTime data, string path)
        {
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);

            string endereco = "http://www.anbima.com.br/merc_sec/arqs/ms" + dataString.ToString() + ".txt";
            string nomeArquivoDestino = "ms" + dataString.ToString() + ".txt";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            
            bool arquivoBaixado = File.Exists(pathArquivoDestino);

            if (arquivoBaixado)
            {
                StringBuilder nomeArquivoNovo = new StringBuilder();
                StringBuilder dataStringAux = Utilitario.FormataData(data, PatternData.formato3);
                nomeArquivoNovo.Append(path)
                           .Append("MercadoAndima").Append(dataStringAux.ToString()).Append(".txt");

                Utilitario.RenameFile(pathArquivoDestino.ToString(), nomeArquivoNovo.ToString());
            }

            // Se arquivo existe retorna true            
            return arquivoBaixado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha"></param>
        private void SetTipoRegistro(int linha) {

            #region Prenche o TipoRegistro do Arquivo Andima
            switch (linha) {
                case 1:
                    this.tipoRegistro = EstruturaArquivoAndima.Header;
                    break;
                case 3:
                    this.tipoRegistro = EstruturaArquivoAndima.SubHeader;
                    break;
                default:
                    this.tipoRegistro = EstruturaArquivoAndima.Dados;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoAndima.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é SubHeader</returns>
        public bool IsTipoRegistroSubHeader() {
            return this.tipoRegistro == EstruturaArquivoAndima.SubHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Dados</returns>
        public bool IsTipoRegistroDados() {
            return this.tipoRegistro == EstruturaArquivoAndima.Dados;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Dados
        /// </summary>
        /// <param name="andima"></param>
        /// 
        /// <returns> true se TipoRegistro = Dados
        ///           false se TipoRegistro != Dados
        /// </returns>
        public bool FilterAndimaByDados(Andima andima) {
            return andima.tipoRegistro == EstruturaArquivoAndima.Dados;
        }
    }
}
