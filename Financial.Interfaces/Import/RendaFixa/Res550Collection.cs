using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa {
    public class Res550Collection {
        private List<Res550> collectionRes550;

        public List<Res550> CollectionRes550 {
            get { return collectionRes550; }
            set { collectionRes550 = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public Res550Collection() {
            this.collectionRes550 = new List<Res550>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="res550">Elemento a inserir na lista de Res550</param>
        public void Add(Res550 res550) {
            collectionRes550.Add(res550);
        }
    }
}
