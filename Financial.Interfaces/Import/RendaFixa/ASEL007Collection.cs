using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa
{
    public class ASEL007Collection
    {
        private List<ASEL007> collectionASEL007;

        public List<ASEL007> CollectionASEL007
        {
            get { return collectionASEL007; }
            set { collectionASEL007 = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ASEL007Collection()
        {
            this.collectionASEL007 = new List<ASEL007>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ASEL007">Elemento a inserir na lista de ASEL007</param>
        public void Add(ASEL007 asel007)
        {
            collectionASEL007.Add(asel007);
        }
    }
}
