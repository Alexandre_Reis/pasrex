using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa {
    public class DebentureCollection {
        private List<Debenture> collectionDebenture;

        public List<Debenture> CollectionDebenture {
            get { return collectionDebenture; }
            set { collectionDebenture = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public DebentureCollection() {
            this.collectionDebenture = new List<Debenture>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="debenture">Elemento a inserir na lista de Debenture</param>
        public void Add(Debenture debenture) {
            collectionDebenture.Add(debenture);
        }
    }
}
