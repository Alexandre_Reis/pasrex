﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Financial.Util;
using System.Configuration;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.BMF.Exceptions;
using Dart.PowerTCP.Zip;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using Financial.Util.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Fundo.Enums;

namespace Financial.Interfaces.Import.RendaFixa
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ASEL007
    {
        // Informação sobre o tipo registro da linha do arquivo ASEL007
        private int tipoRegistro;

        #region Header

        DateTime data;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }
        #endregion

        #region Properties dos registros

        string codigoTitulo;
        public string CodigoTitulo
        {
            get { return codigoTitulo; }
            set { codigoTitulo = value; }
        }

        DateTime dataVencimento;
        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        string codigoISIN;
        public string CodigoISIN
        {
            get { return codigoISIN; }
            set { codigoISIN = value; }
        }

        string siglaTitulo;
        public string SiglaTitulo
        {
            get { return siglaTitulo; }
            set { siglaTitulo = value; }
        }

        string indPossibilidadeNegociacao;
        public string IndPossibilidadeNegociacao
        {
            get { return indPossibilidadeNegociacao; }
            set { indPossibilidadeNegociacao = value; }
        }

        string indBloqueio;
        public string IndBloqueio
        {
            get { return indBloqueio; }
            set { indBloqueio = value; }
        }

        DateTime dataPrimeiraEmissao;
        public DateTime DataPrimeiraEmissao
        {
            get { return dataPrimeiraEmissao; }
            set { dataPrimeiraEmissao = value; }
        }

        string posicaoGeralCustodia;
        public string PosicaoGeralCustodia
        {
            get { return posicaoGeralCustodia; }
            set { posicaoGeralCustodia = value; }
        }

        string tipoRendimento;
        public string TipoRendimento
        {
            get { return tipoRendimento; }
            set { tipoRendimento = value; }
        }

        decimal valorResgate;
        public decimal ValorResgate
        {
            get { return valorResgate; }
            set { valorResgate = value; }
        }

        DateTime dataBase;
        public DateTime DataBase
        {
            get { return dataBase; }
            set { dataBase = value; }
        }

        decimal valorNominalDataBase;
        public decimal ValorNominalDataBase
        {
            get { return valorNominalDataBase; }
            set { valorNominalDataBase = value; }
        }

        string indiceAtualizacaoValorNominal;
        public string IndiceAtualizacaoValorNominal
        {
            get { return indiceAtualizacaoValorNominal; }
            set { indiceAtualizacaoValorNominal = value; }
        }

        string indicadorPagamentoCupomJuros;
        public string IndicadorPagamentoCupomJuros
        {
            get { return indicadorPagamentoCupomJuros; }
            set { indicadorPagamentoCupomJuros = value; }
        }

        string periodicidadePgtoJuros;
        public string PeriodicidadePgtoJuros
        {
            get { return periodicidadePgtoJuros; }
            set { periodicidadePgtoJuros = value; }
        }

        decimal taxaJuros;
        public decimal TaxaJuros
        {
            get { return taxaJuros; }
            set { taxaJuros = value; }
        }

        string regimeCapitalizacaoJuros;
        public string RegimeCapitalizacaoJuros
        {
            get { return regimeCapitalizacaoJuros; }
            set { regimeCapitalizacaoJuros = value; }
        }

        string indicadorPossibilidadeDesmembramento;
        public string IndicadorPossibilidadeDesmembramento
        {
            get { return indicadorPossibilidadeDesmembramento; }
            set { indicadorPossibilidadeDesmembramento = value; }
        }

        public decimal precoUnitarioPagamentoJuros;
        public decimal PrecoUnitarioPagamentoJuros
        {
            get { return precoUnitarioPagamentoJuros; }
            set { precoUnitarioPagamentoJuros = value; }
        }

        string indicadorAmortizacaoPrincipal;
        public string IndicadorAmortizacaoPrincipal
        {
            get { return indicadorAmortizacaoPrincipal; }
            set { indicadorAmortizacaoPrincipal = value; }
        }

        string periodicidadeAmortizacao;
        public string PeriodicidadeAmortizacao
        {
            get { return periodicidadeAmortizacao; }
            set { periodicidadeAmortizacao = value; }
        }

        decimal percentualAmortizacao;
        public decimal PercentualAmortizacao
        {
            get { return percentualAmortizacao; }
            set { percentualAmortizacao = value; }
        }

        int numeroParcelas;
        public int NumeroParcelas
        {
            get { return numeroParcelas; }
            set { numeroParcelas = value; }
        }

        decimal precoUnitarioPagamentoAmortizacao;
        public decimal PrecoUnitarioPagamentoAmortizacao
        {
            get { return precoUnitarioPagamentoAmortizacao; }
            set { precoUnitarioPagamentoAmortizacao = value; }
        }

        decimal precoUnitarioLastro;
        public decimal PrecoUnitarioLastro
        {
            get { return precoUnitarioLastro; }
            set { precoUnitarioLastro = value; }
        }

        decimal valorNominalAtualizado;
        public decimal ValorNominalAtualizado
        {
            get { return valorNominalAtualizado; }
            set { valorNominalAtualizado = value; }
        }

        decimal precoUnitarioRetornoResgate;
        public decimal PrecoUnitarioRetornoResgate
        {
            get { return precoUnitarioRetornoResgate; }
            set { precoUnitarioRetornoResgate = value; }
        }

        decimal precoUnitarioPagamentoResgate;
        public decimal PrecoUnitarioPagamentoResgate
        {
            get { return precoUnitarioPagamentoResgate; }
            set { precoUnitarioPagamentoResgate = value; }
        }

        #endregion

        /// <summary>
        /// Processa arquivo de ASEL007
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Indic será procurado</param>
        /// throws ArquivoTaxaSwapNaoEncontradoException
        public ASEL007Collection ProcessaASEL007(DateTime data, string path)
        {
            #region Verifica se Arquivo está no Diretorio
            int ano = data.Year;
            int mes = data.Month;
            int dia = data.Day;

            string anoString = Convert.ToString(data.Year);
            string mesString = Convert.ToString(data.Month);
            string diaString = Convert.ToString(data.Day);
            if (mes < 10)
            {
                mesString = "0" + mesString;
            }
            if (dia < 10)
            {
                diaString = "0" + diaString;
            }

            StringBuilder dataString = new StringBuilder();
            dataString.Append(anoString).Append(mesString).Append(diaString);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append(dataString).Append("ASEL007").Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString()))
            {
                // Download File    
                this.DownloadASEL007(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString()))
                {
                    throw new ArquivoASEL007NaoEncontradoException("\nArquivo ASEL007: " + nomeArquivo.ToString() + " não encontrado\n");
                }
            }

            ASEL007Collection asel007Collection = new ASEL007Collection();
            int i = 1;
            string linha = "";
            try
            {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default))
                {
                    while ((linha = sr.ReadLine()) != null)
                    {
                        this.TrataASEL007(linha, data);
                        ASEL007 asel007 = (ASEL007)Utilitario.Clone(this);

                        #region Cadastra Papel/Indice/Titulo
                        if (this.IsTipoRegistroDados())
                        {
                            PapelRendaFixa papelRendaFica = this.cadastraPapel();
                            Indice indice = this.cadastraIndice();
                            TituloRendaFixa tituloRendaFixa = this.cadastraTitulo(papelRendaFica, indice);
                            this.cadastraAgenda(tituloRendaFixa);
                        }
                        #endregion

                        asel007Collection.Add(asel007);
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento TaxaSwap com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaASEL007Exception(e.Message);
            }

            return asel007Collection;
        }

        private PapelRendaFixa cadastraPapel()
        {
            //Verifica se papel já existe
            PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
            papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Descricao.Equal(this.siglaTitulo));
            if (papelRendaFixaCollection.Query.Load())
            {
                if (papelRendaFixaCollection.Count > 0) return papelRendaFixaCollection[0];
            }

            //Seleciona último ID cadastradado
            papelRendaFixaCollection = new PapelRendaFixaCollection();
            papelRendaFixaCollection.Query.OrderBy(papelRendaFixaCollection.Query.IdPapel.Descending);
            papelRendaFixaCollection.Query.es.Top = 1;
            papelRendaFixaCollection.Query.Load();
            int idPapel = 1;
            if (papelRendaFixaCollection[0].IdPapel.HasValue)
                idPapel = papelRendaFixaCollection[0].IdPapel.Value + 1;

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.IdPapel = idPapel;
            papelRendaFixa.TipoPapel = (int)TipoPapelTitulo.Publico;
            papelRendaFixa.Descricao = this.siglaTitulo;
            papelRendaFixa.TipoRentabilidade = this.tipoRendimento.Equals("POS")?(byte)TipoRentabilidadeTitulo.PosFixado : (byte)TipoRentabilidadeTitulo.PreFixado;
            papelRendaFixa.CasasDecimaisPU = 8;
            papelRendaFixa.TipoCurva = (int)TipoCurvaTitulo.Exponencial;
            papelRendaFixa.ContagemDias = (int)ContagemDiasTitulo.Uteis;
            papelRendaFixa.BaseAno = (int)BaseCalculoTitulo.Base252;
            papelRendaFixa.TipoVolume = (int)TipoVolumeTitulo.Quantidade;
            papelRendaFixa.TipoCustodia = (byte)LocalCustodiaFixo.Selic;
            papelRendaFixa.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
            papelRendaFixa.IdLocalCustodia = (byte)LocalCustodiaFixo.Selic;
            papelRendaFixa.IdClearing = (byte)ClearingFixo.Selic;
            if (this.siglaTitulo.Substring(0, 3).Equals("LFT"))
                papelRendaFixa.Classe = (int)ClasseRendaFixa.LFT;
            if (this.siglaTitulo.Substring(0, 3).Equals("LTN"))
                papelRendaFixa.Classe = (int)ClasseRendaFixa.LTN;
            if (this.siglaTitulo.Substring(0, 3).Equals("NTN"))
                papelRendaFixa.Classe = (int)ClasseRendaFixa.NTN;
            
            papelRendaFixa.PagamentoJuros = (int)PagamentoJurosTitulo.ContaDias;
            papelRendaFixa.Save();

            return papelRendaFixa;
        }

        private string SelecionaDeParaIndice(string codigoExterno)
        {
            string codigoInterno = codigoExterno;

            DeParaQuery deParaQuery = new DeParaQuery("DPQ");
            TipoDeParaQuery tipoDeParaQuery = new TipoDeParaQuery("TDPQ");
            DeParaCollection deParaCollenction = new DeParaCollection();

            deParaQuery.InnerJoin(tipoDeParaQuery).On(deParaQuery.IdTipoDePara.Equal(tipoDeParaQuery.IdTipoDePara));
            deParaQuery.Where(deParaQuery.CodigoExterno.Equal(codigoExterno),
                              tipoDeParaQuery.EnumTipoDePara.Equal(EnumTipoDePara.Importacao_ASEL007_Indice));
            deParaCollenction.Load(deParaQuery);
            if(deParaCollenction.Count > 0)
                codigoInterno = deParaCollenction[0].CodigoInterno;

            return codigoInterno;
        }

        private Indice cadastraIndice()
        {
            //Se não tem indice, retorna nulo
            if(this.indiceAtualizacaoValorNominal.Trim().Equals("")) return new Indice();

            //Seleciona codigo de pesquisa (De Para)
            string descricaoIndice = this.SelecionaDeParaIndice(this.IndiceAtualizacaoValorNominal);

            IndiceCollection indiceCollection = new IndiceCollection();
            indiceCollection.Query.Where(indiceCollection.Query.Descricao.Equal(descricaoIndice));
            indiceCollection.Query.Load();
            if(indiceCollection.Count > 0) return indiceCollection[0];

            //Seleciona último ID cadastradado
            indiceCollection = new IndiceCollection();
            indiceCollection.Query.OrderBy(indiceCollection.Query.IdIndice.Descending);
            indiceCollection.Query.es.Top = 1;
            indiceCollection.Query.Load();
            int idIndice = 1;
            if(indiceCollection[0].IdIndice.HasValue)
                idIndice = indiceCollection[0].IdIndice.Value + 1;

            Indice indice = new Indice();
            indice.IdIndice = (short)idIndice;
            indice.Descricao = this.IndiceAtualizacaoValorNominal;
            indice.Tipo = (int)TipoIndice.Decimal;
            indice.TipoDivulgacao = (int)TipoDivulgacaoIndice.Diario;
            indice.IdFeeder = 5;
            indice.Save();

            return indice;
        }

        private TituloRendaFixa cadastraTitulo(PapelRendaFixa papelRendaFixa, Indice indice)
        {
            //Verifica se titulo já existe
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.Descricao.Equal(this.siglaTitulo),
                                                  tituloRendaFixaCollection.Query.DataVencimento.Equal(this.dataVencimento));
            if (tituloRendaFixaCollection.Query.Load())
            {
                if (tituloRendaFixaCollection.Count > 0)
                    return tituloRendaFixaCollection[0];
            }

            //Seleciona último ID cadastrado
            tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.OrderBy(tituloRendaFixaCollection.Query.IdTitulo.Descending);
            tituloRendaFixaCollection.Query.es.Top = 1;
            tituloRendaFixaCollection.Query.Load();
            int idTitulo = 1;
            if (tituloRendaFixaCollection[0].IdTitulo.HasValue)
                idTitulo = tituloRendaFixaCollection[0].IdTitulo.Value + 1;

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.IdTitulo = idTitulo;
            tituloRendaFixa.IdPapel = papelRendaFixa.IdPapel.Value;
            if(indice.IdIndice.HasValue)
                tituloRendaFixa.IdIndice = indice.IdIndice.Value;
            tituloRendaFixa.IdEmissor = 99999;
            tituloRendaFixa.Descricao = this.SiglaTitulo;
            tituloRendaFixa.Taxa = this.TaxaJuros;
            if (indice.IdIndice.HasValue)
                tituloRendaFixa.Percentual = 100;
            tituloRendaFixa.DataEmissao = this.DataPrimeiraEmissao;
            tituloRendaFixa.DataVencimento = this.DataVencimento;
            tituloRendaFixa.ValorNominal = this.ValorNominalAtualizado;
            tituloRendaFixa.PUNominal = this.PrecoUnitarioLastro;
            tituloRendaFixa.DescricaoCompleta = this.SiglaTitulo + " Vcto: " + this.dataVencimento.ToString("dd-MM-yyyy");
            tituloRendaFixa.IsentoIR = 1;
            tituloRendaFixa.IsentoIOF = 1;
            tituloRendaFixa.IdMoeda = 1;
            tituloRendaFixa.IdEstrategia = 2;
            tituloRendaFixa.CodigoIsin = this.CodigoISIN;
            tituloRendaFixa.DebentureConversivel = "N";
            tituloRendaFixa.DebentureInfra = "N";
            tituloRendaFixa.CriterioAmortizacao = 2;
            tituloRendaFixa.PermiteRepactuacao = "N";
            tituloRendaFixa.ExecutaProRataEmissao = "N";
            tituloRendaFixa.TipoProRata = 0;
            tituloRendaFixa.DefasagemLiquidacao = 0;
            tituloRendaFixa.ProRataLiquidacao = "N";
            tituloRendaFixa.Save();

            return tituloRendaFixa;

        }

        private void cadastraAgenda(TituloRendaFixa tituloRendaFixa)
        {
            //Se já existir agenda, não faz nada.
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value));
            if (agendaRendaFixaCollection.Query.Load())
            {
                if (agendaRendaFixaCollection.Count > 0)
                    return;

            }

            DateTime dataAux = tituloRendaFixa.DataVencimento.Value;
            int periodicidade = 1;

            //Cadastra Juros
            if (this.IndicadorPagamentoCupomJuros.Equals("S"))
            {
                while (dataAux > tituloRendaFixa.DataEmissao)
                {
                    DateTime dataPagamento = dataAux;
                    if (!Calendario.IsDiaUtil(dataAux))
                    {
                        dataPagamento = Calendario.AdicionaDiaUtil(dataAux, 1);
                    }

                    AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                    agendaRendaFixa.DataAgenda = dataAux;
                    agendaRendaFixa.DataEvento = dataAux;
                    agendaRendaFixa.DataPagamento = dataPagamento;
                    agendaRendaFixa.IdTitulo = tituloRendaFixa.IdTitulo.Value;
                    agendaRendaFixa.Taxa = this.TaxaJuros;
                    agendaRendaFixa.TipoEvento = (int)TipoEventoTitulo.Juros;
                    agendaRendaFixa.Save();

                    if (this.PeriodicidadePgtoJuros.Equals("SEM"))
                        periodicidade = 6;

                    dataAux = dataAux.AddMonths(periodicidade * -1);
                }
            }

            //Cadastra Amortização
            if (this.IndicadorAmortizacaoPrincipal.Equals("S"))
            {
                while (dataAux > tituloRendaFixa.DataEmissao)
                {
                    DateTime dataPagamento = dataAux;
                    if (!Calendario.IsDiaUtil(dataAux))
                    {
                        dataPagamento = Calendario.AdicionaDiaUtil(dataAux, 1);
                    }

                    AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                    agendaRendaFixa.DataAgenda = dataAux;
                    agendaRendaFixa.DataEvento = dataAux;
                    agendaRendaFixa.DataPagamento = dataPagamento;
                    agendaRendaFixa.IdTitulo = tituloRendaFixa.IdTitulo.Value;
                    agendaRendaFixa.Taxa = this.PercentualAmortizacao;
                    agendaRendaFixa.TipoEvento = (int)TipoEventoTitulo.Juros;
                    agendaRendaFixa.Save();

                    if (this.PeriodicidadeAmortizacao.Equals("SEM"))
                        periodicidade = 6;

                    dataAux = dataAux.AddMonths(periodicidade * -1);
                }
            }
        }

        /// <summary>
        /// Salva os atributos do arquivo ASEL007 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataASEL007(string linha, DateTime data)
        {
            //Define qual o tipo de registro a ser percorrido
            int.TryParse(linha.Substring(0, 1), out this.tipoRegistro);

            #region EstruturaArquivoASEL007
            if (this.IsTipoRegistroHeader())
            {
                #region Header
                // Formato = "DD/MM/AAAA"
                string dataReferencia = linha.Substring(11, 8);
                int ano = Convert.ToInt32(dataReferencia.Substring(0, 4));
                int mes = Convert.ToInt32(dataReferencia.Substring(4, 2));
                int dia = Convert.ToInt32(dataReferencia.Substring(6, 2));
                //
                this.data = new DateTime(ano, mes, dia);
                if (this.data.CompareTo(data) != 0)
                {
                    #region Deleta arquivo incorreto
                    // Deleta o arquivo Incorreto
                    string anoString = Convert.ToString(data.Year);
                    string mesString = (data.Month < 10) ? "0" + Convert.ToString(data.Month) : Convert.ToString(data.Month);
                    string diaString = (data.Day < 10) ? "0" + Convert.ToString(data.Day) : Convert.ToString(data.Day);

                    StringBuilder dataString = new StringBuilder();
                    dataString.Append(diaString).Append(mesString).Append(anoString);
                    //  
                    string diretorioBase = Settings.Default.DiretorioBase;
                    StringBuilder nomeArquivo = new StringBuilder();
                    nomeArquivo.Append(diretorioBase)
                                   .Append("ASEL007").Append(dataString).Append(".txt");

                    if (File.Exists(nomeArquivo.ToString()))
                    {
                        File.Delete(nomeArquivo.ToString());
                    }
                    #endregion
                    //
                    throw new ArquivoASEL007IncorretoException("Arquivo ASEL007 " + this.data.ToString("dd/MM/yyyy") + " incorreto ");
                }
                #endregion
            }
            else if (this.IsTipoRegistroDados())
            {
                //Faz o split e carrega linha num array
                string[] arrayLinha = linha.Split(';');

                #region Dados
                this.codigoTitulo                           = arrayLinha[1];
                this.dataVencimento                         = new DateTime(Convert.ToInt32(arrayLinha[2].Substring(0,4)), Convert.ToInt32(arrayLinha[2].Substring(4,2)), Convert.ToInt32(arrayLinha[2].Substring(6,2)));
                this.codigoISIN                             = arrayLinha[3];
                this.siglaTitulo                            = arrayLinha[4].Trim();
                this.indPossibilidadeNegociacao             = arrayLinha[5];
                this.indBloqueio                            = arrayLinha[6];
                this.dataPrimeiraEmissao                    = new DateTime(Convert.ToInt32(arrayLinha[7].Substring(0, 4)), Convert.ToInt32(arrayLinha[7].Substring(4, 2)), Convert.ToInt32(arrayLinha[7].Substring(6, 2)));
                this.posicaoGeralCustodia                   = arrayLinha[8];
                this.tipoRendimento                         = arrayLinha[9];
                this.valorResgate                           = (Convert.ToDecimal(arrayLinha[10]) / 100000000);
                if(arrayLinha[11].Trim() != "")
                    this.dataBase                           = new DateTime(Convert.ToInt32(arrayLinha[11].Substring(0, 4)), Convert.ToInt32(arrayLinha[11].Substring(4, 2)), Convert.ToInt32(arrayLinha[11].Substring(6, 2)));
                this.valorNominalDataBase                   = (Convert.ToDecimal(arrayLinha[12]) / 100000000);
                this.indiceAtualizacaoValorNominal          = arrayLinha[13].Trim();
                this.indicadorPagamentoCupomJuros           = arrayLinha[14];
                this.periodicidadePgtoJuros                 = arrayLinha[15];
                this.taxaJuros                              = (Convert.ToDecimal(arrayLinha[16]) / 100);
                this.regimeCapitalizacaoJuros               = arrayLinha[17];
                this.indicadorPossibilidadeDesmembramento   = arrayLinha[18];
                this.precoUnitarioPagamentoJuros            = (Convert.ToDecimal(arrayLinha[19]) / 100000000);
                this.indicadorAmortizacaoPrincipal          = arrayLinha[20];
                this.periodicidadeAmortizacao               = arrayLinha[21];
                this.percentualAmortizacao                  = (Convert.ToDecimal(arrayLinha[22]) / 100000000);
                this.numeroParcelas                         = Convert.ToInt32(arrayLinha[23]);
                this.precoUnitarioPagamentoAmortizacao      = (Convert.ToDecimal(arrayLinha[24]) / 100000000);
                this.precoUnitarioLastro                    = (Convert.ToDecimal(arrayLinha[25]) / 100000000);
                this.valorNominalAtualizado                 = (Convert.ToDecimal(arrayLinha[26]) / 100000000);
                this.precoUnitarioRetornoResgate            = (Convert.ToDecimal(arrayLinha[27]) / 100000000);
                this.precoUnitarioPagamentoResgate          = (Convert.ToDecimal(arrayLinha[28]) / 100000000);
                #endregion
            }
            else if (this.IsTipoRegistroRodape())
            {
                #region Rodape
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo ASEL007 da Internet. Descompacta o arquivo Zip e apaga arquivo Zip        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Indic</param>
        /// <returns></returns>
        private bool DownloadASEL007(DateTime data, string path)
        {
            #region Download
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato4);

            string endereco = "http://www3.bcb.gov.br/selic/download/cadTit.do?method=transferir&nomeArq=" + dataString.ToString() + "ASEL007" + "&dataInicial=" + dataString.ToString();
            string nomeArquivoDestino = dataString.ToString() + "ASEL007";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            bool arquivoBaixado = File.Exists(pathArquivoDestino);
            if (download.Equals(true))
                System.IO.File.Move(pathArquivoDestino, pathArquivoDestino + ".txt");

            // Se arquivo existe retorna true            
            return arquivoBaixado;
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader()
        {
            return this.tipoRegistro == EstruturaArquivoASEL007.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Dados</returns>
        public bool IsTipoRegistroDados()
        {
            return this.tipoRegistro == EstruturaArquivoASEL007.Dados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Rodape</returns>
        public bool IsTipoRegistroRodape()
        {
            return this.tipoRegistro == EstruturaArquivoASEL007.Rodape;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Dados
        /// </summary>
        /// <param name="ASEL007"></param>
        /// 
        /// <returns> true se TipoRegistro = Dados
        ///           false se TipoRegistro != Dados
        /// </returns>
        public bool FilterASEL007ByDados(ASEL007 asel007)
        {
            return asel007.tipoRegistro == EstruturaArquivoASEL007.Dados;
        }
    }
}
