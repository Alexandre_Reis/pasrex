﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Globalization;
using System.Text;

namespace Financial.Interfaces.Import.RendaFixa {

    /// <summary>
    /// Armazena os Valores de CotacaoDebenture presentes num arquivo .txt proveniente do site www.debentures.com.br
    /// </summary>
    [Serializable]
    public class DebentureSiteDebentures {
        private DateTime data;
        private string codigoAtivo;
        private decimal pu;
        private bool processaDebenture; // Indica se deve Processar a Linha do Arquivo Debenture
        
        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string CodigoAtivo {
            get { return codigoAtivo; }
            set { codigoAtivo = value; }
        }

        public decimal PU {
            get { return pu; }
            set { pu = value; }
        }
        
        /// <summary>
        /// Faz o Download das Debentures do site http://www.debentures.com.br e carrega na Lista de Debentures
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">Path Completo do Diretorio onde o Arquivo com as Debentures será Procurado</param>
        /// <returns>Lista com as Debentures e seus Pu</returns>
        /// <exception cref="ArquivoDebentureSiteDebenturesNaoEncontradoException">Se download falhou e arquivo não existe</exception>
        /// <exception cref="ProcessaDebentureSiteDebenturesException">Se Processamento da Debenture falhou</exception>
        public List<DebentureSiteDebentures> ProcessaDebenture(DateTime data, string path) {

            // Se arquivo não existe dá exceção
            if (!this.DownloadCotacaoDebenture(data, path)) {
                throw new ArquivoDebentureSiteDebenturesNaoEncontradoException("Download falhou. Arquivo não encontrado");
            }

            List<DebentureSiteDebentures> listaRetorno = new List<DebentureSiteDebentures>();

            /* Leitura/Processamento do Arquivo */
            string nomeArquivoDestino = path + "Debenture" + data.ToString("ddMMyyyy") + ".txt";
            
            int i = 1;
            string linha = "";

            try {
                using (StreamReader sr = new StreamReader(nomeArquivoDestino, Encoding.GetEncoding("ISO-8859-1"))) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (i > 3) { // Começa a Tratar a partir da 4 linha
                            if (!String.IsNullOrEmpty(linha)) {                                
                                    this.TrataDebenture(linha);
                                    // Somente é salvo Debenture que possui dados relevantes
                                    DebentureSiteDebentures d = (DebentureSiteDebentures)Utilitario.Clone(this);
                                    if (this.processaDebenture) {
                                        listaRetorno.Add(d);
                                    }
                            }                            
                            else {
                                break; // Para de ler na primeira linha branca que aparecer
                            }
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaDebentureSiteDebenturesException(e.Message);
            }

            #region Deleta o Arquivo se Existir            
            if (File.Exists(nomeArquivoDestino)) {
                File.Delete(nomeArquivoDestino);
            }
            #endregion
            return listaRetorno;
        }

        /// <summary>
        /// Salva os atributos do arquivo Debenture
        /// </summary>
        /// <param name="linha"></param>
        private void TrataDebenture(string linha) {
            this.processaDebenture = true;
            string[] lista = linha.Split(new Char[] { '\t' });
            
            // String Decimal aparecem com formato de ponto
            //NumberFormatInfo provider = new NumberFormatInfo();
            //provider.NumberDecimalSeparator = ",";

            if (lista[5].Trim() != "Não Calculável" && lista[5].Trim() != "-") { // Campo Preço
                this.data = Convert.ToDateTime(lista[0]);
                this.codigoAtivo = lista[1].Trim().ToUpper();

                try {
                    this.pu = Convert.ToDecimal(lista[5]);
                }
                catch (Exception e1) {
                    this.processaDebenture = false;                    
                }               
            }
            else {
                this.processaDebenture = false;
            }
        }

        /// <summary>
        /// Baixa o Arquivo da Internet do Site http://www.debentures.com.br
        /// Endereço exemplo: http://www.debentures.com.br/exploreosnd/consultaadados/emissoesdedebentures/puhistorico_e.asp?ativo=TELE18&dt_ini=15/08/2011&dt_fim=15/08/2011
        /// </summary>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Debenture</param>
        /// <returns>Retorna true se conseguiu fazer o download</returns>
        /// 
        private bool DownloadCotacaoDebenture(DateTime data, string path) {
            #region Download

            string url = @"http://www.debentures.com.br/exploreosnd/consultaadados/emissoesdedebentures/puhistorico_e.asp?dt_ini={0}&dt_fim={1}";

            string endereco = String.Format( url, data.ToString("dd/MM/yyyy"), data.ToString("dd/MM/yyyy") );
            string nomeArquivoDestino = "Debenture" + data.ToString("ddMMyyyy") + ".txt";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            // Se arquivo Debenture+data.txt existe retorna true
            return File.Exists(pathArquivoDestino);
        }
   }
}