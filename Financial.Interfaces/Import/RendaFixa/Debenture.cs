﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Properties;
using Financial.Interfaces.Import.RendaFixa.Exceptions;
using System.Globalization;
using Financial.Interfaces.Import.RendaFixa.Enums;
using Dart.PowerTCP.Zip;


namespace Financial.Interfaces.Import.RendaFixa {
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Debenture {

        // Informação sobre o tipo registro da linha do arquivo Debenture
        private int tipoRegistro;

        #region Properties dos registros

        #region Header
        string header;

        public string Header {
            get { return header; }
            set { header = value; }
        }
        #endregion

        #region SubHeader
        string[] subHeader;

        public string[] SubHeader {
            get { return subHeader; }
            set { subHeader = value; }
        }       
        #endregion

        #region Dados

        string codigo; // Campo 1 - CodigoPapel

        public string Codigo {
            get { return codigo; }
            set { codigo = value; }
        }

        DateTime dataVencimento; // Campo 3

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        decimal taxaIndicativa; // Campo 7

        public decimal TaxaIndicativa {
            get { return taxaIndicativa; }
            set { taxaIndicativa = value; }
        }

        decimal pu; // Campo 11;

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }
        #endregion

        #endregion
       
        /// <summary>
        /// Processa o arquivo de Debenture
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde o arquivo Debenture será procurado</param>
        /// throws ArquivoDebentureNaoEncontradoException
        public DebentureCollection ProcessaDebenture(DateTime data, string path) {
            
            #region Verifica se Arquivo está no Diretorio
            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato3);
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(path)
                       .Append("MercadoDebenture").Append(dataString).Append(".txt");

            #endregion

            // Se arquivo não está no diretorio, tenta baixar da internet
            if (!File.Exists(nomeArquivo.ToString())) {
                // Download File
                this.DownloadDebenture(data, path);

                // Se arquivo não existe dá exceção                                                                    
                if (!File.Exists(nomeArquivo.ToString())) {
                    throw new ArquivoDebentureNaoEncontradoException("\nArquivo Debenture: " + nomeArquivo.ToString() + " não encontrado\n");
                }
            }

            DebentureCollection debentureCollection = new DebentureCollection();
            int i = 1;
            string linha = "";            
            try {
                using (TextReader sr = new StreamReader(nomeArquivo.ToString(), Encoding.Default)) {
                    while ((linha = sr.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {
                            this.SetTipoRegistro(i);
                            this.TrataDebenture(linha, data);
                            Debenture debenture = (Debenture)Utilitario.Clone(this);
                            debentureCollection.Add(debenture);
                        }
                        i++;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                //StringBuilder mensagem = new StringBuilder();
                //mensagem.Append("Processamento Debenture com problema: ")
                //        .AppendLine(e.Message)
                //        .AppendLine(e.StackTrace)
                //        .AppendLine("linha " + i + ":" + linha);
                throw new ProcessaDebentureException(e.Message);
            }

            return debentureCollection;
        }

        /// <summary>
        /// Salva os atributos do arquivo Debenture 
        /// </summary>
        /// <param name="linha"></param>
        /// <param name="data"></param>
        private void TrataDebenture(string linha, DateTime data) {
            
            #region EstruturaArquivoDebenture
            if (this.IsTipoRegistroHeader()) {                
                #region Header
                this.header = linha.TrimStart().TrimEnd();
                #endregion                               
            }
            //
            else if (this.IsTipoRegistroSubHeader()) {
                #region SubHeader
                this.subHeader = linha.Split(new char[] { '@' });
                #endregion
            }
            //
            else if (this.IsTipoRegistroDados()) {
                #region Dados
                //
                string[] dados = linha.Split(new char[] { '@' });
                /* Campo 1 - posicao 0 */
                this.codigo = dados[0];

                /* Campo 3 - posicao 2 */
                string dataVencimento = dados[2];
                // Formato = "DD/MM/AAAA"                
                int dia = Convert.ToInt32(dataVencimento.Substring(0, 2));
                int mes = Convert.ToInt32(dataVencimento.Substring(3, 2));
                int ano = Convert.ToInt32(dataVencimento.Substring(6, 4));
                //
                this.dataVencimento = new DateTime(ano, mes, dia);
                //
                // Campo 7 - posicao 6
                NumberFormatInfo formato = new NumberFormatInfo();
                formato.CurrencyDecimalSeparator = ".";

                string taxaIndicativaAux = dados[6];
                taxaIndicativaAux = taxaIndicativaAux.Replace("," , ".");

                try
                {
                    this.taxaIndicativa = !String.IsNullOrEmpty(taxaIndicativaAux) ? Convert.ToDecimal(taxaIndicativaAux, formato) : 0;
                }
                catch (Exception e)
                {
                    this.taxaIndicativa = 0;
                }                

                // Campo 11 - posicao 10
                string puAux = dados[10];
                puAux = puAux.Replace(",", ".");
                // N/D aparece em algumas linhas
                puAux = puAux.Replace("N/D", "");

                try
                {
                    this.pu = !String.IsNullOrEmpty(puAux) ? Convert.ToDecimal(puAux, formato) : 0;
                }
                catch (Exception e)
                {
                    this.pu = 0;
                }
                
                #endregion                
            }                                 
            #endregion
        }

        /// <summary>
        /// Baixa o arquivo Debenture da Internet.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Debenture</param>
        /// <returns></returns>
        private bool DownloadDebenture(DateTime data, string path)
        {
            return this.DownloadDebentureAnbima(data, path);

            /*if (data >= new DateTime(2012, 07, 10))
            {
                return this.DownloadDebentureAnbima(data, path);
            }
            else
            {
                return this.DownloadDebentureAntigo(data, path);
            }*/
        }

        /// <summary>
        /// Baixa o arquivo Debenture da Internet.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Debenture</param>
        /// <returns></returns>
        private bool DownloadDebentureAntigo(DateTime data, string path) {
            
            #region Download

            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);

            string endereco = "http://www.andima.com.br/merc_sec_debentures/arqs/db" + dataString.ToString() + ".exe";
            string nomeArquivoDestino = "Debenture.exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            StringBuilder nomeArquivoNovo = new StringBuilder();

            #region Executa Exe Shell
            if (download) {

                // Deleta o arquivo DB+data.txt se existir para que não apareça mensagem de sobreposição
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("DB" + dataString.ToString()+".txt");

                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion
                //
                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("Debenture: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion
                //
                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);

                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString())) {
                    StringBuilder dataStringAux = Utilitario.FormataData(data, PatternData.formato3);
                    nomeArquivoNovo.Append(path)
                               .Append("MercadoDebenture").Append(dataStringAux.ToString()).Append(".txt");

                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());                    
                }
                #endregion
            }
            #endregion
           
            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou deleta o zip
            if (arquivoDescompactado) {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString())) {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo Debenture+data.txt existe retorna true            
            return arquivoDescompactado;
        }

        private bool DownloadDebentureAnbimaInativo(DateTime data, string path)
        {
            //Utilizando agora o site da Debentures.com.br

            #region Download

            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);

            string endereco = "http://www.debentures.com.br/exploreosnd/consultaadados/precificacao/arqs/db" + dataString.ToString() + ".exe";

            string nomeArquivoDestino = "Debenture.exe";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion
            
            StringBuilder nomeArquivoNovo = new StringBuilder();

            #region Executa Exe Shell
            if (download)
            {

                // Deleta o arquivo DB+data.txt se existir para que não apareça mensagem de sobreposição
                #region Deleta
                StringBuilder nomeArquivoAntigo = new StringBuilder();
                nomeArquivoAntigo.Append(path).Append("DB" + dataString.ToString() + ".txt");

                if (File.Exists(nomeArquivoAntigo.ToString()))
                {
                    File.Delete(nomeArquivoAntigo.ToString());
                }
                #endregion
                //
                //#region Executa AutoExtrator
                //Process p = new Process();
                //string targetDir = string.Format(@path);
                //p.StartInfo.WorkingDirectory = targetDir;
                //p.StartInfo.FileName = nomeArquivoDestino;
                ////p.StartInfo.Arguments =  string.Format("C-Sharp Console application");
                //p.StartInfo.CreateNoWindow = false;
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                //try {
                //    p.Start();
                //}
                //catch (Exception e) {
                //    Console.WriteLine("Debenture: Problemas Execução Shell Exe:{0},{1}", e.Message, e.StackTrace.ToString());
                //}
                //p.WaitForExit();
                //#endregion
                //
                Archive arquivo = new Archive();
                //
                arquivo.PreservePath = true;
                arquivo.Clear();
                arquivo.Overwrite = Overwrite.Always;
                //
                arquivo.QuickUnzip(pathArquivoDestino, path);

                #region Rename
                if (File.Exists(nomeArquivoAntigo.ToString()))
                {
                    StringBuilder dataStringAux = Utilitario.FormataData(data, PatternData.formato3);
                    nomeArquivoNovo.Append(path)
                               .Append("MercadoDebenture").Append(dataStringAux.ToString()).Append(".txt");

                    Utilitario.RenameFile(nomeArquivoAntigo.ToString(), nomeArquivoNovo.ToString());
                }
                #endregion
            }
            #endregion

            bool arquivoDescompactado = File.Exists(nomeArquivoNovo.ToString());

            #region Deleta arquivo zip
            // se Descompactou deleta o zip
            if (arquivoDescompactado)
            {
                string arquivoExe = path + nomeArquivoDestino;
                if (File.Exists(arquivoExe.ToString()))
                {
                    File.Delete(arquivoExe.ToString());
                }
            }
            #endregion

            // Se arquivo Debenture+data.txt existe retorna true            
            return arquivoDescompactado;
        }

        /// <summary>
        /// Baixa o arquivo Debenture da Internet.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path">path completo do diretorio onde será baixado o arquivo Debenture</param>
        /// <returns></returns>
        private bool DownloadDebentureAnbima(DateTime data, string path)
        {
            //Esse download foi DESATIVADO pois, apesar de funcionar, o histórico é removido de tempos em tempos e ficam buracos se formos
            //importar dados antigos

            StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);
            string endereco = "http://www.anbima.com.br/merc_sec_debentures/arqs/db" + dataString.ToString() + ".txt";
            string nomeArquivoDestino = "db" + dataString.ToString() + ".txt";
            string pathArquivoDestino = path + nomeArquivoDestino;
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);

            bool arquivoBaixado = File.Exists(pathArquivoDestino);

            if (arquivoBaixado)
            {
                StringBuilder nomeArquivoNovo = new StringBuilder();
                StringBuilder dataStringAux = Utilitario.FormataData(data, PatternData.formato3);
                nomeArquivoNovo.Append(path)
                           .Append("MercadoDebenture").Append(dataStringAux.ToString()).Append(".txt");

                Utilitario.RenameFile(pathArquivoDestino.ToString(), nomeArquivoNovo.ToString());
            }

            // Se arquivo existe retorna true            
            return arquivoBaixado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="linha"></param>
        private void SetTipoRegistro(int linha) {

            #region Prenche o TipoRegistro do Arquivo Debenture
            switch (linha) {
                case 1:
                    this.tipoRegistro = EstruturaArquivoDebenture.Header;
                    break;
                case 3:
                    this.tipoRegistro = EstruturaArquivoDebenture.SubHeader;
                    break;
                default:
                    this.tipoRegistro = EstruturaArquivoDebenture.Dados;
                    break;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Header</returns>
        public bool IsTipoRegistroHeader() {
            return this.tipoRegistro == EstruturaArquivoDebenture.Header;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é SubHeader</returns>
        public bool IsTipoRegistroSubHeader() {
            return this.tipoRegistro == EstruturaArquivoDebenture.SubHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Tipo Registro é Dados</returns>
        public bool IsTipoRegistroDados() {
            return this.tipoRegistro == EstruturaArquivoDebenture.Dados;
        }

        /// <summary>
        ///  Função Delegate que Serve para filtrar a Collection pelo campo TipoRegistro = Dados
        /// </summary>
        /// <param name="debenture"></param>
        /// 
        /// <returns> true se TipoRegistro = Dados
        ///           false se TipoRegistro != Dados
        /// </returns>
        public bool FilterDebentureByDados(Debenture debenture) {
            return debenture.tipoRegistro == EstruturaArquivoDebenture.Dados;
        }
    }
}
