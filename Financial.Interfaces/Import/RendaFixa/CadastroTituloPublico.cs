using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;

namespace Financial.Interfaces.Import.RendaFixa
{
    [DelimitedRecord("@")]
    public class CadastroTituloPublico
    {
        public string Titulo;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")] 
        public DateTime DataReferencia;
        public string CodigoSELIC;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")] 
        public DateTime DataEmissao;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")] 
        public DateTime DataVencimento;
        public decimal TaxaMaxima;
        public decimal TaxaMinima;
        public decimal TaxaIndicativa;
        public decimal PU;
        public decimal DesvioPadrao;
        public decimal IntervIndInfD0;
        public decimal IntervIndSupD0;
        public decimal IntervIndInfD1;
        public decimal IntervIndSupD1;
        public string Criterio;
    }
}
