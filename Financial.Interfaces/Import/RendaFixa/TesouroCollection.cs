using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Financial.Interfaces.Import.RendaFixa {
    public class TesouroCollection {
        private List<Tesouro> collectionTesouro;

        public List<Tesouro> CollectionTesouro {
            get { return collectionTesouro; }
            set { collectionTesouro = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public TesouroCollection() {
            this.collectionTesouro = new List<Tesouro>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tesouro">Elemento a inserir na lista de Tesouro</param>
        public void Add(Tesouro tesouro) {
            collectionTesouro.Add(tesouro);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tesouroCollection">Elementos a inserir na lista de Tesouro</param>
        public void Add(List<Tesouro> tesouroCollection) {
            collectionTesouro.AddRange(tesouroCollection);
        }
    }
}
