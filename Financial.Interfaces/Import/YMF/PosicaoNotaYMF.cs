﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class PosicaoNotaYMF
    {
        public int IdNota;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtPosicao;
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        public string IcSnLiberada;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtAplicacao;
        public decimal VlAplicacao;
        public decimal VlCotaAplicacao;
        public decimal VlUfirAplicacao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtUltimoAniverCorrido;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtUltimoAniverUtil;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal VlCotaUltimoAniverUtil;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtProximoAniverUtil;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtProximoAniverCorrido;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal QtCotas;
        public decimal VlCorrigido;
        public decimal VlBruto;
        public decimal? VlCorrigidoPerformance;
        public decimal VlIr;
        public decimal VlIof;
        public decimal? VlPip;
        public decimal? VlIrPip;
        public decimal? VlPenaltyFee;
        public decimal? VlPerformance;
        public decimal? VlRendimentoCompensar;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlTaxaAdministracao;
        public decimal? VlDevolucaoTaxaAdmin;
        public decimal? VlDevolucaoPerformance;
        public decimal? VlReceitaSaqueCarencia;
        public decimal VlResgate;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtUltimoResgateIr;
        public decimal VlCotaUltimoResgateIr;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtProximoResgateIr;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInicioPerformance;
        public decimal? VlCotaPerformance;
        public decimal VlIofVirtual;
        public decimal VlRendimentoResgateIr;
        public decimal? VlCustoMedio;
        public decimal? VlIofAplicacao;
        public int? IdNotaOrigem;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtTransferencia;
        public decimal? VlPerformanceAtivo;
        public decimal? VlCota301294;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtUltimoIrLiminar;
        public decimal? VlPerformanceOriginal;
        public decimal? VlCotaUltimoIrLiminar;
        public decimal? VlCota311201;
        public decimal? VlCustoMedio311201;
        public decimal? VlVariacaoPerformance;
        public string IcSnPfeeNota;
        public string IcSnResgatePfee;
        public decimal? VlRend1;
        public decimal? PcAliq1;
        public decimal? VlRend2;
        public decimal? PcAliq2;
        public decimal? VlCota311204;
        public decimal? VlRendUltimoComeCotas;
        public decimal? VlRendPrimComeCotas;
        public decimal? VlRendSeguComeCotas;
        public decimal? VlRendTercComeCotas;
        public decimal? VlDesenqRendimento;
        public decimal? VlDesenqRendimentoCompl;
        public string CdDesenq;
        public decimal? VlDesenqRendimento1;
        public decimal? VlDesenqRendimentoCompl1;
        public string CdDesenq1;
        public decimal? PcDesenqAliqIr;
        public decimal? PcDesenqAliqIr1;
        public decimal? PcDesenqAliqIrCompl;
        public decimal? PcDesenqAliqIrCompl1;
        public decimal? VlIrContaOrdem;
        public decimal? VlIofContaOrdem;
        public string CdLiquidacao;
        public string CdClearing;
        public int? IdBanco;
        public string CdAgencia;
        public string CdConta;
        public decimal? PcAliquotaPerformance;
        public decimal? PcAliquotaIr;
        public decimal? VlCustoContabil;
        public decimal? VlFatorTxPre1;
        public decimal? VlFatorTxPre2;
        public decimal? VlFatorIndex1;
        public decimal? VlFatorIndex2;
        public int? QtDiasIof;
        public decimal? VlAliquotaIof;
        public string IcPvTipoIof;

    }
}