﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class FundoYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldTrim(TrimMode.Both)]
        public string CdCustodiante;
        public int IdLocal;
        [FieldTrim(TrimMode.Both)]
        public string CdGestor;
        [FieldTrim(TrimMode.Both)]
        public string CdAdministrador;
        [FieldTrim(TrimMode.Both)]
        public string CdCliente;
        public string IcSnAtivo;
        public string IcSnProcessamento;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtCorte020899;
        public string IcSnCotaConferida;
        public string DsObservação;
        [FieldTrim(TrimMode.Both)]
        public string CdEmpresa;
        public string IcSnBloqProc;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInicioAdmFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtFimAdmFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInicioGravacaoIofPfee;

        //Existem 6 colunas ao fim do arquivo que estao fora do layout oficial da YMF
        [FieldOptional()]
        public string Filler1;
        [FieldOptional()]
        public string Filler2;
        [FieldOptional()]
        public string Filler3;
        [FieldOptional()]
        public string Filler4;
        [FieldOptional()]
        public string Filler5;
        [FieldOptional()]
        public string Filler6;
    }
}