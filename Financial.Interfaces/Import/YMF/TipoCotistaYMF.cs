﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class TipoCotistaYMF
    {
        public int IdTipoCotista;
        public string DsTipoCotista;
        public string IcFjPessoa;
        public string IcSnIsentoIr;
        public string IcSnIrAutomatico;
        public decimal? PcAliquotaIr;
        public decimal? PcAliquotaIrFif;
        public string IcSnIofAplicacao;
        public string IcSnIofResgate;
        public string IcSnRespeitarCarencia;
        public string IcSnFuncionario;
        public string IcSnCompensacaoIr;
        public string IcSnFaq;
        public string IcClMotivoIsento;
        public string IcSnIsentoIr2005;
        public string IcSnIrContaOrdem;
        public string IcSnIofContaOrdem;   
    }
}