﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;

namespace Financial.Interfaces.Import.Fundo {

    /// <summary>
    /// Armazena os Valores de HistoricoCota Presentes num arquivo .txt 
    /// </summary>
    [Serializable]
    public class BAD_HistoricoCotaYMF {
        
        private DateTime data;
        private int idCarteira;
        private decimal? cotaAbertura;
        private decimal? cotaFechamento;
        private decimal? cotaBruta;
        private decimal? pLAbertura;
        private decimal? pLFechamento;
        private decimal? patrimonioBruto;
        private decimal? quantidadadeFechamento;

        #region Properties
        public DateTime Data {
            get { return data; }
            set { data = value; }
        }
        
        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        
        public decimal? CotaAbertura {
            get { return cotaAbertura; }
            set { cotaAbertura = value; }
        }

        public decimal? CotaFechamento {
            get { return cotaFechamento; }
            set { cotaFechamento = value; }
        }

        public decimal? CotaBruta {
            get { return cotaBruta; }
            set { cotaBruta = value; }
        }

        public decimal? PLAbertura {
            get { return pLAbertura; }
            set { pLAbertura = value; }
        }

        public decimal? PLFechamento {
            get { return pLFechamento; }
            set { pLFechamento = value; }
        }

        public decimal? PatrimonioBruto {
            get { return patrimonioBruto; }
            set { patrimonioBruto = value; }
        }

        public decimal? QuantidadadeFechamento {
            get { return quantidadadeFechamento; }
            set { quantidadadeFechamento = value; }
        }
               
        #endregion

        /// <summary>
        /// Lê uma Stream e Constroe uma lista de HistoricoCotaYMF
        /// </summary>
        /// <param name="sr">Stream YMF com os Dados do HistoricoCota</param>
        /// <returns></returns>
        /// <exception cref="ProcessaHistoricoCotaYMFException">Se Processamento Historico Cota falhou</exception>
       /* public List<BAD_HistoricoCotaYMF> ProcessaHistoricoCotaYMF(Stream sr) {

            List<HistoricoCotaYMF> listaRetorno = new List<HistoricoCotaYMF>();
            
            string linha = "";

            try {
                using (StreamReader sReader = new StreamReader(sr, Encoding.GetEncoding("ISO-8859-1"))) {
                    while ((linha = sReader.ReadLine()) != null) {
                        if (!String.IsNullOrEmpty(linha)) {                                
                            this.TrataHistoricoCotaYMF(linha);
                            //
                            HistoricoCotaYMF h = (HistoricoCotaYMF)Utilitario.Clone(this);
                            listaRetorno.Add(h);                        
                        }                            
                        else {
                            break; // Para de ler na primeira linha branca que aparecer
                        }
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaHistoricoCotaYMFException(e.Message);
            }
            
            return listaRetorno;
        }

        /// <summary>
        /// Salva os Atributos do Arquivo HistoricoCota
        /// </summary>
        /// <param name="linha"></param>
        private void TrataHistoricoCotaYMF(string linha) {            
            string[] lista = linha.Split(new Char[] { '\t' });

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            //            
            this.idCarteira = Convert.ToInt32(lista[0]); // Considera que Sempre vem valor
            this.data = Convert.ToDateTime(lista[1]); // Considera que Sempre vem valor
            //
            this.pLAbertura = !String.IsNullOrEmpty(lista[3]) ? Convert.ToDecimal(lista[3], provider) : 0.00M;
            this.cotaAbertura = !String.IsNullOrEmpty(lista[4]) ? Convert.ToDecimal(lista[4], provider) : 0.00M;
            this.cotaBruta = !String.IsNullOrEmpty(lista[5]) ? Convert.ToDecimal(lista[5], provider) : 0.00M;
            this.patrimonioBruto = !String.IsNullOrEmpty(lista[6]) ? Convert.ToDecimal(lista[6], provider) : 0.00M;
            this.pLFechamento = !String.IsNullOrEmpty(lista[49]) ? Convert.ToDecimal(lista[49], provider) : 0.00M;
            this.quantidadadeFechamento = !String.IsNullOrEmpty(lista[52]) ? Convert.ToDecimal(lista[52], provider) : 0.00M;
            this.cotaFechamento = !String.IsNullOrEmpty(lista[55]) ? Convert.ToDecimal(lista[55], provider) : 0.00M;            
        }
  */
    }
}