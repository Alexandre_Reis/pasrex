﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class FundoLimiteYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtInicioLimite;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtFimLimite;
        public decimal? VlAplicacaoMinimo;
        public decimal? VlAplicacaoMaximo;
        public decimal? VlResgateMinimo;
        public decimal? VlResgateMaximo;
        public decimal? VlSaldoMinimo;
        public decimal? VlAplicacaoInicial;

    }
}