﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;

namespace Financial.Interfaces.Import.Fundo {

    /// <summary>
    /// Armazena os Valores de PosicaoFundo Presentes num arquivo .txt 
    /// </summary>
    [Serializable]
    public class BAD_PosicaoFundoYMF {
        
        /// <summary>
        /// Lê uma Stream e Constroe um DataTable com as informações do arquivo
        /// </summary>
        /// <param name="sr">Stream YMF com os Dados de PosicaoFundo</param>
        /// <returns></returns>
        /// <exception cref="ProcessaPosicaoFundoYMFException">Se Processamento PosicaoFundo falhou</exception>
        public DataTable ProcessaPosicaoFundoYMF(Stream sr) {
            DataTable dataTable = new DataTable();

            try {
                #region Lê arquivo e Monta um DataTable
                using (StreamReader reader = new StreamReader(sr, Encoding.GetEncoding("ISO-8859-1"))) {

                    //now we need to read the rest of the text file
                    string data = reader.ReadToEnd();

                    if (!String.IsNullOrEmpty(data)) {

                        // Remove os Dois Ultimos Caracteres se for nova linha
                        string doisUltimos = data.Substring(data.Length - 2, 2); // Dois Ultimos Caracteres
                        if (doisUltimos == "\r\n") {
                            data = data.Remove(data.Length - 2, 2);
                        }

                        string[] rows = data.Split("\r".ToCharArray());

                        // Adiciona as Colunas
                        string[] numColunas = rows[0].Split("\t".ToCharArray());
                        for (int i = 0; i < numColunas.Length; i++) {
                            dataTable.Columns.Add("coluna" + i, typeof(System.String));
                        }

                        // loop de linha
                        foreach (string r in rows) {
                            string[] items = r.Split("\t".ToCharArray()); // delimitado por tab
                            dataTable.Rows.Add(items);
                        }
                    }
                }
                #endregion
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new ProcessaPosicaoFundoYMFException(e.Message);
            }

            return dataTable;
        }       
   }
}