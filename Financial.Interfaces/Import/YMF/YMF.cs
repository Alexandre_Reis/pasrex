﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;
using Financial.Util;


namespace Financial.Interfaces.Import.YMF
{
    public class YMF
    {
        public YMF()
        {

        }

        //Mapeado para Pessoa no Financial
        public ClienteYMF[] ImportaClienteYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(ClienteYMF));

            ClienteYMF[] clientes = engine.ReadStream(streamReader) as ClienteYMF[];

            return clientes;

        }

        //Mapeado para PessoaEndereco no Financial
        public EnderecoYMF[] ImportaEnderecoYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(EnderecoYMF));

            EnderecoYMF[] enderecos = engine.ReadStream(streamReader) as EnderecoYMF[];

            return enderecos;

        }

        //Mapeado para Cotista no Financial
        public CotistaYMF[] ImportaCotistaYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(CotistaYMF));

            CotistaYMF[] cotistas = engine.ReadStream(streamReader) as CotistaYMF[];

            return cotistas;

        }

        //Mapeado para OperacaoCotista no Financial
        public MovimentoYMF[] ImportaMovimentoYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(MovimentoYMF));

            MovimentoYMF[] movimentos = engine.ReadStream(streamReader) as MovimentoYMF[];

            return movimentos;

        }
        
        //Mapeado para PosicaoCotista no Financial
        public PosicaoNotaYMF[] ImportaPosicaoNotaYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(PosicaoNotaYMF));

            PosicaoNotaYMF[] posicoes = engine.ReadStream(streamReader) as PosicaoNotaYMF[];

            return posicoes;

        }

        //Mapeado para HistoricoCota no Financial
        public PosicaoFundoYMF[] ImportaPosicaoFundoYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(PosicaoFundoYMF));

            PosicaoFundoYMF[] posicoes = engine.ReadStream(streamReader) as PosicaoFundoYMF[];

            return posicoes;

        }

        //Mapeado para DetalheResgateCotista no Financial
        public ResgateYMF[] ImportaResgateYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(ResgateYMF));

            ResgateYMF[] registros = engine.ReadStream(streamReader) as ResgateYMF[];

            return registros;

        }

        //Mapeado para PrejuizoCotista no Financial
        public RendimentoCompensarYMF[] ImportaRendimentoCompensarYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(RendimentoCompensarYMF));

            RendimentoCompensarYMF[] registros = engine.ReadStream(streamReader) as RendimentoCompensarYMF[];

            return registros;

        }

        //Este arquivo complementa informações do Cotista no Financial
        public TipoCotistaYMF[] ImportaTipoCotistaYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(TipoCotistaYMF));

            TipoCotistaYMF[] registros = engine.ReadStream(streamReader) as TipoCotistaYMF[];

            return registros;

        }


        //Informações de Fundo mapeadas para cliente e carteira do Financial
        public FundoIRYMF[] ImportaFundoIRYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(FundoIRYMF));

            FundoIRYMF[] fundoIRs = engine.ReadStream(streamReader) as FundoIRYMF[];

            return fundoIRs;

        }
        public FundoLimiteYMF[] ImportaFundoLimiteYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(FundoLimiteYMF));

            FundoLimiteYMF[] fundoLimites = engine.ReadStream(streamReader) as FundoLimiteYMF[];

            return fundoLimites;

        }
        public FundoParametroYMF[] ImportaFundoParametroYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(FundoParametroYMF));

            FundoParametroYMF[] fundoParametros = engine.ReadStream(streamReader) as FundoParametroYMF[];

            return fundoParametros;

        }
        public FundoPerformanceYMF[] ImportaFundoPerformanceYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(FundoPerformanceYMF));

            FundoPerformanceYMF[] fundoPerformances = engine.ReadStream(streamReader) as FundoPerformanceYMF[];

            return fundoPerformances;

        }
        public FundoYMF[] ImportaFundoYMF(System.IO.StreamReader streamReader)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(FundoYMF));

            FundoYMF[] fundos = engine.ReadStream(streamReader) as FundoYMF[];

            return fundos;
        }
        public TipoFundoYMF[] ImportaTipoFundoYMF()
        {
            TipoFundoYMF[] tiposFundos = new TipoFundoYMF[9];
            tiposFundos[0] = new TipoFundoYMF();
            tiposFundos[0].CdTipoFundo = "001";
            tiposFundos[0].IcFvCarteira = Enums.TipoCarteiraYMF.RendaFixa;
            tiposFundos[0].IcSnRecebiveis = "S";

            tiposFundos[1] = new TipoFundoYMF();
            tiposFundos[1].CdTipoFundo = "RF";
            tiposFundos[1].IcFvCarteira = Enums.TipoCarteiraYMF.RendaFixa;
            tiposFundos[1].IcSnRecebiveis = "N";

            tiposFundos[2] = new TipoFundoYMF();
            tiposFundos[2].CdTipoFundo = "RV";
            tiposFundos[2].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[2].IcSnRecebiveis = "N";

            tiposFundos[3] = new TipoFundoYMF();
            tiposFundos[3].CdTipoFundo = "CLB";
            tiposFundos[3].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[3].IcSnRecebiveis = "N";

            tiposFundos[4] = new TipoFundoYMF();
            tiposFundos[4].CdTipoFundo = "MUL";
            tiposFundos[4].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[4].IcSnRecebiveis = "N";

            tiposFundos[5] = new TipoFundoYMF();
            tiposFundos[5].CdTipoFundo = "FIA";
            tiposFundos[5].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[5].IcSnRecebiveis = "N";

            tiposFundos[6] = new TipoFundoYMF();
            tiposFundos[6].CdTipoFundo = "FIF";
            tiposFundos[6].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[6].IcSnRecebiveis = "N";

            tiposFundos[7] = new TipoFundoYMF();
            tiposFundos[7].CdTipoFundo = "FII";
            tiposFundos[7].IcFvCarteira = Enums.TipoCarteiraYMF.RendaVariavel;
            tiposFundos[7].IcSnRecebiveis = "N";

            tiposFundos[8] = new TipoFundoYMF();
            tiposFundos[8].CdTipoFundo = "FID";
            tiposFundos[8].IcFvCarteira = Enums.TipoCarteiraYMF.RendaFixa;
            tiposFundos[8].IcSnRecebiveis = "S";

            return tiposFundos;
        }
    }
}
