﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class TipoFundoYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdTipoFundo;
        public string DsTipoFundo;
        public string IcFvCarteira;
        public string IcSnFaq;
        public string IcSnPoupanca;
        public string IcSnRecebiveis;
        public string IcSnFidcFechado;
        public string IcSnNegociacao;
        public string IcSnZeragem;
    }
}