﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class BloqueioYMF
    {
        public int IdBloqueio;
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        public int IdNota;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtBloqueio;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtDesbloqueio;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal QtCotas;
        public string DsMotivo;
        public string NmFavorecido;
        public string IcSnBloqueada;
        public string CdTipoBloqueio;
        public string CdMotivoBloqueio;
        public decimal VlBloqueio;
        public string CdBloqueio;
    }
}