﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class ClienteYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdCliente;
        public string IcFjPessoa;
        public int IdEstadoCivil;
        public int IdConstituicao;
        public int IdRegimeCasamento;
        public int IdEscolaridade;
        public string NmCliente;
        public string NmAbreviado;
        public string DsApelido;
        public decimal? NoCpf;
        public string NrRg;
        public string DsRgEmissor;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtRgEmissao;
        public string IcSnBrasileiro;
        public decimal? NoCvm;
        public string IcMfSexo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtNascimento;
        public string DsNaturalidade;
        public string DsNacionalidade;
        public string DsProfissao;
        public string DsOcupacao;
        public string NmFiliacaoPai;
        public string NmFiliacaoMae;
        public decimal? NoCgc;
        public int? NoInscricaoEstadual;
        public int? NoInscricaoMunicipal;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtConstituicao;
        public string DsDomicilio;
        public string DsAtividade;
        public string NmContato;
        public string CdUsuarioInclusao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInclusao;
        public string CdUsuarioAlteracao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtAlteracao;
        public string NrContatoTelefone;
        [FieldOptional]
        public string CdIsin;
        [FieldOptional]
        public decimal? VlRendaMensal;
        [FieldOptional]
        public decimal? VlSituacaoPatrimonial;
        [FieldOptional]
        public string IcSnExposta;
        [FieldOptional]
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtUltRenovacao;
        [FieldOptional]
        public decimal? CdNire;
        [FieldOptional]
        public string IdCnae;
        [FieldOptional]
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtJuntaComercial;
        [FieldOptional]
        public decimal? VlPatrimonioLiquido;
        [FieldOptional]
        public string CdOcupacaoProfissional;
        [FieldOptional]
        public decimal? NoCGCProfissional;
        [FieldOptional]
        public string CdAtividadePrincipal;

    }
}