﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;
using Financial.Util;


namespace Financial.Interfaces.Import.YMF
{
    public class FundoYMFInMemory
    {
        //Esta classe é utilizada para armazenar em memória todas as estruturas que são lidas dos arquivos relativos
        //a fundos da YMF
        
        public Dictionary<string, FundoIRYMF> FundoIRDictionary = new Dictionary<string, FundoIRYMF>();
        public Dictionary<string, FundoLimiteYMF> FundoLimiteDictionary = new Dictionary<string, FundoLimiteYMF>();
        public Dictionary<string, FundoParametroYMF> FundoParametroDictionary = new Dictionary<string, FundoParametroYMF>();
        public Dictionary<string, FundoPerformanceYMF> FundoPerformanceDictionary = new Dictionary<string, FundoPerformanceYMF>();
        public Dictionary<string, TipoFundoYMF> TipoFundoDictionary = new Dictionary<string,TipoFundoYMF>();
        
        public FundoYMFInMemory()
        {
        }

        public FundoYMFInMemory(Dictionary<string, FundoIRYMF> fundoIRDictionary,
            Dictionary<string, FundoLimiteYMF> fundoLimiteDictionary,
            Dictionary<string, FundoParametroYMF> fundoParametroDictionary,
            Dictionary<string, FundoPerformanceYMF> fundoPerformanceDictionary
            )
        {
            this.FundoIRDictionary = fundoIRDictionary;
            this.FundoLimiteDictionary = fundoLimiteDictionary;
            this.FundoParametroDictionary = fundoParametroDictionary;
            this.FundoPerformanceDictionary = fundoPerformanceDictionary;
        }
    }
}
