﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class FundoPerformanceYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtInicioPerformance;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtFimPerformance;
        [FieldTrim(TrimMode.Both)]
        public string CdIndexadorPerformance;
        public decimal PcAliquotaPerformance;
        [FieldTrim(TrimMode.Both)]
        public string CdTipoRecolhimento;
        public string IcSnRendimentoNegativo;
        public string IcBlCota;
        public decimal PcAliquotaTaxa;
        public string IcElTaxa;
        public string IcUcDias;
        public int? QtDiasBaseTaxa;
        public string IcSnMarcaDagua;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInicioMarcaDagua;
        public int? QtDiasAniversario;
        public string IcUcProRata;
        public string IcSnDuploIndexador;
        [FieldTrim(TrimMode.Both)]
        public string CdCriterio;
        [FieldTrim(TrimMode.Both)]
        public string CdIndexadorPerformance2;
        public string IcUcProRata2;
        public decimal? PcAliquotaPerformance2;
        public string IcElTaxa2;
        public string IcUcDias2;
        public string IcSnRendimentoNegativo2;
        public int? QtDiasBaseTaxa2;
        public string IcBlCota2;
        public decimal? PcAliquotaTaxa2;
        public string IcSnCalculoVariacao;
        public decimal PcAliquotaIndexador;
        public decimal? PcAliquotaIndexador2;
        public string IcScIndexador;
        public string IcSnCalculoNota;
        [FieldTrim(TrimMode.Both)]
        public string CdApropriacao;
        [FieldTrim(TrimMode.Both)]
        public string CdApropriacao2;
        public string IcSnIndexadorTeto;
        public decimal? PcAliquotaTeto;
        public string IcSnIndexadorTeto2;
        public decimal? PcAliquotaTeto2;
        public string IcFvAliquotaPerformance;

    }
}