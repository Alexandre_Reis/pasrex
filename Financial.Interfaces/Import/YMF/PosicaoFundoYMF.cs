﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class PosicaoFundoYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtPosicao;
        public string IcAfPosicao;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPatrimonioAbertura;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotaAbertura;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotaPrePfee;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPatrimonioPrePfee;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlReceitaPfee;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPfeeAcumulado;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasTotalAnterior;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasPfAnterior;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasPjAnterior;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlTaxaAdministracao;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicacoesTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicacoesPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicacoesPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesAplicTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesAplicPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlResgatesAplicPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicacoesTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicacoesPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicacoesPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasResgatesTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasResgatesPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasResgatesPj;
        public int? NoAplicacoesTotal;
        public int? NoAplicacoesPf;
        public int? NoAplicacoesPj;
        public int? NoResgatesTotal;
        public int? NoResgatesPf;
        public int? NoResgatesPj;
        public int? NoCotistasFechamentoTotal;
        public int? NoCotistasFechamentoPf;
        public int? NoCotistasFechamentoPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIofTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIrTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlReceitaSaqueCarencia;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlReceitaPenalty;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotasEmitirAbertura;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotasResgatarAbertura;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotasEmitirFechamento;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotasResgatarFechamento;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPfeeResgatado;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPatrimonioFechamentoTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPatrimonioFechamentoPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPatrimonioFechamentoPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasFechamentoTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasFechamentoPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasFechamentoPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal VlCotaFechamento;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIofPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIofPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIrPf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlIrPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlPfeeCalculadaAtivo;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicPfRetroativa;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicPjRetroativa;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicPfRetroativa;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicPjRetroativa;
        public int? NoAplicPfRetroativa;
        public int? NoAplicPjRetroativa;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicPfTransf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasAplicPjTransf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicPfTransf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAplicPjTransf;
        public int? NoAplicPfTransf;
        public int? NoAplicPjTransf;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotaPreAmortizacao;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAmortizacaoTotal;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAmortizacaoPj;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlAmortizacaoPf;

        //Existem 3 colunas ao fim do arquivo que estao fora do layout oficial da YMF
        [FieldOptional()]
        public string Filler1;
        [FieldOptional()]
        public string Filler2;
        [FieldOptional()]
        public string Filler3;
    }
}