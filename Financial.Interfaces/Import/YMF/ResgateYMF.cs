﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class ResgateYMF
    {
        public int IdNota;
        public int IdNotaAplicacao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtResgate;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtAplicacao;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal QtCotas;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotaAplicacao;
        public decimal VlAplicacao;
        public decimal VlCorrigido;
        public decimal? VlReceitaSaqueCarencia;
        public decimal VlIof;
        public decimal VlBrutoResgate;
        public decimal VlIr;
        public decimal? PcAliquotaIr;
        public decimal? VlPenaltyFee;
        public decimal? VlRendimentoCompensado;
        public decimal VlLiquidoResgate;
        public decimal? VlPerformance;
        public decimal? VlPip;
        public decimal? VlIrPip;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlTaxaAdministracao;
        public decimal? VlDevolucaoTaxaAdmin;
        public decimal? VlDevolucaoPerformance;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlRendimentoTributado;
        public decimal? VlRendimentoIsento;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtCotaUtilizada;
        public decimal? VlCotaUtilizada;
        public decimal? VlIofVirtual;
        public decimal? VlRendimentoResgateIr;
        public decimal? VlCustoMedio;
        public decimal? VlIofAplicacao;
        public decimal? VlPerformanceAtivo;
        public decimal? PcAliq1;
        public decimal? VlRend1;
        public decimal? PcAliq2;
        public decimal? VlRend2;
        public decimal? VlRend1Compensado;
        public decimal? VlRend2Compensado;
        public decimal? VlRendUltimoComeCotas;
        public decimal? VlRendPrimComeCotas;
        public decimal? VlRendSeguComeCotas;
        public decimal? VlRendTercComeCotas;
        public decimal? VlDesenqRendimento;
        public decimal? VlDesenqRendimentoCompl;
        public string CdDesenq;
        public decimal? VlDesenqRendimento1;
        public decimal? VlDesenqRendimentoCompl1;
        public string CdDesenq1;
        public decimal? VlIrContaOrdem;
        public decimal? VlIofContaOrdem;

        //Existem 5 colunas ao fim do arquivo que estao fora do layout oficial da YMF
        [FieldOptional()]
        public string Filler1;
        [FieldOptional()]
        public string Filler2;
        [FieldOptional()]
        public string Filler3;
        [FieldOptional()]
        public string Filler4;
        [FieldOptional()]
        public string Filler5;
    }
}