﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class CotistaYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        [FieldTrim(TrimMode.Both)]
        public string CdOperador;
        public string CdDistribuidor;
        [FieldTrim(TrimMode.Both)]
        public string CdCliente;
        public int IdTipoCotista;
        public string CdConta;
        public string DsObservacao;
        public string CdCriterioResgate;
        public decimal? PcDevolucaoTxAdm;
        public decimal? PcDevolucaoPerformance;
        public string DsTitularidade;
        public string IcSnExtrato;
        public string IcSnDocFundo;
        public int? IdBanco;
        public string CdAgencia;
        public int? IdLocal;
        public string CdRazao;
        public string IcSnAtivo;
        public string CdTipoEndereco;
        public string CdGrupoCotista;
        public string CdGrupoFundo;
        public int? IdClassificacao;
        public string CdAtivo;
        public string IcSnColagem;
        public string IcSnNota;
        public string IcSnInforme;
        public int? IdBancoInvest;
        public string CdAgenciaInvest;
        public string CdContaInvest;
        public string CdCotistaColagem;
        public string CdInterface1;
        public string CdInterface2;
        public string CdInterface3;
        public string CdInterface4;
        public string IcSnCorrentista;
        public string IcSnAtivoLicenca;
        public string IcSnReaplicaRepasse;
        public string CdLiquidacaoRepasse;
        public string CdContaExternaRepasse;
        public string CdClearingRepasse;
        public string CdEmpresa;
        public string IcSnInvQualificado;
        public string CdInterface5;
        public string CdLiquidacaoDefault;
        public string IcSnExtratoContaOrdem;
        public string IcSnInformeContaOrdem;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInclusao;
        public string CdPerfilInvest;

    }
}