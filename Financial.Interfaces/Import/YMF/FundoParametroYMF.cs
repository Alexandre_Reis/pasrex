﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class FundoParametroYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtInicioValidade;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtFimValidade;
        public string IcAfCalculoCota;
        public string IcSnPip;
        [FieldTrim(TrimMode.Both)]
        public string CdTipoFundo;
        public string IcSnPerformance;
        public string IcSnPenalty;
        public string IcSnBalanceamento;
        public string IcSnCompensacaoIr;
        public string IcSnIrAutomatico;
        public string IcSnCalculoCota;
        public string IcSnDuplaLiqFisica;
        public int QtDiasCarencia;
        public int QtDiasAniversario;
        public int QtDiasLiqFisEntrada;
        public string IcUcCalcDiasFisEnt;
        public int QtDiasLiqFisSaida;
        public string IcUcCalcDiasFisSai;
        public int QtDiasLiqFinanEntrada;
        public string IcUcCalcDiasFinanEnt;
        public int QtDiasLiqFinanSaida;
        public string IcUcCalcDiasFinanSai;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime HrInicioMovimento;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime HrFimMovimento;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? HrCotaD0;
        public decimal? PcEnquadramentoMaximo;
        [FieldTrim(TrimMode.Both)]
        public string CdCriterioResgate;
        public int? IdFmpFgtsMatricula;
        [FieldTrim(TrimMode.Both)]
        public string CdAtivo;
        public string IcAtQuantidadeCotas;
        public string IcAtValorCota;
        public string IcAtFinanceiro;
        public int QtDecimalValorCota;
        public int QtDecimalQtdeCotas;
        public int QtDecimalFinanceiro;
        public string IcUpCalculoRentab;
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        public string IcAcCalculoAniver;
        [FieldTrim(TrimMode.Both)]
        public string CdContaFundo;
        public int? IdBancoFundo;
        [FieldTrim(TrimMode.Both)]
        public string CdAgenciaFundo;
        public string IcSnFmpFgts;
        [FieldTrim(TrimMode.Both)]
        public string CdIndexadorCota;
        public decimal? PcAliquotaCpmf;
        public string IcSnLimiteMovimento;
        public string IcBlCalculoRendCompensacao;
        [FieldTrim(TrimMode.Both)]
        public string CdComplContaFundo;
        public string IdBancoCciFundo;
        [FieldTrim(TrimMode.Both)]
        public string CdAgenciaCciFundo;
        [FieldTrim(TrimMode.Both)]
        public string CdContaCciFundo;
        [FieldTrim(TrimMode.Both)]
        public string CdComplContaCciFundo;
        public string IsSnColagem;
        [FieldTrim(TrimMode.Both)]
        public string CdFundoColagem;
        public string IcSnSomaRendNeg;
        public string IcSnAplicacao;
        public string IcSnResgate;
        public string IcSnBacenjud;
        public string IcSnDividendos;
        public string IcMfParamCalcDtLiqFin;
        public string IcSnDistribuido;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtInicioDistribuicao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtFimDistribuicao;
        public string IcSnInvQualificado;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtUltimoRegulamento;
        public string IcSnEncerraTermo;
        public string IcSnExclusivo;
        public string IcSnFeriadoBolsa;
        public string IcSnFeriadoBancario;
        public string IcSnFundoTerceiro;
        public string IcSnLimiteFinanAntecipada;
        [FieldTrim(TrimMode.Both)]
        public string CdPerfil;
        [FieldTrim(TrimMode.Both)]
        public string CdLiquidacaoFisica;
        [FieldTrim(TrimMode.Both)]
        public string CdPerfilInvest;
        public string IcSnTributoSpb;

    }
}