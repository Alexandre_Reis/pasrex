﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class EnderecoYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdCliente;
        public string CdTipoEndereco;
        public string DsLogradouro;
        public string NmBairro;
        public string NmCidade;
        [FieldTrim(TrimMode.Both)]
        public string SgUf;
        public string NrCep;
        public string NrCaixaPostal;
        public string NrTelefone;
        public string NrRamal;
        public string NrFax;
        public string DsEmail;
        public string NrTelex;
        public string IcSnEstrangeiro;
        public string DsPais;
        public string DsEstadoEstrangeiro;
        public string DsZipCode;
        public string IcSnComprovado;
    
    }
}