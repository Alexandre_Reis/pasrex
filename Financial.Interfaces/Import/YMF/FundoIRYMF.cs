﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class FundoIRYMF
    {
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtInicioIr;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtFimIr;
        public decimal PcAliquotaIr;
        public string IcSnCustoMedio;
        public string IcFcAliquota;
        public string IcLcCarteiraFundo;
        public string IcAcCalculoIr;
        public string CdCompensacaoIr;
        public string CdRetencaoDirf;

        //Existem 2 colunas ao fim do arquivo que estao fora do layout oficial da YMF
        [FieldOptional()]
        public string Filler1;
        [FieldOptional()]
        public string Filler2;
        
    }
}