﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class MovimentoYMF
    {
        public int IdNota;
        [FieldTrim(TrimMode.Both)]
        public string CdPadrao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtMovimento;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtRetroativoEstorno;
        [FieldTrim(TrimMode.Both)]
        public string CdTipo;
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtDigitacao;
        public decimal? VlDigitado;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotasDigitada;
        public int? IdNotaDigitada;
        [FieldTrim(TrimMode.Both)]
        public string CdCriterioResgate;
        [FieldTrim(TrimMode.Both)]
        public string CdLiquidacao;
        public int? IdBanco;
        [FieldTrim(TrimMode.Both)]
        public string CdAgenciaExterna;
        [FieldTrim(TrimMode.Both)]
        public string CdContaExterna;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtLiquidacaoFisica;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtLiquidacaoFinanceira;
        public decimal? VlBruto;
        public decimal? VlIr;
        public decimal? VlIof;
        public decimal? VlLiquido;
        public decimal? VlPerformance;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? QtCotas;
        [FieldTrim(TrimMode.Both)]
        public string CdUsuarioDigitacao;
        [FieldTrim(TrimMode.Both)]
        public string CdUsuarioConferente;
        [FieldTrim(TrimMode.Both)]
        public string CdUsuarioAutorizante;
        [FieldTrim(TrimMode.Both)]
        public string IcSnNotaEmitida;
        public int? IdNotaOrigem;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtTransferencia;
        [FieldTrim(TrimMode.Both)]
        public string DsHistorico;
        [FieldTrim(TrimMode.Both)]
        public string CdClearing;
        [FieldTrim(TrimMode.Both)]
        public string IcArLiquidacao;
        [FieldTrim(TrimMode.Both)]
        public string IcSnCpmf;
        public decimal? VlCpmf;
        [FieldTrim(TrimMode.Both)]
        public string IcSnConferido;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtConferencia;
        public decimal? VlPenalty;
        [FieldConverter(typeof(ConvertFloat))]
        public decimal? VlCotaMovimentacao;
        [FieldTrim(TrimMode.Both)]
        public string CdCustodia;
        [FieldTrim(TrimMode.Both)]
        public string CdInterface1;
        public int? IdBancoTerceiros;
        [FieldTrim(TrimMode.Both)]
        public string CdAgenciaTerceiros;
        [FieldTrim(TrimMode.Both)]
        public string CdContaTerceiros;
        [FieldTrim(TrimMode.Both)]
        public string CdContaOrdem;
        [FieldTrim(TrimMode.Both)]
        public string IcSnEnviado;
        [FieldTrim(TrimMode.Both)]
        public string CdDiasLiqResgate;
        public int? IdSacDividendo;
        [FieldTrim(TrimMode.Both)]
        public string CdIf;
        [FieldTrim(TrimMode.Both)]
        public string CdTipoTransferencia;
        [FieldTrim(TrimMode.Both)]
        public string IcSnMovRestaurado;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtLiqFinanAntecipada;
        [FieldTrim(TrimMode.Both)]
        public string IcSnNegociacao;
        [FieldTrim(TrimMode.Both)]
        public string IcSnZeragem;
        [FieldTrim(TrimMode.Both)]
        public string IcSnEnviadoEmail;
    }
}