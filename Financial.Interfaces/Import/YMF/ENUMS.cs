﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.YMF.Enums {
    
    
    public static class IcBlCalculoRendCompensacao{
        public const string Bruto = "B";
        public const string Liquido = "L";
    }
    

    public static class IcUcCalcDiasFinanSai{
        public const string Uteis = "U";
        public const string Corridos = "C";
    }

    public static class IcLcCarteiraFundo{
        public const string CurtoPrazo = "C";
        public const string LongoPrazo = "L";
    }

    public static class IcUpCalculoRentab{
        public const string UltimoDia = "U";
        public const string PrimeiroDia = "P";
    }


    public static class TruncaCotaYMF
    {
        public const string Arrendonda = "A";
        public const string Trunca = "T";
    }

    public static class TruncaQuantidadeYMF
    {
        public const string Arrendonda = "A";
        public const string Trunca = "T";
    }

    public static class TruncaFinanceiroYMF
    {
        public const string Arrendonda = "A";
        public const string Trunca = "T";
    }

    public static class TipoCarteiraYMF
    {
        public const string RendaFixa = "F";
        public const string RendaVariavel = "V";
    }

    public static class TipoCalculoCotaYMF
    {
        public const string Abertura = "A";
        public const string Fechamento = "F";
    }

    public static class TipoMovimentoYMF
    {
        /* COMPRA */
        public const string Aplicacao = "A";

        /* COMPRA AGIO/DESAGIO*/
        public const string AG = "AG";
        
        /* Venda TOTAL */
        public const string ResgateTotal = "RT";
         
        /* Venda pelo valor LIQUIDO */
        public const string ResgateLiquido = "RL";

        /* RA: Resgate Liberado */
        public const string ResgateLiberado = "RA";

        /* RB: Venda pelo valor BRUTO */
        public const string ResgateBruto = "RB";

        /* RC: Venda pela quantidade de COTAS */
        public const string ResgateCotas = "RC";

        /* RI: Resgate para pagamento de IR */
        public const string ResgateIR = "RI";

        /* RM: Amortização */
        public const string RM = "RM";

        /* NT: Venda por NOTA - Total */
        public const string ResgateNotaTotal = "NT";

        /* NL: Venda por NOTA - Valor Liquido */
        public const string ResgateNotaLiquido = "NL";

        /* NB: Venda por NOTA - Valor Bruto */
        public const string ResgateNotaBruto = "NB";

        /* NC: Venda por NOTA - Quantidade de Cotas */
        public const string ResgateNotaCotas = "NC";

        /* TR: DESCONHECIDO */
        public const string TR = "TR";

        /* VR: DESCONHECIDO */
        public const string VR = "VR";

        /* RJ: RESGATE JUROS */
        public const string ResgateJuros = "RJ";

        /* RD: RESGATE DIVIDENDOS */
        public const string ResgateDividendos = "RD";

    }

    public static class CriterioResgateMovimentoYMF
    {
        /* FIFO */
        public const string FIFO = "F";

        /* CONSERVAÇÃO ESTOQUE COTAS */
        public const string ConservacaoCotas = "C";

        /* Melhor Aniversário */
        public const string MelhorAniversario = "A";

        /* DESCONHECIDO NO DICIONARIO YMF - TALVEZ IMPOSTO*/
        public const string I = "I";
    }

    public static class IcFjPessoa
    {
        public const string FISICA = "F";
        public const string JURIDICA = "J";
    }

    public static class IdEstadoCivil
    {
        public const byte NULL = 0;
        public const byte CASADO = 1;
        public const byte SOLTEIRO = 2;
        public const byte VIUVO = 3;
        public const byte DIVORCIADO = 4;
        public const byte OUTROS = 5;
    }
}
