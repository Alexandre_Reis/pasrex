﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.Fundo {

    /// <summary>
    /// Armazena os Valores de PosicaoFundo Presentes num arquivo .txt 
    /// </summary>
    [DelimitedRecord("\t")]
    [IgnoreFirst(1)]
    public class BAD_MovimentoAnaliticoYMF
    {
        public int IdNota;
        public string CdCotista;
        public string CdPadrao;
        public string CdFundo;
        public string CdTipo;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtMovimento;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtLiquidacaoFisica;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtLiquidacaoFinanceira;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DsDtMovimento;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DsDtLiquidacaoFisica;

        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DsDtLiquidacaoFinanceira;

        public decimal VlBrutoTotal;
        public decimal VlIRTotal;
        public decimal QtCotasTotal;
        public decimal VlIOFTotal;
        public decimal VlLiquidoTotal;

        public int? IdNotaAplicacao;
        public string DsTipo;

        public decimal? QtCotas;
        public decimal? VlAplicacao;
        public decimal? VlReceitaSaqueCarencia;
        public decimal? VlIOF;
        public decimal? VlBruto;
        public decimal? VlIR;
        public decimal? VlPenaltyFee;
        public decimal? VlLiquido;

        public string CdLiquidacao;

        //O RESTANTE DOS CAMPOS NÃO ESTÁ DECLARADO, POIS NÃO É USADO...
    }
}