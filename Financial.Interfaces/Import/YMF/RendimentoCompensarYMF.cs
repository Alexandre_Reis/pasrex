﻿using System;
using System.Collections.Generic;
using Financial.Util;
using System.IO;
using Financial.Interfaces.Import.Fundo.Exceptions;
using System.Globalization;
using System.Text;
using System.Data;
using FileHelpers;

namespace Financial.Interfaces.Import.YMF
{

    [DelimitedRecord("\t")]
    public class RendimentoCompensarYMF
    {
        public int IdNota;
        public int IdNotaAplicacao;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtPosicao;
        [FieldTrim(TrimMode.Both)]
        public string CdCotista;
        [FieldTrim(TrimMode.Both)]
        public string CdFundo;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime DtResgate;
        public decimal VlCompensar;
        [FieldConverter(typeof(ConvertFlexDate))]
        public DateTime? DtLimite;
        public decimal PcAliq1;    
    }
}