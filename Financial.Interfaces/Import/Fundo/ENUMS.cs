﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.Fundo.Enums {

    public static class TipoMovimentoSMA
    {
        /* Aplicação */
        public const string Aplicacao = "Aplicação";

        /* Resgate pelo valor LIQUIDO */
        public const string ResgateLiquido = "Resgate Parcial";

        /* Resgate pelo valor TOTAL */
        public const string ResgateTotal = "Resgate Total";

        /* Resgate para Pag. de IR */
        public const string ResgatePagtoIR = "Resgate para Pag. de IR";
    }
}
