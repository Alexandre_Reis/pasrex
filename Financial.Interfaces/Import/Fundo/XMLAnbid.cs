﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using Financial.Common.Enums;

namespace Financial.Interfaces.Import.Fundo
{
    public class XMLAnbid
    {
        public short? RetornaIdIndice(string indice)
        {
            //INDICES NAO MAPEADOS
            //IEE	IEE
            //IGP	Índice de preços
            //IMA	Índice de Mercado Andima Geral
            //IMA-B5	Índice de Mercado Andima NTN-B até 5 anos
            //IMA-B5+	Índice de Mercado Andima NTN-B mais de 5 anos
            //IMA-C5	Índice de Mercado Andima NTN-C até 5 anos
            //IMA-C5+	Índice de Mercado Andima NTN-C mais de 5 anos
            //IPC	Índice de Preços ao Consumidor (IPC/FIPE)
            //ITE	ITEL
            //PIB	Cota de PIBB
            //SB1	Carteira de ações
            //SB2	Carteira de ações
            //TBF	Taxa Básica Financeira
            //

            switch (indice)
            {
                case "ANB":
                    return ListaIndiceFixo.ANBID;
                case "DI1":
                    return ListaIndiceFixo.CDI;
                case "DOL":
                    return ListaIndiceFixo.PTAX_800VENDA;
                case "I50":
                    return ListaIndiceFixo.IBRX50_FECHA;
                case "IAP":
                    return ListaIndiceFixo.IPCA;
                case "IBX":
                    return ListaIndiceFixo.IBRX_FECHA;
                case "IGD":
                    return ListaIndiceFixo.IGPDI;
                case "IGM":
                    return ListaIndiceFixo.IGPM;
                case "IMA-B":
                    return ListaIndiceFixo.IMA_B;
                case "IMA-C":
                    return ListaIndiceFixo.IMA_C;
                case "IMA-S":
                    return ListaIndiceFixo.IMA_S;
                case "IND":
                    return ListaIndiceFixo.IBOVESPA_FECHA;
                case "INP":
                    return ListaIndiceFixo.INPC;
                case "IRF":
                    return ListaIndiceFixo.IRFM;
                case "JPY":
                    return ListaIndiceFixo.YENE;
                case "OZ1":
                    return ListaIndiceFixo.OUROBMF_FECHA;
                case "REU":
                    return ListaIndiceFixo.EURO;
                case "SEL":
                    return ListaIndiceFixo.SELIC;
                case "TJL":
                    return ListaIndiceFixo.TJLP;
                case "TR":
                    return ListaIndiceFixo.TR;
                default:
                    return null;
            }

        }

        /* Sessões do Arquivo XML */
        #region Secoes

        #region Carteira
        const string NODE_HEADER_CARTEIRA = "/arquivoposicao_4_01/carteira/header";
        const string NODE_TITULO_PUBLICO_CARTEIRA = "/arquivoposicao_4_01/carteira/titpublico";
        const string NODE_COMPROMISSO_TITULO_PUBLICO_CARTEIRA = ".//compromisso";
        const string NODE_TITULO_PRIVADO_CARTEIRA = "/arquivoposicao_4_01/carteira/titprivado";
        const string NODE_COMPROMISSO_TITULO_PRIVADO_CARTEIRA = ".//compromisso";
        const string NODE_DEBENTURE_CARTEIRA = "/arquivoposicao_4_01/carteira/debenture";
        const string NODE_ACOES_CARTEIRA = "/arquivoposicao_4_01/carteira/acoes";
        const string NODE_OPCOESACOES_CARTEIRA = "/arquivoposicao_4_01/carteira/opcoesacoes";
        const string NODE_OPCOESDERIV_CARTEIRA = "/arquivoposicao_4_01/carteira/opcoesderiv";
        const string NODE_OPCOESFLX_CARTEIRA = "/arquivoposicao_4_01/carteira/opcoesflx";
        const string NODE_TERMO_RV_CARTEIRA = "/arquivoposicao_4_01/carteira/termorv";
        const string NODE_FUTUROS_CARTEIRA = "/arquivoposicao_4_01/carteira/futuros";
        const string NODE_SWAP_CARTEIRA = "/arquivoposicao_4_01/carteira/swap";
        const string NODE_CAIXA_CARTEIRA = "/arquivoposicao_4_01/carteira/caixa";
        const string NODE_COTAS_CARTEIRA = "/arquivoposicao_4_01/carteira/cotas";
        const string NODE_PROVISAO_CARTEIRA = "/arquivoposicao_4_01/carteira/provisao";
        #endregion

        #region Fundo
        const string NODE_HEADER_FUNDO = "/arquivoposicao_4_01/fundo/header";
        const string NODE_TITULO_PUBLICO_FUNDO = "/arquivoposicao_4_01/fundo/titpublico";
        const string NODE_COMPROMISSO_TITULO_PUBLICO_FUNDO = ".//compromisso";
        const string NODE_TITULO_PRIVADO_FUNDO = "/arquivoposicao_4_01/fundo/titprivado";
        const string NODE_COMPROMISSO_TITULO_PRIVADO_FUNDO = ".//compromisso";
        const string NODE_DEBENTURE_FUNDO = "/arquivoposicao_4_01/fundo/debenture";
        const string NODE_ACOES_FUNDO = "/arquivoposicao_4_01/fundo/acoes";
        const string NODE_OPCOESACOES_FUNDO = "/arquivoposicao_4_01/fundo/opcoesacoes";
        const string NODE_OPCOESDERIV_FUNDO = "/arquivoposicao_4_01/fundo/opcoesderiv";
        const string NODE_OPCOESFLX_FUNDO = "/arquivoposicao_4_01/fundo/opcoesflx";
        const string NODE_TERMO_RV_FUNDO = "/arquivoposicao_4_01/fundo/termorv";
        const string NODE_FUTUROS_FUNDO = "/arquivoposicao_4_01/fundo/futuros";
        const string NODE_SWAP_FUNDO = "/arquivoposicao_4_01/fundo/swap";
        const string NODE_CAIXA_FUNDO = "/arquivoposicao_4_01/fundo/caixa";
        const string NODE_COTAS_FUNDO = "/arquivoposicao_4_01/fundo/cotas";
        const string NODE_PROVISAO_FUNDO = "/arquivoposicao_4_01/fundo/provisao";
        #endregion

        #endregion

        /* Nomes Dos DataTables */
        #region DataTables Names
        const string HEADER = "header";
        const string TITULO_PUBLICO = "titpublico";
        const string TITULO_PRIVADO = "titprivado";
        const string DEBENTURE = "debenture";
        const string ACOES = "acoes";
        const string OPCOESACOES = "opcoesacoes";
        const string OPCOESDERIV = "opcoesderiv";
        const string OPCOESFLX = "opcoesflx";
        const string TERMO_RV = "termorv";
        const string FUTUROS = "futuros";
        const string SWAP = "swap";
        const string CAIXA = "caixa";
        const string COTAS = "cotas";
        const string PROVISAO = "provisao";
        #endregion

        // DataSet Com os DataTables do ArquivoXML
        private DataSet dataSet = new DataSet();

        #region Secoes do ArquivoXML
        public class Secoes
        {
            private XMLAnbid x;

            /* Construtor */
            public Secoes(XMLAnbid x)
            {
                this.x = x;
            }

            public DataTable GetHeader
            {
                get { return this.x.dataSet.Tables[HEADER] != null ? this.x.dataSet.Tables[HEADER] : new DataTable(); }
            }

            public DataTable GetTituloPublico
            {
                get { return this.x.dataSet.Tables[TITULO_PUBLICO] != null ? this.x.dataSet.Tables[TITULO_PUBLICO] : new DataTable(); }
            }

            public DataTable GetTituloPrivado
            {
                get { return this.x.dataSet.Tables[TITULO_PRIVADO] != null ? this.x.dataSet.Tables[TITULO_PRIVADO] : new DataTable(); }
            }

            public DataTable GetDebentures
            {
                get { return this.x.dataSet.Tables[DEBENTURE] != null ? this.x.dataSet.Tables[DEBENTURE] : new DataTable(); }
            }

            public DataTable GetAcoes
            {
                get { return this.x.dataSet.Tables[ACOES] != null ? this.x.dataSet.Tables[ACOES] : new DataTable(); }
            }

            public DataTable GetOpcoesAcoes
            {
                get { return this.x.dataSet.Tables[OPCOESACOES] != null ? this.x.dataSet.Tables[OPCOESACOES] : new DataTable(); }
            }

            public DataTable GetOpcoesDeriv
            {
                get { return this.x.dataSet.Tables[OPCOESDERIV] != null ? this.x.dataSet.Tables[OPCOESDERIV] : new DataTable(); }
            }

            public DataTable GetOpcoesFlx
            {
                get { return this.x.dataSet.Tables[OPCOESFLX] != null ? this.x.dataSet.Tables[OPCOESFLX] : new DataTable(); }
            }

            public DataTable GetTermoRV
            {
                get { return this.x.dataSet.Tables[TERMO_RV] != null ? this.x.dataSet.Tables[TERMO_RV] : new DataTable(); }
            }

            public DataTable GetFuturos
            {
                get { return this.x.dataSet.Tables[FUTUROS] != null ? this.x.dataSet.Tables[FUTUROS] : new DataTable(); }
            }

            public DataTable GetSwap
            {
                get { return this.x.dataSet.Tables[SWAP] != null ? this.x.dataSet.Tables[SWAP] : new DataTable(); }
            }

            public DataTable GetCaixa
            {
                get { return this.x.dataSet.Tables[CAIXA] != null ? this.x.dataSet.Tables[CAIXA] : new DataTable(); }
            }

            public DataTable GetCotas
            {
                get { return this.x.dataSet.Tables[COTAS] != null ? this.x.dataSet.Tables[COTAS] : new DataTable(); }
            }

            public DataTable GetProvisao
            {
                get { return this.x.dataSet.Tables[PROVISAO] != null ? this.x.dataSet.Tables[PROVISAO] : new DataTable(); }
            }
        }
        #endregion

        public Secoes Secao
        {
            get { return new Secoes(this); }
        }

        /// <summary>
        /// Lê um arquivo XML com as seguintes seções:
        /// Header, TituloPublico, TituloPrivado, Debenture, Acões, OpçõesAcoes, OpçõesFlx, Futuros, Swap, 
        /// Caixa, Cotas, Despesas, OutrasDespesas, Provisão, TermoRV
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <exception cref="">
        /// throws Exception Se Arquivo não existir,
        /// Se o arquivo nao for um arquivo XML
        /// Se não existir o nó /arquivoposicao_4_01/carteira/header no arquivo XML
        /// </exception>
        public void LerArquivoXML(StreamReader sr)
        {
            this.dataSet.EnforceConstraints = false;

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(sr.ReadToEnd());

            // Volta o Ponteiro para o Começo 
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            #region Header
            XmlNodeList xnList = xml.SelectNodes(NODE_HEADER_FUNDO);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_HEADER_CARTEIRA);

                if (xnList.Count == 0)
                {
                    throw new Exception("Arquivo XML no formato Incorreto: Node Header Incorreto.");
                }
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);

                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Titulo Publico
            bool isCarteira = false;
            xnList = xml.SelectNodes(NODE_TITULO_PUBLICO_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_TITULO_PUBLICO_FUNDO);
            }
            else
            {
                isCarteira = true;
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);

                //Carregar colunas do compromisso
                XmlNodeList nodesCompromissos;
                if (isCarteira)
                {
                    nodesCompromissos = xn.SelectNodes(NODE_COMPROMISSO_TITULO_PUBLICO_CARTEIRA);
                }
                else
                {
                    nodesCompromissos = xn.SelectNodes(NODE_COMPROMISSO_TITULO_PUBLICO_FUNDO);
                }
                if (nodesCompromissos.Count > 0)
                {
                    XmlNode nodeCompromisso = nodesCompromissos[0];
                    
                    byte[] xmlBufferCompromisso = ASCIIEncoding.ASCII.GetBytes(nodeCompromisso.OuterXml);
                    MemoryStream readerCompromisso = new MemoryStream(xmlBufferCompromisso);
                
                    DataSet dataSet = new DataSet();
                    dataSet.ReadXml(readerCompromisso, XmlReadMode.InferSchema);
                    DataTable nodeTable = dataSet.Tables[0];

                    DataTable dataTable = this.dataSet.Tables[TITULO_PUBLICO];
                    if (!dataTable.Columns.Contains("dtretorno"))
                    {
                        dataTable.Columns.Add("dtretorno");
                        dataTable.Columns.Add("puretorno");
                        dataTable.Columns.Add("indexadorcomp");
                        dataTable.Columns.Add("perindexcomp");
                        dataTable.Columns.Add("txoperacao");
                        dataTable.Columns.Add("classecomp");
                    }

                    int rowIndex = dataTable.Rows.Count - 1;
                    dataTable.Rows[rowIndex]["dtretorno"] = nodeTable.Rows[0]["dtretorno"];
                    dataTable.Rows[rowIndex]["puretorno"] = nodeTable.Rows[0]["puretorno"];
                    dataTable.Rows[rowIndex]["indexadorcomp"] = nodeTable.Rows[0]["indexadorcomp"];
                    dataTable.Rows[rowIndex]["perindexcomp"] = nodeTable.Rows[0]["perindexcomp"];
                    dataTable.Rows[rowIndex]["txoperacao"] = nodeTable.Rows[0]["txoperacao"];
                    dataTable.Rows[rowIndex]["classecomp"] = nodeTable.Rows[0]["classecomp"];

                }
            }
            #endregion

            #region Titulo Privado
            xnList = xml.SelectNodes(NODE_TITULO_PRIVADO_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_TITULO_PRIVADO_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);


                //Carregar colunas do compromisso
                XmlNodeList nodesCompromissos;
                if (isCarteira)
                {
                    nodesCompromissos = xn.SelectNodes(NODE_COMPROMISSO_TITULO_PRIVADO_CARTEIRA);
                }
                else
                {
                    nodesCompromissos = xn.SelectNodes(NODE_COMPROMISSO_TITULO_PRIVADO_FUNDO);
                }
                if (nodesCompromissos.Count > 0)
                {
                    XmlNode nodeCompromisso = nodesCompromissos[0];

                    byte[] xmlBufferCompromisso = ASCIIEncoding.ASCII.GetBytes(nodeCompromisso.OuterXml);
                    MemoryStream readerCompromisso = new MemoryStream(xmlBufferCompromisso);

                    DataSet dataSet = new DataSet();
                    dataSet.ReadXml(readerCompromisso, XmlReadMode.InferSchema);
                    DataTable nodeTable = dataSet.Tables[0];

                    DataTable dataTable = this.dataSet.Tables[TITULO_PRIVADO];
                    if (!dataTable.Columns.Contains("dtretorno"))
                    {
                        dataTable.Columns.Add("dtretorno");
                        dataTable.Columns.Add("puretorno");
                        dataTable.Columns.Add("indexadorcomp");
                        dataTable.Columns.Add("perindexcomp");
                        dataTable.Columns.Add("txoperacao");
                        dataTable.Columns.Add("classecomp");
                    }

                    int rowIndex = dataTable.Rows.Count - 1;
                    dataTable.Rows[rowIndex]["dtretorno"] = nodeTable.Rows[0]["dtretorno"];
                    dataTable.Rows[rowIndex]["puretorno"] = nodeTable.Rows[0]["puretorno"];
                    dataTable.Rows[rowIndex]["indexadorcomp"] = nodeTable.Rows[0]["indexadorcomp"];
                    dataTable.Rows[rowIndex]["perindexcomp"] = nodeTable.Rows[0]["perindexcomp"];
                    dataTable.Rows[rowIndex]["txoperacao"] = nodeTable.Rows[0]["txoperacao"];
                    dataTable.Rows[rowIndex]["classecomp"] = nodeTable.Rows[0]["classecomp"];

                }
 
            }
            #endregion

            #region Debenture
            xnList = xml.SelectNodes(NODE_DEBENTURE_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_DEBENTURE_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Acoes
            xnList = xml.SelectNodes(NODE_ACOES_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_ACOES_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region OpcoesAcoes
            xnList = xml.SelectNodes(NODE_OPCOESACOES_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_OPCOESACOES_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region OpcoesDeriv
            xnList = xml.SelectNodes(NODE_OPCOESDERIV_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_OPCOESDERIV_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Opcoes flx
            xnList = xml.SelectNodes(NODE_OPCOESFLX_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_OPCOESFLX_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region TermoRV
            xnList = xml.SelectNodes(NODE_TERMO_RV_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_TERMO_RV_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Futuros
            xnList = xml.SelectNodes(NODE_FUTUROS_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_FUTUROS_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Swap
            xnList = xml.SelectNodes(NODE_SWAP_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_SWAP_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Caixa
            xnList = xml.SelectNodes(NODE_CAIXA_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_CAIXA_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Cotas
            xnList = xml.SelectNodes(NODE_COTAS_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_COTAS_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Provisao
            xnList = xml.SelectNodes(NODE_PROVISAO_CARTEIRA);

            if (xnList.Count == 0)
            {
                xnList = xml.SelectNodes(NODE_PROVISAO_FUNDO);
            }

            foreach (XmlNode xn in xnList)
            {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion
        }

        /// <summary>
        /// Transforma uma string no formato Ano-Mes-Dia para um Datetime
        /// </summary>
        /// <param name="data">string no formato Ano-Mes-Dia</param>
        /// <returns></returns>
        /// <exception cref="">throws Exception se data nao tiver tamanho 8</exception>
        public DateTime GetDate(string data)
        {
            if (data.Length != 8)
            {
                throw new Exception("Formato de Data do arquivo XML inválido");
            }

            return new DateTime(Convert.ToInt32(data.Substring(0, 4)),
                                 Convert.ToInt32(data.Substring(4, 2)),
                                 Convert.ToInt32(data.Substring(6, 2)));
        }

        public string RetornaTipoLancamento(int tipoLancamento)
        {
            switch (tipoLancamento)
            {
                case 1:
                    return "Despesa com Advogados";
                case 2:
                    return "Despesa de Auditoria";
                case 3:
                    return "Despesas Bancárias";
                case 4:
                    return "Despesa com Cartório";
                case 5:
                    return "Despesa com Correspondências";
                case 6:
                    return "Despesas com Impressos";
                case 7:
                    return "Despesas Jurídicas";
                case 8:
                    return "Outras Despesas Administrativas";
                case 9:
                    return "Outras Despesas Exterior";
                case 10:
                    return "Despesa com Publicação de Atas";
                case 11:
                    return "Despesa com Publicidade";
                case 12:
                    return "Diferimento de despesa Anbima";
                case 13:
                    return "Despesa de CETIP";
                case 14:
                    return "Diferimento de despesa de Taxa C.V.M.";
                case 15:
                    return "Taxa de Custódia Apropriada";
                case 16:
                    return "Despesa de SELIC";
                case 17:
                    return "Taxa SISBACEN";
                case 18:
                    return "Títulos Públicos";
                case 19:
                    return "Títulos Privados";
                case 20:
                    return "Debêntures";
                case 21:
                    return "Valor Líquido (Ações ou Opções de Ações)";
                case 22:
                    return "Valor Líquido (Derivativos)";
                case 23:
                    return "Termo Ações";
                case 24:
                    return "Termo Selic";
                case 26:
                    return "Swap";
                case 27:
                    return "Dividendos a Receber";
                case 28:
                    return "Juros s/ Capital Próprio a Receber";
                case 29:
                    return "Subscrições";
                case 30:
                    return "Juros (RF)";
                case 31:
                    return "Empréstimo Ação";
                case 32:
                    return "Empréstimo Título Público";
                case 33:
                    return "Aluguel Imóvel";
                case 34:
                    return "Taxa de Administração Apropriada";
                case 35:
                    return "Taxa de Performance Apropriada";
                case 36:
                    return "Despesa Corretagem Bovespa";
                case 37:
                    return "Despesa Corretagem BMF";
                case 38:
                    return "Emolumentos";
                case 39:
                    return "Valor Bovespa";
                case 40:
                    return "Valor Repasse Bovespa";
                case 41:
                    return "Valor BMF";
                case 42:
                    return "Valor Repasse BMF";
                case 43:
                    return "Valor Outras Bolsas";
                case 44:
                    return "Valor Repasse Outras Bolsas";
                case 45:
                    return "Aplicação a Converter";
                case 46:
                    return "Resgate a Converter";
                case 47:
                    return "Resgate a Liquidar";
                case 999:
                    return "Outros Valores";
                default:
                    return "";

            }
        }
    }
}
