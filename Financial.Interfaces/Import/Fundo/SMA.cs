﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;
using System.Globalization;


namespace Financial.Interfaces.Import.Fundo
{
    public class SMA
    {
       
        public SMA()
        {

        }

        [DelimitedRecord("\t")]
        [IgnoreFirst(1)]
        public class MovimentoPassivo
        {
            public string CdFundo;
            public string DescricaoFundo;

            [FieldConverter(typeof(ConvertFlexDate))]
            public DateTime DtMovimento;

            [FieldConverter(typeof(ConvertFlexDate))]
            public DateTime? DtCotizacao;
            
            public string CdCotista;
            public string NomeCotista;
            public string CpfCnpj;
            public int IdNota;
            public string TipoMovimentacao;
            public decimal QuantidadeCotas;
            public decimal ValorCota;
            public decimal ValorBruto;
            public decimal ValorIR;
            public decimal ValorIOF;
            public decimal ValorLiquido;
            public int IdNotaAplicacao;
            public decimal RendimentoBruto;
            public decimal ValorPerformance;
            public int CodigoOrdem;
            public string TipoTransacao;
            public string Distribuidor;
            public string Operador;            
        }

        [DelimitedRecord("\t")]
        [IgnoreFirst(1)]
        public class PosicaoDetalhadaPassivo
        {
            public string CdCotista;
            public string NomeCotista;
            public string CpfCnpj;
            public string CdFundo;
            public string NomeFundo;
            public int IdNota;

            [FieldConverter(typeof(ConvertFlexDate))]
            public DateTime DtMovimento;

            public decimal AplicacaoOriginal;
            public decimal AplicacaoCorrigida;

            [FieldConverter(typeof(ConvertFlexDate))]
            public DateTime? DtCota;
                        
            public decimal QuantidadeCotas;
            public decimal ValorCota;
            public decimal ValorBruto;
            public decimal ValorIR;
            public decimal ValorIOF;
            public decimal ValorLiquido;
            public decimal PercentualRendimento;
            public int IdadeNota;

            [FieldConverter(typeof(ConvertFlexDate))]
            public DateTime? DtUltimoResgateIR;

            public decimal ValorCotaUltimoResgateIR;
            public string Distribuidor;
            public string Operador;
        }

        internal class ConvertFlexDate : ConverterBase
        {
            /// <summary>
            /// conversor para campos de data que possuem ou não milissegundos (com todas suas variacoes)
            /// </summary>
            /// <param name="from">the string format of date - first the day</param>
            /// <returns></returns>

            public override object StringToField(string from)
            {
                DateTime dt;

                const string FILEHELPER_DATEFORMAT_DAY = "d/M/yyyy";
                const string FILEHELPER_DATEFORMAT = "dd/MM/yyyy";
                const string FILEHELPER_DATEFORMAT_SECONDS = "dd/MM/yyyy HH:mm:ss";
                const string FILEHELPER_DATEFORMAT_MINUTES = "dd/MM/yyyy HH:mm";
                const string FILEHELPER_DATEFORMAT_MS = "dd/MM/yyyy HH:mm:ss:FFF";

                if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_DAY, null, DateTimeStyles.None, out dt))
                {
                    return dt;
                }

                if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT, null, DateTimeStyles.None, out dt))
                {
                    return dt;
                }

                if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_SECONDS, null, DateTimeStyles.None, out dt))
                {
                    return dt;
                }

                if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_MINUTES, null, DateTimeStyles.None, out dt))
                {
                    return dt;
                }

                if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_MS, null, DateTimeStyles.None, out dt))
                {
                    return dt;
                }

                throw new ArgumentException("can not make a date from " + from, "from");

            }
        }

        public MovimentoPassivo[] ImportaMovimentoPassivo(System.IO.Stream movimentoStream)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(MovimentoPassivo));

            System.IO.TextReader textReader = new System.IO.StreamReader(movimentoStream);

            MovimentoPassivo[] movimentos = engine.ReadStream(textReader) as MovimentoPassivo[];

            return movimentos;
            
        }

        public PosicaoDetalhadaPassivo[] ImportaPosicaoDetalhadaPassivo(System.IO.Stream movimentoStream)
        {
            FileHelperEngine engine = new FileHelperEngine(typeof(PosicaoDetalhadaPassivo));

            System.IO.TextReader textReader = new System.IO.StreamReader(movimentoStream);

            PosicaoDetalhadaPassivo[] movimentos = engine.ReadStream(textReader) as PosicaoDetalhadaPassivo[];

            return movimentos;

        }
    }
}
