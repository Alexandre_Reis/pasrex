﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Interfaces.Import.Fundo.Exceptions {
    /// <summary>
    /// 
    /// </summary>
    public class InterfacesException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InterfacesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InterfacesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InterfacesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    #region HistoricoCotaYMF
    /// <summary>
    /// Exceção ProcessaHistoricoCotaYMFException
    /// </summary>
    public class ProcessaHistoricoCotaYMFException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaHistoricoCotaYMFException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaHistoricoCotaYMFException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region PosicaoFundoYMF
    /// <summary>
    /// Exceção ProcessaPosicaoFundoYMFException
    /// </summary>
    public class ProcessaPosicaoFundoYMFException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPosicaoFundoYMFException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPosicaoFundoYMFException(string mensagem) : base(mensagem) { }
    }
    #endregion

    #region ExtratoFundoYMF
    /// <summary>
    /// Exceção ProcessaExtratoFundoYMFException
    /// </summary>
    public class ProcessaExtratoFundoYMFException : InterfacesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaExtratoFundoYMFException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaExtratoFundoYMFException(string mensagem) : base(mensagem) { }
    }
    #endregion
}