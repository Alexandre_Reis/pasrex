using System;
using System.Collections.Generic;
using System.Text;
using Financial.Interfaces.Properties;

namespace Financial.Interfaces
{
    public static class Util
    {
        public static string DiretorioBase
        {
            get
            {
                return Settings.Default.DiretorioBase;
            }
        }
    }
}
