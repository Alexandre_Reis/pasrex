<%@ WebHandler Language="C#" Class="SendMail" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using HtmlAgilityPack;

public class SendMail : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        const string PATH_IMAGENS_PERSONALIZADAS = @"\imagensPersonalizadas";
        const string SUBJECT_DEFAULT = "Informa��es de Acesso ao FinancialOnline";
        const string BODY_DEFAULT = "Prezado(a) {NomeCliente}<br /><br />Estamos satisfeitos que nos tenham escolhido para fazer seus investimentos.<br /><br />Sua carteira de investimentos poder� ser acompanhada pelo site {HostAplicacao}. Para acessar as informa��es, digite:<br /><br /><br /><b>Login:</b> {LoginUsuario}<br /><br /><b>Senha:</b> {SenhaUsuario}<br /><br /><br />Gostar�amos de lhe dar as boas-vindas e nos colocar � sua disposi��o.<br /><br />Atenciosamente,<br /><br />Equipe {NomeEmpresaClienteFinancial}";
        const string FROM_DEFAULT = "suporte@financialonline.com.br";
        const int START_CID = 1;

        string mailUsuario = "efreitasrj@gmail.com";
        
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        MailMessage mail = new MailMessage();
        mail.To.Add(new MailAddress(mailUsuario));

        string from = settings.Smtp.From.ToString();
        if (String.IsNullOrEmpty(from))
        {
            from = FROM_DEFAULT;
        }
        
        mail.From = new MailAddress(from);

        string subject = ConfigurationManager.AppSettings["SubjectNovoUsuario"];
        if (String.IsNullOrEmpty(subject))
        {
            subject = SUBJECT_DEFAULT;
        }

        mail.Subject = subject;

        //Exemplos do PrivateBP - primeiro sem encode e o segundo encoded - levar para o documents
        //"<div lang=\"PT-BR\" vlink=\"purple\" link=\"blue\"><div><div align=\"center\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;\"><tbody><tr><td width=\"662\" valign=\"top\" style=\"width: 496.75pt; border: 1pt solid white; padding: 0cm 5.4pt;\"><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\"><img src=\"logoReport_Socopa.jpg\" /></span></font></p><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></td></tr><tr><td width=\"662\" valign=\"top\" style=\"width: 496.75pt; border-right: 1pt solid white; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color white white; padding: 0cm 1cm;\"><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Prezado Sr. {NomeCliente},</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"margin-bottom: 12pt; text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Estamos satisfeitos que tenha escolhido a SOCOPA &ndash; CORRETORA PAULISTA para fazer seus investimentos.</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">A partir de agora, voc� contar� com o servi�o diferenciado que oferecemos, buscando sempre atender suas necessidades e expectativas de forma personalizada e exclusiva.</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">Sua carteira de investimentos poder� ser acompanhada pelo site <a target=\"_blank\" href=\"{HostAplicacao}\"><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\">{HostAplicacao}</span></font></a>. Para acessar as informa��es, digite:</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><b><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;\">Login:</span></font></b><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\"> {LoginUsuario}</span></font></p><p style=\"text-align: justify;\"><b><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;\">Senha:</span></font></b><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\"> {SenhaUsuario}</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">A senha � provis�ria e <b><span style=\"font-weight: bold;\">deve ser alterada no primeiro acesso</span></b>.</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">Em caso de d�vida, entre em contato com o seu <i><span style=\"font-style: italic;\">banker</span></i>, {NomeOfficer}, pelo telefone {TelefoneOfficer} ou e-mail <a target=\"_blank\" href=\"mailto:{EmailOfficer}\"><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\">{EmailOfficer}</span></font></a>.</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Gostar�amos de lhe dar as boas-vindas e nos colocar � sua disposi��o. </span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Atenciosamente,</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">SOCOPA - CORRETORA PAULISTA</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Private - Gest�o de Patrim�nio</span></font></p><p class=\"MsoNormal\"><font face=\"Trebuchet MS\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></td></tr></tbody></table></div><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></div></div>"
        //WEBCONFIG: &lt;div lang="PT-BR" vlink="purple" link="blue"&gt;&lt;div&gt;&lt;div align="center"&gt;&lt;table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td width="662" valign="top" style="width: 496.75pt; border: 1pt solid white; padding: 0cm 5.4pt;"&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;img src="logoReport_Socopa.jpg" /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td width="662" valign="top" style="width: 496.75pt; border-right: 1pt solid white; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color white white; padding: 0cm 1cm;"&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Prezado Sr. {NomeCliente},&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="margin-bottom: 12pt; text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Estamos satisfeitos que tenha escolhido a SOCOPA - CORRETORA PAULISTA para fazer seus investimentos.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;A partir de agora, voc� contar� com o servi�o diferenciado que oferecemos, buscando sempre atender suas necessidades e expectativas de forma personalizada e exclusiva.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;Sua carteira de investimentos poder� ser acompanhada pelo site &lt;a target="_blank" href="{HostAplicacao}"&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt;{HostAplicacao}&lt;/span&gt;&lt;/font&gt;&lt;/a&gt;. Para acessar as informa��es, digite:&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;b&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;"&gt;Login:&lt;/span&gt;&lt;/font&gt;&lt;/b&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt; {LoginUsuario}&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;b&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;"&gt;Senha:&lt;/span&gt;&lt;/font&gt;&lt;/b&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt; {SenhaUsuario}&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;A senha � provis�ria e &lt;b&gt;&lt;span style="font-weight: bold;"&gt;deve ser alterada no primeiro acesso&lt;/span&gt;&lt;/b&gt;.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;Em caso de d�vida, entre em contato com o seu &lt;i&gt;&lt;span style="font-style: italic;"&gt;banker&lt;/span&gt;&lt;/i&gt;, {NomeOfficer}, pelo telefone {TelefoneOfficer} ou e-mail &lt;a target="_blank" href="mailto:{EmailOfficer}"&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt;{EmailOfficer}&lt;/span&gt;&lt;/font&gt;&lt;/a&gt;.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Gostar�amos de lhe dar as boas-vindas e nos colocar � sua disposi��o. &lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Atenciosamente,&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;SOCOPA - CORRETORA PAULISTA&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Private - Gest�o de Patrim�nio&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Trebuchet MS" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;/div&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;
        string body = ConfigurationManager.AppSettings["BodyNovoUsuario"];
        if (String.IsNullOrEmpty(body))
        {
            body = BODY_DEFAULT;
        }

        #region Fazer traducao das strings dinamicas (placeholders) como login, senha, nome do cliente
        string[] placeholders = { "{LoginUsuario}", "{SenhaUsuario}", "{NomeCliente}", 
            "{NomeOfficer}", "{TelefoneOfficer}", "{EmailOfficer}", "{HostAplicacao}", 
            "{NomeEmpresaClienteFinancial}" };

        string hostAplicacao = ConfigurationManager.AppSettings["HostAplicacao"];
        string nomeEmpresaClienteFinancial = ConfigurationManager.AppSettings["Cliente"];
        
        string[] placeholdersValues = { "efreitas", "minha_senha", "Eduardo de Freitas",
            "Banker Freitas", "11-5051-5597", "efreitas@atatika.com.br", hostAplicacao, 
            nomeEmpresaClienteFinancial};
        
        
        for (int i = 0; i < placeholders.Length; i++)
        {
            body = body.Replace(placeholders[i], placeholdersValues[i]);
        }
        #endregion

        #region Caso existam imagens locais (sem http no src), fazer o embed no email
        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.LoadHtml(body);

        int cid = START_CID;
        HtmlNodeCollection htmlImageNodeCollection = htmlDocument.DocumentNode.SelectNodes("//img");

        if (htmlImageNodeCollection != null && htmlImageNodeCollection.Count > 0)
        {

            List<string> imageSrcList = new List<string>();
            foreach (HtmlNode htmlImageNode in htmlImageNodeCollection)
            {
                //Nao incluir imagens referenciadas externamente
                string imageSrc = htmlImageNode.GetAttributeValue("src", null);
                if (!String.IsNullOrEmpty(imageSrc) && !imageSrc.ToLower().StartsWith("http"))
                {
                    imageSrcList.Add(imageSrc);
                    htmlImageNode.SetAttributeValue("src", String.Format("cid:{0}", cid));
                    cid++;
                }
            }

            if (imageSrcList.Count > 0)
            {
                //Atualizar html com imagens alteradas
                body = htmlDocument.DocumentNode.WriteTo();

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

                cid = START_CID;
                foreach (string imageSrc in imageSrcList)
                {
                    LinkedResource imagelink = new LinkedResource(
                        String.Format(@"{0}{1}\{2}", AppDomain.CurrentDomain.BaseDirectory,
                        PATH_IMAGENS_PERSONALIZADAS, imageSrc), GetMimeType(imageSrc));

                    imagelink.ContentId = cid.ToString();
                    imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    htmlView.LinkedResources.Add(imagelink);
                    cid++;
                }

                mail.AlternateViews.Add(htmlView);
            }
        }
        #endregion

        mail.Body = body;
        mail.IsBodyHtml = true;
        
        SmtpClient smtp = new SmtpClient();

        if (ConfigurationManager.AppSettings["SMTPEnableSSL"] == "true")
        {
            smtp.EnableSsl = true;
        }
        smtp.Send(mail);

        context.Response.ContentType = "text/plain";
        context.Response.Write(mail.Body);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    //METER NO UTIL !!!!
    public string GetMimeType(string fileName)
    {
        fileName = fileName.ToLower();
        
        string fileExtension = System.IO.Path.GetExtension(fileName);
        string mimeType="";
        
        if (fileExtension == ".gif")
        {
            mimeType = "image/gif";
        }
        else if (fileExtension == ".jpg")
        {
            mimeType = "image/jpeg";
        }
        else if (fileExtension == ".png")
        {
            mimeType = "image/png";
        }

        return mimeType;
    }

}