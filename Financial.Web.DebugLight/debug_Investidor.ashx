<%@ WebHandler Language="C#" Class="debug" %>

using System;
using System.Web;
using Financial.Interfaces.Import.RendaFixa;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Investidor.Controller;
using Financial.BMF;

public class debug : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        ParametrosProcessamento p = new ParametrosProcessamento();
        p.IntegraBMF = false;
        p.IntegraBolsa = false;
        p.IntegraCC = false;
        p.ProcessaBMF = true;
        p.ProcessaBolsa = true;
        p.ProcessaRendaFixa = true;
        p.ProcessaSwap = true;
        
        ControllerInvestidor controller = new ControllerInvestidor();
        //controller.ExecutaFechamento(1007, new DateTime(2011, 05, 03), p);
        controller.ExecutaAbertura(1007, new DateTime(2011, 05, 16), new ParametrosProcessamento());

        /*OperacaoBMF operacaoBMF = new OperacaoBMF();
        operacaoBMF.CalculaCorretagemOperacao(100, new DateTime(2011, 03, 24));*/
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}