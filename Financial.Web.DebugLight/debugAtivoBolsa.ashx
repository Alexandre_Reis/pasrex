<%@ WebHandler Language="C#" Class="debug" %>

using System;
using System.Web;
using Financial.Interfaces.Import.RendaFixa;
using Financial.Common;
using Financial.Bolsa;

public class debug : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        AtivoBolsa ativoBolsa = new AtivoBolsa();
         DateTime data = ativoBolsa.RetornaDataVencimentoOpcaoIBOV(new DateTime(2011, 06, 09), new DateTime(2011, 06, 15));

        context.Response.ContentType = "text/plain";
        context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}