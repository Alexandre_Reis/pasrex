﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Fundo;
namespace Test.Financial.Fundo {
    /// <summary>
    ///This is a test class for Financial.Fundo.CadastroProvisao and is intended
    ///to contain all Financial.Fundo.CadastroProvisao Unit Tests
    ///</summary>
    [TestClass()]
    public class CadastroProvisaoTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for RetornaIdCadastro (int)
        ///</summary>
        [TestMethod()]
        public void RetornaIdCadastroTest() {
            CadastroProvisao target = new CadastroProvisao();

            int tipo = 0;
            int expected = 0;

            int actual = target.RetornaIdCadastro(tipo);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

    }


}
