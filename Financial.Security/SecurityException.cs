﻿using System;

namespace Financial.Security.Exceptions {
    /// <summary>
    /// Classe base de Exceção do componente de Security
    /// </summary>
    public class SecurityException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public SecurityException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public SecurityException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public SecurityException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de UsuarioNaoCadastrado
    /// </summary>
    public class UsuarioNaoCadastradoException : SecurityException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public UsuarioNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public UsuarioNaoCadastradoException(string mensagem) : base(mensagem) { }
    }
}


