using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using Financial.Security.Enums;
using Financial.Util;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.DirectoryServices;

namespace Financial.Security
{
    public class FinancialMembershipProvider : MembershipProvider
    {
        /// <summary>
        /// Classe que Contém as Informações de Configuração da Senha
        /// </summary>
        protected class ConfiguracaoSenha
        {
            private int tamanhoSenha = 0; // Minimo
            private int caracterEspecial = 0; //Minimo
            private int quantidadeNumero = 0; // Minimo
            private int letrasMaiusculas = 0; // Minimo
            private int historicoSenhas = 0;
            private int tempoExpiracaoSenha = 0;

            public int TamanhoSenha { get { return this.tamanhoSenha; } }
            public int CaracterEspecial { get { return this.caracterEspecial; } }
            public int QuantidadeNumero { get { return this.quantidadeNumero; } }
            public int LetrasMaiusculas { get { return this.letrasMaiusculas; } }
            public int HistoricoSenhas { get { return this.historicoSenhas; } }
            public int TempoExpiracaoSenha { get { return this.tempoExpiracaoSenha; } }

            public ConfiguracaoSenha()
            {
                this.tamanhoSenha = ParametrosConfiguracaoSistema.Seguranca.TamanhoMinimoSenha;
                this.caracterEspecial = ParametrosConfiguracaoSistema.Seguranca.MinimoCaracterEspecialSenha;
                this.quantidadeNumero = ParametrosConfiguracaoSistema.Seguranca.MinimoNumeroSenha;
                this.letrasMaiusculas = ParametrosConfiguracaoSistema.Seguranca.LetrasMaiusculas;
                this.historicoSenhas = ParametrosConfiguracaoSistema.Seguranca.HistoricoSenhas;
                this.tempoExpiracaoSenha = ParametrosConfiguracaoSistema.Seguranca.TempoExpiracaoSenha;
            }
        }

        public DateTime DATA_CRIACAO_DEFAULT = new DateTime(1900, 1, 1);

        /* UserName e Password Master */
        private const string USERNAME = "adminmaster@$";
        private const string PASS = "+XmlOP2b/zmUNz5Z+yLUHob797NPh/Bpy3Sq/Q3VYlQ=";

        private string[] userPassMaster = new string[2] { USERNAME, PASS };

        /// <summary>
        /// [0] = Username Master
        /// [1] = Pass code Master codificado
        /// </summary>
        public string[] UserPassMaster
        {
            get { return userPassMaster; }
        }

        /// <summary>
        /// Retorna se é usuario Master
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool IsUserMaster(string user)
        {
            return user == USERNAME;
        }

        //
        // generated password length, generic exception message, event log info.
        //
        private int newPasswordLength = 8;
        private string eventSource = "FinancialMembershipProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "An exception occurred. Please check the Event Log.";

        //
        // Used when determining encryption key values.
        //
        private MachineKeySection machineKey;

        public MachineKeySection MachineKey
        {
            get { return machineKey; }
            set { machineKey = value; }
        }

        //
        // If false, exceptions are thrown to the caller. If true,
        // exceptions are written to the event log.
        //
        private bool pWriteExceptionsToEventLog;

        public bool WriteExceptionsToEventLog
        {
            get { return pWriteExceptionsToEventLog; }
            set { pWriteExceptionsToEventLog = value; }
        }

        #region Variáveis privadas
        private string applicationName; //Essa variável não está sendo usada.
        private bool enablePasswordReset;
        private bool enablePasswordRetrieval;
        private bool requiresQuestionAndAnswer;
        private bool requiresUniqueEmail;
        private int maxInvalidPasswordAttempts;
        private int passwordAttemptWindow;
        private MembershipPasswordFormat passwordFormat;
        private int minRequiredNonAlphanumericCharacters;
        private int minRequiredPasswordLength;
        private string passwordStrengthRegularExpression;
        #endregion

        #region Properties
        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        } //Essa property não está sendo usada.

        public override bool EnablePasswordReset
        {
            get { return enablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return enablePasswordRetrieval; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return requiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return requiresUniqueEmail; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return maxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return passwordAttemptWindow; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return passwordFormat; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return minRequiredNonAlphanumericCharacters; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return minRequiredPasswordLength; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return passwordStrengthRegularExpression; }
        }
        #endregion

        //
        // A helper function to retrieve config values from the configuration file.
        //
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public void Initialize()
        {
            NameValueCollection listaValores = new NameValueCollection();
            listaValores.Add("passwordFormat", "Encrypted");
            this.Initialize(null, listaValores);
        }

        /// <summary>
        /// Inicializa as variáveis.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        /// 
        public override void Initialize(string name, NameValueCollection config)
        {
            //
            // Initialize values from web.config.
            //
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "FinancialMembershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Financial Membership provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);
            //
            int tentativasBloqueioSenha = ParametrosConfiguracaoSistema.Seguranca.TentativasBloqueioSenha;
            int janelaTempoResetSenha = ParametrosConfiguracaoSistema.Seguranca.JanelaTempoResetSenha;

            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], Convert.ToString(tentativasBloqueioSenha)));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], Convert.ToString(janelaTempoResetSenha)));
            minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            // Get encryption and decryption key information from the configuration.

            /*if ((machineKey == null))
            {
                Configuration cfg =
                  WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
                machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");
            }

            if (machineKey.ValidationKey.Contains("AutoGenerate"))
                if (PasswordFormat != MembershipPasswordFormat.Clear)
                    throw new ProviderException("Hashed or Encrypted passwords " +
                                                "are not supported with auto-generated keys.");*/
        }

        public bool ValidatePassword(string oldPwd, string newPwd)
        {
            if (String.IsNullOrEmpty(newPwd))
            {
                throw new MembershipPasswordException("A senha deve ser preenchida.");
            }

            if (oldPwd == newPwd)
            {
                throw new MembershipPasswordException("A nova senha não pode ser igual a anterior.");
            }

            ConfiguracaoSenha conf = new ConfiguracaoSenha();

            // Conferência com as Configurações de Segurança            
            #region Confere Tamanho Minimo da Senha
            if (newPwd.Length < conf.TamanhoSenha)
            {
                throw new MembershipPasswordException(String.Format("Tamanho Mínimo da Senha: {0} {1}.",
                    conf.TamanhoSenha, (conf.TamanhoSenha > 1 ? "caracteres" : "caracter")));
            }
            #endregion

            #region Confere Quantidade de caracteres Numéricos
            string numbersPwd = Regex.Replace(newPwd, @"[^\d]", ""); // String só dos caractetes Númericos
            if (numbersPwd.Length < conf.QuantidadeNumero)
            {
                throw new MembershipPasswordException(String.Format("Quantidade Mínima de Caracteres Numéricos: {0} {1}.",
                    conf.QuantidadeNumero, (conf.QuantidadeNumero > 1 ? "caracteres" : "caracter")));
            }
            #endregion

            #region Confere Quantidade de Caracteres Especiais
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < newPwd.Length; i++)
            {
                if (Utilitario.IsCaracterEspecial(newPwd[i]))
                {
                    sb.Append(newPwd[i]);
                }
            }

            if (sb.ToString().Length < conf.CaracterEspecial)
            {
                throw new MembershipPasswordException(String.Format("Quantidade Mínima de Caracteres Especiais: {0} {1}.",
                    conf.CaracterEspecial, (conf.CaracterEspecial > 1 ? "caracteres" : "caracter")));
            }
            #endregion

            #region Confere Quantidade de Letras Maiusculas
            string uppercasePwd = Regex.Replace(newPwd, @"[^A-Z]", ""); // String só dos caractetes Maiusculos
            if (uppercasePwd.Length < conf.LetrasMaiusculas)
            {
                throw new MembershipPasswordException(String.Format("Quantidade Mínima de Caracteres Maísculos: {0} {1}.",
                    conf.LetrasMaiusculas, (conf.LetrasMaiusculas > 1 ? "caracteres" : "caracter")));
            }
            #endregion

            return true;
        }

        public bool ChecaHistoricoSenha(int idUsuario, string senha)
        {
            ConfiguracaoSenha conf = new ConfiguracaoSenha();

            HistoricoSenhasCollection historicoSenhasCollection = new HistoricoSenhasCollection();
            historicoSenhasCollection.Query.Where(historicoSenhasCollection.Query.IdUsuario.Equal(idUsuario));
            historicoSenhasCollection.Query.OrderBy(historicoSenhasCollection.Query.Data.Descending);
            historicoSenhasCollection.Query.Load();

            int i = 0;
            bool senhaNaoUsada = true;
            foreach (HistoricoSenhas historicoSenhas in historicoSenhasCollection)
            {
                if (historicoSenhas.Senha == senha)
                {
                    if (i <= conf.HistoricoSenhas)
                    {
                        throw new MembershipPasswordException("Esta senha já foi usada no histórico das últimas " + conf.HistoricoSenhas.ToString() + " senhas.");
                    }
                }

                i++;

                if (i > conf.HistoricoSenhas)
                {
                    break;
                }
            }

            return senhaNaoUsada;
        }

        /*public void ResetTentativasAcesso(Usuario usuario)
        {
            usuario.TentativasAcessoSenha = 0;
            usuario.InicioTentativasAcessoSenha = DateTime.Now;
        }*/

        /*
        public bool ChangePassword(Usuario usuario, string oldPwd, string newPwd)
        {
            //Inicializar tipo de encode do password
            this.Initialize();

            if (string.IsNullOrEmpty(oldPwd))
            {
                oldPwd = this.UnEncodePassword(usuario.Senha);
            }

            if (!this.ValidatePassword(oldPwd, newPwd))
            {
                return false;
            }

            usuario.Senha = this.EncodePassword(newPwd);
            usuario.DataUltimaAlteracao = DateTime.Now;
            usuario.TentativasAcessoSenha = 0;
            usuario.InicioTentativasAcessoSenha = DateTime.Now;
            //usuario.TrocarSenha = false; AQUI !
            usuario.Save();

            return true;       
        }*/

        /// <summary>
        /// Atualiza a senha do usuário.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="oldPwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            throw new Exception("Não chamar a troca de senha pelo método ChangePassword do Membership");

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(username);

            return false;
        }

        /// <summary>
        /// Atualiza a pergunta e resposta para o login.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="newPwdQuestion"></param>
        /// <param name="newPwdAnswer"></param>
        /// <returns></returns>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {
            throw new Exception("Não chamar a troca de senha pelo método ChangePassword do Membership");
            if (!ValidateUser(username, password))
                return false;

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(username);

            usuario.PerguntaLogin = newPwdAnswer;
            usuario.RespostaLogin = EncodePassword(newPwdAnswer);
            usuario.DataUltimaAlteracao = DateTime.Now;
            usuario.Save();

            return true;
        }

        /// <summary>
        /// Cria novo usuário.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="passwordQuestion"></param>
        /// <param name="passwordAnswer"></param>
        /// <param name="isApproved"></param>
        /// <param name="providerUserKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public override MembershipUser CreateUser(string username,
                 string password,
                 string email,
                 string passwordQuestion,
                 string passwordAnswer,
                 bool isApproved,
                 object providerUserKey,
                 out MembershipCreateStatus status)
        {

            //Acredito que o código abaixo não esteja em USO ! Checar com o RAUL...
            throw new Exception("Não chamar a criação de usuário pelo método CreateUser do Membership");

            ValidatePasswordEventArgs args =
              new ValidatePasswordEventArgs(username, password, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser membershipUser = GetUser(username, false);

            if (membershipUser == null)
            {
                DateTime createDate = DateTime.Now;

                Usuario usuario = new Usuario();

                string statusAtivo = isApproved ? "S" : "N";
                usuario.StatusAtivo = statusAtivo;

                usuario.StatusBloqueado = "N";
                usuario.DataCadastro = createDate;
                usuario.DataUltimaAlteracao = createDate;
                usuario.DataUltimoLogin = createDate;
                usuario.DataValidade = createDate; //TODO REVER ISSO...
                usuario.Email = email;
                usuario.InicioTentativasAcessoResposta = createDate;
                usuario.InicioTentativasAcessoSenha = createDate;
                usuario.Login = username;
                usuario.Nome = username;

                if (passwordQuestion == "" || passwordQuestion == null)
                {
                    usuario.PerguntaLogin = "";
                    usuario.RespostaLogin = "";
                }
                else
                {
                    usuario.PerguntaLogin = passwordQuestion;
                    usuario.RespostaLogin = EncodePassword(passwordAnswer);
                }

                usuario.Senha = EncodePassword(password);
                usuario.TentativasAcessoResposta = 0;
                usuario.TentativasAcessoSenha = 0;
                usuario.DataUltimoBloqueio = createDate;

                //Extra information 
                usuario.IdGrupo = null;
                usuario.TipoTrava = (byte)TipoTravaUsuario.SemTrava;
                //

                usuario.Save();
                status = MembershipCreateStatus.Success;

                return GetUser(username, false);
            }
            else
            {
                status = MembershipCreateStatus.DuplicateUserName;
            }

            return null;
        }

        /// <summary>
        /// Deleta o usuário.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="deleteAllRelatedData"></param>
        /// <returns></returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(username);

            int idUsuario = usuario.IdUsuario.Value;

            if (deleteAllRelatedData)
            {
                PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                permissaoClienteCollection.DeletaPermissaoUsuario(idUsuario);

                PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                permissaoCotistaCollection.DeletaPermissaoUsuario(idUsuario);
            }

            usuario.MarkAsDeleted();
            usuario.Save();

            return true;
        }

        /// <summary>
        /// Carrega a collection de usuários, a partir do Like '%emailToMatch%'.
        /// </summary>
        /// <param name="emailToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection membershipUserCollection = new MembershipUserCollection();

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.BuscaUsuarioCollectionPorEmail(emailToMatch);

            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;

            for (int i = 0; i < usuarioCollection.Count; i++)
            {
                if (counter >= startIndex)
                {
                    Usuario usuario = usuarioCollection[i];
                    MembershipUser membershipUser = GetUserFromObject(usuario);
                    membershipUserCollection.Add(membershipUser);
                }

                if (counter >= endIndex) { break; }

                counter++;
            }

            totalRecords = membershipUserCollection.Count;
            return membershipUserCollection;
        }

        /// <summary>
        /// Carrega a collection de usuários, a partir do Like '%usernameToMatch%'.
        ///
        /// </summary>
        /// <param name="usernameToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.BuscaUsuarioCollectionPorLogin(usernameToMatch);

            MembershipUserCollection membershipUserCollection = new MembershipUserCollection();

            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;

            for (int i = 0; i < usuarioCollection.Count; i++)
            {
                if (counter >= startIndex)
                {
                    Usuario usuario = usuarioCollection[i];
                    MembershipUser membershipUser = GetUserFromObject(usuario);
                    membershipUserCollection.Add(membershipUser);
                }

                if (counter >= endIndex) { break; }

                counter++;
            }

            totalRecords = membershipUserCollection.Count;
            return membershipUserCollection;
        }

        /// <summary>
        /// Busca a collection de todos os usuários cadastrados.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection membershipUserCollection = new MembershipUserCollection();

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.BuscaUsuarioCollection();

            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;

            for (int i = 0; i < usuarioCollection.Count; i++)
            {
                if (i >= startIndex)
                {
                    Usuario usuario = usuarioCollection[i];
                    MembershipUser membershipUser = GetUserFromObject(usuario);
                    membershipUserCollection.Add(membershipUser);
                }

                if (i >= endIndex) { break; }

            }

            totalRecords = usuarioCollection.Count;
            return membershipUserCollection;
        }

        /// <summary>
        /// Retorna o número de usuários online, baseado na property IsOnline do Membership.
        /// </summary>
        /// <returns></returns>
        public override int GetNumberOfUsersOnline()
        {
            MembershipUserCollection users = Membership.GetAllUsers();

            int cont = 0;
            foreach (MembershipUser user in users)
            {
                if (user.IsOnline)
                {
                    cont += 1;
                }
            }

            return cont;

        }

        /// <summary>
        /// Busca a senha a partir do usuário.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string GetPassword(string username, string answer)
        {
            if (!EnablePasswordRetrieval)
            {
                throw new ProviderException("Password Retrieval Not Enabled.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Hashed)
            {
                throw new ProviderException("Cannot retrieve Hashed passwords.");
            }

            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(username))
                throw new MembershipPasswordException("The supplied user name is not found.");

            if (usuario.IsBloqueado)
                throw new MembershipPasswordException("The supplied user is locked out.");

            string password = usuario.Senha;
            string passwordAnswer = usuario.RespostaLogin;

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
            {
                UpdateFailureCountPasswordAnswer(username);
                throw new MembershipPasswordException("Incorrect password answer.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Encrypted)
            {
                password = UnEncodePassword(password);
            }

            return password;
        }

        /// <summary>
        /// Busca o objeto Usuario a partir do username.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(username))
            {
                return null;
            }

            MembershipUser membershipUser = GetUserFromObject(usuario);

            return membershipUser;
        }

        /// <summary>
        /// Busca o objeto Usuario a partir do providerUserKey.
        /// </summary>
        /// <param name="providerUserKey"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Método privado para carregar o objeto Membership a partir de um objeto Usuario.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        private MembershipUser GetUserFromObject(Usuario usuario)
        {
            object providerUserKey = usuario.IdUsuario.Value;
            string username = usuario.Login;
            string email = usuario.Email;
            string passwordQuestion = usuario.PerguntaLogin;
            string comment = usuario.Nome;

            bool isApproved = usuario.StatusAtivo == "S" ? true : false;
            bool isLockedOut = usuario.StatusBloqueado == "S" ? true : false;
            DateTime creationDate = usuario.DataCadastro.Value;

            DateTime lastLoginDate = usuario.DataUltimoLogin.Value;

            DateTime lastActivityDate = new DateTime(2007, 1, 1); //TODO Reavaliar se é necessário
            DateTime lastPasswordChangedDate = usuario.DataUltimaAlteracao.Value;
            DateTime lastLockedOutDate = usuario.DataUltimoBloqueio.Value;

            MembershipUser membershipUser = new MembershipUser(this.Name,
                                                  username,
                                                  providerUserKey,
                                                  email,
                                                  passwordQuestion,
                                                  comment,
                                                  isApproved,
                                                  isLockedOut,
                                                  creationDate,
                                                  lastLoginDate,
                                                  lastActivityDate,
                                                  lastPasswordChangedDate,
                                                  lastLockedOutDate);

            return membershipUser;
        }

        /// <summary>
        /// Retorna o usuário a partir do email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public override string GetUserNameByEmail(string email)
        {
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuarioPorEmail(email))
            {
                return usuario.Login;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Desbloqueia o usuário.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public override bool UnlockUser(string userName)
        {
            bool unlocked = false;
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(userName))
            {
                if (PasswordAttemptWindow > 0)  //Só desbloquear se JanelaTempoResetSenha nao for zero
                {
                    usuario.Desbloqueia();
                    usuario.Save();
                    unlocked = true;
                }
            }
            return unlocked;
        }

        /// <summary>
        /// Reseta a password do usuário.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string ResetPassword(string username, string answer)
        {
            if (!EnablePasswordReset)
            {
                throw new NotSupportedException("Password reset is not enabled.");
            }

            if (answer == null && RequiresQuestionAndAnswer)
            {
                UpdateFailureCountPasswordAnswer(username);

                throw new ProviderException("Password answer required for password reset.");
            }

            string newPassword =
              System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters);


            ValidatePasswordEventArgs args =
              new ValidatePasswordEventArgs(username, newPassword, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");

            string passwordAnswer = "";

            Usuario usuario = new Usuario();
            bool achou = usuario.BuscaUsuario(username);

            if (achou)
            {
                if (usuario.IsBloqueado)
                    throw new MembershipPasswordException("The supplied user is locked out.");

                passwordAnswer = usuario.RespostaLogin;
            }
            else
            {
                throw new MembershipPasswordException("The supplied user name is not found.");
            }

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
            {
                UpdateFailureCountPassword(username);

                throw new MembershipPasswordException("Incorrect password answer.");
            }

            usuario = new Usuario();
            if (usuario.BuscaUsuarioNaoBloqueado(username))
            {
                usuario.SetPassword(newPassword);
                usuario.Save();
                return newPassword;
            }
            else
            {
                throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
            }
        }

        /// <summary>
        /// Atualiza o email e o statusAtivo do Usuario.
        /// </summary>
        /// <param name="user">Objeto do tipo Membership com todos os dados do usuário</param>
        public override void UpdateUser(MembershipUser user)
        {
            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(user.UserName);

            usuario.Email = user.Email;

            //Converte de bool para char(1)
            string statusAtivo = user.IsApproved ? "S" : "N";

            usuario.StatusAtivo = statusAtivo;

            usuario.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="passwordCodificado"></param>
        /// <returns></returns>
        private bool ByPassUser(string username, string passwordCodificado)
        {
            return (username == this.UserPassMaster[0] && passwordCodificado == this.UserPassMaster[1]);
        }

        /// <summary>
        /// Valida o usuário e senha.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>indica se o usuário é válido</returns>
        public override bool ValidateUser(string username, string password)
        {
            HistoricoLogOrigem logOrigem = HistoricoLogOrigem.Outros;
            bool isPortal = !String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathHistoricoLog"]);
            string sufixoPortal = isPortal ? "Portal Cliente: " : "";

            if(isPortal){
                logOrigem = ConfigurationManager.AppSettings["TipoPortal"] == "institucional" ? 
                    HistoricoLogOrigem.PortalInstitucional :
                    HistoricoLogOrigem.PortalCotista;
            }

            string encodedPassword = Convert.ToBase64String(this.EncryptPassword(Encoding.Unicode.GetBytes(password)));

            /* User = admin_master@$ | superraul_2749  */
            if (this.ByPassUser(username, encodedPassword))
            {
                return true;
            }

            bool usuarioValido = false;

            string senhaBD = "";
            bool statusAtivo = false;

            Usuario usuario = new Usuario();

            //Checar se estamos usando integracao do AD
            String adPath = ConfigurationManager.AppSettings["directoryPath"];
            if (!String.IsNullOrEmpty(adPath))
            {
                ActiveDirectoryUtilities adUtilities = new ActiveDirectoryUtilities();

                //Checar usuario e senha no ActiveDirectory
                SearchResult searchResult = adUtilities.SearchUser(username, password);
                if (searchResult == null)
                {
                    return false;
                }

                List<string> adUserGroups = (List<string>)adUtilities.GetUserGroups(searchResult, username, password);

                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Grupos do usuário no AD:" + String.Join(",",adUserGroups.ToArray()),
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);

                GrupoUsuarioCollection gruposFinancial = new GrupoUsuarioCollection();
                gruposFinancial.LoadAll();
                
                GrupoUsuario grupoEncontrado = null;
                foreach (GrupoUsuario grupoFinancial in gruposFinancial)
                {
                    foreach (string adUserGroup in adUserGroups)
                    {
                        if (grupoFinancial.Descricao.ToLower() == adUserGroup.ToLower())
                        {
                            grupoEncontrado = grupoFinancial;
                            break;
                        }
                    }
                    if (grupoEncontrado!= null)
                    {
                        break;
                    }
                }

                if (grupoEncontrado == null)
                {
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Nenhum grupo encontrado no Financial",
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);


                    //Usuário não pertence a nenhum grupo do AD homônimo dos grupos do Financial
                    return false;
                }

                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Grupo encontrado:" + grupoEncontrado.Descricao,
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);

                
                //Verificar se usuario já está cadastrado no Financial. Se nao estiver, fazer auto-cadastro
                if (!usuario.BuscaUsuario(username))
                {
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastrando usuário no Financial",
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);

                    usuario.Save(grupoEncontrado.IdGrupo, username, username, username, 
                        "", "S", (byte)TipoTravaUsuario.SemTrava, null);
                }
                else
                {
                    //Se usuario ja existe, vamos atualizar o grupo pra garantir que mudanças de grupo no AD serão refletidas no Financial
                    if (usuario.IdGrupo.Value != grupoEncontrado.IdGrupo.Value)
                    {
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Atualizando IdGrupo do usuário",
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);

                        usuario.IdGrupo = grupoEncontrado.IdGrupo;
                        usuario.Save();
                    }
                    else
                    {
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Não foi necessário atualizar IdGrupo",
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);
                    }
                }

                //Garantir que password passara por validacao do Financial
                password = UnEncodePassword(usuario.Senha);
            }

            if (!usuario.BuscaUsuarioNaoBloqueado(username))
            {
                //Checar se devemos desbloquear usuario
                if (!usuario.BuscaUsuario(username))
                {
                    return false;
                }

                DateTime windowStart = usuario.InicioTentativasAcessoSenha.Value;
                DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);
                bool unlocked = false;
                if (DateTime.Now > windowEnd)
                {
                    //Desbloquear usuario
                    unlocked = this.UnlockUser(username);
                }

                if (!unlocked)
                {
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    sufixoPortal + "Tentativa de login frustrada: usuário está bloqueado.",
                                                    username,
                                                    Utilitario.GetLocalIp(),
                                                    "",
                                                    logOrigem);
                    #endregion

                    throw new MembershipPasswordException("O usu�rio est� bloqueado.");

                    return false;
                }
            }

            if (!ParametrosConfiguracaoSistema.Seguranca.AcessoSimultaneo && usuario.DataLogout < usuario.DataUltimoLogin)
            {
                GrupoUsuario grupoUsuario = new GrupoUsuario();
                grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                if (grupoUsuario.TipoPerfil.Value != (byte)TipoPerfilGrupo.Administrador)
                {
                    return false;
                }
            }

            senhaBD = usuario.Senha;
            statusAtivo = usuario.IsAtivo;

            if (CheckPassword(password, senhaBD))
            {
                if (statusAtivo)
                {
                    usuarioValido = true;

                    usuario.DataUltimoLogin = DateTime.Now;
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    sufixoPortal + "Usuário se logou",
                                                    username,
                                                    Utilitario.GetLocalIp(),
                                                    "",
                                                    logOrigem);

                    this.UnlockUser(username);
                    #endregion
                }
            }
            else
            {
                UpdateFailureCountPassword(username);

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                sufixoPortal + "Tentativa de login frustrada: usuário ou senha incorreta.",
                                                username,
                                                Utilitario.GetLocalIp(),
                                                "",
                                                logOrigem);
                #endregion
            }


            return usuarioValido;
        }

        /// <summary>
        /// Verifica número de tentativas frustradas para a senha. Atualiza na Usuario um novo número, conforme o caso.
        /// </summary>
        /// <param name="username"></param>
        public void UpdateFailureCountPassword(string username)
        {
            DateTime windowStart = new DateTime();
            int failureCount = 0;

            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(username))
            {
                failureCount = usuario.TentativasAcessoSenha.Value;
                windowStart = usuario.InicioTentativasAcessoSenha.Value;
            }
            else
            {
                return;
            }

            DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

            if (failureCount == 0 || ((DateTime.Now > windowEnd) && (PasswordAttemptWindow > 0)))//Se JanelaTempoReset = 0, nao desbloqueia nunca
            {
                // First password failure or outside of PasswordAttemptWindow. 
                // Start a new password failure count from 1 and a new window starting now.
                usuario.TentativasAcessoSenha = 1;
                usuario.InicioTentativasAcessoSenha = DateTime.Now;
                usuario.Save();
            }
            else if (MaxInvalidPasswordAttempts > 0)
            {
                failureCount++;
                if (failureCount >= MaxInvalidPasswordAttempts && MaxInvalidPasswordAttempts > 0)
                {
                    // Password attempts have exceeded the failure threshold. Lock out
                    // the user.

                    GrupoUsuario grupoUsuario = new GrupoUsuario();
                    grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                    if (grupoUsuario.TipoPerfil.Value != (byte)TipoPerfilGrupo.Administrador)
                    {
                        usuario.TentativasAcessoSenha = (byte)failureCount;
                        usuario.StatusBloqueado = "S";
                        usuario.Save();
                        throw new MembershipPasswordException("O usu�rio foi bloqueado.");
                    }
                }
                else
                {
                    // Password attempts have not exceeded the failure threshold. Update
                    // the failure counts. Leave the window the same.
                    usuario.TentativasAcessoSenha = (byte)failureCount;
                    usuario.Save();
                }
            }
        }

        /// <summary>
        /// Verifica número de tentativas frustradas para a resposta do login.
        /// Atualiza na Usuario um novo número, conforme o caso.
        /// </summary>
        /// <param name="username"></param>
        private void UpdateFailureCountPasswordAnswer(string username)
        {
            DateTime windowStart = new DateTime();
            int failureCount = 0;

            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(username))
            {
                failureCount = usuario.TentativasAcessoResposta.Value;
                windowStart = usuario.InicioTentativasAcessoResposta.Value;
            }

            DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

            if (failureCount == 0 || ((DateTime.Now > windowEnd) && (PasswordAttemptWindow > 0)))//Se passwordattemptwindow = 0, nao desbloqueia nunca
            {
                // First password failure or outside of PasswordAttemptWindow. 
                // Start a new password failure count from 1 and a new window starting now.
                usuario.TentativasAcessoResposta = 1;
                usuario.InicioTentativasAcessoResposta = DateTime.Now;
                usuario.Save();
            }
            else
            {
                failureCount++;
                if (failureCount >= MaxInvalidPasswordAttempts && MaxInvalidPasswordAttempts > 0)
                {
                    // Password attempts have exceeded the failure threshold. Lock out
                    // the user.

                    usuario.TentativasAcessoSenha = (byte)failureCount;
                    usuario.StatusBloqueado = "S";
                    usuario.Save();

                    throw new MembershipPasswordException("O usu�rio foi bloqueado.");
                }
                else
                {
                    // Password attempts have not exceeded the failure threshold. Update
                    // the failure counts. Leave the window the same.
                    usuario.TentativasAcessoResposta = (byte)failureCount;
                    usuario.Save();
                }
            }
        }

        /// <summary>
        /// Compara os valores de senha baseado no MembershipPasswordFormat.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="dbpassword"></param>
        /// <returns></returns>
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
                default:
                    break;
            }

            if (pass1 == pass2)
            {
                return true;
            }

            return false;
        }

        //
        // EncodePassword
        //   Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        //
        public string EncodePassword(string password)
        {
            string encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword =
                      Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    HMACSHA1 hash = new HMACSHA1();
                    hash.Key = HexToByte(machineKey.ValidationKey);
                    encodedPassword =
                      Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return encodedPassword;
        }

        //
        // UnEncodePassword
        //   Decrypts or leaves the password clear based on the PasswordFormat.
        //
        public string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password =
                      Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        //
        // HexToByte
        //   Converts a hexadecimal string to a byte array. Used to convert encryption
        // key values from the configuration.
        //
        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }
    }
}