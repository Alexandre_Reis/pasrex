﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Security.Exceptions;
using log4net;
using Financial.Investidor;
using Financial.Investidor.Exceptions;
using Financial.Investidor.Enums;
using Financial.Security.Enums;
using Financial.InvestidorCotista;

namespace Financial.Security
{
	public partial class PermissaoCliente : esPermissaoCliente
	{
        /// <summary>
        /// Retorna um boolean indicando se o login tem acesso ao idCliente passado.
        /// Se o cliente for do TipoControle = ApenasCotacao, retorna true!
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="login"></param>  
        public bool RetornaAcessoCliente(int idCliente, string login)
        {
            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Query.Select(usuarioCollection.Query.IdUsuario);
            usuarioCollection.Query.Where(usuarioCollection.Query.Login.Equal(login));
            if (!usuarioCollection.Query.Load())
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            int idUsuario = usuarioCollection[0].IdUsuario.Value;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            
            if (!cliente.LoadByPrimaryKey(campos, idCliente))
                throw new InvestidorException("Cliente " + idCliente + " não cadastrado.");
            if (cliente.TipoControle == (byte)TipoControleCliente.ApenasCotacao)
            {
                return true;
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCliente)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdUsuario == idUsuario);

            return this.Query.Load();                        
        }

        /// <summary>
        /// Retorna um boolean indicando se o login tem acesso ao idCliente passado.
        /// Se o cliente for do TipoControle = ApenasCotacao, retorna true!
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="login"></param>  
        public bool RetornaAcessoClienteComControle(int idCliente, string login)
        {
            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Query.Select(usuarioCollection.Query.IdUsuario);
            usuarioCollection.Query.Where(usuarioCollection.Query.Login.Equal(login));
            if (!usuarioCollection.Query.Load())
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            int idUsuario = usuarioCollection[0].IdUsuario.Value;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);

            if (!cliente.LoadByPrimaryKey(campos, idCliente))
                throw new InvestidorException("Cliente " + idCliente + " não cadastrado.");
            if (cliente.TipoControle == (byte)TipoControleCliente.ApenasCotacao)
            {
                return false;
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCliente)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdUsuario == idUsuario);

            return this.Query.Load();
        }

        /// <summary>
        /// Retorna o Id do cliente associado ao Usuario/Login.
        /// Como ele busca na collection, traz o 1o da lista.
        /// 
        /// Obs: Se não tiver cliente associado ao usuário, retorna Null.
        /// </summary>
        /// <param name="login"></param>  
        public int? RetornaClienteAssociado(string login)
        {
            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(login)) {
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            }
                        
            if (usuario.IdCliente.HasValue)
            {
                return usuario.IdCliente;
            }
            
            int idUsuario = usuario.IdUsuario.Value;
            PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();

            permissaoClienteCollection.Query
                 .Select(permissaoClienteCollection.Query.IdCliente)
                 .Where(permissaoClienteCollection.Query.IdUsuario == idUsuario);

            permissaoClienteCollection.Query.Load();

            return (!permissaoClienteCollection.HasData)
                    ? null
                    : ((PermissaoCliente)permissaoClienteCollection[0]).IdCliente;
        }

        /// <summary>
        /// Retorna o id do usuário vinculado ao idCliente passado, considerando TipoTrava = TravaCliente.
        /// Se não tiver usuário vinculado, retorna 0. Se tiver mais de 1 usuário vinculado, retorna o 1o da lista.
        /// </summary>
        /// <param name="idCliente"></param>  
        public int RetornaIdUsuarioTravaCliente(int idCliente)
        {
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            permissaoClienteQuery.Select(permissaoClienteQuery.IdUsuario);
            permissaoClienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);
            permissaoClienteQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCliente));
            permissaoClienteQuery.Where(permissaoClienteQuery.IdCliente.Equal(idCliente));

            PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
            permissaoClienteCollection.Load(permissaoClienteQuery);

            int idClienteRetorno = 0;

            if (permissaoClienteCollection.Count > 0)
            {
                idClienteRetorno = permissaoClienteCollection[0].IdUsuario.Value;
            }

            return idClienteRetorno;
        }

        public void Save(int idCliente, int idUsuario)
        {
            this.IdCliente = idCliente;
            this.IdUsuario = idUsuario;
            base.Save();
        }

        /// <summary>
        /// Verifica as carteiras associadas ao cotista (em PosicaoCotitaAbertura e OperacaoCotista) e gera PermissaoCliente para o idUsuario nestas carteiras.
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idCotista"></param>
        public void GeraPermissaoCarteira(int idUsuario, int idCotista) {
            CotistaCollection cotistaCollection = new CotistaCollection();
            List<int> idsCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista);

            PermissaoClienteCollection p = new PermissaoClienteCollection();
            //
            foreach (int idCarteira in idsCarteira) {
                                
                if (!new PermissaoCliente().LoadByPrimaryKey(idUsuario, idCarteira)) {
                    
                    PermissaoCliente permissaoCliente = p.AddNew();
                    permissaoCliente.IdCliente = idCarteira;
                    permissaoCliente.IdUsuario = idUsuario;                    
                }
            }

            p.Save();
        }        
	}
}