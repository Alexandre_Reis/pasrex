﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections;
using System.Configuration;

namespace Financial.Security
{
    public class ActiveDirectoryUtilities
    {
        private readonly Dictionary<string, bool> _isGroup = new Dictionary<string, bool>();

        private readonly object _lock = new object();
        private readonly Dictionary<string, SearchResult> _searchUser = new Dictionary<string, SearchResult>();

        ///<sumary>
        ///     Cria nova instancia
        ///</summary>
        public ActiveDirectoryUtilities()
        {
            MemberOfProperty = "memberof";
            MembersProperty = "member";

            //UserFilterTemplate = @"(userprincipalname={user}@{domainFromAD});
            UserFilterTemplate = @"(samaccountname={0})";
            UserDisplayNameProperty = "displayname";
            UserEmailAddressProperty = "mail";

            GroupFilterTemplate = @"(&(objectCategory=group)(SAMAccountName={0}))";
            GroupMemberFilterTemplate = @"(distinguishedName={0})";
            GroupMemberNameProperty = "samaccountname";
        }

        ///<sumary>
        ///     Propriedade do ActiveDirectory que indica os grupos aos quais um usuário/grupo pertence
        ///</summary>
        public string MemberOfProperty;

        ///<sumary>
        ///     Propriedade do ActiveDirectory que indica os membros de um grupo
        ///</summary>
        public string MembersProperty;

        ///<sumary>
        ///     Template parqa busca de usuário
        ///</summary>
        public string UserFilterTemplate;

        ///<sumary>
        ///     Propriedade do ActiveDirectory que indica o nome completo de um usuário
        ///</summary>
        public string UserDisplayNameProperty;

        ///<sumary>
        ///     Propriedade do ActiveDirectory que indica o endereço de e-mail de um usuário
        ///</summary>
        public string UserEmailAddressProperty;

        ///<sumary>
        ///     Template para busca de grupo
        ///</summary>
        public string GroupFilterTemplate;

        ///<sumary>
        ///     Template para busca de membro de grupo
        ///</summary>
        public string GroupMemberFilterTemplate;

        ///<sumary>
        ///     Propriedade do ActiveDirectory que indica o nome de um membro
        ///</summary>
        public string GroupMemberNameProperty;

        ///<summary>
        /// Obtém uma lista dos grupos dos quais o usuário é membro
        /// A propriedade procurada no ActiveDirectory é o valor de 'MemberOfProperty'
        ///</summary>
        ///<param name="user">Nome do usuário no formato 'domínio\usuário'</param>
        ///<returns>Lista dos grupos dos quais o usuário é membro</returns>
        public IEnumerable<string> GetUserGroups(SearchResult searchResult, string username, string pwd)
        {
            //SearchResult searchResult = SearchUser(user, new string[] { MemberOfProperty });

            if (!searchResult.Properties.Contains(MemberOfProperty))
            {
                throw new ApplicationException(
                    string.Format("Propriedade '{0}' não pertence ao resultado da pesquisa de grupos do usuário",
                    MemberOfProperty));
            }

            List<string> groups = new List<string>();
            foreach (string description in searchResult.Properties[MemberOfProperty])
            {
                Match m = Regex.Match(description, @"CN=([^,]+)");
                if (m.Success)
                {
                    string group = m.Groups[1].Value;
                    //Por ora nao vamos encontrar grupos superiores
                    foreach (string groupGroup in GetGroupParents(group, groups, username, pwd))
                    {
                        if (!groups.Contains(groupGroup))
                        {
                            groups.Add(groupGroup);
                        }
                    }

                    if (!groups.Contains(group))
                    {
                        groups.Add(group);
                    }
                }
            }
            return groups;
        }

        ///<sumary>
        ///     Obtém o nome completo do usuário
        ///     A propriedade procurada no ActiveDirectory é o valor de 'UserDisplayNameProperty'
        ///</summary>
        ///<param name="user">Nome do usuário no formato 'domínio\usuário'</param>
        ///<returns>Nome completo do usuário</returns>
        /*public string GetUserName(string user)
        {
            SearchResult searchResult = SearchUser(user, new string[] { UserDisplayNameProperty });

            if (!searchResult.Properties.Contains(UserDisplayNameProperty))
            {
                throw new ApplicationException(
                    string.Format("Propriedade '{0}' não pertence ao resultado da pesquisa de nome do usuário '{1}'",
                    UserDisplayNameProperty, user));
            }

            if (searchResult.Properties[UserDisplayNameProperty].Count <= 0)
            {
                throw new ApplicationException(
                    string.Format("Pesquisa sobre o nome do usuário '{0}' não retornou nenhum resultado", user));
            }

            return (string)searchResult.Properties[UserDisplayNameProperty][0];
        }*/

        ///<sumary>
        ///     Obtém o endereco de e-mail do usuário
        ///     A propriedade procurada no ActiveDirectory é o valor de 'UserEmailAddressProperty'
        ///</summary>
        ///<param name="user">Nome do usuário no formato 'domínio\usuário'</param>
        ///<returns>Endereco de e-mail do usuário</returns>
        /*public string GetUserEmailAddress(string user)
        {
            SearchResult searchResult = SearchUser(user, new string[] { UserEmailAddressProperty });

            if (!searchResult.Properties.Contains(UserEmailAddressProperty))
            {
                throw new ApplicationException(
                    string.Format("Propriedade '{0}' não pertence ao resultado da pesquisa de e-mail do usuário '{1}'",
                    UserEmailAddressProperty, user));
                return string.Empty;
            }

            if (searchResult.Properties[UserEmailAddressProperty].Count <= 0)
            {
                throw new ApplicationException(
                    string.Format("Pesquisa sobre o e-mail do usuário '{0}' n~ao retornou nenhum resultado",
                    user));
                return string.Empty;
            }

            return (string)searchResult.Properties[UserEmailAddressProperty][0];
        }*/

        ///<sumary>
        ///     Obtém uma lista de membros do grupo
        ///     O valor da propriedade 'GroupFilterTemplate' com o placeholder 'groupName' é usado para busca de grupo
        ///     A propriedade procurada do ActiveDirectory é o valor de 'MembersProperty'
        ///     O valor da propriedade 'GroupMemberFilterTemplate' com o placeholder 'memberName' é usado para busca de membro
        ///     A propriedade procurada no ActiveDirectory é o valor de 'GroupMemberNameProperty'
        ///</summary>
        ///<param name="group">Nome do grupo'</param>
        ///<returns>Lista com os membros do grupo no formato 'dominio\usuario'</returns>
        public IEnumerable<string> GetGroupMembers(string group)
        {
            List<string> members = new List<string>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            using (Domain currentDomain = Domain.GetCurrentDomain())
            {
                string filter = String.Format(GroupFilterTemplate, group);

                string netbiosName = DomainExtensions.GetNetbiosName(Domain.GetCurrentDomain());

                using (DirectoryEntry directoryEntry = currentDomain.GetDirectoryEntry())
                {
                    using (DirectorySearcher search = new DirectorySearcher(directoryEntry, filter, new string[] { MembersProperty }, SearchScope.Subtree)
                    )
                    {
                        search.ReferralChasing = ReferralChasingOption.All;
                        SearchResult result = search.FindOne();
                        if (result == null)
                        {
                            return new List<string>();
                        }

                        if (!result.Properties.Contains(MembersProperty))
                        {
                            return new List<string>();
                        }

                        foreach (string member in result.Properties[MembersProperty])
                        {
                            //GroupMemberFilterTemplate = @"(distinguishedName={memberName})";
                            string memberFilter = String.Format(GroupMemberFilterTemplate, member);

                            using (DirectorySearcher memberSearch = new DirectorySearcher(directoryEntry, memberFilter,
                                new string[] { GroupMemberNameProperty }, SearchScope.Subtree))
                            {
                                //nessa secao nenhuma excecao eh lancada
                                //dessa forma evitamos que um unico membro possa comprometer o resultado final

                                SearchResult memberResult = memberSearch.FindOne();
                                if (memberResult == null)
                                {
                                    continue;
                                }

                                if (!memberResult.Properties.Contains(GroupMemberNameProperty))
                                {
                                    continue;
                                }

                                if (memberResult.Properties[GroupMemberNameProperty].Count <= 0)
                                {
                                    continue;
                                }

                                string item = memberResult.Properties[GroupMemberNameProperty][0].ToString();

                                if (IsGroup(item))
                                {
                                    foreach (string itemMember in GetGroupMembers(item))
                                    {
                                        members.Add(itemMember);
                                    }
                                }
                                else
                                {
                                    members.Add(netbiosName + "\\" + item);
                                }
                            }
                        }
                    }
                }
            }

            return members;
        }

        ///<sumary>
        ///     Verifica se <paramref name="name" /> eh um grupo
        ///</summary>
        public bool IsGroup(string name)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            lock (_lock)
            {
                bool isGroup;
                if (_isGroup.TryGetValue(name, out isGroup))
                {
                    return isGroup;
                }

                using (Domain currentDomain = Domain.GetCurrentDomain())
                {
                    string itemFilter = String.Format(GroupFilterTemplate, name);

                    using (DirectoryEntry directoryEntry = currentDomain.GetDirectoryEntry())
                    {
                        using (DirectorySearcher checkIfGroup = new DirectorySearcher(directoryEntry, itemFilter,
                            null, SearchScope.Subtree)
                            )
                        {

                            checkIfGroup.ReferralChasing = ReferralChasingOption.All;

                            SearchResult resultIfGroup = checkIfGroup.FindOne();
                            isGroup = resultIfGroup != null;
                            _isGroup.Add(name, isGroup);

                            return isGroup;
                        }
                    }
                }
            }
        }

        ///<sumary>
        ///     Faz a busca de todos os elementos do qual um grupo faz parte.
        ///     O valor da propriedade 'GroupFilterTemplate' com os placeholders 'groupName' é usado na busca
        ///</summary>
        ///<param name="group">Nome do grupo'</param>
        ///<param name="groupsAlreadyChecked">Guarda os grupos que ja foram checados para nao procurar 2 vezes</param>
        ///<returns>Resultado da busca</returns>
        private IEnumerable<string> GetGroupParents(string group, List<string>
            groupsAlreadyChecked, string username, string pwd)
        {
            if (groupsAlreadyChecked.Contains(group))
            {
                return new List<string>();
            }
            groupsAlreadyChecked.Add(group);

            Stopwatch sw = new Stopwatch();
            sw.Start();


            string _path = ConfigurationManager.AppSettings["directoryPath"];
            string domain = ConfigurationManager.AppSettings["directoryDomain"];

            String domainAndUsername = domain + @"\" + username;
            //DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            string filter = String.Format(GroupFilterTemplate, group);
            //using (DirectoryEntry dictionaryEntry = currentDomain.GetDirectoryEntry())
            using (DirectoryEntry dictionaryEntry = new DirectoryEntry(_path, domainAndUsername, pwd)) 
            {
                string[] properties = new string[] { MemberOfProperty };
                using (DirectorySearcher directorySearcher = new DirectorySearcher(dictionaryEntry,
                    filter, properties, SearchScope.Subtree))
                {
                    SearchResult searchResult = directorySearcher.FindOne();
                    if (searchResult == null)
                    {
                        throw new ApplicationException(string.Format(
                            "Erro na execucao da seguinte pesqusa Active Directory: {0}", filter));
                    }

                    List<string> groupParents = new List<string>();
                    foreach (string property in properties)
                    {
                        if (!searchResult.Properties.Contains(property)) continue;

                        foreach (string description in searchResult.Properties[property])
                        {
                            Match m = Regex.Match(description, @"CN=([^,]+)");
                            if (m.Success)
                            {
                                string item = m.Groups[1].Value;

                                groupParents.Add(item);
                                if (IsGroup(item))
                                {
                                    foreach (string itemGroup in GetGroupParents(item, groupsAlreadyChecked, username, pwd))
                                    {
                                        groupParents.Add(itemGroup);
                                    }
                                }
                            }
                        }
                    }
                    return groupParents;
                }
            }
        }

        ///<sumary>
        ///     Faz a busca por um usuario
        ///     O valor da propriedade 'userFilterTemplate' com os placeholders 'user', 
        ///     'domainFromUser', 'domainFromAD' e 'domainFromForest' é usado na busca
        ///</summary>
        ///<param name="user">Nome do usuario no formato 'dominio\usuario'</param>
        ///<param name="properties">Lista de propriedades que devem ser obtidas do AdtiveDirectory</param>
        ///<returns>Resultado da busca</returns>
        protected SearchResult SearchUserSafra(string user, string[] properties)
        {
            Match match = Regex.Match(user, @"(,+)\\(,+)");
            if (!match.Success)
            {
                throw new Exception("Usuário inválido: " + user);
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();

            lock (_lock)
            {
                string key = string.Format("U:{0};Ps:{1})", user, string.Join(",", properties));
                SearchResult searchResult;
                if (_searchUser.TryGetValue(key, out searchResult))
                {
                    return searchResult;
                }

                using (Domain currentDomain = Domain.GetCurrentDomain())
                {
                    string filter = String.Format(UserFilterTemplate, match.Groups[2].Value);

                    using (DirectoryEntry directoryEntry = currentDomain.GetDirectoryEntry())
                    {
                        using (DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry, filter, properties, SearchScope.Subtree))
                        {
                            searchResult = directorySearcher.FindOne();
                            if (searchResult == null)
                            {
                                throw new ApplicationException("Erro na execução da seguinte pesquisa Active Directory:" + filter);
                            }

                            _searchUser.Add(key, searchResult);

                            return searchResult;
                        }
                    }
                }
            }
        }

        public SearchResult SearchUser(String username, String pwd)
        {
            string _filterAttribute;
            string _path = ConfigurationManager.AppSettings["directoryPath"];
            string domain = ConfigurationManager.AppSettings["directoryDomain"];

            String domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);
            SearchResult result = null;
            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("memberof");
                result = search.FindOne();
            }
            catch
            {

            }
            return result;
        }
    }

    /// <summary>
    /// Defines extensios made to the <see cref="Domain"/> class.
    /// </summary>
    /// <remarks>
    /// Retirado de http://blogs.edwardwilde.com/2009/12/14/lookup-the-netbios-name-of-a-
    /// domain-using-directory-services/
    /// </remarks>
    public static class DomainExtensions
    {
        ///<summary>
        ///Retorna o Nome Netbios de um dominio
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string GetNetbiosName(Domain domain)
        {

            // Bind to RootDSE and grab the configuration naming context
            DirectoryEntry rootDSE = new DirectoryEntry(@"LDAP://RootDSE");
            DirectoryEntry partitions =
                new DirectoryEntry(@"LDAP://cn=Partitions," +
                rootDSE.Properties["configurationNamingContext"].Value);

            DirectoryEntry domainEntry = domain.GetDirectoryEntry();

            //Iterate through the cross references collection in the Partitions container
            DirectorySearcher directorySearcher = new DirectorySearcher(partitions);
            directorySearcher.Filter = "(&(objectCategory=crossRef)(ncName=" +
                         domainEntry.Path
                             .Replace("LDAP://", string.Empty)
                             .Replace(domain.Name + "/", string.Empty) + "))";

            directorySearcher.SearchScope = SearchScope.Subtree;

            directorySearcher.PropertiesToLoad.Add("nETBIOSName");

            //Display result (should only be one)
            SearchResultCollection results = directorySearcher.FindAll();
            if (results.Count == 0)
            {
                return null;
            }

            return results[0].Properties["nETBIOSName"][0].ToString();
        }
    }
}



