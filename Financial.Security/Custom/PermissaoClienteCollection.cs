﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Security
{
	public partial class PermissaoClienteCollection : esPermissaoClienteCollection
	{
        /// <summary>
        /// Deleta todas as permissões associadas ao usuário.
        /// </summary>
        /// <param name="idUsuario"></param>                
        public void DeletaPermissaoUsuario(int idUsuario)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdUsuario == idUsuario);
            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
