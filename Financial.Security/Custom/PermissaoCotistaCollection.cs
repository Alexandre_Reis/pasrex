﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Security.Exceptions;

namespace Financial.Security
{
	public partial class PermissaoCotistaCollection : esPermissaoCotistaCollection
	{
        /// <summary>
        /// Deleta todas as permissões associadas ao usuário.
        /// </summary>
        /// <param name="idUsuario"></param>                
        public void DeletaPermissaoUsuario(int idUsuario)
        {            
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdUsuario == idUsuario);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();            
        }

        public void BuscaCotistasAssociados(string login)
        {
            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(login))
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            int idUsuario = usuario.IdUsuario.Value;

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCotista)
                 .Where(this.Query.IdUsuario.Equal(idUsuario));

            this.Query.Load();
        }
	}
}
