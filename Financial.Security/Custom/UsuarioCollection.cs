using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Security
{
	public partial class UsuarioCollection : esUsuarioCollection
	{
        /// <summary>
        /// Carrega o objeto com todos os campos de UsuarioCollection. Ordenado por Login.
        /// </summary>        
        public void BuscaUsuarioCollection()
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .OrderBy(this.Query.Login.Ascending);

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de UsuarioCollection. Ordenado por Login.
        /// Filtrado por Like(login).
        /// </summary>        
        /// <param name="login"></param>
        public void BuscaUsuarioCollectionPorLogin(string login)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Login.Like(login))
                 .OrderBy(this.Query.Login.Ascending);

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de UsuarioCollection. Ordenado por Email.
        /// Filtrado por Like(email).
        /// </summary>        
        /// <param name="email"></param>
        public void BuscaUsuarioCollectionPorEmail(string email)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Email.Like(email))
                 .OrderBy(this.Query.Email.Ascending);

            this.Query.Load();            
        }

	}
}
