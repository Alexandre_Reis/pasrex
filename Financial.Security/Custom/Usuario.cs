﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Security.Enums;
using Financial.Util;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Configuration;
using System.Net.Mail;
using HtmlAgilityPack;
using Financial.Investidor;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.Fundo;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace Financial.Security
{
    public partial class Usuario : esUsuario
    {
        /// <summary>
        /// True se bloqueado. False se desbloqueado.
        /// </summary>
        public bool IsBloqueado
        {
            get { return this.StatusBloqueado == "S"; }
        }

        /// <summary>
        /// True se ativo. False se inativo.
        /// </summary>
        public bool IsAtivo
        {
            get { return this.StatusAtivo == "S"; }
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de Usuario.
        /// </summary>
        /// <param name="email"></param>        
        /// <returns>bool indicando se achou registro</returns>
        public bool BuscaUsuarioPorEmail(string email)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Email.Equal(email));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de Usuario.
        /// </summary>
        /// <param name="login"></param>        
        /// <returns>bool indicando se achou registro</returns>
        public bool BuscaUsuario(string login)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Login.Equal(login));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de Usuario.
        /// </summary>
        /// <param name="login"></param>
        /// <param name="senha"></param>
        /// <returns>bool indicando se achou registro</returns>
        public bool BuscaUsuario(string login, string senha)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Login.Equal(login),
                        this.Query.Senha.Equal(senha));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de Usuario.
        /// Filtra apenas usuários não bloqueados.
        /// </summary>
        /// <param name="login"></param>
        /// <returns>bool indicando se achou registro</returns>
        public bool BuscaUsuarioNaoBloqueado(string login)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.Login.Equal(login),
                        this.Query.StatusBloqueado.Equal("N"));

            bool retorno = this.Query.Load();

            return retorno;
        }

        public bool ChangePassword(int idUsuario, string newPwd, string newPwdConfirm, string oldPwd)
        {
            if (newPwd != newPwdConfirm)
            {
                throw new Exception("Senha e confirmação de senha não conferem");
            }
            return this.ChangePassword(idUsuario, newPwd, oldPwd);
        }

        //ChangePassword é a troca de senha
        public bool ChangePassword(int idUsuario, string newPwd, string oldPwd)
        {
            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            financialMembershipProvider.Initialize();

            if (string.IsNullOrEmpty(oldPwd))
            {
                oldPwd = financialMembershipProvider.UnEncodePassword(this.Senha);
            }

            if (!financialMembershipProvider.ValidatePassword(oldPwd, newPwd))
            {
                return false;
            }

            if (!financialMembershipProvider.ChecaHistoricoSenha(idUsuario, financialMembershipProvider.EncodePassword(newPwd)))
            {
                return false;
            }

            this.SetPassword(newPwd);

            this.TrocaSenha = "N";
            this.DataUltimaAlteracao = DateTime.Now;
            this.Save();

            HistoricoSenhas historicoSenhas = new HistoricoSenhas();
            historicoSenhas.IdUsuario = idUsuario;
            historicoSenhas.Senha = financialMembershipProvider.EncodePassword(newPwd);
            historicoSenhas.Data = DateTime.Now;
            historicoSenhas.Save();

            return true;
        }

        private string EncodePassword(string pwd)
        {
            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            financialMembershipProvider.Initialize();
            return financialMembershipProvider.EncodePassword(pwd);
        }

        public void SetPassword(string newPwd)
        {
            this.Senha = this.EncodePassword(newPwd);
            this.Desbloqueia();
        }

        /*public override void Save()
        {
            if (!this.es.IsDeleted)
            {
                this.DataUltimaAlteracao = DateTime.Now;
            }

            base.Save();
        }*/

        public void Desbloqueia()//Libera o bloqueio por timeout
        {
            this.TentativasAcessoSenha = 0;
            this.InicioTentativasAcessoSenha = DateTime.Now;
            this.StatusBloqueado = "N";
            this.TentativasAcessoResposta = 0;
            this.InicioTentativasAcessoResposta = DateTime.Now;

            //Vamos sempre setar esta data de logout para recuperar os usuários com sessões simultâneas que não clicaram o botão "sair" antes de fechar o navegador
            this.DataLogout = DateTime.Now;

        }

        private void RemovePermissaoCliente(int idCliente)
        {
            PermissaoCliente permissaoCliente = new PermissaoCliente();
            if (permissaoCliente.LoadByPrimaryKey(this.IdUsuario.Value, idCliente))
            {
                permissaoCliente.MarkAsDeleted();
                permissaoCliente.Save();
            }
        }

        private void AdicionaPermissaoCliente(int idCliente)
        {
            PermissaoCliente permissaoCliente = new PermissaoCliente();
            if (!permissaoCliente.LoadByPrimaryKey(this.IdUsuario.Value, idCliente))
            {
                permissaoCliente.Save(idCliente, this.IdUsuario.Value);
            }
        }

        public bool AcessaCliente(int idCliente)
        {
            PermissaoCliente permissaoCliente = new PermissaoCliente();
            return permissaoCliente.LoadByPrimaryKey(IdUsuario.Value, idCliente);
        }

        public void AdicionaPermissaoCotistas(int idCarteira)
        {
            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                List<int> idsCotistas = carteira.GetIdsCotistas();
                foreach (int idCotista in idsCotistas)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    //Em termos de performance é melhor tentarmos inserir e dar catch no erro do que ficar fazendo lookup
                    try
                    {
                        permissaoCotista.IdCotista = idCotista;
                        permissaoCotista.IdUsuario = this.IdUsuario.Value;
                        permissaoCotista.Save();
                    }
                    catch { }
                }
            }
        }

        public void Save(int? idGrupo, string nome, string login, string senha, string email,
                                string statusAtivo, byte tipoTrava, int? idCliente)
        {
            if (idGrupo != null)
            {
                this.IdGrupo = idGrupo;
            }

            if (!String.IsNullOrEmpty(senha))
            {
                this.SetPassword(senha);

                if (ParametrosConfiguracaoSistema.Seguranca.TempoExpiracaoSenha > 0)
                {
                    //Sempre que uma nova senha for informada pelo administrador vamos pedir troca no próximo login
                    this.TrocaSenha = "S";
                }
                else
                {
                    this.TrocaSenha = "N";
                }
            }

            this.Nome = nome;
            this.Login = login;
            this.Email = email;
            this.StatusAtivo = statusAtivo;
            this.TipoTrava = tipoTrava;
            this.AcessaWin = "S";

            //Guardar IdCliente antigo para comparação após Save
            int? idClienteAntigo = this.IdCliente;
            this.IdCliente = idCliente;

            bool insert = !this.IdUsuario.HasValue;

            if (insert)
            {
                this.DataUltimoLogin = DateTime.Now;
                this.DataCadastro = DateTime.Now;
                this.DataUltimaAlteracao = DateTime.Now;
                this.DataUltimoBloqueio = DateTime.Now;
                this.DataLogout = DateTime.Now;

                //Forçados        
                this.DataValidade = new DateTime(2100, 01, 01);
                this.PerguntaLogin = "";
                this.RespostaLogin = "";
            }

            this.Save();

            if (insert)
            {
                HistoricoSenhas historicoSenhas = new HistoricoSenhas();
                historicoSenhas.IdUsuario = this.IdUsuario.Value;
                historicoSenhas.Senha = this.Senha;
                historicoSenhas.Data = DateTime.Now;
                historicoSenhas.Save();
            }

            #region Checar se houve alteracao do IdCliente

            if (idClienteAntigo != idCliente)
            {
                //Adicionar novo cliente default se preenchido
                if (idCliente != null)
                {
                    this.AdicionaPermissaoCliente(idCliente.Value);
                }
            }
            #endregion
        }

        public bool AcessaCarteiraSimulada()
        {
            GrupoUsuario grupoUsuario = this.UpToGrupoUsuarioByIdGrupo;
            return grupoUsuario.CarteiraSimulada == "S";
        }

        /// <summary>
        /// Cria novo usuário.
        /// Informações não tratadas (que fazem parte do membership):
        ///     PerguntaLogin; RespostaLogin; DataValidade; 
        /// </summary>
        /// <param name="idGrupo"></param>
        /// <param name="nome"></param>
        /// <param name="login"></param>
        /// <param name="senha"></param>
        /// <param name="email"></param>
        /// <param name="statusAtivo"></param>
        /// <param name="tipoTrava"></param>
        /// <returns></returns>
        /* public void CriaUsuario(int? idGrupo, string nome, string login, string senha, string email,
                                 string statusAtivo, byte tipoTrava)
         {
             if (!this.BuscaUsuario(login))
             {
                 FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();

                 if (idGrupo.HasValue)
                 {
                     this.IdGrupo = idGrupo.Value;
                 }
                
                 this.Nome = nome;
                 this.Login = login;
                 this.Senha = financialMembershipProvider.EncodePassword(senha);
                 this.Email = email;
                 this.StatusAtivo = statusAtivo;
                 this.StatusBloqueado = "N";
                 this.PerguntaLogin = ""; //Não tratado!
                 this.RespostaLogin = ""; //Não tratado!                
                 this.DataUltimaAlteracao = DateTime.Now;
                 this.DataValidade = new DateTime(2300, 01, 01); //Não tratado!                
                 this.DataCadastro = DateTime.Now;                
                 this.DataUltimoLogin = DateTime.Now;                
                 this.DataUltimoBloqueio = DateTime.Now;
                 this.InicioTentativasAcessoResposta = DateTime.Now;
                 this.InicioTentativasAcessoSenha = DateTime.Now;                
                 this.TentativasAcessoResposta = 0;
                 this.TentativasAcessoSenha = 0;
                 this.TipoTrava = tipoTrava;

                 this.Save();
             }            
         }*/

        public void EnviaEmailAcesso(bool novoUsuario)
        {
            const string PATH_IMAGENS_PERSONALIZADAS = @"\imagensPersonalizadas";
            const string SUBJECT_DEFAULT_NOVO_USUARIO = "Informações de Acesso ao FinancialOnline";
            const string SUBJECT_DEFAULT_ALTERA_SENHA = "Alteração de Senha para o FinancialOnline";
            //const string BODY_DEFAULT = "Prezado(a) {NomeUsuario}<br /><br />Estamos satisfeitos que nos tenham escolhido para fazer seus investimentos.<br /><br />Sua carteira de investimentos poderá ser acompanhada pelo site {HostAplicacao}. Para acessar as informações, digite:<br /><br /><br /><b>Login:</b> {LoginUsuario}<br /><br /><b>Senha:</b> {SenhaUsuario}<br /><br /><br />Gostaríamos de lhe dar as boas-vindas e nos colocar à sua disposição.<br /><br />Atenciosamente,<br /><br />Equipe {NomeEmpresaClienteFinancial}";
            const string BODY_DEFAULT_NOVO_USUARIO = "Prezado(a) {NomeUsuario},<br /><br />Estamos satisfeitos que nos tenha escolhido para fazer seus investimentos.<br /><br />Sua carteira de investimentos poderá ser acompanhada pelo site {HostAplicacao}. Para acessar as informações, digite:<br /><br /><br /><b>Login:</b> {LoginUsuario}<br /><br /><b>Senha:</b> {SenhaUsuario}<br /><br /><br />Gostaríamos de lhe dar as boas-vindas e nos colocar à sua disposição.<br /><br />";
            const string BODY_DEFAULT_ALTERA_SENHA = "Prezado(a) {NomeUsuario},<br /><br />Conforme sua solicitação, foi gerada uma nova senha para acessar sua carteira de investimentos no site {HostAplicacao}.<br /><br />No seu próximo acesso, por favor utilize o login e a senha abaixo:<br /><br /><br /><b>Login:</b> {LoginUsuario}<br /><br /><b>Senha:</b> {SenhaUsuario}<br /><br /><br />A senha é provisória e deve ser alterada no primeiro acesso.<br /><br />";
            const string TIPO_EMAIL_NOVO_USUARIO = "NovoUsuario";
            const string TIPO_EMAIL_ALTERA_SENHA = "AlteraSenha";

            const string FROM_DEFAULT = "suporte@financialonline.com.br";
            const int START_CID = 1;

            string subjectDefault, bodyDefault, tipoEmail;
            if (novoUsuario)
            {
                subjectDefault = SUBJECT_DEFAULT_NOVO_USUARIO;
                bodyDefault = BODY_DEFAULT_NOVO_USUARIO;
                tipoEmail = TIPO_EMAIL_NOVO_USUARIO;
            }
            else
            {
                subjectDefault = SUBJECT_DEFAULT_ALTERA_SENHA;
                bodyDefault = BODY_DEFAULT_ALTERA_SENHA;
                tipoEmail = TIPO_EMAIL_ALTERA_SENHA;
            }

            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            MailMessage mail = new MailMessage();
            mail.To.Add(new MailAddress(this.Email));

            string from = settings.Smtp.From.ToString();
            if (String.IsNullOrEmpty(from))
            {
                from = FROM_DEFAULT;
            }

            mail.From = new MailAddress(from);

            string subject = ConfigurationManager.AppSettings["Subject" + tipoEmail];
            if (String.IsNullOrEmpty(subject))
            {
                subject = subjectDefault;
            }

            mail.Subject = subject;

            //Exemplos do PrivateBP - primeiro sem encode e o segundo encoded - levar para o documents
            //"<div lang=\"PT-BR\" vlink=\"purple\" link=\"blue\"><div><div align=\"center\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;\"><tbody><tr><td width=\"662\" valign=\"top\" style=\"width: 496.75pt; border: 1pt solid white; padding: 0cm 5.4pt;\"><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\"><img src=\"logoReport_Socopa.jpg\" /></span></font></p><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></td></tr><tr><td width=\"662\" valign=\"top\" style=\"width: 496.75pt; border-right: 1pt solid white; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color white white; padding: 0cm 1cm;\"><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Prezado Sr. {NomeCliente},</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"margin-bottom: 12pt; text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Estamos satisfeitos que tenha escolhido a SOCOPA &ndash; CORRETORA PAULISTA para fazer seus investimentos.</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">A partir de agora, você contará com o serviço diferenciado que oferecemos, buscando sempre atender suas necessidades e expectativas de forma personalizada e exclusiva.</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">Sua carteira de investimentos poderá ser acompanhada pelo site <a target=\"_blank\" href=\"{HostAplicacao}\"><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\">{HostAplicacao}</span></font></a>. Para acessar as informações, digite:</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><b><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;\">Login:</span></font></b><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\"> {LoginUsuario}</span></font></p><p style=\"text-align: justify;\"><b><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;\">Senha:</span></font></b><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\"> {SenhaUsuario}</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">A senha é provisória e <b><span style=\"font-weight: bold;\">deve ser alterada no primeiro acesso</span></b>.</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">Em caso de dúvida, entre em contato com o seu <i><span style=\"font-style: italic;\">banker</span></i>, {NomeOfficer}, pelo telefone {TelefoneOfficer} ou e-mail <a target=\"_blank\" href=\"mailto:{EmailOfficer}\"><font color=\"#002940\"><span style=\"color: rgb(0, 41, 64);\">{EmailOfficer}</span></font></a>.</span></font></p><p style=\"text-align: justify;\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt; color: rgb(0, 41, 64);\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Gostaríamos de lhe dar as boas-vindas e nos colocar à sua disposição. </span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Atenciosamente,</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">SOCOPA - CORRETORA PAULISTA</span></font></p><p style=\"text-align: justify;\" class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">Private - Gestão de Patrimônio</span></font></p><p class=\"MsoNormal\"><font face=\"Trebuchet MS\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></td></tr></tbody></table></div><p class=\"MsoNormal\"><font face=\"Tahoma\" color=\"#002940\" size=\"2\"><span style=\"font-size: 10pt;\">&nbsp;</span></font></p></div></div>"
            //WEBCONFIG: &lt;div lang="PT-BR" vlink="purple" link="blue"&gt;&lt;div&gt;&lt;div align="center"&gt;&lt;table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td width="662" valign="top" style="width: 496.75pt; border: 1pt solid white; padding: 0cm 5.4pt;"&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;img src="logoReport_Socopa.jpg" /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td width="662" valign="top" style="width: 496.75pt; border-right: 1pt solid white; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color white white; padding: 0cm 1cm;"&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Prezado Sr. {NomeCliente},&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="margin-bottom: 12pt; text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Estamos satisfeitos que tenha escolhido a SOCOPA - CORRETORA PAULISTA para fazer seus investimentos.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;A partir de agora, você contará com o serviço diferenciado que oferecemos, buscando sempre atender suas necessidades e expectativas de forma personalizada e exclusiva.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;Sua carteira de investimentos poderá ser acompanhada pelo site &lt;a target="_blank" href="{HostAplicacao}"&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt;{HostAplicacao}&lt;/span&gt;&lt;/font&gt;&lt;/a&gt;. Para acessar as informações, digite:&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;b&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;"&gt;Login:&lt;/span&gt;&lt;/font&gt;&lt;/b&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt; {LoginUsuario}&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;b&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64); font-weight: bold;"&gt;Senha:&lt;/span&gt;&lt;/font&gt;&lt;/b&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt; {SenhaUsuario}&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;A senha é provisória e &lt;b&gt;&lt;span style="font-weight: bold;"&gt;deve ser alterada no primeiro acesso&lt;/span&gt;&lt;/b&gt;.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;Em caso de dúvida, entre em contato com o seu &lt;i&gt;&lt;span style="font-style: italic;"&gt;banker&lt;/span&gt;&lt;/i&gt;, {NomeOfficer}, pelo telefone {TelefoneOfficer} ou e-mail &lt;a target="_blank" href="mailto:{EmailOfficer}"&gt;&lt;font color="#002940"&gt;&lt;span style="color: rgb(0, 41, 64);"&gt;{EmailOfficer}&lt;/span&gt;&lt;/font&gt;&lt;/a&gt;.&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt; color: rgb(0, 41, 64);"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Gostaríamos de lhe dar as boas-vindas e nos colocar à sua disposição. &lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Atenciosamente,&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;SOCOPA - CORRETORA PAULISTA&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p style="text-align: justify;" class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;Private - Gestão de Patrimônio&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Trebuchet MS" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;/div&gt;&lt;p class="MsoNormal"&gt;&lt;font face="Tahoma" color="#002940" size="2"&gt;&lt;span style="font-size: 10pt;"&gt;&lt;br /&gt;&lt;/span&gt;&lt;/font&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;
            string body = ConfigurationManager.AppSettings["Body" + tipoEmail];
            if (String.IsNullOrEmpty(body))
            {
                body = bodyDefault;
            }

            #region Fazer traducao das strings dinamicas (placeholders) como login, senha, nome do cliente
            string[] placeholders = { "{LoginUsuario}", "{SenhaUsuario}", "{NomeUsuario}", 
            "{NomeOfficer}", "{TelefoneOfficer}", "{EmailOfficer}", "{HostAplicacao}", 
            "{NomeEmpresaClienteFinancial}" };

            string hostAplicacao = ConfigurationManager.AppSettings["HostAplicacao"];
            string nomeEmpresaClienteFinancial = ConfigurationManager.AppSettings["Cliente"];

            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            financialMembershipProvider.Initialize();
            string decodedSenha = financialMembershipProvider.UnEncodePassword(this.Senha);

            string nomeOfficer = "", telefoneOfficer = "", emailOfficer = "";

            if (this.IdCliente.HasValue)
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(this.IdCliente.Value);
                if (cliente.IdOfficer.HasValue)
                {

                    Pessoa officer = cliente.RetornaOfficer();

                    nomeOfficer = officer.Nome;
                    emailOfficer = officer.Email;
                    telefoneOfficer = officer.Fone;
                }
            }

            string[] placeholdersValues = { this.Login, decodedSenha, this.Nome,
            nomeOfficer, telefoneOfficer, emailOfficer, hostAplicacao, 
            nomeEmpresaClienteFinancial};


            for (int i = 0; i < placeholders.Length; i++)
            {
                body = body.Replace(placeholders[i], placeholdersValues[i]);
            }
            #endregion

            #region Caso existam imagens locais (sem http no src), fazer o embed no email
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(body);

            int cid = START_CID;
            HtmlNodeCollection htmlImageNodeCollection = htmlDocument.DocumentNode.SelectNodes("//img");

            if (htmlImageNodeCollection != null && htmlImageNodeCollection.Count > 0)
            {

                List<string> imageSrcList = new List<string>();
                foreach (HtmlNode htmlImageNode in htmlImageNodeCollection)
                {
                    //Nao incluir imagens referenciadas externamente
                    string imageSrc = htmlImageNode.GetAttributeValue("src", null);
                    if (!String.IsNullOrEmpty(imageSrc) && !imageSrc.ToLower().StartsWith("http"))
                    {
                        imageSrcList.Add(imageSrc);
                        htmlImageNode.SetAttributeValue("src", String.Format("cid:{0}", cid));
                        cid++;
                    }
                }

                if (imageSrcList.Count > 0)
                {
                    //Atualizar html com imagens alteradas
                    body = htmlDocument.DocumentNode.WriteTo();

                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

                    cid = START_CID;
                    foreach (string imageSrc in imageSrcList)
                    {
                        LinkedResource imagelink = new LinkedResource(
                            String.Format(@"{0}{1}\{2}", AppDomain.CurrentDomain.BaseDirectory,
                            PATH_IMAGENS_PERSONALIZADAS, imageSrc), Financial.Util.Utilitario.GetMimeType(imageSrc));

                        imagelink.ContentId = cid.ToString();
                        imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        htmlView.LinkedResources.Add(imagelink);
                        cid++;
                    }

                    mail.AlternateViews.Add(htmlView);
                }
            }
            #endregion

            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            /*if (settings.Smtp.From.Contains("@gmail"))
            {
                smtp.EnableSsl = true;
            }*/

            string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
            if (!string.IsNullOrEmpty(smtpUsername))
            {
                string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
                smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            }

            if (smtpEnableSSL == "true")
            {
                smtp.EnableSsl = true;
            }

            try
            {
                smtp.Send(mail);
                if (!String.IsNullOrEmpty(emailOfficer))
                {
                    mail.To.Clear();
                    mail.To.Add(new MailAddress(emailOfficer));
                    smtp.Send(mail);
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
