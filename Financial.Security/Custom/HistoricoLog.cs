﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Security.Enums;

namespace Financial.Security {
    public partial class HistoricoLog : esHistoricoLog {
        private static readonly ILog log = LogManager.GetLogger(typeof(HistoricoLog));
        
        /// <summary>
        /// Insere um novo LogHistorico
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="descricao"></param>
        /// <param name="login">Login do Usuario</param>
        /// <param name="maquina"></param>
        /// <param name="cultura"></param>
        /// <param name="origem"></param>
      
        public void InsereHistoricoLog(DateTime dataInicio, DateTime dataFim, string descricao, 
                                       string login, string maquina, string cultura, HistoricoLogOrigem origem) {

            HistoricoLog historicoLogInserir = new HistoricoLog();

            if (descricao.Length > 8000)
            {
                descricao = descricao.Substring(0, 8000);
            }

            // Campos Obrigatórios para a inserção
            historicoLogInserir.DataInicio = dataInicio;
            historicoLogInserir.DataFim = dataFim;
            historicoLogInserir.Descricao = descricao;
            historicoLogInserir.Login = login;
            historicoLogInserir.Maquina = maquina;
            historicoLogInserir.Cultura = cultura;
            historicoLogInserir.Origem = Convert.ToInt32(origem);
            //    
            historicoLogInserir.Save();
        }
    }
}