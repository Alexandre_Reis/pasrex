﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Security.Exceptions;
using Financial.Security.Enums;
using Financial.InvestidorCotista;

namespace Financial.Security
{
	public partial class PermissaoCotista : esPermissaoCotista
	{
        /// <summary>
        /// Retorna um boolean indicando se o login tem acesso ao idCotista passado.
        /// </summary>
        /// <param name="idCotista"></param>  
        /// <param name="login"></param>  
        public bool RetornaAcessoCotista(int idCotista, string login)
        {
            Usuario usuario = new Usuario();            
            if (!usuario.BuscaUsuario(login))
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            int idUsuario = usuario.IdUsuario.Value;

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCotista)
                 .Where(this.Query.IdCotista == idCotista,
                        this.Query.IdUsuario == idUsuario);

            return this.Query.Load();            
        }

        /// <summary>
        /// Retorna o Id do cotista associado ao Usuario/Login.
        /// Como ele busca na collection, traz o 1o da lista.
        /// 
        /// Obs: Se não tiver cotista associado ao usuário, retorna Null.
        /// </summary>
        /// <param name="login"></param>  
        public int? RetornaCotistaAssociado(string login)
        {
            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(login))
                throw new UsuarioNaoCadastradoException("Usuário " + login + " não cadastrado.");
            int idUsuario = usuario.IdUsuario.Value;

            PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();

            permissaoCotistaCollection.Query
                 .Select(permissaoCotistaCollection.Query.IdCotista)
                 .Where(permissaoCotistaCollection.Query.IdUsuario.Equal(idUsuario));

            permissaoCotistaCollection.Query.Load();

            return (!permissaoCotistaCollection.HasData)
                    ? null
                    : ((PermissaoCotista)permissaoCotistaCollection[0]).IdCotista;            
        }

        /// <summary>
        /// Retorna o id do usuário vinculado ao idCotista passado, considerando TipoTrava = TravaCotista.
        /// Se não tiver usuário vinculado, retorna 0. Se tiver mais de 1 usuário vinculado, retorna o 1o da lista.
        /// </summary>
        /// <param name="idCotista"></param>  
        public int RetornaIdUsuarioTravaCotista(int idCotista)
        {
            PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            permissaoCotistaQuery.Select(permissaoCotistaQuery.IdUsuario);
            permissaoCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
            permissaoCotistaQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCotista));
            permissaoCotistaQuery.Where(permissaoCotistaQuery.IdCotista.Equal(idCotista));

            PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
            permissaoCotistaCollection.Load(permissaoCotistaQuery);

            int idCotistaRetorno = 0;

            if (permissaoCotistaCollection.Count > 0)
            {
                idCotistaRetorno = permissaoCotistaCollection[0].IdUsuario.Value;
            }
            
            return idCotistaRetorno;
        }
	}
}
