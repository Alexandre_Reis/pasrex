using System;

namespace Financial.Security.Enums {
       
    /// <summary>
    /// Usado para travar determinados campos
    /// </summary>
    public enum TipoTravaUsuario
    {
        SemTrava = 1,
        TravaCotista = 2,
        TravaCliente = 3
    }

    public enum HistoricoLogOrigem
    {
        Outros = 999999,
        MudancaSenha = 1,
        Processamento = 100,
        IntegracaoItau = 200,
		PortalInstitucional = 300,
        PortalCotista = 301
    }

    public enum SituacaoEnvio
    {
        Todas = 1,
        Distribuicao = 2,
        Aprovacao = 3
    }

    public enum HistoricoLogSubOrigem
    {
        AberturaSucesso = 1,
        AberturaErro = 2,
        RotinaCalculoSucesso = 3,
        RotinaCalculoErro = 4,
        FechamentoSucesso = 5,
        FechamentoErro = 6,
        AvancoPeriodoSucesso = 7,
        AvancoPeriodoErro = 8,
        RetroacaoSucesso = 9,
        RetroacaoErro = 10,
        Divulgacao = 11,
        DivulgacaoComErro = 12
    }

    /// <summary>
    /// Usado para definir perfis de grupos de usuario
    /// </summary>
    public enum TipoPerfilGrupo
    {
        Administrador = 1,
        BackOffice = 2,
        Externo = 3,
        Interno = 4
    }

    public enum PermissaoFront
    {
        Boletagem = 1,
        Aprovacao = 2
    }
}
