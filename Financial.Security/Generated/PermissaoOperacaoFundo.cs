/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/02/2015 18:41:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;













































































































































































































namespace Financial.Security
{

    [Serializable]
    abstract public class esPermissaoOperacaoFundoCollection : esEntityCollection
    {
        public esPermissaoOperacaoFundoCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "PermissaoOperacaoFundoCollection";
        }

        #region Query Logic
        protected void InitQuery(esPermissaoOperacaoFundoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esPermissaoOperacaoFundoQuery);
        }
        #endregion

        virtual public PermissaoOperacaoFundo DetachEntity(PermissaoOperacaoFundo entity)
        {
            return base.DetachEntity(entity) as PermissaoOperacaoFundo;
        }

        virtual public PermissaoOperacaoFundo AttachEntity(PermissaoOperacaoFundo entity)
        {
            return base.AttachEntity(entity) as PermissaoOperacaoFundo;
        }

        virtual public void Combine(PermissaoOperacaoFundoCollection collection)
        {
            base.Combine(collection);
        }

        new public PermissaoOperacaoFundo this[int index]
        {
            get
            {
                return base[index] as PermissaoOperacaoFundo;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(PermissaoOperacaoFundo);
        }
    }



    [Serializable]
    abstract public class esPermissaoOperacaoFundo : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esPermissaoOperacaoFundoQuery GetDynamicQuery()
        {
            return null;
        }

        public esPermissaoOperacaoFundo()
        {

        }

        public esPermissaoOperacaoFundo(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idCotista, System.Int32 idFundo)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idCotista, idFundo);
            else
                return LoadByPrimaryKeyStoredProcedure(idCotista, idFundo);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCotista, System.Int32 idFundo)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esPermissaoOperacaoFundoQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdCotista == idCotista, query.IdFundo == idFundo);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCotista, System.Int32 idFundo)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idCotista, idFundo);
            else
                return LoadByPrimaryKeyStoredProcedure(idCotista, idFundo);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idCotista, System.Int32 idFundo)
        {
            esPermissaoOperacaoFundoQuery query = this.GetDynamicQuery();
            query.Where(query.IdCotista == idCotista, query.IdFundo == idFundo);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCotista, System.Int32 idFundo)
        {
            esParameters parms = new esParameters();
            parms.Add("IdCotista", idCotista); parms.Add("IdFundo", idFundo);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdCotista": this.str.IdCotista = (string)value; break;
                        case "IdFundo": this.str.IdFundo = (string)value; break;
                        case "Email": this.str.Email = (string)value; break;
                        case "SituacaoEnvio": this.str.SituacaoEnvio = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdCotista":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCotista = (System.Int32?)value;
                            break;

                        case "IdFundo":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdFundo = (System.Int32?)value;
                            break;

                        case "SituacaoEnvio":

                            if (value == null || value.GetType().ToString() == "System.Byte")
                                this.SituacaoEnvio = (System.Byte?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to PermissaoOperacaoFundo.IdCotista
        /// </summary>
        virtual public System.Int32? IdCotista
        {
            get
            {
                return base.GetSystemInt32(PermissaoOperacaoFundoMetadata.ColumnNames.IdCotista);
            }

            set
            {
                base.SetSystemInt32(PermissaoOperacaoFundoMetadata.ColumnNames.IdCotista, value);
            }
        }

        /// <summary>
        /// Maps to PermissaoOperacaoFundo.IdFundo
        /// </summary>
        virtual public System.Int32? IdFundo
        {
            get
            {
                return base.GetSystemInt32(PermissaoOperacaoFundoMetadata.ColumnNames.IdFundo);
            }

            set
            {
                if (base.SetSystemInt32(PermissaoOperacaoFundoMetadata.ColumnNames.IdFundo, value))
                {
                    this._UpToClienteByIdFundo = null;
                }
            }
        }

        /// <summary>
        /// Maps to PermissaoOperacaoFundo.Email
        /// </summary>
        virtual public System.String Email
        {
            get
            {
                return base.GetSystemString(PermissaoOperacaoFundoMetadata.ColumnNames.Email);
            }

            set
            {
                base.SetSystemString(PermissaoOperacaoFundoMetadata.ColumnNames.Email, value);
            }
        }

        /// <summary>
        /// Maps to PermissaoOperacaoFundo.SituacaoEnvio
        /// </summary>
        virtual public System.Byte? SituacaoEnvio
        {
            get
            {
                return base.GetSystemByte(PermissaoOperacaoFundoMetadata.ColumnNames.SituacaoEnvio);
            }

            set
            {
                base.SetSystemByte(PermissaoOperacaoFundoMetadata.ColumnNames.SituacaoEnvio, value);
            }
        }

        [CLSCompliant(false)]
        internal protected Cliente _UpToClienteByIdFundo;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esPermissaoOperacaoFundo entity)
            {
                this.entity = entity;
            }


            public System.String IdCotista
            {
                get
                {
                    System.Int32? data = entity.IdCotista;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCotista = null;
                    else entity.IdCotista = Convert.ToInt32(value);
                }
            }

            public System.String IdFundo
            {
                get
                {
                    System.Int32? data = entity.IdFundo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdFundo = null;
                    else entity.IdFundo = Convert.ToInt32(value);
                }
            }

            public System.String Email
            {
                get
                {
                    System.String data = entity.Email;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Email = null;
                    else entity.Email = Convert.ToString(value);
                }
            }

            public System.String SituacaoEnvio
            {
                get
                {
                    System.Byte? data = entity.SituacaoEnvio;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.SituacaoEnvio = null;
                    else entity.SituacaoEnvio = Convert.ToByte(value);
                }
            }


            private esPermissaoOperacaoFundo entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esPermissaoOperacaoFundoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esPermissaoOperacaoFundo can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class PermissaoOperacaoFundo : esPermissaoOperacaoFundo
    {


        #region UpToClienteByIdFundo - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - Cliente_PermissaoOperacaoFundo_FK1
        /// </summary>

        [XmlIgnore]
        public Cliente UpToClienteByIdFundo
        {
            get
            {
                if (this._UpToClienteByIdFundo == null
                    && IdFundo != null)
                {
                    this._UpToClienteByIdFundo = new Cliente();
                    this._UpToClienteByIdFundo.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToClienteByIdFundo", this._UpToClienteByIdFundo);
                    this._UpToClienteByIdFundo.Query.Where(this._UpToClienteByIdFundo.Query.IdCliente == this.IdFundo);
                    this._UpToClienteByIdFundo.Query.Load();
                }

                return this._UpToClienteByIdFundo;
            }

            set
            {
                this.RemovePreSave("UpToClienteByIdFundo");


                if (value == null)
                {
                    this.IdFundo = null;
                    this._UpToClienteByIdFundo = null;
                }
                else
                {
                    this.IdFundo = value.IdCliente;
                    this._UpToClienteByIdFundo = value;
                    this.SetPreSave("UpToClienteByIdFundo", this._UpToClienteByIdFundo);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esPermissaoOperacaoFundoQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return PermissaoOperacaoFundoMetadata.Meta();
            }
        }


        public esQueryItem IdCotista
        {
            get
            {
                return new esQueryItem(this, PermissaoOperacaoFundoMetadata.ColumnNames.IdCotista, esSystemType.Int32);
            }
        }

        public esQueryItem IdFundo
        {
            get
            {
                return new esQueryItem(this, PermissaoOperacaoFundoMetadata.ColumnNames.IdFundo, esSystemType.Int32);
            }
        }

        public esQueryItem Email
        {
            get
            {
                return new esQueryItem(this, PermissaoOperacaoFundoMetadata.ColumnNames.Email, esSystemType.String);
            }
        }

        public esQueryItem SituacaoEnvio
        {
            get
            {
                return new esQueryItem(this, PermissaoOperacaoFundoMetadata.ColumnNames.SituacaoEnvio, esSystemType.Byte);
            }
        }

    }



    [Serializable]
    [XmlType("PermissaoOperacaoFundoCollection")]
    public partial class PermissaoOperacaoFundoCollection : esPermissaoOperacaoFundoCollection, IEnumerable<PermissaoOperacaoFundo>
    {
        public PermissaoOperacaoFundoCollection()
        {

        }

        public static implicit operator List<PermissaoOperacaoFundo>(PermissaoOperacaoFundoCollection coll)
        {
            List<PermissaoOperacaoFundo> list = new List<PermissaoOperacaoFundo>();

            foreach (PermissaoOperacaoFundo emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return PermissaoOperacaoFundoMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new PermissaoOperacaoFundoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new PermissaoOperacaoFundo(row);
        }

        override protected esEntity CreateEntity()
        {
            return new PermissaoOperacaoFundo();
        }


        #endregion


        [BrowsableAttribute(false)]
        public PermissaoOperacaoFundoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new PermissaoOperacaoFundoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(PermissaoOperacaoFundoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public PermissaoOperacaoFundo AddNew()
        {
            PermissaoOperacaoFundo entity = base.AddNewEntity() as PermissaoOperacaoFundo;

            return entity;
        }

        public PermissaoOperacaoFundo FindByPrimaryKey(System.Int32 idCotista, System.Int32 idFundo)
        {
            return base.FindByPrimaryKey(idCotista, idFundo) as PermissaoOperacaoFundo;
        }


        #region IEnumerable<PermissaoOperacaoFundo> Members

        IEnumerator<PermissaoOperacaoFundo> IEnumerable<PermissaoOperacaoFundo>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as PermissaoOperacaoFundo;
            }
        }

        #endregion

        private PermissaoOperacaoFundoQuery query;
    }


    /// <summary>
    /// Encapsulates the 'PermissaoOperacaoFundo' table
    /// </summary>

    [Serializable]
    public partial class PermissaoOperacaoFundo : esPermissaoOperacaoFundo
    {
        public PermissaoOperacaoFundo()
        {

        }

        public PermissaoOperacaoFundo(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return PermissaoOperacaoFundoMetadata.Meta();
            }
        }



        override protected esPermissaoOperacaoFundoQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new PermissaoOperacaoFundoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public PermissaoOperacaoFundoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new PermissaoOperacaoFundoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(PermissaoOperacaoFundoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private PermissaoOperacaoFundoQuery query;
    }



    [Serializable]
    public partial class PermissaoOperacaoFundoQuery : esPermissaoOperacaoFundoQuery
    {
        public PermissaoOperacaoFundoQuery()
        {

        }

        public PermissaoOperacaoFundoQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class PermissaoOperacaoFundoMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected PermissaoOperacaoFundoMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(PermissaoOperacaoFundoMetadata.ColumnNames.IdCotista, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = PermissaoOperacaoFundoMetadata.PropertyNames.IdCotista;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(PermissaoOperacaoFundoMetadata.ColumnNames.IdFundo, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = PermissaoOperacaoFundoMetadata.PropertyNames.IdFundo;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(PermissaoOperacaoFundoMetadata.ColumnNames.Email, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = PermissaoOperacaoFundoMetadata.PropertyNames.Email;
            c.CharacterMaxLength = 200;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(PermissaoOperacaoFundoMetadata.ColumnNames.SituacaoEnvio, 3, typeof(System.Byte), esSystemType.Byte);
            c.PropertyName = PermissaoOperacaoFundoMetadata.PropertyNames.SituacaoEnvio;
            c.NumericPrecision = 3;
            _columns.Add(c);


        }
        #endregion

        static public PermissaoOperacaoFundoMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdCotista = "IdCotista";
            public const string IdFundo = "IdFundo";
            public const string Email = "Email";
            public const string SituacaoEnvio = "SituacaoEnvio";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdCotista = "IdCotista";
            public const string IdFundo = "IdFundo";
            public const string Email = "Email";
            public const string SituacaoEnvio = "SituacaoEnvio";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(PermissaoOperacaoFundoMetadata))
            {
                if (PermissaoOperacaoFundoMetadata.mapDelegates == null)
                {
                    PermissaoOperacaoFundoMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (PermissaoOperacaoFundoMetadata.meta == null)
                {
                    PermissaoOperacaoFundoMetadata.meta = new PermissaoOperacaoFundoMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdFundo", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("SituacaoEnvio", new esTypeMap("tinyint", "System.Byte"));



                meta.Source = "PermissaoOperacaoFundo";
                meta.Destination = "PermissaoOperacaoFundo";

                meta.spInsert = "proc_PermissaoOperacaoFundoInsert";
                meta.spUpdate = "proc_PermissaoOperacaoFundoUpdate";
                meta.spDelete = "proc_PermissaoOperacaoFundoDelete";
                meta.spLoadAll = "proc_PermissaoOperacaoFundoLoadAll";
                meta.spLoadByPrimaryKey = "proc_PermissaoOperacaoFundoLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private PermissaoOperacaoFundoMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
