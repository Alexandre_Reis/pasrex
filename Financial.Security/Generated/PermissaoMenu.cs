/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esPermissaoMenuCollection : esEntityCollection
	{
		public esPermissaoMenuCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PermissaoMenuCollection";
		}

		#region Query Logic
		protected void InitQuery(esPermissaoMenuQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPermissaoMenuQuery);
		}
		#endregion
		
		virtual public PermissaoMenu DetachEntity(PermissaoMenu entity)
		{
			return base.DetachEntity(entity) as PermissaoMenu;
		}
		
		virtual public PermissaoMenu AttachEntity(PermissaoMenu entity)
		{
			return base.AttachEntity(entity) as PermissaoMenu;
		}
		
		virtual public void Combine(PermissaoMenuCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PermissaoMenu this[int index]
		{
			get
			{
				return base[index] as PermissaoMenu;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PermissaoMenu);
		}
	}



	[Serializable]
	abstract public class esPermissaoMenu : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPermissaoMenuQuery GetDynamicQuery()
		{
			return null;
		}

		public esPermissaoMenu()
		{

		}

		public esPermissaoMenu(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo, System.Int32 idMenu)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo, idMenu);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo, idMenu);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo, System.Int32 idMenu)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPermissaoMenuQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo, query.IdMenu == idMenu);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo, System.Int32 idMenu)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo, idMenu);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo, idMenu);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo, System.Int32 idMenu)
		{
			esPermissaoMenuQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo, query.IdMenu == idMenu);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo, System.Int32 idMenu)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);			parms.Add("IdMenu",idMenu);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "IdMenu": this.str.IdMenu = (string)value; break;							
						case "PermissaoLeitura": this.str.PermissaoLeitura = (string)value; break;							
						case "PermissaoAlteracao": this.str.PermissaoAlteracao = (string)value; break;							
						case "PermissaoExclusao": this.str.PermissaoExclusao = (string)value; break;							
						case "PermissaoInclusao": this.str.PermissaoInclusao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "IdMenu":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMenu = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PermissaoMenu.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(PermissaoMenuMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				if(base.SetSystemInt32(PermissaoMenuMetadata.ColumnNames.IdGrupo, value))
				{
					this._UpToGrupoUsuarioByIdGrupo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PermissaoMenu.IdMenu
		/// </summary>
		virtual public System.Int32? IdMenu
		{
			get
			{
				return base.GetSystemInt32(PermissaoMenuMetadata.ColumnNames.IdMenu);
			}
			
			set
			{
				base.SetSystemInt32(PermissaoMenuMetadata.ColumnNames.IdMenu, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoMenu.PermissaoLeitura
		/// </summary>
		virtual public System.String PermissaoLeitura
		{
			get
			{
				return base.GetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoLeitura);
			}
			
			set
			{
				base.SetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoLeitura, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoMenu.PermissaoAlteracao
		/// </summary>
		virtual public System.String PermissaoAlteracao
		{
			get
			{
				return base.GetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoAlteracao);
			}
			
			set
			{
				base.SetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoAlteracao, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoMenu.PermissaoExclusao
		/// </summary>
		virtual public System.String PermissaoExclusao
		{
			get
			{
				return base.GetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoExclusao);
			}
			
			set
			{
				base.SetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoExclusao, value);
			}
		}
		
		/// <summary>
		/// Maps to PermissaoMenu.PermissaoInclusao
		/// </summary>
		virtual public System.String PermissaoInclusao
		{
			get
			{
				return base.GetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoInclusao);
			}
			
			set
			{
				base.SetSystemString(PermissaoMenuMetadata.ColumnNames.PermissaoInclusao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected GrupoUsuario _UpToGrupoUsuarioByIdGrupo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPermissaoMenu entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMenu
			{
				get
				{
					System.Int32? data = entity.IdMenu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMenu = null;
					else entity.IdMenu = Convert.ToInt32(value);
				}
			}
				
			public System.String PermissaoLeitura
			{
				get
				{
					System.String data = entity.PermissaoLeitura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermissaoLeitura = null;
					else entity.PermissaoLeitura = Convert.ToString(value);
				}
			}
				
			public System.String PermissaoAlteracao
			{
				get
				{
					System.String data = entity.PermissaoAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermissaoAlteracao = null;
					else entity.PermissaoAlteracao = Convert.ToString(value);
				}
			}
				
			public System.String PermissaoExclusao
			{
				get
				{
					System.String data = entity.PermissaoExclusao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermissaoExclusao = null;
					else entity.PermissaoExclusao = Convert.ToString(value);
				}
			}
				
			public System.String PermissaoInclusao
			{
				get
				{
					System.String data = entity.PermissaoInclusao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermissaoInclusao = null;
					else entity.PermissaoInclusao = Convert.ToString(value);
				}
			}
			

			private esPermissaoMenu entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPermissaoMenuQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPermissaoMenu can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PermissaoMenu : esPermissaoMenu
	{

				
		#region UpToGrupoUsuarioByIdGrupo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - GrupoUsuario_PermissaoMenu_FK1
		/// </summary>

		[XmlIgnore]
		public GrupoUsuario UpToGrupoUsuarioByIdGrupo
		{
			get
			{
				if(this._UpToGrupoUsuarioByIdGrupo == null
					&& IdGrupo != null					)
				{
					this._UpToGrupoUsuarioByIdGrupo = new GrupoUsuario();
					this._UpToGrupoUsuarioByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoUsuarioByIdGrupo", this._UpToGrupoUsuarioByIdGrupo);
					this._UpToGrupoUsuarioByIdGrupo.Query.Where(this._UpToGrupoUsuarioByIdGrupo.Query.IdGrupo == this.IdGrupo);
					this._UpToGrupoUsuarioByIdGrupo.Query.Load();
				}

				return this._UpToGrupoUsuarioByIdGrupo;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoUsuarioByIdGrupo");
				

				if(value == null)
				{
					this.IdGrupo = null;
					this._UpToGrupoUsuarioByIdGrupo = null;
				}
				else
				{
					this.IdGrupo = value.IdGrupo;
					this._UpToGrupoUsuarioByIdGrupo = value;
					this.SetPreSave("UpToGrupoUsuarioByIdGrupo", this._UpToGrupoUsuarioByIdGrupo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToGrupoUsuarioByIdGrupo != null)
			{
				this.IdGrupo = this._UpToGrupoUsuarioByIdGrupo.IdGrupo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPermissaoMenuQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoMenuMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMenu
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.IdMenu, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PermissaoLeitura
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.PermissaoLeitura, esSystemType.String);
			}
		} 
		
		public esQueryItem PermissaoAlteracao
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.PermissaoAlteracao, esSystemType.String);
			}
		} 
		
		public esQueryItem PermissaoExclusao
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.PermissaoExclusao, esSystemType.String);
			}
		} 
		
		public esQueryItem PermissaoInclusao
		{
			get
			{
				return new esQueryItem(this, PermissaoMenuMetadata.ColumnNames.PermissaoInclusao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PermissaoMenuCollection")]
	public partial class PermissaoMenuCollection : esPermissaoMenuCollection, IEnumerable<PermissaoMenu>
	{
		public PermissaoMenuCollection()
		{

		}
		
		public static implicit operator List<PermissaoMenu>(PermissaoMenuCollection coll)
		{
			List<PermissaoMenu> list = new List<PermissaoMenu>();
			
			foreach (PermissaoMenu emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PermissaoMenuMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoMenuQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PermissaoMenu(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PermissaoMenu();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PermissaoMenuQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoMenuQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PermissaoMenuQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PermissaoMenu AddNew()
		{
			PermissaoMenu entity = base.AddNewEntity() as PermissaoMenu;
			
			return entity;
		}

		public PermissaoMenu FindByPrimaryKey(System.Int32 idGrupo, System.Int32 idMenu)
		{
			return base.FindByPrimaryKey(idGrupo, idMenu) as PermissaoMenu;
		}


		#region IEnumerable<PermissaoMenu> Members

		IEnumerator<PermissaoMenu> IEnumerable<PermissaoMenu>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PermissaoMenu;
			}
		}

		#endregion
		
		private PermissaoMenuQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PermissaoMenu' table
	/// </summary>

	[Serializable]
	public partial class PermissaoMenu : esPermissaoMenu
	{
		public PermissaoMenu()
		{

		}
	
		public PermissaoMenu(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoMenuMetadata.Meta();
			}
		}
		
		
		
		override protected esPermissaoMenuQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoMenuQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PermissaoMenuQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoMenuQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PermissaoMenuQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PermissaoMenuQuery query;
	}



	[Serializable]
	public partial class PermissaoMenuQuery : esPermissaoMenuQuery
	{
		public PermissaoMenuQuery()
		{

		}		
		
		public PermissaoMenuQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PermissaoMenuMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PermissaoMenuMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.IdMenu, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.IdMenu;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.PermissaoLeitura, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.PermissaoLeitura;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.PermissaoAlteracao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.PermissaoAlteracao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.PermissaoExclusao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.PermissaoExclusao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoMenuMetadata.ColumnNames.PermissaoInclusao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PermissaoMenuMetadata.PropertyNames.PermissaoInclusao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PermissaoMenuMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string IdMenu = "IdMenu";
			 public const string PermissaoLeitura = "PermissaoLeitura";
			 public const string PermissaoAlteracao = "PermissaoAlteracao";
			 public const string PermissaoExclusao = "PermissaoExclusao";
			 public const string PermissaoInclusao = "PermissaoInclusao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string IdMenu = "IdMenu";
			 public const string PermissaoLeitura = "PermissaoLeitura";
			 public const string PermissaoAlteracao = "PermissaoAlteracao";
			 public const string PermissaoExclusao = "PermissaoExclusao";
			 public const string PermissaoInclusao = "PermissaoInclusao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PermissaoMenuMetadata))
			{
				if(PermissaoMenuMetadata.mapDelegates == null)
				{
					PermissaoMenuMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PermissaoMenuMetadata.meta == null)
				{
					PermissaoMenuMetadata.meta = new PermissaoMenuMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMenu", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PermissaoLeitura", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PermissaoAlteracao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PermissaoExclusao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PermissaoInclusao", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "PermissaoMenu";
				meta.Destination = "PermissaoMenu";
				
				meta.spInsert = "proc_PermissaoMenuInsert";				
				meta.spUpdate = "proc_PermissaoMenuUpdate";		
				meta.spDelete = "proc_PermissaoMenuDelete";
				meta.spLoadAll = "proc_PermissaoMenuLoadAll";
				meta.spLoadByPrimaryKey = "proc_PermissaoMenuLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PermissaoMenuMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
