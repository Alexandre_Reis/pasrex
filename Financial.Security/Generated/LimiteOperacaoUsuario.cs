/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esLimiteOperacaoUsuarioCollection : esEntityCollection
	{
		public esLimiteOperacaoUsuarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LimiteOperacaoUsuarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esLimiteOperacaoUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLimiteOperacaoUsuarioQuery);
		}
		#endregion
		
		virtual public LimiteOperacaoUsuario DetachEntity(LimiteOperacaoUsuario entity)
		{
			return base.DetachEntity(entity) as LimiteOperacaoUsuario;
		}
		
		virtual public LimiteOperacaoUsuario AttachEntity(LimiteOperacaoUsuario entity)
		{
			return base.AttachEntity(entity) as LimiteOperacaoUsuario;
		}
		
		virtual public void Combine(LimiteOperacaoUsuarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LimiteOperacaoUsuario this[int index]
		{
			get
			{
				return base[index] as LimiteOperacaoUsuario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LimiteOperacaoUsuario);
		}
	}



	[Serializable]
	abstract public class esLimiteOperacaoUsuario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLimiteOperacaoUsuarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esLimiteOperacaoUsuario()
		{

		}

		public esLimiteOperacaoUsuario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idUsuario)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idUsuario)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLimiteOperacaoUsuarioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdUsuario == idUsuario);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idUsuario)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idUsuario)
		{
			esLimiteOperacaoUsuarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdUsuario == idUsuario);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idUsuario)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario",idUsuario);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "MaximoAplicacao": this.str.MaximoAplicacao = (string)value; break;							
						case "MaximoResgate": this.str.MaximoResgate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "MaximoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.MaximoAplicacao = (System.Decimal?)value;
							break;
						
						case "MaximoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.MaximoResgate = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LimiteOperacaoUsuario.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(LimiteOperacaoUsuarioMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				base.SetSystemInt32(LimiteOperacaoUsuarioMetadata.ColumnNames.IdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to LimiteOperacaoUsuario.MaximoAplicacao
		/// </summary>
		virtual public System.Decimal? MaximoAplicacao
		{
			get
			{
				return base.GetSystemDecimal(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LimiteOperacaoUsuario.MaximoResgate
		/// </summary>
		virtual public System.Decimal? MaximoResgate
		{
			get
			{
				return base.GetSystemDecimal(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoResgate, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLimiteOperacaoUsuario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String MaximoAplicacao
			{
				get
				{
					System.Decimal? data = entity.MaximoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MaximoAplicacao = null;
					else entity.MaximoAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String MaximoResgate
			{
				get
				{
					System.Decimal? data = entity.MaximoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MaximoResgate = null;
					else entity.MaximoResgate = Convert.ToDecimal(value);
				}
			}
			

			private esLimiteOperacaoUsuario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLimiteOperacaoUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLimiteOperacaoUsuario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LimiteOperacaoUsuario : esLimiteOperacaoUsuario
	{

		#region UpToUsuario - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - FK_LimiteOperacaoUsuario_Usuario
		/// </summary>

		[XmlIgnore]
		public Usuario UpToUsuario
		{
			get
			{
				if(this._UpToUsuario == null
					&& IdUsuario != null					)
				{
					this._UpToUsuario = new Usuario();
					this._UpToUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToUsuario", this._UpToUsuario);
					this._UpToUsuario.Query.Where(this._UpToUsuario.Query.IdUsuario == this.IdUsuario);
					this._UpToUsuario.Query.Load();
				}

				return this._UpToUsuario;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToUsuario");

				if(value == null)
				{
					this._UpToUsuario = null;
				}
				else
				{
					this._UpToUsuario = value;
					this.SetPreSave("UpToUsuario", this._UpToUsuario);
				}
				
				
			} 
		}

		private Usuario _UpToUsuario;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToUsuario != null)
			{
				this.IdUsuario = this._UpToUsuario.IdUsuario;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLimiteOperacaoUsuarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LimiteOperacaoUsuarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, LimiteOperacaoUsuarioMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem MaximoAplicacao
		{
			get
			{
				return new esQueryItem(this, LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem MaximoResgate
		{
			get
			{
				return new esQueryItem(this, LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoResgate, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LimiteOperacaoUsuarioCollection")]
	public partial class LimiteOperacaoUsuarioCollection : esLimiteOperacaoUsuarioCollection, IEnumerable<LimiteOperacaoUsuario>
	{
		public LimiteOperacaoUsuarioCollection()
		{

		}
		
		public static implicit operator List<LimiteOperacaoUsuario>(LimiteOperacaoUsuarioCollection coll)
		{
			List<LimiteOperacaoUsuario> list = new List<LimiteOperacaoUsuario>();
			
			foreach (LimiteOperacaoUsuario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LimiteOperacaoUsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LimiteOperacaoUsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LimiteOperacaoUsuario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LimiteOperacaoUsuario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LimiteOperacaoUsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LimiteOperacaoUsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LimiteOperacaoUsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LimiteOperacaoUsuario AddNew()
		{
			LimiteOperacaoUsuario entity = base.AddNewEntity() as LimiteOperacaoUsuario;
			
			return entity;
		}

		public LimiteOperacaoUsuario FindByPrimaryKey(System.Int32 idUsuario)
		{
			return base.FindByPrimaryKey(idUsuario) as LimiteOperacaoUsuario;
		}


		#region IEnumerable<LimiteOperacaoUsuario> Members

		IEnumerator<LimiteOperacaoUsuario> IEnumerable<LimiteOperacaoUsuario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LimiteOperacaoUsuario;
			}
		}

		#endregion
		
		private LimiteOperacaoUsuarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LimiteOperacaoUsuario' table
	/// </summary>

	[Serializable]
	public partial class LimiteOperacaoUsuario : esLimiteOperacaoUsuario
	{
		public LimiteOperacaoUsuario()
		{

		}
	
		public LimiteOperacaoUsuario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LimiteOperacaoUsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esLimiteOperacaoUsuarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LimiteOperacaoUsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LimiteOperacaoUsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LimiteOperacaoUsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LimiteOperacaoUsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LimiteOperacaoUsuarioQuery query;
	}



	[Serializable]
	public partial class LimiteOperacaoUsuarioQuery : esLimiteOperacaoUsuarioQuery
	{
		public LimiteOperacaoUsuarioQuery()
		{

		}		
		
		public LimiteOperacaoUsuarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LimiteOperacaoUsuarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LimiteOperacaoUsuarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LimiteOperacaoUsuarioMetadata.ColumnNames.IdUsuario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LimiteOperacaoUsuarioMetadata.PropertyNames.IdUsuario;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoAplicacao, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LimiteOperacaoUsuarioMetadata.PropertyNames.MaximoAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LimiteOperacaoUsuarioMetadata.ColumnNames.MaximoResgate, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LimiteOperacaoUsuarioMetadata.PropertyNames.MaximoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LimiteOperacaoUsuarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string MaximoAplicacao = "MaximoAplicacao";
			 public const string MaximoResgate = "MaximoResgate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string MaximoAplicacao = "MaximoAplicacao";
			 public const string MaximoResgate = "MaximoResgate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LimiteOperacaoUsuarioMetadata))
			{
				if(LimiteOperacaoUsuarioMetadata.mapDelegates == null)
				{
					LimiteOperacaoUsuarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LimiteOperacaoUsuarioMetadata.meta == null)
				{
					LimiteOperacaoUsuarioMetadata.meta = new LimiteOperacaoUsuarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("MaximoAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("MaximoResgate", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "LimiteOperacaoUsuario";
				meta.Destination = "LimiteOperacaoUsuario";
				
				meta.spInsert = "proc_LimiteOperacaoUsuarioInsert";				
				meta.spUpdate = "proc_LimiteOperacaoUsuarioUpdate";		
				meta.spDelete = "proc_LimiteOperacaoUsuarioDelete";
				meta.spLoadAll = "proc_LimiteOperacaoUsuarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_LimiteOperacaoUsuarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LimiteOperacaoUsuarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
