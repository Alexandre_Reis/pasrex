/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esHistoricoSenhasCollection : esEntityCollection
	{
		public esHistoricoSenhasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "HistoricoSenhasCollection";
		}

		#region Query Logic
		protected void InitQuery(esHistoricoSenhasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esHistoricoSenhasQuery);
		}
		#endregion
		
		virtual public HistoricoSenhas DetachEntity(HistoricoSenhas entity)
		{
			return base.DetachEntity(entity) as HistoricoSenhas;
		}
		
		virtual public HistoricoSenhas AttachEntity(HistoricoSenhas entity)
		{
			return base.AttachEntity(entity) as HistoricoSenhas;
		}
		
		virtual public void Combine(HistoricoSenhasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public HistoricoSenhas this[int index]
		{
			get
			{
				return base[index] as HistoricoSenhas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(HistoricoSenhas);
		}
	}



	[Serializable]
	abstract public class esHistoricoSenhas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esHistoricoSenhasQuery GetDynamicQuery()
		{
			return null;
		}

		public esHistoricoSenhas()
		{

		}

		public esHistoricoSenhas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, data, senha);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, data, senha);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esHistoricoSenhasQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdUsuario == idUsuario, query.Data == data, query.Senha == senha);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, data, senha);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, data, senha);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			esHistoricoSenhasQuery query = this.GetDynamicQuery();
			query.Where(query.IdUsuario == idUsuario, query.Data == data, query.Senha == senha);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario",idUsuario);			parms.Add("Data",data);			parms.Add("Senha",senha);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Senha": this.str.Senha = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to HistoricoSenhas.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(HistoricoSenhasMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				if(base.SetSystemInt32(HistoricoSenhasMetadata.ColumnNames.IdUsuario, value))
				{
					this._UpToUsuarioByIdUsuario = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to HistoricoSenhas.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(HistoricoSenhasMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoSenhasMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoSenhas.Senha
		/// </summary>
		virtual public System.String Senha
		{
			get
			{
				return base.GetSystemString(HistoricoSenhasMetadata.ColumnNames.Senha);
			}
			
			set
			{
				base.SetSystemString(HistoricoSenhasMetadata.ColumnNames.Senha, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Usuario _UpToUsuarioByIdUsuario;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esHistoricoSenhas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Senha
			{
				get
				{
					System.String data = entity.Senha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Senha = null;
					else entity.Senha = Convert.ToString(value);
				}
			}
			

			private esHistoricoSenhas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esHistoricoSenhasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esHistoricoSenhas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class HistoricoSenhas : esHistoricoSenhas
	{

				
		#region UpToUsuarioByIdUsuario - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_HistoricoSenhas_Usuario
		/// </summary>

		[XmlIgnore]
		public Usuario UpToUsuarioByIdUsuario
		{
			get
			{
				if(this._UpToUsuarioByIdUsuario == null
					&& IdUsuario != null					)
				{
					this._UpToUsuarioByIdUsuario = new Usuario();
					this._UpToUsuarioByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Where(this._UpToUsuarioByIdUsuario.Query.IdUsuario == this.IdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Load();
				}

				return this._UpToUsuarioByIdUsuario;
			}
			
			set
			{
				this.RemovePreSave("UpToUsuarioByIdUsuario");
				

				if(value == null)
				{
					this.IdUsuario = null;
					this._UpToUsuarioByIdUsuario = null;
				}
				else
				{
					this.IdUsuario = value.IdUsuario;
					this._UpToUsuarioByIdUsuario = value;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToUsuarioByIdUsuario != null)
			{
				this.IdUsuario = this._UpToUsuarioByIdUsuario.IdUsuario;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esHistoricoSenhasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoSenhasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, HistoricoSenhasMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, HistoricoSenhasMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Senha
		{
			get
			{
				return new esQueryItem(this, HistoricoSenhasMetadata.ColumnNames.Senha, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("HistoricoSenhasCollection")]
	public partial class HistoricoSenhasCollection : esHistoricoSenhasCollection, IEnumerable<HistoricoSenhas>
	{
		public HistoricoSenhasCollection()
		{

		}
		
		public static implicit operator List<HistoricoSenhas>(HistoricoSenhasCollection coll)
		{
			List<HistoricoSenhas> list = new List<HistoricoSenhas>();
			
			foreach (HistoricoSenhas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  HistoricoSenhasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoSenhasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new HistoricoSenhas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new HistoricoSenhas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public HistoricoSenhasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoSenhasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(HistoricoSenhasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public HistoricoSenhas AddNew()
		{
			HistoricoSenhas entity = base.AddNewEntity() as HistoricoSenhas;
			
			return entity;
		}

		public HistoricoSenhas FindByPrimaryKey(System.Int32 idUsuario, System.DateTime data, System.String senha)
		{
			return base.FindByPrimaryKey(idUsuario, data, senha) as HistoricoSenhas;
		}


		#region IEnumerable<HistoricoSenhas> Members

		IEnumerator<HistoricoSenhas> IEnumerable<HistoricoSenhas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as HistoricoSenhas;
			}
		}

		#endregion
		
		private HistoricoSenhasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'HistoricoSenhas' table
	/// </summary>

	[Serializable]
	public partial class HistoricoSenhas : esHistoricoSenhas
	{
		public HistoricoSenhas()
		{

		}
	
		public HistoricoSenhas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoSenhasMetadata.Meta();
			}
		}
		
		
		
		override protected esHistoricoSenhasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoSenhasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public HistoricoSenhasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoSenhasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(HistoricoSenhasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private HistoricoSenhasQuery query;
	}



	[Serializable]
	public partial class HistoricoSenhasQuery : esHistoricoSenhasQuery
	{
		public HistoricoSenhasQuery()
		{

		}		
		
		public HistoricoSenhasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class HistoricoSenhasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoSenhasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoSenhasMetadata.ColumnNames.IdUsuario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoSenhasMetadata.PropertyNames.IdUsuario;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoSenhasMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoSenhasMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoSenhasMetadata.ColumnNames.Senha, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoSenhasMetadata.PropertyNames.Senha;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public HistoricoSenhasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string Data = "Data";
			 public const string Senha = "Senha";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string Data = "Data";
			 public const string Senha = "Senha";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoSenhasMetadata))
			{
				if(HistoricoSenhasMetadata.mapDelegates == null)
				{
					HistoricoSenhasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoSenhasMetadata.meta == null)
				{
					HistoricoSenhasMetadata.meta = new HistoricoSenhasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Senha", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "HistoricoSenhas";
				meta.Destination = "HistoricoSenhas";
				
				meta.spInsert = "proc_HistoricoSenhasInsert";				
				meta.spUpdate = "proc_HistoricoSenhasUpdate";		
				meta.spDelete = "proc_HistoricoSenhasDelete";
				meta.spLoadAll = "proc_HistoricoSenhasLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoSenhasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoSenhasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
