/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		






using Financial.InvestidorCotista;
		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esPermissaoCotistaCollection : esEntityCollection
	{
		public esPermissaoCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PermissaoCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPermissaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPermissaoCotistaQuery);
		}
		#endregion
		
		virtual public PermissaoCotista DetachEntity(PermissaoCotista entity)
		{
			return base.DetachEntity(entity) as PermissaoCotista;
		}
		
		virtual public PermissaoCotista AttachEntity(PermissaoCotista entity)
		{
			return base.AttachEntity(entity) as PermissaoCotista;
		}
		
		virtual public void Combine(PermissaoCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PermissaoCotista this[int index]
		{
			get
			{
				return base[index] as PermissaoCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PermissaoCotista);
		}
	}



	[Serializable]
	abstract public class esPermissaoCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPermissaoCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPermissaoCotista()
		{

		}

		public esPermissaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idUsuario, System.Int32 idCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, idCotista);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idUsuario, System.Int32 idCotista)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPermissaoCotistaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdUsuario == idUsuario, query.IdCotista == idCotista);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idUsuario, System.Int32 idCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, idCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idUsuario, System.Int32 idCotista)
		{
			esPermissaoCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdUsuario == idUsuario, query.IdCotista == idCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idUsuario, System.Int32 idCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario",idUsuario);			parms.Add("IdCotista",idCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PermissaoCotista.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(PermissaoCotistaMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				if(base.SetSystemInt32(PermissaoCotistaMetadata.ColumnNames.IdUsuario, value))
				{
					this._UpToUsuarioByIdUsuario = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PermissaoCotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(PermissaoCotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(PermissaoCotistaMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		[CLSCompliant(false)]
		internal protected Usuario _UpToUsuarioByIdUsuario;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPermissaoCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
			

			private esPermissaoCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPermissaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPermissaoCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PermissaoCotista : esPermissaoCotista
	{

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cotista_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

				
		#region UpToUsuarioByIdUsuario - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Usuario_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Usuario UpToUsuarioByIdUsuario
		{
			get
			{
				if(this._UpToUsuarioByIdUsuario == null
					&& IdUsuario != null					)
				{
					this._UpToUsuarioByIdUsuario = new Usuario();
					this._UpToUsuarioByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Where(this._UpToUsuarioByIdUsuario.Query.IdUsuario == this.IdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Load();
				}

				return this._UpToUsuarioByIdUsuario;
			}
			
			set
			{
				this.RemovePreSave("UpToUsuarioByIdUsuario");
				

				if(value == null)
				{
					this.IdUsuario = null;
					this._UpToUsuarioByIdUsuario = null;
				}
				else
				{
					this.IdUsuario = value.IdUsuario;
					this._UpToUsuarioByIdUsuario = value;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToUsuarioByIdUsuario != null)
			{
				this.IdUsuario = this._UpToUsuarioByIdUsuario.IdUsuario;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPermissaoCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, PermissaoCotistaMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, PermissaoCotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PermissaoCotistaCollection")]
	public partial class PermissaoCotistaCollection : esPermissaoCotistaCollection, IEnumerable<PermissaoCotista>
	{
		public PermissaoCotistaCollection()
		{

		}
		
		public static implicit operator List<PermissaoCotista>(PermissaoCotistaCollection coll)
		{
			List<PermissaoCotista> list = new List<PermissaoCotista>();
			
			foreach (PermissaoCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PermissaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PermissaoCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PermissaoCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PermissaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PermissaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PermissaoCotista AddNew()
		{
			PermissaoCotista entity = base.AddNewEntity() as PermissaoCotista;
			
			return entity;
		}

		public PermissaoCotista FindByPrimaryKey(System.Int32 idUsuario, System.Int32 idCotista)
		{
			return base.FindByPrimaryKey(idUsuario, idCotista) as PermissaoCotista;
		}


		#region IEnumerable<PermissaoCotista> Members

		IEnumerator<PermissaoCotista> IEnumerable<PermissaoCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PermissaoCotista;
			}
		}

		#endregion
		
		private PermissaoCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PermissaoCotista' table
	/// </summary>

	[Serializable]
	public partial class PermissaoCotista : esPermissaoCotista
	{
		public PermissaoCotista()
		{

		}
	
		public PermissaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esPermissaoCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PermissaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PermissaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PermissaoCotistaQuery query;
	}



	[Serializable]
	public partial class PermissaoCotistaQuery : esPermissaoCotistaQuery
	{
		public PermissaoCotistaQuery()
		{

		}		
		
		public PermissaoCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PermissaoCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PermissaoCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PermissaoCotistaMetadata.ColumnNames.IdUsuario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoCotistaMetadata.PropertyNames.IdUsuario;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoCotistaMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoCotistaMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PermissaoCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdCotista = "IdCotista";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdCotista = "IdCotista";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PermissaoCotistaMetadata))
			{
				if(PermissaoCotistaMetadata.mapDelegates == null)
				{
					PermissaoCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PermissaoCotistaMetadata.meta == null)
				{
					PermissaoCotistaMetadata.meta = new PermissaoCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PermissaoCotista";
				meta.Destination = "PermissaoCotista";
				
				meta.spInsert = "proc_PermissaoCotistaInsert";				
				meta.spUpdate = "proc_PermissaoCotistaUpdate";		
				meta.spDelete = "proc_PermissaoCotistaDelete";
				meta.spLoadAll = "proc_PermissaoCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PermissaoCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PermissaoCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
