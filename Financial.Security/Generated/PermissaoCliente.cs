/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





using Financial.Investidor;
using Financial.InvestidorCotista;

		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esPermissaoClienteCollection : esEntityCollection
	{
		public esPermissaoClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PermissaoClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esPermissaoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPermissaoClienteQuery);
		}
		#endregion
		
		virtual public PermissaoCliente DetachEntity(PermissaoCliente entity)
		{
			return base.DetachEntity(entity) as PermissaoCliente;
		}
		
		virtual public PermissaoCliente AttachEntity(PermissaoCliente entity)
		{
			return base.AttachEntity(entity) as PermissaoCliente;
		}
		
		virtual public void Combine(PermissaoClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PermissaoCliente this[int index]
		{
			get
			{
				return base[index] as PermissaoCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PermissaoCliente);
		}
	}



	[Serializable]
	abstract public class esPermissaoCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPermissaoClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esPermissaoCliente()
		{

		}

		public esPermissaoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idUsuario, System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idUsuario, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPermissaoClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdUsuario == idUsuario, query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idUsuario, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario, idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idUsuario, System.Int32 idCliente)
		{
			esPermissaoClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdUsuario == idUsuario, query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idUsuario, System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario",idUsuario);			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PermissaoCliente.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(PermissaoClienteMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				if(base.SetSystemInt32(PermissaoClienteMetadata.ColumnNames.IdUsuario, value))
				{
					this._UpToUsuarioByIdUsuario = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PermissaoCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PermissaoClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PermissaoClienteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Usuario _UpToUsuarioByIdUsuario;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPermissaoCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esPermissaoCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPermissaoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPermissaoCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PermissaoCliente : esPermissaoCliente
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToUsuarioByIdUsuario - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Usuario_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public Usuario UpToUsuarioByIdUsuario
		{
			get
			{
				if(this._UpToUsuarioByIdUsuario == null
					&& IdUsuario != null					)
				{
					this._UpToUsuarioByIdUsuario = new Usuario();
					this._UpToUsuarioByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Where(this._UpToUsuarioByIdUsuario.Query.IdUsuario == this.IdUsuario);
					this._UpToUsuarioByIdUsuario.Query.Load();
				}

				return this._UpToUsuarioByIdUsuario;
			}
			
			set
			{
				this.RemovePreSave("UpToUsuarioByIdUsuario");
				

				if(value == null)
				{
					this.IdUsuario = null;
					this._UpToUsuarioByIdUsuario = null;
				}
				else
				{
					this.IdUsuario = value.IdUsuario;
					this._UpToUsuarioByIdUsuario = value;
					this.SetPreSave("UpToUsuarioByIdUsuario", this._UpToUsuarioByIdUsuario);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToUsuarioByIdUsuario != null)
			{
				this.IdUsuario = this._UpToUsuarioByIdUsuario.IdUsuario;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPermissaoClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, PermissaoClienteMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PermissaoClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PermissaoClienteCollection")]
	public partial class PermissaoClienteCollection : esPermissaoClienteCollection, IEnumerable<PermissaoCliente>
	{
		public PermissaoClienteCollection()
		{

		}
		
		public static implicit operator List<PermissaoCliente>(PermissaoClienteCollection coll)
		{
			List<PermissaoCliente> list = new List<PermissaoCliente>();
			
			foreach (PermissaoCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PermissaoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PermissaoCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PermissaoCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PermissaoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PermissaoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PermissaoCliente AddNew()
		{
			PermissaoCliente entity = base.AddNewEntity() as PermissaoCliente;
			
			return entity;
		}

		public PermissaoCliente FindByPrimaryKey(System.Int32 idUsuario, System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idUsuario, idCliente) as PermissaoCliente;
		}


		#region IEnumerable<PermissaoCliente> Members

		IEnumerator<PermissaoCliente> IEnumerable<PermissaoCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PermissaoCliente;
			}
		}

		#endregion
		
		private PermissaoClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PermissaoCliente' table
	/// </summary>

	[Serializable]
	public partial class PermissaoCliente : esPermissaoCliente
	{
		public PermissaoCliente()
		{

		}
	
		public PermissaoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PermissaoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esPermissaoClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PermissaoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PermissaoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PermissaoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PermissaoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PermissaoClienteQuery query;
	}



	[Serializable]
	public partial class PermissaoClienteQuery : esPermissaoClienteQuery
	{
		public PermissaoClienteQuery()
		{

		}		
		
		public PermissaoClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PermissaoClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PermissaoClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PermissaoClienteMetadata.ColumnNames.IdUsuario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoClienteMetadata.PropertyNames.IdUsuario;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PermissaoClienteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PermissaoClienteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PermissaoClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PermissaoClienteMetadata))
			{
				if(PermissaoClienteMetadata.mapDelegates == null)
				{
					PermissaoClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PermissaoClienteMetadata.meta == null)
				{
					PermissaoClienteMetadata.meta = new PermissaoClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PermissaoCliente";
				meta.Destination = "PermissaoCliente";
				
				meta.spInsert = "proc_PermissaoClienteInsert";				
				meta.spUpdate = "proc_PermissaoClienteUpdate";		
				meta.spDelete = "proc_PermissaoClienteDelete";
				meta.spLoadAll = "proc_PermissaoClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_PermissaoClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PermissaoClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
