/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:11
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esGrupoUsuarioCollection : esEntityCollection
	{
		public esGrupoUsuarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoUsuarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoUsuarioQuery);
		}
		#endregion
		
		virtual public GrupoUsuario DetachEntity(GrupoUsuario entity)
		{
			return base.DetachEntity(entity) as GrupoUsuario;
		}
		
		virtual public GrupoUsuario AttachEntity(GrupoUsuario entity)
		{
			return base.AttachEntity(entity) as GrupoUsuario;
		}
		
		virtual public void Combine(GrupoUsuarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoUsuario this[int index]
		{
			get
			{
				return base[index] as GrupoUsuario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoUsuario);
		}
	}



	[Serializable]
	abstract public class esGrupoUsuario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoUsuarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoUsuario()
		{

		}

		public esGrupoUsuario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupoUsuarioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo)
		{
			esGrupoUsuarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoPerfil": this.str.TipoPerfil = (string)value; break;							
						case "Front": this.str.Front = (string)value; break;							
						case "CarteiraSimulada": this.str.CarteiraSimulada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "TipoPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPerfil = (System.Byte?)value;
							break;
						
						case "Front":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Front = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoUsuario.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(GrupoUsuarioMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(GrupoUsuarioMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoUsuario.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(GrupoUsuarioMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(GrupoUsuarioMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoUsuario.TipoPerfil
		/// </summary>
		virtual public System.Byte? TipoPerfil
		{
			get
			{
				return base.GetSystemByte(GrupoUsuarioMetadata.ColumnNames.TipoPerfil);
			}
			
			set
			{
				base.SetSystemByte(GrupoUsuarioMetadata.ColumnNames.TipoPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoUsuario.Front
		/// </summary>
		virtual public System.Byte? Front
		{
			get
			{
				return base.GetSystemByte(GrupoUsuarioMetadata.ColumnNames.Front);
			}
			
			set
			{
				base.SetSystemByte(GrupoUsuarioMetadata.ColumnNames.Front, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoUsuario.CarteiraSimulada
		/// </summary>
		virtual public System.String CarteiraSimulada
		{
			get
			{
				return base.GetSystemString(GrupoUsuarioMetadata.ColumnNames.CarteiraSimulada);
			}
			
			set
			{
				base.SetSystemString(GrupoUsuarioMetadata.ColumnNames.CarteiraSimulada, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoUsuario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoPerfil
			{
				get
				{
					System.Byte? data = entity.TipoPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPerfil = null;
					else entity.TipoPerfil = Convert.ToByte(value);
				}
			}
				
			public System.String Front
			{
				get
				{
					System.Byte? data = entity.Front;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Front = null;
					else entity.Front = Convert.ToByte(value);
				}
			}
				
			public System.String CarteiraSimulada
			{
				get
				{
					System.String data = entity.CarteiraSimulada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CarteiraSimulada = null;
					else entity.CarteiraSimulada = Convert.ToString(value);
				}
			}
			

			private esGrupoUsuario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoUsuario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoUsuario : esGrupoUsuario
	{

				
		#region PermissaoMenuCollectionByIdGrupo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - GrupoUsuario_PermissaoMenu_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoMenuCollection PermissaoMenuCollectionByIdGrupo
		{
			get
			{
				if(this._PermissaoMenuCollectionByIdGrupo == null)
				{
					this._PermissaoMenuCollectionByIdGrupo = new PermissaoMenuCollection();
					this._PermissaoMenuCollectionByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoMenuCollectionByIdGrupo", this._PermissaoMenuCollectionByIdGrupo);
				
					if(this.IdGrupo != null)
					{
						this._PermissaoMenuCollectionByIdGrupo.Query.Where(this._PermissaoMenuCollectionByIdGrupo.Query.IdGrupo == this.IdGrupo);
						this._PermissaoMenuCollectionByIdGrupo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoMenuCollectionByIdGrupo.fks.Add(PermissaoMenuMetadata.ColumnNames.IdGrupo, this.IdGrupo);
					}
				}

				return this._PermissaoMenuCollectionByIdGrupo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoMenuCollectionByIdGrupo != null) 
				{ 
					this.RemovePostSave("PermissaoMenuCollectionByIdGrupo"); 
					this._PermissaoMenuCollectionByIdGrupo = null;
					
				} 
			} 			
		}

		private PermissaoMenuCollection _PermissaoMenuCollectionByIdGrupo;
		#endregion

				
		#region UsuarioCollectionByIdGrupo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - GrupoUsuario_Usuario_FK1
		/// </summary>

		[XmlIgnore]
		public UsuarioCollection UsuarioCollectionByIdGrupo
		{
			get
			{
				if(this._UsuarioCollectionByIdGrupo == null)
				{
					this._UsuarioCollectionByIdGrupo = new UsuarioCollection();
					this._UsuarioCollectionByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UsuarioCollectionByIdGrupo", this._UsuarioCollectionByIdGrupo);
				
					if(this.IdGrupo != null)
					{
						this._UsuarioCollectionByIdGrupo.Query.Where(this._UsuarioCollectionByIdGrupo.Query.IdGrupo == this.IdGrupo);
						this._UsuarioCollectionByIdGrupo.Query.Load();

						// Auto-hookup Foreign Keys
						this._UsuarioCollectionByIdGrupo.fks.Add(UsuarioMetadata.ColumnNames.IdGrupo, this.IdGrupo);
					}
				}

				return this._UsuarioCollectionByIdGrupo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UsuarioCollectionByIdGrupo != null) 
				{ 
					this.RemovePostSave("UsuarioCollectionByIdGrupo"); 
					this._UsuarioCollectionByIdGrupo = null;
					
				} 
			} 			
		}

		private UsuarioCollection _UsuarioCollectionByIdGrupo;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PermissaoMenuCollectionByIdGrupo", typeof(PermissaoMenuCollection), new PermissaoMenu()));
			props.Add(new esPropertyDescriptor(this, "UsuarioCollectionByIdGrupo", typeof(UsuarioCollection), new Usuario()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._PermissaoMenuCollectionByIdGrupo != null)
			{
				foreach(PermissaoMenu obj in this._PermissaoMenuCollectionByIdGrupo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupo = this.IdGrupo;
					}
				}
			}
			if(this._UsuarioCollectionByIdGrupo != null)
			{
				foreach(Usuario obj in this._UsuarioCollectionByIdGrupo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupo = this.IdGrupo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupoUsuarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoUsuarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, GrupoUsuarioMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, GrupoUsuarioMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoPerfil
		{
			get
			{
				return new esQueryItem(this, GrupoUsuarioMetadata.ColumnNames.TipoPerfil, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Front
		{
			get
			{
				return new esQueryItem(this, GrupoUsuarioMetadata.ColumnNames.Front, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CarteiraSimulada
		{
			get
			{
				return new esQueryItem(this, GrupoUsuarioMetadata.ColumnNames.CarteiraSimulada, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoUsuarioCollection")]
	public partial class GrupoUsuarioCollection : esGrupoUsuarioCollection, IEnumerable<GrupoUsuario>
	{
		public GrupoUsuarioCollection()
		{

		}
		
		public static implicit operator List<GrupoUsuario>(GrupoUsuarioCollection coll)
		{
			List<GrupoUsuario> list = new List<GrupoUsuario>();
			
			foreach (GrupoUsuario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoUsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoUsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoUsuario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoUsuario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoUsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoUsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoUsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoUsuario AddNew()
		{
			GrupoUsuario entity = base.AddNewEntity() as GrupoUsuario;
			
			return entity;
		}

		public GrupoUsuario FindByPrimaryKey(System.Int32 idGrupo)
		{
			return base.FindByPrimaryKey(idGrupo) as GrupoUsuario;
		}


		#region IEnumerable<GrupoUsuario> Members

		IEnumerator<GrupoUsuario> IEnumerable<GrupoUsuario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoUsuario;
			}
		}

		#endregion
		
		private GrupoUsuarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoUsuario' table
	/// </summary>

	[Serializable]
	public partial class GrupoUsuario : esGrupoUsuario
	{
		public GrupoUsuario()
		{

		}
	
		public GrupoUsuario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoUsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoUsuarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoUsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoUsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoUsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoUsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoUsuarioQuery query;
	}



	[Serializable]
	public partial class GrupoUsuarioQuery : esGrupoUsuarioQuery
	{
		public GrupoUsuarioQuery()
		{

		}		
		
		public GrupoUsuarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoUsuarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoUsuarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoUsuarioMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoUsuarioMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoUsuarioMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoUsuarioMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoUsuarioMetadata.ColumnNames.TipoPerfil, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GrupoUsuarioMetadata.PropertyNames.TipoPerfil;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoUsuarioMetadata.ColumnNames.Front, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GrupoUsuarioMetadata.PropertyNames.Front;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoUsuarioMetadata.ColumnNames.CarteiraSimulada, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoUsuarioMetadata.PropertyNames.CarteiraSimulada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoUsuarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
			 public const string TipoPerfil = "TipoPerfil";
			 public const string Front = "Front";
			 public const string CarteiraSimulada = "CarteiraSimulada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
			 public const string TipoPerfil = "TipoPerfil";
			 public const string Front = "Front";
			 public const string CarteiraSimulada = "CarteiraSimulada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoUsuarioMetadata))
			{
				if(GrupoUsuarioMetadata.mapDelegates == null)
				{
					GrupoUsuarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoUsuarioMetadata.meta == null)
				{
					GrupoUsuarioMetadata.meta = new GrupoUsuarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoPerfil", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Front", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CarteiraSimulada", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "GrupoUsuario";
				meta.Destination = "GrupoUsuario";
				
				meta.spInsert = "proc_GrupoUsuarioInsert";				
				meta.spUpdate = "proc_GrupoUsuarioUpdate";		
				meta.spDelete = "proc_GrupoUsuarioDelete";
				meta.spLoadAll = "proc_GrupoUsuarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoUsuarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoUsuarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
