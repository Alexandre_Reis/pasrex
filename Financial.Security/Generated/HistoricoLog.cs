/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esHistoricoLogCollection : esEntityCollection
	{
		public esHistoricoLogCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "HistoricoLogCollection";
		}

		#region Query Logic
		protected void InitQuery(esHistoricoLogQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esHistoricoLogQuery);
		}
		#endregion
		
		virtual public HistoricoLog DetachEntity(HistoricoLog entity)
		{
			return base.DetachEntity(entity) as HistoricoLog;
		}
		
		virtual public HistoricoLog AttachEntity(HistoricoLog entity)
		{
			return base.AttachEntity(entity) as HistoricoLog;
		}
		
		virtual public void Combine(HistoricoLogCollection collection)
		{
			base.Combine(collection);
		}
		
		new public HistoricoLog this[int index]
		{
			get
			{
				return base[index] as HistoricoLog;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(HistoricoLog);
		}
	}



	[Serializable]
	abstract public class esHistoricoLog : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esHistoricoLogQuery GetDynamicQuery()
		{
			return null;
		}

		public esHistoricoLog()
		{

		}

		public esHistoricoLog(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLog)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLog);
			else
				return LoadByPrimaryKeyStoredProcedure(idLog);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLog)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esHistoricoLogQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLog == idLog);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLog)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLog);
			else
				return LoadByPrimaryKeyStoredProcedure(idLog);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLog)
		{
			esHistoricoLogQuery query = this.GetDynamicQuery();
			query.Where(query.IdLog == idLog);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLog)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLog",idLog);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLog": this.str.IdLog = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Login": this.str.Login = (string)value; break;							
						case "Maquina": this.str.Maquina = (string)value; break;							
						case "Cultura": this.str.Cultura = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "SubOrigem": this.str.SubOrigem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLog":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLog = (System.Int32?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Origem = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "SubOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.SubOrigem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to HistoricoLog.IdLog
		/// </summary>
		virtual public System.Int32? IdLog
		{
			get
			{
				return base.GetSystemInt32(HistoricoLogMetadata.ColumnNames.IdLog);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoLogMetadata.ColumnNames.IdLog, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(HistoricoLogMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoLogMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(HistoricoLogMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoLogMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(HistoricoLogMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(HistoricoLogMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.Login
		/// </summary>
		virtual public System.String Login
		{
			get
			{
				return base.GetSystemString(HistoricoLogMetadata.ColumnNames.Login);
			}
			
			set
			{
				base.SetSystemString(HistoricoLogMetadata.ColumnNames.Login, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.Maquina
		/// </summary>
		virtual public System.String Maquina
		{
			get
			{
				return base.GetSystemString(HistoricoLogMetadata.ColumnNames.Maquina);
			}
			
			set
			{
				base.SetSystemString(HistoricoLogMetadata.ColumnNames.Maquina, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.Cultura
		/// </summary>
		virtual public System.String Cultura
		{
			get
			{
				return base.GetSystemString(HistoricoLogMetadata.ColumnNames.Cultura);
			}
			
			set
			{
				base.SetSystemString(HistoricoLogMetadata.ColumnNames.Cultura, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.Origem
		/// </summary>
		virtual public System.Int32? Origem
		{
			get
			{
				return base.GetSystemInt32(HistoricoLogMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoLogMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(HistoricoLogMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoLogMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoLog.SubOrigem
		/// </summary>
		virtual public System.Int32? SubOrigem
		{
			get
			{
				return base.GetSystemInt32(HistoricoLogMetadata.ColumnNames.SubOrigem);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoLogMetadata.ColumnNames.SubOrigem, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esHistoricoLog entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLog
			{
				get
				{
					System.Int32? data = entity.IdLog;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLog = null;
					else entity.IdLog = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Login
			{
				get
				{
					System.String data = entity.Login;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Login = null;
					else entity.Login = Convert.ToString(value);
				}
			}
				
			public System.String Maquina
			{
				get
				{
					System.String data = entity.Maquina;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Maquina = null;
					else entity.Maquina = Convert.ToString(value);
				}
			}
				
			public System.String Cultura
			{
				get
				{
					System.String data = entity.Cultura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cultura = null;
					else entity.Cultura = Convert.ToString(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Int32? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String SubOrigem
			{
				get
				{
					System.Int32? data = entity.SubOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SubOrigem = null;
					else entity.SubOrigem = Convert.ToInt32(value);
				}
			}
			

			private esHistoricoLog entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esHistoricoLogQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esHistoricoLog can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class HistoricoLog : esHistoricoLog
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esHistoricoLogQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoLogMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLog
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.IdLog, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Login
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.Login, esSystemType.String);
			}
		} 
		
		public esQueryItem Maquina
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.Maquina, esSystemType.String);
			}
		} 
		
		public esQueryItem Cultura
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.Cultura, esSystemType.String);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.Origem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem SubOrigem
		{
			get
			{
				return new esQueryItem(this, HistoricoLogMetadata.ColumnNames.SubOrigem, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("HistoricoLogCollection")]
	public partial class HistoricoLogCollection : esHistoricoLogCollection, IEnumerable<HistoricoLog>
	{
		public HistoricoLogCollection()
		{

		}
		
		public static implicit operator List<HistoricoLog>(HistoricoLogCollection coll)
		{
			List<HistoricoLog> list = new List<HistoricoLog>();
			
			foreach (HistoricoLog emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  HistoricoLogMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoLogQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new HistoricoLog(row);
		}

		override protected esEntity CreateEntity()
		{
			return new HistoricoLog();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public HistoricoLogQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoLogQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(HistoricoLogQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public HistoricoLog AddNew()
		{
			HistoricoLog entity = base.AddNewEntity() as HistoricoLog;
			
			return entity;
		}

		public HistoricoLog FindByPrimaryKey(System.Int32 idLog)
		{
			return base.FindByPrimaryKey(idLog) as HistoricoLog;
		}


		#region IEnumerable<HistoricoLog> Members

		IEnumerator<HistoricoLog> IEnumerable<HistoricoLog>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as HistoricoLog;
			}
		}

		#endregion
		
		private HistoricoLogQuery query;
	}


	/// <summary>
	/// Encapsulates the 'HistoricoLog' table
	/// </summary>

	[Serializable]
	public partial class HistoricoLog : esHistoricoLog
	{
		public HistoricoLog()
		{

		}
	
		public HistoricoLog(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoLogMetadata.Meta();
			}
		}
		
		
		
		override protected esHistoricoLogQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoLogQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public HistoricoLogQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoLogQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(HistoricoLogQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private HistoricoLogQuery query;
	}



	[Serializable]
	public partial class HistoricoLogQuery : esHistoricoLogQuery
	{
		public HistoricoLogQuery()
		{

		}		
		
		public HistoricoLogQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class HistoricoLogMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoLogMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.IdLog, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.IdLog;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.DataInicio, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.DataFim, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 8000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.Login, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.Login;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.Maquina, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.Maquina;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.Cultura, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.Cultura;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.Origem, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.IdCliente, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoLogMetadata.ColumnNames.SubOrigem, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoLogMetadata.PropertyNames.SubOrigem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public HistoricoLogMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLog = "IdLog";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Descricao = "Descricao";
			 public const string Login = "Login";
			 public const string Maquina = "Maquina";
			 public const string Cultura = "Cultura";
			 public const string Origem = "Origem";
			 public const string IdCliente = "IdCliente";
			 public const string SubOrigem = "SubOrigem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLog = "IdLog";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Descricao = "Descricao";
			 public const string Login = "Login";
			 public const string Maquina = "Maquina";
			 public const string Cultura = "Cultura";
			 public const string Origem = "Origem";
			 public const string IdCliente = "IdCliente";
			 public const string SubOrigem = "SubOrigem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoLogMetadata))
			{
				if(HistoricoLogMetadata.mapDelegates == null)
				{
					HistoricoLogMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoLogMetadata.meta == null)
				{
					HistoricoLogMetadata.meta = new HistoricoLogMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLog", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Login", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Maquina", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Cultura", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Origem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SubOrigem", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "HistoricoLog";
				meta.Destination = "HistoricoLog";
				
				meta.spInsert = "proc_HistoricoLogInsert";				
				meta.spUpdate = "proc_HistoricoLogUpdate";		
				meta.spDelete = "proc_HistoricoLogDelete";
				meta.spLoadAll = "proc_HistoricoLogLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoLogLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoLogMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
