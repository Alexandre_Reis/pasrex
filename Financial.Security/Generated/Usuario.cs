/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/04/2014 16:10:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		




using Financial.Investidor;
using Financial.InvestidorCotista;


		




				
				








				




		

		
		
		
		
		





namespace Financial.Security
{

	[Serializable]
	abstract public class esUsuarioCollection : esEntityCollection
	{
		public esUsuarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "UsuarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esUsuarioQuery);
		}
		#endregion
		
		virtual public Usuario DetachEntity(Usuario entity)
		{
			return base.DetachEntity(entity) as Usuario;
		}
		
		virtual public Usuario AttachEntity(Usuario entity)
		{
			return base.AttachEntity(entity) as Usuario;
		}
		
		virtual public void Combine(UsuarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Usuario this[int index]
		{
			get
			{
				return base[index] as Usuario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Usuario);
		}
	}



	[Serializable]
	abstract public class esUsuario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esUsuarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esUsuario()
		{

		}

		public esUsuario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idUsuario)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idUsuario)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esUsuarioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdUsuario == idUsuario);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idUsuario)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idUsuario);
			else
				return LoadByPrimaryKeyStoredProcedure(idUsuario);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idUsuario)
		{
			esUsuarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdUsuario == idUsuario);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idUsuario)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario",idUsuario);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Login": this.str.Login = (string)value; break;							
						case "Senha": this.str.Senha = (string)value; break;							
						case "Email": this.str.Email = (string)value; break;							
						case "PerguntaLogin": this.str.PerguntaLogin = (string)value; break;							
						case "RespostaLogin": this.str.RespostaLogin = (string)value; break;							
						case "DataUltimaAlteracao": this.str.DataUltimaAlteracao = (string)value; break;							
						case "DataValidade": this.str.DataValidade = (string)value; break;							
						case "DataCadastro": this.str.DataCadastro = (string)value; break;							
						case "DataUltimoLogin": this.str.DataUltimoLogin = (string)value; break;							
						case "DataUltimoBloqueio": this.str.DataUltimoBloqueio = (string)value; break;							
						case "StatusAtivo": this.str.StatusAtivo = (string)value; break;							
						case "StatusBloqueado": this.str.StatusBloqueado = (string)value; break;							
						case "TentativasAcessoSenha": this.str.TentativasAcessoSenha = (string)value; break;							
						case "TentativasAcessoResposta": this.str.TentativasAcessoResposta = (string)value; break;							
						case "InicioTentativasAcessoSenha": this.str.InicioTentativasAcessoSenha = (string)value; break;							
						case "InicioTentativasAcessoResposta": this.str.InicioTentativasAcessoResposta = (string)value; break;							
						case "TipoTrava": this.str.TipoTrava = (string)value; break;							
						case "PaginaInicial": this.str.PaginaInicial = (string)value; break;							
						case "AcessaWin": this.str.AcessaWin = (string)value; break;							
						case "TrocaSenha": this.str.TrocaSenha = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataLogout": this.str.DataLogout = (string)value; break;							
						case "Logotipo": this.str.Logotipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "DataUltimaAlteracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaAlteracao = (System.DateTime?)value;
							break;
						
						case "DataValidade":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataValidade = (System.DateTime?)value;
							break;
						
						case "DataCadastro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCadastro = (System.DateTime?)value;
							break;
						
						case "DataUltimoLogin":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoLogin = (System.DateTime?)value;
							break;
						
						case "DataUltimoBloqueio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoBloqueio = (System.DateTime?)value;
							break;
						
						case "TentativasAcessoSenha":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TentativasAcessoSenha = (System.Byte?)value;
							break;
						
						case "TentativasAcessoResposta":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TentativasAcessoResposta = (System.Byte?)value;
							break;
						
						case "InicioTentativasAcessoSenha":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioTentativasAcessoSenha = (System.DateTime?)value;
							break;
						
						case "InicioTentativasAcessoResposta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioTentativasAcessoResposta = (System.DateTime?)value;
							break;
						
						case "TipoTrava":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTrava = (System.Byte?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataLogout":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLogout = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Usuario.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(UsuarioMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				base.SetSystemInt32(UsuarioMetadata.ColumnNames.IdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(UsuarioMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				if(base.SetSystemInt32(UsuarioMetadata.ColumnNames.IdGrupo, value))
				{
					this._UpToGrupoUsuarioByIdGrupo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Usuario.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.Login
		/// </summary>
		virtual public System.String Login
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.Login);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.Login, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.Senha
		/// </summary>
		virtual public System.String Senha
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.Senha);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.Senha, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.Email
		/// </summary>
		virtual public System.String Email
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.Email);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.Email, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.PerguntaLogin
		/// </summary>
		virtual public System.String PerguntaLogin
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.PerguntaLogin);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.PerguntaLogin, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.RespostaLogin
		/// </summary>
		virtual public System.String RespostaLogin
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.RespostaLogin);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.RespostaLogin, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataUltimaAlteracao
		/// </summary>
		virtual public System.DateTime? DataUltimaAlteracao
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimaAlteracao);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimaAlteracao, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataValidade
		/// </summary>
		virtual public System.DateTime? DataValidade
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataValidade);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataValidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataCadastro
		/// </summary>
		virtual public System.DateTime? DataCadastro
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataCadastro);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataCadastro, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataUltimoLogin
		/// </summary>
		virtual public System.DateTime? DataUltimoLogin
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimoLogin);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimoLogin, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataUltimoBloqueio
		/// </summary>
		virtual public System.DateTime? DataUltimoBloqueio
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimoBloqueio);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataUltimoBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.StatusAtivo
		/// </summary>
		virtual public System.String StatusAtivo
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.StatusAtivo);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.StatusAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.StatusBloqueado
		/// </summary>
		virtual public System.String StatusBloqueado
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.StatusBloqueado);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.StatusBloqueado, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.TentativasAcessoSenha
		/// </summary>
		virtual public System.Byte? TentativasAcessoSenha
		{
			get
			{
				return base.GetSystemByte(UsuarioMetadata.ColumnNames.TentativasAcessoSenha);
			}
			
			set
			{
				base.SetSystemByte(UsuarioMetadata.ColumnNames.TentativasAcessoSenha, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.TentativasAcessoResposta
		/// </summary>
		virtual public System.Byte? TentativasAcessoResposta
		{
			get
			{
				return base.GetSystemByte(UsuarioMetadata.ColumnNames.TentativasAcessoResposta);
			}
			
			set
			{
				base.SetSystemByte(UsuarioMetadata.ColumnNames.TentativasAcessoResposta, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.InicioTentativasAcessoSenha
		/// </summary>
		virtual public System.DateTime? InicioTentativasAcessoSenha
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.InicioTentativasAcessoSenha);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.InicioTentativasAcessoSenha, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.InicioTentativasAcessoResposta
		/// </summary>
		virtual public System.DateTime? InicioTentativasAcessoResposta
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.InicioTentativasAcessoResposta);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.InicioTentativasAcessoResposta, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.TipoTrava
		/// </summary>
		virtual public System.Byte? TipoTrava
		{
			get
			{
				return base.GetSystemByte(UsuarioMetadata.ColumnNames.TipoTrava);
			}
			
			set
			{
				base.SetSystemByte(UsuarioMetadata.ColumnNames.TipoTrava, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.PaginaInicial
		/// </summary>
		virtual public System.String PaginaInicial
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.PaginaInicial);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.PaginaInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.AcessaWin
		/// </summary>
		virtual public System.String AcessaWin
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.AcessaWin);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.AcessaWin, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.TrocaSenha
		/// </summary>
		virtual public System.String TrocaSenha
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.TrocaSenha);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.TrocaSenha, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(UsuarioMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(UsuarioMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Usuario.DataLogout
		/// </summary>
		virtual public System.DateTime? DataLogout
		{
			get
			{
				return base.GetSystemDateTime(UsuarioMetadata.ColumnNames.DataLogout);
			}
			
			set
			{
				base.SetSystemDateTime(UsuarioMetadata.ColumnNames.DataLogout, value);
			}
		}
		
		/// <summary>
		/// Maps to Usuario.Logotipo
		/// </summary>
		virtual public System.String Logotipo
		{
			get
			{
				return base.GetSystemString(UsuarioMetadata.ColumnNames.Logotipo);
			}
			
			set
			{
				base.SetSystemString(UsuarioMetadata.ColumnNames.Logotipo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected GrupoUsuario _UpToGrupoUsuarioByIdGrupo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esUsuario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Login
			{
				get
				{
					System.String data = entity.Login;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Login = null;
					else entity.Login = Convert.ToString(value);
				}
			}
				
			public System.String Senha
			{
				get
				{
					System.String data = entity.Senha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Senha = null;
					else entity.Senha = Convert.ToString(value);
				}
			}
				
			public System.String Email
			{
				get
				{
					System.String data = entity.Email;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Email = null;
					else entity.Email = Convert.ToString(value);
				}
			}
				
			public System.String PerguntaLogin
			{
				get
				{
					System.String data = entity.PerguntaLogin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerguntaLogin = null;
					else entity.PerguntaLogin = Convert.ToString(value);
				}
			}
				
			public System.String RespostaLogin
			{
				get
				{
					System.String data = entity.RespostaLogin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RespostaLogin = null;
					else entity.RespostaLogin = Convert.ToString(value);
				}
			}
				
			public System.String DataUltimaAlteracao
			{
				get
				{
					System.DateTime? data = entity.DataUltimaAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaAlteracao = null;
					else entity.DataUltimaAlteracao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataValidade
			{
				get
				{
					System.DateTime? data = entity.DataValidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataValidade = null;
					else entity.DataValidade = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataCadastro
			{
				get
				{
					System.DateTime? data = entity.DataCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCadastro = null;
					else entity.DataCadastro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataUltimoLogin
			{
				get
				{
					System.DateTime? data = entity.DataUltimoLogin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoLogin = null;
					else entity.DataUltimoLogin = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataUltimoBloqueio
			{
				get
				{
					System.DateTime? data = entity.DataUltimoBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoBloqueio = null;
					else entity.DataUltimoBloqueio = Convert.ToDateTime(value);
				}
			}
				
			public System.String StatusAtivo
			{
				get
				{
					System.String data = entity.StatusAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusAtivo = null;
					else entity.StatusAtivo = Convert.ToString(value);
				}
			}
				
			public System.String StatusBloqueado
			{
				get
				{
					System.String data = entity.StatusBloqueado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusBloqueado = null;
					else entity.StatusBloqueado = Convert.ToString(value);
				}
			}
				
			public System.String TentativasAcessoSenha
			{
				get
				{
					System.Byte? data = entity.TentativasAcessoSenha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TentativasAcessoSenha = null;
					else entity.TentativasAcessoSenha = Convert.ToByte(value);
				}
			}
				
			public System.String TentativasAcessoResposta
			{
				get
				{
					System.Byte? data = entity.TentativasAcessoResposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TentativasAcessoResposta = null;
					else entity.TentativasAcessoResposta = Convert.ToByte(value);
				}
			}
				
			public System.String InicioTentativasAcessoSenha
			{
				get
				{
					System.DateTime? data = entity.InicioTentativasAcessoSenha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioTentativasAcessoSenha = null;
					else entity.InicioTentativasAcessoSenha = Convert.ToDateTime(value);
				}
			}
				
			public System.String InicioTentativasAcessoResposta
			{
				get
				{
					System.DateTime? data = entity.InicioTentativasAcessoResposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioTentativasAcessoResposta = null;
					else entity.InicioTentativasAcessoResposta = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoTrava
			{
				get
				{
					System.Byte? data = entity.TipoTrava;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTrava = null;
					else entity.TipoTrava = Convert.ToByte(value);
				}
			}
				
			public System.String PaginaInicial
			{
				get
				{
					System.String data = entity.PaginaInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PaginaInicial = null;
					else entity.PaginaInicial = Convert.ToString(value);
				}
			}
				
			public System.String AcessaWin
			{
				get
				{
					System.String data = entity.AcessaWin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AcessaWin = null;
					else entity.AcessaWin = Convert.ToString(value);
				}
			}
				
			public System.String TrocaSenha
			{
				get
				{
					System.String data = entity.TrocaSenha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrocaSenha = null;
					else entity.TrocaSenha = Convert.ToString(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLogout
			{
				get
				{
					System.DateTime? data = entity.DataLogout;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLogout = null;
					else entity.DataLogout = Convert.ToDateTime(value);
				}
			}
				
			public System.String Logotipo
			{
				get
				{
					System.String data = entity.Logotipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Logotipo = null;
					else entity.Logotipo = Convert.ToString(value);
				}
			}
			

			private esUsuario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esUsuarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esUsuario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Usuario : esUsuario
	{

				
		#region HistoricoSenhasCollectionByIdUsuario - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_HistoricoSenhas_Usuario
		/// </summary>

		[XmlIgnore]
		public HistoricoSenhasCollection HistoricoSenhasCollectionByIdUsuario
		{
			get
			{
				if(this._HistoricoSenhasCollectionByIdUsuario == null)
				{
					this._HistoricoSenhasCollectionByIdUsuario = new HistoricoSenhasCollection();
					this._HistoricoSenhasCollectionByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("HistoricoSenhasCollectionByIdUsuario", this._HistoricoSenhasCollectionByIdUsuario);
				
					if(this.IdUsuario != null)
					{
						this._HistoricoSenhasCollectionByIdUsuario.Query.Where(this._HistoricoSenhasCollectionByIdUsuario.Query.IdUsuario == this.IdUsuario);
						this._HistoricoSenhasCollectionByIdUsuario.Query.Load();

						// Auto-hookup Foreign Keys
						this._HistoricoSenhasCollectionByIdUsuario.fks.Add(HistoricoSenhasMetadata.ColumnNames.IdUsuario, this.IdUsuario);
					}
				}

				return this._HistoricoSenhasCollectionByIdUsuario;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._HistoricoSenhasCollectionByIdUsuario != null) 
				{ 
					this.RemovePostSave("HistoricoSenhasCollectionByIdUsuario"); 
					this._HistoricoSenhasCollectionByIdUsuario = null;
					
				} 
			} 			
		}

		private HistoricoSenhasCollection _HistoricoSenhasCollectionByIdUsuario;
		#endregion

				
		#region LimiteOperacaoUsuario - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - FK_LimiteOperacaoUsuario_Usuario
		/// </summary>

		[XmlIgnore]
		public LimiteOperacaoUsuario LimiteOperacaoUsuario
		{
			get
			{
				if(this._LimiteOperacaoUsuario == null)
				{
					this._LimiteOperacaoUsuario = new LimiteOperacaoUsuario();
					this._LimiteOperacaoUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("LimiteOperacaoUsuario", this._LimiteOperacaoUsuario);
				
					if(this.IdUsuario != null)
					{
						this._LimiteOperacaoUsuario.Query.Where(this._LimiteOperacaoUsuario.Query.IdUsuario == this.IdUsuario);
						this._LimiteOperacaoUsuario.Query.Load();
					}
				}

				return this._LimiteOperacaoUsuario;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LimiteOperacaoUsuario != null) 
				{ 
					this.RemovePostOneSave("LimiteOperacaoUsuario"); 
					this._LimiteOperacaoUsuario = null;
					
				} 
			}          			
		}

		private LimiteOperacaoUsuario _LimiteOperacaoUsuario;
		#endregion

		#region UpToClienteCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Usuario_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection UpToClienteCollection
		{
			get
			{
				if(this._UpToClienteCollection == null)
				{
					this._UpToClienteCollection = new ClienteCollection();
					this._UpToClienteCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToClienteCollection", this._UpToClienteCollection);
					this._UpToClienteCollection.ManyToManyUsuarioCollection(this.IdUsuario);
				}

				return this._UpToClienteCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToClienteCollection != null) 
				{ 
					this.RemovePostSave("UpToClienteCollection"); 
					this._UpToClienteCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Usuario_PermissaoCliente_FK1
		/// </summary>
		public void AssociateClienteCollection(Cliente entity)
		{
			if (this._PermissaoClienteCollection == null)
			{
				this._PermissaoClienteCollection = new PermissaoClienteCollection();
				this._PermissaoClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoClienteCollection", this._PermissaoClienteCollection);
			}

			PermissaoCliente obj = this._PermissaoClienteCollection.AddNew();
			obj.IdUsuario = this.IdUsuario;
			obj.IdCliente = entity.IdCliente;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Usuario_PermissaoCliente_FK1
		/// </summary>
		public void DissociateClienteCollection(Cliente entity)
		{
			if (this._PermissaoClienteCollection == null)
			{
				this._PermissaoClienteCollection = new PermissaoClienteCollection();
				this._PermissaoClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoClienteCollection", this._PermissaoClienteCollection);
			}

			PermissaoCliente obj = this._PermissaoClienteCollection.AddNew();
			obj.IdUsuario = this.IdUsuario;
			obj.IdCliente = entity.IdCliente;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private ClienteCollection _UpToClienteCollection;
		private PermissaoClienteCollection _PermissaoClienteCollection;
		#endregion

				
		#region PermissaoClienteCollectionByIdUsuario - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Usuario_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoClienteCollection PermissaoClienteCollectionByIdUsuario
		{
			get
			{
				if(this._PermissaoClienteCollectionByIdUsuario == null)
				{
					this._PermissaoClienteCollectionByIdUsuario = new PermissaoClienteCollection();
					this._PermissaoClienteCollectionByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoClienteCollectionByIdUsuario", this._PermissaoClienteCollectionByIdUsuario);
				
					if(this.IdUsuario != null)
					{
						this._PermissaoClienteCollectionByIdUsuario.Query.Where(this._PermissaoClienteCollectionByIdUsuario.Query.IdUsuario == this.IdUsuario);
						this._PermissaoClienteCollectionByIdUsuario.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoClienteCollectionByIdUsuario.fks.Add(PermissaoClienteMetadata.ColumnNames.IdUsuario, this.IdUsuario);
					}
				}

				return this._PermissaoClienteCollectionByIdUsuario;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoClienteCollectionByIdUsuario != null) 
				{ 
					this.RemovePostSave("PermissaoClienteCollectionByIdUsuario"); 
					this._PermissaoClienteCollectionByIdUsuario = null;
					
				} 
			} 			
		}

		private PermissaoClienteCollection _PermissaoClienteCollectionByIdUsuario;
		#endregion

		#region UpToCotistaCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Usuario_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public CotistaCollection UpToCotistaCollection
		{
			get
			{
				if(this._UpToCotistaCollection == null)
				{
					this._UpToCotistaCollection = new CotistaCollection();
					this._UpToCotistaCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToCotistaCollection", this._UpToCotistaCollection);
					this._UpToCotistaCollection.ManyToManyUsuarioCollection(this.IdUsuario);
				}

				return this._UpToCotistaCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToCotistaCollection != null) 
				{ 
					this.RemovePostSave("UpToCotistaCollection"); 
					this._UpToCotistaCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Usuario_PermissaoCotista_FK1
		/// </summary>
		public void AssociateCotistaCollection(Cotista entity)
		{
			if (this._PermissaoCotistaCollection == null)
			{
				this._PermissaoCotistaCollection = new PermissaoCotistaCollection();
				this._PermissaoCotistaCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoCotistaCollection", this._PermissaoCotistaCollection);
			}

			PermissaoCotista obj = this._PermissaoCotistaCollection.AddNew();
			obj.IdUsuario = this.IdUsuario;
			obj.IdCotista = entity.IdCotista;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Usuario_PermissaoCotista_FK1
		/// </summary>
		public void DissociateCotistaCollection(Cotista entity)
		{
			if (this._PermissaoCotistaCollection == null)
			{
				this._PermissaoCotistaCollection = new PermissaoCotistaCollection();
				this._PermissaoCotistaCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoCotistaCollection", this._PermissaoCotistaCollection);
			}

			PermissaoCotista obj = this._PermissaoCotistaCollection.AddNew();
			obj.IdUsuario = this.IdUsuario;
			obj.IdCotista = entity.IdCotista;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private CotistaCollection _UpToCotistaCollection;
		private PermissaoCotistaCollection _PermissaoCotistaCollection;
		#endregion

				
		#region PermissaoCotistaCollectionByIdUsuario - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Usuario_PermissaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoCotistaCollection PermissaoCotistaCollectionByIdUsuario
		{
			get
			{
				if(this._PermissaoCotistaCollectionByIdUsuario == null)
				{
					this._PermissaoCotistaCollectionByIdUsuario = new PermissaoCotistaCollection();
					this._PermissaoCotistaCollectionByIdUsuario.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoCotistaCollectionByIdUsuario", this._PermissaoCotistaCollectionByIdUsuario);
				
					if(this.IdUsuario != null)
					{
						this._PermissaoCotistaCollectionByIdUsuario.Query.Where(this._PermissaoCotistaCollectionByIdUsuario.Query.IdUsuario == this.IdUsuario);
						this._PermissaoCotistaCollectionByIdUsuario.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoCotistaCollectionByIdUsuario.fks.Add(PermissaoCotistaMetadata.ColumnNames.IdUsuario, this.IdUsuario);
					}
				}

				return this._PermissaoCotistaCollectionByIdUsuario;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoCotistaCollectionByIdUsuario != null) 
				{ 
					this.RemovePostSave("PermissaoCotistaCollectionByIdUsuario"); 
					this._PermissaoCotistaCollectionByIdUsuario = null;
					
				} 
			} 			
		}

		private PermissaoCotistaCollection _PermissaoCotistaCollectionByIdUsuario;
		#endregion

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_Usuario_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoUsuarioByIdGrupo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - GrupoUsuario_Usuario_FK1
		/// </summary>

		[XmlIgnore]
		public GrupoUsuario UpToGrupoUsuarioByIdGrupo
		{
			get
			{
				if(this._UpToGrupoUsuarioByIdGrupo == null
					&& IdGrupo != null					)
				{
					this._UpToGrupoUsuarioByIdGrupo = new GrupoUsuario();
					this._UpToGrupoUsuarioByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoUsuarioByIdGrupo", this._UpToGrupoUsuarioByIdGrupo);
					this._UpToGrupoUsuarioByIdGrupo.Query.Where(this._UpToGrupoUsuarioByIdGrupo.Query.IdGrupo == this.IdGrupo);
					this._UpToGrupoUsuarioByIdGrupo.Query.Load();
				}

				return this._UpToGrupoUsuarioByIdGrupo;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoUsuarioByIdGrupo");
				

				if(value == null)
				{
					this.IdGrupo = null;
					this._UpToGrupoUsuarioByIdGrupo = null;
				}
				else
				{
					this.IdGrupo = value.IdGrupo;
					this._UpToGrupoUsuarioByIdGrupo = value;
					this.SetPreSave("UpToGrupoUsuarioByIdGrupo", this._UpToGrupoUsuarioByIdGrupo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "HistoricoSenhasCollectionByIdUsuario", typeof(HistoricoSenhasCollection), new HistoricoSenhas()));
			props.Add(new esPropertyDescriptor(this, "PermissaoClienteCollectionByIdUsuario", typeof(PermissaoClienteCollection), new PermissaoCliente()));
			props.Add(new esPropertyDescriptor(this, "PermissaoCotistaCollectionByIdUsuario", typeof(PermissaoCotistaCollection), new PermissaoCotista()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToGrupoUsuarioByIdGrupo != null)
			{
				this.IdGrupo = this._UpToGrupoUsuarioByIdGrupo.IdGrupo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._HistoricoSenhasCollectionByIdUsuario != null)
			{
				foreach(HistoricoSenhas obj in this._HistoricoSenhasCollectionByIdUsuario)
				{
					if(obj.es.IsAdded)
					{
						obj.IdUsuario = this.IdUsuario;
					}
				}
			}
			if(this._PermissaoClienteCollection != null)
			{
				foreach(PermissaoCliente obj in this._PermissaoClienteCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdUsuario = this.IdUsuario;
					}
				}
			}
			if(this._PermissaoClienteCollectionByIdUsuario != null)
			{
				foreach(PermissaoCliente obj in this._PermissaoClienteCollectionByIdUsuario)
				{
					if(obj.es.IsAdded)
					{
						obj.IdUsuario = this.IdUsuario;
					}
				}
			}
			if(this._PermissaoCotistaCollection != null)
			{
				foreach(PermissaoCotista obj in this._PermissaoCotistaCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdUsuario = this.IdUsuario;
					}
				}
			}
			if(this._PermissaoCotistaCollectionByIdUsuario != null)
			{
				foreach(PermissaoCotista obj in this._PermissaoCotistaCollectionByIdUsuario)
				{
					if(obj.es.IsAdded)
					{
						obj.IdUsuario = this.IdUsuario;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._LimiteOperacaoUsuario != null)
			{
				if(this._LimiteOperacaoUsuario.es.IsAdded)
				{
					this._LimiteOperacaoUsuario.IdUsuario = this.IdUsuario;
				}
			}
		}
		
	}
	
	public partial class UsuarioCollection : esUsuarioCollection
	{
		#region ManyToManyClienteCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyClienteCollection(System.Int32? IdCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente", IdCliente);
	
			return base.Load( esQueryType.ManyToMany, 
				"Usuario,PermissaoCliente|IdUsuario,IdUsuario|IdCliente",	parms);
		}
		#endregion
	}

	
	public partial class UsuarioCollection : esUsuarioCollection
	{
		#region ManyToManyCotistaCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyCotistaCollection(System.Int32? IdCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCotista", IdCotista);
	
			return base.Load( esQueryType.ManyToMany, 
				"Usuario,PermissaoCotista|IdUsuario,IdUsuario|IdCotista",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esUsuarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return UsuarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Login
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.Login, esSystemType.String);
			}
		} 
		
		public esQueryItem Senha
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.Senha, esSystemType.String);
			}
		} 
		
		public esQueryItem Email
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.Email, esSystemType.String);
			}
		} 
		
		public esQueryItem PerguntaLogin
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.PerguntaLogin, esSystemType.String);
			}
		} 
		
		public esQueryItem RespostaLogin
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.RespostaLogin, esSystemType.String);
			}
		} 
		
		public esQueryItem DataUltimaAlteracao
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataUltimaAlteracao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataValidade
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataValidade, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataCadastro
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataCadastro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataUltimoLogin
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataUltimoLogin, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataUltimoBloqueio
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataUltimoBloqueio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem StatusAtivo
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.StatusAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem StatusBloqueado
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.StatusBloqueado, esSystemType.String);
			}
		} 
		
		public esQueryItem TentativasAcessoSenha
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.TentativasAcessoSenha, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TentativasAcessoResposta
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.TentativasAcessoResposta, esSystemType.Byte);
			}
		} 
		
		public esQueryItem InicioTentativasAcessoSenha
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.InicioTentativasAcessoSenha, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem InicioTentativasAcessoResposta
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.InicioTentativasAcessoResposta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoTrava
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.TipoTrava, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PaginaInicial
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.PaginaInicial, esSystemType.String);
			}
		} 
		
		public esQueryItem AcessaWin
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.AcessaWin, esSystemType.String);
			}
		} 
		
		public esQueryItem TrocaSenha
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.TrocaSenha, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLogout
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.DataLogout, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Logotipo
		{
			get
			{
				return new esQueryItem(this, UsuarioMetadata.ColumnNames.Logotipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("UsuarioCollection")]
	public partial class UsuarioCollection : esUsuarioCollection, IEnumerable<Usuario>
	{
		public UsuarioCollection()
		{

		}
		
		public static implicit operator List<Usuario>(UsuarioCollection coll)
		{
			List<Usuario> list = new List<Usuario>();
			
			foreach (Usuario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  UsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new UsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Usuario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Usuario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public UsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new UsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(UsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Usuario AddNew()
		{
			Usuario entity = base.AddNewEntity() as Usuario;
			
			return entity;
		}

		public Usuario FindByPrimaryKey(System.Int32 idUsuario)
		{
			return base.FindByPrimaryKey(idUsuario) as Usuario;
		}


		#region IEnumerable<Usuario> Members

		IEnumerator<Usuario> IEnumerable<Usuario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Usuario;
			}
		}

		#endregion
		
		private UsuarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Usuario' table
	/// </summary>

	[Serializable]
	public partial class Usuario : esUsuario
	{
		public Usuario()
		{

		}
	
		public Usuario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return UsuarioMetadata.Meta();
			}
		}
		
		
		
		override protected esUsuarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new UsuarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public UsuarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new UsuarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(UsuarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private UsuarioQuery query;
	}



	[Serializable]
	public partial class UsuarioQuery : esUsuarioQuery
	{
		public UsuarioQuery()
		{

		}		
		
		public UsuarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class UsuarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected UsuarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.IdUsuario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = UsuarioMetadata.PropertyNames.IdUsuario;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.IdGrupo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = UsuarioMetadata.PropertyNames.IdGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.Nome, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.Login, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.Login;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.Senha, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.Senha;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.Email, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.Email;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.PerguntaLogin, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.PerguntaLogin;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.RespostaLogin, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.RespostaLogin;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataUltimaAlteracao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataUltimaAlteracao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataValidade, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataValidade;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataCadastro, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataCadastro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataUltimoLogin, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataUltimoLogin;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataUltimoBloqueio, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataUltimoBloqueio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.StatusAtivo, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.StatusAtivo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.StatusBloqueado, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.StatusBloqueado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.TentativasAcessoSenha, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = UsuarioMetadata.PropertyNames.TentativasAcessoSenha;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.TentativasAcessoResposta, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = UsuarioMetadata.PropertyNames.TentativasAcessoResposta;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.InicioTentativasAcessoSenha, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.InicioTentativasAcessoSenha;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.InicioTentativasAcessoResposta, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.InicioTentativasAcessoResposta;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.TipoTrava, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = UsuarioMetadata.PropertyNames.TipoTrava;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.PaginaInicial, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.PaginaInicial;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.AcessaWin, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.AcessaWin;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.TrocaSenha, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.TrocaSenha;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.IdCliente, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = UsuarioMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.DataLogout, 24, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = UsuarioMetadata.PropertyNames.DataLogout;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(UsuarioMetadata.ColumnNames.Logotipo, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = UsuarioMetadata.PropertyNames.Logotipo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public UsuarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdGrupo = "IdGrupo";
			 public const string Nome = "Nome";
			 public const string Login = "Login";
			 public const string Senha = "Senha";
			 public const string Email = "Email";
			 public const string PerguntaLogin = "PerguntaLogin";
			 public const string RespostaLogin = "RespostaLogin";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string DataValidade = "DataValidade";
			 public const string DataCadastro = "DataCadastro";
			 public const string DataUltimoLogin = "DataUltimoLogin";
			 public const string DataUltimoBloqueio = "DataUltimoBloqueio";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string StatusBloqueado = "StatusBloqueado";
			 public const string TentativasAcessoSenha = "TentativasAcessoSenha";
			 public const string TentativasAcessoResposta = "TentativasAcessoResposta";
			 public const string InicioTentativasAcessoSenha = "InicioTentativasAcessoSenha";
			 public const string InicioTentativasAcessoResposta = "InicioTentativasAcessoResposta";
			 public const string TipoTrava = "TipoTrava";
			 public const string PaginaInicial = "PaginaInicial";
			 public const string AcessaWin = "AcessaWin";
			 public const string TrocaSenha = "TrocaSenha";
			 public const string IdCliente = "IdCliente";
			 public const string DataLogout = "DataLogout";
			 public const string Logotipo = "Logotipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdUsuario = "IdUsuario";
			 public const string IdGrupo = "IdGrupo";
			 public const string Nome = "Nome";
			 public const string Login = "Login";
			 public const string Senha = "Senha";
			 public const string Email = "Email";
			 public const string PerguntaLogin = "PerguntaLogin";
			 public const string RespostaLogin = "RespostaLogin";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string DataValidade = "DataValidade";
			 public const string DataCadastro = "DataCadastro";
			 public const string DataUltimoLogin = "DataUltimoLogin";
			 public const string DataUltimoBloqueio = "DataUltimoBloqueio";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string StatusBloqueado = "StatusBloqueado";
			 public const string TentativasAcessoSenha = "TentativasAcessoSenha";
			 public const string TentativasAcessoResposta = "TentativasAcessoResposta";
			 public const string InicioTentativasAcessoSenha = "InicioTentativasAcessoSenha";
			 public const string InicioTentativasAcessoResposta = "InicioTentativasAcessoResposta";
			 public const string TipoTrava = "TipoTrava";
			 public const string PaginaInicial = "PaginaInicial";
			 public const string AcessaWin = "AcessaWin";
			 public const string TrocaSenha = "TrocaSenha";
			 public const string IdCliente = "IdCliente";
			 public const string DataLogout = "DataLogout";
			 public const string Logotipo = "Logotipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(UsuarioMetadata))
			{
				if(UsuarioMetadata.mapDelegates == null)
				{
					UsuarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (UsuarioMetadata.meta == null)
				{
					UsuarioMetadata.meta = new UsuarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Login", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Senha", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PerguntaLogin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RespostaLogin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataUltimaAlteracao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataValidade", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataCadastro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataUltimoLogin", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataUltimoBloqueio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("StatusAtivo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("StatusBloqueado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TentativasAcessoSenha", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TentativasAcessoResposta", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("InicioTentativasAcessoSenha", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("InicioTentativasAcessoResposta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoTrava", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PaginaInicial", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AcessaWin", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TrocaSenha", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLogout", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Logotipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Usuario";
				meta.Destination = "Usuario";
				
				meta.spInsert = "proc_UsuarioInsert";				
				meta.spUpdate = "proc_UsuarioUpdate";		
				meta.spDelete = "proc_UsuarioDelete";
				meta.spLoadAll = "proc_UsuarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_UsuarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private UsuarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
