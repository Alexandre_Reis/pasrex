﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common.Exceptions;

namespace Financial.Common
{
    public partial class AgenteMercadoCollection : esAgenteMercadoCollection
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AgenteMercadoCollection));

        /// <summary>
        /// Carrega o objeto AgenteMercadoCollection com os campos IdAgente e Nome.
        /// Filtra por FuncaoAdministrador.Equal("S").
        /// </summary>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAgenteMercadoAdministrador()
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgente, this.Query.Nome)
                 .Where(this.Query.FuncaoAdministrador.Equal("S"));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AgenteMercadoCollection com os campos IdAgente e Nome.
        /// Filtra por FuncaoGestor.Equal("S").
        /// </summary>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAgenteMercadoGestor()
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgente, this.Query.Nome)
                 .Where(this.Query.FuncaoGestor.Equal("S"));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AgenteMercadoCollection com os campos IdAgente e Nome.
        /// Filtra por FuncaoDistribuidor.Equal("S").
        /// </summary>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAgenteMercadoDistribuidor()
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgente, this.Query.Nome)
                 .Where(this.Query.FuncaoDistribuidor.Equal("S"));

            bool retorno = this.Query.Load();

            return retorno;
        }

        public AgenteMercado BuscaAgenteMercadoPorCodigoBovespa(int codigoBovespa) 
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.CodigoBovespa.Equal(codigoBovespa));
            this.Query.Load();

            if (this.Count == 0)
            {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("IdAgenteMercado não cadastrado para o código Bovespa " + codigoBovespa);
                throw new IdAgenteNaoCadastradoException(mensagemAux.ToString());
            }
            else if (this.Count > 1)
            {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("Mais de 1 Agente Mercado encontrado para o código Bovespa " + codigoBovespa);
                throw new IdAgenteNaoCadastradoException(mensagemAux.ToString());
            }
            
            return this[0];
        }

        public AgenteMercado BuscaAgenteMercadoPorNome(string nome)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.Nome.ToUpper().Equal(nome.ToUpper()));
            this.Query.Load();

            if (this.Count == 0)
            {
                return null;
            }
            else
            {
                return this[0];
            }
        }

    }
}
