using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Common
{
	public partial class CotacaoIndiceCollection : esCotacaoIndiceCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoIndiceCollection));

        /// <summary>
        /// Carrega o objeto CotacaoIndiceCollection com os campos Data, Valor.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaCotacao(DateTime dataInicio, DateTime dataFim, int idIndice)
        {

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.Data, this.Query.Valor)
                 .Where(this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.IdIndice == idIndice);

            bool retorno = this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataInicio + "'");
                sql = sql.Replace("@Data2", "'" + dataFim + "'");
                sql = sql.Replace("@IdIndice3", "'" + idIndice + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            return retorno;            
        }

	}
}
