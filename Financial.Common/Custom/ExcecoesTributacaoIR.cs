/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2014 12:01:32
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util.Enums;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Tributo.Custom;

using Financial.Util.Enums;
using Financial.Common.Enums;

namespace Financial.Common
{
	public partial class ExcecoesTributacaoIR : esExcecoesTributacaoIR
	{
        public decimal RetornaAliquotaFundo(Carteira carteira, int idCategoria, DateTime data, DateTime dataAplicacao, ListaTipoInvestidor tipoInvestidor)
        {
            decimal aliquotaIR = 0;
            CalculoTributo calculoTributo = new CalculoTributo();

            ExcecoesTributacaoIRCollection excecoesTributariaIRColl = new ExcecoesTributacaoIRCollection();
            excecoesTributariaIRColl.Query.Where(excecoesTributariaIRColl.Query.Mercado.Equal((int)TipoMercado.Fundos) &
                                                 excecoesTributariaIRColl.Query.TipoClasseAtivo.Equal(idCategoria) &
                                                 excecoesTributariaIRColl.Query.TipoInvestidor.Equal((int)tipoInvestidor) &
                                                 excecoesTributariaIRColl.Query.DataFim.GreaterThanOrEqual(data) &
                                                 excecoesTributariaIRColl.Query.DataInicio.LessThanOrEqual(data));

            if (excecoesTributariaIRColl.Query.Load())
            {
                List<ExcecoesTributacaoIR> lstExcecoesTributacaoIR = new List<ExcecoesTributacaoIR>();
                lstExcecoesTributacaoIR = (List<ExcecoesTributacaoIR>)excecoesTributariaIRColl;

                ExcecoesTributacaoIR excecaoTributariaIR = lstExcecoesTributacaoIR.Find(delegate(ExcecoesTributacaoIR x) { return x.IdCarteira == carteira.IdCarteira.Value; });

                if (!(excecaoTributariaIR != null && excecaoTributariaIR.IdCarteira.HasValue))
                {
                    excecaoTributariaIR = lstExcecoesTributacaoIR.Find(delegate(ExcecoesTributacaoIR x) { return !x.IdCarteira.HasValue; });
                }

                if (excecaoTributariaIR.IsencaoIR.Value == (int)ListaIsencaoIR.Isento)
                    aliquotaIR = 0;
                else
                    aliquotaIR = Convert.ToDecimal(excecaoTributariaIR.AliquotaIR);

            }
            else
            {
                aliquotaIR = carteira.TipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas ? calculoTributo.RetornaAliquotaIRCurtoPrazo(dataAplicacao, data) : calculoTributo.RetornaAliquotaIRLongoPrazo(dataAplicacao, data);
            }

            return aliquotaIR;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ativo"></param>
        /// <param name="data"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="tipoInvestidor"></param>
        public void RetornaAliquota(string ativo, DateTime data, TipoMercado tipoMercado, ListaTipoInvestidor tipoInvestidor)
        {
            this.QueryReset();

            if (tipoMercado.Equals(TipoMercado.RendaFixa))
            {
                int idTituloRendaFixa = Convert.ToInt32(ativo);

                this.Query.Select(this.Query.IsencaoIR, this.Query.AliquotaIR);
                this.Query.Where(this.Query.DataFim.GreaterThanOrEqual(data),
                             this.Query.DataInicio.LessThanOrEqual(data),
                             this.Query.TipoInvestidor.Equal(tipoInvestidor.GetHashCode()),
                             this.Query.IdTituloRendaFixa.Equal(idTituloRendaFixa));
            }
            else if (tipoMercado.Equals(TipoMercado.Fundos))
            {
                int idCarteira = Convert.ToInt32(ativo);

                this.Query.Select(this.Query.IsencaoIR, this.Query.AliquotaIR);
                this.Query.Where(this.Query.DataFim.GreaterThanOrEqual(data),
                             this.Query.DataInicio.LessThanOrEqual(data),
                             this.Query.TipoInvestidor.Equal(tipoInvestidor.GetHashCode()),
                             this.Query.IdCarteira.Equal(idCarteira));
            }
            else if (tipoMercado.Equals(TipoMercado.Bolsa))
            {
                this.Query.Select(this.Query.IsencaoIR, this.Query.AliquotaIR);
                this.Query.Where(this.Query.DataFim.GreaterThanOrEqual(data),
                             this.Query.DataInicio.LessThanOrEqual(data),
                             this.Query.TipoInvestidor.Equal(tipoInvestidor.GetHashCode()),
                             this.Query.CdAtivoBolsa.Equal(ativo));
            }

            this.Query.Load();

        }

	}
}
