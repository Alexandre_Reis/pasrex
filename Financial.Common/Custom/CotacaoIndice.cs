﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Exceptions;
using Financial.BMF;
using Financial.Common.Enums;
using log4net;
using Financial.Util;
using Financial.Interfaces.Import.BMF;
using Financial.Interfaces.Import.BMF.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.RendaFixa;
using System.Net;
using System.IO;
using System.Globalization;
using Financial.Interfaces.Import.Offshore;
using Financial.Fundo;

namespace Financial.Common
{
    public partial class CotacaoIndice : esCotacaoIndice
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoIndice));

        /// <summary>
        /// Retorna a cotação do índice na primeira data disponível, referente ao mês da data passada.
        /// Exception CotacaoIndiceNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idIndice"></param>
        /// <param name="data"></param>
        /// <returns>valor do índice na data</returns>
        public decimal RetornaCotacaoIndiceMes(int idIndice, DateTime data)
        {
            DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.Query.Select(cotacaoIndice.Query.Valor);
            cotacaoIndice.Query.Where(cotacaoIndice.Query.IdIndice.Equal(idIndice),
                                      cotacaoIndice.Query.Data.Equal(primeiroDiaMes));
            if (!cotacaoIndice.Query.Load())
            {
                Indice indice = new Indice();
                indice.LoadByPrimaryKey((short)idIndice);

                throw new CotacaoIndiceNaoCadastradoException("Cotação de " + indice.Descricao + " não cadastrada na data " + primeiroDiaMes);
            }

            return cotacaoIndice.Valor.Value;

        }

        /// <summary>
        /// Retorna a cotação do índice na data.
        /// Exception CotacaoIndiceNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idIndice"></param>
        /// <param name="data"></param>
        /// <returns>valor do índice na data</returns>
        public decimal BuscaCotacaoIndice(int idIndice, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.Valor)
                 .Where(this.query.IdIndice == idIndice,
                        this.query.Data == data);

            this.Query.Load();

            if (!this.es.HasData)
            {
                Indice indice = new Indice();
                indice.LoadByPrimaryKey((short)idIndice);
                if (indice.es.HasData)
                {
                    throw new CotacaoIndiceNaoCadastradoException("Cotação de " + indice.Descricao + " não cadastrada na data " + data);
                }
                else
                {
                    throw new CotacaoIndiceNaoCadastradoException("Cotação de " + idIndice + " não cadastrada na data " + data);
                }
            }

            return this.Valor.Value;

        }

        /// <summary>
        /// Carrega na CotacaoIndice as cotações de Ptax e Ouro vindas no BdPregao.
        /// </summary>
        /// <param name="data">Data de referência do BdPregao</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoIndiceBdPregao(DateTime data, string pathArquivo)
        {
            BdPregao bdPregao = new BdPregao();
            BdPregaoCollection bdPregaoCollection = bdPregao.ProcessaBdPregao(data, pathArquivo);

            // Transação
            using (esTransactionScope scope = new esTransactionScope())
            {
                bool achouPtax800Venda = false;
                bool achouPtaxRef = false;
                bool achouOuro = false;
                for (int i = 0; i < bdPregaoCollection.CollectionBdPregao.Count; i++)
                {
                    bdPregao = bdPregaoCollection.CollectionBdPregao[i];

                    if (AtivoBMF.IsAtivoBond(bdPregao.CdAtivoBMF))
                    {
                        achouPtax800Venda = true;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        #region Insert / Update na CotacaoIndice
                        if (cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.PTAX_800VENDA))
                        {
                            cotacaoIndice.Valor = bdPregao.Ptax;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = data;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.PTAX_800VENDA;
                            cotacaoIndice.Valor = bdPregao.Ptax;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }

                    if (bdPregao.CdAtivoBMF == "ICF")
                    {
                        achouPtaxRef = true;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        #region Insert / Update na CotacaoIndice
                        if (cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.PTAX_REF))
                        {
                            cotacaoIndice.Valor = bdPregao.Ptax;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = data;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.PTAX_REF;
                            cotacaoIndice.Valor = bdPregao.Ptax;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }

                    if (bdPregao.CdAtivoBMF == "OZ1") //Médio e Fechamento
                    {
                        achouOuro = true;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        #region Insert / Update na CotacaoIndice

                        if (cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.OUROBMF_FECHA))
                        {
                            cotacaoIndice.Valor = bdPregao.CotacaoFechamento;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = data;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.OUROBMF_FECHA;
                            cotacaoIndice.Valor = bdPregao.CotacaoFechamento;
                            cotacaoIndice.Save();
                        }

                        cotacaoIndice = new CotacaoIndice();
                        if (cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.OUROBMF_MEDIO))
                        {
                            cotacaoIndice.Valor = bdPregao.CotacaoMedio;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = data;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.OUROBMF_MEDIO;
                            cotacaoIndice.Valor = bdPregao.CotacaoMedio;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }

                    if (achouOuro)
                    { break; }
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Carrega algumas das cotações vindas no arquivo TarPar na CotacaoIndice.
        /// </summary>
        /// <param name="data">Data de referência do TarPar</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaCotacaoIndiceTarPar(DateTime data, string pathArquivo)
        {
            TarPar tarPar = new TarPar();
            TarParCollection tarParCollection = tarPar.ProcessaTarPar(data, pathArquivo);         

            // Transação
            using (esTransactionScope scope = new esTransactionScope())
            {
                for (int i = 0; i < tarParCollection.CollectionTarPar.Count; i++)
                {
                    tarPar = tarParCollection.CollectionTarPar[i];

                    DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(tarPar.DataReferencia, 0);   

                    if (tarPar.Parametro == "IGPM_MEDIA_MENSAL")
                    {
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();

                        #region Insert / Update na CotacaoIndice (IGPM)
                        if (cotacaoIndice.LoadByPrimaryKey(primeiroDiaMes, ListaIndiceFixo.IGPM))
                        {
                            cotacaoIndice.Valor = tarPar.Valor;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = primeiroDiaMes;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.IGPM;
                            cotacaoIndice.Valor = tarPar.Valor;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }

                    if (tarPar.Parametro == "TAMANHO_CONTRATO_IDI2003")
                    {
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();

                        #region Insert / Update na CotacaoIndice (IDI)
                        if (cotacaoIndice.LoadByPrimaryKey(primeiroDiaMes, ListaIndiceFixo.IDI))
                        {
                            cotacaoIndice.Valor = tarPar.Valor;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = primeiroDiaMes;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.IDI;
                            cotacaoIndice.Valor = tarPar.Valor;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }
                }
                scope.Complete();
            }

        }

        /// <summary>
        /// Carrega algumas das cotações vindas da Bloomberg.
        /// </summary>
        /// <param name="data">Data de referência do TarPar</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaCotacaoIndiceBloomberg(DateTime dataInicio, DateTime dataFim)
        {
            CotacaoBloomberg cotacaoBloomberg = new CotacaoBloomberg();

            List<string> listaAtivos = new List<string>();
            List<string> listaEspec = new List<string>();

            listaAtivos.Add("EURUSD");
            listaEspec.Add("CUR");
            List<CotacaoBloomberg> listaCotacao = cotacaoBloomberg.CarregaCotacaoBloomberg(listaAtivos, listaEspec, dataInicio, dataFim);

            foreach (CotacaoBloomberg cotacaoBloombergDados in listaCotacao)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();

                if (cotacaoBloombergDados.CdAtivo.Contains("EURUSD"))
                {
                    #region Insert / Update na CotacaoIndice (EUR_DOLAR)
                    if (cotacaoIndice.LoadByPrimaryKey(cotacaoBloombergDados.Data, ListaIndiceFixo.EURO_DOLAR))
                    {
                        cotacaoIndice.Valor = cotacaoBloombergDados.Pu;
                        cotacaoIndice.Save();
                    }
                    else
                    {
                        cotacaoIndice = new CotacaoIndice();
                        cotacaoIndice.Data = cotacaoBloombergDados.Data;
                        cotacaoIndice.IdIndice = ListaIndiceFixo.EURO_DOLAR;
                        cotacaoIndice.Valor = cotacaoBloombergDados.Pu;
                        cotacaoIndice.Save();
                    }
                    #endregion
                }
            }

        }

        /// <summary>
        /// Carrega algumas das cotações vindas no arquivo Bdin na CotacaoIndice.
        /// </summary>
        /// <param name="data">Data de referência do Bdin</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoIndiceBdin(DateTime data, string pathArquivo)
        {
            Bdin bdin = new Bdin();
            BdinCollection indicesCollection = new BdinCollection();
            BdinCollection bdinCollection = bdin.ProcessaBdin(data, pathArquivo);
            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();

            // Filtra a Collection somente os Indices - tipo registro = 01
            indicesCollection.CollectionBdin = bdinCollection.CollectionBdin.FindAll(bdin.FilterBdinByIndice);

            for (int i = 0; i < indicesCollection.CollectionBdin.Count; i++)
            {
                bdin = indicesCollection.CollectionBdin[i];

                if (bdin.IsIndiceIBOVESPA())
                {
                    #region Ibovespa
                    CotacaoIndice cotacaoIndiceMedio = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBOVESPA Medio)
                    if (cotacaoIndiceMedio.LoadByPrimaryKey(data, ListaIndiceFixo.IBOVESPA_MEDIO))
                    {
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    else
                    {
                        cotacaoIndiceMedio = new CotacaoIndice();
                        cotacaoIndiceMedio.IdIndice = ListaIndiceFixo.IBOVESPA_MEDIO;
                        cotacaoIndiceMedio.Data = data;
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    #endregion

                    CotacaoIndice cotacaoIndiceFechamento = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBOVESPA Fechamento)
                    if (cotacaoIndiceFechamento.LoadByPrimaryKey(data, ListaIndiceFixo.IBOVESPA_FECHA))
                    {
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        cotacaoIndiceFechamento = new CotacaoIndice();
                        cotacaoIndiceFechamento.IdIndice = ListaIndiceFixo.IBOVESPA_FECHA;
                        cotacaoIndiceFechamento.Data = data;
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    #endregion

                    // Adiciona 2 registros na collection
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceMedio);
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceFechamento);
                    #endregion
                }
                else if (bdin.IsIndiceIBRX_BRASIL())
                {
                    #region IBRX_BRASIL
                    CotacaoIndice cotacaoIndiceMedio = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBRX_BRASIL Medio)
                    if (cotacaoIndiceMedio.LoadByPrimaryKey(data, ListaIndiceFixo.IBRX_MEDIO))
                    {
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    else
                    {
                        cotacaoIndiceMedio = new CotacaoIndice();
                        cotacaoIndiceMedio.IdIndice = ListaIndiceFixo.IBRX_MEDIO;
                        cotacaoIndiceMedio.Data = data;
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    #endregion

                    CotacaoIndice cotacaoIndiceFechamento = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBRX_BRASIL Fechamento)
                    if (cotacaoIndiceFechamento.LoadByPrimaryKey(data, ListaIndiceFixo.IBRX_FECHA))
                    {
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        cotacaoIndiceFechamento = new CotacaoIndice();
                        cotacaoIndiceFechamento.IdIndice = ListaIndiceFixo.IBRX_FECHA;
                        cotacaoIndiceFechamento.Data = data;
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    #endregion

                    // Adiciona 2 registros na collection
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceMedio);
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceFechamento);
                    #endregion
                }
                else if (bdin.IsIndiceIBRX_50())
                {
                    #region IBRX_50
                    CotacaoIndice cotacaoIndiceMedio = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBRX_50 Medio)
                    if (cotacaoIndiceMedio.LoadByPrimaryKey(data, ListaIndiceFixo.IBRX50_MEDIO))
                    {
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    else
                    {
                        cotacaoIndiceMedio = new CotacaoIndice();
                        cotacaoIndiceMedio.IdIndice = ListaIndiceFixo.IBRX50_MEDIO;
                        cotacaoIndiceMedio.Data = data;
                        cotacaoIndiceMedio.Valor = bdin.IndiceMedio;
                    }
                    #endregion

                    CotacaoIndice cotacaoIndiceFechamento = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBRX_50 Fechamento)
                    if (cotacaoIndiceFechamento.LoadByPrimaryKey(data, ListaIndiceFixo.IBRX50_FECHA))
                    {
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        cotacaoIndiceFechamento = new CotacaoIndice();
                        cotacaoIndiceFechamento.IdIndice = ListaIndiceFixo.IBRX50_FECHA;
                        cotacaoIndiceFechamento.Data = data;
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    #endregion

                    // Adiciona 2 registros na collection
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceMedio);
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceFechamento);
                    #endregion
                }
                else if (bdin.IsIndiceIBRA())
                {
                    #region IBRA
                    CotacaoIndice cotacaoIndiceFechamento = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IBRA Fechamento)
                    if (cotacaoIndiceFechamento.LoadByPrimaryKey(data, ListaIndiceFixo.IBRA))
                    {
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        Indice indiceExiste = new Indice();
                        if (indiceExiste.LoadByPrimaryKey(ListaIndiceFixo.IBRA))
                        {
                            cotacaoIndiceFechamento = new CotacaoIndice();
                            cotacaoIndiceFechamento.IdIndice = ListaIndiceFixo.IBRA;
                            cotacaoIndiceFechamento.Data = data;
                            cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                        }                        
                    }
                    #endregion

                    // Adiciona registro na collection
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceFechamento);
                    #endregion
                }
                else if (bdin.IsIndiceIFIX())
                {
                    #region IFIX
                    CotacaoIndice cotacaoIndiceFechamento = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IFIX)
                    if (cotacaoIndiceFechamento.LoadByPrimaryKey(data, ListaIndiceFixo.IFIX))
                    {
                        cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        Indice indiceExiste = new Indice();
                        if (indiceExiste.LoadByPrimaryKey(ListaIndiceFixo.IFIX))
                        {
                            cotacaoIndiceFechamento = new CotacaoIndice();
                            cotacaoIndiceFechamento.IdIndice = ListaIndiceFixo.IFIX;
                            cotacaoIndiceFechamento.Data = data;
                            cotacaoIndiceFechamento.Valor = bdin.IndiceFechamento;
                        }
                    }
                    #endregion

                    // Adiciona registro na collection
                    cotacaoIndiceCollection.AttachEntity(cotacaoIndiceFechamento);
                    #endregion
                }
                else if (bdin.IsIndiceIDIVIDENDOS())
                {
                    CotacaoIndice cotacaoIDIVIDENDOS = new CotacaoIndice();
                    #region Insert / Update na CotacaoIndice (IDIVIDENDOS)
                    if (cotacaoIDIVIDENDOS.LoadByPrimaryKey(data, ListaIndiceFixo.IDIVIDENDOS))
                    {
                        cotacaoIDIVIDENDOS.Valor = bdin.IndiceFechamento;
                    }
                    else
                    {
                        cotacaoIDIVIDENDOS = new CotacaoIndice();
                        cotacaoIDIVIDENDOS.IdIndice = ListaIndiceFixo.IDIVIDENDOS;
                        cotacaoIDIVIDENDOS.Data = data;
                        cotacaoIDIVIDENDOS.Valor = bdin.IndiceFechamento;
                    }
                    #endregion
                }
            }

            if (cotacaoIndiceCollection.HasData)
            {
                cotacaoIndiceCollection.Save();
            }
        }

        /// <summary>
        /// Carrega algumas das cotações vindas no arquivo Indic na CotacaoIndice.
        /// </summary>
        /// <param name="data">Data de referência do Indic</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaCotacaoIndiceIndic(DateTime data, string pathArquivo)
        {
            Indic indic = new Indic();
            IndicCollection cdiCollection = new IndicCollection();
            IndicCollection indicCollection = indic.ProcessaIndic(data, pathArquivo);
            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();

            // Filtra a Collection somente alguns CDis
            cdiCollection.CollectionIndic = indicCollection.CollectionIndic.FindAll(indic.FilterIndicByCodigoGrupo);

            for (int i = 0; i < cdiCollection.CollectionIndic.Count; i++)
            {
                bool importar = true;

                indic = cdiCollection.CollectionIndic[i];
                DateTime dataReferencia = indic.DataReferencia;

                indic.CodigoIndicador = indic.CodigoIndicador.Trim();

                if (indic.CodigoIndicador.Contains("TR 0") || indic.CodigoIndicador.Contains("TR 1") ||
                    indic.CodigoIndicador.Contains("TR 2") || indic.CodigoIndicador.Contains("TR 3"))
                {
                    importar = false;

                    int diaTR = Convert.ToInt32(indic.CodigoIndicador.Replace("TR", "").Trim());
                    if (dataReferencia.Day == diaTR)
                    {
                        importar = true;
                    }
                    else if (indic.CodigoIndicador == "TR 01")
                    {
                        if (dataReferencia == Calendario.RetornaPrimeiroDiaUtilMes(dataReferencia))
                        {
                            dataReferencia = Calendario.RetornaPrimeiroDiaCorridoMes(dataReferencia, 0);
                            importar = true;
                        }
                    }

                    indic.CodigoIndicador = "TR";
                }

                if (importar)
                {
                    short? indice = null;
                    #region Conversão de IdIndice
                    switch (indic.CodigoIndicador)
                    {
                        case CDISIndic.DI1:
                            indice = ListaIndiceFixo.CDI;
                            break;
                        case CDISIndic.DOL_T1:
                            indice = ListaIndiceFixo.PTAX_800VENDA;
                            break;
                        case CDISIndic.DOL_T2:
                            indice = ListaIndiceFixo.PTAX_800COMPRA;
                            break;
                        case CDISIndic.DOL_T3:
                            indice = ListaIndiceFixo.PTAX_800MEDIO;
                            break;
                        case CDISIndic.DOL_D1:
                            indice = ListaIndiceFixo.PTAX_REF;
                            break;
                        case CDISIndic.DOL_D2:
                            indice = ListaIndiceFixo.PTAX_REF_2;
                            break;
                        case CDISIndic.SEL:
                            indice = ListaIndiceFixo.SELIC;
                            break;
                        case CDISIndic.TR:
                            indice = ListaIndiceFixo.TR;
                            break;
                    }
                    #endregion

                    if (indice.HasValue)
                    {
                        CotacaoIndice cotacao = new CotacaoIndice();
                        #region Insert / Update na CotacaoIndice
                        if (cotacao.LoadByPrimaryKey(dataReferencia, indice.Value))
                        {
                            cotacao.Valor = indic.Valor;
                        }
                        else
                        {
                            cotacao = new CotacaoIndice();
                            cotacao.IdIndice = indice;
                            cotacao.Data = dataReferencia;
                            cotacao.Valor = indic.Valor;
                        }
                        #endregion

                        // Adiciona  na collection
                        cotacaoIndiceCollection.AttachEntity(cotacao);
                    }
                }
            }

            cotacaoIndiceCollection.Save();
        }

        /// <summary>
        /// Carrega algumas das cotações vindas no arquivo de cotacoes do site IBGE.gov na CotacaoIndice.
        /// </summary>
        /// <param name="data">Data de referência do TarPar</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaCotacaoIndiceIBGE(DateTime data, string pathArquivo)
        {
            CotacaoIBGE cotacaoIBGEProcessa = new CotacaoIBGE();
            List<CotacaoIBGE> listaCotacaoIBGE = cotacaoIBGEProcessa.ProcessaCotacaoIBGE(data, pathArquivo);

            DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);            

            // Transação
            using (esTransactionScope scope = new esTransactionScope())
            {
                for (int i = 0; i < listaCotacaoIBGE.Count; i++)
                {
                    CotacaoIBGE cotacaoIBGE = listaCotacaoIBGE[i];

                    if (cotacaoIBGE.Indice == ListaIndiceFixo.IPCA)
                    {
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();

                        #region Insert / Update na CotacaoIndice (IPCA)
                        if (cotacaoIndice.LoadByPrimaryKey(primeiroDiaMes, ListaIndiceFixo.IPCA))
                        {
                            cotacaoIndice.Valor = cotacaoIBGE.Cotacao;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = primeiroDiaMes;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.IPCA;
                            cotacaoIndice.Valor = cotacaoIBGE.Cotacao;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }
                    else if (cotacaoIBGE.Indice == ListaIndiceFixo.INPC)
                    {
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();

                        #region Insert / Update na CotacaoIndice (INPC)
                        if (cotacaoIndice.LoadByPrimaryKey(primeiroDiaMes, ListaIndiceFixo.INPC))
                        {
                            cotacaoIndice.Valor = cotacaoIBGE.Cotacao;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = primeiroDiaMes;
                            cotacaoIndice.IdIndice = ListaIndiceFixo.INPC;
                            cotacaoIndice.Valor = cotacaoIBGE.Cotacao;
                            cotacaoIndice.Save();
                        }
                        #endregion
                    }

                }
                scope.Complete();
            }

        }

        /// <summary>
        /// Carrega as cotações projetadas disponíveis no site da Andima para IPCA e IGPM
        /// </summary>
        /// <param name="data">Data de referência do TarPar</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaCotacaoIndiceAndima(DateTime data, string pathArquivo)
        {
            AnbimaIndicadores anbimaIndicadoresProcessar = new AnbimaIndicadores();
            List<AnbimaIndicadores> lista = anbimaIndicadoresProcessar.ProcessaAnbimaIndicadores(data, pathArquivo);

            // Transação
            using (esTransactionScope scope = new esTransactionScope())
            {
                foreach (AnbimaIndicadores anbimaIndicadores in lista)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();

                    #region Insert / Update na CotacaoIndice (IPCA ou IGPM)
                    int idIndice = 0;
                    decimal cotacao = 0;
                    if (anbimaIndicadores.Indice.Contains("IPCA"))
                    {
                        idIndice = ListaIndiceFixo.IPCA;
                        cotacao = Financial.Util.CalculoFinanceiro.RetornaNumeroIndice(idIndice, anbimaIndicadores.Data, anbimaIndicadores.Valor);                        
                    }
                    else if (anbimaIndicadores.Indice.Contains("IGP-M"))
                    {
                        idIndice = ListaIndiceFixo.IGPM;
                        cotacao = Financial.Util.CalculoFinanceiro.RetornaNumeroIndice(idIndice, anbimaIndicadores.Data, anbimaIndicadores.Valor);
                    }
                    else if (anbimaIndicadores.Indice.Contains("Euro"))
                    {
                        idIndice = ListaIndiceFixo.EURO;
                        cotacao = anbimaIndicadores.Valor;
                    }

                    if (idIndice != 0)
                    {
                        if (cotacaoIndice.LoadByPrimaryKey(anbimaIndicadores.Data, (short)idIndice))
                        {
                            cotacaoIndice.Valor = cotacao;
                            cotacaoIndice.Save();
                        }
                        else
                        {
                            cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.Data = anbimaIndicadores.Data;
                            cotacaoIndice.IdIndice = (short)idIndice;
                            cotacaoIndice.Valor = cotacao;
                            cotacaoIndice.Save();
                        }
                    }    
                    #endregion
                }
                scope.Complete();
            }

        }

        /// <summary>
        /// Insere a Cotação do Índice Ima-B / Ima-C/ Ima-S e IRF-M
        /// </summary>
        /// <param name="data"></param>
        public void CarregaCotacaoIndiceIMA(DateTime data) {

            const string url = "http://www.anbima.com.br/ima/IMA-geral-down.asp";
            
            //aqui
            string paramData1 = data.ToString("ddMMyyyy");
            string paramData2 = data.ToString("dd/MM/yyyy");
            
            string dataPost = "Titulo_1=ima-geral&Consulta_1=Carteira&Idioma=PT&Dt_Ref=" + paramData2 + "&DataIni=" + paramData2 +
                "&DataFim=" + paramData2 + "&Indice=ima-geral&Consulta=Carteira&saida=txt";

            try {
                Uri uri = new Uri(url);

                if (uri.Scheme == Uri.UriSchemeHttp) {
                    #region Dados do Arquivo
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
                    request.Method = WebRequestMethods.Http.Post;
                    request.ContentLength = dataPost.Length;
                    request.ContentType = "application/x-www-form-urlencoded";


                    string proxyServer = Financial.Util.Utilitario.WebProxyInfo.ProxyServer;
                    string internetProxy = System.Configuration.ConfigurationSettings.AppSettings["InternetProxy"];

                    if (!String.IsNullOrEmpty(proxyServer) || !String.IsNullOrEmpty(internetProxy))
                    {
                        IWebProxy proxy = Financial.Util.Utilitario.GetWebProxy();
                        request.Proxy = proxy;
                    }


                    //
                    StreamWriter writer = new StreamWriter(request.GetRequestStream());
                    writer.Write(dataPost);
                    writer.Close();
                    //
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("ISO-8859-1"));
                    string tmp = reader.ReadToEnd();
                    response.Close();
                    #endregion

                    #region Valor do Indice
                    string[] linhas = tmp.Split("\n".ToCharArray());
                    if (linhas.Length >= 13)
                    {

                        /* Localização 
                            IRF-M-1 - linha 4 - coluna 5
                            IRF-M-1+ - linha 5 - coluna 5
                            IRF-M - linha 6 - coluna 5
                            
                            IMA-C - linha 7 - coluna 5                      
                            
                            IMA-B-5 - linha 8 - coluna 5
                            IMA-B-5+ - linha 9 - coluna 5
                            IMA-B - linha 10 - coluna 5
                           
                            IMA-S - linha 11 - coluna 5  
                                                 
                            IMA-GERAL - linha 13 - coluna 5
                        */
                        string[] dados_IRF_M_1 = linhas[3].Split("@".ToCharArray()); // linha 4
                        string[] dados_IRF_M_1_MAIS = linhas[4].Split("@".ToCharArray()); // linha 5
                        string[] dados_IRF_M = linhas[5].Split("@".ToCharArray()); // linha 6
                        
                        string[] dados_IMA_C = linhas[6].Split("@".ToCharArray()); // linha 7

                        string[] dados_IMA_B_5 = linhas[7].Split("@".ToCharArray()); // linha 8
                        string[] dados_IMA_B_5_MAIS = linhas[8].Split("@".ToCharArray()); // linha 9
                        string[] dados_IMA_B = linhas[9].Split("@".ToCharArray()); // linha 10
                        
                        string[] dados_IMA_S = linhas[10].Split("@".ToCharArray()); // linha 11

                        string[] dados_IMA_GERAL_EX_C = linhas[11].Split("@".ToCharArray()); // linha 12

                        string[] dados_IMA_GERAL = linhas[12].Split("@".ToCharArray()); // linha 13
                        //
                        NumberFormatInfo provider = new NumberFormatInfo();
                        provider.NumberGroupSeparator = ".";
                        provider.NumberDecimalSeparator = ",";
                        //
                        if (dados_IRF_M.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IRFM, Convert.ToDecimal(dados_IRF_M[4], provider), data);
                        }
                        if (dados_IRF_M_1.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IRFM_1, Convert.ToDecimal(dados_IRF_M_1[4], provider), data);
                        }
                        if (dados_IRF_M_1_MAIS.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IRFM_1_MAIS, Convert.ToDecimal(dados_IRF_M_1_MAIS[4], provider), data);
                        }
                        if (dados_IMA_C.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_C, Convert.ToDecimal(dados_IMA_C[4], provider), data);
                        }
                        if (dados_IMA_B.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_B, Convert.ToDecimal(dados_IMA_B[4], provider), data);
                        }
                        if (dados_IMA_B_5.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_B_5, Convert.ToDecimal(dados_IMA_B_5[4], provider), data);
                        }
                        if (dados_IMA_B_5_MAIS.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_B_5_MAIS, Convert.ToDecimal(dados_IMA_B_5_MAIS[4], provider), data);
                        }
                        if (dados_IMA_S.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_S, Convert.ToDecimal(dados_IMA_S[4], provider), data);
                        }
                        if (dados_IMA_GERAL_EX_C.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_GERAL_EX_C, Convert.ToDecimal(dados_IMA_GERAL_EX_C[4], provider), data);
                        }
                        if (dados_IMA_GERAL.Length >= 5)
                        {
                            this.InsereCotacaoIndice((int)ListaIndiceFixo.IMA_GERAL, Convert.ToDecimal(dados_IMA_GERAL[4], provider), data);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception e1) {
                throw new Exception(e1.Message);
            }
        }

        /// <summary>
        /// Insere uma cotacaoIndice se não existir
        /// Se já existir faz Update
        /// </summary>
        /// <param name="idIndice"></param>
        /// <param name="valor"></param>
        /// <param name="data"></param>
        private void InsereCotacaoIndice(int idIndice, decimal valor, DateTime data)
        {

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            CotacaoIndice cotacaoIndiceInserir = new CotacaoIndice();
            // Update
            if (cotacaoIndice.LoadByPrimaryKey(data, (short)idIndice))
            {
                cotacaoIndice.Valor = valor; // Atualiza Somente Valor
                cotacaoIndice.Save();
            }
            // Insert
            else
            {
                cotacaoIndiceInserir.IdIndice = (short)idIndice;
                cotacaoIndiceInserir.Valor = valor;
                cotacaoIndiceInserir.Data = data;
                //
                cotacaoIndiceInserir.Save();
            }
        }

        /// <summary>
        /// atualiza fatordiario e fator acumulado da série histórica
        /// </summary>
        /// <param name="idIndice"></param>
        public void AtualizaFatorIndice(short idIndice)
        {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey(idIndice);

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.IdIndice.Equal(idIndice));
            cotacaoIndiceCollection.Query.OrderBy(cotacaoIndiceCollection.Query.Data.Ascending);

            if (cotacaoIndiceCollection.Query.Load())
            {
                decimal retornoIndice;
                decimal fatorDiario;
                decimal fatorAcumulado = 1;
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(cotacaoIndiceCollection[0].Data.Value, 1);
                DateTime dataAtual = cotacaoIndiceCollection[0].Data.Value;
                CalculoMedida calculoMedida = new CalculoMedida();

                for (int i = 0; i < cotacaoIndiceCollection.Count; i++)
                {
                    try
                    {
                        if (indice.Tipo.Value == (byte)TipoIndice.Percentual && (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Diario ||
                                                                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Diario_1 ||
                                                                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Diario_2 ||
                                                                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Diario_3))
                        {
                            fatorDiario = CalculoFinanceiro.CalculaFatorIndicePercentual(dataAtual, dataAtual, indice, 100, false);
                        }
                        else
                        {
                            retornoIndice = calculoMedida.CalculaRetornoIndice(dataAnterior, dataAtual, indice);
                            fatorDiario = (retornoIndice / 100) + 1;
                        }

                        fatorAcumulado = fatorAcumulado * fatorDiario;
                        cotacaoIndiceCollection[i].FatorDiario = fatorDiario;
                        cotacaoIndiceCollection[i].FatorAcumulado = fatorAcumulado;
                    }
                    catch { }

                    dataAnterior = dataAtual;

                    if (i + 1 < cotacaoIndiceCollection.Count)
                    {
                        dataAtual = (DateTime)cotacaoIndiceCollection[i + 1].Data.Value;
                    }
                }

                cotacaoIndiceCollection.Save();
            }
        }

    }
}
