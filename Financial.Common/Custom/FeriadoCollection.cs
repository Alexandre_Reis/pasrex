using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common {
    public partial class FeriadoCollection : esFeriadoCollection 
    {
        /// <summary>
        /// Carrega o objeto FeriadoCollection com o campo Data.
        /// OrderBy(this.Query.Data.Ascending).
        /// </summary>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaFeriado(int idLocal, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Data)
                 .Where(this.Query.IdLocal.Equal(idLocal),
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim));
            this.Query.OrderBy(this.Query.Data.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}
