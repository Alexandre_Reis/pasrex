using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;

namespace Financial.Common
{
    public partial class Indice : esIndice
    {

        private IndiceCollection RetornaIndicesComCotacoes(DateTime? data)
        {
            DateTime dataBase = DateTime.Now;
            CotacaoIndice cotacaoIndice = new CotacaoIndice();

            if (data == null)
            {

                cotacaoIndice.Query.Select(cotacaoIndice.Query.Data.Max());
                cotacaoIndice.Query.Load();

                dataBase = cotacaoIndice.Data.HasValue ? cotacaoIndice.Data.Value : DateTime.Now;

            }
            else
            {
                dataBase = (DateTime)data;
            }

            DateTime data30Dias = dataBase.AddMonths(-3);

            IndiceQuery indiceQuery = new IndiceQuery("I");
            CotacaoIndiceQuery cotacaoIndiceQuery = new CotacaoIndiceQuery("C");

            indiceQuery.Select(indiceQuery.IdIndice.Distinct(), indiceQuery.Descricao);
            indiceQuery.InnerJoin(cotacaoIndiceQuery).On(cotacaoIndiceQuery.IdIndice == indiceQuery.IdIndice);
            indiceQuery.Where(cotacaoIndiceQuery.Data.GreaterThanOrEqual(data30Dias));
            indiceQuery.OrderBy(indiceQuery.Descricao.Ascending);
            indiceQuery.es.Distinct = true;

            IndiceCollection indiceCollection = new IndiceCollection();
            indiceCollection.Load(indiceQuery);


            indiceQuery = new IndiceQuery("I");
            indiceQuery.InnerJoin(cotacaoIndiceQuery).On(cotacaoIndiceQuery.IdIndice == indiceQuery.IdIndiceBase);
            indiceQuery.Where(cotacaoIndiceQuery.Data.GreaterThanOrEqual(data30Dias));
            indiceQuery.OrderBy(indiceQuery.Descricao.Ascending);
            indiceQuery.es.Distinct = true;

            IndiceCollection indiceBaseCollection = new IndiceCollection();
            indiceBaseCollection.Load(indiceQuery);

            indiceCollection.Combine(indiceBaseCollection);

            return indiceCollection;
        }

        public IndiceCollection RetornaCollectionIndicesComCotacoes()
        {
            DateTime? dataNull = null;
            IndiceCollection indiceCollection = this.RetornaIndicesComCotacoes(dataNull);
            IndiceCollection indiceCollectionFinal = new IndiceCollection();

            List<int> idIndicesUsados = new List<int>();
            foreach (Indice indice in indiceCollection)
            {
                if (!idIndicesUsados.Contains(indice.IdIndice.Value))
                {
                    Indice indiceColl = indiceCollectionFinal.AddNew();
                    indiceColl.Descricao = indice.Descricao;
                    indiceColl.IdIndice = indice.IdIndice;
                    indiceColl.IdIndiceBase = indice.IdIndiceBase;
                    indiceColl.Percentual = indice.Percentual;
                    indiceColl.Taxa = indice.Taxa;
                    indiceColl.Tipo = indice.Tipo;
                    indiceColl.TipoDivulgacao = indice.TipoDivulgacao;

                    idIndicesUsados.Add(indice.IdIndice.Value);
                }
            }

            return indiceCollectionFinal;
        }

        public DataTable RetornaDataTableIndicesComCotacoes()
        {
            DateTime? dataNull = null;
            IndiceCollection indices = this.RetornaIndicesComCotacoes(dataNull);

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("IdIndice", typeof(int));
            dataTable.Columns.Add("Descricao");

            List<int> idIndicesUsados = new List<int>();
            foreach (Indice indice in indices)
            {
                if (!idIndicesUsados.Contains(indice.IdIndice.Value))
                {
                    dataTable.Rows.Add(indice.IdIndice.Value, indice.Descricao);
                    idIndicesUsados.Add(indice.IdIndice.Value);
                }
            }

            return dataTable;
        }

        public DataTable RetornaDataTableIndicesComCotacoes(DateTime data)
        {
            IndiceCollection indices = this.RetornaIndicesComCotacoes(data);

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("IdIndice", typeof(int));
            dataTable.Columns.Add("Descricao");

            List<int> idIndicesUsados = new List<int>();
            foreach (Indice indice in indices)
            {
                if (!idIndicesUsados.Contains(indice.IdIndice.Value))
                {
                    dataTable.Rows.Add(indice.IdIndice.Value, indice.Descricao);
                    idIndicesUsados.Add(indice.IdIndice.Value);
                }
            }

            return dataTable;
        }
    }
}
