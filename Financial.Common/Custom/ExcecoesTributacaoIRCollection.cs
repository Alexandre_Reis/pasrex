﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2014 12:01:32
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common
{
	public partial class ExcecoesTributacaoIRCollection : esExcecoesTributacaoIRCollection
	{
        public bool RetornaExcessaoTributacaoIR(int? tipoInvestidor, DateTime data, int tipoMercado)
        {
            if (!tipoInvestidor.HasValue)
                return false;

            this.Query.Where(this.Query.DataFim.GreaterThanOrEqual(data) &
                             this.Query.DataInicio.LessThanOrEqual(data) &
                             this.Query.TipoInvestidor.Equal(tipoInvestidor) &
                             this.Query.Mercado.Equal(tipoMercado));

            return this.Query.Load();
        }

        public ExcecoesTributacaoIR ObtemPrioridade(int? tipoInvestidor, int tipoMercado, int tipoClasseAtivo, int ativo)
        {
            if(!tipoInvestidor.HasValue)
                return new ExcecoesTributacaoIR();

            List<ExcecoesTributacaoIR> lstExcecoesTributacaoIR = (List<ExcecoesTributacaoIR>)this;
            ExcecoesTributacaoIR excecoesTributacaoIR;

            #region Por Título
            excecoesTributacaoIR = lstExcecoesTributacaoIR.Find(delegate(ExcecoesTributacaoIR x)
                                                                {
                                                                    return x.TipoInvestidor == tipoInvestidor && x.Mercado == tipoMercado && x.TipoClasseAtivo == tipoClasseAtivo && x.Ativo != null && x.Ativo.Equals(ativo.ToString());
                                                                });

            if (excecoesTributacaoIR != null && excecoesTributacaoIR.IdExcecoesTributacaoIR.HasValue && excecoesTributacaoIR.IdExcecoesTributacaoIR.Value > 0)
            {
                return excecoesTributacaoIR;
            }
            #endregion

            #region Por papel
            excecoesTributacaoIR = lstExcecoesTributacaoIR.Find(delegate(ExcecoesTributacaoIR x)
                                                                {
                                                                    return x.TipoInvestidor == tipoInvestidor && x.Mercado == tipoMercado && x.TipoClasseAtivo == tipoClasseAtivo && x.Ativo == null;
                                                                });

            if (excecoesTributacaoIR != null && excecoesTributacaoIR.IdExcecoesTributacaoIR.HasValue && excecoesTributacaoIR.IdExcecoesTributacaoIR.Value > 0)
            {
                return excecoesTributacaoIR;
            }
            #endregion

            #region Por Geral
            excecoesTributacaoIR = lstExcecoesTributacaoIR.Find(delegate(ExcecoesTributacaoIR x)
                                                                {
                                                                    return x.TipoInvestidor == tipoInvestidor && x.Mercado == tipoMercado && x.TipoClasseAtivo == null && x.Ativo == null;
                                                                });

            if (excecoesTributacaoIR != null && excecoesTributacaoIR.IdExcecoesTributacaoIR.HasValue && excecoesTributacaoIR.IdExcecoesTributacaoIR.Value > 0)
            {
                return excecoesTributacaoIR;
            }
            #endregion

            return new ExcecoesTributacaoIR();
        }
	}
}
