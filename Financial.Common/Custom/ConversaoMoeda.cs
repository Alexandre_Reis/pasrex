using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;

namespace Financial.Common
{
	public partial class ConversaoMoeda : esConversaoMoeda
	{
        public decimal RetornaFatorConversao(int idMoeda1, int idMoeda2, DateTime data)
        {
            decimal fator = 1;

            if (idMoeda1 != idMoeda2)
            {                
                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda1),
                                           conversaoMoeda.Query.IdMoedaPara.Equal(idMoeda2));
                if (conversaoMoeda.Query.Load())
                {
                    int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                    {
                        fator = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                    }
                    else
                    {
                        fator = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                    }
                }                
            }

            return fator;
        }

	}
}
