﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;
using log4net;

namespace Financial.Common {
    public partial class Feriado : esFeriado {
        private static readonly ILog log = LogManager.GetLogger(typeof(Feriado));
        private static List<Feriado> listaFeriado;

        static readonly object lockObject = new object();
        
        public static void limpaCache()
        {
            listaFeriado = null;
        }

        /// <summary>
        /// Retorna o número de feriados entre as datas Inicio e Fim, dado o IdLocal (feriado Brasil é fixo).
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idLocal"></param>
        /// <returns></returns>
        public int RetornaNumeroFeriados(DateTime dataInicio, DateTime dataFim, int idLocal) 
        {                         
            this.Query.es.CountAll = true;   
            this.Query.es.CountAllAlias = "Feriado";   
            this.Query
                .Where(this.Query.Data.Between(dataInicio, dataFim),
                       this.Query.IdLocal.In(idLocal, LocalFeriadoFixo.Brasil));
            this.Query.Load();
            
            int numeroDiasFeriado = this.es.HasData ? (int)this.GetColumn("Feriado") : 0;
            return numeroDiasFeriado;
        }

        private void InicializaListaFeriado()
        {
            FeriadoCollection coll = new FeriadoCollection();
            coll.LoadAll();

            listaFeriado = (List<Feriado>)coll;
            
        }

        /// <summary>
        /// Retorna um boolean indicando se a data passada é feriado. Feriados nacionais (Brasil) são sempre testados.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idLocal"></param>
        /// <returns></returns>
        public bool BuscaFeriadoBrasil(DateTime data, int idLocal) 
        {
            lock (lockObject)
            {
                if (listaFeriado == null)
                {
                    this.InicializaListaFeriado();
                }
            }

            Feriado feriado = listaFeriado.Find(delegate(Feriado x)
            {
                return x.Data == data && (x.IdLocal == idLocal || x.IdLocal == LocalFeriadoFixo.Brasil);
            });

            return (feriado != null);

        }

        /// <summary>
        /// Retorna um boolean indicando se a data passada é feriado. Feriados nacionais (Brasil) são sempre testados.
        /// Aqui também é testado se é feriado em NY.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idLocal"></param>
        /// <returns></returns>
        public bool BuscaFeriadoNovaYorkBMF(DateTime data, int idLocal)
        {
            lock (lockObject)
            {
                if (listaFeriado == null)
                {
                    this.InicializaListaFeriado();
                }
            }

            Feriado feriado = listaFeriado.Find(delegate(Feriado x)
            {
                return x.Data == data && (x.IdLocal == idLocal || x.IdLocal == LocalFeriadoFixo.Brasil || x.IdLocal == LocalFeriadoFixo.NovaYork);
            });

            return (feriado != null);

         }

        /// <summary>
        /// Retorna um boolean indicando se a data passada é feriado, dado o IdLocal passado.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idLocal"></param>
        /// <returns></returns>
        public bool BuscaFeriadoLocal(DateTime data, int idLocal)
        {
            lock (lockObject)
            {
                if (listaFeriado == null)
                {
                    this.InicializaListaFeriado();
                }
            }           

            Feriado feriado = listaFeriado.Find(delegate(Feriado x) { return x.Data == data && x.IdLocal == idLocal; });
            return (feriado != null);
        }

    }
}