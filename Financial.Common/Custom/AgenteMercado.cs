﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;
using Financial.Bolsa;
using Financial.Common.Exceptions;
using log4net;
using Financial.InvestidorCotista;
using Financial.CRM;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Financial.Common {
    public partial class AgenteMercado : esAgenteMercado 
    {
        /// <summary>
        /// Carrega objeto AgenteMercado com o campo IdAgente a partir do codigo na Bovespa.
        /// Exception IdAgenteNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="codigoBovespa"></param>
        public int BuscaIdAgenteMercadoBovespa(int codigoBovespa) 
        {
            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.IdAgente);
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.CodigoBovespa.Equal(codigoBovespa));
            agenteMercadoCollection.Query.Load();

            if (agenteMercadoCollection.Count == 0)
            {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("IdAgenteMercado não cadastrado para o código Bovespa " + codigoBovespa);
                throw new IdAgenteNaoCadastradoException(mensagemAux.ToString());
            }
            else if (agenteMercadoCollection.Count > 1)
            {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("Mais de 1 Agente Mercado encontrado para o código Bovespa " + codigoBovespa);
                throw new IdAgenteNaoCadastradoException(mensagemAux.ToString());
            }

            return agenteMercadoCollection[0].IdAgente.Value;
        }

        /// <summary>
        /// Carrega objeto AgenteMercado com o campo IdAgente a partir do codigo na BMF.
        /// Exception IdAgenteNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="codigoBMF"></param>
        public void BuscaIdAgenteMercadoBMF(int codigoBMF) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdAgente)
                 .Where(this.query.CodigoBMF == codigoBMF);           
            this.Query.Load();

            if (!this.es.HasData) {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("IdAgenteMercado não cadastrado para o código BMF " + codigoBMF);
                throw new IdAgenteNaoCadastradoException(mensagemAux.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se CorretagemAdicional é por Mercado
        ///          False caso Contrario
        /// </returns>
        public bool IsCorretagemAdicionalPorMercado() {
            return (this.CorretagemAdicional == (int)TipoCorretagemAdicional.PorMercado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se CorretagemAdicional é por RateiaAcoesTermoFuturo
        ///          False caso Contrario
        /// </returns>
        public bool IsCorretagemAdicionalRateioAcoesTermoFuturo() {
            return (this.CorretagemAdicional == (int)TipoCorretagemAdicional.RateiaAcoesTermoFuturo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se CorretagemAdicional é por RateiaAcoes
        ///          False caso Contrario
        /// </returns>
        public bool IsCorretagemAdicionalRateioAcoes() {
            return (this.CorretagemAdicional == (int)TipoCorretagemAdicional.RateiaAcoes);
        }
        
        public bool BuscaPorCodigoAnbid(string codigoAnbid) {
            this.QueryReset();
            this.Query.Where(this.query.CodigoAnbid == codigoAnbid);
            this.Query.Load();
            return this.es.HasData;
        }

        public void SaveAnbid(string nome, string codigoAnbid)
        {
            this.CodigoAnbid = codigoAnbid;
            this.FuncaoCorretora = "N";
            this.FuncaoGestor = "S";
            this.FuncaoAdministrador = "S";
            this.FuncaoLiquidante = "N";
            this.FuncaoCustodiante = "N";
            this.FuncaoDistribuidor = "N";

            this.Save(nome, false);
        }

        /// <summary>
        /// Salva um novo agente mercado
        /// </summary>
        public void Save(string nome, bool deferSave)
        {
            this.Nome = nome;

            if (!deferSave)
            {
                this.Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAgente"></param>
        /// <param name="CodigoAnbid"></param>
        public bool AtualizaAnbid(int idAgente, string CodigoAnbid)
        {
            this.QueryReset();
            this.Query.Where(this.query.IdAgente.Equal(idAgente));
            this.Query.Load();

            if (this.es.HasData)
            {
                this.CodigoAnbid = CodigoAnbid;
                this.Save();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Busca por nome
        /// </summary>
        /// <param name="nomeAgente"></param>
        /// <returns></returns>
        public int? BuscaPorNome(string nomeAgente)
        {
            AgenteMercadoCollection agenteMercadoColl = new AgenteMercadoCollection();
            agenteMercadoColl.LoadAll();
            foreach (AgenteMercado agenteMercado in agenteMercadoColl)
            {
                string nomeDB = RemoverAcentos(agenteMercado.Nome).Trim().ToUpper();
                nomeDB = Regex.Replace(nomeDB, @"[^0-9a-zA-Z]+", " ");

                string nomeImportacao = RemoverAcentos(nomeAgente).Trim().ToUpper().Replace(" ", " ");
                nomeImportacao = Regex.Replace(nomeImportacao, @"[^0-9a-zA-Z]+", " ");

                if (nomeDB.Equals(nomeImportacao))
                {
                    return agenteMercado.IdAgente;
                }
            }

            return null;
        }

        private static string RemoverAcentos(string nome)
        {
            string s = nome.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(s[k]);
                }
            }
            return sb.ToString();
        }
    }
}
