﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using System.Reflection;

namespace Financial.Common.Enums {

    public enum TipoFeriado
    {
        Brasil = 1,
        NovaYorkBMF = 2,
        Outros = 3
    }

    public static class LocalNegociacaoFixo
    {
        public const int Brasil = 1; //Default
        public const int SELIC = 2;
        public const int CETIP = 3;
        public const int CBLC = 4;
        public const int BOVESPA = 5;
    }

    public enum LocalCustodiaFixo
    {
        [StringValue("Selic")]
        Selic = 1,

        [StringValue("Cetip")]
        Cetip = 2,

        [StringValue("CBLC")]
        CBLC = 3
    }

    public enum ClearingFixo
    {
        Selic = 1,
        Cetip = 2,
        CBLC = 3,
        BOVESPA = 4,
    }

    public enum TipoOperacaoBanco
    {
        Alteração = 1,
        Inclusão = 2,
        Exclusão = 3
    }

    /// <summary>
    /// Enum dos tipos locais de feriados.
    /// Estes são os fixos, que devem existir e não ser alterados na base de dados.
    /// </summary>
    public static class LocalFeriadoFixo {
        public const int Brasil = 1;
        public const int Bovespa = 2;
        public const int NovaYork = 4;        

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Brasil);
            valoresPossiveis.Add(Bovespa);
            valoresPossiveis.Add(NovaYork);
            return valoresPossiveis;
        }
    }

    public enum ListaMoedaFixo
    {
        Real = 1,
        Dolar = 2,
        Euro = 3
    }

    public enum TipoConversaoMoeda
    {
        Divide = 1,
        Multiplica = 2
    }

    /// <summary>
    /// Enum dos indices fixos, que devem existir e não ser alterados na base de dados.
    /// </summary>
    public static class ListaIndiceFixo 
    {
        public const int CDI = 1;
        public const int SELIC = 10;
        public const int PTAX_800COMPRA = 20;
        public const int PTAX_800VENDA = 21;       
        public const int PTAX_REF = 22;
        public const int PTAX_800MEDIO = 23;
        public const int PTAX_800FLU = 24;
        public const int PTAX_800VENDA_1130 = 25;
        public const int PTAX_REF_2 = 26;
        public const int TR = 40;
        public const int POUPANCA = 45;
        public const int IGPM = 60;
        public const int IPCA = 61;
        public const int INPC = 62;
        public const int IGPDI = 63;
        public const int IBOVESPA_FECHA = 70;
        public const int IBOVESPA_MEDIO = 71;
        public const int IBOVESPA_MEDIOMENSAL = 72;
        public const int IDIVIDENDOS = 73;
        public const int IBRX_FECHA = 80;
        public const int IBRX_MEDIO = 81;
        public const int IBRX50_FECHA = 90;
        public const int IBRX50_MEDIO = 91;
        public const int IBRX50_MEDIOMENSAL = 92;
        public const int IBRA = 93;
        public const int IFIX = 94;
        
        public const int IMA_GERAL = 100;
        public const int IMA_C = 101;
        public const int IMA_B = 102;
        public const int IMA_S = 103;
        public const int IRFM = 104;

        public const int IRFM_1 = 105;
        public const int IRFM_1_MAIS = 106;
        public const int IMA_B_5 = 107;
        public const int IMA_B_5_MAIS = 108;
        public const int IMA_GERAL_EX_C = 110;
        
        public const int TJLP = 200;
        public const int ANBID = 210;                
        public const int UFIR = 220;        
        public const int OUROBMF_FECHA = 230;
        public const int OUROBMF_MEDIO = 231;
        public const int IDI = 400;
        public const int LFT = 410;
        public const int EURO = 500;
        public const int EURO_DOLAR = 501;
        public const int YENE = 510;
        public const int PESOARGENTINO = 520;
    }

    /// <summary>
    /// Indica se é decimal ou taxa diária.
    /// </summary>
    public enum TipoIndice
    {
        Percentual = 1,
        Decimal = 2
    }

    /// <summary>
    /// Indica se é diária ou mensal a divulgação do indice.
    /// </summary>
    public enum TipoDivulgacaoIndice
    {
        Diario = 1,
        Mensal = 2,
        Mensal_1 = 3,
        Mensal_2 = 4,
        Mensal_3 = 5,
        Diario_1 = 6,
        Diario_2 = 7,
        Diario_3 = 8
    }

    public static class TipoDivulgacaoIndiceDescricao
    {
        public static string RetornaDescricao(int idTipoDivulgacaoIndice)
        {
            return Enum.GetName(typeof(TipoDivulgacaoIndice), idTipoDivulgacaoIndice);
        }

        public static string RetornaStringValue(int idTipoDivulgacaoIndice)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoDivulgacaoIndice), RetornaDescricao(idTipoDivulgacaoIndice)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    /// <summary>
    /// Ainda sem uso.
    /// </summary>
    public enum TipoLiquidacao
    {
        Adm = 1,
        Reserva = 2
    }

    public enum ListaEmissorFixo
    {
        Padrao = 1,
        TesouroNacional = 99999
    }

    /// <summary>
    /// Usado para o enquadramento
    /// </summary>
    public enum TipoEmissor
    {
        Uniao = 1,
        Estado = 2,
        Municipio = 3,
        PessoaFisica = 4,
        PessoaJuridica = 5,
        InstituicaoFinanceira = 6
    }


    public enum ExecutaFuncao
    {
        [StringValue("Sim")]
        Sim = 1,

        [StringValue("Não")]
        Nao = 0,
    }

    public static class ExecutaFuncaoDescricao
    {
        public static string RetornaDescricao(int idExecutaFuncao)
        {
            return Enum.GetName(typeof(ExecutaFuncao), idExecutaFuncao);
        }

        public static string RetornaStringValue(int idExecutaFuncao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ExecutaFuncao), RetornaDescricao(idExecutaFuncao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum EnumTipoDePara
    {
        [StringValue("Fincs Indexador")]
        Fincs_Indexador = 1,

        [StringValue("Importacao ASEL007 Indice")]
        Importacao_ASEL007_Indice = 2
    }

    public static class EnumTipoDeParaDescricao
    {
        public static string RetornaDescricao(int idTipoDePara)
        {
            return Enum.GetName(typeof(EnumTipoDePara), idTipoDePara);
        }

        public static string RetornaStringValue(int idTipoDePara)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(EnumTipoDePara), RetornaDescricao(idTipoDePara)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoDado
    {
        [StringValue("Inteiro")]
        Inteiro = 1,

        [StringValue("Data")]
        Data = 2,

        [StringValue("Texto")]
        Texto = 3
    }

    public static class TipoDadoDescricao
    {
        public static string RetornaDescricao(int idTipoDado)
        {
            return Enum.GetName(typeof(TipoDado), idTipoDado);
        }

        public static string RetornaStringValue(int idTipoDado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoDado), RetornaDescricao(idTipoDado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }


    public enum Periodicidade
    {
        [StringValue("Mensal")]
        Mensal = 1,

        [StringValue("Bimestral")]
        Bimestral = 2,

        [StringValue("Trimestral")]
        Trimestral = 3,

        [StringValue("Quadrimestral")]
        Quadrimestal = 4,

        [StringValue("Semestral")]
        Semestral = 6,

        [StringValue("Anual")]
        Anual = 12
    }

    public enum TipoCarenciaResgate
    {
        [StringValue("Hard")]
        Hard = 1,

        [StringValue("Soft")]
        Soft = 2
    }

    public static class PeriodicidadeDescricao
    {
        public static string RetornaDescricao(int idPeriodicidade)
        {
            return Enum.GetName(typeof(Periodicidade), idPeriodicidade);
        }

        public static string RetornaStringValue(int idPeriodicidade)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(Periodicidade), RetornaDescricao(idPeriodicidade)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

     public enum ListaCadastroComplementar
    {
        [StringValue("Numerico")]
        Numerico = -1,

        [StringValue("Alfanumerico")]
        Alfanumerico = -2,

        [StringValue("Data")]
        Data = -3,

        [StringValue("Emissores")]
        Emissores = -4,

        [StringValue("Administradores")]
        Administradores = -5,

        [StringValue("Custodiantes")]
        Custodiantes = -6,

        [StringValue("Corretora")]
        Corretora = -7,

        [StringValue("Administrador")]
        Administrador = -8,

        [StringValue("Gestor")]
        Gestor = -9,

        [StringValue("Liquidante")]
        Liquidante = -10,

        [StringValue("Distribuidor")]
        Distribuidor = -11,

        [StringValue("Carteira/Fundo")]
        CarteiraFundo = -12,

        [StringValue("Cotista")]
        Cotista = -13
    }

    public static class ListaCadastroComplementarDescricao
    {
        public static string RetornaDescricao(int idCadastroComplementar)
        {
            return Enum.GetName(typeof(ListaCadastroComplementar), idCadastroComplementar);
        }

        public static string RetornaStringValue(int idCadastroComplementar)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaCadastroComplementar), RetornaDescricao(idCadastroComplementar)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }
  
    public enum ListaCadastro
    {
        //[StringValue("Ativos")]
        //Numerico = 1,
 
        //[StringValue("Pessoas")]
        //Alfanumerico = 2,

        //[StringValue("Carteiras")]
        //Data = 3,

        [StringValue("Emissores")]
        Emissores = -1,

        [StringValue("Administradores")]
        Administradores = -2,

        [StringValue("Custodiantes")]
        Custodiantes = -3,

        [StringValue("Corretora")]
        Corretora = -4,

        [StringValue("Gestor")]
        Gestor = -5,

        [StringValue("Liquidante")]
        Liquidante = -6,

        [StringValue("Distribuidor")]
        Distribuidor = -7,

        [StringValue("Carteira/Fundo")]
        CarteiraFundo = -8,

        [StringValue("Cotista")]
        Cotista = -9,

        [StringValue("Ativo Bolsa")]
        AtivoBolsa = -10,

        [StringValue("Ativo BMF")]
        AtivoBMF = -11,

        [StringValue("Renda Fixa Titulo")]
        RendaFixaTitulo = -12,

        [StringValue("Renda Fixa Papel")]
        RendaFixaPapel = -13
    }

    public static class ListaCadastroDescricao
    {
        public static string RetornaDescricao(int idCadastro)
        {
            return Enum.GetName(typeof(ListaCadastro), idCadastro);
        }

        public static string RetornaStringValue(int idCadastro)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaCadastro), RetornaDescricao(idCadastro)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum ListaMercadoTipoPessoa
    {
        [StringValue("Bolsa")]
        Bolsa = 1,

        [StringValue("BM&F")]
        BMF = 2,

        [StringValue("Fundos")]
        Fundos = 3,

        [StringValue("Renda Fixa")]
        RendaFixa = 4,
    }

    public static class ListaMercadoTipoPessoaDescricao
    {
        public static string RetornaDescricao(int idMercadoTipoPessoa)
        {
            return Enum.GetName(typeof(ListaMercadoTipoPessoa), idMercadoTipoPessoa);
        }

        public static string RetornaStringValue(int idMercadoTipoPessoa)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaMercadoTipoPessoa), RetornaDescricao(idMercadoTipoPessoa)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }
    
    //PAS 30699 - Tratamento Cadastral para Tributação de Ativos
    public enum ListaTipoInvestidor
    {
        [StringValue("Pessoa Física")]
        PessoaFisica = 1,

        [StringValue("Pessoa Jurídica")]
        PessoaJuridica = 2,

        [StringValue("OffShore")]
        OffShore = 3,
    }

    public static class ListaTipoInvestidorDescricao
    {
        public static string RetornaDescricao(int idExcecaoMercado)
        {
            return Enum.GetName(typeof(ListaTipoInvestidor), idExcecaoMercado);
        }

        public static string RetornaStringValue(int idExcecaoMercado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaTipoInvestidor), RetornaDescricao(idExcecaoMercado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum ListaIsencaoIR
    {
        [StringValue("Isento")]
        Isento = 1,

        [StringValue("Tributado")]
        Tributado = 2,
    }

    public static class ListaIsencaoIRDescricao
    {
        public static string RetornaDescricao(int idExcecaoMercado)
        {
            return Enum.GetName(typeof(ListaIsencaoIR), idExcecaoMercado);
        }

        public static string RetornaStringValue(int idExcecaoMercado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaIsencaoIR), RetornaDescricao(idExcecaoMercado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    //PAS 93 
    //Padrão de Notas
    public enum ListaItemAvaliado
    {
        [StringValue("Títulos")]
        Titulo = 1,

        [StringValue("Fundo de Investimentos")]
        FundoInvestimento = 2,

        [StringValue("Emissor")]
        Emissor = 3
    }
    public static class ListaItemAvaliadoDescricao
    {
        public static string RetornaDescricao(int idItemAvaliado)
        {
            return Enum.GetName(typeof(ListaItemAvaliado), idItemAvaliado);
        }

        public static string RetornaStringValue(int idItemAvaliado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaItemAvaliado), RetornaDescricao(idItemAvaliado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    //Classificação Faixa Rating
    public enum ListaClassificaoFaixaRating
    {
        [StringValue("Baixo Risco")]
        BaixoRisco = 1,

        [StringValue("Médio Risco")]
        MedioRisco = 2,

        [StringValue("Alto Risco")]
        AltoRisco = 3
    }
    public static class ListaClassificaoFaixaRatingDescricao
    {
        public static string RetornaDescricao(int idClassificacao)
        {
            return Enum.GetName(typeof(ListaClassificaoFaixaRating), idClassificacao);
        }

        public static string RetornaStringValue(int idClassificacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaClassificaoFaixaRating), RetornaDescricao(idClassificacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

}
