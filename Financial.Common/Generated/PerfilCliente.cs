/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;



namespace Financial.Common
{

	[Serializable]
	abstract public class esPerfilClienteCollection : esEntityCollection
	{
		public esPerfilClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilClienteQuery);
		}
		#endregion
		
		virtual public PerfilCliente DetachEntity(PerfilCliente entity)
		{
			return base.DetachEntity(entity) as PerfilCliente;
		}
		
		virtual public PerfilCliente AttachEntity(PerfilCliente entity)
		{
			return base.AttachEntity(entity) as PerfilCliente;
		}
		
		virtual public void Combine(PerfilClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilCliente this[int index]
		{
			get
			{
				return base[index] as PerfilCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilCliente);
		}
	}



	[Serializable]
	abstract public class esPerfilCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilCliente()
		{

		}

		public esPerfilCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilCliente)
		{
			esPerfilClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilCliente == idPerfilCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilCliente",idPerfilCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilCliente": this.str.IdPerfilCliente = (string)value; break;							
						case "IdPerfilClienteFaixaRating": this.str.IdPerfilClienteFaixaRating = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilCliente = (System.Int32?)value;
							break;
						
						case "IdPerfilClienteFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClienteFaixaRating = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilCliente.IdPerfilCliente
		/// </summary>
		virtual public System.Int32? IdPerfilCliente
		{
			get
			{
				return base.GetSystemInt32(PerfilClienteMetadata.ColumnNames.IdPerfilCliente);
			}
			
			set
			{
				base.SetSystemInt32(PerfilClienteMetadata.ColumnNames.IdPerfilCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCliente.IdPerfilClienteFaixaRating
		/// </summary>
		virtual public System.Int32? IdPerfilClienteFaixaRating
		{
			get
			{
				return base.GetSystemInt32(PerfilClienteMetadata.ColumnNames.IdPerfilClienteFaixaRating);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClienteMetadata.ColumnNames.IdPerfilClienteFaixaRating, value))
				{
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCliente.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PerfilClienteMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClienteMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected PerfilClienteFaixaRating _UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilCliente
			{
				get
				{
					System.Int32? data = entity.IdPerfilCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilCliente = null;
					else entity.IdPerfilCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilClienteFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdPerfilClienteFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClienteFaixaRating = null;
					else entity.IdPerfilClienteFaixaRating = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
			

			private esPerfilCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilCliente : esPerfilCliente
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__PerfilCli__IdCar__37461F20
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__PerfilCli__IdPer__3651FAE7
		/// </summary>

		[XmlIgnore]
		public PerfilClienteFaixaRating UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating
		{
			get
			{
				if(this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating == null
					&& IdPerfilClienteFaixaRating != null					)
				{
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating = new PerfilClienteFaixaRating();
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating", this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating);
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating.Query.Where(this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating.Query.IdPerfilClienteFaixaRating == this.IdPerfilClienteFaixaRating);
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating.Query.Load();
				}

				return this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating");
				

				if(value == null)
				{
					this.IdPerfilClienteFaixaRating = null;
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating = null;
				}
				else
				{
					this.IdPerfilClienteFaixaRating = value.IdPerfilClienteFaixaRating;
					this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating = value;
					this.SetPreSave("UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating", this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating != null)
			{
				this.IdPerfilClienteFaixaRating = this._UpToPerfilClienteFaixaRatingByIdPerfilClienteFaixaRating.IdPerfilClienteFaixaRating;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilCliente
		{
			get
			{
				return new esQueryItem(this, PerfilClienteMetadata.ColumnNames.IdPerfilCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilClienteFaixaRating
		{
			get
			{
				return new esQueryItem(this, PerfilClienteMetadata.ColumnNames.IdPerfilClienteFaixaRating, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PerfilClienteMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilClienteCollection")]
	public partial class PerfilClienteCollection : esPerfilClienteCollection, IEnumerable<PerfilCliente>
	{
		public PerfilClienteCollection()
		{

		}
		
		public static implicit operator List<PerfilCliente>(PerfilClienteCollection coll)
		{
			List<PerfilCliente> list = new List<PerfilCliente>();
			
			foreach (PerfilCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilCliente AddNew()
		{
			PerfilCliente entity = base.AddNewEntity() as PerfilCliente;
			
			return entity;
		}

		public PerfilCliente FindByPrimaryKey(System.Int32 idPerfilCliente)
		{
			return base.FindByPrimaryKey(idPerfilCliente) as PerfilCliente;
		}


		#region IEnumerable<PerfilCliente> Members

		IEnumerator<PerfilCliente> IEnumerable<PerfilCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilCliente;
			}
		}

		#endregion
		
		private PerfilClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilCliente' table
	/// </summary>

	[Serializable]
	public partial class PerfilCliente : esPerfilCliente
	{
		public PerfilCliente()
		{

		}
	
		public PerfilCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilClienteQuery query;
	}



	[Serializable]
	public partial class PerfilClienteQuery : esPerfilClienteQuery
	{
		public PerfilClienteQuery()
		{

		}		
		
		public PerfilClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilClienteMetadata.ColumnNames.IdPerfilCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClienteMetadata.PropertyNames.IdPerfilCliente;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClienteMetadata.ColumnNames.IdPerfilClienteFaixaRating, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClienteMetadata.PropertyNames.IdPerfilClienteFaixaRating;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClienteMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClienteMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilCliente = "IdPerfilCliente";
			 public const string IdPerfilClienteFaixaRating = "IdPerfilClienteFaixaRating";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilCliente = "IdPerfilCliente";
			 public const string IdPerfilClienteFaixaRating = "IdPerfilClienteFaixaRating";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilClienteMetadata))
			{
				if(PerfilClienteMetadata.mapDelegates == null)
				{
					PerfilClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilClienteMetadata.meta == null)
				{
					PerfilClienteMetadata.meta = new PerfilClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilClienteFaixaRating", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PerfilCliente";
				meta.Destination = "PerfilCliente";
				
				meta.spInsert = "proc_PerfilClienteInsert";				
				meta.spUpdate = "proc_PerfilClienteUpdate";		
				meta.spDelete = "proc_PerfilClienteDelete";
				meta.spLoadAll = "proc_PerfilClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
