/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2015 16:34:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esPerfilClienteFaixaRatingCollection : esEntityCollection
	{
		public esPerfilClienteFaixaRatingCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilClienteFaixaRatingCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilClienteFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilClienteFaixaRatingQuery);
		}
		#endregion
		
		virtual public PerfilClienteFaixaRating DetachEntity(PerfilClienteFaixaRating entity)
		{
			return base.DetachEntity(entity) as PerfilClienteFaixaRating;
		}
		
		virtual public PerfilClienteFaixaRating AttachEntity(PerfilClienteFaixaRating entity)
		{
			return base.AttachEntity(entity) as PerfilClienteFaixaRating;
		}
		
		virtual public void Combine(PerfilClienteFaixaRatingCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilClienteFaixaRating this[int index]
		{
			get
			{
				return base[index] as PerfilClienteFaixaRating;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilClienteFaixaRating);
		}
	}



	[Serializable]
	abstract public class esPerfilClienteFaixaRating : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilClienteFaixaRatingQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilClienteFaixaRating()
		{

		}

		public esPerfilClienteFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilClienteFaixaRating)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClienteFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClienteFaixaRating);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilClienteFaixaRating)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClienteFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClienteFaixaRating);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilClienteFaixaRating)
		{
			esPerfilClienteFaixaRatingQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilClienteFaixaRating == idPerfilClienteFaixaRating);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilClienteFaixaRating)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilClienteFaixaRating",idPerfilClienteFaixaRating);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilClienteFaixaRating": this.str.IdPerfilClienteFaixaRating = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdPerfilFaixaRating": this.str.IdPerfilFaixaRating = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilClienteFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClienteFaixaRating = (System.Int32?)value;
							break;
						
						case "IdPerfilFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilFaixaRating = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilClienteFaixaRating.IdPerfilClienteFaixaRating
		/// </summary>
		virtual public System.Int32? IdPerfilClienteFaixaRating
		{
			get
			{
				return base.GetSystemInt32(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilClienteFaixaRating);
			}
			
			set
			{
				base.SetSystemInt32(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilClienteFaixaRating, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClienteFaixaRating.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PerfilClienteFaixaRatingMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PerfilClienteFaixaRatingMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClienteFaixaRating.IdPerfilFaixaRating
		/// </summary>
		virtual public System.Int32? IdPerfilFaixaRating
		{
			get
			{
				return base.GetSystemInt32(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, value))
				{
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected PerfilFaixaRating _UpToPerfilFaixaRatingByIdPerfilFaixaRating;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilClienteFaixaRating entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilClienteFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdPerfilClienteFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClienteFaixaRating = null;
					else entity.IdPerfilClienteFaixaRating = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdPerfilFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdPerfilFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilFaixaRating = null;
					else entity.IdPerfilFaixaRating = Convert.ToInt32(value);
				}
			}
			

			private esPerfilClienteFaixaRating entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilClienteFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilClienteFaixaRating can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilClienteFaixaRating : esPerfilClienteFaixaRating
	{

				
		#region PerfilClienteCollectionByIdPerfilClienteFaixaRating - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__PerfilCli__IdPer__035C66C6
		/// </summary>

		[XmlIgnore]
		public PerfilClienteCollection PerfilClienteCollectionByIdPerfilClienteFaixaRating
		{
			get
			{
				if(this._PerfilClienteCollectionByIdPerfilClienteFaixaRating == null)
				{
					this._PerfilClienteCollectionByIdPerfilClienteFaixaRating = new PerfilClienteCollection();
					this._PerfilClienteCollectionByIdPerfilClienteFaixaRating.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilClienteCollectionByIdPerfilClienteFaixaRating", this._PerfilClienteCollectionByIdPerfilClienteFaixaRating);
				
					if(this.IdPerfilClienteFaixaRating != null)
					{
						this._PerfilClienteCollectionByIdPerfilClienteFaixaRating.Query.Where(this._PerfilClienteCollectionByIdPerfilClienteFaixaRating.Query.IdPerfilClienteFaixaRating == this.IdPerfilClienteFaixaRating);
						this._PerfilClienteCollectionByIdPerfilClienteFaixaRating.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilClienteCollectionByIdPerfilClienteFaixaRating.fks.Add(PerfilClienteMetadata.ColumnNames.IdPerfilClienteFaixaRating, this.IdPerfilClienteFaixaRating);
					}
				}

				return this._PerfilClienteCollectionByIdPerfilClienteFaixaRating;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilClienteCollectionByIdPerfilClienteFaixaRating != null) 
				{ 
					this.RemovePostSave("PerfilClienteCollectionByIdPerfilClienteFaixaRating"); 
					this._PerfilClienteCollectionByIdPerfilClienteFaixaRating = null;
					
				} 
			} 			
		}

		private PerfilClienteCollection _PerfilClienteCollectionByIdPerfilClienteFaixaRating;
		#endregion

				
		#region UpToPerfilFaixaRatingByIdPerfilFaixaRating - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__PerfilCli__IdPer__007FFA1B
		/// </summary>

		[XmlIgnore]
		public PerfilFaixaRating UpToPerfilFaixaRatingByIdPerfilFaixaRating
		{
			get
			{
				if(this._UpToPerfilFaixaRatingByIdPerfilFaixaRating == null
					&& IdPerfilFaixaRating != null					)
				{
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = new PerfilFaixaRating();
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating", this._UpToPerfilFaixaRatingByIdPerfilFaixaRating);
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.Where(this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.IdPerfilFaixaRating == this.IdPerfilFaixaRating);
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.Load();
				}

				return this._UpToPerfilFaixaRatingByIdPerfilFaixaRating;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating");
				

				if(value == null)
				{
					this.IdPerfilFaixaRating = null;
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = null;
				}
				else
				{
					this.IdPerfilFaixaRating = value.IdPerfilFaixaRating;
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = value;
					this.SetPreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating", this._UpToPerfilFaixaRatingByIdPerfilFaixaRating);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PerfilClienteCollectionByIdPerfilClienteFaixaRating", typeof(PerfilClienteCollection), new PerfilCliente()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilFaixaRatingByIdPerfilFaixaRating != null)
			{
				this.IdPerfilFaixaRating = this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.IdPerfilFaixaRating;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._PerfilClienteCollectionByIdPerfilClienteFaixaRating != null)
			{
				foreach(PerfilCliente obj in this._PerfilClienteCollectionByIdPerfilClienteFaixaRating)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPerfilClienteFaixaRating = this.IdPerfilClienteFaixaRating;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilClienteFaixaRatingQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClienteFaixaRatingMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilClienteFaixaRating
		{
			get
			{
				return new esQueryItem(this, PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilClienteFaixaRating, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PerfilClienteFaixaRatingMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPerfilFaixaRating
		{
			get
			{
				return new esQueryItem(this, PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilClienteFaixaRatingCollection")]
	public partial class PerfilClienteFaixaRatingCollection : esPerfilClienteFaixaRatingCollection, IEnumerable<PerfilClienteFaixaRating>
	{
		public PerfilClienteFaixaRatingCollection()
		{

		}
		
		public static implicit operator List<PerfilClienteFaixaRating>(PerfilClienteFaixaRatingCollection coll)
		{
			List<PerfilClienteFaixaRating> list = new List<PerfilClienteFaixaRating>();
			
			foreach (PerfilClienteFaixaRating emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilClienteFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClienteFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilClienteFaixaRating(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilClienteFaixaRating();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilClienteFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClienteFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilClienteFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilClienteFaixaRating AddNew()
		{
			PerfilClienteFaixaRating entity = base.AddNewEntity() as PerfilClienteFaixaRating;
			
			return entity;
		}

		public PerfilClienteFaixaRating FindByPrimaryKey(System.Int32 idPerfilClienteFaixaRating)
		{
			return base.FindByPrimaryKey(idPerfilClienteFaixaRating) as PerfilClienteFaixaRating;
		}


		#region IEnumerable<PerfilClienteFaixaRating> Members

		IEnumerator<PerfilClienteFaixaRating> IEnumerable<PerfilClienteFaixaRating>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilClienteFaixaRating;
			}
		}

		#endregion
		
		private PerfilClienteFaixaRatingQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilClienteFaixaRating' table
	/// </summary>

	[Serializable]
	public partial class PerfilClienteFaixaRating : esPerfilClienteFaixaRating
	{
		public PerfilClienteFaixaRating()
		{

		}
	
		public PerfilClienteFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClienteFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilClienteFaixaRatingQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClienteFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilClienteFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClienteFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilClienteFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilClienteFaixaRatingQuery query;
	}



	[Serializable]
	public partial class PerfilClienteFaixaRatingQuery : esPerfilClienteFaixaRatingQuery
	{
		public PerfilClienteFaixaRatingQuery()
		{

		}		
		
		public PerfilClienteFaixaRatingQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilClienteFaixaRatingMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilClienteFaixaRatingMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilClienteFaixaRating, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClienteFaixaRatingMetadata.PropertyNames.IdPerfilClienteFaixaRating;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClienteFaixaRatingMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilClienteFaixaRatingMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClienteFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClienteFaixaRatingMetadata.PropertyNames.IdPerfilFaixaRating;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilClienteFaixaRatingMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilClienteFaixaRating = "IdPerfilClienteFaixaRating";
			 public const string Descricao = "Descricao";
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilClienteFaixaRating = "IdPerfilClienteFaixaRating";
			 public const string Descricao = "Descricao";
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilClienteFaixaRatingMetadata))
			{
				if(PerfilClienteFaixaRatingMetadata.mapDelegates == null)
				{
					PerfilClienteFaixaRatingMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilClienteFaixaRatingMetadata.meta == null)
				{
					PerfilClienteFaixaRatingMetadata.meta = new PerfilClienteFaixaRatingMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilClienteFaixaRating", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPerfilFaixaRating", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PerfilClienteFaixaRating";
				meta.Destination = "PerfilClienteFaixaRating";
				
				meta.spInsert = "proc_PerfilClienteFaixaRatingInsert";				
				meta.spUpdate = "proc_PerfilClienteFaixaRatingUpdate";		
				meta.spDelete = "proc_PerfilClienteFaixaRatingDelete";
				meta.spLoadAll = "proc_PerfilClienteFaixaRatingLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilClienteFaixaRatingLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilClienteFaixaRatingMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
