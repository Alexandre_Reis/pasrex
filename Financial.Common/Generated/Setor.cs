/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		



using Financial.Enquadra;




	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esSetorCollection : esEntityCollection
	{
		public esSetorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SetorCollection";
		}

		#region Query Logic
		protected void InitQuery(esSetorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSetorQuery);
		}
		#endregion
		
		virtual public Setor DetachEntity(Setor entity)
		{
			return base.DetachEntity(entity) as Setor;
		}
		
		virtual public Setor AttachEntity(Setor entity)
		{
			return base.AttachEntity(entity) as Setor;
		}
		
		virtual public void Combine(SetorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Setor this[int index]
		{
			get
			{
				return base[index] as Setor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Setor);
		}
	}



	[Serializable]
	abstract public class esSetor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSetorQuery GetDynamicQuery()
		{
			return null;
		}

		public esSetor()
		{

		}

		public esSetor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idSetor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSetor);
			else
				return LoadByPrimaryKeyStoredProcedure(idSetor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idSetor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSetorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdSetor == idSetor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idSetor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSetor);
			else
				return LoadByPrimaryKeyStoredProcedure(idSetor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idSetor)
		{
			esSetorQuery query = this.GetDynamicQuery();
			query.Where(query.IdSetor == idSetor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idSetor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSetor",idSetor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSetor": this.str.IdSetor = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSetor":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdSetor = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Setor.IdSetor
		/// </summary>
		virtual public System.Int16? IdSetor
		{
			get
			{
				return base.GetSystemInt16(SetorMetadata.ColumnNames.IdSetor);
			}
			
			set
			{
				base.SetSystemInt16(SetorMetadata.ColumnNames.IdSetor, value);
			}
		}
		
		/// <summary>
		/// Maps to Setor.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(SetorMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(SetorMetadata.ColumnNames.Nome, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSetor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSetor
			{
				get
				{
					System.Int16? data = entity.IdSetor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSetor = null;
					else entity.IdSetor = Convert.ToInt16(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
			

			private esSetor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSetorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSetor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Setor : esSetor
	{

				
		#region EmissorCollectionByIdSetor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Setor_Emissor_FK1
		/// </summary>

		[XmlIgnore]
		public EmissorCollection EmissorCollectionByIdSetor
		{
			get
			{
				if(this._EmissorCollectionByIdSetor == null)
				{
					this._EmissorCollectionByIdSetor = new EmissorCollection();
					this._EmissorCollectionByIdSetor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EmissorCollectionByIdSetor", this._EmissorCollectionByIdSetor);
				
					if(this.IdSetor != null)
					{
						this._EmissorCollectionByIdSetor.Query.Where(this._EmissorCollectionByIdSetor.Query.IdSetor == this.IdSetor);
						this._EmissorCollectionByIdSetor.Query.Load();

						// Auto-hookup Foreign Keys
						this._EmissorCollectionByIdSetor.fks.Add(EmissorMetadata.ColumnNames.IdSetor, this.IdSetor);
					}
				}

				return this._EmissorCollectionByIdSetor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EmissorCollectionByIdSetor != null) 
				{ 
					this.RemovePostSave("EmissorCollectionByIdSetor"); 
					this._EmissorCollectionByIdSetor = null;
					
				} 
			} 			
		}

		private EmissorCollection _EmissorCollectionByIdSetor;
		#endregion

				
		#region EnquadraItemAcaoCollectionByIdSetor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Setor_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemAcaoCollection EnquadraItemAcaoCollectionByIdSetor
		{
			get
			{
				if(this._EnquadraItemAcaoCollectionByIdSetor == null)
				{
					this._EnquadraItemAcaoCollectionByIdSetor = new EnquadraItemAcaoCollection();
					this._EnquadraItemAcaoCollectionByIdSetor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemAcaoCollectionByIdSetor", this._EnquadraItemAcaoCollectionByIdSetor);
				
					if(this.IdSetor != null)
					{
						this._EnquadraItemAcaoCollectionByIdSetor.Query.Where(this._EnquadraItemAcaoCollectionByIdSetor.Query.IdSetor == this.IdSetor);
						this._EnquadraItemAcaoCollectionByIdSetor.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemAcaoCollectionByIdSetor.fks.Add(EnquadraItemAcaoMetadata.ColumnNames.IdSetor, this.IdSetor);
					}
				}

				return this._EnquadraItemAcaoCollectionByIdSetor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemAcaoCollectionByIdSetor != null) 
				{ 
					this.RemovePostSave("EnquadraItemAcaoCollectionByIdSetor"); 
					this._EnquadraItemAcaoCollectionByIdSetor = null;
					
				} 
			} 			
		}

		private EnquadraItemAcaoCollection _EnquadraItemAcaoCollectionByIdSetor;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EmissorCollectionByIdSetor", typeof(EmissorCollection), new Emissor()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemAcaoCollectionByIdSetor", typeof(EnquadraItemAcaoCollection), new EnquadraItemAcao()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EmissorCollectionByIdSetor != null)
			{
				foreach(Emissor obj in this._EmissorCollectionByIdSetor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSetor = this.IdSetor;
					}
				}
			}
			if(this._EnquadraItemAcaoCollectionByIdSetor != null)
			{
				foreach(EnquadraItemAcao obj in this._EnquadraItemAcaoCollectionByIdSetor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSetor = this.IdSetor;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSetorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SetorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSetor
		{
			get
			{
				return new esQueryItem(this, SetorMetadata.ColumnNames.IdSetor, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, SetorMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SetorCollection")]
	public partial class SetorCollection : esSetorCollection, IEnumerable<Setor>
	{
		public SetorCollection()
		{

		}
		
		public static implicit operator List<Setor>(SetorCollection coll)
		{
			List<Setor> list = new List<Setor>();
			
			foreach (Setor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SetorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SetorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Setor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Setor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SetorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SetorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SetorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Setor AddNew()
		{
			Setor entity = base.AddNewEntity() as Setor;
			
			return entity;
		}

		public Setor FindByPrimaryKey(System.Int16 idSetor)
		{
			return base.FindByPrimaryKey(idSetor) as Setor;
		}


		#region IEnumerable<Setor> Members

		IEnumerator<Setor> IEnumerable<Setor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Setor;
			}
		}

		#endregion
		
		private SetorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Setor' table
	/// </summary>

	[Serializable]
	public partial class Setor : esSetor
	{
		public Setor()
		{

		}
	
		public Setor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SetorMetadata.Meta();
			}
		}
		
		
		
		override protected esSetorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SetorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SetorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SetorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SetorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SetorQuery query;
	}



	[Serializable]
	public partial class SetorQuery : esSetorQuery
	{
		public SetorQuery()
		{

		}		
		
		public SetorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SetorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SetorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SetorMetadata.ColumnNames.IdSetor, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = SetorMetadata.PropertyNames.IdSetor;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SetorMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SetorMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SetorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSetor = "IdSetor";
			 public const string Nome = "Nome";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSetor = "IdSetor";
			 public const string Nome = "Nome";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SetorMetadata))
			{
				if(SetorMetadata.mapDelegates == null)
				{
					SetorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SetorMetadata.meta == null)
				{
					SetorMetadata.meta = new SetorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSetor", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Setor";
				meta.Destination = "Setor";
				
				meta.spInsert = "proc_SetorInsert";				
				meta.spUpdate = "proc_SetorUpdate";		
				meta.spDelete = "proc_SetorDelete";
				meta.spLoadAll = "proc_SetorLoadAll";
				meta.spLoadByPrimaryKey = "proc_SetorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SetorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
