/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/12/2015 15:37:36
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;	
using Financial.BMF;
using Financial.Bolsa;
using Financial.Gerencial;
using Financial.Fundo;

namespace Financial.Common
{

	[Serializable]
	abstract public class esTraderCollection : esEntityCollection
	{
		public esTraderCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TraderCollection";
		}

		#region Query Logic
		protected void InitQuery(esTraderQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTraderQuery);
		}
		#endregion
		
		virtual public Trader DetachEntity(Trader entity)
		{
			return base.DetachEntity(entity) as Trader;
		}
		
		virtual public Trader AttachEntity(Trader entity)
		{
			return base.AttachEntity(entity) as Trader;
		}
		
		virtual public void Combine(TraderCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Trader this[int index]
		{
			get
			{
				return base[index] as Trader;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Trader);
		}
	}



	[Serializable]
	abstract public class esTrader : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTraderQuery GetDynamicQuery()
		{
			return null;
		}

		public esTrader()
		{

		}

		public esTrader(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTrader)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTrader);
			else
				return LoadByPrimaryKeyStoredProcedure(idTrader);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTrader)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTraderQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTrader == idTrader);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTrader)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTrader);
			else
				return LoadByPrimaryKeyStoredProcedure(idTrader);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTrader)
		{
			esTraderQuery query = this.GetDynamicQuery();
			query.Where(query.IdTrader == idTrader);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTrader)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTrader",idTrader);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "AlgoTrader": this.str.AlgoTrader = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Trader.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(TraderMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				base.SetSystemInt32(TraderMetadata.ColumnNames.IdTrader, value);
			}
		}
		
		/// <summary>
		/// Maps to Trader.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(TraderMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(TraderMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Trader.AlgoTrader
		/// </summary>
		virtual public System.String AlgoTrader
		{
			get
			{
				return base.GetSystemString(TraderMetadata.ColumnNames.AlgoTrader);
			}
			
			set
			{
				base.SetSystemString(TraderMetadata.ColumnNames.AlgoTrader, value);
			}
		}
		
		/// <summary>
		/// Maps to Trader.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TraderMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TraderMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTrader entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String AlgoTrader
			{
				get
				{
					System.String data = entity.AlgoTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AlgoTrader = null;
					else entity.AlgoTrader = Convert.ToString(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
			

			private esTrader entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTraderQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTrader can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Trader : esTrader
	{

				
		#region CalculoGerencialCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_CalculoGerencial_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoGerencialCollection CalculoGerencialCollectionByIdTrader
		{
			get
			{
				if(this._CalculoGerencialCollectionByIdTrader == null)
				{
					this._CalculoGerencialCollectionByIdTrader = new CalculoGerencialCollection();
					this._CalculoGerencialCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoGerencialCollectionByIdTrader", this._CalculoGerencialCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._CalculoGerencialCollectionByIdTrader.Query.Where(this._CalculoGerencialCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._CalculoGerencialCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoGerencialCollectionByIdTrader.fks.Add(CalculoGerencialMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._CalculoGerencialCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoGerencialCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("CalculoGerencialCollectionByIdTrader"); 
					this._CalculoGerencialCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private CalculoGerencialCollection _CalculoGerencialCollectionByIdTrader;
		#endregion

				
		#region GerOperacaoBMFCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBMFCollection GerOperacaoBMFCollectionByIdTrader
		{
			get
			{
				if(this._GerOperacaoBMFCollectionByIdTrader == null)
				{
					this._GerOperacaoBMFCollectionByIdTrader = new GerOperacaoBMFCollection();
					this._GerOperacaoBMFCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBMFCollectionByIdTrader", this._GerOperacaoBMFCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerOperacaoBMFCollectionByIdTrader.Query.Where(this._GerOperacaoBMFCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerOperacaoBMFCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBMFCollectionByIdTrader.fks.Add(GerOperacaoBMFMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerOperacaoBMFCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBMFCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerOperacaoBMFCollectionByIdTrader"); 
					this._GerOperacaoBMFCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerOperacaoBMFCollection _GerOperacaoBMFCollectionByIdTrader;
		#endregion

				
		#region GerOperacaoBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBolsaCollection GerOperacaoBolsaCollectionByIdTrader
		{
			get
			{
				if(this._GerOperacaoBolsaCollectionByIdTrader == null)
				{
					this._GerOperacaoBolsaCollectionByIdTrader = new GerOperacaoBolsaCollection();
					this._GerOperacaoBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBolsaCollectionByIdTrader", this._GerOperacaoBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerOperacaoBolsaCollectionByIdTrader.Query.Where(this._GerOperacaoBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerOperacaoBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBolsaCollectionByIdTrader.fks.Add(GerOperacaoBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerOperacaoBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerOperacaoBolsaCollectionByIdTrader"); 
					this._GerOperacaoBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerOperacaoBolsaCollection _GerOperacaoBolsaCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBMFCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFCollection GerPosicaoBMFCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBMFCollectionByIdTrader == null)
				{
					this._GerPosicaoBMFCollectionByIdTrader = new GerPosicaoBMFCollection();
					this._GerPosicaoBMFCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFCollectionByIdTrader", this._GerPosicaoBMFCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBMFCollectionByIdTrader.Query.Where(this._GerPosicaoBMFCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBMFCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFCollectionByIdTrader.fks.Add(GerPosicaoBMFMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBMFCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFCollectionByIdTrader"); 
					this._GerPosicaoBMFCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFCollection _GerPosicaoBMFCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBMFAberturaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFAberturaCollection GerPosicaoBMFAberturaCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBMFAberturaCollectionByIdTrader == null)
				{
					this._GerPosicaoBMFAberturaCollectionByIdTrader = new GerPosicaoBMFAberturaCollection();
					this._GerPosicaoBMFAberturaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFAberturaCollectionByIdTrader", this._GerPosicaoBMFAberturaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBMFAberturaCollectionByIdTrader.Query.Where(this._GerPosicaoBMFAberturaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBMFAberturaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFAberturaCollectionByIdTrader.fks.Add(GerPosicaoBMFAberturaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBMFAberturaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFAberturaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFAberturaCollectionByIdTrader"); 
					this._GerPosicaoBMFAberturaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFAberturaCollection _GerPosicaoBMFAberturaCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBMFHistoricoCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFHistoricoCollection GerPosicaoBMFHistoricoCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBMFHistoricoCollectionByIdTrader == null)
				{
					this._GerPosicaoBMFHistoricoCollectionByIdTrader = new GerPosicaoBMFHistoricoCollection();
					this._GerPosicaoBMFHistoricoCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFHistoricoCollectionByIdTrader", this._GerPosicaoBMFHistoricoCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBMFHistoricoCollectionByIdTrader.Query.Where(this._GerPosicaoBMFHistoricoCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBMFHistoricoCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFHistoricoCollectionByIdTrader.fks.Add(GerPosicaoBMFHistoricoMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBMFHistoricoCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFHistoricoCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFHistoricoCollectionByIdTrader"); 
					this._GerPosicaoBMFHistoricoCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFHistoricoCollection _GerPosicaoBMFHistoricoCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaCollection GerPosicaoBolsaCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBolsaCollectionByIdTrader == null)
				{
					this._GerPosicaoBolsaCollectionByIdTrader = new GerPosicaoBolsaCollection();
					this._GerPosicaoBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaCollectionByIdTrader", this._GerPosicaoBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBolsaCollectionByIdTrader.Query.Where(this._GerPosicaoBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaCollectionByIdTrader.fks.Add(GerPosicaoBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaCollectionByIdTrader"); 
					this._GerPosicaoBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaCollection _GerPosicaoBolsaCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBolsaAberturaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaAberturaCollection GerPosicaoBolsaAberturaCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBolsaAberturaCollectionByIdTrader == null)
				{
					this._GerPosicaoBolsaAberturaCollectionByIdTrader = new GerPosicaoBolsaAberturaCollection();
					this._GerPosicaoBolsaAberturaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaAberturaCollectionByIdTrader", this._GerPosicaoBolsaAberturaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBolsaAberturaCollectionByIdTrader.Query.Where(this._GerPosicaoBolsaAberturaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBolsaAberturaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaAberturaCollectionByIdTrader.fks.Add(GerPosicaoBolsaAberturaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBolsaAberturaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaAberturaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaAberturaCollectionByIdTrader"); 
					this._GerPosicaoBolsaAberturaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaAberturaCollection _GerPosicaoBolsaAberturaCollectionByIdTrader;
		#endregion

				
		#region GerPosicaoBolsaHistoricoCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_GerPosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaHistoricoCollection GerPosicaoBolsaHistoricoCollectionByIdTrader
		{
			get
			{
				if(this._GerPosicaoBolsaHistoricoCollectionByIdTrader == null)
				{
					this._GerPosicaoBolsaHistoricoCollectionByIdTrader = new GerPosicaoBolsaHistoricoCollection();
					this._GerPosicaoBolsaHistoricoCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaHistoricoCollectionByIdTrader", this._GerPosicaoBolsaHistoricoCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._GerPosicaoBolsaHistoricoCollectionByIdTrader.Query.Where(this._GerPosicaoBolsaHistoricoCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._GerPosicaoBolsaHistoricoCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaHistoricoCollectionByIdTrader.fks.Add(GerPosicaoBolsaHistoricoMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._GerPosicaoBolsaHistoricoCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaHistoricoCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaHistoricoCollectionByIdTrader"); 
					this._GerPosicaoBolsaHistoricoCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaHistoricoCollection _GerPosicaoBolsaHistoricoCollectionByIdTrader;
		#endregion

				
		#region OperacaoEmprestimoBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_OperacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoEmprestimoBolsaCollection OperacaoEmprestimoBolsaCollectionByIdTrader
		{
			get
			{
				if(this._OperacaoEmprestimoBolsaCollectionByIdTrader == null)
				{
					this._OperacaoEmprestimoBolsaCollectionByIdTrader = new OperacaoEmprestimoBolsaCollection();
					this._OperacaoEmprestimoBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoEmprestimoBolsaCollectionByIdTrader", this._OperacaoEmprestimoBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._OperacaoEmprestimoBolsaCollectionByIdTrader.Query.Where(this._OperacaoEmprestimoBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._OperacaoEmprestimoBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoEmprestimoBolsaCollectionByIdTrader.fks.Add(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._OperacaoEmprestimoBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoEmprestimoBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("OperacaoEmprestimoBolsaCollectionByIdTrader"); 
					this._OperacaoEmprestimoBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private OperacaoEmprestimoBolsaCollection _OperacaoEmprestimoBolsaCollectionByIdTrader;
		#endregion

				
		#region OrdemBMFCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBMFCollection OrdemBMFCollectionByIdTrader
		{
			get
			{
				if(this._OrdemBMFCollectionByIdTrader == null)
				{
					this._OrdemBMFCollectionByIdTrader = new OrdemBMFCollection();
					this._OrdemBMFCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBMFCollectionByIdTrader", this._OrdemBMFCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._OrdemBMFCollectionByIdTrader.Query.Where(this._OrdemBMFCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._OrdemBMFCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBMFCollectionByIdTrader.fks.Add(OrdemBMFMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._OrdemBMFCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBMFCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("OrdemBMFCollectionByIdTrader"); 
					this._OrdemBMFCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private OrdemBMFCollection _OrdemBMFCollectionByIdTrader;
		#endregion

				
		#region OrdemBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByIdTrader
		{
			get
			{
				if(this._OrdemBolsaCollectionByIdTrader == null)
				{
					this._OrdemBolsaCollectionByIdTrader = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByIdTrader", this._OrdemBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._OrdemBolsaCollectionByIdTrader.Query.Where(this._OrdemBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._OrdemBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByIdTrader.fks.Add(OrdemBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._OrdemBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByIdTrader"); 
					this._OrdemBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByIdTrader;
		#endregion

				
		#region PosicaoEmprestimoBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaCollection PosicaoEmprestimoBolsaCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaCollectionByIdTrader == null)
				{
					this._PosicaoEmprestimoBolsaCollectionByIdTrader = new PosicaoEmprestimoBolsaCollection();
					this._PosicaoEmprestimoBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaCollectionByIdTrader", this._PosicaoEmprestimoBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoEmprestimoBolsaCollectionByIdTrader.Query.Where(this._PosicaoEmprestimoBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoEmprestimoBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaCollectionByIdTrader.fks.Add(PosicaoEmprestimoBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoEmprestimoBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaCollectionByIdTrader"); 
					this._PosicaoEmprestimoBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaCollection _PosicaoEmprestimoBolsaCollectionByIdTrader;
		#endregion

				
		#region PosicaoEmprestimoBolsaAberturaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoEmprestimoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaAberturaCollection PosicaoEmprestimoBolsaAberturaCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader == null)
				{
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader = new PosicaoEmprestimoBolsaAberturaCollection();
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdTrader", this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader.Query.Where(this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader.fks.Add(PosicaoEmprestimoBolsaAberturaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdTrader"); 
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaAberturaCollection _PosicaoEmprestimoBolsaAberturaCollectionByIdTrader;
		#endregion

				
		#region PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaHistoricoCollection PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader == null)
				{
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader = new PosicaoEmprestimoBolsaHistoricoCollection();
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader", this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader.Query.Where(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader.fks.Add(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader"); 
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaHistoricoCollection _PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader;
		#endregion

				
		#region PosicaoTermoBolsaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoTermoBolsaCollectionByIdTrader == null)
				{
					this._PosicaoTermoBolsaCollectionByIdTrader = new PosicaoTermoBolsaCollection();
					this._PosicaoTermoBolsaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaCollectionByIdTrader", this._PosicaoTermoBolsaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoTermoBolsaCollectionByIdTrader.Query.Where(this._PosicaoTermoBolsaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoTermoBolsaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaCollectionByIdTrader.fks.Add(PosicaoTermoBolsaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoTermoBolsaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaCollectionByIdTrader"); 
					this._PosicaoTermoBolsaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByIdTrader;
		#endregion

				
		#region PosicaoTermoBolsaAberturaCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaAberturaCollection PosicaoTermoBolsaAberturaCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoTermoBolsaAberturaCollectionByIdTrader == null)
				{
					this._PosicaoTermoBolsaAberturaCollectionByIdTrader = new PosicaoTermoBolsaAberturaCollection();
					this._PosicaoTermoBolsaAberturaCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaAberturaCollectionByIdTrader", this._PosicaoTermoBolsaAberturaCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoTermoBolsaAberturaCollectionByIdTrader.Query.Where(this._PosicaoTermoBolsaAberturaCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoTermoBolsaAberturaCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaAberturaCollectionByIdTrader.fks.Add(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoTermoBolsaAberturaCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaAberturaCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaAberturaCollectionByIdTrader"); 
					this._PosicaoTermoBolsaAberturaCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaAberturaCollection _PosicaoTermoBolsaAberturaCollectionByIdTrader;
		#endregion

				
		#region PosicaoTermoBolsaHistoricoCollectionByIdTrader - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_PosicaoTermoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaHistoricoCollection PosicaoTermoBolsaHistoricoCollectionByIdTrader
		{
			get
			{
				if(this._PosicaoTermoBolsaHistoricoCollectionByIdTrader == null)
				{
					this._PosicaoTermoBolsaHistoricoCollectionByIdTrader = new PosicaoTermoBolsaHistoricoCollection();
					this._PosicaoTermoBolsaHistoricoCollectionByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaHistoricoCollectionByIdTrader", this._PosicaoTermoBolsaHistoricoCollectionByIdTrader);
				
					if(this.IdTrader != null)
					{
						this._PosicaoTermoBolsaHistoricoCollectionByIdTrader.Query.Where(this._PosicaoTermoBolsaHistoricoCollectionByIdTrader.Query.IdTrader == this.IdTrader);
						this._PosicaoTermoBolsaHistoricoCollectionByIdTrader.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaHistoricoCollectionByIdTrader.fks.Add(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.IdTrader, this.IdTrader);
					}
				}

				return this._PosicaoTermoBolsaHistoricoCollectionByIdTrader;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaHistoricoCollectionByIdTrader != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaHistoricoCollectionByIdTrader"); 
					this._PosicaoTermoBolsaHistoricoCollectionByIdTrader = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByIdTrader;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CalculoGerencialCollectionByIdTrader", typeof(CalculoGerencialCollection), new CalculoGerencial()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBMFCollectionByIdTrader", typeof(GerOperacaoBMFCollection), new GerOperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBolsaCollectionByIdTrader", typeof(GerOperacaoBolsaCollection), new GerOperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFCollectionByIdTrader", typeof(GerPosicaoBMFCollection), new GerPosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFAberturaCollectionByIdTrader", typeof(GerPosicaoBMFAberturaCollection), new GerPosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFHistoricoCollectionByIdTrader", typeof(GerPosicaoBMFHistoricoCollection), new GerPosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaCollectionByIdTrader", typeof(GerPosicaoBolsaCollection), new GerPosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaAberturaCollectionByIdTrader", typeof(GerPosicaoBolsaAberturaCollection), new GerPosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaHistoricoCollectionByIdTrader", typeof(GerPosicaoBolsaHistoricoCollection), new GerPosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "OperacaoEmprestimoBolsaCollectionByIdTrader", typeof(OperacaoEmprestimoBolsaCollection), new OperacaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBMFCollectionByIdTrader", typeof(OrdemBMFCollection), new OrdemBMF()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByIdTrader", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaCollectionByIdTrader", typeof(PosicaoEmprestimoBolsaCollection), new PosicaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaAberturaCollectionByIdTrader", typeof(PosicaoEmprestimoBolsaAberturaCollection), new PosicaoEmprestimoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader", typeof(PosicaoEmprestimoBolsaHistoricoCollection), new PosicaoEmprestimoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaCollectionByIdTrader", typeof(PosicaoTermoBolsaCollection), new PosicaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaAberturaCollectionByIdTrader", typeof(PosicaoTermoBolsaAberturaCollection), new PosicaoTermoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaHistoricoCollectionByIdTrader", typeof(PosicaoTermoBolsaHistoricoCollection), new PosicaoTermoBolsaHistorico()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CalculoGerencialCollectionByIdTrader != null)
			{
				foreach(CalculoGerencial obj in this._CalculoGerencialCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerOperacaoBMFCollectionByIdTrader != null)
			{
				foreach(GerOperacaoBMF obj in this._GerOperacaoBMFCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerOperacaoBolsaCollectionByIdTrader != null)
			{
				foreach(GerOperacaoBolsa obj in this._GerOperacaoBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBMFCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBMF obj in this._GerPosicaoBMFCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBMFAberturaCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBMFAbertura obj in this._GerPosicaoBMFAberturaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBMFHistoricoCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBMFHistorico obj in this._GerPosicaoBMFHistoricoCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBolsaCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBolsa obj in this._GerPosicaoBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBolsaAberturaCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBolsaAbertura obj in this._GerPosicaoBolsaAberturaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._GerPosicaoBolsaHistoricoCollectionByIdTrader != null)
			{
				foreach(GerPosicaoBolsaHistorico obj in this._GerPosicaoBolsaHistoricoCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._OperacaoEmprestimoBolsaCollectionByIdTrader != null)
			{
				foreach(OperacaoEmprestimoBolsa obj in this._OperacaoEmprestimoBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._OrdemBMFCollectionByIdTrader != null)
			{
				foreach(OrdemBMF obj in this._OrdemBMFCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._OrdemBolsaCollectionByIdTrader != null)
			{
				foreach(OrdemBolsa obj in this._OrdemBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaCollectionByIdTrader != null)
			{
				foreach(PosicaoEmprestimoBolsa obj in this._PosicaoEmprestimoBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader != null)
			{
				foreach(PosicaoEmprestimoBolsaAbertura obj in this._PosicaoEmprestimoBolsaAberturaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader != null)
			{
				foreach(PosicaoEmprestimoBolsaHistorico obj in this._PosicaoEmprestimoBolsaHistoricoCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoTermoBolsaCollectionByIdTrader != null)
			{
				foreach(PosicaoTermoBolsa obj in this._PosicaoTermoBolsaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoTermoBolsaAberturaCollectionByIdTrader != null)
			{
				foreach(PosicaoTermoBolsaAbertura obj in this._PosicaoTermoBolsaAberturaCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
			if(this._PosicaoTermoBolsaHistoricoCollectionByIdTrader != null)
			{
				foreach(PosicaoTermoBolsaHistorico obj in this._PosicaoTermoBolsaHistoricoCollectionByIdTrader)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTrader = this.IdTrader;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTraderQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TraderMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, TraderMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, TraderMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem AlgoTrader
		{
			get
			{
				return new esQueryItem(this, TraderMetadata.ColumnNames.AlgoTrader, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TraderMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TraderCollection")]
	public partial class TraderCollection : esTraderCollection, IEnumerable<Trader>
	{
		public TraderCollection()
		{

		}
		
		public static implicit operator List<Trader>(TraderCollection coll)
		{
			List<Trader> list = new List<Trader>();
			
			foreach (Trader emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TraderMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TraderQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Trader(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Trader();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TraderQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TraderQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TraderQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Trader AddNew()
		{
			Trader entity = base.AddNewEntity() as Trader;
			
			return entity;
		}

		public Trader FindByPrimaryKey(System.Int32 idTrader)
		{
			return base.FindByPrimaryKey(idTrader) as Trader;
		}


		#region IEnumerable<Trader> Members

		IEnumerator<Trader> IEnumerable<Trader>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Trader;
			}
		}

		#endregion
		
		private TraderQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Trader' table
	/// </summary>

	[Serializable]
	public partial class Trader : esTrader
	{
		public Trader()
		{

		}
	
		public Trader(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TraderMetadata.Meta();
			}
		}
		
		
		
		override protected esTraderQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TraderQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TraderQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TraderQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TraderQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TraderQuery query;
	}



	[Serializable]
	public partial class TraderQuery : esTraderQuery
	{
		public TraderQuery()
		{

		}		
		
		public TraderQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TraderMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TraderMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TraderMetadata.ColumnNames.IdTrader, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TraderMetadata.PropertyNames.IdTrader;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TraderMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TraderMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TraderMetadata.ColumnNames.AlgoTrader, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TraderMetadata.PropertyNames.AlgoTrader;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TraderMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TraderMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TraderMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTrader = "IdTrader";
			 public const string Nome = "Nome";
			 public const string AlgoTrader = "AlgoTrader";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTrader = "IdTrader";
			 public const string Nome = "Nome";
			 public const string AlgoTrader = "AlgoTrader";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TraderMetadata))
			{
				if(TraderMetadata.mapDelegates == null)
				{
					TraderMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TraderMetadata.meta == null)
				{
					TraderMetadata.meta = new TraderMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AlgoTrader", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Trader";
				meta.Destination = "Trader";
				
				meta.spInsert = "proc_TraderInsert";				
				meta.spUpdate = "proc_TraderUpdate";		
				meta.spDelete = "proc_TraderDelete";
				meta.spLoadAll = "proc_TraderLoadAll";
				meta.spLoadByPrimaryKey = "proc_TraderLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TraderMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
