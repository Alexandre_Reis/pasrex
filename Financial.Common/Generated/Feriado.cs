/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		







	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esFeriadoCollection : esEntityCollection
	{
		public esFeriadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "FeriadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esFeriadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esFeriadoQuery);
		}
		#endregion
		
		virtual public Feriado DetachEntity(Feriado entity)
		{
			return base.DetachEntity(entity) as Feriado;
		}
		
		virtual public Feriado AttachEntity(Feriado entity)
		{
			return base.AttachEntity(entity) as Feriado;
		}
		
		virtual public void Combine(FeriadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Feriado this[int index]
		{
			get
			{
				return base[index] as Feriado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Feriado);
		}
	}



	[Serializable]
	abstract public class esFeriado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esFeriadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esFeriado()
		{

		}

		public esFeriado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int16 idLocal)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idLocal);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int16 idLocal)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esFeriadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdLocal == idLocal);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int16 idLocal)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idLocal);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int16 idLocal)
		{
			esFeriadoQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdLocal == idLocal);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int16 idLocal)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdLocal",idLocal);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Feriado.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(FeriadoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(FeriadoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to Feriado.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(FeriadoMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				if(base.SetSystemInt16(FeriadoMetadata.ColumnNames.IdLocal, value))
				{
					this._UpToLocalFeriadoByIdLocal = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Feriado.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(FeriadoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(FeriadoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected LocalFeriado _UpToLocalFeriadoByIdLocal;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esFeriado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esFeriado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esFeriadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esFeriado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Feriado : esFeriado
	{

				
		#region UpToLocalFeriadoByIdLocal - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - LocalFeriado_Feriado_FK1
		/// </summary>

		[XmlIgnore]
		public LocalFeriado UpToLocalFeriadoByIdLocal
		{
			get
			{
				if(this._UpToLocalFeriadoByIdLocal == null
					&& IdLocal != null					)
				{
					this._UpToLocalFeriadoByIdLocal = new LocalFeriado();
					this._UpToLocalFeriadoByIdLocal.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToLocalFeriadoByIdLocal", this._UpToLocalFeriadoByIdLocal);
					this._UpToLocalFeriadoByIdLocal.Query.Where(this._UpToLocalFeriadoByIdLocal.Query.IdLocal == this.IdLocal);
					this._UpToLocalFeriadoByIdLocal.Query.Load();
				}

				return this._UpToLocalFeriadoByIdLocal;
			}
			
			set
			{
				this.RemovePreSave("UpToLocalFeriadoByIdLocal");
				

				if(value == null)
				{
					this.IdLocal = null;
					this._UpToLocalFeriadoByIdLocal = null;
				}
				else
				{
					this.IdLocal = value.IdLocal;
					this._UpToLocalFeriadoByIdLocal = value;
					this.SetPreSave("UpToLocalFeriadoByIdLocal", this._UpToLocalFeriadoByIdLocal);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToLocalFeriadoByIdLocal != null)
			{
				this.IdLocal = this._UpToLocalFeriadoByIdLocal.IdLocal;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esFeriadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FeriadoMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, FeriadoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, FeriadoMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, FeriadoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("FeriadoCollection")]
	public partial class FeriadoCollection : esFeriadoCollection, IEnumerable<Feriado>
	{
		public FeriadoCollection()
		{

		}
		
		public static implicit operator List<Feriado>(FeriadoCollection coll)
		{
			List<Feriado> list = new List<Feriado>();
			
			foreach (Feriado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  FeriadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FeriadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Feriado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Feriado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public FeriadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FeriadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(FeriadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Feriado AddNew()
		{
			Feriado entity = base.AddNewEntity() as Feriado;
			
			return entity;
		}

		public Feriado FindByPrimaryKey(System.DateTime data, System.Int16 idLocal)
		{
			return base.FindByPrimaryKey(data, idLocal) as Feriado;
		}


		#region IEnumerable<Feriado> Members

		IEnumerator<Feriado> IEnumerable<Feriado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Feriado;
			}
		}

		#endregion
		
		private FeriadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Feriado' table
	/// </summary>

	[Serializable]
	public partial class Feriado : esFeriado
	{
		public Feriado()
		{

		}
	
		public Feriado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FeriadoMetadata.Meta();
			}
		}
		
		
		
		override protected esFeriadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FeriadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public FeriadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FeriadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(FeriadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private FeriadoQuery query;
	}



	[Serializable]
	public partial class FeriadoQuery : esFeriadoQuery
	{
		public FeriadoQuery()
		{

		}		
		
		public FeriadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class FeriadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FeriadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FeriadoMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = FeriadoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeriadoMetadata.ColumnNames.IdLocal, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = FeriadoMetadata.PropertyNames.IdLocal;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeriadoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = FeriadoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public FeriadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdLocal = "IdLocal";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdLocal = "IdLocal";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FeriadoMetadata))
			{
				if(FeriadoMetadata.mapDelegates == null)
				{
					FeriadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FeriadoMetadata.meta == null)
				{
					FeriadoMetadata.meta = new FeriadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Feriado";
				meta.Destination = "Feriado";
				
				meta.spInsert = "proc_FeriadoInsert";				
				meta.spUpdate = "proc_FeriadoUpdate";		
				meta.spDelete = "proc_FeriadoDelete";
				meta.spLoadAll = "proc_FeriadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_FeriadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FeriadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
