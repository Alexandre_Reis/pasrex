/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/01/2015 17:14:30
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esClassificacaoFaixaRatingCollection : esEntityCollection
	{
		public esClassificacaoFaixaRatingCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClassificacaoFaixaRatingCollection";
		}

		#region Query Logic
		protected void InitQuery(esClassificacaoFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClassificacaoFaixaRatingQuery);
		}
		#endregion
		
		virtual public ClassificacaoFaixaRating DetachEntity(ClassificacaoFaixaRating entity)
		{
			return base.DetachEntity(entity) as ClassificacaoFaixaRating;
		}
		
		virtual public ClassificacaoFaixaRating AttachEntity(ClassificacaoFaixaRating entity)
		{
			return base.AttachEntity(entity) as ClassificacaoFaixaRating;
		}
		
		virtual public void Combine(ClassificacaoFaixaRatingCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClassificacaoFaixaRating this[int index]
		{
			get
			{
				return base[index] as ClassificacaoFaixaRating;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClassificacaoFaixaRating);
		}
	}



	[Serializable]
	abstract public class esClassificacaoFaixaRating : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClassificacaoFaixaRatingQuery GetDynamicQuery()
		{
			return null;
		}

		public esClassificacaoFaixaRating()
		{

		}

		public esClassificacaoFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClassificacaoFaixaRating)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassificacaoFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassificacaoFaixaRating);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClassificacaoFaixaRating)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassificacaoFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassificacaoFaixaRating);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClassificacaoFaixaRating)
		{
			esClassificacaoFaixaRatingQuery query = this.GetDynamicQuery();
			query.Where(query.IdClassificacaoFaixaRating == idClassificacaoFaixaRating);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClassificacaoFaixaRating)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClassificacaoFaixaRating",idClassificacaoFaixaRating);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClassificacaoFaixaRating": this.str.IdClassificacaoFaixaRating = (string)value; break;							
						case "SequenciaAgencia": this.str.SequenciaAgencia = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "Classificacao": this.str.Classificacao = (string)value; break;							
						case "IdPerfilFaixaRating": this.str.IdPerfilFaixaRating = (string)value; break;							
						case "IdAgenciaClassificadora": this.str.IdAgenciaClassificadora = (string)value; break;							
						case "IdPadraoNotas": this.str.IdPadraoNotas = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClassificacaoFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClassificacaoFaixaRating = (System.Int32?)value;
							break;
						
						case "SequenciaAgencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.SequenciaAgencia = (System.Int32?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "Classificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Classificacao = (System.Int32?)value;
							break;
						
						case "IdPerfilFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilFaixaRating = (System.Int32?)value;
							break;
						
						case "IdAgenciaClassificadora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenciaClassificadora = (System.Int32?)value;
							break;
						
						case "IdPadraoNotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPadraoNotas = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.IdClassificacaoFaixaRating
		/// </summary>
		virtual public System.Int32? IdClassificacaoFaixaRating
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdClassificacaoFaixaRating);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdClassificacaoFaixaRating, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.SequenciaAgencia
		/// </summary>
		virtual public System.Int32? SequenciaAgencia
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.SequenciaAgencia);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.SequenciaAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(ClassificacaoFaixaRatingMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(ClassificacaoFaixaRatingMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.Classificacao
		/// </summary>
		virtual public System.Int32? Classificacao
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.Classificacao);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.Classificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.IdPerfilFaixaRating
		/// </summary>
		virtual public System.Int32? IdPerfilFaixaRating
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, value))
				{
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.IdAgenciaClassificadora
		/// </summary>
		virtual public System.Int32? IdAgenciaClassificadora
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdAgenciaClassificadora);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdAgenciaClassificadora, value))
				{
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoFaixaRating.IdPadraoNotas
		/// </summary>
		virtual public System.Int32? IdPadraoNotas
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPadraoNotas);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPadraoNotas, value))
				{
					this._UpToPadraoNotasByIdPadraoNotas = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenciaClassificadora _UpToAgenciaClassificadoraByIdAgenciaClassificadora;
		[CLSCompliant(false)]
		internal protected PadraoNotas _UpToPadraoNotasByIdPadraoNotas;
		[CLSCompliant(false)]
		internal protected PerfilFaixaRating _UpToPerfilFaixaRatingByIdPerfilFaixaRating;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClassificacaoFaixaRating entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClassificacaoFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdClassificacaoFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClassificacaoFaixaRating = null;
					else entity.IdClassificacaoFaixaRating = Convert.ToInt32(value);
				}
			}
				
			public System.String SequenciaAgencia
			{
				get
				{
					System.Int32? data = entity.SequenciaAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SequenciaAgencia = null;
					else entity.SequenciaAgencia = Convert.ToInt32(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Classificacao
			{
				get
				{
					System.Int32? data = entity.Classificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Classificacao = null;
					else entity.Classificacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdPerfilFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilFaixaRating = null;
					else entity.IdPerfilFaixaRating = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenciaClassificadora
			{
				get
				{
					System.Int32? data = entity.IdAgenciaClassificadora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenciaClassificadora = null;
					else entity.IdAgenciaClassificadora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPadraoNotas
			{
				get
				{
					System.Int32? data = entity.IdPadraoNotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPadraoNotas = null;
					else entity.IdPadraoNotas = Convert.ToInt32(value);
				}
			}
			

			private esClassificacaoFaixaRating entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClassificacaoFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClassificacaoFaixaRating can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClassificacaoFaixaRating : esClassificacaoFaixaRating
	{

				
		#region UpToAgenciaClassificadoraByIdAgenciaClassificadora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Classific__IdAge__1A3FCC1E
		/// </summary>

		[XmlIgnore]
		public AgenciaClassificadora UpToAgenciaClassificadoraByIdAgenciaClassificadora
		{
			get
			{
				if(this._UpToAgenciaClassificadoraByIdAgenciaClassificadora == null
					&& IdAgenciaClassificadora != null					)
				{
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = new AgenciaClassificadora();
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora", this._UpToAgenciaClassificadoraByIdAgenciaClassificadora);
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.Where(this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.IdAgenciaClassificadora == this.IdAgenciaClassificadora);
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.Load();
				}

				return this._UpToAgenciaClassificadoraByIdAgenciaClassificadora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora");
				

				if(value == null)
				{
					this.IdAgenciaClassificadora = null;
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = null;
				}
				else
				{
					this.IdAgenciaClassificadora = value.IdAgenciaClassificadora;
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = value;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora", this._UpToAgenciaClassificadoraByIdAgenciaClassificadora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPadraoNotasByIdPadraoNotas - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Classific__IdPad__1B33F057
		/// </summary>

		[XmlIgnore]
		public PadraoNotas UpToPadraoNotasByIdPadraoNotas
		{
			get
			{
				if(this._UpToPadraoNotasByIdPadraoNotas == null
					&& IdPadraoNotas != null					)
				{
					this._UpToPadraoNotasByIdPadraoNotas = new PadraoNotas();
					this._UpToPadraoNotasByIdPadraoNotas.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPadraoNotasByIdPadraoNotas", this._UpToPadraoNotasByIdPadraoNotas);
					this._UpToPadraoNotasByIdPadraoNotas.Query.Where(this._UpToPadraoNotasByIdPadraoNotas.Query.IdPadraoNotas == this.IdPadraoNotas);
					this._UpToPadraoNotasByIdPadraoNotas.Query.Load();
				}

				return this._UpToPadraoNotasByIdPadraoNotas;
			}
			
			set
			{
				this.RemovePreSave("UpToPadraoNotasByIdPadraoNotas");
				

				if(value == null)
				{
					this.IdPadraoNotas = null;
					this._UpToPadraoNotasByIdPadraoNotas = null;
				}
				else
				{
					this.IdPadraoNotas = value.IdPadraoNotas;
					this._UpToPadraoNotasByIdPadraoNotas = value;
					this.SetPreSave("UpToPadraoNotasByIdPadraoNotas", this._UpToPadraoNotasByIdPadraoNotas);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilFaixaRatingByIdPerfilFaixaRating - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Classific__IdPer__194BA7E5
		/// </summary>

		[XmlIgnore]
		public PerfilFaixaRating UpToPerfilFaixaRatingByIdPerfilFaixaRating
		{
			get
			{
				if(this._UpToPerfilFaixaRatingByIdPerfilFaixaRating == null
					&& IdPerfilFaixaRating != null					)
				{
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = new PerfilFaixaRating();
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating", this._UpToPerfilFaixaRatingByIdPerfilFaixaRating);
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.Where(this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.IdPerfilFaixaRating == this.IdPerfilFaixaRating);
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.Query.Load();
				}

				return this._UpToPerfilFaixaRatingByIdPerfilFaixaRating;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating");
				

				if(value == null)
				{
					this.IdPerfilFaixaRating = null;
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = null;
				}
				else
				{
					this.IdPerfilFaixaRating = value.IdPerfilFaixaRating;
					this._UpToPerfilFaixaRatingByIdPerfilFaixaRating = value;
					this.SetPreSave("UpToPerfilFaixaRatingByIdPerfilFaixaRating", this._UpToPerfilFaixaRatingByIdPerfilFaixaRating);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenciaClassificadoraByIdAgenciaClassificadora != null)
			{
				this.IdAgenciaClassificadora = this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.IdAgenciaClassificadora;
			}
			if(!this.es.IsDeleted && this._UpToPadraoNotasByIdPadraoNotas != null)
			{
				this.IdPadraoNotas = this._UpToPadraoNotasByIdPadraoNotas.IdPadraoNotas;
			}
			if(!this.es.IsDeleted && this._UpToPerfilFaixaRatingByIdPerfilFaixaRating != null)
			{
				this.IdPerfilFaixaRating = this._UpToPerfilFaixaRatingByIdPerfilFaixaRating.IdPerfilFaixaRating;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClassificacaoFaixaRatingQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClassificacaoFaixaRatingMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClassificacaoFaixaRating
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.IdClassificacaoFaixaRating, esSystemType.Int32);
			}
		} 
		
		public esQueryItem SequenciaAgencia
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.SequenciaAgencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Classificacao
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.Classificacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilFaixaRating
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenciaClassificadora
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.IdAgenciaClassificadora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPadraoNotas
		{
			get
			{
				return new esQueryItem(this, ClassificacaoFaixaRatingMetadata.ColumnNames.IdPadraoNotas, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClassificacaoFaixaRatingCollection")]
	public partial class ClassificacaoFaixaRatingCollection : esClassificacaoFaixaRatingCollection, IEnumerable<ClassificacaoFaixaRating>
	{
		public ClassificacaoFaixaRatingCollection()
		{

		}
		
		public static implicit operator List<ClassificacaoFaixaRating>(ClassificacaoFaixaRatingCollection coll)
		{
			List<ClassificacaoFaixaRating> list = new List<ClassificacaoFaixaRating>();
			
			foreach (ClassificacaoFaixaRating emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClassificacaoFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassificacaoFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClassificacaoFaixaRating(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClassificacaoFaixaRating();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClassificacaoFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassificacaoFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClassificacaoFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClassificacaoFaixaRating AddNew()
		{
			ClassificacaoFaixaRating entity = base.AddNewEntity() as ClassificacaoFaixaRating;
			
			return entity;
		}

		public ClassificacaoFaixaRating FindByPrimaryKey(System.Int32 idClassificacaoFaixaRating)
		{
			return base.FindByPrimaryKey(idClassificacaoFaixaRating) as ClassificacaoFaixaRating;
		}


		#region IEnumerable<ClassificacaoFaixaRating> Members

		IEnumerator<ClassificacaoFaixaRating> IEnumerable<ClassificacaoFaixaRating>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClassificacaoFaixaRating;
			}
		}

		#endregion
		
		private ClassificacaoFaixaRatingQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClassificacaoFaixaRating' table
	/// </summary>

	[Serializable]
	public partial class ClassificacaoFaixaRating : esClassificacaoFaixaRating
	{
		public ClassificacaoFaixaRating()
		{

		}
	
		public ClassificacaoFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClassificacaoFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esClassificacaoFaixaRatingQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassificacaoFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClassificacaoFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassificacaoFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClassificacaoFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClassificacaoFaixaRatingQuery query;
	}



	[Serializable]
	public partial class ClassificacaoFaixaRatingQuery : esClassificacaoFaixaRatingQuery
	{
		public ClassificacaoFaixaRatingQuery()
		{

		}		
		
		public ClassificacaoFaixaRatingQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClassificacaoFaixaRatingMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClassificacaoFaixaRatingMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.IdClassificacaoFaixaRating, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.IdClassificacaoFaixaRating;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.SequenciaAgencia, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.SequenciaAgencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.DataVigencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.DataVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.Classificacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.Classificacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.IdPerfilFaixaRating;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.IdAgenciaClassificadora, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.IdAgenciaClassificadora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoFaixaRatingMetadata.ColumnNames.IdPadraoNotas, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoFaixaRatingMetadata.PropertyNames.IdPadraoNotas;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClassificacaoFaixaRatingMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClassificacaoFaixaRating = "IdClassificacaoFaixaRating";
			 public const string SequenciaAgencia = "SequenciaAgencia";
			 public const string DataVigencia = "DataVigencia";
			 public const string Classificacao = "Classificacao";
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string IdPadraoNotas = "IdPadraoNotas";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClassificacaoFaixaRating = "IdClassificacaoFaixaRating";
			 public const string SequenciaAgencia = "SequenciaAgencia";
			 public const string DataVigencia = "DataVigencia";
			 public const string Classificacao = "Classificacao";
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string IdPadraoNotas = "IdPadraoNotas";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClassificacaoFaixaRatingMetadata))
			{
				if(ClassificacaoFaixaRatingMetadata.mapDelegates == null)
				{
					ClassificacaoFaixaRatingMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClassificacaoFaixaRatingMetadata.meta == null)
				{
					ClassificacaoFaixaRatingMetadata.meta = new ClassificacaoFaixaRatingMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClassificacaoFaixaRating", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SequenciaAgencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Classificacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilFaixaRating", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenciaClassificadora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPadraoNotas", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ClassificacaoFaixaRating";
				meta.Destination = "ClassificacaoFaixaRating";
				
				meta.spInsert = "proc_ClassificacaoFaixaRatingInsert";				
				meta.spUpdate = "proc_ClassificacaoFaixaRatingUpdate";		
				meta.spDelete = "proc_ClassificacaoFaixaRatingDelete";
				meta.spLoadAll = "proc_ClassificacaoFaixaRatingLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClassificacaoFaixaRatingLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClassificacaoFaixaRatingMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
