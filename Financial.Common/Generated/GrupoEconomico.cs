/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/06/2014 15:44:59
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esGrupoEconomicoCollection : esEntityCollection
	{
		public esGrupoEconomicoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoEconomicoCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoEconomicoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoEconomicoQuery);
		}
		#endregion
		
		virtual public GrupoEconomico DetachEntity(GrupoEconomico entity)
		{
			return base.DetachEntity(entity) as GrupoEconomico;
		}
		
		virtual public GrupoEconomico AttachEntity(GrupoEconomico entity)
		{
			return base.AttachEntity(entity) as GrupoEconomico;
		}
		
		virtual public void Combine(GrupoEconomicoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoEconomico this[int index]
		{
			get
			{
				return base[index] as GrupoEconomico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoEconomico);
		}
	}



	[Serializable]
	abstract public class esGrupoEconomico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoEconomicoQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoEconomico()
		{

		}

		public esGrupoEconomico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupoEconomicoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo)
		{
			esGrupoEconomicoQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "OrdemRelatorio": this.str.OrdemRelatorio = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "OrdemRelatorio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.OrdemRelatorio = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoEconomico.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(GrupoEconomicoMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(GrupoEconomicoMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoEconomico.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(GrupoEconomicoMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(GrupoEconomicoMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoEconomico.OrdemRelatorio
		/// </summary>
		virtual public System.Int32? OrdemRelatorio
		{
			get
			{
				return base.GetSystemInt32(GrupoEconomicoMetadata.ColumnNames.OrdemRelatorio);
			}
			
			set
			{
				base.SetSystemInt32(GrupoEconomicoMetadata.ColumnNames.OrdemRelatorio, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoEconomico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String OrdemRelatorio
			{
				get
				{
					System.Int32? data = entity.OrdemRelatorio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OrdemRelatorio = null;
					else entity.OrdemRelatorio = Convert.ToInt32(value);
				}
			}
			

			private esGrupoEconomico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoEconomicoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoEconomico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoEconomico : esGrupoEconomico
	{

				
		#region AtivoBolsaCollectionByIdGrupoEconomico - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_GrupoEconomico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsaCollection AtivoBolsaCollectionByIdGrupoEconomico
		{
			get
			{
				if(this._AtivoBolsaCollectionByIdGrupoEconomico == null)
				{
					this._AtivoBolsaCollectionByIdGrupoEconomico = new AtivoBolsaCollection();
					this._AtivoBolsaCollectionByIdGrupoEconomico.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBolsaCollectionByIdGrupoEconomico", this._AtivoBolsaCollectionByIdGrupoEconomico);
				
					if(this.IdGrupo != null)
					{
						this._AtivoBolsaCollectionByIdGrupoEconomico.Query.Where(this._AtivoBolsaCollectionByIdGrupoEconomico.Query.IdGrupoEconomico == this.IdGrupo);
						this._AtivoBolsaCollectionByIdGrupoEconomico.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBolsaCollectionByIdGrupoEconomico.fks.Add(AtivoBolsaMetadata.ColumnNames.IdGrupoEconomico, this.IdGrupo);
					}
				}

				return this._AtivoBolsaCollectionByIdGrupoEconomico;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBolsaCollectionByIdGrupoEconomico != null) 
				{ 
					this.RemovePostSave("AtivoBolsaCollectionByIdGrupoEconomico"); 
					this._AtivoBolsaCollectionByIdGrupoEconomico = null;
					
				} 
			} 			
		}

		private AtivoBolsaCollection _AtivoBolsaCollectionByIdGrupoEconomico;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AtivoBolsaCollectionByIdGrupoEconomico", typeof(AtivoBolsaCollection), new AtivoBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AtivoBolsaCollectionByIdGrupoEconomico != null)
			{
				foreach(AtivoBolsa obj in this._AtivoBolsaCollectionByIdGrupoEconomico)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoEconomico = this.IdGrupo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupoEconomicoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoEconomicoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, GrupoEconomicoMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, GrupoEconomicoMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem OrdemRelatorio
		{
			get
			{
				return new esQueryItem(this, GrupoEconomicoMetadata.ColumnNames.OrdemRelatorio, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoEconomicoCollection")]
	public partial class GrupoEconomicoCollection : esGrupoEconomicoCollection, IEnumerable<GrupoEconomico>
	{
		public GrupoEconomicoCollection()
		{

		}
		
		public static implicit operator List<GrupoEconomico>(GrupoEconomicoCollection coll)
		{
			List<GrupoEconomico> list = new List<GrupoEconomico>();
			
			foreach (GrupoEconomico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoEconomicoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoEconomicoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoEconomico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoEconomico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoEconomicoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoEconomicoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoEconomicoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoEconomico AddNew()
		{
			GrupoEconomico entity = base.AddNewEntity() as GrupoEconomico;
			
			return entity;
		}

		public GrupoEconomico FindByPrimaryKey(System.Int32 idGrupo)
		{
			return base.FindByPrimaryKey(idGrupo) as GrupoEconomico;
		}


		#region IEnumerable<GrupoEconomico> Members

		IEnumerator<GrupoEconomico> IEnumerable<GrupoEconomico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoEconomico;
			}
		}

		#endregion
		
		private GrupoEconomicoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoEconomico' table
	/// </summary>

	[Serializable]
	public partial class GrupoEconomico : esGrupoEconomico
	{
		public GrupoEconomico()
		{

		}
	
		public GrupoEconomico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoEconomicoMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoEconomicoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoEconomicoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoEconomicoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoEconomicoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoEconomicoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoEconomicoQuery query;
	}



	[Serializable]
	public partial class GrupoEconomicoQuery : esGrupoEconomicoQuery
	{
		public GrupoEconomicoQuery()
		{

		}		
		
		public GrupoEconomicoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoEconomicoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoEconomicoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoEconomicoMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoEconomicoMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoEconomicoMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoEconomicoMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoEconomicoMetadata.ColumnNames.OrdemRelatorio, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoEconomicoMetadata.PropertyNames.OrdemRelatorio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoEconomicoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Nome = "Nome";
			 public const string OrdemRelatorio = "OrdemRelatorio";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Nome = "Nome";
			 public const string OrdemRelatorio = "OrdemRelatorio";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoEconomicoMetadata))
			{
				if(GrupoEconomicoMetadata.mapDelegates == null)
				{
					GrupoEconomicoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoEconomicoMetadata.meta == null)
				{
					GrupoEconomicoMetadata.meta = new GrupoEconomicoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("OrdemRelatorio", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "GrupoEconomico";
				meta.Destination = "GrupoEconomico";
				
				meta.spInsert = "proc_GrupoEconomicoInsert";				
				meta.spUpdate = "proc_GrupoEconomicoUpdate";		
				meta.spDelete = "proc_GrupoEconomicoDelete";
				meta.spLoadAll = "proc_GrupoEconomicoLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoEconomicoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoEconomicoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
