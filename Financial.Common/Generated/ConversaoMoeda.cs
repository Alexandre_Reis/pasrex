/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esConversaoMoedaCollection : esEntityCollection
	{
		public esConversaoMoedaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ConversaoMoedaCollection";
		}

		#region Query Logic
		protected void InitQuery(esConversaoMoedaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esConversaoMoedaQuery);
		}
		#endregion
		
		virtual public ConversaoMoeda DetachEntity(ConversaoMoeda entity)
		{
			return base.DetachEntity(entity) as ConversaoMoeda;
		}
		
		virtual public ConversaoMoeda AttachEntity(ConversaoMoeda entity)
		{
			return base.AttachEntity(entity) as ConversaoMoeda;
		}
		
		virtual public void Combine(ConversaoMoedaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ConversaoMoeda this[int index]
		{
			get
			{
				return base[index] as ConversaoMoeda;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ConversaoMoeda);
		}
	}



	[Serializable]
	abstract public class esConversaoMoeda : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esConversaoMoedaQuery GetDynamicQuery()
		{
			return null;
		}

		public esConversaoMoeda()
		{

		}

		public esConversaoMoeda(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMoedaDe, idMoedaPara);
			else
				return LoadByPrimaryKeyStoredProcedure(idMoedaDe, idMoedaPara);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esConversaoMoedaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdMoedaDe == idMoedaDe, query.IdMoedaPara == idMoedaPara);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMoedaDe, idMoedaPara);
			else
				return LoadByPrimaryKeyStoredProcedure(idMoedaDe, idMoedaPara);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			esConversaoMoedaQuery query = this.GetDynamicQuery();
			query.Where(query.IdMoedaDe == idMoedaDe, query.IdMoedaPara == idMoedaPara);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMoedaDe",idMoedaDe);			parms.Add("IdMoedaPara",idMoedaPara);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMoedaDe": this.str.IdMoedaDe = (string)value; break;							
						case "IdMoedaPara": this.str.IdMoedaPara = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMoedaDe":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaDe = (System.Int16?)value;
							break;
						
						case "IdMoedaPara":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaPara = (System.Int16?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ConversaoMoeda.IdMoedaDe
		/// </summary>
		virtual public System.Int16? IdMoedaDe
		{
			get
			{
				return base.GetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe);
			}
			
			set
			{
				if(base.SetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe, value))
				{
					this._UpToMoedaByIdMoedaDe = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ConversaoMoeda.IdMoedaPara
		/// </summary>
		virtual public System.Int16? IdMoedaPara
		{
			get
			{
				return base.GetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara);
			}
			
			set
			{
				if(base.SetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara, value))
				{
					this._UpToMoedaByIdMoedaPara = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ConversaoMoeda.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(ConversaoMoedaMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoMoeda.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(ConversaoMoedaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(ConversaoMoedaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaDe;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaPara;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esConversaoMoeda entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMoedaDe
			{
				get
				{
					System.Int16? data = entity.IdMoedaDe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaDe = null;
					else entity.IdMoedaDe = Convert.ToInt16(value);
				}
			}
				
			public System.String IdMoedaPara
			{
				get
				{
					System.Int16? data = entity.IdMoedaPara;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaPara = null;
					else entity.IdMoedaPara = Convert.ToInt16(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esConversaoMoeda entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esConversaoMoedaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esConversaoMoeda can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ConversaoMoeda : esConversaoMoeda
	{

				
		#region UpToMoedaByIdMoedaDe - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Moeda_ConversaoMoeda_FK1
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaDe
		{
			get
			{
				if(this._UpToMoedaByIdMoedaDe == null
					&& IdMoedaDe != null					)
				{
					this._UpToMoedaByIdMoedaDe = new Moeda();
					this._UpToMoedaByIdMoedaDe.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaDe", this._UpToMoedaByIdMoedaDe);
					this._UpToMoedaByIdMoedaDe.Query.Where(this._UpToMoedaByIdMoedaDe.Query.IdMoeda == this.IdMoedaDe);
					this._UpToMoedaByIdMoedaDe.Query.Load();
				}

				return this._UpToMoedaByIdMoedaDe;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaDe");
				

				if(value == null)
				{
					this.IdMoedaDe = null;
					this._UpToMoedaByIdMoedaDe = null;
				}
				else
				{
					this.IdMoedaDe = value.IdMoeda;
					this._UpToMoedaByIdMoedaDe = value;
					this.SetPreSave("UpToMoedaByIdMoedaDe", this._UpToMoedaByIdMoedaDe);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoedaPara - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Moeda_ConversaoMoeda_FK2
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaPara
		{
			get
			{
				if(this._UpToMoedaByIdMoedaPara == null
					&& IdMoedaPara != null					)
				{
					this._UpToMoedaByIdMoedaPara = new Moeda();
					this._UpToMoedaByIdMoedaPara.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaPara", this._UpToMoedaByIdMoedaPara);
					this._UpToMoedaByIdMoedaPara.Query.Where(this._UpToMoedaByIdMoedaPara.Query.IdMoeda == this.IdMoedaPara);
					this._UpToMoedaByIdMoedaPara.Query.Load();
				}

				return this._UpToMoedaByIdMoedaPara;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaPara");
				

				if(value == null)
				{
					this.IdMoedaPara = null;
					this._UpToMoedaByIdMoedaPara = null;
				}
				else
				{
					this.IdMoedaPara = value.IdMoeda;
					this._UpToMoedaByIdMoedaPara = value;
					this.SetPreSave("UpToMoedaByIdMoedaPara", this._UpToMoedaByIdMoedaPara);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaDe != null)
			{
				this.IdMoedaDe = this._UpToMoedaByIdMoedaDe.IdMoeda;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaPara != null)
			{
				this.IdMoedaPara = this._UpToMoedaByIdMoedaPara.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esConversaoMoedaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ConversaoMoedaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMoedaDe
		{
			get
			{
				return new esQueryItem(this, ConversaoMoedaMetadata.ColumnNames.IdMoedaDe, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdMoedaPara
		{
			get
			{
				return new esQueryItem(this, ConversaoMoedaMetadata.ColumnNames.IdMoedaPara, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, ConversaoMoedaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, ConversaoMoedaMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ConversaoMoedaCollection")]
	public partial class ConversaoMoedaCollection : esConversaoMoedaCollection, IEnumerable<ConversaoMoeda>
	{
		public ConversaoMoedaCollection()
		{

		}
		
		public static implicit operator List<ConversaoMoeda>(ConversaoMoedaCollection coll)
		{
			List<ConversaoMoeda> list = new List<ConversaoMoeda>();
			
			foreach (ConversaoMoeda emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ConversaoMoedaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConversaoMoedaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ConversaoMoeda(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ConversaoMoeda();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ConversaoMoedaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConversaoMoedaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ConversaoMoedaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ConversaoMoeda AddNew()
		{
			ConversaoMoeda entity = base.AddNewEntity() as ConversaoMoeda;
			
			return entity;
		}

		public ConversaoMoeda FindByPrimaryKey(System.Int16 idMoedaDe, System.Int16 idMoedaPara)
		{
			return base.FindByPrimaryKey(idMoedaDe, idMoedaPara) as ConversaoMoeda;
		}


		#region IEnumerable<ConversaoMoeda> Members

		IEnumerator<ConversaoMoeda> IEnumerable<ConversaoMoeda>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ConversaoMoeda;
			}
		}

		#endregion
		
		private ConversaoMoedaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ConversaoMoeda' table
	/// </summary>

	[Serializable]
	public partial class ConversaoMoeda : esConversaoMoeda
	{
		public ConversaoMoeda()
		{

		}
	
		public ConversaoMoeda(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ConversaoMoedaMetadata.Meta();
			}
		}
		
		
		
		override protected esConversaoMoedaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConversaoMoedaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ConversaoMoedaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConversaoMoedaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ConversaoMoedaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ConversaoMoedaQuery query;
	}



	[Serializable]
	public partial class ConversaoMoedaQuery : esConversaoMoedaQuery
	{
		public ConversaoMoedaQuery()
		{

		}		
		
		public ConversaoMoedaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ConversaoMoedaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ConversaoMoedaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ConversaoMoedaMetadata.PropertyNames.IdMoedaDe;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ConversaoMoedaMetadata.PropertyNames.IdMoedaPara;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoMoedaMetadata.ColumnNames.IdIndice, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ConversaoMoedaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoMoedaMetadata.ColumnNames.Tipo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ConversaoMoedaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ConversaoMoedaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMoedaDe = "IdMoedaDe";
			 public const string IdMoedaPara = "IdMoedaPara";
			 public const string IdIndice = "IdIndice";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMoedaDe = "IdMoedaDe";
			 public const string IdMoedaPara = "IdMoedaPara";
			 public const string IdIndice = "IdIndice";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ConversaoMoedaMetadata))
			{
				if(ConversaoMoedaMetadata.mapDelegates == null)
				{
					ConversaoMoedaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ConversaoMoedaMetadata.meta == null)
				{
					ConversaoMoedaMetadata.meta = new ConversaoMoedaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMoedaDe", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdMoedaPara", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "ConversaoMoeda";
				meta.Destination = "ConversaoMoeda";
				
				meta.spInsert = "proc_ConversaoMoedaInsert";				
				meta.spUpdate = "proc_ConversaoMoedaUpdate";		
				meta.spDelete = "proc_ConversaoMoedaDelete";
				meta.spLoadAll = "proc_ConversaoMoedaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ConversaoMoedaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ConversaoMoedaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
