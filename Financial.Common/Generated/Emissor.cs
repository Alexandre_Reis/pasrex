/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/08/2015 16:37:53
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		























using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Enquadra;
using Financial.CRM;



		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esEmissorCollection : esEntityCollection
	{
		public esEmissorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EmissorCollection";
		}

		#region Query Logic
		protected void InitQuery(esEmissorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEmissorQuery);
		}
		#endregion
		
		virtual public Emissor DetachEntity(Emissor entity)
		{
			return base.DetachEntity(entity) as Emissor;
		}
		
		virtual public Emissor AttachEntity(Emissor entity)
		{
			return base.AttachEntity(entity) as Emissor;
		}
		
		virtual public void Combine(EmissorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Emissor this[int index]
		{
			get
			{
				return base[index] as Emissor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Emissor);
		}
	}



	[Serializable]
	abstract public class esEmissor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEmissorQuery GetDynamicQuery()
		{
			return null;
		}

		public esEmissor()
		{

		}

		public esEmissor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEmissor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmissor);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmissor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEmissor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEmissorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEmissor == idEmissor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEmissor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmissor);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmissor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEmissor)
		{
			esEmissorQuery query = this.GetDynamicQuery();
			query.Where(query.IdEmissor == idEmissor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEmissor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEmissor",idEmissor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEmissor": this.str.IdEmissor = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "TipoEmissor": this.str.TipoEmissor = (string)value; break;							
						case "IdSetor": this.str.IdSetor = (string)value; break;							
						case "Cnpj": this.str.Cnpj = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmissor = (System.Int32?)value;
							break;
						
						case "TipoEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEmissor = (System.Byte?)value;
							break;
						
						case "IdSetor":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdSetor = (System.Int16?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Emissor.IdEmissor
		/// </summary>
		virtual public System.Int32? IdEmissor
		{
			get
			{
				return base.GetSystemInt32(EmissorMetadata.ColumnNames.IdEmissor);
			}
			
			set
			{
				base.SetSystemInt32(EmissorMetadata.ColumnNames.IdEmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to Emissor.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(EmissorMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(EmissorMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Emissor.TipoEmissor
		/// </summary>
		virtual public System.Byte? TipoEmissor
		{
			get
			{
				return base.GetSystemByte(EmissorMetadata.ColumnNames.TipoEmissor);
			}
			
			set
			{
				base.SetSystemByte(EmissorMetadata.ColumnNames.TipoEmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to Emissor.IdSetor
		/// </summary>
		virtual public System.Int16? IdSetor
		{
			get
			{
				return base.GetSystemInt16(EmissorMetadata.ColumnNames.IdSetor);
			}
			
			set
			{
				if(base.SetSystemInt16(EmissorMetadata.ColumnNames.IdSetor, value))
				{
					this._UpToSetorByIdSetor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Emissor.CNPJ
		/// </summary>
		virtual public System.String Cnpj
		{
			get
			{
				return base.GetSystemString(EmissorMetadata.ColumnNames.Cnpj);
			}
			
			set
			{
				base.SetSystemString(EmissorMetadata.ColumnNames.Cnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to Emissor.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(EmissorMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(EmissorMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Emissor.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(EmissorMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(EmissorMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to Emissor.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(EmissorMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(EmissorMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		[CLSCompliant(false)]
		internal protected Setor _UpToSetorByIdSetor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEmissor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEmissor
			{
				get
				{
					System.Int32? data = entity.IdEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmissor = null;
					else entity.IdEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String TipoEmissor
			{
				get
				{
					System.Byte? data = entity.TipoEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEmissor = null;
					else entity.TipoEmissor = Convert.ToByte(value);
				}
			}
				
			public System.String IdSetor
			{
				get
				{
					System.Int16? data = entity.IdSetor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSetor = null;
					else entity.IdSetor = Convert.ToInt16(value);
				}
			}
				
			public System.String Cnpj
			{
				get
				{
					System.String data = entity.Cnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cnpj = null;
					else entity.Cnpj = Convert.ToString(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
			

			private esEmissor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEmissorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEmissor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Emissor : esEmissor
	{

				
		#region AtivoBolsaCollectionByIdEmissor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Emissor_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsaCollection AtivoBolsaCollectionByIdEmissor
		{
			get
			{
				if(this._AtivoBolsaCollectionByIdEmissor == null)
				{
					this._AtivoBolsaCollectionByIdEmissor = new AtivoBolsaCollection();
					this._AtivoBolsaCollectionByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBolsaCollectionByIdEmissor", this._AtivoBolsaCollectionByIdEmissor);
				
					if(this.IdEmissor != null)
					{
						this._AtivoBolsaCollectionByIdEmissor.Query.Where(this._AtivoBolsaCollectionByIdEmissor.Query.IdEmissor == this.IdEmissor);
						this._AtivoBolsaCollectionByIdEmissor.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBolsaCollectionByIdEmissor.fks.Add(AtivoBolsaMetadata.ColumnNames.IdEmissor, this.IdEmissor);
					}
				}

				return this._AtivoBolsaCollectionByIdEmissor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBolsaCollectionByIdEmissor != null) 
				{ 
					this.RemovePostSave("AtivoBolsaCollectionByIdEmissor"); 
					this._AtivoBolsaCollectionByIdEmissor = null;
					
				} 
			} 			
		}

		private AtivoBolsaCollection _AtivoBolsaCollectionByIdEmissor;
		#endregion

				
		#region EnquadraItemAcaoCollectionByIdEmissor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Emissor_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemAcaoCollection EnquadraItemAcaoCollectionByIdEmissor
		{
			get
			{
				if(this._EnquadraItemAcaoCollectionByIdEmissor == null)
				{
					this._EnquadraItemAcaoCollectionByIdEmissor = new EnquadraItemAcaoCollection();
					this._EnquadraItemAcaoCollectionByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemAcaoCollectionByIdEmissor", this._EnquadraItemAcaoCollectionByIdEmissor);
				
					if(this.IdEmissor != null)
					{
						this._EnquadraItemAcaoCollectionByIdEmissor.Query.Where(this._EnquadraItemAcaoCollectionByIdEmissor.Query.IdEmissor == this.IdEmissor);
						this._EnquadraItemAcaoCollectionByIdEmissor.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemAcaoCollectionByIdEmissor.fks.Add(EnquadraItemAcaoMetadata.ColumnNames.IdEmissor, this.IdEmissor);
					}
				}

				return this._EnquadraItemAcaoCollectionByIdEmissor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemAcaoCollectionByIdEmissor != null) 
				{ 
					this.RemovePostSave("EnquadraItemAcaoCollectionByIdEmissor"); 
					this._EnquadraItemAcaoCollectionByIdEmissor = null;
					
				} 
			} 			
		}

		private EnquadraItemAcaoCollection _EnquadraItemAcaoCollectionByIdEmissor;
		#endregion

				
		#region EnquadraItemRendaFixaCollectionByIdEmissor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Emissor_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemRendaFixaCollection EnquadraItemRendaFixaCollectionByIdEmissor
		{
			get
			{
				if(this._EnquadraItemRendaFixaCollectionByIdEmissor == null)
				{
					this._EnquadraItemRendaFixaCollectionByIdEmissor = new EnquadraItemRendaFixaCollection();
					this._EnquadraItemRendaFixaCollectionByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemRendaFixaCollectionByIdEmissor", this._EnquadraItemRendaFixaCollectionByIdEmissor);
				
					if(this.IdEmissor != null)
					{
						this._EnquadraItemRendaFixaCollectionByIdEmissor.Query.Where(this._EnquadraItemRendaFixaCollectionByIdEmissor.Query.IdEmissor == this.IdEmissor);
						this._EnquadraItemRendaFixaCollectionByIdEmissor.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemRendaFixaCollectionByIdEmissor.fks.Add(EnquadraItemRendaFixaMetadata.ColumnNames.IdEmissor, this.IdEmissor);
					}
				}

				return this._EnquadraItemRendaFixaCollectionByIdEmissor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemRendaFixaCollectionByIdEmissor != null) 
				{ 
					this.RemovePostSave("EnquadraItemRendaFixaCollectionByIdEmissor"); 
					this._EnquadraItemRendaFixaCollectionByIdEmissor = null;
					
				} 
			} 			
		}

		private EnquadraItemRendaFixaCollection _EnquadraItemRendaFixaCollectionByIdEmissor;
		#endregion

				
		#region TituloRendaFixaCollectionByIdEmissor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Emissor_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixaCollection TituloRendaFixaCollectionByIdEmissor
		{
			get
			{
				if(this._TituloRendaFixaCollectionByIdEmissor == null)
				{
					this._TituloRendaFixaCollectionByIdEmissor = new TituloRendaFixaCollection();
					this._TituloRendaFixaCollectionByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TituloRendaFixaCollectionByIdEmissor", this._TituloRendaFixaCollectionByIdEmissor);
				
					if(this.IdEmissor != null)
					{
						this._TituloRendaFixaCollectionByIdEmissor.Query.Where(this._TituloRendaFixaCollectionByIdEmissor.Query.IdEmissor == this.IdEmissor);
						this._TituloRendaFixaCollectionByIdEmissor.Query.Load();

						// Auto-hookup Foreign Keys
						this._TituloRendaFixaCollectionByIdEmissor.fks.Add(TituloRendaFixaMetadata.ColumnNames.IdEmissor, this.IdEmissor);
					}
				}

				return this._TituloRendaFixaCollectionByIdEmissor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TituloRendaFixaCollectionByIdEmissor != null) 
				{ 
					this.RemovePostSave("TituloRendaFixaCollectionByIdEmissor"); 
					this._TituloRendaFixaCollectionByIdEmissor = null;
					
				} 
			} 			
		}

		private TituloRendaFixaCollection _TituloRendaFixaCollectionByIdEmissor;
		#endregion

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Emissor_AgenteMercado
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Emissor_Pessoa_FK
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSetorByIdSetor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Setor_Emissor_FK1
		/// </summary>

		[XmlIgnore]
		public Setor UpToSetorByIdSetor
		{
			get
			{
				if(this._UpToSetorByIdSetor == null
					&& IdSetor != null					)
				{
					this._UpToSetorByIdSetor = new Setor();
					this._UpToSetorByIdSetor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSetorByIdSetor", this._UpToSetorByIdSetor);
					this._UpToSetorByIdSetor.Query.Where(this._UpToSetorByIdSetor.Query.IdSetor == this.IdSetor);
					this._UpToSetorByIdSetor.Query.Load();
				}

				return this._UpToSetorByIdSetor;
			}
			
			set
			{
				this.RemovePreSave("UpToSetorByIdSetor");
				

				if(value == null)
				{
					this.IdSetor = null;
					this._UpToSetorByIdSetor = null;
				}
				else
				{
					this.IdSetor = value.IdSetor;
					this._UpToSetorByIdSetor = value;
					this.SetPreSave("UpToSetorByIdSetor", this._UpToSetorByIdSetor);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AtivoBolsaCollectionByIdEmissor", typeof(AtivoBolsaCollection), new AtivoBolsa()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemAcaoCollectionByIdEmissor", typeof(EnquadraItemAcaoCollection), new EnquadraItemAcao()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemRendaFixaCollectionByIdEmissor", typeof(EnquadraItemRendaFixaCollection), new EnquadraItemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "TituloRendaFixaCollectionByIdEmissor", typeof(TituloRendaFixaCollection), new TituloRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToSetorByIdSetor != null)
			{
				this.IdSetor = this._UpToSetorByIdSetor.IdSetor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AtivoBolsaCollectionByIdEmissor != null)
			{
				foreach(AtivoBolsa obj in this._AtivoBolsaCollectionByIdEmissor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEmissor = this.IdEmissor;
					}
				}
			}
			if(this._EnquadraItemAcaoCollectionByIdEmissor != null)
			{
				foreach(EnquadraItemAcao obj in this._EnquadraItemAcaoCollectionByIdEmissor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEmissor = this.IdEmissor;
					}
				}
			}
			if(this._EnquadraItemRendaFixaCollectionByIdEmissor != null)
			{
				foreach(EnquadraItemRendaFixa obj in this._EnquadraItemRendaFixaCollectionByIdEmissor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEmissor = this.IdEmissor;
					}
				}
			}
			if(this._TituloRendaFixaCollectionByIdEmissor != null)
			{
				foreach(TituloRendaFixa obj in this._TituloRendaFixaCollectionByIdEmissor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEmissor = this.IdEmissor;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEmissorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EmissorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEmissor
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.IdEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoEmissor
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.TipoEmissor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdSetor
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.IdSetor, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Cnpj
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.Cnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, EmissorMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EmissorCollection")]
	public partial class EmissorCollection : esEmissorCollection, IEnumerable<Emissor>
	{
		public EmissorCollection()
		{

		}
		
		public static implicit operator List<Emissor>(EmissorCollection coll)
		{
			List<Emissor> list = new List<Emissor>();
			
			foreach (Emissor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EmissorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EmissorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Emissor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Emissor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EmissorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmissorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EmissorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Emissor AddNew()
		{
			Emissor entity = base.AddNewEntity() as Emissor;
			
			return entity;
		}

		public Emissor FindByPrimaryKey(System.Int32 idEmissor)
		{
			return base.FindByPrimaryKey(idEmissor) as Emissor;
		}


		#region IEnumerable<Emissor> Members

		IEnumerator<Emissor> IEnumerable<Emissor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Emissor;
			}
		}

		#endregion
		
		private EmissorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Emissor' table
	/// </summary>

	[Serializable]
	public partial class Emissor : esEmissor
	{
		public Emissor()
		{

		}
	
		public Emissor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EmissorMetadata.Meta();
			}
		}
		
		
		
		override protected esEmissorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EmissorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EmissorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmissorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EmissorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EmissorQuery query;
	}



	[Serializable]
	public partial class EmissorQuery : esEmissorQuery
	{
		public EmissorQuery()
		{

		}		
		
		public EmissorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EmissorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EmissorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EmissorMetadata.ColumnNames.IdEmissor, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmissorMetadata.PropertyNames.IdEmissor;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EmissorMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.TipoEmissor, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EmissorMetadata.PropertyNames.TipoEmissor;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.IdSetor, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EmissorMetadata.PropertyNames.IdSetor;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.Cnpj, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EmissorMetadata.PropertyNames.Cnpj;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.IdAgente, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmissorMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.CodigoInterface, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = EmissorMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmissorMetadata.ColumnNames.IdPessoa, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmissorMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EmissorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEmissor = "IdEmissor";
			 public const string Nome = "Nome";
			 public const string TipoEmissor = "TipoEmissor";
			 public const string IdSetor = "IdSetor";
			 public const string Cnpj = "CNPJ";
			 public const string IdAgente = "IdAgente";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEmissor = "IdEmissor";
			 public const string Nome = "Nome";
			 public const string TipoEmissor = "TipoEmissor";
			 public const string IdSetor = "IdSetor";
			 public const string Cnpj = "Cnpj";
			 public const string IdAgente = "IdAgente";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EmissorMetadata))
			{
				if(EmissorMetadata.mapDelegates == null)
				{
					EmissorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EmissorMetadata.meta == null)
				{
					EmissorMetadata.meta = new EmissorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEmissor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoEmissor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdSetor", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("CNPJ", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Emissor";
				meta.Destination = "Emissor";
				
				meta.spInsert = "proc_EmissorInsert";				
				meta.spUpdate = "proc_EmissorUpdate";		
				meta.spDelete = "proc_EmissorDelete";
				meta.spLoadAll = "proc_EmissorLoadAll";
				meta.spLoadByPrimaryKey = "proc_EmissorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EmissorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
