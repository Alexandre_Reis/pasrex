/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		







	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esCorretoraCollection : esEntityCollection
	{
		public esCorretoraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CorretoraCollection";
		}

		#region Query Logic
		protected void InitQuery(esCorretoraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCorretoraQuery);
		}
		#endregion
		
		virtual public Corretora DetachEntity(Corretora entity)
		{
			return base.DetachEntity(entity) as Corretora;
		}
		
		virtual public Corretora AttachEntity(Corretora entity)
		{
			return base.AttachEntity(entity) as Corretora;
		}
		
		virtual public void Combine(CorretoraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Corretora this[int index]
		{
			get
			{
				return base[index] as Corretora;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Corretora);
		}
	}



	[Serializable]
	abstract public class esCorretora : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCorretoraQuery GetDynamicQuery()
		{
			return null;
		}

		public esCorretora()
		{

		}

		public esCorretora(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCorretora)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCorretora);
			else
				return LoadByPrimaryKeyStoredProcedure(idCorretora);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCorretora)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCorretoraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCorretora == idCorretora);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCorretora)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCorretora);
			else
				return LoadByPrimaryKeyStoredProcedure(idCorretora);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCorretora)
		{
			esCorretoraQuery query = this.GetDynamicQuery();
			query.Where(query.IdCorretora == idCorretora);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCorretora)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCorretora",idCorretora);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCorretora": this.str.IdCorretora = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CorretagemAdicional": this.str.CorretagemAdicional = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCorretora = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "CorretagemAdicional":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CorretagemAdicional = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Corretora.IdCorretora
		/// </summary>
		virtual public System.Int32? IdCorretora
		{
			get
			{
				return base.GetSystemInt32(CorretoraMetadata.ColumnNames.IdCorretora);
			}
			
			set
			{
				base.SetSystemInt32(CorretoraMetadata.ColumnNames.IdCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to Corretora.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(CorretoraMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(CorretoraMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Corretora.CorretagemAdicional
		/// </summary>
		virtual public System.Byte? CorretagemAdicional
		{
			get
			{
				return base.GetSystemByte(CorretoraMetadata.ColumnNames.CorretagemAdicional);
			}
			
			set
			{
				base.SetSystemByte(CorretoraMetadata.ColumnNames.CorretagemAdicional, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCorretora entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCorretora
			{
				get
				{
					System.Int32? data = entity.IdCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCorretora = null;
					else entity.IdCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CorretagemAdicional
			{
				get
				{
					System.Byte? data = entity.CorretagemAdicional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CorretagemAdicional = null;
					else entity.CorretagemAdicional = Convert.ToByte(value);
				}
			}
			

			private esCorretora entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCorretoraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCorretora can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Corretora : esCorretora
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_Corretora_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCorretoraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CorretoraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCorretora
		{
			get
			{
				return new esQueryItem(this, CorretoraMetadata.ColumnNames.IdCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, CorretoraMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CorretagemAdicional
		{
			get
			{
				return new esQueryItem(this, CorretoraMetadata.ColumnNames.CorretagemAdicional, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CorretoraCollection")]
	public partial class CorretoraCollection : esCorretoraCollection, IEnumerable<Corretora>
	{
		public CorretoraCollection()
		{

		}
		
		public static implicit operator List<Corretora>(CorretoraCollection coll)
		{
			List<Corretora> list = new List<Corretora>();
			
			foreach (Corretora emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CorretoraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CorretoraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Corretora(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Corretora();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CorretoraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CorretoraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CorretoraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Corretora AddNew()
		{
			Corretora entity = base.AddNewEntity() as Corretora;
			
			return entity;
		}

		public Corretora FindByPrimaryKey(System.Int32 idCorretora)
		{
			return base.FindByPrimaryKey(idCorretora) as Corretora;
		}


		#region IEnumerable<Corretora> Members

		IEnumerator<Corretora> IEnumerable<Corretora>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Corretora;
			}
		}

		#endregion
		
		private CorretoraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Corretora' table
	/// </summary>

	[Serializable]
	public partial class Corretora : esCorretora
	{
		public Corretora()
		{

		}
	
		public Corretora(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CorretoraMetadata.Meta();
			}
		}
		
		
		
		override protected esCorretoraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CorretoraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CorretoraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CorretoraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CorretoraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CorretoraQuery query;
	}



	[Serializable]
	public partial class CorretoraQuery : esCorretoraQuery
	{
		public CorretoraQuery()
		{

		}		
		
		public CorretoraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CorretoraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CorretoraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CorretoraMetadata.ColumnNames.IdCorretora, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CorretoraMetadata.PropertyNames.IdCorretora;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CorretoraMetadata.ColumnNames.IdAgente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CorretoraMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CorretoraMetadata.ColumnNames.CorretagemAdicional, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CorretoraMetadata.PropertyNames.CorretagemAdicional;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CorretoraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCorretora = "IdCorretora";
			 public const string IdAgente = "IdAgente";
			 public const string CorretagemAdicional = "CorretagemAdicional";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCorretora = "IdCorretora";
			 public const string IdAgente = "IdAgente";
			 public const string CorretagemAdicional = "CorretagemAdicional";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CorretoraMetadata))
			{
				if(CorretoraMetadata.mapDelegates == null)
				{
					CorretoraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CorretoraMetadata.meta == null)
				{
					CorretoraMetadata.meta = new CorretoraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CorretagemAdicional", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "Corretora";
				meta.Destination = "Corretora";
				
				meta.spInsert = "proc_CorretoraInsert";				
				meta.spUpdate = "proc_CorretoraUpdate";		
				meta.spDelete = "proc_CorretoraDelete";
				meta.spLoadAll = "proc_CorretoraLoadAll";
				meta.spLoadByPrimaryKey = "proc_CorretoraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CorretoraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
