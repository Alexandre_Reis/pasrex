/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/09/2014 17:48:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa;

namespace Financial.Common
{

	[Serializable]
	abstract public class esFeederCollection : esEntityCollection
	{
		public esFeederCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "FeederCollection";
		}

		#region Query Logic
		protected void InitQuery(esFeederQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esFeederQuery);
		}
		#endregion
		
		virtual public Feeder DetachEntity(Feeder entity)
		{
			return base.DetachEntity(entity) as Feeder;
		}
		
		virtual public Feeder AttachEntity(Feeder entity)
		{
			return base.AttachEntity(entity) as Feeder;
		}
		
		virtual public void Combine(FeederCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Feeder this[int index]
		{
			get
			{
				return base[index] as Feeder;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Feeder);
		}
	}



	[Serializable]
	abstract public class esFeeder : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esFeederQuery GetDynamicQuery()
		{
			return null;
		}

		public esFeeder()
		{

		}

		public esFeeder(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idFeeder)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFeeder);
			else
				return LoadByPrimaryKeyStoredProcedure(idFeeder);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idFeeder)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFeeder);
			else
				return LoadByPrimaryKeyStoredProcedure(idFeeder);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idFeeder)
		{
			esFeederQuery query = this.GetDynamicQuery();
			query.Where(query.IdFeeder == idFeeder);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idFeeder)
		{
			esParameters parms = new esParameters();
			parms.Add("IdFeeder",idFeeder);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdFeeder": this.str.IdFeeder = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Url": this.str.Url = (string)value; break;							
						case "LocalJarFile": this.str.LocalJarFile = (string)value; break;							
						case "LocalDados": this.str.LocalDados = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdFeeder":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFeeder = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Feeder.IdFeeder
		/// </summary>
		virtual public System.Int32? IdFeeder
		{
			get
			{
				return base.GetSystemInt32(FeederMetadata.ColumnNames.IdFeeder);
			}
			
			set
			{
				base.SetSystemInt32(FeederMetadata.ColumnNames.IdFeeder, value);
			}
		}
		
		/// <summary>
		/// Maps to Feeder.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(FeederMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(FeederMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Feeder.Url
		/// </summary>
		virtual public System.String Url
		{
			get
			{
				return base.GetSystemString(FeederMetadata.ColumnNames.Url);
			}
			
			set
			{
				base.SetSystemString(FeederMetadata.ColumnNames.Url, value);
			}
		}
		
		/// <summary>
		/// Maps to Feeder.LocalJarFile
		/// </summary>
		virtual public System.String LocalJarFile
		{
			get
			{
				return base.GetSystemString(FeederMetadata.ColumnNames.LocalJarFile);
			}
			
			set
			{
				base.SetSystemString(FeederMetadata.ColumnNames.LocalJarFile, value);
			}
		}
		
		/// <summary>
		/// Maps to Feeder.LocalDados
		/// </summary>
		virtual public System.String LocalDados
		{
			get
			{
				return base.GetSystemString(FeederMetadata.ColumnNames.LocalDados);
			}
			
			set
			{
				base.SetSystemString(FeederMetadata.ColumnNames.LocalDados, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esFeeder entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdFeeder
			{
				get
				{
					System.Int32? data = entity.IdFeeder;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFeeder = null;
					else entity.IdFeeder = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Url
			{
				get
				{
					System.String data = entity.Url;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Url = null;
					else entity.Url = Convert.ToString(value);
				}
			}
				
			public System.String LocalJarFile
			{
				get
				{
					System.String data = entity.LocalJarFile;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LocalJarFile = null;
					else entity.LocalJarFile = Convert.ToString(value);
				}
			}
				
			public System.String LocalDados
			{
				get
				{
					System.String data = entity.LocalDados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LocalDados = null;
					else entity.LocalDados = Convert.ToString(value);
				}
			}
			

			private esFeeder entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esFeederQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esFeeder can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Feeder : esFeeder
	{

				
		#region SerieRendaFixaCollectionByIdFeeder - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - SerieRF_Feeder_FK
		/// </summary>

		[XmlIgnore]
		public SerieRendaFixaCollection SerieRendaFixaCollectionByIdFeeder
		{
			get
			{
				if(this._SerieRendaFixaCollectionByIdFeeder == null)
				{
					this._SerieRendaFixaCollectionByIdFeeder = new SerieRendaFixaCollection();
					this._SerieRendaFixaCollectionByIdFeeder.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SerieRendaFixaCollectionByIdFeeder", this._SerieRendaFixaCollectionByIdFeeder);
				
					if(this.IdFeeder != null)
					{
						this._SerieRendaFixaCollectionByIdFeeder.Query.Where(this._SerieRendaFixaCollectionByIdFeeder.Query.IdFeeder == this.IdFeeder);
						this._SerieRendaFixaCollectionByIdFeeder.Query.Load();

						// Auto-hookup Foreign Keys
						this._SerieRendaFixaCollectionByIdFeeder.fks.Add(SerieRendaFixaMetadata.ColumnNames.IdFeeder, this.IdFeeder);
					}
				}

				return this._SerieRendaFixaCollectionByIdFeeder;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SerieRendaFixaCollectionByIdFeeder != null) 
				{ 
					this.RemovePostSave("SerieRendaFixaCollectionByIdFeeder"); 
					this._SerieRendaFixaCollectionByIdFeeder = null;
					
				} 
			} 			
		}

		private SerieRendaFixaCollection _SerieRendaFixaCollectionByIdFeeder;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SerieRendaFixaCollectionByIdFeeder", typeof(SerieRendaFixaCollection), new SerieRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._SerieRendaFixaCollectionByIdFeeder != null)
			{
				foreach(SerieRendaFixa obj in this._SerieRendaFixaCollectionByIdFeeder)
				{
					if(obj.es.IsAdded)
					{
						obj.IdFeeder = this.IdFeeder;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esFeederQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FeederMetadata.Meta();
			}
		}	
		

		public esQueryItem IdFeeder
		{
			get
			{
				return new esQueryItem(this, FeederMetadata.ColumnNames.IdFeeder, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, FeederMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Url
		{
			get
			{
				return new esQueryItem(this, FeederMetadata.ColumnNames.Url, esSystemType.String);
			}
		} 
		
		public esQueryItem LocalJarFile
		{
			get
			{
				return new esQueryItem(this, FeederMetadata.ColumnNames.LocalJarFile, esSystemType.String);
			}
		} 
		
		public esQueryItem LocalDados
		{
			get
			{
				return new esQueryItem(this, FeederMetadata.ColumnNames.LocalDados, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("FeederCollection")]
	public partial class FeederCollection : esFeederCollection, IEnumerable<Feeder>
	{
		public FeederCollection()
		{

		}
		
		public static implicit operator List<Feeder>(FeederCollection coll)
		{
			List<Feeder> list = new List<Feeder>();
			
			foreach (Feeder emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  FeederMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FeederQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Feeder(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Feeder();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public FeederQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FeederQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(FeederQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Feeder AddNew()
		{
			Feeder entity = base.AddNewEntity() as Feeder;
			
			return entity;
		}

		public Feeder FindByPrimaryKey(System.Int32 idFeeder)
		{
			return base.FindByPrimaryKey(idFeeder) as Feeder;
		}


		#region IEnumerable<Feeder> Members

		IEnumerator<Feeder> IEnumerable<Feeder>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Feeder;
			}
		}

		#endregion
		
		private FeederQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Feeder' table
	/// </summary>

	[Serializable]
	public partial class Feeder : esFeeder
	{
		public Feeder()
		{

		}
	
		public Feeder(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FeederMetadata.Meta();
			}
		}
		
		
		
		override protected esFeederQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FeederQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public FeederQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FeederQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(FeederQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private FeederQuery query;
	}



	[Serializable]
	public partial class FeederQuery : esFeederQuery
	{
		public FeederQuery()
		{

		}		
		
		public FeederQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class FeederMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FeederMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FeederMetadata.ColumnNames.IdFeeder, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FeederMetadata.PropertyNames.IdFeeder;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeederMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = FeederMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeederMetadata.ColumnNames.Url, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = FeederMetadata.PropertyNames.Url;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeederMetadata.ColumnNames.LocalJarFile, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = FeederMetadata.PropertyNames.LocalJarFile;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FeederMetadata.ColumnNames.LocalDados, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = FeederMetadata.PropertyNames.LocalDados;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public FeederMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdFeeder = "IdFeeder";
			 public const string Descricao = "Descricao";
			 public const string Url = "Url";
			 public const string LocalJarFile = "LocalJarFile";
			 public const string LocalDados = "LocalDados";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdFeeder = "IdFeeder";
			 public const string Descricao = "Descricao";
			 public const string Url = "Url";
			 public const string LocalJarFile = "LocalJarFile";
			 public const string LocalDados = "LocalDados";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FeederMetadata))
			{
				if(FeederMetadata.mapDelegates == null)
				{
					FeederMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FeederMetadata.meta == null)
				{
					FeederMetadata.meta = new FeederMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdFeeder", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Url", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("LocalJarFile", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("LocalDados", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Feeder";
				meta.Destination = "Feeder";
				
				meta.spInsert = "proc_FeederInsert";				
				meta.spUpdate = "proc_FeederUpdate";		
				meta.spDelete = "proc_FeederDelete";
				meta.spLoadAll = "proc_FeederLoadAll";
				meta.spLoadByPrimaryKey = "proc_FeederLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FeederMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
