/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 24/06/2014 17:06:38
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		







	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esCotacaoIndiceCollection : esEntityCollection
	{
		public esCotacaoIndiceCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoIndiceCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoIndiceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoIndiceQuery);
		}
		#endregion
		
		virtual public CotacaoIndice DetachEntity(CotacaoIndice entity)
		{
			return base.DetachEntity(entity) as CotacaoIndice;
		}
		
		virtual public CotacaoIndice AttachEntity(CotacaoIndice entity)
		{
			return base.AttachEntity(entity) as CotacaoIndice;
		}
		
		virtual public void Combine(CotacaoIndiceCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoIndice this[int index]
		{
			get
			{
				return base[index] as CotacaoIndice;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoIndice);
		}
	}



	[Serializable]
	abstract public class esCotacaoIndice : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoIndiceQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoIndice()
		{

		}

		public esCotacaoIndice(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int16 idIndice)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idIndice);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idIndice);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int16 idIndice)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoIndiceQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdIndice == idIndice);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int16 idIndice)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idIndice);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idIndice);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int16 idIndice)
		{
			esCotacaoIndiceQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdIndice == idIndice);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int16 idIndice)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdIndice",idIndice);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "FatorDiario": this.str.FatorDiario = (string)value; break;							
						case "FatorAcumulado": this.str.FatorAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "FatorDiario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorDiario = (System.Decimal?)value;
							break;
						
						case "FatorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoIndice.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoIndiceMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoIndiceMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoIndice.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(CotacaoIndiceMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(CotacaoIndiceMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CotacaoIndice.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoIndice.FatorDiario
		/// </summary>
		virtual public System.Decimal? FatorDiario
		{
			get
			{
				return base.GetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.FatorDiario);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.FatorDiario, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoIndice.FatorAcumulado
		/// </summary>
		virtual public System.Decimal? FatorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.FatorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoIndiceMetadata.ColumnNames.FatorAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoIndice entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorDiario
			{
				get
				{
					System.Decimal? data = entity.FatorDiario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorDiario = null;
					else entity.FatorDiario = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorAcumulado
			{
				get
				{
					System.Decimal? data = entity.FatorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorAcumulado = null;
					else entity.FatorAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoIndice entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoIndiceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoIndice can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoIndice : esCotacaoIndice
	{

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_CotacaoIndice_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoIndiceQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoIndiceMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoIndiceMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, CotacaoIndiceMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, CotacaoIndiceMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorDiario
		{
			get
			{
				return new esQueryItem(this, CotacaoIndiceMetadata.ColumnNames.FatorDiario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorAcumulado
		{
			get
			{
				return new esQueryItem(this, CotacaoIndiceMetadata.ColumnNames.FatorAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoIndiceCollection")]
	public partial class CotacaoIndiceCollection : esCotacaoIndiceCollection, IEnumerable<CotacaoIndice>
	{
		public CotacaoIndiceCollection()
		{

		}
		
		public static implicit operator List<CotacaoIndice>(CotacaoIndiceCollection coll)
		{
			List<CotacaoIndice> list = new List<CotacaoIndice>();
			
			foreach (CotacaoIndice emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoIndiceMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoIndiceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoIndice(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoIndice();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoIndiceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoIndiceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoIndiceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoIndice AddNew()
		{
			CotacaoIndice entity = base.AddNewEntity() as CotacaoIndice;
			
			return entity;
		}

		public CotacaoIndice FindByPrimaryKey(System.DateTime data, System.Int16 idIndice)
		{
			return base.FindByPrimaryKey(data, idIndice) as CotacaoIndice;
		}


		#region IEnumerable<CotacaoIndice> Members

		IEnumerator<CotacaoIndice> IEnumerable<CotacaoIndice>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoIndice;
			}
		}

		#endregion
		
		private CotacaoIndiceQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoIndice' table
	/// </summary>

	[Serializable]
	public partial class CotacaoIndice : esCotacaoIndice
	{
		public CotacaoIndice()
		{

		}
	
		public CotacaoIndice(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoIndiceMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoIndiceQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoIndiceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoIndiceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoIndiceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoIndiceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoIndiceQuery query;
	}



	[Serializable]
	public partial class CotacaoIndiceQuery : esCotacaoIndiceQuery
	{
		public CotacaoIndiceQuery()
		{

		}		
		
		public CotacaoIndiceQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoIndiceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoIndiceMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoIndiceMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoIndiceMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoIndiceMetadata.ColumnNames.IdIndice, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CotacaoIndiceMetadata.PropertyNames.IdIndice;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoIndiceMetadata.ColumnNames.Valor, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoIndiceMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoIndiceMetadata.ColumnNames.FatorDiario, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoIndiceMetadata.PropertyNames.FatorDiario;	
			c.NumericPrecision = 28;
			c.NumericScale = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoIndiceMetadata.ColumnNames.FatorAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoIndiceMetadata.PropertyNames.FatorAcumulado;	
			c.NumericPrecision = 28;
			c.NumericScale = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoIndiceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdIndice = "IdIndice";
			 public const string Valor = "Valor";
			 public const string FatorDiario = "FatorDiario";
			 public const string FatorAcumulado = "FatorAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdIndice = "IdIndice";
			 public const string Valor = "Valor";
			 public const string FatorDiario = "FatorDiario";
			 public const string FatorAcumulado = "FatorAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoIndiceMetadata))
			{
				if(CotacaoIndiceMetadata.mapDelegates == null)
				{
					CotacaoIndiceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoIndiceMetadata.meta == null)
				{
					CotacaoIndiceMetadata.meta = new CotacaoIndiceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorDiario", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoIndice";
				meta.Destination = "CotacaoIndice";
				
				meta.spInsert = "proc_CotacaoIndiceInsert";				
				meta.spUpdate = "proc_CotacaoIndiceUpdate";		
				meta.spDelete = "proc_CotacaoIndiceDelete";
				meta.spLoadAll = "proc_CotacaoIndiceLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoIndiceLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoIndiceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
