/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2015 10:10:50
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esPadraoNotasCollection : esEntityCollection
	{
		public esPadraoNotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PadraoNotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esPadraoNotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPadraoNotasQuery);
		}
		#endregion
		
		virtual public PadraoNotas DetachEntity(PadraoNotas entity)
		{
			return base.DetachEntity(entity) as PadraoNotas;
		}
		
		virtual public PadraoNotas AttachEntity(PadraoNotas entity)
		{
			return base.AttachEntity(entity) as PadraoNotas;
		}
		
		virtual public void Combine(PadraoNotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PadraoNotas this[int index]
		{
			get
			{
				return base[index] as PadraoNotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PadraoNotas);
		}
	}



	[Serializable]
	abstract public class esPadraoNotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPadraoNotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esPadraoNotas()
		{

		}

		public esPadraoNotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPadraoNotas)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPadraoNotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idPadraoNotas);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPadraoNotas)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPadraoNotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idPadraoNotas);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPadraoNotas)
		{
			esPadraoNotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdPadraoNotas == idPadraoNotas);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPadraoNotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPadraoNotas",idPadraoNotas);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPadraoNotas": this.str.IdPadraoNotas = (string)value; break;							
						case "ItemAvaliado": this.str.ItemAvaliado = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "CodigoNota": this.str.CodigoNota = (string)value; break;							
						case "DescricaoNota": this.str.DescricaoNota = (string)value; break;							
						case "Sequencia": this.str.Sequencia = (string)value; break;							
						case "IdAgencia": this.str.IdAgencia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPadraoNotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPadraoNotas = (System.Int32?)value;
							break;
						
						case "ItemAvaliado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ItemAvaliado = (System.Int32?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "Sequencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Sequencia = (System.Int32?)value;
							break;
						
						case "IdAgencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgencia = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PadraoNotas.IdPadraoNotas
		/// </summary>
		virtual public System.Int32? IdPadraoNotas
		{
			get
			{
				return base.GetSystemInt32(PadraoNotasMetadata.ColumnNames.IdPadraoNotas);
			}
			
			set
			{
				base.SetSystemInt32(PadraoNotasMetadata.ColumnNames.IdPadraoNotas, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.ItemAvaliado
		/// </summary>
		virtual public System.Int32? ItemAvaliado
		{
			get
			{
				return base.GetSystemInt32(PadraoNotasMetadata.ColumnNames.ItemAvaliado);
			}
			
			set
			{
				base.SetSystemInt32(PadraoNotasMetadata.ColumnNames.ItemAvaliado, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(PadraoNotasMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(PadraoNotasMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.CodigoNota
		/// </summary>
		virtual public System.String CodigoNota
		{
			get
			{
				return base.GetSystemString(PadraoNotasMetadata.ColumnNames.CodigoNota);
			}
			
			set
			{
				base.SetSystemString(PadraoNotasMetadata.ColumnNames.CodigoNota, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.DescricaoNota
		/// </summary>
		virtual public System.String DescricaoNota
		{
			get
			{
				return base.GetSystemString(PadraoNotasMetadata.ColumnNames.DescricaoNota);
			}
			
			set
			{
				base.SetSystemString(PadraoNotasMetadata.ColumnNames.DescricaoNota, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.Sequencia
		/// </summary>
		virtual public System.Int32? Sequencia
		{
			get
			{
				return base.GetSystemInt32(PadraoNotasMetadata.ColumnNames.Sequencia);
			}
			
			set
			{
				base.SetSystemInt32(PadraoNotasMetadata.ColumnNames.Sequencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PadraoNotas.IdAgencia
		/// </summary>
		virtual public System.Int32? IdAgencia
		{
			get
			{
				return base.GetSystemInt32(PadraoNotasMetadata.ColumnNames.IdAgencia);
			}
			
			set
			{
				if(base.SetSystemInt32(PadraoNotasMetadata.ColumnNames.IdAgencia, value))
				{
					this._UpToAgenciaClassificadoraByIdAgencia = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenciaClassificadora _UpToAgenciaClassificadoraByIdAgencia;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPadraoNotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPadraoNotas
			{
				get
				{
					System.Int32? data = entity.IdPadraoNotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPadraoNotas = null;
					else entity.IdPadraoNotas = Convert.ToInt32(value);
				}
			}
				
			public System.String ItemAvaliado
			{
				get
				{
					System.Int32? data = entity.ItemAvaliado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ItemAvaliado = null;
					else entity.ItemAvaliado = Convert.ToInt32(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoNota
			{
				get
				{
					System.String data = entity.CodigoNota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoNota = null;
					else entity.CodigoNota = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoNota
			{
				get
				{
					System.String data = entity.DescricaoNota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoNota = null;
					else entity.DescricaoNota = Convert.ToString(value);
				}
			}
				
			public System.String Sequencia
			{
				get
				{
					System.Int32? data = entity.Sequencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sequencia = null;
					else entity.Sequencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgencia
			{
				get
				{
					System.Int32? data = entity.IdAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgencia = null;
					else entity.IdAgencia = Convert.ToInt32(value);
				}
			}
			

			private esPadraoNotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPadraoNotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPadraoNotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PadraoNotas : esPadraoNotas
	{

				
		#region UpToAgenciaClassificadoraByIdAgencia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__PadraoNot__IdAge__7BBB44FE
		/// </summary>

		[XmlIgnore]
		public AgenciaClassificadora UpToAgenciaClassificadoraByIdAgencia
		{
			get
			{
				if(this._UpToAgenciaClassificadoraByIdAgencia == null
					&& IdAgencia != null					)
				{
					this._UpToAgenciaClassificadoraByIdAgencia = new AgenciaClassificadora();
					this._UpToAgenciaClassificadoraByIdAgencia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgencia", this._UpToAgenciaClassificadoraByIdAgencia);
					this._UpToAgenciaClassificadoraByIdAgencia.Query.Where(this._UpToAgenciaClassificadoraByIdAgencia.Query.IdAgenciaClassificadora == this.IdAgencia);
					this._UpToAgenciaClassificadoraByIdAgencia.Query.Load();
				}

				return this._UpToAgenciaClassificadoraByIdAgencia;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenciaClassificadoraByIdAgencia");
				

				if(value == null)
				{
					this.IdAgencia = null;
					this._UpToAgenciaClassificadoraByIdAgencia = null;
				}
				else
				{
					this.IdAgencia = value.IdAgenciaClassificadora;
					this._UpToAgenciaClassificadoraByIdAgencia = value;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgencia", this._UpToAgenciaClassificadoraByIdAgencia);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenciaClassificadoraByIdAgencia != null)
			{
				this.IdAgencia = this._UpToAgenciaClassificadoraByIdAgencia.IdAgenciaClassificadora;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPadraoNotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PadraoNotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPadraoNotas
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.IdPadraoNotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ItemAvaliado
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.ItemAvaliado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoNota
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.CodigoNota, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoNota
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.DescricaoNota, esSystemType.String);
			}
		} 
		
		public esQueryItem Sequencia
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.Sequencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgencia
		{
			get
			{
				return new esQueryItem(this, PadraoNotasMetadata.ColumnNames.IdAgencia, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PadraoNotasCollection")]
	public partial class PadraoNotasCollection : esPadraoNotasCollection, IEnumerable<PadraoNotas>
	{
		public PadraoNotasCollection()
		{

		}
		
		public static implicit operator List<PadraoNotas>(PadraoNotasCollection coll)
		{
			List<PadraoNotas> list = new List<PadraoNotas>();
			
			foreach (PadraoNotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PadraoNotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PadraoNotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PadraoNotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PadraoNotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PadraoNotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PadraoNotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PadraoNotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PadraoNotas AddNew()
		{
			PadraoNotas entity = base.AddNewEntity() as PadraoNotas;
			
			return entity;
		}

		public PadraoNotas FindByPrimaryKey(System.Int32 idPadraoNotas)
		{
			return base.FindByPrimaryKey(idPadraoNotas) as PadraoNotas;
		}


		#region IEnumerable<PadraoNotas> Members

		IEnumerator<PadraoNotas> IEnumerable<PadraoNotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PadraoNotas;
			}
		}

		#endregion
		
		private PadraoNotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PadraoNotas' table
	/// </summary>

	[Serializable]
	public partial class PadraoNotas : esPadraoNotas
	{
		public PadraoNotas()
		{

		}
	
		public PadraoNotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PadraoNotasMetadata.Meta();
			}
		}
		
		
		
		override protected esPadraoNotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PadraoNotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PadraoNotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PadraoNotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PadraoNotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PadraoNotasQuery query;
	}



	[Serializable]
	public partial class PadraoNotasQuery : esPadraoNotasQuery
	{
		public PadraoNotasQuery()
		{

		}		
		
		public PadraoNotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PadraoNotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PadraoNotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.IdPadraoNotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.IdPadraoNotas;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.ItemAvaliado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.ItemAvaliado;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.DataVigencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.DataVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.CodigoNota, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.CodigoNota;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.DescricaoNota, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.DescricaoNota;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.Sequencia, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.Sequencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PadraoNotasMetadata.ColumnNames.IdAgencia, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PadraoNotasMetadata.PropertyNames.IdAgencia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PadraoNotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPadraoNotas = "IdPadraoNotas";
			 public const string ItemAvaliado = "ItemAvaliado";
			 public const string DataVigencia = "DataVigencia";
			 public const string CodigoNota = "CodigoNota";
			 public const string DescricaoNota = "DescricaoNota";
			 public const string Sequencia = "Sequencia";
			 public const string IdAgencia = "IdAgencia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPadraoNotas = "IdPadraoNotas";
			 public const string ItemAvaliado = "ItemAvaliado";
			 public const string DataVigencia = "DataVigencia";
			 public const string CodigoNota = "CodigoNota";
			 public const string DescricaoNota = "DescricaoNota";
			 public const string Sequencia = "Sequencia";
			 public const string IdAgencia = "IdAgencia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PadraoNotasMetadata))
			{
				if(PadraoNotasMetadata.mapDelegates == null)
				{
					PadraoNotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PadraoNotasMetadata.meta == null)
				{
					PadraoNotasMetadata.meta = new PadraoNotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPadraoNotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ItemAvaliado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoNota", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoNota", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Sequencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgencia", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PadraoNotas";
				meta.Destination = "PadraoNotas";
				
				meta.spInsert = "proc_PadraoNotasInsert";				
				meta.spUpdate = "proc_PadraoNotasUpdate";		
				meta.spDelete = "proc_PadraoNotasDelete";
				meta.spLoadAll = "proc_PadraoNotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_PadraoNotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PadraoNotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
