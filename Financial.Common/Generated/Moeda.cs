/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/07/2015 14:29:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























using Financial.Bolsa;
using Financial.Investidor;
		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esMoedaCollection : esEntityCollection
	{
		public esMoedaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "MoedaCollection";
		}

		#region Query Logic
		protected void InitQuery(esMoedaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esMoedaQuery);
		}
		#endregion
		
		virtual public Moeda DetachEntity(Moeda entity)
		{
			return base.DetachEntity(entity) as Moeda;
		}
		
		virtual public Moeda AttachEntity(Moeda entity)
		{
			return base.AttachEntity(entity) as Moeda;
		}
		
		virtual public void Combine(MoedaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Moeda this[int index]
		{
			get
			{
				return base[index] as Moeda;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Moeda);
		}
	}



	[Serializable]
	abstract public class esMoeda : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esMoedaQuery GetDynamicQuery()
		{
			return null;
		}

		public esMoeda()
		{

		}

		public esMoeda(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idMoeda)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMoeda);
			else
				return LoadByPrimaryKeyStoredProcedure(idMoeda);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idMoeda)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esMoedaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdMoeda == idMoeda);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idMoeda)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMoeda);
			else
				return LoadByPrimaryKeyStoredProcedure(idMoeda);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idMoeda)
		{
			esMoedaQuery query = this.GetDynamicQuery();
			query.Where(query.IdMoeda == idMoeda);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idMoeda)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMoeda",idMoeda);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "CodigoISO": this.str.CodigoISO = (string)value; break;
                        case "CdIso4217": this.str.CdIso = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoeda = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Moeda.IdMoeda
		/// </summary>
		virtual public System.Int16? IdMoeda
		{
			get
			{
				return base.GetSystemInt16(MoedaMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt16(MoedaMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to Moeda.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(MoedaMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(MoedaMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Moeda.CodigoISO
		/// </summary>
		virtual public System.String CodigoISO
		{
			get
			{
				return base.GetSystemString(MoedaMetadata.ColumnNames.CodigoISO);
			}
			
			set
			{
				base.SetSystemString(MoedaMetadata.ColumnNames.CodigoISO, value);
			}
		}
		
        /// <summary>
        /// Maps to Moeda.CdIso
        /// </summary>
        virtual public System.String CdIso
        {
            get
            {
                return base.GetSystemString(MoedaMetadata.ColumnNames.CdIso);
            }

            set
            {
                base.SetSystemString(MoedaMetadata.ColumnNames.CdIso, value);
            }
        }

		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esMoeda entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMoeda
			{
				get
				{
					System.Int16? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt16(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String CodigoISO
			{
				get
				{
					System.String data = entity.CodigoISO;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoISO = null;
					else entity.CodigoISO = Convert.ToString(value);
				}
			}
			
            public System.String CdIso
            {
                get
                {
                    System.String data = entity.CdIso;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CdIso = null;
                    else entity.CdIso = Convert.ToString(value);
                }
            }

			private esMoeda entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esMoedaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esMoeda can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Moeda : esMoeda
	{

				
		#region AtivoBolsaCollectionByIdMoeda - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Moeda_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsaCollection AtivoBolsaCollectionByIdMoeda
		{
			get
			{
				if(this._AtivoBolsaCollectionByIdMoeda == null)
				{
					this._AtivoBolsaCollectionByIdMoeda = new AtivoBolsaCollection();
					this._AtivoBolsaCollectionByIdMoeda.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBolsaCollectionByIdMoeda", this._AtivoBolsaCollectionByIdMoeda);
				
					if(this.IdMoeda != null)
					{
						this._AtivoBolsaCollectionByIdMoeda.Query.Where(this._AtivoBolsaCollectionByIdMoeda.Query.IdMoeda == this.IdMoeda);
						this._AtivoBolsaCollectionByIdMoeda.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBolsaCollectionByIdMoeda.fks.Add(AtivoBolsaMetadata.ColumnNames.IdMoeda, this.IdMoeda);
					}
				}

				return this._AtivoBolsaCollectionByIdMoeda;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBolsaCollectionByIdMoeda != null) 
				{ 
					this.RemovePostSave("AtivoBolsaCollectionByIdMoeda"); 
					this._AtivoBolsaCollectionByIdMoeda = null;
					
				} 
			} 			
		}

		private AtivoBolsaCollection _AtivoBolsaCollectionByIdMoeda;
		#endregion

				
		#region ClienteCollectionByIdMoeda - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Moeda_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdMoeda
		{
			get
			{
				if(this._ClienteCollectionByIdMoeda == null)
				{
					this._ClienteCollectionByIdMoeda = new ClienteCollection();
					this._ClienteCollectionByIdMoeda.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdMoeda", this._ClienteCollectionByIdMoeda);
				
					if(this.IdMoeda != null)
					{
						this._ClienteCollectionByIdMoeda.Query.Where(this._ClienteCollectionByIdMoeda.Query.IdMoeda == this.IdMoeda);
						this._ClienteCollectionByIdMoeda.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdMoeda.fks.Add(ClienteMetadata.ColumnNames.IdMoeda, this.IdMoeda);
					}
				}

				return this._ClienteCollectionByIdMoeda;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdMoeda != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdMoeda"); 
					this._ClienteCollectionByIdMoeda = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdMoeda;
		#endregion

				
		#region ConversaoMoedaCollectionByIdMoedaDe - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Moeda_ConversaoMoeda_FK1
		/// </summary>

		[XmlIgnore]
		public ConversaoMoedaCollection ConversaoMoedaCollectionByIdMoedaDe
		{
			get
			{
				if(this._ConversaoMoedaCollectionByIdMoedaDe == null)
				{
					this._ConversaoMoedaCollectionByIdMoedaDe = new ConversaoMoedaCollection();
					this._ConversaoMoedaCollectionByIdMoedaDe.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ConversaoMoedaCollectionByIdMoedaDe", this._ConversaoMoedaCollectionByIdMoedaDe);
				
					if(this.IdMoeda != null)
					{
						this._ConversaoMoedaCollectionByIdMoedaDe.Query.Where(this._ConversaoMoedaCollectionByIdMoedaDe.Query.IdMoedaDe == this.IdMoeda);
						this._ConversaoMoedaCollectionByIdMoedaDe.Query.Load();

						// Auto-hookup Foreign Keys
						this._ConversaoMoedaCollectionByIdMoedaDe.fks.Add(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe, this.IdMoeda);
					}
				}

				return this._ConversaoMoedaCollectionByIdMoedaDe;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ConversaoMoedaCollectionByIdMoedaDe != null) 
				{ 
					this.RemovePostSave("ConversaoMoedaCollectionByIdMoedaDe"); 
					this._ConversaoMoedaCollectionByIdMoedaDe = null;
					
				} 
			} 			
		}

		private ConversaoMoedaCollection _ConversaoMoedaCollectionByIdMoedaDe;
		#endregion

				
		#region ConversaoMoedaCollectionByIdMoedaPara - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Moeda_ConversaoMoeda_FK2
		/// </summary>

		[XmlIgnore]
		public ConversaoMoedaCollection ConversaoMoedaCollectionByIdMoedaPara
		{
			get
			{
				if(this._ConversaoMoedaCollectionByIdMoedaPara == null)
				{
					this._ConversaoMoedaCollectionByIdMoedaPara = new ConversaoMoedaCollection();
					this._ConversaoMoedaCollectionByIdMoedaPara.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ConversaoMoedaCollectionByIdMoedaPara", this._ConversaoMoedaCollectionByIdMoedaPara);
				
					if(this.IdMoeda != null)
					{
						this._ConversaoMoedaCollectionByIdMoedaPara.Query.Where(this._ConversaoMoedaCollectionByIdMoedaPara.Query.IdMoedaPara == this.IdMoeda);
						this._ConversaoMoedaCollectionByIdMoedaPara.Query.Load();

						// Auto-hookup Foreign Keys
						this._ConversaoMoedaCollectionByIdMoedaPara.fks.Add(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara, this.IdMoeda);
					}
				}

				return this._ConversaoMoedaCollectionByIdMoedaPara;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ConversaoMoedaCollectionByIdMoedaPara != null) 
				{ 
					this.RemovePostSave("ConversaoMoedaCollectionByIdMoedaPara"); 
					this._ConversaoMoedaCollectionByIdMoedaPara = null;
					
				} 
			} 			
		}

		private ConversaoMoedaCollection _ConversaoMoedaCollectionByIdMoedaPara;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AtivoBolsaCollectionByIdMoeda", typeof(AtivoBolsaCollection), new AtivoBolsa()));
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdMoeda", typeof(ClienteCollection), new Cliente()));
			props.Add(new esPropertyDescriptor(this, "ConversaoMoedaCollectionByIdMoedaDe", typeof(ConversaoMoedaCollection), new ConversaoMoeda()));
			props.Add(new esPropertyDescriptor(this, "ConversaoMoedaCollectionByIdMoedaPara", typeof(ConversaoMoedaCollection), new ConversaoMoeda()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AtivoBolsaCollectionByIdMoeda != null)
			{
				foreach(AtivoBolsa obj in this._AtivoBolsaCollectionByIdMoeda)
				{
					if(obj.es.IsAdded)
					{
						obj.IdMoeda = this.IdMoeda;
					}
				}
			}
			if(this._ClienteCollectionByIdMoeda != null)
			{
				foreach(Cliente obj in this._ClienteCollectionByIdMoeda)
				{
					if(obj.es.IsAdded)
					{
						obj.IdMoeda = this.IdMoeda;
					}
				}
			}
			if(this._ConversaoMoedaCollectionByIdMoedaDe != null)
			{
				foreach(ConversaoMoeda obj in this._ConversaoMoedaCollectionByIdMoedaDe)
				{
					if(obj.es.IsAdded)
					{
						obj.IdMoedaDe = this.IdMoeda;
					}
				}
			}
			if(this._ConversaoMoedaCollectionByIdMoedaPara != null)
			{
				foreach(ConversaoMoeda obj in this._ConversaoMoedaCollectionByIdMoedaPara)
				{
					if(obj.es.IsAdded)
					{
						obj.IdMoedaPara = this.IdMoeda;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esMoedaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MoedaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, MoedaMetadata.ColumnNames.IdMoeda, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, MoedaMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoISO
		{
			get
			{
				return new esQueryItem(this, MoedaMetadata.ColumnNames.CodigoISO, esSystemType.String);
			}
		} 
		
        public esQueryItem CdIso
        {
            get
            {
                return new esQueryItem(this, MoedaMetadata.ColumnNames.CdIso, esSystemType.String);
            }
        } 
	}



	[Serializable]
	[XmlType("MoedaCollection")]
	public partial class MoedaCollection : esMoedaCollection, IEnumerable<Moeda>
	{
		public MoedaCollection()
		{

		}
		
		public static implicit operator List<Moeda>(MoedaCollection coll)
		{
			List<Moeda> list = new List<Moeda>();
			
			foreach (Moeda emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  MoedaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MoedaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Moeda(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Moeda();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public MoedaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MoedaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(MoedaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Moeda AddNew()
		{
			Moeda entity = base.AddNewEntity() as Moeda;
			
			return entity;
		}

		public Moeda FindByPrimaryKey(System.Int16 idMoeda)
		{
			return base.FindByPrimaryKey(idMoeda) as Moeda;
		}


		#region IEnumerable<Moeda> Members

		IEnumerator<Moeda> IEnumerable<Moeda>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Moeda;
			}
		}

		#endregion
		
		private MoedaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Moeda' table
	/// </summary>

	[Serializable]
	public partial class Moeda : esMoeda
	{
		public Moeda()
		{

		}
	
		public Moeda(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MoedaMetadata.Meta();
			}
		}
		
		
		
		override protected esMoedaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MoedaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public MoedaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MoedaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(MoedaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private MoedaQuery query;
	}



	[Serializable]
	public partial class MoedaQuery : esMoedaQuery
	{
		public MoedaQuery()
		{

		}		
		
		public MoedaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class MoedaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected MoedaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(MoedaMetadata.ColumnNames.IdMoeda, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = MoedaMetadata.PropertyNames.IdMoeda;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MoedaMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = MoedaMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c);

            c = new esColumnMetadata(MoedaMetadata.ColumnNames.CdIso, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = MoedaMetadata.PropertyNames.CdIso;
            c.CharacterMaxLength = 3;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c); 
				
			c = new esColumnMetadata(MoedaMetadata.ColumnNames.CodigoISO, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = MoedaMetadata.PropertyNames.CodigoISO;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public MoedaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMoeda = "IdMoeda";
			 public const string Nome = "Nome";
			 public const string CodigoISO = "CodigoISO";
             public const string CdIso = "CdIso";

		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMoeda = "IdMoeda";
			 public const string Nome = "Nome";
			 public const string CodigoISO = "CodigoISO";
             public const string CdIso = "CdIso";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MoedaMetadata))
			{
				if(MoedaMetadata.mapDelegates == null)
				{
					MoedaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (MoedaMetadata.meta == null)
				{
					MoedaMetadata.meta = new MoedaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMoeda", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoISO", new esTypeMap("char", "System.String"));			
                meta.AddTypeMap("CdIso", new esTypeMap("char", "System.String"));
			
				meta.Source = "Moeda";
				meta.Destination = "Moeda";
				
				meta.spInsert = "proc_MoedaInsert";				
				meta.spUpdate = "proc_MoedaUpdate";		
				meta.spDelete = "proc_MoedaDelete";
				meta.spLoadAll = "proc_MoedaLoadAll";
				meta.spLoadByPrimaryKey = "proc_MoedaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private MoedaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
