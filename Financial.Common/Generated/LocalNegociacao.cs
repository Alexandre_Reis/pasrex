/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/02/2015 14:12:33
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common
{

	[Serializable]
	abstract public class esLocalNegociacaoCollection : esEntityCollection
	{
		public esLocalNegociacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LocalNegociacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esLocalNegociacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLocalNegociacaoQuery);
		}
		#endregion
		
		virtual public LocalNegociacao DetachEntity(LocalNegociacao entity)
		{
			return base.DetachEntity(entity) as LocalNegociacao;
		}
		
		virtual public LocalNegociacao AttachEntity(LocalNegociacao entity)
		{
			return base.AttachEntity(entity) as LocalNegociacao;
		}
		
		virtual public void Combine(LocalNegociacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LocalNegociacao this[int index]
		{
			get
			{
				return base[index] as LocalNegociacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LocalNegociacao);
		}
	}



	[Serializable]
	abstract public class esLocalNegociacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLocalNegociacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esLocalNegociacao()
		{

		}

		public esLocalNegociacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLocalNegociacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocalNegociacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocalNegociacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLocalNegociacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocalNegociacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocalNegociacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLocalNegociacao)
		{
			esLocalNegociacaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdLocalNegociacao == idLocalNegociacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLocalNegociacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLocalNegociacao",idLocalNegociacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Pais": this.str.Pais = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdLocalFeriado": this.str.IdLocalFeriado = (string)value; break;							
						case "ParaisoFiscal": this.str.ParaisoFiscal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoeda = (System.Int16?)value;
							break;
						
						case "IdLocalFeriado":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocalFeriado = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LocalNegociacao.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(LocalNegociacaoMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(LocalNegociacaoMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(LocalNegociacaoMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(LocalNegociacaoMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(LocalNegociacaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(LocalNegociacaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.Pais
		/// </summary>
		virtual public System.String Pais
		{
			get
			{
				return base.GetSystemString(LocalNegociacaoMetadata.ColumnNames.Pais);
			}
			
			set
			{
				base.SetSystemString(LocalNegociacaoMetadata.ColumnNames.Pais, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.IdMoeda
		/// </summary>
		virtual public System.Int16? IdMoeda
		{
			get
			{
				return base.GetSystemInt16(LocalNegociacaoMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt16(LocalNegociacaoMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.IdLocalFeriado
		/// </summary>
		virtual public System.Int16? IdLocalFeriado
		{
			get
			{
				return base.GetSystemInt16(LocalNegociacaoMetadata.ColumnNames.IdLocalFeriado);
			}
			
			set
			{
				if(base.SetSystemInt16(LocalNegociacaoMetadata.ColumnNames.IdLocalFeriado, value))
				{
					this._UpToLocalFeriadoByIdLocalFeriado = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LocalNegociacao.ParaisoFiscal
		/// </summary>
		virtual public System.String ParaisoFiscal
		{
			get
			{
				return base.GetSystemString(LocalNegociacaoMetadata.ColumnNames.ParaisoFiscal);
			}
			
			set
			{
				base.SetSystemString(LocalNegociacaoMetadata.ColumnNames.ParaisoFiscal, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected LocalFeriado _UpToLocalFeriadoByIdLocalFeriado;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLocalNegociacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Pais
			{
				get
				{
					System.String data = entity.Pais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pais = null;
					else entity.Pais = Convert.ToString(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int16? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt16(value);
				}
			}
				
			public System.String IdLocalFeriado
			{
				get
				{
					System.Int16? data = entity.IdLocalFeriado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalFeriado = null;
					else entity.IdLocalFeriado = Convert.ToInt16(value);
				}
			}
				
			public System.String ParaisoFiscal
			{
				get
				{
					System.String data = entity.ParaisoFiscal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParaisoFiscal = null;
					else entity.ParaisoFiscal = Convert.ToString(value);
				}
			}
			

			private esLocalNegociacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLocalNegociacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLocalNegociacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LocalNegociacao : esLocalNegociacao
	{

				
		#region UpToLocalFeriadoByIdLocalFeriado - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - LocalNegociacao_LocalFer_FK
		/// </summary>

		[XmlIgnore]
		public LocalFeriado UpToLocalFeriadoByIdLocalFeriado
		{
			get
			{
				if(this._UpToLocalFeriadoByIdLocalFeriado == null
					&& IdLocalFeriado != null					)
				{
					this._UpToLocalFeriadoByIdLocalFeriado = new LocalFeriado();
					this._UpToLocalFeriadoByIdLocalFeriado.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToLocalFeriadoByIdLocalFeriado", this._UpToLocalFeriadoByIdLocalFeriado);
					this._UpToLocalFeriadoByIdLocalFeriado.Query.Where(this._UpToLocalFeriadoByIdLocalFeriado.Query.IdLocal == this.IdLocalFeriado);
					this._UpToLocalFeriadoByIdLocalFeriado.Query.Load();
				}

				return this._UpToLocalFeriadoByIdLocalFeriado;
			}
			
			set
			{
				this.RemovePreSave("UpToLocalFeriadoByIdLocalFeriado");
				

				if(value == null)
				{
					this.IdLocalFeriado = null;
					this._UpToLocalFeriadoByIdLocalFeriado = null;
				}
				else
				{
					this.IdLocalFeriado = value.IdLocal;
					this._UpToLocalFeriadoByIdLocalFeriado = value;
					this.SetPreSave("UpToLocalFeriadoByIdLocalFeriado", this._UpToLocalFeriadoByIdLocalFeriado);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToLocalFeriadoByIdLocalFeriado != null)
			{
				this.IdLocalFeriado = this._UpToLocalFeriadoByIdLocalFeriado.IdLocal;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLocalNegociacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LocalNegociacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Pais
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.Pais, esSystemType.String);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.IdMoeda, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdLocalFeriado
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.IdLocalFeriado, esSystemType.Int16);
			}
		} 
		
		public esQueryItem ParaisoFiscal
		{
			get
			{
				return new esQueryItem(this, LocalNegociacaoMetadata.ColumnNames.ParaisoFiscal, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LocalNegociacaoCollection")]
	public partial class LocalNegociacaoCollection : esLocalNegociacaoCollection, IEnumerable<LocalNegociacao>
	{
		public LocalNegociacaoCollection()
		{

		}
		
		public static implicit operator List<LocalNegociacao>(LocalNegociacaoCollection coll)
		{
			List<LocalNegociacao> list = new List<LocalNegociacao>();
			
			foreach (LocalNegociacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LocalNegociacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalNegociacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LocalNegociacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LocalNegociacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LocalNegociacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalNegociacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LocalNegociacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LocalNegociacao AddNew()
		{
			LocalNegociacao entity = base.AddNewEntity() as LocalNegociacao;
			
			return entity;
		}

		public LocalNegociacao FindByPrimaryKey(System.Int32 idLocalNegociacao)
		{
			return base.FindByPrimaryKey(idLocalNegociacao) as LocalNegociacao;
		}


		#region IEnumerable<LocalNegociacao> Members

		IEnumerator<LocalNegociacao> IEnumerable<LocalNegociacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LocalNegociacao;
			}
		}

		#endregion
		
		private LocalNegociacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LocalNegociacao' table
	/// </summary>

	[Serializable]
	public partial class LocalNegociacao : esLocalNegociacao
	{
		public LocalNegociacao()
		{

		}
	
		public LocalNegociacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LocalNegociacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esLocalNegociacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalNegociacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LocalNegociacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalNegociacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LocalNegociacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LocalNegociacaoQuery query;
	}



	[Serializable]
	public partial class LocalNegociacaoQuery : esLocalNegociacaoQuery
	{
		public LocalNegociacaoQuery()
		{

		}		
		
		public LocalNegociacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LocalNegociacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LocalNegociacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.IdLocalNegociacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.IdLocalNegociacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.Codigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.Pais, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.Pais;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.IdMoeda, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.IdLocalFeriado, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.IdLocalFeriado;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalNegociacaoMetadata.ColumnNames.ParaisoFiscal, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalNegociacaoMetadata.PropertyNames.ParaisoFiscal;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LocalNegociacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string Pais = "Pais";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalFeriado = "IdLocalFeriado";
			 public const string ParaisoFiscal = "ParaisoFiscal";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string Pais = "Pais";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalFeriado = "IdLocalFeriado";
			 public const string ParaisoFiscal = "ParaisoFiscal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LocalNegociacaoMetadata))
			{
				if(LocalNegociacaoMetadata.mapDelegates == null)
				{
					LocalNegociacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LocalNegociacaoMetadata.meta == null)
				{
					LocalNegociacaoMetadata.meta = new LocalNegociacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Pais", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdLocalFeriado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ParaisoFiscal", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "LocalNegociacao";
				meta.Destination = "LocalNegociacao";
				
				meta.spInsert = "proc_LocalNegociacaoInsert";				
				meta.spUpdate = "proc_LocalNegociacaoUpdate";		
				meta.spDelete = "proc_LocalNegociacaoDelete";
				meta.spLoadAll = "proc_LocalNegociacaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_LocalNegociacaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LocalNegociacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
