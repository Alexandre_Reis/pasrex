/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/05/2015 14:29:53
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Enquadra;
using Financial.Bolsa;
using Financial.Swap;
using Financial.RendaFixa;
using Financial.Investidor;



namespace Financial.Common
{

	[Serializable]
	abstract public class esIndiceCollection : esEntityCollection
	{
		public esIndiceCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IndiceCollection";
		}

		#region Query Logic
		protected void InitQuery(esIndiceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIndiceQuery);
		}
		#endregion
		
		virtual public Indice DetachEntity(Indice entity)
		{
			return base.DetachEntity(entity) as Indice;
		}
		
		virtual public Indice AttachEntity(Indice entity)
		{
			return base.AttachEntity(entity) as Indice;
		}
		
		virtual public void Combine(IndiceCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Indice this[int index]
		{
			get
			{
				return base[index] as Indice;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Indice);
		}
	}



	[Serializable]
	abstract public class esIndice : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIndiceQuery GetDynamicQuery()
		{
			return null;
		}

		public esIndice()
		{

		}

		public esIndice(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idIndice)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIndice);
			else
				return LoadByPrimaryKeyStoredProcedure(idIndice);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idIndice)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esIndiceQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdIndice == idIndice);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idIndice)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIndice);
			else
				return LoadByPrimaryKeyStoredProcedure(idIndice);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idIndice)
		{
			esIndiceQuery query = this.GetDynamicQuery();
			query.Where(query.IdIndice == idIndice);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idIndice)
		{
			esParameters parms = new esParameters();
			parms.Add("IdIndice",idIndice);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "TipoDivulgacao": this.str.TipoDivulgacao = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "IdIndiceBase": this.str.IdIndiceBase = (string)value; break;							
						case "IdFeeder": this.str.IdFeeder = (string)value; break;							
						case "IdCurva": this.str.IdCurva = (string)value; break;							
						case "TabelaBDS": this.str.TabelaBDS = (string)value; break;							
						case "IntervaloBDS": this.str.IntervaloBDS = (string)value; break;							
						case "AtributoBDS": this.str.AtributoBDS = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
						
						case "TipoDivulgacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoDivulgacao = (System.Byte?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "IdIndiceBase":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdIndiceBase = (System.Int32?)value;
							break;
						
						case "IdFeeder":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFeeder = (System.Int32?)value;
							break;
						
						case "IdCurva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCurva = (System.Int32?)value;
							break;
						
						case "IntervaloBDS":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IntervaloBDS = (System.Byte?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Indice.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(IndiceMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(IndiceMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(IndiceMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(IndiceMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(IndiceMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(IndiceMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.TipoDivulgacao
		/// </summary>
		virtual public System.Byte? TipoDivulgacao
		{
			get
			{
				return base.GetSystemByte(IndiceMetadata.ColumnNames.TipoDivulgacao);
			}
			
			set
			{
				base.SetSystemByte(IndiceMetadata.ColumnNames.TipoDivulgacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(IndiceMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(IndiceMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(IndiceMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(IndiceMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.IdIndiceBase
		/// </summary>
		virtual public System.Int32? IdIndiceBase
		{
			get
			{
				return base.GetSystemInt32(IndiceMetadata.ColumnNames.IdIndiceBase);
			}
			
			set
			{
				base.SetSystemInt32(IndiceMetadata.ColumnNames.IdIndiceBase, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.IdFeeder
		/// </summary>
		virtual public System.Int32? IdFeeder
		{
			get
			{
				return base.GetSystemInt32(IndiceMetadata.ColumnNames.IdFeeder);
			}
			
			set
			{
				if(base.SetSystemInt32(IndiceMetadata.ColumnNames.IdFeeder, value))
				{
					this._UpToFeederByIdFeeder = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Indice.IdCurva
		/// </summary>
		virtual public System.Int32? IdCurva
		{
			get
			{
				return base.GetSystemInt32(IndiceMetadata.ColumnNames.IdCurva);
			}
			
			set
			{
				if(base.SetSystemInt32(IndiceMetadata.ColumnNames.IdCurva, value))
				{
					this._UpToCurvaRendaFixaByIdCurva = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Indice.TabelaBDS
		/// </summary>
		virtual public System.String TabelaBDS
		{
			get
			{
				return base.GetSystemString(IndiceMetadata.ColumnNames.TabelaBDS);
			}
			
			set
			{
				base.SetSystemString(IndiceMetadata.ColumnNames.TabelaBDS, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.IntervaloBDS
		/// </summary>
		virtual public System.Byte? IntervaloBDS
		{
			get
			{
				return base.GetSystemByte(IndiceMetadata.ColumnNames.IntervaloBDS);
			}
			
			set
			{
				base.SetSystemByte(IndiceMetadata.ColumnNames.IntervaloBDS, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.AtributoBDS
		/// </summary>
		virtual public System.String AtributoBDS
		{
			get
			{
				return base.GetSystemString(IndiceMetadata.ColumnNames.AtributoBDS);
			}
			
			set
			{
				base.SetSystemString(IndiceMetadata.ColumnNames.AtributoBDS, value);
			}
		}
		
		/// <summary>
		/// Maps to Indice.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(IndiceMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(IndiceMetadata.ColumnNames.CodigoBDS, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CurvaRendaFixa _UpToCurvaRendaFixaByIdCurva;
		[CLSCompliant(false)]
		internal protected Feeder _UpToFeederByIdFeeder;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIndice entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
				
			public System.String TipoDivulgacao
			{
				get
				{
					System.Byte? data = entity.TipoDivulgacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoDivulgacao = null;
					else entity.TipoDivulgacao = Convert.ToByte(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdIndiceBase
			{
				get
				{
					System.Int32? data = entity.IdIndiceBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceBase = null;
					else entity.IdIndiceBase = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFeeder
			{
				get
				{
					System.Int32? data = entity.IdFeeder;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFeeder = null;
					else entity.IdFeeder = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCurva
			{
				get
				{
					System.Int32? data = entity.IdCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCurva = null;
					else entity.IdCurva = Convert.ToInt32(value);
				}
			}
				
			public System.String TabelaBDS
			{
				get
				{
					System.String data = entity.TabelaBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TabelaBDS = null;
					else entity.TabelaBDS = Convert.ToString(value);
				}
			}
				
			public System.String IntervaloBDS
			{
				get
				{
					System.Byte? data = entity.IntervaloBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IntervaloBDS = null;
					else entity.IntervaloBDS = Convert.ToByte(value);
				}
			}
				
			public System.String AtributoBDS
			{
				get
				{
					System.String data = entity.AtributoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AtributoBDS = null;
					else entity.AtributoBDS = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}
			

			private esIndice entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIndiceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIndice can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Indice : esIndice
	{

				
		#region CarteiraCollectionByIdIndiceBenchmark - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdIndiceBenchmark
		{
			get
			{
				if(this._CarteiraCollectionByIdIndiceBenchmark == null)
				{
					this._CarteiraCollectionByIdIndiceBenchmark = new CarteiraCollection();
					this._CarteiraCollectionByIdIndiceBenchmark.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdIndiceBenchmark", this._CarteiraCollectionByIdIndiceBenchmark);
				
					if(this.IdIndice != null)
					{
						this._CarteiraCollectionByIdIndiceBenchmark.Query.Where(this._CarteiraCollectionByIdIndiceBenchmark.Query.IdIndiceBenchmark == this.IdIndice);
						this._CarteiraCollectionByIdIndiceBenchmark.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdIndiceBenchmark.fks.Add(CarteiraMetadata.ColumnNames.IdIndiceBenchmark, this.IdIndice);
					}
				}

				return this._CarteiraCollectionByIdIndiceBenchmark;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdIndiceBenchmark != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdIndiceBenchmark"); 
					this._CarteiraCollectionByIdIndiceBenchmark = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdIndiceBenchmark;
		#endregion

				
		#region ClassesOffShoreCollectionByIndexadorPerf - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__ClassesOf__Index__477C86E9
		/// </summary>

		[XmlIgnore]
		public ClassesOffShoreCollection ClassesOffShoreCollectionByIndexadorPerf
		{
			get
			{
				if(this._ClassesOffShoreCollectionByIndexadorPerf == null)
				{
					this._ClassesOffShoreCollectionByIndexadorPerf = new ClassesOffShoreCollection();
					this._ClassesOffShoreCollectionByIndexadorPerf.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClassesOffShoreCollectionByIndexadorPerf", this._ClassesOffShoreCollectionByIndexadorPerf);
				
					if(this.IdIndice != null)
					{
						this._ClassesOffShoreCollectionByIndexadorPerf.Query.Where(this._ClassesOffShoreCollectionByIndexadorPerf.Query.IndexadorPerf == this.IdIndice);
						this._ClassesOffShoreCollectionByIndexadorPerf.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClassesOffShoreCollectionByIndexadorPerf.fks.Add(ClassesOffShoreMetadata.ColumnNames.IndexadorPerf, this.IdIndice);
					}
				}

				return this._ClassesOffShoreCollectionByIndexadorPerf;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClassesOffShoreCollectionByIndexadorPerf != null) 
				{ 
					this.RemovePostSave("ClassesOffShoreCollectionByIndexadorPerf"); 
					this._ClassesOffShoreCollectionByIndexadorPerf = null;
					
				} 
			} 			
		}

		private ClassesOffShoreCollection _ClassesOffShoreCollectionByIndexadorPerf;
		#endregion

				
		#region ClienteCollectionByIdIndiceAbertura - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_Indice_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdIndiceAbertura
		{
			get
			{
				if(this._ClienteCollectionByIdIndiceAbertura == null)
				{
					this._ClienteCollectionByIdIndiceAbertura = new ClienteCollection();
					this._ClienteCollectionByIdIndiceAbertura.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdIndiceAbertura", this._ClienteCollectionByIdIndiceAbertura);
				
					if(this.IdIndice != null)
					{
						this._ClienteCollectionByIdIndiceAbertura.Query.Where(this._ClienteCollectionByIdIndiceAbertura.Query.IdIndiceAbertura == this.IdIndice);
						this._ClienteCollectionByIdIndiceAbertura.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdIndiceAbertura.fks.Add(ClienteMetadata.ColumnNames.IdIndiceAbertura, this.IdIndice);
					}
				}

				return this._ClienteCollectionByIdIndiceAbertura;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdIndiceAbertura != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdIndiceAbertura"); 
					this._ClienteCollectionByIdIndiceAbertura = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdIndiceAbertura;
		#endregion

				
		#region CotacaoIndiceCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_CotacaoIndice_FK1
		/// </summary>

		[XmlIgnore]
		public CotacaoIndiceCollection CotacaoIndiceCollectionByIdIndice
		{
			get
			{
				if(this._CotacaoIndiceCollectionByIdIndice == null)
				{
					this._CotacaoIndiceCollectionByIdIndice = new CotacaoIndiceCollection();
					this._CotacaoIndiceCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotacaoIndiceCollectionByIdIndice", this._CotacaoIndiceCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._CotacaoIndiceCollectionByIdIndice.Query.Where(this._CotacaoIndiceCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._CotacaoIndiceCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotacaoIndiceCollectionByIdIndice.fks.Add(CotacaoIndiceMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._CotacaoIndiceCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotacaoIndiceCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("CotacaoIndiceCollectionByIdIndice"); 
					this._CotacaoIndiceCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private CotacaoIndiceCollection _CotacaoIndiceCollectionByIdIndice;
		#endregion

				
		#region CurvaRendaFixaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Curva_Indice_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixaCollection CurvaRendaFixaCollectionByIdIndice
		{
			get
			{
				if(this._CurvaRendaFixaCollectionByIdIndice == null)
				{
					this._CurvaRendaFixaCollectionByIdIndice = new CurvaRendaFixaCollection();
					this._CurvaRendaFixaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CurvaRendaFixaCollectionByIdIndice", this._CurvaRendaFixaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._CurvaRendaFixaCollectionByIdIndice.Query.Where(this._CurvaRendaFixaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._CurvaRendaFixaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._CurvaRendaFixaCollectionByIdIndice.fks.Add(CurvaRendaFixaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._CurvaRendaFixaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CurvaRendaFixaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("CurvaRendaFixaCollectionByIdIndice"); 
					this._CurvaRendaFixaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private CurvaRendaFixaCollection _CurvaRendaFixaCollectionByIdIndice;
		#endregion

				
		#region EnquadraItemFundoCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFundoCollection EnquadraItemFundoCollectionByIdIndice
		{
			get
			{
				if(this._EnquadraItemFundoCollectionByIdIndice == null)
				{
					this._EnquadraItemFundoCollectionByIdIndice = new EnquadraItemFundoCollection();
					this._EnquadraItemFundoCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemFundoCollectionByIdIndice", this._EnquadraItemFundoCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._EnquadraItemFundoCollectionByIdIndice.Query.Where(this._EnquadraItemFundoCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._EnquadraItemFundoCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemFundoCollectionByIdIndice.fks.Add(EnquadraItemFundoMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._EnquadraItemFundoCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFundoCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("EnquadraItemFundoCollectionByIdIndice"); 
					this._EnquadraItemFundoCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private EnquadraItemFundoCollection _EnquadraItemFundoCollectionByIdIndice;
		#endregion

				
		#region EnquadraItemRendaFixaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemRendaFixaCollection EnquadraItemRendaFixaCollectionByIdIndice
		{
			get
			{
				if(this._EnquadraItemRendaFixaCollectionByIdIndice == null)
				{
					this._EnquadraItemRendaFixaCollectionByIdIndice = new EnquadraItemRendaFixaCollection();
					this._EnquadraItemRendaFixaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemRendaFixaCollectionByIdIndice", this._EnquadraItemRendaFixaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._EnquadraItemRendaFixaCollectionByIdIndice.Query.Where(this._EnquadraItemRendaFixaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._EnquadraItemRendaFixaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemRendaFixaCollectionByIdIndice.fks.Add(EnquadraItemRendaFixaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._EnquadraItemRendaFixaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemRendaFixaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("EnquadraItemRendaFixaCollectionByIdIndice"); 
					this._EnquadraItemRendaFixaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private EnquadraItemRendaFixaCollection _EnquadraItemRendaFixaCollectionByIdIndice;
		#endregion

				
		#region EstrategiaCollectionByIdIndiceBenchmark - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Estrategia_Indice
		/// </summary>

		[XmlIgnore]
		public EstrategiaCollection EstrategiaCollectionByIdIndiceBenchmark
		{
			get
			{
				if(this._EstrategiaCollectionByIdIndiceBenchmark == null)
				{
					this._EstrategiaCollectionByIdIndiceBenchmark = new EstrategiaCollection();
					this._EstrategiaCollectionByIdIndiceBenchmark.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EstrategiaCollectionByIdIndiceBenchmark", this._EstrategiaCollectionByIdIndiceBenchmark);
				
					if(this.IdIndice != null)
					{
						this._EstrategiaCollectionByIdIndiceBenchmark.Query.Where(this._EstrategiaCollectionByIdIndiceBenchmark.Query.IdIndiceBenchmark == this.IdIndice);
						this._EstrategiaCollectionByIdIndiceBenchmark.Query.Load();

						// Auto-hookup Foreign Keys
						this._EstrategiaCollectionByIdIndiceBenchmark.fks.Add(EstrategiaMetadata.ColumnNames.IdIndiceBenchmark, this.IdIndice);
					}
				}

				return this._EstrategiaCollectionByIdIndiceBenchmark;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EstrategiaCollectionByIdIndiceBenchmark != null) 
				{ 
					this.RemovePostSave("EstrategiaCollectionByIdIndiceBenchmark"); 
					this._EstrategiaCollectionByIdIndiceBenchmark = null;
					
				} 
			} 			
		}

		private EstrategiaCollection _EstrategiaCollectionByIdIndiceBenchmark;
		#endregion

		#region UpToCarteiraCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Indice_ListaBenchmark_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection UpToCarteiraCollection
		{
			get
			{
				if(this._UpToCarteiraCollection == null)
				{
					this._UpToCarteiraCollection = new CarteiraCollection();
					this._UpToCarteiraCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToCarteiraCollection", this._UpToCarteiraCollection);
					this._UpToCarteiraCollection.ManyToManyIndiceCollection(this.IdIndice);
				}

				return this._UpToCarteiraCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToCarteiraCollection != null) 
				{ 
					this.RemovePostSave("UpToCarteiraCollection"); 
					this._UpToCarteiraCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Indice_ListaBenchmark_FK1
		/// </summary>
		public void AssociateCarteiraCollection(Carteira entity)
		{
			if (this._ListaBenchmarkCollection == null)
			{
				this._ListaBenchmarkCollection = new ListaBenchmarkCollection();
				this._ListaBenchmarkCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ListaBenchmarkCollection", this._ListaBenchmarkCollection);
			}

			ListaBenchmark obj = this._ListaBenchmarkCollection.AddNew();
			obj.IdIndice = this.IdIndice;
			obj.IdCarteira = entity.IdCarteira;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Indice_ListaBenchmark_FK1
		/// </summary>
		public void DissociateCarteiraCollection(Carteira entity)
		{
			if (this._ListaBenchmarkCollection == null)
			{
				this._ListaBenchmarkCollection = new ListaBenchmarkCollection();
				this._ListaBenchmarkCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ListaBenchmarkCollection", this._ListaBenchmarkCollection);
			}

			ListaBenchmark obj = this._ListaBenchmarkCollection.AddNew();
			obj.IdIndice = this.IdIndice;
			obj.IdCarteira = entity.IdCarteira;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private CarteiraCollection _UpToCarteiraCollection;
		private ListaBenchmarkCollection _ListaBenchmarkCollection;
		#endregion

				
		#region ListaBenchmarkCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_ListaBenchmark_FK1
		/// </summary>

		[XmlIgnore]
		public ListaBenchmarkCollection ListaBenchmarkCollectionByIdIndice
		{
			get
			{
				if(this._ListaBenchmarkCollectionByIdIndice == null)
				{
					this._ListaBenchmarkCollectionByIdIndice = new ListaBenchmarkCollection();
					this._ListaBenchmarkCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ListaBenchmarkCollectionByIdIndice", this._ListaBenchmarkCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._ListaBenchmarkCollectionByIdIndice.Query.Where(this._ListaBenchmarkCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._ListaBenchmarkCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._ListaBenchmarkCollectionByIdIndice.fks.Add(ListaBenchmarkMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._ListaBenchmarkCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ListaBenchmarkCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("ListaBenchmarkCollectionByIdIndice"); 
					this._ListaBenchmarkCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private ListaBenchmarkCollection _ListaBenchmarkCollectionByIdIndice;
		#endregion

				
		#region OrdemTermoBolsaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_OrdemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemTermoBolsaCollection OrdemTermoBolsaCollectionByIdIndice
		{
			get
			{
				if(this._OrdemTermoBolsaCollectionByIdIndice == null)
				{
					this._OrdemTermoBolsaCollectionByIdIndice = new OrdemTermoBolsaCollection();
					this._OrdemTermoBolsaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemTermoBolsaCollectionByIdIndice", this._OrdemTermoBolsaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._OrdemTermoBolsaCollectionByIdIndice.Query.Where(this._OrdemTermoBolsaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._OrdemTermoBolsaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemTermoBolsaCollectionByIdIndice.fks.Add(OrdemTermoBolsaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._OrdemTermoBolsaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemTermoBolsaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("OrdemTermoBolsaCollectionByIdIndice"); 
					this._OrdemTermoBolsaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private OrdemTermoBolsaCollection _OrdemTermoBolsaCollectionByIdIndice;
		#endregion

				
		#region ParidadeCambialCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_ParidadeCambial_Indice
		/// </summary>

		[XmlIgnore]
		public ParidadeCambialCollection ParidadeCambialCollectionByIdIndice
		{
			get
			{
				if(this._ParidadeCambialCollectionByIdIndice == null)
				{
					this._ParidadeCambialCollectionByIdIndice = new ParidadeCambialCollection();
					this._ParidadeCambialCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ParidadeCambialCollectionByIdIndice", this._ParidadeCambialCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._ParidadeCambialCollectionByIdIndice.Query.Where(this._ParidadeCambialCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._ParidadeCambialCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._ParidadeCambialCollectionByIdIndice.fks.Add(ParidadeCambialMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._ParidadeCambialCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ParidadeCambialCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("ParidadeCambialCollectionByIdIndice"); 
					this._ParidadeCambialCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private ParidadeCambialCollection _ParidadeCambialCollectionByIdIndice;
		#endregion

				
		#region PosicaoSwapCollectionByIdIndiceContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoSwap_FK2
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdIndiceContraParte
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdIndiceContraParte == null)
				{
					this._PosicaoSwapCollectionByIdIndiceContraParte = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdIndiceContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdIndiceContraParte", this._PosicaoSwapCollectionByIdIndiceContraParte);
				
					if(this.IdIndice != null)
					{
						this._PosicaoSwapCollectionByIdIndiceContraParte.Query.Where(this._PosicaoSwapCollectionByIdIndiceContraParte.Query.IdIndiceContraParte == this.IdIndice);
						this._PosicaoSwapCollectionByIdIndiceContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdIndiceContraParte.fks.Add(PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte, this.IdIndice);
					}
				}

				return this._PosicaoSwapCollectionByIdIndiceContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdIndiceContraParte != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdIndiceContraParte"); 
					this._PosicaoSwapCollectionByIdIndiceContraParte = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdIndiceContraParte;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdIndice
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdIndice == null)
				{
					this._PosicaoSwapAberturaCollectionByIdIndice = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdIndice", this._PosicaoSwapAberturaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._PosicaoSwapAberturaCollectionByIdIndice.Query.Where(this._PosicaoSwapAberturaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._PosicaoSwapAberturaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdIndice.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdIndice"); 
					this._PosicaoSwapAberturaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdIndice;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdIndiceContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoSwapAbertura_FK2
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdIndiceContraParte
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdIndiceContraParte == null)
				{
					this._PosicaoSwapAberturaCollectionByIdIndiceContraParte = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdIndiceContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdIndiceContraParte", this._PosicaoSwapAberturaCollectionByIdIndiceContraParte);
				
					if(this.IdIndice != null)
					{
						this._PosicaoSwapAberturaCollectionByIdIndiceContraParte.Query.Where(this._PosicaoSwapAberturaCollectionByIdIndiceContraParte.Query.IdIndiceContraParte == this.IdIndice);
						this._PosicaoSwapAberturaCollectionByIdIndiceContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdIndiceContraParte.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdIndiceContraParte, this.IdIndice);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdIndiceContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdIndiceContraParte != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdIndiceContraParte"); 
					this._PosicaoSwapAberturaCollectionByIdIndiceContraParte = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdIndiceContraParte;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoSwapHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdIndice
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdIndice == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdIndice = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdIndice", this._PosicaoSwapHistoricoCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdIndice.Query.Where(this._PosicaoSwapHistoricoCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._PosicaoSwapHistoricoCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdIndice.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdIndice"); 
					this._PosicaoSwapHistoricoCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdIndice;
		#endregion

				
		#region PosicaoTermoBolsaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollectionByIdIndice
		{
			get
			{
				if(this._PosicaoTermoBolsaCollectionByIdIndice == null)
				{
					this._PosicaoTermoBolsaCollectionByIdIndice = new PosicaoTermoBolsaCollection();
					this._PosicaoTermoBolsaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaCollectionByIdIndice", this._PosicaoTermoBolsaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._PosicaoTermoBolsaCollectionByIdIndice.Query.Where(this._PosicaoTermoBolsaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._PosicaoTermoBolsaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaCollectionByIdIndice.fks.Add(PosicaoTermoBolsaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._PosicaoTermoBolsaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaCollectionByIdIndice"); 
					this._PosicaoTermoBolsaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByIdIndice;
		#endregion

				
		#region PosicaoTermoBolsaAberturaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaAberturaCollection PosicaoTermoBolsaAberturaCollectionByIdIndice
		{
			get
			{
				if(this._PosicaoTermoBolsaAberturaCollectionByIdIndice == null)
				{
					this._PosicaoTermoBolsaAberturaCollectionByIdIndice = new PosicaoTermoBolsaAberturaCollection();
					this._PosicaoTermoBolsaAberturaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaAberturaCollectionByIdIndice", this._PosicaoTermoBolsaAberturaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._PosicaoTermoBolsaAberturaCollectionByIdIndice.Query.Where(this._PosicaoTermoBolsaAberturaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._PosicaoTermoBolsaAberturaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaAberturaCollectionByIdIndice.fks.Add(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._PosicaoTermoBolsaAberturaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaAberturaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaAberturaCollectionByIdIndice"); 
					this._PosicaoTermoBolsaAberturaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaAberturaCollection _PosicaoTermoBolsaAberturaCollectionByIdIndice;
		#endregion

				
		#region PosicaoTermoBolsaHistoricoCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_PosicaoTermoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaHistoricoCollection PosicaoTermoBolsaHistoricoCollectionByIdIndice
		{
			get
			{
				if(this._PosicaoTermoBolsaHistoricoCollectionByIdIndice == null)
				{
					this._PosicaoTermoBolsaHistoricoCollectionByIdIndice = new PosicaoTermoBolsaHistoricoCollection();
					this._PosicaoTermoBolsaHistoricoCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaHistoricoCollectionByIdIndice", this._PosicaoTermoBolsaHistoricoCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._PosicaoTermoBolsaHistoricoCollectionByIdIndice.Query.Where(this._PosicaoTermoBolsaHistoricoCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._PosicaoTermoBolsaHistoricoCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaHistoricoCollectionByIdIndice.fks.Add(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._PosicaoTermoBolsaHistoricoCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaHistoricoCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaHistoricoCollectionByIdIndice"); 
					this._PosicaoTermoBolsaHistoricoCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByIdIndice;
		#endregion

				
		#region TabelaTaxaPerformanceCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_TabelaTaxaPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaPerformanceCollection TabelaTaxaPerformanceCollectionByIdIndice
		{
			get
			{
				if(this._TabelaTaxaPerformanceCollectionByIdIndice == null)
				{
					this._TabelaTaxaPerformanceCollectionByIdIndice = new TabelaTaxaPerformanceCollection();
					this._TabelaTaxaPerformanceCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaPerformanceCollectionByIdIndice", this._TabelaTaxaPerformanceCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._TabelaTaxaPerformanceCollectionByIdIndice.Query.Where(this._TabelaTaxaPerformanceCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._TabelaTaxaPerformanceCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaPerformanceCollectionByIdIndice.fks.Add(TabelaTaxaPerformanceMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._TabelaTaxaPerformanceCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaPerformanceCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("TabelaTaxaPerformanceCollectionByIdIndice"); 
					this._TabelaTaxaPerformanceCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private TabelaTaxaPerformanceCollection _TabelaTaxaPerformanceCollectionByIdIndice;
		#endregion

				
		#region TituloRendaFixaCollectionByIdIndice - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Indice_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixaCollection TituloRendaFixaCollectionByIdIndice
		{
			get
			{
				if(this._TituloRendaFixaCollectionByIdIndice == null)
				{
					this._TituloRendaFixaCollectionByIdIndice = new TituloRendaFixaCollection();
					this._TituloRendaFixaCollectionByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TituloRendaFixaCollectionByIdIndice", this._TituloRendaFixaCollectionByIdIndice);
				
					if(this.IdIndice != null)
					{
						this._TituloRendaFixaCollectionByIdIndice.Query.Where(this._TituloRendaFixaCollectionByIdIndice.Query.IdIndice == this.IdIndice);
						this._TituloRendaFixaCollectionByIdIndice.Query.Load();

						// Auto-hookup Foreign Keys
						this._TituloRendaFixaCollectionByIdIndice.fks.Add(TituloRendaFixaMetadata.ColumnNames.IdIndice, this.IdIndice);
					}
				}

				return this._TituloRendaFixaCollectionByIdIndice;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TituloRendaFixaCollectionByIdIndice != null) 
				{ 
					this.RemovePostSave("TituloRendaFixaCollectionByIdIndice"); 
					this._TituloRendaFixaCollectionByIdIndice = null;
					
				} 
			} 			
		}

		private TituloRendaFixaCollection _TituloRendaFixaCollectionByIdIndice;
		#endregion

				
		#region UpToCurvaRendaFixaByIdCurva - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_Curva_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixa UpToCurvaRendaFixaByIdCurva
		{
			get
			{
				if(this._UpToCurvaRendaFixaByIdCurva == null
					&& IdCurva != null					)
				{
					this._UpToCurvaRendaFixaByIdCurva = new CurvaRendaFixa();
					this._UpToCurvaRendaFixaByIdCurva.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurva", this._UpToCurvaRendaFixaByIdCurva);
					this._UpToCurvaRendaFixaByIdCurva.Query.Where(this._UpToCurvaRendaFixaByIdCurva.Query.IdCurvaRendaFixa == this.IdCurva);
					this._UpToCurvaRendaFixaByIdCurva.Query.Load();
				}

				return this._UpToCurvaRendaFixaByIdCurva;
			}
			
			set
			{
				this.RemovePreSave("UpToCurvaRendaFixaByIdCurva");
				

				if(value == null)
				{
					this.IdCurva = null;
					this._UpToCurvaRendaFixaByIdCurva = null;
				}
				else
				{
					this.IdCurva = value.IdCurvaRendaFixa;
					this._UpToCurvaRendaFixaByIdCurva = value;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurva", this._UpToCurvaRendaFixaByIdCurva);
				}
				
			}
		}
		#endregion
		

				
		#region UpToFeederByIdFeeder - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_Feeder_FK
		/// </summary>

		[XmlIgnore]
		public Feeder UpToFeederByIdFeeder
		{
			get
			{
				if(this._UpToFeederByIdFeeder == null
					&& IdFeeder != null					)
				{
					this._UpToFeederByIdFeeder = new Feeder();
					this._UpToFeederByIdFeeder.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToFeederByIdFeeder", this._UpToFeederByIdFeeder);
					this._UpToFeederByIdFeeder.Query.Where(this._UpToFeederByIdFeeder.Query.IdFeeder == this.IdFeeder);
					this._UpToFeederByIdFeeder.Query.Load();
				}

				return this._UpToFeederByIdFeeder;
			}
			
			set
			{
				this.RemovePreSave("UpToFeederByIdFeeder");
				

				if(value == null)
				{
					this.IdFeeder = null;
					this._UpToFeederByIdFeeder = null;
				}
				else
				{
					this.IdFeeder = value.IdFeeder;
					this._UpToFeederByIdFeeder = value;
					this.SetPreSave("UpToFeederByIdFeeder", this._UpToFeederByIdFeeder);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdIndiceBenchmark", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "ClassesOffShoreCollectionByIndexadorPerf", typeof(ClassesOffShoreCollection), new ClassesOffShore()));
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdIndiceAbertura", typeof(ClienteCollection), new Cliente()));
			props.Add(new esPropertyDescriptor(this, "CotacaoIndiceCollectionByIdIndice", typeof(CotacaoIndiceCollection), new CotacaoIndice()));
			props.Add(new esPropertyDescriptor(this, "CurvaRendaFixaCollectionByIdIndice", typeof(CurvaRendaFixaCollection), new CurvaRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemFundoCollectionByIdIndice", typeof(EnquadraItemFundoCollection), new EnquadraItemFundo()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemRendaFixaCollectionByIdIndice", typeof(EnquadraItemRendaFixaCollection), new EnquadraItemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "EstrategiaCollectionByIdIndiceBenchmark", typeof(EstrategiaCollection), new Estrategia()));
			props.Add(new esPropertyDescriptor(this, "ListaBenchmarkCollectionByIdIndice", typeof(ListaBenchmarkCollection), new ListaBenchmark()));
			props.Add(new esPropertyDescriptor(this, "OrdemTermoBolsaCollectionByIdIndice", typeof(OrdemTermoBolsaCollection), new OrdemTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "ParidadeCambialCollectionByIdIndice", typeof(ParidadeCambialCollection), new ParidadeCambial()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdIndiceContraParte", typeof(PosicaoSwapCollection), new PosicaoSwap()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdIndice", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdIndiceContraParte", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdIndice", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaCollectionByIdIndice", typeof(PosicaoTermoBolsaCollection), new PosicaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaAberturaCollectionByIdIndice", typeof(PosicaoTermoBolsaAberturaCollection), new PosicaoTermoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaHistoricoCollectionByIdIndice", typeof(PosicaoTermoBolsaHistoricoCollection), new PosicaoTermoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaPerformanceCollectionByIdIndice", typeof(TabelaTaxaPerformanceCollection), new TabelaTaxaPerformance()));
			props.Add(new esPropertyDescriptor(this, "TituloRendaFixaCollectionByIdIndice", typeof(TituloRendaFixaCollection), new TituloRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToFeederByIdFeeder != null)
			{
				this.IdFeeder = this._UpToFeederByIdFeeder.IdFeeder;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class IndiceCollection : esIndiceCollection
	{
		#region ManyToManyCarteiraCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyCarteiraCollection(System.Int32? IdCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira", IdCarteira);
	
			return base.Load( esQueryType.ManyToMany, 
				"Indice,ListaBenchmark|IdIndice,IdIndice|IdCarteira",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esIndiceQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IndiceMetadata.Meta();
			}
		}	
		

		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoDivulgacao
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.TipoDivulgacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdIndiceBase
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.IdIndiceBase, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFeeder
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.IdFeeder, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCurva
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.IdCurva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TabelaBDS
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.TabelaBDS, esSystemType.String);
			}
		} 
		
		public esQueryItem IntervaloBDS
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.IntervaloBDS, esSystemType.Byte);
			}
		} 
		
		public esQueryItem AtributoBDS
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.AtributoBDS, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, IndiceMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IndiceCollection")]
	public partial class IndiceCollection : esIndiceCollection, IEnumerable<Indice>
	{
		public IndiceCollection()
		{

		}
		
		public static implicit operator List<Indice>(IndiceCollection coll)
		{
			List<Indice> list = new List<Indice>();
			
			foreach (Indice emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IndiceMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IndiceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Indice(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Indice();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IndiceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IndiceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IndiceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Indice AddNew()
		{
			Indice entity = base.AddNewEntity() as Indice;
			
			return entity;
		}

		public Indice FindByPrimaryKey(System.Int16 idIndice)
		{
			return base.FindByPrimaryKey(idIndice) as Indice;
		}


		#region IEnumerable<Indice> Members

		IEnumerator<Indice> IEnumerable<Indice>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Indice;
			}
		}

		#endregion
		
		private IndiceQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Indice' table
	/// </summary>

	[Serializable]
	public partial class Indice : esIndice
	{
		public Indice()
		{

		}
	
		public Indice(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IndiceMetadata.Meta();
			}
		}
		
		
		
		override protected esIndiceQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IndiceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IndiceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IndiceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IndiceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IndiceQuery query;
	}



	[Serializable]
	public partial class IndiceQuery : esIndiceQuery
	{
		public IndiceQuery()
		{

		}		
		
		public IndiceQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IndiceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IndiceMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IndiceMetadata.ColumnNames.IdIndice, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = IndiceMetadata.PropertyNames.IdIndice;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = IndiceMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.Tipo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = IndiceMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.TipoDivulgacao, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = IndiceMetadata.PropertyNames.TipoDivulgacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.Taxa, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndiceMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.Percentual, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndiceMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 6;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.IdIndiceBase, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IndiceMetadata.PropertyNames.IdIndiceBase;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.IdFeeder, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IndiceMetadata.PropertyNames.IdFeeder;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('5')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.IdCurva, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IndiceMetadata.PropertyNames.IdCurva;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.TabelaBDS, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = IndiceMetadata.PropertyNames.TabelaBDS;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.IntervaloBDS, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = IndiceMetadata.PropertyNames.IntervaloBDS;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.AtributoBDS, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = IndiceMetadata.PropertyNames.AtributoBDS;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndiceMetadata.ColumnNames.CodigoBDS, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IndiceMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IndiceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdIndice = "IdIndice";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string TipoDivulgacao = "TipoDivulgacao";
			 public const string Taxa = "Taxa";
			 public const string Percentual = "Percentual";
			 public const string IdIndiceBase = "IdIndiceBase";
			 public const string IdFeeder = "IdFeeder";
			 public const string IdCurva = "IdCurva";
			 public const string TabelaBDS = "TabelaBDS";
			 public const string IntervaloBDS = "IntervaloBDS";
			 public const string AtributoBDS = "AtributoBDS";
			 public const string CodigoBDS = "CodigoBDS";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdIndice = "IdIndice";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string TipoDivulgacao = "TipoDivulgacao";
			 public const string Taxa = "Taxa";
			 public const string Percentual = "Percentual";
			 public const string IdIndiceBase = "IdIndiceBase";
			 public const string IdFeeder = "IdFeeder";
			 public const string IdCurva = "IdCurva";
			 public const string TabelaBDS = "TabelaBDS";
			 public const string IntervaloBDS = "IntervaloBDS";
			 public const string AtributoBDS = "AtributoBDS";
			 public const string CodigoBDS = "CodigoBDS";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IndiceMetadata))
			{
				if(IndiceMetadata.mapDelegates == null)
				{
					IndiceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IndiceMetadata.meta == null)
				{
					IndiceMetadata.meta = new IndiceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoDivulgacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdIndiceBase", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFeeder", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCurva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TabelaBDS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IntervaloBDS", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("AtributoBDS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Indice";
				meta.Destination = "Indice";
				
				meta.spInsert = "proc_IndiceInsert";				
				meta.spUpdate = "proc_IndiceUpdate";		
				meta.spDelete = "proc_IndiceDelete";
				meta.spLoadAll = "proc_IndiceLoadAll";
				meta.spLoadByPrimaryKey = "proc_IndiceLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IndiceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
