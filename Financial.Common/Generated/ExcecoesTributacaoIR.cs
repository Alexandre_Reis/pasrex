/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/02/2016 18:21:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.Swap;
using Financial.RendaFixa;



namespace Financial.Common
{

    [Serializable]
    abstract public class esExcecoesTributacaoIRCollection : esEntityCollection
    {
        public esExcecoesTributacaoIRCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "ExcecoesTributacaoIRCollection";
        }

        #region Query Logic
        protected void InitQuery(esExcecoesTributacaoIRQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esExcecoesTributacaoIRQuery);
        }
        #endregion

        virtual public ExcecoesTributacaoIR DetachEntity(ExcecoesTributacaoIR entity)
        {
            return base.DetachEntity(entity) as ExcecoesTributacaoIR;
        }

        virtual public ExcecoesTributacaoIR AttachEntity(ExcecoesTributacaoIR entity)
        {
            return base.AttachEntity(entity) as ExcecoesTributacaoIR;
        }

        virtual public void Combine(ExcecoesTributacaoIRCollection collection)
        {
            base.Combine(collection);
        }

        new public ExcecoesTributacaoIR this[int index]
        {
            get
            {
                return base[index] as ExcecoesTributacaoIR;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(ExcecoesTributacaoIR);
        }
    }



    [Serializable]
    abstract public class esExcecoesTributacaoIR : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esExcecoesTributacaoIRQuery GetDynamicQuery()
        {
            return null;
        }

        public esExcecoesTributacaoIR()
        {

        }

        public esExcecoesTributacaoIR(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idExcecoesTributacaoIR)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idExcecoesTributacaoIR);
            else
                return LoadByPrimaryKeyStoredProcedure(idExcecoesTributacaoIR);
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idExcecoesTributacaoIR)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idExcecoesTributacaoIR);
            else
                return LoadByPrimaryKeyStoredProcedure(idExcecoesTributacaoIR);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idExcecoesTributacaoIR)
        {
            esExcecoesTributacaoIRQuery query = this.GetDynamicQuery();
            query.Where(query.IdExcecoesTributacaoIR == idExcecoesTributacaoIR);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idExcecoesTributacaoIR)
        {
            esParameters parms = new esParameters();
            parms.Add("IdExcecoesTributacaoIR", idExcecoesTributacaoIR);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdExcecoesTributacaoIR": this.str.IdExcecoesTributacaoIR = (string)value; break;
                        case "Mercado": this.str.Mercado = (string)value; break;
                        case "TipoClasseAtivo": this.str.TipoClasseAtivo = (string)value; break;
                        case "Ativo": this.str.Ativo = (string)value; break;
                        case "TipoInvestidor": this.str.TipoInvestidor = (string)value; break;
                        case "IsencaoIR": this.str.IsencaoIR = (string)value; break;
                        case "AliquotaIR": this.str.AliquotaIR = (string)value; break;
                        case "IsencaoGanhoCapital": this.str.IsencaoGanhoCapital = (string)value; break;
                        case "AliquotaIRGanhoCapital": this.str.AliquotaIRGanhoCapital = (string)value; break;
                        case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;
                        case "SerieBMF": this.str.SerieBMF = (string)value; break;
                        case "IdTituloRendaFixa": this.str.IdTituloRendaFixa = (string)value; break;
                        case "IdOperacaoSwap": this.str.IdOperacaoSwap = (string)value; break;
                        case "IdCarteira": this.str.IdCarteira = (string)value; break;
                        case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;
                        case "DataInicio": this.str.DataInicio = (string)value; break;
                        case "DataFim": this.str.DataFim = (string)value; break;
                        case "RegimeEspecialTributacao": this.str.RegimeEspecialTributacao = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdExcecoesTributacaoIR":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdExcecoesTributacaoIR = (System.Int32?)value;
                            break;

                        case "Mercado":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.Mercado = (System.Int32?)value;
                            break;

                        case "TipoClasseAtivo":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.TipoClasseAtivo = (System.Int32?)value;
                            break;

                        case "TipoInvestidor":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.TipoInvestidor = (System.Int32?)value;
                            break;

                        case "IsencaoIR":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IsencaoIR = (System.Int32?)value;
                            break;

                        case "IsencaoGanhoCapital":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IsencaoGanhoCapital = (System.Int32?)value;
                            break;

                        case "IdTituloRendaFixa":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdTituloRendaFixa = (System.Int32?)value;
                            break;

                        case "IdOperacaoSwap":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdOperacaoSwap = (System.Int32?)value;
                            break;

                        case "IdCarteira":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCarteira = (System.Int32?)value;
                            break;

                        case "DataInicio":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataInicio = (System.DateTime?)value;
                            break;

                        case "DataFim":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataFim = (System.DateTime?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IdExcecoesTributacaoIR
        /// </summary>
        virtual public System.Int32? IdExcecoesTributacaoIR
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdExcecoesTributacaoIR);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdExcecoesTributacaoIR, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.Mercado
        /// </summary>
        virtual public System.Int32? Mercado
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.Mercado);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.Mercado, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.TipoClasseAtivo
        /// </summary>
        virtual public System.Int32? TipoClasseAtivo
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.TipoClasseAtivo);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.TipoClasseAtivo, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.Ativo
        /// </summary>
        virtual public System.String Ativo
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.Ativo);
            }

            set
            {
                base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.Ativo, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.TipoInvestidor
        /// </summary>
        virtual public System.Int32? TipoInvestidor
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.TipoInvestidor);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.TipoInvestidor, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IsencaoIR
        /// </summary>
        virtual public System.Int32? IsencaoIR
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoIR);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoIR, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.AliquotaIR
        /// </summary>
        virtual public System.String AliquotaIR
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIR);
            }

            set
            {
                base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIR, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IsencaoGanhoCapital
        /// </summary>
        virtual public System.Int32? IsencaoGanhoCapital
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoGanhoCapital);
            }

            set
            {
                base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoGanhoCapital, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.AliquotaIRGanhoCapital
        /// </summary>
        virtual public System.String AliquotaIRGanhoCapital
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIRGanhoCapital);
            }

            set
            {
                base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIRGanhoCapital, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.CdAtivoBMF
        /// </summary>
        virtual public System.String CdAtivoBMF
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBMF);
            }

            set
            {
                if (base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBMF, value))
                {
                    this._UpToAtivoBMFByCdAtivoBMF = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.SerieBMF
        /// </summary>
        virtual public System.String SerieBMF
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.SerieBMF);
            }

            set
            {
                if (base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.SerieBMF, value))
                {
                    this._UpToAtivoBMFByCdAtivoBMF = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IdTituloRendaFixa
        /// </summary>
        virtual public System.Int32? IdTituloRendaFixa
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa);
            }

            set
            {
                if (base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa, value))
                {
                    this._UpToTituloRendaFixaByIdTituloRendaFixa = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IdOperacaoSwap
        /// </summary>
        virtual public System.Int32? IdOperacaoSwap
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdOperacaoSwap);
            }

            set
            {
                if (base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdOperacaoSwap, value))
                {
                    this._UpToOperacaoSwapByIdOperacaoSwap = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.IdCarteira
        /// </summary>
        virtual public System.Int32? IdCarteira
        {
            get
            {
                return base.GetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira);
            }

            set
            {
                if (base.SetSystemInt32(ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira, value))
                {
                    this._UpToCarteiraByIdCarteira = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.CdAtivoBolsa
        /// </summary>
        virtual public System.String CdAtivoBolsa
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa);
            }

            set
            {
                if (base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa, value))
                {
                    this._UpToAtivoBolsaByCdAtivoBolsa = null;
                }
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.DataInicio
        /// </summary>
        virtual public System.DateTime? DataInicio
        {
            get
            {
                return base.GetSystemDateTime(ExcecoesTributacaoIRMetadata.ColumnNames.DataInicio);
            }

            set
            {
                base.SetSystemDateTime(ExcecoesTributacaoIRMetadata.ColumnNames.DataInicio, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.DataFim
        /// </summary>
        virtual public System.DateTime? DataFim
        {
            get
            {
                return base.GetSystemDateTime(ExcecoesTributacaoIRMetadata.ColumnNames.DataFim);
            }

            set
            {
                base.SetSystemDateTime(ExcecoesTributacaoIRMetadata.ColumnNames.DataFim, value);
            }
        }

        /// <summary>
        /// Maps to ExcecoesTributacaoIR.RegimeEspecialTributacao
        /// </summary>
        virtual public System.String RegimeEspecialTributacao
        {
            get
            {
                return base.GetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.RegimeEspecialTributacao);
            }

            set
            {
                base.SetSystemString(ExcecoesTributacaoIRMetadata.ColumnNames.RegimeEspecialTributacao, value);
            }
        }

        [CLSCompliant(false)]
        internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
        [CLSCompliant(false)]
        internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
        [CLSCompliant(false)]
        internal protected Carteira _UpToCarteiraByIdCarteira;
        [CLSCompliant(false)]
        internal protected OperacaoSwap _UpToOperacaoSwapByIdOperacaoSwap;
        [CLSCompliant(false)]
        internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTituloRendaFixa;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esExcecoesTributacaoIR entity)
            {
                this.entity = entity;
            }


            public System.String IdExcecoesTributacaoIR
            {
                get
                {
                    System.Int32? data = entity.IdExcecoesTributacaoIR;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdExcecoesTributacaoIR = null;
                    else entity.IdExcecoesTributacaoIR = Convert.ToInt32(value);
                }
            }

            public System.String Mercado
            {
                get
                {
                    System.Int32? data = entity.Mercado;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Mercado = null;
                    else entity.Mercado = Convert.ToInt32(value);
                }
            }

            public System.String TipoClasseAtivo
            {
                get
                {
                    System.Int32? data = entity.TipoClasseAtivo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoClasseAtivo = null;
                    else entity.TipoClasseAtivo = Convert.ToInt32(value);
                }
            }

            public System.String Ativo
            {
                get
                {
                    System.String data = entity.Ativo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Ativo = null;
                    else entity.Ativo = Convert.ToString(value);
                }
            }

            public System.String TipoInvestidor
            {
                get
                {
                    System.Int32? data = entity.TipoInvestidor;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoInvestidor = null;
                    else entity.TipoInvestidor = Convert.ToInt32(value);
                }
            }

            public System.String IsencaoIR
            {
                get
                {
                    System.Int32? data = entity.IsencaoIR;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IsencaoIR = null;
                    else entity.IsencaoIR = Convert.ToInt32(value);
                }
            }

            public System.String AliquotaIR
            {
                get
                {
                    System.String data = entity.AliquotaIR;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.AliquotaIR = null;
                    else entity.AliquotaIR = Convert.ToString(value);
                }
            }

            public System.String IsencaoGanhoCapital
            {
                get
                {
                    System.Int32? data = entity.IsencaoGanhoCapital;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IsencaoGanhoCapital = null;
                    else entity.IsencaoGanhoCapital = Convert.ToInt32(value);
                }
            }

            public System.String AliquotaIRGanhoCapital
            {
                get
                {
                    System.String data = entity.AliquotaIRGanhoCapital;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.AliquotaIRGanhoCapital = null;
                    else entity.AliquotaIRGanhoCapital = Convert.ToString(value);
                }
            }

            public System.String CdAtivoBMF
            {
                get
                {
                    System.String data = entity.CdAtivoBMF;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
                    else entity.CdAtivoBMF = Convert.ToString(value);
                }
            }

            public System.String SerieBMF
            {
                get
                {
                    System.String data = entity.SerieBMF;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.SerieBMF = null;
                    else entity.SerieBMF = Convert.ToString(value);
                }
            }

            public System.String IdTituloRendaFixa
            {
                get
                {
                    System.Int32? data = entity.IdTituloRendaFixa;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdTituloRendaFixa = null;
                    else entity.IdTituloRendaFixa = Convert.ToInt32(value);
                }
            }

            public System.String IdOperacaoSwap
            {
                get
                {
                    System.Int32? data = entity.IdOperacaoSwap;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdOperacaoSwap = null;
                    else entity.IdOperacaoSwap = Convert.ToInt32(value);
                }
            }

            public System.String IdCarteira
            {
                get
                {
                    System.Int32? data = entity.IdCarteira;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCarteira = null;
                    else entity.IdCarteira = Convert.ToInt32(value);
                }
            }

            public System.String CdAtivoBolsa
            {
                get
                {
                    System.String data = entity.CdAtivoBolsa;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
                    else entity.CdAtivoBolsa = Convert.ToString(value);
                }
            }

            public System.String DataInicio
            {
                get
                {
                    System.DateTime? data = entity.DataInicio;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataInicio = null;
                    else entity.DataInicio = Convert.ToDateTime(value);
                }
            }

            public System.String DataFim
            {
                get
                {
                    System.DateTime? data = entity.DataFim;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataFim = null;
                    else entity.DataFim = Convert.ToDateTime(value);
                }
            }

            public System.String RegimeEspecialTributacao
            {
                get
                {
                    System.String data = entity.RegimeEspecialTributacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.RegimeEspecialTributacao = null;
                    else entity.RegimeEspecialTributacao = Convert.ToString(value);
                }
            }


            private esExcecoesTributacaoIR entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esExcecoesTributacaoIRQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esExcecoesTributacaoIR can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class ExcecoesTributacaoIR : esExcecoesTributacaoIR
    {


        #region UpToAtivoBMFByCdAtivoBMF - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_AtivoBMF
        /// </summary>

        [XmlIgnore]
        public AtivoBMF UpToAtivoBMFByCdAtivoBMF
        {
            get
            {
                if (this._UpToAtivoBMFByCdAtivoBMF == null
                    && CdAtivoBMF != null && SerieBMF != null)
                {
                    this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
                    this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
                    this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
                    this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.SerieBMF);
                    this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
                }

                return this._UpToAtivoBMFByCdAtivoBMF;
            }

            set
            {
                this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");


                if (value == null)
                {
                    this.CdAtivoBMF = null;
                    this.SerieBMF = null;
                    this._UpToAtivoBMFByCdAtivoBMF = null;
                }
                else
                {
                    this.CdAtivoBMF = value.CdAtivoBMF;
                    this.SerieBMF = value.Serie;
                    this._UpToAtivoBMFByCdAtivoBMF = value;
                    this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
                }

            }
        }
        #endregion



        #region UpToAtivoBolsaByCdAtivoBolsa - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK__ExcecoesT__CdAti__69527C00
        /// </summary>

        [XmlIgnore]
        public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
        {
            get
            {
                if (this._UpToAtivoBolsaByCdAtivoBolsa == null
                    && CdAtivoBolsa != null)
                {
                    this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
                    this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
                    this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
                    this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
                }

                return this._UpToAtivoBolsaByCdAtivoBolsa;
            }

            set
            {
                this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");


                if (value == null)
                {
                    this.CdAtivoBolsa = null;
                    this._UpToAtivoBolsaByCdAtivoBolsa = null;
                }
                else
                {
                    this.CdAtivoBolsa = value.CdAtivoBolsa;
                    this._UpToAtivoBolsaByCdAtivoBolsa = value;
                    this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
                }

            }
        }
        #endregion



        #region UpToCarteiraByIdCarteira - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK__ExcecoesT__IdCar__685E57C7
        /// </summary>

        [XmlIgnore]
        public Carteira UpToCarteiraByIdCarteira
        {
            get
            {
                if (this._UpToCarteiraByIdCarteira == null
                    && IdCarteira != null)
                {
                    this._UpToCarteiraByIdCarteira = new Carteira();
                    this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Load();
                }

                return this._UpToCarteiraByIdCarteira;
            }

            set
            {
                this.RemovePreSave("UpToCarteiraByIdCarteira");


                if (value == null)
                {
                    this.IdCarteira = null;
                    this._UpToCarteiraByIdCarteira = null;
                }
                else
                {
                    this.IdCarteira = value.IdCarteira;
                    this._UpToCarteiraByIdCarteira = value;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                }

            }
        }
        #endregion



        #region UpToOperacaoSwapByIdOperacaoSwap - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK__ExcecoesT__IdOpe__676A338E
        /// </summary>

        [XmlIgnore]
        public OperacaoSwap UpToOperacaoSwapByIdOperacaoSwap
        {
            get
            {
                if (this._UpToOperacaoSwapByIdOperacaoSwap == null
                    && IdOperacaoSwap != null)
                {
                    this._UpToOperacaoSwapByIdOperacaoSwap = new OperacaoSwap();
                    this._UpToOperacaoSwapByIdOperacaoSwap.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToOperacaoSwapByIdOperacaoSwap", this._UpToOperacaoSwapByIdOperacaoSwap);
                    this._UpToOperacaoSwapByIdOperacaoSwap.Query.Where(this._UpToOperacaoSwapByIdOperacaoSwap.Query.IdOperacao == this.IdOperacaoSwap);
                    this._UpToOperacaoSwapByIdOperacaoSwap.Query.Load();
                }

                return this._UpToOperacaoSwapByIdOperacaoSwap;
            }

            set
            {
                this.RemovePreSave("UpToOperacaoSwapByIdOperacaoSwap");


                if (value == null)
                {
                    this.IdOperacaoSwap = null;
                    this._UpToOperacaoSwapByIdOperacaoSwap = null;
                }
                else
                {
                    this.IdOperacaoSwap = value.IdOperacao;
                    this._UpToOperacaoSwapByIdOperacaoSwap = value;
                    this.SetPreSave("UpToOperacaoSwapByIdOperacaoSwap", this._UpToOperacaoSwapByIdOperacaoSwap);
                }

            }
        }
        #endregion



        #region UpToTituloRendaFixaByIdTituloRendaFixa - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK__ExcecoesT__IdTit__66760F55
        /// </summary>

        [XmlIgnore]
        public TituloRendaFixa UpToTituloRendaFixaByIdTituloRendaFixa
        {
            get
            {
                if (this._UpToTituloRendaFixaByIdTituloRendaFixa == null
                    && IdTituloRendaFixa != null)
                {
                    this._UpToTituloRendaFixaByIdTituloRendaFixa = new TituloRendaFixa();
                    this._UpToTituloRendaFixaByIdTituloRendaFixa.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToTituloRendaFixaByIdTituloRendaFixa", this._UpToTituloRendaFixaByIdTituloRendaFixa);
                    this._UpToTituloRendaFixaByIdTituloRendaFixa.Query.Where(this._UpToTituloRendaFixaByIdTituloRendaFixa.Query.IdTitulo == this.IdTituloRendaFixa);
                    this._UpToTituloRendaFixaByIdTituloRendaFixa.Query.Load();
                }

                return this._UpToTituloRendaFixaByIdTituloRendaFixa;
            }

            set
            {
                this.RemovePreSave("UpToTituloRendaFixaByIdTituloRendaFixa");


                if (value == null)
                {
                    this.IdTituloRendaFixa = null;
                    this._UpToTituloRendaFixaByIdTituloRendaFixa = null;
                }
                else
                {
                    this.IdTituloRendaFixa = value.IdTitulo;
                    this._UpToTituloRendaFixaByIdTituloRendaFixa = value;
                    this.SetPreSave("UpToTituloRendaFixaByIdTituloRendaFixa", this._UpToTituloRendaFixaByIdTituloRendaFixa);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
            if (!this.es.IsDeleted && this._UpToOperacaoSwapByIdOperacaoSwap != null)
            {
                this.IdOperacaoSwap = this._UpToOperacaoSwapByIdOperacaoSwap.IdOperacao;
            }
            if (!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTituloRendaFixa != null)
            {
                this.IdTituloRendaFixa = this._UpToTituloRendaFixaByIdTituloRendaFixa.IdTitulo;
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esExcecoesTributacaoIRQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return ExcecoesTributacaoIRMetadata.Meta();
            }
        }


        public esQueryItem IdExcecoesTributacaoIR
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IdExcecoesTributacaoIR, esSystemType.Int32);
            }
        }

        public esQueryItem Mercado
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.Mercado, esSystemType.Int32);
            }
        }

        public esQueryItem TipoClasseAtivo
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.TipoClasseAtivo, esSystemType.Int32);
            }
        }

        public esQueryItem Ativo
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.Ativo, esSystemType.String);
            }
        }

        public esQueryItem TipoInvestidor
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.TipoInvestidor, esSystemType.Int32);
            }
        }

        public esQueryItem IsencaoIR
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoIR, esSystemType.Int32);
            }
        }

        public esQueryItem AliquotaIR
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIR, esSystemType.String);
            }
        }

        public esQueryItem IsencaoGanhoCapital
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoGanhoCapital, esSystemType.Int32);
            }
        }

        public esQueryItem AliquotaIRGanhoCapital
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIRGanhoCapital, esSystemType.String);
            }
        }

        public esQueryItem CdAtivoBMF
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
            }
        }

        public esQueryItem SerieBMF
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.SerieBMF, esSystemType.String);
            }
        }

        public esQueryItem IdTituloRendaFixa
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa, esSystemType.Int32);
            }
        }

        public esQueryItem IdOperacaoSwap
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IdOperacaoSwap, esSystemType.Int32);
            }
        }

        public esQueryItem IdCarteira
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
            }
        }

        public esQueryItem CdAtivoBolsa
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
            }
        }

        public esQueryItem DataInicio
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
            }
        }

        public esQueryItem DataFim
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.DataFim, esSystemType.DateTime);
            }
        }

        public esQueryItem RegimeEspecialTributacao
        {
            get
            {
                return new esQueryItem(this, ExcecoesTributacaoIRMetadata.ColumnNames.RegimeEspecialTributacao, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("ExcecoesTributacaoIRCollection")]
    public partial class ExcecoesTributacaoIRCollection : esExcecoesTributacaoIRCollection, IEnumerable<ExcecoesTributacaoIR>
    {
        public ExcecoesTributacaoIRCollection()
        {

        }

        public static implicit operator List<ExcecoesTributacaoIR>(ExcecoesTributacaoIRCollection coll)
        {
            List<ExcecoesTributacaoIR> list = new List<ExcecoesTributacaoIR>();

            foreach (ExcecoesTributacaoIR emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ExcecoesTributacaoIRMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ExcecoesTributacaoIRQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new ExcecoesTributacaoIR(row);
        }

        override protected esEntity CreateEntity()
        {
            return new ExcecoesTributacaoIR();
        }


        #endregion


        [BrowsableAttribute(false)]
        public ExcecoesTributacaoIRQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ExcecoesTributacaoIRQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(ExcecoesTributacaoIRQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public ExcecoesTributacaoIR AddNew()
        {
            ExcecoesTributacaoIR entity = base.AddNewEntity() as ExcecoesTributacaoIR;

            return entity;
        }

        public ExcecoesTributacaoIR FindByPrimaryKey(System.Int32 idExcecoesTributacaoIR)
        {
            return base.FindByPrimaryKey(idExcecoesTributacaoIR) as ExcecoesTributacaoIR;
        }


        #region IEnumerable<ExcecoesTributacaoIR> Members

        IEnumerator<ExcecoesTributacaoIR> IEnumerable<ExcecoesTributacaoIR>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as ExcecoesTributacaoIR;
            }
        }

        #endregion

        private ExcecoesTributacaoIRQuery query;
    }


    /// <summary>
    /// Encapsulates the 'ExcecoesTributacaoIR' table
    /// </summary>

    [Serializable]
    public partial class ExcecoesTributacaoIR : esExcecoesTributacaoIR
    {
        public ExcecoesTributacaoIR()
        {

        }

        public ExcecoesTributacaoIR(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ExcecoesTributacaoIRMetadata.Meta();
            }
        }



        override protected esExcecoesTributacaoIRQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ExcecoesTributacaoIRQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public ExcecoesTributacaoIRQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ExcecoesTributacaoIRQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(ExcecoesTributacaoIRQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private ExcecoesTributacaoIRQuery query;
    }



    [Serializable]
    public partial class ExcecoesTributacaoIRQuery : esExcecoesTributacaoIRQuery
    {
        public ExcecoesTributacaoIRQuery()
        {

        }

        public ExcecoesTributacaoIRQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class ExcecoesTributacaoIRMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected ExcecoesTributacaoIRMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IdExcecoesTributacaoIR, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IdExcecoesTributacaoIR;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.Mercado, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.Mercado;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.TipoClasseAtivo, 2, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.TipoClasseAtivo;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.Ativo, 3, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.Ativo;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.TipoInvestidor, 4, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.TipoInvestidor;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoIR, 5, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IsencaoIR;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIR, 6, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.AliquotaIR;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IsencaoGanhoCapital, 7, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IsencaoGanhoCapital;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.AliquotaIRGanhoCapital, 8, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.AliquotaIRGanhoCapital;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBMF, 9, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.CdAtivoBMF;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.SerieBMF, 10, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.SerieBMF;
            c.CharacterMaxLength = 5;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa, 11, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IdTituloRendaFixa;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IdOperacaoSwap, 12, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IdOperacaoSwap;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira, 13, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.IdCarteira;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa, 14, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.CdAtivoBolsa;
            c.CharacterMaxLength = 25;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.DataInicio, 15, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.DataInicio;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.DataFim, 16, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.DataFim;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ExcecoesTributacaoIRMetadata.ColumnNames.RegimeEspecialTributacao, 17, typeof(System.String), esSystemType.String);
            c.PropertyName = ExcecoesTributacaoIRMetadata.PropertyNames.RegimeEspecialTributacao;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);


        }
        #endregion

        static public ExcecoesTributacaoIRMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdExcecoesTributacaoIR = "IdExcecoesTributacaoIR";
            public const string Mercado = "Mercado";
            public const string TipoClasseAtivo = "TipoClasseAtivo";
            public const string Ativo = "Ativo";
            public const string TipoInvestidor = "TipoInvestidor";
            public const string IsencaoIR = "IsencaoIR";
            public const string AliquotaIR = "AliquotaIR";
            public const string IsencaoGanhoCapital = "IsencaoGanhoCapital";
            public const string AliquotaIRGanhoCapital = "AliquotaIRGanhoCapital";
            public const string CdAtivoBMF = "CdAtivoBMF";
            public const string SerieBMF = "SerieBMF";
            public const string IdTituloRendaFixa = "IdTituloRendaFixa";
            public const string IdOperacaoSwap = "IdOperacaoSwap";
            public const string IdCarteira = "IdCarteira";
            public const string CdAtivoBolsa = "CdAtivoBolsa";
            public const string DataInicio = "DataInicio";
            public const string DataFim = "DataFim";
            public const string RegimeEspecialTributacao = "RegimeEspecialTributacao";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdExcecoesTributacaoIR = "IdExcecoesTributacaoIR";
            public const string Mercado = "Mercado";
            public const string TipoClasseAtivo = "TipoClasseAtivo";
            public const string Ativo = "Ativo";
            public const string TipoInvestidor = "TipoInvestidor";
            public const string IsencaoIR = "IsencaoIR";
            public const string AliquotaIR = "AliquotaIR";
            public const string IsencaoGanhoCapital = "IsencaoGanhoCapital";
            public const string AliquotaIRGanhoCapital = "AliquotaIRGanhoCapital";
            public const string CdAtivoBMF = "CdAtivoBMF";
            public const string SerieBMF = "SerieBMF";
            public const string IdTituloRendaFixa = "IdTituloRendaFixa";
            public const string IdOperacaoSwap = "IdOperacaoSwap";
            public const string IdCarteira = "IdCarteira";
            public const string CdAtivoBolsa = "CdAtivoBolsa";
            public const string DataInicio = "DataInicio";
            public const string DataFim = "DataFim";
            public const string RegimeEspecialTributacao = "RegimeEspecialTributacao";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(ExcecoesTributacaoIRMetadata))
            {
                if (ExcecoesTributacaoIRMetadata.mapDelegates == null)
                {
                    ExcecoesTributacaoIRMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (ExcecoesTributacaoIRMetadata.meta == null)
                {
                    ExcecoesTributacaoIRMetadata.meta = new ExcecoesTributacaoIRMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdExcecoesTributacaoIR", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Mercado", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("TipoClasseAtivo", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Ativo", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("TipoInvestidor", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IsencaoIR", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("AliquotaIR", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("IsencaoGanhoCapital", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("AliquotaIRGanhoCapital", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("SerieBMF", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("IdTituloRendaFixa", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdOperacaoSwap", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("RegimeEspecialTributacao", new esTypeMap("char", "System.String"));



                meta.Source = "ExcecoesTributacaoIR";
                meta.Destination = "ExcecoesTributacaoIR";

                meta.spInsert = "proc_ExcecoesTributacaoIRInsert";
                meta.spUpdate = "proc_ExcecoesTributacaoIRUpdate";
                meta.spDelete = "proc_ExcecoesTributacaoIRDelete";
                meta.spLoadAll = "proc_ExcecoesTributacaoIRLoadAll";
                meta.spLoadByPrimaryKey = "proc_ExcecoesTributacaoIRLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private ExcecoesTributacaoIRMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
