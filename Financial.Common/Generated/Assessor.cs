/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		






















using Financial.BMF;
using Financial.Bolsa;
using Financial.Swap;
using Financial.RendaFixa;




		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esAssessorCollection : esEntityCollection
	{
		public esAssessorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AssessorCollection";
		}

		#region Query Logic
		protected void InitQuery(esAssessorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAssessorQuery);
		}
		#endregion
		
		virtual public Assessor DetachEntity(Assessor entity)
		{
			return base.DetachEntity(entity) as Assessor;
		}
		
		virtual public Assessor AttachEntity(Assessor entity)
		{
			return base.AttachEntity(entity) as Assessor;
		}
		
		virtual public void Combine(AssessorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Assessor this[int index]
		{
			get
			{
				return base[index] as Assessor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Assessor);
		}
	}



	[Serializable]
	abstract public class esAssessor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAssessorQuery GetDynamicQuery()
		{
			return null;
		}

		public esAssessor()
		{

		}

		public esAssessor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAssessor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAssessor);
			else
				return LoadByPrimaryKeyStoredProcedure(idAssessor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idAssessor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAssessorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAssessor == idAssessor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAssessor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAssessor);
			else
				return LoadByPrimaryKeyStoredProcedure(idAssessor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAssessor)
		{
			esAssessorQuery query = this.GetDynamicQuery();
			query.Where(query.IdAssessor == idAssessor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAssessor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAssessor",idAssessor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Assessor.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(AssessorMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				base.SetSystemInt32(AssessorMetadata.ColumnNames.IdAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to Assessor.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(AssessorMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(AssessorMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Assessor.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(AssessorMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(AssessorMetadata.ColumnNames.Apelido, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAssessor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
			

			private esAssessor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAssessorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAssessor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Assessor : esAssessor
	{

				
		#region ClienteBMFCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_ClienteBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteBMFCollection ClienteBMFCollectionByIdAssessor
		{
			get
			{
				if(this._ClienteBMFCollectionByIdAssessor == null)
				{
					this._ClienteBMFCollectionByIdAssessor = new ClienteBMFCollection();
					this._ClienteBMFCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteBMFCollectionByIdAssessor", this._ClienteBMFCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._ClienteBMFCollectionByIdAssessor.Query.Where(this._ClienteBMFCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._ClienteBMFCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteBMFCollectionByIdAssessor.fks.Add(ClienteBMFMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._ClienteBMFCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteBMFCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("ClienteBMFCollectionByIdAssessor"); 
					this._ClienteBMFCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private ClienteBMFCollection _ClienteBMFCollectionByIdAssessor;
		#endregion

				
		#region ClienteBolsaCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_ClienteBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteBolsaCollection ClienteBolsaCollectionByIdAssessor
		{
			get
			{
				if(this._ClienteBolsaCollectionByIdAssessor == null)
				{
					this._ClienteBolsaCollectionByIdAssessor = new ClienteBolsaCollection();
					this._ClienteBolsaCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteBolsaCollectionByIdAssessor", this._ClienteBolsaCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._ClienteBolsaCollectionByIdAssessor.Query.Where(this._ClienteBolsaCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._ClienteBolsaCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteBolsaCollectionByIdAssessor.fks.Add(ClienteBolsaMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._ClienteBolsaCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteBolsaCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("ClienteBolsaCollectionByIdAssessor"); 
					this._ClienteBolsaCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private ClienteBolsaCollection _ClienteBolsaCollectionByIdAssessor;
		#endregion

				
		#region ClienteRendaFixaCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_ClienteRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteRendaFixaCollection ClienteRendaFixaCollectionByIdAssessor
		{
			get
			{
				if(this._ClienteRendaFixaCollectionByIdAssessor == null)
				{
					this._ClienteRendaFixaCollectionByIdAssessor = new ClienteRendaFixaCollection();
					this._ClienteRendaFixaCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteRendaFixaCollectionByIdAssessor", this._ClienteRendaFixaCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._ClienteRendaFixaCollectionByIdAssessor.Query.Where(this._ClienteRendaFixaCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._ClienteRendaFixaCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteRendaFixaCollectionByIdAssessor.fks.Add(ClienteRendaFixaMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._ClienteRendaFixaCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteRendaFixaCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("ClienteRendaFixaCollectionByIdAssessor"); 
					this._ClienteRendaFixaCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private ClienteRendaFixaCollection _ClienteRendaFixaCollectionByIdAssessor;
		#endregion

				
		#region PosicaoSwapCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdAssessor
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdAssessor == null)
				{
					this._PosicaoSwapCollectionByIdAssessor = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdAssessor", this._PosicaoSwapCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._PosicaoSwapCollectionByIdAssessor.Query.Where(this._PosicaoSwapCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._PosicaoSwapCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdAssessor.fks.Add(PosicaoSwapMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._PosicaoSwapCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdAssessor"); 
					this._PosicaoSwapCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdAssessor;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdAssessor
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdAssessor == null)
				{
					this._PosicaoSwapAberturaCollectionByIdAssessor = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdAssessor", this._PosicaoSwapAberturaCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._PosicaoSwapAberturaCollectionByIdAssessor.Query.Where(this._PosicaoSwapAberturaCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._PosicaoSwapAberturaCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdAssessor.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdAssessor"); 
					this._PosicaoSwapAberturaCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdAssessor;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdAssessor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Assessor_PosicaoSwapHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdAssessor
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdAssessor == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdAssessor = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdAssessor", this._PosicaoSwapHistoricoCollectionByIdAssessor);
				
					if(this.IdAssessor != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdAssessor.Query.Where(this._PosicaoSwapHistoricoCollectionByIdAssessor.Query.IdAssessor == this.IdAssessor);
						this._PosicaoSwapHistoricoCollectionByIdAssessor.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdAssessor.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdAssessor, this.IdAssessor);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdAssessor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdAssessor != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdAssessor"); 
					this._PosicaoSwapHistoricoCollectionByIdAssessor = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdAssessor;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteBMFCollectionByIdAssessor", typeof(ClienteBMFCollection), new ClienteBMF()));
			props.Add(new esPropertyDescriptor(this, "ClienteBolsaCollectionByIdAssessor", typeof(ClienteBolsaCollection), new ClienteBolsa()));
			props.Add(new esPropertyDescriptor(this, "ClienteRendaFixaCollectionByIdAssessor", typeof(ClienteRendaFixaCollection), new ClienteRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdAssessor", typeof(PosicaoSwapCollection), new PosicaoSwap()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdAssessor", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdAssessor", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ClienteBMFCollectionByIdAssessor != null)
			{
				foreach(ClienteBMF obj in this._ClienteBMFCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
			if(this._ClienteBolsaCollectionByIdAssessor != null)
			{
				foreach(ClienteBolsa obj in this._ClienteBolsaCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
			if(this._ClienteRendaFixaCollectionByIdAssessor != null)
			{
				foreach(ClienteRendaFixa obj in this._ClienteRendaFixaCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
			if(this._PosicaoSwapCollectionByIdAssessor != null)
			{
				foreach(PosicaoSwap obj in this._PosicaoSwapCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
			if(this._PosicaoSwapAberturaCollectionByIdAssessor != null)
			{
				foreach(PosicaoSwapAbertura obj in this._PosicaoSwapAberturaCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
			if(this._PosicaoSwapHistoricoCollectionByIdAssessor != null)
			{
				foreach(PosicaoSwapHistorico obj in this._PosicaoSwapHistoricoCollectionByIdAssessor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAssessor = this.IdAssessor;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAssessorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AssessorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, AssessorMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, AssessorMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, AssessorMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AssessorCollection")]
	public partial class AssessorCollection : esAssessorCollection, IEnumerable<Assessor>
	{
		public AssessorCollection()
		{

		}
		
		public static implicit operator List<Assessor>(AssessorCollection coll)
		{
			List<Assessor> list = new List<Assessor>();
			
			foreach (Assessor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AssessorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AssessorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Assessor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Assessor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AssessorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AssessorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AssessorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Assessor AddNew()
		{
			Assessor entity = base.AddNewEntity() as Assessor;
			
			return entity;
		}

		public Assessor FindByPrimaryKey(System.Int32 idAssessor)
		{
			return base.FindByPrimaryKey(idAssessor) as Assessor;
		}


		#region IEnumerable<Assessor> Members

		IEnumerator<Assessor> IEnumerable<Assessor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Assessor;
			}
		}

		#endregion
		
		private AssessorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Assessor' table
	/// </summary>

	[Serializable]
	public partial class Assessor : esAssessor
	{
		public Assessor()
		{

		}
	
		public Assessor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AssessorMetadata.Meta();
			}
		}
		
		
		
		override protected esAssessorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AssessorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AssessorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AssessorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AssessorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AssessorQuery query;
	}



	[Serializable]
	public partial class AssessorQuery : esAssessorQuery
	{
		public AssessorQuery()
		{

		}		
		
		public AssessorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AssessorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AssessorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AssessorMetadata.ColumnNames.IdAssessor, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AssessorMetadata.PropertyNames.IdAssessor;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AssessorMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AssessorMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AssessorMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AssessorMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AssessorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAssessor = "IdAssessor";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAssessor = "IdAssessor";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AssessorMetadata))
			{
				if(AssessorMetadata.mapDelegates == null)
				{
					AssessorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AssessorMetadata.meta == null)
				{
					AssessorMetadata.meta = new AssessorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Assessor";
				meta.Destination = "Assessor";
				
				meta.spInsert = "proc_AssessorInsert";				
				meta.spUpdate = "proc_AssessorUpdate";		
				meta.spDelete = "proc_AssessorDelete";
				meta.spLoadAll = "proc_AssessorLoadAll";
				meta.spLoadByPrimaryKey = "proc_AssessorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AssessorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
