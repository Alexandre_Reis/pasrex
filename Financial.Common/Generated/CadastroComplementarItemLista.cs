/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/11/2014 14:04:44
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esCadastroComplementarItemListaCollection : esEntityCollection
	{
		public esCadastroComplementarItemListaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroComplementarItemListaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroComplementarItemListaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroComplementarItemListaQuery);
		}
		#endregion
		
		virtual public CadastroComplementarItemLista DetachEntity(CadastroComplementarItemLista entity)
		{
			return base.DetachEntity(entity) as CadastroComplementarItemLista;
		}
		
		virtual public CadastroComplementarItemLista AttachEntity(CadastroComplementarItemLista entity)
		{
			return base.AttachEntity(entity) as CadastroComplementarItemLista;
		}
		
		virtual public void Combine(CadastroComplementarItemListaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroComplementarItemLista this[int index]
		{
			get
			{
				return base[index] as CadastroComplementarItemLista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroComplementarItemLista);
		}
	}



	[Serializable]
	abstract public class esCadastroComplementarItemLista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroComplementarItemListaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroComplementarItemLista()
		{

		}

		public esCadastroComplementarItemLista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idLista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idLista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLista)
		{
			esCadastroComplementarItemListaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLista == idLista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLista",idLista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLista": this.str.IdLista = (string)value; break;							
						case "IdTipoLista": this.str.IdTipoLista = (string)value; break;							
						case "ItemLista": this.str.ItemLista = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLista = (System.Int32?)value;
							break;
						
						case "IdTipoLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoLista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroComplementarItemLista.IdLista
		/// </summary>
		virtual public System.Int32? IdLista
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarItemListaMetadata.ColumnNames.IdLista);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarItemListaMetadata.ColumnNames.IdLista, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarItemLista.IdTipoLista
		/// </summary>
		virtual public System.Int32? IdTipoLista
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarItemListaMetadata.ColumnNames.IdTipoLista);
			}
			
			set
			{
				if(base.SetSystemInt32(CadastroComplementarItemListaMetadata.ColumnNames.IdTipoLista, value))
				{
					this._UpToCadastroComplementarTipoListaByIdTipoLista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarItemLista.ItemLista
		/// </summary>
		virtual public System.String ItemLista
		{
			get
			{
				return base.GetSystemString(CadastroComplementarItemListaMetadata.ColumnNames.ItemLista);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarItemListaMetadata.ColumnNames.ItemLista, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CadastroComplementarTipoLista _UpToCadastroComplementarTipoListaByIdTipoLista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroComplementarItemLista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLista
			{
				get
				{
					System.Int32? data = entity.IdLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLista = null;
					else entity.IdLista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTipoLista
			{
				get
				{
					System.Int32? data = entity.IdTipoLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoLista = null;
					else entity.IdTipoLista = Convert.ToInt32(value);
				}
			}
				
			public System.String ItemLista
			{
				get
				{
					System.String data = entity.ItemLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ItemLista = null;
					else entity.ItemLista = Convert.ToString(value);
				}
			}
			

			private esCadastroComplementarItemLista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroComplementarItemListaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroComplementarItemLista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroComplementarItemLista : esCadastroComplementarItemLista
	{

				
		#region UpToCadastroComplementarTipoListaByIdTipoLista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__CadastroC__IdTip__25276EE5
		/// </summary>

		[XmlIgnore]
		public CadastroComplementarTipoLista UpToCadastroComplementarTipoListaByIdTipoLista
		{
			get
			{
				if(this._UpToCadastroComplementarTipoListaByIdTipoLista == null
					&& IdTipoLista != null					)
				{
					this._UpToCadastroComplementarTipoListaByIdTipoLista = new CadastroComplementarTipoLista();
					this._UpToCadastroComplementarTipoListaByIdTipoLista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCadastroComplementarTipoListaByIdTipoLista", this._UpToCadastroComplementarTipoListaByIdTipoLista);
					this._UpToCadastroComplementarTipoListaByIdTipoLista.Query.Where(this._UpToCadastroComplementarTipoListaByIdTipoLista.Query.IdTipoLista == this.IdTipoLista);
					this._UpToCadastroComplementarTipoListaByIdTipoLista.Query.Load();
				}

				return this._UpToCadastroComplementarTipoListaByIdTipoLista;
			}
			
			set
			{
				this.RemovePreSave("UpToCadastroComplementarTipoListaByIdTipoLista");
				

				if(value == null)
				{
					this.IdTipoLista = null;
					this._UpToCadastroComplementarTipoListaByIdTipoLista = null;
				}
				else
				{
					this.IdTipoLista = value.IdTipoLista;
					this._UpToCadastroComplementarTipoListaByIdTipoLista = value;
					this.SetPreSave("UpToCadastroComplementarTipoListaByIdTipoLista", this._UpToCadastroComplementarTipoListaByIdTipoLista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCadastroComplementarTipoListaByIdTipoLista != null)
			{
				this.IdTipoLista = this._UpToCadastroComplementarTipoListaByIdTipoLista.IdTipoLista;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroComplementarItemListaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarItemListaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarItemListaMetadata.ColumnNames.IdLista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTipoLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarItemListaMetadata.ColumnNames.IdTipoLista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ItemLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarItemListaMetadata.ColumnNames.ItemLista, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroComplementarItemListaCollection")]
	public partial class CadastroComplementarItemListaCollection : esCadastroComplementarItemListaCollection, IEnumerable<CadastroComplementarItemLista>
	{
		public CadastroComplementarItemListaCollection()
		{

		}
		
		public static implicit operator List<CadastroComplementarItemLista>(CadastroComplementarItemListaCollection coll)
		{
			List<CadastroComplementarItemLista> list = new List<CadastroComplementarItemLista>();
			
			foreach (CadastroComplementarItemLista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroComplementarItemListaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarItemListaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroComplementarItemLista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroComplementarItemLista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroComplementarItemListaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarItemListaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroComplementarItemListaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroComplementarItemLista AddNew()
		{
			CadastroComplementarItemLista entity = base.AddNewEntity() as CadastroComplementarItemLista;
			
			return entity;
		}

		public CadastroComplementarItemLista FindByPrimaryKey(System.Int32 idLista)
		{
			return base.FindByPrimaryKey(idLista) as CadastroComplementarItemLista;
		}


		#region IEnumerable<CadastroComplementarItemLista> Members

		IEnumerator<CadastroComplementarItemLista> IEnumerable<CadastroComplementarItemLista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroComplementarItemLista;
			}
		}

		#endregion
		
		private CadastroComplementarItemListaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroComplementarItemLista' table
	/// </summary>

	[Serializable]
	public partial class CadastroComplementarItemLista : esCadastroComplementarItemLista
	{
		public CadastroComplementarItemLista()
		{

		}
	
		public CadastroComplementarItemLista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarItemListaMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroComplementarItemListaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarItemListaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroComplementarItemListaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarItemListaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroComplementarItemListaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroComplementarItemListaQuery query;
	}



	[Serializable]
	public partial class CadastroComplementarItemListaQuery : esCadastroComplementarItemListaQuery
	{
		public CadastroComplementarItemListaQuery()
		{

		}		
		
		public CadastroComplementarItemListaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroComplementarItemListaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroComplementarItemListaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroComplementarItemListaMetadata.ColumnNames.IdLista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarItemListaMetadata.PropertyNames.IdLista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarItemListaMetadata.ColumnNames.IdTipoLista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarItemListaMetadata.PropertyNames.IdTipoLista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarItemListaMetadata.ColumnNames.ItemLista, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarItemListaMetadata.PropertyNames.ItemLista;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroComplementarItemListaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLista = "IdLista";
			 public const string IdTipoLista = "IdTipoLista";
			 public const string ItemLista = "ItemLista";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLista = "IdLista";
			 public const string IdTipoLista = "IdTipoLista";
			 public const string ItemLista = "ItemLista";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroComplementarItemListaMetadata))
			{
				if(CadastroComplementarItemListaMetadata.mapDelegates == null)
				{
					CadastroComplementarItemListaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroComplementarItemListaMetadata.meta == null)
				{
					CadastroComplementarItemListaMetadata.meta = new CadastroComplementarItemListaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTipoLista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ItemLista", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "CadastroComplementarItemLista";
				meta.Destination = "CadastroComplementarItemLista";
				
				meta.spInsert = "proc_CadastroComplementarItemListaInsert";				
				meta.spUpdate = "proc_CadastroComplementarItemListaUpdate";		
				meta.spDelete = "proc_CadastroComplementarItemListaDelete";
				meta.spLoadAll = "proc_CadastroComplementarItemListaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroComplementarItemListaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroComplementarItemListaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
