/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 24/12/2014 11:22:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esParametroAdministradorFundoInvestimentoCollection : esEntityCollection
	{
		public esParametroAdministradorFundoInvestimentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ParametroAdministradorFundoInvestimentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esParametroAdministradorFundoInvestimentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esParametroAdministradorFundoInvestimentoQuery);
		}
		#endregion
		
		virtual public ParametroAdministradorFundoInvestimento DetachEntity(ParametroAdministradorFundoInvestimento entity)
		{
			return base.DetachEntity(entity) as ParametroAdministradorFundoInvestimento;
		}
		
		virtual public ParametroAdministradorFundoInvestimento AttachEntity(ParametroAdministradorFundoInvestimento entity)
		{
			return base.AttachEntity(entity) as ParametroAdministradorFundoInvestimento;
		}
		
		virtual public void Combine(ParametroAdministradorFundoInvestimentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ParametroAdministradorFundoInvestimento this[int index]
		{
			get
			{
				return base[index] as ParametroAdministradorFundoInvestimento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ParametroAdministradorFundoInvestimento);
		}
	}



	[Serializable]
	abstract public class esParametroAdministradorFundoInvestimento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esParametroAdministradorFundoInvestimentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esParametroAdministradorFundoInvestimento()
		{

		}

		public esParametroAdministradorFundoInvestimento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idParametro)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametro);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametro);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idParametro)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametro);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametro);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idParametro)
		{
			esParametroAdministradorFundoInvestimentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdParametro == idParametro);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idParametro)
		{
			esParameters parms = new esParameters();
			parms.Add("IdParametro",idParametro);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdParametro": this.str.IdParametro = (string)value; break;							
						case "ExecucaoRecolhimento": this.str.ExecucaoRecolhimento = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "DataRecolhimento": this.str.DataRecolhimento = (string)value; break;							
						case "Administrador": this.str.Administrador = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdParametro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParametro = (System.Int32?)value;
							break;
						
						case "ExecucaoRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExecucaoRecolhimento = (System.Int32?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AliquotaIR = (System.Int32?)value;
							break;
						
						case "DataRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRecolhimento = (System.DateTime?)value;
							break;
						
						case "Administrador":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Administrador = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ParametroAdministradorFundoInvestimento.IdParametro
		/// </summary>
		virtual public System.Int32? IdParametro
		{
			get
			{
				return base.GetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.IdParametro);
			}
			
			set
			{
				base.SetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.IdParametro, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroAdministradorFundoInvestimento.ExecucaoRecolhimento
		/// </summary>
		virtual public System.Int32? ExecucaoRecolhimento
		{
			get
			{
				return base.GetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.ExecucaoRecolhimento);
			}
			
			set
			{
				base.SetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.ExecucaoRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroAdministradorFundoInvestimento.AliquotaIR
		/// </summary>
		virtual public System.Int32? AliquotaIR
		{
			get
			{
				return base.GetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroAdministradorFundoInvestimento.DataRecolhimento
		/// </summary>
		virtual public System.DateTime? DataRecolhimento
		{
			get
			{
				return base.GetSystemDateTime(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.DataRecolhimento);
			}
			
			set
			{
				base.SetSystemDateTime(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.DataRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroAdministradorFundoInvestimento.Administrador
		/// </summary>
		virtual public System.Int32? Administrador
		{
			get
			{
				return base.GetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.Administrador);
			}
			
			set
			{
				if(base.SetSystemInt32(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.Administrador, value))
				{
					this._UpToAgenteMercadoByAdministrador = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByAdministrador;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esParametroAdministradorFundoInvestimento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdParametro
			{
				get
				{
					System.Int32? data = entity.IdParametro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametro = null;
					else entity.IdParametro = Convert.ToInt32(value);
				}
			}
				
			public System.String ExecucaoRecolhimento
			{
				get
				{
					System.Int32? data = entity.ExecucaoRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecucaoRecolhimento = null;
					else entity.ExecucaoRecolhimento = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Int32? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRecolhimento
			{
				get
				{
					System.DateTime? data = entity.DataRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRecolhimento = null;
					else entity.DataRecolhimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Administrador
			{
				get
				{
					System.Int32? data = entity.Administrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Administrador = null;
					else entity.Administrador = Convert.ToInt32(value);
				}
			}
			

			private esParametroAdministradorFundoInvestimento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esParametroAdministradorFundoInvestimentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esParametroAdministradorFundoInvestimento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ParametroAdministradorFundoInvestimento : esParametroAdministradorFundoInvestimento
	{

				
		#region UpToAgenteMercadoByAdministrador - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Parametro__Admin__47477CBF
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByAdministrador
		{
			get
			{
				if(this._UpToAgenteMercadoByAdministrador == null
					&& Administrador != null					)
				{
					this._UpToAgenteMercadoByAdministrador = new AgenteMercado();
					this._UpToAgenteMercadoByAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByAdministrador", this._UpToAgenteMercadoByAdministrador);
					this._UpToAgenteMercadoByAdministrador.Query.Where(this._UpToAgenteMercadoByAdministrador.Query.IdAgente == this.Administrador);
					this._UpToAgenteMercadoByAdministrador.Query.Load();
				}

				return this._UpToAgenteMercadoByAdministrador;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByAdministrador");
				

				if(value == null)
				{
					this.Administrador = null;
					this._UpToAgenteMercadoByAdministrador = null;
				}
				else
				{
					this.Administrador = value.IdAgente;
					this._UpToAgenteMercadoByAdministrador = value;
					this.SetPreSave("UpToAgenteMercadoByAdministrador", this._UpToAgenteMercadoByAdministrador);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByAdministrador != null)
			{
				this.Administrador = this._UpToAgenteMercadoByAdministrador.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esParametroAdministradorFundoInvestimentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ParametroAdministradorFundoInvestimentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdParametro
		{
			get
			{
				return new esQueryItem(this, ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.IdParametro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExecucaoRecolhimento
		{
			get
			{
				return new esQueryItem(this, ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.ExecucaoRecolhimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.AliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRecolhimento
		{
			get
			{
				return new esQueryItem(this, ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.DataRecolhimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Administrador
		{
			get
			{
				return new esQueryItem(this, ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.Administrador, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ParametroAdministradorFundoInvestimentoCollection")]
	public partial class ParametroAdministradorFundoInvestimentoCollection : esParametroAdministradorFundoInvestimentoCollection, IEnumerable<ParametroAdministradorFundoInvestimento>
	{
		public ParametroAdministradorFundoInvestimentoCollection()
		{

		}
		
		public static implicit operator List<ParametroAdministradorFundoInvestimento>(ParametroAdministradorFundoInvestimentoCollection coll)
		{
			List<ParametroAdministradorFundoInvestimento> list = new List<ParametroAdministradorFundoInvestimento>();
			
			foreach (ParametroAdministradorFundoInvestimento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ParametroAdministradorFundoInvestimentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParametroAdministradorFundoInvestimentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ParametroAdministradorFundoInvestimento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ParametroAdministradorFundoInvestimento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ParametroAdministradorFundoInvestimentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametroAdministradorFundoInvestimentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ParametroAdministradorFundoInvestimentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ParametroAdministradorFundoInvestimento AddNew()
		{
			ParametroAdministradorFundoInvestimento entity = base.AddNewEntity() as ParametroAdministradorFundoInvestimento;
			
			return entity;
		}

		public ParametroAdministradorFundoInvestimento FindByPrimaryKey(System.Int32 idParametro)
		{
			return base.FindByPrimaryKey(idParametro) as ParametroAdministradorFundoInvestimento;
		}


		#region IEnumerable<ParametroAdministradorFundoInvestimento> Members

		IEnumerator<ParametroAdministradorFundoInvestimento> IEnumerable<ParametroAdministradorFundoInvestimento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ParametroAdministradorFundoInvestimento;
			}
		}

		#endregion
		
		private ParametroAdministradorFundoInvestimentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ParametroAdministradorFundoInvestimento' table
	/// </summary>

	[Serializable]
	public partial class ParametroAdministradorFundoInvestimento : esParametroAdministradorFundoInvestimento
	{
		public ParametroAdministradorFundoInvestimento()
		{

		}
	
		public ParametroAdministradorFundoInvestimento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ParametroAdministradorFundoInvestimentoMetadata.Meta();
			}
		}
		
		
		
		override protected esParametroAdministradorFundoInvestimentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParametroAdministradorFundoInvestimentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ParametroAdministradorFundoInvestimentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametroAdministradorFundoInvestimentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ParametroAdministradorFundoInvestimentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ParametroAdministradorFundoInvestimentoQuery query;
	}



	[Serializable]
	public partial class ParametroAdministradorFundoInvestimentoQuery : esParametroAdministradorFundoInvestimentoQuery
	{
		public ParametroAdministradorFundoInvestimentoQuery()
		{

		}		
		
		public ParametroAdministradorFundoInvestimentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ParametroAdministradorFundoInvestimentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ParametroAdministradorFundoInvestimentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.IdParametro, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroAdministradorFundoInvestimentoMetadata.PropertyNames.IdParametro;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.ExecucaoRecolhimento, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroAdministradorFundoInvestimentoMetadata.PropertyNames.ExecucaoRecolhimento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.AliquotaIR, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroAdministradorFundoInvestimentoMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.DataRecolhimento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ParametroAdministradorFundoInvestimentoMetadata.PropertyNames.DataRecolhimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.Administrador, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroAdministradorFundoInvestimentoMetadata.PropertyNames.Administrador;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ParametroAdministradorFundoInvestimentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdParametro = "IdParametro";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string Administrador = "Administrador";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdParametro = "IdParametro";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string Administrador = "Administrador";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ParametroAdministradorFundoInvestimentoMetadata))
			{
				if(ParametroAdministradorFundoInvestimentoMetadata.mapDelegates == null)
				{
					ParametroAdministradorFundoInvestimentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ParametroAdministradorFundoInvestimentoMetadata.meta == null)
				{
					ParametroAdministradorFundoInvestimentoMetadata.meta = new ParametroAdministradorFundoInvestimentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdParametro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExecucaoRecolhimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRecolhimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Administrador", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ParametroAdministradorFundoInvestimento";
				meta.Destination = "ParametroAdministradorFundoInvestimento";
				
				meta.spInsert = "proc_ParametroAdministradorFundoInvestimentoInsert";				
				meta.spUpdate = "proc_ParametroAdministradorFundoInvestimentoUpdate";		
				meta.spDelete = "proc_ParametroAdministradorFundoInvestimentoDelete";
				meta.spLoadAll = "proc_ParametroAdministradorFundoInvestimentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ParametroAdministradorFundoInvestimentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ParametroAdministradorFundoInvestimentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
