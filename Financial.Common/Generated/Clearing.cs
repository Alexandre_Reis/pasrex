/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/02/2015 13:20:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;

namespace Financial.Common
{

	[Serializable]
	abstract public class esClearingCollection : esEntityCollection
	{
		public esClearingCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClearingCollection";
		}

		#region Query Logic
		protected void InitQuery(esClearingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClearingQuery);
		}
		#endregion
		
		virtual public Clearing DetachEntity(Clearing entity)
		{
			return base.DetachEntity(entity) as Clearing;
		}
		
		virtual public Clearing AttachEntity(Clearing entity)
		{
			return base.AttachEntity(entity) as Clearing;
		}
		
		virtual public void Combine(ClearingCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Clearing this[int index]
		{
			get
			{
				return base[index] as Clearing;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Clearing);
		}
	}



	[Serializable]
	abstract public class esClearing : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClearingQuery GetDynamicQuery()
		{
			return null;
		}

		public esClearing()
		{

		}

		public esClearing(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClearing)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClearing);
			else
				return LoadByPrimaryKeyStoredProcedure(idClearing);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClearing)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClearing);
			else
				return LoadByPrimaryKeyStoredProcedure(idClearing);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClearing)
		{
			esClearingQuery query = this.GetDynamicQuery();
			query.Where(query.IdClearing == idClearing);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClearing)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClearing",idClearing);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Cnpj": this.str.Cnpj = (string)value; break;							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "IdLocalFeriado": this.str.IdLocalFeriado = (string)value; break;							
						case "CodigoCETIP": this.str.CodigoCETIP = (string)value; break;							
						case "CodigoSELIC": this.str.CodigoSELIC = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "IdLocalFeriado":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocalFeriado = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Clearing.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(ClearingMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(ClearingMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to Clearing.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(ClearingMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(ClearingMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to Clearing.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ClearingMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ClearingMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Clearing.CNPJ
		/// </summary>
		virtual public System.String Cnpj
		{
			get
			{
				return base.GetSystemString(ClearingMetadata.ColumnNames.Cnpj);
			}
			
			set
			{
				base.SetSystemString(ClearingMetadata.ColumnNames.Cnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to Clearing.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(ClearingMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				if(base.SetSystemByte(ClearingMetadata.ColumnNames.IdFormaLiquidacao, value))
				{
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Clearing.IdLocalFeriado
		/// </summary>
		virtual public System.Int16? IdLocalFeriado
		{
			get
			{
				return base.GetSystemInt16(ClearingMetadata.ColumnNames.IdLocalFeriado);
			}
			
			set
			{
				if(base.SetSystemInt16(ClearingMetadata.ColumnNames.IdLocalFeriado, value))
				{
					this._UpToLocalFeriadoByIdLocalFeriado = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Clearing.CodigoCETIP
		/// </summary>
		virtual public System.String CodigoCETIP
		{
			get
			{
				return base.GetSystemString(ClearingMetadata.ColumnNames.CodigoCETIP);
			}
			
			set
			{
				base.SetSystemString(ClearingMetadata.ColumnNames.CodigoCETIP, value);
			}
		}
		
		/// <summary>
		/// Maps to Clearing.CodigoSELIC
		/// </summary>
		virtual public System.String CodigoSELIC
		{
			get
			{
				return base.GetSystemString(ClearingMetadata.ColumnNames.CodigoSELIC);
			}
			
			set
			{
				base.SetSystemString(ClearingMetadata.ColumnNames.CodigoSELIC, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected FormaLiquidacao _UpToFormaLiquidacaoByIdFormaLiquidacao;
		[CLSCompliant(false)]
		internal protected LocalFeriado _UpToLocalFeriadoByIdLocalFeriado;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClearing entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Cnpj
			{
				get
				{
					System.String data = entity.Cnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cnpj = null;
					else entity.Cnpj = Convert.ToString(value);
				}
			}
				
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdLocalFeriado
			{
				get
				{
					System.Int16? data = entity.IdLocalFeriado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalFeriado = null;
					else entity.IdLocalFeriado = Convert.ToInt16(value);
				}
			}
				
			public System.String CodigoCETIP
			{
				get
				{
					System.String data = entity.CodigoCETIP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCETIP = null;
					else entity.CodigoCETIP = Convert.ToString(value);
				}
			}
				
			public System.String CodigoSELIC
			{
				get
				{
					System.String data = entity.CodigoSELIC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSELIC = null;
					else entity.CodigoSELIC = Convert.ToString(value);
				}
			}
			

			private esClearing entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClearingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClearing can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Clearing : esClearing
	{

				
		#region UpToFormaLiquidacaoByIdFormaLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Clearing_FormaLiq_FK
		/// </summary>

		[XmlIgnore]
		public FormaLiquidacao UpToFormaLiquidacaoByIdFormaLiquidacao
		{
			get
			{
				if(this._UpToFormaLiquidacaoByIdFormaLiquidacao == null
					&& IdFormaLiquidacao != null					)
				{
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = new FormaLiquidacao();
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Where(this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Load();
				}

				return this._UpToFormaLiquidacaoByIdFormaLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToFormaLiquidacaoByIdFormaLiquidacao");
				

				if(value == null)
				{
					this.IdFormaLiquidacao = null;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
				else
				{
					this.IdFormaLiquidacao = value.IdFormaLiquidacao;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = value;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToLocalFeriadoByIdLocalFeriado - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Clearing_LocalFer_FK
		/// </summary>

		[XmlIgnore]
		public LocalFeriado UpToLocalFeriadoByIdLocalFeriado
		{
			get
			{
				if(this._UpToLocalFeriadoByIdLocalFeriado == null
					&& IdLocalFeriado != null					)
				{
					this._UpToLocalFeriadoByIdLocalFeriado = new LocalFeriado();
					this._UpToLocalFeriadoByIdLocalFeriado.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToLocalFeriadoByIdLocalFeriado", this._UpToLocalFeriadoByIdLocalFeriado);
					this._UpToLocalFeriadoByIdLocalFeriado.Query.Where(this._UpToLocalFeriadoByIdLocalFeriado.Query.IdLocal == this.IdLocalFeriado);
					this._UpToLocalFeriadoByIdLocalFeriado.Query.Load();
				}

				return this._UpToLocalFeriadoByIdLocalFeriado;
			}
			
			set
			{
				this.RemovePreSave("UpToLocalFeriadoByIdLocalFeriado");
				

				if(value == null)
				{
					this.IdLocalFeriado = null;
					this._UpToLocalFeriadoByIdLocalFeriado = null;
				}
				else
				{
					this.IdLocalFeriado = value.IdLocal;
					this._UpToLocalFeriadoByIdLocalFeriado = value;
					this.SetPreSave("UpToLocalFeriadoByIdLocalFeriado", this._UpToLocalFeriadoByIdLocalFeriado);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToFormaLiquidacaoByIdFormaLiquidacao != null)
			{
				this.IdFormaLiquidacao = this._UpToFormaLiquidacaoByIdFormaLiquidacao.IdFormaLiquidacao;
			}
			if(!this.es.IsDeleted && this._UpToLocalFeriadoByIdLocalFeriado != null)
			{
				this.IdLocalFeriado = this._UpToLocalFeriadoByIdLocalFeriado.IdLocal;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClearingQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClearingMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Cnpj
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.Cnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdLocalFeriado
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.IdLocalFeriado, esSystemType.Int16);
			}
		} 
		
		public esQueryItem CodigoCETIP
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.CodigoCETIP, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoSELIC
		{
			get
			{
				return new esQueryItem(this, ClearingMetadata.ColumnNames.CodigoSELIC, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClearingCollection")]
	public partial class ClearingCollection : esClearingCollection, IEnumerable<Clearing>
	{
		public ClearingCollection()
		{

		}
		
		public static implicit operator List<Clearing>(ClearingCollection coll)
		{
			List<Clearing> list = new List<Clearing>();
			
			foreach (Clearing emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClearingMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClearingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Clearing(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Clearing();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClearingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClearingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClearingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Clearing AddNew()
		{
			Clearing entity = base.AddNewEntity() as Clearing;
			
			return entity;
		}

		public Clearing FindByPrimaryKey(System.Int32 idClearing)
		{
			return base.FindByPrimaryKey(idClearing) as Clearing;
		}


		#region IEnumerable<Clearing> Members

		IEnumerator<Clearing> IEnumerable<Clearing>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Clearing;
			}
		}

		#endregion
		
		private ClearingQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Clearing' table
	/// </summary>

	[Serializable]
	public partial class Clearing : esClearing
	{
		public Clearing()
		{

		}
	
		public Clearing(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClearingMetadata.Meta();
			}
		}
		
		
		
		override protected esClearingQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClearingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClearingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClearingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClearingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClearingQuery query;
	}



	[Serializable]
	public partial class ClearingQuery : esClearingQuery
	{
		public ClearingQuery()
		{

		}		
		
		public ClearingQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClearingMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClearingMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClearingMetadata.ColumnNames.IdClearing, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClearingMetadata.PropertyNames.IdClearing;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.Codigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClearingMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClearingMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.Cnpj, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ClearingMetadata.PropertyNames.Cnpj;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.IdFormaLiquidacao, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClearingMetadata.PropertyNames.IdFormaLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.IdLocalFeriado, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClearingMetadata.PropertyNames.IdLocalFeriado;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.CodigoCETIP, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = ClearingMetadata.PropertyNames.CodigoCETIP;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClearingMetadata.ColumnNames.CodigoSELIC, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ClearingMetadata.PropertyNames.CodigoSELIC;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClearingMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClearing = "IdClearing";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string Cnpj = "CNPJ";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string IdLocalFeriado = "IdLocalFeriado";
			 public const string CodigoCETIP = "CodigoCETIP";
			 public const string CodigoSELIC = "CodigoSELIC";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClearing = "IdClearing";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string Cnpj = "Cnpj";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string IdLocalFeriado = "IdLocalFeriado";
			 public const string CodigoCETIP = "CodigoCETIP";
			 public const string CodigoSELIC = "CodigoSELIC";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClearingMetadata))
			{
				if(ClearingMetadata.mapDelegates == null)
				{
					ClearingMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClearingMetadata.meta == null)
				{
					ClearingMetadata.meta = new ClearingMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CNPJ", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdLocalFeriado", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("CodigoCETIP", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoSELIC", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Clearing";
				meta.Destination = "Clearing";
				
				meta.spInsert = "proc_ClearingInsert";				
				meta.spUpdate = "proc_ClearingUpdate";		
				meta.spDelete = "proc_ClearingDelete";
				meta.spLoadAll = "proc_ClearingLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClearingLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClearingMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
