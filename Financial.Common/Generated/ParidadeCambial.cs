/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 3/12/2015 3:38:44 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esParidadeCambialCollection : esEntityCollection
	{
		public esParidadeCambialCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ParidadeCambialCollection";
		}

		#region Query Logic
		protected void InitQuery(esParidadeCambialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esParidadeCambialQuery);
		}
		#endregion
		
		virtual public ParidadeCambial DetachEntity(ParidadeCambial entity)
		{
			return base.DetachEntity(entity) as ParidadeCambial;
		}
		
		virtual public ParidadeCambial AttachEntity(ParidadeCambial entity)
		{
			return base.AttachEntity(entity) as ParidadeCambial;
		}
		
		virtual public void Combine(ParidadeCambialCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ParidadeCambial this[int index]
		{
			get
			{
				return base[index] as ParidadeCambial;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ParidadeCambial);
		}
	}



	[Serializable]
	abstract public class esParidadeCambial : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esParidadeCambialQuery GetDynamicQuery()
		{
			return null;
		}

		public esParidadeCambial()
		{

		}

		public esParidadeCambial(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idParidade)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idParidade);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idParidade)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esParidadeCambialQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdParidade == idParidade);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idParidade)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idParidade);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idParidade)
		{
			esParidadeCambialQuery query = this.GetDynamicQuery();
			query.Where(query.IdParidade == idParidade);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idParidade)
		{
			esParameters parms = new esParameters();
			parms.Add("IdParidade",idParidade);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdParidade": this.str.IdParidade = (string)value; break;							
						case "IdMoedaNumerador": this.str.IdMoedaNumerador = (string)value; break;							
						case "IdMoedaDenominador": this.str.IdMoedaDenominador = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdParidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParidade = (System.Int32?)value;
							break;
						
						case "IdMoedaNumerador":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaNumerador = (System.Int16?)value;
							break;
						
						case "IdMoedaDenominador":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaDenominador = (System.Int16?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ParidadeCambial.IdParidade
		/// </summary>
		virtual public System.Int32? IdParidade
		{
			get
			{
				return base.GetSystemInt32(ParidadeCambialMetadata.ColumnNames.IdParidade);
			}
			
			set
			{
				base.SetSystemInt32(ParidadeCambialMetadata.ColumnNames.IdParidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ParidadeCambial.IdMoedaNumerador
		/// </summary>
		virtual public System.Int16? IdMoedaNumerador
		{
			get
			{
				return base.GetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador);
			}
			
			set
			{
				if(base.SetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador, value))
				{
					this._UpToMoedaByIdMoedaNumerador = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ParidadeCambial.IdMoedaDenominador
		/// </summary>
		virtual public System.Int16? IdMoedaDenominador
		{
			get
			{
				return base.GetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador);
			}
			
			set
			{
				if(base.SetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador, value))
				{
					this._UpToMoedaByIdMoedaDenominador = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ParidadeCambial.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ParidadeCambialMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ParidadeCambialMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to ParidadeCambial.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(ParidadeCambialMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaDenominador;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaNumerador;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esParidadeCambial entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdParidade
			{
				get
				{
					System.Int32? data = entity.IdParidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParidade = null;
					else entity.IdParidade = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMoedaNumerador
			{
				get
				{
					System.Int16? data = entity.IdMoedaNumerador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaNumerador = null;
					else entity.IdMoedaNumerador = Convert.ToInt16(value);
				}
			}
				
			public System.String IdMoedaDenominador
			{
				get
				{
					System.Int16? data = entity.IdMoedaDenominador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaDenominador = null;
					else entity.IdMoedaDenominador = Convert.ToInt16(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
			

			private esParidadeCambial entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esParidadeCambialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esParidadeCambial can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ParidadeCambial : esParidadeCambial
	{

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_ParidadeCambial_Indice
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoedaDenominador - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_ParidadeCambial_Moeda1
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaDenominador
		{
			get
			{
				if(this._UpToMoedaByIdMoedaDenominador == null
					&& IdMoedaDenominador != null					)
				{
					this._UpToMoedaByIdMoedaDenominador = new Moeda();
					this._UpToMoedaByIdMoedaDenominador.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaDenominador", this._UpToMoedaByIdMoedaDenominador);
					this._UpToMoedaByIdMoedaDenominador.Query.Where(this._UpToMoedaByIdMoedaDenominador.Query.IdMoeda == this.IdMoedaDenominador);
					this._UpToMoedaByIdMoedaDenominador.Query.Load();
				}

				return this._UpToMoedaByIdMoedaDenominador;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaDenominador");
				

				if(value == null)
				{
					this.IdMoedaDenominador = null;
					this._UpToMoedaByIdMoedaDenominador = null;
				}
				else
				{
					this.IdMoedaDenominador = value.IdMoeda;
					this._UpToMoedaByIdMoedaDenominador = value;
					this.SetPreSave("UpToMoedaByIdMoedaDenominador", this._UpToMoedaByIdMoedaDenominador);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoedaNumerador - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_ParidadeCambial_Moeda2
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaNumerador
		{
			get
			{
				if(this._UpToMoedaByIdMoedaNumerador == null
					&& IdMoedaNumerador != null					)
				{
					this._UpToMoedaByIdMoedaNumerador = new Moeda();
					this._UpToMoedaByIdMoedaNumerador.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaNumerador", this._UpToMoedaByIdMoedaNumerador);
					this._UpToMoedaByIdMoedaNumerador.Query.Where(this._UpToMoedaByIdMoedaNumerador.Query.IdMoeda == this.IdMoedaNumerador);
					this._UpToMoedaByIdMoedaNumerador.Query.Load();
				}

				return this._UpToMoedaByIdMoedaNumerador;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaNumerador");
				

				if(value == null)
				{
					this.IdMoedaNumerador = null;
					this._UpToMoedaByIdMoedaNumerador = null;
				}
				else
				{
					this.IdMoedaNumerador = value.IdMoeda;
					this._UpToMoedaByIdMoedaNumerador = value;
					this.SetPreSave("UpToMoedaByIdMoedaNumerador", this._UpToMoedaByIdMoedaNumerador);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaDenominador != null)
			{
				this.IdMoedaDenominador = this._UpToMoedaByIdMoedaDenominador.IdMoeda;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaNumerador != null)
			{
				this.IdMoedaNumerador = this._UpToMoedaByIdMoedaNumerador.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esParidadeCambialQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ParidadeCambialMetadata.Meta();
			}
		}	
		

		public esQueryItem IdParidade
		{
			get
			{
				return new esQueryItem(this, ParidadeCambialMetadata.ColumnNames.IdParidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMoedaNumerador
		{
			get
			{
				return new esQueryItem(this, ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdMoedaDenominador
		{
			get
			{
				return new esQueryItem(this, ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ParidadeCambialMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, ParidadeCambialMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ParidadeCambialCollection")]
	public partial class ParidadeCambialCollection : esParidadeCambialCollection, IEnumerable<ParidadeCambial>
	{
		public ParidadeCambialCollection()
		{

		}
		
		public static implicit operator List<ParidadeCambial>(ParidadeCambialCollection coll)
		{
			List<ParidadeCambial> list = new List<ParidadeCambial>();
			
			foreach (ParidadeCambial emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ParidadeCambialMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParidadeCambialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ParidadeCambial(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ParidadeCambial();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ParidadeCambialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParidadeCambialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ParidadeCambialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ParidadeCambial AddNew()
		{
			ParidadeCambial entity = base.AddNewEntity() as ParidadeCambial;
			
			return entity;
		}

		public ParidadeCambial FindByPrimaryKey(System.Int32 idParidade)
		{
			return base.FindByPrimaryKey(idParidade) as ParidadeCambial;
		}


		#region IEnumerable<ParidadeCambial> Members

		IEnumerator<ParidadeCambial> IEnumerable<ParidadeCambial>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ParidadeCambial;
			}
		}

		#endregion
		
		private ParidadeCambialQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ParidadeCambial' table
	/// </summary>

	[Serializable]
	public partial class ParidadeCambial : esParidadeCambial
	{
		public ParidadeCambial()
		{

		}
	
		public ParidadeCambial(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ParidadeCambialMetadata.Meta();
			}
		}
		
		
		
		override protected esParidadeCambialQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParidadeCambialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ParidadeCambialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParidadeCambialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ParidadeCambialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ParidadeCambialQuery query;
	}



	[Serializable]
	public partial class ParidadeCambialQuery : esParidadeCambialQuery
	{
		public ParidadeCambialQuery()
		{

		}		
		
		public ParidadeCambialQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ParidadeCambialMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ParidadeCambialMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ParidadeCambialMetadata.ColumnNames.IdParidade, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParidadeCambialMetadata.PropertyNames.IdParidade;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParidadeCambialMetadata.PropertyNames.IdMoedaNumerador;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParidadeCambialMetadata.PropertyNames.IdMoedaDenominador;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParidadeCambialMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ParidadeCambialMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParidadeCambialMetadata.ColumnNames.IdIndice, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ParidadeCambialMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ParidadeCambialMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdParidade = "IdParidade";
			 public const string IdMoedaNumerador = "IdMoedaNumerador";
			 public const string IdMoedaDenominador = "IdMoedaDenominador";
			 public const string Descricao = "Descricao";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdParidade = "IdParidade";
			 public const string IdMoedaNumerador = "IdMoedaNumerador";
			 public const string IdMoedaDenominador = "IdMoedaDenominador";
			 public const string Descricao = "Descricao";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ParidadeCambialMetadata))
			{
				if(ParidadeCambialMetadata.mapDelegates == null)
				{
					ParidadeCambialMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ParidadeCambialMetadata.meta == null)
				{
					ParidadeCambialMetadata.meta = new ParidadeCambialMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdParidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMoedaNumerador", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdMoedaDenominador", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "ParidadeCambial";
				meta.Destination = "ParidadeCambial";
				
				meta.spInsert = "proc_ParidadeCambialInsert";				
				meta.spUpdate = "proc_ParidadeCambialUpdate";		
				meta.spDelete = "proc_ParidadeCambialDelete";
				meta.spLoadAll = "proc_ParidadeCambialLoadAll";
				meta.spLoadByPrimaryKey = "proc_ParidadeCambialLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ParidadeCambialMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
