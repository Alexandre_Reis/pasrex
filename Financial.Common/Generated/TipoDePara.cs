/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/11/2014 15:27:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common
{

	[Serializable]
	abstract public class esTipoDeParaCollection : esEntityCollection
	{
		public esTipoDeParaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoDeParaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoDeParaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoDeParaQuery);
		}
		#endregion
		
		virtual public TipoDePara DetachEntity(TipoDePara entity)
		{
			return base.DetachEntity(entity) as TipoDePara;
		}
		
		virtual public TipoDePara AttachEntity(TipoDePara entity)
		{
			return base.AttachEntity(entity) as TipoDePara;
		}
		
		virtual public void Combine(TipoDeParaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoDePara this[int index]
		{
			get
			{
				return base[index] as TipoDePara;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoDePara);
		}
	}



	[Serializable]
	abstract public class esTipoDePara : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoDeParaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoDePara()
		{

		}

		public esTipoDePara(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTipoDePara)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoDePara);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoDePara);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTipoDePara)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoDePara);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoDePara);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTipoDePara)
		{
			esTipoDeParaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTipoDePara == idTipoDePara);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTipoDePara)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTipoDePara",idTipoDePara);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTipoDePara": this.str.IdTipoDePara = (string)value; break;							
						case "TipoCampo": this.str.TipoCampo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "EnumTipoDePara": this.str.EnumTipoDePara = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTipoDePara":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoDePara = (System.Int32?)value;
							break;
						
						case "TipoCampo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCampo = (System.Int32?)value;
							break;
						
						case "EnumTipoDePara":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.EnumTipoDePara = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoDePara.IdTipoDePara
		/// </summary>
		virtual public System.Int32? IdTipoDePara
		{
			get
			{
				return base.GetSystemInt32(TipoDeParaMetadata.ColumnNames.IdTipoDePara);
			}
			
			set
			{
				base.SetSystemInt32(TipoDeParaMetadata.ColumnNames.IdTipoDePara, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoDePara.TipoCampo
		/// </summary>
		virtual public System.Int32? TipoCampo
		{
			get
			{
				return base.GetSystemInt32(TipoDeParaMetadata.ColumnNames.TipoCampo);
			}
			
			set
			{
				base.SetSystemInt32(TipoDeParaMetadata.ColumnNames.TipoCampo, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoDePara.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TipoDeParaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TipoDeParaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoDePara.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(TipoDeParaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(TipoDeParaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoDePara.EnumTipoDePara
		/// </summary>
		virtual public System.Int32? EnumTipoDePara
		{
			get
			{
				return base.GetSystemInt32(TipoDeParaMetadata.ColumnNames.EnumTipoDePara);
			}
			
			set
			{
				base.SetSystemInt32(TipoDeParaMetadata.ColumnNames.EnumTipoDePara, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoDePara entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTipoDePara
			{
				get
				{
					System.Int32? data = entity.IdTipoDePara;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoDePara = null;
					else entity.IdTipoDePara = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCampo
			{
				get
				{
					System.Int32? data = entity.TipoCampo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCampo = null;
					else entity.TipoCampo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String EnumTipoDePara
			{
				get
				{
					System.Int32? data = entity.EnumTipoDePara;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EnumTipoDePara = null;
					else entity.EnumTipoDePara = Convert.ToInt32(value);
				}
			}
			

			private esTipoDePara entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoDeParaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoDePara can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoDePara : esTipoDePara
	{

				
		#region DeParaCollectionByIdTipoDePara - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - DePara_TipoDePara_FK
		/// </summary>

		[XmlIgnore]
		public DeParaCollection DeParaCollectionByIdTipoDePara
		{
			get
			{
				if(this._DeParaCollectionByIdTipoDePara == null)
				{
					this._DeParaCollectionByIdTipoDePara = new DeParaCollection();
					this._DeParaCollectionByIdTipoDePara.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DeParaCollectionByIdTipoDePara", this._DeParaCollectionByIdTipoDePara);
				
					if(this.IdTipoDePara != null)
					{
						this._DeParaCollectionByIdTipoDePara.Query.Where(this._DeParaCollectionByIdTipoDePara.Query.IdTipoDePara == this.IdTipoDePara);
						this._DeParaCollectionByIdTipoDePara.Query.Load();

						// Auto-hookup Foreign Keys
						this._DeParaCollectionByIdTipoDePara.fks.Add(DeParaMetadata.ColumnNames.IdTipoDePara, this.IdTipoDePara);
					}
				}

				return this._DeParaCollectionByIdTipoDePara;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DeParaCollectionByIdTipoDePara != null) 
				{ 
					this.RemovePostSave("DeParaCollectionByIdTipoDePara"); 
					this._DeParaCollectionByIdTipoDePara = null;
					
				} 
			} 			
		}

		private DeParaCollection _DeParaCollectionByIdTipoDePara;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "DeParaCollectionByIdTipoDePara", typeof(DeParaCollection), new DePara()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._DeParaCollectionByIdTipoDePara != null)
			{
				foreach(DePara obj in this._DeParaCollectionByIdTipoDePara)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTipoDePara = this.IdTipoDePara;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoDeParaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoDeParaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTipoDePara
		{
			get
			{
				return new esQueryItem(this, TipoDeParaMetadata.ColumnNames.IdTipoDePara, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCampo
		{
			get
			{
				return new esQueryItem(this, TipoDeParaMetadata.ColumnNames.TipoCampo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TipoDeParaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, TipoDeParaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem EnumTipoDePara
		{
			get
			{
				return new esQueryItem(this, TipoDeParaMetadata.ColumnNames.EnumTipoDePara, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoDeParaCollection")]
	public partial class TipoDeParaCollection : esTipoDeParaCollection, IEnumerable<TipoDePara>
	{
		public TipoDeParaCollection()
		{

		}
		
		public static implicit operator List<TipoDePara>(TipoDeParaCollection coll)
		{
			List<TipoDePara> list = new List<TipoDePara>();
			
			foreach (TipoDePara emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoDeParaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoDeParaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoDePara(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoDePara();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoDeParaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoDeParaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoDeParaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoDePara AddNew()
		{
			TipoDePara entity = base.AddNewEntity() as TipoDePara;
			
			return entity;
		}

		public TipoDePara FindByPrimaryKey(System.Int32 idTipoDePara)
		{
			return base.FindByPrimaryKey(idTipoDePara) as TipoDePara;
		}


		#region IEnumerable<TipoDePara> Members

		IEnumerator<TipoDePara> IEnumerable<TipoDePara>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoDePara;
			}
		}

		#endregion
		
		private TipoDeParaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoDePara' table
	/// </summary>

	[Serializable]
	public partial class TipoDePara : esTipoDePara
	{
		public TipoDePara()
		{

		}
	
		public TipoDePara(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoDeParaMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoDeParaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoDeParaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoDeParaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoDeParaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoDeParaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoDeParaQuery query;
	}



	[Serializable]
	public partial class TipoDeParaQuery : esTipoDeParaQuery
	{
		public TipoDeParaQuery()
		{

		}		
		
		public TipoDeParaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoDeParaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoDeParaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoDeParaMetadata.ColumnNames.IdTipoDePara, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoDeParaMetadata.PropertyNames.IdTipoDePara;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoDeParaMetadata.ColumnNames.TipoCampo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoDeParaMetadata.PropertyNames.TipoCampo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoDeParaMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoDeParaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoDeParaMetadata.ColumnNames.Observacao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoDeParaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoDeParaMetadata.ColumnNames.EnumTipoDePara, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoDeParaMetadata.PropertyNames.EnumTipoDePara;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoDeParaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTipoDePara = "IdTipoDePara";
			 public const string TipoCampo = "TipoCampo";
			 public const string Descricao = "Descricao";
			 public const string Observacao = "Observacao";
			 public const string EnumTipoDePara = "EnumTipoDePara";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTipoDePara = "IdTipoDePara";
			 public const string TipoCampo = "TipoCampo";
			 public const string Descricao = "Descricao";
			 public const string Observacao = "Observacao";
			 public const string EnumTipoDePara = "EnumTipoDePara";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoDeParaMetadata))
			{
				if(TipoDeParaMetadata.mapDelegates == null)
				{
					TipoDeParaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoDeParaMetadata.meta == null)
				{
					TipoDeParaMetadata.meta = new TipoDeParaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTipoDePara", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCampo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EnumTipoDePara", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TipoDePara";
				meta.Destination = "TipoDePara";
				
				meta.spInsert = "proc_TipoDeParaInsert";				
				meta.spUpdate = "proc_TipoDeParaUpdate";		
				meta.spDelete = "proc_TipoDeParaDelete";
				meta.spLoadAll = "proc_TipoDeParaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoDeParaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoDeParaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
