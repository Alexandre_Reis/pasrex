/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esGrauRiscoCollection : esEntityCollection
	{
		public esGrauRiscoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrauRiscoCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrauRiscoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrauRiscoQuery);
		}
		#endregion
		
		virtual public GrauRisco DetachEntity(GrauRisco entity)
		{
			return base.DetachEntity(entity) as GrauRisco;
		}
		
		virtual public GrauRisco AttachEntity(GrauRisco entity)
		{
			return base.AttachEntity(entity) as GrauRisco;
		}
		
		virtual public void Combine(GrauRiscoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrauRisco this[int index]
		{
			get
			{
				return base[index] as GrauRisco;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrauRisco);
		}
	}



	[Serializable]
	abstract public class esGrauRisco : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrauRiscoQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrauRisco()
		{

		}

		public esGrauRisco(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrauRisco)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrauRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrauRisco);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrauRisco)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrauRiscoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrauRisco == idGrauRisco);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrauRisco)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrauRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrauRisco);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrauRisco)
		{
			esGrauRiscoQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrauRisco == idGrauRisco);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrauRisco)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrauRisco",idGrauRisco);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrauRisco": this.str.IdGrauRisco = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrauRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrauRisco = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrauRisco.IdGrauRisco
		/// </summary>
		virtual public System.Int32? IdGrauRisco
		{
			get
			{
				return base.GetSystemInt32(GrauRiscoMetadata.ColumnNames.IdGrauRisco);
			}
			
			set
			{
				base.SetSystemInt32(GrauRiscoMetadata.ColumnNames.IdGrauRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to GrauRisco.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(GrauRiscoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(GrauRiscoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrauRisco entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrauRisco
			{
				get
				{
					System.Int32? data = entity.IdGrauRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrauRisco = null;
					else entity.IdGrauRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esGrauRisco entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrauRiscoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrauRisco can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrauRisco : esGrauRisco
	{

				
		#region CarteiraCollectionByIdGrauRisco - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - GrauRisco_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdGrauRisco
		{
			get
			{
				if(this._CarteiraCollectionByIdGrauRisco == null)
				{
					this._CarteiraCollectionByIdGrauRisco = new CarteiraCollection();
					this._CarteiraCollectionByIdGrauRisco.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdGrauRisco", this._CarteiraCollectionByIdGrauRisco);
				
					if(this.IdGrauRisco != null)
					{
						this._CarteiraCollectionByIdGrauRisco.Query.Where(this._CarteiraCollectionByIdGrauRisco.Query.IdGrauRisco == this.IdGrauRisco);
						this._CarteiraCollectionByIdGrauRisco.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdGrauRisco.fks.Add(CarteiraMetadata.ColumnNames.IdGrauRisco, this.IdGrauRisco);
					}
				}

				return this._CarteiraCollectionByIdGrauRisco;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdGrauRisco != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdGrauRisco"); 
					this._CarteiraCollectionByIdGrauRisco = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdGrauRisco;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdGrauRisco", typeof(CarteiraCollection), new Carteira()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CarteiraCollectionByIdGrauRisco != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdGrauRisco)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrauRisco = this.IdGrauRisco;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrauRiscoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrauRiscoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrauRisco
		{
			get
			{
				return new esQueryItem(this, GrauRiscoMetadata.ColumnNames.IdGrauRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, GrauRiscoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrauRiscoCollection")]
	public partial class GrauRiscoCollection : esGrauRiscoCollection, IEnumerable<GrauRisco>
	{
		public GrauRiscoCollection()
		{

		}
		
		public static implicit operator List<GrauRisco>(GrauRiscoCollection coll)
		{
			List<GrauRisco> list = new List<GrauRisco>();
			
			foreach (GrauRisco emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrauRiscoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrauRiscoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrauRisco(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrauRisco();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrauRiscoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrauRiscoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrauRiscoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrauRisco AddNew()
		{
			GrauRisco entity = base.AddNewEntity() as GrauRisco;
			
			return entity;
		}

		public GrauRisco FindByPrimaryKey(System.Int32 idGrauRisco)
		{
			return base.FindByPrimaryKey(idGrauRisco) as GrauRisco;
		}


		#region IEnumerable<GrauRisco> Members

		IEnumerator<GrauRisco> IEnumerable<GrauRisco>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrauRisco;
			}
		}

		#endregion
		
		private GrauRiscoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrauRisco' table
	/// </summary>

	[Serializable]
	public partial class GrauRisco : esGrauRisco
	{
		public GrauRisco()
		{

		}
	
		public GrauRisco(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrauRiscoMetadata.Meta();
			}
		}
		
		
		
		override protected esGrauRiscoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrauRiscoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrauRiscoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrauRiscoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrauRiscoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrauRiscoQuery query;
	}



	[Serializable]
	public partial class GrauRiscoQuery : esGrauRiscoQuery
	{
		public GrauRiscoQuery()
		{

		}		
		
		public GrauRiscoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrauRiscoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrauRiscoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrauRiscoMetadata.ColumnNames.IdGrauRisco, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrauRiscoMetadata.PropertyNames.IdGrauRisco;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrauRiscoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrauRiscoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrauRiscoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrauRisco = "IdGrauRisco";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrauRisco = "IdGrauRisco";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrauRiscoMetadata))
			{
				if(GrauRiscoMetadata.mapDelegates == null)
				{
					GrauRiscoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrauRiscoMetadata.meta == null)
				{
					GrauRiscoMetadata.meta = new GrauRiscoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrauRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GrauRisco";
				meta.Destination = "GrauRisco";
				
				meta.spInsert = "proc_GrauRiscoInsert";				
				meta.spUpdate = "proc_GrauRiscoUpdate";		
				meta.spDelete = "proc_GrauRiscoDelete";
				meta.spLoadAll = "proc_GrauRiscoLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrauRiscoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrauRiscoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
