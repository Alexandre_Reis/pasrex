/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/08/2014 16:39:18
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esDataFiscalCollection : esEntityCollection
	{
		public esDataFiscalCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DataFiscalCollection";
		}

		#region Query Logic
		protected void InitQuery(esDataFiscalQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDataFiscalQuery);
		}
		#endregion
		
		virtual public DataFiscal DetachEntity(DataFiscal entity)
		{
			return base.DetachEntity(entity) as DataFiscal;
		}
		
		virtual public DataFiscal AttachEntity(DataFiscal entity)
		{
			return base.AttachEntity(entity) as DataFiscal;
		}
		
		virtual public void Combine(DataFiscalCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DataFiscal this[int index]
		{
			get
			{
				return base[index] as DataFiscal;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DataFiscal);
		}
	}



	[Serializable]
	abstract public class esDataFiscal : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDataFiscalQuery GetDynamicQuery()
		{
			return null;
		}

		public esDataFiscal()
		{

		}

		public esDataFiscal(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDiaFiscal, idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(idDiaFiscal, idLocal);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esDataFiscalQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdDiaFiscal == idDiaFiscal, query.IdLocal == idLocal);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDiaFiscal, idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(idDiaFiscal, idLocal);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			esDataFiscalQuery query = this.GetDynamicQuery();
			query.Where(query.IdDiaFiscal == idDiaFiscal, query.IdLocal == idLocal);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDiaFiscal",idDiaFiscal);			parms.Add("IdLocal",idLocal);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDiaFiscal": this.str.IdDiaFiscal = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDiaFiscal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDiaFiscal = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocal = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DataFiscal.IdDiaFiscal
		/// </summary>
		virtual public System.Int32? IdDiaFiscal
		{
			get
			{
				return base.GetSystemInt32(DataFiscalMetadata.ColumnNames.IdDiaFiscal);
			}
			
			set
			{
				base.SetSystemInt32(DataFiscalMetadata.ColumnNames.IdDiaFiscal, value);
			}
		}
		
		/// <summary>
		/// Maps to DataFiscal.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(DataFiscalMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(DataFiscalMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to DataFiscal.IdLocal
		/// </summary>
		virtual public System.Int32? IdLocal
		{
			get
			{
				return base.GetSystemInt32(DataFiscalMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt32(DataFiscalMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDataFiscal entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDiaFiscal
			{
				get
				{
					System.Int32? data = entity.IdDiaFiscal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDiaFiscal = null;
					else entity.IdDiaFiscal = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int32? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt32(value);
				}
			}
			

			private esDataFiscal entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDataFiscalQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDataFiscal can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DataFiscal : esDataFiscal
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDataFiscalQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DataFiscalMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDiaFiscal
		{
			get
			{
				return new esQueryItem(this, DataFiscalMetadata.ColumnNames.IdDiaFiscal, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, DataFiscalMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, DataFiscalMetadata.ColumnNames.IdLocal, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DataFiscalCollection")]
	public partial class DataFiscalCollection : esDataFiscalCollection, IEnumerable<DataFiscal>
	{
		public DataFiscalCollection()
		{

		}
		
		public static implicit operator List<DataFiscal>(DataFiscalCollection coll)
		{
			List<DataFiscal> list = new List<DataFiscal>();
			
			foreach (DataFiscal emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DataFiscalMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DataFiscalQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DataFiscal(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DataFiscal();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DataFiscalQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DataFiscalQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DataFiscalQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DataFiscal AddNew()
		{
			DataFiscal entity = base.AddNewEntity() as DataFiscal;
			
			return entity;
		}

		public DataFiscal FindByPrimaryKey(System.Int32 idDiaFiscal, System.Int32 idLocal)
		{
			return base.FindByPrimaryKey(idDiaFiscal, idLocal) as DataFiscal;
		}


		#region IEnumerable<DataFiscal> Members

		IEnumerator<DataFiscal> IEnumerable<DataFiscal>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DataFiscal;
			}
		}

		#endregion
		
		private DataFiscalQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DataFiscal' table
	/// </summary>

	[Serializable]
	public partial class DataFiscal : esDataFiscal
	{
		public DataFiscal()
		{

		}
	
		public DataFiscal(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DataFiscalMetadata.Meta();
			}
		}
		
		
		
		override protected esDataFiscalQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DataFiscalQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DataFiscalQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DataFiscalQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DataFiscalQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DataFiscalQuery query;
	}



	[Serializable]
	public partial class DataFiscalQuery : esDataFiscalQuery
	{
		public DataFiscalQuery()
		{

		}		
		
		public DataFiscalQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DataFiscalMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DataFiscalMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DataFiscalMetadata.ColumnNames.IdDiaFiscal, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DataFiscalMetadata.PropertyNames.IdDiaFiscal;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DataFiscalMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DataFiscalMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DataFiscalMetadata.ColumnNames.IdLocal, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DataFiscalMetadata.PropertyNames.IdLocal;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DataFiscalMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDiaFiscal = "IdDiaFiscal";
			 public const string Data = "Data";
			 public const string IdLocal = "IdLocal";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDiaFiscal = "IdDiaFiscal";
			 public const string Data = "Data";
			 public const string IdLocal = "IdLocal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DataFiscalMetadata))
			{
				if(DataFiscalMetadata.mapDelegates == null)
				{
					DataFiscalMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DataFiscalMetadata.meta == null)
				{
					DataFiscalMetadata.meta = new DataFiscalMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDiaFiscal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdLocal", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "DataFiscal";
				meta.Destination = "DataFiscal";
				
				meta.spInsert = "proc_DataFiscalInsert";				
				meta.spUpdate = "proc_DataFiscalUpdate";		
				meta.spDelete = "proc_DataFiscalDelete";
				meta.spLoadAll = "proc_DataFiscalLoadAll";
				meta.spLoadByPrimaryKey = "proc_DataFiscalLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DataFiscalMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
