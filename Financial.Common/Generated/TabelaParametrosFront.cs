/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esTabelaParametrosFrontCollection : esEntityCollection
	{
		public esTabelaParametrosFrontCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaParametrosFrontCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaParametrosFrontQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaParametrosFrontQuery);
		}
		#endregion
		
		virtual public TabelaParametrosFront DetachEntity(TabelaParametrosFront entity)
		{
			return base.DetachEntity(entity) as TabelaParametrosFront;
		}
		
		virtual public TabelaParametrosFront AttachEntity(TabelaParametrosFront entity)
		{
			return base.AttachEntity(entity) as TabelaParametrosFront;
		}
		
		virtual public void Combine(TabelaParametrosFrontCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaParametrosFront this[int index]
		{
			get
			{
				return base[index] as TabelaParametrosFront;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaParametrosFront);
		}
	}



	[Serializable]
	abstract public class esTabelaParametrosFront : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaParametrosFrontQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaParametrosFront()
		{

		}

		public esTabelaParametrosFront(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaParametrosFrontQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaParametrosFrontQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "ObservacaoFundo": this.str.ObservacaoFundo = (string)value; break;							
						case "ObservacaoCotista": this.str.ObservacaoCotista = (string)value; break;							
						case "ObservacaoRendaFixa": this.str.ObservacaoRendaFixa = (string)value; break;							
						case "HorarioFimFundo": this.str.HorarioFimFundo = (string)value; break;							
						case "HorarioFimCotista": this.str.HorarioFimCotista = (string)value; break;							
						case "HorarioFimRendaFixa": this.str.HorarioFimRendaFixa = (string)value; break;							
						case "EmailFundo": this.str.EmailFundo = (string)value; break;							
						case "EmailCotista": this.str.EmailCotista = (string)value; break;							
						case "EmailRendaFixa": this.str.EmailRendaFixa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "HorarioFimFundo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioFimFundo = (System.DateTime?)value;
							break;
						
						case "HorarioFimCotista":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioFimCotista = (System.DateTime?)value;
							break;
						
						case "HorarioFimRendaFixa":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioFimRendaFixa = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaParametrosFront.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaParametrosFrontMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaParametrosFrontMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.ObservacaoFundo
		/// </summary>
		virtual public System.String ObservacaoFundo
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoFundo);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.ObservacaoCotista
		/// </summary>
		virtual public System.String ObservacaoCotista
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoCotista);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.ObservacaoRendaFixa
		/// </summary>
		virtual public System.String ObservacaoRendaFixa
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoRendaFixa);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoRendaFixa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.HorarioFimFundo
		/// </summary>
		virtual public System.DateTime? HorarioFimFundo
		{
			get
			{
				return base.GetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimFundo);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.HorarioFimCotista
		/// </summary>
		virtual public System.DateTime? HorarioFimCotista
		{
			get
			{
				return base.GetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimCotista);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.HorarioFimRendaFixa
		/// </summary>
		virtual public System.DateTime? HorarioFimRendaFixa
		{
			get
			{
				return base.GetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimRendaFixa);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimRendaFixa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.EmailFundo
		/// </summary>
		virtual public System.String EmailFundo
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailFundo);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.EmailCotista
		/// </summary>
		virtual public System.String EmailCotista
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailCotista);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaParametrosFront.EmailRendaFixa
		/// </summary>
		virtual public System.String EmailRendaFixa
		{
			get
			{
				return base.GetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailRendaFixa);
			}
			
			set
			{
				base.SetSystemString(TabelaParametrosFrontMetadata.ColumnNames.EmailRendaFixa, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaParametrosFront entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String ObservacaoFundo
			{
				get
				{
					System.String data = entity.ObservacaoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ObservacaoFundo = null;
					else entity.ObservacaoFundo = Convert.ToString(value);
				}
			}
				
			public System.String ObservacaoCotista
			{
				get
				{
					System.String data = entity.ObservacaoCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ObservacaoCotista = null;
					else entity.ObservacaoCotista = Convert.ToString(value);
				}
			}
				
			public System.String ObservacaoRendaFixa
			{
				get
				{
					System.String data = entity.ObservacaoRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ObservacaoRendaFixa = null;
					else entity.ObservacaoRendaFixa = Convert.ToString(value);
				}
			}
				
			public System.String HorarioFimFundo
			{
				get
				{
					System.DateTime? data = entity.HorarioFimFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioFimFundo = null;
					else entity.HorarioFimFundo = Convert.ToDateTime(value);
				}
			}
				
			public System.String HorarioFimCotista
			{
				get
				{
					System.DateTime? data = entity.HorarioFimCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioFimCotista = null;
					else entity.HorarioFimCotista = Convert.ToDateTime(value);
				}
			}
				
			public System.String HorarioFimRendaFixa
			{
				get
				{
					System.DateTime? data = entity.HorarioFimRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioFimRendaFixa = null;
					else entity.HorarioFimRendaFixa = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmailFundo
			{
				get
				{
					System.String data = entity.EmailFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmailFundo = null;
					else entity.EmailFundo = Convert.ToString(value);
				}
			}
				
			public System.String EmailCotista
			{
				get
				{
					System.String data = entity.EmailCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmailCotista = null;
					else entity.EmailCotista = Convert.ToString(value);
				}
			}
				
			public System.String EmailRendaFixa
			{
				get
				{
					System.String data = entity.EmailRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmailRendaFixa = null;
					else entity.EmailRendaFixa = Convert.ToString(value);
				}
			}
			

			private esTabelaParametrosFront entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaParametrosFrontQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaParametrosFront can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaParametrosFront : esTabelaParametrosFront
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaParametrosFrontQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaParametrosFrontMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ObservacaoFundo
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.ObservacaoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem ObservacaoCotista
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.ObservacaoCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem ObservacaoRendaFixa
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.ObservacaoRendaFixa, esSystemType.String);
			}
		} 
		
		public esQueryItem HorarioFimFundo
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.HorarioFimFundo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HorarioFimCotista
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.HorarioFimCotista, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HorarioFimRendaFixa
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.HorarioFimRendaFixa, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem EmailFundo
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.EmailFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem EmailCotista
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.EmailCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem EmailRendaFixa
		{
			get
			{
				return new esQueryItem(this, TabelaParametrosFrontMetadata.ColumnNames.EmailRendaFixa, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaParametrosFrontCollection")]
	public partial class TabelaParametrosFrontCollection : esTabelaParametrosFrontCollection, IEnumerable<TabelaParametrosFront>
	{
		public TabelaParametrosFrontCollection()
		{

		}
		
		public static implicit operator List<TabelaParametrosFront>(TabelaParametrosFrontCollection coll)
		{
			List<TabelaParametrosFront> list = new List<TabelaParametrosFront>();
			
			foreach (TabelaParametrosFront emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaParametrosFrontMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaParametrosFrontQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaParametrosFront(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaParametrosFront();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaParametrosFrontQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaParametrosFrontQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaParametrosFrontQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaParametrosFront AddNew()
		{
			TabelaParametrosFront entity = base.AddNewEntity() as TabelaParametrosFront;
			
			return entity;
		}

		public TabelaParametrosFront FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaParametrosFront;
		}


		#region IEnumerable<TabelaParametrosFront> Members

		IEnumerator<TabelaParametrosFront> IEnumerable<TabelaParametrosFront>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaParametrosFront;
			}
		}

		#endregion
		
		private TabelaParametrosFrontQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaParametrosFront' table
	/// </summary>

	[Serializable]
	public partial class TabelaParametrosFront : esTabelaParametrosFront
	{
		public TabelaParametrosFront()
		{

		}
	
		public TabelaParametrosFront(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaParametrosFrontMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaParametrosFrontQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaParametrosFrontQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaParametrosFrontQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaParametrosFrontQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaParametrosFrontQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaParametrosFrontQuery query;
	}



	[Serializable]
	public partial class TabelaParametrosFrontQuery : esTabelaParametrosFrontQuery
	{
		public TabelaParametrosFrontQuery()
		{

		}		
		
		public TabelaParametrosFrontQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaParametrosFrontMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaParametrosFrontMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoFundo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.ObservacaoFundo;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoCotista, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.ObservacaoCotista;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.ObservacaoRendaFixa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.ObservacaoRendaFixa;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimFundo, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.HorarioFimFundo;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimCotista, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.HorarioFimCotista;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.HorarioFimRendaFixa, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.HorarioFimRendaFixa;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.EmailFundo, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.EmailFundo;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.EmailCotista, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.EmailCotista;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaParametrosFrontMetadata.ColumnNames.EmailRendaFixa, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaParametrosFrontMetadata.PropertyNames.EmailRendaFixa;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaParametrosFrontMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string ObservacaoFundo = "ObservacaoFundo";
			 public const string ObservacaoCotista = "ObservacaoCotista";
			 public const string ObservacaoRendaFixa = "ObservacaoRendaFixa";
			 public const string HorarioFimFundo = "HorarioFimFundo";
			 public const string HorarioFimCotista = "HorarioFimCotista";
			 public const string HorarioFimRendaFixa = "HorarioFimRendaFixa";
			 public const string EmailFundo = "EmailFundo";
			 public const string EmailCotista = "EmailCotista";
			 public const string EmailRendaFixa = "EmailRendaFixa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string ObservacaoFundo = "ObservacaoFundo";
			 public const string ObservacaoCotista = "ObservacaoCotista";
			 public const string ObservacaoRendaFixa = "ObservacaoRendaFixa";
			 public const string HorarioFimFundo = "HorarioFimFundo";
			 public const string HorarioFimCotista = "HorarioFimCotista";
			 public const string HorarioFimRendaFixa = "HorarioFimRendaFixa";
			 public const string EmailFundo = "EmailFundo";
			 public const string EmailCotista = "EmailCotista";
			 public const string EmailRendaFixa = "EmailRendaFixa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaParametrosFrontMetadata))
			{
				if(TabelaParametrosFrontMetadata.mapDelegates == null)
				{
					TabelaParametrosFrontMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaParametrosFrontMetadata.meta == null)
				{
					TabelaParametrosFrontMetadata.meta = new TabelaParametrosFrontMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ObservacaoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ObservacaoCotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ObservacaoRendaFixa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("HorarioFimFundo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("HorarioFimCotista", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("HorarioFimRendaFixa", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EmailFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EmailCotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EmailRendaFixa", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TabelaParametrosFront";
				meta.Destination = "TabelaParametrosFront";
				
				meta.spInsert = "proc_TabelaParametrosFrontInsert";				
				meta.spUpdate = "proc_TabelaParametrosFrontUpdate";		
				meta.spDelete = "proc_TabelaParametrosFrontDelete";
				meta.spLoadAll = "proc_TabelaParametrosFrontLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaParametrosFrontLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaParametrosFrontMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
