/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/01/2015 16:19:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esAgenciaClassificadoraCollection : esEntityCollection
	{
		public esAgenciaClassificadoraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgenciaClassificadoraCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgenciaClassificadoraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgenciaClassificadoraQuery);
		}
		#endregion
		
		virtual public AgenciaClassificadora DetachEntity(AgenciaClassificadora entity)
		{
			return base.DetachEntity(entity) as AgenciaClassificadora;
		}
		
		virtual public AgenciaClassificadora AttachEntity(AgenciaClassificadora entity)
		{
			return base.AttachEntity(entity) as AgenciaClassificadora;
		}
		
		virtual public void Combine(AgenciaClassificadoraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgenciaClassificadora this[int index]
		{
			get
			{
				return base[index] as AgenciaClassificadora;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgenciaClassificadora);
		}
	}



	[Serializable]
	abstract public class esAgenciaClassificadora : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgenciaClassificadoraQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgenciaClassificadora()
		{

		}

		public esAgenciaClassificadora(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgenciaClassificadora)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenciaClassificadora);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenciaClassificadora);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgenciaClassificadora)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenciaClassificadora);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenciaClassificadora);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgenciaClassificadora)
		{
			esAgenciaClassificadoraQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgenciaClassificadora == idAgenciaClassificadora);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgenciaClassificadora)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgenciaClassificadora",idAgenciaClassificadora);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgenciaClassificadora": this.str.IdAgenciaClassificadora = (string)value; break;							
						case "CodigoAgencia": this.str.CodigoAgencia = (string)value; break;							
						case "DescricaoAgencia": this.str.DescricaoAgencia = (string)value; break;							
						case "Cnpj": this.str.Cnpj = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgenciaClassificadora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenciaClassificadora = (System.Int32?)value;
							break;
						
						case "CodigoAgencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoAgencia = (System.Int32?)value;
							break;
						
						case "CodigoExterno":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoExterno = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgenciaClassificadora.IdAgenciaClassificadora
		/// </summary>
		virtual public System.Int32? IdAgenciaClassificadora
		{
			get
			{
				return base.GetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.IdAgenciaClassificadora);
			}
			
			set
			{
				base.SetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.IdAgenciaClassificadora, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenciaClassificadora.CodigoAgencia
		/// </summary>
		virtual public System.Int32? CodigoAgencia
		{
			get
			{
				return base.GetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.CodigoAgencia);
			}
			
			set
			{
				base.SetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.CodigoAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenciaClassificadora.DescricaoAgencia
		/// </summary>
		virtual public System.String DescricaoAgencia
		{
			get
			{
				return base.GetSystemString(AgenciaClassificadoraMetadata.ColumnNames.DescricaoAgencia);
			}
			
			set
			{
				base.SetSystemString(AgenciaClassificadoraMetadata.ColumnNames.DescricaoAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenciaClassificadora.CNPJ
		/// </summary>
		virtual public System.String Cnpj
		{
			get
			{
				return base.GetSystemString(AgenciaClassificadoraMetadata.ColumnNames.Cnpj);
			}
			
			set
			{
				base.SetSystemString(AgenciaClassificadoraMetadata.ColumnNames.Cnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenciaClassificadora.CodigoExterno
		/// </summary>
		virtual public System.Int32? CodigoExterno
		{
			get
			{
				return base.GetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemInt32(AgenciaClassificadoraMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgenciaClassificadora entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgenciaClassificadora
			{
				get
				{
					System.Int32? data = entity.IdAgenciaClassificadora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenciaClassificadora = null;
					else entity.IdAgenciaClassificadora = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAgencia
			{
				get
				{
					System.Int32? data = entity.CodigoAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAgencia = null;
					else entity.CodigoAgencia = Convert.ToInt32(value);
				}
			}
				
			public System.String DescricaoAgencia
			{
				get
				{
					System.String data = entity.DescricaoAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoAgencia = null;
					else entity.DescricaoAgencia = Convert.ToString(value);
				}
			}
				
			public System.String Cnpj
			{
				get
				{
					System.String data = entity.Cnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cnpj = null;
					else entity.Cnpj = Convert.ToString(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.Int32? data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToInt32(value);
				}
			}
			

			private esAgenciaClassificadora entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgenciaClassificadoraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgenciaClassificadora can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgenciaClassificadora : esAgenciaClassificadora
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgenciaClassificadoraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgenciaClassificadoraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgenciaClassificadora
		{
			get
			{
				return new esQueryItem(this, AgenciaClassificadoraMetadata.ColumnNames.IdAgenciaClassificadora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAgencia
		{
			get
			{
				return new esQueryItem(this, AgenciaClassificadoraMetadata.ColumnNames.CodigoAgencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DescricaoAgencia
		{
			get
			{
				return new esQueryItem(this, AgenciaClassificadoraMetadata.ColumnNames.DescricaoAgencia, esSystemType.String);
			}
		} 
		
		public esQueryItem Cnpj
		{
			get
			{
				return new esQueryItem(this, AgenciaClassificadoraMetadata.ColumnNames.Cnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, AgenciaClassificadoraMetadata.ColumnNames.CodigoExterno, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgenciaClassificadoraCollection")]
	public partial class AgenciaClassificadoraCollection : esAgenciaClassificadoraCollection, IEnumerable<AgenciaClassificadora>
	{
		public AgenciaClassificadoraCollection()
		{

		}
		
		public static implicit operator List<AgenciaClassificadora>(AgenciaClassificadoraCollection coll)
		{
			List<AgenciaClassificadora> list = new List<AgenciaClassificadora>();
			
			foreach (AgenciaClassificadora emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgenciaClassificadoraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenciaClassificadoraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgenciaClassificadora(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgenciaClassificadora();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgenciaClassificadoraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenciaClassificadoraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgenciaClassificadoraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgenciaClassificadora AddNew()
		{
			AgenciaClassificadora entity = base.AddNewEntity() as AgenciaClassificadora;
			
			return entity;
		}

		public AgenciaClassificadora FindByPrimaryKey(System.Int32 idAgenciaClassificadora)
		{
			return base.FindByPrimaryKey(idAgenciaClassificadora) as AgenciaClassificadora;
		}


		#region IEnumerable<AgenciaClassificadora> Members

		IEnumerator<AgenciaClassificadora> IEnumerable<AgenciaClassificadora>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgenciaClassificadora;
			}
		}

		#endregion
		
		private AgenciaClassificadoraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgenciaClassificadora' table
	/// </summary>

	[Serializable]
	public partial class AgenciaClassificadora : esAgenciaClassificadora
	{
		public AgenciaClassificadora()
		{

		}
	
		public AgenciaClassificadora(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgenciaClassificadoraMetadata.Meta();
			}
		}
		
		
		
		override protected esAgenciaClassificadoraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenciaClassificadoraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgenciaClassificadoraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenciaClassificadoraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgenciaClassificadoraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgenciaClassificadoraQuery query;
	}



	[Serializable]
	public partial class AgenciaClassificadoraQuery : esAgenciaClassificadoraQuery
	{
		public AgenciaClassificadoraQuery()
		{

		}		
		
		public AgenciaClassificadoraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgenciaClassificadoraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgenciaClassificadoraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgenciaClassificadoraMetadata.ColumnNames.IdAgenciaClassificadora, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenciaClassificadoraMetadata.PropertyNames.IdAgenciaClassificadora;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaClassificadoraMetadata.ColumnNames.CodigoAgencia, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenciaClassificadoraMetadata.PropertyNames.CodigoAgencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaClassificadoraMetadata.ColumnNames.DescricaoAgencia, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenciaClassificadoraMetadata.PropertyNames.DescricaoAgencia;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaClassificadoraMetadata.ColumnNames.Cnpj, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenciaClassificadoraMetadata.PropertyNames.Cnpj;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaClassificadoraMetadata.ColumnNames.CodigoExterno, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenciaClassificadoraMetadata.PropertyNames.CodigoExterno;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgenciaClassificadoraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string CodigoAgencia = "CodigoAgencia";
			 public const string DescricaoAgencia = "DescricaoAgencia";
			 public const string Cnpj = "CNPJ";
			 public const string CodigoExterno = "CodigoExterno";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string CodigoAgencia = "CodigoAgencia";
			 public const string DescricaoAgencia = "DescricaoAgencia";
			 public const string Cnpj = "Cnpj";
			 public const string CodigoExterno = "CodigoExterno";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgenciaClassificadoraMetadata))
			{
				if(AgenciaClassificadoraMetadata.mapDelegates == null)
				{
					AgenciaClassificadoraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgenciaClassificadoraMetadata.meta == null)
				{
					AgenciaClassificadoraMetadata.meta = new AgenciaClassificadoraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgenciaClassificadora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAgencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DescricaoAgencia", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CNPJ", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "AgenciaClassificadora";
				meta.Destination = "AgenciaClassificadora";
				
				meta.spInsert = "proc_AgenciaClassificadoraInsert";				
				meta.spUpdate = "proc_AgenciaClassificadoraUpdate";		
				meta.spDelete = "proc_AgenciaClassificadoraDelete";
				meta.spLoadAll = "proc_AgenciaClassificadoraLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgenciaClassificadoraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgenciaClassificadoraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
