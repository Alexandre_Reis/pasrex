/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























using Financial.Bolsa;

		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esLocalFeriadoCollection : esEntityCollection
	{
		public esLocalFeriadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LocalFeriadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esLocalFeriadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLocalFeriadoQuery);
		}
		#endregion
		
		virtual public LocalFeriado DetachEntity(LocalFeriado entity)
		{
			return base.DetachEntity(entity) as LocalFeriado;
		}
		
		virtual public LocalFeriado AttachEntity(LocalFeriado entity)
		{
			return base.AttachEntity(entity) as LocalFeriado;
		}
		
		virtual public void Combine(LocalFeriadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LocalFeriado this[int index]
		{
			get
			{
				return base[index] as LocalFeriado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LocalFeriado);
		}
	}



	[Serializable]
	abstract public class esLocalFeriado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLocalFeriadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esLocalFeriado()
		{

		}

		public esLocalFeriado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idLocal)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocal);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idLocal)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLocalFeriadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLocal == idLocal);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idLocal)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocal);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocal);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idLocal)
		{
			esLocalFeriadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdLocal == idLocal);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idLocal)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLocal",idLocal);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LocalFeriado.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(LocalFeriadoMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt16(LocalFeriadoMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalFeriado.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(LocalFeriadoMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(LocalFeriadoMetadata.ColumnNames.Nome, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLocalFeriado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
			

			private esLocalFeriado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLocalFeriadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLocalFeriado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LocalFeriado : esLocalFeriado
	{

				
		#region FeriadoCollectionByIdLocal - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - LocalFeriado_Feriado_FK1
		/// </summary>

		[XmlIgnore]
		public FeriadoCollection FeriadoCollectionByIdLocal
		{
			get
			{
				if(this._FeriadoCollectionByIdLocal == null)
				{
					this._FeriadoCollectionByIdLocal = new FeriadoCollection();
					this._FeriadoCollectionByIdLocal.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("FeriadoCollectionByIdLocal", this._FeriadoCollectionByIdLocal);
				
					if(this.IdLocal != null)
					{
						this._FeriadoCollectionByIdLocal.Query.Where(this._FeriadoCollectionByIdLocal.Query.IdLocal == this.IdLocal);
						this._FeriadoCollectionByIdLocal.Query.Load();

						// Auto-hookup Foreign Keys
						this._FeriadoCollectionByIdLocal.fks.Add(FeriadoMetadata.ColumnNames.IdLocal, this.IdLocal);
					}
				}

				return this._FeriadoCollectionByIdLocal;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._FeriadoCollectionByIdLocal != null) 
				{ 
					this.RemovePostSave("FeriadoCollectionByIdLocal"); 
					this._FeriadoCollectionByIdLocal = null;
					
				} 
			} 			
		}

		private FeriadoCollection _FeriadoCollectionByIdLocal;
		#endregion

				
		#region OperacaoBolsaCollectionByIdLocal - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - LocalFeriado_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByIdLocal
		{
			get
			{
				if(this._OperacaoBolsaCollectionByIdLocal == null)
				{
					this._OperacaoBolsaCollectionByIdLocal = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByIdLocal.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByIdLocal", this._OperacaoBolsaCollectionByIdLocal);
				
					if(this.IdLocal != null)
					{
						this._OperacaoBolsaCollectionByIdLocal.Query.Where(this._OperacaoBolsaCollectionByIdLocal.Query.IdLocal == this.IdLocal);
						this._OperacaoBolsaCollectionByIdLocal.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByIdLocal.fks.Add(OperacaoBolsaMetadata.ColumnNames.IdLocal, this.IdLocal);
					}
				}

				return this._OperacaoBolsaCollectionByIdLocal;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByIdLocal != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByIdLocal"); 
					this._OperacaoBolsaCollectionByIdLocal = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByIdLocal;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "FeriadoCollectionByIdLocal", typeof(FeriadoCollection), new Feriado()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByIdLocal", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._FeriadoCollectionByIdLocal != null)
			{
				foreach(Feriado obj in this._FeriadoCollectionByIdLocal)
				{
					if(obj.es.IsAdded)
					{
						obj.IdLocal = this.IdLocal;
					}
				}
			}
			if(this._OperacaoBolsaCollectionByIdLocal != null)
			{
				foreach(OperacaoBolsa obj in this._OperacaoBolsaCollectionByIdLocal)
				{
					if(obj.es.IsAdded)
					{
						obj.IdLocal = this.IdLocal;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLocalFeriadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LocalFeriadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, LocalFeriadoMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, LocalFeriadoMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LocalFeriadoCollection")]
	public partial class LocalFeriadoCollection : esLocalFeriadoCollection, IEnumerable<LocalFeriado>
	{
		public LocalFeriadoCollection()
		{

		}
		
		public static implicit operator List<LocalFeriado>(LocalFeriadoCollection coll)
		{
			List<LocalFeriado> list = new List<LocalFeriado>();
			
			foreach (LocalFeriado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LocalFeriadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalFeriadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LocalFeriado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LocalFeriado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LocalFeriadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalFeriadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LocalFeriadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LocalFeriado AddNew()
		{
			LocalFeriado entity = base.AddNewEntity() as LocalFeriado;
			
			return entity;
		}

		public LocalFeriado FindByPrimaryKey(System.Int16 idLocal)
		{
			return base.FindByPrimaryKey(idLocal) as LocalFeriado;
		}


		#region IEnumerable<LocalFeriado> Members

		IEnumerator<LocalFeriado> IEnumerable<LocalFeriado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LocalFeriado;
			}
		}

		#endregion
		
		private LocalFeriadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LocalFeriado' table
	/// </summary>

	[Serializable]
	public partial class LocalFeriado : esLocalFeriado
	{
		public LocalFeriado()
		{

		}
	
		public LocalFeriado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LocalFeriadoMetadata.Meta();
			}
		}
		
		
		
		override protected esLocalFeriadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalFeriadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LocalFeriadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalFeriadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LocalFeriadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LocalFeriadoQuery query;
	}



	[Serializable]
	public partial class LocalFeriadoQuery : esLocalFeriadoQuery
	{
		public LocalFeriadoQuery()
		{

		}		
		
		public LocalFeriadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LocalFeriadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LocalFeriadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LocalFeriadoMetadata.ColumnNames.IdLocal, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = LocalFeriadoMetadata.PropertyNames.IdLocal;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalFeriadoMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalFeriadoMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LocalFeriadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLocal = "IdLocal";
			 public const string Nome = "Nome";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLocal = "IdLocal";
			 public const string Nome = "Nome";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LocalFeriadoMetadata))
			{
				if(LocalFeriadoMetadata.mapDelegates == null)
				{
					LocalFeriadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LocalFeriadoMetadata.meta == null)
				{
					LocalFeriadoMetadata.meta = new LocalFeriadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LocalFeriado";
				meta.Destination = "LocalFeriado";
				
				meta.spInsert = "proc_LocalFeriadoInsert";				
				meta.spUpdate = "proc_LocalFeriadoUpdate";		
				meta.spDelete = "proc_LocalFeriadoDelete";
				meta.spLoadAll = "proc_LocalFeriadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_LocalFeriadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LocalFeriadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
