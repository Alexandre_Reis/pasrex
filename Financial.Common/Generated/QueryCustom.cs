/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 26/06/2015 19:02:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esQueryCustomCollection : esEntityCollection
	{
		public esQueryCustomCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "QueryCustomCollection";
		}

		#region Query Logic
		protected void InitQuery(esQueryCustomQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esQueryCustomQuery);
		}
		#endregion
		
		virtual public QueryCustom DetachEntity(QueryCustom entity)
		{
			return base.DetachEntity(entity) as QueryCustom;
		}
		
		virtual public QueryCustom AttachEntity(QueryCustom entity)
		{
			return base.AttachEntity(entity) as QueryCustom;
		}
		
		virtual public void Combine(QueryCustomCollection collection)
		{
			base.Combine(collection);
		}
		
		new public QueryCustom this[int index]
		{
			get
			{
				return base[index] as QueryCustom;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(QueryCustom);
		}
	}



	[Serializable]
	abstract public class esQueryCustom : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esQueryCustomQuery GetDynamicQuery()
		{
			return null;
		}

		public esQueryCustom()
		{

		}

		public esQueryCustom(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idQuery)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idQuery);
			else
				return LoadByPrimaryKeyStoredProcedure(idQuery);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idQuery)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idQuery);
			else
				return LoadByPrimaryKeyStoredProcedure(idQuery);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idQuery)
		{
			esQueryCustomQuery query = this.GetDynamicQuery();
			query.Where(query.IdQuery == idQuery);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idQuery)
		{
			esParameters parms = new esParameters();
			parms.Add("IdQuery",idQuery);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdQuery": this.str.IdQuery = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "QueryCustom": this.str.QueryCustom = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdQuery":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuery = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to QueryCustom.IdQuery
		/// </summary>
		virtual public System.Int32? IdQuery
		{
			get
			{
				return base.GetSystemInt32(QueryCustomMetadata.ColumnNames.IdQuery);
			}
			
			set
			{
				base.SetSystemInt32(QueryCustomMetadata.ColumnNames.IdQuery, value);
			}
		}
		
		/// <summary>
		/// Maps to QueryCustom.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(QueryCustomMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(QueryCustomMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to QueryCustom.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(QueryCustomMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(QueryCustomMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to QueryCustom.QueryCustom
		/// </summary>
		virtual public System.String QueryCustom
		{
			get
			{
				return base.GetSystemString(QueryCustomMetadata.ColumnNames.QueryCustom);
			}
			
			set
			{
				base.SetSystemString(QueryCustomMetadata.ColumnNames.QueryCustom, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esQueryCustom entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdQuery
			{
				get
				{
					System.Int32? data = entity.IdQuery;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuery = null;
					else entity.IdQuery = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String QueryCustom
			{
				get
				{
					System.String data = entity.QueryCustom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QueryCustom = null;
					else entity.QueryCustom = Convert.ToString(value);
				}
			}
			

			private esQueryCustom entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esQueryCustomQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esQueryCustom can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class QueryCustom : esQueryCustom
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esQueryCustomQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return QueryCustomMetadata.Meta();
			}
		}	
		

		public esQueryItem IdQuery
		{
			get
			{
				return new esQueryItem(this, QueryCustomMetadata.ColumnNames.IdQuery, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, QueryCustomMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, QueryCustomMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem QueryCustom
		{
			get
			{
				return new esQueryItem(this, QueryCustomMetadata.ColumnNames.QueryCustom, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("QueryCustomCollection")]
	public partial class QueryCustomCollection : esQueryCustomCollection, IEnumerable<QueryCustom>
	{
		public QueryCustomCollection()
		{

		}
		
		public static implicit operator List<QueryCustom>(QueryCustomCollection coll)
		{
			List<QueryCustom> list = new List<QueryCustom>();
			
			foreach (QueryCustom emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  QueryCustomMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new QueryCustomQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new QueryCustom(row);
		}

		override protected esEntity CreateEntity()
		{
			return new QueryCustom();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public QueryCustomQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new QueryCustomQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(QueryCustomQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public QueryCustom AddNew()
		{
			QueryCustom entity = base.AddNewEntity() as QueryCustom;
			
			return entity;
		}

		public QueryCustom FindByPrimaryKey(System.Int32 idQuery)
		{
			return base.FindByPrimaryKey(idQuery) as QueryCustom;
		}


		#region IEnumerable<QueryCustom> Members

		IEnumerator<QueryCustom> IEnumerable<QueryCustom>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as QueryCustom;
			}
		}

		#endregion
		
		private QueryCustomQuery query;
	}


	/// <summary>
	/// Encapsulates the 'QueryCustom' table
	/// </summary>

	[Serializable]
	public partial class QueryCustom : esQueryCustom
	{
		public QueryCustom()
		{

		}
	
		public QueryCustom(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return QueryCustomMetadata.Meta();
			}
		}
		
		
		
		override protected esQueryCustomQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new QueryCustomQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public QueryCustomQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new QueryCustomQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(QueryCustomQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private QueryCustomQuery query;
	}



	[Serializable]
	public partial class QueryCustomQuery : esQueryCustomQuery
	{
		public QueryCustomQuery()
		{

		}		
		
		public QueryCustomQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class QueryCustomMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected QueryCustomMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(QueryCustomMetadata.ColumnNames.IdQuery, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = QueryCustomMetadata.PropertyNames.IdQuery;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QueryCustomMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = QueryCustomMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QueryCustomMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = QueryCustomMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QueryCustomMetadata.ColumnNames.QueryCustom, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = QueryCustomMetadata.PropertyNames.QueryCustom;
			c.CharacterMaxLength = 8000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public QueryCustomMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdQuery = "IdQuery";
			 public const string Nome = "Nome";
			 public const string Descricao = "Descricao";
			 public const string QueryCustom = "QueryCustom";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdQuery = "IdQuery";
			 public const string Nome = "Nome";
			 public const string Descricao = "Descricao";
			 public const string QueryCustom = "QueryCustom";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(QueryCustomMetadata))
			{
				if(QueryCustomMetadata.mapDelegates == null)
				{
					QueryCustomMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (QueryCustomMetadata.meta == null)
				{
					QueryCustomMetadata.meta = new QueryCustomMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdQuery", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("QueryCustom", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "QueryCustom";
				meta.Destination = "QueryCustom";
				
				meta.spInsert = "proc_QueryCustomInsert";				
				meta.spUpdate = "proc_QueryCustomUpdate";		
				meta.spDelete = "proc_QueryCustomDelete";
				meta.spLoadAll = "proc_QueryCustomLoadAll";
				meta.spLoadByPrimaryKey = "proc_QueryCustomLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private QueryCustomMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
