/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/07/2015 18:05:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.CRM;

namespace Financial.Common
{

	[Serializable]
	abstract public class esDiretorCollection : esEntityCollection
	{
		public esDiretorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DiretorCollection";
		}

		#region Query Logic
		protected void InitQuery(esDiretorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDiretorQuery);
		}
		#endregion
		
		virtual public Diretor DetachEntity(Diretor entity)
		{
			return base.DetachEntity(entity) as Diretor;
		}
		
		virtual public Diretor AttachEntity(Diretor entity)
		{
			return base.AttachEntity(entity) as Diretor;
		}
		
		virtual public void Combine(DiretorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Diretor this[int index]
		{
			get
			{
				return base[index] as Diretor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Diretor);
		}
	}



	[Serializable]
	abstract public class esDiretor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDiretorQuery GetDynamicQuery()
		{
			return null;
		}

		public esDiretor()
		{

		}

		public esDiretor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDiretor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDiretor);
			else
				return LoadByPrimaryKeyStoredProcedure(idDiretor);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDiretor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDiretor);
			else
				return LoadByPrimaryKeyStoredProcedure(idDiretor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDiretor)
		{
			esDiretorQuery query = this.GetDynamicQuery();
			query.Where(query.IdDiretor == idDiretor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDiretor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDiretor",idDiretor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDiretor": this.str.IdDiretor = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDiretor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDiretor = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Diretor.IdDiretor
		/// </summary>
		virtual public System.Int32? IdDiretor
		{
			get
			{
				return base.GetSystemInt32(DiretorMetadata.ColumnNames.IdDiretor);
			}
			
			set
			{
				base.SetSystemInt32(DiretorMetadata.ColumnNames.IdDiretor, value);
			}
		}
		
		/// <summary>
		/// Maps to Diretor.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(DiretorMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(DiretorMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Diretor.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(DiretorMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(DiretorMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDiretor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDiretor
			{
				get
				{
					System.Int32? data = entity.IdDiretor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDiretor = null;
					else entity.IdDiretor = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
			

			private esDiretor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDiretorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDiretor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Diretor : esDiretor
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_Diretor_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDiretorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DiretorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDiretor
		{
			get
			{
				return new esQueryItem(this, DiretorMetadata.ColumnNames.IdDiretor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, DiretorMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, DiretorMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DiretorCollection")]
	public partial class DiretorCollection : esDiretorCollection, IEnumerable<Diretor>
	{
		public DiretorCollection()
		{

		}
		
		public static implicit operator List<Diretor>(DiretorCollection coll)
		{
			List<Diretor> list = new List<Diretor>();
			
			foreach (Diretor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DiretorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DiretorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Diretor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Diretor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DiretorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DiretorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DiretorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Diretor AddNew()
		{
			Diretor entity = base.AddNewEntity() as Diretor;
			
			return entity;
		}

		public Diretor FindByPrimaryKey(System.Int32 idDiretor)
		{
			return base.FindByPrimaryKey(idDiretor) as Diretor;
		}


		#region IEnumerable<Diretor> Members

		IEnumerator<Diretor> IEnumerable<Diretor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Diretor;
			}
		}

		#endregion
		
		private DiretorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Diretor' table
	/// </summary>

	[Serializable]
	public partial class Diretor : esDiretor
	{
		public Diretor()
		{

		}
	
		public Diretor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DiretorMetadata.Meta();
			}
		}
		
		
		
		override protected esDiretorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DiretorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DiretorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DiretorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DiretorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DiretorQuery query;
	}



	[Serializable]
	public partial class DiretorQuery : esDiretorQuery
	{
		public DiretorQuery()
		{

		}		
		
		public DiretorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DiretorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DiretorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DiretorMetadata.ColumnNames.IdDiretor, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DiretorMetadata.PropertyNames.IdDiretor;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DiretorMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = DiretorMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DiretorMetadata.ColumnNames.IdPessoa, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DiretorMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DiretorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDiretor = "IdDiretor";
			 public const string Nome = "Nome";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDiretor = "IdDiretor";
			 public const string Nome = "Nome";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DiretorMetadata))
			{
				if(DiretorMetadata.mapDelegates == null)
				{
					DiretorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DiretorMetadata.meta == null)
				{
					DiretorMetadata.meta = new DiretorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDiretor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Diretor";
				meta.Destination = "Diretor";
				
				meta.spInsert = "proc_DiretorInsert";				
				meta.spUpdate = "proc_DiretorUpdate";		
				meta.spDelete = "proc_DiretorDelete";
				meta.spLoadAll = "proc_DiretorLoadAll";
				meta.spLoadByPrimaryKey = "proc_DiretorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DiretorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
