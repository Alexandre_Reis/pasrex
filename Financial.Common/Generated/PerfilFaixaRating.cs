/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2015 15:32:52
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esPerfilFaixaRatingCollection : esEntityCollection
	{
		public esPerfilFaixaRatingCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilFaixaRatingCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilFaixaRatingQuery);
		}
		#endregion
		
		virtual public PerfilFaixaRating DetachEntity(PerfilFaixaRating entity)
		{
			return base.DetachEntity(entity) as PerfilFaixaRating;
		}
		
		virtual public PerfilFaixaRating AttachEntity(PerfilFaixaRating entity)
		{
			return base.AttachEntity(entity) as PerfilFaixaRating;
		}
		
		virtual public void Combine(PerfilFaixaRatingCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilFaixaRating this[int index]
		{
			get
			{
				return base[index] as PerfilFaixaRating;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilFaixaRating);
		}
	}



	[Serializable]
	abstract public class esPerfilFaixaRating : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilFaixaRatingQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilFaixaRating()
		{

		}

		public esPerfilFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilFaixaRating)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilFaixaRating);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilFaixaRating)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilFaixaRating);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilFaixaRating);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilFaixaRating)
		{
			esPerfilFaixaRatingQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilFaixaRating == idPerfilFaixaRating);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilFaixaRating)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilFaixaRating",idPerfilFaixaRating);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilFaixaRating": this.str.IdPerfilFaixaRating = (string)value; break;							
						case "CodigoPerfil": this.str.CodigoPerfil = (string)value; break;							
						case "DescricaoPerfil": this.str.DescricaoPerfil = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilFaixaRating":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilFaixaRating = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilFaixaRating.IdPerfilFaixaRating
		/// </summary>
		virtual public System.Int32? IdPerfilFaixaRating
		{
			get
			{
				return base.GetSystemInt32(PerfilFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating);
			}
			
			set
			{
				base.SetSystemInt32(PerfilFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilFaixaRating.CodigoPerfil
		/// </summary>
		virtual public System.String CodigoPerfil
		{
			get
			{
				return base.GetSystemString(PerfilFaixaRatingMetadata.ColumnNames.CodigoPerfil);
			}
			
			set
			{
				base.SetSystemString(PerfilFaixaRatingMetadata.ColumnNames.CodigoPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilFaixaRating.DescricaoPerfil
		/// </summary>
		virtual public System.String DescricaoPerfil
		{
			get
			{
				return base.GetSystemString(PerfilFaixaRatingMetadata.ColumnNames.DescricaoPerfil);
			}
			
			set
			{
				base.SetSystemString(PerfilFaixaRatingMetadata.ColumnNames.DescricaoPerfil, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilFaixaRating entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilFaixaRating
			{
				get
				{
					System.Int32? data = entity.IdPerfilFaixaRating;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilFaixaRating = null;
					else entity.IdPerfilFaixaRating = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoPerfil
			{
				get
				{
					System.String data = entity.CodigoPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoPerfil = null;
					else entity.CodigoPerfil = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoPerfil
			{
				get
				{
					System.String data = entity.DescricaoPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoPerfil = null;
					else entity.DescricaoPerfil = Convert.ToString(value);
				}
			}
			

			private esPerfilFaixaRating entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilFaixaRatingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilFaixaRating can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilFaixaRating : esPerfilFaixaRating
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilFaixaRatingQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilFaixaRatingMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilFaixaRating
		{
			get
			{
				return new esQueryItem(this, PerfilFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilFaixaRatingMetadata.ColumnNames.CodigoPerfil, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilFaixaRatingMetadata.ColumnNames.DescricaoPerfil, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilFaixaRatingCollection")]
	public partial class PerfilFaixaRatingCollection : esPerfilFaixaRatingCollection, IEnumerable<PerfilFaixaRating>
	{
		public PerfilFaixaRatingCollection()
		{

		}
		
		public static implicit operator List<PerfilFaixaRating>(PerfilFaixaRatingCollection coll)
		{
			List<PerfilFaixaRating> list = new List<PerfilFaixaRating>();
			
			foreach (PerfilFaixaRating emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilFaixaRating(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilFaixaRating();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilFaixaRating AddNew()
		{
			PerfilFaixaRating entity = base.AddNewEntity() as PerfilFaixaRating;
			
			return entity;
		}

		public PerfilFaixaRating FindByPrimaryKey(System.Int32 idPerfilFaixaRating)
		{
			return base.FindByPrimaryKey(idPerfilFaixaRating) as PerfilFaixaRating;
		}


		#region IEnumerable<PerfilFaixaRating> Members

		IEnumerator<PerfilFaixaRating> IEnumerable<PerfilFaixaRating>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilFaixaRating;
			}
		}

		#endregion
		
		private PerfilFaixaRatingQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilFaixaRating' table
	/// </summary>

	[Serializable]
	public partial class PerfilFaixaRating : esPerfilFaixaRating
	{
		public PerfilFaixaRating()
		{

		}
	
		public PerfilFaixaRating(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilFaixaRatingMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilFaixaRatingQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilFaixaRatingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilFaixaRatingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilFaixaRatingQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilFaixaRatingQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilFaixaRatingQuery query;
	}



	[Serializable]
	public partial class PerfilFaixaRatingQuery : esPerfilFaixaRatingQuery
	{
		public PerfilFaixaRatingQuery()
		{

		}		
		
		public PerfilFaixaRatingQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilFaixaRatingMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilFaixaRatingMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilFaixaRatingMetadata.ColumnNames.IdPerfilFaixaRating, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilFaixaRatingMetadata.PropertyNames.IdPerfilFaixaRating;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilFaixaRatingMetadata.ColumnNames.CodigoPerfil, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilFaixaRatingMetadata.PropertyNames.CodigoPerfil;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilFaixaRatingMetadata.ColumnNames.DescricaoPerfil, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilFaixaRatingMetadata.PropertyNames.DescricaoPerfil;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilFaixaRatingMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
			 public const string CodigoPerfil = "CodigoPerfil";
			 public const string DescricaoPerfil = "DescricaoPerfil";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilFaixaRating = "IdPerfilFaixaRating";
			 public const string CodigoPerfil = "CodigoPerfil";
			 public const string DescricaoPerfil = "DescricaoPerfil";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilFaixaRatingMetadata))
			{
				if(PerfilFaixaRatingMetadata.mapDelegates == null)
				{
					PerfilFaixaRatingMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilFaixaRatingMetadata.meta == null)
				{
					PerfilFaixaRatingMetadata.meta = new PerfilFaixaRatingMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilFaixaRating", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoPerfil", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoPerfil", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PerfilFaixaRating";
				meta.Destination = "PerfilFaixaRating";
				
				meta.spInsert = "proc_PerfilFaixaRatingInsert";				
				meta.spUpdate = "proc_PerfilFaixaRatingUpdate";		
				meta.spDelete = "proc_PerfilFaixaRatingDelete";
				meta.spLoadAll = "proc_PerfilFaixaRatingLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilFaixaRatingLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilFaixaRatingMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
