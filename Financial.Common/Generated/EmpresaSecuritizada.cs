/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/12/2014 11:32:40
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa;



namespace Financial.Common
{

	[Serializable]
	abstract public class esEmpresaSecuritizadaCollection : esEntityCollection
	{
		public esEmpresaSecuritizadaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EmpresaSecuritizadaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEmpresaSecuritizadaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEmpresaSecuritizadaQuery);
		}
		#endregion
		
		virtual public EmpresaSecuritizada DetachEntity(EmpresaSecuritizada entity)
		{
			return base.DetachEntity(entity) as EmpresaSecuritizada;
		}
		
		virtual public EmpresaSecuritizada AttachEntity(EmpresaSecuritizada entity)
		{
			return base.AttachEntity(entity) as EmpresaSecuritizada;
		}
		
		virtual public void Combine(EmpresaSecuritizadaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EmpresaSecuritizada this[int index]
		{
			get
			{
				return base[index] as EmpresaSecuritizada;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EmpresaSecuritizada);
		}
	}



	[Serializable]
	abstract public class esEmpresaSecuritizada : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEmpresaSecuritizadaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEmpresaSecuritizada()
		{

		}

		public esEmpresaSecuritizada(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEmpresaSecuritizada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmpresaSecuritizada);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmpresaSecuritizada);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEmpresaSecuritizada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmpresaSecuritizada);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmpresaSecuritizada);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEmpresaSecuritizada)
		{
			esEmpresaSecuritizadaQuery query = this.GetDynamicQuery();
			query.Where(query.IdEmpresaSecuritizada == idEmpresaSecuritizada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEmpresaSecuritizada)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEmpresaSecuritizada",idEmpresaSecuritizada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEmpresaSecuritizada": this.str.IdEmpresaSecuritizada = (string)value; break;							
						case "IdAgenteMercado": this.str.IdAgenteMercado = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEmpresaSecuritizada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmpresaSecuritizada = (System.Int32?)value;
							break;
						
						case "IdAgenteMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteMercado = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EmpresaSecuritizada.IdEmpresaSecuritizada
		/// </summary>
		virtual public System.Int32? IdEmpresaSecuritizada
		{
			get
			{
				return base.GetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdEmpresaSecuritizada);
			}
			
			set
			{
				base.SetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdEmpresaSecuritizada, value);
			}
		}
		
		/// <summary>
		/// Maps to EmpresaSecuritizada.IdAgenteMercado
		/// </summary>
		virtual public System.Int32? IdAgenteMercado
		{
			get
			{
				return base.GetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdAgenteMercado);
			}
			
			set
			{
				if(base.SetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdAgenteMercado, value))
				{
					this._UpToAgenteMercadoByIdAgenteMercado = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EmpresaSecuritizada.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(EmpresaSecuritizadaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteMercado;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEmpresaSecuritizada entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEmpresaSecuritizada
			{
				get
				{
					System.Int32? data = entity.IdEmpresaSecuritizada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmpresaSecuritizada = null;
					else entity.IdEmpresaSecuritizada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteMercado
			{
				get
				{
					System.Int32? data = entity.IdAgenteMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteMercado = null;
					else entity.IdAgenteMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
			

			private esEmpresaSecuritizada entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEmpresaSecuritizadaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEmpresaSecuritizada can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EmpresaSecuritizada : esEmpresaSecuritizada
	{

				
		#region UpToAgenteMercadoByIdAgenteMercado - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__EmpresaSe__IdAge__2062B9C8
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteMercado
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteMercado == null
					&& IdAgenteMercado != null					)
				{
					this._UpToAgenteMercadoByIdAgenteMercado = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteMercado.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteMercado", this._UpToAgenteMercadoByIdAgenteMercado);
					this._UpToAgenteMercadoByIdAgenteMercado.Query.Where(this._UpToAgenteMercadoByIdAgenteMercado.Query.IdAgente == this.IdAgenteMercado);
					this._UpToAgenteMercadoByIdAgenteMercado.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteMercado;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteMercado");
				

				if(value == null)
				{
					this.IdAgenteMercado = null;
					this._UpToAgenteMercadoByIdAgenteMercado = null;
				}
				else
				{
					this.IdAgenteMercado = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteMercado = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteMercado", this._UpToAgenteMercadoByIdAgenteMercado);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__EmpresaSe__IdTit__2156DE01
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteMercado != null)
			{
				this.IdAgenteMercado = this._UpToAgenteMercadoByIdAgenteMercado.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEmpresaSecuritizadaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EmpresaSecuritizadaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEmpresaSecuritizada
		{
			get
			{
				return new esQueryItem(this, EmpresaSecuritizadaMetadata.ColumnNames.IdEmpresaSecuritizada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteMercado
		{
			get
			{
				return new esQueryItem(this, EmpresaSecuritizadaMetadata.ColumnNames.IdAgenteMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, EmpresaSecuritizadaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EmpresaSecuritizadaCollection")]
	public partial class EmpresaSecuritizadaCollection : esEmpresaSecuritizadaCollection, IEnumerable<EmpresaSecuritizada>
	{
		public EmpresaSecuritizadaCollection()
		{

		}
		
		public static implicit operator List<EmpresaSecuritizada>(EmpresaSecuritizadaCollection coll)
		{
			List<EmpresaSecuritizada> list = new List<EmpresaSecuritizada>();
			
			foreach (EmpresaSecuritizada emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EmpresaSecuritizadaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EmpresaSecuritizadaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EmpresaSecuritizada(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EmpresaSecuritizada();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EmpresaSecuritizadaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmpresaSecuritizadaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EmpresaSecuritizadaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EmpresaSecuritizada AddNew()
		{
			EmpresaSecuritizada entity = base.AddNewEntity() as EmpresaSecuritizada;
			
			return entity;
		}

		public EmpresaSecuritizada FindByPrimaryKey(System.Int32 idEmpresaSecuritizada)
		{
			return base.FindByPrimaryKey(idEmpresaSecuritizada) as EmpresaSecuritizada;
		}


		#region IEnumerable<EmpresaSecuritizada> Members

		IEnumerator<EmpresaSecuritizada> IEnumerable<EmpresaSecuritizada>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EmpresaSecuritizada;
			}
		}

		#endregion
		
		private EmpresaSecuritizadaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EmpresaSecuritizada' table
	/// </summary>

	[Serializable]
	public partial class EmpresaSecuritizada : esEmpresaSecuritizada
	{
		public EmpresaSecuritizada()
		{

		}
	
		public EmpresaSecuritizada(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EmpresaSecuritizadaMetadata.Meta();
			}
		}
		
		
		
		override protected esEmpresaSecuritizadaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EmpresaSecuritizadaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EmpresaSecuritizadaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EmpresaSecuritizadaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EmpresaSecuritizadaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EmpresaSecuritizadaQuery query;
	}



	[Serializable]
	public partial class EmpresaSecuritizadaQuery : esEmpresaSecuritizadaQuery
	{
		public EmpresaSecuritizadaQuery()
		{

		}		
		
		public EmpresaSecuritizadaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EmpresaSecuritizadaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EmpresaSecuritizadaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EmpresaSecuritizadaMetadata.ColumnNames.IdEmpresaSecuritizada, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmpresaSecuritizadaMetadata.PropertyNames.IdEmpresaSecuritizada;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmpresaSecuritizadaMetadata.ColumnNames.IdAgenteMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmpresaSecuritizadaMetadata.PropertyNames.IdAgenteMercado;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EmpresaSecuritizadaMetadata.ColumnNames.IdTitulo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EmpresaSecuritizadaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EmpresaSecuritizadaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEmpresaSecuritizada = "IdEmpresaSecuritizada";
			 public const string IdAgenteMercado = "IdAgenteMercado";
			 public const string IdTitulo = "IdTitulo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEmpresaSecuritizada = "IdEmpresaSecuritizada";
			 public const string IdAgenteMercado = "IdAgenteMercado";
			 public const string IdTitulo = "IdTitulo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EmpresaSecuritizadaMetadata))
			{
				if(EmpresaSecuritizadaMetadata.mapDelegates == null)
				{
					EmpresaSecuritizadaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EmpresaSecuritizadaMetadata.meta == null)
				{
					EmpresaSecuritizadaMetadata.meta = new EmpresaSecuritizadaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEmpresaSecuritizada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EmpresaSecuritizada";
				meta.Destination = "EmpresaSecuritizada";
				
				meta.spInsert = "proc_EmpresaSecuritizadaInsert";				
				meta.spUpdate = "proc_EmpresaSecuritizadaUpdate";		
				meta.spDelete = "proc_EmpresaSecuritizadaDelete";
				meta.spLoadAll = "proc_EmpresaSecuritizadaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EmpresaSecuritizadaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EmpresaSecuritizadaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
