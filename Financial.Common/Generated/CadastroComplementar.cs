/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 14:59:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esCadastroComplementarCollection : esEntityCollection
	{
		public esCadastroComplementarCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroComplementarCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroComplementarQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroComplementarQuery);
		}
		#endregion
		
		virtual public CadastroComplementar DetachEntity(CadastroComplementar entity)
		{
			return base.DetachEntity(entity) as CadastroComplementar;
		}
		
		virtual public CadastroComplementar AttachEntity(CadastroComplementar entity)
		{
			return base.AttachEntity(entity) as CadastroComplementar;
		}
		
		virtual public void Combine(CadastroComplementarCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroComplementar this[int index]
		{
			get
			{
				return base[index] as CadastroComplementar;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroComplementar);
		}
	}



	[Serializable]
	abstract public class esCadastroComplementar : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroComplementarQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroComplementar()
		{

		}

		public esCadastroComplementar(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCadastroComplementares)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastroComplementares);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastroComplementares);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCadastroComplementares)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastroComplementares);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastroComplementares);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCadastroComplementares)
		{
			esCadastroComplementarQuery query = this.GetDynamicQuery();
			query.Where(query.IdCadastroComplementares == idCadastroComplementares);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCadastroComplementares)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCadastroComplementares",idCadastroComplementares);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCadastroComplementares": this.str.IdCadastroComplementares = (string)value; break;							
						case "IdCamposComplementares": this.str.IdCamposComplementares = (string)value; break;							
						case "IdMercadoTipoPessoa": this.str.IdMercadoTipoPessoa = (string)value; break;							
						case "DescricaoMercadoTipoPessoa": this.str.DescricaoMercadoTipoPessoa = (string)value; break;							
						case "ValorCampo": this.str.ValorCampo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCadastroComplementares":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCadastroComplementares = (System.Int32?)value;
							break;
						
						case "IdCamposComplementares":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCamposComplementares = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroComplementar.IdCadastroComplementares
		/// </summary>
		virtual public System.Int32? IdCadastroComplementares
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarMetadata.ColumnNames.IdCadastroComplementares);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarMetadata.ColumnNames.IdCadastroComplementares, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementar.IdCamposComplementares
		/// </summary>
		virtual public System.Int32? IdCamposComplementares
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarMetadata.ColumnNames.IdCamposComplementares);
			}
			
			set
			{
				if(base.SetSystemInt32(CadastroComplementarMetadata.ColumnNames.IdCamposComplementares, value))
				{
					this._UpToCadastroComplementarCamposByIdCamposComplementares = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementar.IdMercadoTipoPessoa
		/// </summary>
		virtual public System.String IdMercadoTipoPessoa
		{
			get
			{
				return base.GetSystemString(CadastroComplementarMetadata.ColumnNames.IdMercadoTipoPessoa);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarMetadata.ColumnNames.IdMercadoTipoPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementar.DescricaoMercadoTipoPessoa
		/// </summary>
		virtual public System.String DescricaoMercadoTipoPessoa
		{
			get
			{
				return base.GetSystemString(CadastroComplementarMetadata.ColumnNames.DescricaoMercadoTipoPessoa);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarMetadata.ColumnNames.DescricaoMercadoTipoPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementar.ValorCampo
		/// </summary>
		virtual public System.String ValorCampo
		{
			get
			{
				return base.GetSystemString(CadastroComplementarMetadata.ColumnNames.ValorCampo);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarMetadata.ColumnNames.ValorCampo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CadastroComplementarCampos _UpToCadastroComplementarCamposByIdCamposComplementares;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroComplementar entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCadastroComplementares
			{
				get
				{
					System.Int32? data = entity.IdCadastroComplementares;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCadastroComplementares = null;
					else entity.IdCadastroComplementares = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCamposComplementares
			{
				get
				{
					System.Int32? data = entity.IdCamposComplementares;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCamposComplementares = null;
					else entity.IdCamposComplementares = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMercadoTipoPessoa
			{
				get
				{
					System.String data = entity.IdMercadoTipoPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMercadoTipoPessoa = null;
					else entity.IdMercadoTipoPessoa = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoMercadoTipoPessoa
			{
				get
				{
					System.String data = entity.DescricaoMercadoTipoPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoMercadoTipoPessoa = null;
					else entity.DescricaoMercadoTipoPessoa = Convert.ToString(value);
				}
			}
				
			public System.String ValorCampo
			{
				get
				{
					System.String data = entity.ValorCampo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCampo = null;
					else entity.ValorCampo = Convert.ToString(value);
				}
			}
			

			private esCadastroComplementar entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroComplementarQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroComplementar can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroComplementar : esCadastroComplementar
	{

				
		#region UpToCadastroComplementarCamposByIdCamposComplementares - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__CadastroC__IdCam__75435199
		/// </summary>

		[XmlIgnore]
		public CadastroComplementarCampos UpToCadastroComplementarCamposByIdCamposComplementares
		{
			get
			{
				if(this._UpToCadastroComplementarCamposByIdCamposComplementares == null
					&& IdCamposComplementares != null					)
				{
					this._UpToCadastroComplementarCamposByIdCamposComplementares = new CadastroComplementarCampos();
					this._UpToCadastroComplementarCamposByIdCamposComplementares.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCadastroComplementarCamposByIdCamposComplementares", this._UpToCadastroComplementarCamposByIdCamposComplementares);
					this._UpToCadastroComplementarCamposByIdCamposComplementares.Query.Where(this._UpToCadastroComplementarCamposByIdCamposComplementares.Query.IdCamposComplementares == this.IdCamposComplementares);
					this._UpToCadastroComplementarCamposByIdCamposComplementares.Query.Load();
				}

				return this._UpToCadastroComplementarCamposByIdCamposComplementares;
			}
			
			set
			{
				this.RemovePreSave("UpToCadastroComplementarCamposByIdCamposComplementares");
				

				if(value == null)
				{
					this.IdCamposComplementares = null;
					this._UpToCadastroComplementarCamposByIdCamposComplementares = null;
				}
				else
				{
					this.IdCamposComplementares = value.IdCamposComplementares;
					this._UpToCadastroComplementarCamposByIdCamposComplementares = value;
					this.SetPreSave("UpToCadastroComplementarCamposByIdCamposComplementares", this._UpToCadastroComplementarCamposByIdCamposComplementares);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCadastroComplementarCamposByIdCamposComplementares != null)
			{
				this.IdCamposComplementares = this._UpToCadastroComplementarCamposByIdCamposComplementares.IdCamposComplementares;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroComplementarQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCadastroComplementares
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarMetadata.ColumnNames.IdCadastroComplementares, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCamposComplementares
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarMetadata.ColumnNames.IdCamposComplementares, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMercadoTipoPessoa
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarMetadata.ColumnNames.IdMercadoTipoPessoa, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoMercadoTipoPessoa
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarMetadata.ColumnNames.DescricaoMercadoTipoPessoa, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorCampo
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarMetadata.ColumnNames.ValorCampo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroComplementarCollection")]
	public partial class CadastroComplementarCollection : esCadastroComplementarCollection, IEnumerable<CadastroComplementar>
	{
		public CadastroComplementarCollection()
		{

		}
		
		public static implicit operator List<CadastroComplementar>(CadastroComplementarCollection coll)
		{
			List<CadastroComplementar> list = new List<CadastroComplementar>();
			
			foreach (CadastroComplementar emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroComplementarMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroComplementar(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroComplementar();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroComplementarQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroComplementarQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroComplementar AddNew()
		{
			CadastroComplementar entity = base.AddNewEntity() as CadastroComplementar;
			
			return entity;
		}

		public CadastroComplementar FindByPrimaryKey(System.Int32 idCadastroComplementares)
		{
			return base.FindByPrimaryKey(idCadastroComplementares) as CadastroComplementar;
		}


		#region IEnumerable<CadastroComplementar> Members

		IEnumerator<CadastroComplementar> IEnumerable<CadastroComplementar>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroComplementar;
			}
		}

		#endregion
		
		private CadastroComplementarQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroComplementar' table
	/// </summary>

	[Serializable]
	public partial class CadastroComplementar : esCadastroComplementar
	{
		public CadastroComplementar()
		{

		}
	
		public CadastroComplementar(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroComplementarQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroComplementarQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroComplementarQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroComplementarQuery query;
	}



	[Serializable]
	public partial class CadastroComplementarQuery : esCadastroComplementarQuery
	{
		public CadastroComplementarQuery()
		{

		}		
		
		public CadastroComplementarQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroComplementarMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroComplementarMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroComplementarMetadata.ColumnNames.IdCadastroComplementares, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarMetadata.PropertyNames.IdCadastroComplementares;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarMetadata.ColumnNames.IdCamposComplementares, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarMetadata.PropertyNames.IdCamposComplementares;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarMetadata.ColumnNames.IdMercadoTipoPessoa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarMetadata.PropertyNames.IdMercadoTipoPessoa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarMetadata.ColumnNames.DescricaoMercadoTipoPessoa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarMetadata.PropertyNames.DescricaoMercadoTipoPessoa;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarMetadata.ColumnNames.ValorCampo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarMetadata.PropertyNames.ValorCampo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroComplementarMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCadastroComplementares = "IdCadastroComplementares";
			 public const string IdCamposComplementares = "IdCamposComplementares";
			 public const string IdMercadoTipoPessoa = "IdMercadoTipoPessoa";
			 public const string DescricaoMercadoTipoPessoa = "DescricaoMercadoTipoPessoa";
			 public const string ValorCampo = "ValorCampo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCadastroComplementares = "IdCadastroComplementares";
			 public const string IdCamposComplementares = "IdCamposComplementares";
			 public const string IdMercadoTipoPessoa = "IdMercadoTipoPessoa";
			 public const string DescricaoMercadoTipoPessoa = "DescricaoMercadoTipoPessoa";
			 public const string ValorCampo = "ValorCampo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroComplementarMetadata))
			{
				if(CadastroComplementarMetadata.mapDelegates == null)
				{
					CadastroComplementarMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroComplementarMetadata.meta == null)
				{
					CadastroComplementarMetadata.meta = new CadastroComplementarMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCadastroComplementares", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCamposComplementares", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMercadoTipoPessoa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoMercadoTipoPessoa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorCampo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "CadastroComplementar";
				meta.Destination = "CadastroComplementar";
				
				meta.spInsert = "proc_CadastroComplementarInsert";				
				meta.spUpdate = "proc_CadastroComplementarUpdate";		
				meta.spDelete = "proc_CadastroComplementarDelete";
				meta.spLoadAll = "proc_CadastroComplementarLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroComplementarLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroComplementarMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
