/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/10/2014 14:31:59
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esLocalCustodiaCollection : esEntityCollection
	{
		public esLocalCustodiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LocalCustodiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLocalCustodiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLocalCustodiaQuery);
		}
		#endregion
		
		virtual public LocalCustodia DetachEntity(LocalCustodia entity)
		{
			return base.DetachEntity(entity) as LocalCustodia;
		}
		
		virtual public LocalCustodia AttachEntity(LocalCustodia entity)
		{
			return base.AttachEntity(entity) as LocalCustodia;
		}
		
		virtual public void Combine(LocalCustodiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LocalCustodia this[int index]
		{
			get
			{
				return base[index] as LocalCustodia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LocalCustodia);
		}
	}



	[Serializable]
	abstract public class esLocalCustodia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLocalCustodiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLocalCustodia()
		{

		}

		public esLocalCustodia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLocalCustodia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocalCustodia);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocalCustodia);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLocalCustodia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLocalCustodia);
			else
				return LoadByPrimaryKeyStoredProcedure(idLocalCustodia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLocalCustodia)
		{
			esLocalCustodiaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLocalCustodia == idLocalCustodia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLocalCustodia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLocalCustodia",idLocalCustodia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Cnpj": this.str.Cnpj = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LocalCustodia.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(LocalCustodiaMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(LocalCustodiaMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalCustodia.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(LocalCustodiaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(LocalCustodiaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalCustodia.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(LocalCustodiaMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(LocalCustodiaMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to LocalCustodia.Cnpj
		/// </summary>
		virtual public System.String Cnpj
		{
			get
			{
				return base.GetSystemString(LocalCustodiaMetadata.ColumnNames.Cnpj);
			}
			
			set
			{
				base.SetSystemString(LocalCustodiaMetadata.ColumnNames.Cnpj, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLocalCustodia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Cnpj
			{
				get
				{
					System.String data = entity.Cnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cnpj = null;
					else entity.Cnpj = Convert.ToString(value);
				}
			}
			

			private esLocalCustodia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLocalCustodiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLocalCustodia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LocalCustodia : esLocalCustodia
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLocalCustodiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LocalCustodiaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, LocalCustodiaMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, LocalCustodiaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, LocalCustodiaMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Cnpj
		{
			get
			{
				return new esQueryItem(this, LocalCustodiaMetadata.ColumnNames.Cnpj, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LocalCustodiaCollection")]
	public partial class LocalCustodiaCollection : esLocalCustodiaCollection, IEnumerable<LocalCustodia>
	{
		public LocalCustodiaCollection()
		{

		}
		
		public static implicit operator List<LocalCustodia>(LocalCustodiaCollection coll)
		{
			List<LocalCustodia> list = new List<LocalCustodia>();
			
			foreach (LocalCustodia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LocalCustodiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalCustodiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LocalCustodia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LocalCustodia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LocalCustodiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalCustodiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LocalCustodiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LocalCustodia AddNew()
		{
			LocalCustodia entity = base.AddNewEntity() as LocalCustodia;
			
			return entity;
		}

		public LocalCustodia FindByPrimaryKey(System.Int32 idLocalCustodia)
		{
			return base.FindByPrimaryKey(idLocalCustodia) as LocalCustodia;
		}


		#region IEnumerable<LocalCustodia> Members

		IEnumerator<LocalCustodia> IEnumerable<LocalCustodia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LocalCustodia;
			}
		}

		#endregion
		
		private LocalCustodiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LocalCustodia' table
	/// </summary>

	[Serializable]
	public partial class LocalCustodia : esLocalCustodia
	{
		public LocalCustodia()
		{

		}
	
		public LocalCustodia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LocalCustodiaMetadata.Meta();
			}
		}
		
		
		
		override protected esLocalCustodiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LocalCustodiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LocalCustodiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LocalCustodiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LocalCustodiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LocalCustodiaQuery query;
	}



	[Serializable]
	public partial class LocalCustodiaQuery : esLocalCustodiaQuery
	{
		public LocalCustodiaQuery()
		{

		}		
		
		public LocalCustodiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LocalCustodiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LocalCustodiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LocalCustodiaMetadata.ColumnNames.IdLocalCustodia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LocalCustodiaMetadata.PropertyNames.IdLocalCustodia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalCustodiaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalCustodiaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalCustodiaMetadata.ColumnNames.Codigo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalCustodiaMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 250;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LocalCustodiaMetadata.ColumnNames.Cnpj, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = LocalCustodiaMetadata.PropertyNames.Cnpj;
			c.CharacterMaxLength = 250;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LocalCustodiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string Descricao = "Descricao";
			 public const string Codigo = "Codigo";
			 public const string Cnpj = "Cnpj";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string Descricao = "Descricao";
			 public const string Codigo = "Codigo";
			 public const string Cnpj = "Cnpj";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LocalCustodiaMetadata))
			{
				if(LocalCustodiaMetadata.mapDelegates == null)
				{
					LocalCustodiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LocalCustodiaMetadata.meta == null)
				{
					LocalCustodiaMetadata.meta = new LocalCustodiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Cnpj", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LocalCustodia";
				meta.Destination = "LocalCustodia";
				
				meta.spInsert = "proc_LocalCustodiaInsert";				
				meta.spUpdate = "proc_LocalCustodiaUpdate";		
				meta.spDelete = "proc_LocalCustodiaDelete";
				meta.spLoadAll = "proc_LocalCustodiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LocalCustodiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LocalCustodiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
