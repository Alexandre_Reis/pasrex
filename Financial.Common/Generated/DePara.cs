/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/11/2014 15:27:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common
{

	[Serializable]
	abstract public class esDeParaCollection : esEntityCollection
	{
		public esDeParaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DeParaCollection";
		}

		#region Query Logic
		protected void InitQuery(esDeParaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDeParaQuery);
		}
		#endregion
		
		virtual public DePara DetachEntity(DePara entity)
		{
			return base.DetachEntity(entity) as DePara;
		}
		
		virtual public DePara AttachEntity(DePara entity)
		{
			return base.AttachEntity(entity) as DePara;
		}
		
		virtual public void Combine(DeParaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DePara this[int index]
		{
			get
			{
				return base[index] as DePara;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DePara);
		}
	}



	[Serializable]
	abstract public class esDePara : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDeParaQuery GetDynamicQuery()
		{
			return null;
		}

		public esDePara()
		{

		}

		public esDePara(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDePara)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDePara);
			else
				return LoadByPrimaryKeyStoredProcedure(idDePara);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDePara)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDePara);
			else
				return LoadByPrimaryKeyStoredProcedure(idDePara);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDePara)
		{
			esDeParaQuery query = this.GetDynamicQuery();
			query.Where(query.IdDePara == idDePara);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDePara)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDePara",idDePara);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDePara": this.str.IdDePara = (string)value; break;							
						case "IdTipoDePara": this.str.IdTipoDePara = (string)value; break;							
						case "CodigoInterno": this.str.CodigoInterno = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;							
						case "DescricaoOrigem": this.str.DescricaoOrigem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDePara":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDePara = (System.Int32?)value;
							break;
						
						case "IdTipoDePara":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoDePara = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DePara.IdDePara
		/// </summary>
		virtual public System.Int32? IdDePara
		{
			get
			{
				return base.GetSystemInt32(DeParaMetadata.ColumnNames.IdDePara);
			}
			
			set
			{
				base.SetSystemInt32(DeParaMetadata.ColumnNames.IdDePara, value);
			}
		}
		
		/// <summary>
		/// Maps to DePara.IdTipoDePara
		/// </summary>
		virtual public System.Int32? IdTipoDePara
		{
			get
			{
				return base.GetSystemInt32(DeParaMetadata.ColumnNames.IdTipoDePara);
			}
			
			set
			{
				if(base.SetSystemInt32(DeParaMetadata.ColumnNames.IdTipoDePara, value))
				{
					this._UpToTipoDeParaByIdTipoDePara = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to DePara.CodigoInterno
		/// </summary>
		virtual public System.String CodigoInterno
		{
			get
			{
				return base.GetSystemString(DeParaMetadata.ColumnNames.CodigoInterno);
			}
			
			set
			{
				base.SetSystemString(DeParaMetadata.ColumnNames.CodigoInterno, value);
			}
		}
		
		/// <summary>
		/// Maps to DePara.CodigoExterno
		/// </summary>
		virtual public System.String CodigoExterno
		{
			get
			{
				return base.GetSystemString(DeParaMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemString(DeParaMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to DePara.DescricaoOrigem
		/// </summary>
		virtual public System.String DescricaoOrigem
		{
			get
			{
				return base.GetSystemString(DeParaMetadata.ColumnNames.DescricaoOrigem);
			}
			
			set
			{
				base.SetSystemString(DeParaMetadata.ColumnNames.DescricaoOrigem, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TipoDePara _UpToTipoDeParaByIdTipoDePara;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDePara entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDePara
			{
				get
				{
					System.Int32? data = entity.IdDePara;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDePara = null;
					else entity.IdDePara = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTipoDePara
			{
				get
				{
					System.Int32? data = entity.IdTipoDePara;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoDePara = null;
					else entity.IdTipoDePara = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoInterno
			{
				get
				{
					System.String data = entity.CodigoInterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterno = null;
					else entity.CodigoInterno = Convert.ToString(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.String data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoOrigem
			{
				get
				{
					System.String data = entity.DescricaoOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoOrigem = null;
					else entity.DescricaoOrigem = Convert.ToString(value);
				}
			}
			

			private esDePara entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDeParaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDePara can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DePara : esDePara
	{

				
		#region UpToTipoDeParaByIdTipoDePara - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - DePara_TipoDePara_FK
		/// </summary>

		[XmlIgnore]
		public TipoDePara UpToTipoDeParaByIdTipoDePara
		{
			get
			{
				if(this._UpToTipoDeParaByIdTipoDePara == null
					&& IdTipoDePara != null					)
				{
					this._UpToTipoDeParaByIdTipoDePara = new TipoDePara();
					this._UpToTipoDeParaByIdTipoDePara.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTipoDeParaByIdTipoDePara", this._UpToTipoDeParaByIdTipoDePara);
					this._UpToTipoDeParaByIdTipoDePara.Query.Where(this._UpToTipoDeParaByIdTipoDePara.Query.IdTipoDePara == this.IdTipoDePara);
					this._UpToTipoDeParaByIdTipoDePara.Query.Load();
				}

				return this._UpToTipoDeParaByIdTipoDePara;
			}
			
			set
			{
				this.RemovePreSave("UpToTipoDeParaByIdTipoDePara");
				

				if(value == null)
				{
					this.IdTipoDePara = null;
					this._UpToTipoDeParaByIdTipoDePara = null;
				}
				else
				{
					this.IdTipoDePara = value.IdTipoDePara;
					this._UpToTipoDeParaByIdTipoDePara = value;
					this.SetPreSave("UpToTipoDeParaByIdTipoDePara", this._UpToTipoDeParaByIdTipoDePara);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTipoDeParaByIdTipoDePara != null)
			{
				this.IdTipoDePara = this._UpToTipoDeParaByIdTipoDePara.IdTipoDePara;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDeParaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DeParaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDePara
		{
			get
			{
				return new esQueryItem(this, DeParaMetadata.ColumnNames.IdDePara, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTipoDePara
		{
			get
			{
				return new esQueryItem(this, DeParaMetadata.ColumnNames.IdTipoDePara, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoInterno
		{
			get
			{
				return new esQueryItem(this, DeParaMetadata.ColumnNames.CodigoInterno, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, DeParaMetadata.ColumnNames.CodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoOrigem
		{
			get
			{
				return new esQueryItem(this, DeParaMetadata.ColumnNames.DescricaoOrigem, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DeParaCollection")]
	public partial class DeParaCollection : esDeParaCollection, IEnumerable<DePara>
	{
		public DeParaCollection()
		{

		}
		
		public static implicit operator List<DePara>(DeParaCollection coll)
		{
			List<DePara> list = new List<DePara>();
			
			foreach (DePara emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DeParaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DeParaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DePara(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DePara();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DeParaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DeParaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DeParaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DePara AddNew()
		{
			DePara entity = base.AddNewEntity() as DePara;
			
			return entity;
		}

		public DePara FindByPrimaryKey(System.Int32 idDePara)
		{
			return base.FindByPrimaryKey(idDePara) as DePara;
		}


		#region IEnumerable<DePara> Members

		IEnumerator<DePara> IEnumerable<DePara>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DePara;
			}
		}

		#endregion
		
		private DeParaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DePara' table
	/// </summary>

	[Serializable]
	public partial class DePara : esDePara
	{
		public DePara()
		{

		}
	
		public DePara(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DeParaMetadata.Meta();
			}
		}
		
		
		
		override protected esDeParaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DeParaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DeParaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DeParaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DeParaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DeParaQuery query;
	}



	[Serializable]
	public partial class DeParaQuery : esDeParaQuery
	{
		public DeParaQuery()
		{

		}		
		
		public DeParaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DeParaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DeParaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DeParaMetadata.ColumnNames.IdDePara, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DeParaMetadata.PropertyNames.IdDePara;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaMetadata.ColumnNames.IdTipoDePara, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DeParaMetadata.PropertyNames.IdTipoDePara;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaMetadata.ColumnNames.CodigoInterno, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = DeParaMetadata.PropertyNames.CodigoInterno;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaMetadata.ColumnNames.CodigoExterno, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = DeParaMetadata.PropertyNames.CodigoExterno;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DeParaMetadata.ColumnNames.DescricaoOrigem, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = DeParaMetadata.PropertyNames.DescricaoOrigem;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DeParaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDePara = "IdDePara";
			 public const string IdTipoDePara = "IdTipoDePara";
			 public const string CodigoInterno = "CodigoInterno";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DescricaoOrigem = "DescricaoOrigem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDePara = "IdDePara";
			 public const string IdTipoDePara = "IdTipoDePara";
			 public const string CodigoInterno = "CodigoInterno";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DescricaoOrigem = "DescricaoOrigem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DeParaMetadata))
			{
				if(DeParaMetadata.mapDelegates == null)
				{
					DeParaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DeParaMetadata.meta == null)
				{
					DeParaMetadata.meta = new DeParaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDePara", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTipoDePara", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoInterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoOrigem", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "DePara";
				meta.Destination = "DePara";
				
				meta.spInsert = "proc_DeParaInsert";				
				meta.spUpdate = "proc_DeParaUpdate";		
				meta.spDelete = "proc_DeParaDelete";
				meta.spLoadAll = "proc_DeParaLoadAll";
				meta.spLoadByPrimaryKey = "proc_DeParaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DeParaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
