/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/01/2016 11:50:31
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Common
{

	[Serializable]
	abstract public class esCategoriaMovimentacaoCollection : esEntityCollection
	{
		public esCategoriaMovimentacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CategoriaMovimentacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCategoriaMovimentacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCategoriaMovimentacaoQuery);
		}
		#endregion
		
		virtual public CategoriaMovimentacao DetachEntity(CategoriaMovimentacao entity)
		{
			return base.DetachEntity(entity) as CategoriaMovimentacao;
		}
		
		virtual public CategoriaMovimentacao AttachEntity(CategoriaMovimentacao entity)
		{
			return base.AttachEntity(entity) as CategoriaMovimentacao;
		}
		
		virtual public void Combine(CategoriaMovimentacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CategoriaMovimentacao this[int index]
		{
			get
			{
				return base[index] as CategoriaMovimentacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CategoriaMovimentacao);
		}
	}



	[Serializable]
	abstract public class esCategoriaMovimentacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCategoriaMovimentacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCategoriaMovimentacao()
		{

		}

		public esCategoriaMovimentacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCategoriaMovimentacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoriaMovimentacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoriaMovimentacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCategoriaMovimentacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoriaMovimentacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoriaMovimentacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCategoriaMovimentacao)
		{
			esCategoriaMovimentacaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCategoriaMovimentacao == idCategoriaMovimentacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCategoriaMovimentacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCategoriaMovimentacao",idCategoriaMovimentacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;							
						case "CodigoCategoria": this.str.CodigoCategoria = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CategoriaMovimentacao.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(CategoriaMovimentacaoMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				base.SetSystemInt32(CategoriaMovimentacaoMetadata.ColumnNames.IdCategoriaMovimentacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CategoriaMovimentacao.CodigoCategoria
		/// </summary>
		virtual public System.String CodigoCategoria
		{
			get
			{
				return base.GetSystemString(CategoriaMovimentacaoMetadata.ColumnNames.CodigoCategoria);
			}
			
			set
			{
				base.SetSystemString(CategoriaMovimentacaoMetadata.ColumnNames.CodigoCategoria, value);
			}
		}
		
		/// <summary>
		/// Maps to CategoriaMovimentacao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CategoriaMovimentacaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CategoriaMovimentacaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCategoriaMovimentacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCategoria
			{
				get
				{
					System.String data = entity.CodigoCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCategoria = null;
					else entity.CodigoCategoria = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esCategoriaMovimentacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCategoriaMovimentacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCategoriaMovimentacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CategoriaMovimentacao : esCategoriaMovimentacao
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCategoriaMovimentacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CategoriaMovimentacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, CategoriaMovimentacaoMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCategoria
		{
			get
			{
				return new esQueryItem(this, CategoriaMovimentacaoMetadata.ColumnNames.CodigoCategoria, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CategoriaMovimentacaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CategoriaMovimentacaoCollection")]
	public partial class CategoriaMovimentacaoCollection : esCategoriaMovimentacaoCollection, IEnumerable<CategoriaMovimentacao>
	{
		public CategoriaMovimentacaoCollection()
		{

		}
		
		public static implicit operator List<CategoriaMovimentacao>(CategoriaMovimentacaoCollection coll)
		{
			List<CategoriaMovimentacao> list = new List<CategoriaMovimentacao>();
			
			foreach (CategoriaMovimentacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CategoriaMovimentacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CategoriaMovimentacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CategoriaMovimentacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CategoriaMovimentacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CategoriaMovimentacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CategoriaMovimentacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CategoriaMovimentacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CategoriaMovimentacao AddNew()
		{
			CategoriaMovimentacao entity = base.AddNewEntity() as CategoriaMovimentacao;
			
			return entity;
		}

		public CategoriaMovimentacao FindByPrimaryKey(System.Int32 idCategoriaMovimentacao)
		{
			return base.FindByPrimaryKey(idCategoriaMovimentacao) as CategoriaMovimentacao;
		}


		#region IEnumerable<CategoriaMovimentacao> Members

		IEnumerator<CategoriaMovimentacao> IEnumerable<CategoriaMovimentacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CategoriaMovimentacao;
			}
		}

		#endregion
		
		private CategoriaMovimentacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CategoriaMovimentacao' table
	/// </summary>

	[Serializable]
	public partial class CategoriaMovimentacao : esCategoriaMovimentacao
	{
		public CategoriaMovimentacao()
		{

		}
	
		public CategoriaMovimentacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CategoriaMovimentacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esCategoriaMovimentacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CategoriaMovimentacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CategoriaMovimentacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CategoriaMovimentacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CategoriaMovimentacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CategoriaMovimentacaoQuery query;
	}



	[Serializable]
	public partial class CategoriaMovimentacaoQuery : esCategoriaMovimentacaoQuery
	{
		public CategoriaMovimentacaoQuery()
		{

		}		
		
		public CategoriaMovimentacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CategoriaMovimentacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CategoriaMovimentacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CategoriaMovimentacaoMetadata.ColumnNames.IdCategoriaMovimentacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CategoriaMovimentacaoMetadata.PropertyNames.IdCategoriaMovimentacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CategoriaMovimentacaoMetadata.ColumnNames.CodigoCategoria, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CategoriaMovimentacaoMetadata.PropertyNames.CodigoCategoria;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CategoriaMovimentacaoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CategoriaMovimentacaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CategoriaMovimentacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string CodigoCategoria = "CodigoCategoria";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string CodigoCategoria = "CodigoCategoria";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CategoriaMovimentacaoMetadata))
			{
				if(CategoriaMovimentacaoMetadata.mapDelegates == null)
				{
					CategoriaMovimentacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CategoriaMovimentacaoMetadata.meta == null)
				{
					CategoriaMovimentacaoMetadata.meta = new CategoriaMovimentacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCategoria", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "CategoriaMovimentacao";
				meta.Destination = "CategoriaMovimentacao";
				
				meta.spInsert = "proc_CategoriaMovimentacaoInsert";				
				meta.spUpdate = "proc_CategoriaMovimentacaoUpdate";		
				meta.spDelete = "proc_CategoriaMovimentacaoDelete";
				meta.spLoadAll = "proc_CategoriaMovimentacaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CategoriaMovimentacaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CategoriaMovimentacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
