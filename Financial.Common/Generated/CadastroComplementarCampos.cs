/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/07/2015 17:06:12
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esCadastroComplementarCamposCollection : esEntityCollection
	{
		public esCadastroComplementarCamposCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroComplementarCamposCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroComplementarCamposQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroComplementarCamposQuery);
		}
		#endregion
		
		virtual public CadastroComplementarCampos DetachEntity(CadastroComplementarCampos entity)
		{
			return base.DetachEntity(entity) as CadastroComplementarCampos;
		}
		
		virtual public CadastroComplementarCampos AttachEntity(CadastroComplementarCampos entity)
		{
			return base.AttachEntity(entity) as CadastroComplementarCampos;
		}
		
		virtual public void Combine(CadastroComplementarCamposCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroComplementarCampos this[int index]
		{
			get
			{
				return base[index] as CadastroComplementarCampos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroComplementarCampos);
		}
	}



	[Serializable]
	abstract public class esCadastroComplementarCampos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroComplementarCamposQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroComplementarCampos()
		{

		}

		public esCadastroComplementarCampos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCamposComplementares)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCamposComplementares);
			else
				return LoadByPrimaryKeyStoredProcedure(idCamposComplementares);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCamposComplementares)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCamposComplementares);
			else
				return LoadByPrimaryKeyStoredProcedure(idCamposComplementares);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCamposComplementares)
		{
			esCadastroComplementarCamposQuery query = this.GetDynamicQuery();
			query.Where(query.IdCamposComplementares == idCamposComplementares);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCamposComplementares)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCamposComplementares",idCamposComplementares);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCamposComplementares": this.str.IdCamposComplementares = (string)value; break;							
						case "TipoCadastro": this.str.TipoCadastro = (string)value; break;							
						case "NomeCampo": this.str.NomeCampo = (string)value; break;							
						case "DescricaoCampo": this.str.DescricaoCampo = (string)value; break;							
						case "ValorDefault": this.str.ValorDefault = (string)value; break;							
						case "TipoCampo": this.str.TipoCampo = (string)value; break;							
						case "CampoObrigatorio": this.str.CampoObrigatorio = (string)value; break;							
						case "Tamanho": this.str.Tamanho = (string)value; break;							
						case "CasasDecimais": this.str.CasasDecimais = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCamposComplementares":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCamposComplementares = (System.Int32?)value;
							break;
						
						case "TipoCadastro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCadastro = (System.Int32?)value;
							break;
						
						case "TipoCampo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCampo = (System.Int32?)value;
							break;
						
						case "Tamanho":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tamanho = (System.Int32?)value;
							break;
						
						case "CasasDecimais":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CasasDecimais = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.IdCamposComplementares
		/// </summary>
		virtual public System.Int32? IdCamposComplementares
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.IdCamposComplementares);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.IdCamposComplementares, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.TipoCadastro
		/// </summary>
		virtual public System.Int32? TipoCadastro
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.TipoCadastro);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.TipoCadastro, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.NomeCampo
		/// </summary>
		virtual public System.String NomeCampo
		{
			get
			{
				return base.GetSystemString(CadastroComplementarCamposMetadata.ColumnNames.NomeCampo);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarCamposMetadata.ColumnNames.NomeCampo, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.DescricaoCampo
		/// </summary>
		virtual public System.String DescricaoCampo
		{
			get
			{
				return base.GetSystemString(CadastroComplementarCamposMetadata.ColumnNames.DescricaoCampo);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarCamposMetadata.ColumnNames.DescricaoCampo, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.ValorDefault
		/// </summary>
		virtual public System.String ValorDefault
		{
			get
			{
				return base.GetSystemString(CadastroComplementarCamposMetadata.ColumnNames.ValorDefault);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarCamposMetadata.ColumnNames.ValorDefault, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.TipoCampo
		/// </summary>
		virtual public System.Int32? TipoCampo
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.TipoCampo);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.TipoCampo, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.CampoObrigatorio
		/// </summary>
		virtual public System.String CampoObrigatorio
		{
			get
			{
				return base.GetSystemString(CadastroComplementarCamposMetadata.ColumnNames.CampoObrigatorio);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarCamposMetadata.ColumnNames.CampoObrigatorio, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.Tamanho
		/// </summary>
		virtual public System.Int32? Tamanho
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.Tamanho);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.Tamanho, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarCampos.CasasDecimais
		/// </summary>
		virtual public System.Int32? CasasDecimais
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.CasasDecimais);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarCamposMetadata.ColumnNames.CasasDecimais, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroComplementarCampos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCamposComplementares
			{
				get
				{
					System.Int32? data = entity.IdCamposComplementares;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCamposComplementares = null;
					else entity.IdCamposComplementares = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCadastro
			{
				get
				{
					System.Int32? data = entity.TipoCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCadastro = null;
					else entity.TipoCadastro = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeCampo
			{
				get
				{
					System.String data = entity.NomeCampo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeCampo = null;
					else entity.NomeCampo = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoCampo
			{
				get
				{
					System.String data = entity.DescricaoCampo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoCampo = null;
					else entity.DescricaoCampo = Convert.ToString(value);
				}
			}
				
			public System.String ValorDefault
			{
				get
				{
					System.String data = entity.ValorDefault;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDefault = null;
					else entity.ValorDefault = Convert.ToString(value);
				}
			}
				
			public System.String TipoCampo
			{
				get
				{
					System.Int32? data = entity.TipoCampo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCampo = null;
					else entity.TipoCampo = Convert.ToInt32(value);
				}
			}
				
			public System.String CampoObrigatorio
			{
				get
				{
					System.String data = entity.CampoObrigatorio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CampoObrigatorio = null;
					else entity.CampoObrigatorio = Convert.ToString(value);
				}
			}
				
			public System.String Tamanho
			{
				get
				{
					System.Int32? data = entity.Tamanho;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tamanho = null;
					else entity.Tamanho = Convert.ToInt32(value);
				}
			}
				
			public System.String CasasDecimais
			{
				get
				{
					System.Int32? data = entity.CasasDecimais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CasasDecimais = null;
					else entity.CasasDecimais = Convert.ToInt32(value);
				}
			}
			

			private esCadastroComplementarCampos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroComplementarCamposQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroComplementarCampos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroComplementarCampos : esCadastroComplementarCampos
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroComplementarCamposQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarCamposMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCamposComplementares
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.IdCamposComplementares, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCadastro
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.TipoCadastro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeCampo
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.NomeCampo, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoCampo
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.DescricaoCampo, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorDefault
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.ValorDefault, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCampo
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.TipoCampo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CampoObrigatorio
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.CampoObrigatorio, esSystemType.String);
			}
		} 
		
		public esQueryItem Tamanho
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.Tamanho, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CasasDecimais
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarCamposMetadata.ColumnNames.CasasDecimais, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroComplementarCamposCollection")]
	public partial class CadastroComplementarCamposCollection : esCadastroComplementarCamposCollection, IEnumerable<CadastroComplementarCampos>
	{
		public CadastroComplementarCamposCollection()
		{

		}
		
		public static implicit operator List<CadastroComplementarCampos>(CadastroComplementarCamposCollection coll)
		{
			List<CadastroComplementarCampos> list = new List<CadastroComplementarCampos>();
			
			foreach (CadastroComplementarCampos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroComplementarCamposMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarCamposQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroComplementarCampos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroComplementarCampos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroComplementarCamposQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarCamposQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroComplementarCamposQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroComplementarCampos AddNew()
		{
			CadastroComplementarCampos entity = base.AddNewEntity() as CadastroComplementarCampos;
			
			return entity;
		}

		public CadastroComplementarCampos FindByPrimaryKey(System.Int32 idCamposComplementares)
		{
			return base.FindByPrimaryKey(idCamposComplementares) as CadastroComplementarCampos;
		}


		#region IEnumerable<CadastroComplementarCampos> Members

		IEnumerator<CadastroComplementarCampos> IEnumerable<CadastroComplementarCampos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroComplementarCampos;
			}
		}

		#endregion
		
		private CadastroComplementarCamposQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroComplementarCampos' table
	/// </summary>

	[Serializable]
	public partial class CadastroComplementarCampos : esCadastroComplementarCampos
	{
		public CadastroComplementarCampos()
		{

		}
	
		public CadastroComplementarCampos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarCamposMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroComplementarCamposQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarCamposQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroComplementarCamposQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarCamposQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroComplementarCamposQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroComplementarCamposQuery query;
	}



	[Serializable]
	public partial class CadastroComplementarCamposQuery : esCadastroComplementarCamposQuery
	{
		public CadastroComplementarCamposQuery()
		{

		}		
		
		public CadastroComplementarCamposQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroComplementarCamposMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroComplementarCamposMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.IdCamposComplementares, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.IdCamposComplementares;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.TipoCadastro, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.TipoCadastro;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.NomeCampo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.NomeCampo;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.DescricaoCampo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.DescricaoCampo;
			c.CharacterMaxLength = 160;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.ValorDefault, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.ValorDefault;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.TipoCampo, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.TipoCampo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.CampoObrigatorio, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.CampoObrigatorio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.Tamanho, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.Tamanho;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarCamposMetadata.ColumnNames.CasasDecimais, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarCamposMetadata.PropertyNames.CasasDecimais;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroComplementarCamposMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCamposComplementares = "IdCamposComplementares";
			 public const string TipoCadastro = "TipoCadastro";
			 public const string NomeCampo = "NomeCampo";
			 public const string DescricaoCampo = "DescricaoCampo";
			 public const string ValorDefault = "ValorDefault";
			 public const string TipoCampo = "TipoCampo";
			 public const string CampoObrigatorio = "CampoObrigatorio";
			 public const string Tamanho = "Tamanho";
			 public const string CasasDecimais = "CasasDecimais";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCamposComplementares = "IdCamposComplementares";
			 public const string TipoCadastro = "TipoCadastro";
			 public const string NomeCampo = "NomeCampo";
			 public const string DescricaoCampo = "DescricaoCampo";
			 public const string ValorDefault = "ValorDefault";
			 public const string TipoCampo = "TipoCampo";
			 public const string CampoObrigatorio = "CampoObrigatorio";
			 public const string Tamanho = "Tamanho";
			 public const string CasasDecimais = "CasasDecimais";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroComplementarCamposMetadata))
			{
				if(CadastroComplementarCamposMetadata.mapDelegates == null)
				{
					CadastroComplementarCamposMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroComplementarCamposMetadata.meta == null)
				{
					CadastroComplementarCamposMetadata.meta = new CadastroComplementarCamposMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCamposComplementares", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCadastro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeCampo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoCampo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorDefault", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCampo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CampoObrigatorio", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Tamanho", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CasasDecimais", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CadastroComplementarCampos";
				meta.Destination = "CadastroComplementarCampos";
				
				meta.spInsert = "proc_CadastroComplementarCamposInsert";				
				meta.spUpdate = "proc_CadastroComplementarCamposUpdate";		
				meta.spDelete = "proc_CadastroComplementarCamposDelete";
				meta.spLoadAll = "proc_CadastroComplementarCamposLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroComplementarCamposLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroComplementarCamposMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
