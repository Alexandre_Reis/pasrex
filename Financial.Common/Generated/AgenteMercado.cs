/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/06/2015 11:58:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Swap;
using Financial.Enquadra;
using Financial.ContaCorrente;
using Financial.Tributo;
using Financial.BMF;
using Financial.Captacao;
using Financial.CRM;



namespace Financial.Common
{

	[Serializable]
	abstract public class esAgenteMercadoCollection : esEntityCollection
	{
		public esAgenteMercadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgenteMercadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgenteMercadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgenteMercadoQuery);
		}
		#endregion
		
		virtual public AgenteMercado DetachEntity(AgenteMercado entity)
		{
			return base.DetachEntity(entity) as AgenteMercado;
		}
		
		virtual public AgenteMercado AttachEntity(AgenteMercado entity)
		{
			return base.AttachEntity(entity) as AgenteMercado;
		}
		
		virtual public void Combine(AgenteMercadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgenteMercado this[int index]
		{
			get
			{
				return base[index] as AgenteMercado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgenteMercado);
		}
	}



	[Serializable]
	abstract public class esAgenteMercado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgenteMercadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgenteMercado()
		{

		}

		public esAgenteMercado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idAgente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAgenteMercadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAgente == idAgente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgente)
		{
			esAgenteMercadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgente == idAgente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgente",idAgente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "CodigoBovespa": this.str.CodigoBovespa = (string)value; break;							
						case "CodigoBMF": this.str.CodigoBMF = (string)value; break;							
						case "Cnpj": this.str.Cnpj = (string)value; break;							
						case "Endereco": this.str.Endereco = (string)value; break;							
						case "Bairro": this.str.Bairro = (string)value; break;							
						case "Cidade": this.str.Cidade = (string)value; break;							
						case "Uf": this.str.Uf = (string)value; break;							
						case "Cep": this.str.Cep = (string)value; break;							
						case "Telefone": this.str.Telefone = (string)value; break;							
						case "Fax": this.str.Fax = (string)value; break;							
						case "CorretagemAdicional": this.str.CorretagemAdicional = (string)value; break;							
						case "FuncaoCorretora": this.str.FuncaoCorretora = (string)value; break;							
						case "FuncaoAdministrador": this.str.FuncaoAdministrador = (string)value; break;							
						case "FuncaoGestor": this.str.FuncaoGestor = (string)value; break;							
						case "FuncaoLiquidante": this.str.FuncaoLiquidante = (string)value; break;							
						case "FuncaoCustodiante": this.str.FuncaoCustodiante = (string)value; break;							
						case "FuncaoDistribuidor": this.str.FuncaoDistribuidor = (string)value; break;							
						case "InfosComplementares": this.str.InfosComplementares = (string)value; break;							
						case "CodigoAnbid": this.str.CodigoAnbid = (string)value; break;							
						case "Ddd": this.str.Ddd = (string)value; break;							
						case "Ramal": this.str.Ramal = (string)value; break;							
						case "Email": this.str.Email = (string)value; break;							
						case "CPFResponsavel": this.str.CPFResponsavel = (string)value; break;							
						case "CPFResponsavel2": this.str.CPFResponsavel2 = (string)value; break;							
						case "NomeResponsavel": this.str.NomeResponsavel = (string)value; break;							
						case "Website": this.str.Website = (string)value; break;							
						case "TelOuvidoria": this.str.TelOuvidoria = (string)value; break;							
						case "EmailOuvidoria": this.str.EmailOuvidoria = (string)value; break;							
						case "DigitoCodigoBovespa": this.str.DigitoCodigoBovespa = (string)value; break;							
						case "DigitoCodigoBMF": this.str.DigitoCodigoBMF = (string)value; break;							
						case "CartaPatente": this.str.CartaPatente = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "CodigoCetip": this.str.CodigoCetip = (string)value; break;							
						case "EmpSecuritizada": this.str.EmpSecuritizada = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "CodigoBovespa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBovespa = (System.Int32?)value;
							break;
						
						case "CodigoBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBMF = (System.Int32?)value;
							break;
						
						case "CorretagemAdicional":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CorretagemAdicional = (System.Byte?)value;
							break;
						
						case "DigitoCodigoBovespa":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DigitoCodigoBovespa = (System.Byte?)value;
							break;
						
						case "DigitoCodigoBMF":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DigitoCodigoBMF = (System.Byte?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgenteMercado.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(AgenteMercadoMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(AgenteMercadoMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CodigoBovespa
		/// </summary>
		virtual public System.Int32? CodigoBovespa
		{
			get
			{
				return base.GetSystemInt32(AgenteMercadoMetadata.ColumnNames.CodigoBovespa);
			}
			
			set
			{
				base.SetSystemInt32(AgenteMercadoMetadata.ColumnNames.CodigoBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CodigoBMF
		/// </summary>
		virtual public System.Int32? CodigoBMF
		{
			get
			{
				return base.GetSystemInt32(AgenteMercadoMetadata.ColumnNames.CodigoBMF);
			}
			
			set
			{
				base.SetSystemInt32(AgenteMercadoMetadata.ColumnNames.CodigoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CNPJ
		/// </summary>
		virtual public System.String Cnpj
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Cnpj);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Cnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Endereco
		/// </summary>
		virtual public System.String Endereco
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Endereco);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Endereco, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Bairro
		/// </summary>
		virtual public System.String Bairro
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Bairro);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Bairro, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Cidade
		/// </summary>
		virtual public System.String Cidade
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Cidade);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Cidade, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.UF
		/// </summary>
		virtual public System.String Uf
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Uf);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Uf, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CEP
		/// </summary>
		virtual public System.String Cep
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Cep);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Cep, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Telefone
		/// </summary>
		virtual public System.String Telefone
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Telefone);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Telefone, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Fax
		/// </summary>
		virtual public System.String Fax
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Fax);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Fax, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CorretagemAdicional
		/// </summary>
		virtual public System.Byte? CorretagemAdicional
		{
			get
			{
				return base.GetSystemByte(AgenteMercadoMetadata.ColumnNames.CorretagemAdicional);
			}
			
			set
			{
				base.SetSystemByte(AgenteMercadoMetadata.ColumnNames.CorretagemAdicional, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoCorretora
		/// </summary>
		virtual public System.String FuncaoCorretora
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoCorretora);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoAdministrador
		/// </summary>
		virtual public System.String FuncaoAdministrador
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoAdministrador);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoAdministrador, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoGestor
		/// </summary>
		virtual public System.String FuncaoGestor
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoGestor);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoLiquidante
		/// </summary>
		virtual public System.String FuncaoLiquidante
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoLiquidante);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoLiquidante, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoCustodiante
		/// </summary>
		virtual public System.String FuncaoCustodiante
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoCustodiante);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoCustodiante, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.FuncaoDistribuidor
		/// </summary>
		virtual public System.String FuncaoDistribuidor
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoDistribuidor);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.FuncaoDistribuidor, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.InfosComplementares
		/// </summary>
		virtual public System.String InfosComplementares
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.InfosComplementares);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.InfosComplementares, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CodigoAnbid
		/// </summary>
		virtual public System.String CodigoAnbid
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.CodigoAnbid);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.CodigoAnbid, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.DDD
		/// </summary>
		virtual public System.String Ddd
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Ddd);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Ddd, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Ramal
		/// </summary>
		virtual public System.String Ramal
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Ramal);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Ramal, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Email
		/// </summary>
		virtual public System.String Email
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Email);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Email, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CPFResponsavel
		/// </summary>
		virtual public System.String CPFResponsavel
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.CPFResponsavel);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.CPFResponsavel, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CPFResponsavel2
		/// </summary>
		virtual public System.String CPFResponsavel2
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.CPFResponsavel2);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.CPFResponsavel2, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.NomeResponsavel
		/// </summary>
		virtual public System.String NomeResponsavel
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.NomeResponsavel);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.NomeResponsavel, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Website
		/// </summary>
		virtual public System.String Website
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Website);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Website, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.TelOuvidoria
		/// </summary>
		virtual public System.String TelOuvidoria
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.TelOuvidoria);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.TelOuvidoria, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.EmailOuvidoria
		/// </summary>
		virtual public System.String EmailOuvidoria
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.EmailOuvidoria);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.EmailOuvidoria, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.DigitoCodigoBovespa
		/// </summary>
		virtual public System.Byte? DigitoCodigoBovespa
		{
			get
			{
				return base.GetSystemByte(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa);
			}
			
			set
			{
				base.SetSystemByte(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.DigitoCodigoBMF
		/// </summary>
		virtual public System.Byte? DigitoCodigoBMF
		{
			get
			{
				return base.GetSystemByte(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF);
			}
			
			set
			{
				base.SetSystemByte(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CartaPatente
		/// </summary>
		virtual public System.String CartaPatente
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.CartaPatente);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.CartaPatente, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.CodigoCetip
		/// </summary>
		virtual public System.String CodigoCetip
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.CodigoCetip);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.CodigoCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.EmpSecuritizada
		/// </summary>
		virtual public System.String EmpSecuritizada
		{
			get
			{
				return base.GetSystemString(AgenteMercadoMetadata.ColumnNames.EmpSecuritizada);
			}
			
			set
			{
				base.SetSystemString(AgenteMercadoMetadata.ColumnNames.EmpSecuritizada, value);
			}
		}
		
		/// <summary>
		/// Maps to AgenteMercado.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(AgenteMercadoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(AgenteMercadoMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgenteMercado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBovespa
			{
				get
				{
					System.Int32? data = entity.CodigoBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBovespa = null;
					else entity.CodigoBovespa = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoBMF
			{
				get
				{
					System.Int32? data = entity.CodigoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBMF = null;
					else entity.CodigoBMF = Convert.ToInt32(value);
				}
			}
				
			public System.String Cnpj
			{
				get
				{
					System.String data = entity.Cnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cnpj = null;
					else entity.Cnpj = Convert.ToString(value);
				}
			}
				
			public System.String Endereco
			{
				get
				{
					System.String data = entity.Endereco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Endereco = null;
					else entity.Endereco = Convert.ToString(value);
				}
			}
				
			public System.String Bairro
			{
				get
				{
					System.String data = entity.Bairro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Bairro = null;
					else entity.Bairro = Convert.ToString(value);
				}
			}
				
			public System.String Cidade
			{
				get
				{
					System.String data = entity.Cidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cidade = null;
					else entity.Cidade = Convert.ToString(value);
				}
			}
				
			public System.String Uf
			{
				get
				{
					System.String data = entity.Uf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Uf = null;
					else entity.Uf = Convert.ToString(value);
				}
			}
				
			public System.String Cep
			{
				get
				{
					System.String data = entity.Cep;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cep = null;
					else entity.Cep = Convert.ToString(value);
				}
			}
				
			public System.String Telefone
			{
				get
				{
					System.String data = entity.Telefone;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Telefone = null;
					else entity.Telefone = Convert.ToString(value);
				}
			}
				
			public System.String Fax
			{
				get
				{
					System.String data = entity.Fax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fax = null;
					else entity.Fax = Convert.ToString(value);
				}
			}
				
			public System.String CorretagemAdicional
			{
				get
				{
					System.Byte? data = entity.CorretagemAdicional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CorretagemAdicional = null;
					else entity.CorretagemAdicional = Convert.ToByte(value);
				}
			}
				
			public System.String FuncaoCorretora
			{
				get
				{
					System.String data = entity.FuncaoCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoCorretora = null;
					else entity.FuncaoCorretora = Convert.ToString(value);
				}
			}
				
			public System.String FuncaoAdministrador
			{
				get
				{
					System.String data = entity.FuncaoAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoAdministrador = null;
					else entity.FuncaoAdministrador = Convert.ToString(value);
				}
			}
				
			public System.String FuncaoGestor
			{
				get
				{
					System.String data = entity.FuncaoGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoGestor = null;
					else entity.FuncaoGestor = Convert.ToString(value);
				}
			}
				
			public System.String FuncaoLiquidante
			{
				get
				{
					System.String data = entity.FuncaoLiquidante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoLiquidante = null;
					else entity.FuncaoLiquidante = Convert.ToString(value);
				}
			}
				
			public System.String FuncaoCustodiante
			{
				get
				{
					System.String data = entity.FuncaoCustodiante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoCustodiante = null;
					else entity.FuncaoCustodiante = Convert.ToString(value);
				}
			}
				
			public System.String FuncaoDistribuidor
			{
				get
				{
					System.String data = entity.FuncaoDistribuidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FuncaoDistribuidor = null;
					else entity.FuncaoDistribuidor = Convert.ToString(value);
				}
			}
				
			public System.String InfosComplementares
			{
				get
				{
					System.String data = entity.InfosComplementares;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InfosComplementares = null;
					else entity.InfosComplementares = Convert.ToString(value);
				}
			}
				
			public System.String CodigoAnbid
			{
				get
				{
					System.String data = entity.CodigoAnbid;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAnbid = null;
					else entity.CodigoAnbid = Convert.ToString(value);
				}
			}
				
			public System.String Ddd
			{
				get
				{
					System.String data = entity.Ddd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ddd = null;
					else entity.Ddd = Convert.ToString(value);
				}
			}
				
			public System.String Ramal
			{
				get
				{
					System.String data = entity.Ramal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ramal = null;
					else entity.Ramal = Convert.ToString(value);
				}
			}
				
			public System.String Email
			{
				get
				{
					System.String data = entity.Email;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Email = null;
					else entity.Email = Convert.ToString(value);
				}
			}
				
			public System.String CPFResponsavel
			{
				get
				{
					System.String data = entity.CPFResponsavel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CPFResponsavel = null;
					else entity.CPFResponsavel = Convert.ToString(value);
				}
			}
				
			public System.String CPFResponsavel2
			{
				get
				{
					System.String data = entity.CPFResponsavel2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CPFResponsavel2 = null;
					else entity.CPFResponsavel2 = Convert.ToString(value);
				}
			}
				
			public System.String NomeResponsavel
			{
				get
				{
					System.String data = entity.NomeResponsavel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeResponsavel = null;
					else entity.NomeResponsavel = Convert.ToString(value);
				}
			}
				
			public System.String Website
			{
				get
				{
					System.String data = entity.Website;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Website = null;
					else entity.Website = Convert.ToString(value);
				}
			}
				
			public System.String TelOuvidoria
			{
				get
				{
					System.String data = entity.TelOuvidoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TelOuvidoria = null;
					else entity.TelOuvidoria = Convert.ToString(value);
				}
			}
				
			public System.String EmailOuvidoria
			{
				get
				{
					System.String data = entity.EmailOuvidoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmailOuvidoria = null;
					else entity.EmailOuvidoria = Convert.ToString(value);
				}
			}
				
			public System.String DigitoCodigoBovespa
			{
				get
				{
					System.Byte? data = entity.DigitoCodigoBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitoCodigoBovespa = null;
					else entity.DigitoCodigoBovespa = Convert.ToByte(value);
				}
			}
				
			public System.String DigitoCodigoBMF
			{
				get
				{
					System.Byte? data = entity.DigitoCodigoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitoCodigoBMF = null;
					else entity.DigitoCodigoBMF = Convert.ToByte(value);
				}
			}
				
			public System.String CartaPatente
			{
				get
				{
					System.String data = entity.CartaPatente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CartaPatente = null;
					else entity.CartaPatente = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCetip
			{
				get
				{
					System.String data = entity.CodigoCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCetip = null;
					else entity.CodigoCetip = Convert.ToString(value);
				}
			}
				
			public System.String EmpSecuritizada
			{
				get
				{
					System.String data = entity.EmpSecuritizada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmpSecuritizada = null;
					else entity.EmpSecuritizada = Convert.ToString(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
			

			private esAgenteMercado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgenteMercadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgenteMercado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgenteMercado : esAgenteMercado
	{

				
		#region AcumuladoCorretagemBMFCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_CorretagemAcumuladaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AcumuladoCorretagemBMFCollection AcumuladoCorretagemBMFCollectionByIdAgente
		{
			get
			{
				if(this._AcumuladoCorretagemBMFCollectionByIdAgente == null)
				{
					this._AcumuladoCorretagemBMFCollectionByIdAgente = new AcumuladoCorretagemBMFCollection();
					this._AcumuladoCorretagemBMFCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AcumuladoCorretagemBMFCollectionByIdAgente", this._AcumuladoCorretagemBMFCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._AcumuladoCorretagemBMFCollectionByIdAgente.Query.Where(this._AcumuladoCorretagemBMFCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._AcumuladoCorretagemBMFCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._AcumuladoCorretagemBMFCollectionByIdAgente.fks.Add(AcumuladoCorretagemBMFMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._AcumuladoCorretagemBMFCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AcumuladoCorretagemBMFCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("AcumuladoCorretagemBMFCollectionByIdAgente"); 
					this._AcumuladoCorretagemBMFCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private AcumuladoCorretagemBMFCollection _AcumuladoCorretagemBMFCollectionByIdAgente;
		#endregion

				
		#region CalculoRebateCarteiraCollectionByIdAgenteDistribuidor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_CalculoRebateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoRebateCarteiraCollection CalculoRebateCarteiraCollectionByIdAgenteDistribuidor
		{
			get
			{
				if(this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor == null)
				{
					this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor = new CalculoRebateCarteiraCollection();
					this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoRebateCarteiraCollectionByIdAgenteDistribuidor", this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor);
				
					if(this.IdAgente != null)
					{
						this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor.Query.Where(this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor.Query.IdAgenteDistribuidor == this.IdAgente);
						this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor.fks.Add(CalculoRebateCarteiraMetadata.ColumnNames.IdAgenteDistribuidor, this.IdAgente);
					}
				}

				return this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor != null) 
				{ 
					this.RemovePostSave("CalculoRebateCarteiraCollectionByIdAgenteDistribuidor"); 
					this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor = null;
					
				} 
			} 			
		}

		private CalculoRebateCarteiraCollection _CalculoRebateCarteiraCollectionByIdAgenteDistribuidor;
		#endregion

				
		#region CarteiraCollectionByIdAgenteGestor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdAgenteGestor
		{
			get
			{
				if(this._CarteiraCollectionByIdAgenteGestor == null)
				{
					this._CarteiraCollectionByIdAgenteGestor = new CarteiraCollection();
					this._CarteiraCollectionByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdAgenteGestor", this._CarteiraCollectionByIdAgenteGestor);
				
					if(this.IdAgente != null)
					{
						this._CarteiraCollectionByIdAgenteGestor.Query.Where(this._CarteiraCollectionByIdAgenteGestor.Query.IdAgenteGestor == this.IdAgente);
						this._CarteiraCollectionByIdAgenteGestor.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdAgenteGestor.fks.Add(CarteiraMetadata.ColumnNames.IdAgenteGestor, this.IdAgente);
					}
				}

				return this._CarteiraCollectionByIdAgenteGestor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdAgenteGestor != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdAgenteGestor"); 
					this._CarteiraCollectionByIdAgenteGestor = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdAgenteGestor;
		#endregion

				
		#region CarteiraCollectionByIdAgenteAdministrador - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdAgenteAdministrador
		{
			get
			{
				if(this._CarteiraCollectionByIdAgenteAdministrador == null)
				{
					this._CarteiraCollectionByIdAgenteAdministrador = new CarteiraCollection();
					this._CarteiraCollectionByIdAgenteAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdAgenteAdministrador", this._CarteiraCollectionByIdAgenteAdministrador);
				
					if(this.IdAgente != null)
					{
						this._CarteiraCollectionByIdAgenteAdministrador.Query.Where(this._CarteiraCollectionByIdAgenteAdministrador.Query.IdAgenteAdministrador == this.IdAgente);
						this._CarteiraCollectionByIdAgenteAdministrador.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdAgenteAdministrador.fks.Add(CarteiraMetadata.ColumnNames.IdAgenteAdministrador, this.IdAgente);
					}
				}

				return this._CarteiraCollectionByIdAgenteAdministrador;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdAgenteAdministrador != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdAgenteAdministrador"); 
					this._CarteiraCollectionByIdAgenteAdministrador = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdAgenteAdministrador;
		#endregion

				
		#region CodigoClienteAgenteCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_CodigoClienteAgente_FK1
		/// </summary>

		[XmlIgnore]
		public CodigoClienteAgenteCollection CodigoClienteAgenteCollectionByIdAgente
		{
			get
			{
				if(this._CodigoClienteAgenteCollectionByIdAgente == null)
				{
					this._CodigoClienteAgenteCollectionByIdAgente = new CodigoClienteAgenteCollection();
					this._CodigoClienteAgenteCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CodigoClienteAgenteCollectionByIdAgente", this._CodigoClienteAgenteCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._CodigoClienteAgenteCollectionByIdAgente.Query.Where(this._CodigoClienteAgenteCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._CodigoClienteAgenteCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CodigoClienteAgenteCollectionByIdAgente.fks.Add(CodigoClienteAgenteMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._CodigoClienteAgenteCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CodigoClienteAgenteCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("CodigoClienteAgenteCollectionByIdAgente"); 
					this._CodigoClienteAgenteCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private CodigoClienteAgenteCollection _CodigoClienteAgenteCollectionByIdAgente;
		#endregion

				
		#region CorretoraCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_Corretora_FK1
		/// </summary>

		[XmlIgnore]
		public CorretoraCollection CorretoraCollectionByIdAgente
		{
			get
			{
				if(this._CorretoraCollectionByIdAgente == null)
				{
					this._CorretoraCollectionByIdAgente = new CorretoraCollection();
					this._CorretoraCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CorretoraCollectionByIdAgente", this._CorretoraCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._CorretoraCollectionByIdAgente.Query.Where(this._CorretoraCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._CorretoraCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CorretoraCollectionByIdAgente.fks.Add(CorretoraMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._CorretoraCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CorretoraCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("CorretoraCollectionByIdAgente"); 
					this._CorretoraCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private CorretoraCollection _CorretoraCollectionByIdAgente;
		#endregion

				
		#region CustodiaBMFCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public CustodiaBMFCollection CustodiaBMFCollectionByIdAgente
		{
			get
			{
				if(this._CustodiaBMFCollectionByIdAgente == null)
				{
					this._CustodiaBMFCollectionByIdAgente = new CustodiaBMFCollection();
					this._CustodiaBMFCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CustodiaBMFCollectionByIdAgente", this._CustodiaBMFCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._CustodiaBMFCollectionByIdAgente.Query.Where(this._CustodiaBMFCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._CustodiaBMFCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CustodiaBMFCollectionByIdAgente.fks.Add(CustodiaBMFMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._CustodiaBMFCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CustodiaBMFCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("CustodiaBMFCollectionByIdAgente"); 
					this._CustodiaBMFCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private CustodiaBMFCollection _CustodiaBMFCollectionByIdAgente;
		#endregion

				
		#region EmissorCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Emissor_AgenteMercado
		/// </summary>

		[XmlIgnore]
		public EmissorCollection EmissorCollectionByIdAgente
		{
			get
			{
				if(this._EmissorCollectionByIdAgente == null)
				{
					this._EmissorCollectionByIdAgente = new EmissorCollection();
					this._EmissorCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EmissorCollectionByIdAgente", this._EmissorCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._EmissorCollectionByIdAgente.Query.Where(this._EmissorCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._EmissorCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._EmissorCollectionByIdAgente.fks.Add(EmissorMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._EmissorCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EmissorCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("EmissorCollectionByIdAgente"); 
					this._EmissorCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private EmissorCollection _EmissorCollectionByIdAgente;
		#endregion

				
		#region EmpresaSecuritizadaCollectionByIdAgenteMercado - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__EmpresaSe__IdAge__18C19800
		/// </summary>

		[XmlIgnore]
		public EmpresaSecuritizadaCollection EmpresaSecuritizadaCollectionByIdAgenteMercado
		{
			get
			{
				if(this._EmpresaSecuritizadaCollectionByIdAgenteMercado == null)
				{
					this._EmpresaSecuritizadaCollectionByIdAgenteMercado = new EmpresaSecuritizadaCollection();
					this._EmpresaSecuritizadaCollectionByIdAgenteMercado.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EmpresaSecuritizadaCollectionByIdAgenteMercado", this._EmpresaSecuritizadaCollectionByIdAgenteMercado);
				
					if(this.IdAgente != null)
					{
						this._EmpresaSecuritizadaCollectionByIdAgenteMercado.Query.Where(this._EmpresaSecuritizadaCollectionByIdAgenteMercado.Query.IdAgenteMercado == this.IdAgente);
						this._EmpresaSecuritizadaCollectionByIdAgenteMercado.Query.Load();

						// Auto-hookup Foreign Keys
						this._EmpresaSecuritizadaCollectionByIdAgenteMercado.fks.Add(EmpresaSecuritizadaMetadata.ColumnNames.IdAgenteMercado, this.IdAgente);
					}
				}

				return this._EmpresaSecuritizadaCollectionByIdAgenteMercado;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EmpresaSecuritizadaCollectionByIdAgenteMercado != null) 
				{ 
					this.RemovePostSave("EmpresaSecuritizadaCollectionByIdAgenteMercado"); 
					this._EmpresaSecuritizadaCollectionByIdAgenteMercado = null;
					
				} 
			} 			
		}

		private EmpresaSecuritizadaCollection _EmpresaSecuritizadaCollectionByIdAgenteMercado;
		#endregion

				
		#region EnquadraItemFundoCollectionByIdAgenteAdministrador - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFundoCollection EnquadraItemFundoCollectionByIdAgenteAdministrador
		{
			get
			{
				if(this._EnquadraItemFundoCollectionByIdAgenteAdministrador == null)
				{
					this._EnquadraItemFundoCollectionByIdAgenteAdministrador = new EnquadraItemFundoCollection();
					this._EnquadraItemFundoCollectionByIdAgenteAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemFundoCollectionByIdAgenteAdministrador", this._EnquadraItemFundoCollectionByIdAgenteAdministrador);
				
					if(this.IdAgente != null)
					{
						this._EnquadraItemFundoCollectionByIdAgenteAdministrador.Query.Where(this._EnquadraItemFundoCollectionByIdAgenteAdministrador.Query.IdAgenteAdministrador == this.IdAgente);
						this._EnquadraItemFundoCollectionByIdAgenteAdministrador.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemFundoCollectionByIdAgenteAdministrador.fks.Add(EnquadraItemFundoMetadata.ColumnNames.IdAgenteAdministrador, this.IdAgente);
					}
				}

				return this._EnquadraItemFundoCollectionByIdAgenteAdministrador;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFundoCollectionByIdAgenteAdministrador != null) 
				{ 
					this.RemovePostSave("EnquadraItemFundoCollectionByIdAgenteAdministrador"); 
					this._EnquadraItemFundoCollectionByIdAgenteAdministrador = null;
					
				} 
			} 			
		}

		private EnquadraItemFundoCollection _EnquadraItemFundoCollectionByIdAgenteAdministrador;
		#endregion

				
		#region EnquadraItemFundoCollectionByIdAgenteGestor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_EnquadraItemFundo_FK2
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFundoCollection EnquadraItemFundoCollectionByIdAgenteGestor
		{
			get
			{
				if(this._EnquadraItemFundoCollectionByIdAgenteGestor == null)
				{
					this._EnquadraItemFundoCollectionByIdAgenteGestor = new EnquadraItemFundoCollection();
					this._EnquadraItemFundoCollectionByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemFundoCollectionByIdAgenteGestor", this._EnquadraItemFundoCollectionByIdAgenteGestor);
				
					if(this.IdAgente != null)
					{
						this._EnquadraItemFundoCollectionByIdAgenteGestor.Query.Where(this._EnquadraItemFundoCollectionByIdAgenteGestor.Query.IdAgenteGestor == this.IdAgente);
						this._EnquadraItemFundoCollectionByIdAgenteGestor.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemFundoCollectionByIdAgenteGestor.fks.Add(EnquadraItemFundoMetadata.ColumnNames.IdAgenteGestor, this.IdAgente);
					}
				}

				return this._EnquadraItemFundoCollectionByIdAgenteGestor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFundoCollectionByIdAgenteGestor != null) 
				{ 
					this.RemovePostSave("EnquadraItemFundoCollectionByIdAgenteGestor"); 
					this._EnquadraItemFundoCollectionByIdAgenteGestor = null;
					
				} 
			} 			
		}

		private EnquadraItemFundoCollection _EnquadraItemFundoCollectionByIdAgenteGestor;
		#endregion

				
		#region EventoFisicoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFisicoBolsaCollection EventoFisicoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._EventoFisicoBolsaCollectionByIdAgente == null)
				{
					this._EventoFisicoBolsaCollectionByIdAgente = new EventoFisicoBolsaCollection();
					this._EventoFisicoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFisicoBolsaCollectionByIdAgente", this._EventoFisicoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._EventoFisicoBolsaCollectionByIdAgente.Query.Where(this._EventoFisicoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._EventoFisicoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFisicoBolsaCollectionByIdAgente.fks.Add(EventoFisicoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._EventoFisicoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFisicoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("EventoFisicoBolsaCollectionByIdAgente"); 
					this._EventoFisicoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private EventoFisicoBolsaCollection _EventoFisicoBolsaCollectionByIdAgente;
		#endregion

				
		#region IRFonteCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_IRFonte_FK1
		/// </summary>

		[XmlIgnore]
		public IRFonteCollection IRFonteCollectionByIdAgente
		{
			get
			{
				if(this._IRFonteCollectionByIdAgente == null)
				{
					this._IRFonteCollectionByIdAgente = new IRFonteCollection();
					this._IRFonteCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("IRFonteCollectionByIdAgente", this._IRFonteCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._IRFonteCollectionByIdAgente.Query.Where(this._IRFonteCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._IRFonteCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._IRFonteCollectionByIdAgente.fks.Add(IRFonteMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._IRFonteCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._IRFonteCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("IRFonteCollectionByIdAgente"); 
					this._IRFonteCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private IRFonteCollection _IRFonteCollectionByIdAgente;
		#endregion

				
		#region LiquidacaoEmprestimoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoEmprestimoBolsaCollection LiquidacaoEmprestimoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._LiquidacaoEmprestimoBolsaCollectionByIdAgente == null)
				{
					this._LiquidacaoEmprestimoBolsaCollectionByIdAgente = new LiquidacaoEmprestimoBolsaCollection();
					this._LiquidacaoEmprestimoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoEmprestimoBolsaCollectionByIdAgente", this._LiquidacaoEmprestimoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._LiquidacaoEmprestimoBolsaCollectionByIdAgente.Query.Where(this._LiquidacaoEmprestimoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._LiquidacaoEmprestimoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoEmprestimoBolsaCollectionByIdAgente.fks.Add(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._LiquidacaoEmprestimoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoEmprestimoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("LiquidacaoEmprestimoBolsaCollectionByIdAgente"); 
					this._LiquidacaoEmprestimoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private LiquidacaoEmprestimoBolsaCollection _LiquidacaoEmprestimoBolsaCollectionByIdAgente;
		#endregion

				
		#region LiquidacaoFuturoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_LiquidacaoFuturo_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoFuturoCollection LiquidacaoFuturoCollectionByIdAgente
		{
			get
			{
				if(this._LiquidacaoFuturoCollectionByIdAgente == null)
				{
					this._LiquidacaoFuturoCollectionByIdAgente = new LiquidacaoFuturoCollection();
					this._LiquidacaoFuturoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoFuturoCollectionByIdAgente", this._LiquidacaoFuturoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._LiquidacaoFuturoCollectionByIdAgente.Query.Where(this._LiquidacaoFuturoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._LiquidacaoFuturoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoFuturoCollectionByIdAgente.fks.Add(LiquidacaoFuturoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._LiquidacaoFuturoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoFuturoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("LiquidacaoFuturoCollectionByIdAgente"); 
					this._LiquidacaoFuturoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private LiquidacaoFuturoCollection _LiquidacaoFuturoCollectionByIdAgente;
		#endregion

				
		#region LiquidacaoHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_LiquidacaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoHistoricoCollection LiquidacaoHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._LiquidacaoHistoricoCollectionByIdAgente == null)
				{
					this._LiquidacaoHistoricoCollectionByIdAgente = new LiquidacaoHistoricoCollection();
					this._LiquidacaoHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoHistoricoCollectionByIdAgente", this._LiquidacaoHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._LiquidacaoHistoricoCollectionByIdAgente.Query.Where(this._LiquidacaoHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._LiquidacaoHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoHistoricoCollectionByIdAgente.fks.Add(LiquidacaoHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._LiquidacaoHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("LiquidacaoHistoricoCollectionByIdAgente"); 
					this._LiquidacaoHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private LiquidacaoHistoricoCollection _LiquidacaoHistoricoCollectionByIdAgente;
		#endregion

				
		#region LiquidacaoTermoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoTermoBolsaCollection LiquidacaoTermoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._LiquidacaoTermoBolsaCollectionByIdAgente == null)
				{
					this._LiquidacaoTermoBolsaCollectionByIdAgente = new LiquidacaoTermoBolsaCollection();
					this._LiquidacaoTermoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoTermoBolsaCollectionByIdAgente", this._LiquidacaoTermoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._LiquidacaoTermoBolsaCollectionByIdAgente.Query.Where(this._LiquidacaoTermoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._LiquidacaoTermoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoTermoBolsaCollectionByIdAgente.fks.Add(LiquidacaoTermoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._LiquidacaoTermoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoTermoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("LiquidacaoTermoBolsaCollectionByIdAgente"); 
					this._LiquidacaoTermoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private LiquidacaoTermoBolsaCollection _LiquidacaoTermoBolsaCollectionByIdAgente;
		#endregion

				
		#region OperacaoBMFCollectionByIdAgenteLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBMFCollection OperacaoBMFCollectionByIdAgenteLiquidacao
		{
			get
			{
				if(this._OperacaoBMFCollectionByIdAgenteLiquidacao == null)
				{
					this._OperacaoBMFCollectionByIdAgenteLiquidacao = new OperacaoBMFCollection();
					this._OperacaoBMFCollectionByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBMFCollectionByIdAgenteLiquidacao", this._OperacaoBMFCollectionByIdAgenteLiquidacao);
				
					if(this.IdAgente != null)
					{
						this._OperacaoBMFCollectionByIdAgenteLiquidacao.Query.Where(this._OperacaoBMFCollectionByIdAgenteLiquidacao.Query.IdAgenteLiquidacao == this.IdAgente);
						this._OperacaoBMFCollectionByIdAgenteLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBMFCollectionByIdAgenteLiquidacao.fks.Add(OperacaoBMFMetadata.ColumnNames.IdAgenteLiquidacao, this.IdAgente);
					}
				}

				return this._OperacaoBMFCollectionByIdAgenteLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBMFCollectionByIdAgenteLiquidacao != null) 
				{ 
					this.RemovePostSave("OperacaoBMFCollectionByIdAgenteLiquidacao"); 
					this._OperacaoBMFCollectionByIdAgenteLiquidacao = null;
					
				} 
			} 			
		}

		private OperacaoBMFCollection _OperacaoBMFCollectionByIdAgenteLiquidacao;
		#endregion

				
		#region OperacaoBolsaCollectionByIdAgenteLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByIdAgenteLiquidacao
		{
			get
			{
				if(this._OperacaoBolsaCollectionByIdAgenteLiquidacao == null)
				{
					this._OperacaoBolsaCollectionByIdAgenteLiquidacao = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByIdAgenteLiquidacao", this._OperacaoBolsaCollectionByIdAgenteLiquidacao);
				
					if(this.IdAgente != null)
					{
						this._OperacaoBolsaCollectionByIdAgenteLiquidacao.Query.Where(this._OperacaoBolsaCollectionByIdAgenteLiquidacao.Query.IdAgenteLiquidacao == this.IdAgente);
						this._OperacaoBolsaCollectionByIdAgenteLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByIdAgenteLiquidacao.fks.Add(OperacaoBolsaMetadata.ColumnNames.IdAgenteLiquidacao, this.IdAgente);
					}
				}

				return this._OperacaoBolsaCollectionByIdAgenteLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByIdAgenteLiquidacao != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByIdAgenteLiquidacao"); 
					this._OperacaoBolsaCollectionByIdAgenteLiquidacao = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByIdAgenteLiquidacao;
		#endregion

				
		#region OperacaoBolsaCollectionByIdAgenteCorretora - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OperacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByIdAgenteCorretora
		{
			get
			{
				if(this._OperacaoBolsaCollectionByIdAgenteCorretora == null)
				{
					this._OperacaoBolsaCollectionByIdAgenteCorretora = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByIdAgenteCorretora", this._OperacaoBolsaCollectionByIdAgenteCorretora);
				
					if(this.IdAgente != null)
					{
						this._OperacaoBolsaCollectionByIdAgenteCorretora.Query.Where(this._OperacaoBolsaCollectionByIdAgenteCorretora.Query.IdAgenteCorretora == this.IdAgente);
						this._OperacaoBolsaCollectionByIdAgenteCorretora.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByIdAgenteCorretora.fks.Add(OperacaoBolsaMetadata.ColumnNames.IdAgenteCorretora, this.IdAgente);
					}
				}

				return this._OperacaoBolsaCollectionByIdAgenteCorretora;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByIdAgenteCorretora != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByIdAgenteCorretora"); 
					this._OperacaoBolsaCollectionByIdAgenteCorretora = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByIdAgenteCorretora;
		#endregion

				
		#region OrdemBMFCollectionByIdAgenteCorretora - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBMFCollection OrdemBMFCollectionByIdAgenteCorretora
		{
			get
			{
				if(this._OrdemBMFCollectionByIdAgenteCorretora == null)
				{
					this._OrdemBMFCollectionByIdAgenteCorretora = new OrdemBMFCollection();
					this._OrdemBMFCollectionByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBMFCollectionByIdAgenteCorretora", this._OrdemBMFCollectionByIdAgenteCorretora);
				
					if(this.IdAgente != null)
					{
						this._OrdemBMFCollectionByIdAgenteCorretora.Query.Where(this._OrdemBMFCollectionByIdAgenteCorretora.Query.IdAgenteCorretora == this.IdAgente);
						this._OrdemBMFCollectionByIdAgenteCorretora.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBMFCollectionByIdAgenteCorretora.fks.Add(OrdemBMFMetadata.ColumnNames.IdAgenteCorretora, this.IdAgente);
					}
				}

				return this._OrdemBMFCollectionByIdAgenteCorretora;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBMFCollectionByIdAgenteCorretora != null) 
				{ 
					this.RemovePostSave("OrdemBMFCollectionByIdAgenteCorretora"); 
					this._OrdemBMFCollectionByIdAgenteCorretora = null;
					
				} 
			} 			
		}

		private OrdemBMFCollection _OrdemBMFCollectionByIdAgenteCorretora;
		#endregion

				
		#region OrdemBolsaCollectionByIdAgenteLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByIdAgenteLiquidacao
		{
			get
			{
				if(this._OrdemBolsaCollectionByIdAgenteLiquidacao == null)
				{
					this._OrdemBolsaCollectionByIdAgenteLiquidacao = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByIdAgenteLiquidacao", this._OrdemBolsaCollectionByIdAgenteLiquidacao);
				
					if(this.IdAgente != null)
					{
						this._OrdemBolsaCollectionByIdAgenteLiquidacao.Query.Where(this._OrdemBolsaCollectionByIdAgenteLiquidacao.Query.IdAgenteLiquidacao == this.IdAgente);
						this._OrdemBolsaCollectionByIdAgenteLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByIdAgenteLiquidacao.fks.Add(OrdemBolsaMetadata.ColumnNames.IdAgenteLiquidacao, this.IdAgente);
					}
				}

				return this._OrdemBolsaCollectionByIdAgenteLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByIdAgenteLiquidacao != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByIdAgenteLiquidacao"); 
					this._OrdemBolsaCollectionByIdAgenteLiquidacao = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByIdAgenteLiquidacao;
		#endregion

				
		#region OrdemBolsaCollectionByIdAgenteCorretora - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_OrdemBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByIdAgenteCorretora
		{
			get
			{
				if(this._OrdemBolsaCollectionByIdAgenteCorretora == null)
				{
					this._OrdemBolsaCollectionByIdAgenteCorretora = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByIdAgenteCorretora", this._OrdemBolsaCollectionByIdAgenteCorretora);
				
					if(this.IdAgente != null)
					{
						this._OrdemBolsaCollectionByIdAgenteCorretora.Query.Where(this._OrdemBolsaCollectionByIdAgenteCorretora.Query.IdAgenteCorretora == this.IdAgente);
						this._OrdemBolsaCollectionByIdAgenteCorretora.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByIdAgenteCorretora.fks.Add(OrdemBolsaMetadata.ColumnNames.IdAgenteCorretora, this.IdAgente);
					}
				}

				return this._OrdemBolsaCollectionByIdAgenteCorretora;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByIdAgenteCorretora != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByIdAgenteCorretora"); 
					this._OrdemBolsaCollectionByIdAgenteCorretora = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByIdAgenteCorretora;
		#endregion

				
		#region ParametroAdministradorFundoInvestimentoCollectionByAdministrador - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__Parametro__Admin__24334AAC
		/// </summary>

		[XmlIgnore]
		public ParametroAdministradorFundoInvestimentoCollection ParametroAdministradorFundoInvestimentoCollectionByAdministrador
		{
			get
			{
				if(this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador == null)
				{
					this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador = new ParametroAdministradorFundoInvestimentoCollection();
					this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ParametroAdministradorFundoInvestimentoCollectionByAdministrador", this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador);
				
					if(this.IdAgente != null)
					{
						this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador.Query.Where(this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador.Query.Administrador == this.IdAgente);
						this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador.Query.Load();

						// Auto-hookup Foreign Keys
						this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador.fks.Add(ParametroAdministradorFundoInvestimentoMetadata.ColumnNames.Administrador, this.IdAgente);
					}
				}

				return this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador != null) 
				{ 
					this.RemovePostSave("ParametroAdministradorFundoInvestimentoCollectionByAdministrador"); 
					this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador = null;
					
				} 
			} 			
		}

		private ParametroAdministradorFundoInvestimentoCollection _ParametroAdministradorFundoInvestimentoCollectionByAdministrador;
		#endregion

				
		#region PerfilCorretagemBMFCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PerfilCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBMFCollection PerfilCorretagemBMFCollectionByIdAgente
		{
			get
			{
				if(this._PerfilCorretagemBMFCollectionByIdAgente == null)
				{
					this._PerfilCorretagemBMFCollectionByIdAgente = new PerfilCorretagemBMFCollection();
					this._PerfilCorretagemBMFCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilCorretagemBMFCollectionByIdAgente", this._PerfilCorretagemBMFCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PerfilCorretagemBMFCollectionByIdAgente.Query.Where(this._PerfilCorretagemBMFCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PerfilCorretagemBMFCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilCorretagemBMFCollectionByIdAgente.fks.Add(PerfilCorretagemBMFMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PerfilCorretagemBMFCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilCorretagemBMFCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PerfilCorretagemBMFCollectionByIdAgente"); 
					this._PerfilCorretagemBMFCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PerfilCorretagemBMFCollection _PerfilCorretagemBMFCollectionByIdAgente;
		#endregion

				
		#region PerfilCorretagemBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBolsaCollection PerfilCorretagemBolsaCollectionByIdAgente
		{
			get
			{
				if(this._PerfilCorretagemBolsaCollectionByIdAgente == null)
				{
					this._PerfilCorretagemBolsaCollectionByIdAgente = new PerfilCorretagemBolsaCollection();
					this._PerfilCorretagemBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilCorretagemBolsaCollectionByIdAgente", this._PerfilCorretagemBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PerfilCorretagemBolsaCollectionByIdAgente.Query.Where(this._PerfilCorretagemBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PerfilCorretagemBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilCorretagemBolsaCollectionByIdAgente.fks.Add(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PerfilCorretagemBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilCorretagemBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PerfilCorretagemBolsaCollectionByIdAgente"); 
					this._PerfilCorretagemBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PerfilCorretagemBolsaCollection _PerfilCorretagemBolsaCollectionByIdAgente;
		#endregion

				
		#region PosicaoBMFCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFCollection PosicaoBMFCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBMFCollectionByIdAgente == null)
				{
					this._PosicaoBMFCollectionByIdAgente = new PosicaoBMFCollection();
					this._PosicaoBMFCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFCollectionByIdAgente", this._PosicaoBMFCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBMFCollectionByIdAgente.Query.Where(this._PosicaoBMFCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBMFCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFCollectionByIdAgente.fks.Add(PosicaoBMFMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBMFCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFCollectionByIdAgente"); 
					this._PosicaoBMFCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBMFCollection _PosicaoBMFCollectionByIdAgente;
		#endregion

				
		#region PosicaoBMFAberturaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFAberturaCollection PosicaoBMFAberturaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBMFAberturaCollectionByIdAgente == null)
				{
					this._PosicaoBMFAberturaCollectionByIdAgente = new PosicaoBMFAberturaCollection();
					this._PosicaoBMFAberturaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFAberturaCollectionByIdAgente", this._PosicaoBMFAberturaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBMFAberturaCollectionByIdAgente.Query.Where(this._PosicaoBMFAberturaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBMFAberturaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFAberturaCollectionByIdAgente.fks.Add(PosicaoBMFAberturaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBMFAberturaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFAberturaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFAberturaCollectionByIdAgente"); 
					this._PosicaoBMFAberturaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBMFAberturaCollection _PosicaoBMFAberturaCollectionByIdAgente;
		#endregion

				
		#region PosicaoBMFHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFHistoricoCollection PosicaoBMFHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBMFHistoricoCollectionByIdAgente == null)
				{
					this._PosicaoBMFHistoricoCollectionByIdAgente = new PosicaoBMFHistoricoCollection();
					this._PosicaoBMFHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFHistoricoCollectionByIdAgente", this._PosicaoBMFHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBMFHistoricoCollectionByIdAgente.Query.Where(this._PosicaoBMFHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBMFHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFHistoricoCollectionByIdAgente.fks.Add(PosicaoBMFHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBMFHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFHistoricoCollectionByIdAgente"); 
					this._PosicaoBMFHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBMFHistoricoCollection _PosicaoBMFHistoricoCollectionByIdAgente;
		#endregion

				
		#region PosicaoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaCollection PosicaoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBolsaCollectionByIdAgente == null)
				{
					this._PosicaoBolsaCollectionByIdAgente = new PosicaoBolsaCollection();
					this._PosicaoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaCollectionByIdAgente", this._PosicaoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBolsaCollectionByIdAgente.Query.Where(this._PosicaoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaCollectionByIdAgente.fks.Add(PosicaoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaCollectionByIdAgente"); 
					this._PosicaoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaCollection _PosicaoBolsaCollectionByIdAgente;
		#endregion

				
		#region PosicaoBolsaAberturaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaAberturaCollection PosicaoBolsaAberturaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBolsaAberturaCollectionByIdAgente == null)
				{
					this._PosicaoBolsaAberturaCollectionByIdAgente = new PosicaoBolsaAberturaCollection();
					this._PosicaoBolsaAberturaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaAberturaCollectionByIdAgente", this._PosicaoBolsaAberturaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBolsaAberturaCollectionByIdAgente.Query.Where(this._PosicaoBolsaAberturaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBolsaAberturaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaAberturaCollectionByIdAgente.fks.Add(PosicaoBolsaAberturaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBolsaAberturaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaAberturaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaAberturaCollectionByIdAgente"); 
					this._PosicaoBolsaAberturaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaAberturaCollection _PosicaoBolsaAberturaCollectionByIdAgente;
		#endregion

				
		#region PosicaoBolsaDetalheCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaDetalheCollection PosicaoBolsaDetalheCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBolsaDetalheCollectionByIdAgente == null)
				{
					this._PosicaoBolsaDetalheCollectionByIdAgente = new PosicaoBolsaDetalheCollection();
					this._PosicaoBolsaDetalheCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaDetalheCollectionByIdAgente", this._PosicaoBolsaDetalheCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBolsaDetalheCollectionByIdAgente.Query.Where(this._PosicaoBolsaDetalheCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBolsaDetalheCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaDetalheCollectionByIdAgente.fks.Add(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBolsaDetalheCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaDetalheCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaDetalheCollectionByIdAgente"); 
					this._PosicaoBolsaDetalheCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaDetalheCollection _PosicaoBolsaDetalheCollectionByIdAgente;
		#endregion

				
		#region PosicaoBolsaHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaHistoricoCollection PosicaoBolsaHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoBolsaHistoricoCollectionByIdAgente == null)
				{
					this._PosicaoBolsaHistoricoCollectionByIdAgente = new PosicaoBolsaHistoricoCollection();
					this._PosicaoBolsaHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaHistoricoCollectionByIdAgente", this._PosicaoBolsaHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoBolsaHistoricoCollectionByIdAgente.Query.Where(this._PosicaoBolsaHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoBolsaHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaHistoricoCollectionByIdAgente.fks.Add(PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoBolsaHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaHistoricoCollectionByIdAgente"); 
					this._PosicaoBolsaHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaHistoricoCollection _PosicaoBolsaHistoricoCollectionByIdAgente;
		#endregion

				
		#region PosicaoEmprestimoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaCollection PosicaoEmprestimoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaCollectionByIdAgente == null)
				{
					this._PosicaoEmprestimoBolsaCollectionByIdAgente = new PosicaoEmprestimoBolsaCollection();
					this._PosicaoEmprestimoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaCollectionByIdAgente", this._PosicaoEmprestimoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoEmprestimoBolsaCollectionByIdAgente.Query.Where(this._PosicaoEmprestimoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoEmprestimoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaCollectionByIdAgente.fks.Add(PosicaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoEmprestimoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaCollectionByIdAgente"); 
					this._PosicaoEmprestimoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaCollection _PosicaoEmprestimoBolsaCollectionByIdAgente;
		#endregion

				
		#region PosicaoEmprestimoBolsaAberturaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoEmprestimoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaAberturaCollection PosicaoEmprestimoBolsaAberturaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente == null)
				{
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente = new PosicaoEmprestimoBolsaAberturaCollection();
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdAgente", this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente.Query.Where(this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente.fks.Add(PosicaoEmprestimoBolsaAberturaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdAgente"); 
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaAberturaCollection _PosicaoEmprestimoBolsaAberturaCollectionByIdAgente;
		#endregion

				
		#region PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaHistoricoCollection PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente == null)
				{
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente = new PosicaoEmprestimoBolsaHistoricoCollection();
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente", this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente.Query.Where(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente.fks.Add(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente"); 
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaHistoricoCollection _PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente;
		#endregion

				
		#region PosicaoSwapCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdAgente == null)
				{
					this._PosicaoSwapCollectionByIdAgente = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdAgente", this._PosicaoSwapCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoSwapCollectionByIdAgente.Query.Where(this._PosicaoSwapCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoSwapCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdAgente.fks.Add(PosicaoSwapMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoSwapCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdAgente"); 
					this._PosicaoSwapCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdAgente;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdAgente == null)
				{
					this._PosicaoSwapAberturaCollectionByIdAgente = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdAgente", this._PosicaoSwapAberturaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoSwapAberturaCollectionByIdAgente.Query.Where(this._PosicaoSwapAberturaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoSwapAberturaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdAgente.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdAgente"); 
					this._PosicaoSwapAberturaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdAgente;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoSwapHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdAgente == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdAgente = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdAgente", this._PosicaoSwapHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdAgente.Query.Where(this._PosicaoSwapHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoSwapHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdAgente.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdAgente"); 
					this._PosicaoSwapHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdAgente;
		#endregion

				
		#region PosicaoTermoBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoTermoBolsaCollectionByIdAgente == null)
				{
					this._PosicaoTermoBolsaCollectionByIdAgente = new PosicaoTermoBolsaCollection();
					this._PosicaoTermoBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaCollectionByIdAgente", this._PosicaoTermoBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoTermoBolsaCollectionByIdAgente.Query.Where(this._PosicaoTermoBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoTermoBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaCollectionByIdAgente.fks.Add(PosicaoTermoBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoTermoBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaCollectionByIdAgente"); 
					this._PosicaoTermoBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByIdAgente;
		#endregion

				
		#region PosicaoTermoBolsaAberturaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaAberturaCollection PosicaoTermoBolsaAberturaCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoTermoBolsaAberturaCollectionByIdAgente == null)
				{
					this._PosicaoTermoBolsaAberturaCollectionByIdAgente = new PosicaoTermoBolsaAberturaCollection();
					this._PosicaoTermoBolsaAberturaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaAberturaCollectionByIdAgente", this._PosicaoTermoBolsaAberturaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoTermoBolsaAberturaCollectionByIdAgente.Query.Where(this._PosicaoTermoBolsaAberturaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoTermoBolsaAberturaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaAberturaCollectionByIdAgente.fks.Add(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoTermoBolsaAberturaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaAberturaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaAberturaCollectionByIdAgente"); 
					this._PosicaoTermoBolsaAberturaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaAberturaCollection _PosicaoTermoBolsaAberturaCollectionByIdAgente;
		#endregion

				
		#region PosicaoTermoBolsaHistoricoCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_PosicaoTermoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaHistoricoCollection PosicaoTermoBolsaHistoricoCollectionByIdAgente
		{
			get
			{
				if(this._PosicaoTermoBolsaHistoricoCollectionByIdAgente == null)
				{
					this._PosicaoTermoBolsaHistoricoCollectionByIdAgente = new PosicaoTermoBolsaHistoricoCollection();
					this._PosicaoTermoBolsaHistoricoCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaHistoricoCollectionByIdAgente", this._PosicaoTermoBolsaHistoricoCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._PosicaoTermoBolsaHistoricoCollectionByIdAgente.Query.Where(this._PosicaoTermoBolsaHistoricoCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._PosicaoTermoBolsaHistoricoCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaHistoricoCollectionByIdAgente.fks.Add(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._PosicaoTermoBolsaHistoricoCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaHistoricoCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaHistoricoCollectionByIdAgente"); 
					this._PosicaoTermoBolsaHistoricoCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByIdAgente;
		#endregion

				
		#region TabelaRebateCorretagemCollectionByIdAgenteCorretora - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TabelaRebateCorretagem_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateCorretagemCollection TabelaRebateCorretagemCollectionByIdAgenteCorretora
		{
			get
			{
				if(this._TabelaRebateCorretagemCollectionByIdAgenteCorretora == null)
				{
					this._TabelaRebateCorretagemCollectionByIdAgenteCorretora = new TabelaRebateCorretagemCollection();
					this._TabelaRebateCorretagemCollectionByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateCorretagemCollectionByIdAgenteCorretora", this._TabelaRebateCorretagemCollectionByIdAgenteCorretora);
				
					if(this.IdAgente != null)
					{
						this._TabelaRebateCorretagemCollectionByIdAgenteCorretora.Query.Where(this._TabelaRebateCorretagemCollectionByIdAgenteCorretora.Query.IdAgenteCorretora == this.IdAgente);
						this._TabelaRebateCorretagemCollectionByIdAgenteCorretora.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateCorretagemCollectionByIdAgenteCorretora.fks.Add(TabelaRebateCorretagemMetadata.ColumnNames.IdAgenteCorretora, this.IdAgente);
					}
				}

				return this._TabelaRebateCorretagemCollectionByIdAgenteCorretora;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateCorretagemCollectionByIdAgenteCorretora != null) 
				{ 
					this.RemovePostSave("TabelaRebateCorretagemCollectionByIdAgenteCorretora"); 
					this._TabelaRebateCorretagemCollectionByIdAgenteCorretora = null;
					
				} 
			} 			
		}

		private TabelaRebateCorretagemCollection _TabelaRebateCorretagemCollectionByIdAgenteCorretora;
		#endregion

				
		#region TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TabelaRebateDistribuidor_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateDistribuidorCollection TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor
		{
			get
			{
				if(this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor == null)
				{
					this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor = new TabelaRebateDistribuidorCollection();
					this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor", this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor);
				
					if(this.IdAgente != null)
					{
						this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor.Query.Where(this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor.Query.IdAgenteDistribuidor == this.IdAgente);
						this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor.fks.Add(TabelaRebateDistribuidorMetadata.ColumnNames.IdAgenteDistribuidor, this.IdAgente);
					}
				}

				return this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor != null) 
				{ 
					this.RemovePostSave("TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor"); 
					this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor = null;
					
				} 
			} 			
		}

		private TabelaRebateDistribuidorCollection _TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor;
		#endregion

				
		#region TabelaRebateGestorCollectionByIdAgenteGestor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TabelaRebateGestor_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateGestorCollection TabelaRebateGestorCollectionByIdAgenteGestor
		{
			get
			{
				if(this._TabelaRebateGestorCollectionByIdAgenteGestor == null)
				{
					this._TabelaRebateGestorCollectionByIdAgenteGestor = new TabelaRebateGestorCollection();
					this._TabelaRebateGestorCollectionByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateGestorCollectionByIdAgenteGestor", this._TabelaRebateGestorCollectionByIdAgenteGestor);
				
					if(this.IdAgente != null)
					{
						this._TabelaRebateGestorCollectionByIdAgenteGestor.Query.Where(this._TabelaRebateGestorCollectionByIdAgenteGestor.Query.IdAgenteGestor == this.IdAgente);
						this._TabelaRebateGestorCollectionByIdAgenteGestor.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateGestorCollectionByIdAgenteGestor.fks.Add(TabelaRebateGestorMetadata.ColumnNames.IdAgenteGestor, this.IdAgente);
					}
				}

				return this._TabelaRebateGestorCollectionByIdAgenteGestor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateGestorCollectionByIdAgenteGestor != null) 
				{ 
					this.RemovePostSave("TabelaRebateGestorCollectionByIdAgenteGestor"); 
					this._TabelaRebateGestorCollectionByIdAgenteGestor = null;
					
				} 
			} 			
		}

		private TabelaRebateGestorCollection _TabelaRebateGestorCollectionByIdAgenteGestor;
		#endregion

				
		#region TabelaTaxaCustodiaBolsaCollectionByIdAgente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TabelaTaxaCustodiaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaCustodiaBolsaCollection TabelaTaxaCustodiaBolsaCollectionByIdAgente
		{
			get
			{
				if(this._TabelaTaxaCustodiaBolsaCollectionByIdAgente == null)
				{
					this._TabelaTaxaCustodiaBolsaCollectionByIdAgente = new TabelaTaxaCustodiaBolsaCollection();
					this._TabelaTaxaCustodiaBolsaCollectionByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaCustodiaBolsaCollectionByIdAgente", this._TabelaTaxaCustodiaBolsaCollectionByIdAgente);
				
					if(this.IdAgente != null)
					{
						this._TabelaTaxaCustodiaBolsaCollectionByIdAgente.Query.Where(this._TabelaTaxaCustodiaBolsaCollectionByIdAgente.Query.IdAgente == this.IdAgente);
						this._TabelaTaxaCustodiaBolsaCollectionByIdAgente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaCustodiaBolsaCollectionByIdAgente.fks.Add(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdAgente, this.IdAgente);
					}
				}

				return this._TabelaTaxaCustodiaBolsaCollectionByIdAgente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaCustodiaBolsaCollectionByIdAgente != null) 
				{ 
					this.RemovePostSave("TabelaTaxaCustodiaBolsaCollectionByIdAgente"); 
					this._TabelaTaxaCustodiaBolsaCollectionByIdAgente = null;
					
				} 
			} 			
		}

		private TabelaTaxaCustodiaBolsaCollection _TabelaTaxaCustodiaBolsaCollectionByIdAgente;
		#endregion

				
		#region TransferenciaBMFCollectionByIdAgenteOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBMFCollection TransferenciaBMFCollectionByIdAgenteOrigem
		{
			get
			{
				if(this._TransferenciaBMFCollectionByIdAgenteOrigem == null)
				{
					this._TransferenciaBMFCollectionByIdAgenteOrigem = new TransferenciaBMFCollection();
					this._TransferenciaBMFCollectionByIdAgenteOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBMFCollectionByIdAgenteOrigem", this._TransferenciaBMFCollectionByIdAgenteOrigem);
				
					if(this.IdAgente != null)
					{
						this._TransferenciaBMFCollectionByIdAgenteOrigem.Query.Where(this._TransferenciaBMFCollectionByIdAgenteOrigem.Query.IdAgenteOrigem == this.IdAgente);
						this._TransferenciaBMFCollectionByIdAgenteOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBMFCollectionByIdAgenteOrigem.fks.Add(TransferenciaBMFMetadata.ColumnNames.IdAgenteOrigem, this.IdAgente);
					}
				}

				return this._TransferenciaBMFCollectionByIdAgenteOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBMFCollectionByIdAgenteOrigem != null) 
				{ 
					this.RemovePostSave("TransferenciaBMFCollectionByIdAgenteOrigem"); 
					this._TransferenciaBMFCollectionByIdAgenteOrigem = null;
					
				} 
			} 			
		}

		private TransferenciaBMFCollection _TransferenciaBMFCollectionByIdAgenteOrigem;
		#endregion

				
		#region TransferenciaBMFCollectionByIdAgenteDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TransferenciaBMF_FK2
		/// </summary>

		[XmlIgnore]
		public TransferenciaBMFCollection TransferenciaBMFCollectionByIdAgenteDestino
		{
			get
			{
				if(this._TransferenciaBMFCollectionByIdAgenteDestino == null)
				{
					this._TransferenciaBMFCollectionByIdAgenteDestino = new TransferenciaBMFCollection();
					this._TransferenciaBMFCollectionByIdAgenteDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBMFCollectionByIdAgenteDestino", this._TransferenciaBMFCollectionByIdAgenteDestino);
				
					if(this.IdAgente != null)
					{
						this._TransferenciaBMFCollectionByIdAgenteDestino.Query.Where(this._TransferenciaBMFCollectionByIdAgenteDestino.Query.IdAgenteDestino == this.IdAgente);
						this._TransferenciaBMFCollectionByIdAgenteDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBMFCollectionByIdAgenteDestino.fks.Add(TransferenciaBMFMetadata.ColumnNames.IdAgenteDestino, this.IdAgente);
					}
				}

				return this._TransferenciaBMFCollectionByIdAgenteDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBMFCollectionByIdAgenteDestino != null) 
				{ 
					this.RemovePostSave("TransferenciaBMFCollectionByIdAgenteDestino"); 
					this._TransferenciaBMFCollectionByIdAgenteDestino = null;
					
				} 
			} 			
		}

		private TransferenciaBMFCollection _TransferenciaBMFCollectionByIdAgenteDestino;
		#endregion

				
		#region TransferenciaBolsaCollectionByIdAgenteOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBolsaCollection TransferenciaBolsaCollectionByIdAgenteOrigem
		{
			get
			{
				if(this._TransferenciaBolsaCollectionByIdAgenteOrigem == null)
				{
					this._TransferenciaBolsaCollectionByIdAgenteOrigem = new TransferenciaBolsaCollection();
					this._TransferenciaBolsaCollectionByIdAgenteOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBolsaCollectionByIdAgenteOrigem", this._TransferenciaBolsaCollectionByIdAgenteOrigem);
				
					if(this.IdAgente != null)
					{
						this._TransferenciaBolsaCollectionByIdAgenteOrigem.Query.Where(this._TransferenciaBolsaCollectionByIdAgenteOrigem.Query.IdAgenteOrigem == this.IdAgente);
						this._TransferenciaBolsaCollectionByIdAgenteOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBolsaCollectionByIdAgenteOrigem.fks.Add(TransferenciaBolsaMetadata.ColumnNames.IdAgenteOrigem, this.IdAgente);
					}
				}

				return this._TransferenciaBolsaCollectionByIdAgenteOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBolsaCollectionByIdAgenteOrigem != null) 
				{ 
					this.RemovePostSave("TransferenciaBolsaCollectionByIdAgenteOrigem"); 
					this._TransferenciaBolsaCollectionByIdAgenteOrigem = null;
					
				} 
			} 			
		}

		private TransferenciaBolsaCollection _TransferenciaBolsaCollectionByIdAgenteOrigem;
		#endregion

				
		#region TransferenciaBolsaCollectionByIdAgenteDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgenteMercado_TransferenciaBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public TransferenciaBolsaCollection TransferenciaBolsaCollectionByIdAgenteDestino
		{
			get
			{
				if(this._TransferenciaBolsaCollectionByIdAgenteDestino == null)
				{
					this._TransferenciaBolsaCollectionByIdAgenteDestino = new TransferenciaBolsaCollection();
					this._TransferenciaBolsaCollectionByIdAgenteDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBolsaCollectionByIdAgenteDestino", this._TransferenciaBolsaCollectionByIdAgenteDestino);
				
					if(this.IdAgente != null)
					{
						this._TransferenciaBolsaCollectionByIdAgenteDestino.Query.Where(this._TransferenciaBolsaCollectionByIdAgenteDestino.Query.IdAgenteDestino == this.IdAgente);
						this._TransferenciaBolsaCollectionByIdAgenteDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBolsaCollectionByIdAgenteDestino.fks.Add(TransferenciaBolsaMetadata.ColumnNames.IdAgenteDestino, this.IdAgente);
					}
				}

				return this._TransferenciaBolsaCollectionByIdAgenteDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBolsaCollectionByIdAgenteDestino != null) 
				{ 
					this.RemovePostSave("TransferenciaBolsaCollectionByIdAgenteDestino"); 
					this._TransferenciaBolsaCollectionByIdAgenteDestino = null;
					
				} 
			} 			
		}

		private TransferenciaBolsaCollection _TransferenciaBolsaCollectionByIdAgenteDestino;
		#endregion

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_Pessoa_FK
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AcumuladoCorretagemBMFCollectionByIdAgente", typeof(AcumuladoCorretagemBMFCollection), new AcumuladoCorretagemBMF()));
			props.Add(new esPropertyDescriptor(this, "CalculoRebateCarteiraCollectionByIdAgenteDistribuidor", typeof(CalculoRebateCarteiraCollection), new CalculoRebateCarteira()));
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdAgenteGestor", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdAgenteAdministrador", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "CodigoClienteAgenteCollectionByIdAgente", typeof(CodigoClienteAgenteCollection), new CodigoClienteAgente()));
			props.Add(new esPropertyDescriptor(this, "CorretoraCollectionByIdAgente", typeof(CorretoraCollection), new Corretora()));
			props.Add(new esPropertyDescriptor(this, "CustodiaBMFCollectionByIdAgente", typeof(CustodiaBMFCollection), new CustodiaBMF()));
			props.Add(new esPropertyDescriptor(this, "EmissorCollectionByIdAgente", typeof(EmissorCollection), new Emissor()));
			props.Add(new esPropertyDescriptor(this, "EmpresaSecuritizadaCollectionByIdAgenteMercado", typeof(EmpresaSecuritizadaCollection), new EmpresaSecuritizada()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemFundoCollectionByIdAgenteAdministrador", typeof(EnquadraItemFundoCollection), new EnquadraItemFundo()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemFundoCollectionByIdAgenteGestor", typeof(EnquadraItemFundoCollection), new EnquadraItemFundo()));
			props.Add(new esPropertyDescriptor(this, "EventoFisicoBolsaCollectionByIdAgente", typeof(EventoFisicoBolsaCollection), new EventoFisicoBolsa()));
			props.Add(new esPropertyDescriptor(this, "IRFonteCollectionByIdAgente", typeof(IRFonteCollection), new IRFonte()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoEmprestimoBolsaCollectionByIdAgente", typeof(LiquidacaoEmprestimoBolsaCollection), new LiquidacaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoFuturoCollectionByIdAgente", typeof(LiquidacaoFuturoCollection), new LiquidacaoFuturo()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoHistoricoCollectionByIdAgente", typeof(LiquidacaoHistoricoCollection), new LiquidacaoHistorico()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoTermoBolsaCollectionByIdAgente", typeof(LiquidacaoTermoBolsaCollection), new LiquidacaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBMFCollectionByIdAgenteLiquidacao", typeof(OperacaoBMFCollection), new OperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByIdAgenteLiquidacao", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByIdAgenteCorretora", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBMFCollectionByIdAgenteCorretora", typeof(OrdemBMFCollection), new OrdemBMF()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByIdAgenteLiquidacao", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByIdAgenteCorretora", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "ParametroAdministradorFundoInvestimentoCollectionByAdministrador", typeof(ParametroAdministradorFundoInvestimentoCollection), new ParametroAdministradorFundoInvestimento()));
			props.Add(new esPropertyDescriptor(this, "PerfilCorretagemBMFCollectionByIdAgente", typeof(PerfilCorretagemBMFCollection), new PerfilCorretagemBMF()));
			props.Add(new esPropertyDescriptor(this, "PerfilCorretagemBolsaCollectionByIdAgente", typeof(PerfilCorretagemBolsaCollection), new PerfilCorretagemBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFCollectionByIdAgente", typeof(PosicaoBMFCollection), new PosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFAberturaCollectionByIdAgente", typeof(PosicaoBMFAberturaCollection), new PosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFHistoricoCollectionByIdAgente", typeof(PosicaoBMFHistoricoCollection), new PosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaCollectionByIdAgente", typeof(PosicaoBolsaCollection), new PosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaAberturaCollectionByIdAgente", typeof(PosicaoBolsaAberturaCollection), new PosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaDetalheCollectionByIdAgente", typeof(PosicaoBolsaDetalheCollection), new PosicaoBolsaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaHistoricoCollectionByIdAgente", typeof(PosicaoBolsaHistoricoCollection), new PosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaCollectionByIdAgente", typeof(PosicaoEmprestimoBolsaCollection), new PosicaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaAberturaCollectionByIdAgente", typeof(PosicaoEmprestimoBolsaAberturaCollection), new PosicaoEmprestimoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente", typeof(PosicaoEmprestimoBolsaHistoricoCollection), new PosicaoEmprestimoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdAgente", typeof(PosicaoSwapCollection), new PosicaoSwap()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdAgente", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdAgente", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaCollectionByIdAgente", typeof(PosicaoTermoBolsaCollection), new PosicaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaAberturaCollectionByIdAgente", typeof(PosicaoTermoBolsaAberturaCollection), new PosicaoTermoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaHistoricoCollectionByIdAgente", typeof(PosicaoTermoBolsaHistoricoCollection), new PosicaoTermoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateCorretagemCollectionByIdAgenteCorretora", typeof(TabelaRebateCorretagemCollection), new TabelaRebateCorretagem()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor", typeof(TabelaRebateDistribuidorCollection), new TabelaRebateDistribuidor()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateGestorCollectionByIdAgenteGestor", typeof(TabelaRebateGestorCollection), new TabelaRebateGestor()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaCustodiaBolsaCollectionByIdAgente", typeof(TabelaTaxaCustodiaBolsaCollection), new TabelaTaxaCustodiaBolsa()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBMFCollectionByIdAgenteOrigem", typeof(TransferenciaBMFCollection), new TransferenciaBMF()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBMFCollectionByIdAgenteDestino", typeof(TransferenciaBMFCollection), new TransferenciaBMF()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBolsaCollectionByIdAgenteOrigem", typeof(TransferenciaBolsaCollection), new TransferenciaBolsa()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBolsaCollectionByIdAgenteDestino", typeof(TransferenciaBolsaCollection), new TransferenciaBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AcumuladoCorretagemBMFCollectionByIdAgente != null)
			{
				foreach(AcumuladoCorretagemBMF obj in this._AcumuladoCorretagemBMFCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor != null)
			{
				foreach(CalculoRebateCarteira obj in this._CalculoRebateCarteiraCollectionByIdAgenteDistribuidor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteDistribuidor = this.IdAgente;
					}
				}
			}
			if(this._CarteiraCollectionByIdAgenteGestor != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdAgenteGestor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteGestor = this.IdAgente;
					}
				}
			}
			if(this._CarteiraCollectionByIdAgenteAdministrador != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdAgenteAdministrador)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteAdministrador = this.IdAgente;
					}
				}
			}
			if(this._CodigoClienteAgenteCollectionByIdAgente != null)
			{
				foreach(CodigoClienteAgente obj in this._CodigoClienteAgenteCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._CorretoraCollectionByIdAgente != null)
			{
				foreach(Corretora obj in this._CorretoraCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._CustodiaBMFCollectionByIdAgente != null)
			{
				foreach(CustodiaBMF obj in this._CustodiaBMFCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._EmissorCollectionByIdAgente != null)
			{
				foreach(Emissor obj in this._EmissorCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._EmpresaSecuritizadaCollectionByIdAgenteMercado != null)
			{
				foreach(EmpresaSecuritizada obj in this._EmpresaSecuritizadaCollectionByIdAgenteMercado)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteMercado = this.IdAgente;
					}
				}
			}
			if(this._EnquadraItemFundoCollectionByIdAgenteAdministrador != null)
			{
				foreach(EnquadraItemFundo obj in this._EnquadraItemFundoCollectionByIdAgenteAdministrador)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteAdministrador = this.IdAgente;
					}
				}
			}
			if(this._EnquadraItemFundoCollectionByIdAgenteGestor != null)
			{
				foreach(EnquadraItemFundo obj in this._EnquadraItemFundoCollectionByIdAgenteGestor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteGestor = this.IdAgente;
					}
				}
			}
			if(this._EventoFisicoBolsaCollectionByIdAgente != null)
			{
				foreach(EventoFisicoBolsa obj in this._EventoFisicoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._IRFonteCollectionByIdAgente != null)
			{
				foreach(IRFonte obj in this._IRFonteCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._LiquidacaoEmprestimoBolsaCollectionByIdAgente != null)
			{
				foreach(LiquidacaoEmprestimoBolsa obj in this._LiquidacaoEmprestimoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._LiquidacaoFuturoCollectionByIdAgente != null)
			{
				foreach(LiquidacaoFuturo obj in this._LiquidacaoFuturoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._LiquidacaoHistoricoCollectionByIdAgente != null)
			{
				foreach(LiquidacaoHistorico obj in this._LiquidacaoHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._LiquidacaoTermoBolsaCollectionByIdAgente != null)
			{
				foreach(LiquidacaoTermoBolsa obj in this._LiquidacaoTermoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._OperacaoBMFCollectionByIdAgenteLiquidacao != null)
			{
				foreach(OperacaoBMF obj in this._OperacaoBMFCollectionByIdAgenteLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteLiquidacao = this.IdAgente;
					}
				}
			}
			if(this._OperacaoBolsaCollectionByIdAgenteLiquidacao != null)
			{
				foreach(OperacaoBolsa obj in this._OperacaoBolsaCollectionByIdAgenteLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteLiquidacao = this.IdAgente;
					}
				}
			}
			if(this._OperacaoBolsaCollectionByIdAgenteCorretora != null)
			{
				foreach(OperacaoBolsa obj in this._OperacaoBolsaCollectionByIdAgenteCorretora)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteCorretora = this.IdAgente;
					}
				}
			}
			if(this._OrdemBMFCollectionByIdAgenteCorretora != null)
			{
				foreach(OrdemBMF obj in this._OrdemBMFCollectionByIdAgenteCorretora)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteCorretora = this.IdAgente;
					}
				}
			}
			if(this._OrdemBolsaCollectionByIdAgenteLiquidacao != null)
			{
				foreach(OrdemBolsa obj in this._OrdemBolsaCollectionByIdAgenteLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteLiquidacao = this.IdAgente;
					}
				}
			}
			if(this._OrdemBolsaCollectionByIdAgenteCorretora != null)
			{
				foreach(OrdemBolsa obj in this._OrdemBolsaCollectionByIdAgenteCorretora)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteCorretora = this.IdAgente;
					}
				}
			}
			if(this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador != null)
			{
				foreach(ParametroAdministradorFundoInvestimento obj in this._ParametroAdministradorFundoInvestimentoCollectionByAdministrador)
				{
					if(obj.es.IsAdded)
					{
						obj.Administrador = this.IdAgente;
					}
				}
			}
			if(this._PerfilCorretagemBMFCollectionByIdAgente != null)
			{
				foreach(PerfilCorretagemBMF obj in this._PerfilCorretagemBMFCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PerfilCorretagemBolsaCollectionByIdAgente != null)
			{
				foreach(PerfilCorretagemBolsa obj in this._PerfilCorretagemBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBMFCollectionByIdAgente != null)
			{
				foreach(PosicaoBMF obj in this._PosicaoBMFCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBMFAberturaCollectionByIdAgente != null)
			{
				foreach(PosicaoBMFAbertura obj in this._PosicaoBMFAberturaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBMFHistoricoCollectionByIdAgente != null)
			{
				foreach(PosicaoBMFHistorico obj in this._PosicaoBMFHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBolsaCollectionByIdAgente != null)
			{
				foreach(PosicaoBolsa obj in this._PosicaoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBolsaAberturaCollectionByIdAgente != null)
			{
				foreach(PosicaoBolsaAbertura obj in this._PosicaoBolsaAberturaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBolsaDetalheCollectionByIdAgente != null)
			{
				foreach(PosicaoBolsaDetalhe obj in this._PosicaoBolsaDetalheCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoBolsaHistoricoCollectionByIdAgente != null)
			{
				foreach(PosicaoBolsaHistorico obj in this._PosicaoBolsaHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaCollectionByIdAgente != null)
			{
				foreach(PosicaoEmprestimoBolsa obj in this._PosicaoEmprestimoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente != null)
			{
				foreach(PosicaoEmprestimoBolsaAbertura obj in this._PosicaoEmprestimoBolsaAberturaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente != null)
			{
				foreach(PosicaoEmprestimoBolsaHistorico obj in this._PosicaoEmprestimoBolsaHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoSwapCollectionByIdAgente != null)
			{
				foreach(PosicaoSwap obj in this._PosicaoSwapCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoSwapAberturaCollectionByIdAgente != null)
			{
				foreach(PosicaoSwapAbertura obj in this._PosicaoSwapAberturaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoSwapHistoricoCollectionByIdAgente != null)
			{
				foreach(PosicaoSwapHistorico obj in this._PosicaoSwapHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoTermoBolsaCollectionByIdAgente != null)
			{
				foreach(PosicaoTermoBolsa obj in this._PosicaoTermoBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoTermoBolsaAberturaCollectionByIdAgente != null)
			{
				foreach(PosicaoTermoBolsaAbertura obj in this._PosicaoTermoBolsaAberturaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._PosicaoTermoBolsaHistoricoCollectionByIdAgente != null)
			{
				foreach(PosicaoTermoBolsaHistorico obj in this._PosicaoTermoBolsaHistoricoCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._TabelaRebateCorretagemCollectionByIdAgenteCorretora != null)
			{
				foreach(TabelaRebateCorretagem obj in this._TabelaRebateCorretagemCollectionByIdAgenteCorretora)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteCorretora = this.IdAgente;
					}
				}
			}
			if(this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor != null)
			{
				foreach(TabelaRebateDistribuidor obj in this._TabelaRebateDistribuidorCollectionByIdAgenteDistribuidor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteDistribuidor = this.IdAgente;
					}
				}
			}
			if(this._TabelaRebateGestorCollectionByIdAgenteGestor != null)
			{
				foreach(TabelaRebateGestor obj in this._TabelaRebateGestorCollectionByIdAgenteGestor)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteGestor = this.IdAgente;
					}
				}
			}
			if(this._TabelaTaxaCustodiaBolsaCollectionByIdAgente != null)
			{
				foreach(TabelaTaxaCustodiaBolsa obj in this._TabelaTaxaCustodiaBolsaCollectionByIdAgente)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgente = this.IdAgente;
					}
				}
			}
			if(this._TransferenciaBMFCollectionByIdAgenteOrigem != null)
			{
				foreach(TransferenciaBMF obj in this._TransferenciaBMFCollectionByIdAgenteOrigem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteOrigem = this.IdAgente;
					}
				}
			}
			if(this._TransferenciaBMFCollectionByIdAgenteDestino != null)
			{
				foreach(TransferenciaBMF obj in this._TransferenciaBMFCollectionByIdAgenteDestino)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteDestino = this.IdAgente;
					}
				}
			}
			if(this._TransferenciaBolsaCollectionByIdAgenteOrigem != null)
			{
				foreach(TransferenciaBolsa obj in this._TransferenciaBolsaCollectionByIdAgenteOrigem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteOrigem = this.IdAgente;
					}
				}
			}
			if(this._TransferenciaBolsaCollectionByIdAgenteDestino != null)
			{
				foreach(TransferenciaBolsa obj in this._TransferenciaBolsaCollectionByIdAgenteDestino)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgenteDestino = this.IdAgente;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgenteMercadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgenteMercadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBovespa
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CodigoBovespa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoBMF
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CodigoBMF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Cnpj
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Cnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem Endereco
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Endereco, esSystemType.String);
			}
		} 
		
		public esQueryItem Bairro
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Bairro, esSystemType.String);
			}
		} 
		
		public esQueryItem Cidade
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Cidade, esSystemType.String);
			}
		} 
		
		public esQueryItem Uf
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Uf, esSystemType.String);
			}
		} 
		
		public esQueryItem Cep
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Cep, esSystemType.String);
			}
		} 
		
		public esQueryItem Telefone
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Telefone, esSystemType.String);
			}
		} 
		
		public esQueryItem Fax
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Fax, esSystemType.String);
			}
		} 
		
		public esQueryItem CorretagemAdicional
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CorretagemAdicional, esSystemType.Byte);
			}
		} 
		
		public esQueryItem FuncaoCorretora
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoCorretora, esSystemType.String);
			}
		} 
		
		public esQueryItem FuncaoAdministrador
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoAdministrador, esSystemType.String);
			}
		} 
		
		public esQueryItem FuncaoGestor
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoGestor, esSystemType.String);
			}
		} 
		
		public esQueryItem FuncaoLiquidante
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoLiquidante, esSystemType.String);
			}
		} 
		
		public esQueryItem FuncaoCustodiante
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoCustodiante, esSystemType.String);
			}
		} 
		
		public esQueryItem FuncaoDistribuidor
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.FuncaoDistribuidor, esSystemType.String);
			}
		} 
		
		public esQueryItem InfosComplementares
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.InfosComplementares, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoAnbid
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CodigoAnbid, esSystemType.String);
			}
		} 
		
		public esQueryItem Ddd
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Ddd, esSystemType.String);
			}
		} 
		
		public esQueryItem Ramal
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Ramal, esSystemType.String);
			}
		} 
		
		public esQueryItem Email
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Email, esSystemType.String);
			}
		} 
		
		public esQueryItem CPFResponsavel
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CPFResponsavel, esSystemType.String);
			}
		} 
		
		public esQueryItem CPFResponsavel2
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CPFResponsavel2, esSystemType.String);
			}
		} 
		
		public esQueryItem NomeResponsavel
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.NomeResponsavel, esSystemType.String);
			}
		} 
		
		public esQueryItem Website
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Website, esSystemType.String);
			}
		} 
		
		public esQueryItem TelOuvidoria
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.TelOuvidoria, esSystemType.String);
			}
		} 
		
		public esQueryItem EmailOuvidoria
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.EmailOuvidoria, esSystemType.String);
			}
		} 
		
		public esQueryItem DigitoCodigoBovespa
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DigitoCodigoBMF
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CartaPatente
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CartaPatente, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCetip
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.CodigoCetip, esSystemType.String);
			}
		} 
		
		public esQueryItem EmpSecuritizada
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.EmpSecuritizada, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, AgenteMercadoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgenteMercadoCollection")]
	public partial class AgenteMercadoCollection : esAgenteMercadoCollection, IEnumerable<AgenteMercado>
	{
		public AgenteMercadoCollection()
		{

		}
		
		public static implicit operator List<AgenteMercado>(AgenteMercadoCollection coll)
		{
			List<AgenteMercado> list = new List<AgenteMercado>();
			
			foreach (AgenteMercado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgenteMercadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenteMercadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgenteMercado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgenteMercado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgenteMercadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenteMercadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgenteMercadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgenteMercado AddNew()
		{
			AgenteMercado entity = base.AddNewEntity() as AgenteMercado;
			
			return entity;
		}

		public AgenteMercado FindByPrimaryKey(System.Int32 idAgente)
		{
			return base.FindByPrimaryKey(idAgente) as AgenteMercado;
		}


		#region IEnumerable<AgenteMercado> Members

		IEnumerator<AgenteMercado> IEnumerable<AgenteMercado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgenteMercado;
			}
		}

		#endregion
		
		private AgenteMercadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgenteMercado' table
	/// </summary>

	[Serializable]
	public partial class AgenteMercado : esAgenteMercado
	{
		public AgenteMercado()
		{

		}
	
		public AgenteMercado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgenteMercadoMetadata.Meta();
			}
		}
		
		
		
		override protected esAgenteMercadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenteMercadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgenteMercadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenteMercadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgenteMercadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgenteMercadoQuery query;
	}



	[Serializable]
	public partial class AgenteMercadoQuery : esAgenteMercadoQuery
	{
		public AgenteMercadoQuery()
		{

		}		
		
		public AgenteMercadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgenteMercadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgenteMercadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.IdAgente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CodigoBovespa, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CodigoBovespa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CodigoBMF, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CodigoBMF;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Cnpj, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Cnpj;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Endereco, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Endereco;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Bairro, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Bairro;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Cidade, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Cidade;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Uf, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Uf;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Cep, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Cep;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Telefone, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Telefone;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Fax, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Fax;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CorretagemAdicional, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CorretagemAdicional;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoCorretora, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoCorretora;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoAdministrador, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoAdministrador;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoGestor, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoGestor;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoLiquidante, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoLiquidante;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoCustodiante, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoCustodiante;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.FuncaoDistribuidor, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.FuncaoDistribuidor;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.InfosComplementares, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.InfosComplementares;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CodigoAnbid, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CodigoAnbid;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Ddd, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Ddd;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Ramal, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Ramal;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Email, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Email;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CPFResponsavel, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CPFResponsavel;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CPFResponsavel2, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CPFResponsavel2;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.NomeResponsavel, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.NomeResponsavel;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Website, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Website;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.TelOuvidoria, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.TelOuvidoria;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.EmailOuvidoria, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.EmailOuvidoria;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa, 30, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.DigitoCodigoBovespa;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF, 31, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.DigitoCodigoBMF;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CartaPatente, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CartaPatente;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.Apelido, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.CodigoCetip, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.CodigoCetip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.EmpSecuritizada, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.EmpSecuritizada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenteMercadoMetadata.ColumnNames.IdPessoa, 36, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenteMercadoMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgenteMercadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgente = "IdAgente";
			 public const string Nome = "Nome";
			 public const string CodigoBovespa = "CodigoBovespa";
			 public const string CodigoBMF = "CodigoBMF";
			 public const string Cnpj = "CNPJ";
			 public const string Endereco = "Endereco";
			 public const string Bairro = "Bairro";
			 public const string Cidade = "Cidade";
			 public const string Uf = "UF";
			 public const string Cep = "CEP";
			 public const string Telefone = "Telefone";
			 public const string Fax = "Fax";
			 public const string CorretagemAdicional = "CorretagemAdicional";
			 public const string FuncaoCorretora = "FuncaoCorretora";
			 public const string FuncaoAdministrador = "FuncaoAdministrador";
			 public const string FuncaoGestor = "FuncaoGestor";
			 public const string FuncaoLiquidante = "FuncaoLiquidante";
			 public const string FuncaoCustodiante = "FuncaoCustodiante";
			 public const string FuncaoDistribuidor = "FuncaoDistribuidor";
			 public const string InfosComplementares = "InfosComplementares";
			 public const string CodigoAnbid = "CodigoAnbid";
			 public const string Ddd = "DDD";
			 public const string Ramal = "Ramal";
			 public const string Email = "Email";
			 public const string CPFResponsavel = "CPFResponsavel";
			 public const string CPFResponsavel2 = "CPFResponsavel2";
			 public const string NomeResponsavel = "NomeResponsavel";
			 public const string Website = "Website";
			 public const string TelOuvidoria = "TelOuvidoria";
			 public const string EmailOuvidoria = "EmailOuvidoria";
			 public const string DigitoCodigoBovespa = "DigitoCodigoBovespa";
			 public const string DigitoCodigoBMF = "DigitoCodigoBMF";
			 public const string CartaPatente = "CartaPatente";
			 public const string Apelido = "Apelido";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string EmpSecuritizada = "EmpSecuritizada";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgente = "IdAgente";
			 public const string Nome = "Nome";
			 public const string CodigoBovespa = "CodigoBovespa";
			 public const string CodigoBMF = "CodigoBMF";
			 public const string Cnpj = "Cnpj";
			 public const string Endereco = "Endereco";
			 public const string Bairro = "Bairro";
			 public const string Cidade = "Cidade";
			 public const string Uf = "Uf";
			 public const string Cep = "Cep";
			 public const string Telefone = "Telefone";
			 public const string Fax = "Fax";
			 public const string CorretagemAdicional = "CorretagemAdicional";
			 public const string FuncaoCorretora = "FuncaoCorretora";
			 public const string FuncaoAdministrador = "FuncaoAdministrador";
			 public const string FuncaoGestor = "FuncaoGestor";
			 public const string FuncaoLiquidante = "FuncaoLiquidante";
			 public const string FuncaoCustodiante = "FuncaoCustodiante";
			 public const string FuncaoDistribuidor = "FuncaoDistribuidor";
			 public const string InfosComplementares = "InfosComplementares";
			 public const string CodigoAnbid = "CodigoAnbid";
			 public const string Ddd = "Ddd";
			 public const string Ramal = "Ramal";
			 public const string Email = "Email";
			 public const string CPFResponsavel = "CPFResponsavel";
			 public const string CPFResponsavel2 = "CPFResponsavel2";
			 public const string NomeResponsavel = "NomeResponsavel";
			 public const string Website = "Website";
			 public const string TelOuvidoria = "TelOuvidoria";
			 public const string EmailOuvidoria = "EmailOuvidoria";
			 public const string DigitoCodigoBovespa = "DigitoCodigoBovespa";
			 public const string DigitoCodigoBMF = "DigitoCodigoBMF";
			 public const string CartaPatente = "CartaPatente";
			 public const string Apelido = "Apelido";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string EmpSecuritizada = "EmpSecuritizada";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgenteMercadoMetadata))
			{
				if(AgenteMercadoMetadata.mapDelegates == null)
				{
					AgenteMercadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgenteMercadoMetadata.meta == null)
				{
					AgenteMercadoMetadata.meta = new AgenteMercadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBovespa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoBMF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CNPJ", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Endereco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Bairro", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Cidade", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CEP", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Telefone", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fax", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CorretagemAdicional", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("FuncaoCorretora", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FuncaoAdministrador", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FuncaoGestor", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FuncaoLiquidante", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FuncaoCustodiante", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FuncaoDistribuidor", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InfosComplementares", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoAnbid", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DDD", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Ramal", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CPFResponsavel", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CPFResponsavel2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NomeResponsavel", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Website", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TelOuvidoria", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EmailOuvidoria", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DigitoCodigoBovespa", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DigitoCodigoBMF", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CartaPatente", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCetip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EmpSecuritizada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "AgenteMercado";
				meta.Destination = "AgenteMercado";
				
				meta.spInsert = "proc_AgenteMercadoInsert";				
				meta.spUpdate = "proc_AgenteMercadoUpdate";		
				meta.spDelete = "proc_AgenteMercadoDelete";
				meta.spLoadAll = "proc_AgenteMercadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgenteMercadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgenteMercadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
