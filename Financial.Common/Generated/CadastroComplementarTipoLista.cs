/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/11/2014 14:04:57
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esCadastroComplementarTipoListaCollection : esEntityCollection
	{
		public esCadastroComplementarTipoListaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroComplementarTipoListaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroComplementarTipoListaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroComplementarTipoListaQuery);
		}
		#endregion
		
		virtual public CadastroComplementarTipoLista DetachEntity(CadastroComplementarTipoLista entity)
		{
			return base.DetachEntity(entity) as CadastroComplementarTipoLista;
		}
		
		virtual public CadastroComplementarTipoLista AttachEntity(CadastroComplementarTipoLista entity)
		{
			return base.AttachEntity(entity) as CadastroComplementarTipoLista;
		}
		
		virtual public void Combine(CadastroComplementarTipoListaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroComplementarTipoLista this[int index]
		{
			get
			{
				return base[index] as CadastroComplementarTipoLista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroComplementarTipoLista);
		}
	}



	[Serializable]
	abstract public class esCadastroComplementarTipoLista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroComplementarTipoListaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroComplementarTipoLista()
		{

		}

		public esCadastroComplementarTipoLista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTipoLista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoLista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTipoLista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoLista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTipoLista)
		{
			esCadastroComplementarTipoListaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTipoLista == idTipoLista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTipoLista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTipoLista",idTipoLista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTipoLista": this.str.IdTipoLista = (string)value; break;							
						case "NomeLista": this.str.NomeLista = (string)value; break;							
						case "TipoLista": this.str.TipoLista = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTipoLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoLista = (System.Int32?)value;
							break;
						
						case "TipoLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoLista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroComplementarTipoLista.IdTipoLista
		/// </summary>
		virtual public System.Int32? IdTipoLista
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarTipoListaMetadata.ColumnNames.IdTipoLista);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarTipoListaMetadata.ColumnNames.IdTipoLista, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarTipoLista.NomeLista
		/// </summary>
		virtual public System.String NomeLista
		{
			get
			{
				return base.GetSystemString(CadastroComplementarTipoListaMetadata.ColumnNames.NomeLista);
			}
			
			set
			{
				base.SetSystemString(CadastroComplementarTipoListaMetadata.ColumnNames.NomeLista, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroComplementarTipoLista.TipoLista
		/// </summary>
		virtual public System.Int32? TipoLista
		{
			get
			{
				return base.GetSystemInt32(CadastroComplementarTipoListaMetadata.ColumnNames.TipoLista);
			}
			
			set
			{
				base.SetSystemInt32(CadastroComplementarTipoListaMetadata.ColumnNames.TipoLista, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroComplementarTipoLista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTipoLista
			{
				get
				{
					System.Int32? data = entity.IdTipoLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoLista = null;
					else entity.IdTipoLista = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeLista
			{
				get
				{
					System.String data = entity.NomeLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeLista = null;
					else entity.NomeLista = Convert.ToString(value);
				}
			}
				
			public System.String TipoLista
			{
				get
				{
					System.Int32? data = entity.TipoLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLista = null;
					else entity.TipoLista = Convert.ToInt32(value);
				}
			}
			

			private esCadastroComplementarTipoLista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroComplementarTipoListaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroComplementarTipoLista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroComplementarTipoLista : esCadastroComplementarTipoLista
	{

				
		#region CadastroComplementarItemListaCollectionByIdTipoLista - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__CadastroC__IdTip__25276EE5
		/// </summary>

		[XmlIgnore]
		public CadastroComplementarItemListaCollection CadastroComplementarItemListaCollectionByIdTipoLista
		{
			get
			{
				if(this._CadastroComplementarItemListaCollectionByIdTipoLista == null)
				{
					this._CadastroComplementarItemListaCollectionByIdTipoLista = new CadastroComplementarItemListaCollection();
					this._CadastroComplementarItemListaCollectionByIdTipoLista.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CadastroComplementarItemListaCollectionByIdTipoLista", this._CadastroComplementarItemListaCollectionByIdTipoLista);
				
					if(this.IdTipoLista != null)
					{
						this._CadastroComplementarItemListaCollectionByIdTipoLista.Query.Where(this._CadastroComplementarItemListaCollectionByIdTipoLista.Query.IdTipoLista == this.IdTipoLista);
						this._CadastroComplementarItemListaCollectionByIdTipoLista.Query.Load();

						// Auto-hookup Foreign Keys
						this._CadastroComplementarItemListaCollectionByIdTipoLista.fks.Add(CadastroComplementarItemListaMetadata.ColumnNames.IdTipoLista, this.IdTipoLista);
					}
				}

				return this._CadastroComplementarItemListaCollectionByIdTipoLista;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CadastroComplementarItemListaCollectionByIdTipoLista != null) 
				{ 
					this.RemovePostSave("CadastroComplementarItemListaCollectionByIdTipoLista"); 
					this._CadastroComplementarItemListaCollectionByIdTipoLista = null;
					
				} 
			} 			
		}

		private CadastroComplementarItemListaCollection _CadastroComplementarItemListaCollectionByIdTipoLista;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CadastroComplementarItemListaCollectionByIdTipoLista", typeof(CadastroComplementarItemListaCollection), new CadastroComplementarItemLista()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CadastroComplementarItemListaCollectionByIdTipoLista != null)
			{
				foreach(CadastroComplementarItemLista obj in this._CadastroComplementarItemListaCollectionByIdTipoLista)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTipoLista = this.IdTipoLista;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroComplementarTipoListaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarTipoListaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTipoLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarTipoListaMetadata.ColumnNames.IdTipoLista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarTipoListaMetadata.ColumnNames.NomeLista, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoLista
		{
			get
			{
				return new esQueryItem(this, CadastroComplementarTipoListaMetadata.ColumnNames.TipoLista, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroComplementarTipoListaCollection")]
	public partial class CadastroComplementarTipoListaCollection : esCadastroComplementarTipoListaCollection, IEnumerable<CadastroComplementarTipoLista>
	{
		public CadastroComplementarTipoListaCollection()
		{

		}
		
		public static implicit operator List<CadastroComplementarTipoLista>(CadastroComplementarTipoListaCollection coll)
		{
			List<CadastroComplementarTipoLista> list = new List<CadastroComplementarTipoLista>();
			
			foreach (CadastroComplementarTipoLista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroComplementarTipoListaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarTipoListaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroComplementarTipoLista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroComplementarTipoLista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroComplementarTipoListaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarTipoListaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroComplementarTipoListaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroComplementarTipoLista AddNew()
		{
			CadastroComplementarTipoLista entity = base.AddNewEntity() as CadastroComplementarTipoLista;
			
			return entity;
		}

		public CadastroComplementarTipoLista FindByPrimaryKey(System.Int32 idTipoLista)
		{
			return base.FindByPrimaryKey(idTipoLista) as CadastroComplementarTipoLista;
		}


		#region IEnumerable<CadastroComplementarTipoLista> Members

		IEnumerator<CadastroComplementarTipoLista> IEnumerable<CadastroComplementarTipoLista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroComplementarTipoLista;
			}
		}

		#endregion
		
		private CadastroComplementarTipoListaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroComplementarTipoLista' table
	/// </summary>

	[Serializable]
	public partial class CadastroComplementarTipoLista : esCadastroComplementarTipoLista
	{
		public CadastroComplementarTipoLista()
		{

		}
	
		public CadastroComplementarTipoLista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroComplementarTipoListaMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroComplementarTipoListaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroComplementarTipoListaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroComplementarTipoListaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroComplementarTipoListaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroComplementarTipoListaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroComplementarTipoListaQuery query;
	}



	[Serializable]
	public partial class CadastroComplementarTipoListaQuery : esCadastroComplementarTipoListaQuery
	{
		public CadastroComplementarTipoListaQuery()
		{

		}		
		
		public CadastroComplementarTipoListaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroComplementarTipoListaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroComplementarTipoListaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroComplementarTipoListaMetadata.ColumnNames.IdTipoLista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarTipoListaMetadata.PropertyNames.IdTipoLista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarTipoListaMetadata.ColumnNames.NomeLista, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroComplementarTipoListaMetadata.PropertyNames.NomeLista;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroComplementarTipoListaMetadata.ColumnNames.TipoLista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroComplementarTipoListaMetadata.PropertyNames.TipoLista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroComplementarTipoListaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTipoLista = "IdTipoLista";
			 public const string NomeLista = "NomeLista";
			 public const string TipoLista = "TipoLista";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTipoLista = "IdTipoLista";
			 public const string NomeLista = "NomeLista";
			 public const string TipoLista = "TipoLista";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroComplementarTipoListaMetadata))
			{
				if(CadastroComplementarTipoListaMetadata.mapDelegates == null)
				{
					CadastroComplementarTipoListaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroComplementarTipoListaMetadata.meta == null)
				{
					CadastroComplementarTipoListaMetadata.meta = new CadastroComplementarTipoListaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTipoLista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeLista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoLista", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CadastroComplementarTipoLista";
				meta.Destination = "CadastroComplementarTipoLista";
				
				meta.spInsert = "proc_CadastroComplementarTipoListaInsert";				
				meta.spUpdate = "proc_CadastroComplementarTipoListaUpdate";		
				meta.spDelete = "proc_CadastroComplementarTipoListaDelete";
				meta.spLoadAll = "proc_CadastroComplementarTipoListaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroComplementarTipoListaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroComplementarTipoListaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
