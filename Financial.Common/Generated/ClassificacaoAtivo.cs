/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/01/2015 14:08:11
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Common
{

	[Serializable]
	abstract public class esClassificacaoAtivoCollection : esEntityCollection
	{
		public esClassificacaoAtivoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClassificacaoAtivoCollection";
		}

		#region Query Logic
		protected void InitQuery(esClassificacaoAtivoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClassificacaoAtivoQuery);
		}
		#endregion
		
		virtual public ClassificacaoAtivo DetachEntity(ClassificacaoAtivo entity)
		{
			return base.DetachEntity(entity) as ClassificacaoAtivo;
		}
		
		virtual public ClassificacaoAtivo AttachEntity(ClassificacaoAtivo entity)
		{
			return base.AttachEntity(entity) as ClassificacaoAtivo;
		}
		
		virtual public void Combine(ClassificacaoAtivoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClassificacaoAtivo this[int index]
		{
			get
			{
				return base[index] as ClassificacaoAtivo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClassificacaoAtivo);
		}
	}



	[Serializable]
	abstract public class esClassificacaoAtivo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClassificacaoAtivoQuery GetDynamicQuery()
		{
			return null;
		}

		public esClassificacaoAtivo()
		{

		}

		public esClassificacaoAtivo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClassificacaoAtivo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassificacaoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassificacaoAtivo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClassificacaoAtivo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassificacaoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassificacaoAtivo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClassificacaoAtivo)
		{
			esClassificacaoAtivoQuery query = this.GetDynamicQuery();
			query.Where(query.IdClassificacaoAtivo == idClassificacaoAtivo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClassificacaoAtivo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClassificacaoAtivo",idClassificacaoAtivo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClassificacaoAtivo": this.str.IdClassificacaoAtivo = (string)value; break;							
						case "ItemAvaliado": this.str.ItemAvaliado = (string)value; break;							
						case "Ativo": this.str.Ativo = (string)value; break;							
						case "IdAgenciaClassificadora": this.str.IdAgenciaClassificadora = (string)value; break;							
						case "IdPadraoNotas": this.str.IdPadraoNotas = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClassificacaoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClassificacaoAtivo = (System.Int32?)value;
							break;
						
						case "ItemAvaliado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ItemAvaliado = (System.Int32?)value;
							break;
						
						case "Ativo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Ativo = (System.Int32?)value;
							break;
						
						case "IdAgenciaClassificadora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenciaClassificadora = (System.Int32?)value;
							break;
						
						case "IdPadraoNotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPadraoNotas = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClassificacaoAtivo.IdClassificacaoAtivo
		/// </summary>
		virtual public System.Int32? IdClassificacaoAtivo
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdClassificacaoAtivo);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdClassificacaoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoAtivo.ItemAvaliado
		/// </summary>
		virtual public System.Int32? ItemAvaliado
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.ItemAvaliado);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.ItemAvaliado, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoAtivo.Ativo
		/// </summary>
		virtual public System.Int32? Ativo
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.Ativo);
			}
			
			set
			{
				base.SetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.Ativo, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoAtivo.IdAgenciaClassificadora
		/// </summary>
		virtual public System.Int32? IdAgenciaClassificadora
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdAgenciaClassificadora);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdAgenciaClassificadora, value))
				{
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassificacaoAtivo.IdPadraoNotas
		/// </summary>
		virtual public System.Int32? IdPadraoNotas
		{
			get
			{
				return base.GetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdPadraoNotas);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassificacaoAtivoMetadata.ColumnNames.IdPadraoNotas, value))
				{
					this._UpToPadraoNotasByIdPadraoNotas = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenciaClassificadora _UpToAgenciaClassificadoraByIdAgenciaClassificadora;
		[CLSCompliant(false)]
		internal protected PadraoNotas _UpToPadraoNotasByIdPadraoNotas;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClassificacaoAtivo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClassificacaoAtivo
			{
				get
				{
					System.Int32? data = entity.IdClassificacaoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClassificacaoAtivo = null;
					else entity.IdClassificacaoAtivo = Convert.ToInt32(value);
				}
			}
				
			public System.String ItemAvaliado
			{
				get
				{
					System.Int32? data = entity.ItemAvaliado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ItemAvaliado = null;
					else entity.ItemAvaliado = Convert.ToInt32(value);
				}
			}
				
			public System.String Ativo
			{
				get
				{
					System.Int32? data = entity.Ativo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ativo = null;
					else entity.Ativo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenciaClassificadora
			{
				get
				{
					System.Int32? data = entity.IdAgenciaClassificadora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenciaClassificadora = null;
					else entity.IdAgenciaClassificadora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPadraoNotas
			{
				get
				{
					System.Int32? data = entity.IdPadraoNotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPadraoNotas = null;
					else entity.IdPadraoNotas = Convert.ToInt32(value);
				}
			}
			

			private esClassificacaoAtivo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClassificacaoAtivoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClassificacaoAtivo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClassificacaoAtivo : esClassificacaoAtivo
	{

				
		#region UpToAgenciaClassificadoraByIdAgenciaClassificadora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Classific__IdAge__24BD5A91
		/// </summary>

		[XmlIgnore]
		public AgenciaClassificadora UpToAgenciaClassificadoraByIdAgenciaClassificadora
		{
			get
			{
				if(this._UpToAgenciaClassificadoraByIdAgenciaClassificadora == null
					&& IdAgenciaClassificadora != null					)
				{
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = new AgenciaClassificadora();
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora", this._UpToAgenciaClassificadoraByIdAgenciaClassificadora);
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.Where(this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.IdAgenciaClassificadora == this.IdAgenciaClassificadora);
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.Query.Load();
				}

				return this._UpToAgenciaClassificadoraByIdAgenciaClassificadora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora");
				

				if(value == null)
				{
					this.IdAgenciaClassificadora = null;
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = null;
				}
				else
				{
					this.IdAgenciaClassificadora = value.IdAgenciaClassificadora;
					this._UpToAgenciaClassificadoraByIdAgenciaClassificadora = value;
					this.SetPreSave("UpToAgenciaClassificadoraByIdAgenciaClassificadora", this._UpToAgenciaClassificadoraByIdAgenciaClassificadora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPadraoNotasByIdPadraoNotas - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Classific__IdPad__25B17ECA
		/// </summary>

		[XmlIgnore]
		public PadraoNotas UpToPadraoNotasByIdPadraoNotas
		{
			get
			{
				if(this._UpToPadraoNotasByIdPadraoNotas == null
					&& IdPadraoNotas != null					)
				{
					this._UpToPadraoNotasByIdPadraoNotas = new PadraoNotas();
					this._UpToPadraoNotasByIdPadraoNotas.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPadraoNotasByIdPadraoNotas", this._UpToPadraoNotasByIdPadraoNotas);
					this._UpToPadraoNotasByIdPadraoNotas.Query.Where(this._UpToPadraoNotasByIdPadraoNotas.Query.IdPadraoNotas == this.IdPadraoNotas);
					this._UpToPadraoNotasByIdPadraoNotas.Query.Load();
				}

				return this._UpToPadraoNotasByIdPadraoNotas;
			}
			
			set
			{
				this.RemovePreSave("UpToPadraoNotasByIdPadraoNotas");
				

				if(value == null)
				{
					this.IdPadraoNotas = null;
					this._UpToPadraoNotasByIdPadraoNotas = null;
				}
				else
				{
					this.IdPadraoNotas = value.IdPadraoNotas;
					this._UpToPadraoNotasByIdPadraoNotas = value;
					this.SetPreSave("UpToPadraoNotasByIdPadraoNotas", this._UpToPadraoNotasByIdPadraoNotas);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenciaClassificadoraByIdAgenciaClassificadora != null)
			{
				this.IdAgenciaClassificadora = this._UpToAgenciaClassificadoraByIdAgenciaClassificadora.IdAgenciaClassificadora;
			}
			if(!this.es.IsDeleted && this._UpToPadraoNotasByIdPadraoNotas != null)
			{
				this.IdPadraoNotas = this._UpToPadraoNotasByIdPadraoNotas.IdPadraoNotas;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClassificacaoAtivoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClassificacaoAtivoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClassificacaoAtivo
		{
			get
			{
				return new esQueryItem(this, ClassificacaoAtivoMetadata.ColumnNames.IdClassificacaoAtivo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ItemAvaliado
		{
			get
			{
				return new esQueryItem(this, ClassificacaoAtivoMetadata.ColumnNames.ItemAvaliado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Ativo
		{
			get
			{
				return new esQueryItem(this, ClassificacaoAtivoMetadata.ColumnNames.Ativo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenciaClassificadora
		{
			get
			{
				return new esQueryItem(this, ClassificacaoAtivoMetadata.ColumnNames.IdAgenciaClassificadora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPadraoNotas
		{
			get
			{
				return new esQueryItem(this, ClassificacaoAtivoMetadata.ColumnNames.IdPadraoNotas, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClassificacaoAtivoCollection")]
	public partial class ClassificacaoAtivoCollection : esClassificacaoAtivoCollection, IEnumerable<ClassificacaoAtivo>
	{
		public ClassificacaoAtivoCollection()
		{

		}
		
		public static implicit operator List<ClassificacaoAtivo>(ClassificacaoAtivoCollection coll)
		{
			List<ClassificacaoAtivo> list = new List<ClassificacaoAtivo>();
			
			foreach (ClassificacaoAtivo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClassificacaoAtivoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassificacaoAtivoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClassificacaoAtivo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClassificacaoAtivo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClassificacaoAtivoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassificacaoAtivoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClassificacaoAtivoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClassificacaoAtivo AddNew()
		{
			ClassificacaoAtivo entity = base.AddNewEntity() as ClassificacaoAtivo;
			
			return entity;
		}

		public ClassificacaoAtivo FindByPrimaryKey(System.Int32 idClassificacaoAtivo)
		{
			return base.FindByPrimaryKey(idClassificacaoAtivo) as ClassificacaoAtivo;
		}


		#region IEnumerable<ClassificacaoAtivo> Members

		IEnumerator<ClassificacaoAtivo> IEnumerable<ClassificacaoAtivo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClassificacaoAtivo;
			}
		}

		#endregion
		
		private ClassificacaoAtivoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClassificacaoAtivo' table
	/// </summary>

	[Serializable]
	public partial class ClassificacaoAtivo : esClassificacaoAtivo
	{
		public ClassificacaoAtivo()
		{

		}
	
		public ClassificacaoAtivo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClassificacaoAtivoMetadata.Meta();
			}
		}
		
		
		
		override protected esClassificacaoAtivoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassificacaoAtivoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClassificacaoAtivoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassificacaoAtivoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClassificacaoAtivoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClassificacaoAtivoQuery query;
	}



	[Serializable]
	public partial class ClassificacaoAtivoQuery : esClassificacaoAtivoQuery
	{
		public ClassificacaoAtivoQuery()
		{

		}		
		
		public ClassificacaoAtivoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClassificacaoAtivoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClassificacaoAtivoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClassificacaoAtivoMetadata.ColumnNames.IdClassificacaoAtivo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoAtivoMetadata.PropertyNames.IdClassificacaoAtivo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoAtivoMetadata.ColumnNames.ItemAvaliado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoAtivoMetadata.PropertyNames.ItemAvaliado;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoAtivoMetadata.ColumnNames.Ativo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoAtivoMetadata.PropertyNames.Ativo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoAtivoMetadata.ColumnNames.IdAgenciaClassificadora, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoAtivoMetadata.PropertyNames.IdAgenciaClassificadora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassificacaoAtivoMetadata.ColumnNames.IdPadraoNotas, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassificacaoAtivoMetadata.PropertyNames.IdPadraoNotas;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClassificacaoAtivoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClassificacaoAtivo = "IdClassificacaoAtivo";
			 public const string ItemAvaliado = "ItemAvaliado";
			 public const string Ativo = "Ativo";
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string IdPadraoNotas = "IdPadraoNotas";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClassificacaoAtivo = "IdClassificacaoAtivo";
			 public const string ItemAvaliado = "ItemAvaliado";
			 public const string Ativo = "Ativo";
			 public const string IdAgenciaClassificadora = "IdAgenciaClassificadora";
			 public const string IdPadraoNotas = "IdPadraoNotas";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClassificacaoAtivoMetadata))
			{
				if(ClassificacaoAtivoMetadata.mapDelegates == null)
				{
					ClassificacaoAtivoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClassificacaoAtivoMetadata.meta == null)
				{
					ClassificacaoAtivoMetadata.meta = new ClassificacaoAtivoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClassificacaoAtivo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ItemAvaliado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Ativo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenciaClassificadora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPadraoNotas", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ClassificacaoAtivo";
				meta.Destination = "ClassificacaoAtivo";
				
				meta.spInsert = "proc_ClassificacaoAtivoInsert";				
				meta.spUpdate = "proc_ClassificacaoAtivoUpdate";		
				meta.spDelete = "proc_ClassificacaoAtivoDelete";
				meta.spLoadAll = "proc_ClassificacaoAtivoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClassificacaoAtivoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClassificacaoAtivoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
