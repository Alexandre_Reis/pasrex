/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/04/2014 19:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Common
{

	[Serializable]
	abstract public class esGrupoAfinidadeCollection : esEntityCollection
	{
		public esGrupoAfinidadeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoAfinidadeCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoAfinidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoAfinidadeQuery);
		}
		#endregion
		
		virtual public GrupoAfinidade DetachEntity(GrupoAfinidade entity)
		{
			return base.DetachEntity(entity) as GrupoAfinidade;
		}
		
		virtual public GrupoAfinidade AttachEntity(GrupoAfinidade entity)
		{
			return base.AttachEntity(entity) as GrupoAfinidade;
		}
		
		virtual public void Combine(GrupoAfinidadeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoAfinidade this[int index]
		{
			get
			{
				return base[index] as GrupoAfinidade;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoAfinidade);
		}
	}



	[Serializable]
	abstract public class esGrupoAfinidade : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoAfinidadeQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoAfinidade()
		{

		}

		public esGrupoAfinidade(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupoAfinidadeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo)
		{
			esGrupoAfinidadeQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoAfinidade.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(GrupoAfinidadeMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(GrupoAfinidadeMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoAfinidade.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(GrupoAfinidadeMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(GrupoAfinidadeMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoAfinidade entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esGrupoAfinidade entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoAfinidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoAfinidade can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoAfinidade : esGrupoAfinidade
	{

				
		#region ClienteCollectionByIdGrupoAfinidade - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - GrupoAfinidade_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdGrupoAfinidade
		{
			get
			{
				if(this._ClienteCollectionByIdGrupoAfinidade == null)
				{
					this._ClienteCollectionByIdGrupoAfinidade = new ClienteCollection();
					this._ClienteCollectionByIdGrupoAfinidade.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdGrupoAfinidade", this._ClienteCollectionByIdGrupoAfinidade);
				
					if(this.IdGrupo != null)
					{
						this._ClienteCollectionByIdGrupoAfinidade.Query.Where(this._ClienteCollectionByIdGrupoAfinidade.Query.IdGrupoAfinidade == this.IdGrupo);
						this._ClienteCollectionByIdGrupoAfinidade.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdGrupoAfinidade.fks.Add(ClienteMetadata.ColumnNames.IdGrupoAfinidade, this.IdGrupo);
					}
				}

				return this._ClienteCollectionByIdGrupoAfinidade;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdGrupoAfinidade != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdGrupoAfinidade"); 
					this._ClienteCollectionByIdGrupoAfinidade = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdGrupoAfinidade;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdGrupoAfinidade", typeof(ClienteCollection), new Cliente()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ClienteCollectionByIdGrupoAfinidade != null)
			{
				foreach(Cliente obj in this._ClienteCollectionByIdGrupoAfinidade)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoAfinidade = this.IdGrupo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupoAfinidadeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoAfinidadeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, GrupoAfinidadeMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, GrupoAfinidadeMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoAfinidadeCollection")]
	public partial class GrupoAfinidadeCollection : esGrupoAfinidadeCollection, IEnumerable<GrupoAfinidade>
	{
		public GrupoAfinidadeCollection()
		{

		}
		
		public static implicit operator List<GrupoAfinidade>(GrupoAfinidadeCollection coll)
		{
			List<GrupoAfinidade> list = new List<GrupoAfinidade>();
			
			foreach (GrupoAfinidade emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoAfinidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoAfinidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoAfinidade(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoAfinidade();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoAfinidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoAfinidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoAfinidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoAfinidade AddNew()
		{
			GrupoAfinidade entity = base.AddNewEntity() as GrupoAfinidade;
			
			return entity;
		}

		public GrupoAfinidade FindByPrimaryKey(System.Int32 idGrupo)
		{
			return base.FindByPrimaryKey(idGrupo) as GrupoAfinidade;
		}


		#region IEnumerable<GrupoAfinidade> Members

		IEnumerator<GrupoAfinidade> IEnumerable<GrupoAfinidade>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoAfinidade;
			}
		}

		#endregion
		
		private GrupoAfinidadeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoAfinidade' table
	/// </summary>

	[Serializable]
	public partial class GrupoAfinidade : esGrupoAfinidade
	{
		public GrupoAfinidade()
		{

		}
	
		public GrupoAfinidade(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoAfinidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoAfinidadeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoAfinidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoAfinidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoAfinidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoAfinidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoAfinidadeQuery query;
	}



	[Serializable]
	public partial class GrupoAfinidadeQuery : esGrupoAfinidadeQuery
	{
		public GrupoAfinidadeQuery()
		{

		}		
		
		public GrupoAfinidadeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoAfinidadeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoAfinidadeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoAfinidadeMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoAfinidadeMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoAfinidadeMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoAfinidadeMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoAfinidadeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoAfinidadeMetadata))
			{
				if(GrupoAfinidadeMetadata.mapDelegates == null)
				{
					GrupoAfinidadeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoAfinidadeMetadata.meta == null)
				{
					GrupoAfinidadeMetadata.meta = new GrupoAfinidadeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GrupoAfinidade";
				meta.Destination = "GrupoAfinidade";
				
				meta.spInsert = "proc_GrupoAfinidadeInsert";				
				meta.spUpdate = "proc_GrupoAfinidadeUpdate";		
				meta.spDelete = "proc_GrupoAfinidadeDelete";
				meta.spLoadAll = "proc_GrupoAfinidadeLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoAfinidadeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoAfinidadeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
