﻿using System;

namespace Financial.Common.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de Common
    /// </summary>
    public class CommonException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CommonException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CommonException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public CommonException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de IdAgenteMercado
    /// </summary>
    public class IdAgenteNaoCadastradoException : CommonException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public IdAgenteNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public IdAgenteNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de cotacaoIndice
    /// </summary>
    public class CotacaoIndiceNaoCadastradoException : CommonException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotacaoIndiceNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotacaoIndiceNaoCadastradoException(string mensagem) : base(mensagem) { }
    }
    
}


