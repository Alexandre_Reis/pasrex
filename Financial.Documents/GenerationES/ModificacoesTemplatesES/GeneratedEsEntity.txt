/*
* Template MyGeneration - Generated/Generated - esEntity
* Funcionalidade de LoadByPrimaryKey - acrescentar codigo abaixo na linha 141
* Data: 30/05/2011
*/		
		
		
		
		
		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, <%
		comma = "";
		foreach(EntitySpaces.MetadataEngine.IColumn col in table.PrimaryKeys)
		{
			%><%=comma%><%
			%><%=esMeta.esPlugIn.CSharpToSystemType(col) + " " + esMeta.esPlugIn.ParameterName(col)%><%
			comma = ", ";
		}%>)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			<%=esMeta.esPlugIn.esQuery(source)%> query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(<%
		comma = "";
		foreach(EntitySpaces.MetadataEngine.IColumn col in table.PrimaryKeys)
		{
			%><%=comma%>query.<%
			%><%=esMeta.esPlugIn.PropertyName(col) + " == " + esMeta.esPlugIn.ParameterName(col)%><%
			comma = ", ";
		}%>);
		
			return query.Load();
		}

