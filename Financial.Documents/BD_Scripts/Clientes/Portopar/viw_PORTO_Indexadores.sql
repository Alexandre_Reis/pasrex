if exists (select 1 from sysobjects where id = OBJECT_ID('viw_PORTO_Indexadores') and type = 'V')
    drop view dbo.viw_PORTO_Indexadores
go
    
create view dbo.viw_PORTO_Indexadores as
select convert(varchar(20), idindice) as CODINDEX, data, valor
from CotacaoIndice
union all
select convert(varchar(20), p.CodigoInterface) as CODINDEX, data, CotaFechamento
from HistoricoCota h, Pessoa p
where IdCarteira in (select idcliente from Cliente where IdTipo = 500)
and h.IdCarteira = p.IdPessoa
go

grant all on dbo.viw_PORTO_Indexadores to public
go
