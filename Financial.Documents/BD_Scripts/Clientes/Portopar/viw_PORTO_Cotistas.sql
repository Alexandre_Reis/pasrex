﻿if exists (select 1 from sysobjects where id = OBJECT_ID('viw_PORTO_Cotistas') and type = 'V')
    drop view dbo.viw_PORTO_Cotistas
go
    
create view dbo.viw_PORTO_Cotistas as
 select o.idcotista
        ,o.CodigoInterface as Cliente
        ,p.Cpfcnpj as Cgccpf
        ,p.DataNascimento
        ,null as 'assessor' --código assessor
        ,null as 'nome'     --nome do assessor
        ,p.Tipo as TipoPessoa
        ,pe.Endereco as Logradouro
        ,pe.Numero
        ,pe.Complemento
        ,pe.Bairro
        ,pe.Cidade
        ,pe.UF as Estado
        ,pe.CEP
        ,null as 'DDD'
        ,pt.Numero as 'Telefone'
        ,pm.Email
        ,p.IdPessoa
    from Pessoa p left outer join pessoaendereco pe on pe.IdPessoa = p.IdPessoa
			  left outer join (select IdPessoa, MIN(Numero) Numero from pessoatelefone group by IdPessoa) pt on pt.IdPessoa = p.IdPessoa	
			  left outer join (select IdPessoa, MIN(Email) Email from pessoaemail group by IdPessoa) pm on pm.IdPessoa = p.IdPessoa	
        , Cotista o
   where o.IdCotista = p.IdPessoa
     and pe.RecebeCorrespondencia = 'S'
go

grant all on dbo.viw_PORTO_Cotistas to public
go
