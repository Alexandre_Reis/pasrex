IF EXISTS(SELECT 1 FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'Fromtis_InformacoesDiarias')) 
drop Procedure Fromtis_InformacoesDiarias
go

Create Procedure Fromtis_InformacoesDiarias (@data datetime)
as
select h.data as Data,
	   h.idcarteira as CodFundo, 
	   h.cotafechamento as CotaLiquida, 
	   h.cotabruta as CotaBruta, 
	   h.plfechamento as PatrimonioLiquido,
	   h.quantidadefechamento as QuantidadeCotas,
	   count(distinct idcotista) as NumeroCotistas
from historicocota h, posicaocotistahistorico p
where h.idcarteira = p.idcarteira
and p.datahistorico = @data
and h.data = @data
and h.idcarteira in (select idcliente from cliente where tipocontrole = 5 and idtipo in (500, 510))
group by h.data, h.idcarteira, h.cotafechamento, h.cotabruta, h.plfechamento, h.quantidadefechamento
go