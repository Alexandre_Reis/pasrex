alter table AtivoBMF add CodigoIsin varchar(50) null
go

update AtivoBMF set CodigoIsin = ''
go

alter table AtivoBMF alter column CodigoIsin varchar(50) not null
go

update AtivoBMF set CodigoIsin = 'BRBMEFWIN1C2' where cdativobmf = 'WIN' and serie = 'V12'
update AtivoBMF set CodigoIsin = 'BRBMEFIBV1J2' where cdativobmf = 'IND' and serie = 'V12'

update ativobmf set codigoisin  = 'BRBMEFWIN1D0' where cdativobmf = 'WIN' and serie = 'Z12'
update ativobmf set codigoisin  = 'BRBMEFIBV1E3' where cdativobmf = 'IND' and serie = 'Z12'

----------------------------------------------------------------------

alter table ativobolsa add TipoPapel tinyint null
go
update ativobolsa set TipoPapel = 1 -- Normal
go
alter table ativobolsa alter column TipoPapel tinyint not null
go
----------------------------------------------------------------------

ALTER TABLE AtivoBMF
DROP COLUMN CodigoIsin;

ALTER TABLE ativobolsa
DROP COLUMN TipoPapel;




