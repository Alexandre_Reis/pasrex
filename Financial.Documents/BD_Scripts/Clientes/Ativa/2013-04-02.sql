DROP VIEW V_OPERACAOCOTISTA
go
CREATE VIEW V_OPERACAOCOTISTA AS  

select case when (o.codigointerface is null or o.codigointerface = '') then LTRIM(RTRIM(str(o.IdCotista))) else LTRIM(RTRIM(REPLACE(o.codigointerface, 'AT', ''))) end as CODIGO_CLIENTE_ATIVA,  
 case when (c.CodigoYMF is null or c.CodigoYMF = '') then LTRIM(RTRIM(str(c.IdCliente))) else LTRIM(RTRIM(REPLACE(c.CodigoYMF, 'AT', ''))) end as CODIGO_FUNDO_ATIVA,  
 LTRIM(RTRIM(str(o.IdCotista))) as CODIGO_CLIENTE_FINANCIAL,
 LTRIM(RTRIM(str(c.IdCliente))) as CODIGO_FUNDO_FINANCIAL,
 
 o.Nome as NM_CLIENTE,  
 p.IdOperacao as ID_OPERACAO,  
 p.TipoOperacao as TIPO_RESGATE,  
 p.DataOperacao as DATA_OPERACAO,  
 p.DataConversao as DATA_CONVERSAO,  
 p.DataLiquidacao as DATA_LIQUIDACAO,  
 p.Quantidade as QTDCOTAS,  
 p.ValorBruto as VALORBRUTO,  
 p.ValorIOF as VALORIOF,  
 p.ValorIR as VALORIR,  
 p.ValorLiquido as VALORLIQUIDO  
from Cotista o, ClienteInterface c, OperacaoCotista p  
where p.IdCotista = o.IdCotista  
and p.IdCarteira = c.IdCliente  
and p.TipoOperacao in (2, 3, 4, 5, 20)  
and p.IdCarteira <> p.IdCotista  
go

DROP VIEW V_POSICAOCOTISTA
go
CREATE VIEW V_POSICAOCOTISTA AS  
select l.DataDia as DATAPOSICAO,  
 case when (o.codigointerface is null or o.codigointerface = '') then LTRIM(RTRIM(str(o.IdCotista))) else LTRIM(RTRIM(REPLACE(o.codigointerface, 'AT', ''))) end as CODIGO_CLIENTE_ATIVA,
 case when (c.CodigoYMF is null or c.CodigoYMF = '') then LTRIM(RTRIM(str(c.IdCliente))) else LTRIM(RTRIM(REPLACE(c.CodigoYMF, 'AT', ''))) end as CODIGO_FUNDO_ATIVA,
 LTRIM(RTRIM(str(o.IdCotista))) as CODIGO_CLIENTE_FINANCIAL,
 LTRIM(RTRIM(str(c.IdCliente))) as CODIGO_FUNDO_FINANCIAL,
 p.DataAplicacao as DT_APLICACAO,  
 o.Nome as NM_CLIENTE,  
 p.Quantidade as QTDCOTAS,  
 p.ValorAplicacao as VALORAPLICACAO,  
 p.ValorBruto as VALORCORRIGIDO,  
 p.CotaDia as VALORCOTA,  
 p.ValorIOF as VALORIOF,  
 p.ValorIR as VALORIR,  
 p.Quantidade * (p.CotaDia - p.CotaAplicacao) as VALORRENDIMENTO,  
 p.ValorLiquido as VALORRESGATE  
from Cotista o, ClienteInterface c, PosicaoCotista p, Cliente l  
where p.IdCotista = o.IdCotista  
and p.IdCarteira = c.IdCliente  
and p.IdCarteira = l.IdCliente  
and p.IdCarteira <> p.IdCotista
and p.Quantidade > 0
and l.StatusAtivo = 1  
go

DROP VIEW  V_CADASTROCLIENTE
go
CREATE VIEW V_CADASTROCLIENTE
AS
select 
p.IdPessoa, 
MIN(p.Nome) as Nome,
MIN(p.Apelido) as Apelido,
MIN(Tipo) as Tipo,
MIN(DataCadatro) as DataCadastro,
MIN(DataUltimaAlteracao) as DataUltimaAlteracao,
MIN(Cpfcnpj) as Cpfcnpj,
MIN(IdAgenteDistribuidor) as IdAgenteDistribuidor,

COALESCE(
  CASE WHEN (MIN(t.codigointerface) is null or MIN(t.codigointerface) = '') THEN NULL ELSE LTRIM(RTRIM(REPLACE(MIN(t.codigointerface), 'AT', '')))  END,
  CASE WHEN (MIN(i.codigoymf) is null or MIN(i.codigoymf) = '') THEN NULL ELSE LTRIM(RTRIM(REPLACE(MIN(i.codigoYMF), 'AT', '')))  END,
  CASE WHEN (MIN(p.codigointerface) is null or MIN(p.codigointerface) = '') THEN LTRIM(RTRIM(str(p.IdPessoa))) ELSE LTRIM(RTRIM(REPLACE(MIN(p.codigointerface), 'AT', '')))  END
) as CODIGO_CLIENTE_ATIVA,


MIN(EstadoCivil) as EstadoCivil,
MIN(NumeroRG) as NumeroRG,
MIN(EmissorRG) as EmissorRG,
MIN(DataEmissaoRG) as DataEmissaoRG,
MIN(Sexo) as Sexo,
MIN(DataNascimento) as DataNascimento,
MIN(Profissao) as Profissao,
MIN(RecebeCorrespondencia) as RecebeCorrespondencia,
MIN(Endereco) as Endereco,
MIN(Numero) as Numero,
MIN(Complemento) as Complemento,
MIN(Bairro) as Bairro,
MIN(Cidade) as Cidade,
MIN(CEP) as CEP,
MIN(UF) as UF,
MIN(Pais) as Pais,
MIN(Fone) as Fone,
MIN(Email) as Email
from Pessoa p
left JOIN (select * from PessoaEndereco where (Endereco != '' and Endereco is not NULL))e
ON p.IdPessoa = e.IdPessoa
left JOIN (select IdCliente from Cliente where IdTipo = 1 and TipoControle = 5) c on c.IdCliente = p.IdPessoa

left JOIN (select IdCotista, CodigoInterface from Cotista where StatusAtivo != 1) t on t.IdCotista = p.IdPessoa

left JOIN ClienteInterface i on i.IdCliente = p.IdPessoa

WHERE c.IdCliente IS null and t.IdCotista IS null

group by p.IdPessoa
go
