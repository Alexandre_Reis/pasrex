DROP VIEW V_OPERACAOCOTISTA
go
CREATE VIEW V_OPERACAOCOTISTA AS  

select case when (o.codigointerface is null or o.codigointerface = '') then LTRIM(RTRIM(str(o.IdCotista))) else LTRIM(RTRIM(REPLACE(o.codigointerface, 'AT', ''))) end as CODIGO_CLIENTE_ATIVA,  
 case when (c.CodigoYMF is null or c.CodigoYMF = '') then LTRIM(RTRIM(str(c.IdCliente))) else LTRIM(RTRIM(REPLACE(c.CodigoYMF, 'AT', ''))) end as CODIGO_FUNDO_ATIVA,  
 LTRIM(RTRIM(str(o.IdCotista))) as CODIGO_CLIENTE_FINANCIAL,
 LTRIM(RTRIM(str(c.IdCliente))) as CODIGO_FUNDO_FINANCIAL, 
 o.Nome as NM_CLIENTE,  
 p.IdOperacao as ID_OPERACAO,  
 p.TipoOperacao as TIPO_RESGATE,  
 p.DataOperacao as DATA_OPERACAO,  
 p.DataConversao as DATA_CONVERSAO,  
 p.DataLiquidacao as DATA_LIQUIDACAO,  
 p.Quantidade as QTDCOTAS,  
 p.ValorBruto as VALORBRUTO,  
 p.ValorIOF as VALORIOF,  
 p.ValorIR as VALORIR,  
 p.ValorLiquido as VALORLIQUIDO,
 p.IdOperacaoAuxiliar IDOPERACAOAUXILIAR
from Cotista o, ClienteInterface c, OperacaoCotista p  
where p.IdCotista = o.IdCotista  
and p.IdCarteira = c.IdCliente  
and p.TipoOperacao in (2, 3, 4, 5, 20)  
and p.IdCarteira <> p.IdCotista  
go
