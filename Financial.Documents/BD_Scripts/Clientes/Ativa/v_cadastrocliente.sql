DROP VIEW V_CADASTROCLIENTE
GO
CREATE VIEW V_CADASTROCLIENTE
AS
select 
p.IdPessoa, 
MIN(p.Nome) as Nome,
MIN(p.Apelido) as Apelido,
MIN(Tipo) as Tipo,
MIN(DataCadatro) as DataCadastro,
MIN(DataUltimaAlteracao) as DataUltimaAlteracao,
MIN(Cpfcnpj) as Cpfcnpj,
MIN(IdAgenteDistribuidor) as IdAgenteDistribuidor,

COALESCE(
  CASE WHEN (MIN(t.codigointerface) is null or MIN(t.codigointerface) = '') THEN NULL ELSE LTRIM(RTRIM(REPLACE(MIN(t.codigointerface), 'AT', '')))  END,
  CASE WHEN (MIN(i.codigoymf) is null or MIN(i.codigoymf) = '') THEN NULL ELSE LTRIM(RTRIM(REPLACE(MIN(i.codigoYMF), 'AT', '')))  END,
  CASE WHEN (MIN(p.codigointerface) is null or MIN(p.codigointerface) = '') THEN LTRIM(RTRIM(str(p.IdPessoa))) ELSE LTRIM(RTRIM(REPLACE(MIN(p.codigointerface), 'AT', '')))  END
) as CODIGO_CLIENTE_ATIVA,


MIN(EstadoCivil) as EstadoCivil,
MIN(NumeroDocumento) as NumeroRG,
MIN(OrgaoEmissor) as EmissorRG,
MIN(DataExpedicao) as DataEmissaoRG,
MIN(Sexo) as Sexo,
MIN(DataNascimento) as DataNascimento,
MIN(Profissao) as Profissao,
MIN(RecebeCorrespondencia) as RecebeCorrespondencia,
MIN(Endereco) as Endereco,
MIN(Numero) as Numero,
MIN(Complemento) as Complemento,
MIN(Bairro) as Bairro,
MIN(Cidade) as Cidade,
MIN(CEP) as CEP,
MIN(UF) as UF,
MIN(Pais) as Pais,
MIN(Telefone) as Fone,
MIN(em.Email) as Email
from Pessoa p
left Join (select IdPessoa, min(IdEndereco) IdEndereco from PessoaEndereco 
			where (Endereco != '' and Endereco is not NULL)
			group by IdPessoa) pei on p.IdPessoa = pei.IdPessoa
left JOIN (select * from PessoaEndereco)e ON pei.IdEndereco = e.IdEndereco
left JOIN (select IdCliente from Cliente where IdTipo = 1 and TipoControle = 5) c on c.IdCliente = p.IdPessoa
left JOIN (select IdCotista, CodigoInterface from Cotista where StatusAtivo != 1) t on t.IdCotista = p.IdPessoa
left JOIN ClienteInterface i on i.IdCliente = p.IdPessoa
left JOIN (select IdPessoa, min(IdDocumento) IdDocumento from PessoaDocumento group by IdPessoa) di on p.IdPessoa = di.IdPessoa
left join (select IdDocumento, NumeroDocumento, OrgaoEmissor, DataExpedicao from PessoaDocumento) d on di.IdDocumento = d.IdDocumento
left JOIN (select IdPessoa, min(IdTelefone) IdTelefone from PessoaTelefone group by IdPessoa) ti on p.IdPessoa = di.IdPessoa
left join (select IdTelefone, DDI, DDD, Numero Telefone, Ramal from PessoaTelefone) te on ti.IdTelefone = te.IdTelefone
left join (select IdPessoa, min(IdEmail) IdEmail from PessoaEmail group by IdPessoa) ei on p.IdPessoa = ei.IdPessoa
left join (select IdEmail, Email from PessoaEmail) em on ei.IdEmail = em.IdEmail

WHERE c.IdCliente IS null and t.IdCotista IS null

group by p.IdPessoa
go
