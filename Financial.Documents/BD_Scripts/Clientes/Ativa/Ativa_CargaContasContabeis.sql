
insert into ContabPlanoContas
(IdPlano
,TipoConta
,Descricao
,IdContaMae
,Codigo
,CodigoReduzida)
select IdPlano, 1, '', null, ContaDebito, ContaDebitoReduzida from ContabRoteiro
group by IdPlano, ContaDebito, ContaDebitoReduzida
go

insert into ContabPlanoContas
(IdPlano
,TipoConta
,Descricao
,IdContaMae
,Codigo
,CodigoReduzida)
select IdPlano, 1, '', null, ContaCredito, ContaCreditoReduzida from ContabRoteiro
group by IdPlano, ContaCredito, ContaCreditoReduzida
go

update ContabLancamento set IdContaDebito = c.IdConta
from ContabLancamento l, ContabPlanoContas c
where l.ContaDebito = c.Codigo
go

update ContabLancamento set IdContaCredito = c.IdConta
from ContabLancamento l, ContabPlanoContas c
where l.ContaCredito = c.Codigo
go