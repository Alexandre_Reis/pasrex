CREATE VIEW V_OPERACAOCOTISTA AS  
select case when (o.codigointerface is null or o.codigointerface = '') then str(o.IdCotista) else o.codigointerface end as CODIGO_CLIENTE_ATIVA,  
 case when (c.CodigoYMF is null or c.CodigoYMF = '') then str(c.IdCliente) else c.CodigoYMF end as CODIGO_FUNDO_ATIVA,  
 o.Nome as NM_CLIENTE,  
 p.IdOperacao as ID_OPERACAO,  
 p.TipoOperacao as TIPO_RESGATE,  
 p.DataOperacao as DATA_OPERACAO,  
 p.DataConversao as DATA_CONVERSAO,  
 p.DataLiquidacao as DATA_LIQUIDACAO,  
 p.Quantidade as QTDCOTAS,  
 p.ValorBruto as VALORBRUTO,  
 p.ValorIOF as VALORIOF,  
 p.ValorIR as VALORIR,  
 p.ValorLiquido as VALORLIQUIDO  
from Cotista o, ClienteInterface c, OperacaoCotista p  
where p.IdCotista = o.IdCotista  
and p.IdCarteira = c.IdCliente  
and p.TipoOperacao in (2, 3, 4, 5, 20)  
and p.IdCarteira <> p.IdCotista  


CREATE VIEW V_POSICAOCOTISTA AS  
select l.DataDia as DATAPOSICAO,  
 case when (o.codigointerface is null or o.codigointerface = '') then str(o.IdCotista) else o.codigointerface end as CODIGO_CLIENTE_ATIVA,  
 case when (c.CodigoYMF is null or c.CodigoYMF = '') then str(c.IdCliente) else c.CodigoYMF end as CODIGO_FUNDO_ATIVA,  
 p.DataAplicacao as DT_APLICACAO,  
 o.Nome as NM_CLIENTE,  
 p.Quantidade as QTDCOTAS,  
 p.ValorAplicacao as VALORAPLICACAO,  
 p.ValorBruto as VALORCORRIGIDO,  
 p.CotaDia as VALORCOTA,  
 p.ValorIOF as VALORIOF,  
 p.ValorIR as VALORIR,  
 p.Quantidade * (p.CotaDia - p.CotaAplicacao) as VALORRENDIMENTO,  
 p.ValorLiquido as VALORRESGATE  
from Cotista o, ClienteInterface c, PosicaoCotista p, Cliente l  
where p.IdCotista = o.IdCotista  
and p.IdCarteira = c.IdCliente  
and p.IdCarteira = l.IdCliente  
and p.IdCarteira <> p.IdCotista  