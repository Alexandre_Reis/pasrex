create view V_OPERACOESTOTAL AS
select c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
p.DataOperacao, p.DataConversao, p.DataLiquidacao, p.ValorBruto, p.ValorLiquido, p.Quantidade, p.TipoOperacao
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao not in (10, 11, 12, 20, 100, 101)
