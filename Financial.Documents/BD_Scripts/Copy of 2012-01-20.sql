alter table agentemercado add ISS decimal (8, 2) null
go
update agentemercado set ISS = 0
go
alter table agentemercado alter column ISS decimal (8, 2) not null
go