﻿update cliente set nome = 'Cliente ' + ltrim(rtrim(idcliente)), apelido = 'Cliente ' + ltrim(rtrim(idcliente))
where idtipo not in (500, 200) -- Clientes diferentes de Clube, Fundo
update carteira set nome = 'Cliente ' + ltrim(rtrim(idcarteira)), apelido = 'Cliente ' + ltrim(rtrim(idcarteira))
where idcarteira not in (select idcliente from cliente where idtipo in (500, 200)) -- Clientes diferentes de Clube, Fundo
update pessoa set nome = 'Cliente ' + ltrim(rtrim(idpessoa)), apelido = 'Cliente ' + ltrim(rtrim(idpessoa))
where idpessoa not in (select idcliente from cliente where idtipo in (500, 200)) -- Clientes diferentes de Clube, Fundo

update cliente set nome = 'Clube ' + ltrim(rtrim(idcliente)), apelido = 'Clube ' + ltrim(rtrim(idcliente))
where idtipo in (200) -- Clube
update carteira set nome = 'Clube ' + ltrim(rtrim(idcarteira)), apelido = 'Clube ' + ltrim(rtrim(idcarteira))
where idcarteira in (select idcliente from cliente where idtipo in (200)) -- Clube
update pessoa set nome = 'Clube ' + ltrim(rtrim(idpessoa)), apelido = 'Clube ' + ltrim(rtrim(idpessoa))
where idpessoa in (select idcliente from cliente where idtipo in (200)) -- Clube

update cliente set nome = 'Fundo ' + ltrim(rtrim(idcliente)), apelido = 'Fundo ' + ltrim(rtrim(idcliente))
where idtipo in (500) and tipocontrole in (4, 5) -- Fundo apenas cotista e completo
update carteira set nome = 'Fundo ' + ltrim(rtrim(idcarteira)), apelido = 'Fundo ' + ltrim(rtrim(idcarteira))
where idcarteira in (select idcliente from cliente where idtipo in (500) and tipocontrole in (4, 5)) -- Fundo apenas cotista e completo
update pessoa set nome = 'Fundo ' + ltrim(rtrim(idpessoa)), apelido = 'Fundo ' + ltrim(rtrim(idpessoa))
where idpessoa in (select idcliente from cliente where idtipo in (500) and tipocontrole in (4, 5)) -- Fundo apenas cotista e completo

update cotista set nome = 'Cotista ' + ltrim(rtrim(idcotista)), apelido = 'Cotista ' + ltrim(rtrim(idcotista))

update pessoa set cpfcnpj = '06972123780' where tipo = 1 
update pessoa set cpfcnpj = '10309578000188' where tipo = 2

update pessoaendereco set endereco = 'RUA ARMANDO PENTEADO', numero = '558', complemento = '4o ANDAR', BAIRRO = 'PINHEIROS', CIDADE = 'SÃO PAULO', CEP = '19781-340', UF = 'SP', FONE = '(011) 3456-9801', EMAIL = 'EFREITAS@FINANCIAL.COM.BR'

delete from usuario where idusuario <> 1 and idcliente is null
update usuario set senha = 'ZmQFeslzoCMuis4ztTfa0hwpcKsIqpFe/Z+WprEJPEE=', email = 'efreitas@atatika.com',
				   paginainicial = '/FIN_TRIAL/Relatorios/Fundo/FiltroReportComposicaoCarteira.aspx',
				   trocasenha = 'N', tipotrava = 1, statusativo = 'S', statusbloqueado = 'N'
update usuario set nome = ltrim(rtrim(idcliente)), login = ltrim(rtrim(idcliente)) where idcliente is not null

update configuracao set valortexto = '<span style="font-family: ''Times New Roman''; font-size: 8pt;''>&nbsp;Os dados apresentados estão sujeitos à confirmação. As informações apresentadas não são oficiais e no caso de discrepância entre os valores apresentados por administradores e custodiantes, e o sistema Financial, considerar oficial os valores obtidos diretamente junto ao administrador ou custodiante.</span><span style=''font-family: ''Times New Roman''; font-size: 8pt;''>&nbsp;Caso tenha alguma dúvida, favor entrar em contato por e-mail. </span><span style="font-family: ''Times New Roman''; font-size: 7pt;''>.</span>'
where id = 4001
update officer set nome = 'Officer ' + ltrim(rtrim(idofficer)), apelido = 'Officer ' + ltrim(rtrim(idofficer))

update assessor set nome = 'Assessor ' + rtrim(str(idassessor)), apelido = 'Assessor ' + rtrim(str(idassessor))

update ativobolsa set idestrategia = 1

update cliente set BookEmail = 'eduardo@financialonline.com.br'
update cliente set BookAssunto = 'Relatórios Diários'
update cliente set BookMensagem = 'A Área de investimentos permanece à disposição para quaisquer informações ou esclarecimentos adicionais.'

update book set assunto = 'Relatórios Diários'
update book set mensagem = 'Qualquer dúvida entre em contato.'
update book set email = 'eduardo@financialonline.com.br'
update book set emailfrom = 'backoffice@financialonline.com.br'

update liquidacaohistorico set descricao = 'Valor Líquido de Bolsa'
where descricao like '%quido de Bolsa%'

update liquidacaohistorico set descricao = 'Valor Líquido de BMF'
where descricao like '%quido de BMF%'

update liquidacaoabertura set descricao = 'Valor Líquido de Bolsa'
where descricao like '%quido de Bolsa%'

update liquidacaoabertura set descricao = 'Valor Líquido de BMF'
where descricao like '%quido de BMF%'

update liquidacao set descricao = 'Valor Líquido de Bolsa'
where descricao like '%quido de Bolsa%'

update liquidacao set descricao = 'Valor Líquido de BMF'
where descricao like '%quido de BMF%'

update liquidacaofuturo set descricao = 'Valor Líquido de Bolsa'
where descricao like '%quido de Bolsa%'

update liquidacaofuturo set descricao = 'Valor Líquido de BMF'
where descricao like '%quido de BMF%'

update cliente set calculacontabil = 'S', IdPlano = 1, ContabilAcoes = 1, ContabilBMF = 1, ContabilFundos = 1, ContabilLiquidacao = 1
where nome like 'Clube%' or nome like 'Fundo%'

update carteira set idagenteadministrador = 173, idagentegestor = 173 -- Fundo/Clube controle completo
where idcarteira in (select idcliente from cliente where idtipo in (200, 500) and tipocontrole = 5) 

update posicaobolsa set idagente = 173
update posicaobolsahistorico set idagente = 173
update posicaobolsaabertura set idagente = 173
update ordembolsa set idagentecorretora = 173
update operacaobolsa set idagentecorretora = 173
update posicaotermobolsa set idagente = 173 
update posicaotermobolsahistorico set idagente = 173
update posicaotermobolsaabertura set idagente = 173
update posicaoemprestimobolsa set idagente = 173
update posicaoemprestimobolsahistorico set idagente = 173
update posicaoemprestimobolsaabertura set idagente = 173
update operacaoemprestimobolsa set idagente = 173
update ordembmf set idagentecorretora = 173
update operacaobmf set idagentecorretora = 173
update posicaobmf set idagente = 173
update posicaobmfhistorico set idagente = 173
update posicaobmfabertura set idagente = 173

