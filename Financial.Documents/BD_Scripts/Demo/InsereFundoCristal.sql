set identity_insert [dbo].PapelRendaFixa on
insert into PapelRendaFixa
(IdPapel
,TipoPapel
,Descricao
,TipoRentabilidade
,CasasDecimaisPU
,TipoCurva
,ContagemDias
,BaseAno
,TipoVolume
,TipoCustodia
,Classe
,PagamentoJuros)
select IdPapel
,TipoPapel
,Descricao
,TipoRentabilidade
,CasasDecimaisPU
,TipoCurva
,ContagemDias
,BaseAno
,TipoVolume
,TipoCustodia
,Classe
,PagamentoJuros
from FIN_CRISTAL..PapelRendaFixa
where IdPapel not in (select IdPapel from PapelRendaFixa)
set identity_insert [dbo].PapelRendaFixa off

set identity_insert [dbo].TituloRendaFixa on
insert into TituloRendaFixa
(IdTitulo
,IdPapel
,IdIndice
,IdEmissor
,Descricao
,Taxa
,Percentual
,DataEmissao
,DataVencimento
,ValorNominal
,CodigoCustodia
,PUNominal
,TipoMTM
,IdSerie
,DescricaoCompleta
,IsentoIR
,IsentoIOF
,IdMoeda
,IdEstrategia
,CodigoCDA)
select IdTitulo
,IdPapel
,IdIndice
,IdEmissor
,Descricao
,Taxa
,Percentual
,DataEmissao
,DataVencimento
,ValorNominal
,CodigoCustodia
,PUNominal
,TipoMTM
,IdSerie
,DescricaoCompleta
,IsentoIR
,IsentoIOF
,IdMoeda
,IdEstrategia
,CodigoCDA
from FIN_CRISTAL..TituloRendaFixa
where IdTitulo not in (select IdTitulo from TituloRendaFixa)
set identity_insert [dbo].TituloRendaFixa off

insert into PosicaoRendaFixa
(IdCliente
,IdTitulo
,TipoOperacao
,DataVencimento
,Quantidade
,QuantidadeBloqueada
,DataOperacao
,DataLiquidacao
,PUOperacao
,PUCurva
,ValorCurva
,PUMercado
,ValorMercado
,PUJuros
,ValorJuros
,DataVolta
,TaxaVolta
,PUVolta
,ValorVolta
,QuantidadeInicial
,ValorIR
,ValorIOF
,TipoNegociacao
,PUCorrecao
,ValorCorrecao
,TaxaOperacao
,TaxaCustodia)
select IdCliente
,IdTitulo
,TipoOperacao
,DataVencimento
,Quantidade
,QuantidadeBloqueada
,DataOperacao
,DataLiquidacao
,PUOperacao
,PUCurva
,ValorCurva
,PUMercado
,ValorMercado
,PUJuros
,ValorJuros
,DataVolta
,TaxaVolta
,PUVolta
,ValorVolta
,QuantidadeInicial
,ValorIR
,ValorIOF
,TipoNegociacao
,PUCorrecao
,ValorCorrecao
,TaxaOperacao
,TaxaCustodia
from fin_cristal..PosicaoRendaFixa

insert into PosicaoRendaFixaHistorico
(DataHistorico
,IdCliente
,IdTitulo
,TipoOperacao
,DataVencimento
,Quantidade
,QuantidadeBloqueada
,DataOperacao
,DataLiquidacao
,PUOperacao
,PUCurva
,ValorCurva
,PUMercado
,ValorMercado
,PUJuros
,ValorJuros
,DataVolta
,TaxaVolta
,PUVolta
,ValorVolta
,QuantidadeInicial
,ValorIR
,ValorIOF
,TipoNegociacao
,PUCorrecao
,ValorCorrecao
,TaxaOperacao
,TaxaCustodia)
select DataHistorico
,IdCliente
,IdTitulo
,TipoOperacao
,DataVencimento
,Quantidade
,QuantidadeBloqueada
,DataOperacao
,DataLiquidacao
,PUOperacao
,PUCurva
,ValorCurva
,PUMercado
,ValorMercado
,PUJuros
,ValorJuros
,DataVolta
,TaxaVolta
,PUVolta
,ValorVolta
,QuantidadeInicial
,ValorIR
,ValorIOF
,TipoNegociacao
,PUCorrecao
,ValorCorrecao
,TaxaOperacao
,TaxaCustodia
from fin_cristal..PosicaoRendaFixaHistorico

insert into PosicaoBolsa
(IdAgente
,IdCliente
,CdAtivoBolsa
,TipoMercado
,DataVencimento
,PUMercado
,PUCusto
,PUCustoLiquido
,ValorMercado
,ValorCustoLiquido
,Quantidade
,QuantidadeInicial
,QuantidadeBloqueada
,ResultadoRealizar)
select IdAgente
,IdCliente
,CdAtivoBolsa
,TipoMercado
,DataVencimento
,PUMercado
,PUCusto
,PUCustoLiquido
,ValorMercado
,ValorCustoLiquido
,Quantidade
,QuantidadeInicial
,QuantidadeBloqueada
,ResultadoRealizar
from FIN_CRISTAL..PosicaoBolsa

insert into PosicaoBolsaHistorico
(DataHistorico
,IdAgente
,IdCliente
,CdAtivoBolsa
,TipoMercado
,DataVencimento
,PUMercado
,PUCusto
,PUCustoLiquido
,ValorMercado
,ValorCustoLiquido
,Quantidade
,QuantidadeInicial
,QuantidadeBloqueada
,ResultadoRealizar)
select DataHistorico
,IdAgente
,IdCliente
,CdAtivoBolsa
,TipoMercado
,DataVencimento
,PUMercado
,PUCusto
,PUCustoLiquido
,ValorMercado
,ValorCustoLiquido
,Quantidade
,QuantidadeInicial
,QuantidadeBloqueada
,ResultadoRealizar
from FIN_CRISTAL..PosicaoBolsaHistorico

insert into PosicaoEmprestimoBolsa
(IdOperacao
,IdCliente
,IdAgente
,CdAtivoBolsa
,Quantidade
,PUMercado
,PULiquidoOriginal
,ValorCorrigidoJuros
,ValorCorrigidoComissao
,ValorCorrigidoCBLC
,ValorBase
,PontaEmprestimo
,DataRegistro
,DataVencimento
,TaxaOperacao
,TaxaComissao
,NumeroContrato
,TipoEmprestimo
,ValorMercado
,IdTrader
,ValorDiarioJuros
,ValorDiarioComissao
,ValorDiarioCBLC)
select IdOperacao
,IdCliente
,IdAgente
,CdAtivoBolsa
,Quantidade
,PUMercado
,PULiquidoOriginal
,ValorCorrigidoJuros
,ValorCorrigidoComissao
,ValorCorrigidoCBLC
,ValorBase
,PontaEmprestimo
,DataRegistro
,DataVencimento
,TaxaOperacao
,TaxaComissao
,NumeroContrato
,TipoEmprestimo
,ValorMercado
,IdTrader
,ValorDiarioJuros
,ValorDiarioComissao
,ValorDiarioCBLC
from FIN_CRISTAL..PosicaoEmprestimoBolsa

insert into PosicaoEmprestimoBolsaHistorico
(DataHistorico
,IdOperacao
,IdCliente
,IdAgente
,CdAtivoBolsa
,Quantidade
,PUMercado
,PULiquidoOriginal
,ValorCorrigidoJuros
,ValorCorrigidoComissao
,ValorCorrigidoCBLC
,ValorBase
,PontaEmprestimo
,DataRegistro
,DataVencimento
,TaxaOperacao
,TaxaComissao
,NumeroContrato
,TipoEmprestimo
,ValorMercado
,IdTrader
,ValorDiarioJuros
,ValorDiarioComissao
,ValorDiarioCBLC)
select DataHistorico
,IdOperacao
,IdCliente
,IdAgente
,CdAtivoBolsa
,Quantidade
,PUMercado
,PULiquidoOriginal
,ValorCorrigidoJuros
,ValorCorrigidoComissao
,ValorCorrigidoCBLC
,ValorBase
,PontaEmprestimo
,DataRegistro
,DataVencimento
,TaxaOperacao
,TaxaComissao
,NumeroContrato
,TipoEmprestimo
,ValorMercado
,IdTrader
,ValorDiarioJuros
,ValorDiarioComissao
,ValorDiarioCBLC
from FIN_CRISTAL..PosicaoEmprestimoBolsaHistorico

insert into PosicaoFundo
(IdOperacao
,IdCliente
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento)
select IdOperacao
,IdCliente
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento
from FIN_CRISTAL..PosicaoFundo

insert into PosicaoFundoHistorico
(DataHistorico
,IdOperacao
,IdCliente
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento)
select DataHistorico
,IdOperacao
,IdCliente
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento
from FIN_CRISTAL..PosicaoFundoHistorico

insert into Liquidacao
(IdCliente
,DataLancamento
,DataVencimento
,Descricao
,Valor
,Situacao
,Origem
,Fonte
,IdAgente
,IdentificadorOrigem
,IdConta
,IdEvento)
select IdCliente
,DataLancamento
,DataVencimento
,Descricao
,Valor
,Situacao
,Origem
,Fonte
,IdAgente
,IdentificadorOrigem
,IdConta
,IdEvento
from FIN_CRISTAL..Liquidacao

insert into LiquidacaoHistorico
(DataHistorico
,IdCliente
,DataLancamento
,DataVencimento
,Descricao
,Valor
,Situacao
,Origem
,Fonte
,IdAgente
,IdentificadorOrigem
,IdConta
,IdEvento)
select DataHistorico
,IdCliente
,DataLancamento
,DataVencimento
,Descricao
,Valor
,Situacao
,Origem
,Fonte
,IdAgente
,IdentificadorOrigem
,IdConta
,IdEvento
from FIN_CRISTAL..LiquidacaoHistorico

insert into SaldoCaixa
(IdCliente
,Data
,IdConta
,SaldoAbertura
,SaldoFechamento)
select IdCliente
,Data
,IdConta
,SaldoAbertura
,SaldoFechamento
from FIN_CRISTAL..SaldoCaixa

insert into PosicaoCotista
(IdOperacao
,IdCotista
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento)
select IdOperacao
,IdCotista
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento
from FIN_CRISTAL..PosicaoCotista

insert into PosicaoCotistaHistorico
(DataHistorico
,IdOperacao
,IdCotista
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento)
select DataHistorico
,IdOperacao
,IdCotista
,IdCarteira
,ValorAplicacao
,DataAplicacao
,DataConversao
,CotaAplicacao
,CotaDia
,ValorBruto
,ValorLiquido
,QuantidadeInicial
,Quantidade
,QuantidadeBloqueada
,DataUltimaCobrancaIR
,ValorIR
,ValorIOF
,ValorPerformance
,ValorIOFVirtual
,QuantidadeAntesCortes
,ValorRendimento
from FIN_CRISTAL..PosicaoCotistaHistorico

insert into OperacaoCotista
(IdCotista
,IdCarteira
,DataOperacao
,DataConversao
,DataLiquidacao
,DataAgendamento
,TipoOperacao
,TipoResgate
,IdPosicaoResgatada
,IdFormaLiquidacao
,Quantidade
,CotaOperacao
,ValorBruto
,ValorLiquido
,ValorIR
,ValorIOF
,ValorCPMF
,ValorPerformance
,PrejuizoUsado
,RendimentoResgate
,VariacaoResgate
,Observacao
,DadosBancarios
,Fonte
,IdConta
,CotaInformada)
select IdCotista
,IdCarteira
,DataOperacao
,DataConversao
,DataLiquidacao
,DataAgendamento
,TipoOperacao
,TipoResgate
,IdPosicaoResgatada
,IdFormaLiquidacao
,Quantidade
,CotaOperacao
,ValorBruto
,ValorLiquido
,ValorIR
,ValorIOF
,ValorCPMF
,ValorPerformance
,PrejuizoUsado
,RendimentoResgate
,VariacaoResgate
,Observacao
,DadosBancarios
,Fonte
,IdConta
,CotaInformada
from FIN_CRISTAL..OperacaoCotista

insert into HistoricoCota
(Data
,IdCarteira
,CotaAbertura
,CotaFechamento
,CotaBruta
,PLAbertura
,PLFechamento
,PatrimonioBruto
,QuantidadeFechamento
,AjustePL)
select Data
,IdCarteira
,CotaAbertura
,CotaFechamento
,CotaBruta
,PLAbertura
,PLFechamento
,PatrimonioBruto
,QuantidadeFechamento
,AjustePL
from FIN_CRISTAL..HistoricoCota

set identity_insert [dbo].TabelaTaxaAdministracao on
insert into TabelaTaxaAdministracao
(IdTabela
,DataReferencia
,IdCarteira
,TipoCalculo
,TipoApropriacao
,Taxa
,BaseApuracao
,BaseAno
,TipoLimitePL
,ValorLimite
,ValorFixoTotal
,ContagemDias
,ValorMinimo
,ValorMaximo
,NumeroMesesRenovacao
,NumeroDiasPagamento
,ImpactaPL
,IdCadastro
,DataFim
,IdEventoProvisao
,IdEventoPagamento)
select IdTabela
,DataReferencia
,IdCarteira
,TipoCalculo
,TipoApropriacao
,Taxa
,BaseApuracao
,BaseAno
,TipoLimitePL
,ValorLimite
,ValorFixoTotal
,ContagemDias
,ValorMinimo
,ValorMaximo
,NumeroMesesRenovacao
,NumeroDiasPagamento
,ImpactaPL
,IdCadastro
,DataFim
,IdEventoProvisao
,IdEventoPagamento
from FIN_CRISTAL..TabelaTaxaAdministracao
set identity_insert [dbo].TabelaTaxaAdministracao off

insert into CalculoAdministracao
(IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado)
select IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado
from FIN_CRISTAL..CalculoAdministracao

insert into CalculoAdministracaoHistorico
(DataHistorico
,IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado)
select DataHistorico
,IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado
from FIN_CRISTAL..CalculoAdministracaoHistorico

set identity_insert [dbo].CadastroProvisao on
insert into CadastroProvisao
(IdCadastro
,Descricao
,Tipo)
select IdCadastro
,Descricao
,Tipo
from FIN_CRISTAL..CadastroProvisao
set identity_insert [dbo].CadastroProvisao off

set identity_insert [dbo].TabelaProvisao on
insert into TabelaProvisao
(IdTabela
,DataReferencia
,IdCarteira
,TipoCalculo
,ValorTotal
,ContagemDias
,DiaRenovacao
,NumeroMesesRenovacao
,NumeroDiasPagamento
,IdCadastro
,DataFim
,IdEventoProvisao
,IdEventoPagamento)
select IdTabela
,DataReferencia
,IdCarteira
,TipoCalculo
,ValorTotal
,ContagemDias
,DiaRenovacao
,NumeroMesesRenovacao
,NumeroDiasPagamento
,IdCadastro
,DataFim
,IdEventoProvisao
,IdEventoPagamento
from FIN_CRISTAL..TabelaProvisao
set identity_insert [dbo].TabelaProvisao off

insert into CalculoProvisao
(IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado)
select IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado
from FIN_CRISTAL..CalculoProvisao

insert into CalculoProvisaoHistorico
(DataHistorico
,IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado)
select DataHistorico
,IdTabela
,IdCarteira
,ValorDia
,ValorAcumulado
,DataFimApropriacao
,DataPagamento
,ValorCPMFDia
,ValorCPMFAcumulado
from FIN_CRISTAL..CalculoProvisaoHistorico

insert into CalculoGerencial
(IdCliente
,IdTrader
,CdAtivo
,DataCalculo
,TipoMercado
,DataVencimento
,Quantidade
,PUCusto
,PUMercado
,ValorMercado
,ResultadoRealizar
,ResultadoDayTrade
,ResultadoNormal
,ResultadoAcumulado
,ResultadoDia
,ValorizacaoDia)
select IdCliente
,IdTrader
,CdAtivo
,DataCalculo
,TipoMercado
,DataVencimento
,Quantidade
,PUCusto
,PUMercado
,ValorMercado
,ResultadoRealizar
,ResultadoDayTrade
,ResultadoNormal
,ResultadoAcumulado
,ResultadoDia
,ValorizacaoDia
from FIN_CRISTAL..CalculoGerencial

