/*************************     PerfilCorretagemBMF	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 7000
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PerfilCorretagemBMF where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPerfilCorretagem ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PerfilCorretagemBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/