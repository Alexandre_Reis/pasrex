/*************************      Liquidacao	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 41623
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.Liquidacao where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.Liquidacao SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/

/*************************    LiquidacaoAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 52374
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.LiquidacaoAbertura where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/

/*************************    LiquidacaoHistorico	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 52374
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.LiquidacaoHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente

/********************************************************************************************************************/

/*************************      LiquidacaoFuturo	*****************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 7000
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoFuturo ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.LiquidacaoFuturo where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/