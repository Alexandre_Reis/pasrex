﻿-- BMF
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from PosicaoBMF where idcliente = @idCliente
select * from PosicaoBMFAbertura where idcliente = @idCliente
select * from PosicaoBMFhistorico where idcliente = @idCliente
select * from OperacaoBMF where idcliente = @idCliente
select * from OrdemBMF where idcliente = @idCliente
select * from PerfilCorretagemBMF where idcliente = @idCliente
----------------------------------------------------------------


-- Bolsa
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from PosicaoBolsa where idcliente = @idCliente
select * from PosicaoBolsaAbertura where idcliente = @idCliente
select * from PosicaoBolsaHistorico where idcliente = @idCliente
select * from Posicaoemprestimobolsa where idcliente = @idCliente
select * from PosicaoEmprestimoBolsaAbertura where idcliente = @idCliente
select * from PosicaoEmprestimoBolsaHistorico where idcliente = @idCliente
select * from OperacaoBolsa where idcliente = @idCliente
select * from OperacaoEmprestimoBolsa where idcliente = @idCliente
select * from OrdemBolsa where idcliente = @idCliente
select * from PerfilCorretagemBolsa where idcliente = @idCliente
select * from ProventoBolsaCliente where idcliente = @idCliente
----------------------------------------------------------------


-- HistoricoCota
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from HistoricoCota where idcarteira = @idCliente
select * from Posicaocotista where idcarteira = @idCliente
select * from PosicaocotistaAbertura where idcarteira = @idCliente
select * from PosicaocotistaHistorico where idcarteira = @idCliente
select * from OperacaoCotista where idcarteira = @idCliente
----------------------------------------------------------------

-- Fundo
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from Posicaofundo where idcliente = @idCliente
select * from PosicaofundoAbertura where idcliente = @idCliente
select * from PosicaofundoHistorico where idcliente = @idCliente
select * from OperacaoFundo where idcliente = @idCliente
select * from PrejuizoFundo where idcliente = @idCliente
select * from PrejuizoFundoHistorico where idcliente = @idCliente
----------------------------------------------------------------

-- RendaFixa
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from Posicaorendafixa where idcliente = @idCliente
select * from PosicaorendafixaAbertura where idcliente = @idCliente
select * from PosicaorendafixaHistorico where idcliente = @idCliente
select * from OperacaoRendaFixa where idcliente = @idCliente
----------------------------------------------------------------

-- Liquidação
----------------------------------------------------------------
DECLARE @idCliente int

SET @idCliente = 1361

select * from Liquidacao where idcliente = @idCliente
select * from LiquidacaoAbertura where idcliente = @idCliente
select * from LiquidacaoHistorico where idcliente = @idCliente
select * from LiquidacaoFuturo where idcliente = @idCliente
select * from SaldoCaixa where idcliente = @idCliente
----------------------------------------------------------------