-- Procedure para copiar Dados de um Banco para outro
--
CREATE PROCEDURE CopyTables
	@Banco_Origem varchar(200), @Banco_Destino nvarchar(200),
	@idClienteOrigem int, @idClienteDestino int 

AS

DECLARE @sql nvarchar(2000)

BEGIN TRAN UpdateTransaction

BEGIN

-- /************************* PosicaoBMF	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoBMF where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PosicaoBMF'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************/
-- /************************* PosicaoBMFAbertura	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBMFAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBMFAbertura P, ' + @Banco_Origem + '.dbo.PosicaoBMFAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoBMFAbertura'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************/
-- /************************* PosicaoBMFHistorico	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBMFHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBMFHistorico P, ' + @Banco_Origem + '.dbo.PosicaoBMFHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoBMFHistorico'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************/

-- /*************************  PosicaoBolsa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoBolsa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsa SELECT * FROM #TempTable ' +		   
		   ' DROP TABLE #TempTable '
print 'PosicaoBolsa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************/
-- /************************* PosicaoBolsaAbertura	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBolsaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBolsaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoBolsaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoBolsaAbertura'  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************/
-- /************************* PosicaoBolsaHistorico	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBolsaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBolsaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoBolsaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoBolsaHistorico'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
-- /********************************************************************************************************************

-- /************************* PosicaoCotista ******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoCotista where IdCarteira = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotista SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PosicaoCotista'		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoCotistaAbertura ******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoCotistaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoCotistaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoCotistaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoCotistaAbertura'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoCotistaHistorico ******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoCotistaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoCotistaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoCotistaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoCotistaHistorico'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************

--/*************************  PosicaoEmprestimoBolsa ******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PosicaoEmprestimoBolsa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoEmprestimoBolsaAbertura  *****************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoEmprestimoBolsaAbertura'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************    PosicaoEmprestimoBolsaHistorico	*****************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente =  @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoEmprestimoBolsaHistorico'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************

--//*************************  PosicaoFundo	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoFundo where IdCarteira = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundo SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PosicaoFundo'		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoFundoAbertura	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoFundoAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoFundoAbertura P, ' + @Banco_Origem + '.dbo.PosicaoFundoAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoFundoAbertura'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoFundoHistorico	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoFundoHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoFundoHistorico P, ' + @Banco_Origem + '.dbo.PosicaoFundoHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoFundoHistorico'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--/*************************  PosicaoRendaFixa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoRendaFixa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoRendaFixa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PosicaoRendaFixa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoRendaFixaAbertura	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoRendaFixaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoRendaFixaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoRendaFixaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoRendaFixaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoRendaFixaAbertura'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************  PosicaoRendaFixaHistorico	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoRendaFixaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoRendaFixaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoRendaFixaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoRendaFixaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
print 'PosicaoRendaFixaHistorico'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/

--/*************************    OperacaoBMF	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoBMF where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoBMF'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************    OperacaoBolsa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoBolsa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoBolsa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoBolsa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************    OperacaoCotista	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoCotista where IdCarteira = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoCotista SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoCotista'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************    OperacaoEmprestimoBolsa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoEmprestimoBolsa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoEmprestimoBolsa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************    OperacaoFundo	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoFundo where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoFundo SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoFundo'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************    OperacaoRendaFixa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoRendaFixa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoRendaFixa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OperacaoRendaFixa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/

--/*************************      HistoricoCota	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.HistoricoCota where IdCarteira = @idClienteOrigem ' +
		   ' Update #TempTable set idCarteira = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.HistoricoCota SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'HistoricoCota'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************

--//*************************      Liquidacao	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.Liquidacao where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.Liquidacao SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'Liquidacao'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************    LiquidacaoAbertura	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoAbertura L, ' + @Banco_Origem + '.dbo.LiquidacaoAbertura L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '

print 'LiquidacaoAbertura'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************    LiquidacaoHistorico	******************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoHistorico L, ' + @Banco_Origem + '.dbo.LiquidacaoHistorico L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '

print 'LiquidacaoHistorico'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino                         
--/********************************************************************************************************************/
--/*************************      LiquidacaoFuturo	*****************************************************************/
SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoFuturo' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoFuturo L, ' + @Banco_Origem + '.dbo.LiquidacaoFuturo L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoFuturo SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '

print 'LiquidacaoFuturo'		  		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino                         
--/********************************************************************************************************************

--//*************************     OrdemBMF	 ******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OrdemBMF where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OrdemBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OrdemBMF'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************     OrdemBolsa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OrdemBolsa where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OrdemBolsa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'OrdemBolsa'		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************     PerfilCorretagemBMF	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PerfilCorretagemBMF where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPerfilCorretagem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PerfilCorretagemBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PerfilCorretagemBMF'		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************     PerfilCorretagemBolsa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PerfilCorretagemBolsa where idCliente = @idClienteOrigem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PerfilCorretagemBolsa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PerfilCorretagemBolsa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************      PrejuizoFundo	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PrejuizoFundo where idCliente = @idClienteOrigem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PrejuizoFundo SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PrejuizoFundo'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************      PrejuizoFundoHistorico	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PrejuizoFundoHistorico where idCliente = @idClienteOrigem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PrejuizoFundoHistorico SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'PrejuizoFundoHistorico'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************
--//*************************     ProventoBolsaCliente	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.ProventoBolsaCliente where IdCliente = @idClienteOrigem ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idProvento ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.ProventoBolsaCliente SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'ProventoBolsaCliente'		   
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/
--/*************************     SaldoCaixa	******************************************************************/
SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.SaldoCaixa where idCliente = @idClienteOrigem ' +
		   ' Update #TempTable set idCliente = @idClienteDestino ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.SaldoCaixa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
print 'SaldoCaixa'
EXECUTE sp_executesql @sql, 
					  N'@idClienteOrigem int, @idClienteDestino int',
				      @idClienteOrigem = @idClienteOrigem, 
				      @idClienteDestino = @idClienteDestino
--/********************************************************************************************************************/

END

IF @@ERROR <> 0 -- Erro
	BEGIN

		print '------- ROLLBACK Transaction pois houve ERRO'

		ROLLBACK		
	END
ELSE

BEGIN
	print 'COMMIT'
	
	COMMIT
END


GO