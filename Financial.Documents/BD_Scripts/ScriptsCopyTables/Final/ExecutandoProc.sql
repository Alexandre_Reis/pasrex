metodo 1

USE [FIN_TESTE_IRMAO]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[CopyTables]
		@Banco_Origem = N'fin_teste',
		@Banco_Destino = N'fin_teste_irmao',
		@idClienteOrigem = 91434,
		@idClienteDestino = 7000

SELECT	'Return Value' = @return_value

GO
-------------------------------------------------------
metodo 2

USE [FIN_TESTE_IRMAO]
GO

EXEC CopyTables 'fin_teste', 'fin_teste_irmao', 91434, 7000

GO
-------------------------------------------------------------

metodo 3

DECLARE @RC int
DECLARE @Banco_Origem varchar(200)
DECLARE @Banco_Destino nvarchar(200)
DECLARE @idClienteOrigem int
DECLARE @idClienteDestino int

-- TODO: Set parameter values here.

EXECUTE @RC = [FIN_TESTE_IRMAO].[dbo].[CopyTables] 
   @Banco_Origem
  ,@Banco_Destino
  ,@idClienteOrigem
  ,@idClienteDestino
GO
