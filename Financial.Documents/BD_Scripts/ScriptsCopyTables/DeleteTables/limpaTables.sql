DECLARE @idCliente int
DECLARE @idCarteira int

SET @idCliente = 1361
SET @idCarteira = 1361

select * from PosicaoBMF where idcliente = @idCliente
select * from PosicaoBMFAbertura where idcliente = @idCliente
select * from PosicaoBMFhistorico where idcliente = @idCliente
select * from PosicaoBolsa where idcliente = @idCliente
select * from PosicaoBolsaAbertura where idcliente = @idCliente
select * from PosicaoBolsaHistorico where idcliente = @idCliente
select * from Posicaocotista where idcarteira = @idCarteira
select * from PosicaocotistaAbertura where idcarteira = @idCarteira
select * from PosicaocotistaHistorico where idcarteira = @idCarteira
select * from Posicaoemprestimobolsa where idcliente = @idCliente
select * from PosicaoEmprestimoBolsaAbertura where idcliente = @idCliente
select * from PosicaoEmprestimoBolsaHistorico where idcliente = @idCliente
select * from Posicaofundo where idcliente = @idCliente
select * from PosicaofundoAbertura where idcliente = @idCliente
select * from PosicaofundoHistorico where idcliente = @idCliente
select * from Posicaorendafixa where idcliente = @idCliente
select * from PosicaorendafixaAbertura where idcliente = @idCliente
select * from PosicaorendafixaHistorico where idcliente = @idCliente
select * from OperacaoBMF where idcliente = @idCliente
select * from OperacaoBolsa where idcliente = @idCliente
select * from OperacaoCotista where idcarteira = @idCarteira
select * from OperacaoEmprestimoBolsa where idcliente = @idCliente
select * from OperacaoFundo where idcliente = @idCliente
select * from OperacaoRendaFixa where idcliente = @idCliente
select * from HistoricoCota where idcarteira = @idCarteira
select * from Liquidacao where idcliente = @idCliente
select * from LiquidacaoAbertura where idcliente = @idCliente
select * from Liquidacao where idcliente = @idCliente
select * from LiquidacaoAbertura where idcliente = @idCliente
select * from LiquidacaoHistorico where idcliente = @idCliente
select * from LiquidacaoFuturo where idcliente = @idCliente
select * from OrdemBMF where idcliente = @idCliente
select * from OrdemBolsa where idcliente = @idCliente
select * from PerfilCorretagemBMF where idcliente = @idCliente
select * from PerfilCorretagemBolsa where idcliente = @idCliente
select * from PrejuizoFundo where idcliente = @idCliente
select * from PrejuizoFundoHistorico where idcliente = @idCliente
select * from ProventoBolsaCliente where idcliente = @idCliente
select * from SaldoCaixa where idcliente = @idCliente

delete from PosicaoBMF where idcliente = @idCliente
delete from PosicaoBMFAbertura where idcliente = @idCliente
delete from PosicaoBMFhistorico where idcliente = @idCliente
delete from PosicaoBolsa where idcliente = @idCliente
delete from PosicaoBolsaAbertura where idcliente = @idCliente
delete from PosicaoBolsaHistorico where idcliente = @idCliente
delete from Posicaocotista where idcarteira = @idCarteira
delete from PosicaocotistaAbertura where idcarteira = @idCarteira
delete from PosicaocotistaHistorico where idcarteira = @idCarteira
delete from Posicaoemprestimobolsa where idcliente = @idCliente
delete from PosicaoEmprestimoBolsaAbertura where idcliente = @idCliente
delete from PosicaoEmprestimoBolsaHistorico where idcliente = @idCliente
delete from Posicaofundo where idcliente = @idCliente
delete from PosicaofundoAbertura where idcliente = @idCliente
delete from PosicaofundoHistorico where idcliente = @idCliente
delete from Posicaorendafixa where idcliente = @idCliente
delete from PosicaorendafixaAbertura where idcliente = @idCliente
delete from PosicaorendafixaHistorico where idcliente = @idCliente
delete from OperacaoBMF where idcliente = @idCliente
delete from OperacaoBolsa where idcliente = @idCliente
delete from OperacaoCotista where idcarteira = @idCarteira
delete from OperacaoEmprestimoBolsa where idcliente = @idCliente
delete from OperacaoFundo where idcliente = @idCliente
delete from OperacaoRendaFixa where idcliente = @idCliente
delete from HistoricoCota where idcarteira = @idCarteira
delete from Liquidacao where idcliente = @idCliente
delete from LiquidacaoAbertura where idcliente = @idCliente
delete from Liquidacao where idcliente = @idCliente
delete from LiquidacaoAbertura where idcliente = @idCliente
delete from LiquidacaoHistorico where idcliente = @idCliente
delete from LiquidacaoFuturo where idcliente = @idCliente
delete from OrdemBMF where idcliente = @idCliente
delete from OrdemBolsa where idcliente = @idCliente
delete from PerfilCorretagemBMF where idcliente = @idCliente
delete from PerfilCorretagemBolsa where idcliente = @idCliente
delete from PrejuizoFundo where idcliente = @idCliente
delete from PrejuizoFundoHistorico where idcliente = @idCliente
delete from ProventoBolsaCliente where idcliente = @idCliente
delete from SaldoCaixa where idcliente = @idCliente