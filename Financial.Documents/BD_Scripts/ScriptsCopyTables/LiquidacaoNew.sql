/*************************    LiquidacaoAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 52374
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoAbertura L, ' + @Banco_Origem + '.dbo.LiquidacaoAbertura L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente                         

/********************************************************************************************************************/


/*************************    LiquidacaoHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 7000
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoHistorico L, ' + @Banco_Origem + '.dbo.LiquidacaoHistorico L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente                         

/********************************************************************************************************************/

/*************************      LiquidacaoFuturo	*****************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 7000
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idliquidacao),0) from ' + @Banco_Destino + '.dbo.LiquidacaoFuturo' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
		     from ' + @Banco_Origem + '.dbo.LiquidacaoFuturo L, ' + @Banco_Origem + '.dbo.LiquidacaoFuturo L1
			 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.LiquidacaoFuturo SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente                         

/********************************************************************************************************************/