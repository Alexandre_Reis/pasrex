/*************************      HistoricoCota	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCarteira int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 7000
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.HistoricoCota ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.HistoricoCota where idCarteira = @idCarteira'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCarteira int',
          @idCarteira = @idCarteira
/********************************************************************************************************************/