/*************************    PosicaoEmprestimoBolsa	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE	@tabela nvarchar(200), @tabela1 nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 438566
-- *****************************************************
SET @tabela = @Banco_Origem + '.dbo.PosicaoEmprestimoBolsa'
SET @tabela1 = @Banco_Destino + '.dbo.PosicaoEmprestimoBolsa'

SET @sql = ' SELECT * INTO #TempTable FROM ' + @tabela + ' where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' INSERT INTO ' + @tabela1 + ' SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
--print @sql
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/

/*************************    PosicaoEmprestimoBolsaAbertura	*****************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 52374
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaAbertura where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/

/*************************    PosicaoEmprestimoBolsaHistorico	*****************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 91434
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoEmprestimoBolsaHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoEmprestimoBolsaHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/