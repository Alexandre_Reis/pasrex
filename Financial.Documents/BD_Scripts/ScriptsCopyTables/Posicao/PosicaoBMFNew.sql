/*************************      PosicaoBMFAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBMFAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBMFAbertura P, ' + @Banco_Origem + '.dbo.PosicaoBMFAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente
          
/********************************************************************************************************************/

/*************************      PosicaoBMFHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBMFHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBMFHistorico P, ' + @Banco_Origem + '.dbo.PosicaoBMFHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/