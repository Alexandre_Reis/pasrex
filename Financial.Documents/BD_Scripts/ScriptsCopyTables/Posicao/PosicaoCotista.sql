/*************************      PosicaoCotista	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 7000
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoCotista where IdCarteira = @idCarteira ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotista SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCarteira int', @idCarteira = @idCarteira
/********************************************************************************************************************/

/*************************      PosicaoCotistaAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 37464
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoCotistaAbertura where idCarteira = @idCarteira'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCarteira int',
          @idCarteira = @idCarteira
/********************************************************************************************************************/

/*************************      PosicaoCotistaHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 37464
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoCotistaHistorico where idCarteira = @idCarteira'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCarteira int',
          @idCarteira = @idCarteira
/********************************************************************************************************************/