/*************************      PosicaoBolsaAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBolsaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBolsaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoBolsaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente


/********************************************************************************************************************/

/*************************      PosicaoBolsaHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoBolsaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoBolsaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoBolsaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idCliente' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCliente int',
          @idCliente = @idCliente

/********************************************************************************************************************/