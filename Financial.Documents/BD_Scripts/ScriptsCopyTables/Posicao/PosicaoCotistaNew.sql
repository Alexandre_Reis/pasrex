
/*************************      PosicaoCotistaAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 37464
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoCotistaAbertura' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoCotistaAbertura P, ' + @Banco_Origem + '.dbo.PosicaoCotistaAbertura P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idCarteira' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCarteira int',
          @idCarteira = @idCarteira

/********************************************************************************************************************/

/*************************      PosicaoCotistaHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql Nvarchar(1000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 37464
-- *****************************************************

SET @sql = ' DECLARE @i int ' + CHAR(13) +
		   ' SELECT @i = isnull( max(idposicao),0) from ' + @Banco_Destino + '.dbo.PosicaoCotistaHistorico' + CHAR(13) +
		   		   		   
		   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
		   
		   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
		     from ' + @Banco_Origem + '.dbo.PosicaoCotistaHistorico P, ' + @Banco_Origem + '.dbo.PosicaoCotistaHistorico P1
			 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idCarteira' +
		   ') as T' + CHAR(13) +
		   
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
		   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoCotistaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
		   ' DROP TABLE #TempTable '
		  		   
EXECUTE sp_executesql
		  @sql,
          N'@idCarteira int',
          @idCarteira = @idCarteira
/********************************************************************************************************************/