/*************************      PosicaoFundo	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 224340
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.PosicaoFundo where IdCarteira = @idCarteira ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundo SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCarteira int', @idCarteira = @idCarteira
/********************************************************************************************************************/

/*************************      PosicaoFundoAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 49195
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundoAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoFundoAbertura where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente

/********************************************************************************************************************/

/*************************      PosicaoFundoHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 49187
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoFundoHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoFundoHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/