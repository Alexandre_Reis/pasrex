/******************************************************************************************************************
Funcional mas se mudar 1 coluna precisa atualizar
DECLARE	@idCliente int
SET @idCliente = 228430 -- Entrada

DELETE from fin_teste_irmao.dbo.posicaobolsa where idcliente = @idCliente
INSERT INTO fin_teste_irmao.dbo.posicaobolsa
SELECT [IdAgente]
      ,[IdCliente]
      ,[CdAtivoBolsa]
      ,[TipoMercado]
      ,[DataVencimento]
      ,[PUMercado]
      ,[PUCusto]
      ,[PUCustoLiquido]
      ,[ValorMercado]
      ,[ValorCustoLiquido]
      ,[Quantidade]
      ,[QuantidadeInicial]
      ,[QuantidadeBloqueada]
      ,[ResultadoRealizar] FROM fin_teste.dbo.posicaobolsa where idcliente = @idCliente
/********************************************************************************************************************/

/*************************      PosicaoBolsa	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE	@tabela nvarchar(200), @tabela1 nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 228430
-- *****************************************************
SET @tabela = @Banco_Origem + '.dbo.PosicaoBolsa'
SET @tabela1 = @Banco_Destino + '.dbo.PosicaoBolsa'

SET @sql = ' SELECT * INTO #TempTable FROM ' + @tabela + ' where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' INSERT INTO ' + @tabela1 + ' SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
--print @sql
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/

/*************************      PosicaoBolsaAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 449800
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoBolsaAbertura where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/

/*************************      PosicaoBolsaHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 449800
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBolsaHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoBolsaHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente

/********************************************************************************************************************/