/*************************      PosicaoBMF	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE	@tabela nvarchar(200), @tabela1 nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 423364
-- *****************************************************
SET @tabela = @Banco_Origem + '.dbo.PosicaoBMF'
SET @tabela1 = @Banco_Destino + '.dbo.PosicaoBMF'

SET @sql = ' SELECT * INTO #TempTable FROM ' + @tabela + ' where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
		   ' INSERT INTO ' + @tabela1 + ' SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
--print @sql
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/


/*************************      PosicaoBMFAbertura	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFAbertura ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoBMFAbertura where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
          
/********************************************************************************************************************/

/*************************      PosicaoBMFHistorico	******************************************************************/

DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PosicaoBMFHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PosicaoBMFHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente

/********************************************************************************************************************/