/*************************      PrejuizoFundoHistorico	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sqlCommand Nvarchar(1000)
DECLARE @idCliente int
-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 7000
-- *****************************************************

SET @sqlCommand = N'INSERT INTO ' + @Banco_Destino + '.dbo.PrejuizoFundoHistorico ' +
				   'SELECT * FROM ' + @Banco_Origem + '.dbo.PrejuizoFundoHistorico where idCliente = @idCliente'

EXECUTE sp_executesql
		  @sqlCommand,
          N'@idCliente int',
          @idCliente = @idCliente
/********************************************************************************************************************/