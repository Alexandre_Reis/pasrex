/*************************    OperacaoBMF	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 208528
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoBMF where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoBMF SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/