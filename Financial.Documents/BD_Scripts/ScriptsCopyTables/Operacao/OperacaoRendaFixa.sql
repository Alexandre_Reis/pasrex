/*************************    OperacaoRendaFixa	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCliente int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCliente = 218140
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoRendaFixa where IdCliente = @idCliente ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoRendaFixa SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCliente int', @idCliente = @idCliente
/********************************************************************************************************************/