/*************************    OperacaoCotista	******************************************************************/
DECLARE	@Banco_Origem nvarchar(200), @Banco_Destino nvarchar(200)
DECLARE @sql nvarchar(2000)
DECLARE @idCarteira int

-- Entradas:
-- *****************************************************
SET @Banco_Origem = 'fin_teste'	 
SET @Banco_Destino = 'fin_teste_irmao' 
SET @idCarteira = 291395
-- *****************************************************

SET @sql = ' SELECT * INTO #TempTable FROM ' + @Banco_Origem + '.dbo.OperacaoCotista where IdCarteira = @idCarteira ' +
		   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
		   ' INSERT INTO ' + @Banco_Destino + '.dbo.OperacaoCotista SELECT * FROM #TempTable ' +
		   ' DROP TABLE #TempTable '
		   
EXECUTE sp_executesql @sql, N'@idCarteira int', @idCarteira = @idCarteira
/********************************************************************************************************************/