﻿create table AtivoBolsaHistorico
(DataReferencia datetime not null,
CdAtivoBolsa varchar(20) not null,
DataVencimento datetime null,
	CONSTRAINT [AtivoBolsaHistorico_PK] PRIMARY KEY NONCLUSTERED
	(
		[DataReferencia] ASC,
		[CdAtivoBolsa] ASC
	)
)

ALTER TABLE [AtivoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_AtivoBolsaHistorico_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [AtivoBolsa] ([CdAtivoBolsa])
ON DELETE CASCADE
GO

--Inclusao do Acesso ao menu de Historico de Opções 
insert into permissaomenu(IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) 
select idgrupo,	2230, 'N', 'N', 'N', 'N' from GrupoUsuario