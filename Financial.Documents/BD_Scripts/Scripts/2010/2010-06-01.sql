ALTER TABLE Cliente add CalculaContabil char(1) NULL
go
ALTER TABLE Cliente add IdPlano int NULL
go
ALTER TABLE Cliente WITH CHECK ADD  CONSTRAINT ContabPlano_Cliente_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
GO