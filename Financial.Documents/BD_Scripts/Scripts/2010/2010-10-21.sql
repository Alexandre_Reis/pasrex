alter table PosicaoRendaFixa add PUCorrecao decimal (25, 12) null
go
alter table PosicaoRendaFixa add ValorCorrecao decimal (16, 2) null
go
alter table PosicaoRendaFixa add TaxaOperacao decimal (25, 16) null
go

alter table PosicaoRendaFixaHistorico add PUCorrecao decimal (25, 12) null
go
alter table PosicaoRendaFixaHistorico add ValorCorrecao decimal (16, 2) null
go
alter table PosicaoRendaFixaHistorico add TaxaOperacao decimal (25, 16) null
go

alter table PosicaoRendaFixaAbertura add PUCorrecao decimal (25, 12) null
go
alter table PosicaoRendaFixaAbertura add ValorCorrecao decimal (16, 2) null
go
alter table PosicaoRendaFixaAbertura add TaxaOperacao decimal (25, 16) null
go

update PosicaoRendaFixa set PUCorrecao = 0
go
update PosicaoRendaFixa set ValorCorrecao = 0
go
update PosicaoRendaFixa set TaxaOperacao = 0
go

update PosicaoRendaFixaHistorico set PUCorrecao = 0
go
update PosicaoRendaFixaHistorico set ValorCorrecao = 0
go
update PosicaoRendaFixaHistorico set TaxaOperacao = 0
go

update PosicaoRendaFixaAbertura set PUCorrecao = 0
go
update PosicaoRendaFixaAbertura set ValorCorrecao = 0
go
update PosicaoRendaFixaAbertura set TaxaOperacao = 0
go

alter table PosicaoRendaFixa alter column PUCorrecao decimal (25, 12) not null
go
alter table PosicaoRendaFixa alter column ValorCorrecao decimal (16, 2) not null
go

alter table PosicaoRendaFixaHistorico alter column PUCorrecao decimal (25, 12) not null
go
alter table PosicaoRendaFixaHistorico alter column ValorCorrecao decimal (16, 2) not null
go

alter table PosicaoRendaFixaAbertura alter column PUCorrecao decimal (25, 12) not null
go
alter table PosicaoRendaFixaAbertura alter column ValorCorrecao decimal (16, 2) not null
go




