update historicolog set Origem = 9999999
go

alter table historicolog alter column Origem int not null
go

update titulorendafixa set IsentoIR = 1
go

update titulorendafixa set IsentoIOF = 1
go

alter table titulorendafixa alter column IsentoIR tinyint not null
go
alter table titulorendafixa alter column IsentoIOF tinyint not null
go

alter table ordembolsa alter column pu decimal(25, 16) not null
go

alter table operacaobolsa alter column pu decimal(25, 16) not null
go