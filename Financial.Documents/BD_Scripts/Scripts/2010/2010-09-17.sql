--Inclusao do acesso ao menu de Processamento/Configuracao
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14500, 'N', 'N', 'N', 'N' from GrupoUsuario
go
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14520, 'N', 'N', 'N', 'N' from GrupoUsuario
go
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14540, 'N', 'N', 'N', 'N' from GrupoUsuario
go
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14560, 'N', 'N', 'N', 'N' from GrupoUsuario
go

--Inclusao do acesso ao menu de Exportacao
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14120, 'N', 'N', 'N', 'N' from GrupoUsuario
go

alter table ativobmf add IdMoeda int null
go

update ativobmf set IdMoeda = 1 --Real
go

alter table ativobmf alter column IdMoeda int not null
go

alter table ordembmf add IdMoeda int null
go

update ordembmf set IdMoeda = 1 --Real
go

alter table ordembmf alter column IdMoeda int not null
go

alter table operacaobmf add IdMoeda int null
go

update operacaobmf set IdMoeda = 1 --Real
go

alter table operacaobmf alter column IdMoeda int not null
go

alter table titulorendafixa add IdMoeda int null
go

update titulorendafixa set IdMoeda = 1 --Real
go

alter table titulorendafixa alter column IdMoeda int not null
go
