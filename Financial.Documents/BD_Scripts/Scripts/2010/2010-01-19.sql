alter table clientebmf add TipoPlataforma tinyint null
go
update clientebmf set TipoPlataforma = 1
go
alter table clientebmf alter column TipoPlataforma tinyint not null
go

alter table trader add AlgoTrader char(1) null
go
update trader set AlgoTrader = 'N'
go
alter table trader alter column AlgoTrader char(1) not null
go

alter table ordembmf add DescontoEmolumento decimal (8, 4) null
go
alter table operacaobmf add DescontoEmolumento decimal (8, 4) null
go
