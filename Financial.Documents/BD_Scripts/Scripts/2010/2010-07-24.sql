alter table posicaotermobolsa add RendimentoTotal decimal (16,2) null
go
alter table posicaotermobolsa add RendimentoApropriar decimal (16,2) null
go
alter table posicaotermobolsa add RendimentoApropriado decimal (16,2) null
go
alter table posicaotermobolsa add RendimentoDia decimal (16,2) null
go

update posicaotermobolsa set RendimentoTotal = 0
go
update posicaotermobolsa set RendimentoApropriar = 0
go
update posicaotermobolsa set RendimentoApropriado = 0
go
update posicaotermobolsa set RendimentoDia = 0
go

alter table posicaotermobolsa alter column RendimentoTotal decimal (16,2) not null
go
alter table posicaotermobolsa alter column RendimentoApropriar decimal (16,2) not null
go
alter table posicaotermobolsa alter column RendimentoApropriado decimal (16,2) not null
go
alter table posicaotermobolsa alter column RendimentoDia decimal (16,2) not null
go

alter table posicaotermobolsahistorico  add RendimentoTotal decimal (16,2) null
go
alter table posicaotermobolsahistorico  add RendimentoApropriar decimal (16,2) null
go
alter table posicaotermobolsahistorico  add RendimentoApropriado decimal (16,2) null
go
alter table posicaotermobolsahistorico  add RendimentoDia decimal (16,2) null
go

update posicaotermobolsahistorico  set RendimentoTotal = 0
go
update posicaotermobolsahistorico  set RendimentoApropriar = 0
go
update posicaotermobolsahistorico  set RendimentoApropriado = 0
go
update posicaotermobolsahistorico  set RendimentoDia = 0
go

alter table posicaotermobolsahistorico  alter column RendimentoTotal decimal (16,2) not null
go
alter table posicaotermobolsahistorico  alter column RendimentoApropriar decimal (16,2) not null
go
alter table posicaotermobolsahistorico  alter column RendimentoApropriado decimal (16,2) not null
go
alter table posicaotermobolsahistorico  alter column RendimentoDia decimal (16,2) not null
go

alter table posicaotermobolsaabertura  add RendimentoTotal decimal (16,2) null
go
alter table posicaotermobolsaabertura  add RendimentoApropriar decimal (16,2) null
go
alter table posicaotermobolsaabertura  add RendimentoApropriado decimal (16,2) null
go
alter table posicaotermobolsaabertura  add RendimentoDia decimal (16,2) null
go

update posicaotermobolsaabertura  set RendimentoTotal = 0
go
update posicaotermobolsaabertura  set RendimentoApropriar = 0
go
update posicaotermobolsaabertura  set RendimentoApropriado = 0
go
update posicaotermobolsaabertura  set RendimentoDia = 0
go

alter table posicaotermobolsaabertura  alter column RendimentoTotal decimal (16,2) not null
go
alter table posicaotermobolsaabertura  alter column RendimentoApropriar decimal (16,2) not null
go
alter table posicaotermobolsaabertura  alter column RendimentoApropriado decimal (16,2) not null
go
alter table posicaotermobolsaabertura  alter column RendimentoDia decimal (16,2) not null
go

ALTER TABLE PosicaoTermoBolsa ADD  CONSTRAINT DF_PosicaoTermoBolsa_RendimentoApropriado  DEFAULT ((0)) FOR RendimentoApropriado
go
ALTER TABLE PosicaoTermoBolsa ADD  CONSTRAINT DF_PosicaoTermoBolsa_RendimentoApropriar  DEFAULT ((0)) FOR RendimentoApropriar
go
ALTER TABLE PosicaoTermoBolsa ADD  CONSTRAINT DF_PosicaoTermoBolsa_RendimentoDia  DEFAULT ((0)) FOR RendimentoDia
go
ALTER TABLE PosicaoTermoBolsa ADD  CONSTRAINT DF_PosicaoTermoBolsa_RendimentoTotal  DEFAULT ((0)) FOR RendimentoTotal
go

ALTER TABLE PosicaoTermoBolsaHistorico ADD  CONSTRAINT DF_PosicaoTermoBolsaHistorico_RendimentoApropriado  DEFAULT ((0)) FOR RendimentoApropriado
go
ALTER TABLE PosicaoTermoBolsaHistorico ADD  CONSTRAINT DF_PosicaoTermoBolsaHistorico_RendimentoApropriar  DEFAULT ((0)) FOR RendimentoApropriar
go
ALTER TABLE PosicaoTermoBolsaHistorico ADD  CONSTRAINT DF_PosicaoTermoBolsaHistorico_RendimentoDia  DEFAULT ((0)) FOR RendimentoDia
go
ALTER TABLE PosicaoTermoBolsaHistorico ADD  CONSTRAINT DF_PosicaoTermoBolsaHistorico_RendimentoTotal  DEFAULT ((0)) FOR RendimentoTotal
go

ALTER TABLE PosicaoTermoBolsaAbertura ADD  CONSTRAINT DF_PosicaoTermoBolsaAbertura_RendimentoApropriado  DEFAULT ((0)) FOR RendimentoApropriado
go
ALTER TABLE PosicaoTermoBolsaAbertura ADD  CONSTRAINT DF_PosicaoTermoBolsaAbertura_RendimentoApropriar  DEFAULT ((0)) FOR RendimentoApropriar
go
ALTER TABLE PosicaoTermoBolsaAbertura ADD  CONSTRAINT DF_PosicaoTermoBolsaAbertura_RendimentoDia  DEFAULT ((0)) FOR RendimentoDia
go
ALTER TABLE PosicaoTermoBolsaAbertura ADD  CONSTRAINT DF_PosicaoTermoBolsaAbertura_RendimentoTotal  DEFAULT ((0)) FOR RendimentoTotal
go


alter table contacorrente add IdMoeda int null
go
update contacorrente set IdMoeda = 1 --Real
go
alter table contacorrente alter column IdMoeda int not null
go


