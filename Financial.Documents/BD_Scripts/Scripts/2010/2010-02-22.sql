CREATE TABLE [DetalheResgateFundo](
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL CONSTRAINT [DF_DetalheResgateCliente_ValorCPMF]  DEFAULT ((0)),
	[ValorPerformance] [decimal](16, 2) NOT NULL CONSTRAINT [DF_DetalheResgateCliente_ValorPerformance]  DEFAULT ((0)),
	[PrejuizoUsado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_DetalheResgateCliente_PrejuizoUsado]  DEFAULT ((0)),
	[RendimentoResgate] [decimal](16, 2) NOT NULL CONSTRAINT [DF_DetalheResgateCliente_RendimentoResgate]  DEFAULT ((0)),
	[VariacaoResgate] [decimal](16, 2) NOT NULL CONSTRAINT [DF_DetalheResgateCliente_VariacaoResgate]  DEFAULT ((0)),
 CONSTRAINT [PK_DetalheResgateFundo] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC,
	[IdPosicaoResgatada] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [DetalheResgateFundo]  WITH CHECK ADD  CONSTRAINT [Carteira_DetalheResgateFundo_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [DetalheResgateFundo] CHECK CONSTRAINT [Carteira_DetalheResgateFundo_FK1]
GO
ALTER TABLE [DetalheResgateFundo]  WITH CHECK ADD  CONSTRAINT [Cliente_DetalheResgateFundo_FK1] FOREIGN KEY([IdCliente])
REFERENCES [Cliente] ([IdCliente])
GO
ALTER TABLE [DetalheResgateFundo] CHECK CONSTRAINT [Cliente_DetalheResgateFundo_FK1]
GO



CREATE TABLE [dbo].[PrejuizoFundoHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Data] [datetime] NOT NULL,
	[ValorPrejuizo] [decimal](16, 2) NULL,
	[DataLimiteCompensacao] [datetime] NULL,
 CONSTRAINT [PK_PrejuizoFundoHistorico] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdCliente] ASC,
	[IdCarteira] ASC,
	[Data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PrejuizoFundoHistorico]  WITH CHECK ADD  CONSTRAINT [Carteira_PrejuizoFundoHistorico_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PrejuizoFundoHistorico] CHECK CONSTRAINT [Carteira_PrejuizoFundoHistorico_FK1]
GO
ALTER TABLE [dbo].[PrejuizoFundoHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_PrejuizoFundoHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PrejuizoFundoHistorico] CHECK CONSTRAINT [Cliente_PrejuizoFundoHistorico_FK1]
GO

ALTER TABLE PosicaoCotista DROP COLUMN CotaAnterior
GO

ALTER TABLE PosicaoCotistaHistorico DROP COLUMN CotaAnterior
GO

ALTER TABLE PosicaoCotistaAbertura DROP COLUMN CotaAnterior
GO

ALTER TABLE PosicaoFundo ADD ValorRendimento decimal(16, 2) null
go
update PosicaoFundo set ValorRendimento = 0
go
ALTER TABLE PosicaoFundo alter column ValorRendimento decimal(16, 2) not null
go

ALTER TABLE PosicaoFundoHistorico ADD ValorRendimento decimal(16, 2) null
go
update PosicaoFundoHistorico set ValorRendimento = 0
go
ALTER TABLE PosicaoFundoHistorico alter column ValorRendimento decimal(16, 2) not null
go

ALTER TABLE PosicaoFundoAbertura ADD ValorRendimento decimal(16, 2) null
go
update PosicaoFundoAbertura set ValorRendimento = 0
go
ALTER TABLE PosicaoFundoAbertura alter column ValorRendimento decimal(16, 2) not null
go

ALTER TABLE ProventoBolsa alter column Valor decimal(20,11) not null
go