alter table historicolog add Origem int null
go

update historicolog set Origem = 9999999
go

alter table historicolog alter column Origem int not null
go