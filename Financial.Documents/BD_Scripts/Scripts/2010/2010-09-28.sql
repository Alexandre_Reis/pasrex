﻿--Inclusao do acesso ao menu de Configuracao Seguranca
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	40050, 'N', 'N', 'N', 'N' from GrupoUsuario
go

insert into configuracao values (2000, 'Tamanho Minimo Senha', 6, null)
go
insert into configuracao values (2010, 'Tempo Expiração Senha', 45, null)
go
insert into configuracao values (2020, 'Tentativas Bloqueio Senha', 3, null)
go
insert into configuracao values (2030, 'Histórico Senhas', 6, null)
go
insert into configuracao values (2040, 'Mínimo Caracter Especial Senha', 0, null)
go
insert into configuracao values (2041, 'Mínimo Número Senha', 0, null)
go
insert into configuracao values (2042, 'Letras Maisculas', 0, null)
go
insert into configuracao values (2050, 'Janela de Tempo para Reset de Senha em Minutos', 10, null)
go

alter table indice add IdIndiceBase int null
go