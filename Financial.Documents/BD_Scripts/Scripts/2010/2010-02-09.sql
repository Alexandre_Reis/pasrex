CREATE TABLE [dbo].[TabelaCRCA](
	[IdTabela] [int] IDENTITY(1,1) NOT NULL,
	[DataMovimento] [datetime] NOT NULL,
	[Modalidade] [char](1) NOT NULL,
	[CpfCNPJ] [int] NOT NULL,
	[CodigoCustodiante] [int] NOT NULL,
	[CodigoCliente] [int] NOT NULL,
	[QuantidadeEfetiva] [int] NOT NULL,
	[ValorEfetivo] [decimal](16, 2) NOT NULL,
	[CodigoIsin] [varchar](12) NOT NULL,
 CONSTRAINT [PK_TabelaCRCA] PRIMARY KEY CLUSTERED 
(
	[IdTabela] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
go
