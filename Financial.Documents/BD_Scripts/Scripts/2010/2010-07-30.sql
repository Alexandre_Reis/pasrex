alter table ordembolsa add IdMoeda int null
go

update ordembolsa set IdMoeda = 1
go

alter table ordembolsa alter column IdMoeda int not null
go

alter table operacaobolsa add IdMoeda int null
go

update operacaobolsa set IdMoeda = 1
go

alter table operacaobolsa alter column IdMoeda int not null
go
