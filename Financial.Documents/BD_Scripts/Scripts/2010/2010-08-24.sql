--Inclusao do acesso ao menu de Transferencia Custodia BMF
insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	3170, 'N', 'N', 'N', 'N' from GrupoUsuario
go

alter table Indice add Taxa decimal(8,4) null
go

alter table Indice add Percentual decimal(6,2) null
go