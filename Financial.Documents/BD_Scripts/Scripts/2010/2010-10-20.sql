﻿--Inclusao do Acesso aos Menus
INSERT INTO Configuracao VALUES (3000, 'Acões/Opções', Null, 'S');
INSERT INTO Configuracao VALUES (3001, 'Termo Ações', Null, 'S');
INSERT INTO Configuracao VALUES (3002, 'BTC/Aluguel Ações', Null, 'S');
INSERT INTO Configuracao VALUES (3003, 'BMF', Null, 'S');
INSERT INTO Configuracao VALUES (3004, 'Renda Fixa', Null, 'S');
INSERT INTO Configuracao VALUES (3005, 'Fundos', Null, 'S');
INSERT INTO Configuracao VALUES (3006, 'Swap', Null, 'S');
INSERT INTO Configuracao VALUES (3007, 'Gerencial', Null, 'S');
INSERT INTO Configuracao VALUES (3008, 'Rebates', Null, 'S');
INSERT INTO Configuracao VALUES (3009, 'Cálculo Despesas', Null, 'S');
INSERT INTO Configuracao VALUES (3010, 'Enquadramento', Null, 'S');
INSERT INTO Configuracao VALUES (3011, 'Cotista', Null, 'S');
INSERT INTO Configuracao VALUES (3012, 'Contábil', Null, 'S');
INSERT INTO Configuracao VALUES (3013, 'Apuração IR', Null, 'S');
INSERT INTO Configuracao VALUES (3014, 'Informes Fundos', Null, 'S');
INSERT INTO Configuracao VALUES (3015, 'Informes Clubes', Null, 'S');