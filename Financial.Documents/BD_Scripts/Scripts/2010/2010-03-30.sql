alter table cotacaobolsa alter column PUFechamento decimal(25, 16)
go
alter table posicaobolsa alter column pumercado decimal(25, 16)
go
alter table posicaobolsa alter column pucusto decimal(25, 16)
go
alter table posicaobolsa alter column pucustoliquido decimal(25, 16)
go
alter table posicaobolsahistorico alter column pumercado decimal(25, 16)
go
alter table posicaobolsahistorico alter column pucusto decimal(25, 16)
go
alter table posicaobolsahistorico alter column pucustoliquido decimal(25, 16)
go
alter table posicaobolsaabertura alter column pumercado decimal(25, 16)
go
alter table posicaobolsaabertura alter column pucusto decimal(25, 16)
go
alter table posicaobolsaabertura alter column pucustoliquido decimal(25, 16)
go