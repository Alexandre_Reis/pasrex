alter table detalheresgatecotista add ValorIR decimal(16, 2) null
go
alter table detalheresgatecotista add ValorIOF decimal(16, 2) null
go

update detalheresgatecotista set ValorIR = 0
go
update detalheresgatecotista set ValorIOF = 0
go

alter table detalheresgatecotista alter column ValorIR decimal(16, 2) not null
go
alter table detalheresgatecotista alter column ValorIOF decimal(16, 2) not null
go

ALTER TABLE DetalheResgateCotista ADD CONSTRAINT DF_DetalheResgateCotista_ValorIR  
	DEFAULT ((0)) FOR ValorIR
go
ALTER TABLE DetalheResgateCotista ADD CONSTRAINT DF_DetalheResgateCotista_ValorIOF  
	DEFAULT ((0)) FOR ValorIOF
go