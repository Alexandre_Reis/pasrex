alter table titulorendafixa add IsentoIR tinyint null
go
alter table titulorendafixa add IsentoIOF tinyint null
go

update titulorendafixa set IsentoIR = 1
go

update titulorendafixa set IsentoIOF = 1
go

alter table titulorendafixa alter column IsentoIR tinyint not null
go
alter table titulorendafixa alter column IsentoIOF tinyint not null
go


create table ContabPlano
(IdPlano int IDENTITY (1, 1) not null,
Descricao varchar(100) not null)

create table ContabCentroCusto
(IdCentroCusto int IDENTITY (1, 1) not null,
Descricao varchar(100) not null)

create table ContabRoteiro
(IdEvento int IDENTITY (1, 1) not null,
ContaDebito varchar(20) not null,
ContaCredito varchar(20) not null,
Origem int not null,
Identificador varchar(20) null,
Descricao varchar(200) not null,
IdPlano int not null,
IdCentroCusto int null)

create table ContabLancamento
(IdLancamento int IDENTITY (1, 1) not null,
IdCliente int not null,
DataLancamento datetime not null,
Valor decimal(16,2) not null,
ContaDebito varchar(20) not null,
ContaCredito varchar(20) not null,
Origem int not null,
Identificador varchar(20) null,
Descricao varchar(200) not null,
IdPlano int not null,
IdCentroCusto int null)

ALTER TABLE ContabPlano ADD CONSTRAINT [ContabPlano_PK] PRIMARY KEY NONCLUSTERED 
(
	IdPlano ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
go

ALTER TABLE ContabCentroCusto ADD CONSTRAINT [ContabCentroCusto_PK] PRIMARY KEY NONCLUSTERED 
(
	IdCentroCusto ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
go

ALTER TABLE ContabRoteiro ADD CONSTRAINT [ContabRoteiro_PK] PRIMARY KEY NONCLUSTERED 
(
	IdEvento ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
go

ALTER TABLE ContabLancamento ADD CONSTRAINT [ContabLancamento_PK] PRIMARY KEY NONCLUSTERED 
(
	IdLancamento ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
go

ALTER TABLE ContabRoteiro WITH CHECK ADD  CONSTRAINT ContabPlano_ContabRoteiro_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
ON DELETE CASCADE
go

ALTER TABLE ContabRoteiro WITH CHECK ADD  CONSTRAINT ContabCentroCusto_ContabRoteiro_FK1 FOREIGN KEY(IdCentroCusto)
REFERENCES ContabCentroCusto (IdCentroCusto)
ON DELETE CASCADE
go

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT Cliente_ContabLancamento_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
go

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT ContabPlano_ContabLancamento_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
ON DELETE CASCADE
go

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT ContabCentroCusto_ContabLancamento_FK1 FOREIGN KEY(IdCentroCusto)
REFERENCES ContabCentroCusto (IdCentroCusto)
ON DELETE CASCADE
go


alter table TabelaProvisao add IdEventoProvisao int null
go
alter table TabelaProvisao add IdEventoPagamento int null
go

alter table TabelaTaxaAdministracao add IdEventoProvisao int null
go
alter table TabelaTaxaAdministracao add IdEventoPagamento int null
go

alter table TabelaTaxaPerformance add IdEventoProvisao int null
go
alter table TabelaTaxaPerformance add IdEventoPagamento int null
go

ALTER TABLE TabelaProvisao WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaProvisao_FK1 FOREIGN KEY(IdEventoProvisao)
REFERENCES ContabRoteiro (IdEvento)
go

ALTER TABLE TabelaProvisao WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaProvisao_FK2 FOREIGN KEY(IdEventoPagamento)
REFERENCES ContabRoteiro (IdEvento)
go


ALTER TABLE TabelaTaxaAdministracao WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaTaxaAdministracao_FK1 FOREIGN KEY(IdEventoProvisao)
REFERENCES ContabRoteiro (IdEvento)
go

ALTER TABLE TabelaTaxaAdministracao WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaTaxaAdministracao_FK2 FOREIGN KEY(IdEventoPagamento)
REFERENCES ContabRoteiro (IdEvento)
go

ALTER TABLE TabelaTaxaPerformance WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaTaxaPerformance_FK1 FOREIGN KEY(IdEventoProvisao)
REFERENCES ContabRoteiro (IdEvento)
go

ALTER TABLE TabelaTaxaPerformance WITH CHECK ADD  CONSTRAINT ContabRoteiro_TabelaTaxaPerformance_FK2 FOREIGN KEY(IdEventoPagamento)
REFERENCES ContabRoteiro (IdEvento)
go
