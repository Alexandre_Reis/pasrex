CREATE TABLE TabelaEscalonamentoAdm(
	IdTabela int NOT NULL,
	FaixaPL decimal(16, 2) NOT NULL,
	Percentual decimal(16, 2) NOT NULL,
 CONSTRAINT PK_TabelaEscalonamentoAdm PRIMARY KEY CLUSTERED 
(
	IdTabela ASC,
	FaixaPL ASC
)
)

GO
ALTER TABLE TabelaEscalonamentoAdm  WITH CHECK ADD  CONSTRAINT FK_TabelaTaxaAdministracao_TabelaEscalonamentoAdm_1 FOREIGN KEY(IdTabela)
REFERENCES TabelaTaxaAdministracao (IdTabela)
ON DELETE CASCADE
GO

CREATE TABLE TabelaEscalonamentoRF(
	IdTitulo int NOT NULL,
	Prazo int NOT NULL,
	Taxa decimal(16, 2) NOT NULL,
 CONSTRAINT PK_TabelaEscalonamentoRF PRIMARY KEY CLUSTERED 
(
	IdTitulo ASC,
	Prazo ASC
)
)

GO
ALTER TABLE TabelaEscalonamentoRF  WITH CHECK ADD  CONSTRAINT FK_TituloRendaFixa_TabelaEscalonamentoRF_1 FOREIGN KEY(IdTitulo)
REFERENCES TituloRendaFixa (IdTitulo)
GO
