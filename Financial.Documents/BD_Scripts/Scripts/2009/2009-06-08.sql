alter table posicaoswap add ValorIndicePartida decimal(20,10) null
go
alter table posicaoswap add ValorIndicePartidaContraParte decimal(20,10) null
go
alter table posicaoswapabertura add ValorIndicePartida decimal(20,10) null
go
alter table posicaoswapabertura add ValorIndicePartidaContraParte decimal(20,10) null
go
alter table posicaoswaphistorico add ValorIndicePartida decimal(20,10) null
go
alter table posicaoswaphistorico add ValorIndicePartidaContraParte decimal(20,10) null
go
alter table operacaoswap add ValorIndicePartida decimal(20,10) null
go
alter table operacaoswap add ValorIndicePartidaContraParte decimal(20,10) null
go

