alter table operacaobolsa add IdTrader int null
go
alter table operacaobmf add IdTrader int null
go

alter table posicaocotista add CotaAnterior decimal(28,12) null
go
alter table posicaocotistaabertura add CotaAnterior decimal(28,12) null
go
alter table posicaocotistahistorico add CotaAnterior decimal(28,12) null
go

update posicaocotista set CotaAnterior = CotaAplicacao
go
update posicaocotistaabertura set CotaAnterior = CotaAplicacao
go
update posicaocotistahistorico set CotaAnterior = CotaAplicacao
go

alter table posicaocotista alter column CotaAnterior decimal(28,12) not null
go
alter table posicaocotistaabertura alter column CotaAnterior decimal(28,12) not null
go
alter table posicaocotistahistorico alter column CotaAnterior decimal(28,12) not null
go

alter table posicaocotista add ValorRendimento decimal(16,2) null
go
alter table posicaocotistaabertura add ValorRendimento decimal(16,2) null
go
alter table posicaocotistahistorico add ValorRendimento decimal(16,2) null
go

update posicaocotista set ValorRendimento = 0
go
update posicaocotistaabertura set ValorRendimento = 0
go
update posicaocotistahistorico set ValorRendimento = 0
go

alter table posicaocotista alter column ValorRendimento decimal(16,2) not null
go
alter table posicaocotistaabertura alter column ValorRendimento decimal(16,2) not null
go
alter table posicaocotistahistorico alter column ValorRendimento decimal(16,2) not null
go


create table "CalculoGerencial" ( 
	"IdCliente" int not null,
	"IdTrader" int not null,
	"CdAtivo" varchar(20) not null,
	"DataCalculo" datetime not null,
	"TipoMercado" tinyint not null,
	"DataVencimento" datetime null,
	"Quantidade" decimal(20,0) not null,
	"PUCusto" decimal(20,10) not null,
	"PUMercado" decimal(20,2) not null,
	"ValorMercado" decimal(20,2) not null,
	"ResultadoRealizar" decimal(20,2) not null,
	"ResultadoDayTrade" decimal(20,2) not null,
	"ResultadoNormal" decimal(20,2) not null,
	"ResultadoAcumulado" decimal(20,2) not null) ON 'PRIMARY'  
go

alter table "CalculoGerencial"
	add constraint "PK_CalculoGerencial" primary key clustered ("IdCliente", "IdTrader", "CdAtivo", "DataCalculo")   
go

/* Add foreign key constraints to table "CalculoGerencial".                                   */
alter table "CalculoGerencial"
	add constraint "Cliente_CalculoGerencial_FK1" foreign key (
		"IdCliente")
	 references "Cliente" (
		"IdCliente") on update no action on delete no action 
go

alter table "CalculoGerencial"
	add constraint "Trader_CalculoGerencial_FK1" foreign key (
		"IdTrader")
	 references "Trader" (
		"IdTrader") on update no action on delete no action  
go


USE [TESTE_ALL]
GO
ALTER TABLE [dbo].[GerOperacaoBMF] DROP CONSTRAINT [AtivoBMF_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBMF] DROP CONSTRAINT [Cliente_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBMF] DROP CONSTRAINT [Trader_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] DROP CONSTRAINT [Cliente_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] DROP CONSTRAINT [Trader_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF] DROP CONSTRAINT [AtivoBMF_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF] DROP CONSTRAINT [Cliente_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF] DROP CONSTRAINT [Trader_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] DROP CONSTRAINT [AtivoBMF_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] DROP CONSTRAINT [Cliente_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] DROP CONSTRAINT [Trader_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] DROP CONSTRAINT [AtivoBMF_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] DROP CONSTRAINT [Cliente_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] DROP CONSTRAINT [Trader_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] DROP CONSTRAINT [Cliente_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] DROP CONSTRAINT [Trader_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] DROP CONSTRAINT [Cliente_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] DROP CONSTRAINT [Trader_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] DROP CONSTRAINT [Cliente_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] DROP CONSTRAINT [Trader_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] DROP CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] DROP CONSTRAINT [Cliente_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] DROP CONSTRAINT [Trader_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] DROP CONSTRAINT [Cliente_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] DROP CONSTRAINT [Trader_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] DROP CONSTRAINT [Cliente_GerPosicaoTermoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] DROP CONSTRAINT [Trader_GerPosicaoTermoBolsaHistorico_FK1]
GO
DROP TABLE [dbo].[GerPosicaoTermoBolsaAbertura]
GO
DROP TABLE [dbo].[GerPosicaoTermoBolsaHistorico]
GO
DROP TABLE [dbo].[GerPosicaoBolsaAbertura]
GO
DROP TABLE [dbo].[GerPosicaoBolsa]
GO
DROP TABLE [dbo].[GerPosicaoBolsaHistorico]
GO
DROP TABLE [dbo].[GerOperacaoBMF]
GO
DROP TABLE [dbo].[GerOperacaoBolsa]
GO
DROP TABLE [dbo].[GerPosicaoBMF]
GO
DROP TABLE [dbo].[GerPosicaoTermoBolsa]
GO
DROP TABLE [dbo].[GerPosicaoBMFAbertura]
GO
DROP TABLE [dbo].[GerPosicaoBMFHistorico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoTermoBolsaAbertura](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[DataVencimento] [datetime] NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorTermoLiquido] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[PUTermoLiquido] [decimal](16, 10) NOT NULL,
	[PUCustoLiquidoAcao] [decimal](16, 10) NOT NULL,
	[PUMercado] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoTermoBolsaAbertura_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoTermoBolsaHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[DataVencimento] [datetime] NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorTermoLiquido] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[PUTermoLiquido] [decimal](16, 10) NOT NULL,
	[PUCustoLiquidoAcao] [decimal](16, 10) NOT NULL,
	[PUMercado] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoTermoBolsaHistorico_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoBolsaAbertura](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[TipoMercado] [char](3) NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 2) NOT NULL,
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[ResultadoRealizar] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoBolsaAbertura_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoBolsa](
	[IdPosicao] [int] IDENTITY(1,1) NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[TipoMercado] [char](3) NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoBolsa_PUMercado]  DEFAULT ((0)),
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoBolsa_ValorMercado]  DEFAULT ((0)),
	[ResultadoRealizar] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoBolsa_ResultadoRealizar]  DEFAULT ((0)),
 CONSTRAINT [GerPosicaoBolsa_PK] PRIMARY KEY CLUSTERED 
(
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerOperacaoBMF](
	[IdOperacao] [int] IDENTITY(1,1) NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBMF] [varchar](20) NOT NULL,
	[Serie] [varchar](5) NOT NULL,
	[TipoMercado] [tinyint] NOT NULL,
	[TipoOperacao] [varchar](2) NOT NULL,
	[Data] [datetime] NOT NULL,
	[Quantidade] [int] NOT NULL,
	[PU] [decimal](16, 4) NOT NULL,
	[PULiquido] [decimal](16, 10) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_PULiquido]  DEFAULT ((0)),
	[Valor] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_Valor]  DEFAULT ((0)),
	[ValorLiquido] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_ValorLiquido]  DEFAULT ((0)),
	[Despesas] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_Despesas]  DEFAULT ((0)),
	[ResultadoRealizado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_ResultadoRealizado]  DEFAULT ((0)),
	[Origem] [tinyint] NOT NULL,
	[Ajuste] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBMF_Ajuste]  DEFAULT ((0)),
	[Taxa] [decimal](8, 4) NULL,
	[Fonte] [tinyint] NOT NULL,
 CONSTRAINT [GerOperacaoBMF_PK] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoBolsaHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[TipoMercado] [char](3) NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 2) NOT NULL,
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[ResultadoRealizar] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoBolsaHistorico_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerOperacaoBolsa](
	[IdOperacao] [int] IDENTITY(1,1) NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[TipoMercado] [char](3) NOT NULL,
	[TipoOperacao] [varchar](2) NOT NULL,
	[Data] [datetime] NOT NULL,
	[Quantidade] [decimal](16, 2) NULL,
	[PU] [decimal](14, 4) NOT NULL,
	[PULiquido] [decimal](16, 10) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_PULiquido]  DEFAULT ((0)),
	[Valor] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_Valor]  DEFAULT ((0)),
	[ValorLiquido] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_ValorLiquido]  DEFAULT ((0)),
	[Despesas] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_Despesas]  DEFAULT ((0)),
	[ResultadoRealizado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_ResultadoRealizado]  DEFAULT ((0)),
	[Origem] [tinyint] NOT NULL,
	[QuantidadeCasadaTermo] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_QuantidadeCasadaTermo]  DEFAULT ((0)),
	[QuantidadeCasadaExercicio] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_QuantidadeCasadaExercicio]  DEFAULT ((0)),
	[ResultadoTermo] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_ResultadoTermo]  DEFAULT ((0)),
	[ResultadoExercicio] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerOperacaoBolsa_ResultadoExercicio]  DEFAULT ((0)),
	[Fonte] [tinyint] NOT NULL,
	[CdAtivoBolsaOpcao] [varchar](20) NULL,
 CONSTRAINT [GerOperacaoBolsa_PK] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoBMF](
	[IdPosicao] [int] IDENTITY(1,1) NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBMF] [varchar](20) NOT NULL,
	[Serie] [varchar](5) NOT NULL,
	[TipoMercado] [tinyint] NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 4) NOT NULL CONSTRAINT [DF_GerPosicaoBMF_PUMercado]  DEFAULT ((0)),
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [int] NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoBMF_ValorMercado]  DEFAULT ((0)),
	[ResultadoRealizar] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoBMF_ResultadoRealizar]  DEFAULT ((0)),
 CONSTRAINT [GerPosicaoBMF_PK] PRIMARY KEY CLUSTERED 
(
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoTermoBolsa](
	[IdPosicao] [int] IDENTITY(1,1) NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[DataVencimento] [datetime] NOT NULL,
	[Quantidade] [decimal](16, 2) NOT NULL,
	[ValorTermoLiquido] [decimal](16, 2) NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoTermoBolsa_ValorMercado]  DEFAULT ((0)),
	[PUTermoLiquido] [decimal](16, 10) NOT NULL,
	[PUCustoLiquidoAcao] [decimal](16, 10) NOT NULL,
	[PUMercado] [decimal](16, 2) NOT NULL CONSTRAINT [DF_GerPosicaoTermoBolsa_PUMercado]  DEFAULT ((0)),
 CONSTRAINT [GerPosicaoTermoBolsa_PK] PRIMARY KEY CLUSTERED 
(
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GerPosicaoBMFAbertura](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBMF] [varchar](20) NOT NULL,
	[Serie] [varchar](5) NOT NULL,
	[TipoMercado] [tinyint] NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 4) NOT NULL,
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [int] NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[ResultadoRealizar] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoBMFAbertura_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GerPosicaoBMFHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[IdTrader] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[CdAtivoBMF] [varchar](20) NOT NULL,
	[Serie] [varchar](5) NOT NULL,
	[TipoMercado] [tinyint] NOT NULL,
	[DataVencimento] [datetime] NULL,
	[PUMercado] [decimal](16, 4) NOT NULL,
	[PUCusto] [decimal](16, 10) NOT NULL,
	[Quantidade] [int] NOT NULL,
	[ValorMercado] [decimal](16, 2) NOT NULL,
	[ResultadoRealizar] [decimal](16, 2) NOT NULL,
 CONSTRAINT [GerPosicaoBMFHistorico_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdPosicao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GerOperacaoBMF]  WITH CHECK ADD  CONSTRAINT [AtivoBMF_GerOperacaoBMF_FK1] FOREIGN KEY([CdAtivoBMF], [Serie])
REFERENCES [dbo].[AtivoBMF] ([CdAtivoBMF], [Serie])
GO
ALTER TABLE [dbo].[GerOperacaoBMF] CHECK CONSTRAINT [AtivoBMF_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBMF]  WITH CHECK ADD  CONSTRAINT [Cliente_GerOperacaoBMF_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerOperacaoBMF] CHECK CONSTRAINT [Cliente_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBMF]  WITH CHECK ADD  CONSTRAINT [Trader_GerOperacaoBMF_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerOperacaoBMF] CHECK CONSTRAINT [Trader_GerOperacaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] CHECK CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_GerOperacaoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] CHECK CONSTRAINT [Cliente_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerOperacaoBolsa]  WITH CHECK ADD  CONSTRAINT [Trader_GerOperacaoBolsa_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] CHECK CONSTRAINT [Trader_GerOperacaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF]  WITH CHECK ADD  CONSTRAINT [AtivoBMF_GerPosicaoBMF_FK1] FOREIGN KEY([CdAtivoBMF], [Serie])
REFERENCES [dbo].[AtivoBMF] ([CdAtivoBMF], [Serie])
GO
ALTER TABLE [dbo].[GerPosicaoBMF] CHECK CONSTRAINT [AtivoBMF_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMF_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBMF] CHECK CONSTRAINT [Cliente_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMF]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBMF_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBMF] CHECK CONSTRAINT [Trader_GerPosicaoBMF_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura]  WITH CHECK ADD  CONSTRAINT [AtivoBMF_GerPosicaoBMFAbertura_FK1] FOREIGN KEY([CdAtivoBMF], [Serie])
REFERENCES [dbo].[AtivoBMF] ([CdAtivoBMF], [Serie])
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] CHECK CONSTRAINT [AtivoBMF_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMFAbertura_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] CHECK CONSTRAINT [Cliente_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBMFAbertura_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] CHECK CONSTRAINT [Trader_GerPosicaoBMFAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico]  WITH CHECK ADD  CONSTRAINT [AtivoBMF_GerPosicaoBMFHistorico_FK1] FOREIGN KEY([CdAtivoBMF], [Serie])
REFERENCES [dbo].[AtivoBMF] ([CdAtivoBMF], [Serie])
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] CHECK CONSTRAINT [AtivoBMF_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMFHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] CHECK CONSTRAINT [Cliente_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBMFHistorico_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] CHECK CONSTRAINT [Trader_GerPosicaoBMFHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] CHECK CONSTRAINT [Cliente_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsa]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBolsa_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] CHECK CONSTRAINT [Trader_GerPosicaoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] CHECK CONSTRAINT [Cliente_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] CHECK CONSTRAINT [Trader_GerPosicaoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] CHECK CONSTRAINT [Cliente_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] CHECK CONSTRAINT [Trader_GerPosicaoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsa_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoTermoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] CHECK CONSTRAINT [Cliente_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoTermoBolsa_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsa] CHECK CONSTRAINT [Trader_GerPosicaoTermoBolsa_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaAbertura_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoTermoBolsaAbertura_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] CHECK CONSTRAINT [Cliente_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoTermoBolsaAbertura_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaAbertura] CHECK CONSTRAINT [Trader_GerPosicaoTermoBolsaAbertura_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaHistorico_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] CHECK CONSTRAINT [AtivoBolsa_GerPosicaoTermoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoTermoBolsaHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] CHECK CONSTRAINT [Cliente_GerPosicaoTermoBolsaHistorico_FK1]
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [Trader_GerPosicaoTermoBolsaHistorico_FK1] FOREIGN KEY([IdTrader])
REFERENCES [dbo].[Trader] ([IdTrader])
GO
ALTER TABLE [dbo].[GerPosicaoTermoBolsaHistorico] CHECK CONSTRAINT [Trader_GerPosicaoTermoBolsaHistorico_FK1]
GO



