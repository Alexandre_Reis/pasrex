/****** Object:  Index [DataHistorico_IdCarteira]    Script Date: 11/04/2009 20:17:18 ******/
CREATE NONCLUSTERED INDEX [DataHistorico_IdCarteira] ON [dbo].[PosicaoCotistaAbertura] 
(
	[DataHistorico] ASC,
	[IdCarteira] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]