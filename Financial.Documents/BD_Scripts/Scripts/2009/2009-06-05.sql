alter table cliente add StatusRealTime tinyint null
go
update cliente set StatusRealTime = 1
go
alter table cliente alter column StatusRealTime tinyint not null
go