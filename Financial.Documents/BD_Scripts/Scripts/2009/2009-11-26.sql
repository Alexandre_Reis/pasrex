alter table CalculoGerencial add ResultadoDia decimal (20, 2) null
go
update CalculoGerencial set ResultadoDia = 0
go
alter table CalculoGerencial alter column ResultadoDia decimal (20, 2) not null
go

alter table CalculoGerencial add ValorizacaoDia decimal (20, 2) null
go
update CalculoGerencial set ValorizacaoDia = 0
go
alter table CalculoGerencial alter column ValorizacaoDia decimal (20, 2) not null
go
