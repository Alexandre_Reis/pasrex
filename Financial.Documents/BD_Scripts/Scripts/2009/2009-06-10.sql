alter table cliente add CalculaRealTime char(1) null
go
update cliente set CalculaRealTime = 'N'
go
alter table cliente alter column CalculaRealTime char(1) not null
go