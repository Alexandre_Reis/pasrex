alter table book add Tipo tinyint null
go
update book set Tipo = 1
go
alter table book alter column Tipo tinyint not null
go
alter table book add Email varchar(1000) null
go
alter table book add Assunto varchar(1000) null
go
alter table book add Mensagem varchar(2000) null
go

alter table bookcliente add Imprime char(1) null
go
update bookcliente set Imprime = 'S'
go
alter table bookcliente alter column Imprime char(1) not null
go




