alter table cliente add CalculaGerencial char(1) null
go
update cliente set calculagerencial = 'N'
go
alter table cliente alter column CalculaGerencial char(1) not null
go