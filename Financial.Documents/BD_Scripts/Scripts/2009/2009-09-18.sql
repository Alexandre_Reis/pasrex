drop table gerposicaotermobolsa
go
drop table gerposicaotermobolsahistorico
go
drop table gerposicaotermobolsaabertura
go

alter table posicaotermobolsa add IdTrader int null
go
alter table posicaotermobolsahistorico add IdTrader int null
go
alter table posicaotermobolsaabertura add IdTrader int null
go

alter table posicaoemprestimobolsa add IdTrader int null
go
alter table posicaoemprestimobolsahistorico add IdTrader int null
go
alter table posicaoemprestimobolsaabertura add IdTrader int null
go

ALTER TABLE PosicaoTermoBolsa  WITH CHECK ADD  CONSTRAINT Trader_PosicaoTermoBolsa_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO
ALTER TABLE PosicaoTermoBolsaHistorico  WITH CHECK ADD  CONSTRAINT Trader_PosicaoTermoBolsaHistorico_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO
ALTER TABLE PosicaoTermoBolsaAbertura  WITH CHECK ADD  CONSTRAINT Trader_PosicaoTermoBolsaAbertura_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO

ALTER TABLE PosicaoEmprestimoBolsa  WITH CHECK ADD  CONSTRAINT Trader_PosicaoEmprestimoBolsa_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO
ALTER TABLE PosicaoEmprestimoBolsaHistorico  WITH CHECK ADD  CONSTRAINT Trader_PosicaoEmprestimoBolsaHistorico_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO
ALTER TABLE PosicaoEmprestimoBolsaAbertura  WITH CHECK ADD  CONSTRAINT Trader_PosicaoEmprestimoBolsaAbertura_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO

alter table operacaoemprestimobolsa add IdTrader int null
go
ALTER TABLE operacaoemprestimobolsa  WITH CHECK ADD  CONSTRAINT Trader_OperacaoEmprestimoBolsa_FK1 FOREIGN KEY(IdTrader)
REFERENCES Trader (IdTrader)
GO