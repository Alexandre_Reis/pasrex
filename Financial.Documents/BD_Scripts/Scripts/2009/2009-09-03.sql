ALTER TABLE Carteira Add "TipoVisaoFundo" tinyint null
go
update carteira set TipoVisaoFundo = 1
go
ALTER TABLE Carteira alter column "TipoVisaoFundo" tinyint not null
go

ALTER TABLE Carteira Add "DistribuicaoDividendo" tinyint null
go
update carteira set DistribuicaoDividendo = 1
go
ALTER TABLE Carteira alter column "DistribuicaoDividendo" tinyint not null
go


alter table operacaocotista add Fonte tinyint null
go
update operacaocotista set Fonte = 1
go
alter table operacaocotista alter column Fonte tinyint not null
go

alter table ordembmf add PercentualDesconto decimal(8,2) null
go
update ordembmf set PercentualDesconto = 0
go
alter table ordembmf alter column PercentualDesconto decimal(8,2) not null
go

alter table operacaobmf add PercentualDesconto decimal(8,2) null
go
update operacaobmf set PercentualDesconto = 0
go
alter table operacaobmf alter column PercentualDesconto decimal(8,2) not null
go

alter table ordembmf add CONSTRAINT [DF_OrdemBMF_PercentualDesconto]  DEFAULT ((0)) for PercentualDesconto
go

alter table operacaobmf add CONSTRAINT [DF_OperacaoBMF_PercentualDesconto]  DEFAULT ((0)) for PercentualDesconto
go

