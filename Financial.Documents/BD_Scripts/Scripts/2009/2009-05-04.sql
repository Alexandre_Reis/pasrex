--***********************************DEPRECATED
if exists (select * from dbo.sysobjects where name = 'TabelaRebateCarteiraAdministracao')
drop table TabelaRebateCarteiraAdministracao
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateConsultorAdministracao')
drop table TabelaRebateConsultorAdministracao
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateConsultorPerformance')
drop table TabelaRebateConsultorPerformance
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateDistribuidorAdministracao')
drop table TabelaRebateDistribuidorAdministracao
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateDistribuidorPerformance')
drop table TabelaRebateDistribuidorPerformance
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateGestorAdministracao')
drop table TabelaRebateGestorAdministracao
go
if exists (select * from dbo.sysobjects where name = 'TabelaRebateGestorPerformance')
drop table TabelaRebateGestorPerformance
go
--***********************************

if exists (select * from dbo.sysobjects where name = 'TabelaRebateDistribuidor')
drop table TabelaRebateDistribuidor
go

if exists (select * from dbo.sysobjects where name = 'TabelaRebateOfficer')
drop table TabelaRebateOfficer
go

if exists (select * from dbo.sysobjects where name = 'TabelaRebateCarteira')
drop table TabelaRebateCarteira
go

if exists (select * from dbo.sysobjects where name = 'TabelaRebateGestor')
drop table TabelaRebateGestor
go

if exists (select * from dbo.sysobjects where name = 'TabelaRebateCorretagem')
drop table TabelaRebateCorretagem
go

if exists (select * from dbo.sysobjects where name = 'Officer')
drop table Officer
go

if exists (select * from dbo.sysobjects where name = 'CalculoRebateCarteira')
drop table CalculoRebateCarteira
go

if exists (select * from dbo.sysobjects where name = 'CalculoRebateCotista')
drop table CalculoRebateCotista
go


create table "TabelaRebateDistribuidor" ( 
	"DataReferencia" datetime not null,
	"IdCarteira" int not null,
	"IdAgenteDistribuidor" int not null,
	"Faixa" decimal(16,2) not null,
	"PercentualRebateAdministracao" decimal(8,4) not null,
	"PercentualRebatePerformance" decimal(8,4) not null)  
go

alter table "TabelaRebateDistribuidor"
	add constraint "TabelaRebateDistribuidor_PK" primary key ("DataReferencia", "IdAgenteDistribuidor", "IdCarteira", "Faixa")   
go

create table "CalculoRebateCotista" ( 
	"DataCalculo" datetime not null,
	"IdCarteira" int not null,
	"IdCotista" int not null,
	"RebateAdministracaoDia" decimal(16,2) not null,
	"RebateAdministracaoAcumulado" decimal(16,2) not null,
	"RebatePerformanceDia" decimal(16,2) not null,
	"RebatePerformanceAcumulado" decimal(16,2) not null)  
go

alter table "CalculoRebateCotista"
	add constraint "CalculoRebateCotista_PK" primary key ("DataCalculo", "IdCarteira", "IdCotista")   
go

create table "CalculoRebateCarteira" ( 
	"DataCalculo" datetime not null,
	"IdCarteira" int not null,
	"IdAgenteDistribuidor" int not null,
	"RebateAdministracaoDia" decimal(16,2) not null,
	"RebateAdministracaoAcumulado" decimal(16,2) not null,
	"RebatePerformanceDia" decimal(16,2) not null,
	"RebatePerformanceAcumulado" decimal(16,2) not null)  
go

alter table "CalculoRebateCarteira"
	add constraint "CalculoRebateCarteira_PK" primary key ("DataCalculo", "IdCarteira", "IdAgenteDistribuidor")   
go

create table "TabelaRebateGestor" ( 
	"DataReferencia" datetime not null,
	"IdAgenteGestor" int not null,
	"Faixa" decimal(16,2) not null,
	"PercentualRebateAdministracao" decimal(8,4) not null,
	"PercentualRebatePerformance" decimal(8,4) not null) ON 'PRIMARY'  
go

alter table "TabelaRebateGestor"
	add constraint "TabelaRebateGestor_PK" primary key clustered ("IdAgenteGestor", "Faixa", "DataReferencia")   
go

create table "TabelaRebateCorretagem" ( 
	"DataReferencia" datetime not null,
	"IdCliente" int not null,
	"IdAgenteCorretora" int not null,
	"PercentualRebate" decimal(8,4) not null) ON 'PRIMARY'  
go

alter table "TabelaRebateCorretagem"
	add constraint "TabelaRebateCorretagem_PK" primary key clustered ("IdAgenteCorretora", "IdCliente", "DataReferencia")   
go

create table "TabelaRebateOfficer" ( 
	"DataReferencia" datetime not null,
	"IdCarteira" int not null,
	"IdOfficer" int not null,
	"PercentualRebate" decimal(8,4) not null) ON 'PRIMARY'  
go

alter table "TabelaRebateOfficer"
	add constraint "TabelaRebateOfficer_PK" primary key clustered ("DataReferencia", "IdCarteira", "IdOfficer")   
go

create table "TabelaRebateCarteira" ( 
	"DataReferencia" datetime not null,
	"IdCarteira" int not null,
	"Faixa" decimal(16,2) not null,
	"PercentualRebateAdministracao" decimal(8,4) not null,
	"PercentualRebatePerformance" decimal(8,4) not null) ON 'PRIMARY'  
go

alter table "TabelaRebateCarteira"
	add constraint "TabelaRebateCarteira_PK" primary key clustered ("DataReferencia", "IdCarteira", "Faixa")   
go

create table "Officer" ( 
	"IdOfficer" int IDENTITY(1,1) not null,
	"Nome" varchar(255) not null,
	"Apelido" varchar(30) not null,
	"Tipo" tinyint not null) ON 'PRIMARY'  
go

alter table "Officer"
	add constraint "Officer_PK" primary key clustered ("IdOfficer")   
go

/* Add foreign key constraints to table "TabelaRebateDistribuidor".                           */
alter table "TabelaRebateDistribuidor"
	add constraint "Carteira_TabelaRebateDistribuidor_FK1" foreign key (
		"IdCarteira")
	 references "Carteira" (
		"IdCarteira") on update no action on delete no action  
go

alter table "TabelaRebateDistribuidor"
	add constraint "AgenteMercado_TabelaRebateDistribuidor_FK1" foreign key (
		"IdAgenteDistribuidor")
	 references "AgenteMercado" (
		"IdAgente") on update no action on delete no action  
go

/* Add foreign key constraints to table "CalculoRebateCotista".                               */
alter table "CalculoRebateCotista"
	add constraint "Carteira_CalculoRebateCotista_FK1" foreign key (
		"IdCarteira")
	 references "Carteira" (
		"IdCarteira") on update no action on delete no action
go

alter table "CalculoRebateCotista"
	add constraint "Cotista_CalculoRebateCotista_FK1" foreign key (
		"IdCotista")
	 references "Cotista" (
		"IdCotista") on update no action on delete no action 
go

/* Add foreign key constraints to table "CalculoRebateCarteira".                              */
alter table "CalculoRebateCarteira"
	add constraint "Carteira_CalculoRebateCarteira_FK1" foreign key (
		"IdCarteira")
	 references "Carteira" (
		"IdCarteira") on update no action on delete no action
go

alter table "CalculoRebateCarteira"
	add constraint "AgenteMercado_CalculoRebateCarteira_FK1" foreign key (
		"IdAgenteDistribuidor")
	 references "AgenteMercado" (
		"IdAgente") on update no action on delete no action  
go

/* Add foreign key constraints to table "TabelaRebateGestor".                                 */
alter table "TabelaRebateGestor"
	add constraint "AgenteMercado_TabelaRebateGestor_FK1" foreign key (
		"IdAgenteGestor")
	 references "AgenteMercado" (
		"IdAgente") on update no action on delete cascade  
go

/* Add foreign key constraints to table "TabelaRebateCorretagem".                             */
alter table "TabelaRebateCorretagem"
	add constraint "AgenteMercado_TabelaRebateCorretagem_FK1" foreign key (
		"IdAgenteCorretora")
	 references "AgenteMercado" (
		"IdAgente") on update no action on delete cascade  
go

alter table "TabelaRebateCorretagem"
	add constraint "Cliente_TabelaRebateCorretagem_FK1" foreign key (
		"IdCliente")
	 references "Cliente" (
		"IdCliente") on update no action on delete cascade 
go

/* Add foreign key constraints to table "TabelaRebateOfficer".                              */
alter table "TabelaRebateOfficer"
	add constraint "Carteira_TabelaRebateOfficer_FK1" foreign key (
		"IdCarteira")
	 references "Carteira" (
		"IdCarteira") on update no action on delete cascade
go

alter table "TabelaRebateOfficer"
	add constraint "Officer_TabelaRebateOfficer_FK1" foreign key (
		"IdOfficer")
	 references "Officer" (
		"IdOfficer") on update no action on delete cascade  
go

/* Add foreign key constraints to table "TabelaRebateCarteira".                               */
alter table "TabelaRebateCarteira"
	add constraint "Carteira_TabelaRebateCarteira_FK1" foreign key (
		"IdCarteira")
	 references "Carteira" (
		"IdCarteira") on update no action on delete cascade  
go

