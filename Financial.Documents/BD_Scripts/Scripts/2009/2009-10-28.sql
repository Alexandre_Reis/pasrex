drop table SubscricaoBolsa
go
create table SubscricaoBolsa
(IdSubscricao int identity
,DataLancamento datetime not null
,DataEx datetime not null
,DataReferencia datetime not null
,PrecoSubscricao decimal(16,8) not null
,Fator decimal(20,11) not null
,CdAtivoBolsaDireito varchar(20) not null
,CdAtivoBolsa varchar(20) not null
,Fonte int not null
,DataFimNegociacao datetime not null,
CONSTRAINT [SubscricaoBolsa_PK] PRIMARY KEY CLUSTERED 
(
	[IdSubscricao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
go

ALTER TABLE [dbo].[SubscricaoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscricaoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK2] FOREIGN KEY([CdAtivoBolsaDireito])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO