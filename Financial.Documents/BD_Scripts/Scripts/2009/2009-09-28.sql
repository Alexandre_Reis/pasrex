ALTER TABLE [dbo].[CalculoRebateCarteira] drop constraint Carteira_CalculoRebateCarteira_FK1
go
ALTER TABLE [dbo].[CalculoRebateCarteira]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoRebateCarteira_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CalculoRebateCarteira] CHECK CONSTRAINT [Carteira_CalculoRebateCarteira_FK1]
go

ALTER TABLE [dbo].[CalculoRebateCotista] drop constraint Carteira_CalculoRebateCotista_FK1
GO
ALTER TABLE [dbo].[CalculoRebateCotista]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoRebateCotista_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CalculoRebateCotista] CHECK CONSTRAINT [Carteira_CalculoRebateCotista_FK1]
go

ALTER TABLE [dbo].[BookCliente] drop constraint Cliente_BookCliente_FK1
GO
ALTER TABLE [dbo].[BookCliente]  WITH CHECK ADD  CONSTRAINT [Cliente_BookCliente_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookCliente] CHECK CONSTRAINT [Cliente_BookCliente_FK1]
go

ALTER TABLE [dbo].[CalculoGerencial] drop Cliente_CalculoGerencial_FK1
ALTER TABLE [dbo].[CalculoGerencial]  WITH CHECK ADD  CONSTRAINT [Cliente_CalculoGerencial_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CalculoGerencial] CHECK CONSTRAINT [Cliente_CalculoGerencial_FK1]
GO

ALTER TABLE [dbo].[GerOperacaoBMF] drop Cliente_GerOperacaoBMF_FK1
ALTER TABLE [dbo].[GerOperacaoBMF]  WITH CHECK ADD  CONSTRAINT [Cliente_GerOperacaoBMF_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerOperacaoBMF] CHECK CONSTRAINT [Cliente_GerOperacaoBMF_FK1]
GO

ALTER TABLE [dbo].[GerOperacaoBolsa]  drop Cliente_GerOperacaoBolsa_FK1
ALTER TABLE [dbo].[GerOperacaoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_GerOperacaoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerOperacaoBolsa] CHECK CONSTRAINT [Cliente_GerOperacaoBolsa_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBMF] drop Cliente_GerPosicaoBMF_FK1
ALTER TABLE [dbo].[GerPosicaoBMF]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMF_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBMF] CHECK CONSTRAINT [Cliente_GerPosicaoBMF_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBMFAbertura]  drop Cliente_GerPosicaoBMFAbertura_FK1
ALTER TABLE [dbo].[GerPosicaoBMFAbertura]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMFAbertura_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBMFAbertura] CHECK CONSTRAINT [Cliente_GerPosicaoBMFAbertura_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBMFHistorico]  drop Cliente_GerPosicaoBMFHistorico_FK1
ALTER TABLE [dbo].[GerPosicaoBMFHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBMFHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBMFHistorico] CHECK CONSTRAINT [Cliente_GerPosicaoBMFHistorico_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBolsa] drop Cliente_GerPosicaoBolsa_FK1
ALTER TABLE [dbo].[GerPosicaoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBolsa] CHECK CONSTRAINT [Cliente_GerPosicaoBolsa_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBolsaAbertura]  drop Cliente_GerPosicaoBolsaAbertura_FK1
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] CHECK CONSTRAINT [Cliente_GerPosicaoBolsaAbertura_FK1]
go

ALTER TABLE [dbo].[GerPosicaoBolsaHistorico]  drop Cliente_GerPosicaoBolsaHistorico_FK1
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico]  WITH CHECK ADD  CONSTRAINT [Cliente_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] CHECK CONSTRAINT [Cliente_GerPosicaoBolsaHistorico_FK1]
go

ALTER TABLE [dbo].[PosicaoEmprestimoBolsa]  drop Cliente_PosicaoEmprestimoBolsa_FK1
ALTER TABLE [dbo].[PosicaoEmprestimoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_PosicaoEmprestimoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PosicaoEmprestimoBolsa] CHECK CONSTRAINT [Cliente_PosicaoEmprestimoBolsa_FK1]
go

ALTER TABLE [dbo].[PosicaoTermoBolsa] drop Cliente_PosicaoTermoBolsa_FK1
ALTER TABLE [dbo].[PosicaoTermoBolsa]  WITH CHECK ADD  CONSTRAINT [Cliente_PosicaoTermoBolsa_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PosicaoTermoBolsa] CHECK CONSTRAINT [Cliente_PosicaoTermoBolsa_FK1]
go

ALTER TABLE [dbo].[EnquadraItemFundo] drop Carteira_EnquadraItemFundo_FK1
ALTER TABLE [dbo].[EnquadraItemFundo]  WITH CHECK ADD  CONSTRAINT [Carteira_EnquadraItemFundo_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EnquadraItemFundo] CHECK CONSTRAINT [Carteira_EnquadraItemFundo_FK1]
go

ALTER TABLE [dbo].[TabelaRebateDistribuidor]  drop Carteira_TabelaRebateDistribuidor_FK1
ALTER TABLE [dbo].[TabelaRebateDistribuidor]  WITH CHECK ADD  CONSTRAINT [Carteira_TabelaRebateDistribuidor_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TabelaRebateDistribuidor] CHECK CONSTRAINT [Carteira_TabelaRebateDistribuidor_FK1]
GO

ALTER TABLE [dbo].[TemplateCarteira] drop Carteira_TemplateCarteira_FK1
ALTER TABLE [dbo].[TemplateCarteira]  WITH CHECK ADD  CONSTRAINT [Carteira_TemplateCarteira_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TemplateCarteira] CHECK CONSTRAINT [Carteira_TemplateCarteira_FK1]
go