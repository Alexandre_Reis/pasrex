CREATE TABLE dbo.PerfilCorretagemBolsa_Aux(
	IdCliente int NOT NULL,
	IdAgente int NOT NULL,
	DataReferencia datetime NOT NULL,
	TipoMercado char(3) NOT NULL,
	IdTemplate int NOT NULL,
	PercentualDesconto decimal(8, 2) NOT NULL,	
 CONSTRAINT PerfilCorretagemBolsa_Aux_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	IdAgente ASC,
	DataReferencia ASC,
	TipoMercado ASC
)
)
GO

insert into PerfilCorretagemBolsa_Aux
select IdCliente,
	IdAgente,
	DataReferencia,
	'',
	IdTemplate,
	PercentualDesconto
from PerfilCorretagemBolsa
go

drop table PerfilCorretagemBolsa
go

CREATE TABLE dbo.PerfilCorretagemBolsa(
	IdCliente int NOT NULL,
	IdAgente int NOT NULL,
	DataReferencia datetime NOT NULL,
	TipoMercado char(3) NOT NULL,
	IdTemplate int NOT NULL,
	PercentualDesconto decimal(8, 2) NOT NULL,	
 CONSTRAINT PerfilCorretagemBolsa_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	IdAgente ASC,
	DataReferencia ASC,
	TipoMercado ASC
)
) 
GO

ALTER TABLE dbo.PerfilCorretagemBolsa  WITH CHECK ADD  CONSTRAINT AgenteMercado_PerfilCorretagemBolsa_FK1 FOREIGN KEY(IdAgente)
REFERENCES dbo.AgenteMercado (IdAgente)
ON DELETE CASCADE
GO
ALTER TABLE dbo.PerfilCorretagemBolsa CHECK CONSTRAINT AgenteMercado_PerfilCorretagemBolsa_FK1
go

ALTER TABLE dbo.PerfilCorretagemBolsa  WITH CHECK ADD  CONSTRAINT Cliente_PerfilCorretagemBolsa_FK1 FOREIGN KEY(IdCliente)
REFERENCES dbo.Cliente (IdCliente)
ON DELETE CASCADE
GO
ALTER TABLE dbo.PerfilCorretagemBolsa CHECK CONSTRAINT Cliente_PerfilCorretagemBolsa_FK1
go

ALTER TABLE dbo.PerfilCorretagemBolsa  WITH CHECK ADD  CONSTRAINT TemplateCorretagemBolsa_PerfilCorretagemBolsa_FK1 FOREIGN KEY(IdTemplate)
REFERENCES dbo.TemplateCorretagemBolsa (IdTemplate)
ON DELETE CASCADE
GO
ALTER TABLE dbo.PerfilCorretagemBolsa CHECK CONSTRAINT TemplateCorretagemBolsa_PerfilCorretagemBolsa_FK1
go

insert into PerfilCorretagemBolsa
select IdCliente,
	IdAgente,
	DataReferencia,
	TipoMercado,
	IdTemplate,
	PercentualDesconto
from PerfilCorretagemBolsa_Aux
go

drop table PerfilCorretagemBolsa_Aux
go