﻿alter table bloqueiobolsa add IdAgente int not null
go

CREATE TABLE [dbo].[ProventoBolsa](
	[IdProvento] [int] IDENTITY(1,1) NOT NULL,
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[DataLancamento] [datetime] NOT NULL,
	[DataEx] [datetime] NOT NULL,
	[DataPagamento] [datetime] NOT NULL,
	[TipoProvento] [tinyint] NOT NULL,
	[Valor] [decimal](16, 8) NOT NULL
 CONSTRAINT [ProventoBolsa_PK] PRIMARY KEY CLUSTERED 
(
	[IdProvento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProventoBolsa]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProventoBolsa] CHECK CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2]
GO

insert into tipoinvestidorcvm values (1, 'Companhia Aberta')
insert into tipoinvestidorcvm values (2, 'Sociedade Beneficiária')
insert into tipoinvestidorcvm values (3, 'Instituição Financeira')
insert into tipoinvestidorcvm values (4, 'Investidor Estrangeiro')
insert into tipoinvestidorcvm values (5, 'Fundo Investimento')
insert into tipoinvestidorcvm values (6, 'Fundo Aplicação Cotas')

insert into tabelataxafiscalizacaocvm values (5, .00, 600.00)
insert into tabelataxafiscalizacaocvm values (5, 2500000.00, 900.00)
insert into tabelataxafiscalizacaocvm values (5, 5000000.00, 1350.00)
insert into tabelataxafiscalizacaocvm values (5, 10000000.00, 1800.00)
insert into tabelataxafiscalizacaocvm values (5, 20000000.00, 2400.00)
insert into tabelataxafiscalizacaocvm values (5, 40000000.00, 3840.00)
insert into tabelataxafiscalizacaocvm values (5, 80000000.00, 5760.00)
insert into tabelataxafiscalizacaocvm values (5, 160000000.00, 7680.00)
insert into tabelataxafiscalizacaocvm values (5, 320000000.00, 9600.00)
insert into tabelataxafiscalizacaocvm values (5, 640000000.00, 10800.00)
insert into tabelataxafiscalizacaocvm values (6, .00, 300.00)
insert into tabelataxafiscalizacaocvm values (6, 2500000.00, 450.00)
insert into tabelataxafiscalizacaocvm values (6, 5000000.00, 675.00)
insert into tabelataxafiscalizacaocvm values (6, 10000000.00, 900.00)
insert into tabelataxafiscalizacaocvm values (6, 20000000.00, 1200.00)
insert into tabelataxafiscalizacaocvm values (6, 40000000.00, 1920.00)
insert into tabelataxafiscalizacaocvm values (6, 80000000.00, 2880.00)
insert into tabelataxafiscalizacaocvm values (6, 160000000.00, 3840.00)
insert into tabelataxafiscalizacaocvm values (6, 320000000.00, 4800.00)
insert into tabelataxafiscalizacaocvm values (6, 640000000.00, 5400.00)
