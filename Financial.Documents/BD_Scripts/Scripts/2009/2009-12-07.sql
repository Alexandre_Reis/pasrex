CREATE TABLE [dbo].[TabelaCONR](
	[IdConr] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NOT NULL,
	[Data] [datetime] NOT NULL,
	[CdAtivoBolsa] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NumeroNegocio] [int] NOT NULL,
	[TipoOperacao] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Quantidade] [decimal](16, 4) NOT NULL,
	[Preco] [decimal](16, 8) NOT NULL,
	[NumeroContrato] [int] NOT NULL,
 CONSTRAINT [PK_CONR] PRIMARY KEY CLUSTERED 
(
	[IdConr] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TabelaCONR]  WITH CHECK ADD  CONSTRAINT [AtivoBolsa_TabelaCONR_FK1] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO
ALTER TABLE [dbo].[TabelaCONR] CHECK CONSTRAINT [AtivoBolsa_TabelaCONR_FK1]
GO
ALTER TABLE [dbo].[TabelaCONR]  WITH CHECK ADD  CONSTRAINT [Cliente_TabelaCONR_FK1] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[TabelaCONR] CHECK CONSTRAINT [Cliente_TabelaCONR_FK1]