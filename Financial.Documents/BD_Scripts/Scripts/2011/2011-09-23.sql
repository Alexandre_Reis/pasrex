alter table operacaorendafixa alter column quantidade decimal(25, 12) not null
go
alter table posicaorendafixa alter column quantidade decimal(25, 12) not null
go
alter table posicaorendafixahistorico alter column quantidade decimal(25, 12) not null
go
alter table posicaorendafixaabertura alter column quantidade decimal(25, 12) not null
go

alter table tabelaparametrosfront add EmailFundo varchar(max) null
go
alter table tabelaparametrosfront add EmailCotista varchar(max) null
go
alter table tabelaparametrosfront add EmailRendaFixa varchar(max) null
go