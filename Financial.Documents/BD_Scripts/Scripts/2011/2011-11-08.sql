﻿alter table carteira add ContagemPrazoIOF tinyint null
go
update carteira set ContagemPrazoIOF = 1 --Mellon
go
alter table carteira alter column ContagemPrazoIOF tinyint not null
go

CREATE TABLE AgendaFundo(
	IdAgenda int IDENTITY(1,1) NOT NULL,
	IdCarteira int NOT NULL,
	TipoEvento tinyint NOT NULL,
	DataEvento datetime NOT NULL,
	Valor decimal(20, 8) NULL,
 CONSTRAINT AgendaFundo_PK PRIMARY KEY CLUSTERED 
(
	IdAgenda ASC
)
)

GO
ALTER TABLE AgendaFundo WITH CHECK ADD  CONSTRAINT Carteira_AgendaFundo_FK1 FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
ON DELETE CASCADE

--Acesso à nova entrada de agenda de eventos para fundos (amortização e juros)
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,4840,'S','S','S','S')
go
insert into tipocliente values (510, 'FIDC')
go