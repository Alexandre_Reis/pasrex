alter table operacaorendafixa add IdFormaLiquidacao tinyint null
go
update operacaorendafixa set IdFormaLiquidacao = tipoliquidacao
go
alter table operacaorendafixa alter column IdFormaLiquidacao tinyint not null
go

alter table operacaorendafixa drop column tipoliquidacao
go

alter table ordemrendafixa add IdFormaLiquidacao tinyint null
go
update ordemrendafixa set IdFormaLiquidacao = tipoliquidacao
go
alter table ordemrendafixa alter column IdFormaLiquidacao tinyint not null
go

alter table ordemrendafixa drop column tipoliquidacao
go