alter table cliente add DescontaTributoPL char(1) null
alter table cliente add GrossUP char(1) null
go

update cliente set DescontaTributoPL = 'N'
update cliente set GrossUP = 'N'
go

alter table cliente alter column DescontaTributoPL char(1) not null
alter table cliente alter column GrossUP char(1) not null
go