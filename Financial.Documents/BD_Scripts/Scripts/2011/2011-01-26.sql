alter table OrdemBMF drop constraint DF_OrdemBMF_CorretagemDayTrade
go
alter table OrdemBMF drop constraint DF_OrdemBMF_EmolumentoDayTrade
go
alter table OrdemBMF drop constraint DF_OrdemBMF_RegistroDayTrade
go
alter table OrdemBMF drop constraint DF_OrdemBMF_CustoWTRDayTrade
go
alter table OrdemBMF drop column CorretagemDayTrade
go
alter table OrdemBMF drop column EmolumentoDayTrade
go
alter table OrdemBMF drop column RegistroDayTrade
go
alter table OrdemBMF drop column CustoWTRDayTrade
go

--Cotacao Tesouro
insert into permissaomenu(IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) 
select idgrupo,	3760, 'N', 'N', 'N', 'N' from GrupoUsuario
go

create table CotacaoMercadoTesouro
(DataReferencia datetime not null,
 Descricao varchar(20) not null,
 DataVencimento datetime not null,
 PU decimal (28, 16) not null
CONSTRAINT [PK_CotacaoMercadoTesouro] PRIMARY KEY CLUSTERED 
(
	[Descricao] ASC,
	[DataReferencia] ASC,
	[DataVencimento] ASC
)
)
go

INSERT INTO Configuracao VALUES (44, 'IR Simulado', NULL, 'S');
go

-- Criacao da Tabela TabelaExtratoCliente
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TabelaExtratoCliente](
	[IdCliente] [int] NOT NULL,
	[IdSubReport] [int] NOT NULL,
	[PosicaoX] [int] NOT NULL,
	[PosicaoY] [int] NOT NULL,
 CONSTRAINT [PK_TabelaExtratoCliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC,
	[IdSubReport] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TabelaExtratoCliente]  WITH CHECK ADD  CONSTRAINT [FK_TabelaExtratoCliente_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[TabelaExtratoCliente] CHECK CONSTRAINT [FK_TabelaExtratoCliente_Cliente]