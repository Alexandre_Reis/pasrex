alter table saldocaixa drop constraint ContaCorrente_SaldoCaixa_FK1
go

CREATE TABLE ContaCorrenteAux(
	IdConta int NOT NULL,
	IdBanco int NULL,
	IdAgencia int NULL,
	IdPessoa int NOT NULL,
	Numero varchar(10) NOT NULL,
	TipoConta tinyint NOT NULL,
	ContaDefault char(1) NOT NULL,
	IdMoeda int NOT NULL,
 CONSTRAINT ContaCorrenteAux_PK PRIMARY KEY CLUSTERED 
(
	IdConta ASC
)
)
GO

insert into ContaCorrenteAux
(IdConta
,IdBanco
,IdAgencia
,IdPessoa
,Numero
,TipoConta
,ContaDefault
,IdMoeda)
select * from ContaCorrente
go

drop table ContaCorrente
go

CREATE TABLE ContaCorrente(
	IdConta int IDENTITY(1,1) NOT NULL,
	IdBanco int NULL,
	IdAgencia int NULL,
	IdPessoa int NOT NULL,
	Numero varchar(10) NOT NULL,
	TipoConta tinyint NOT NULL,
	ContaDefault char(1) NOT NULL,
	IdMoeda int NOT NULL,
 CONSTRAINT ContaCorrente_PK PRIMARY KEY CLUSTERED 
(
	IdConta ASC
)
)
GO

ALTER TABLE ContaCorrente  WITH CHECK ADD  CONSTRAINT Agencia_ContaCorrente_FK1 FOREIGN KEY(IdAgencia)
REFERENCES Agencia (IdAgencia)
GO
ALTER TABLE ContaCorrente CHECK CONSTRAINT Agencia_ContaCorrente_FK1
GO
ALTER TABLE ContaCorrente  WITH CHECK ADD  CONSTRAINT Banco_ContaCorrente_FK1 FOREIGN KEY(IdBanco)
REFERENCES Banco (IdBanco)
ON DELETE CASCADE
GO
ALTER TABLE ContaCorrente CHECK CONSTRAINT Banco_ContaCorrente_FK1
GO
ALTER TABLE ContaCorrente  WITH CHECK ADD  CONSTRAINT Pessoa_ContaCorrente_FK1 FOREIGN KEY(IdPessoa)
REFERENCES Pessoa (IdPessoa)
ON DELETE CASCADE
GO
ALTER TABLE ContaCorrente CHECK CONSTRAINT Pessoa_ContaCorrente_FK1

set identity_insert ContaCorrente on
go
insert into ContaCorrente
(IdConta
,IdBanco
,IdAgencia
,IdPessoa
,Numero
,TipoConta
,ContaDefault
,IdMoeda)
select * from ContaCorrenteAux
go
set identity_insert ContaCorrente off
go

ALTER TABLE SaldoCaixa  WITH CHECK ADD  CONSTRAINT ContaCorrente_SaldoCaixa_FK1 FOREIGN KEY(IdConta)
REFERENCES ContaCorrente (IdConta)
GO
ALTER TABLE SaldoCaixa CHECK CONSTRAINT ContaCorrente_SaldoCaixa_FK1

drop table ContaCorrenteAux
go

alter table operacaocotista add IdConta int null
go
alter table ordemcotista add IdConta int null
go
alter table ordemfundo add IdConta int null
go

alter table ordemrendafixa add Observacao varchar(max) null
go
alter table ordemrendafixa alter column Observacao varchar(max) not null
go

alter table ordemrendafixa alter column IdTitulo int null
go
alter table ordemfundo alter column IdCarteira int null
go
alter table ordemcotista alter column IdCarteira int null
go

create table TabelaParametrosFront
(IdTabela int IDENTITY(1,1) not null,
ObservacaoFundo varchar(max) null,
ObservacaoCotista varchar(max) null,
ObservacaoRendaFixa varchar(max) null,
HorarioFimFundo datetime null,
HorarioFimCotista datetime null,
HorarioFimRendaFixa datetime null,
CONSTRAINT TabelaParametrosFront_PK PRIMARY KEY CLUSTERED 
(
	IdTabela ASC
)
)

--Acesso a TabelaConfiguracaoFront
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,1805,'N','N','N','N')
