alter table usuario add TrocaSenha char(1) null
go
update usuario set TrocaSenha = 'N'
go
alter table usuario alter column TrocaSenha char(1) not null
go