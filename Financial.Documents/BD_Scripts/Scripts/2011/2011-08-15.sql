alter table cliente add ZeraCaixa char(1) null
go

update cliente set ZeraCaixa = 'N'
go

alter table cliente alter column ZeraCaixa char(1) not null
go

CREATE TABLE OrdemRendaFixa(
	IdOperacao int IDENTITY(1,1) NOT NULL,
	IdCliente int NOT NULL,
	IdTitulo int NOT NULL,
	DataOperacao datetime NOT NULL,
	DataLiquidacao datetime NOT NULL,
	TipoOperacao tinyint NOT NULL,
	Quantidade decimal(16, 2) NOT NULL,
	PUOperacao decimal(25, 12) NOT NULL,
	Valor decimal(16, 2) NOT NULL,
	TaxaOperacao decimal(25, 16) NULL,
	DataVolta datetime NULL,
	TaxaVolta decimal(8, 4) NULL,
	PUVolta decimal(25, 12) NULL,
	ValorVolta decimal(16, 2) NULL,
	IdOperacaoResgatada int NULL,
	TipoNegociacao tinyint NOT NULL,
	TipoLiquidacao tinyint NOT NULL,
	LocalCustodia tinyint NOT NULL,
	IdPosicaoResgatada int NULL,
	Status tinyint not null
 CONSTRAINT OrdemRendaFixa_PK PRIMARY KEY NONCLUSTERED 
(
	IdOperacao ASC
)
)

GO
ALTER TABLE OrdemRendaFixa  WITH CHECK ADD  CONSTRAINT Cliente_OrdemRendaFixa_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO
ALTER TABLE OrdemRendaFixa CHECK CONSTRAINT Cliente_OrdemRendaFixa_FK1
GO
ALTER TABLE OrdemRendaFixa  WITH CHECK ADD  CONSTRAINT TituloRendaFixa_OrdemRendaFixa_FK1 FOREIGN KEY(IdTitulo)
REFERENCES TituloRendaFixa (IdTitulo)
ON DELETE CASCADE
GO
ALTER TABLE OrdemRendaFixa CHECK CONSTRAINT TituloRendaFixa_OrdemRendaFixa_FK1