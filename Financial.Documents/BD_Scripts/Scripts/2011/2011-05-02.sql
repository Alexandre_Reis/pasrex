create table HistoricoEstrategia
(Data datetime not null,
 IdCarteira int not null,
 IdEstrategia int not null,
 ValorCarteira decimal (18, 2) not null,
 ValorLiquidar decimal (18, 2) not null,
 SaldoCaixa decimal (18, 2) not null, 
 ValorPL decimal (18, 2) not null, 
 Quantidade decimal (26,8) not null,
 Cota decimal (26,8) not null
 )
go

ALTER TABLE HistoricoEstrategia ADD  CONSTRAINT HistoricoEstrategia_PK PRIMARY KEY CLUSTERED 
(
	Data ASC,
	IdCarteira ASC,
	IdEstrategia ASC
)
go

ALTER TABLE HistoricoEstrategia  WITH CHECK ADD  CONSTRAINT Carteira_HistoricoEstrategia_FK1 FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
ON DELETE CASCADE

create table LiquidacaoEstrategia
(IdLiquidacao int IDENTITY(1,1) not null,
 IdCarteira int not null,
 IdEstrategia int not null,
 DataReferencia datetime not null,
 DataVencimento datetime not null,
 Valor decimal (18, 2) not null 
 )
go

ALTER TABLE LiquidacaoEstrategia ADD  CONSTRAINT LiquidacaoEstrategia_PK PRIMARY KEY CLUSTERED 
(
	IdLiquidacao ASC
)
go

--1 = admin, 2 = interno, 3 = externo
alter table GrupoUsuario add TipoPerfil tinyint null
go

update GrupoUsuario set TipoPerfil = 1 where descricao like '%admin%'
go
update GrupoUsuario set TipoPerfil = 3 where descricao not like '%admin%'
go

alter table GrupoUsuario alter column TipoPerfil tinyint not null
go
