CREATE TABLE OrdemFundo(
	IdOperacao int IDENTITY(1,1) NOT NULL,
	IdCliente int NOT NULL,
	IdCarteira int NOT NULL,
	DataOperacao datetime NOT NULL,
	DataConversao datetime NOT NULL,
	DataLiquidacao datetime NOT NULL,
	DataAgendamento datetime NOT NULL,
	TipoOperacao tinyint NOT NULL,
	TipoResgate tinyint NULL,
	IdPosicaoResgatada int NULL,
	IdFormaLiquidacao tinyint NOT NULL,
	Quantidade decimal(28, 12) NOT NULL CONSTRAINT DF_OrdemFundo_Quantidade  DEFAULT ((0)),
	ValorBruto decimal(16, 2) NOT NULL CONSTRAINT DF_OrdemFundo_ValorBruto  DEFAULT ((0)),
	ValorLiquido decimal(16, 2) NOT NULL CONSTRAINT DF_OrdemFundo_ValorLiquido  DEFAULT ((0)),
	Observacao varchar(400) NOT NULL CONSTRAINT DF_OrdemFundo_Observacao  DEFAULT (''),
	Status tinyint NOT NULL,
 CONSTRAINT OrdemFundo_PK PRIMARY KEY NONCLUSTERED 
(
	IdOperacao ASC
)
)
go

ALTER TABLE OrdemFundo  WITH CHECK ADD  CONSTRAINT Carteira_OrdemFundo_FK1 FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
ON DELETE CASCADE
GO
ALTER TABLE OrdemFundo CHECK CONSTRAINT Carteira_OrdemFundo_FK1
GO
ALTER TABLE OrdemFundo  WITH CHECK ADD  CONSTRAINT Cliente_OrdemFundo_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO
ALTER TABLE OrdemFundo CHECK CONSTRAINT Cliente_OrdemFundo_FK1
GO
ALTER TABLE OrdemFundo  WITH CHECK ADD  CONSTRAINT FormaLiquidacao_OrdemFundo_FK1 FOREIGN KEY(IdFormaLiquidacao)
REFERENCES FormaLiquidacao (IdFormaLiquidacao)
ON DELETE CASCADE
GO
ALTER TABLE OrdemFundo CHECK CONSTRAINT FormaLiquidacao_OrdemFundo_FK1


CREATE TABLE OrdemCotista(
	IdOperacao int IDENTITY(1,1) NOT NULL,
	IdCotista int NOT NULL,
	IdCarteira int NOT NULL,
	DataOperacao datetime NOT NULL,
	DataConversao datetime NOT NULL,
	DataLiquidacao datetime NOT NULL,
	DataAgendamento datetime NOT NULL,
	TipoOperacao tinyint NOT NULL,
	TipoResgate tinyint NULL,
	IdPosicaoResgatada int NULL,
	IdFormaLiquidacao tinyint NOT NULL,
	Quantidade decimal(28, 12) NOT NULL CONSTRAINT DF_OrdemCotista_Quantidade  DEFAULT ((0)),
	ValorBruto decimal(16, 2) NOT NULL CONSTRAINT DF_OrdemCotista_ValorBruto  DEFAULT ((0)),
	ValorLiquido decimal(16, 2) NOT NULL CONSTRAINT DF_OrdemCotista_ValorLiquido  DEFAULT ((0)),
	Observacao varchar(400) NOT NULL CONSTRAINT DF_OrdemCotista_Observacao  DEFAULT (''),
	Status tinyint NOT NULL,
 CONSTRAINT OrdemCotista_PK PRIMARY KEY NONCLUSTERED 
(
	IdOperacao ASC
)
)
go

ALTER TABLE OrdemCotista  WITH CHECK ADD  CONSTRAINT Carteira_OrdemCotista_FK1 FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
ON DELETE CASCADE
GO
ALTER TABLE OrdemCotista CHECK CONSTRAINT Carteira_OrdemCotista_FK1
GO
ALTER TABLE OrdemCotista  WITH CHECK ADD  CONSTRAINT Cotista_OrdemCotista_FK1 FOREIGN KEY(IdCotista)
REFERENCES Cotista (IdCotista)
ON DELETE CASCADE
GO
ALTER TABLE OrdemCotista CHECK CONSTRAINT Cotista_OrdemCotista_FK1
GO
ALTER TABLE OrdemCotista  WITH CHECK ADD  CONSTRAINT FormaLiquidacao_OrdemCotista_FK1 FOREIGN KEY(IdFormaLiquidacao)
REFERENCES FormaLiquidacao (IdFormaLiquidacao)
ON DELETE CASCADE
GO
ALTER TABLE OrdemCotista CHECK CONSTRAINT FormaLiquidacao_OrdemCotista_FK1


alter table GrupoUsuario add Front tinyint null
go
update GrupoUsuario set Front = 1
go
alter table GrupoUsuario alter column Front tinyint not null
go