﻿alter table operacaorendafixa add ValorLiquido decimal(16, 2) null
go

update operacaorendafixa set ValorLiquido = valor - valorIR - valoriof
go

alter table operacaorendafixa alter column ValorLiquido decimal(16, 2) not null
go

ALTER TABLE OperacaoRendaFixa ADD  CONSTRAINT DF_OperacaoRendaFixa_ValorLiquido  DEFAULT ((0)) FOR ValorLiquido
go

CREATE TABLE [CotacaoDebenture](
	[Data] [datetime] NOT NULL,
	[CodigoPapel] [nvarchar](12) NOT NULL,
	[PU] [decimal](28, 16) NOT NULL,
 CONSTRAINT [PK_CotacaoDebenture] PRIMARY KEY CLUSTERED 
 (
	[Data] ASC,
	[CodigoPapel] ASC
 )
)
go

--Acesso à nova entrada do extrato de cliente (Configuracao)
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,15020,'S','S','S','S')

--Acesso à nova entrada de Consulta de Vencimentos
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,7230,'S','S','S','S')
