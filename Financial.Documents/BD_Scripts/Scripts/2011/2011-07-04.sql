﻿alter table agentemercado add CodigoAnbid varchar(10) null
go

set identity_insert Estrategia on
go
insert into Estrategia (IdEstrategia, Descricao) values (6, 'Offshore')
insert into Estrategia (IdEstrategia, Descricao) values (7, 'Capital Protegido')
insert into Estrategia (IdEstrategia, Descricao) values (8, 'Fundo Imobiliário')
insert into Estrategia (IdEstrategia, Descricao) values (9, 'FIP')
insert into Estrategia (IdEstrategia, Descricao) values (10, 'LongShort')
insert into Estrategia (IdEstrategia, Descricao) values (11, 'Previdência')
insert into Estrategia (IdEstrategia, Descricao) values (12, 'FIDC')
set identity_insert Estrategia off
go