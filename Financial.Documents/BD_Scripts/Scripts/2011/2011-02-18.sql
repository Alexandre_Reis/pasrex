﻿--Inclusao do Acesso a nova tabela de ExtratoCliente
insert into permissaomenu(IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) 
select idgrupo,	15115, 'N', 'N', 'N', 'N' from GrupoUsuario

--Inclusao do Acesso a nova tabela de Estrategia
insert into permissaomenu(IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) 
select idgrupo,	7700, 'N', 'N', 'N', 'N' from GrupoUsuario


create table Estrategia
(IdEstrategia int IDENTITY(1,1) not null,
 Descricao varchar(50) not null,
CONSTRAINT [Estrategia_PK] PRIMARY KEY CLUSTERED
(
	IdEstrategia ASC
)
)
go

set identity_insert Estrategia on
go
insert into Estrategia (IdEstrategia, Descricao) values (1, 'Ações')
insert into Estrategia (IdEstrategia, Descricao) values (2, 'Renda Fixa')
insert into Estrategia (IdEstrategia, Descricao) values (3, 'DI')
insert into Estrategia (IdEstrategia, Descricao) values (4, 'Multimercado')
insert into Estrategia (IdEstrategia, Descricao) values (5, 'Cambial')
set identity_insert Estrategia off
go

alter table AtivoBolsa add IdEstrategia int null
go
ALTER TABLE AtivoBolsa ADD CONSTRAINT Estrategia_AtivoBolsa_FK1 FOREIGN KEY(IdEstrategia)
REFERENCES Estrategia (IdEstrategia)
ON DELETE SET NULL
GO

update AtivoBolsa set IdEstrategia = 1
go

alter table AtivoBMF add IdEstrategia int null
go
ALTER TABLE AtivoBMF ADD CONSTRAINT Estrategia_AtivoBMF_FK1 FOREIGN KEY(IdEstrategia)
REFERENCES Estrategia (IdEstrategia)
ON DELETE SET NULL
GO

alter table TituloRendaFixa add IdEstrategia int null
go
ALTER TABLE TituloRendaFixa ADD CONSTRAINT Estrategia_TituloRendaFixa_FK1 FOREIGN KEY(IdEstrategia)
REFERENCES Estrategia (IdEstrategia)
ON DELETE SET NULL
GO

update TituloRendaFixa set IdEstrategia = 2
go

alter table Carteira add IdEstrategia int null
go
ALTER TABLE Carteira ADD CONSTRAINT Estrategia_Carteira_FK1 FOREIGN KEY(IdEstrategia)
REFERENCES Estrategia (IdEstrategia)
ON DELETE SET NULL
GO

update Carteira set IdEstrategia = 2 where tipocarteira = 1
update Carteira set IdEstrategia = 1 where tipocarteira = 2


