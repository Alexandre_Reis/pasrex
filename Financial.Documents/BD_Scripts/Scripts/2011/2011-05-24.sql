--NAO ESQUECER DE MATAR O IDENTITY DA TABELA OFFICER!!!!

alter table usuario add IdCliente int null
go

ALTER TABLE usuario WITH CHECK ADD CONSTRAINT Cliente_Usuario_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
go

alter table cliente add IdOfficer int null
go

ALTER TABLE cliente WITH CHECK ADD CONSTRAINT Officer_Cliente_FK1 FOREIGN KEY(IdOfficer)
REFERENCES Officer (IdOfficer)
go


ALTER TABLE Cliente WITH CHECK ADD  CONSTRAINT Pessoa_Officer_FK1 FOREIGN KEY(IdOfficer)
REFERENCES Pessoa (IdPessoa)

--Acesso a TabelaOfficerCliente
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,9045,'S','S','S','S')