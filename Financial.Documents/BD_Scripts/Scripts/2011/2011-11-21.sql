create table IncorporacaoFundo(
	IdCarteiraOrigem int not null,  
	IdCarteiraDestino int not null,
	Data datetime not null,
CONSTRAINT IncorporacaoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdCarteiraOrigem ASC,  
	IdCarteiraDestino ASC,
	Data ASC
)
)