CREATE TABLE [dbo].[ExtratoFundoYMF](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCotista] [int] NULL,
	[IdCarteira] [int] NULL,
	[DataHistorico] [datetime] NULL,
	[Historico] [varchar](max) NULL,
	[DataOperacao] [datetime] NULL,
	[DataConversao] [datetime] NULL,
	[ValorAplicacao] [decimal](16, 2) NULL,
	[QuantidadeCotas] [decimal](28, 12) NULL,
	[DataCota] [datetime] NULL,
	[ValorCota] [decimal](28, 12) NULL,
	[ValorBruto] [decimal](16, 2) NULL,
	[ValorIR] [decimal](16, 2) NULL,
	[ValorIOF] [decimal](16, 2) NULL,
	[ValorLiquido] [decimal](16, 2) NULL,
	[IdNota] [int] NULL,
	[ValorRendimento] [decimal](16, 2) NULL,
 CONSTRAINT [PK_ExtratoFundoYMF] PRIMARY KEY CLUSTERED 
 (
	[Id] ASC
 )
)
go

