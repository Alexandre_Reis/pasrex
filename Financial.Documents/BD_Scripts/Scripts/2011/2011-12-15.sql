alter table operacaorendafixa add TaxaNegociacao decimal (16, 2) null
go
update operacaorendafixa set TaxaNegociacao = 0
go
alter table operacaorendafixa alter column TaxaNegociacao decimal (16, 2) not null
go

ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_TaxaNegociacao DEFAULT ((0)) FOR TaxaNegociacao
go


alter table posicaorendafixa add TaxaCustodia decimal (16, 2) null
go
update posicaorendafixa set TaxaCustodia = 0
go
alter table posicaorendafixa alter column TaxaCustodia decimal (16, 2) not null
go

ALTER TABLE posicaorendafixa ADD  CONSTRAINT DF_PosicaoRendaFixa_TaxaCustodia DEFAULT ((0)) FOR TaxaCustodia
go

alter table PosicaoRendaFixaAbertura add TaxaCustodia decimal (16, 2) null
go
update PosicaoRendaFixaAbertura set TaxaCustodia = 0
go
alter table PosicaoRendaFixaAbertura alter column TaxaCustodia decimal (16, 2) not null
go

ALTER TABLE PosicaoRendaFixaAbertura ADD  CONSTRAINT DF_PosicaoRendaFixaAbertura_TaxaCustodia DEFAULT ((0)) FOR TaxaCustodia
go

alter table PosicaoRendaFixaHistorico add TaxaCustodia decimal (16, 2) null
go
update PosicaoRendaFixaHistorico set TaxaCustodia = 0
go
alter table PosicaoRendaFixaHistorico alter column TaxaCustodia decimal (16, 2) not null
go

ALTER TABLE PosicaoRendaFixaHistorico ADD  CONSTRAINT DF_PosicaoRendaFixaHistorico_TaxaCustodia DEFAULT ((0)) FOR TaxaCustodia
go