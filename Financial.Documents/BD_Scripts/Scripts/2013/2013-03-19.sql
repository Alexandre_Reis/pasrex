﻿declare @data_versao char(10)
set @data_versao = '2013-03-19'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Acesso à nova entrada para Reset de status IsProcessando
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14580,'N','S','S','S' from grupousuario where idgrupo <> 0
go

alter table liquidacaoswap add ValorIR decimal(16, 2) null
go
update liquidacaoswap set ValorIR = 0
go
alter table liquidacaoswap alter column ValorIR decimal(16, 2) not null
go

ALTER TABLE LiquidacaoSwap ADD CONSTRAINT DF_LiquidacaoSwap_ValorIR DEFAULT ((0)) FOR ValorIR
GO

alter table cotista add TipoTributacao tinyint null
go
update cotista set TipoTributacao = 1 --Residentes
go
alter table cotista alter column TipoTributacao tinyint not null
go