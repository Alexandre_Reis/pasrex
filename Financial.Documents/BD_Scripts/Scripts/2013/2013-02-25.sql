﻿declare @data_versao char(10)
set @data_versao = '2013-02-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE GrupoUsuario add CarteiraSimulada char(1) null
go
update GrupoUsuario set CarteiraSimulada = 'N'
go
ALTER TABLE GrupoUsuario alter column CarteiraSimulada char(1) not null
go

--Acesso à nova entrada para FDesk
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,30000,'N','S','S','S' from grupousuario where idgrupo <> 0
go
