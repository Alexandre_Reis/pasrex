declare @data_versao char(10)
set @data_versao = '2013-05-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table incorporacaofundo add Taxa decimal (25,18) null
go

--Ajustado retorna os fluxos de modo a nao gerar impacto na cota 'careca'
insert into configuracao values (107, 'Retorno FDIC Ajustado', null, 'N')
go


