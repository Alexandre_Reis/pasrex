declare @data_versao char(10)
set @data_versao = '2013-04-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)



CREATE TABLE [dbo].[EvolucaoCapitalSocial](
	[CdAtivoBolsa] [varchar](20) NOT NULL,
	[Data] [datetime] NOT NULL,
	[Quantidade] [decimal](18, 2) NOT NULL,
	[Valor] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_EvolucaoCapitalSocial] PRIMARY KEY CLUSTERED 
(
	[CdAtivoBolsa] ASC,
	[Data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EvolucaoCapitalSocial]  WITH CHECK ADD  CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa] FOREIGN KEY([CdAtivoBolsa])
REFERENCES [dbo].[AtivoBolsa] ([CdAtivoBolsa])
GO

ALTER TABLE [dbo].[EvolucaoCapitalSocial] CHECK CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa]
GO


