declare @data_versao char(10)
set @data_versao = '2013-05-31'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

INSERT INTO [dbo].configuracao VALUES (4010, 'ObservacaoCotista', null, null)

alter table operacaofundo add CotaInformada decimal (28, 12) null

