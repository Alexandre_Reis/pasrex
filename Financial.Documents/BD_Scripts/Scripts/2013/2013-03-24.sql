declare @data_versao char(10)
set @data_versao = '2013-03-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)



alter table agendafundo add Taxa decimal (30,10)
go

update agendafundo set Taxa = Valor
go
update agendafundo set Valor = 0
go

alter table proventobolsacliente add IdAgenteMercado int null
go