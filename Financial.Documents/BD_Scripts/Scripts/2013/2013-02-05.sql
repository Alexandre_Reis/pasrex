﻿CREATE TABLE [dbo].[VersaoSchema](
	[DataVersao] [char](10) NOT NULL,
 CONSTRAINT [PK_VersaoSchema] PRIMARY KEY CLUSTERED 
(
	[DataVersao] ASC
)
)

-- O código abaixo deve ser inserido em todos os novos scripts, atualizando-se a data_versao:
declare @data_versao char(10)
set @data_versao = '2013-02-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return

--Inserir aqui scripts a serem executados
ALTER TABLE agentemercado ALTER COLUMN Telefone varchar(9) null

ALTER TABLE agentemercado ALTER COLUMN Fax varchar(9) null

alter table agentemercado add DDD char(2) null

alter table agentemercado add Ramal varchar(6) null

alter table agentemercado add Email varchar(50) null

alter table agentemercado add CPFResponsavel varchar(11) null

alter table agentemercado add CPFResponsavel2 varchar(11) null

alter table agentemercado add NomeResponsavel varchar(60) null


--Ao final sempre atualizar a tabela de VersaoSchema com o script abaixo
insert into VersaoSchema VALUES(@data_versao)


