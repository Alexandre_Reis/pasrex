declare @data_versao char(10)
set @data_versao = '2013-03-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table agendafundo alter column Taxa decimal (30,20) null
go

alter table agendafundo alter column Valor decimal (30,20) null
go

