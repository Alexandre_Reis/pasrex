declare @data_versao char(10)
set @data_versao = '2013-04-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table posicaocotista add DataUltimoCortePfee datetime null
go
alter table posicaocotistahistorico add DataUltimoCortePfee datetime null
go
alter table posicaocotistaabertura add DataUltimoCortePfee datetime null
go

alter table posicaofundo add DataUltimoCortePfee datetime null
go
alter table posicaofundohistorico add DataUltimoCortePfee datetime null
go
alter table posicaofundoabertura add DataUltimoCortePfee datetime null
go

update posicaocotista set DataUltimoCortePfee = c.DataUltimoCorte
from posicaocotista p, calculoperformance c
where p.idcarteira = c.idcarteira
go
update posicaocotistahistorico set DataUltimoCortePfee = c.DataUltimoCorte
from posicaocotistahistorico p, calculoperformancehistorico c
where p.idcarteira = c.idcarteira
and p.datahistorico = c.datahistorico
go
update posicaocotistaabertura set DataUltimoCortePfee = c.DataUltimoCorte
from posicaocotistaabertura p, calculoperformancehistorico c
where p.idcarteira = c.idcarteira
and p.datahistorico = c.datahistorico
go

--ALTER TABLE calculoperformance DROP COLUMN dataultimocorte
--ALTER TABLE calculoperformancehistorico DROP COLUMN dataultimocorte

ALTER TABLE CalculoPerformance ALTER COLUMN dataultimocorte datetime NULL
ALTER TABLE CalculoPerformanceHistorico ALTER COLUMN dataultimocorte datetime NULL

go