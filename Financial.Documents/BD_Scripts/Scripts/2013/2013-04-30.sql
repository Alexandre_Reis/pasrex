﻿declare @data_versao char(10)
set @data_versao = '2013-04-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

update tabelataxaadministracao set ideventoprovisao = c.idevento
from tabelataxaadministracao t, contabroteiro c
where c.descricao = 'PROVISÃO - TAXA ADMINISTRAÇÃO'
go
update tabelataxaadministracao set ideventopagamento = c.idevento
from tabelataxaadministracao t, contabroteiro c
where c.descricao = 'PAGTO - TAXA ADMINISTRAÇÃO'
go

update tabelataxaperformance set ideventoprovisao = c.idevento
from tabelataxaperformance t, contabroteiro c
where c.descricao = 'PROVISÃO - TAXA PERFORMANCE'
go
update tabelataxaperformance set ideventopagamento = c.idevento
from tabelataxaperformance t, contabroteiro c
where c.descricao = 'PAGTO - TAXA PERFORMANCE'
go


