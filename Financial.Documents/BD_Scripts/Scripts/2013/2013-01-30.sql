﻿CREATE TABLE [TabelaInformeDesempenho](
	[IdCarteira] [int] NOT NULL,
	[Data] [datetime] NOT NULL,
	[ValorAdmAdministrador] [decimal](16, 2) NOT NULL,
	[ValorPfeeAdministrador] [decimal](16, 2) NOT NULL,
	[ValorOutrasAdministrador] [decimal](16, 2) NOT NULL,
	[ValorAdmGestor] [decimal](16, 2) NOT NULL,
	[ValorPfeeGestor] [decimal](16, 2) NOT NULL,
	[ValorOutrasGestor] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_TabelaInformeDesempenho] PRIMARY KEY CLUSTERED 
(
	[IdCarteira] ASC,
	[Data] ASC
)
)
go

insert into Configuracao values(105, 'Usa Cota Inicial Posicao IR', NULL, 'N')
go

CREATE INDEX [IDX_IdCotista] ON PosicaoCotistaAbertura (IdCotista)

--Acesso à nova entrada para Interface de Informe de Desempenho
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14250,'N','S','S','S' from grupousuario where idgrupo <> 0
go