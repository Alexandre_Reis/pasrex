alter table cliente drop column CodigoYMF
go

alter table clienteinterface add CodigoYMF varchar(20) null
go

alter table pessoa add EstadoCivil tinyint null
go
alter table pessoa add NumeroRG varchar(20) null
go
alter table pessoa add EmissorRG varchar(20) null
go
alter table pessoa add DataEmissaoRG datetime null
go
alter table pessoa add Sexo char(1) null
go
alter table pessoa add DataNascimento datetime null
go
alter table pessoa add Profissao varchar(50) null
go
