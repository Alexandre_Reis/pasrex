insert into configuracao values (2060, 'Acesso Simultaneo', null, 'S')
go
alter table Usuario add DataLogout datetime null
go
update Usuario set DataLogout = dataultimologin
go
alter table Usuario alter column DataLogout datetime not null
go
