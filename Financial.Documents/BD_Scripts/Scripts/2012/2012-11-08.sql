﻿alter table cotista add TipoCotistaCVM int null
go

update cotista set TipoCotistaCVM = 2 --Pessoa Fisica Varejo
go

alter table ClienteRendaFixa add CodigoInterface varchar(12) null
go


--Acesso à nova entrada para Interfaces Importacao Custom
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14140,'N','N','N','N' from grupousuario where idgrupo <> 0
go

update permissaomenu set [PermissaoLeitura] = 'S'
where idmenu in (14140) and idgrupo = 0
go


CREATE TABLE HistoricoSenhas(
	IdUsuario int NOT NULL,
	Data datetime NOT NULL,
	Senha varchar(255) NOT NULL,
 CONSTRAINT PK_HistoricoSenhas PRIMARY KEY CLUSTERED 
(
	IdUsuario ASC,
	Data ASC,
	Senha ASC
)
)

GO

ALTER TABLE HistoricoSenhas  WITH CHECK ADD  CONSTRAINT FK_HistoricoSenhas_Usuario FOREIGN KEY(IdUsuario)
REFERENCES Usuario (IdUsuario)
ON DELETE CASCADE
GO

