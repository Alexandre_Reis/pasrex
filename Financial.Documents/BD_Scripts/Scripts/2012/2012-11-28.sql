
CREATE TABLE [dbo].[TabelaStress](
	[IdAtivo] [varchar](10) NOT NULL,
	[DataReferencia] [datetime] NOT NULL,
	[TipoAtivo] [tinyint] NOT NULL,
	[Stress] [decimal](12, 6) NOT NULL,
 CONSTRAINT [PK_TabelaStress] PRIMARY KEY CLUSTERED 
(
	[IdAtivo] ASC,
	[DataReferencia] ASC,
	[TipoAtivo] ASC
)
)