﻿--Acesso à nova entrada para Tabela de Reclamacoes de Cotistas
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14220,'S','S','S','S' from grupousuario where idgrupo <> 0
go

CREATE TABLE TabelaReclamacoesCotistas(
	Data datetime NOT NULL,
	Procedentes int NOT NULL,
	Improcedentes int NOT NULL,
	Resolvidas int NOT NULL,
	NaoResolvidas int NOT NULL,
	Administracao int NOT NULL,
	Gestao int NOT NULL,
	Taxas int NOT NULL,
	PoliticaInvestimento int NOT NULL,
	Aportes int NOT NULL,
	Resgates int NOT NULL,
	FornecimentoInformacoes int NOT NULL,
	AssembleiasGerais int NOT NULL,
	Cotas int NOT NULL,
	Outros int NOT NULL,
 CONSTRAINT PK_TabelaReclamacoesCotistas PRIMARY KEY CLUSTERED 
(
	Data ASC
)
)
GO




