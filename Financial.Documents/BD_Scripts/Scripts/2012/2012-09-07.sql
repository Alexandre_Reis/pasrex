alter table papelrendafixa add PagamentoJuros tinyint null
go
update papelrendafixa set PagamentoJuros = 1
go
alter table papelrendafixa alter column PagamentoJuros tinyint not null
go