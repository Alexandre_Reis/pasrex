drop table ExtratoFundoYMF
go

alter table operacaofundo add Fonte tinyint null
go
update operacaofundo set fonte = 2
go
alter table operacaofundo alter column Fonte tinyint not null
go