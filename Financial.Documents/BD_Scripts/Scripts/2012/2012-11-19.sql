alter table emissor add IdAgente int null
go
ALTER TABLE Emissor  WITH CHECK ADD  CONSTRAINT FK_Emissor_AgenteMercado FOREIGN KEY(IdAgente)
REFERENCES AgenteMercado (IdAgente)
ON DELETE SET NULL
GO