ALTER TABLE PosicaoEmprestimoBolsa add ValorDiarioJuros decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsa add ValorDiarioComissao decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsa add ValorDiarioCBLC decimal(16, 2) null
go
update PosicaoEmprestimoBolsa set ValorDiarioJUros = 0
go
update PosicaoEmprestimoBolsa set ValorDiarioComissao = 0
go
update PosicaoEmprestimoBolsa set ValorDiarioCBLC = 0
go
ALTER TABLE PosicaoEmprestimoBolsa alter column ValorDiarioJuros decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsa alter column ValorDiarioComissao decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsa alter column ValorDiarioCBLC decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsa ADD  CONSTRAINT DF_PosicaoEmprestimoBolsa_ValorDiarioJuros DEFAULT ((0)) FOR ValorDiarioJuros
GO
ALTER TABLE PosicaoEmprestimoBolsa ADD  CONSTRAINT DF_PosicaoEmprestimoBolsa_ValorDiarioComissao DEFAULT ((0)) FOR ValorDiarioComissao
GO
ALTER TABLE PosicaoEmprestimoBolsa ADD  CONSTRAINT DF_PosicaoEmprestimoBolsa_ValorDiarioCBLC  DEFAULT ((0)) FOR ValorDiarioCBLC
GO

ALTER TABLE PosicaoEmprestimoBolsaHistorico add ValorDiarioJuros decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico add ValorDiarioComissao decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico add ValorDiarioCBLC decimal(16, 2) null
go
update PosicaoEmprestimoBolsaHistorico set ValorDiarioJUros = 0
go
update PosicaoEmprestimoBolsaHistorico set ValorDiarioComissao = 0
go
update PosicaoEmprestimoBolsaHistorico set ValorDiarioCBLC = 0
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico alter column ValorDiarioJuros decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico alter column ValorDiarioComissao decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico alter column ValorDiarioCBLC decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaHistorico ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaHistorico_ValorDiarioJuros DEFAULT ((0)) FOR ValorDiarioJuros
GO
ALTER TABLE PosicaoEmprestimoBolsaHistorico ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaHistorico_ValorDiarioComissao DEFAULT ((0)) FOR ValorDiarioComissao
GO
ALTER TABLE PosicaoEmprestimoBolsaHistorico ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaHistorico_ValorDiarioCBLC  DEFAULT ((0)) FOR ValorDiarioCBLC
GO

ALTER TABLE PosicaoEmprestimoBolsaAbertura add ValorDiarioJuros decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura add ValorDiarioComissao decimal(16, 2) null
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura add ValorDiarioCBLC decimal(16, 2) null
go
update PosicaoEmprestimoBolsaAbertura set ValorDiarioJUros = 0
go
update PosicaoEmprestimoBolsaAbertura set ValorDiarioComissao = 0
go
update PosicaoEmprestimoBolsaAbertura set ValorDiarioCBLC = 0
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura alter column ValorDiarioJuros decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura alter column ValorDiarioComissao decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura alter column ValorDiarioCBLC decimal(16, 2) not null
go
ALTER TABLE PosicaoEmprestimoBolsaAbertura ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaAbertura_ValorDiarioJuros DEFAULT ((0)) FOR ValorDiarioJuros
GO
ALTER TABLE PosicaoEmprestimoBolsaAbertura ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaAbertura_ValorDiarioComissao DEFAULT ((0)) FOR ValorDiarioComissao
GO
ALTER TABLE PosicaoEmprestimoBolsaAbertura ADD  CONSTRAINT DF_PosicaoEmprestimoBolsaAbertura_ValorDiarioCBLC  DEFAULT ((0)) FOR ValorDiarioCBLC
GO