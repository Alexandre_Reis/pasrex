alter table ContabLancamento add Fonte tinyint null
go

update ContabLancamento set Fonte = 1
go

alter table ContabLancamento alter column Fonte tinyint not null
go