CREATE TABLE TabelaPerfilMensalCVM
(
	Data datetime NOT NULL,
	IdCarteira int NOT NULL,
	Secao3 varchar(4000) NOT NULL,
	Secao4 varchar(4000) NOT NULL,
	Secao5 decimal(16, 4) NULL,
	Secao6 tinyint NULL,
	Secao7 decimal(16, 4) NOT NULL,
	Secao8 varchar(4000) NOT NULL,
	Secao11_PercentCota decimal(24, 12) NULL,
	Secao11_Fator1 varchar(150) NOT NULL,
	Secao11_Cenario1 varchar(150) NOT NULL,
	Secao11_Fator2 varchar(150) NOT NULL,
	Secao11_Cenario2 varchar(150) NOT NULL,
	Secao11_Fator3 varchar(150) NOT NULL,
	Secao11_Cenario3 varchar(150) NOT NULL,
	Secao11_Fator4 varchar(150) NOT NULL,
	Secao11_Cenario4 varchar(150) NOT NULL,
	Secao11_Fator5 varchar(150) NOT NULL,
	Secao11_Cenario5 varchar(150) NOT NULL,
	Secao12 decimal(8, 2) NULL,
	Secao13 decimal(8, 2) NULL,
	Secao14 decimal(8, 2) NULL,
	Secao15 decimal(8, 2) NULL,
	Secao16_Fator varchar(150) NOT NULL,
	Secao16_Variacao decimal(8, 2) NULL,
 CONSTRAINT PK_TabelaPerfilMensalCVM PRIMARY KEY CLUSTERED 
(
	Data ASC,
	IdCarteira ASC
)
)
GO

ALTER TABLE dbo.TabelaPerfilMensalCVM  WITH CHECK ADD  CONSTRAINT FK_TabelaPerfilMensalCVM_Carteira FOREIGN KEY(IdCarteira)
REFERENCES dbo.Carteira (IdCarteira)
ON DELETE CASCADE
GO