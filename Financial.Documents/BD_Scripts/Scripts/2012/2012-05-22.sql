﻿CREATE TABLE LiquidacaoFuturo
(
	IdLiquidacao int NOT NULL,
	DataHistorico datetime NOT NULL,
	IdCliente int NOT NULL,
	DataLancamento datetime NOT NULL,
	DataVencimento datetime NOT NULL,
	Descricao varchar(255) NOT NULL,
	Valor decimal(16, 2) NOT NULL,
	Situacao tinyint NOT NULL,
	Origem int NOT NULL,
	Fonte tinyint NOT NULL,
	IdAgente int NULL,
	IdentificadorOrigem int NULL,
	IdConta int NOT NULL,
 CONSTRAINT LiquidacaoFuturo_PK PRIMARY KEY CLUSTERED 
(
	IdLiquidacao ASC,
	DataHistorico ASC
)
)
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE LiquidacaoFuturo  WITH CHECK ADD  CONSTRAINT AgenteMercado_LiquidacaoFuturo_FK1 FOREIGN KEY(IdAgente)
REFERENCES AgenteMercado (IdAgente)
ON DELETE CASCADE
GO

ALTER TABLE LiquidacaoFuturo CHECK CONSTRAINT AgenteMercado_LiquidacaoFuturo_FK1
GO

ALTER TABLE LiquidacaoFuturo  WITH CHECK ADD  CONSTRAINT Cliente_LiquidacaoFuturo_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO

ALTER TABLE LiquidacaoFuturo CHECK CONSTRAINT Cliente_LiquidacaoFuturo_FK1
GO

--Acesso à nova entrada de relatorio de analise de resultados
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,15230,'S','S','S','S' from grupousuario
go


