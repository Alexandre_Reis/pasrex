alter table perfilcorretagembolsa add ISS decimal (8,4) null
go
update perfilcorretagembolsa set ISS = 0
go
alter table perfilcorretagembolsa alter column ISS decimal (8,4) not null
go