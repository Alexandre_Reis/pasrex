﻿--Acesso à nova entrada para Memoria de calculo e Simulação de cálculo de RF
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,3832,'N','N','N','N' from grupousuario where idgrupo <> 0
go
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,3834,'N','N','N','N' from grupousuario where idgrupo <> 0
go

--Acesso à nova entrada para Nr Distribuição Bolsa
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,2225,'N','N','N','N' from grupousuario where idgrupo <> 0
go

--Acesso à nova entrada para Interfaces Custom
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14130,'N','N','N','N' from grupousuario where idgrupo <> 0
go

update permissaomenu set [PermissaoLeitura] = 'S'
where idmenu in (3832, 3834, 2225, 14130) and idgrupo = 0
go

CREATE TABLE DistribuicaoBolsa
(
	DataReferencia datetime NOT NULL,
	CdAtivoBolsa varchar(20) NOT NULL,
	Distribuicao int NOT NULL,
 CONSTRAINT DistribuicaoBolsa_PK PRIMARY KEY NONCLUSTERED 
(
	DataReferencia ASC,
	CdAtivoBolsa ASC
)
)

ALTER TABLE DistribuicaoBolsa  WITH CHECK ADD  CONSTRAINT AtivoBolsa_DistribuicaoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
REFERENCES AtivoBolsa (CdAtivoBolsa)
ON DELETE CASCADE
GO