﻿CREATE TABLE TabelaExpurgoAdministracao(
	DataReferencia datetime NOT NULL,
	IdTabela int NOT NULL,
	Tipo int NOT NULL,
	Codigo varchar(14) NOT NULL,
 CONSTRAINT PK_TabelaExpurgoAdministracao PRIMARY KEY CLUSTERED 
(
	DataReferencia ASC,
	IdTabela ASC,
	Tipo ASC,
	Codigo ASC
)
)
GO

ALTER TABLE TabelaExpurgoAdministracao  WITH CHECK ADD  CONSTRAINT FK_TabelaExpurgoAdministracao_TabelaTaxaAdministracao FOREIGN KEY(IdTabela)
REFERENCES TabelaTaxaAdministracao (IdTabela)
ON DELETE CASCADE
GO

delete from tabelacustosbolsa where idtabela = 70
go
INSERT INTO tabelacustosbolsa ([DataReferencia],[IdTabela],[Descricao],[PercentualEmolumentoBolsa],[PercentualRegistroBolsa],[PercentualLiquidacaoCBLC],[PercentualRegistroCBLC],[MinimoBovespa],[MinimoCBLC])VALUES('Jan  1 1900 12:00:00:000AM',70,'Soma (Balcão)',0.068,0.0000,0.006,0.0000,NULL,NULL)
go
delete from tabelacustosbolsa where idtabela = 200
go
