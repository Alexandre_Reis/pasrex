set identity_insert [dbo].grupousuario on
insert into [dbo].grupousuario (IdGrupo, Descricao, TipoPerfil, Front) values (0, 'AdminMaster', 1, 1)
set identity_insert [dbo].grupousuario off
go

alter table LiquidacaoRendaFixa add ValorBruto decimal (16, 2) CONSTRAINT DF_LiquidacaoRendaFixa_1 DEFAULT 0 not null
go
alter table LiquidacaoRendaFixa add ValorLiquido decimal (16, 2) CONSTRAINT DF_LiquidacaoRendaFixa_2 DEFAULT 0 not null
go
alter table LiquidacaoRendaFixa add PULiquidacao decimal (25, 12) CONSTRAINT DF_LiquidacaoRendaFixa_3 DEFAULT 0 not null
go

update LiquidacaoRendaFixa set valorbruto = rendimento 
go
update LiquidacaoRendaFixa set valorliquido = valorbruto - (valorir + valoriof)
go
update LiquidacaoRendaFixa set rendimento = 0
where tipolancamento = 1
go




