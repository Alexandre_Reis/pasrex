insert into configuracao values (104, 'Retorno Benchmark', 2, null)
go

alter table OperacaoSwap add IdEstrategia int null
go
ALTER TABLE OperacaoSwap  WITH CHECK 
ADD  CONSTRAINT Estrategia_OperacaoSwap_FK1 FOREIGN KEY(IdEstrategia)
REFERENCES Estrategia (IdEstrategia)
ON DELETE SET NULL
GO