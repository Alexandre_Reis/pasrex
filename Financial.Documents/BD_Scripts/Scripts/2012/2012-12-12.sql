update ativobmf set codigoisin = left(codigoisin, 12)
go
update ativobolsa set codigoisin = left(codigoisin, 12)
go
alter table ativobolsa alter column CodigoIsin varchar(12) null
go
alter table ativobmf alter column CodigoIsin varchar(12) null
go
alter table titulorendafixa add CodigoIsin varchar(12) null
go


