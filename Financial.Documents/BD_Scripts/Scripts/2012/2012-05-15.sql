alter table book add EmailFrom varchar(100) null
go

update book set EmailFrom = ''
go

alter table book alter column EmailFrom varchar(100) not null
go