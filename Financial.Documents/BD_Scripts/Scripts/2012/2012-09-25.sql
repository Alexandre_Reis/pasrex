﻿--Acesso à nova entrada para Tabela de Pessoa x Distribuidor
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,1170,'S','S','S','S' from grupousuario where idgrupo <> 0
go

alter table Pessoa add IdAgenteDistribuidor int null
go

update pessoa set IdAgenteDistribuidor = c.IdAgenteDistribuidor
from pessoa p, cotista c
where p.idpessoa = c.idcotista

alter table Cotista drop constraint AgenteMercado_Cotista_FK1
go

alter table Cotista drop column IdAgenteDistribuidor
go

alter table CalculoRebateCotista drop constraint Cotista_CalculoRebateCotista_FK1
go

