﻿alter table clienteinterface add CodigoContabil varchar (12) null
go
alter table carteira add CompensacaoPrejuizo tinyint null
go
update carteira set CompensacaoPrejuizo = 1 --Compensação padrão
go
alter table carteira alter column CompensacaoPrejuizo tinyint not null
go