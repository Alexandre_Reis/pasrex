CREATE TABLE TabelaFundoCategoria(
	IdCarteira int NOT NULL,
	IdLista int NOT NULL,
	IdCategoriaFundo int NOT NULL,
 CONSTRAINT PK_TabelaFundoCategoria PRIMARY KEY CLUSTERED 
(
	IdCarteira ASC,
	IdLista ASC
)
)
GO