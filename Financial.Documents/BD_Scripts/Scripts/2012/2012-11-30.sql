CREATE INDEX [IDX_IdCliente] ON PosicaoBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoBolsaAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoBolsaHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON PosicaoCotista (IdCarteira)
CREATE INDEX [IDX_IdCliente] ON PosicaoCotistaAbertura (IdCarteira)
CREATE INDEX [IDX_IdCliente] ON PosicaoCotistaHistorico (IdCarteira)

CREATE INDEX [IDX_IdCliente] ON PosicaoEmprestimoBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoEmprestimoBolsaAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoEmprestimoBolsaHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON PosicaoFundo (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoFundoAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoFundoHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON PosicaoRendaFixa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoRendaFixaAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoRendaFixaHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON PosicaoSwap (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoSwapAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoSwapHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON PosicaoTermoBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoTermoBolsaAbertura (IdCliente)
CREATE INDEX [IDX_IdCliente] ON PosicaoTermoBolsaHistorico (IdCliente)

CREATE INDEX [IDX_IdCliente] ON OperacaoBMF (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OperacaoBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OperacaoCotista (IdCarteira)
CREATE INDEX [IDX_IdCliente] ON OperacaoEmprestimoBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OperacaoFundo (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OperacaoRendaFixa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OperacaoSwap (IdCliente)

CREATE INDEX [IDX_IdCliente] ON OrdemBMF (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OrdemBolsa (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OrdemCotista (IdCarteira)
CREATE INDEX [IDX_IdCliente] ON OrdemFundo (IdCliente)
CREATE INDEX [IDX_IdCliente] ON OrdemRendaFixa (IdCliente)
