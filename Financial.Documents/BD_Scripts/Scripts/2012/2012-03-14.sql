alter table carteira add PrioridadeOperacao tinyint null
go

--Prioridade ResgateAntes = 1 
--Prioridade ResgateDepois = 2

update carteira set PrioridadeOperacao = 1
go

alter table carteira alter column PrioridadeOperacao tinyint not null
go