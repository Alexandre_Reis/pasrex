alter table ativobolsa add NumeroSerie int null
go
update ativobolsa set NumeroSerie = 0
go
alter table ativobolsa alter column NumeroSerie int not null
go

ALTER TABLE ativobolsa
            ADD CONSTRAINT DF_NumeroSerie
            DEFAULT 0 FOR NumeroSerie
go