Create Table ApuracaoIRImobiliario
(IdCliente int not null
,Ano int not null
,Mes tinyint not null
,ResultadoNormal decimal (16,2) not null
,ResultadoDT decimal (16,2) not null
,ResultadoLiquidoMes decimal (16,2) not null
,ResultadoNegativoAnterior decimal (16,2) not null
,BaseCalculo decimal (16,2) not null
,PrejuizoCompensar decimal (16,2) not null
,ImpostoCalculado decimal (16,2) not null
,ResultadoLiquidoMesDT decimal (16,2) not null
,ResultadoNegativoAnteriorDT decimal (16,2) not null
,BaseCalculoDT decimal (16,2) not null
,PrejuizoCompensarDT decimal (16,2) not null
,ImpostoCalculadoDT decimal (16,2) not null
,TotalImposto decimal (16,2) not null
,IRDayTradeMes decimal (16,2) not null
,IRDayTradeMesAnterior decimal (16,2) not null
,IRDayTradeCompensar decimal (16,2) not null
,IRFonteNormal decimal (16,2) not null
,ImpostoPagar decimal (16,2) not null
,ImpostoPago decimal (16,2) not null,
CONSTRAINT [PK_ApuracaoIRImobiliario] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC,
	[Ano] ASC,
	[Mes] ASC
)
)