alter table AtivoBMF add CodigoIsin varchar(50) null
go

update AtivoBMF set CodigoIsin = ''
go

alter table AtivoBMF alter column CodigoIsin varchar(50) not null
go

update AtivoBMF set CodigoIsin = 'BRBMEFWIN1C2'
where cdativobmf = 'WIN' and serie = 'V12'
go

update AtivoBMF set CodigoIsin = 'BRBMEFIBV1J2'
where cdativobmf = 'IND' and serie = 'V12'
go