﻿--Acesso à nova entrada de ajuste de operacao fundo
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,4860,'S','S','S','S' from grupousuario
where idgrupo <> 1
go

drop table DetalheResgateFundo
go

CREATE TABLE [DetalheResgateFundo](
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[PrejuizoUsado] [decimal](16, 2) NOT NULL,
	[RendimentoResgate] [decimal](16, 2) NOT NULL,
	[VariacaoResgate] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_DetalheResgateFundo] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC,
	[IdPosicaoResgatada] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [DetalheResgateFundo]  WITH CHECK ADD  CONSTRAINT [Carteira_DetalheResgateFundo_FK1] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
ON DELETE CASCADE
GO

ALTER TABLE [DetalheResgateFundo] CHECK CONSTRAINT [Carteira_DetalheResgateFundo_FK1]
GO

ALTER TABLE [DetalheResgateFundo]  WITH CHECK ADD  CONSTRAINT [Cliente_DetalheResgateFundo_FK1] FOREIGN KEY([IdCliente])
REFERENCES [Cliente] ([IdCliente])
GO

ALTER TABLE [DetalheResgateFundo] CHECK CONSTRAINT [Cliente_DetalheResgateFundo_FK1]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_ValorCPMF]  DEFAULT ((0)) FOR [ValorCPMF]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_ValorPerformance]  DEFAULT ((0)) FOR [ValorPerformance]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_PrejuizoUsado]  DEFAULT ((0)) FOR [PrejuizoUsado]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_RendimentoResgate]  DEFAULT ((0)) FOR [RendimentoResgate]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_VariacaoResgate]  DEFAULT ((0)) FOR [VariacaoResgate]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_ValorIR]  DEFAULT ((0)) FOR [ValorIR]
ALTER TABLE [DetalheResgateFundo] ADD  CONSTRAINT [DF_DetalheResgateCliente_ValorIOF]  DEFAULT ((0)) FOR [ValorIOF]