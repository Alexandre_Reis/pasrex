﻿alter table AtivoBolsa add DataInicioVigencia datetime null
go
alter table AtivoBolsa add DataFimVigencia datetime null
go
alter table AtivoBMF add DataInicioVigencia datetime null
go
alter table AtivoBMF add DataFimVigencia datetime null
go
alter table Carteira add DiasConfidencialidade int null
go

--Acesso à nova entrada para Tabela CDA
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,14240,'S','S','S','S' from grupousuario where idgrupo <> 0
go