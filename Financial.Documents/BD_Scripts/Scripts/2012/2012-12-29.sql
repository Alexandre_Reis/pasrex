alter table tabelarebateofficer add IdPessoa int null
go
update tabelarebateofficer set idpessoa = idcarteira
go
alter table tabelarebateofficer alter column IdPessoa int not null
go

alter table tabelarebateofficer drop constraint Carteira_TabelaRebateOfficer_FK1
go
alter table tabelarebateofficer drop constraint TabelaRebateOfficer_PK
go
alter table tabelarebateofficer drop column IdCarteira
go

ALTER TABLE TabelaRebateOfficer  WITH CHECK ADD  CONSTRAINT Pessoa_TabelaRebateOfficer_FK1 FOREIGN KEY(IdPessoa)
REFERENCES Pessoa (IdPessoa)
ON DELETE CASCADE
GO

ALTER TABLE TabelaRebateOfficer ADD  CONSTRAINT TabelaRebateOfficer_PK PRIMARY KEY CLUSTERED 
(
	DataReferencia ASC,
	IdPessoa ASC,
	IdOfficer ASC
)
GO