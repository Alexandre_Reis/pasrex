﻿alter table ativobolsa add TipoPapel tinyint null
go
update ativobolsa set TipoPapel = 1 -- Normal
go
alter table ativobolsa alter column TipoPapel tinyint not null
go

-- 1=Normal, 2=Bonus de subscrição, 3=Recibo de subscrição, 4=ETF
