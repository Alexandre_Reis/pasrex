CREATE TABLE [TabelaCargaPassivo](
	[IdTabela] [int] IDENTITY(1,1) NOT NULL,
	[Data] [datetime] NOT NULL,
	[IdCarteira] varchar(20) NOT NULL,
	[IdCotista] varchar(20) NOT NULL,
	[Tipo] [tinyint] NOT NULL,
	[IdNotaOperacao] [int] NULL,
	[IdNotaResgatada] [int] NULL,
	[DataAplicacao] datetime null,
	[ValorCota] [decimal](28, 12) NOT NULL,
	[QuantidadeCotas] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_TabelaCargaPassivo] PRIMARY KEY CLUSTERED 
(
	[Idtabela] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
