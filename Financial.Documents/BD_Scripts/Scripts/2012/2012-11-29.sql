

CREATE TABLE [dbo].[TabelaRetornoEsperado](
	[IdAtivo] [varchar](10) NOT NULL,
	[DataReferencia] [datetime] NOT NULL,
	[TipoAtivo] [tinyint] NOT NULL,
	[Valor] [decimal](12, 6) NOT NULL,
 CONSTRAINT [PK_TabelaRetornoEsperado] PRIMARY KEY CLUSTERED 
(
	[IdAtivo] ASC,
	[DataReferencia] ASC,
	[TipoAtivo] ASC
)
)