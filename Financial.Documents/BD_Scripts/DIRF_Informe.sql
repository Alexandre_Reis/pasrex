﻿use FIN_ATIVA
go

--TipoControleCliente = 4 (Cotista), 5 (Completo)
--TipoCliente = 2 (Clube), 500 (Fundo)

--CONSULTA DE SALDOS HISTORICOS NOS 2 ANOS
select Year(datahistorico) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   p.DataHistorico as DataSaldo, sum(p.Quantidade) as QuantidadeCotas, sum(p.Quantidade * p.CotaAplicacao) as SaldoFinanceiro
from Carteira c, ClienteInterface l, Cotista o, PosicaoCotistaHistorico p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataHistorico in ('31/dec/2013', '31/dec/2014')
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
group by i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome, p.DataHistorico
order by p.DataHistorico asc, i.IdTipo, c.IdCarteira, o.IdCotista

--CONSULTA DE RENDIMENTOS LIQUIDOS NO ANO (COM CONVERSÃO FEITA NO ANO)
select Year(p.DataOperacao) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   sum(p.RendimentoResgate - p. ValorIR) as RendimentoLiquido
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataOperacao between '01/jan/2014' and '31/dec/2014'
and p.DataConversao between '01/jan/2014' and '31/dec/2014'
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao not in (1, 10, 11, 12, 100, 101)
group by Year(p.DataOperacao), i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome
order by i.IdTipo, c.IdCarteira, o.IdCotista

--CONSULTA DE APLICACOES EM TRANSITO 2014
select Year(p.DataOperacao) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   sum(p.ValorBruto) as ValorAplicacao
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataOperacao < '01/jan/2014'
and p.DataConversao >= '01/jan/2014'
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao = 1
group by Year(p.DataOperacao), i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome
order by i.IdTipo, c.IdCarteira, o.IdCotista

--CONSULTA DE APLICACOES EM TRANSITO 2015
select Year(p.DataOperacao) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   sum(p.ValorBruto) as ValorAplicacao
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataOperacao < '01/jan/2015'
and p.DataConversao >= '01/jan/2015'
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao = 1
group by Year(p.DataOperacao), i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome
order by i.IdTipo, c.IdCarteira, o.IdCotista

--CONSULTA DE RESGATES EM TRANSITO 2014
select Year(p.DataConversao) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   sum(p.ValorLiquido) as ValorResgate
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataConversao < '01/jan/2014'
and p.DataLiquidacao >= '01/jan/2014'
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao IN (2, 3, 4, 5, 20)
group by Year(p.DataConversao), i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome
order by i.IdTipo, c.IdCarteira, o.IdCotista

--CONSULTA DE RESGATES EM TRANSITO 2015
select Year(p.DataConversao) as Ano, case i.IdTipo when 200 then 'Clube' else 'Fundo' end as Tipo,  c.idcarteira as IdFundo, l.codigoYMF as CodFundo, c.nome as NomeFundo,
	   o.idcotista as IdCotista, o.CodigoInterface as CodCotista, o.nome as NomeCotista,
	   sum(p.ValorLiquido) as ValorResgate
from Carteira c, ClienteInterface l, Cotista o, OperacaoCotista p, Cliente i
where c.IdCarteira = p.IdCarteira
and p.IdCarteira = l.IdCliente
and p.IdCotista = o.IdCotista
and p.IdCarteira = i.IdCliente
and p.DataConversao < '01/jan/2015'
and p.DataLiquidacao >= '01/jan/2015'
and i.TipoControle in (4, 5) and i.IdTipo in (200, 500)
and p.TipoOperacao IN (2, 3, 4, 5, 20)
group by Year(p.DataConversao), i.IdTipo, c.idcarteira, l.codigoYMF, c.nome, o.idcotista, o.CodigoInterface, o.nome
order by i.IdTipo, c.IdCarteira, o.IdCotista