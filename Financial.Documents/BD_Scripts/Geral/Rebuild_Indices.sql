ALTER INDEX TabelaCustosBMF_PK ON TabelaCustosBMF REBUILD 
go

ALTER INDEX TabelaVencimentosBMF_PK ON TabelaVencimentosBMF REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoFundoHistorico REBUILD 
go

ALTER INDEX PosicaoCotistaHistorico_PK ON PosicaoCotistaHistorico REBUILD 
go

ALTER INDEX PosicaoRendaFixaAbertura_PK ON PosicaoRendaFixaAbertura REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoCotistaAbertura REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoBolsaAbertura REBUILD 
go

ALTER INDEX PosicaoFundoHistorico_PK ON PosicaoFundoHistorico REBUILD 
go

ALTER INDEX PosicaoFundoAbertura_PK ON PosicaoFundoAbertura REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoFundoAbertura REBUILD 
go

ALTER INDEX HistoricoCota_PK ON HistoricoCota REBUILD 
go

ALTER INDEX PK_CotacaoMercadoAndima ON CotacaoMercadoAndima REBUILD 
go

ALTER INDEX AtivoBMF_PK ON AtivoBMF REBUILD 
go

ALTER INDEX TabelaCalculoBMF_PK ON TabelaCalculoBMF REBUILD 
go

ALTER INDEX PosicaoCotistaAbertura_PK ON PosicaoCotistaAbertura REBUILD 
go


ALTER INDEX IDX_IdCliente ON PosicaoCotistaHistorico REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoRendaFixaAbertura REBUILD 
go

ALTER INDEX IDX_IdCotista ON PosicaoCotistaAbertura REBUILD 
go


ALTER INDEX IDX_IdCliente ON OperacaoFundo REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoBolsaHistorico REBUILD 
go

ALTER INDEX DistribuicaoBolsa_PK ON DistribuicaoBolsa REBUILD 
go

ALTER INDEX PosicaoEmprestimoBolsaAbertura_PK ON PosicaoEmprestimoBolsaAbertura REBUILD 
go

ALTER INDEX IDX_IdCliente_DataConversao ON OperacaoFundo REBUILD 
go

ALTER INDEX PosicaoEmprestimoBolsaHistorico_PK ON PosicaoEmprestimoBolsaHistorico REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoRendaFixaHistorico REBUILD 
go

ALTER INDEX CotacaoBMF_PK ON CotacaoBMF REBUILD 
go

ALTER INDEX AtivoBolsa_PK ON AtivoBolsa REBUILD 
go

ALTER INDEX IDX_IdCliente ON OperacaoCotista REBUILD 
go

ALTER INDEX FatorCotacaoBolsa_PK ON FatorCotacaoBolsa REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoRendaFixaAbertura REBUILD 
go

ALTER INDEX SaldoCaixa_PK ON SaldoCaixa REBUILD 
go

ALTER INDEX PK_CotacaoMercadoDebenture ON CotacaoMercadoDebenture REBUILD 
go

ALTER INDEX CotacaoIndice_PK ON CotacaoIndice REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoFundoAbertura REBUILD 
go

ALTER INDEX IDX_DataHistorico_IdCliente ON PosicaoFundoHistorico REBUILD 
go

ALTER INDEX LiquidacaoAbertura_PK ON LiquidacaoAbertura REBUILD 
go


ALTER INDEX IDX_IdCliente ON PosicaoBolsaAbertura REBUILD 
go

ALTER INDEX PK_DetalheResgateFundo ON DetalheResgateFundo REBUILD 
go

ALTER INDEX PosicaoRendaFixa_PK ON PosicaoRendaFixa REBUILD 
go

ALTER INDEX CalculoAdministracaoHistorico_PK ON CalculoAdministracaoHistorico REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoBolsaHistorico REBUILD 
go

ALTER INDEX LiquidacaoHistorico_PK ON LiquidacaoHistorico REBUILD 
go

ALTER INDEX CotacaoBolsa_PK ON CotacaoBolsa REBUILD 
go

ALTER INDEX IDX_IdCliente ON PosicaoRendaFixaHistorico REBUILD 
go

ALTER INDEX PosicaoRendaFixaHistorico_PK ON PosicaoRendaFixaHistorico REBUILD 
go

ALTER INDEX PK_CotacaoResolucao238 ON CotacaoResolucao238 REBUILD 
go

ALTER INDEX PosicaoFundo_PK ON PosicaoFundo REBUILD 
go

ALTER INDEX ProventoBolsa_PK ON ProventoBolsa REBUILD 
go

ALTER INDEX OperacaoCotista_PK ON OperacaoCotista REBUILD 
go

ALTER INDEX PosicaoBolsaAbertura_PK ON PosicaoBolsaAbertura REBUILD 
go

ALTER INDEX PosicaoCotista_PK ON PosicaoCotista REBUILD 
go

ALTER INDEX PosicaoBolsaHistorico_PK ON PosicaoBolsaHistorico REBUILD 
go

ALTER INDEX Liquidacao_PK ON Liquidacao REBUILD 
go


ALTER INDEX PK_DetalheResgateCotista ON DetalheResgateCotista REBUILD 
go

ALTER INDEX CalculoProvisaoHistorico_PK ON CalculoProvisaoHistorico REBUILD 
go


