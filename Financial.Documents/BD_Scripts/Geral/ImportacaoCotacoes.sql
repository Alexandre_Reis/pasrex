delete from cotacaoindice
go

insert into cotacaoindice
(Data
,IdIndice
,Valor)
select * from fin_quadrante..cotacaoindice
where idindice in (select idindice from indice)
go


delete from cotacaobolsa
go

insert into cotacaobolsa
(
Data
,CdAtivoBolsa
,PUMedio
,PUFechamento
,PUAbertura
,PUGerencial)
select * from fin_quadrante..cotacaobolsa
where cdativobolsa in (select cdativobolsa from ativobolsa)
and data >= '01/01/2013'
go

insert into cotacaobmf
(Data
,CdAtivoBmf
,Serie
,PUFechamento
,PUMedio
,PUCorrigido
,PUGerencial)
select * from fin_quadrante..cotacaobmf
where (cdativobmf+serie) in (select (cdativobmf+serie) from ativobmf)
and data >= '01/01/2013'
go

insert into cotacaomercadoandima
(DataReferencia
,Descricao
,CodigoSELIC
,DataEmissao
,DataVencimento
,TaxaIndicativa
,PU)
select * from fin_quadrante..cotacaomercadoandima
where codigoselic+CONVERT(VARCHAR(10),datareferencia)+CONVERT(VARCHAR(10),datavencimento) not in (
select codigoselic+CONVERT(VARCHAR(10),datareferencia)+CONVERT(VARCHAR(10),datavencimento) from cotacaomercadoandima)




--Copy Emissor
SET IDENTITY_INSERT FIN_TENANT_DESTINO..emissor ON
insert into FIN_TENANT_DESTINO..emissor (IdEmissor, Nome, TipoEmissor, IdSetor, CNPJ, IdAgente)
select IdEmissor, Nome, TipoEmissor, IdSetor, CNPJ, IdAgente from FIN_TENANT_ORIGEM..emissor 
where idemissor not in (select idemissor from FIN_TENANT_DESTINO..emissor)
SET IDENTITY_INSERT FIN_TENANT_DESTINO..emissor OFF

--Copy Setor
SET IDENTITY_INSERT FIN_TENANT_DESTINO..setor ON
insert into FIN_TENANT_DESTINO..setor (IdSetor, Nome)
select IdSetor, Nome from FIN_TENANT_ORIGEM..setor
where idsetor not in (select idsetor from FIN_TENANT_DESTINO..setor)
SET IDENTITY_INSERT FIN_TENANT_DESTINO..setor OFF

--Copy AtivoBolsa
insert into FIN_TENANT_DESTINO..ativobolsa 
select * from FIN_TENANT_ORIGEM..ativobolsa

--Copy ProventoBolsa
sp_help proventobolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..proventobolsa ON
insert into FIN_TENANT_DESTINO..proventobolsa (IdProvento, Cdativobolsa, datalancamento, dataex, datapagamento, tipoprovento, valor)
select IdProvento, Cdativobolsa, datalancamento, dataex, datapagamento, tipoprovento, valor from FIN_TENANT_ORIGEM..proventobolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..proventobolsa OFF

--Copy  BonificacaoBolsa
sp_help bonificacaobolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..Bonificacaobolsa ON
insert into FIN_TENANT_DESTINO..Bonificacaobolsa (IdBonificacao,DataLancamento,DataEx,DataReferencia,TipoFracao,PUBonificacao,CdAtivoBolsa,Percentual,CdAtivoBolsaDestino,Fonte)
select IdBonificacao,DataLancamento,DataEx,DataReferencia,TipoFracao,PUBonificacao,CdAtivoBolsa,Percentual,CdAtivoBolsaDestino,Fonte from FIN_TENANT_ORIGEM..Bonificacaobolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..Bonificacaobolsa OFF

--Copy  ConversaoBolsa
sp_help ConversaoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..ConversaoBolsa ON
insert into FIN_TENANT_DESTINO..ConversaoBolsa (IdConversao,DataLancamento,DataEx,DataReferencia,CdAtivoBolsa,FatorQuantidade,FatorPU,CdAtivoBolsaDestino,Fonte)
select IdConversao,DataLancamento,DataEx,DataReferencia,CdAtivoBolsa,FatorQuantidade,FatorPU,CdAtivoBolsaDestino,Fonte from FIN_TENANT_ORIGEM..ConversaoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..ConversaoBolsa OFF

--Copy  SubscricaoBolsa
sp_help SubscricaoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..SubscricaoBolsa ON
insert into FIN_TENANT_DESTINO..SubscricaoBolsa (IdSubscricao,DataLancamento,DataEx,DataReferencia,PrecoSubscricao,Fator,CdAtivoBolsaDireito,CdAtivoBolsa,Fonte,DataFimNegociacao)
select IdSubscricao,DataLancamento,DataEx,DataReferencia,PrecoSubscricao,Fator,CdAtivoBolsaDireito,CdAtivoBolsa,Fonte,DataFimNegociacao from FIN_TENANT_ORIGEM..SubscricaoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..SubscricaoBolsa OFF


--Copy  GrupamentoBolsa
sp_help GrupamentoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..GrupamentoBolsa ON
insert into FIN_TENANT_DESTINO..GrupamentoBolsa (IdGrupamento,CdAtivoBolsa,DataLancamento,DataEx,DataReferencia,FatorQuantidade,FatorPU,Fonte,TipoGrupamento)
select IdGrupamento,CdAtivoBolsa,DataLancamento,DataEx,DataReferencia,FatorQuantidade,FatorPU,Fonte,TipoGrupamento from FIN_TENANT_ORIGEM..GrupamentoBolsa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..GrupamentoBolsa OFF


--PapelRendaFixa
--delete from FIN_TENANT_DESTINO..PapelRendaFixa
sp_help papelrendafixa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..PapelRendaFixa ON
insert into FIN_TENANT_DESTINO..PapelRendaFixa (IdPapel,TipoPapel,Descricao,TipoRentabilidade,CasasDecimaisPU,TipoCurva,ContagemDias,BaseAno,TipoVolume,TipoCustodia,Classe,PagamentoJuros)
select IdPapel,TipoPapel,Descricao,TipoRentabilidade,CasasDecimaisPU,TipoCurva,ContagemDias,BaseAno,TipoVolume,TipoCustodia,Classe,PagamentoJuros from FIN_TENANT_ORIGEM..PapelRendaFixa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..PapelRendaFixa OFF

--TituloRendaFixa Publico
sp_help titulorendafixa
SET IDENTITY_INSERT FIN_TENANT_DESTINO..TituloRendaFixa ON
insert into FIN_TENANT_DESTINO..TituloRendaFixa (IdTitulo,IdPapel,IdIndice,IdEmissor,Descricao,Taxa,Percentual,DataEmissao,DataVencimento,ValorNominal,CodigoCustodia,PUNominal,TipoMTM,IdSerie,DescricaoCompleta,IsentoIR,IsentoIOF,IdMoeda,IdEstrategia,CodigoCDA,CodigoIsin)
select IdTitulo,IdPapel,IdIndice,IdEmissor,Descricao,Taxa,Percentual,DataEmissao,DataVencimento,ValorNominal,CodigoCustodia,PUNominal,TipoMTM,IdSerie,DescricaoCompleta,IsentoIR,IsentoIOF,IdMoeda,2,CodigoCDA,CodigoIsin from FIN_TENANT_ORIGEM..TituloRendaFixa 
where descricao in ('LFT',
'LTN',
'NTN-B',
'NTN-C',
'NTN-F') and IdSerie is null
SET IDENTITY_INSERT FIN_TENANT_DESTINO..TituloRendaFixa OFF

--CotacaoMercadoAnbima
insert into FIN_TENANT_DESTINO..cotacaomercadoandima
(DataReferencia
,Descricao
,CodigoSELIC
,DataEmissao
,DataVencimento
,TaxaIndicativa
,PU)
select * from FIN_TENANT_ORIGEM..cotacaomercadoandima
where codigoselic+CONVERT(VARCHAR(10),datareferencia)+CONVERT(VARCHAR(10),datavencimento) not in (
select codigoselic+CONVERT(VARCHAR(10),datareferencia)+CONVERT(VARCHAR(10),datavencimento) from FIN_TENANT_DESTINO..cotacaomercadoandima)