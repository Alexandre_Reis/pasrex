select c.idcliente as IdCliente, 
	datadia as DataAtual,
	'Renda Fixa',
	str(t.idtitulo) as CodigoAtivo,
	rtrim(t.Descricao) + '  ' + CASE WHEN p.taxavolta is not null THEN ltrim(str(p.taxavolta, 10, 2)) + '% ' ELSE CASE WHEN t.percentual is null THEN ' ' ELSE ltrim(str(t.percentual, 10, 2)) + '% ' END END + 
		CASE WHEN t.idindice is null THEN '' ELSE i.descricao END +
		CASE WHEN (p.taxaoperacao is null OR p.taxaoperacao = 0)  THEN '' ELSE ' + ' + ltrim(str(p.taxaoperacao, 10, 2)) + '% ' END as Descricao,
	dataoperacao as DataOperacao, 
	p.datavencimento as DataVencimento,
	Quantidade, 
	PUOperacao as PUCusto,
	(PUOperacao * Quantidade) as ValorCusto,
	valormercado as SaldoBruto, 
	valorir + valoriof as Tributos,
	valormercado - (valorir + valoriof) as SaldoLiquido,
	(valormercado - (PUOperacao * Quantidade)) as ResultadoRealizar
from posicaorendafixa p, cliente c, 
	titulorendafixa t LEFT JOIN indice i
on t.idindice = i.idindice
where p.idtitulo = t.idtitulo
and p.idcliente = c.idcliente
and quantidade <> 0
union all
select c.idcliente as IdCliente, 
	datadia as DataAtual,
	'Cotas de Fundos',
	a.CodigoAnbid as CodigoAtivo,
	a.apelido as Descricao,
	dataaplicacao as DataOperacao, 
	'',
	Quantidade,
	CotaAplicacao as PUCusto,
	(CotaAplicacao * Quantidade) as ValorCusto,
	ValorBruto, 
	valorir + valoriof as Tributos,
	ValorLiquido,
	(valorbruto - (CotaAplicacao * Quantidade)) as ResultadoRealizar
from posicaofundo p, carteira a, cliente c
where p.idcarteira = a.idcarteira
and p.idcliente = c.idcliente
and quantidade <> 0
union all
select c.idcliente as IdCliente,
	datadia as DataAtual,
	'Bolsa',
	a.cdativobolsa as CodigoAtivo,
	a.Descricao as Descricao,
	'',
	'', 
	Quantidade, 
	PUCustoLiquido as PUCusto,
	(PUCustoLiquido * Quantidade) as ValorCusto,
	ValorMercado as ValorBruto, 
	0 as Tributos,
	ValorMercado as ValorLiquido,
	ValorMercado - (PUCustoLiquido * Quantidade) as ResultadoRealizar
from posicaobolsa p, ativobolsa a, cliente c
where p.cdativobolsa = a.cdativobolsa
and p.idcliente = c.idcliente
and quantidade <> 0
union all
select c.idcliente as IdCliente,
	datadia as DataAtual,
	'Valores a Liquidar',
	'' as CodigoAtivo,
	l.Descricao as Descricao,
	datalancamento as DataOperacao,
	DataVencimento,
	0, 
	0, 
	0, 
	Valor as ValorBruto, 
	0 as Tributos,
	Valor as ValorLiquido,
	0
from liquidacao l, cliente c
where l.idcliente = c.idcliente
and l.datavencimento > c.datadia
and l.datalancamento <= c.datadia
and valor <> 0
union all
select c.idcliente as IdCliente,
	datadia as DataAtual,
	'Caixa',
	'' as CodigoAtivo,
	'Saldo Caixa' as Descricao,
	'' as DataOperacao,
	'' as DataVencimento,
	0,
	0, 
	0, 
	SaldoFechamento as ValorBruto, 
	0 as Tributos,
	SaldoFechamento as ValorLiquido,
	0
from saldocaixa s, cliente c
where s.idcliente = c.idcliente
and s.data = c.datadia
