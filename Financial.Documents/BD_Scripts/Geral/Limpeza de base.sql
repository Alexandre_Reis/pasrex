
truncate table OperacaoBMF;
GO
delete from OperacaoBolsa;
GO
truncate table OperacaoCotista;
GO
delete from  OperacaoEmprestimoBolsa;
GO
truncate table OperacaoFundo;
GO
truncate table OperacaoRendaFixa;
GO
truncate table OperacaoSwap;
GO
truncate table OperacaoTermoBolsa;
GO
truncate table PosicaoBMF;
GO
truncate table PosicaoBMFAbertura;
GO
truncate table PosicaoBMFHistorico;
GO
truncate table PosicaoBolsa;
GO
truncate table PosicaoBolsaAbertura;
GO
truncate table PosicaoBolsaDetalhe;
GO
truncate table PosicaoBolsaHistorico;
GO
truncate table PosicaoCotista;
GO
truncate table PosicaoCotistaAbertura;
GO
truncate table PosicaoCotistaHistorico;
GO
truncate table PosicaoEmprestimoBolsa;
GO
truncate table PosicaoEmprestimoBolsaAbertura;
GO
truncate table PosicaoEmprestimoBolsaHistorico;
GO
truncate table PosicaoFundo;
GO
truncate table PosicaoFundoAbertura;
GO
truncate table PosicaoFundoHistorico;
GO
truncate table PosicaoRendaFixa;
GO
truncate table PosicaoRendaFixaAbertura;
GO
truncate table PosicaoRendaFixaDetalhe;
GO
truncate table PosicaoRendaFixaHistorico;
GO
truncate table PosicaoSwap;
GO
truncate table PosicaoSwapAbertura;
GO
truncate table PosicaoSwapHistorico;
GO
truncate table PosicaoTermoBolsa;
GO
truncate table PosicaoTermoBolsaAbertura;
GO
truncate table PosicaoTermoBolsaHistorico;
GO
truncate table DetalheResgateCotista;
GO
truncate table Ordemcotista;
GO
Truncate table PermissaoCotista;
GO
truncate table PrejuizoCotista;
GO
truncate table PrejuizoCotistaHistorico;
go
truncate table PrejuizoCotistaUsado;
go
truncate table TemplateCotista;
go
truncate table TransferenciaCota
Go
delete from cotista;
go
truncate table AgendaFundo
go
truncate table CalculoAdministracao
go
truncate table CalculoAdministracaoHistorico
go
truncate table CalculoPerformance
go
truncate table CalculoPerformanceHistorico
go
truncate table CalculoProvisao
go
truncate table CalculoProvisaoHistorico
go
truncate table CalculoRebateCarteira
go
truncate table CalculoRebateCotista
go
truncate table DetalheResgateCotista
go
truncate table DetalheResgateFundo
go
truncate table EnquadraItemFundo
go
truncate table EnquadraResultadoDetalhe
go
delete from EnquadraResultado
go
truncate table EnquadraResultadoDetalhe
go
delete from EnquadraRegra
go
truncate table EnquadraResultadoDetalhe
go
truncate table HistoricoCota
go
truncate table ListaBenchmark
go
truncate table OrdemCotista
go
truncate table OrdemFundo
go
truncate table PrejuizoCotista
go
truncate table PrejuizoCotistaHistorico
go
truncate table PrejuizoCotistaUsado
go
truncate table PrejuizoFundo
go
truncate table PrejuizoFundoHistorico
go
truncate table TabelaPerfilMensalCVM
go
truncate table TabelaRebateCarteira
go
truncate table TabelaRebateDistribuidor
go
truncate table TemplateCarteira
go
truncate table TransferenciaCota
go
delete from carteira;
GO
truncate table AcumuladoCorretagemBMF
GO
truncate table ApuracaoIRImobiliario
GO
truncate table ApuracaoRendaVariavelIR
GO
truncate table BloqueioBolsa
GO
truncate table BloqueioRendaFixa
GO
truncate table BookCliente
GO
truncate table CalculoGerencial
GO
truncate table ClienteBMF
GO
truncate table ClienteBolsa
GO
truncate table ClienteInterface
GO
truncate table ClienteRendaFixa
GO
truncate table CodigoClienteAgente
GO
truncate table ContabLancamento
GO
truncate table ContabSaldo
GO
truncate table CustodiaBMF
GO
truncate table DetalheResgateFundo
GO
truncate table EventoFisicoBolsa
GO
truncate table EventoRendaFixa
GO
truncate table GerOperacaoBMF
GO
truncate table GerOperacaoBolsa
GO
truncate table GerPosicaoBMF
GO
truncate table GerPosicaoBMFAbertura
GO
truncate table GerPosicaoBMFHistorico
GO
truncate table GerPosicaoBolsa
GO
truncate table GerPosicaoBolsaAbertura
GO
truncate table GerPosicaoBolsaHistorico
GO
truncate table IRFonte
GO
truncate table Liquidacao
GO
truncate table LiquidacaoAbertura
GO
truncate table LiquidacaoEmprestimoBolsa
GO
truncate table LiquidacaoFuturo
GO
truncate table LiquidacaoHistorico
GO
truncate table LiquidacaoRendaFixa
GO
truncate table LiquidacaoSwap
GO
truncate table LiquidacaoTermoBolsa
GO
truncate table OrdemBMF
GO
truncate table OrdemTermoBolsa
go
delete from OrdemBolsa
GO
truncate table OrdemFundo
GO
truncate table OrdemRendaFixa
GO
truncate table TabelaCorretagemBMF
go
delete from  PerfilCorretagemBMF
GO
truncate table PerfilCorretagemBolsa
GO
truncate table PermissaoCliente
GO
truncate table PermissaoOperacaoFundo
GO
truncate table ProventoBolsaCliente
GO
truncate table SaldoCaixa
GO
truncate table TabelaCONR
GO
truncate table TabelaCustosRendaFixa
GO
truncate table TabelaExtratoCliente
GO
truncate table TabelaInterfaceCliente
GO
truncate table TabelaMTMRendaFixa
GO
truncate table TabelaProcessamento
GO
truncate table TabelaRebateCorretagem
GO
truncate table TabelaTaxaCustodiaBolsa
GO
truncate table TransferenciaBMF
GO
truncate table TransferenciaBolsa
GO
delete from  Usuario where login <> 'admin'
GO
delete from cliente;
GO
truncate table SaldoCaixa;
go
delete from ContaCorrente;
go
truncate table PessoaDocumento;
go
truncate table PessoaEmail;
go
truncate table PessoaEndereco;
go
truncate table PessoaTelefone;
go
truncate table SuitabilityResposta;
go
truncate table TabelaRebateOfficer;
go
delete from pessoa;
GO
truncate table historicosenhas






