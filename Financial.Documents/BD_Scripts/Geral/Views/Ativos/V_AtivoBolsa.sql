create view V_AtivoBolsa as 
select CdAtivoBolsa
,Especificacao
,a.Descricao
,TipoMercado
,PUExercicio
,DataVencimento
,CdAtivoBolsaObjeto
,CodigoIsin
,e.IdEmissor
,e.Nome as Emissor
,e.CNPJ as CNPJEmissor
,s.IdSetor
,s.Nome as Setor
,m.IdMoeda
,m.Nome as Moeda
,r.IdEstrategia
,r.Descricao as Estrategia
from 
ativobolsa a left outer join Estrategia r on r.IdEstrategia = a.IdEstrategia
, Emissor e, Setor s, Moeda m
where a.IdEmissor = e.IdEmissor
and e.IdSetor = s.IdSetor
and a.IdMoeda = m.IdMoeda
