CREATE VIEW V_AtivoRendaFixa as
select 
IdTitulo
,i.IdIndice
,i.Descricao as Indice
,t.Descricao
,t.Taxa
,t.Percentual
,DataEmissao
,DataVencimento
,CodigoCustodia
,PUNominal
,CodigoIsin
,CodigoCetip
,CodigoCBLC
,e.IdEmissor
,e.Nome as Emissor
,e.CNPJ as CNPJEmissor
,s.IdSetor
,s.Nome as Setor
,m.IdMoeda
,m.Nome as Moeda
,r.IdEstrategia
,r.Descricao as Estrategia
from 
TituloRendaFixa t left outer join Indice i on i.IdIndice = t.idindice
left outer join Estrategia r on r.IdEstrategia = t.IdEstrategia
, Emissor e, Setor s, Moeda m
where t.IdEmissor = e.IdEmissor
and e.IdSetor = s.IdSetor
and t.IdMoeda = m.IdMoeda