create view V_AtivoFundo as 
select IdCarteira
,a.Nome
,a.DiasCotizacaoAplicacao
,a.DiasCotizacaoResgate
,a.DiasLiquidacaoAplicacao
,a.DiasLiquidacaoResgate
,case when a.ContagemDiasConversaoResgate = 1 then 'Uteis' else 'Corridos' end as ContagemDiasResgate
,IdAgenteAdministrador
,a1.Nome as Administrador
,IdAgenteGestor
,a2.Nome as Gestor
,CodigoIsin
,m.IdMoeda
,m.Nome as Moeda
,s.IdEstrategia
,s.Descricao as Estrategia
from 
Carteira a left outer join Estrategia s on s.IdEstrategia = s.IdEstrategia
, Moeda m, Cliente c, AgenteMercado a1, AgenteMercado a2
where c.IdMoeda = m.IdMoeda
and a.IdCarteira = c.IdCliente
and a.IdAgenteAdministrador = a1.IdAgente
and a.IdAgenteGestor = a2.IdAgente
and c.IdTipo in (200, 500)
