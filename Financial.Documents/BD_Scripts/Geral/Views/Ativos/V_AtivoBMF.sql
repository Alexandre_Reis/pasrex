create view V_AtivoBMF as 
select CdAtivoBMF
,Serie
,TipoMercado
,TipoSerie
,PrecoExercicio
,DataVencimento
,Peso
,CodigoIsin
,m.IdMoeda
,m.Nome as Moeda
,r.IdEstrategia
,r.Descricao as Estrategia
from 
ativobMF a left outer join Estrategia r on r.IdEstrategia = a.IdEstrategia
, Moeda m
where a.IdMoeda = m.IdMoeda
