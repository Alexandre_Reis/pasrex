create view V_Patrimonio as
select Data
,IdCarteira
,(PatrimonioBruto/CotaBruta) as QuantidadeBruta
,CotaBruta
,PatrimonioBruto
,QuantidadeFechamento as Quantidade
,PLAbertura
,CotaAbertura
,PLFechamento
,CotaFechamento
from HistoricoCota
where CotaBruta <> 0
