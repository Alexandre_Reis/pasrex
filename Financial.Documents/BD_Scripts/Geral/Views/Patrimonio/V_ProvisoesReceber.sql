create View V_ProvisoesReceber as
select 
IdCliente as IdCarteira
,DataHistorico
,sum(Valor) as ValorReceber
from LiquidacaoHistorico
where Valor > 0
and DataVencimento > DataHistorico
group by IdCliente, DataHistorico 

