create View V_CotasEmitir as
select H.Data
,H.IdCarteira
,sum(O.ValorBruto) as CotasResgatar
from HistoricoCota H , OperacaoCotista O
where H.IdCarteira = O.IdCarteira
and O.IdCarteira <> O.IdCotista
and O.TipoOperacao = 1 and O.DataConversao > H.Data and O.DataOperacao <= H.Data
group by H.Data, H.IdCarteira

