create view V_ValorVendas as

select isnull(a.IdCarteira,isnull(b.IdCarteira,isnull(c.IdCarteira,d.IdCarteira))) IdCarteira, 
isnull(a.data,isnull(b.data,isnull(c.data,d.data))) Data, 
(isnull(a.somaValor,0)+isnull(b.somaValor,0)+isnull(c.somaValor,0)+isnull(d.somaValor,0)) ValorVendas
from 
(select idCliente IdCarteira, dataOperacao data, sum(valor) somaValor from OperacaoRendaFixa
where tipoOperacao = 2 group by IdCliente, dataOperacao) a
full join
(select idCliente IdCarteira, dataOperacao data, sum(valorbruto) somaValor from OperacaoFundo
where tipoOperacao in (2, 3, 4, 5) group by IdCliente, DataOperacao) b
on a.IdCarteira = b.IdCarteira and a.data = b.data full join
(select idCliente IdCarteira, data, sum(valor) somaValor from OperacaoBolsa
where tipoOperacao in ('V', 'VD') group by idCliente, data) c
on b.IdCarteira = c.IdCarteira and b.data = c.data full join
(select idCliente IdCarteira, data, sum(valor) somaValor from OperacaoBMF
where tipoOperacao in ('V', 'VD') and tipoMercado <> 2 group by IdCliente, Data) d
on c.IdCarteira = d.IdCarteira and c.data = d.data


