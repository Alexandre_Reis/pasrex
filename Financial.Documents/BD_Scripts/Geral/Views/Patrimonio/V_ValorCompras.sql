create view V_ValorCompras as

select isnull(a.IdCarteira,isnull(b.IdCarteira,isnull(c.IdCarteira,d.IdCarteira))) IdCarteira, 
isnull(a.data,isnull(b.data,isnull(c.data,d.data))) Data, 
(isnull(a.somaValor,0)+isnull(b.somaValor,0)+isnull(c.somaValor,0)+isnull(d.somaValor,0)) ValorCompras
from 
(select idCliente IdCarteira, dataOperacao data, sum(valor) somaValor from OperacaoRendaFixa 
where tipoOperacao in (1, 3) group by idCliente, dataOperacao) a full join
(select idCliente IdCarteira, dataOperacao data, sum(valorBruto) somaValor from OperacaoFundo
where tipooperacao = 1 group by idCliente, dataOperacao) b
on a.IdCarteira = b.IdCarteira and a.data = b.data full join 
(select IdCliente IdCarteira, data, sum(valor) somaValor from OperacaoBolsa
where tipoOperacao in ('C', 'CD') group by idCliente, Data) c
on b.IdCarteira = c.IdCarteira and b.data = c.data full join
(select IdCliente IdCarteira, data, sum(valor) somaValor from OperacaoBMF
where tipoOperacao in ('C', 'CD') and tipoMercado <> 2
 group by idCliente, Data) d
 on c.IdCarteira = d.IdCarteira and c.data = d.data

