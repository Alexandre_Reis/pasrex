create View V_ProvisoesPagar as
select 
IdCliente as IdCarteira
,DataHistorico
,sum(abs(Valor)) as ValorPagar
from LiquidacaoHistorico
where Valor < 0
and DataVencimento > DataHistorico
group by IdCliente, DataHistorico 

