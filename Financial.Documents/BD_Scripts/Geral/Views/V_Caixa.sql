create view V_Caixa as
select IdLiquidacao
,DataLancamento
,DataVencimento
,Valor
,Descricao
,IdCliente as IdCarteira
from Liquidacao