create view V_CotacaoIndexador as
select i.IdIndice
,Descricao
,case when TipoDivulgacao = 1 then 'Diario' else 'Mensal' end as Periodicidade
,Valor
from Indice i, CotacaoIndice c
where i.IdIndice = c.IdIndice
