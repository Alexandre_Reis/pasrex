create view V_Movimento as
select IdOperacao
,'Bolsa' as TipoMercado
,Data
,idcliente as IdCarteira
,CdAtivoBolsa as CodigoAtivo
,left(TipoOperacao, 1) as TipoOperacao
,PU
,Quantidade
,Valor as ValorBruto
,(Valor - ValorLiquido) as Despesas
,0 as ValorIR
,0 as ValorIOF
,ValorLiquido
from OperacaoBolsa
union all
select IdOperacao
,'BMF' as TipoMercado
,Data
,idcliente as IdCarteira
,CdAtivoBMF + Serie as CodigoAtivo
,left(TipoOperacao, 1) as TipoOperacao
,PU
,Quantidade
,Valor as ValorBruto
,(Valor - ValorLiquido) as Despesas
,0 as ValorIR
,0 as ValorIOF
,ValorLiquido
from OperacaoBMF
union all
select IdOperacao
,'Fundos' as TipoMercado
,DataOperacao
,idcliente as IdCarteira
,str(IdCarteira) as CodigoAtivo
,case when TipoOperacao = 1 then 'C' else 'V' end as TipoOperacao
,CotaOperacao as PU
,Quantidade
,ValorBruto
,0 as Despesas
,ValorIR
,ValorIOF
,ValorLiquido
from OperacaoFundo
where TipoOperacao in (1, 2, 3, 4, 5)
union all
select IdOperacao
,'RendaFixa' as TipoMercado
,DataOperacao
,idcliente as IdCarteira
,str(IdTitulo) as CodigoAtivo
,case when (TipoOperacao = 1 or TipoOperacao = 3) then 'C' else 'V' end as TipoOperacao
,PUOperacao as PU
,Quantidade
,Valor
,0 as Despesas
,ValorIR
,ValorIOF
,ValorLiquido
from OperacaoRendaFixa
