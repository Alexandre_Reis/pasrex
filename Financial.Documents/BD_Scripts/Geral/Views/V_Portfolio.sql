﻿create view V_Portfolio as
select IdCarteira
,p.Apelido
,p.Nome
,case when p.Tipo = 1 then 'Fisica' else 'Juridica' end as TipoPessoa
,case when c.TipoCarteira = 1 then 'Renda Fixa' else 'Renda Variável' end as TipoCarteira
,CodigoIsin
,c.IdAgenteAdministrador
,a1.Nome as Administrador
,c.IdAgenteGestor
,a2.Nome as Gestor
,c.IdAgenteCustodiante
,a3.Nome as Custodiante
,c.CodigoAnbid as CodigoAnbima
,m.IdMoeda
,m.Nome as Moeda
,v.IdPerfilInvestidor
,v.Descricao as PerfilInvestidor
,d.IdIndice as IdBenchmark
,d.Descricao as Benchmark
,p.CodigoInterface
from 
Carteira c left outer join AgenteMercado a3 on a3.IdAgente = c.IdAgenteCustodiante, 
Pessoa p left outer join PerfilInvestidor v on v.IdPerfilInvestidor = p.IdPerfilInvestidor,
Cliente l, Moeda m, Indice d, AgenteMercado a1, AgenteMercado a2
where c.IdCarteira = p.IdPessoa
and c.IdCarteira = l.IdCliente
and c.IdAgenteAdministrador = a1.IdAgente
and c.IdAgenteGestor = a2.IdAgente
and l.IdMoeda = m.IdMoeda
and c.IdIndiceBenchmark = d.IdIndice