select c.idcliente as IdCliente, 
	datadia as DataAtual,
	str(t.idtitulo) as CodigoAtivo,
	t.DescricaoCompleta,
	p.datavencimento as DataVencimento,
	sum(Quantidade) as Quantidade, 
	sum(Quantidade * PUOperacao) as ValorCusto,
	sum(valormercado) as SaldoBruto, 
	sum(valorir + valoriof) as Tributos,
	sum(valormercado - (valorir + valoriof)) as SaldoLiquido,
	sum((Quantidade * (PUMercado - PUOperacao))) as ResultadoRealizar
from posicaorendafixa p, cliente c, titulorendafixa t
where p.idtitulo = t.idtitulo
and p.idcliente = c.idcliente
and quantidade <> 0
group by c.idcliente, datadia, t.idtitulo, t.DescricaoCompleta, p.datavencimento
union all
select c.idcliente as IdCliente, 
	datadia as DataAtual,
	a.CodigoAnbid as CodigoAtivo,
	a.apelido as Descricao,
	'',
	sum(Quantidade) as Quantidade,
	sum((Quantidade * CotaAplicacao)) as ValorCusto,
	sum(ValorBruto) as SaldoBruto, 
	sum(valorir + valoriof) as Tributos,
	sum(ValorLiquido) as SaldoLiquido,
	sum((Quantidade * (CotaDia - CotaAplicacao))) as ResultadoRealizar
from posicaofundo p, carteira a, cliente c
where p.idcarteira = a.idcarteira
and p.idcliente = c.idcliente
and quantidade <> 0
group by c.idcliente, datadia, a.CodigoAnbid, a.apelido
union all
select c.idcliente as IdCliente,
	datadia as DataAtual,
	a.cdativobolsa as CodigoAtivo,
	a.Descricao as Descricao,
	'', 
	sum(Quantidade) as Quantidade, 
	sum((Quantidade * PUCustoLiquido)) as ValorCusto,
	sum(ValorMercado) as SaldoBruto, 
	sum(0) as Tributos,
	sum(ValorMercado) as SaldoLiquido,
	sum(ResultadoRealizar) as ResultadoRealizar
from posicaobolsa p, ativobolsa a, cliente c
where p.cdativobolsa = a.cdativobolsa
and p.idcliente = c.idcliente
and quantidade <> 0
group by c.idcliente, datadia, a.cdativobolsa, a.Descricao
union all
select s.idcliente as IdCliente,
	datadia as DataAtual,
	'' as CodigoAtivo,
	'Saldo Caixa' as Descricao,
	'' as DataVencimento,
	sum(0) as Quantidade,
	sum(0) as ValorCusto,
	sum(SaldoFechamento) as SaldoBruto, 
	sum(0) as Tributos,
	sum(SaldoFechamento) as ValorLiquido,
	sum(0) as ResultadoRealizar
from saldocaixa s, cliente c
where s.idcliente = c.idcliente
and s.data = c.datadia
group by s.idcliente, datadia