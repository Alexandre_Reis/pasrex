select CdAtivoBolsa,
Especificacao,
Descricao,
TipoMercado,
ISNULL(    REPLACE(   CONVERT(VARCHAR, PuExercicio)  ,  '.'  , ',' )  ,  ''   ) AS PuExercicio,
ISNULL(    CONVERT(   VARCHAR(10),  DataVencimento,  103  ) ,  '' ) AS DataVencimento,
ISNULL( CdAtivobolsaObjeto, '') AS CdAtivobolsaObjeto,
CodigoIsin
from ativobolsa order by CdAtivoBolsa