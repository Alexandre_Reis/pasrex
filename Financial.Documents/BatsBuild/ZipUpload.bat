@ECHO -----------------------------------	------
@ECHO Criando ZIP do Financial Atatika para upload
c:
cd C:\Program Files\WinRAR

winrar a -afzip A:\ZipParaUpload\Financial_Atatika "A:\WebApp\Financial_Atatika\" -r -s -m5 -ag+ddmmyyyyhhss_ -ilog -ep1 -x*\imagensPersonalizadas -x*\imagensPersonalizadas\* -x*\Downloads -x*\Downloads\* -x*\web.config -x*\Web.Config

@pause