@ECHO /*
@ECHO Disco D do Servidor deve estar mapeado como A:
@ECHO Deve Ser Usado Ap�s Deploy no Financial_Atatika
@ECHO Copia Arquivos do Financial_Atatika para:

@ECHO Financial_Interno
@ECHO Financial_Target
@ECHO Financial_ALL
@ECHO Financial_Atatika_IR
@ECHO Financial_Graphus
@ECHO Financial_Foco
@ECHO Financial_Mindex
@ECHO Financial_OGCapital

@ECHO */

@ECHO /*------------Financial_INTERNO-------------------------*/

del /Q /F /S A:\WebApp\Financial_INTERNO\App_Themes
del /Q /F /S A:\WebApp\Financial_INTERNO\App_Data
del /Q /F /S A:\WebApp\Financial_INTERNO\Downloads
del /Q /F /S A:\WebApp\Financial_INTERNO\bin
del /Q /F /S A:\WebApp\Financial_INTERNO\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_INTERNO\Consultas
del /Q /F /S A:\WebApp\Financial_INTERNO\css
del /Q /F /S A:\WebApp\Financial_INTERNO\Erro
del /Q /F /S A:\WebApp\Financial_INTERNO\imagens
del /Q /F /S A:\WebApp\Financial_INTERNO\js
del /Q /F /S A:\WebApp\Financial_INTERNO\jsKey
del /Q /F /S A:\WebApp\Financial_INTERNO\Login
del /Q /F /S A:\WebApp\Financial_INTERNO\Processamento
del /Q /F /S A:\WebApp\Financial_INTERNO\Relatorios
del /Q /F /S A:\WebApp\Financial_INTERNO\Configuracao
del /Q /F /S A:\WebApp\Financial_INTERNO\Interfaces
del /Q /F /S A:\WebApp\Financial_INTERNO\Processamento

rd /Q /S A:\WebApp\Financial_INTERNO\App_Themes
rd /Q /S A:\WebApp\Financial_INTERNO\App_Data
rd /Q /S A:\WebApp\Financial_INTERNO\bin
rd /Q /S A:\WebApp\Financial_INTERNO\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_INTERNO\Consultas
rd /Q /S A:\WebApp\Financial_INTERNO\css
rd /Q /S A:\WebApp\Financial_INTERNO\Erro
rd /Q /S A:\WebApp\Financial_INTERNO\imagens
rd /Q /S A:\WebApp\Financial_INTERNO\js
rd /Q /S A:\WebApp\Financial_INTERNO\jsKey
rd /Q /S A:\WebApp\Financial_INTERNO\Login
rd /Q /S A:\WebApp\Financial_INTERNO\Processamento
rd /Q /S A:\WebApp\Financial_INTERNO\Relatorios
rd /Q /S A:\WebApp\Financial_INTERNO\Configuracao
rd /Q /S A:\WebApp\Financial_INTERNO\Interfaces
rd /Q /S A:\WebApp\Financial_INTERNO\Processamento

del /Q /F /S A:\WebApp\Financial_INTERNO\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_INTERNO\Default.aspx
del /Q /F /S A:\WebApp\Financial_INTERNO\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_INTERNO\Home.aspx
del /Q /F /S A:\WebApp\Financial_INTERNO\Search.aspx
del /Q /F /S A:\WebApp\Financial_INTERNO\Web.sitemap
del /Q /F /S A:\WebApp\Financial_INTERNO\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_INTERNO\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_INTERNO\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_INTERNO\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_INTERNO\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_INTERNO\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_INTERNO\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_INTERNO\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_INTERNO\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_INTERNO\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_INTERNO\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_INTERNO\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_INTERNO\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_INTERNO\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_INTERNO\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_INTERNO\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_INTERNO\

@ECHO /*------------Financial_Target-------------------------*/

del /Q /F /S A:\WebApp\Financial_Target\App_Themes
del /Q /F /S A:\WebApp\Financial_Target\App_Data
del /Q /F /S A:\WebApp\Financial_Target\Downloads
del /Q /F /S A:\WebApp\Financial_Target\bin
del /Q /F /S A:\WebApp\Financial_Target\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_Target\Consultas
del /Q /F /S A:\WebApp\Financial_Target\css
del /Q /F /S A:\WebApp\Financial_Target\Erro
del /Q /F /S A:\WebApp\Financial_Target\imagens
del /Q /F /S A:\WebApp\Financial_Target\js
del /Q /F /S A:\WebApp\Financial_Target\jsKey
del /Q /F /S A:\WebApp\Financial_Target\Login
del /Q /F /S A:\WebApp\Financial_Target\Processamento
del /Q /F /S A:\WebApp\Financial_Target\Relatorios
del /Q /F /S A:\WebApp\Financial_Target\Configuracao
del /Q /F /S A:\WebApp\Financial_Target\Interfaces
del /Q /F /S A:\WebApp\Financial_Target\Processamento

rd /Q /S A:\WebApp\Financial_Target\App_Themes
rd /Q /S A:\WebApp\Financial_Target\App_Data
rd /Q /S A:\WebApp\Financial_Target\bin
rd /Q /S A:\WebApp\Financial_Target\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Target\Consultas
rd /Q /S A:\WebApp\Financial_Target\css
rd /Q /S A:\WebApp\Financial_Target\Erro
rd /Q /S A:\WebApp\Financial_Target\imagens
rd /Q /S A:\WebApp\Financial_Target\js
rd /Q /S A:\WebApp\Financial_Target\jsKey
rd /Q /S A:\WebApp\Financial_Target\Login
rd /Q /S A:\WebApp\Financial_Target\Processamento
rd /Q /S A:\WebApp\Financial_Target\Relatorios
rd /Q /S A:\WebApp\Financial_Target\Configuracao
rd /Q /S A:\WebApp\Financial_Target\Interfaces
rd /Q /S A:\WebApp\Financial_Target\Processamento

del /Q /F /S A:\WebApp\Financial_Target\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_Target\Default.aspx
del /Q /F /S A:\WebApp\Financial_Target\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_Target\Home.aspx
del /Q /F /S A:\WebApp\Financial_Target\Search.aspx
del /Q /F /S A:\WebApp\Financial_Target\Web.sitemap
del /Q /F /S A:\WebApp\Financial_Target\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_Target\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_Target\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_Target\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_Target\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_Target\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_Target\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_Target\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_Target\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_Target\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_Target\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_Target\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_Target\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_Target\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_Target\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_Target\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_Target\

@ECHO /*------------Financial_All-------------------------*/

del /Q /F /S A:\WebApp\Financial_All\App_Themes
del /Q /F /S A:\WebApp\Financial_All\App_Data
del /Q /F /S A:\WebApp\Financial_All\Downloads
del /Q /F /S A:\WebApp\Financial_All\bin
del /Q /F /S A:\WebApp\Financial_All\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_All\Consultas
del /Q /F /S A:\WebApp\Financial_All\css
del /Q /F /S A:\WebApp\Financial_All\Erro
del /Q /F /S A:\WebApp\Financial_All\imagens
del /Q /F /S A:\WebApp\Financial_All\js
del /Q /F /S A:\WebApp\Financial_All\jsKey
del /Q /F /S A:\WebApp\Financial_All\Login
del /Q /F /S A:\WebApp\Financial_All\Processamento
del /Q /F /S A:\WebApp\Financial_All\Relatorios
del /Q /F /S A:\WebApp\Financial_All\Configuracao
del /Q /F /S A:\WebApp\Financial_All\Interfaces
del /Q /F /S A:\WebApp\Financial_All\Processamento

rd /Q /S A:\WebApp\Financial_All\App_Themes
rd /Q /S A:\WebApp\Financial_All\App_Data
rd /Q /S A:\WebApp\Financial_All\bin
rd /Q /S A:\WebApp\Financial_All\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_All\Consultas
rd /Q /S A:\WebApp\Financial_All\css
rd /Q /S A:\WebApp\Financial_All\Erro
rd /Q /S A:\WebApp\Financial_All\imagens
rd /Q /S A:\WebApp\Financial_All\js
rd /Q /S A:\WebApp\Financial_All\jsKey
rd /Q /S A:\WebApp\Financial_All\Login
rd /Q /S A:\WebApp\Financial_All\Processamento
rd /Q /S A:\WebApp\Financial_All\Relatorios
rd /Q /S A:\WebApp\Financial_All\Configuracao
rd /Q /S A:\WebApp\Financial_All\Interfaces
rd /Q /S A:\WebApp\Financial_All\Processamento

del /Q /F /S A:\WebApp\Financial_All\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_All\Default.aspx
del /Q /F /S A:\WebApp\Financial_All\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_All\Home.aspx
del /Q /F /S A:\WebApp\Financial_All\Search.aspx
del /Q /F /S A:\WebApp\Financial_All\Web.sitemap
del /Q /F /S A:\WebApp\Financial_All\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_All\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_All\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_All\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_All\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_All\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_All\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_All\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_All\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_All\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_All\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_All\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_All\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_All\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_All\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_All\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_All\

@ECHO /*------------Financial_Atatika_IR-------------------------*/

del /Q /F /S A:\WebApp\Financial_Atatika_IR\App_Themes
del /Q /F /S A:\WebApp\Financial_Atatika_IR\App_Data
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Downloads
del /Q /F /S A:\WebApp\Financial_Atatika_IR\bin
del /Q /F /S A:\WebApp\Financial_Atatika_IR\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Consultas
del /Q /F /S A:\WebApp\Financial_Atatika_IR\css
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Erro
del /Q /F /S A:\WebApp\Financial_Atatika_IR\imagens
del /Q /F /S A:\WebApp\Financial_Atatika_IR\js
del /Q /F /S A:\WebApp\Financial_Atatika_IR\jsKey
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Login
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Processamento
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Relatorios
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Configuracao
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Interfaces
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Processamento

rd /Q /S A:\WebApp\Financial_Atatika_IR\App_Themes
rd /Q /S A:\WebApp\Financial_Atatika_IR\App_Data
rd /Q /S A:\WebApp\Financial_Atatika_IR\bin
rd /Q /S A:\WebApp\Financial_Atatika_IR\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Atatika_IR\Consultas
rd /Q /S A:\WebApp\Financial_Atatika_IR\css
rd /Q /S A:\WebApp\Financial_Atatika_IR\Erro
rd /Q /S A:\WebApp\Financial_Atatika_IR\imagens
rd /Q /S A:\WebApp\Financial_Atatika_IR\js
rd /Q /S A:\WebApp\Financial_Atatika_IR\jsKey
rd /Q /S A:\WebApp\Financial_Atatika_IR\Login
rd /Q /S A:\WebApp\Financial_Atatika_IR\Processamento
rd /Q /S A:\WebApp\Financial_Atatika_IR\Relatorios
rd /Q /S A:\WebApp\Financial_Atatika_IR\Configuracao
rd /Q /S A:\WebApp\Financial_Atatika_IR\Interfaces
rd /Q /S A:\WebApp\Financial_Atatika_IR\Processamento

del /Q /F /S A:\WebApp\Financial_Atatika_IR\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Default.aspx
del /Q /F /S A:\WebApp\Financial_Atatika_IR\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Home.aspx
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Search.aspx
del /Q /F /S A:\WebApp\Financial_Atatika_IR\Web.sitemap
del /Q /F /S A:\WebApp\Financial_Atatika_IR\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_Atatika_IR\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_Atatika_IR\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_Atatika_IR\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_Atatika_IR\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_Atatika_IR\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_Atatika_IR\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_Atatika_IR\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_Atatika_IR\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_Atatika_IR\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_Atatika_IR\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_Atatika_IR\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_Atatika_IR\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_Atatika_IR\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_Atatika_IR\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_Atatika_IR\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_Atatika_IR\

@ECHO /*------------Financial_Graphus-------------------------*/

del /Q /F /S A:\WebApp\Financial_Graphus\App_Themes
del /Q /F /S A:\WebApp\Financial_Graphus\App_Data
del /Q /F /S A:\WebApp\Financial_Graphus\Downloads
del /Q /F /S A:\WebApp\Financial_Graphus\bin
del /Q /F /S A:\WebApp\Financial_Graphus\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_Graphus\Consultas
del /Q /F /S A:\WebApp\Financial_Graphus\css
del /Q /F /S A:\WebApp\Financial_Graphus\Erro
del /Q /F /S A:\WebApp\Financial_Graphus\imagens
del /Q /F /S A:\WebApp\Financial_Graphus\js
del /Q /F /S A:\WebApp\Financial_Graphus\jsKey
del /Q /F /S A:\WebApp\Financial_Graphus\Login
del /Q /F /S A:\WebApp\Financial_Graphus\Processamento
del /Q /F /S A:\WebApp\Financial_Graphus\Relatorios
del /Q /F /S A:\WebApp\Financial_Graphus\Configuracao
del /Q /F /S A:\WebApp\Financial_Graphus\Interfaces
del /Q /F /S A:\WebApp\Financial_Graphus\Processamento

rd /Q /S A:\WebApp\Financial_Graphus\App_Themes
rd /Q /S A:\WebApp\Financial_Graphus\App_Data
rd /Q /S A:\WebApp\Financial_Graphus\bin
rd /Q /S A:\WebApp\Financial_Graphus\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Graphus\Consultas
rd /Q /S A:\WebApp\Financial_Graphus\css
rd /Q /S A:\WebApp\Financial_Graphus\Erro
rd /Q /S A:\WebApp\Financial_Graphus\imagens
rd /Q /S A:\WebApp\Financial_Graphus\js
rd /Q /S A:\WebApp\Financial_Graphus\jsKey
rd /Q /S A:\WebApp\Financial_Graphus\Login
rd /Q /S A:\WebApp\Financial_Graphus\Processamento
rd /Q /S A:\WebApp\Financial_Graphus\Relatorios
rd /Q /S A:\WebApp\Financial_Graphus\Configuracao
rd /Q /S A:\WebApp\Financial_Graphus\Interfaces
rd /Q /S A:\WebApp\Financial_Graphus\Processamento

del /Q /F /S A:\WebApp\Financial_Graphus\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_Graphus\Default.aspx
del /Q /F /S A:\WebApp\Financial_Graphus\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_Graphus\Home.aspx
del /Q /F /S A:\WebApp\Financial_Graphus\Search.aspx
del /Q /F /S A:\WebApp\Financial_Graphus\Web.sitemap
del /Q /F /S A:\WebApp\Financial_Graphus\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_Graphus\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_Graphus\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_Graphus\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_Graphus\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_Graphus\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_Graphus\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_Graphus\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_Graphus\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_Graphus\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_Graphus\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_Graphus\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_Graphus\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_Graphus\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_Graphus\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_Graphus\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_Graphus\

@ECHO /*------------Financial_Foco-------------------------*/

del /Q /F /S A:\WebApp\Financial_Foco\App_Themes
del /Q /F /S A:\WebApp\Financial_Foco\App_Data
del /Q /F /S A:\WebApp\Financial_Foco\Downloads
del /Q /F /S A:\WebApp\Financial_Foco\bin
del /Q /F /S A:\WebApp\Financial_Foco\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_Foco\Consultas
del /Q /F /S A:\WebApp\Financial_Foco\css
del /Q /F /S A:\WebApp\Financial_Foco\Erro
del /Q /F /S A:\WebApp\Financial_Foco\imagens
del /Q /F /S A:\WebApp\Financial_Foco\js
del /Q /F /S A:\WebApp\Financial_Foco\jsKey
del /Q /F /S A:\WebApp\Financial_Foco\Login
del /Q /F /S A:\WebApp\Financial_Foco\Processamento
del /Q /F /S A:\WebApp\Financial_Foco\Relatorios
del /Q /F /S A:\WebApp\Financial_Foco\Configuracao
del /Q /F /S A:\WebApp\Financial_Foco\Interfaces
del /Q /F /S A:\WebApp\Financial_Foco\Processamento

rd /Q /S A:\WebApp\Financial_Foco\App_Themes
rd /Q /S A:\WebApp\Financial_Foco\App_Data
rd /Q /S A:\WebApp\Financial_Foco\bin
rd /Q /S A:\WebApp\Financial_Foco\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Foco\Consultas
rd /Q /S A:\WebApp\Financial_Foco\css
rd /Q /S A:\WebApp\Financial_Foco\Erro
rd /Q /S A:\WebApp\Financial_Foco\imagens
rd /Q /S A:\WebApp\Financial_Foco\js
rd /Q /S A:\WebApp\Financial_Foco\jsKey
rd /Q /S A:\WebApp\Financial_Foco\Login
rd /Q /S A:\WebApp\Financial_Foco\Processamento
rd /Q /S A:\WebApp\Financial_Foco\Relatorios
rd /Q /S A:\WebApp\Financial_Foco\Configuracao
rd /Q /S A:\WebApp\Financial_Foco\Interfaces
rd /Q /S A:\WebApp\Financial_Foco\Processamento

del /Q /F /S A:\WebApp\Financial_Foco\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_Foco\Default.aspx
del /Q /F /S A:\WebApp\Financial_Foco\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_Foco\Home.aspx
del /Q /F /S A:\WebApp\Financial_Foco\Search.aspx
del /Q /F /S A:\WebApp\Financial_Foco\Web.sitemap
del /Q /F /S A:\WebApp\Financial_Foco\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_Foco\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_Foco\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_Foco\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_Foco\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_Foco\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_Foco\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_Foco\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_Foco\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_Foco\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_Foco\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_Foco\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_Foco\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_Foco\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_Foco\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_Foco\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_Foco\

@ECHO /*------------Financial_Mindex-------------------------*/

del /Q /F /S A:\WebApp\Financial_Mindex\App_Themes
del /Q /F /S A:\WebApp\Financial_Mindex\App_Data
del /Q /F /S A:\WebApp\Financial_Mindex\Downloads
del /Q /F /S A:\WebApp\Financial_Mindex\bin
del /Q /F /S A:\WebApp\Financial_Mindex\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_Mindex\Consultas
del /Q /F /S A:\WebApp\Financial_Mindex\css
del /Q /F /S A:\WebApp\Financial_Mindex\Erro
del /Q /F /S A:\WebApp\Financial_Mindex\imagens
del /Q /F /S A:\WebApp\Financial_Mindex\js
del /Q /F /S A:\WebApp\Financial_Mindex\jsKey
del /Q /F /S A:\WebApp\Financial_Mindex\Login
del /Q /F /S A:\WebApp\Financial_Mindex\Processamento
del /Q /F /S A:\WebApp\Financial_Mindex\Relatorios
del /Q /F /S A:\WebApp\Financial_Mindex\Configuracao
del /Q /F /S A:\WebApp\Financial_Mindex\Interfaces
del /Q /F /S A:\WebApp\Financial_Mindex\Processamento

rd /Q /S A:\WebApp\Financial_Mindex\App_Themes
rd /Q /S A:\WebApp\Financial_Mindex\App_Data
rd /Q /S A:\WebApp\Financial_Mindex\bin
rd /Q /S A:\WebApp\Financial_Mindex\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Mindex\Consultas
rd /Q /S A:\WebApp\Financial_Mindex\css
rd /Q /S A:\WebApp\Financial_Mindex\Erro
rd /Q /S A:\WebApp\Financial_Mindex\imagens
rd /Q /S A:\WebApp\Financial_Mindex\js
rd /Q /S A:\WebApp\Financial_Mindex\jsKey
rd /Q /S A:\WebApp\Financial_Mindex\Login
rd /Q /S A:\WebApp\Financial_Mindex\Processamento
rd /Q /S A:\WebApp\Financial_Mindex\Relatorios
rd /Q /S A:\WebApp\Financial_Mindex\Configuracao
rd /Q /S A:\WebApp\Financial_Mindex\Interfaces
rd /Q /S A:\WebApp\Financial_Mindex\Processamento

del /Q /F /S A:\WebApp\Financial_Mindex\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_Mindex\Default.aspx
del /Q /F /S A:\WebApp\Financial_Mindex\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_Mindex\Home.aspx
del /Q /F /S A:\WebApp\Financial_Mindex\Search.aspx
del /Q /F /S A:\WebApp\Financial_Mindex\Web.sitemap
del /Q /F /S A:\WebApp\Financial_Mindex\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_Mindex\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_Mindex\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_Mindex\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_Mindex\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_Mindex\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_Mindex\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_Mindex\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_Mindex\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_Mindex\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_Mindex\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_Mindex\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_Mindex\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_Mindex\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_Mindex\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_Mindex\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_Mindex\

@ECHO /*------------Financial_OGCapital-------------------------*/

del /Q /F /S A:\WebApp\Financial_OGCapital\App_Themes
del /Q /F /S A:\WebApp\Financial_OGCapital\App_Data
del /Q /F /S A:\WebApp\Financial_OGCapital\Downloads
del /Q /F /S A:\WebApp\Financial_OGCapital\bin
del /Q /F /S A:\WebApp\Financial_OGCapital\CadastrosBasicos
del /Q /F /S A:\WebApp\Financial_OGCapital\Consultas
del /Q /F /S A:\WebApp\Financial_OGCapital\css
del /Q /F /S A:\WebApp\Financial_OGCapital\Erro
del /Q /F /S A:\WebApp\Financial_OGCapital\imagens
del /Q /F /S A:\WebApp\Financial_OGCapital\js
del /Q /F /S A:\WebApp\Financial_OGCapital\jsKey
del /Q /F /S A:\WebApp\Financial_OGCapital\Login
del /Q /F /S A:\WebApp\Financial_OGCapital\Processamento
del /Q /F /S A:\WebApp\Financial_OGCapital\Relatorios
del /Q /F /S A:\WebApp\Financial_OGCapital\Configuracao
del /Q /F /S A:\WebApp\Financial_OGCapital\Interfaces
del /Q /F /S A:\WebApp\Financial_OGCapital\Processamento

rd /Q /S A:\WebApp\Financial_OGCapital\App_Themes
rd /Q /S A:\WebApp\Financial_OGCapital\App_Data
rd /Q /S A:\WebApp\Financial_OGCapital\bin
rd /Q /S A:\WebApp\Financial_OGCapital\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_OGCapital\Consultas
rd /Q /S A:\WebApp\Financial_OGCapital\css
rd /Q /S A:\WebApp\Financial_OGCapital\Erro
rd /Q /S A:\WebApp\Financial_OGCapital\imagens
rd /Q /S A:\WebApp\Financial_OGCapital\js
rd /Q /S A:\WebApp\Financial_OGCapital\jsKey
rd /Q /S A:\WebApp\Financial_OGCapital\Login
rd /Q /S A:\WebApp\Financial_OGCapital\Processamento
rd /Q /S A:\WebApp\Financial_OGCapital\Relatorios
rd /Q /S A:\WebApp\Financial_OGCapital\Configuracao
rd /Q /S A:\WebApp\Financial_OGCapital\Interfaces
rd /Q /S A:\WebApp\Financial_OGCapital\Processamento

del /Q /F /S A:\WebApp\Financial_OGCapital\ELBScriptAdvanced.js
del /Q /F /S A:\WebApp\Financial_OGCapital\Default.aspx
del /Q /F /S A:\WebApp\Financial_OGCapital\ELB_handlerFile.aspx
del /Q /F /S A:\WebApp\Financial_OGCapital\Home.aspx
del /Q /F /S A:\WebApp\Financial_OGCapital\Search.aspx
del /Q /F /S A:\WebApp\Financial_OGCapital\Web.sitemap
del /Q /F /S A:\WebApp\Financial_OGCapital\PrecompiledApp.config

@ECHO Copia Arquivos do Financial_Atatika
XCOPY A:\WebApp\Financial_Atatika\App_Themes\*.* /E /S A:\WebApp\Financial_OGCapital\App_Themes\
XCOPY A:\WebApp\Financial_Atatika\bin\*.* /E /S A:\WebApp\Financial_OGCapital\bin\
XCOPY A:\WebApp\Financial_Atatika\CadastrosBasicos\*.* /E /S A:\WebApp\Financial_OGCapital\CadastrosBasicos\
XCOPY A:\WebApp\Financial_Atatika\Consultas\*.* /E /S A:\WebApp\Financial_OGCapital\Consultas\
XCOPY A:\WebApp\Financial_Atatika\css\*.* /E /S A:\WebApp\Financial_OGCapital\css\
XCOPY A:\WebApp\Financial_Atatika\Erro\*.* /E /S A:\WebApp\Financial_OGCapital\Erro\
XCOPY A:\WebApp\Financial_Atatika\imagens\*.* /E /S A:\WebApp\Financial_OGCapital\imagens\
XCOPY A:\WebApp\Financial_Atatika\js\*.* /E /S A:\WebApp\Financial_OGCapital\js\
XCOPY A:\WebApp\Financial_Atatika\jsKey\*.* /E /S A:\WebApp\Financial_OGCapital\jsKey\
XCOPY A:\WebApp\Financial_Atatika\Login\*.* /E /S A:\WebApp\Financial_OGCapital\Login\
XCOPY A:\WebApp\Financial_Atatika\Relatorios\*.* /E /S A:\WebApp\Financial_OGCapital\Relatorios\
XCOPY A:\WebApp\Financial_Atatika\Configuracao\*.* /E /S A:\WebApp\Financial_OGCapital\Configuracao\
XCOPY A:\WebApp\Financial_Atatika\Interfaces\*.* /E /S A:\WebApp\Financial_OGCapital\Interfaces\
XCOPY A:\WebApp\Financial_Atatika\Processamento\*.* /E /S A:\WebApp\Financial_OGCapital\Processamento\

XCOPY A:\WebApp\Financial_Atatika\ELBScriptAdvanced.js A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\Default.aspx A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\ELB_handlerFile.aspx A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\Home.aspx A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\Search.aspx A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\Web.sitemap A:\WebApp\Financial_OGCapital\
XCOPY A:\WebApp\Financial_Atatika\PrecompiledApp.config A:\WebApp\Financial_OGCapital\

@pause