@ECHO -----------------------------------------
@ECHO Criando RAR no diretorio Backup
c:
cd C:\Arquivos de programas\WinRAR

winrar a A:\Temp\Financial_Atatika A:\WebApp\Financial_Atatika -r -s -m5 -ag+ddmmyyyy_ -ilog

@ECHO Apaga arquivos do Servidor
del /Q /F /S A:\WebApp\Financial_Atatika\*.*

@ECHO Apaga os diretorios do Servidor
rd /Q /S A:\WebApp\Financial_Atatika\App_Themes
rd /Q /S A:\WebApp\Financial_Atatika\bin
rd /Q /S A:\WebApp\Financial_Atatika\CadastrosBasicos
rd /Q /S A:\WebApp\Financial_Atatika\Consultas
rd /Q /S A:\WebApp\Financial_Atatika\css
rd /Q /S A:\WebApp\Financial_Atatika\Erro
rd /Q /S A:\WebApp\Financial_Atatika\imagens
rd /Q /S A:\WebApp\Financial_Atatika\imagensPersonalizadas
rd /Q /S A:\WebApp\Financial_Atatika\js
rd /Q /S A:\WebApp\Financial_Atatika\jsKey
rd /Q /S A:\WebApp\Financial_Atatika\Login
rd /Q /S A:\WebApp\Financial_Atatika\Relatorios
rd /Q /S A:\WebApp\Financial_Atatika\Configuracao
rd /Q /S A:\WebApp\Financial_Atatika\Interfaces
rd /Q /S A:\WebApp\Financial_Atatika\Processamento

@ECHO -----------------------------------------

@ECHO Copia Arquivos de Deploy Localmente para o Servidor

XCOPY S:\Projetos\Financial\Componentes\Financial.Web_Deploy_Release\*.* /E /S A:\WebApp\Financial_Atatika

@ECHO Copia DLLS Extras presentes no diretorio do servidor para o diretorio BIN da Aplicacao
XCOPY A:\Temp\Dlls\v.9.2.9.9355\*.* /E /S A:\WebApp\Financial_Atatika\bin

@ECHO Copia WebConfig Financial Atatika
XCOPY A:\Temp\WebConfigs\Financial_Atatika\web.config /E /S /R /Y A:\WebApp\Financial_Atatika

@ECHO Deleta Arquivos de Documentação XML e Debug(PDB)
@ECHO Apaga arquivos do Servidor
del /F A:\WebApp\Financial_Atatika\bin\*.pdb
del /F A:\WebApp\Financial_Atatika\bin\*.xml

@ECHO Deleta DLLS Interfaces
del /F A:\WebApp\Financial_Atatika\bin\Financial.Interfaces.DB.dll

@pause