Logar como user TFSETUP/TFSETUP no computador


outros comandos:
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportRetornoEstrategiaBenchmark.cs
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportRetornoEstrategia.cs
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportResultadoEstrategia.cs
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportGraficoAlocacaoAtivo2.cs
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportGraficoAlocacaoAtivo.cs
tf undo /workspace:luiz-PC;gallo /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/App_Code/Relatorios/Fundo/ExtratoCliente/SubReportContaCorrente.cs


Comando Funcional:
tf undo /workspace:EDUARDO-VOSTRO;efreitas /server:http://server:8080/ /recursive $/Financial/Financial/Financial.Web/Web.sitemap
----------------------------------------------------------------------------------------------------------------------------------

There are 2 possible options for unlocking, viz. unlock or undo (if checked out for Edit).
The respective command to use is tf lock /lock:none or tf undo

The parameters in both cases are the same:

filename (in the help topic, this is the itemspec parameter)
This is the file you want to unlock

/workspace: [user's workspace];[user name]
The user�s workspace needs to be figured out�by right-clicking the locked item and selecting Properties� and then 
the Status tab
The user name is the fully qualified user name, in the form: domain/user

/s:[server path]
This is the TFS server in question. Typically it�s http://servername:8080/

So, a sample command would be something like:
tf undo $/Apps/Dev/file1.aspx /workspace:�Dev station�;corp1\ra /s:http://altair:8080

(You run this is on a VS client, at the Visual Studio command prompt)

 
Another command I found useful during the process was 
tf status /user:Joe /s:server path, which essentially lists all files locked by Joe. 
I piped it to a text file (with >> C:\joe.locked.txt) to get a report.

-------------------------------------------------------------------------------------------

The specific commands for unlock and undo are as follows:

    TF LOCK filespec /LOCK:NONE /WORKSPACE:workspace;lock_user
    TF UNDO filespec /WORKSPACE:workspace;checkout_user

The important things to realize about these commands are as follows:

    Filespec will most frequently be a path to a file or folder in Team Foundation Vesion Control (TFVC).  For example: "$/MyTeamProject/MyFolder/Foo.cs".  If you have embedded spaces in the path, make sure to use quotes around the filespec.
    Filespec can can be a local file (e.g., "C:\MyTeamProject\MyFolder\Foo.cs") only if that local path is in the workspace holding the lock or having the file checked out.  That means that it will have to be on the workstation of the user holding the lock/checkout.  You shouldn't need the workspace switch in this case, but I haven't actually verified this personally.
    If TF can't detemine the server, you're also need a server switch on the command line (e.g., "/server:mytfs").
    You can add the "/recursive" switch to unlock or undo all changes for a specific user recursively under a folder in TFVC.
    The workspace can be queried by looking at the properties of the item in Source Control Explorer and switching to the Status tab.  It can also be queried using the TF command line utility.
    The lock_user and checkout_user refer to the login for the user holding the lock or checkout.  For example: "notion\dmckinstry".

The examples above would look like this if you wanted to unlock the file "Foo.cs" from my "DaveWorkspace" workspace:

    TF LOCK C:\MyTeamProject\MyFolder\Foo.cs /LOCK:NONE
           /WORKSPACE:DaveWorkspace;notion\dmckinstry

In theory, this same basic approach can be used to check in changes made by other users, but the note above pertaining to being in that user's workspace on their workstation would defintely apply.

------------------------------------------------------------------------------------------


You can do this through the command line client (start -> all programs -> visual studio 2005 beta 2 -> visual studio tools -> visual studio 2005 command prompt).  You have to specify the other user's workspace using the /workspace option. 

h lock /lock:none $/<full path to the file>/filename /workspace:usersworkspacename

For example, say that Joe has a file locked in workspace "JoesWorkspace"

h lock /lock:none $/TeamProject/WidgetProject/Widgets/main.cs /workspace:JoesWorkspace

-----------------------------------------------------------------------------------------

TF Command Line Utilty Undo Command:
http://www.kodyaz.com/articles/tfs-undo-checkout-unlock-pending-changes-using-team-foundation-server.aspx