﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using Financial.Util;
using Financial.Common.Enums;
using System.Data.SqlClient;
using Financial.Bolsa.Properties;
using System.Threading;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.Investidor.Enums;
using Financial.Investidor;

namespace Financial.Gerencial.Controller
{

    public class ControllerGerencial
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ControllerGerencial));

        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento"></param>
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            #region Deletes Posicao
            #region Delete GerPosicaoBolsa
            GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
            try
            {
                gerPosicaoBolsaCollection.DeletaPosicaoBolsa(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBolsa");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete GerPosicaoBMF
            GerPosicaoBMFCollection gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
            try
            {
                gerPosicaoBMFCollection.DeletaPosicaoBMF(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBMF");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            string sql = "";
            esUtility u = new esUtility();
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    u = new esUtility();
                    sql = " set identity_insert GerPosicaoBolsa on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert GerPosicaoBolsa a partir de GerPosicaoBolsaHistorico
                        gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                        try
                        {
                            gerPosicaoBolsaCollection.InserePosicaoBolsaHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert GerPosicaoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert GerPosicaoBolsa a partir de GerPosicaoBolsaAbertura
                        gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                        try
                        {
                            gerPosicaoBolsaCollection.InserePosicaoBolsaAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert GerPosicaoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    scope.Complete();
                }

                u = new esUtility();
                sql = " set identity_insert GerPosicaoBolsa off ";
                u.ExecuteNonQuery(esQueryType.Text, sql, "");


                using (esTransactionScope scope = new esTransactionScope())
                {
                    u = new esUtility();
                    sql = " set identity_insert GerPosicaoBMF on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert GerPosicaoBMF a partir de GerPosicaoBMFHistorico
                        gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
                        try
                        {
                            gerPosicaoBMFCollection.InserePosicaoBMFHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert GerPosicaoBMF");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert GerPosicaoBMF a partir de GerPosicaoBMFAbertura
                        gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
                        try
                        {
                            gerPosicaoBMFCollection.InserePosicaoBMFAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert GerPosicaoBMF");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    scope.Complete();
                }

                u = new esUtility();
                sql = " set identity_insert GerPosicaoBMF off ";
                u.ExecuteNonQuery(esQueryType.Text, sql, "");
            }
            else
            {
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                    #region Insert GerPosicaoBolsa a partir de GerPosicaoBolsaHistorico
                    gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                    try
                    {
                        gerPosicaoBolsaCollection.InserePosicaoBolsaHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert GerPosicaoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert GerPosicaoBMF a partir de GerPosicaoBMFHistorico
                    gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
                    try
                    {
                        gerPosicaoBMFCollection.InserePosicaoBMFHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert GerPosicaoBMF");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert GerPosicaoBolsa a partir de GerPosicaoBolsaAbertura
                    gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                    try
                    {
                        gerPosicaoBolsaCollection.InserePosicaoBolsaAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert GerPosicaoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert GerPosicaoBMF a partir de GerPosicaoBMFAbertura
                    gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
                    try
                    {
                        gerPosicaoBMFCollection.InserePosicaoBMFAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert GerPosicaoBMF");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }

            }
            #endregion

            #region Deletes das Posições Históricas
            #region Delete PosicaoBolsaHistorico
            GerPosicaoBolsaHistoricoCollection gerPosicaoBolsaHistoricoCollection = new GerPosicaoBolsaHistoricoCollection();
            try
            {
                gerPosicaoBolsaHistoricoCollection.DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBolsaHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete PosicaoBMFHistorico
            GerPosicaoBMFHistoricoCollection gerPosicaoBMFHistoricoCollection = new GerPosicaoBMFHistoricoCollection();
            try
            {
                gerPosicaoBMFHistoricoCollection.DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBMFHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion
            #endregion

            if (tipoReprocessamento != TipoReprocessamento.CalculoDiario)
            {
                #region Deletes das Posições de Abertura
                #region Delete PosicaoBolsaAbertura
                GerPosicaoBolsaAberturaCollection gerPosicaoBolsaAberturaCollection = new GerPosicaoBolsaAberturaCollection();
                try
                {
                    gerPosicaoBolsaAberturaCollection.DeletaPosicaoBolsaAberturaDataHistoricoMaior(idCliente, data);
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Delete GerPosicaoBolsaAbertura");
                    Console.WriteLine(e.LineNumber);
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine(e.ToString());
                }
                #endregion

                #region Delete PosicaoBMFAbertura
                GerPosicaoBMFAberturaCollection gerPosicaoBMFAberturaCollection = new GerPosicaoBMFAberturaCollection();
                try
                {
                    gerPosicaoBMFAberturaCollection.DeletaPosicaoBMFAberturaDataHistoricoMaior(idCliente, data);
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Delete GerPosicaoBMFAbertura");
                    Console.WriteLine(e.LineNumber);
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine(e.ToString());
                }
                #endregion
                #endregion
            }

            if (tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                CalculoGerencial calculoGerencial = new CalculoGerencial();
                calculoGerencial.DeletaGerencialDataMaiorIgual(idCliente, data);
            }
            else
            {
                CalculoGerencial calculoGerencial = new CalculoGerencial();
                calculoGerencial.DeletaGerencialDataMaior(idCliente, data);
            }

        }

        /// <summary>
        /// Atualiza as posições de abertura de termo e empréstimo.
        /// Joga de Posicao para PosicaoAbertura (PosicaoBolsa, PosicaoTermoBolsa, PosicaoBMF).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
            GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();
            EventoBolsa eventoBolsa = new EventoBolsa();

            #region ProcessaDireitoSubscricao
            eventoBolsa.ProcessaDireitoSubscricao(idCliente, data);
            #endregion

            #region ProcessaBonificacao
            eventoBolsa.ProcessaBonificacao(idCliente, data);
            #endregion

            #region ProcessaConversao
            eventoBolsa = new EventoBolsa();
            eventoBolsa.ProcessaConversao(idCliente, data);
            #endregion

            #region ProcessaGrupamento
            eventoBolsa = new EventoBolsa();
            eventoBolsa.ProcessaGrupamento(idCliente, data);
            #endregion

            //Carrega as posições atuais para Posicao de Abertura (Bolsa, Termo, BMF)
            gerPosicaoBolsa.GeraPosicaoAbertura(idCliente, data);
            gerPosicaoBMF.GeraPosicaoAbertura(idCliente, data);
            //
        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data)
        {
            GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
            GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();
            //
            gerPosicaoBolsa.GeraBackup(idCliente, data);
            gerPosicaoBMF.GeraBackup(idCliente, data);
            //
        }

        /// <summary>
        /// Realiza todos os cálculos do Gerencial para Bolsa (incluindo a parte de termos no gerencial).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="integraBolsa">Indica se integra ordens do Sinacor e carrega para operações</param>
        private void ExecutaFechamentoBolsa(int idCliente, DateTime data, bool integraBolsa)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdMoeda);
            cliente.LoadByPrimaryKey(campos, idCliente);
            int idMoeda = cliente.IdMoeda.Value;

            if (idMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                {
                    return;
                }
            }
            else if (idMoeda == (int)ListaMoedaFixo.Dolar)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    return;
                }
            }

            // Inicialização objetos
            CalculoGerencial calculoGerencial = new CalculoGerencial();
            OrdemBolsa ordemBolsa = new OrdemBolsa();
            GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
            GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
            GerLiquidacaoTermoBolsa gerLiquidacaoTermoBolsa = new GerLiquidacaoTermoBolsa();


            #region CasaDayTrade (Gerencial)
            calculoGerencial.CasaDayTradeBolsa(idCliente, data);
            #endregion

            #region CarregaOrdemCasada (Gerencial)
            gerOperacaoBolsa.CarregaOrdemCasada(idCliente, data);
            #endregion

            #region TransfereDespesas (Gerencial)
            calculoGerencial = new CalculoGerencial();
            calculoGerencial.TransfereDespesasGerencialBolsa(idCliente, data);
            #endregion


            //Tratamento de Termo (operação, liquidação)
            #region Termos
            #region Método central para deleção de Operações de bolsa originadas de Vcto e/ou Liquidação de Termo
            gerOperacaoBolsa.DeletaOperacoesLiquidacaoTermo(idCliente, data);
            #endregion

            #region ProcessaLiquidacaoTermo
            gerLiquidacaoTermoBolsa.ProcessaLiquidacaoTermo(idCliente, data);
            #endregion
            #endregion
            //

            #region CalculaValoresLiquidos
            gerOperacaoBolsa = new GerOperacaoBolsa();
            gerOperacaoBolsa.CalculaValoresLiquidos(idCliente, data);
            #endregion

            #region ZeraResultados
            gerOperacaoBolsa = new GerOperacaoBolsa();
            gerOperacaoBolsa.ZeraResultados(idCliente, data);
            #endregion

            #region TrataExercicioOpcao
            gerOperacaoBolsa = new GerOperacaoBolsa();
            gerOperacaoBolsa.TrataExercicioOpcao(idCliente, data);
            #endregion

            #region CasaVencimento
            gerOperacaoBolsa = new GerOperacaoBolsa();

            if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.ExercicioPrimeiro)
            {
                gerOperacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.ExercicioPrimeiro);
            }
            else if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.TermoPrimeiro)
            {
                gerOperacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.TermoPrimeiro);
            }
            #endregion

            #region ProcessaCompra
            gerOperacaoBolsa = new GerOperacaoBolsa();
            gerOperacaoBolsa.ProcessaCompra(idCliente, data);
            #endregion

            #region ProcessaVenda
            gerOperacaoBolsa = new GerOperacaoBolsa();
            gerOperacaoBolsa.ProcessaVenda(idCliente, data);
            #endregion

            #region ProcessaVencimentoOpcao
            gerPosicaoBolsa = new GerPosicaoBolsa();
            gerPosicaoBolsa.ProcessaVencimentoOpcao(idCliente, data);
            #endregion

            #region AtualizaValores (GerPosicaoBolsa)
            gerPosicaoBolsa = new GerPosicaoBolsa();
            gerPosicaoBolsa.AtualizaValores(idCliente, data);
            #endregion
        }

        /// <summary>
        /// Realiza todos os cálculos do Gerencial para BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="integraBMF"></param>
        private void ExecutaFechamentoBMF(int idCliente, DateTime data, bool integraBMF)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdMoeda);
            cliente.LoadByPrimaryKey(campos, idCliente);
            int idMoeda = cliente.IdMoeda.Value;

            if (idMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                {
                    return;
                }
            }
            else if (idMoeda == (int)ListaMoedaFixo.Dolar)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    return;
                }
            }

            CalculoGerencial calculoGerencial = new CalculoGerencial();

            GerOperacaoBMF gerOperacaoBMF = new GerOperacaoBMF();
            GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();

            #region Casamento DayTrade (Gerencial)
            calculoGerencial.CasaDayTradeBMF(idCliente, data);
            #endregion

            #region CarregaOrdemCasada (Gerencial)
            gerOperacaoBMF.CarregaOrdemCasada(idCliente, data);
            #endregion

            #region TransfereDespesas (Gerencial)
            calculoGerencial = new CalculoGerencial();
            calculoGerencial.TransfereDespesasGerencialBMF(idCliente, data);
            #endregion

            #region CalculaValoresLiquidos (Gerencial)
            gerOperacaoBMF = new GerOperacaoBMF();
            gerOperacaoBMF.CalculaValoresLiquidos(idCliente, data);
            #endregion

            #region Ajuste de posição (Gerencial)
            gerPosicaoBMF.CalculaAjustePosicao(idCliente, data);
            #endregion

            #region Ajuste de operação (Gerencial)
            gerOperacaoBMF = new GerOperacaoBMF();
            gerOperacaoBMF.CalculaAjusteOperacao(idCliente, data);
            #endregion

            #region Processa posições (compras/vendas) (Gerencial)
            gerOperacaoBMF = new GerOperacaoBMF();
            gerOperacaoBMF.ProcessaVenda(idCliente, data);

            gerOperacaoBMF = new GerOperacaoBMF();
            gerOperacaoBMF.ProcessaCompra(idCliente, data);
            #endregion

            #region Cálculo de despesas no vencimento
            //posicaoBMF = new PosicaoBMF();
            //posicaoBMF.CalculaTaxasVencimento(idCliente, data);
            #endregion

            #region Baixa de contratos vencidos (Gerencial)
            gerPosicaoBMF = new GerPosicaoBMF();
            gerPosicaoBMF.ProcessaVencimento(idCliente, data);
            #endregion

            #region Atualiza valores (Gerencial)
            gerPosicaoBMF = new GerPosicaoBMF();
            gerPosicaoBMF.AtualizaValores(idCliente, data);
            #endregion
        }

        /// <summary>
        /// Método central para executar o Gerencial.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCliente, DateTime data)
        {
            this.ChecaOrdensSemTrader(idCliente, data);

            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);

            ExecutaFechamentoBolsa(idCliente, data, true);
            ExecutaFechamentoBMF(idCliente, data, true);

            CalculoGerencial calculoGerencial = new CalculoGerencial();
            calculoGerencial.CalculoGerencialDiario(idCliente, data);
        }

        /// <summary>
        /// Processa as estratégias vinculadas a carteira passada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaCalculoEstrategia(int idCliente, DateTime data)
        {
            HistoricoEstrategia historicoEstrategia = new HistoricoEstrategia();
            historicoEstrategia.ProcessaEstrategia(idCliente, data);
        }

        /// <summary>
        /// Checa se tem alguma boleta sem trader associado, se tiver envia exceção com a 1a boleta (bolsa ou BMF) sem trader.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ChecaOrdensSemTrader(int idCliente, DateTime data)
        {
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                                             ordemBolsaCollection.Query.Data.Equal(data),
                                             ordemBolsaCollection.Query.IdTrader.IsNull());
            ordemBolsaCollection.Query.Load();

            if (ordemBolsaCollection.Count > 0)
            {
                string cdAtivoBolsa = ordemBolsaCollection[0].CdAtivoBolsa;
                string tipoOperacao = ordemBolsaCollection[0].TipoOrdem;
                decimal quantidade = ordemBolsaCollection[0].Quantidade.Value;
                decimal pu = ordemBolsaCollection[0].Pu.Value;

                if (tipoOperacao == "C")
                {
                    tipoOperacao = "Compra";
                }
                else
                {
                    tipoOperacao = "Venda";
                }
                throw new Exception("Boleta não tem trader especificado na data! " + tipoOperacao + " de " + cdAtivoBolsa + "-> Qtde: " + quantidade.ToString() + "  PU:" + pu.ToString());
            }

            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            ordemBMFCollection.Query.Where(ordemBMFCollection.Query.IdCliente.Equal(idCliente),
                                             ordemBMFCollection.Query.Data.Equal(data),
                                             ordemBMFCollection.Query.IdTrader.IsNull());
            ordemBMFCollection.Query.Load();

            if (ordemBMFCollection.Count > 0)
            {
                string cdAtivoBMF = ordemBMFCollection[0].CdAtivoBMF;
                string serie = ordemBMFCollection[0].Serie;
                string tipoOperacao = ordemBMFCollection[0].TipoOrdem;
                int quantidade = ordemBMFCollection[0].Quantidade.Value;
                decimal pu = ordemBMFCollection[0].Pu.Value;

                if (tipoOperacao == "C")
                {
                    tipoOperacao = "Compra";
                }
                else
                {
                    tipoOperacao = "Venda";
                }
                throw new Exception("Boleta não tem trader especificado na data! " + tipoOperacao + " de " + cdAtivoBMF + serie + "-> Qtde: " + quantidade.ToString() + "  PU:" + pu.ToString());
            }

        }
    }
}
