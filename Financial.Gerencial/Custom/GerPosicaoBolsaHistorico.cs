using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Gerencial {
    public partial class GerPosicaoBolsaHistorico : esGerPosicaoBolsaHistorico {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBolsaHistorico));

        /// <summary>
        /// Carrega o objeto GerPosicaoBolsaHistorico com os campos IdPosicao, PUCusto.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idTrader"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public bool BuscaPosicaoBolsaHistorico(int idCliente, DateTime dataHistorico, string cdAtivoBolsa, int idTrader) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.PUCusto)
                 .Where(this.Query.DataHistorico == dataHistorico,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.IdCliente == idCliente,
                        this.Query.IdTrader == idTrader);

            return this.Query.Load();
        }
    }
}
