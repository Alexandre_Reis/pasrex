﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Gerencial
{
	public partial class GerPosicaoBMFHistoricoCollection : esGerPosicaoBMFHistoricoCollection
	{
        /// <summary>
        /// Carrega o objeto GerPosicaoBMFHistoricoCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMFHistoricoCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();

         }

	}
}
