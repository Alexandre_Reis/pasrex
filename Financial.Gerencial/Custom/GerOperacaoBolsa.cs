﻿using System;
using System.Text;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

using Financial.Util;
using Financial.Bolsa;

using Financial.Bolsa.Enums;
using Financial.Common.Enums;

using Financial.Bolsa.Exceptions;

namespace Financial.Gerencial {

    public partial class GerOperacaoBolsa : esGerOperacaoBolsa {

        private static readonly ILog log = LogManager.GetLogger(typeof(GerOperacaoBolsa));

        /// <summary>
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaValoresLiquidos(int idCliente, DateTime data) {

            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaValores(idCliente, data);
            //
            for (int i = 0; i < operacaoBolsaCollection.Count; i++) {
                GerOperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal pu = operacaoBolsa.Pu.Value;

                decimal fator = 0;
                if (valor != 0)
                {
                    fator = (quantidade * pu) / valor;
                }

                decimal puLiquido = operacaoBolsa.PULiquido.Value;
                // taxas
                decimal despesas = operacaoBolsa.Despesas.Value;
                decimal valorLiquido = 0;

                switch (operacaoBolsa.TipoOperacao) {
                    case TipoOperacaoBolsa.Compra:
                    case TipoOperacaoBolsa.CompraDaytrade:
                        if (fator != 0)
                        {
                            puLiquido = pu + (despesas * fator) / quantidade;
                        }
                        else
                        {
                            puLiquido = pu;
                        }

                        valorLiquido = valor + despesas;
                        break;
                    case TipoOperacaoBolsa.Venda:
                    case TipoOperacaoBolsa.VendaDaytrade:
                        if (fator != 0)
                        {
                            puLiquido = pu - (despesas * fator) / quantidade;
                        }
                        else
                        {
                            puLiquido = pu;
                        }

                        valorLiquido = valor - despesas;
                        break;
                }

                operacaoBolsa.ValorLiquido = valorLiquido;
                operacaoBolsa.PULiquido = puLiquido;
            }

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="prioridadeCasamentoVencimento"></param>        
        public void CasaVencimento(int idCliente, DateTime data, PrioridadeCasamentoVencimento prioridadeCasamentoVencimento) {
            
            //Zera as quantidades casadas e resultados de termo e exercício
            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            operacaoBolsaCollection.ZeraValoresCasamamentoTermoExecicio(idCliente, data);
            //

            if (prioridadeCasamentoVencimento == PrioridadeCasamentoVencimento.ExercicioPrimeiro) {
                this.CasaVencimentoExercicio(idCliente, data);
                this.CasaVencimentoTermo(idCliente, data);
            }
            if (prioridadeCasamentoVencimento == PrioridadeCasamentoVencimento.TermoPrimeiro) {
                this.CasaVencimentoTermo(idCliente, data);
                this.CasaVencimentoExercicio(idCliente, data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void CasaVencimentoTermo(int idCliente, DateTime data) {

            GerOperacaoBolsaCollection operacaoBolsaCollectionVenda = new GerOperacaoBolsaCollection();
            GerOperacaoBolsaCollection operacaoBolsaCollectionCompra = new GerOperacaoBolsaCollection();

            // Collection de Operacoes de Venda
            operacaoBolsaCollectionVenda.BuscaOperacaoBolsaVendaCasaVencimentoTermo(idCliente, data);

            for (int i = 0; i < operacaoBolsaCollectionVenda.Count; i++) {
                GerOperacaoBolsa operacaoBolsaVenda = operacaoBolsaCollectionVenda[i];
                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.Value - operacaoBolsaVenda.QuantidadeCasadaExercicio.Value;
                decimal quantidadeVendaCasada = operacaoBolsaVenda.QuantidadeCasadaTermo.Value;
                int idOperacaoVenda = operacaoBolsaVenda.IdOperacao.Value;
                int idTrader = operacaoBolsaVenda.IdTrader.Value;
                string cdAtivoBolsa = operacaoBolsaVenda.CdAtivoBolsa;
                decimal quantidadeDiferenca;

                // Collection de Operacoes de Compra
                if (!operacaoBolsaCollectionCompra.BuscaOperacaoBolsaCompraCasaVencimentoTermo(idCliente, data, idTrader, cdAtivoBolsa)) {
                    break;
                }

                for (int j = 0; j < operacaoBolsaCollectionCompra.Count; j++) {
                    GerOperacaoBolsa operacaoBolsaCompra = operacaoBolsaCollectionCompra[j];

                    // Uma das pontas (operacaoCompra ou operacaoVenda) deve ser LiquidacaoTermo para poder casar
                    if (!(operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.LiquidacaoTermo ||
                           operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.LiquidacaoTermo)) {
                        continue;
                    }

                    decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.Value - operacaoBolsaCompra.QuantidadeCasadaExercicio.Value;
                    decimal quantidadeCompraCasada = operacaoBolsaCompra.QuantidadeCasadaTermo.Value;
                    int idOperacaoCompra = operacaoBolsaCompra.IdOperacao.Value;

                    if (quantidadeCompra == quantidadeCompraCasada) continue;
                    if (quantidadeVenda == quantidadeVendaCasada) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaCasada;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraCasada;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra) {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0) {
                        operacaoBolsaCompra.QuantidadeCasadaTermo = quantidadeDiferenca + quantidadeCompraCasada;
                        operacaoBolsaVenda.QuantidadeCasadaTermo = quantidadeDiferenca + quantidadeVendaCasada;
                        quantidadeVendaCasada += quantidadeDiferenca;
                    }

                    this.ProcessaResultadoVencimentoTermo(operacaoBolsaCompra, operacaoBolsaVenda, quantidadeDiferenca);
                }

                operacaoBolsaCollectionCompra.Save();
            }

            operacaoBolsaCollectionVenda.Save();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void CasaVencimentoExercicio(int idCliente, DateTime data) {

            GerOperacaoBolsaCollection operacaoBolsaCollectionVenda = new GerOperacaoBolsaCollection();
            GerOperacaoBolsaCollection operacaoBolsaCollectionCompra = new GerOperacaoBolsaCollection();

            // Collection de Operacoes de Venda
            operacaoBolsaCollectionVenda.BuscaOperacaoBolsaVendaCasaVencimentoExercicio(idCliente, data);
            for (int i = 0; i < operacaoBolsaCollectionVenda.Count; i++) {
                GerOperacaoBolsa operacaoBolsaVenda = operacaoBolsaCollectionVenda[i];
                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.Value - operacaoBolsaVenda.QuantidadeCasadaTermo.Value;
                decimal quantidadeVendaCasada = operacaoBolsaVenda.QuantidadeCasadaExercicio.Value;
                int idOperacaoVenda = operacaoBolsaVenda.IdOperacao.Value;
                int idTrader = operacaoBolsaVenda.IdTrader.Value;
                string cdAtivoBolsa = operacaoBolsaVenda.CdAtivoBolsa;
                decimal quantidadeDiferenca;

                // Collection de Operacoes de Compra
                if (!operacaoBolsaCollectionCompra.BuscaOperacaoBolsaCompraCasaVencimentoExercicio(idCliente, data, idTrader, cdAtivoBolsa)) {
                    break;
                }

                for (int j = 0; j < operacaoBolsaCollectionCompra.Count; j++) {
                    GerOperacaoBolsa operacaoBolsaCompra = operacaoBolsaCollectionCompra[j];
                    // Uma das pontas (operacaoCompra ou operacaoVenda) deve ser ExercicioOpcao para poder casar
                    if (!(operacaoBolsaCompra.IsOrigemExercicioOpcao() ||
                           operacaoBolsaVenda.IsOrigemExercicioOpcao())) {
                        continue;
                    }

                    decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.Value - operacaoBolsaCompra.QuantidadeCasadaTermo.Value;
                    decimal quantidadeCompraCasada = operacaoBolsaCompra.QuantidadeCasadaExercicio.Value;
                    int idOperacaoCompra = operacaoBolsaCompra.IdOperacao.Value;

                    if (quantidadeCompra == quantidadeCompraCasada) continue;
                    if (quantidadeVenda == quantidadeVendaCasada) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaCasada;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraCasada;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra) {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0) {
                        operacaoBolsaCompra.QuantidadeCasadaExercicio = quantidadeDiferenca + quantidadeCompraCasada;
                        operacaoBolsaVenda.QuantidadeCasadaExercicio = quantidadeDiferenca + quantidadeVendaCasada;
                        quantidadeVendaCasada += quantidadeDiferenca;
                    }

                    this.ProcessaResultadoVencimentoExercicio(operacaoBolsaCompra, operacaoBolsaVenda, quantidadeDiferenca);
                }

                operacaoBolsaCollectionCompra.Save();
            }

            operacaoBolsaCollectionVenda.Save();           
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void TrataExercicioOpcao(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            //
            operacaoBolsaCollection.BuscaOperacaoBolsaCompoeCustoOpcao(idCliente, data);

            for (int i = 0; i < operacaoBolsaCollection.Count; i++) {
                GerOperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                // Altera os Custos das opções.
                this.CompoeCustoOpcao(operacaoBolsa);
                //Baixa as opções exercidas
                this.BaixaOpcaoExercida(operacaoBolsa);
            }
            // Salva todas as Alterações no final
            operacaoBolsaCollection.Save();            
        }

        /// <summary>
        /// Baixa as opcoes que foram exercidas.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        private void BaixaOpcaoExercida(GerOperacaoBolsa gerOperacaoBolsa)
        {
            GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
            gerPosicaoBolsa.Query.Select(gerPosicaoBolsa.Query.IdPosicao, gerPosicaoBolsa.Query.Quantidade);
            gerPosicaoBolsa.Query.Where(gerPosicaoBolsa.Query.IdCliente.Equal(gerOperacaoBolsa.IdCliente.Value),
                                     gerPosicaoBolsa.Query.CdAtivoBolsa.Equal(gerOperacaoBolsa.CdAtivoBolsaOpcao),
                                     gerPosicaoBolsa.Query.IdTrader.Equal(gerOperacaoBolsa.IdTrader.Value),
                                     gerPosicaoBolsa.Query.Quantidade.NotEqual(0));

            if (gerPosicaoBolsa.Query.Load())
            {
                if (gerOperacaoBolsa.Quantidade.Value > Math.Abs(gerOperacaoBolsa.Quantidade.Value))
                {
                    throw new QuantidadeCarteiraInsuficienteException("Gerencial - Quantidade exercida não corresponde à quantidade em posição da opção " + CdAtivoBolsaOpcao);
                }

                if (gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra || gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    if (gerPosicaoBolsa.Quantidade.Value < 0)
                    {
                        throw new QuantidadeCarteiraInsuficienteException("Gerencial - Quantidade exercida na Compra não corresponde à quantidade negativa da posição da opção " + CdAtivoBolsaOpcao);
                    }
                    else
                    {
                        gerPosicaoBolsa.Quantidade -= gerOperacaoBolsa.Quantidade.Value;
                    }
                }
                else if (gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Venda || gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade)
                {
                    if (gerPosicaoBolsa.Quantidade.Value > 0)
                    {
                        throw new QuantidadeCarteiraInsuficienteException("Gerencial - Quantidade exercida na Venda não corresponde à quantidade positiva da posição da opção " + CdAtivoBolsaOpcao);
                    }
                    else
                    {
                        gerPosicaoBolsa.Quantidade += gerOperacaoBolsa.Quantidade.Value;
                    }
                }

                gerPosicaoBolsa.Save();
            }
            else
            {
                throw new QuantidadeCarteiraInsuficienteException("Gerencial - Quantidade exercida não corresponde à quantidade em posição da opção " + CdAtivoBolsaOpcao);
            }

        }

        /// <summary>
        /// OBS: Não Salva a Operação. Apenas calcula os novos valores de Custo da Opção.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        /// <exception cref="ArgumentNullException">Se alguns campos da Operação forem nulos</exception>
        private void CompoeCustoOpcao(GerOperacaoBolsa operacaoBolsa) {
           
            #region ArgumentosNulos - throw ArgumentNullException
            if (!operacaoBolsa.Data.HasValue ||
                !operacaoBolsa.IdTrader.HasValue ||
                !operacaoBolsa.IdCliente.HasValue ||
                !operacaoBolsa.PULiquido.HasValue ||
                String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsaOpcao) ||
                String.IsNullOrEmpty(operacaoBolsa.TipoOperacao)) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("OperacaoBolsa.Data não pode ser null ")
                .Append("OperacaoBolsa.IdAgenteCorretora não pode ser null ")
                .Append("OperacaoBolsa.IdCliente não pode ser null ")
                .Append("OperacaoBolsa.PuLiquido não pode ser null ")
                .Append("OperacaoBolsa.idLocal não pode ser null ")
                .Append("OperacaoBolsa.CdAtivoBolsaOpcao não pode ser null ")
                .Append("OperacaoBolsa.TipoOperacao não pode ser null ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            GerPosicaoBolsaHistorico posicaoBolsaHistorico = new GerPosicaoBolsaHistorico();

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(operacaoBolsa.Data.Value, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            int idCliente = operacaoBolsa.IdCliente.Value;
            int idTrader = operacaoBolsa.IdTrader.Value;
            decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;
            string tipoOperacao = operacaoBolsa.TipoOperacao;
            string cdAtivoBolsaOpcao = operacaoBolsa.CdAtivoBolsaOpcao;
            //
            bool existe = posicaoBolsaHistorico.BuscaPosicaoBolsaHistorico(idCliente, dataAnterior, cdAtivoBolsaOpcao, idTrader);
            decimal puLiquidoOpcao = existe == true ? posicaoBolsaHistorico.PUCusto.Value : 0;

            puLiquidoOperacao += puLiquidoOpcao;
            //
            operacaoBolsa.PULiquido = puLiquidoOperacao;            
        }

        /// <summary>
        ///    Função para Calcular o Estoque das Posições do cliente
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataCalculo">Data usada para processamento</param>        
        public void ProcessaCompra(int idCliente, DateTime dataCalculo) {
            
            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            GerPosicaoBolsa posicaoBolsa = new GerPosicaoBolsa();

            operacaoBolsaCollection.BuscaOperacaoBolsaCompra(idCliente, dataCalculo);
            for (int i = 0; i < operacaoBolsaCollection.Count; i++) {
                GerOperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];                
                int idTrader = operacaoBolsa.IdTrader.Value;
                string cdAtivo = operacaoBolsa.CdAtivoBolsa;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal quantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.Value;
                decimal quantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.Value;
                decimal quantidadeOperacao = quantidade - quantidadeCasadaExercicio - quantidadeCasadaTermo;
                decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;

                if (quantidadeOperacao != 0) {
                    // Tem Posição para o ativo - Atualiza a posição
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivo)) {
                        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        // Comprado
                        if (quantidadePosicao >= 0) {
                            // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                            posicaoBolsa.AtualizaPosicaoCompraComprado(posicaoBolsa.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                        }
                        // Vendido
                        else {
                            // Se quantidade comprada for suficiente para passar de uma posicao negativa 
                            // para positivo
                            if (quantidadeOperacao > Math.Abs(quantidadePosicao)) {
                                // Baixa posicao - Atualizar quantidade                                                        
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoCompra() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemAberturaEmprestimo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemLiquidacaoEmprestimo() && operacaoBolsa.IsTipoDeposito()) {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                posicaoBolsa.AtualizaPosicaoCompraVendido(posicaoBolsa.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                            }
                            else {
                                // Baixa posicao - Atualizar apenas a quantidade
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoCompra() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemAberturaEmprestimo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemLiquidacaoEmprestimo() && operacaoBolsa.IsTipoDeposito()) {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeOperacao);
                            }
                        }
                    }
                    // Não tem posição para o Ativo - Inclusão da nova Posição
                    else {
                        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
                    }
                }
            }           
        }

        /// <summary>
        ///  Atualiza Resultado Realizado da operacao.
        /// </summary>
        /// <param name="operacaoBolsa">Varios Campos devem estar Preenchidos</param>
        /// <param name="posicaoBolsa"></param>
        /// <param name="quantidadeOperacao"></param>
        /// <exception cref="ArgumentNullException">Quando os parametros de entrada necessários estão nulos</exception>
        /// <exception cref="FatorCotacaoNaoCadastradoException">Se não Existir Fator na Data da Operação</exception>
        public void ProcessaResultadoNormal(GerOperacaoBolsa operacaoBolsa, GerPosicaoBolsa posicaoBolsa, decimal quantidadeOperacao) {
            
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsa) ||
                 !operacaoBolsa.IdOperacao.HasValue ||
                 !operacaoBolsa.IdCliente.HasValue ||
                 !operacaoBolsa.Data.HasValue ||
                 !operacaoBolsa.Quantidade.HasValue ||
                 String.IsNullOrEmpty(operacaoBolsa.TipoOperacao) ||
                 !operacaoBolsa.PULiquido.HasValue ||
                 !operacaoBolsa.Pu.HasValue ||
                 !posicaoBolsa.Quantidade.HasValue ||
                 !posicaoBolsa.PUCusto.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsa.CdAtivoBolsa ou ")
                .Append("operacaoBolsa.IdOperacao ou ")
                .Append("operacaoBolsa.IdCliente ou ")
                .Append("operacaoBolsa.Data ou ")
                .Append("operacaoBolsa.Quantidade ou ")
                .Append("operacaoBolsa.PULiquido ou ")
                .Append("operacaoBolsa.PU ou ")
                .Append("posicaoBolsa.Quantidade ou ")
                .Append("posicaoBolsa.PUCustoLiquido");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsa.CdAtivoBolsa, operacaoBolsa.Data.Value)) {
                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + operacaoBolsa.CdAtivoBolsa + " " + operacaoBolsa.Data.Value.ToString("d"));
            }

            decimal quantidadeBase;
            decimal resultado = 0;

            if (Math.Abs(posicaoBolsa.Quantidade.Value) < quantidadeOperacao) {
                quantidadeBase = Math.Abs(posicaoBolsa.Quantidade.Value);
            }
            else {
                quantidadeBase = quantidadeOperacao;
            }

            // Verifica se está vendido
            switch (operacaoBolsa.TipoOperacao) {
                case TipoOperacaoBolsa.Compra:
                case TipoOperacaoBolsa.Deposito:
                    resultado = Math.Round(quantidadeBase *
                        (posicaoBolsa.PUCusto.Value - operacaoBolsa.PULiquido.Value) / fator.Fator.Value, 2);
                    break;
                case TipoOperacaoBolsa.Venda:
                case TipoOperacaoBolsa.Retirada:
                    resultado = Math.Round(quantidadeBase *
                            (operacaoBolsa.PULiquido.Value - posicaoBolsa.PUCusto.Value) / fator.Fator.Value, 2);
                    break;
            }

            GerOperacaoBolsa operacaoBolsaAux = new GerOperacaoBolsa();
            // Atualiza resultado realizado da Operação                                         
            operacaoBolsaAux.IdOperacao = operacaoBolsa.IdOperacao;
            operacaoBolsaAux.AcceptChanges();
            operacaoBolsaAux.ResultadoRealizado = resultado;
            operacaoBolsaAux.Save();           
        }

        /// <summary>
        /// Calcula o resultado quando uma das pontas casadas é exercicio
        /// </summary>
        /// <param name="operacaoBolsaCompra"></param>
        /// <param name="operacaoBolsaVenda"></param>
        /// <param name="quantidadeCasadaExercicio"></param>
        /// <exception cref="ArgumentNullException">
        ///  Se os campos abaixo não possuirem valor
        ///                Se operacaoBolsaCompra.CdAtivoBolsa ou 
        ///                   operacaoBolsaCompra.Data ou 
        ///                   operacaoBolsaCompra.Origem ou
        ///                   operacaoBolsaCompra.QuantidadeCasadaExercicio ou
        ///                   operacaoBolsaCompra.PULiquido ou
        ///                   operacaoBolsaCompra.quantidadeCasadaTermo
        ///                ou
        ///                   operacaoBolsaVenda.Origem
        ///                   operacaoBolsaVenda.QuantidadeCasadaExercicio
        ///                   operacaoBolsaVenda.PULiquido
        ///                   operacaoBolsaVenda.quantidadeCasadaTermo                
        /// 
        /// </exception>
        /// <exception cref="FatorCotacaoNaoCadastradoException">Se não Existir Fator na Data da Operação de Compra</exception>
        private void ProcessaResultadoVencimentoExercicio(GerOperacaoBolsa operacaoBolsaCompra, GerOperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaExercicio) {
           
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsaCompra.CdAtivoBolsa) ||
                 !operacaoBolsaCompra.Data.HasValue ||
                 !operacaoBolsaCompra.Origem.HasValue ||
                 !operacaoBolsaVenda.Origem.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaCompra.PULiquido.HasValue ||
                 !operacaoBolsaVenda.PULiquido.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaTermo.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaTermo.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsaCompra.CdAtivoBolsa ou ")
                .Append("operacaoBolsaCompra.Data ou ")
                .Append("operacaoBolsaCompra.Origem ou ")
                .Append("operacaoBolsaVenda.Origem ou ")
                .Append("operacaoBolsaCompra.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaVenda.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaCompra.PULiquido ou ")
                .Append("operacaoBolsaVenda.PULiquido ou ")
                .Append("operacaoBolsaCompra.quantidadeCasadaTermo ou ")
                .Append("operacaoBolsaVenda.quantidadeCasadaTermo");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            #region fatorInvalido - throw FatorCotacaoNaoCadastradoException
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsaCompra.CdAtivoBolsa, operacaoBolsaCompra.Data.Value)) {
                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + operacaoBolsaCompra.CdAtivoBolsa + " " + operacaoBolsaCompra.Data.Value.ToString("d"));
            }
            #endregion

            if (operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                operacaoBolsaCompra.ResultadoExercicio += Math.Round(quantidadeCasadaExercicio *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value) / fator.Fator.Value, 2);
            }
            else if (operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                     operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                operacaoBolsaVenda.ResultadoExercicio += Math.Round(quantidadeCasadaExercicio *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value) / fator.Fator.Value, 2);
            }           
        }

        /// <summary>
        /// Calcula o Resultado Quando uma das pontas casadas é liquidacao de termo 
        /// </summary>
        /// <param name="operacaoBolsaCompra"></param>
        /// <param name="operacaoBolsaVenda"></param>
        /// <param name="quantidadeCasadaTermo"></param>
        /// <exception cref="ArgumentNullException">
        ///  Se os campos abaixo não possuirem valor
        ///                Se operacaoBolsaCompra.CdAtivoBolsa ou 
        ///                   operacaoBolsaCompra.Data ou 
        ///                   operacaoBolsaCompra.Origem ou
        ///                   operacaoBolsaCompra.QuantidadeCasadaExercicio ou
        ///                   operacaoBolsaCompra.PULiquido ou
        ///                   operacaoBolsaCompra.quantidadeCasadaTermo
        ///                ou
        ///                   operacaoBolsaVenda.Origem
        ///                   operacaoBolsaVenda.QuantidadeCasadaExercicio
        ///                   operacaoBolsaVenda.PULiquido
        ///                   operacaoBolsaVenda.quantidadeCasadaTermo                
        /// 
        /// </exception>
        /// <exception cref="FatorCotacaoNaoCadastradoException">Se não Existir Fator na Data da Operação de Compra</exception>
        private void ProcessaResultadoVencimentoTermo(GerOperacaoBolsa operacaoBolsaCompra, GerOperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaTermo) {
            
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsaCompra.CdAtivoBolsa) ||
                 !operacaoBolsaCompra.Data.HasValue ||
                 !operacaoBolsaCompra.Origem.HasValue ||
                 !operacaoBolsaVenda.Origem.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaCompra.PULiquido.HasValue ||
                 !operacaoBolsaVenda.PULiquido.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaTermo.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaTermo.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsaCompra.CdAtivoBolsa ou ")
                .Append("operacaoBolsaCompra.Data ou ")
                .Append("operacaoBolsaCompra.Origem ou ")
                .Append("operacaoBolsaVenda.Origem ou ")
                .Append("operacaoBolsaCompra.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaVenda.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaCompra.PULiquido ou ")
                .Append("operacaoBolsaVenda.PULiquido ou ")
                .Append("operacaoBolsaCompra.quantidadeCasadaTermo ou ")
                .Append("operacaoBolsaVenda.quantidadeCasadaTermo");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion
            
            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            #region fatorInvalido - throw FatorCotacaoNaoCadastradoException
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsaCompra.CdAtivoBolsa, operacaoBolsaCompra.Data.Value)) {
                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + operacaoBolsaCompra.CdAtivoBolsa + " " + operacaoBolsaCompra.Data.Value.ToString("d"));
            }
            #endregion

            if (operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.LiquidacaoTermo) {
                operacaoBolsaCompra.ResultadoTermo += Math.Round(quantidadeCasadaTermo *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value) / fator.Fator.Value, 2);
            }
            else if (operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.LiquidacaoTermo) {
                operacaoBolsaVenda.ResultadoTermo += Math.Round(quantidadeCasadaTermo *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value) / fator.Fator.Value, 2);
            }           
        }

        /// <summary>
        ///  Função para Calcular o Estoque das Posições do Cliente
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="data">Data usada para processamento</param>        
        public void ProcessaVenda(int idCliente, DateTime data) {

            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            GerPosicaoBolsa posicaoBolsa = new GerPosicaoBolsa();
            //
            operacaoBolsaCollection.BuscaOperacaoBolsaVenda(idCliente, data);
            for (int i = 0; i < operacaoBolsaCollection.Count; i++) {
                GerOperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal quantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.Value;
                decimal quantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.Value;
                decimal quantidadeOperacao = quantidade - quantidadeCasadaExercicio - quantidadeCasadaTermo;
                string cdAtivo = operacaoBolsa.CdAtivoBolsa;
                int idTrader = operacaoBolsa.IdTrader.Value;

                if (quantidadeOperacao != 0) {
                    // Tem Posição para o ativo - Atualiza a posição
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivo)) {
                        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        // Vendido
                        if (quantidadePosicao <= 0) {
                            // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                            posicaoBolsa.AtualizaPosicaoVendaVendido(posicaoBolsa.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                        }
                        // Comprado
                        else {
                            // Se quantidade comprada for suficiente para passar de uma posicao positiva 
                            // para negativa
                            decimal quantidadeOperacaoNegativa = -1 * quantidadeOperacao;
                            if (quantidadeOperacao > quantidadePosicao) {
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoVenda() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoRetirada()) {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                // Baixa posicao - Atualizar quantidade                                                        
                                posicaoBolsa.AtualizaPosicaoVendaComprado(posicaoBolsa.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                            }
                            else {
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoVenda() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoRetirada()) {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                // Baixa posicao - Atualizar apenas a quantidade                            
                                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeOperacaoNegativa);
                            }
                        }
                    }
                    // Não tem posição para o Ativo - Inclusão da nova Posição
                    else {
                        // Quantidade da operação é positiva, mas quantidade da  posição fica negativa
                        operacaoBolsa.Quantidade = -1 * operacaoBolsa.Quantidade;
                        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
                    }
                }
            }           
        }

        /// <summary>
        /// Zera Todos os Resultados (Exercicio, Termo, Normal) na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ZeraResultados(int idCliente, DateTime data) {

            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            //
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.IdOperacao,
                                                 operacaoBolsaCollection.Query.ResultadoExercicio,
                                                 operacaoBolsaCollection.Query.ResultadoRealizado,
                                                 operacaoBolsaCollection.Query.ResultadoTermo);

            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente == idCliente,
                                                operacaoBolsaCollection.Query.Data == data);
            //
            operacaoBolsaCollection.Query.Load();
            //
            foreach (GerOperacaoBolsa operacaoBolsa in operacaoBolsaCollection) {
                operacaoBolsa.ResultadoExercicio = 0;
                operacaoBolsa.ResultadoRealizado = 0;
                operacaoBolsa.ResultadoTermo = 0;
            }

            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Metodo de Insercao na OperacaoBolsa
        /// </summary>
        /// <param name="operacaoBolsa">operacao com os dados a inserir</param>
        /// <exception cref="ArgumentNullException">
        /// Se campos Obrigatórios da inclusão estiverem nulos
        /// operacaoBolsa.idCliente ou operacaoBolsa.IdTrader ou 
        /// operacaoBolsa.cdAtivoBolsa ou operacaoBolsa.TipoMercado ou
        /// operacaoBolsa.TipoOperacao ou operacaoBolsa.Data ou 
        /// operacaoBolsa.Quantidade ou operacaoBolsa.Pu ou
        /// operacaoBolsa.PuLiquido ou operacaoBolsa.Valor ou  
        /// operacaoBolsa.Origem ou operacaoBolsa.cdAtivoBolsaOpcao
        /// forem nulos ou vazio
        /// </exception>
        public void InsereOperacaoBolsa(GerOperacaoBolsa operacaoBolsa) {

            #region ArgumentosNulos - throw ArgumentNullException
            if (!operacaoBolsa.IdCliente.HasValue ||
                !operacaoBolsa.IdTrader.HasValue ||
                String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsa) ||
                String.IsNullOrEmpty(operacaoBolsa.TipoMercado) ||
                String.IsNullOrEmpty(operacaoBolsa.TipoOperacao) ||               
                !operacaoBolsa.Data.HasValue ||
                !operacaoBolsa.Quantidade.HasValue ||
                !operacaoBolsa.Pu.HasValue ||
                !operacaoBolsa.PULiquido.HasValue ||
                !operacaoBolsa.Valor.HasValue ||
                !operacaoBolsa.Origem.HasValue ) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsa.idCliente ou ")
                .Append("operacaoBolsa.idTrader ou ")
                .Append("operacaoBolsa.cdAtivoBolsa ou ")
                .Append("operacaoBolsa.TipoMercado ou ")
                .Append("operacaoBolsa.TipoOperacao ou ")
                .Append("operacaoBolsa.Data ou ")
                .Append("operacaoBolsa.Quantidade ou ")
                .Append("operacaoBolsa.Pu ou ")
                .Append("operacaoBolsa.PULiquido ou ")
                .Append("operacaoBolsa.Valor ou ")
                .Append("operacaoBolsa.Origem ");
                
                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            GerOperacaoBolsa operacaoBolsaInserir = new GerOperacaoBolsa();
            //            
            // Campos Obrigatórios Para a Inserção
            operacaoBolsaInserir.IdCliente = operacaoBolsa.IdCliente;
            operacaoBolsaInserir.IdTrader = operacaoBolsa.IdTrader;
            operacaoBolsaInserir.CdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
            operacaoBolsaInserir.TipoMercado = operacaoBolsa.TipoMercado;
            operacaoBolsaInserir.TipoOperacao = operacaoBolsa.TipoOperacao;
            operacaoBolsaInserir.Data = operacaoBolsa.Data;
            operacaoBolsaInserir.Quantidade = operacaoBolsa.Quantidade;
            operacaoBolsaInserir.Pu = operacaoBolsa.Pu;
            operacaoBolsaInserir.PULiquido = operacaoBolsa.PULiquido;
            operacaoBolsaInserir.Valor = operacaoBolsa.Valor;
            operacaoBolsaInserir.Origem = operacaoBolsa.Origem;
            operacaoBolsaInserir.CdAtivoBolsaOpcao = operacaoBolsa.CdAtivoBolsaOpcao;
            operacaoBolsaInserir.Fonte = operacaoBolsa.Fonte;
            
            // Campos não obrigatórios - se não For Passado Coloca Valor Default            
            operacaoBolsaInserir.ValorLiquido = operacaoBolsa.ValorLiquido.HasValue ? operacaoBolsa.ValorLiquido : 0;
            operacaoBolsaInserir.Despesas = operacaoBolsa.Despesas.HasValue ? operacaoBolsa.Despesas : 0;
            operacaoBolsaInserir.ResultadoRealizado = operacaoBolsa.ResultadoRealizado.HasValue ? operacaoBolsa.ResultadoRealizado : 0;
            operacaoBolsaInserir.QuantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.HasValue ? operacaoBolsa.QuantidadeCasadaTermo : 0;
            operacaoBolsaInserir.QuantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.HasValue ? operacaoBolsa.QuantidadeCasadaExercicio : 0;
            operacaoBolsaInserir.ResultadoTermo = operacaoBolsa.ResultadoTermo.HasValue ? operacaoBolsa.ResultadoTermo : 0;
            operacaoBolsaInserir.ResultadoExercicio = operacaoBolsa.ResultadoExercicio.HasValue ? operacaoBolsa.ResultadoExercicio : 0;                                 
            //                         
            operacaoBolsaInserir.Save();          
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é Primaria
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemPrimaria() {
            return this.Origem == OrigemOperacaoBolsa.Primaria;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoCompra ou ExercicioOpcaoVenda
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcao() {
            return (this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                    this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoVenda
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcaoVenda() {
            return this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoCompra
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcaoCompra() {
            return this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é LiquidacaoTermo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemLiquidacaoTermo() {
            return this.Origem == OrigemOperacaoBolsa.LiquidacaoTermo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é TransferenciaCustodia
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemTransferenciaCustodia() {
            return this.Origem == OrigemOperacaoBolsa.TransferenciaCorretora;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é AberturaEmprestimo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemAberturaEmprestimo() {
            return this.Origem == OrigemOperacaoBolsa.AberturaEmprestimo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é LiquidacaoEmprestimo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemLiquidacaoEmprestimo() {
            return this.Origem == OrigemOperacaoBolsa.LiquidacaoEmprestimo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompra() {
            return this.TipoOperacao == TipoOperacaoBolsa.Compra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVenda() {
            return this.TipoOperacao == TipoOperacaoBolsa.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Retirada
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoRetirada() {
            return this.TipoOperacao == TipoOperacaoBolsa.Retirada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Deposito
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoDeposito() {
            return this.TipoOperacao == TipoOperacaoBolsa.Deposito;
        }

        /// <summary>
        ///Transfere as ordens já casadas (em normais e DT) da OrdemBolsa para GerOperacaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException se fator não estiver cadastrado
        public void CarregaOrdemCasada(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection gerOperacaoBolsaCollectionDeletar = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollectionDeletar.DeletaOperacaoOrdemCasada(idCliente, data);
                        
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.BuscaOrdemBolsaCompleta(idCliente, data);
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                OrdemBolsa ordemBolsa = ordemBolsaCollection[i];
                string cdAtivoBolsa = ordemBolsa.CdAtivoBolsa;

                FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsa + " " + data.ToString("d"));
                }

                int? idTrader = ordemBolsa.IdTrader;

                if (idTrader.HasValue)
                {
                    #region OrdemBolsa
                    int idOrdem = ordemBolsa.IdOrdem.Value;
                    string tipoMercado = ordemBolsa.TipoMercado;
                    string tipoOrdem = ordemBolsa.TipoOrdem;
                    decimal pu = ordemBolsa.Pu.Value;
                    decimal quantidade = ordemBolsa.Quantidade.Value;
                    decimal quantidadeDaytrade = ordemBolsa.QuantidadeDayTrade.Value;
                    decimal quantidadeNormal = quantidade - quantidadeDaytrade;
                    decimal valorNormal = Utilitario.Truncate(((quantidadeNormal * pu) / fator.Fator.Value), 2);
                    decimal valorDaytrade = Utilitario.Truncate(((quantidadeDaytrade * pu) / fator.Fator.Value), 2);
                    decimal corretagem = ordemBolsa.Corretagem.Value;
                    decimal emolumento = ordemBolsa.Emolumento.Value;
                    decimal liquidacaoCBLC = ordemBolsa.LiquidacaoCBLC.Value;
                    decimal registroBolsa = ordemBolsa.RegistroBolsa.Value;
                    decimal registroCBLC = ordemBolsa.RegistroCBLC.Value;
                    decimal percentualDesconto = ordemBolsa.PercentualDesconto.Value;
                    decimal desconto = ordemBolsa.Desconto.Value;
                    int idAgenteLiquidacao = ordemBolsa.IdAgenteLiquidacao.Value;
                    int idAgenteCorretora = ordemBolsa.IdAgenteCorretora.Value;
                    int fonteOrdem = ordemBolsa.Fonte.Value;
                    #endregion

                    //Para ordens com fonte manual, identifica de forma especifica na GerOperacaoBolsa
                    //Para os outros tipos de fonte, vale o mesmo tipo de fonte da OrdemBolsa
                    int fonteOperacao = fonteOrdem;
                    if (fonteOperacao == (int)FonteOrdemBolsa.Manual)
                    {
                        fonteOperacao = (int)FonteOperacaoBolsa.OrdemBolsa;
                    }

                    #region InsereOperacaoBolsa
                    //
                    decimal corretagemDaytrade = 0;
                    decimal emolumentoDaytrade = 0;
                    decimal liquidacaoCBLCDaytrade = 0;
                    decimal registroBolsaDaytrade = 0;
                    decimal registroCBLCDaytrade = 0;
                    //
                    if (quantidadeDaytrade != 0)
                    {
                        /* Proporcionalizo as taxas em relação ao todo da operação */
                        corretagemDaytrade = Utilitario.Truncate(corretagem * (quantidadeDaytrade / quantidade), 2);
                        emolumentoDaytrade = Utilitario.Truncate(emolumento * (quantidadeDaytrade / quantidade), 2);
                        liquidacaoCBLCDaytrade = Utilitario.Truncate(liquidacaoCBLC * (quantidadeDaytrade / quantidade), 2);
                        registroBolsaDaytrade = Utilitario.Truncate(registroBolsa * (quantidadeDaytrade / quantidade), 2);
                        registroCBLCDaytrade = Utilitario.Truncate(registroCBLC * (quantidadeDaytrade / quantidade), 2);
                        decimal despesas = corretagemDaytrade + emolumentoDaytrade + liquidacaoCBLCDaytrade +
                            registroBolsaDaytrade + registroCBLCDaytrade;

                        /* Adiciono 'D' porque a operação é DT / C = CD, V = VD */
                        string tipoOperacao = tipoOrdem + "D";

                        #region InsereOperacao
                        GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                        gerOperacaoBolsa.TipoMercado = tipoMercado;
                        gerOperacaoBolsa.TipoOperacao = tipoOperacao;
                        gerOperacaoBolsa.Data = data;
                        gerOperacaoBolsa.Pu = pu;
                        gerOperacaoBolsa.Valor = valorDaytrade;
                        gerOperacaoBolsa.ValorLiquido = ordemBolsa.IsTipoOrdemCompra()
                                                                ? gerOperacaoBolsa.Valor + despesas
                                                                : gerOperacaoBolsa.Valor - despesas;

                        gerOperacaoBolsa.PULiquido = Utilitario.Truncate((gerOperacaoBolsa.ValorLiquido.Value / quantidadeDaytrade) * fator.Fator.Value, 10);
                        /* Taxas */
                        gerOperacaoBolsa.Despesas = despesas;
                        //
                        gerOperacaoBolsa.Quantidade = quantidadeDaytrade;
                        gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        gerOperacaoBolsa.IdTrader = ordemBolsa.IdTrader;
                        gerOperacaoBolsa.Origem = ordemBolsa.Origem;
                        gerOperacaoBolsa.Fonte = (byte)fonteOperacao;

                        gerOperacaoBolsa.IdCliente = idCliente;
                        gerOperacaoBolsa.CdAtivoBolsaOpcao = ordemBolsa.CdAtivoBolsaOpcao;

                        gerOperacaoBolsa.Save();
                        #endregion
                    }

                    if (quantidadeNormal != 0)
                    {
                        decimal corretagemNormal = corretagem - corretagemDaytrade;
                        decimal emolumentoNormal = emolumento - emolumentoDaytrade;
                        decimal liquidacaoCBLCNormal = liquidacaoCBLC - liquidacaoCBLCDaytrade;
                        decimal registroBolsaNormal = registroBolsa - registroBolsaDaytrade;
                        decimal registroCBLCNormal = registroCBLC - registroCBLCDaytrade;
                        decimal despesas = corretagemNormal + emolumentoNormal + liquidacaoCBLCNormal +
                            registroBolsaNormal + registroCBLCNormal;

                        string tipoOperacao = tipoOrdem;

                        #region InsereOperacao
                        GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                        gerOperacaoBolsa.TipoMercado = tipoMercado;
                        gerOperacaoBolsa.TipoOperacao = tipoOperacao;
                        gerOperacaoBolsa.Data = data;
                        gerOperacaoBolsa.Pu = pu;
                        gerOperacaoBolsa.Valor = valorNormal;
                        gerOperacaoBolsa.ValorLiquido = ordemBolsa.IsTipoOrdemCompra()
                                                                ? gerOperacaoBolsa.Valor + despesas
                                                                : gerOperacaoBolsa.Valor - despesas;
                        gerOperacaoBolsa.PULiquido = Utilitario.Truncate((gerOperacaoBolsa.ValorLiquido.Value / quantidadeNormal) * fator.Fator.Value, 10);

                        /* Taxas */
                        gerOperacaoBolsa.Despesas = despesas;
                        //
                        gerOperacaoBolsa.Quantidade = quantidadeNormal;
                        gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        gerOperacaoBolsa.IdTrader = ordemBolsa.IdTrader;
                        gerOperacaoBolsa.Origem = ordemBolsa.Origem;
                        gerOperacaoBolsa.Fonte = (byte)fonteOperacao;

                        gerOperacaoBolsa.IdCliente = idCliente;
                        gerOperacaoBolsa.CdAtivoBolsaOpcao = ordemBolsa.CdAtivoBolsaOpcao;

                        gerOperacaoBolsa.Save();
                        #endregion
                    }
                    #endregion
                }
            }

        }

        /// <summary>
        /// Método central para deletar operações originadas de Liquidação e/ou Vencimento de Termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaOperacoesLiquidacaoTermo(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollection.Query.Select(gerOperacaoBolsaCollection.Query.IdOperacao);
            gerOperacaoBolsaCollection.Query.Where(gerOperacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                             gerOperacaoBolsaCollection.Query.Data.Equal(data),
                             gerOperacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada),
                             gerOperacaoBolsaCollection.Query.Origem.Equal((int)OrigemOperacaoBolsa.LiquidacaoTermo));
            gerOperacaoBolsaCollection.Query.Load();

            gerOperacaoBolsaCollection.MarkAllAsDeleted();
            gerOperacaoBolsaCollection.Save();
        }
    }
}