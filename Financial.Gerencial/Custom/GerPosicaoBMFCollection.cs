﻿using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

using Financial.BMF.Enums;

namespace Financial.Gerencial {
    public partial class GerPosicaoBMFCollection : esGerPosicaoBMFCollection {

        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBMFCollection));

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdPosicao, CdAtivoBMF, Serie, Quantidade, 
        /// ValorMercado, ResultadoRealizar, PUMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBMF(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.Quantidade,
                         this.Query.ValorMercado, this.Query.ResultadoRealizar, this.Query.PUMercado,
                         this.Query.PUCusto)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto GerPosicaoBMFCollection com o campo IdPosicao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaPosicaoBMFVencimentoDia(int idCliente, DateTime dataVencimento) {     
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento == dataVencimento);

            this.Query.Load();           
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com todos os campos de PosicaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBMFCompleta(int idCliente) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdPosicao, CdAtivoBMF, Serie, IdTrader, 
        /// TipoMercado, Quantidade
        /// Busca posições de futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>        
        public void BuscaPosicaoBMFCalculoAjuste(int idCliente) {
            const string DLA = "DLA";

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdTrader,
                         this.Query.TipoMercado, this.Query.Quantidade)
                 .Where(this.Query.IdCliente == idCliente &
                       ( this.Query.TipoMercado == TipoMercadoBMF.Futuro | this.Query.CdAtivoBMF == DLA) );
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as posições do cliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoBMF(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao)
                 .Where(this.Query.IdCliente == idCliente);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Insere novas posições na GerPosicaoBMF a partir de posições históricas (GerPosicaoBMFHistorico) da data informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoBMFHistorico(int idCliente, DateTime data)
        {
            this.QueryReset();
            GerPosicaoBMFHistoricoCollection gerPosicaoBMFHistoricoCollection = new GerPosicaoBMFHistoricoCollection();
            gerPosicaoBMFHistoricoCollection.BuscaPosicaoBMFHistoricoCompleta(idCliente, data);
            for (int i = 0; i < gerPosicaoBMFHistoricoCollection.Count; i++)
            {
                GerPosicaoBMFHistorico gerPosicaoBMFHistorico = gerPosicaoBMFHistoricoCollection[i];

                // Copia para PosicaoBMF
                GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();
                gerPosicaoBMF.IdPosicao = gerPosicaoBMFHistorico.IdPosicao;
                gerPosicaoBMF.IdCliente = gerPosicaoBMFHistorico.IdCliente;
                gerPosicaoBMF.IdTrader = gerPosicaoBMFHistorico.IdTrader;
                gerPosicaoBMF.CdAtivoBMF = gerPosicaoBMFHistorico.CdAtivoBMF;
                gerPosicaoBMF.Serie = gerPosicaoBMFHistorico.Serie;
                gerPosicaoBMF.TipoMercado = gerPosicaoBMFHistorico.TipoMercado;
                gerPosicaoBMF.DataVencimento = gerPosicaoBMFHistorico.DataVencimento;
                gerPosicaoBMF.PUMercado = gerPosicaoBMFHistorico.PUMercado;
                gerPosicaoBMF.PUCusto = gerPosicaoBMFHistorico.PUCusto;
                gerPosicaoBMF.Quantidade = gerPosicaoBMFHistorico.Quantidade;
                gerPosicaoBMF.ValorMercado = gerPosicaoBMFHistorico.ValorMercado;
                gerPosicaoBMF.ResultadoRealizar = gerPosicaoBMFHistorico.ResultadoRealizar;
                //
                this.AttachEntity(gerPosicaoBMF);
            }

            this.Save();
        }

        /// <summary>
        /// Insere novas posições na GerPosicaoBMF a partir de posições históricas (GerPosicaoBMFAbertura) da data informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoBMFAbertura(int idCliente, DateTime data)
        {
            this.QueryReset();
            GerPosicaoBMFAberturaCollection gerPosicaoBMFAberturaCollection = new GerPosicaoBMFAberturaCollection();
            gerPosicaoBMFAberturaCollection.BuscaPosicaoBMFAberturaCompleta(idCliente, data);
            for (int i = 0; i < gerPosicaoBMFAberturaCollection.Count; i++)
            {
                GerPosicaoBMFAbertura gerPosicaoBMFAbertura = gerPosicaoBMFAberturaCollection[i];

                // Copia para PosicaoBMF
                GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();
                gerPosicaoBMF.IdPosicao = gerPosicaoBMFAbertura.IdPosicao;
                gerPosicaoBMF.IdCliente = gerPosicaoBMFAbertura.IdCliente;
                gerPosicaoBMF.IdTrader = gerPosicaoBMFAbertura.IdTrader;
                gerPosicaoBMF.CdAtivoBMF = gerPosicaoBMFAbertura.CdAtivoBMF;
                gerPosicaoBMF.Serie = gerPosicaoBMFAbertura.Serie;
                gerPosicaoBMF.TipoMercado = gerPosicaoBMFAbertura.TipoMercado;
                gerPosicaoBMF.DataVencimento = gerPosicaoBMFAbertura.DataVencimento;
                gerPosicaoBMF.PUMercado = gerPosicaoBMFAbertura.PUMercado;
                gerPosicaoBMF.PUCusto = gerPosicaoBMFAbertura.PUCusto;
                gerPosicaoBMF.Quantidade = gerPosicaoBMFAbertura.Quantidade;
                gerPosicaoBMF.ValorMercado = gerPosicaoBMFAbertura.ValorMercado;
                gerPosicaoBMF.ResultadoRealizar = gerPosicaoBMFAbertura.ResultadoRealizar;
                //
                this.AttachEntity(gerPosicaoBMF);
            }

            this.Save();
        }
       
    }
}