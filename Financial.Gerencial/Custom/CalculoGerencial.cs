﻿using System;
using System.Data;
using System.Collections.Generic;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

using Financial.Util;

using Financial.Bolsa;
using Financial.BMF;

using Financial.Common.Enums;
using Financial.Gerencial.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Investidor;
using Financial.Common;

namespace Financial.Gerencial {
    public partial class CalculoGerencial : esCalculoGerencial {
        private static readonly ILog log = LogManager.GetLogger(typeof(CalculoGerencial));

        /// <summary>
        /// Conversão do Tipo MercadoBolsa Para TipoMercadoGerencial
        /// </summary>
        /// <param name="tipoMercado">Tipo Mercado de Bolsa</param>
        /// <returns>TipoMercadoGerencial</returns>
        /// <exception cref="ArgumentException">Tipo Mercado Bolsa Não Mapeado</exception>
        private TipoMercadoGerencial ConverteTipoMercadoBolsa(string tipoMercadoBolsa) {

            List<string> tipoMercadoPossiveis = TipoMercadoBolsa.Values();
            if (!tipoMercadoPossiveis.Contains(tipoMercadoBolsa)) {
                throw new ArgumentException("TipoMercadoBolsa: " + tipoMercadoBolsa + " invalido");
            }

            TipoMercadoGerencial tipoMercadoRetorno;

            #region Define TipoMercado Gerencial
            switch (tipoMercadoBolsa) {
                case TipoMercadoBolsa.MercadoVista: tipoMercadoRetorno = TipoMercadoGerencial.Acoes;
                    break;
                case TipoMercadoBolsa.OpcaoCompra: tipoMercadoRetorno = TipoMercadoGerencial.OpcaoCompraBolsa;
                    break;
                case TipoMercadoBolsa.OpcaoVenda: tipoMercadoRetorno = TipoMercadoGerencial.OpcaoVendaBolsa;
                    break;
                case TipoMercadoBolsa.Termo: tipoMercadoRetorno = TipoMercadoGerencial.TermoBolsa;
                    break;
                case TipoMercadoBolsa.Futuro: tipoMercadoRetorno = TipoMercadoGerencial.FuturoBolsa;
                    break;
                default: throw new Exception("Mapeamento Tipo Mercado Bolsa Inválido");
            }
            #endregion

            return tipoMercadoRetorno;
        }

        /// <summary>
        /// Conversão do Tipo MercadoBMF Para TipoMercadoGerencial
        /// </summary>
        /// <param name="tipoMercado">Tipo Mercado de BMF</param>
        /// <returns>TipoMercadoGerencial</returns>
        /// <exception cref="ArgumentException">Tipo Mercado BMF Não Mapeado</exception>
        private TipoMercadoGerencial ConverteTipoMercadoBMF(int tipoMercadoBMF) {

            List<int> tipoMercadoPossiveis = TipoMercadoBMF.Values();
            if (!tipoMercadoPossiveis.Contains(tipoMercadoBMF)) {
                throw new ArgumentException("TipoMercadoBMF: " + tipoMercadoBMF + " invalido");
            }

            TipoMercadoGerencial tipoMercadoRetorno;

            #region Define TipoMercado Gerencial
            switch (tipoMercadoBMF) {
                case TipoMercadoBMF.Disponivel: tipoMercadoRetorno = TipoMercadoGerencial.DisponivelBMF;
                    break;
                case TipoMercadoBMF.Futuro: tipoMercadoRetorno = TipoMercadoGerencial.FuturoBMF;
                    break;
                case TipoMercadoBMF.OpcaoDisponivel: tipoMercadoRetorno = TipoMercadoGerencial.OpcaoDisponivelBMF;
                    break;
                case TipoMercadoBMF.OpcaoFuturo: tipoMercadoRetorno = TipoMercadoGerencial.OpcaoFuturoBMF; ;
                    break;
                case TipoMercadoBMF.Termo: tipoMercadoRetorno = TipoMercadoGerencial.TermoBMF;
                    break;
                default: throw new Exception("Mapeamento Tipo Mercado BMF Inválido");
            }
            #endregion

            return tipoMercadoRetorno;
        }

        /// <summary>
        /// Deleta todos os registros da CalculoGerencial dado o cliente e data passados (DataCalculo.GreaterThanOrEqual(data)).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaGerencialDataMaiorIgual(int idCliente, DateTime data)
        {
            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                         calculoGerencialCollection.Query.DataCalculo.GreaterThanOrEqual(data));
            calculoGerencialCollection.Query.Load();
            calculoGerencialCollection.MarkAllAsDeleted();
            calculoGerencialCollection.Save();
        }

        /// <summary>
        /// Deleta todos os registros da CalculoGerencial dado o cliente e data passados (DataCalculo.GreaterThan(data)).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaGerencialDataMaior(int idCliente, DateTime data)
        {
            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                         calculoGerencialCollection.Query.DataCalculo.GreaterThan(data));
            calculoGerencialCollection.Query.Load();
            calculoGerencialCollection.MarkAllAsDeleted();
            calculoGerencialCollection.Save();
        }

        /// <summary>
        /// Deleta todos os registros da CalculoGerencial onde os valores estejam todos zerados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void DeletaGerencialZerado(int idCliente, DateTime data)
        {
            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                         calculoGerencialCollection.Query.DataCalculo.Equal(data),
                                         calculoGerencialCollection.Query.Quantidade.Equal(0),
                                         calculoGerencialCollection.Query.ValorMercado.Equal(0),
                                         calculoGerencialCollection.Query.ResultadoRealizar.Equal(0),
                                         calculoGerencialCollection.Query.ResultadoDayTrade.Equal(0),
                                         calculoGerencialCollection.Query.ResultadoNormal.Equal(0),
                                         calculoGerencialCollection.Query.ResultadoAcumulado.Equal(0));
            calculoGerencialCollection.Query.Load();
            calculoGerencialCollection.MarkAllAsDeleted();
            calculoGerencialCollection.Save();    
        }

        /// <summary>
        /// Zera resultado realizar, normal, DT, resultado dia e valorizacao dia para eliminar eventuais lixos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ZeraValoresGerencialDia(int idCliente, DateTime data)
        {
            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                         calculoGerencialCollection.Query.DataCalculo.Equal(data));
            calculoGerencialCollection.Query.Load();

            foreach (CalculoGerencial calculoGerencial in calculoGerencialCollection)
            {
                calculoGerencial.ResultadoDayTrade = 0;
                calculoGerencial.ResultadoNormal = 0;
                calculoGerencial.ResultadoRealizar = 0;
                calculoGerencial.ResultadoDia = 0;
                calculoGerencial.ValorizacaoDia = 0;
            }

            calculoGerencialCollection.Save();
        }

        /// <summary>
        /// Zera a quantidade, os PUs e o valor mercado de ativos que não estejam presentes em GerPosicaoBolsa nem GerPosicaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ZeraQuantidadeBaixada(int idCliente, DateTime data)
        {
            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                                   calculoGerencialCollection.Query.DataCalculo.Equal(data));
            calculoGerencialCollection.Query.Load();

            foreach (CalculoGerencial calculoGerencial in calculoGerencialCollection)
            {
                bool achouPosicao = false;
                int idTrader = calculoGerencial.IdTrader.Value;
                string cdAtivo = calculoGerencial.CdAtivo;
                DateTime? dataVencimento = calculoGerencial.DataVencimento;

                byte tipoMercado = calculoGerencial.TipoMercado.Value;

                //Procura em GerPosicaoBolsa
                if (tipoMercado == (byte)TipoMercadoGerencial.Acoes ||
                    tipoMercado == (byte)TipoMercadoGerencial.FuturoBolsa ||
                    tipoMercado == (byte)TipoMercadoGerencial.OpcaoCompraBolsa ||
                    tipoMercado == (byte)TipoMercadoGerencial.OpcaoVendaBolsa ||
                    tipoMercado == (byte)TipoMercadoGerencial.TermoBolsa)
                {
                    GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
                    gerPosicaoBolsa.Query.Select(gerPosicaoBolsa.Query.IdPosicao);
                    gerPosicaoBolsa.Query.Where(gerPosicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                gerPosicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivo),
                                                gerPosicaoBolsa.Query.IdTrader.Equal(idTrader),
                                                gerPosicaoBolsa.Query.Quantidade.NotEqual(0));
                    if (gerPosicaoBolsa.Query.Load())
                    {
                        achouPosicao = true;
                    }
                }

                //Procura em PosicaoTermoBolsa
                if (!achouPosicao &&
                    tipoMercado == (byte)TipoMercadoGerencial.TermoBolsa)
                {
                    PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                    posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.IdPosicao);
                    posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivo),
                                                posicaoTermoBolsaCollection.Query.IdTrader.Equal(idTrader),
                                                posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
                    posicaoTermoBolsaCollection.Query.Load();

                    if (posicaoTermoBolsaCollection.Count > 0)
                    {
                        achouPosicao = true;
                    }
                }

                //Procura em PosicaoEmprestimoBolsa - Tratamento especial!
                bool tomado = false;
                if (!achouPosicao && (cdAtivo.Contains("Tomado") || cdAtivo.Contains("Doado")))
                {
                    if (cdAtivo.Contains("Tomado"))
                    {
                        tomado = true;
                        cdAtivo = cdAtivo.Replace(" - Tomado", "");
                    }
                    else
                    {
                        cdAtivo = cdAtivo.Replace(" - Doado", "");
                    }

                    if (!achouPosicao)
                    {
                        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.IdPosicao);
                        posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                    posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivo),
                                                    posicaoEmprestimoBolsaCollection.Query.IdTrader.Equal(idTrader),
                                                    posicaoEmprestimoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                    posicaoEmprestimoBolsaCollection.Query.DataVencimento.Equal(dataVencimento.Value));

                        if (tomado)
                        {
                            posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador));
                        }
                        else
                        {
                            posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
                        }

                        posicaoEmprestimoBolsaCollection.Query.Load();

                        if (posicaoEmprestimoBolsaCollection.Count > 0)
                        {
                            achouPosicao = true;
                        }
                    }
                }


                //Procura em GerPosicaoBMF
                if (!achouPosicao &&
                    (tipoMercado == (byte)TipoMercadoGerencial.DisponivelBMF ||
                     tipoMercado == (byte)TipoMercadoGerencial.FuturoBMF ||
                     tipoMercado == (byte)TipoMercadoGerencial.OpcaoDisponivelBMF ||
                     tipoMercado == (byte)TipoMercadoGerencial.OpcaoFuturoBMF ||
                     tipoMercado == (byte)TipoMercadoGerencial.TermoBMF))
                {
                    string cdAtivoBMF = cdAtivo.Substring(0, 3);
                    string serie = cdAtivo.Replace(cdAtivoBMF, "");

                    GerPosicaoBMF gerPosicaoBMF = new GerPosicaoBMF();
                    gerPosicaoBMF.Query.Select(gerPosicaoBMF.Query.IdPosicao);
                    gerPosicaoBMF.Query.Where(gerPosicaoBMF.Query.IdCliente.Equal(idCliente),
                                                gerPosicaoBMF.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                gerPosicaoBMF.Query.Serie.Equal(serie),
                                                gerPosicaoBMF.Query.IdTrader.Equal(idTrader),
                                                gerPosicaoBMF.Query.Quantidade.NotEqual(0));
                    if (gerPosicaoBMF.Query.Load())
                    {
                        achouPosicao = true;
                    }
                }

                if (!achouPosicao)
                {
                    calculoGerencial.Quantidade = 0;
                    calculoGerencial.PUCusto = 0;
                    calculoGerencial.PUMercado = 0;
                    calculoGerencial.ValorMercado = 0;
                    calculoGerencial.ResultadoRealizar = 0;

                    if (calculoGerencial.ResultadoDayTrade.Value == 0 && calculoGerencial.ResultadoNormal.Value == 0)
                    {
                        calculoGerencial.ResultadoDia = 0;
                    }
                }
            }

            calculoGerencialCollection.Save();
        }

        /// <summary>
        /// Insert/Update CalculoGerencial de Acordo com GerPosicaoBolsa e GerOperacaoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private void CalculoGerencialDiarioBolsa(int idCliente, DateTime data)
        {
            DateTime dataAnterior = new DateTime();
            dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            #region Pega idMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdMoeda);
            cliente.LoadByPrimaryKey(campos, idCliente);
            int idMoeda = cliente.IdMoeda.Value;
            #endregion

            #region Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoeda == (int)ListaMoedaFixo.Dolar)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }
            #endregion

            #region Insert/Update CalculoGerencial de Acordo com GerPosicaoBolsa
            #region Procura CalculoGerencial na GerPosicaoBolsa
            GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
            gerPosicaoBolsaCollection.Query
                            .Select(gerPosicaoBolsaCollection.Query.IdTrader,
                                    gerPosicaoBolsaCollection.Query.CdAtivoBolsa,
                                    gerPosicaoBolsaCollection.Query.DataVencimento,
                                    gerPosicaoBolsaCollection.Query.TipoMercado,
                                    gerPosicaoBolsaCollection.Query.Quantidade.Sum(),
                                    gerPosicaoBolsaCollection.Query.ValorMercado.Sum(),
                                    gerPosicaoBolsaCollection.Query.ResultadoRealizar.Sum(),
                                    gerPosicaoBolsaCollection.Query.PUMercado.Avg(),
                                    gerPosicaoBolsaCollection.Query.PUCusto.Avg())
                            .Where(gerPosicaoBolsaCollection.Query.IdCliente == idCliente)
                            .GroupBy(gerPosicaoBolsaCollection.Query.CdAtivoBolsa,
                                     gerPosicaoBolsaCollection.Query.IdTrader,
                                     gerPosicaoBolsaCollection.Query.TipoMercado,
                                     gerPosicaoBolsaCollection.Query.DataVencimento);
            //                            
            gerPosicaoBolsaCollection.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            //
            
            for (int i = 0; i < gerPosicaoBolsaCollection.Count; i++)
            {
                GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                #region Trata multi-moeda (calcula fator de conversão)
                decimal fatorConversao = 1;
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, gerPosicaoBolsa.CdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoeda != idMoedaAtivo)
                {
                    if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }
                }
                #endregion

                if (c.LoadByPrimaryKey(idCliente, gerPosicaoBolsa.IdTrader.Value, gerPosicaoBolsa.CdAtivoBolsa, data))
                {
                    #region Calculo da valorização/desvalorização dia
                    decimal valorizacaoDia = 0;
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    if (cotacaoBolsa.BuscaCotacaoBolsa(gerPosicaoBolsa.CdAtivoBolsa, dataAnterior))
                    {
                        valorizacaoDia = Utilitario.Truncate(gerPosicaoBolsa.Quantidade.Value * 
                                                        (gerPosicaoBolsa.PUMercado.Value - cotacaoBolsa.PUFechamento.Value), 2);                        
                    }
                    #endregion

                    #region Se existe CalculoGerencial na GerPosicaoBolsa - Faz Update
                    c.Quantidade = gerPosicaoBolsa.Quantidade;
                    c.PUCusto = gerPosicaoBolsa.PUCusto * fatorConversao;
                    c.PUMercado = Math.Round(gerPosicaoBolsa.PUMercado.Value * fatorConversao, 2);
                    c.ValorMercado = Math.Round(gerPosicaoBolsa.ValorMercado.Value * fatorConversao, 2);
                    c.ResultadoRealizar = Math.Round(gerPosicaoBolsa.ResultadoRealizar.Value * fatorConversao, 2);
                    c.ResultadoDayTrade = 0;
                    c.ResultadoNormal = 0;
                    c.ValorizacaoDia = Math.Round(valorizacaoDia, 2);
                    c.ResultadoDia = 0;
                    //
                    calculoGerencialCollection.AttachEntity(c);
                    #endregion
                }
                else
                {                    
                    #region Se não Existe CalculoGerencial na GerPosicaoBolsa - Faz Insert
                    CalculoGerencial calculoGerencialInsert = calculoGerencialCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerPosicaoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerPosicaoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(gerPosicaoBolsa.TipoMercado);
                    calculoGerencialInsert.DataVencimento = gerPosicaoBolsa.DataVencimento;
                    calculoGerencialInsert.Quantidade = gerPosicaoBolsa.Quantidade;
                    calculoGerencialInsert.PUCusto = gerPosicaoBolsa.PUCusto * fatorConversao;

                    calculoGerencialInsert.PUMercado = Math.Round(gerPosicaoBolsa.PUMercado.Value * fatorConversao, 2);
                    calculoGerencialInsert.ValorMercado = Math.Round(gerPosicaoBolsa.ValorMercado.Value * fatorConversao, 2);
                    calculoGerencialInsert.ResultadoRealizar = Math.Round(gerPosicaoBolsa.ResultadoRealizar.Value * fatorConversao, 2);
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = 0;
                    #endregion
                }
            }

            calculoGerencialCollection.Save();
            //
            #endregion

            #region Insert/Update CalculoGerencial de Acordo com PosicaoTermoBolsa
            #region Procura CalculoGerencial na PosicaoTermoBolsa
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.Query
                            .Select(posicaoTermoBolsaCollection.Query.IdTrader,
                                    posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                    posicaoTermoBolsaCollection.Query.DataVencimento,
                                    posicaoTermoBolsaCollection.Query.Quantidade.Sum(),
                                    posicaoTermoBolsaCollection.Query.ValorMercado.Sum(),
                                    posicaoTermoBolsaCollection.Query.ValorTermoLiquido.Sum(),
                                    posicaoTermoBolsaCollection.Query.PUMercado.Avg(),
                                    posicaoTermoBolsaCollection.Query.PUTermoLiquido.Avg())
                            .Where(posicaoTermoBolsaCollection.Query.IdCliente == idCliente,
                                   posicaoTermoBolsaCollection.Query.IdTrader.IsNotNull())
                            .GroupBy(posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                     posicaoTermoBolsaCollection.Query.IdTrader,
                                     posicaoTermoBolsaCollection.Query.DataVencimento);
            //                            
            posicaoTermoBolsaCollection.Query.Load();
            #endregion

            calculoGerencialCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                if (c.LoadByPrimaryKey(idCliente, posicaoTermoBolsa.IdTrader.Value, posicaoTermoBolsa.CdAtivoBolsa, data))
                {
                    #region Calculo da valorização/desvalorização dia
                    decimal valorizacaoDia = 0;
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    if (cotacaoBolsa.BuscaCotacaoBolsa(posicaoTermoBolsa.CdAtivoBolsa, dataAnterior))
                    {
                        valorizacaoDia = Utilitario.Truncate(posicaoTermoBolsa.Quantidade.Value *
                                                        (posicaoTermoBolsa.PUMercado.Value - cotacaoBolsa.PUFechamento.Value), 2);
                    }
                    #endregion

                    #region Se existe CalculoGerencial na GerPosicaoBolsa - Faz Update
                    c.Quantidade = posicaoTermoBolsa.Quantidade;
                    c.PUCusto = posicaoTermoBolsa.PUTermoLiquido;
                    c.PUMercado = posicaoTermoBolsa.PUMercado;
                    c.ValorMercado = posicaoTermoBolsa.ValorMercado;

                    decimal resultadoTermo = Utilitario.Truncate(posicaoTermoBolsa.Quantidade.Value * (posicaoTermoBolsa.PUMercado.Value -
                                                    posicaoTermoBolsa.PUTermoLiquido.Value), 2);

                    c.ResultadoRealizar = resultadoTermo;
                    c.ResultadoDayTrade = 0;
                    c.ResultadoNormal = 0;
                    c.ValorizacaoDia = valorizacaoDia;
                    //
                    calculoGerencialCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerPosicaoBolsa - Faz Insert
                    CalculoGerencial calculoGerencialInsert = calculoGerencialCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = posicaoTermoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = posicaoTermoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(TipoMercadoBolsa.Termo);
                    calculoGerencialInsert.DataVencimento = posicaoTermoBolsa.DataVencimento;
                    calculoGerencialInsert.Quantidade = posicaoTermoBolsa.Quantidade;
                    calculoGerencialInsert.PUCusto = posicaoTermoBolsa.PUTermoLiquido.Value;

                    calculoGerencialInsert.PUMercado = posicaoTermoBolsa.PUMercado;
                    calculoGerencialInsert.ValorMercado = posicaoTermoBolsa.ValorMercado;

                    decimal resultadoTermo = Utilitario.Truncate(posicaoTermoBolsa.Quantidade.Value * (posicaoTermoBolsa.PUMercado.Value - 
                                                    posicaoTermoBolsa.PUTermoLiquido.Value), 2);

                    calculoGerencialInsert.ResultadoRealizar = resultadoTermo;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = 0;
                    #endregion
                }
            }

            calculoGerencialCollection.Save();
            //
            #endregion

            #region Insert/Update CalculoGerencial de Acordo com PosicaoEmprestimoBolsa
            #region Procura CalculoGerencial na PosicaoEmprestimoBolsa
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.Query
                            .Select(posicaoEmprestimoBolsaCollection.Query.IdTrader,
                                    posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                    posicaoEmprestimoBolsaCollection.Query.DataVencimento,
                                    posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo,
                                    posicaoEmprestimoBolsaCollection.Query.PUMercado.Avg(),
                                    posicaoEmprestimoBolsaCollection.Query.PULiquidoOriginal.Avg(),
                                    posicaoEmprestimoBolsaCollection.Query.Quantidade.Sum(),
                                    posicaoEmprestimoBolsaCollection.Query.ValorMercado.Sum(),
                                    posicaoEmprestimoBolsaCollection.Query.ValorBase.Sum(),
                                    posicaoEmprestimoBolsaCollection.Query.ValorCorrigidoJuros.Sum())
                            .Where(posicaoEmprestimoBolsaCollection.Query.IdCliente == idCliente,
                                   posicaoEmprestimoBolsaCollection.Query.IdTrader.IsNotNull())
                            .GroupBy(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo,
                                     posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                     posicaoEmprestimoBolsaCollection.Query.IdTrader,
                                     posicaoEmprestimoBolsaCollection.Query.DataVencimento);
            //                            
            posicaoEmprestimoBolsaCollection.Query.Load();
            #endregion

            calculoGerencialCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++)
            {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                string cdAtivoEmprestimo = "";
                if (posicaoEmprestimoBolsa.PontaEmprestimo.Value == (byte)PontaEmprestimoBolsa.Doador)
                {
                    cdAtivoEmprestimo = posicaoEmprestimoBolsa.CdAtivoBolsa + " - Doado " + posicaoEmprestimoBolsa.DataVencimento.Value.ToShortDateString();
                }
                else
                {
                    cdAtivoEmprestimo = posicaoEmprestimoBolsa.CdAtivoBolsa + " - Tomado" + posicaoEmprestimoBolsa.DataVencimento.Value.ToShortDateString();
                }

                if (c.LoadByPrimaryKey(idCliente, posicaoEmprestimoBolsa.IdTrader.Value, cdAtivoEmprestimo, data))
                {
                    #region Calculo da valorização/desvalorização dia
                    PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                    posicaoEmprestimoBolsaHistorico.Query
                                    .Select(posicaoEmprestimoBolsaHistorico.Query.ValorBase.Sum(),
                                            posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoJuros.Sum())
                                    .Where(posicaoEmprestimoBolsaCollection.Query.IdCliente == idCliente,
                                           posicaoEmprestimoBolsaCollection.Query.IdTrader == posicaoEmprestimoBolsa.IdTrader.Value,
                                           posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo == posicaoEmprestimoBolsa.PontaEmprestimo.Value,
                                           posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa == posicaoEmprestimoBolsa.CdAtivoBolsa,
                                           posicaoEmprestimoBolsaCollection.Query.DataVencimento == posicaoEmprestimoBolsa.DataVencimento.Value);                                    
                    //                            
                    posicaoEmprestimoBolsaHistorico.Query.Load();

                    decimal valorBaseAnterior = 0;
                    decimal totalJurosAnterior = 0;
                    if (posicaoEmprestimoBolsaHistorico.ValorBase.HasValue)
                    {
                        valorBaseAnterior = posicaoEmprestimoBolsaHistorico.ValorBase.Value;
                        totalJurosAnterior = posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value - posicaoEmprestimoBolsaHistorico.ValorBase.Value;
                    }

                    decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;
                    decimal totalJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value - posicaoEmprestimoBolsa.ValorBase.Value;

                    decimal jurosDia = 0;
                    if (valorBaseAnterior == 0)
                    {
                        jurosDia = totalJuros - totalJurosAnterior;
                    }
                    else
                    {
                        jurosDia = Math.Round((totalJuros - totalJurosAnterior) * (valorBase / valorBaseAnterior), 2);
                    }                    

                    if (posicaoEmprestimoBolsa.PontaEmprestimo.Value == (byte)PontaEmprestimoBolsa.Tomador)
                    {
                        jurosDia = jurosDia * -1;
                    }
                    #endregion

                    #region Se existe CalculoGerencial na PosicaoEmprestimoBolsa - Faz Update
                    c.Quantidade = posicaoEmprestimoBolsa.Quantidade;
                    c.PUCusto = posicaoEmprestimoBolsa.PULiquidoOriginal;
                    c.PUMercado = posicaoEmprestimoBolsa.PUMercado;
                    c.ValorMercado = posicaoEmprestimoBolsa.ValorMercado;
                    c.ResultadoRealizar = 0;
                    c.ResultadoDayTrade = 0;
                    c.ResultadoNormal = 0;
                    c.ResultadoDia = jurosDia;
                    //
                    calculoGerencialCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na PosicaoEmprestimoBolsa - Faz Insert
                    CalculoGerencial calculoGerencialInsert = calculoGerencialCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = cdAtivoEmprestimo;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(TipoMercadoBolsa.MercadoVista);
                    calculoGerencialInsert.DataVencimento = posicaoEmprestimoBolsa.DataVencimento;
                    calculoGerencialInsert.Quantidade = posicaoEmprestimoBolsa.Quantidade;
                    calculoGerencialInsert.PUCusto = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;

                    calculoGerencialInsert.PUMercado = posicaoEmprestimoBolsa.PUMercado;
                    calculoGerencialInsert.ValorMercado = posicaoEmprestimoBolsa.ValorMercado;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = 0;
                    #endregion
                }
            }

            calculoGerencialCollection.Save();
            //
            #endregion

            #region Insert/Update CalculoGerencial de Acordo com GerOperacaoBolsa
            #region GerOperacaoBolsa com TipoOperacao != CompraDayTrade/VendaDayTrade (Exclui Liquidacao de Termo, Operacoes com Emprestimo)
            #region Procura CalculoGerencial com os Dados de GerOperacaoBolsa (Exclui Liquidacao de Termo)
            // TipoOperacao != CompraDayTrade e != VendaDayTrade
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollection.Query
                            .Select(gerOperacaoBolsaCollection.Query.IdTrader,
                                    gerOperacaoBolsaCollection.Query.CdAtivoBolsa,
                                    gerOperacaoBolsaCollection.Query.TipoMercado,
                                    gerOperacaoBolsaCollection.Query.TipoOperacao,
                                    gerOperacaoBolsaCollection.Query.ValorLiquido.Sum(),
                                    (gerOperacaoBolsaCollection.Query.ResultadoRealizado.Sum() +
                                      gerOperacaoBolsaCollection.Query.ResultadoTermo.Sum() +
                                      gerOperacaoBolsaCollection.Query.ResultadoExercicio.Sum()
                                    ).As("Resultado"))
                            .Where(gerOperacaoBolsaCollection.Query.IdCliente == idCliente &
                                   gerOperacaoBolsaCollection.Query.Data.Equal(data) & 
                                   gerOperacaoBolsaCollection.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                                   gerOperacaoBolsaCollection.Query.Origem.NotIn(OrigemOperacaoBolsa.LiquidacaoTermo, 
                                                                                 OrigemOperacaoBolsa.AberturaEmprestimo,
                                                                                 OrigemOperacaoBolsa.LiquidacaoEmprestimo))
                            .GroupBy(gerOperacaoBolsaCollection.Query.CdAtivoBolsa,
                                     gerOperacaoBolsaCollection.Query.IdTrader,
                                     gerOperacaoBolsaCollection.Query.TipoMercado,
                                     gerOperacaoBolsaCollection.Query.TipoOperacao);
            //                            
            gerOperacaoBolsaCollection.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialOperacaoBolsaCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBolsaCollection.Count; i++)
            {
                GerOperacaoBolsa gerOperacaoBolsa = gerOperacaoBolsaCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                #region Trata multi-moeda (calcula fator de conversão)
                decimal fatorConversao = 1;
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoeda != idMoedaAtivo)
                {
                    if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }
                }
                #endregion

                #region Pega o o valor das compras e vendas liquidas
                decimal valorLiquidoOperacaoDia = gerOperacaoBolsa.ValorLiquido.Value;

                if (gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra || gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                {
                    valorLiquidoOperacaoDia = valorLiquidoOperacaoDia * -1;
                }
                #endregion

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBolsa.IdTrader.Value, gerOperacaoBolsa.CdAtivoBolsa, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBolsa - Faz Update
                    c.ResultadoNormal += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("Resultado")) * fatorConversao, 2);
                    c.ResultadoAcumulado += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("Resultado")) * fatorConversao, 2);
                    c.ResultadoDia += Math.Round(valorLiquidoOperacaoDia * fatorConversao, 2);
                    //
                    c.Save();
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBolsa - Faz Insert
                    //                                                            
                    CalculoGerencial calculoGerencialInsert = calculoGerencialOperacaoBolsaCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(gerOperacaoBolsa.TipoMercado);
                    //
                    AtivoBolsa a = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("Resultado")) * fatorConversao, 2);
                    calculoGerencialInsert.ResultadoAcumulado = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("Resultado")) * fatorConversao, 2);
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = Math.Round(valorLiquidoOperacaoDia * fatorConversao, 2);
                    #endregion
                }
            }
            #endregion

            #region GerOperacaoBolsa com Origem = Liquidacao de Termo
            #region Procura CalculoGerencial com os Dados de GerOperacaoBolsa (Origem = Liquidacao de Termo)
            gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollection.Query
                            .Select(gerOperacaoBolsaCollection.Query.IdTrader,
                                    gerOperacaoBolsaCollection.Query.CdAtivoBolsa,
                                    gerOperacaoBolsaCollection.Query.TipoMercado,
                                    gerOperacaoBolsaCollection.Query.TipoOperacao,
                                    gerOperacaoBolsaCollection.Query.Quantidade.Sum()
                                    )
                            .Where(gerOperacaoBolsaCollection.Query.IdCliente == idCliente &
                                   gerOperacaoBolsaCollection.Query.Data.Equal(data) &
                                   gerOperacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada),
                                   gerOperacaoBolsaCollection.Query.Origem.In(OrigemOperacaoBolsa.LiquidacaoTermo))
                            .GroupBy(gerOperacaoBolsaCollection.Query.CdAtivoBolsa,
                                     gerOperacaoBolsaCollection.Query.IdTrader,
                                     gerOperacaoBolsaCollection.Query.TipoMercado,
                                     gerOperacaoBolsaCollection.Query.TipoOperacao);
            //                            
            gerOperacaoBolsaCollection.Query.Load();
            #endregion

            calculoGerencialOperacaoBolsaCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBolsaCollection.Count; i++)
            {
                GerOperacaoBolsa gerOperacaoBolsa = gerOperacaoBolsaCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                #region Calcula o valor do termo compondo a qtde liquidada com a cotaçao da acao equivalente na data!
                decimal quantidadeDia = gerOperacaoBolsa.Quantidade.Value;
                string cdAtivoBolsa = gerOperacaoBolsa.CdAtivoBolsa;

                if (gerOperacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                {
                    quantidadeDia = quantidadeDia * -1;
                }

                decimal valorTermoAtualizado = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data))
                {
                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                    if (fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                    {
                        valorTermoAtualizado = Utilitario.Truncate(quantidadeDia * cotacaoBolsa.PUFechamento.Value / fator.Fator.Value, 2);
                    }
                }
                #endregion

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBolsa.IdTrader.Value, gerOperacaoBolsa.CdAtivoBolsa, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBolsa - Faz Update
                    c.ResultadoDia += valorTermoAtualizado;
                    //
                    c.Save();
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBolsa - Faz Insert
                    //                                                            
                    CalculoGerencial calculoGerencialInsert = calculoGerencialOperacaoBolsaCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(gerOperacaoBolsa.TipoMercado);
                    //
                    AtivoBolsa a = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = valorTermoAtualizado;
                    #endregion
                }
            }
            #endregion

            #region GerOperacaoBolsa com TipoOperacao = VendaDayTrade
            #region Procura CalculoGerencial com os Dados de GerOperacaoBolsa
            // TipoOperacao = VendaDayTrade
            GerOperacaoBolsaCollection gerOperacaoBolsaCollectionVendaDaytrade = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollectionVendaDaytrade.Query
                            .Select(gerOperacaoBolsaCollectionVendaDaytrade.Query.IdTrader,
                                    gerOperacaoBolsaCollectionVendaDaytrade.Query.CdAtivoBolsa,
                                    gerOperacaoBolsaCollectionVendaDaytrade.Query.TipoMercado,
                                    gerOperacaoBolsaCollectionVendaDaytrade.Query.ValorLiquido.Sum().As("ResultadoDTVenda"))
                            .Where(gerOperacaoBolsaCollectionVendaDaytrade.Query.IdCliente == idCliente &
                                   gerOperacaoBolsaCollection.Query.Data.Equal(data) & 
                                   gerOperacaoBolsaCollectionVendaDaytrade.Query.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade)
                            .GroupBy(gerOperacaoBolsaCollectionVendaDaytrade.Query.CdAtivoBolsa,
                                     gerOperacaoBolsaCollectionVendaDaytrade.Query.IdTrader,
                                     gerOperacaoBolsaCollectionVendaDaytrade.Query.TipoMercado);
            //                            
            gerOperacaoBolsaCollectionVendaDaytrade.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialVendaDayTradeCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBolsaCollectionVendaDaytrade.Count; i++)
            {
                GerOperacaoBolsa gerOperacaoBolsa = gerOperacaoBolsaCollectionVendaDaytrade[i];
                CalculoGerencial c = new CalculoGerencial();

                #region Trata multi-moeda (calcula fator de conversão)
                decimal fatorConversao = 1;
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoeda != idMoedaAtivo)
                {
                    if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }
                }
                #endregion

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBolsa.IdTrader.Value, gerOperacaoBolsa.CdAtivoBolsa, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBolsa - Faz Update
                    c.ResultadoDayTrade += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    c.ResultadoAcumulado += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    c.ResultadoDia += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    //
                    calculoGerencialVendaDayTradeCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBolsa - Faz Insert
                    //                  
                    CalculoGerencial calculoGerencialInsert = calculoGerencialVendaDayTradeCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(gerOperacaoBolsa.TipoMercado);
                    //
                    AtivoBolsa a = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTVenda")) * fatorConversao, 2);
                    #endregion
                }
            }

            calculoGerencialVendaDayTradeCollection.Save();
            #endregion

            #region GerOperacaoBolsa com TipoOperacao = CompraDayTrade
            #region Procura CalculoGerencial com os Dados de GerOperacaoBolsa
            // TipoOperacao = CompraDayTrade
            GerOperacaoBolsaCollection gerOperacaoBolsaCollectionCompraDaytrade = new GerOperacaoBolsaCollection();
            gerOperacaoBolsaCollectionCompraDaytrade.Query
                            .Select(gerOperacaoBolsaCollectionCompraDaytrade.Query.IdTrader,
                                    gerOperacaoBolsaCollectionCompraDaytrade.Query.CdAtivoBolsa,
                                    gerOperacaoBolsaCollectionCompraDaytrade.Query.TipoMercado,
                                    (
                                       gerOperacaoBolsaCollectionCompraDaytrade.Query.ValorLiquido.Sum() * -1
                                    ).As("ResultadoDTCompra")
                             )
                            .Where(gerOperacaoBolsaCollectionCompraDaytrade.Query.IdCliente == idCliente &
                                   gerOperacaoBolsaCollection.Query.Data.Equal(data) &
                                   gerOperacaoBolsaCollectionCompraDaytrade.Query.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                            .GroupBy(gerOperacaoBolsaCollectionCompraDaytrade.Query.CdAtivoBolsa,
                                     gerOperacaoBolsaCollectionCompraDaytrade.Query.IdTrader,
                                     gerOperacaoBolsaCollectionCompraDaytrade.Query.TipoMercado);
            //                            
            gerOperacaoBolsaCollectionCompraDaytrade.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialCompraDayTradeCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBolsaCollectionCompraDaytrade.Count; i++)
            {
                GerOperacaoBolsa gerOperacaoBolsa = gerOperacaoBolsaCollectionCompraDaytrade[i];
                CalculoGerencial c = new CalculoGerencial();

                #region Trata multi-moeda (calcula fator de conversão)
                decimal fatorConversao = 1;
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoeda != idMoedaAtivo)
                {
                    if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }
                }
                #endregion
                //
                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBolsa.IdTrader.Value, gerOperacaoBolsa.CdAtivoBolsa, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBolsa - Faz Update
                    c.ResultadoDayTrade += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    c.ResultadoAcumulado += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    c.ResultadoDia += Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    //
                    calculoGerencialCompraDayTradeCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBolsa - Faz Insert
                    //                                        
                    CalculoGerencial calculoGerencialInsert = calculoGerencialCompraDayTradeCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBolsa.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBolsa.CdAtivoBolsa;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBolsa(gerOperacaoBolsa.TipoMercado);
                    //
                    AtivoBolsa a = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBolsa.CdAtivoBolsa);
                    //                    
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = Math.Round(Convert.ToDecimal(gerOperacaoBolsa.GetColumn("ResultadoDTCompra")) * fatorConversao, 2);
                    #endregion
                }
            }

            calculoGerencialCompraDayTradeCollection.Save();
            #endregion
            #endregion

            #region Atualização com a valorização do dia (posição de fechamento total - posição de abertura total) -> Exclui posições de emprestimo!
            calculoGerencialCollection = new CalculoGerencialCollection();
            calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdCliente.Equal(idCliente),
                                                   calculoGerencialCollection.Query.DataCalculo.Equal(data),
                                                   calculoGerencialCollection.Query.TipoMercado.In((byte)TipoMercadoGerencial.Acoes,
                                                                                                   (byte)TipoMercadoGerencial.OpcaoCompraBolsa,
                                                                                                   (byte)TipoMercadoGerencial.OpcaoVendaBolsa,
                                                                                                   (byte)TipoMercadoGerencial.TermoBolsa),
                                                   calculoGerencialCollection.Query.CdAtivo.NotLike("%Doado%"),
                                                   calculoGerencialCollection.Query.CdAtivo.NotLike("%Tomado%"));
            calculoGerencialCollection.Query.Load();
            //

            for (int i = 0; i < calculoGerencialCollection.Count; i++)
            {
                CalculoGerencial c = calculoGerencialCollection[i];
                
                string cdAtivo = c.CdAtivo;
                int idTrader = c.IdTrader.Value;
                byte tipoMercadoGerencial = c.TipoMercado.Value;
                decimal valorFechamento = c.ValorMercado.Value;

                //Trata conversão (tanto para ativo origem qto para ativo destino)
                ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
                conversaoBolsaCollection.Query.Select(conversaoBolsaCollection.Query.CdAtivoBolsa);
                conversaoBolsaCollection.Query.Where(conversaoBolsaCollection.Query.CdAtivoBolsaDestino.Equal(cdAtivo),
                                                     conversaoBolsaCollection.Query.DataEx.Equal(data));
                conversaoBolsaCollection.Query.Load();
                List<string> listaAtivoOrigem = new List<string>();
                if (conversaoBolsaCollection.Count > 0)
                {
                    listaAtivoOrigem.Add(cdAtivo);
                    foreach (ConversaoBolsa conversaoBolsa in conversaoBolsaCollection)
                    {
                        listaAtivoOrigem.Add(conversaoBolsa.CdAtivoBolsa);
                    }
                }
                
                //Checa se o ativo foi convertido, zera o resultado dia nesse caso
                ConversaoBolsaCollection conversaoBolsaCollection2 = new ConversaoBolsaCollection();
                conversaoBolsaCollection2.Query.Select(conversaoBolsaCollection2.Query.CdAtivoBolsa);
                conversaoBolsaCollection2.Query.Where(conversaoBolsaCollection2.Query.CdAtivoBolsa.Equal(cdAtivo),
                                                     conversaoBolsaCollection2.Query.DataEx.Equal(data));
                if (conversaoBolsaCollection2.Query.Load())
                {
                    c.ResultadoDia = 0;
                }
                else
                {
                    decimal valorAbertura = 0;
                    if (tipoMercadoGerencial == (byte)TipoMercadoGerencial.TermoBolsa)
                    {
                        PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
                        posicaoTermoBolsaAbertura.Query.Select(posicaoTermoBolsaAbertura.Query.ValorMercado.Sum());
                        posicaoTermoBolsaAbertura.Query.Where(posicaoTermoBolsaAbertura.Query.IdCliente.Equal(idCliente),
                                                            posicaoTermoBolsaAbertura.Query.IdTrader.Equal(idTrader),
                                                            posicaoTermoBolsaAbertura.Query.DataHistorico.Equal(data));
                        if (conversaoBolsaCollection.Count > 0)
                        {
                            posicaoTermoBolsaAbertura.Query.Where(posicaoTermoBolsaAbertura.Query.CdAtivoBolsa.In(listaAtivoOrigem));
                        }
                        else
                        {
                            posicaoTermoBolsaAbertura.Query.Where(posicaoTermoBolsaAbertura.Query.CdAtivoBolsa.Equal(cdAtivo));
                        }

                        if (posicaoTermoBolsaAbertura.Query.Load())
                        {
                            if (posicaoTermoBolsaAbertura.ValorMercado.HasValue)
                            {
                                valorAbertura = posicaoTermoBolsaAbertura.ValorMercado.Value;
                            }
                        }
                    }
                    else
                    {
                        GerPosicaoBolsaAbertura gerPosicaoBolsaAbertura = new GerPosicaoBolsaAbertura();
                        gerPosicaoBolsaAbertura.Query.Select(gerPosicaoBolsaAbertura.Query.ValorMercado.Sum());
                        gerPosicaoBolsaAbertura.Query.Where(gerPosicaoBolsaAbertura.Query.IdCliente.Equal(idCliente),
                                                            gerPosicaoBolsaAbertura.Query.IdTrader.Equal(idTrader),
                                                            gerPosicaoBolsaAbertura.Query.DataHistorico.Equal(data));
                        if (conversaoBolsaCollection.Count > 0)
                        {
                            gerPosicaoBolsaAbertura.Query.Where(gerPosicaoBolsaAbertura.Query.CdAtivoBolsa.In(listaAtivoOrigem));
                        }
                        else
                        {
                            gerPosicaoBolsaAbertura.Query.Where(gerPosicaoBolsaAbertura.Query.CdAtivoBolsa.Equal(cdAtivo));
                        }

                        if (gerPosicaoBolsaAbertura.Query.Load())
                        {
                            if (gerPosicaoBolsaAbertura.ValorMercado.HasValue)
                            {
                                valorAbertura = gerPosicaoBolsaAbertura.ValorMercado.Value;
                            }
                        }
                    }

                    decimal valorizacaoDia = valorFechamento - valorAbertura;

                    c.ResultadoDia += valorizacaoDia;
                }
            }

            calculoGerencialCollection.Save();
            //
            #endregion
        }

        /// <summary>
        /// Insert/Update CalculoGerencial de Acordo com GerPosicaoBMF e GerOperacaoBMF
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private void CalculoGerencialDiarioBMF(int idCliente, DateTime data)
        {
            #region Insert/Update CalculoGerencial de Acordo com GerPosicaoBMF
            #region Procura CalculoGerencial na GerPosicaoBMF
            GerPosicaoBMFCollection gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
            gerPosicaoBMFCollection.Query
                            .Select(gerPosicaoBMFCollection.Query.IdTrader,
                                    gerPosicaoBMFCollection.Query.CdAtivoBMF,
                                    gerPosicaoBMFCollection.Query.Serie,
                                    gerPosicaoBMFCollection.Query.DataVencimento,
                                    gerPosicaoBMFCollection.Query.TipoMercado,
                                    gerPosicaoBMFCollection.Query.Quantidade.Sum(),
                                    gerPosicaoBMFCollection.Query.ValorMercado.Sum(),
                                    gerPosicaoBMFCollection.Query.ResultadoRealizar.Sum(),
                                    gerPosicaoBMFCollection.Query.PUMercado.Avg(),
                                    gerPosicaoBMFCollection.Query.PUCusto.Avg())
                            .Where(gerPosicaoBMFCollection.Query.IdCliente == idCliente)
                            .GroupBy(gerPosicaoBMFCollection.Query.CdAtivoBMF,
                                     gerPosicaoBMFCollection.Query.Serie,
                                     gerPosicaoBMFCollection.Query.IdTrader,
                                     gerPosicaoBMFCollection.Query.TipoMercado,
                                     gerPosicaoBMFCollection.Query.DataVencimento);
            //                            
            gerPosicaoBMFCollection.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerPosicaoBMFCollection.Count; i++)
            {
                GerPosicaoBMF gerPosicaoBMF = gerPosicaoBMFCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                byte tipoMercado = gerPosicaoBMF.TipoMercado.Value;

                decimal resultadoRealizar = 0;
                if (tipoMercado != TipoMercadoBMF.Futuro)
                {
                    resultadoRealizar = gerPosicaoBMF.ResultadoRealizar.Value;
                }
                
                if (c.LoadByPrimaryKey(idCliente, gerPosicaoBMF.IdTrader.Value, gerPosicaoBMF.CdAtivoBMF + gerPosicaoBMF.Serie, data))
                {
                    #region Se existe CalculoGerencial na GerPosicaoBMF - Faz Update
                    c.Quantidade = gerPosicaoBMF.Quantidade;
                    c.PUCusto = gerPosicaoBMF.PUCusto;
                    c.PUMercado = gerPosicaoBMF.PUMercado;
                    c.ValorMercado = gerPosicaoBMF.ValorMercado;
                    c.ResultadoRealizar = resultadoRealizar;
                    c.ResultadoDayTrade = 0;
                    c.ResultadoNormal = 0;
                    //
                    calculoGerencialCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerPosicaoBMF - Faz Insert
                    CalculoGerencial calculoGerencialInsert = calculoGerencialCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerPosicaoBMF.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerPosicaoBMF.CdAtivoBMF.Trim() + gerPosicaoBMF.Serie.Trim();
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBMF((int)gerPosicaoBMF.TipoMercado);
                    calculoGerencialInsert.DataVencimento = gerPosicaoBMF.DataVencimento;
                    calculoGerencialInsert.Quantidade = gerPosicaoBMF.Quantidade;
                    calculoGerencialInsert.PUCusto = gerPosicaoBMF.PUCusto;

                    calculoGerencialInsert.PUMercado = gerPosicaoBMF.PUMercado;
                    calculoGerencialInsert.ValorMercado = gerPosicaoBMF.ValorMercado;
                    calculoGerencialInsert.ResultadoRealizar = resultadoRealizar;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDia = 0;
                    #endregion
                }
            }

            calculoGerencialCollection.Save();
            //
            #endregion

            #region Insert/Update CalculoGerencial de Acordo com GerOperacaoBMF
            #region GerOperacaoBMF com TipoOperacao != CompraDayTrade/VendaDayTrade (Futuros)
            #region Procura CalculoGerencial com os Dados de GerOperacaoBMF
            // TipoOperacao != CompraDayTrade e != VendaDayTrade
            GerOperacaoBMFCollection gerOperacaoBMFCollection = new GerOperacaoBMFCollection();
            gerOperacaoBMFCollection.Query
                            .Select(gerOperacaoBMFCollection.Query.IdTrader,
                                    gerOperacaoBMFCollection.Query.CdAtivoBMF,
                                    gerOperacaoBMFCollection.Query.Serie,
                                    gerOperacaoBMFCollection.Query.TipoMercado,
                                    gerOperacaoBMFCollection.Query.Ajuste.Sum().As("Resultado"),
                                    gerOperacaoBMFCollection.Query.Despesas.Sum().As("Despesas"))
                            .Where(gerOperacaoBMFCollection.Query.IdCliente == idCliente &
                                   gerOperacaoBMFCollection.Query.Data.Equal(data) &
                                   gerOperacaoBMFCollection.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                                   gerOperacaoBMFCollection.Query.TipoMercado.Equal(((byte)TipoMercadoBMF.Futuro)))
                            .GroupBy(gerOperacaoBMFCollection.Query.CdAtivoBMF,
                                     gerOperacaoBMFCollection.Query.Serie,
                                     gerOperacaoBMFCollection.Query.IdTrader,
                                     gerOperacaoBMFCollection.Query.TipoMercado);
            //                            
            gerOperacaoBMFCollection.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialOperacaoBMFCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBMFCollection.Count; i++)
            {
                GerOperacaoBMF gerOperacaoBMF = gerOperacaoBMFCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBMF.IdTrader.Value, gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBMF - Faz Update
                    c.ResultadoNormal += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                         Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    c.ResultadoDia += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                         Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    c.ResultadoAcumulado += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                         Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    //
                    calculoGerencialOperacaoBMFCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se Não Existe CalculoGerencial na GerOperacaoBMF - Faz Insert
                    //                                                            
                    CalculoGerencial calculoGerencialInsert = calculoGerencialOperacaoBMFCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBMF.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBMF((int)gerOperacaoBMF.TipoMercado);
                    //
                    AtivoBMF a = new AtivoBMF();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBMF.CdAtivoBMF, gerOperacaoBMF.Serie);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoNormal = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                             Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    calculoGerencialInsert.ResultadoAcumulado = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                             Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    calculoGerencialInsert.ResultadoDia = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                            Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    #endregion
                }
            }

            calculoGerencialOperacaoBMFCollection.Save();
            #endregion

            #region GerOperacaoBMF com TipoOperacao = CompraDaytrade ou VendaDayTrade (Futuros)
            #region Procura CalculoGerencial com os Dados de GerOperacaoBMF
            // TipoOperacao = CompraDaytrade ou VendaDayTrade
            GerOperacaoBMFCollection gerOperacaoBMFCollectionDaytrade = new GerOperacaoBMFCollection();
            gerOperacaoBMFCollectionDaytrade.Query
                            .Select(gerOperacaoBMFCollectionDaytrade.Query.IdTrader,
                                    gerOperacaoBMFCollectionDaytrade.Query.CdAtivoBMF,
                                    gerOperacaoBMFCollectionDaytrade.Query.Serie,
                                    gerOperacaoBMFCollectionDaytrade.Query.TipoMercado,
                                    gerOperacaoBMFCollectionDaytrade.Query.Ajuste.Sum().As("Resultado"),
                                    gerOperacaoBMFCollectionDaytrade.Query.Despesas.Sum().As("Despesas"))
                            .Where(gerOperacaoBMFCollectionDaytrade.Query.IdCliente == idCliente &
                                   gerOperacaoBMFCollectionDaytrade.Query.Data.Equal(data) &
                                   gerOperacaoBMFCollectionDaytrade.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                                   gerOperacaoBMFCollection.Query.TipoMercado.Equal(((byte)TipoMercadoBMF.Futuro)))
                            .GroupBy(gerOperacaoBMFCollectionDaytrade.Query.CdAtivoBMF,
                                     gerOperacaoBMFCollectionDaytrade.Query.Serie,
                                     gerOperacaoBMFCollectionDaytrade.Query.IdTrader,
                                     gerOperacaoBMFCollectionDaytrade.Query.TipoMercado);
            //                            
            gerOperacaoBMFCollectionDaytrade.Query.Load();
            #endregion

            CalculoGerencialCollection calculoGerencialDayTradeCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBMFCollectionDaytrade.Count; i++)
            {
                GerOperacaoBMF gerOperacaoBMF = gerOperacaoBMFCollectionDaytrade[i];
                CalculoGerencial c = new CalculoGerencial();

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBMF.IdTrader.Value, gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBMF - Faz Update
                    c.ResultadoDayTrade += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                           Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    c.ResultadoAcumulado += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                           Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    c.ResultadoDia += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                           Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    //
                    calculoGerencialDayTradeCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBMF - Faz Insert
                    //                  
                    CalculoGerencial calculoGerencialInsert = calculoGerencialDayTradeCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBMF.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBMF((int)gerOperacaoBMF.TipoMercado);
                    //
                    //
                    AtivoBMF a = new AtivoBMF();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBMF.CdAtivoBMF, gerOperacaoBMF.Serie);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoDayTrade = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                               Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ResultadoAcumulado = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                               Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    calculoGerencialInsert.ResultadoDia = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado")) -
                                                               Convert.ToDecimal(gerOperacaoBMF.GetColumn("Despesas"));
                    #endregion
                }
            }

            calculoGerencialDayTradeCollection.Save();
            #endregion

            #region GerOperacaoBMF com TipoOperacao != CompraDayTrade/VendaDayTrade (Opções e a Vista)
            #region Procura CalculoGerencial com os Dados de GerOperacaoBMF
            // TipoOperacao != CompraDayTrade e != VendaDayTrade
            gerOperacaoBMFCollection = new GerOperacaoBMFCollection();
            gerOperacaoBMFCollection.Query
                            .Select(gerOperacaoBMFCollection.Query.IdTrader,
                                    gerOperacaoBMFCollection.Query.CdAtivoBMF,
                                    gerOperacaoBMFCollection.Query.Serie,
                                    gerOperacaoBMFCollection.Query.TipoMercado,
                                    gerOperacaoBMFCollection.Query.ResultadoRealizado.Sum().As("Resultado"))
                            .Where(gerOperacaoBMFCollection.Query.IdCliente == idCliente &
                                   gerOperacaoBMFCollection.Query.Data.Equal(data) &
                                   gerOperacaoBMFCollection.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                                   gerOperacaoBMFCollectionDaytrade.Query.TipoMercado.NotEqual((byte)TipoMercadoBMF.Futuro))
                            .GroupBy(gerOperacaoBMFCollection.Query.CdAtivoBMF,
                                     gerOperacaoBMFCollection.Query.Serie,
                                     gerOperacaoBMFCollection.Query.IdTrader,
                                     gerOperacaoBMFCollection.Query.TipoMercado);
            //                            
            gerOperacaoBMFCollection.Query.Load();
            #endregion

            calculoGerencialOperacaoBMFCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBMFCollection.Count; i++)
            {
                GerOperacaoBMF gerOperacaoBMF = gerOperacaoBMFCollection[i];
                CalculoGerencial c = new CalculoGerencial();

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBMF.IdTrader.Value, gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBMF - Faz Update
                    c.ResultadoNormal += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    c.ResultadoAcumulado += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    c.ResultadoDia += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    //
                    calculoGerencialOperacaoBMFCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se Não Existe CalculoGerencial na GerOperacaoBMF - Faz Insert
                    //                                                            
                    CalculoGerencial calculoGerencialInsert = calculoGerencialOperacaoBMFCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBMF.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBMF((int)gerOperacaoBMF.TipoMercado);
                    //
                    AtivoBMF a = new AtivoBMF();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBMF.CdAtivoBMF, gerOperacaoBMF.Serie);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoNormal = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    calculoGerencialInsert.ResultadoAcumulado = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    calculoGerencialInsert.ResultadoDia = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    #endregion
                }
            }

            calculoGerencialOperacaoBMFCollection.Save();
            #endregion

            #region GerOperacaoBMF com TipoOperacao = CompraDaytrade ou VendaDayTrade (Opções e a Vista)
            #region Procura CalculoGerencial com os Dados de GerOperacaoBMF
            // TipoOperacao = CompraDaytrade ou VendaDayTrade
            gerOperacaoBMFCollectionDaytrade = new GerOperacaoBMFCollection();
            gerOperacaoBMFCollectionDaytrade.Query
                            .Select(gerOperacaoBMFCollectionDaytrade.Query.IdTrader,
                                    gerOperacaoBMFCollectionDaytrade.Query.CdAtivoBMF,
                                    gerOperacaoBMFCollectionDaytrade.Query.Serie,
                                    gerOperacaoBMFCollectionDaytrade.Query.TipoMercado,
                                    gerOperacaoBMFCollectionDaytrade.Query.ValorLiquido.Sum().As("Resultado"))
                            .Where(gerOperacaoBMFCollectionDaytrade.Query.IdCliente == idCliente &
                                   gerOperacaoBMFCollectionDaytrade.Query.Data.Equal(data) &
                                   gerOperacaoBMFCollectionDaytrade.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                                   gerOperacaoBMFCollectionDaytrade.Query.TipoMercado.NotEqual((byte)TipoMercadoBMF.Futuro))
                            .GroupBy(gerOperacaoBMFCollectionDaytrade.Query.CdAtivoBMF,
                                     gerOperacaoBMFCollectionDaytrade.Query.Serie,
                                     gerOperacaoBMFCollectionDaytrade.Query.IdTrader,
                                     gerOperacaoBMFCollectionDaytrade.Query.TipoMercado);
            //                            
            gerOperacaoBMFCollectionDaytrade.Query.Load();
            #endregion

            calculoGerencialDayTradeCollection = new CalculoGerencialCollection();
            //
            for (int i = 0; i < gerOperacaoBMFCollectionDaytrade.Count; i++)
            {
                GerOperacaoBMF gerOperacaoBMF = gerOperacaoBMFCollectionDaytrade[i];
                CalculoGerencial c = new CalculoGerencial();

                if (c.LoadByPrimaryKey(idCliente, gerOperacaoBMF.IdTrader.Value, gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie, data))
                {
                    #region Se existe CalculoGerencial na GerOperacaoBMF - Faz Update
                    c.ResultadoDayTrade += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    c.ResultadoAcumulado += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    c.ResultadoDia += Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    //
                    calculoGerencialDayTradeCollection.AttachEntity(c);
                    #endregion
                }
                else
                {
                    #region Se não Existe CalculoGerencial na GerOperacaoBMF - Faz Insert
                    //                  
                    CalculoGerencial calculoGerencialInsert = calculoGerencialDayTradeCollection.AddNew();
                    calculoGerencialInsert.DataCalculo = data;
                    calculoGerencialInsert.IdCliente = idCliente;
                    calculoGerencialInsert.IdTrader = gerOperacaoBMF.IdTrader;
                    calculoGerencialInsert.CdAtivo = gerOperacaoBMF.CdAtivoBMF + gerOperacaoBMF.Serie;
                    calculoGerencialInsert.TipoMercado = (byte)this.ConverteTipoMercadoBMF((int)gerOperacaoBMF.TipoMercado);
                    //
                    //
                    AtivoBMF a = new AtivoBMF();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(a.Query.DataVencimento);
                    a.LoadByPrimaryKey(campos, gerOperacaoBMF.CdAtivoBMF, gerOperacaoBMF.Serie);
                    //
                    calculoGerencialInsert.DataVencimento = a.es.HasData ? a.DataVencimento : null;
                    //
                    calculoGerencialInsert.Quantidade = 0;
                    calculoGerencialInsert.PUCusto = 0;
                    calculoGerencialInsert.PUMercado = 0;
                    calculoGerencialInsert.ValorMercado = 0;
                    calculoGerencialInsert.ResultadoRealizar = 0;
                    calculoGerencialInsert.ResultadoDayTrade = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    calculoGerencialInsert.ResultadoNormal = 0;
                    calculoGerencialInsert.ValorizacaoDia = 0;
                    calculoGerencialInsert.ResultadoAcumulado = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    calculoGerencialInsert.ResultadoDia = Convert.ToDecimal(gerOperacaoBMF.GetColumn("Resultado"));
                    #endregion
                }
            }

            calculoGerencialDayTradeCollection.Save();
            #endregion
            #endregion
        }

        /// <summary>
        /// Replica os Campos do Dia Anterior no Dia Atual
        /// Insert/Update CalculoGerencial de Acordo com GerPosicaoBolsa e GerOperacaoBolsa
        /// Insert/Update CalculoGerencial de Acordo com GerPosicaoBMF e GerOperacaoBMF
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculoGerencialDiario(int idCliente, DateTime data) {

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            #region Seleciona Calculos Gerenciais do Dia Anterior e Insere Novos Registros com a Data passada
            CalculoGerencialCollection calculoGerencialCollectionInsercao = new CalculoGerencialCollection();
            calculoGerencialCollectionInsercao.Query
                            .Where(calculoGerencialCollectionInsercao.Query.IdCliente == idCliente &
                                   calculoGerencialCollectionInsercao.Query.DataCalculo == dataAnterior &
                                   (calculoGerencialCollectionInsercao.Query.ResultadoAcumulado != 0 |
                                    calculoGerencialCollectionInsercao.Query.Quantidade != 0));

            calculoGerencialCollectionInsercao.Query.Load();

            foreach (CalculoGerencial c in calculoGerencialCollectionInsercao) {
                // Como há Alteração da Chave Primaria DataCalculo é necessario a linha abaixo
                c.MarkAllColumnsAsDirty(DataRowState.Added);
                c.DataCalculo = data;
            }
            calculoGerencialCollectionInsercao.Save();
            #endregion

            // Garante que não haverá lixo na CalculoGerencial
            this.ZeraValoresGerencialDia(idCliente, data);
            //Zera quantidade já baixada da CalculoGerencial
            this.ZeraQuantidadeBaixada(idCliente, data);
            // Calculo Diario de Bolsa
            this.CalculoGerencialDiarioBolsa(idCliente, data);
            // Calculo Diario de BMF
            this.CalculoGerencialDiarioBMF(idCliente, data);                        
            //Deleta registros zerados
            this.DeletaGerencialZerado(idCliente, data);
        }

        #region Transferência das despesas calculadas em Operacao para GerOperacao (Bolsa e BMF)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="data"></param>
        public void TransfereDespesasGerencialBolsa(int idCliente, DateTime data) {
            #region OperacaoBolsa
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollectionAux = new OperacaoBolsaCollection();

            operacaoBolsaCollection.Query
                .Select("<sub.IdTrader>",
                        "<sub.CdAtivoBolsa>",
                        "<sub.TipoOperacao>",
                        "<sum(sub.Quantidade) As Quantidade>",
                        "<sum( sub.Corretagem + sub.Emolumento + sub.LiquidacaoCBLC + sub.RegistroBolsa + sub.RegistroCBLC ) As Despesas >"
                       )
                .From
                (
                    operacaoBolsaCollectionAux.Query
                         .Select(operacaoBolsaCollectionAux.Query.IdTrader,
                                 operacaoBolsaCollectionAux.Query.CdAtivoBolsa,
                                 operacaoBolsaCollectionAux.Query.TipoOperacao.Substring(1).As("TipoOperacao"),
                                 operacaoBolsaCollectionAux.Query.Quantidade,
                                 operacaoBolsaCollectionAux.Query.Corretagem,
                                 operacaoBolsaCollectionAux.Query.Emolumento,
                                 operacaoBolsaCollectionAux.Query.LiquidacaoCBLC,
                                 operacaoBolsaCollectionAux.Query.RegistroBolsa,
                                 operacaoBolsaCollectionAux.Query.RegistroCBLC)
                         .Where(operacaoBolsaCollectionAux.Query.IdCliente == idCliente &
                                operacaoBolsaCollectionAux.Query.Data == data &
                                operacaoBolsaCollectionAux.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada) &
                                operacaoBolsaCollectionAux.Query.IdTrader.IsNotNull())
                ).As("sub");

            operacaoBolsaCollection.Query.GroupBy("IdTrader",
                                                  "CdAtivoBolsa",
                                                  "TipoOperacao");
            operacaoBolsaCollection.Query.OrderBy(operacaoBolsaCollection.Query.IdTrader.Ascending,
                                                  operacaoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                  operacaoBolsaCollection.Query.TipoOperacao.Ascending);
            //
            operacaoBolsaCollection.Query.Load();
            //
            #endregion

            for (int i = 0; i < operacaoBolsaCollection.Count; i++) {
                //
                decimal despesasResidual = (decimal)operacaoBolsaCollection[i].GetColumn("Despesas");
                //
                #region Para cada GerOperacaoBolsa
                GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();
                gerOperacaoBolsaCollection.Query
                                .Select(gerOperacaoBolsaCollection.Query.IdOperacao,
                                        gerOperacaoBolsaCollection.Query.Despesas,
                                        gerOperacaoBolsaCollection.Query.Quantidade)
                                .Where(gerOperacaoBolsaCollection.Query.IdCliente == idCliente &
                                       gerOperacaoBolsaCollection.Query.CdAtivoBolsa == operacaoBolsaCollection[i].CdAtivoBolsa &
                                       gerOperacaoBolsaCollection.Query.Data == data &
                                       gerOperacaoBolsaCollection.Query.IdTrader == operacaoBolsaCollection[i].IdTrader);
                //
                if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Compra) {
                    gerOperacaoBolsaCollection.Query
                        .Where(gerOperacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                }
                else if (operacaoBolsaCollection[i].TipoOperacao == TipoOperacaoBolsa.Venda) {
                    gerOperacaoBolsaCollection.Query
                        .Where(gerOperacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                }
                gerOperacaoBolsaCollection.Query.Load();
                //
                for (int j = 0; j < gerOperacaoBolsaCollection.Count; j++) {
                    decimal totalDespesas = (decimal)operacaoBolsaCollection[i].GetColumn("Despesas");
                    decimal totalQuantidade = (decimal)operacaoBolsaCollection[i].Quantidade.Value;
                    //                    
                    decimal despesas = (totalDespesas * gerOperacaoBolsaCollection[j].Quantidade.Value) / totalQuantidade;
                    decimal despesasTruncada = Utilitario.Truncate(despesas, 2);

                    despesasResidual -= despesasTruncada;

                    // Se é a Ultima Operacao Garante-se que 100% das Despesas
                    // foi distribuida, jogando todo o residual para a ultima operacao
                    if (j == gerOperacaoBolsaCollection.Count - 1) {
                        despesasTruncada += despesasResidual;
                    }

                    gerOperacaoBolsaCollection[j].Despesas = despesasTruncada;
                }
                // Salva a Collection de operações se ocorreu Update
                if (gerOperacaoBolsaCollection.Count > 0) {
                    gerOperacaoBolsaCollection.Save();
                }
                #endregion
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="data"></param>
        public void TransfereDespesasGerencialBMF(int idCliente, DateTime data)
        {
            #region OperacaoBMF
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            OperacaoBMFCollection operacaoBMFCollectionAux = new OperacaoBMFCollection();

            operacaoBMFCollection.Query
                .Select("<sub.IdTrader>",
                        "<sub.CdAtivoBMF>",
                        "<sub.Serie>",
                        "<sub.TipoOperacao>",
                        "<sum(sub.Quantidade) As Quantidade>",
                        "<sum( sub.Corretagem + sub.Emolumento + sub.Registro ) As Despesas >"
                       )
                .From
                (
                    operacaoBMFCollectionAux.Query
                         .Select(operacaoBMFCollectionAux.Query.IdTrader,
                                 operacaoBMFCollectionAux.Query.CdAtivoBMF,
                                 operacaoBMFCollectionAux.Query.Serie,
                                 operacaoBMFCollectionAux.Query.TipoOperacao.Substring(1).As("TipoOperacao"),
                                 operacaoBMFCollectionAux.Query.Quantidade,
                                 operacaoBMFCollectionAux.Query.Corretagem,
                                 operacaoBMFCollectionAux.Query.Emolumento,
                                 operacaoBMFCollectionAux.Query.Registro)
                         .Where(operacaoBMFCollectionAux.Query.IdCliente == idCliente &
                                operacaoBMFCollectionAux.Query.Data == data &
                                operacaoBMFCollectionAux.Query.TipoOperacao.NotIn(TipoOperacaoBMF.Deposito, TipoOperacaoBMF.Retirada) &
                                operacaoBMFCollectionAux.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo) &
                                operacaoBMFCollectionAux.Query.IdTrader.IsNotNull())
                ).As("sub");

            operacaoBMFCollection.Query.GroupBy("IdTrader",
                                                  "CdAtivoBMF",
                                                  "Serie",
                                                  "TipoOperacao");
            operacaoBMFCollection.Query.OrderBy(operacaoBMFCollection.Query.IdTrader.Ascending,
                                                operacaoBMFCollection.Query.CdAtivoBMF.Ascending,
                                                operacaoBMFCollection.Query.Serie.Ascending,
                                                operacaoBMFCollection.Query.TipoOperacao.Ascending);
            //
            operacaoBMFCollection.Query.Load();
            //
            #endregion

            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                //
                decimal despesasResidual = (decimal)operacaoBMFCollection[i].GetColumn("Despesas");
                //
                #region Para cada GerOperacaoBMF
                GerOperacaoBMFCollection gerOperacaoBMFCollection = new GerOperacaoBMFCollection();
                gerOperacaoBMFCollection.Query
                                .Select(gerOperacaoBMFCollection.Query.IdOperacao,
                                        gerOperacaoBMFCollection.Query.Despesas,
                                        gerOperacaoBMFCollection.Query.Quantidade)
                                .Where(gerOperacaoBMFCollection.Query.IdCliente == idCliente &
                                       gerOperacaoBMFCollection.Query.CdAtivoBMF == operacaoBMFCollection[i].CdAtivoBMF &
                                       gerOperacaoBMFCollection.Query.Serie == operacaoBMFCollection[i].Serie &
                                       gerOperacaoBMFCollection.Query.Data == data &
                                       gerOperacaoBMFCollection.Query.IdTrader == operacaoBMFCollection[i].IdTrader,
                                       gerOperacaoBMFCollection.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo));
                //
                if (operacaoBMFCollection[i].TipoOperacao == TipoOperacaoBMF.Compra)
                {
                    gerOperacaoBMFCollection.Query
                        .Where(gerOperacaoBMFCollection.Query.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade));
                }
                else if (operacaoBMFCollection[i].TipoOperacao == TipoOperacaoBMF.Venda)
                {
                    gerOperacaoBMFCollection.Query
                        .Where(gerOperacaoBMFCollection.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade));
                }
                gerOperacaoBMFCollection.Query.Load();
                //
                for (int j = 0; j < gerOperacaoBMFCollection.Count; j++)
                {
                    decimal totalDespesas = (decimal)operacaoBMFCollection[i].GetColumn("Despesas");
                    decimal totalQuantidade = (decimal)operacaoBMFCollection[i].Quantidade.Value;
                    //                    
                    decimal despesas = (totalDespesas * gerOperacaoBMFCollection[j].Quantidade.Value) / totalQuantidade;
                    decimal despesasTruncada = Utilitario.Truncate(despesas, 2);

                    despesasResidual -= despesasTruncada;

                    // Se é a Ultima Operacao Garante-se que 100% das Despesas
                    // foi distribuida, jogando todo o residual para a ultima operacao
                    if (j == gerOperacaoBMFCollection.Count - 1)
                    {
                        despesasTruncada += despesasResidual;
                    }

                    gerOperacaoBMFCollection[j].Despesas = despesasTruncada;
                }
                // Salva a Collection de operações se ocorreu Update
                if (gerOperacaoBMFCollection.Count > 0)
                {
                    gerOperacaoBMFCollection.Save();
                }
                #endregion
            }
        }
        #endregion

        #region Casamento DayTrade (Bolsa e BMF) do Gerencial
        /// <summary>
        /// Casamento de daytrade de bolsa para o gerencial.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CasaDayTradeBolsa(int idCliente, DateTime data) {           
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionCompra = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionVenda = new OrdemBolsaCollection();
            OrdemBolsa ordemBolsaCompra = new OrdemBolsa();
            OrdemBolsa ordemBolsaVenda = new OrdemBolsa();
            //
            OrdemBolsa ordemBolsaAux = new OrdemBolsa();
            //
            decimal quantidadeDayTrade = 0;
            // Atualiza Quantidade Daytrade = 0 de todas as ordens do cliente
            ordemBolsaCollection.AtualizaOrdemBolsa(idCliente, data, quantidadeDayTrade);
            //
            // Collection de Ordens de Venda
            ordemBolsaCollectionVenda.BuscaOrdemBolsaVendaGerencial(idCliente, data);

            for (int i = 0; i < ordemBolsaCollectionVenda.Count; i++) {
                ordemBolsaVenda = ordemBolsaCollectionVenda[i];
                decimal quantidadeVenda = ordemBolsaVenda.Quantidade.Value;
                decimal quantidadeVendaDT = ordemBolsaVenda.QuantidadeDayTrade.Value;
                int idOrdemVenda = ordemBolsaVenda.IdOrdem.Value;
                int idTrader = ordemBolsaVenda.IdTrader.Value;
                string cdAtivoBolsa = ordemBolsaVenda.CdAtivoBolsa;
                
                decimal quantidadeDiferenca;

                // Collection de Ordens de Compra
                if (!ordemBolsaCollectionCompra.BuscaOrdemBolsaCompraGerencial(idCliente, data, idTrader, cdAtivoBolsa)) {
                    continue;
                }

                for (int j = 0; j < ordemBolsaCollectionCompra.Count; j++) {
                    ordemBolsaCompra = ordemBolsaCollectionCompra[j];
                    decimal quantidadeCompra = ordemBolsaCompra.Quantidade.Value;
                    decimal quantidadeCompraDT = ordemBolsaCompra.QuantidadeDayTrade.Value;
                    int idOrdemCompra = ordemBolsaCompra.IdOrdem.Value;

                    if (quantidadeCompra == quantidadeCompraDT) continue;
                    if (quantidadeVenda == quantidadeVendaDT) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaDT;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraDT;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra) {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0) {
                        ordemBolsaAux.AtualizaOrdemBolsa(idOrdemVenda, quantidadeDiferenca + quantidadeVendaDT);
                        ordemBolsaAux.AtualizaOrdemBolsa(idOrdemCompra, quantidadeDiferenca + quantidadeCompraDT);
                        quantidadeVendaDT += quantidadeDiferenca;
                    }
                }
            }
        }

        /// <summary>
        /// Casamento de daytrade de bmf para o gerencial.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CasaDayTradeBMF(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionCompra = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionVenda = new OrdemBMFCollection();
            OrdemBMF ordemBMFCompra = new OrdemBMF();
            OrdemBMF ordemBMFVenda = new OrdemBMF();

            OrdemBMF ordemBMFCompraAux = new OrdemBMF();
            //
            int quantidadeDayTrade = 0;
            // Atualiza quantidade daytrade = 0 de todas as ordens do cliente
            ordemBMFCollection.AtualizaOrdemBMF(idCliente, data, quantidadeDayTrade);
            //
            // Collection de Ordens de Venda
            ordemBMFCollectionVenda.BuscaOrdemBMFVendaGerencial(idCliente, data);

            for (int i = 0; i < ordemBMFCollectionVenda.Count; i++)
            {
                ordemBMFVenda = ordemBMFCollectionVenda[i];
                int quantidadeVenda = ordemBMFVenda.Quantidade.Value;
                int quantidadeVendaDT = ordemBMFVenda.QuantidadeDayTrade.Value;
                int idOrdemVenda = ordemBMFVenda.IdOrdem.Value;
                int idTrader = ordemBMFVenda.IdTrader.Value;
                string cdAtivoBMF = ordemBMFVenda.CdAtivoBMF;
                string serie = ordemBMFVenda.Serie;
                int quantidadeDiferenca;

                // Collection de ordens de compra
                if (!ordemBMFCollectionCompra.BuscaOrdemBMFCompraGerencial(idCliente, data, idTrader, cdAtivoBMF, serie))
                {
                    continue;
                }

                for (int j = 0; j < ordemBMFCollectionCompra.Count; j++)
                {
                    ordemBMFCompra = ordemBMFCollectionCompra[j];
                    int quantidadeCompra = ordemBMFCompra.Quantidade.Value;
                    int quantidadeCompraDT = ordemBMFCompra.QuantidadeDayTrade.Value;
                    int idOrdemCompra = ordemBMFCompra.IdOrdem.Value;

                    if (quantidadeCompra == quantidadeCompraDT)
                        continue;
                    if (quantidadeVenda == quantidadeVendaDT)
                        break;

                    int deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaDT;
                    int deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraDT;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra)
                    {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else
                    {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0)
                    {
                        ordemBMFCompraAux.AtualizaOrdemBMF(idOrdemVenda, quantidadeDiferenca + quantidadeVendaDT);
                        ordemBMFCompraAux.AtualizaOrdemBMF(idOrdemCompra, quantidadeDiferenca + quantidadeCompraDT);
                        quantidadeVendaDT += quantidadeDiferenca;
                    }
                }
            }
        }
        #endregion

        #region Tratamento de Empréstimo Bolsa para o Gerencial
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaOperacaoEmprestimo(int idCliente, DateTime data)
        {
            // Collections que serão usadas para salvar tudo de uma vez no final do loop
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();

            //
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
            operacaoEmprestimoBolsaCollection.BuscaOperacaoEmprestimoBolsaCompleta(idCliente, data);

            //Deleta antes todas as operações Bolsa do tipo deposito (origem = Emprestimo) na data
            if (operacaoEmprestimoBolsaCollection.Count > 0)
            {
                List<int> origemLista = new List<int>();
                origemLista.Add(OrigemOperacaoBolsa.AberturaEmprestimo);
                GerOperacaoBolsaCollection gerOperacaoBolsaCollectionDeletar = new GerOperacaoBolsaCollection();
                gerOperacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemLista, data);
            }

            for (int i = 0; i < operacaoEmprestimoBolsaCollection.Count; i++)
            {
                OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = operacaoEmprestimoBolsaCollection[i];

                #region Valores operacaoEmprestimoBolsa
                int idOperacao = operacaoEmprestimoBolsa.IdOperacao.Value;
                string cdAtivoBolsa = operacaoEmprestimoBolsa.CdAtivoBolsa;
                int idAgente = operacaoEmprestimoBolsa.IdAgente.Value;
                byte pontaEmprestimo = operacaoEmprestimoBolsa.PontaEmprestimo.Value;
                DateTime dataVencimento = operacaoEmprestimoBolsa.DataVencimento.Value;
                decimal taxaOperacao = operacaoEmprestimoBolsa.TaxaOperacao.Value;
                decimal taxaComissao = operacaoEmprestimoBolsa.TaxaComissao.Value;
                decimal quantidade = operacaoEmprestimoBolsa.Quantidade.Value;
                decimal pu = operacaoEmprestimoBolsa.Pu.Value;
                decimal valor = operacaoEmprestimoBolsa.Valor.Value;
                byte tipoEmprestimo = operacaoEmprestimoBolsa.TipoEmprestimo.Value;
                int numeroContrato = operacaoEmprestimoBolsa.NumeroContrato.Value;
                #endregion

                int idLocal = LocalFeriadoFixo.Bovespa;
                string tipoMercado = TipoMercadoBolsa.MercadoVista;
                int origem = OrigemOperacaoBolsa.AberturaEmprestimo;

                string tipoOperacaoBolsa = null;
                if (operacaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                {
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Retirada;
                }
                else if (operacaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                {
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Deposito;
                }

                #region Calcula o valor de mercado do próprio dia
                //Busca fatorCotacaoBolsa
                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                decimal fator = 1;
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    fator = fatorCotacao.Fator.Value;
                }
                else
                {
                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsa + " " + data.ToString("d"));
                }
                //

                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (!cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data))
                {
                    throw new CotacaoNaoCadastradaException("Cotação não cadastrada para o ativo " + cdAtivoBolsa + " em " + data.ToShortDateString());
                }

                decimal puFechamento = cotacaoBolsa.PUFechamento.Value;

                decimal valorMercado = Math.Round(quantidade * puFechamento / fator, 2);
                #endregion
                
                #region Insercao OperacaoBolsa
                GerOperacaoBolsa gerOperacaoBolsa = gerOperacaoBolsaCollection.AddNew();
                gerOperacaoBolsa.TipoMercado = tipoMercado;
                gerOperacaoBolsa.TipoOperacao = tipoOperacaoBolsa;
                gerOperacaoBolsa.Data = data;
                gerOperacaoBolsa.Pu = pu;
                gerOperacaoBolsa.PULiquido = pu;
                gerOperacaoBolsa.Quantidade = quantidade;
                gerOperacaoBolsa.Origem = (byte)origem;
                gerOperacaoBolsa.IdCliente = idCliente;
                gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                gerOperacaoBolsa.IdTrader = operacaoEmprestimoBolsa.IdTrader;
                gerOperacaoBolsa.Valor = 0;
                gerOperacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                #endregion
            }

            gerOperacaoBolsaCollection.Save();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException se fator não estiver cadastrado
        public void ProcessaLiquidacaoEmprestimo(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();
            
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollection.BuscaLiquidacaoEmprestimoBolsa(idCliente, data);

            for (int i = 0; i < liquidacaoEmprestimoBolsaCollection.Count; i++)
            {
                LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = liquidacaoEmprestimoBolsaCollection[i];

                #region Valores de LiquidacaoEmprestimoBolsa
                byte tipoLiquidacao = liquidacaoEmprestimoBolsa.TipoLiquidacao.Value;
                int idPosicao = liquidacaoEmprestimoBolsa.IdPosicao.Value;
                int idAgente = liquidacaoEmprestimoBolsa.IdAgente.Value;
                string cdAtivoBolsa = liquidacaoEmprestimoBolsa.CdAtivoBolsa;
                decimal quantidade = liquidacaoEmprestimoBolsa.Quantidade.Value;
                decimal valor = liquidacaoEmprestimoBolsa.Valor.Value;
                decimal valorLiquidacao = liquidacaoEmprestimoBolsa.ValorLiquidacao.Value;
                int? idOperacao = liquidacaoEmprestimoBolsa.IdOperacao;
                byte fonte = liquidacaoEmprestimoBolsa.Fonte.Value;
                #endregion

                #region Valores de PosicaoEmprestimoBolsa
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
                if (!posicaoEmprestimoBolsa.LoadByPrimaryKey(idPosicao))
                {
                    throw new Exception("A liquidação de " + cdAtivoBolsa + " - " + quantidade.ToString() + " perdeu o vínculo original com a posição. Favor relançar a liquidação.");
                }

                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                decimal puMercado = posicaoEmprestimoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoEmprestimoBolsa.ValorMercado.Value;
                decimal valorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value;
                decimal valorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value;
                decimal valorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value;
                decimal quantidadePosicao = posicaoEmprestimoBolsa.Quantidade.Value;
                decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;
                #endregion

                #region Cálculo de juros, comissao, CBLC em caso de os valores não virem por arquivo
                //Calculo os valores proporcionais da antecipação
                decimal valorBaseAntecipado = Utilitario.Truncate(valorBase * quantidade / quantidadePosicao, 2);
                decimal valorJurosLiquidacao = Utilitario.Truncate(valorCorrigidoJuros * quantidade / quantidadePosicao, 2);
                decimal valorComissaoLiquidacao = Utilitario.Truncate(valorCorrigidoComissao * quantidade / quantidadePosicao, 2);
                decimal valorCBLCLiquidacao = Utilitario.Truncate(valorCorrigidoCBLC * quantidade / quantidadePosicao, 2);

                // Se vier integrado pelo arquivo DBTL (ou se for vencimento), já existirão os valores da liquidação 
                // (juros, comissao, CBCL). Caso contrário, o valorLiquidacao será calculado aqui abaixo:
                if (valorLiquidacao == 0 && fonte == (byte)FonteLiquidacaoEmprestimoBolsa.Manual)
                {
                    if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                    {
                        valorLiquidacao = valorJurosLiquidacao - valorBaseAntecipado;
                    }
                    else
                    {
                        valorLiquidacao = (valorJurosLiquidacao + valorComissaoLiquidacao + valorCBLCLiquidacao) - valorBaseAntecipado * 3M;
                    }
                }
                #endregion


                liquidacaoEmprestimoBolsa.Valor = valorBaseAntecipado;

                int origemOperacao = OrigemOperacaoBolsa.LiquidacaoEmprestimo;
                string tipoOperacaoBolsa = null;
                if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                {
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Deposito;
                }
                else if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                {
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Retirada;
                }

                #region Insercao OperacaoBolsa
                GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                gerOperacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                gerOperacaoBolsa.TipoOperacao = tipoOperacaoBolsa;
                gerOperacaoBolsa.Data = data;
                gerOperacaoBolsa.Pu = puLiquidoOriginal;
                gerOperacaoBolsa.PULiquido = puLiquidoOriginal;
                gerOperacaoBolsa.Quantidade = quantidade;
                gerOperacaoBolsa.Origem = (byte)origemOperacao;
                gerOperacaoBolsa.IdCliente = idCliente;
                gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                gerOperacaoBolsa.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                gerOperacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;

                gerOperacaoBolsaCollection.AttachEntity(gerOperacaoBolsa);
                #endregion
            }

            gerOperacaoBolsaCollection.Save();
        }
        #endregion

        #region Tratamento de Termo Bolsa para o Gerencial
        /// <summary>
        /// Processa a parte de Liquidacao de Termo de Bolsa para o Gerencial.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaLiquidacaoTermo(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();

            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollection.BuscaLiquidacaoTermoBolsa(idCliente, data);

            for (int i = 0; i < liquidacaoTermoBolsaCollection.Count; i++)
            {
                LiquidacaoTermoBolsa liquidacaoTermoBolsa = liquidacaoTermoBolsaCollection[i];

                #region Carrega variáveis de LiquidacaoTermoBolsa
                byte tipoLiquidacao = liquidacaoTermoBolsa.TipoLiquidacao.Value;
                string cdAtivoBolsa = liquidacaoTermoBolsa.CdAtivoBolsa;
                DateTime dataLiquidacao = liquidacaoTermoBolsa.DataLiquidacao.Value;
                decimal quantidadeLiquidada = liquidacaoTermoBolsa.Quantidade.Value;
                decimal pu = liquidacaoTermoBolsa.Pu.Value;
                decimal valor = liquidacaoTermoBolsa.Valor.Value;
                int idPosicao = liquidacaoTermoBolsa.IdPosicao.Value;
                #endregion

                #region Busca em PosicaoTermoBolsa o IdTrader da posicao
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(posicaoTermoBolsa.Query.IdTrader);
                if (!posicaoTermoBolsa.LoadByPrimaryKey(campos, idPosicao))
                {
                    return;
                }
                #endregion

                #region Verifica os tipos de depósito/retirada a serem feitos para a ação e para o termo
                string tipoOperacaoAcao;
                if (quantidadeLiquidada > 0)
                {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Deposito;
                }
                else
                {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Retirada;
                }
                #endregion

                decimal quantidadeAbsoluta = Math.Abs(quantidadeLiquidada);

                #region Inserção na OperacaoBolsa para entrada/saída do a vista
                if (posicaoTermoBolsa.IdTrader.HasValue) //Precisa garantir que a posição em termo tinha o IdTrader vinculado!!!!
                {
                    //Retorna a ação objeto do termo
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    string cdAtivoBolsaAcao = ativoBolsa.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                    //
                    GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                    gerOperacaoBolsa.AddNew();
                    gerOperacaoBolsa.IdCliente = idCliente;
                    gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsaAcao;
                    gerOperacaoBolsa.IdTrader = posicaoTermoBolsa.IdTrader;
                    gerOperacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                    gerOperacaoBolsa.TipoOperacao = tipoOperacaoAcao;
                    gerOperacaoBolsa.Data = data;
                    gerOperacaoBolsa.Pu = pu;
                    gerOperacaoBolsa.PULiquido = pu;
                    gerOperacaoBolsa.Valor = valor;
                    gerOperacaoBolsa.ValorLiquido = valor;
                    gerOperacaoBolsa.Quantidade = quantidadeAbsoluta;
                    gerOperacaoBolsa.Origem = OrigemOperacaoBolsa.LiquidacaoTermo;
                    gerOperacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;

                    gerOperacaoBolsaCollection.AttachEntity(gerOperacaoBolsa);
                }
                #endregion
            }

            gerOperacaoBolsaCollection.Save();            
        }
        #endregion
    }
}