﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using Financial.Fundo.Enums;

namespace Financial.Gerencial
{
	public partial class HistoricoEstrategia : esHistoricoEstrategia
	{
        /// <summary>
        /// Processa as estratégias vinculadas a carteira passada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaEstrategia(int idCarteira, DateTime data)
        {
            EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
            estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
            estrategiaCollection.Query.Load();

            foreach (Estrategia estrategia in estrategiaCollection)
            {
                this.AtualizaValorEstrategia(idCarteira, estrategia.IdEstrategia.Value, data);
            }
        }

        /// <summary>
        /// Executa o cálculo da estratégia da carteira passada, na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idEstrategia"></param>
        /// <param name="data"></param>
        private void AtualizaValorEstrategia(int idCarteira, int idEstrategia, DateTime data)
        {
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            decimal quantidadeAnterior = 0;
            decimal cotaAnterior = 0;
            HistoricoEstrategia historicoEstrategia = new HistoricoEstrategia();
            bool historicoAnterior = historicoEstrategia.LoadByPrimaryKey(dataAnterior, idCarteira, idEstrategia);
            if (historicoAnterior)
            {
                quantidadeAnterior = historicoEstrategia.Quantidade.Value;
                cotaAnterior = historicoEstrategia.Cota.Value;
            }

            #region Calcula valor da carteira
            decimal valorCarteira = 0;
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCarteira),
                                    ativoBolsaQuery.IdEstrategia.Equal(idEstrategia));
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.Load(posicaoBolsaQuery);

            if (posicaoBolsa.ValorMercado.HasValue)
            {
                valorCarteira += posicaoBolsa.ValorMercado.Value;
            }
            
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCarteira),
                                    carteiraQuery.IdEstrategia.Equal(idEstrategia));
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Load(posicaoFundoQuery);

            if (posicaoFundo.ValorBruto.HasValue)
            {
                valorCarteira += posicaoFundo.ValorBruto.Value;
            }

            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCarteira),
                                        tituloRendaFixaQuery.IdEstrategia.Equal(idEstrategia));
            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixa.ValorMercado.HasValue)
            {
                valorCarteira += posicaoRendaFixa.ValorMercado.Value;
            }
            #endregion

            #region Verifica valores a liquidar (ou liquidando na data) da estrategia e projeta os valores em LiquidacaoEstrategia
            LiquidacaoEstrategiaCollection liquidacaoEstrategiaCollectionDeletar = new LiquidacaoEstrategiaCollection();
            liquidacaoEstrategiaCollectionDeletar.Query.Select(liquidacaoEstrategiaCollectionDeletar.Query.IdLiquidacao);
            liquidacaoEstrategiaCollectionDeletar.Query.Where(liquidacaoEstrategiaCollectionDeletar.Query.IdCarteira.Equal(idCarteira),
                                                              liquidacaoEstrategiaCollectionDeletar.Query.IdEstrategia.Equal(idEstrategia),
                                                              liquidacaoEstrategiaCollectionDeletar.Query.DataReferencia.Equal(data));
            liquidacaoEstrategiaCollectionDeletar.Query.Load();
            liquidacaoEstrategiaCollectionDeletar.MarkAllAsDeleted();
            liquidacaoEstrategiaCollectionDeletar.Save();

            LiquidacaoEstrategiaCollection liquidacaoEstrategiaCollection = new LiquidacaoEstrategiaCollection();

            #region ProventoBolsa
            ProventoBolsaClienteQuery proventoBolsaClienteQuery = new ProventoBolsaClienteQuery("P");
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            proventoBolsaClienteQuery.Select(proventoBolsaClienteQuery.DataPagamento,
                                             proventoBolsaClienteQuery.Valor.Sum());
            proventoBolsaClienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == proventoBolsaClienteQuery.CdAtivoBolsa);
            proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.IdCliente.Equal(idCarteira),
                                            proventoBolsaClienteQuery.DataEx.LessThanOrEqual(data),    
                                            proventoBolsaClienteQuery.DataPagamento.GreaterThanOrEqual(data),
                                            ativoBolsaQuery.IdEstrategia.Equal(idEstrategia));
            proventoBolsaClienteQuery.GroupBy(proventoBolsaClienteQuery.DataPagamento);
            
            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.Load(proventoBolsaClienteQuery);

            foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
            {
                DateTime dataPagamento = proventoBolsaCliente.DataPagamento.Value;
                decimal valor = proventoBolsaCliente.Valor.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = dataPagamento;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }
            #endregion

            #region OperacaoBolsa (Compra)
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            operacaoBolsaQuery.Select(operacaoBolsaQuery.DataLiquidacao,
                                      operacaoBolsaQuery.ValorLiquido.Sum());
            operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == operacaoBolsaQuery.CdAtivoBolsa);
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente.Equal(idCarteira),
                                     operacaoBolsaQuery.Data.LessThanOrEqual(data),
                                     operacaoBolsaQuery.DataLiquidacao.GreaterThanOrEqual(data),
                                     operacaoBolsaQuery.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade),
                                     ativoBolsaQuery.IdEstrategia.Equal(idEstrategia));
            operacaoBolsaQuery.GroupBy(operacaoBolsaQuery.DataLiquidacao);

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Load(operacaoBolsaQuery);

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                DateTime dataLiquidacao = operacaoBolsa.DataLiquidacao.Value;
                decimal valor = operacaoBolsa.ValorLiquido.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = dataLiquidacao;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor * -1;
            }
            #endregion

            #region OperacaoBolsa (Venda)
            operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            operacaoBolsaQuery.Select(operacaoBolsaQuery.DataLiquidacao,
                                      operacaoBolsaQuery.ValorLiquido.Sum());
            operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == operacaoBolsaQuery.CdAtivoBolsa);
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente.Equal(idCarteira),
                                     operacaoBolsaQuery.Data.LessThanOrEqual(data),
                                     operacaoBolsaQuery.DataLiquidacao.GreaterThanOrEqual(data),
                                     operacaoBolsaQuery.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade),
                                     ativoBolsaQuery.IdEstrategia.Equal(idEstrategia));
            operacaoBolsaQuery.GroupBy(operacaoBolsaQuery.DataLiquidacao);

            operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Load(operacaoBolsaQuery);

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                DateTime dataLiquidacao = operacaoBolsa.DataLiquidacao.Value;
                decimal valor = operacaoBolsa.ValorLiquido.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = dataLiquidacao;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }
            #endregion

            DateTime dataAnteriorBolsa = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            #region OperacaoBMF
            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Ajuste.Sum(),
                                    operacaoBMFQuery.Corretagem.Sum(),
                                    operacaoBMFQuery.Emolumento.Sum(),
                                    operacaoBMFQuery.Registro.Sum(),
                                    operacaoBMFQuery.CustoWtr.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCarteira),
                                   operacaoBMFQuery.Data.Equal(dataAnteriorBolsa),
                                   operacaoBMFQuery.TipoMercado.Equal(TipoMercadoBMF.Futuro),
                                   ativoBMFQuery.IdEstrategia.Equal(idEstrategia));
            
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);                            

            if (operacaoBMF.Ajuste.HasValue)
            {
                decimal valor = operacaoBMF.Ajuste.Value - operacaoBMF.Corretagem.Value - operacaoBMF.Emolumento.Value -
                                        operacaoBMF.Registro.Value - operacaoBMF.CustoWtr.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = data;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Ajuste.Sum(),
                                    operacaoBMFQuery.Corretagem.Sum(),
                                    operacaoBMFQuery.Emolumento.Sum(),
                                    operacaoBMFQuery.Registro.Sum(),
                                    operacaoBMFQuery.CustoWtr.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCarteira),
                                   operacaoBMFQuery.Data.Equal(data),
                                   operacaoBMFQuery.TipoMercado.Equal(TipoMercadoBMF.Futuro),
                                   ativoBMFQuery.IdEstrategia.Equal(idEstrategia));

            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.Ajuste.HasValue)
            {
                decimal valor = operacaoBMF.Ajuste.Value - operacaoBMF.Corretagem.Value - operacaoBMF.Emolumento.Value -
                                        operacaoBMF.Registro.Value - operacaoBMF.CustoWtr.Value;

                DateTime dataProxBolsa = Calendario.AdicionaDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = dataProxBolsa;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }
            #endregion

            #region OperacaoRendaFixa (Compra)
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.Valor.Sum());
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCarteira),
                                   operacaoRendaFixaQuery.DataLiquidacao.Equal(data),
                                   operacaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                   tituloRendaFixaQuery.IdEstrategia.Equal(idEstrategia));

            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            operacaoRendaFixa.Load(operacaoRendaFixaQuery);

            if (operacaoRendaFixa.Valor.HasValue)
            {
                decimal valor = operacaoRendaFixa.Valor.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = data;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor * -1;
            }
            #endregion

            #region OperacaoRendaFixa (Venda)
            operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.Valor.Sum(),
                                          operacaoRendaFixaQuery.ValorIR.Sum(),
                                          operacaoRendaFixaQuery.ValorIOF.Sum());
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCarteira),
                                   operacaoRendaFixaQuery.DataLiquidacao.Equal(data),
                                   operacaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.VendaFinal),
                                   tituloRendaFixaQuery.IdEstrategia.Equal(idEstrategia));

            operacaoRendaFixa = new OperacaoRendaFixa();
            operacaoRendaFixa.Load(operacaoRendaFixaQuery);

            if (operacaoRendaFixa.Valor.HasValue)
            {
                decimal valor = operacaoRendaFixa.Valor.Value - operacaoRendaFixa.ValorIR.Value - operacaoRendaFixa.ValorIOF.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = data;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }
            #endregion

            #region OperacaoFundo (Aplicacao)
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            operacaoFundoQuery.Select(operacaoFundoQuery.ValorBruto.Sum());
            operacaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente.Equal(idCarteira),
                                   operacaoFundoQuery.DataConversao.Equal(data),
                                   operacaoFundoQuery.TipoOperacao.Equal((byte)TipoOperacaoFundo.Aplicacao),
                                   carteiraQuery.IdEstrategia.Equal(idEstrategia));

            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.Load(operacaoFundoQuery);

            if (operacaoFundo.ValorLiquido.HasValue)
            {
                decimal valor = operacaoFundo.ValorLiquido.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = data;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor * -1;
            }
            #endregion

            #region OperacaoFundo (Resgate)
            operacaoFundoQuery = new OperacaoFundoQuery("O");
            carteiraQuery = new CarteiraQuery("C");
            operacaoFundoQuery.Select(operacaoFundoQuery.DataLiquidacao, 
                                      operacaoFundoQuery.ValorLiquido.Sum());
            operacaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente.Equal(idCarteira),
                                   operacaoFundoQuery.DataConversao.LessThanOrEqual(data),
                                   operacaoFundoQuery.DataLiquidacao.GreaterThanOrEqual(data),
                                   operacaoFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                       (byte)TipoOperacaoFundo.ResgateCotas,
                                                                       (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                       (byte)TipoOperacaoFundo.ResgateTotal),
                                   carteiraQuery.IdEstrategia.Equal(idEstrategia));
            operacaoFundoQuery.GroupBy(operacaoFundoQuery.DataLiquidacao);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Load(operacaoFundoQuery);

            foreach (OperacaoFundo operacaoFundoResgate in operacaoFundoCollection)
            {
                DateTime dataLiquidacao = operacaoFundoResgate.DataLiquidacao.Value;
                decimal valor = operacaoFundoResgate.ValorLiquido.Value;

                LiquidacaoEstrategia liquidacaoEstrategia = liquidacaoEstrategiaCollection.AddNew();
                liquidacaoEstrategia.DataReferencia = data;
                liquidacaoEstrategia.DataVencimento = dataLiquidacao;
                liquidacaoEstrategia.IdCarteira = idCarteira;
                liquidacaoEstrategia.IdEstrategia = idEstrategia;
                liquidacaoEstrategia.Valor = valor;
            }
            #endregion
            
            liquidacaoEstrategiaCollection.Save();
            #endregion

            #region Calcula liquidado na data da estrategia
            LiquidacaoEstrategia liquidacaoEstrategiaSum = new LiquidacaoEstrategia();
            liquidacaoEstrategiaSum.Query.Select(liquidacaoEstrategiaSum.Query.Valor.Sum());
            liquidacaoEstrategiaSum.Query.Where(liquidacaoEstrategiaSum.Query.IdCarteira.Equal(idCarteira),
                                             liquidacaoEstrategiaSum.Query.IdEstrategia.Equal(idEstrategia),
                                             liquidacaoEstrategiaSum.Query.DataReferencia.Equal(data),
                                             liquidacaoEstrategiaSum.Query.DataVencimento.Equal(data));
            liquidacaoEstrategiaSum.Query.Load();

            decimal valorLiquidacaoDia = 0;
            if (liquidacaoEstrategiaSum.Valor.HasValue)
            {
                valorLiquidacaoDia = liquidacaoEstrategiaSum.Valor.Value;
            }            
            #endregion

            decimal valorLiquidar = 0;
            #region Calcula Valores a Liquidar
            LiquidacaoEstrategia liquidacaoEstrategiaLiquidar = new LiquidacaoEstrategia();
            liquidacaoEstrategiaLiquidar.Query.Select(liquidacaoEstrategiaLiquidar.Query.Valor.Sum());
            liquidacaoEstrategiaLiquidar.Query.Where(liquidacaoEstrategiaLiquidar.Query.IdCarteira.Equal(idCarteira),
                                             liquidacaoEstrategiaLiquidar.Query.IdEstrategia.Equal(idEstrategia),
                                             liquidacaoEstrategiaLiquidar.Query.DataReferencia.Equal(data),
                                             liquidacaoEstrategiaLiquidar.Query.DataVencimento.GreaterThan(data));
            liquidacaoEstrategiaLiquidar.Query.Load();

            if (liquidacaoEstrategiaLiquidar.Valor.HasValue)
            {
                valorLiquidar = liquidacaoEstrategiaLiquidar.Valor.Value;
            }
            #endregion

            decimal valorPL = valorCarteira + valorLiquidar;

            if (!historicoAnterior && valorPL == 0)
            {
                return;
            }

            decimal quantidade = quantidadeAnterior;
            if (quantidade == 0 && cotaAnterior != 0)
            {
                quantidade = valorPL / cotaAnterior;
            }

            #region Calcula Quantidade da carteira (se valorLiquidacaoDia != 0, gera aporte ou resgate)
            if (valorLiquidacaoDia != 0 && historicoAnterior)
            {
                decimal quantidadeOperacao = 0;
                if (valorLiquidacaoDia > 0)
                {
                    quantidadeOperacao = (valorLiquidacaoDia / cotaAnterior) * -1;
                }
                else
                {
                    quantidadeOperacao = (Math.Abs(valorLiquidacaoDia) / cotaAnterior);
                }

                quantidade += quantidadeOperacao;
            }
            #endregion
                        
            decimal cotaDia = 0;
            if (!historicoAnterior)
            {
                cotaDia = 1;
                quantidade = valorPL / cotaDia;
            }
            else if (valorPL == 0)
            {
                cotaDia = cotaAnterior;
                valorLiquidar = 0;
                quantidade = 0;
            }
            else
            {
                cotaDia = valorPL / quantidade;
            }

            HistoricoEstrategia historicoEstrategiaAtual = new HistoricoEstrategia();
            if (historicoEstrategiaAtual.LoadByPrimaryKey(data, idCarteira, idEstrategia))
            {
                historicoEstrategiaAtual.Cota = cotaDia;
                historicoEstrategiaAtual.ValorPL = valorPL;
                historicoEstrategiaAtual.Quantidade = quantidade;
                historicoEstrategiaAtual.SaldoCaixa = valorLiquidacaoDia; //Apenas mostra o liquidado na data
                historicoEstrategiaAtual.ValorCarteira = valorCarteira;
                historicoEstrategiaAtual.ValorLiquidar = valorLiquidar;
                historicoEstrategiaAtual.Save();
            }
            else
            {
                HistoricoEstrategia historicoEstrategiaNovo = new HistoricoEstrategia();
                historicoEstrategiaNovo.IdEstrategia = idEstrategia;
                historicoEstrategiaNovo.IdCarteira = idCarteira;
                historicoEstrategiaNovo.Data = data;
                historicoEstrategiaNovo.Cota = cotaDia;
                historicoEstrategiaNovo.ValorPL = valorPL;
                historicoEstrategiaNovo.Quantidade = quantidade;
                historicoEstrategiaNovo.SaldoCaixa = 0; //Hoje o caixa é sempre considerado como zero
                historicoEstrategiaNovo.ValorCarteira = valorCarteira;
                historicoEstrategiaNovo.ValorLiquidar = valorLiquidar;
                historicoEstrategiaNovo.Save();
            }

        }
        
        private DateTime? RetornaDataInicio(int idCarteira, int idEstrategia)
        {
            DateTime? data = null;
            HistoricoEstrategia historicoEstrategia = new HistoricoEstrategia();
            historicoEstrategia.Query.Select(historicoEstrategia.Query.Data.Min());
            historicoEstrategia.Query.Where(historicoEstrategia.Query.IdCarteira.Equal(idCarteira),
                                          historicoEstrategia.Query.IdEstrategia.Equal(idEstrategia));
            historicoEstrategia.Query.es.Distinct = true;
            historicoEstrategia.Query.Load();

            if (historicoEstrategia.Data.HasValue)
            {
                data = historicoEstrategia.Data.Value;
            }

            return data;
        }

        private decimal? RetornaCotaEstrategia(int idCarteira, int idEstrategia, DateTime data)
        {
            decimal? cota = null;

            HistoricoEstrategiaCollection historicoEstrategiaCollection = new HistoricoEstrategiaCollection();
            historicoEstrategiaCollection.Query.Select(historicoEstrategiaCollection.Query.Cota);
            historicoEstrategiaCollection.Query.Where(historicoEstrategiaCollection.Query.IdCarteira.Equal(idCarteira),
                                                      historicoEstrategiaCollection.Query.IdEstrategia.Equal(idEstrategia),
                                                      historicoEstrategiaCollection.Query.Data.Equal(data));
            historicoEstrategiaCollection.Query.Load();

            if (historicoEstrategiaCollection.Count > 0)
            {
                cota = historicoEstrategiaCollection[0].Cota.Value;
            }

            return cota;
        }

	}
}
