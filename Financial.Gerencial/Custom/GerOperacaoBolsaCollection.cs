﻿using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Bolsa.Enums;

using log4net;
using System.Collections.Generic;
using System.Text;

namespace Financial.Gerencial {
    public partial class GerOperacaoBolsaCollection : esGerOperacaoBolsaCollection {

        private static readonly ILog log = LogManager.GetLogger(typeof(GerOperacaoBolsaCollection));

        /// <summary>
        /// Busca operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idTrader"></param>
        /// <param name="origemOperacaoBolsa"></param>
        /// <exception cref="ArgumentException">se Origem for diferente dos tipos do enum OrigemOperacaoBolsa</exception>
        public void BuscaOperacaoBolsa(int idCliente, DateTime data, string cdAtivoBolsa, int idTrader, int origemOperacaoBolsa) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBolsa incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");

            List<int> valoresPossiveis = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis.Contains(origemOperacaoBolsa)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(this.Query.Data == data,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.IdCliente == idCliente,
                        this.Query.IdTrader == idTrader,
                        this.Query.Origem == origemOperacaoBolsa);

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações do dia. Não traz operações de Termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaValores(int idCliente, DateTime dataOperacao) {
          
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente,
                        this.Query.Quantidade, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.Despesas,                        
                        this.Query.Valor, this.Query.TipoOperacao)
                 .Where(this.Query.Data == dataOperacao &
                        this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Compra |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Venda |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade
                        )
                   );

            this.Query.Load();
            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                sql = sql.Replace("@TipoOperacao3", "'" + TipoOperacaoBolsa.Compra + "'");
                sql = sql.Replace("@TipoOperacao4", "'" + TipoOperacaoBolsa.CompraDaytrade + "'");
                sql = sql.Replace("@TipoOperacao5", "'" + TipoOperacaoBolsa.Venda + "'");
                sql = sql.Replace("@TipoOperacao6", "'" + TipoOperacaoBolsa.VendaDaytrade + "'");
                log.Info(sql);
            }

            #endregion
        }

        /// <summary>
        /// Busca operações de Venda no Mercado a Vista do dia. 
        /// Usada no casamento de vencimento Termo/Exercicio.            
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVendaCasaVencimentoTermo(int idCliente, DateTime dataOperacao) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Origem, this.Query.PULiquido,
                         this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data.Equal(dataOperacao) &
                         this.Query.IdCliente == idCliente &

                         (
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista))
                             |
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Retirada) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                              this.Query.Origem.Equal(OrigemOperacaoBolsa.LiquidacaoTermo))
                         )
                 )
                 .OrderBy(this.Query.IdTrader.Ascending,
                          this.Query.CdAtivoBolsa.Ascending,
                          this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
            
            this.Query.Load();
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivo">codigo do Ativo usado para Filtro</param>
        /// <returns>True se foi achado algum registro
        ///          False caso contrario
        /// </returns>
        public bool BuscaOperacaoBolsaCompraCasaVencimentoTermo(int idCliente, DateTime dataOperacao, int idTrader, string cdAtivo) {
            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Data, this.Query.Origem,
                         this.Query.PULiquido, this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data == dataOperacao,
                         this.Query.IdCliente == idCliente,
                         this.Query.IdTrader == idTrader,
                         this.Query.CdAtivoBolsa == cdAtivo,

                         this.Query.Or(
                                this.Query.And(
                                    this.Query.TipoOperacao == TipoOperacaoBolsa.Compra,
                                    this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista
                                 ),
                                 this.Query.And(
                                    this.Query.TipoOperacao == TipoOperacaoBolsa.Deposito,
                                    this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista,
                                    this.Query.Origem == OrigemOperacaoBolsa.LiquidacaoTermo
                             )
                         )
                 )
                 .OrderBy(this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
            
            return this.Query.Load();            
        }

        /// <summary>
        ///  Busca operações de Venda no Mercado a Vista do dia. Usada no casamento de vencimento Termo/Exercicio.   
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVendaCasaVencimentoExercicio(int idCliente, DateTime dataOperacao) {
            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Origem, this.Query.PULiquido,
                         this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data == dataOperacao &
                         this.Query.IdCliente == idCliente &

                         (
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista))
                             |
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Retirada) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                              this.Query.Origem.Equal(OrigemOperacaoBolsa.LiquidacaoTermo))
                         )
                 )
                 .OrderBy(this.Query.IdTrader.Ascending,
                          this.Query.CdAtivoBolsa.Ascending,
                          this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
            
            this.Query.Load();            
        }

        /// <summary>
        /// Busca operações de Compra no Mercado a Vista do dia. Usada no casamento de vencimento Termo/Exercicio.       
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivo"></param>
        /// <returns>Indica se achou registro</returns>
        public bool BuscaOperacaoBolsaCompraCasaVencimentoExercicio(int idCliente, DateTime dataOperacao, int idTrader, string cdAtivo) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                        this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                        this.Query.QuantidadeCasadaTermo, this.Query.Data, this.Query.Origem,
                        this.Query.PULiquido, this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                        this.Query.Data == dataOperacao,
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao == TipoOperacaoBolsa.Compra,
                        this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista,
                        this.Query.IdTrader == idTrader,
                        this.Query.CdAtivoBolsa == cdAtivo
                 )
                 .OrderBy(this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);


            return this.Query.Load();            
        }

        /// <summary>
        /// Busca operações para o caso de uso CompoeCustoOpcao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaCompoeCustoOpcao(int idCliente, DateTime data) {
          
            this.QueryReset();
            this.Query
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(OrigemOperacaoBolsa.ExercicioOpcaoCompra, OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                  );

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                string origem = "'" + OrigemOperacaoBolsa.ExercicioOpcaoCompra + "','" + OrigemOperacaoBolsa.ExercicioOpcaoVenda + "'";
                sql = sql.Replace("@Origem3", origem);
                log.Info(sql);
            }
            #endregion
        }

        /// <summary>
        /// Busca operações de Compra/Deposito do dia. Não traz operações de termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaCompra(int idCliente, DateTime dataOperacao) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data == dataOperacao &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.VencimentoOpcao) &
                        this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Compra |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Deposito
                        )
                 );

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                sql = sql.Replace("@TipoOperacao3", "'" + TipoOperacaoBolsa.Compra + "'");
                sql = sql.Replace("@TipoOperacao4", "'" + TipoOperacaoBolsa.Deposito + "'");
                log.Info(sql);
            }

            #endregion
        }

        /// <summary>
        /// Busca operações de Venda/Retirada do dia. Não traz operações de termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVenda(int idCliente, DateTime dataOperacao) {
            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdTrader,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data == dataOperacao &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.VencimentoOpcao) &
                        this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Venda |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Retirada
                        )
                 );

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                sql = sql.Replace("@TipoOperacao3", "'" + TipoOperacaoBolsa.Venda + "'");
                sql = sql.Replace("@TipoOperacao4", "'" + TipoOperacaoBolsa.Retirada + "'");
                log.Info(sql);
            }

            #endregion           
        }

        /// <summary>
        /// Zera as Quantidades Casadas e Resultados de Termo e Exercicio.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="data"></param>        
        public void ZeraValoresCasamamentoTermoExecicio(int idCliente, DateTime data) {
           
            this.Query.Select(this.Query.IdOperacao, 
                              this.Query.QuantidadeCasadaExercicio, 
                              this.Query.QuantidadeCasadaTermo,
                              this.Query.ResultadoExercicio, 
                              this.Query.ResultadoTermo);

            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.Data == data);

            this.Query.Load();

            foreach (GerOperacaoBolsa operacaoBolsa in this) {
                operacaoBolsa.QuantidadeCasadaExercicio = 0;
                operacaoBolsa.QuantidadeCasadaTermo = 0;
                operacaoBolsa.ResultadoExercicio = 0;
                operacaoBolsa.ResultadoTermo = 0;
            }

            this.Save();            
        }

        /// <summary>
        /// Busca Operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="cdAtivoBolsaObjeto"></param>
        /// <param name="idTrader"></param>
        public void BuscaOperacaoBolsaExercicio(int idCliente, DateTime data, string cdAtivoBolsa, string cdAtivoBolsaObjeto, int idTrader) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(this.Query.Data == data,
                        this.Query.CdAtivoBolsaOpcao == cdAtivoBolsa,
                        this.Query.CdAtivoBolsa == cdAtivoBolsaObjeto,
                        this.Query.IdCliente == idCliente,
                        this.Query.IdTrader == idTrader,
                        this.Query.Origem.In((int)OrigemOperacaoBolsa.ExercicioOpcaoCompra,
                                             (int)OrigemOperacaoBolsa.ExercicioOpcaoVenda));

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="origem"></param>
        public void DeletaOperacaoBolsa(int idCliente, List<int> origem, DateTime data) {
           
            #region ArgumentosNulos - throw ArgumentException
            /*StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBolsa incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");

            List<int> valoresPossiveis = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis.Find(origem)) {
                throw new ArgumentException(mensagem.ToString());
            }
             */
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origem),
                        this.Query.Data == data);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as operações em GerOperacaoBolsa com Fonte != (Interno, Manual).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void DeletaOperacaoOrdemCasada(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.NotIn((int)FonteOperacaoBolsa.Interno, (int)FonteOperacaoBolsa.Manual),
                        this.Query.Data.Equal(data));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();         
        }
    }
}