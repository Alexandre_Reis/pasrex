﻿using System;
using System.Text;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Collections.Generic;
using log4net;
using Financial.BMF.Enums;

namespace Financial.Gerencial {
    public partial class GerOperacaoBMFCollection : esGerOperacaoBMFCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerOperacaoBMFCollection));
        
        /// <summary>
        /// Carrega o objeto GerOperacaoBMFCollection com os campos 
        /// IdOperacao, CdAtivoBMF, Serie, IdTrader, Quantidade, TipoOperacao, Pu, Ajuste.        
        /// Entram todos os futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBMFCalculaAjuste(int idCliente, DateTime data) {
            const string DLA = "DLA";
            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdTrader,
                         this.Query.Quantidade, this.Query.TipoOperacao, this.Query.Pu, this.Query.Ajuste)
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo) &
                        this.Query.Data == data &
                        (
                            this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                            this.Query.CdAtivoBMF == DLA)
                        );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto GerOperacaoBMFCollection com os campos 
        /// IdOperacao, IdCliente, CdAtivoBMF, Serie, IdTrader,
        /// TipoMercado, Quantidade, Pu, PULiquido, ValorLiquido, TipoOperacao, ResultadoRealizado, Ajuste.
        /// Busca operações de Compra e Depósito.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFCompra(int idCliente, DateTime dataOperacao) {            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                         this.Query.IdTrader, this.Query.TipoMercado, this.Query.Quantidade, this.Query.Pu,
                         this.Query.PULiquido, this.Query.ValorLiquido, this.Query.TipoOperacao,
                         this.Query.ResultadoRealizado, this.Query.Ajuste)
                 .Where(
                        this.Query.Data == dataOperacao,
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.Deposito),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                 );

            this.Query.Load();                      
        }

        /// <summary>
        /// Carrega o objeto GerOperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, IdTrader,
        /// TipoMercado, Quantidade, Pu, PULiquido, ValorLiquido, TipoOperacao, ResultadoRealizado, Ajuste.
        /// Busca operações de Venda e Retirada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFVenda(int idCliente, DateTime dataOperacao) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                         this.Query.IdTrader, this.Query.TipoMercado, this.Query.Quantidade, this.Query.Pu,
                         this.Query.PULiquido, this.Query.ValorLiquido, this.Query.TipoOperacao, this.Query.Ajuste,
                         this.Query.ResultadoRealizado)
                 .Where(
                        this.Query.Data == dataOperacao,
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.Retirada),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Deleta operações do dia, dada a origem informada.
        /// /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="origem"></param>
        /// <exception cref="ArgumentException">De Origem for diferente dos tipos do enum OrigemOperacaoBMF</exception>
        public void DeletaOperacaoBMF(int idCliente, int origem, DateTime data) {

            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBMF incorreto. Os valores possiveis estão no enum OrigemOperacaoBMF");

            List<int> valoresPossiveis = OrigemOperacaoBMF.Values();
            if (!valoresPossiveis.Contains(origem)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem == origem,
                        this.Query.Data == data);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto GerOperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, TipoMercado,
        /// Quantidade, PULiquido, ValorLiquido, Pu, Despesas, Valor, TipoOperacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBMFValores(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                        this.Query.TipoMercado, this.Query.Quantidade, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.Despesas,
                        this.Query.Valor, this.Query.TipoOperacao)
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente.Equal(idCliente),
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();            
        }

        /// <summary>
        /// Deleta todas as operações com Fonte=OrdemBMF e Fonte=RCOMIESP.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOperacaoOrdemCasada(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.In((byte)FonteOperacaoBMF.ArquivoRCOMIESP,
                                            (byte)FonteOperacaoBMF.OrdemBMF),
                        this.Query.Data.Equal(data));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}