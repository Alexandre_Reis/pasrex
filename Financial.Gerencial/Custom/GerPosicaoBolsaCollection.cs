﻿using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

namespace Financial.Gerencial {
    public partial class GerPosicaoBolsaCollection : esGerPosicaoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBolsaCollection));

        /// <summary>
        /// Carrega o objeto GerPosicaoBolsaCollection com os campos IdPosicao, CdAtivoBolsa, Quantidade,
        /// ValorMercado, ResultadoRealizar, PUMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBolsa(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBolsa, this.Query.Quantidade, this.Query.PUCusto,
                         this.Query.ValorMercado, this.Query.ResultadoRealizar, this.Query.PUMercado)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto GerPosicaoBolsaCollection com os campos IdPosicao, CdAtivoBolsa, Quantidade,
        /// IdCliente, PUCusto, CdAtivoBolsa, IdTrader, PUMercado, ValorMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBolsa(int idCliente, DateTime dataVencimento) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBolsa, this.Query.Quantidade,
                         this.Query.IdCliente, this.Query.PUCusto,
                         this.Query.CdAtivoBolsa, this.Query.IdTrader, this.Query.PUMercado,
                         this.Query.ValorMercado)
                 .Where(this.Query.DataVencimento == dataVencimento,
                        this.Query.IdCliente == idCliente,
                        this.Query.Quantidade != 0);

            return this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto GerPosicaoBolsaCollection com todos os campos de GerPosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBolsaCompleta(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as posições do idCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoBolsa(int idCliente)
        {

            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em GerPosicaoBolsa, a partir de GerPosicaoBolsaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoBolsaHistorico(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            GerPosicaoBolsaHistoricoCollection gerPosicaoBolsaHistoricoCollection = new GerPosicaoBolsaHistoricoCollection();
            gerPosicaoBolsaHistoricoCollection.BuscaPosicaoBolsaHistoricoCompleta(idCliente, dataHistorico);
            for (int i = 0; i < gerPosicaoBolsaHistoricoCollection.Count; i++)
            {
                GerPosicaoBolsaHistorico gerPosicaoBolsaHistorico = gerPosicaoBolsaHistoricoCollection[i];
                // Copia para GerPosicaoBolsa
                GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
                gerPosicaoBolsa.IdPosicao = gerPosicaoBolsaHistorico.IdPosicao;
                gerPosicaoBolsa.IdTrader = gerPosicaoBolsaHistorico.IdTrader;
                gerPosicaoBolsa.IdCliente = gerPosicaoBolsaHistorico.IdCliente;
                gerPosicaoBolsa.CdAtivoBolsa = gerPosicaoBolsaHistorico.CdAtivoBolsa;
                gerPosicaoBolsa.TipoMercado = gerPosicaoBolsaHistorico.TipoMercado;
                gerPosicaoBolsa.DataVencimento = gerPosicaoBolsaHistorico.DataVencimento;
                gerPosicaoBolsa.PUMercado = gerPosicaoBolsaHistorico.PUMercado;
                gerPosicaoBolsa.PUCusto = gerPosicaoBolsaHistorico.PUCusto;
                gerPosicaoBolsa.ValorMercado = gerPosicaoBolsaHistorico.ValorMercado;
                gerPosicaoBolsa.Quantidade = gerPosicaoBolsaHistorico.Quantidade;
                gerPosicaoBolsa.ResultadoRealizar = gerPosicaoBolsaHistorico.ResultadoRealizar;
                //
                this.AttachEntity(gerPosicaoBolsa);
            }

            if (this.HasData)
            {
                this.Save();
            }
        }

        /// <summary>
        /// Método de inserção em GerPosicaoBolsa, a partir de GerPosicaoBolsaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoBolsaAbertura(int idCliente, DateTime dataAbertura)
        {
            this.QueryReset();
            GerPosicaoBolsaAberturaCollection gerPosicaoBolsaAberturaCollection = new GerPosicaoBolsaAberturaCollection();
            gerPosicaoBolsaAberturaCollection.BuscaPosicaoBolsaAberturaCompleta(idCliente, dataAbertura);
            for (int i = 0; i < gerPosicaoBolsaAberturaCollection.Count; i++)
            {
                GerPosicaoBolsaAbertura gerPosicaoBolsaAbertura = gerPosicaoBolsaAberturaCollection[i];
                // Copia para GerPosicaoBolsa
                GerPosicaoBolsa gerPosicaoBolsa = new GerPosicaoBolsa();
                gerPosicaoBolsa.IdPosicao = gerPosicaoBolsaAbertura.IdPosicao;
                gerPosicaoBolsa.IdTrader = gerPosicaoBolsaAbertura.IdTrader;
                gerPosicaoBolsa.IdCliente = gerPosicaoBolsaAbertura.IdCliente;
                gerPosicaoBolsa.CdAtivoBolsa = gerPosicaoBolsaAbertura.CdAtivoBolsa;
                gerPosicaoBolsa.TipoMercado = gerPosicaoBolsaAbertura.TipoMercado;
                gerPosicaoBolsa.DataVencimento = gerPosicaoBolsaAbertura.DataVencimento;
                gerPosicaoBolsa.PUMercado = gerPosicaoBolsaAbertura.PUMercado;
                gerPosicaoBolsa.PUCusto = gerPosicaoBolsaAbertura.PUCusto;
                gerPosicaoBolsa.ValorMercado = gerPosicaoBolsaAbertura.ValorMercado;
                gerPosicaoBolsa.Quantidade = gerPosicaoBolsaAbertura.Quantidade;
                gerPosicaoBolsa.ResultadoRealizar = gerPosicaoBolsaAbertura.ResultadoRealizar;
                //
                this.AttachEntity(gerPosicaoBolsa);
            }

            if (this.HasData)
            {
                this.Save();
            }
        }
    }
}