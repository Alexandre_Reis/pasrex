﻿using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;
using System.Collections.Generic;

using Financial.Util;
using Financial.BMF;

using Financial.BMF.Enums;
using Financial.Common.Enums;

using Financial.BMF.Exceptions;
using System.Data.SqlClient;

namespace Financial.Gerencial {
    public partial class GerPosicaoBMF : esGerPosicaoBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBMF));

        /// <summary>
        /// Carrega o objeto GerPosicaoBMF com os campos 
        /// IdPosicao, PUCusto, Quantidade, IdCliente, CdAtivoBMF, Serie.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivo"></param>
        /// <param name="serie"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMF(int idCliente, int idTrader, string cdAtivo, string serie) {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdPosicao, this.query.PUCusto, this.query.Quantidade,
                         this.query.IdCliente, this.query.CdAtivoBMF, this.query.Serie)
                 .Where(this.query.IdTrader == idTrader,
                        this.query.IdCliente == idCliente,
                        this.query.CdAtivoBMF == cdAtivo,
                        this.query.Serie == serie);

            return this.Query.Load();
        }

        /// <summary>
        /// Calcula ajuste de posição de futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaAjustePosicao(int idCliente, DateTime data) {
            const string DOL = "DOL";
            const string WDL = "WDL";
            const string EUR = "EUR";
            const string DDI = "DDI";

            GerPosicaoBMFCollection posicaoBMFCollection = new GerPosicaoBMFCollection();
            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();

            posicaoBMFCollection.BuscaPosicaoBMFCalculoAjuste(idCliente);

            if (posicaoBMFCollection.Count > 0) { //Deleta as operações internas com origem = Ajuste
                GerOperacaoBMFCollection operacaoBMFCollectionDeletar = new GerOperacaoBMFCollection();
                operacaoBMFCollectionDeletar.DeletaOperacaoBMF(idCliente, (byte)OrigemOperacaoBMF.AjusteFuturo, data);
            }

            for (int i = 0; i < posicaoBMFCollection.Count; i++) {
                #region Valores PosicaoBMF
                GerPosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idTrader = posicaoBMF.IdTrader.Value;
                int quantidade = posicaoBMF.Quantidade.Value;
                byte tipoMercado = posicaoBMF.TipoMercado.Value;
                #endregion

                DateTime dataVencimento = posicaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento.Value;

                //Futuro de Dolar, MiniDolar e Euro só calculam o ajuste até D-1 do vencimento.
                if ((cdAtivoBMF == DOL || cdAtivoBMF == WDL || cdAtivoBMF == EUR) && (dataVencimento == data)) {
                    continue;
                }

                decimal peso = posicaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value;

                #region Cotacoes de cdAtivoBMF
                CotacaoBMF cotacaoBMF = new CotacaoBMF();

                //Busca cotacoes do dia e do dia anterior
                //Futuros de Juros (DI1, DDI) usam o PU corrigido de D0, os outros usam o PU de fechamento de D-1 
                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                decimal puFechamento = 0;

                if (cotacaoBMF.PUGerencial.HasValue)
                {
                    puFechamento = cotacaoBMF.PUGerencial.Value;
                }
                else
                {
                    puFechamento = cotacaoBMF.PUFechamento.Value;
                }

                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil); ;
                decimal puAnterior;

                if (AtivoBMF.IsAtivoJuros(cdAtivoBMF)) 
                {
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                    puAnterior = cotacaoBMF.PUCorrigido.Value;
                }
                else 
                {
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, dataAnterior);

                    if (cotacaoBMF.PUGerencial.HasValue)
                    {
                        puAnterior = cotacaoBMF.PUGerencial.Value;
                    }
                    else
                    {
                        puAnterior = cotacaoBMF.PUFechamento.Value;
                    }
                }
                #endregion

                #region Cotacao PTax
                //DDI usa a Ptax de D-1, os outros usam a Ptax de D0

                decimal ptax;
                if (cdAtivoBMF == DDI) {
                    ptax = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, dataAnterior);
                }
                else {
                    ptax = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, dataAnterior);
                }
                #endregion

                decimal ajuste = Utilitario.Truncate(Utilitario.Truncate((puFechamento - puAnterior), 7) * peso * ptax, 2);
                ajuste = Utilitario.Truncate(ajuste * quantidade, 2);

                string tipoOperacao = quantidade > 0 ? TipoOperacaoBMF.Compra : TipoOperacaoBMF.Venda;

                #region Lanca em OperacaoBMF o ajuste de posição
                GerOperacaoBMF operacaoBMF = operacaoBMFCollection.AddNew();
                operacaoBMF.IdCliente = idCliente;
                operacaoBMF.IdTrader = IdTrader;
                operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                operacaoBMF.Serie = serie;
                operacaoBMF.Ajuste = ajuste;
                operacaoBMF.Data = data;
                operacaoBMF.Origem = (byte)OrigemOperacaoBMF.AjusteFuturo;
                operacaoBMF.Quantidade = quantidade;
                operacaoBMF.TipoMercado = tipoMercado;
                operacaoBMF.TipoOperacao = tipoOperacao;
                operacaoBMF.Pu = 0;
                operacaoBMF.IdTrader = posicaoBMF.IdTrader.Value;
                operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Interno;
                #endregion
            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Deleta posições vencidas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaVencimento(int idCliente, DateTime data) {
            GerPosicaoBMFCollection posicaoBMFCollection = new GerPosicaoBMFCollection();

            posicaoBMFCollection.BuscaPosicaoBMFVencimentoDia(idCliente, data);

            posicaoBMFCollection.MarkAllAsDeleted();
            posicaoBMFCollection.Save();
        }

        /// <summary>
        /// Deleta todos os ajustes diários lançados no dia, por boleta.
        /// Serve para Garantir em caso de feriado BMF na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ZeraAjusteDiario(int idCliente, DateTime data) {
            // Deleta Operações com Origem = Ajuste            
            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.IdOperacao);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente == idCliente,
                                               operacaoBMFCollection.Query.Data == data,
                                               operacaoBMFCollection.Query.Origem.Equal((byte)OrigemOperacaoBMF.AjusteFuturo));
            operacaoBMFCollection.Query.Load();
            operacaoBMFCollection.MarkAllAsDeleted();
            operacaoBMFCollection.Save();
        }

        /// <summary>
        ///  Atualiza ValorMercado, ValorCustoLiquido, ResultadoRealizar e PuMercado de uma posicao, 
        ///  dado um cliente e uma data de cotação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <exception cref="ClienteBMFNaoCadastradoException">Quando não Existe ClienteBMF para o IdCliente passado</exception>
        public void AtualizaValores(int idCliente, DateTime data) {
            CotacaoBMF cotacao = new CotacaoBMF();
            GerPosicaoBMFCollection posicaoBMFCollection = new GerPosicaoBMFCollection();
            GerPosicaoBMF posicaoBMF = new GerPosicaoBMF();

            // Arraylist com os Ids das posicões Zeradas
            List<int> listaPosicaoZerada = new List<int>();
            posicaoBMFCollection.BuscaPosicaoBMF(idCliente);

            for (int i = 0; i < posicaoBMFCollection.Count; i++) {
                posicaoBMF = posicaoBMFCollection[i];

                // Se posicaoBMF.Quantidade != 0 - update das posições
                if (posicaoBMF.Quantidade != 0) {
                    #region Busca cotação média ou fechamento, de acordo com o perfil do cliente BMF
                    cotacao.BuscaCotacaoBMF(posicaoBMF.CdAtivoBMF, posicaoBMF.Serie, data);

                    //Verifico se o cliente usa cotação média ou de fechamento
                    ClienteBMF clienteBMF = new ClienteBMF();
                    clienteBMF.LoadByPrimaryKey(idCliente);

                    if (!clienteBMF.es.HasData) {
                        throw new ClienteBMFNaoCadastradoException("Cliente " + idCliente + " sem cadastro para BMF.");
                    }

                    decimal cotacaoDia;
                    if (clienteBMF.TipoCotacao == (int)TipoCotacaoBMF.Medio) {
                        if (!cotacao.PUMedio.HasValue) {
                            cotacao.PUMedio = 0;
                        }
                        cotacaoDia = cotacao.PUMedio.Value;
                    }
                    else {
                        if (!cotacao.PUFechamento.HasValue) {
                            cotacao.PUFechamento = 0;
                        }
                        cotacaoDia = cotacao.PUFechamento.Value;
                    }
                    #endregion

                    decimal peso = posicaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                    decimal valorMercado = (posicaoBMF.Quantidade.Value * cotacaoDia) * peso;
                    decimal resultadoRealizar = (posicaoBMF.Quantidade.Value * (cotacaoDia - posicaoBMF.PUCusto.Value)) * peso;

                    posicaoBMF.PUMercado = cotacaoDia;
                    posicaoBMF.ValorMercado = valorMercado;
                    posicaoBMF.ResultadoRealizar = resultadoRealizar;
                }
                // Se posicaoBMF.Quantidade = 0 - Delete das posições
                else {
                    listaPosicaoZerada.Add(posicaoBMF.IdPosicao.Value);
                }
            }

            posicaoBMFCollection.Save();

            // Deleta as Posicões zeradas
            this.ExcluiPosicaoBMFZerada(listaPosicaoZerada);
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU), para o caso de posição comprada e operação de Compra.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>
        public void AtualizaPosicaoCompraComprado(int idPosicao, int quantidade, decimal pu) {
            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            decimal totalFinanceiroAntes = this.Quantidade.Value * this.PUCusto.Value;
            decimal totalFinanceiroCompra = quantidade * pu;

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroCompra) / this.Quantidade;
            
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU) para o caso de posição Comprada e operação de Venda.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>
        public void AtualizaPosicaoCompraVendido(int idPosicao, int quantidade, decimal pu) {            
            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            this.PUCusto = pu;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU) para o caso de posição Vendida e operação de Venda
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>
        public void AtualizaPosicaoVendaVendido(int idPosicao, int quantidade, decimal pu) {
            this.LoadByPrimaryKey(idPosicao);
            //
            int moduloQuantidadePosicao = Math.Abs(this.Quantidade.Value);
            decimal totalFinanceiroAntes = moduloQuantidadePosicao * this.PUCusto.Value;
            decimal totalFinanceiroCompra = quantidade * pu;

            // nova Quantidade - Soma em módulo e multiplica por -1
            this.Quantidade = -1 * (moduloQuantidadePosicao + quantidade);
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroCompra) / Math.Abs(this.Quantidade.Value);
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU) para o caso de posição Comprada e operação de Venda.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>        
        public void AtualizaPosicaoVendaComprado(int idPosicao, int quantidade, decimal pu) {
            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade - quantidade;
            this.PUCusto = pu;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a quantidade de uma posição específica.
        /// </summary>
        /// <param name="idPosicao">PK da posição.</param>
        /// <param name="quantidade">quantidade a ser alterada.</param>
        public void AtualizaQuantidade(int idPosicao, int quantidade) {

            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Função para Inserção na GerPosicaoBMF a partir do objeto GerOperacaoBMF.
        /// </summary>
        /// <param name="operacaoBMF"></param>
        public void InserePosicaoBMF(GerOperacaoBMF operacaoBMF) {
            GerPosicaoBMF posicaoBMF = new GerPosicaoBMF();

            posicaoBMF.IdCliente = operacaoBMF.IdCliente;
            posicaoBMF.IdTrader = operacaoBMF.IdTrader;
            posicaoBMF.CdAtivoBMF = operacaoBMF.CdAtivoBMF;
            posicaoBMF.Serie = operacaoBMF.Serie;
            posicaoBMF.TipoMercado = operacaoBMF.TipoMercado;

            if (operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento.HasValue) {
                posicaoBMF.DataVencimento = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento;
            }
            posicaoBMF.PUCusto = operacaoBMF.Pu;
            posicaoBMF.PUMercado = 0;
            posicaoBMF.ValorMercado = 0;
            posicaoBMF.ResultadoRealizar = 0;
            posicaoBMF.Quantidade = operacaoBMF.Quantidade;

            posicaoBMF.Save();
        }

        /// <summary>
        /// Exclui da PosicaoBMF todas as posições com quantidade = 0.
        /// </summary>
        /// <param name="listaIdPosicao">ids das posições a serem excluidas</param>
        private void ExcluiPosicaoBMFZerada(List<int> listaIdPosicao) {
            // Deleta as posições zeradas sem carregá-las para a memoria                        
            GerPosicaoBMFCollection posicaoBMFCollection = new GerPosicaoBMFCollection();

            for (int i = 0; i < listaIdPosicao.Count; i++) {
                GerPosicaoBMF posicaoBMF = posicaoBMFCollection.AddNew();
                posicaoBMF.IdPosicao = listaIdPosicao[i];
                posicaoBMF.AcceptChanges();
            }
            posicaoBMFCollection.MarkAllAsDeleted();
            posicaoBMFCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {
            GerPosicaoBMFCollection gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
            gerPosicaoBMFCollection.BuscaPosicaoBMFCompleta(idCliente);
            //
            GerPosicaoBMFAberturaCollection gerPosicaoBMFAberturaCollection = new GerPosicaoBMFAberturaCollection();
            //
            #region Copia de GerPosicaoBMF para GerPosicaoBMFAbertura
            for (int i = 0; i < gerPosicaoBMFCollection.Count; i++)
            {
                GerPosicaoBMF gerPosicaoBMF = gerPosicaoBMFCollection[i];

                // Copia de GerPosicaoBMF para GerPosicaoBMFAbertura
                GerPosicaoBMFAbertura gerPosicaoBMFAbertura = gerPosicaoBMFAberturaCollection.AddNew();
                //
                gerPosicaoBMFAbertura.IdPosicao = gerPosicaoBMF.IdPosicao;
                gerPosicaoBMFAbertura.IdTrader = gerPosicaoBMF.IdTrader;
                gerPosicaoBMFAbertura.IdCliente = gerPosicaoBMF.IdCliente;
                gerPosicaoBMFAbertura.CdAtivoBMF = gerPosicaoBMF.CdAtivoBMF;
                gerPosicaoBMFAbertura.Serie = gerPosicaoBMF.Serie;
                gerPosicaoBMFAbertura.TipoMercado = gerPosicaoBMF.TipoMercado;
                gerPosicaoBMFAbertura.DataVencimento = gerPosicaoBMF.DataVencimento;
                gerPosicaoBMFAbertura.DataHistorico = data;
                gerPosicaoBMFAbertura.PUMercado = gerPosicaoBMF.PUMercado;
                gerPosicaoBMFAbertura.PUCusto = gerPosicaoBMF.PUCusto;
                gerPosicaoBMFAbertura.ValorMercado = gerPosicaoBMF.ValorMercado;
                gerPosicaoBMFAbertura.Quantidade = gerPosicaoBMF.Quantidade;
                gerPosicaoBMFAbertura.ResultadoRealizar = gerPosicaoBMF.ResultadoRealizar;                
            }
            #endregion

            gerPosicaoBMFAberturaCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            #region Delete PosicaoBMFHistorico
            GerPosicaoBMFHistoricoCollection gerPosicaoBMFDeletarHistoricoCollection = new GerPosicaoBMFHistoricoCollection();
            try
            {
                gerPosicaoBMFDeletarHistoricoCollection.DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBMFHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            GerPosicaoBMFCollection gerPosicaoBMFCollection = new GerPosicaoBMFCollection();
            gerPosicaoBMFCollection.BuscaPosicaoBMFCompleta(idCliente);
            //
            GerPosicaoBMFHistoricoCollection gerPosicaoBMFHistoricoCollection = new GerPosicaoBMFHistoricoCollection();
            //
            #region Copia de GerPosicaoBMF para GerPosicaoBMFHistorico
            for (int i = 0; i < gerPosicaoBMFCollection.Count; i++)
            {
                GerPosicaoBMF gerPosicaoBMF = gerPosicaoBMFCollection[i];

                // Copia de GerPosicaoBMF para GerPosicaoBMFHistorico
                GerPosicaoBMFHistorico gerPosicaoBMFHistorico = gerPosicaoBMFHistoricoCollection.AddNew();
                //
                gerPosicaoBMFHistorico.IdPosicao = gerPosicaoBMF.IdPosicao;
                gerPosicaoBMFHistorico.IdTrader = gerPosicaoBMF.IdTrader;
                gerPosicaoBMFHistorico.IdCliente = gerPosicaoBMF.IdCliente;
                gerPosicaoBMFHistorico.CdAtivoBMF = gerPosicaoBMF.CdAtivoBMF;
                gerPosicaoBMFHistorico.Serie = gerPosicaoBMF.Serie;
                gerPosicaoBMFHistorico.TipoMercado = gerPosicaoBMF.TipoMercado;
                gerPosicaoBMFHistorico.DataVencimento = gerPosicaoBMF.DataVencimento;
                gerPosicaoBMFHistorico.DataHistorico = data;
                gerPosicaoBMFHistorico.PUMercado = gerPosicaoBMF.PUMercado;
                gerPosicaoBMFHistorico.PUCusto = gerPosicaoBMF.PUCusto;
                gerPosicaoBMFHistorico.ValorMercado = gerPosicaoBMF.ValorMercado;
                gerPosicaoBMFHistorico.Quantidade = gerPosicaoBMF.Quantidade;
                gerPosicaoBMFHistorico.ResultadoRealizar = gerPosicaoBMF.ResultadoRealizar;                
            }
            #endregion

            gerPosicaoBMFHistoricoCollection.Save();
        }
    }
}