﻿using System;
using System.Data;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa;
using Financial.Gerencial.Enums;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;

namespace Financial.Gerencial 
{
    public class GerLiquidacaoTermoBolsa
    {
        /// <summary>
        /// Processa todas as liquidações (LPD, LPDE, LA, Decurso automático) do dia,
        /// atualizando a posição física do termo e da ação objeto (por depósito/retirada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaLiquidacaoTermo(int idCliente, DateTime data)
        {
            GerOperacaoBolsaCollection gerOperacaoBolsaCollection = new GerOperacaoBolsaCollection();

            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollection.BuscaLiquidacaoTermoBolsa(idCliente, data);

            for (int i = 0; i < liquidacaoTermoBolsaCollection.Count; i++)
            {
                LiquidacaoTermoBolsa liquidacaoTermoBolsa = liquidacaoTermoBolsaCollection[i];

                #region Carrega variáveis de LiquidacaoTermoBolsa
                byte tipoLiquidacao = liquidacaoTermoBolsa.TipoLiquidacao.Value;
                string cdAtivoBolsa = liquidacaoTermoBolsa.CdAtivoBolsa;
                DateTime dataLiquidacao = liquidacaoTermoBolsa.DataLiquidacao.Value;
                decimal quantidadeLiquidada = liquidacaoTermoBolsa.Quantidade.Value;
                decimal pu = liquidacaoTermoBolsa.Pu.Value;
                decimal valor = liquidacaoTermoBolsa.Valor.Value;
                string numeroContrato = liquidacaoTermoBolsa.NumeroContrato;
                int idPosicao = liquidacaoTermoBolsa.IdPosicao.Value;
                #endregion

                #region Verifica os tipos de depósito/retirada a serem feitos para a ação
                string tipoOperacaoAcao;
                if (quantidadeLiquidada > 0)
                {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Deposito;
                }
                else
                {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Retirada;
                }
                #endregion

                decimal quantidadeAbsoluta = Math.Abs(quantidadeLiquidada);

                #region Inserção na GerOperacaoBolsa para entrada/saída do a vista
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(posicaoTermoBolsa.Query.IdTrader);
                if (posicaoTermoBolsa.LoadByPrimaryKey(campos, idPosicao))
                {
                    if (posicaoTermoBolsa.IdTrader.HasValue)
                    {
                        int idTrader = posicaoTermoBolsa.IdTrader.Value;

                        //Retorna a ação objeto do termo
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        string cdAtivoBolsaAcao = ativoBolsa.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                        //
                        GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                        gerOperacaoBolsa.AddNew();
                        gerOperacaoBolsa.IdCliente = idCliente;
                        gerOperacaoBolsa.CdAtivoBolsa = cdAtivoBolsaAcao;
                        gerOperacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        gerOperacaoBolsa.TipoOperacao = tipoOperacaoAcao;
                        gerOperacaoBolsa.Data = data;
                        gerOperacaoBolsa.Pu = pu;
                        gerOperacaoBolsa.PULiquido = pu;
                        gerOperacaoBolsa.Valor = valor;
                        gerOperacaoBolsa.ValorLiquido = valor;
                        gerOperacaoBolsa.Quantidade = quantidadeAbsoluta;
                        gerOperacaoBolsa.Origem = OrigemOperacaoBolsa.LiquidacaoTermo;
                        gerOperacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                        gerOperacaoBolsa.IdTrader = idTrader;

                        gerOperacaoBolsaCollection.AttachEntity(gerOperacaoBolsa);
                    }
                }
                #endregion
            }

            //Save da collection
            gerOperacaoBolsaCollection.Save();            
        }
    }
}
