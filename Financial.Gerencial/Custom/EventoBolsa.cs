﻿using System;
using System.Data;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa;
using Financial.Gerencial.Enums;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;

namespace Financial.Gerencial {
    public class EventoBolsa {
        /// <summary>
        /// Processa atualizações de GerGerPosicaoBolsa a partir de conversões efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaConversao(int idCliente, DateTime data) {
            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection.BuscaConversaoBolsa(data);
            
            // Guarda a Situação Anterior da Posicao ou Null Se não Existir Posicao
            GerPosicaoBolsaCollection situacaoProvisoriaPosicoes = null;

            #region Para Cada Conversão
            for (int i = 0; i < conversaoBolsaCollection.Count; i++) {
                #region Dados de Conversão
                ConversaoBolsa conversaoBolsa = conversaoBolsaCollection[i];
                string cdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
                string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
                decimal fatorQuantidade = conversaoBolsa.FatorQuantidade.Value;
                decimal fatorPu = conversaoBolsa.FatorPU.Value;
                #endregion

                 // Para Segundo elemento em Diante
                if (i >= 1) {
                    #region Segundo Elemento em Diante
                    string cdAtivoConversaoAtual = conversaoBolsaCollection[i].CdAtivoBolsa;
                    string cdAtivoConversaoAnterior = conversaoBolsaCollection[i-1].CdAtivoBolsa;

                    // Identificado Conversão em Multiplos Passos
                    if (cdAtivoConversaoAtual == cdAtivoConversaoAnterior && situacaoProvisoriaPosicoes != null) {
                        // Retorna a Posicao Provisoria Anterior para Afetar em cima dela
                        #region Conversao Multipla
                        GerPosicaoBolsaCollection gerPosicaoBolsaCollection = (GerPosicaoBolsaCollection)Utilitario.Clone(situacaoProvisoriaPosicoes);
                        
                        #region Para cada Posicao
                        for (int j = 0; j < gerPosicaoBolsaCollection.Count; j++) {
                            GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[j];

                            #region Dados de GerPosicaoBolsa
                            decimal quantidade = gerPosicaoBolsa.Quantidade.Value;
                            decimal puCusto = gerPosicaoBolsa.PUCusto.Value;
                            int idTrader = gerPosicaoBolsa.IdTrader.Value;
                            decimal puMercado = gerPosicaoBolsa.PUMercado.Value;
                            decimal valorMercado = gerPosicaoBolsa.ValorMercado.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puCustoConvertido = puCusto * fatorPu;
                            #endregion

                            decimal quantidadeFinal = quantidadeConvertida;
                            decimal puCustoFinal = puCustoConvertido;
                            decimal puMercadoFinal = puMercado;
                            decimal valorMercadoFinal = valorMercado;

                            #region Trata caso específico para quando já tem o ativo destino na carteira
                            if (cdAtivoBolsa != cdAtivoBolsaDestino) { //Meio esquisito este teste, mas só para garantir!
                                GerPosicaoBolsa gerPosicaoBolsaExistente = new GerPosicaoBolsa();
                                if (gerPosicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivoBolsaDestino)) {
                                    decimal quantidadeExistente = gerPosicaoBolsaExistente.Quantidade.Value;
                                    decimal puCustoExistente = gerPosicaoBolsaExistente.PUCusto.Value;
                                    puMercadoFinal = gerPosicaoBolsaExistente.PUMercado.Value;
                                    valorMercadoFinal += gerPosicaoBolsaExistente.ValorMercado.Value;

                                    gerPosicaoBolsaExistente.MarkAsDeleted();
                                    gerPosicaoBolsaExistente.Save();

                                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) {
                                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                    }

                                    quantidadeFinal = quantidadeConvertida + quantidadeExistente;

                                    decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                    decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;

                                    puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                }
                            }
                            #endregion

                            #region Atualiza em GerPosicaoBolsa
                            gerPosicaoBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                            //
                            gerPosicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            gerPosicaoBolsa.Quantidade = quantidadeFinal;
                            gerPosicaoBolsa.PUCusto = puCustoFinal;
                            gerPosicaoBolsa.PUMercado = puMercadoFinal;
                            gerPosicaoBolsa.ValorMercado = valorMercadoFinal;
                            #endregion
                        }
                        #endregion

                        if (gerPosicaoBolsaCollection.Count >= 1) {
                            gerPosicaoBolsaCollection.Save();
                        }
                        #endregion                        
                    }
                    else {
                        #region Conversao Normal
                        GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                        gerPosicaoBolsaCollection.Query.Where(gerPosicaoBolsaCollection.Query.IdCliente == idCliente,
                                                              gerPosicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                        //
                        if (gerPosicaoBolsaCollection.Query.Load()) {
                            situacaoProvisoriaPosicoes = (GerPosicaoBolsaCollection)Utilitario.Clone(gerPosicaoBolsaCollection);
                        }
                        else {
                            situacaoProvisoriaPosicoes = null;
                        }

                        #region Para cada Posicao
                        for (int j = 0; j < gerPosicaoBolsaCollection.Count; j++) {
                            GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[j];

                            #region Dados de GerPosicaoBolsa
                            decimal quantidade = gerPosicaoBolsa.Quantidade.Value;
                            decimal puCusto = gerPosicaoBolsa.PUCusto.Value;
                            int idTrader = gerPosicaoBolsa.IdTrader.Value;
                            decimal puMercado = gerPosicaoBolsa.PUMercado.Value;
                            decimal valorMercado = gerPosicaoBolsa.ValorMercado.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puCustoConvertido = puCusto * fatorPu;
                            #endregion

                            decimal quantidadeFinal = quantidadeConvertida;
                            decimal puCustoFinal = puCustoConvertido;
                            decimal puMercadoFinal = puMercado;
                            decimal valorMercadoFinal = valorMercado;

                            #region Trata caso específico para quando já tem o ativo destino na carteira
                            if (cdAtivoBolsa != cdAtivoBolsaDestino) { //Meio esquisito este teste, mas só para garantir!
                                GerPosicaoBolsa gerPosicaoBolsaExistente = new GerPosicaoBolsa();
                                if (gerPosicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivoBolsaDestino)) {
                                    decimal quantidadeExistente = gerPosicaoBolsaExistente.Quantidade.Value;
                                    decimal puCustoExistente = gerPosicaoBolsaExistente.PUCusto.Value;
                                    puMercadoFinal = gerPosicaoBolsaExistente.PUMercado.Value;
                                    valorMercadoFinal += gerPosicaoBolsaExistente.ValorMercado.Value;

                                    gerPosicaoBolsaExistente.MarkAsDeleted();
                                    gerPosicaoBolsaExistente.Save();

                                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) {
                                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                    }

                                    quantidadeFinal = quantidadeConvertida + quantidadeExistente;

                                    decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                    decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;

                                    puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                }
                            }
                            #endregion

                            #region Atualiza em GerPosicaoBolsa
                            gerPosicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            gerPosicaoBolsa.Quantidade = quantidadeFinal;
                            gerPosicaoBolsa.PUCusto = puCustoFinal;
                            gerPosicaoBolsa.PUMercado = puMercadoFinal;
                            gerPosicaoBolsa.ValorMercado = valorMercadoFinal;
                            #endregion
                        }
                        #endregion

                        if (gerPosicaoBolsaCollection.Count >= 1) {
                            gerPosicaoBolsaCollection.Save();
                        }
                        #endregion                        
                    }
                    #endregion
                }

                // Primeira vez
                if (i == 0) {
                    #region Entra na Primeira vez

                    GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                    gerPosicaoBolsaCollection.Query.Where(gerPosicaoBolsaCollection.Query.IdCliente == idCliente,
                                                          gerPosicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                    //
                    if (gerPosicaoBolsaCollection.Query.Load()) {
                        situacaoProvisoriaPosicoes = (GerPosicaoBolsaCollection)Utilitario.Clone(gerPosicaoBolsaCollection);
                    }
                    else {
                        situacaoProvisoriaPosicoes = null;
                    }

                    #region Para cada Posicao
                    for (int j = 0; j < gerPosicaoBolsaCollection.Count; j++) {
                        GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[j];

                        #region Dados de GerPosicaoBolsa
                        decimal quantidade = gerPosicaoBolsa.Quantidade.Value;
                        decimal puCusto = gerPosicaoBolsa.PUCusto.Value;
                        int idTrader = gerPosicaoBolsa.IdTrader.Value;
                        decimal puMercado = gerPosicaoBolsa.PUMercado.Value;
                        decimal valorMercado = gerPosicaoBolsa.ValorMercado.Value;
                        #endregion

                        #region Converte quantidade e PU
                        decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        decimal puCustoConvertido = puCusto * fatorPu;
                        #endregion

                        decimal quantidadeFinal = quantidadeConvertida;
                        decimal puCustoFinal = puCustoConvertido;
                        decimal puMercadoFinal = puMercado;
                        decimal valorMercadoFinal = valorMercado;

                        #region Trata caso específico para quando já tem o ativo destino na carteira
                        if (cdAtivoBolsa != cdAtivoBolsaDestino) { //Meio esquisito este teste, mas só para garantir!
                            GerPosicaoBolsa gerPosicaoBolsaExistente = new GerPosicaoBolsa();
                            if (gerPosicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivoBolsaDestino)) {
                                decimal quantidadeExistente = gerPosicaoBolsaExistente.Quantidade.Value;
                                decimal puCustoExistente = gerPosicaoBolsaExistente.PUCusto.Value;
                                puMercadoFinal = gerPosicaoBolsaExistente.PUMercado.Value;
                                valorMercadoFinal += gerPosicaoBolsaExistente.ValorMercado.Value;

                                gerPosicaoBolsaExistente.MarkAsDeleted();
                                gerPosicaoBolsaExistente.Save();

                                FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) {
                                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                }

                                quantidadeFinal = quantidadeConvertida + quantidadeExistente;

                                decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;

                                puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                            }
                        }
                        #endregion

                        #region Atualiza em GerPosicaoBolsa
                        gerPosicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        gerPosicaoBolsa.Quantidade = quantidadeFinal;
                        gerPosicaoBolsa.PUCusto = puCustoFinal;
                        gerPosicaoBolsa.PUMercado = puMercadoFinal;
                        gerPosicaoBolsa.ValorMercado = valorMercadoFinal;
                        #endregion
                    }
                    #endregion

                    if (gerPosicaoBolsaCollection.Count >= 1) {
                        gerPosicaoBolsaCollection.Save();
                    }
                    #endregion
                }
            }
            #endregion
        }

        /// <summary>
        /// Processa atualizações de GerPosicaoBolsa, a partir de grupamentos efetivados no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGrupamento(int idCliente, DateTime data) {
            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection.BuscaGrupamentoBolsa(data);
            for (int i = 0; i < grupamentoBolsaCollection.Count; i++) {
                #region Dados de Grupamento
                GrupamentoBolsa grupamentoBolsa = grupamentoBolsaCollection[i];
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                decimal fatorQuantidade = grupamentoBolsa.FatorQuantidade.Value;
                decimal fatorPu = grupamentoBolsa.FatorPU.Value;
                byte tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;
                #endregion

                GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                gerPosicaoBolsaCollection.Query.Select(gerPosicaoBolsaCollection.Query.IdPosicao,
                                                    gerPosicaoBolsaCollection.Query.Quantidade,
                                                    gerPosicaoBolsaCollection.Query.PUCusto);
                gerPosicaoBolsaCollection.Query.Where(gerPosicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   gerPosicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                gerPosicaoBolsaCollection.Query.Load();

                for (int j = 0; j < gerPosicaoBolsaCollection.Count; j++) {
                    GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[j];

                    #region Dados de GerPosicaoBolsa
                    decimal quantidade = gerPosicaoBolsa.Quantidade.Value;
                    decimal puCusto = gerPosicaoBolsa.PUCusto.Value;
                    #endregion

                    #region Calcula nova quantidade e PU
                    decimal novaQuantidade = 0;
                    decimal novoPUCusto = 0;
                    if (tipoGrupamento == (int)TipoGrupamentoBolsa.Grupamento) {
                        novaQuantidade = Utilitario.Truncate(quantidade / fatorQuantidade, 0);
                        novoPUCusto = puCusto / fatorPu;
                    }
                    else {
                        novaQuantidade = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        novoPUCusto = puCusto * fatorPu;
                    }
                    #endregion

                    #region Atualiza em GerPosicaoBolsa
                    gerPosicaoBolsa.Quantidade = novaQuantidade;
                    gerPosicaoBolsa.PUCusto = novoPUCusto;
                    #endregion
                }
                gerPosicaoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Processa atualizações de GerPosicaoBolsa, a partir de bonificações efetivadas no dia.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaBonificacao(int idCliente, DateTime data) {
            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection.BuscaBonificacaoBolsa(data);

            for (int i = 0; i < bonificacaoBolsaCollection.Count; i++) {
                #region Dados de Bonificacao
                BonificacaoBolsa bonificacaoBolsa = bonificacaoBolsaCollection[i];
                string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;
                string cdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
                decimal percentual = bonificacaoBolsa.Percentual.Value / 100;
                decimal puBonificacao = bonificacaoBolsa.PUBonificacao.Value;
                #endregion

                GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
                gerPosicaoBolsaCollection.Query.Select(gerPosicaoBolsaCollection.Query.IdPosicao,
                                                    gerPosicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    gerPosicaoBolsaCollection.Query.Quantidade,
                                                    gerPosicaoBolsaCollection.Query.PUCusto,
                                                    gerPosicaoBolsaCollection.Query.IdTrader,
                                                    gerPosicaoBolsaCollection.Query.TipoMercado,
                                                    gerPosicaoBolsaCollection.Query.DataVencimento);
                gerPosicaoBolsaCollection.Query.Where(gerPosicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                      gerPosicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                      gerPosicaoBolsaCollection.Query.Quantidade.GreaterThan(0));
                gerPosicaoBolsaCollection.Query.Load();

                for (int j = 0; j < gerPosicaoBolsaCollection.Count; j++) {
                    #region Dados de GerPosicaoBolsa
                    GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[j];
                    decimal quantidade = gerPosicaoBolsa.Quantidade.Value;
                    decimal puCusto = gerPosicaoBolsa.PUCusto.Value;
                    int idTrader = gerPosicaoBolsa.IdTrader.Value;
                    string tipoMercado = gerPosicaoBolsa.TipoMercado;
                    DateTime? dataVencimento = gerPosicaoBolsa.DataVencimento;
                    #endregion

                    decimal quantidadeBonificada = Utilitario.Truncate(quantidade * percentual, 0);

                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsa + " " + data.ToString("d"));
                    }

                    decimal custoBonificacao = quantidadeBonificada * puBonificacao / fator.Fator.Value;

                    decimal quantidadeTotal = quantidadeBonificada + quantidade;

                    decimal custoOriginal = quantidade * puCusto / fator.Fator.Value;

                    decimal puCustoBonificado = ((custoOriginal + custoBonificacao) / quantidadeTotal) / fator.Fator.Value;

                    decimal quantidadeFinal = quantidadeTotal;
                    decimal puCustoFinal = puCustoBonificado;

                    if (cdAtivoBolsa != cdAtivoBolsaDestino) {
                        #region Trata quando o ativo origem é diferente do ativo destino
                        GerPosicaoBolsa gerPosicaoBolsaExistente = new GerPosicaoBolsa();
                        if (gerPosicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idTrader, cdAtivoBolsaDestino)) {
                            #region Se o ativo já existe, faz custo médio com a quantidade bonificada
                            decimal quantidadeExistente = gerPosicaoBolsaExistente.Quantidade.Value;
                            decimal puCustoExistente = gerPosicaoBolsaExistente.PUCusto.Value;

                            fator = new FatorCotacaoBolsa();
                            if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) {
                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                            }

                            quantidadeFinal = quantidadeBonificada + quantidadeExistente;

                            decimal custoExistente = quantidadeExistente * puCustoExistente / fator.Fator.Value;

                            puCustoFinal = ((custoExistente + custoBonificacao) / quantidadeFinal) / fator.Fator.Value;

                            gerPosicaoBolsaExistente.Quantidade = quantidadeFinal;
                            gerPosicaoBolsaExistente.PUCusto = puCustoFinal;
                            gerPosicaoBolsaExistente.Save();
                            #endregion
                        }
                        else {
                            #region Cria posição nova com o ativo destino
                            GerPosicaoBolsa gerPosicaoBolsaInsert = new GerPosicaoBolsa();
                            gerPosicaoBolsaInsert.IdCliente = idCliente;
                            gerPosicaoBolsaInsert.IdTrader = idTrader;
                            gerPosicaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsaDestino;
                            gerPosicaoBolsaInsert.Quantidade = quantidadeBonificada;
                            gerPosicaoBolsaInsert.PUCusto = puBonificacao;
                            gerPosicaoBolsaInsert.TipoMercado = tipoMercado;
                            gerPosicaoBolsaInsert.DataVencimento = dataVencimento;
                            gerPosicaoBolsaInsert.Save();
                            #endregion
                        }
                        #endregion
                    }
                    else {
                        #region Atualiza gerPosicaoBolsa
                        gerPosicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        gerPosicaoBolsa.Quantidade = quantidadeFinal;
                        gerPosicaoBolsa.PUCusto = puCustoBonificado;
                        #endregion
                    }
                }
                gerPosicaoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Deleta em OperacaoBolsa depositos e retiradas com Origem = DireitoSubscricao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void DeletaEventosDireitoSubscricao(int idCliente, DateTime data)
        {
            List<int> listaSubscricao = new List<int>();
            listaSubscricao.Add((int)OrigemOperacaoBolsa.DireitoSubscricao);

            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            operacaoBolsaCollection.DeletaOperacaoBolsa(idCliente, listaSubscricao, data);
        }

        /// <summary>
        /// Processa atualizações de GerPosicaoBolsa a partir de lançamentos de direito de subscrição efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ProcessaNovoDireitoSubscricao(int idCliente, DateTime data)
        {
            SubscricaoBolsaCollection subscricaoBolsaCollection = new SubscricaoBolsaCollection();
            subscricaoBolsaCollection.BuscaSubscricaoBolsa(data);

            for (int i = 0; i < subscricaoBolsaCollection.Count; i++)
            {
                #region Dados de Subscrição
                SubscricaoBolsa subscricaoBolsa = subscricaoBolsaCollection[i];
                string cdAtivoBolsaDireito = subscricaoBolsa.CdAtivoBolsaDireito;
                string cdAtivoBolsa = subscricaoBolsa.CdAtivoBolsa;
                decimal fator = subscricaoBolsa.Fator.Value;
                #endregion

                GerPosicaoBolsaCollection posicaoBolsaCollection = new GerPosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao,
                                                    posicaoBolsaCollection.Query.Quantidade,
                                                    posicaoBolsaCollection.Query.IdTrader);
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaCollection.Query.Load();

                for (int j = 0; j < posicaoBolsaCollection.Count; j++)
                {
                    GerPosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];

                    decimal quantidade = posicaoBolsa.Quantidade.Value;
                    int idTrader = posicaoBolsa.IdTrader.Value;

                    decimal quantidadeSubscrever = Utilitario.Truncate(quantidade * fator / 100, 0);

                    if (quantidadeSubscrever != 0)
                    {
                        //Cria um novo depósito para o direito de subscrição
                        GerOperacaoBolsa operacaoBolsa = new GerOperacaoBolsa();
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDireito;
                        operacaoBolsa.IdTrader = idTrader;
                        operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                        operacaoBolsa.Pu = 0;
                        operacaoBolsa.PULiquido = 0;
                        operacaoBolsa.Valor = 0;
                        operacaoBolsa.Quantidade = quantidadeSubscrever;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.DireitoSubscricao;
                        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                        
                        operacaoBolsa.Save();
                    }
                }
            }
        }

        /// <summary>
        /// Verifica todos os direitos vencendo (data fim de negociacao) e baixa-os da PosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ProcessaBaixaDireitoSubscricao(int idCliente, DateTime data)
        {
            GerPosicaoBolsaQuery posicaoBolsaQuery = new GerPosicaoBolsaQuery("P");
            SubscricaoBolsaQuery subscricaoBolsaQuery = new SubscricaoBolsaQuery("S");
            posicaoBolsaQuery.Select(posicaoBolsaQuery.CdAtivoBolsa,
                                     posicaoBolsaQuery.Quantidade,
                                     posicaoBolsaQuery.IdTrader);
            posicaoBolsaQuery.InnerJoin(subscricaoBolsaQuery).On(subscricaoBolsaQuery.CdAtivoBolsaDireito == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(subscricaoBolsaQuery.DataFimNegociacao.Equal(data));
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente));
            posicaoBolsaQuery.GroupBy(posicaoBolsaQuery.CdAtivoBolsa,
                                      posicaoBolsaQuery.Quantidade,
                                      posicaoBolsaQuery.IdTrader);

            GerPosicaoBolsaCollection posicaoBolsaCollection = new GerPosicaoBolsaCollection();

            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (GerPosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoBolsa.Quantidade.Value;
                int idTrader = posicaoBolsa.IdTrader.Value;

                string tipoOperacao = "";
                if (quantidade > 0)
                {
                    tipoOperacao = TipoOperacaoBolsa.Retirada;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBolsa.Deposito;
                }

                GerOperacaoBolsa operacaoBolsa = new GerOperacaoBolsa();
                operacaoBolsa.IdCliente = idCliente;
                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                operacaoBolsa.IdTrader = idTrader;
                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                operacaoBolsa.TipoOperacao = tipoOperacao;
                operacaoBolsa.Pu = 0;
                operacaoBolsa.PULiquido = 0;
                operacaoBolsa.Valor = 0;
                operacaoBolsa.Quantidade = Math.Abs(quantidade);
                operacaoBolsa.Data = data;
                operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.DireitoSubscricao;
                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                
                operacaoBolsa.Save();
            }
        }

        /// <summary>
        /// Processa todos os eventos relativos a direito de subscrição.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaDireitoSubscricao(int idCliente, DateTime data)
        {
            this.DeletaEventosDireitoSubscricao(idCliente, data);
            this.ProcessaNovoDireitoSubscricao(idCliente, data);
            this.ProcessaBaixaDireitoSubscricao(idCliente, data);
        }
    }
}