﻿using System;
using System.Text;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

using Financial.Util;
using Financial.BMF;
using Financial.Common;

using Financial.BMF.Enums;
using Financial.Common.Enums;

using Financial.Common.Exceptions;

namespace Financial.Gerencial {
    public partial class GerOperacaoBMF : esGerOperacaoBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerOperacaoBMF));

        /// <summary>
        /// Calcula os Ajustes sobre Operações de Futuros e Opções com Ajustes.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <exception cref="CotacaoIndiceNaoCadastradoException">
        /// Se não existir Cotação para o Indice IPCA na data passada
        /// </exception>
        public void CalculaAjusteOperacao(int idCliente, DateTime data) 
        {
            const string DDI = "DDI";
            const string DAP = "DAP";

            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();
            //
            operacaoBMFCollection.BuscaOperacaoBMFCalculaAjuste(idCliente, data);
            //
            for (int i = 0; i < operacaoBMFCollection.Count; i++) {
                #region Valores OperacaoBMF
                GerOperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;                
                decimal quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                decimal pu = operacaoBMF.Pu.Value;
                #endregion

                #region Busco a Ptax/IPCA e o PU de fechamento do dia do ativoBMF
                CotacaoBMF cotacaoBMF = new CotacaoBMF();

                decimal valorIndice;
                if (cdAtivoBMF == DDI) {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valorIndice = cotacaoBMF.BuscaPtaxAtivo(CdAtivoBMF, dataAnterior);
                }
                else if (cdAtivoBMF == DAP) {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    if (!cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.IPCA)) {
                        throw new CotacaoIndiceNaoCadastradoException("Cotação IPCA não Cadastrada na data " + data);
                    }
                    valorIndice = cotacaoIndice.Valor.Value;
                }
                else {
                    valorIndice = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, data);
                }

                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                decimal puFechamento = 0;
                if (cotacaoBMF.PUGerencial.HasValue)
                {
                    puFechamento = cotacaoBMF.PUGerencial.Value;
                }
                else
                {
                    puFechamento = cotacaoBMF.PUFechamento.Value;
                }
                #endregion

                decimal peso = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value;

                #region Cálculo do Ajuste Diário
                decimal ajuste = (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                         ? Utilitario.Truncate(Utilitario.Truncate(puFechamento - pu, 7) * peso * valorIndice, 2)
                         : -1 * Utilitario.Truncate(Utilitario.Truncate(puFechamento - pu, 7) * peso * valorIndice, 2);
                //               
                ajuste = Utilitario.Truncate(ajuste * quantidade, 2);
                #endregion

                operacaoBMF.Ajuste = ajuste;
            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Processa as Compras do dia, Gerando nova Custódia e Apurando o Resultado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void ProcessaCompra(int idCliente, DateTime data) {
            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();
            GerPosicaoBMF posicaoBMF = new GerPosicaoBMF();

            operacaoBMFCollection.BuscaOperacaoBMFCompra(idCliente, data);

            for (int i = 0; i < operacaoBMFCollection.Count; i++) {
                GerOperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                int idTrader = operacaoBMF.IdTrader.Value;
                string cdAtivo = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int quantidadeOperacao = operacaoBMF.Quantidade.Value;                
                decimal puLiquidoOperacao = operacaoBMF.PULiquido.Value;
                decimal ajuste = operacaoBMF.Ajuste.Value;

                // Tem Posição para o ativo - Atualiza a posição
                if (posicaoBMF.BuscaPosicaoBMF(idCliente, idTrader, cdAtivo, serie)) {
                    decimal quantidadePosicao = posicaoBMF.Quantidade.Value;
                    // Comprado
                    if (quantidadePosicao >= 0) {
                        // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                        posicaoBMF.AtualizaPosicaoCompraComprado(posicaoBMF.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                    }
                    // Vendido
                    else {
                        // Se quantidade comprada for suficiente para passar de uma posicao negativa 
                        // para positivo
                        if (quantidadeOperacao > Math.Abs(quantidadePosicao)) {
                            // Baixa posicao - Atualizar quantidade                                                        
                            if (operacaoBMF.IsTipoMercadoOpcaoDisponivel() || operacaoBMF.IsTipoMercadoOpcaoFuturo() ||
                                operacaoBMF.IsTipoMercadoVista()) {
                                operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            }
                            posicaoBMF.AtualizaPosicaoCompraVendido(posicaoBMF.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                        }
                        else {
                            // Baixa posicao - Atualizar apenas a quantidade
                            if (operacaoBMF.IsTipoMercadoOpcaoDisponivel() || operacaoBMF.IsTipoMercadoOpcaoFuturo() ||
                                operacaoBMF.IsTipoMercadoVista()) {
                                operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            }
                            posicaoBMF.AtualizaQuantidade(posicaoBMF.IdPosicao.Value, quantidadeOperacao);
                        }
                    }
                }
                // Não tem posição para o Ativo - Inclusão da nova Posição
                else {
                    posicaoBMF.InserePosicaoBMF(operacaoBMF);
                }
            }
        }

        /// <summary>
        /// Processa as Vendas do Dia, Gerando nova Custódia e Apurando o Resultado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void ProcessaVenda(int idCliente, DateTime data) {           
            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();
            GerPosicaoBMF posicaoBMF = new GerPosicaoBMF();
            //
            operacaoBMFCollection.BuscaOperacaoBMFVenda(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++) {
                GerOperacaoBMF operacaoBMF = operacaoBMFCollection[i];                
                decimal puLiquidoOperacao = operacaoBMF.PULiquido.Value;
                int quantidadeOperacao = operacaoBMF.Quantidade.Value;
                string cdAtivo = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int idTrader = operacaoBMF.IdTrader.Value;
                decimal ajuste = operacaoBMF.Ajuste.Value;

                // Tem Posição para o ativo - Atualiza a posição
                if (posicaoBMF.BuscaPosicaoBMF(idCliente, idTrader, cdAtivo, serie)) {
                    decimal quantidadePosicao = posicaoBMF.Quantidade.Value;
                    // Vendido
                    if (quantidadePosicao <= 0) {
                        // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                        posicaoBMF.AtualizaPosicaoVendaVendido(posicaoBMF.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                    }
                    // Comprado
                    else {
                        // Se quantidade comprada for suficiente para passar de uma posicao positiva 
                        // para negativa
                        int quantidadeOperacaoNegativa = -1 * quantidadeOperacao;
                        if (quantidadeOperacao > quantidadePosicao) {
                            if (operacaoBMF.IsTipoMercadoOpcaoDisponivel() || operacaoBMF.IsTipoMercadoOpcaoFuturo() ||
                                operacaoBMF.IsTipoMercadoVista()) {

                                operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            }
                            // Baixa posicao - Atualizar quantidade                                                        
                            posicaoBMF.AtualizaPosicaoVendaComprado(posicaoBMF.IdPosicao.Value, quantidadeOperacao, puLiquidoOperacao);
                        }
                        else {
                            if (operacaoBMF.IsTipoMercadoOpcaoDisponivel() || operacaoBMF.IsTipoMercadoOpcaoFuturo() ||
                                operacaoBMF.IsTipoMercadoVista()) {
                                operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            }
                            // Baixa posicao - Atualizar apenas a quantidade                            
                            posicaoBMF.AtualizaQuantidade(posicaoBMF.IdPosicao.Value, quantidadeOperacaoNegativa);
                        }
                    }                   
                }
                // Não tem posição para o Ativo - Inclusão da nova Posição
                else {
                    // Quantidade da operação é positiva, mas quantidade da  posição fica negativa
                    operacaoBMF.Quantidade = -1 * operacaoBMF.Quantidade;
                    posicaoBMF.InserePosicaoBMF(operacaoBMF);
                }
            }           
        }

        /// <summary>
        /// Apura o Resultado de operações Normais, confrontando o custo operado com o custo de estoque em posição.
        /// </summary>
        /// <param name="operacaoBMF">objeto com informações de GerOperacaoBMF</param>
        /// <param name="posicaoBMF">objeto com informações de GerPosicaoBMF</param>
        /// <exception cref="ArgumentNullException">
        /// Se operacaoBMF.CdAtivoBMF ou
        ///    operacaoBMF.IdOperacao ou
        ///    operacaoBMF.IdCliente ou
        ///    operacaoBMF.Quantidade ou
        ///    operacaoBMF.PULiquido ou
        ///    operacaoBMF.TipoOperacao ou
        ///    posicaoBMF.Quantidade
        ///    for nulo
        /// </exception>
        public void ProcessaResultadoNormal(GerOperacaoBMF operacaoBMF, GerPosicaoBMF posicaoBMF) {
           
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBMF.CdAtivoBMF) ||
                 !operacaoBMF.IdOperacao.HasValue ||
                 !operacaoBMF.IdCliente.HasValue ||
                 !operacaoBMF.Quantidade.HasValue ||
                 !operacaoBMF.PULiquido.HasValue ||
                 String.IsNullOrEmpty(operacaoBMF.TipoOperacao) ||
                 !posicaoBMF.Quantidade.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBMF.CdAtivoBMF ou ")
                .Append("operacaoBMF.IdOperacao ou ")
                .Append("operacaoBMF.IdCliente ou ")
                .Append("operacaoBMF.Quantidade ou ")
                .Append("operacaoBMF.PULiquido ou ")
                .Append("operacaoBMF.TipoOperacao ou ")
                .Append("posicaoBMF.Quantidade");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion
            
            decimal resultado = 0;
            decimal peso = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; // HIERARQUICO

            decimal quantidadeBase = (Math.Abs(posicaoBMF.Quantidade.Value) < operacaoBMF.Quantidade)
                                     ? Math.Abs(posicaoBMF.Quantidade.Value)
                                     : operacaoBMF.Quantidade.Value;

            // Verifica se está Vendido
            switch (operacaoBMF.TipoOperacao) {
                case TipoOperacaoBMF.Compra:
                    resultado = quantidadeBase * 
                        (posicaoBMF.PUCusto.Value - operacaoBMF.PULiquido.Value) * peso;
                    break;
                case TipoOperacaoBMF.Venda:
                    resultado = quantidadeBase * 
                        (operacaoBMF.PULiquido.Value - posicaoBMF.PUCusto.Value) * peso;
                    break;
            }

            GerOperacaoBMF operacaoBMFAux = new GerOperacaoBMF();
            // Atualiza Resultado Realizado da Operação                                         
            operacaoBMFAux.IdOperacao = operacaoBMF.IdOperacao;
            operacaoBMFAux.AcceptChanges();
            operacaoBMFAux.ResultadoRealizado = resultado;
            operacaoBMFAux.Save();           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompra() {
            return this.TipoOperacao == TipoOperacaoBMF.Compra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVenda() {
            return this.TipoOperacao == TipoOperacaoBMF.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra Daytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompraDaytrade() {
            return this.TipoOperacao == TipoOperacaoBMF.CompraDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda Daytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVendaDaytrade() {
            return this.TipoOperacao == TipoOperacaoBMF.VendaDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é Vista
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoVista() {
            return this.TipoMercado == TipoMercadoBMF.Disponivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é Primaria
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemPrimaria() {
            return this.Origem == OrigemOperacaoBMF.Primaria;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpcaoDisponivel
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoDisponivel() {
            return this.TipoMercado == TipoMercadoBMF.OpcaoDisponivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpcaoFuturo
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoFuturo() {
            return this.TipoMercado == TipoMercadoBMF.OpcaoFuturo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoDisponivel ou ExercicioOpcaoFuturo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcao() {
            return (this.Origem == OrigemOperacaoBMF.ExercicioOpcaoCompra ||
                    this.Origem == OrigemOperacaoBMF.ExercicioOpcaoVenda);
        }

        /// <summary>
        /// Calcula valores líquidos, descontando as despesas. Atualiza o valor líquido e o PU líquido.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaValoresLiquidos(int idCliente, DateTime data)
        {
            GerOperacaoBMFCollection operacaoBMFCollection = new GerOperacaoBMFCollection();
            operacaoBMFCollection.BuscaOperacaoBMFValores(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                GerOperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                decimal valor = operacaoBMF.Valor.Value;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                decimal pu = operacaoBMF.Pu.Value;
                decimal peso = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal puLiquido = operacaoBMF.PULiquido.Value;
                // taxas
                decimal despesas = operacaoBMF.Despesas.Value;
                decimal valorLiquido = 0;

                switch (operacaoBMF.TipoOperacao)
                {
                    case TipoOperacaoBMF.Compra:
                    case TipoOperacaoBMF.CompraDaytrade:
                        puLiquido = (valor + despesas) / (quantidade * peso);
                        valorLiquido = valor + despesas;
                        break;
                    case TipoOperacaoBMF.Venda:
                    case TipoOperacaoBMF.VendaDaytrade:
                        puLiquido = (valor - despesas) / (quantidade * peso);
                        valorLiquido = valor - despesas;
                        break;
                }

                operacaoBMF.ValorLiquido = valorLiquido;
                operacaoBMF.PULiquido = puLiquido;
            }

            // Salva a collection de operações
            operacaoBMFCollection.Save();
        }
        
        /// <summary>
        /// Transfere as ordens já casadas (em normais e DT) da OrdemBMF para GerOperacaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemCasada(int idCliente, DateTime data)
        {
            GerOperacaoBMFCollection gerOperacaoBMFCollectionDeletar = new GerOperacaoBMFCollection();
            gerOperacaoBMFCollectionDeletar.DeletaOperacaoOrdemCasada(idCliente, data);
                       
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            ordemBMFCollection.BuscaOrdemBMFCompleta(idCliente, data);
            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                OrdemBMF ordemBMF = ordemBMFCollection[i];

                int? idTrader = ordemBMF.IdTrader;

                if (idTrader.HasValue)
                {
                    #region OrdemBMF
                    string cdAtivoBMF = ordemBMF.CdAtivoBMF;
                    string serie = ordemBMF.Serie;
                    // Hierarquico
                    decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value;
                    int tipoMercado = ordemBMF.TipoMercado.Value;
                    string tipoOrdem = ordemBMF.TipoOrdem;
                    decimal pu = ordemBMF.Pu.Value;
                    int quantidade = ordemBMF.Quantidade.Value;
                    int quantidadeDayTrade = ordemBMF.QuantidadeDayTrade.Value;
                    int quantidadeNormal = quantidade - quantidadeDayTrade;
                    decimal valorNormal = Utilitario.Truncate(quantidadeNormal * pu * peso, 2);
                    decimal valorDaytrade = Utilitario.Truncate(quantidadeDayTrade * pu * peso, 2);

                    decimal corretagem = ordemBMF.Corretagem.Value;
                    decimal emolumento = ordemBMF.Emolumento.Value;
                    decimal registro = ordemBMF.Registro.Value;
                    decimal custoWtr = ordemBMF.CustoWtr.Value;

                    decimal corretagemNormal = Utilitario.Truncate(corretagem * quantidadeNormal / quantidade, 2);
                    decimal emolumentoNormal = Utilitario.Truncate(emolumento * quantidadeNormal / quantidade, 2);
                    decimal registroNormal = Utilitario.Truncate(registro * quantidadeNormal / quantidade, 2);
                    decimal custoWtrNormal = Utilitario.Truncate(custoWtr * quantidadeNormal / quantidade, 2);

                    decimal corretagemDayTrade = corretagem - corretagemNormal;
                    decimal emolumentoDayTrade = emolumento - emolumentoNormal;
                    decimal registroDayTrade = registro - registroNormal;
                    decimal custoWtrDayTrade = custoWtr - custoWtrNormal;
                    
                    int pontaEstrategia = ordemBMF.PontaEstrategia.Value;
                    byte fonteOrdem = ordemBMF.Fonte.Value;
                    #endregion
                    //
                    byte fonteOperacao = fonteOrdem;
                    if (fonteOperacao == (int)FonteOrdemBMF.Manual)
                    {
                        fonteOperacao = (int)FonteOperacaoBMF.OrdemBMF;
                    }

                    if (quantidadeDayTrade != 0)
                    {
                        /* Adiciono 'D' porque a operação é DT / C = CD, V = VD */
                        string tipoOperacao = tipoOrdem + "D";

                        #region InsereOperacao
                        GerOperacaoBMF gerOperacaoBMF = new GerOperacaoBMF();

                        gerOperacaoBMF.TipoMercado = (byte)tipoMercado;
                        gerOperacaoBMF.TipoOperacao = tipoOperacao;
                        gerOperacaoBMF.Data = data;
                        gerOperacaoBMF.Pu = pu;
                        gerOperacaoBMF.Valor = valorDaytrade;
                        gerOperacaoBMF.ValorLiquido = ordemBMF.IsTipoOrdemCompra()
                                                            ? valorDaytrade + (corretagemDayTrade + emolumentoDayTrade + registroDayTrade + custoWtrDayTrade)
                                                            : valorDaytrade - (corretagemDayTrade + emolumentoDayTrade + registroDayTrade + custoWtrDayTrade);

                        gerOperacaoBMF.PULiquido = Utilitario.Truncate((gerOperacaoBMF.ValorLiquido.Value / quantidadeDayTrade) / peso, 10);
                        /* Taxas */
                        gerOperacaoBMF.Despesas = corretagemDayTrade + emolumentoDayTrade + registroDayTrade + custoWtrDayTrade;
                        //
                        gerOperacaoBMF.Quantidade = quantidadeDayTrade;
                        gerOperacaoBMF.CdAtivoBMF = cdAtivoBMF;
                        gerOperacaoBMF.Serie = serie;
                        gerOperacaoBMF.IdTrader = idTrader;
                        gerOperacaoBMF.Origem = ordemBMF.Origem;
                        gerOperacaoBMF.Fonte = fonteOperacao;
                        gerOperacaoBMF.IdCliente = idCliente;
                        gerOperacaoBMF.Taxa = ordemBMF.Taxa.HasValue ? ordemBMF.Taxa : null;

                        gerOperacaoBMF.Save();
                        #endregion
                    }

                    if (quantidadeNormal != 0)
                    {
                        decimal despesas = corretagem + emolumento + registro + custoWtr;
                        string tipoOperacao = tipoOrdem;

                        #region InsereOperacao
                        GerOperacaoBMF gerOperacaoBMF = new GerOperacaoBMF();
                        gerOperacaoBMF.TipoMercado = (byte)tipoMercado;
                        gerOperacaoBMF.TipoOperacao = tipoOperacao;
                        gerOperacaoBMF.Data = data;
                        gerOperacaoBMF.Pu = pu;
                        gerOperacaoBMF.Valor = valorNormal;
                        gerOperacaoBMF.ValorLiquido = ordemBMF.IsTipoOrdemCompra()
                                                            ? valorNormal + (corretagemNormal + emolumentoNormal + registroNormal + custoWtrNormal)
                                                            : valorNormal - (corretagemNormal + emolumentoNormal + registroNormal + custoWtrNormal);

                        gerOperacaoBMF.PULiquido = Utilitario.Truncate((gerOperacaoBMF.ValorLiquido.Value / quantidadeNormal) / peso, 10);

                        /* Taxas */
                        gerOperacaoBMF.Despesas = corretagemNormal + emolumentoNormal + registroNormal + custoWtrNormal;
                        //
                        gerOperacaoBMF.Quantidade = quantidadeNormal;
                        gerOperacaoBMF.CdAtivoBMF = cdAtivoBMF;
                        gerOperacaoBMF.Serie = serie;
                        gerOperacaoBMF.IdTrader = idTrader;
                        gerOperacaoBMF.Origem = ordemBMF.Origem;
                        gerOperacaoBMF.Fonte = fonteOperacao;
                        gerOperacaoBMF.IdCliente = idCliente;
                        gerOperacaoBMF.Taxa = ordemBMF.Taxa.HasValue ? ordemBMF.Taxa : null;
                        gerOperacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;

                        gerOperacaoBMF.Save();
                        #endregion
                    }
                }
            }

        }
    }
}