﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Gerencial
{
	public partial class GerPosicaoBolsaHistoricoCollection : esGerPosicaoBolsaHistoricoCollection
	{
        /// <summary>
        /// Deleta posições de bolsa históricas.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {

            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoBolsaHistoricoCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();            
        }
	}
}
