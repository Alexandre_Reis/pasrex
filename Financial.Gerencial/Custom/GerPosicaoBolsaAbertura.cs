﻿using System;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

namespace Financial.Gerencial {
    public partial class GerPosicaoBolsaAbertura : esGerPosicaoBolsaAbertura {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBolsaAbertura));

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataHistorico"></param> 
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, string cdAtivoBolsa, DateTime dataHistorico) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.DataHistorico == dataHistorico);

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }
    }
}