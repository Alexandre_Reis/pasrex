﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Gerencial
{
	public partial class GerPosicaoBMFAberturaCollection : esGerPosicaoBMFAberturaCollection
	{
        /// <summary>
        /// Deleta todas as posições históricas de abertura com data > que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBMFAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto GerPosicaoBMFAberturaCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMFAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();

            return retorno;
        }

	}
}
