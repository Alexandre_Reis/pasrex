﻿using System;
using System.Data;
using System.Collections.Generic;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using log4net;

using Financial.Util;
using Financial.Bolsa;

using Financial.Bolsa.Enums;
using Financial.Common.Enums;

using Financial.Bolsa.Exceptions;
using System.Data.SqlClient;

namespace Financial.Gerencial {
    public partial class GerPosicaoBolsa : esGerPosicaoBolsa {
        private static readonly ILog log = LogManager.GetLogger(typeof(GerPosicaoBolsa));

        /// <summary>
        /// Carrega o Objeto GerPosicaoBolsa com todos os campos de GerPosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivoBolsa">Codigo do Ativo de Bolsa</param>
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoBolsa(int idCliente, int idTrader, String cdAtivoBolsa) {

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdTrader == idTrader,
                        this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa);

            return this.Query.Load();
        }

        /// <summary>
        /// Atualiza a quantidade, somando a atual à quantidade passada.
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="quantidade"></param>
        public void AtualizaQuantidade(int idPosicao, decimal quantidade) {

            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu"></param>       
        public void AtualizaPosicaoVendaComprado(int idPosicao, decimal quantidade, decimal pu) {

            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade - quantidade;
            this.PUCusto = pu;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>        
        public void AtualizaPosicaoCompraComprado(int idPosicao, decimal quantidade, decimal pu) {

            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            decimal totalFinanceiroAntes = this.Quantidade.Value * this.PUCusto.Value;
            decimal totalFinanceiroOperacao = quantidade * pu;

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroOperacao) / this.Quantidade;
            
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">pu da operação</param>
        public void AtualizaPosicaoCompraVendido(int idPosicao, decimal quantidade, decimal pu) {

            try {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e) {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            this.PUCusto = pu;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>
        public void AtualizaPosicaoVendaVendido(int idPosicao, decimal quantidade, decimal pu) {
            this.LoadByPrimaryKey(idPosicao);
            //
            decimal moduloQuantidadePosicao = Math.Abs(this.Quantidade.Value);
            decimal totalFinanceiroAntes = moduloQuantidadePosicao * this.PUCusto.Value;
            decimal totalFinanceiroOperacao = quantidade * pu;
            
            // nova Quantidade - Soma em módulo e multiplica por -1
            this.Quantidade = -1 * (moduloQuantidadePosicao + quantidade);
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroOperacao) / Math.Abs(this.Quantidade.Value);
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Função básica de Inserção na GerPosicaoBolsa.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        public void InserePosicaoBolsa(GerOperacaoBolsa operacaoBolsa) {

            GerPosicaoBolsa posicaoBolsa = new GerPosicaoBolsa();
            if (operacaoBolsa.UpToAtivoBolsaByCdAtivoBolsa.DataVencimento.HasValue) {
                posicaoBolsa.DataVencimento = operacaoBolsa.UpToAtivoBolsaByCdAtivoBolsa.DataVencimento;
            }
            posicaoBolsa.IdCliente = operacaoBolsa.IdCliente;
            posicaoBolsa.IdTrader = operacaoBolsa.IdTrader;
            posicaoBolsa.CdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
            posicaoBolsa.Quantidade = operacaoBolsa.Quantidade;
            posicaoBolsa.PUCusto = operacaoBolsa.PULiquido;
            posicaoBolsa.TipoMercado = operacaoBolsa.TipoMercado;
            posicaoBolsa.Save();
        }

        /// <summary>
        ///  Atualiza ValorMercado, ValorCustoLiquido, ResultadoRealizar de uma posicao, dado um cliente 
        ///  e uma data de cotação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <exception cref="ClienteBolsaNaoCadastradoException">Quando não existe ClienteBolsa para o IdCliente passado</exception>
        /// <exception cref="FatorCotacaoNaoCadastradoException">Quando não há FatorCotacao Cadastrado</exception>
        public void AtualizaValores(int idCliente, DateTime data) {
            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            GerPosicaoBolsaCollection posicaoBolsaCollection = new GerPosicaoBolsaCollection();
            GerPosicaoBolsa posicaoBolsa = new GerPosicaoBolsa();

            #region Busca o tipo de cotação - média ou fechamento
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            clienteBolsa.LoadByPrimaryKey(idCliente);

            if (!clienteBolsa.es.HasData) {
                throw new ClienteBolsaNaoCadastradoException("Cliente " + idCliente + " sem cadastro para Bolsa.");
            }
            int tipoCotacao = clienteBolsa.TipoCotacao.Value;
            #endregion

            // Arraylist com os Ids das posicões zeradas
            List<int> listaPosicaoZerada = new List<int>();
            posicaoBolsaCollection.BuscaPosicaoBolsa(idCliente);

            for (int i = 0; i < posicaoBolsaCollection.Count; i++) {
                posicaoBolsa = posicaoBolsaCollection[i];

                // Se posicaoBolsa.Quantidade != 0 - update das posições
                if (posicaoBolsa.Quantidade != 0) 
                {
                    #region Busca cotação do dia
                    CotacaoBolsa cotacao = new CotacaoBolsa();
                    cotacao.BuscaCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data);

                    decimal cotacaoDia;
                    if (cotacao.es.HasData && cotacao.PUGerencial.HasValue)
                    {
                        cotacaoDia = cotacao.PUGerencial.Value;
                    }
                    else
                    {
                        if (tipoCotacao == (int)TipoCotacaoBolsa.Medio)
                        {
                            if (!cotacao.es.HasData || !cotacao.PUMedio.HasValue)
                            {
                                cotacao = new CotacaoBolsa();
                                cotacao.PUMedio = 0;
                            }
                            cotacaoDia = cotacao.PUMedio.Value;
                        }
                        else
                        {
                            if (!cotacao.es.HasData || !cotacao.PUFechamento.HasValue)
                            {
                                cotacao = new CotacaoBolsa();
                                cotacao.PUFechamento = 0;
                            }
                            cotacaoDia = cotacao.PUFechamento.Value;
                        }
                    }
                    //Se a cotação vier zerada, busca a última disponível (para a situação de feriado na Bovespa)
                    if (cotacaoDia == 0) 
                    {
                        CotacaoBolsa cotacaoAnterior = new CotacaoBolsa();
                        {
                            if (cotacaoAnterior.BuscaUltimaCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data))
                            {
                                if (cotacaoAnterior.es.HasData && cotacaoAnterior.PUGerencial.HasValue)
                                {
                                    cotacaoDia = cotacaoAnterior.PUGerencial.Value;
                                }
                                else
                                {
                                    cotacaoDia = tipoCotacao == (int)TipoCotacaoBolsa.Medio
                                                ? cotacaoAnterior.PUMedio.Value
                                                : cotacaoAnterior.PUFechamento.Value;
                                }
                            }
                        }
                    }
                    #endregion

                    if (!fator.BuscaFatorCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data)) 
                    {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + posicaoBolsa.CdAtivoBolsa + " " + data.ToString("d"));
                    }

                    //
                    decimal valorMercado = Utilitario.Truncate((posicaoBolsa.Quantidade.Value * cotacaoDia) / (decimal)fator.Fator, 2);
                    //
                    decimal resultadoRealizar = Utilitario.Truncate((posicaoBolsa.Quantidade.Value *
                                                (cotacaoDia - posicaoBolsa.PUCusto.Value)) / (decimal)fator.Fator, 2);

                    posicaoBolsa.PUMercado = cotacaoDia;
                    posicaoBolsa.ValorMercado = valorMercado;                    
                    posicaoBolsa.ResultadoRealizar = resultadoRealizar;
                }
                // Se posicaoBolsa.Quantidade = 0 - delete das posições
                else {
                    listaPosicaoZerada.Add(posicaoBolsa.IdPosicao.Value);
                }
            }

            // Update das posicões
            posicaoBolsaCollection.Save();

            // Deleta as Posicões Zeradas
            this.ExcluiPosicaoBolsaZerada(listaPosicaoZerada);
        }

        /// <summary>
        /// Exclui da GerPosicaoBolsa Todas as Posições com Quantidade = 0.
        /// </summary>
        /// <param name="listaIdPosicao">ids das posições a serem excluidas</param>
        private void ExcluiPosicaoBolsaZerada(List<int> listaIdPosicao) {
            // Deleta as Posições zeradas sem Carregá-las para a Memoria                        
            GerPosicaoBolsaCollection posicaoBolsaCollection = new GerPosicaoBolsaCollection();

            for (int i = 0; i < listaIdPosicao.Count; i++) {
                GerPosicaoBolsa posicaoBolsa = posicaoBolsaCollection.AddNew();
                posicaoBolsa.IdPosicao = listaIdPosicao[i];
                posicaoBolsa.AcceptChanges();
            }
            posicaoBolsaCollection.MarkAllAsDeleted();
            posicaoBolsaCollection.Save();
        }

        /// <summary>
        /// Lança Depositos/Retiradas Para Zerar as posições Vencidas do dia da GerPosicaoBolsa.
        /// Joga o resultado (pó) do vencimento nas boletas de Deposito/Retirada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <exception cref="VencimentoOpcaoException">Se Houver Abertura de novas Posições no dia Vencimento</exception>
        public void ProcessaVencimentoOpcao(int idCliente, DateTime data) {
            GerPosicaoBolsaCollection posicaoBolsaCollection = new GerPosicaoBolsaCollection();
            GerOperacaoBolsaCollection operacaoBolsaCollection = new GerOperacaoBolsaCollection();
            GerOperacaoBolsa operacaoBolsa = new GerOperacaoBolsa();

            // Busca as Posicões de Opcões que venceram para dar baixa.
            posicaoBolsaCollection.BuscaPosicaoBolsa(idCliente, data);

            #region Deleta operações com origem = VencimentoOpcao na GerOperacaoBolsa
            if (posicaoBolsaCollection.Count > 0) {
                List<int> origemOperacao = new List<int>();
                origemOperacao.Add(OrigemOperacaoBolsa.VencimentoOpcao);

                GerOperacaoBolsaCollection operacaoBolsaCollectionDeletar = new GerOperacaoBolsaCollection();
                operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemOperacao, data);
            }
            #endregion

            for (int i = 0; i < posicaoBolsaCollection.Count; i++) {
                GerPosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                int idTrader = posicaoBolsa.IdTrader.Value;
                decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                decimal puMercado = posicaoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal quantidadeExercicio = 0;
                #region Verifica exercícios das posições (se tiver, precisa tratar p não gerar resultado de pó)
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                string cdAtivoBolsaObjeto = ativoBolsa.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                if (cdAtivoBolsaObjeto != "") {
                    operacaoBolsaCollection.BuscaOperacaoBolsaExercicio(idCliente, data, cdAtivoBolsa, cdAtivoBolsaObjeto, idTrader);

                    for (int j = 0; j < operacaoBolsaCollection.Count; j++) {
                        GerOperacaoBolsa operacaoBolsaAux = operacaoBolsaCollection[j];
                        decimal quantidadeOperacao = operacaoBolsaAux.Quantidade.Value;
                        quantidadeExercicio += quantidadeOperacao;
                    }
                }
                #endregion

                string tipoOperacao;                
                if (quantidadePosicao < 0) 
                {
                    tipoOperacao = TipoOperacaoBolsa.Deposito;
                }
                else 
                {
                    tipoOperacao = TipoOperacaoBolsa.Retirada;
                }
                decimal quantidadeVencida = Math.Abs(quantidadePosicao);

                if (quantidadeExercicio >= quantidadeVencida)
                {
                    quantidadeVencida = 0;
                }
                else
                {
                    quantidadeVencida -= quantidadeExercicio;
                }

                #region Busca fatorCotacaoBolsa
                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                decimal fator = 1;
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                    fator = fatorCotacao.Fator.Value;
                }
                else {
                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivoBolsa + " " + data.ToString("d"));
                }
                #endregion

                if (quantidadeVencida != 0) {
                    decimal valor = Math.Round((quantidadeVencida * posicaoBolsa.PUCusto.Value) / fator, 2);
                    
                    // Posições vendidas geram resultado positivo no vencimento
                    // Posições compradas geram resultado negativo no vencimento               
                    decimal resultadoRealizado = quantidadePosicao > 0 ? valor * -1 : valor;
                    
                    #region cria Nova Operacao (não impactará posição, Apenas para Guardar os Resultados do pó)
                    operacaoBolsa = new GerOperacaoBolsa();
                    operacaoBolsa.TipoMercado = posicaoBolsa.UpToAtivoBolsaByCdAtivoBolsa.TipoMercado;
                    operacaoBolsa.TipoOperacao = tipoOperacao;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = posicaoBolsa.PUCusto;
                    operacaoBolsa.Quantidade = quantidadeVencida;
                    operacaoBolsa.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                    operacaoBolsa.IdTrader = posicaoBolsa.IdTrader;                   
                    operacaoBolsa.PULiquido = posicaoBolsa.PUCusto;
                    operacaoBolsa.Valor = 0;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.VencimentoOpcao;                                        
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.ValorLiquido = 0;
                    operacaoBolsa.ResultadoRealizado = resultadoRealizado;
                    operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                    
                    operacaoBolsa.InsereOperacaoBolsa(operacaoBolsa);
                    #endregion
                }

                posicaoBolsa.Quantidade = 0;
            }

            posicaoBolsaCollection.Save();           
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            #region Delete PosicaoBolsaHistorico
            GerPosicaoBolsaHistoricoCollection gerPosicaoBolsaDeletarHistoricoCollection = new GerPosicaoBolsaHistoricoCollection();
            try
            {
                gerPosicaoBolsaDeletarHistoricoCollection.DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete GerPosicaoBolsaHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
            gerPosicaoBolsaCollection.BuscaPosicaoBolsaCompleta(idCliente);
            //
            GerPosicaoBolsaHistoricoCollection gerPosicaoBolsaHistoricoCollection = new GerPosicaoBolsaHistoricoCollection();
            //
            // Total de atributos que devem ser copiados
            GerPosicaoBolsa gerPosicaoBolsaAux = new GerPosicaoBolsa();
            int totalAtributos = gerPosicaoBolsaAux.es.Meta.Columns.Count;
            int totalAtributosCopiados = 0;

            #region Copia de gerPosicaoBolsa para gerPosicaoBolsaHistorico
            for (int i = 0; i < gerPosicaoBolsaCollection.Count; i++)
            {
                GerPosicaoBolsa gerPosicaoBolsa = gerPosicaoBolsaCollection[i];

                // Copia de gerPosicaoBolsa para gerPosicaoBolsaHistorico
                GerPosicaoBolsaHistorico gerPosicaoBolsaHistorico = gerPosicaoBolsaHistoricoCollection.AddNew();
                //
                totalAtributosCopiados = 0;
                gerPosicaoBolsaHistorico.IdPosicao = gerPosicaoBolsa.IdPosicao;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.IdTrader = gerPosicaoBolsa.IdTrader;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.IdCliente = gerPosicaoBolsa.IdCliente;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.CdAtivoBolsa = gerPosicaoBolsa.CdAtivoBolsa;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.TipoMercado = gerPosicaoBolsa.TipoMercado;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.DataVencimento = gerPosicaoBolsa.DataVencimento;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.DataHistorico = data; //DataHistorico não conta                
                gerPosicaoBolsaHistorico.PUMercado = gerPosicaoBolsa.PUMercado;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.PUCusto = gerPosicaoBolsa.PUCusto;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.ValorMercado = gerPosicaoBolsa.ValorMercado;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.Quantidade = gerPosicaoBolsa.Quantidade;
                totalAtributosCopiados++;
                gerPosicaoBolsaHistorico.ResultadoRealizar = gerPosicaoBolsa.ResultadoRealizar;
                totalAtributosCopiados++;
            }
            #endregion

            // Fail se faltou copiar algum atributo
            if (gerPosicaoBolsaCollection.Count > 0 && totalAtributos != totalAtributosCopiados)
            {
                string msg = "Faltou Copiar Atributos de GerPosicaoBolsa: Numero Atributos GerPosicaoBolsa = " + totalAtributos + ", ";
                msg += "Atributos GerPosicaoBolsaHistorico = " + totalAtributosCopiados;                
            }

            gerPosicaoBolsaHistoricoCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {
            GerPosicaoBolsaCollection gerPosicaoBolsaCollection = new GerPosicaoBolsaCollection();
            gerPosicaoBolsaCollection.BuscaPosicaoBolsaCompleta(idCliente);
            //
            GerPosicaoBolsaAberturaCollection gerPosicaoBolsaAberturaCollection = new GerPosicaoBolsaAberturaCollection();
            //
            #region Copia de gerPosicaoBolsa para gerPosicaoBolsaAbertura
            for (int i = 0; i < gerPosicaoBolsaCollection.Count; i++)
            {
                GerPosicaoBolsa gerPosicaoBolsa = (GerPosicaoBolsa)gerPosicaoBolsaCollection[i];

                // Copia de gerPosicaoBolsa para gerPosicaoBolsaAbertura
                GerPosicaoBolsaAbertura gerPosicaoBolsaAbertura = gerPosicaoBolsaAberturaCollection.AddNew();
                //
                gerPosicaoBolsaAbertura.IdPosicao = gerPosicaoBolsa.IdPosicao;
                gerPosicaoBolsaAbertura.IdTrader = gerPosicaoBolsa.IdTrader;
                gerPosicaoBolsaAbertura.IdCliente = gerPosicaoBolsa.IdCliente;
                gerPosicaoBolsaAbertura.CdAtivoBolsa = gerPosicaoBolsa.CdAtivoBolsa;
                gerPosicaoBolsaAbertura.TipoMercado = gerPosicaoBolsa.TipoMercado;
                gerPosicaoBolsaAbertura.DataVencimento = gerPosicaoBolsa.DataVencimento;
                gerPosicaoBolsaAbertura.DataHistorico = data;
                gerPosicaoBolsaAbertura.PUMercado = gerPosicaoBolsa.PUMercado;
                gerPosicaoBolsaAbertura.PUCusto = gerPosicaoBolsa.PUCusto;
                gerPosicaoBolsaAbertura.ValorMercado = gerPosicaoBolsa.ValorMercado;
                gerPosicaoBolsaAbertura.Quantidade = gerPosicaoBolsa.Quantidade;
                gerPosicaoBolsaAbertura.ResultadoRealizar = gerPosicaoBolsa.ResultadoRealizar;
            }
            #endregion

            gerPosicaoBolsaAberturaCollection.Save();
        }
    }
}