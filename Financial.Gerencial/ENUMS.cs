﻿using Financial.Util;
using System;
namespace Financial.Gerencial.Enums {
    
    /// <summary>
    /// 
    /// </summary>
    public enum TipoMercadoGerencial {
        [StringValue("Bolsa Ações")]
        Acoes = 1,

        [StringValue("Bolsa Opção Compra")]
        OpcaoCompraBolsa = 2,

        [StringValue("Bolsa Opção Venda")]
        OpcaoVendaBolsa = 3,

        [StringValue("Termo Bolsa")]
        TermoBolsa = 4,

        [StringValue("Bolsa Futuro")]
        FuturoBolsa = 5,

        [StringValue("Bolsa Ações - ADRs")]
        AcoesADR = 6,

        [StringValue("BMF Disponível")]
        DisponivelBMF = 10,
        
        [StringValue("BMF Futuro")]
        FuturoBMF = 11,

        [StringValue("BMF Opção Disponível")]
        OpcaoDisponivelBMF = 12,

        [StringValue("BMF Opção Futuro")]
        OpcaoFuturoBMF = 13,

        [StringValue("Termo BMF")]
        TermoBMF = 14
    }

    public static class TipoProcessamentoGerencial
    {
        public const string GerencialOperacoes = "S";
        public const string GerencialEstrategia = "E";
        public const string Ambos = "A";
        public const string NaoCalcula = "N";
    }
    
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Usado para a Tradução dos Enums: Momentaneamente Desligado")]
    public static class TraducaoEnumsGerencial {

        //#region Funções privates para fazer Pesquisa dentro dos Enums
        ///// <summary>
        ///// Dado um Codigo retorna um Enum do tipo TipoMercado
        ///// </summary>
        ///// <exception>throws ArgumentException se não existir um enum com o valor do Código</exception> 
        ///// <returns></returns>
        //private static TipoMercado SearchEnumTipoMercado(int codigo) {
        //    int[] tipoMercadoValues = (int[])Enum.GetValues(typeof(TipoMercado));

        //    int? posicao = null;
        //    for (int i = 0; i < tipoMercadoValues.Length; i++) {
        //        if (tipoMercadoValues[i] == codigo) {
        //            posicao = i;
        //            break;
        //        }
        //    }

        //    if (posicao.HasValue) {
        //        string tipoMercadoString = Enum.GetNames(typeof(TipoMercado))[posicao.Value];
        //        // Monta o Enum de acordo com a string
        //        TipoMercado tipoMercado = (TipoMercado)Enum.Parse(typeof(TipoMercado), tipoMercadoString);
        //        return tipoMercado;
        //    }
        //    else {
        //        throw new ArgumentException("TipoMercado não possue Constante com valor " + codigo);
        //    }
        //}

        //#endregion

        //public static class EnumTipoMercado {
        //    /// <summary>
        //    /// Realiza a tradução do Enum TipoMercado
        //    /// </summary>
        //    /// <param name="codigo"></param>
        //    /// <returns></returns>
        //    public static string TraduzEnum(int codigo) {
        //        TipoMercado t = TraducaoEnumsGerencial.SearchEnumTipoMercado(codigo);
        //        string chave = "#" + StringEnum.GetStringValue(t);
        //        //return System.Resources.ResourceManager.GetString(chave);
        //        return "none";
        //    }
        //}
    }
}
