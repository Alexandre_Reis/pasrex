/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		

		
		

using Financial.Bolsa;
using Financial.Investidor;
using Financial.Common;
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esGerPosicaoBolsaCollection : esEntityCollection
	{
		public esGerPosicaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GerPosicaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esGerPosicaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGerPosicaoBolsaQuery);
		}
		#endregion
		
		virtual public GerPosicaoBolsa DetachEntity(GerPosicaoBolsa entity)
		{
			return base.DetachEntity(entity) as GerPosicaoBolsa;
		}
		
		virtual public GerPosicaoBolsa AttachEntity(GerPosicaoBolsa entity)
		{
			return base.AttachEntity(entity) as GerPosicaoBolsa;
		}
		
		virtual public void Combine(GerPosicaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GerPosicaoBolsa this[int index]
		{
			get
			{
				return base[index] as GerPosicaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GerPosicaoBolsa);
		}
	}



	[Serializable]
	abstract public class esGerPosicaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGerPosicaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esGerPosicaoBolsa()
		{

		}

		public esGerPosicaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGerPosicaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esGerPosicaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(GerPosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(GerPosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(GerPosicaoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(GerPosicaoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(GerPosicaoBolsaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(GerPosicaoBolsaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBolsa.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBolsaMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGerPosicaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
			

			private esGerPosicaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGerPosicaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGerPosicaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GerPosicaoBolsa : esGerPosicaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGerPosicaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBolsaMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GerPosicaoBolsaCollection")]
	public partial class GerPosicaoBolsaCollection : esGerPosicaoBolsaCollection, IEnumerable<GerPosicaoBolsa>
	{
		public GerPosicaoBolsaCollection()
		{

		}
		
		public static implicit operator List<GerPosicaoBolsa>(GerPosicaoBolsaCollection coll)
		{
			List<GerPosicaoBolsa> list = new List<GerPosicaoBolsa>();
			
			foreach (GerPosicaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GerPosicaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GerPosicaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GerPosicaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GerPosicaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GerPosicaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GerPosicaoBolsa AddNew()
		{
			GerPosicaoBolsa entity = base.AddNewEntity() as GerPosicaoBolsa;
			
			return entity;
		}

		public GerPosicaoBolsa FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as GerPosicaoBolsa;
		}


		#region IEnumerable<GerPosicaoBolsa> Members

		IEnumerator<GerPosicaoBolsa> IEnumerable<GerPosicaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GerPosicaoBolsa;
			}
		}

		#endregion
		
		private GerPosicaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GerPosicaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class GerPosicaoBolsa : esGerPosicaoBolsa
	{
		public GerPosicaoBolsa()
		{

		}
	
		public GerPosicaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esGerPosicaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GerPosicaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GerPosicaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GerPosicaoBolsaQuery query;
	}



	[Serializable]
	public partial class GerPosicaoBolsaQuery : esGerPosicaoBolsaQuery
	{
		public GerPosicaoBolsaQuery()
		{

		}		
		
		public GerPosicaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GerPosicaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GerPosicaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.IdTrader, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.TipoMercado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.DataVencimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.PUMercado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.PUCusto, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.Quantidade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.ValorMercado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBolsaMetadata.ColumnNames.ResultadoRealizar, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBolsaMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GerPosicaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GerPosicaoBolsaMetadata))
			{
				if(GerPosicaoBolsaMetadata.mapDelegates == null)
				{
					GerPosicaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GerPosicaoBolsaMetadata.meta == null)
				{
					GerPosicaoBolsaMetadata.meta = new GerPosicaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "GerPosicaoBolsa";
				meta.Destination = "GerPosicaoBolsa";
				
				meta.spInsert = "proc_GerPosicaoBolsaInsert";				
				meta.spUpdate = "proc_GerPosicaoBolsaUpdate";		
				meta.spDelete = "proc_GerPosicaoBolsaDelete";
				meta.spLoadAll = "proc_GerPosicaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_GerPosicaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GerPosicaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
