/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		

using Financial.BMF;
using Financial.Investidor;
using Financial.Common;

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esGerPosicaoBMFCollection : esEntityCollection
	{
		public esGerPosicaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GerPosicaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esGerPosicaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGerPosicaoBMFQuery);
		}
		#endregion
		
		virtual public GerPosicaoBMF DetachEntity(GerPosicaoBMF entity)
		{
			return base.DetachEntity(entity) as GerPosicaoBMF;
		}
		
		virtual public GerPosicaoBMF AttachEntity(GerPosicaoBMF entity)
		{
			return base.AttachEntity(entity) as GerPosicaoBMF;
		}
		
		virtual public void Combine(GerPosicaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GerPosicaoBMF this[int index]
		{
			get
			{
				return base[index] as GerPosicaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GerPosicaoBMF);
		}
	}



	[Serializable]
	abstract public class esGerPosicaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGerPosicaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esGerPosicaoBMF()
		{

		}

		public esGerPosicaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGerPosicaoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esGerPosicaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GerPosicaoBMF.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(GerPosicaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(GerPosicaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(GerPosicaoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(GerPosicaoBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(GerPosicaoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(GerPosicaoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(GerPosicaoBMFMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(GerPosicaoBMFMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(GerPosicaoBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMF.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGerPosicaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
			

			private esGerPosicaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGerPosicaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGerPosicaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GerPosicaoBMF : esGerPosicaoBMF
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGerPosicaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GerPosicaoBMFCollection")]
	public partial class GerPosicaoBMFCollection : esGerPosicaoBMFCollection, IEnumerable<GerPosicaoBMF>
	{
		public GerPosicaoBMFCollection()
		{

		}
		
		public static implicit operator List<GerPosicaoBMF>(GerPosicaoBMFCollection coll)
		{
			List<GerPosicaoBMF> list = new List<GerPosicaoBMF>();
			
			foreach (GerPosicaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GerPosicaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GerPosicaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GerPosicaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GerPosicaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GerPosicaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GerPosicaoBMF AddNew()
		{
			GerPosicaoBMF entity = base.AddNewEntity() as GerPosicaoBMF;
			
			return entity;
		}

		public GerPosicaoBMF FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as GerPosicaoBMF;
		}


		#region IEnumerable<GerPosicaoBMF> Members

		IEnumerator<GerPosicaoBMF> IEnumerable<GerPosicaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GerPosicaoBMF;
			}
		}

		#endregion
		
		private GerPosicaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GerPosicaoBMF' table
	/// </summary>

	[Serializable]
	public partial class GerPosicaoBMF : esGerPosicaoBMF
	{
		public GerPosicaoBMF()
		{

		}
	
		public GerPosicaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esGerPosicaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GerPosicaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GerPosicaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GerPosicaoBMFQuery query;
	}



	[Serializable]
	public partial class GerPosicaoBMFQuery : esGerPosicaoBMFQuery
	{
		public GerPosicaoBMFQuery()
		{

		}		
		
		public GerPosicaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GerPosicaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GerPosicaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.IdTrader, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.CdAtivoBMF, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.Serie, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.TipoMercado, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.PUMercado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.PUCusto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.Quantidade, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.ValorMercado, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFMetadata.ColumnNames.ResultadoRealizar, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GerPosicaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GerPosicaoBMFMetadata))
			{
				if(GerPosicaoBMFMetadata.mapDelegates == null)
				{
					GerPosicaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GerPosicaoBMFMetadata.meta == null)
				{
					GerPosicaoBMFMetadata.meta = new GerPosicaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "GerPosicaoBMF";
				meta.Destination = "GerPosicaoBMF";
				
				meta.spInsert = "proc_GerPosicaoBMFInsert";				
				meta.spUpdate = "proc_GerPosicaoBMFUpdate";		
				meta.spDelete = "proc_GerPosicaoBMFDelete";
				meta.spLoadAll = "proc_GerPosicaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_GerPosicaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GerPosicaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
