/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esHistoricoEstrategiaCollection : esEntityCollection
	{
		public esHistoricoEstrategiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "HistoricoEstrategiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esHistoricoEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esHistoricoEstrategiaQuery);
		}
		#endregion
		
		virtual public HistoricoEstrategia DetachEntity(HistoricoEstrategia entity)
		{
			return base.DetachEntity(entity) as HistoricoEstrategia;
		}
		
		virtual public HistoricoEstrategia AttachEntity(HistoricoEstrategia entity)
		{
			return base.AttachEntity(entity) as HistoricoEstrategia;
		}
		
		virtual public void Combine(HistoricoEstrategiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public HistoricoEstrategia this[int index]
		{
			get
			{
				return base[index] as HistoricoEstrategia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(HistoricoEstrategia);
		}
	}



	[Serializable]
	abstract public class esHistoricoEstrategia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esHistoricoEstrategiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esHistoricoEstrategia()
		{

		}

		public esHistoricoEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira, idEstrategia);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira, idEstrategia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esHistoricoEstrategiaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdCarteira == idCarteira, query.IdEstrategia == idEstrategia);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira, idEstrategia);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira, idEstrategia);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			esHistoricoEstrategiaQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCarteira == idCarteira, query.IdEstrategia == idEstrategia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCarteira",idCarteira);			parms.Add("IdEstrategia",idEstrategia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "ValorCarteira": this.str.ValorCarteira = (string)value; break;							
						case "ValorLiquidar": this.str.ValorLiquidar = (string)value; break;							
						case "SaldoCaixa": this.str.SaldoCaixa = (string)value; break;							
						case "ValorPL": this.str.ValorPL = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Cota": this.str.Cota = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "ValorCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCarteira = (System.Decimal?)value;
							break;
						
						case "ValorLiquidar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquidar = (System.Decimal?)value;
							break;
						
						case "SaldoCaixa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoCaixa = (System.Decimal?)value;
							break;
						
						case "ValorPL":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPL = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Cota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Cota = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to HistoricoEstrategia.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(HistoricoEstrategiaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoEstrategiaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(HistoricoEstrategiaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoEstrategiaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(HistoricoEstrategiaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoEstrategiaMetadata.ColumnNames.IdEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.ValorCarteira
		/// </summary>
		virtual public System.Decimal? ValorCarteira
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorCarteira);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.ValorLiquidar
		/// </summary>
		virtual public System.Decimal? ValorLiquidar
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorLiquidar);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorLiquidar, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.SaldoCaixa
		/// </summary>
		virtual public System.Decimal? SaldoCaixa
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.SaldoCaixa);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.SaldoCaixa, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.ValorPL
		/// </summary>
		virtual public System.Decimal? ValorPL
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorPL);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.ValorPL, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoEstrategia.Cota
		/// </summary>
		virtual public System.Decimal? Cota
		{
			get
			{
				return base.GetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.Cota);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoEstrategiaMetadata.ColumnNames.Cota, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esHistoricoEstrategia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorCarteira
			{
				get
				{
					System.Decimal? data = entity.ValorCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCarteira = null;
					else entity.ValorCarteira = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquidar
			{
				get
				{
					System.Decimal? data = entity.ValorLiquidar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquidar = null;
					else entity.ValorLiquidar = Convert.ToDecimal(value);
				}
			}
				
			public System.String SaldoCaixa
			{
				get
				{
					System.Decimal? data = entity.SaldoCaixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoCaixa = null;
					else entity.SaldoCaixa = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPL
			{
				get
				{
					System.Decimal? data = entity.ValorPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPL = null;
					else entity.ValorPL = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Cota
			{
				get
				{
					System.Decimal? data = entity.Cota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cota = null;
					else entity.Cota = Convert.ToDecimal(value);
				}
			}
			

			private esHistoricoEstrategia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esHistoricoEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esHistoricoEstrategia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class HistoricoEstrategia : esHistoricoEstrategia
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esHistoricoEstrategiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoEstrategiaMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorCarteira
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.ValorCarteira, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquidar
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.ValorLiquidar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem SaldoCaixa
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.SaldoCaixa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPL
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.ValorPL, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Cota
		{
			get
			{
				return new esQueryItem(this, HistoricoEstrategiaMetadata.ColumnNames.Cota, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("HistoricoEstrategiaCollection")]
	public partial class HistoricoEstrategiaCollection : esHistoricoEstrategiaCollection, IEnumerable<HistoricoEstrategia>
	{
		public HistoricoEstrategiaCollection()
		{

		}
		
		public static implicit operator List<HistoricoEstrategia>(HistoricoEstrategiaCollection coll)
		{
			List<HistoricoEstrategia> list = new List<HistoricoEstrategia>();
			
			foreach (HistoricoEstrategia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  HistoricoEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new HistoricoEstrategia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new HistoricoEstrategia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public HistoricoEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(HistoricoEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public HistoricoEstrategia AddNew()
		{
			HistoricoEstrategia entity = base.AddNewEntity() as HistoricoEstrategia;
			
			return entity;
		}

		public HistoricoEstrategia FindByPrimaryKey(System.DateTime data, System.Int32 idCarteira, System.Int32 idEstrategia)
		{
			return base.FindByPrimaryKey(data, idCarteira, idEstrategia) as HistoricoEstrategia;
		}


		#region IEnumerable<HistoricoEstrategia> Members

		IEnumerator<HistoricoEstrategia> IEnumerable<HistoricoEstrategia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as HistoricoEstrategia;
			}
		}

		#endregion
		
		private HistoricoEstrategiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'HistoricoEstrategia' table
	/// </summary>

	[Serializable]
	public partial class HistoricoEstrategia : esHistoricoEstrategia
	{
		public HistoricoEstrategia()
		{

		}
	
		public HistoricoEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esHistoricoEstrategiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public HistoricoEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(HistoricoEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private HistoricoEstrategiaQuery query;
	}



	[Serializable]
	public partial class HistoricoEstrategiaQuery : esHistoricoEstrategiaQuery
	{
		public HistoricoEstrategiaQuery()
		{

		}		
		
		public HistoricoEstrategiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class HistoricoEstrategiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoEstrategiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.IdEstrategia, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.IdEstrategia;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.ValorCarteira, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.ValorCarteira;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.ValorLiquidar, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.ValorLiquidar;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.SaldoCaixa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.SaldoCaixa;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.ValorPL, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.ValorPL;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 26;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoEstrategiaMetadata.ColumnNames.Cota, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoEstrategiaMetadata.PropertyNames.Cota;	
			c.NumericPrecision = 26;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public HistoricoEstrategiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorCarteira = "ValorCarteira";
			 public const string ValorLiquidar = "ValorLiquidar";
			 public const string SaldoCaixa = "SaldoCaixa";
			 public const string ValorPL = "ValorPL";
			 public const string Quantidade = "Quantidade";
			 public const string Cota = "Cota";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorCarteira = "ValorCarteira";
			 public const string ValorLiquidar = "ValorLiquidar";
			 public const string SaldoCaixa = "SaldoCaixa";
			 public const string ValorPL = "ValorPL";
			 public const string Quantidade = "Quantidade";
			 public const string Cota = "Cota";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoEstrategiaMetadata))
			{
				if(HistoricoEstrategiaMetadata.mapDelegates == null)
				{
					HistoricoEstrategiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoEstrategiaMetadata.meta == null)
				{
					HistoricoEstrategiaMetadata.meta = new HistoricoEstrategiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorCarteira", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquidar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("SaldoCaixa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPL", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Cota", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "HistoricoEstrategia";
				meta.Destination = "HistoricoEstrategia";
				
				meta.spInsert = "proc_HistoricoEstrategiaInsert";				
				meta.spUpdate = "proc_HistoricoEstrategiaUpdate";		
				meta.spDelete = "proc_HistoricoEstrategiaDelete";
				meta.spLoadAll = "proc_HistoricoEstrategiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoEstrategiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoEstrategiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
