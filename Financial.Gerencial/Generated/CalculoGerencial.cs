/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				



using Financial.Investidor;
using Financial.Common;


		

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esCalculoGerencialCollection : esEntityCollection
	{
		public esCalculoGerencialCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoGerencialCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoGerencialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoGerencialQuery);
		}
		#endregion
		
		virtual public CalculoGerencial DetachEntity(CalculoGerencial entity)
		{
			return base.DetachEntity(entity) as CalculoGerencial;
		}
		
		virtual public CalculoGerencial AttachEntity(CalculoGerencial entity)
		{
			return base.AttachEntity(entity) as CalculoGerencial;
		}
		
		virtual public void Combine(CalculoGerencialCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoGerencial this[int index]
		{
			get
			{
				return base[index] as CalculoGerencial;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoGerencial);
		}
	}



	[Serializable]
	abstract public class esCalculoGerencial : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoGerencialQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoGerencial()
		{

		}

		public esCalculoGerencial(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idTrader, cdAtivo, dataCalculo);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idTrader, cdAtivo, dataCalculo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoGerencialQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdTrader == idTrader, query.CdAtivo == cdAtivo, query.DataCalculo == dataCalculo);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idTrader, cdAtivo, dataCalculo);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idTrader, cdAtivo, dataCalculo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			esCalculoGerencialQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdTrader == idTrader, query.CdAtivo == cdAtivo, query.DataCalculo == dataCalculo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdTrader",idTrader);			parms.Add("CdAtivo",cdAtivo);			parms.Add("DataCalculo",dataCalculo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "CdAtivo": this.str.CdAtivo = (string)value; break;							
						case "DataCalculo": this.str.DataCalculo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;							
						case "ResultadoDayTrade": this.str.ResultadoDayTrade = (string)value; break;							
						case "ResultadoNormal": this.str.ResultadoNormal = (string)value; break;							
						case "ResultadoAcumulado": this.str.ResultadoAcumulado = (string)value; break;							
						case "ResultadoDia": this.str.ResultadoDia = (string)value; break;							
						case "ValorizacaoDia": this.str.ValorizacaoDia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "DataCalculo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCalculo = (System.DateTime?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
						
						case "ResultadoDayTrade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoDayTrade = (System.Decimal?)value;
							break;
						
						case "ResultadoNormal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNormal = (System.Decimal?)value;
							break;
						
						case "ResultadoAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoAcumulado = (System.Decimal?)value;
							break;
						
						case "ResultadoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoDia = (System.Decimal?)value;
							break;
						
						case "ValorizacaoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorizacaoDia = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoGerencial.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(CalculoGerencialMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoGerencialMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(CalculoGerencialMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoGerencialMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.CdAtivo
		/// </summary>
		virtual public System.String CdAtivo
		{
			get
			{
				return base.GetSystemString(CalculoGerencialMetadata.ColumnNames.CdAtivo);
			}
			
			set
			{
				base.SetSystemString(CalculoGerencialMetadata.ColumnNames.CdAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.DataCalculo
		/// </summary>
		virtual public System.DateTime? DataCalculo
		{
			get
			{
				return base.GetSystemDateTime(CalculoGerencialMetadata.ColumnNames.DataCalculo);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoGerencialMetadata.ColumnNames.DataCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(CalculoGerencialMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(CalculoGerencialMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CalculoGerencialMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoGerencialMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ResultadoDayTrade
		/// </summary>
		virtual public System.Decimal? ResultadoDayTrade
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoDayTrade);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoDayTrade, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ResultadoNormal
		/// </summary>
		virtual public System.Decimal? ResultadoNormal
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoNormal);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoNormal, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ResultadoAcumulado
		/// </summary>
		virtual public System.Decimal? ResultadoAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ResultadoDia
		/// </summary>
		virtual public System.Decimal? ResultadoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ResultadoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoGerencial.ValorizacaoDia
		/// </summary>
		virtual public System.Decimal? ValorizacaoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ValorizacaoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoGerencialMetadata.ColumnNames.ValorizacaoDia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoGerencial entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivo
			{
				get
				{
					System.String data = entity.CdAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivo = null;
					else entity.CdAtivo = Convert.ToString(value);
				}
			}
				
			public System.String DataCalculo
			{
				get
				{
					System.DateTime? data = entity.DataCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCalculo = null;
					else entity.DataCalculo = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoDayTrade
			{
				get
				{
					System.Decimal? data = entity.ResultadoDayTrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoDayTrade = null;
					else entity.ResultadoDayTrade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoNormal
			{
				get
				{
					System.Decimal? data = entity.ResultadoNormal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNormal = null;
					else entity.ResultadoNormal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoAcumulado
			{
				get
				{
					System.Decimal? data = entity.ResultadoAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoAcumulado = null;
					else entity.ResultadoAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoDia
			{
				get
				{
					System.Decimal? data = entity.ResultadoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoDia = null;
					else entity.ResultadoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorizacaoDia
			{
				get
				{
					System.Decimal? data = entity.ValorizacaoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorizacaoDia = null;
					else entity.ValorizacaoDia = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoGerencial entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoGerencialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoGerencial can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoGerencial : esCalculoGerencial
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_CalculoGerencial_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_CalculoGerencial_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoGerencialQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoGerencialMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivo
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.CdAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem DataCalculo
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.DataCalculo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoDayTrade
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ResultadoDayTrade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoNormal
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ResultadoNormal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ResultadoAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoDia
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ResultadoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorizacaoDia
		{
			get
			{
				return new esQueryItem(this, CalculoGerencialMetadata.ColumnNames.ValorizacaoDia, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoGerencialCollection")]
	public partial class CalculoGerencialCollection : esCalculoGerencialCollection, IEnumerable<CalculoGerencial>
	{
		public CalculoGerencialCollection()
		{

		}
		
		public static implicit operator List<CalculoGerencial>(CalculoGerencialCollection coll)
		{
			List<CalculoGerencial> list = new List<CalculoGerencial>();
			
			foreach (CalculoGerencial emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoGerencialMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoGerencialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoGerencial(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoGerencial();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoGerencialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoGerencialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoGerencialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoGerencial AddNew()
		{
			CalculoGerencial entity = base.AddNewEntity() as CalculoGerencial;
			
			return entity;
		}

		public CalculoGerencial FindByPrimaryKey(System.Int32 idCliente, System.Int32 idTrader, System.String cdAtivo, System.DateTime dataCalculo)
		{
			return base.FindByPrimaryKey(idCliente, idTrader, cdAtivo, dataCalculo) as CalculoGerencial;
		}


		#region IEnumerable<CalculoGerencial> Members

		IEnumerator<CalculoGerencial> IEnumerable<CalculoGerencial>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoGerencial;
			}
		}

		#endregion
		
		private CalculoGerencialQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoGerencial' table
	/// </summary>

	[Serializable]
	public partial class CalculoGerencial : esCalculoGerencial
	{
		public CalculoGerencial()
		{

		}
	
		public CalculoGerencial(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoGerencialMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoGerencialQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoGerencialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoGerencialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoGerencialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoGerencialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoGerencialQuery query;
	}



	[Serializable]
	public partial class CalculoGerencialQuery : esCalculoGerencialQuery
	{
		public CalculoGerencialQuery()
		{

		}		
		
		public CalculoGerencialQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoGerencialMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoGerencialMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.IdTrader, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.IdTrader;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.CdAtivo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.CdAtivo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.DataCalculo, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.DataCalculo;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.TipoMercado, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.DataVencimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 20;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.PUCusto, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.PUMercado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ValorMercado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ResultadoRealizar, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ResultadoDayTrade, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ResultadoDayTrade;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ResultadoNormal, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ResultadoNormal;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ResultadoAcumulado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ResultadoAcumulado;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ResultadoDia, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ResultadoDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoGerencialMetadata.ColumnNames.ValorizacaoDia, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoGerencialMetadata.PropertyNames.ValorizacaoDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoGerencialMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdTrader = "IdTrader";
			 public const string CdAtivo = "CdAtivo";
			 public const string DataCalculo = "DataCalculo";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string PUCusto = "PUCusto";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string ResultadoDayTrade = "ResultadoDayTrade";
			 public const string ResultadoNormal = "ResultadoNormal";
			 public const string ResultadoAcumulado = "ResultadoAcumulado";
			 public const string ResultadoDia = "ResultadoDia";
			 public const string ValorizacaoDia = "ValorizacaoDia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdTrader = "IdTrader";
			 public const string CdAtivo = "CdAtivo";
			 public const string DataCalculo = "DataCalculo";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string PUCusto = "PUCusto";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string ResultadoDayTrade = "ResultadoDayTrade";
			 public const string ResultadoNormal = "ResultadoNormal";
			 public const string ResultadoAcumulado = "ResultadoAcumulado";
			 public const string ResultadoDia = "ResultadoDia";
			 public const string ValorizacaoDia = "ValorizacaoDia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoGerencialMetadata))
			{
				if(CalculoGerencialMetadata.mapDelegates == null)
				{
					CalculoGerencialMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoGerencialMetadata.meta == null)
				{
					CalculoGerencialMetadata.meta = new CalculoGerencialMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataCalculo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoDayTrade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoNormal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorizacaoDia", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoGerencial";
				meta.Destination = "CalculoGerencial";
				
				meta.spInsert = "proc_CalculoGerencialInsert";				
				meta.spUpdate = "proc_CalculoGerencialUpdate";		
				meta.spDelete = "proc_CalculoGerencialDelete";
				meta.spLoadAll = "proc_CalculoGerencialLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoGerencialLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoGerencialMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
