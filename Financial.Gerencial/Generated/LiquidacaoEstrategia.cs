/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esLiquidacaoEstrategiaCollection : esEntityCollection
	{
		public esLiquidacaoEstrategiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoEstrategiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoEstrategiaQuery);
		}
		#endregion
		
		virtual public LiquidacaoEstrategia DetachEntity(LiquidacaoEstrategia entity)
		{
			return base.DetachEntity(entity) as LiquidacaoEstrategia;
		}
		
		virtual public LiquidacaoEstrategia AttachEntity(LiquidacaoEstrategia entity)
		{
			return base.AttachEntity(entity) as LiquidacaoEstrategia;
		}
		
		virtual public void Combine(LiquidacaoEstrategiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoEstrategia this[int index]
		{
			get
			{
				return base[index] as LiquidacaoEstrategia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoEstrategia);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoEstrategia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoEstrategiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoEstrategia()
		{

		}

		public esLiquidacaoEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoEstrategiaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao)
		{
			esLiquidacaoEstrategiaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEstrategiaMetadata.ColumnNames.IdEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoEstrategiaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoEstrategiaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoEstrategiaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoEstrategiaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEstrategia.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoEstrategiaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoEstrategiaMetadata.ColumnNames.Valor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoEstrategia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esLiquidacaoEstrategia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoEstrategia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoEstrategia : esLiquidacaoEstrategia
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoEstrategiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoEstrategiaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEstrategiaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoEstrategiaCollection")]
	public partial class LiquidacaoEstrategiaCollection : esLiquidacaoEstrategiaCollection, IEnumerable<LiquidacaoEstrategia>
	{
		public LiquidacaoEstrategiaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoEstrategia>(LiquidacaoEstrategiaCollection coll)
		{
			List<LiquidacaoEstrategia> list = new List<LiquidacaoEstrategia>();
			
			foreach (LiquidacaoEstrategia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoEstrategia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoEstrategia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoEstrategia AddNew()
		{
			LiquidacaoEstrategia entity = base.AddNewEntity() as LiquidacaoEstrategia;
			
			return entity;
		}

		public LiquidacaoEstrategia FindByPrimaryKey(System.Int32 idLiquidacao)
		{
			return base.FindByPrimaryKey(idLiquidacao) as LiquidacaoEstrategia;
		}


		#region IEnumerable<LiquidacaoEstrategia> Members

		IEnumerator<LiquidacaoEstrategia> IEnumerable<LiquidacaoEstrategia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoEstrategia;
			}
		}

		#endregion
		
		private LiquidacaoEstrategiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoEstrategia' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoEstrategia : esLiquidacaoEstrategia
	{
		public LiquidacaoEstrategia()
		{

		}
	
		public LiquidacaoEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoEstrategiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoEstrategiaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoEstrategiaQuery : esLiquidacaoEstrategiaQuery
	{
		public LiquidacaoEstrategiaQuery()
		{

		}		
		
		public LiquidacaoEstrategiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoEstrategiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoEstrategiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.IdEstrategia, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEstrategiaMetadata.ColumnNames.Valor, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoEstrategiaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoEstrategiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string DataReferencia = "DataReferencia";
			 public const string DataVencimento = "DataVencimento";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string DataReferencia = "DataReferencia";
			 public const string DataVencimento = "DataVencimento";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoEstrategiaMetadata))
			{
				if(LiquidacaoEstrategiaMetadata.mapDelegates == null)
				{
					LiquidacaoEstrategiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoEstrategiaMetadata.meta == null)
				{
					LiquidacaoEstrategiaMetadata.meta = new LiquidacaoEstrategiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "LiquidacaoEstrategia";
				meta.Destination = "LiquidacaoEstrategia";
				
				meta.spInsert = "proc_LiquidacaoEstrategiaInsert";				
				meta.spUpdate = "proc_LiquidacaoEstrategiaUpdate";		
				meta.spDelete = "proc_LiquidacaoEstrategiaDelete";
				meta.spLoadAll = "proc_LiquidacaoEstrategiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoEstrategiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoEstrategiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
