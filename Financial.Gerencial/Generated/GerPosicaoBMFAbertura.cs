/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		


using Financial.BMF;
using Financial.Investidor;
using Financial.Common;
		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esGerPosicaoBMFAberturaCollection : esEntityCollection
	{
		public esGerPosicaoBMFAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GerPosicaoBMFAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esGerPosicaoBMFAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGerPosicaoBMFAberturaQuery);
		}
		#endregion
		
		virtual public GerPosicaoBMFAbertura DetachEntity(GerPosicaoBMFAbertura entity)
		{
			return base.DetachEntity(entity) as GerPosicaoBMFAbertura;
		}
		
		virtual public GerPosicaoBMFAbertura AttachEntity(GerPosicaoBMFAbertura entity)
		{
			return base.AttachEntity(entity) as GerPosicaoBMFAbertura;
		}
		
		virtual public void Combine(GerPosicaoBMFAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GerPosicaoBMFAbertura this[int index]
		{
			get
			{
				return base[index] as GerPosicaoBMFAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GerPosicaoBMFAbertura);
		}
	}



	[Serializable]
	abstract public class esGerPosicaoBMFAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGerPosicaoBMFAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esGerPosicaoBMFAbertura()
		{

		}

		public esGerPosicaoBMFAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGerPosicaoBMFAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdPosicao == idPosicao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			esGerPosicaoBMFAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(GerPosicaoBMFAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(GerPosicaoBMFAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(GerPosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(GerPosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(GerPosicaoBMFAberturaMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(GerPosicaoBMFAberturaMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(GerPosicaoBMFAberturaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(GerPosicaoBMFAberturaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(GerPosicaoBMFAberturaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(GerPosicaoBMFAberturaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(GerPosicaoBMFAberturaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerPosicaoBMFAbertura.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(GerPosicaoBMFAberturaMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGerPosicaoBMFAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
			

			private esGerPosicaoBMFAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGerPosicaoBMFAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGerPosicaoBMFAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GerPosicaoBMFAbertura : esGerPosicaoBMFAbertura
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGerPosicaoBMFAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBMFAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, GerPosicaoBMFAberturaMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GerPosicaoBMFAberturaCollection")]
	public partial class GerPosicaoBMFAberturaCollection : esGerPosicaoBMFAberturaCollection, IEnumerable<GerPosicaoBMFAbertura>
	{
		public GerPosicaoBMFAberturaCollection()
		{

		}
		
		public static implicit operator List<GerPosicaoBMFAbertura>(GerPosicaoBMFAberturaCollection coll)
		{
			List<GerPosicaoBMFAbertura> list = new List<GerPosicaoBMFAbertura>();
			
			foreach (GerPosicaoBMFAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GerPosicaoBMFAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBMFAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GerPosicaoBMFAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GerPosicaoBMFAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GerPosicaoBMFAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBMFAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GerPosicaoBMFAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GerPosicaoBMFAbertura AddNew()
		{
			GerPosicaoBMFAbertura entity = base.AddNewEntity() as GerPosicaoBMFAbertura;
			
			return entity;
		}

		public GerPosicaoBMFAbertura FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(dataHistorico, idPosicao) as GerPosicaoBMFAbertura;
		}


		#region IEnumerable<GerPosicaoBMFAbertura> Members

		IEnumerator<GerPosicaoBMFAbertura> IEnumerable<GerPosicaoBMFAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GerPosicaoBMFAbertura;
			}
		}

		#endregion
		
		private GerPosicaoBMFAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GerPosicaoBMFAbertura' table
	/// </summary>

	[Serializable]
	public partial class GerPosicaoBMFAbertura : esGerPosicaoBMFAbertura
	{
		public GerPosicaoBMFAbertura()
		{

		}
	
		public GerPosicaoBMFAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GerPosicaoBMFAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esGerPosicaoBMFAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerPosicaoBMFAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GerPosicaoBMFAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerPosicaoBMFAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GerPosicaoBMFAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GerPosicaoBMFAberturaQuery query;
	}



	[Serializable]
	public partial class GerPosicaoBMFAberturaQuery : esGerPosicaoBMFAberturaQuery
	{
		public GerPosicaoBMFAberturaQuery()
		{

		}		
		
		public GerPosicaoBMFAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GerPosicaoBMFAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GerPosicaoBMFAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.IdTrader, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.Serie, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.TipoMercado, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.DataVencimento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.PUMercado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.PUCusto, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.Quantidade, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.ValorMercado, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerPosicaoBMFAberturaMetadata.ColumnNames.ResultadoRealizar, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerPosicaoBMFAberturaMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GerPosicaoBMFAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GerPosicaoBMFAberturaMetadata))
			{
				if(GerPosicaoBMFAberturaMetadata.mapDelegates == null)
				{
					GerPosicaoBMFAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GerPosicaoBMFAberturaMetadata.meta == null)
				{
					GerPosicaoBMFAberturaMetadata.meta = new GerPosicaoBMFAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "GerPosicaoBMFAbertura";
				meta.Destination = "GerPosicaoBMFAbertura";
				
				meta.spInsert = "proc_GerPosicaoBMFAberturaInsert";				
				meta.spUpdate = "proc_GerPosicaoBMFAberturaUpdate";		
				meta.spDelete = "proc_GerPosicaoBMFAberturaDelete";
				meta.spLoadAll = "proc_GerPosicaoBMFAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_GerPosicaoBMFAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GerPosicaoBMFAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
