/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




using Financial.BMF;
using Financial.Investidor;
using Financial.Common;

		

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esGerOperacaoBMFCollection : esEntityCollection
	{
		public esGerOperacaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GerOperacaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esGerOperacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGerOperacaoBMFQuery);
		}
		#endregion
		
		virtual public GerOperacaoBMF DetachEntity(GerOperacaoBMF entity)
		{
			return base.DetachEntity(entity) as GerOperacaoBMF;
		}
		
		virtual public GerOperacaoBMF AttachEntity(GerOperacaoBMF entity)
		{
			return base.AttachEntity(entity) as GerOperacaoBMF;
		}
		
		virtual public void Combine(GerOperacaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GerOperacaoBMF this[int index]
		{
			get
			{
				return base[index] as GerOperacaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GerOperacaoBMF);
		}
	}



	[Serializable]
	abstract public class esGerOperacaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGerOperacaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esGerOperacaoBMF()
		{

		}

		public esGerOperacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGerOperacaoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esGerOperacaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "PULiquido": this.str.PULiquido = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "Despesas": this.str.Despesas = (string)value; break;							
						case "ResultadoRealizado": this.str.ResultadoRealizado = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Ajuste": this.str.Ajuste = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "PULiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquido = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "Despesas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Despesas = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizado = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "Ajuste":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Ajuste = (System.Decimal?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GerOperacaoBMF.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(GerOperacaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(GerOperacaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(GerOperacaoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(GerOperacaoBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(GerOperacaoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(GerOperacaoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.TipoOperacao
		/// </summary>
		virtual public System.String TipoOperacao
		{
			get
			{
				return base.GetSystemString(GerOperacaoBMFMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemString(GerOperacaoBMFMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(GerOperacaoBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(GerOperacaoBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(GerOperacaoBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.PULiquido
		/// </summary>
		virtual public System.Decimal? PULiquido
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.PULiquido);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.PULiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Despesas
		/// </summary>
		virtual public System.Decimal? Despesas
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Despesas);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Despesas, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.ResultadoRealizado
		/// </summary>
		virtual public System.Decimal? ResultadoRealizado
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.ResultadoRealizado);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.ResultadoRealizado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(GerOperacaoBMFMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(GerOperacaoBMFMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Ajuste
		/// </summary>
		virtual public System.Decimal? Ajuste
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Ajuste);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Ajuste, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBMFMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBMF.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(GerOperacaoBMFMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(GerOperacaoBMFMetadata.ColumnNames.Fonte, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGerOperacaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.String data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquido
			{
				get
				{
					System.Decimal? data = entity.PULiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquido = null;
					else entity.PULiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Despesas
			{
				get
				{
					System.Decimal? data = entity.Despesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Despesas = null;
					else entity.Despesas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizado
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizado = null;
					else entity.ResultadoRealizado = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String Ajuste
			{
				get
				{
					System.Decimal? data = entity.Ajuste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ajuste = null;
					else entity.Ajuste = Convert.ToDecimal(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
			

			private esGerOperacaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGerOperacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGerOperacaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GerOperacaoBMF : esGerOperacaoBMF
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGerOperacaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GerOperacaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.TipoOperacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquido
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.PULiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Despesas
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Despesas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizado
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.ResultadoRealizado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Ajuste
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Ajuste, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBMFMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GerOperacaoBMFCollection")]
	public partial class GerOperacaoBMFCollection : esGerOperacaoBMFCollection, IEnumerable<GerOperacaoBMF>
	{
		public GerOperacaoBMFCollection()
		{

		}
		
		public static implicit operator List<GerOperacaoBMF>(GerOperacaoBMFCollection coll)
		{
			List<GerOperacaoBMF> list = new List<GerOperacaoBMF>();
			
			foreach (GerOperacaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GerOperacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerOperacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GerOperacaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GerOperacaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GerOperacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerOperacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GerOperacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GerOperacaoBMF AddNew()
		{
			GerOperacaoBMF entity = base.AddNewEntity() as GerOperacaoBMF;
			
			return entity;
		}

		public GerOperacaoBMF FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as GerOperacaoBMF;
		}


		#region IEnumerable<GerOperacaoBMF> Members

		IEnumerator<GerOperacaoBMF> IEnumerable<GerOperacaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GerOperacaoBMF;
			}
		}

		#endregion
		
		private GerOperacaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GerOperacaoBMF' table
	/// </summary>

	[Serializable]
	public partial class GerOperacaoBMF : esGerOperacaoBMF
	{
		public GerOperacaoBMF()
		{

		}
	
		public GerOperacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GerOperacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esGerOperacaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerOperacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GerOperacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerOperacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GerOperacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GerOperacaoBMFQuery query;
	}



	[Serializable]
	public partial class GerOperacaoBMFQuery : esGerOperacaoBMFQuery
	{
		public GerOperacaoBMFQuery()
		{

		}		
		
		public GerOperacaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GerOperacaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GerOperacaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.IdTrader, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.CdAtivoBMF, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Serie, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.TipoMercado, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.TipoOperacao, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.TipoOperacao;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Data, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Quantidade, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Pu, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.PULiquido, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.PULiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Valor, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.ValorLiquido, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Despesas, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Despesas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.ResultadoRealizado, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.ResultadoRealizado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Origem, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Ajuste, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Ajuste;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Taxa, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBMFMetadata.ColumnNames.Fonte, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerOperacaoBMFMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GerOperacaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Despesas = "Despesas";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string Ajuste = "Ajuste";
			 public const string Taxa = "Taxa";
			 public const string Fonte = "Fonte";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Despesas = "Despesas";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string Ajuste = "Ajuste";
			 public const string Taxa = "Taxa";
			 public const string Fonte = "Fonte";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GerOperacaoBMFMetadata))
			{
				if(GerOperacaoBMFMetadata.mapDelegates == null)
				{
					GerOperacaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GerOperacaoBMFMetadata.meta == null)
				{
					GerOperacaoBMFMetadata.meta = new GerOperacaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Despesas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Ajuste", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "GerOperacaoBMF";
				meta.Destination = "GerOperacaoBMF";
				
				meta.spInsert = "proc_GerOperacaoBMFInsert";				
				meta.spUpdate = "proc_GerOperacaoBMFUpdate";		
				meta.spDelete = "proc_GerOperacaoBMFDelete";
				meta.spLoadAll = "proc_GerOperacaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_GerOperacaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GerOperacaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
