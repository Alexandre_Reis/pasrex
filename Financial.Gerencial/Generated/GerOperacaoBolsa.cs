/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/05/2011 11:47:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		
























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				





using Financial.Bolsa;
using Financial.Investidor;
using Financial.Common;
		

		
		
		
		
		





namespace Financial.Gerencial
{

	[Serializable]
	abstract public class esGerOperacaoBolsaCollection : esEntityCollection
	{
		public esGerOperacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GerOperacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esGerOperacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGerOperacaoBolsaQuery);
		}
		#endregion
		
		virtual public GerOperacaoBolsa DetachEntity(GerOperacaoBolsa entity)
		{
			return base.DetachEntity(entity) as GerOperacaoBolsa;
		}
		
		virtual public GerOperacaoBolsa AttachEntity(GerOperacaoBolsa entity)
		{
			return base.AttachEntity(entity) as GerOperacaoBolsa;
		}
		
		virtual public void Combine(GerOperacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GerOperacaoBolsa this[int index]
		{
			get
			{
				return base[index] as GerOperacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GerOperacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esGerOperacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGerOperacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esGerOperacaoBolsa()
		{

		}

		public esGerOperacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGerOperacaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
				
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esGerOperacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "PULiquido": this.str.PULiquido = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "Despesas": this.str.Despesas = (string)value; break;							
						case "ResultadoRealizado": this.str.ResultadoRealizado = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "QuantidadeCasadaTermo": this.str.QuantidadeCasadaTermo = (string)value; break;							
						case "QuantidadeCasadaExercicio": this.str.QuantidadeCasadaExercicio = (string)value; break;							
						case "ResultadoTermo": this.str.ResultadoTermo = (string)value; break;							
						case "ResultadoExercicio": this.str.ResultadoExercicio = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "CdAtivoBolsaOpcao": this.str.CdAtivoBolsaOpcao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "PULiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquido = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "Despesas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Despesas = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizado = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "QuantidadeCasadaTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCasadaTermo = (System.Decimal?)value;
							break;
						
						case "QuantidadeCasadaExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCasadaExercicio = (System.Decimal?)value;
							break;
						
						case "ResultadoTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermo = (System.Decimal?)value;
							break;
						
						case "ResultadoExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoExercicio = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(GerOperacaoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(GerOperacaoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(GerOperacaoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.TipoOperacao
		/// </summary>
		virtual public System.String TipoOperacao
		{
			get
			{
				return base.GetSystemString(GerOperacaoBolsaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemString(GerOperacaoBolsaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(GerOperacaoBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(GerOperacaoBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.PULiquido
		/// </summary>
		virtual public System.Decimal? PULiquido
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.PULiquido);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.PULiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Despesas
		/// </summary>
		virtual public System.Decimal? Despesas
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Despesas);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.Despesas, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.ResultadoRealizado
		/// </summary>
		virtual public System.Decimal? ResultadoRealizado
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoRealizado);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(GerOperacaoBolsaMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(GerOperacaoBolsaMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.QuantidadeCasadaTermo
		/// </summary>
		virtual public System.Decimal? QuantidadeCasadaTermo
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.QuantidadeCasadaExercicio
		/// </summary>
		virtual public System.Decimal? QuantidadeCasadaExercicio
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.ResultadoTermo
		/// </summary>
		virtual public System.Decimal? ResultadoTermo
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoTermo);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.ResultadoExercicio
		/// </summary>
		virtual public System.Decimal? ResultadoExercicio
		{
			get
			{
				return base.GetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(GerOperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(GerOperacaoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(GerOperacaoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to GerOperacaoBolsa.CdAtivoBolsaOpcao
		/// </summary>
		virtual public System.String CdAtivoBolsaOpcao
		{
			get
			{
				return base.GetSystemString(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao);
			}
			
			set
			{
				base.SetSystemString(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGerOperacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.String data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquido
			{
				get
				{
					System.Decimal? data = entity.PULiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquido = null;
					else entity.PULiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Despesas
			{
				get
				{
					System.Decimal? data = entity.Despesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Despesas = null;
					else entity.Despesas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizado
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizado = null;
					else entity.ResultadoRealizado = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String QuantidadeCasadaTermo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCasadaTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCasadaTermo = null;
					else entity.QuantidadeCasadaTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeCasadaExercicio
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCasadaExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCasadaExercicio = null;
					else entity.QuantidadeCasadaExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermo
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermo = null;
					else entity.ResultadoTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoExercicio
			{
				get
				{
					System.Decimal? data = entity.ResultadoExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoExercicio = null;
					else entity.ResultadoExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String CdAtivoBolsaOpcao
			{
				get
				{
					System.String data = entity.CdAtivoBolsaOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaOpcao = null;
					else entity.CdAtivoBolsaOpcao = Convert.ToString(value);
				}
			}
			

			private esGerOperacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGerOperacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGerOperacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GerOperacaoBolsa : esGerOperacaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGerOperacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GerOperacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.TipoOperacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquido
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.PULiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Despesas
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Despesas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizado
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem QuantidadeCasadaTermo
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeCasadaExercicio
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermo
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.ResultadoTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoExercicio
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CdAtivoBolsaOpcao
		{
			get
			{
				return new esQueryItem(this, GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GerOperacaoBolsaCollection")]
	public partial class GerOperacaoBolsaCollection : esGerOperacaoBolsaCollection, IEnumerable<GerOperacaoBolsa>
	{
		public GerOperacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<GerOperacaoBolsa>(GerOperacaoBolsaCollection coll)
		{
			List<GerOperacaoBolsa> list = new List<GerOperacaoBolsa>();
			
			foreach (GerOperacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GerOperacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerOperacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GerOperacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GerOperacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GerOperacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerOperacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GerOperacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GerOperacaoBolsa AddNew()
		{
			GerOperacaoBolsa entity = base.AddNewEntity() as GerOperacaoBolsa;
			
			return entity;
		}

		public GerOperacaoBolsa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as GerOperacaoBolsa;
		}


		#region IEnumerable<GerOperacaoBolsa> Members

		IEnumerator<GerOperacaoBolsa> IEnumerable<GerOperacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GerOperacaoBolsa;
			}
		}

		#endregion
		
		private GerOperacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GerOperacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class GerOperacaoBolsa : esGerOperacaoBolsa
	{
		public GerOperacaoBolsa()
		{

		}
	
		public GerOperacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GerOperacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esGerOperacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GerOperacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GerOperacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GerOperacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GerOperacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GerOperacaoBolsaQuery query;
	}



	[Serializable]
	public partial class GerOperacaoBolsaQuery : esGerOperacaoBolsaQuery
	{
		public GerOperacaoBolsaQuery()
		{

		}		
		
		public GerOperacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GerOperacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GerOperacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.IdTrader, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.TipoMercado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.TipoOperacao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.TipoOperacao;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Data, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Pu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 14;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.PULiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.PULiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Valor, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.ValorLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Despesas, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Despesas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.ResultadoRealizado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Origem, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.QuantidadeCasadaTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.QuantidadeCasadaExercicio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.ResultadoTermo, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.ResultadoTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.ResultadoExercicio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.Fonte, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = GerOperacaoBolsaMetadata.PropertyNames.CdAtivoBolsaOpcao;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GerOperacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Despesas = "Despesas";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string QuantidadeCasadaTermo = "QuantidadeCasadaTermo";
			 public const string QuantidadeCasadaExercicio = "QuantidadeCasadaExercicio";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string ResultadoExercicio = "ResultadoExercicio";
			 public const string Fonte = "Fonte";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Despesas = "Despesas";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string QuantidadeCasadaTermo = "QuantidadeCasadaTermo";
			 public const string QuantidadeCasadaExercicio = "QuantidadeCasadaExercicio";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string ResultadoExercicio = "ResultadoExercicio";
			 public const string Fonte = "Fonte";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GerOperacaoBolsaMetadata))
			{
				if(GerOperacaoBolsaMetadata.mapDelegates == null)
				{
					GerOperacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GerOperacaoBolsaMetadata.meta == null)
				{
					GerOperacaoBolsaMetadata.meta = new GerOperacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Despesas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("QuantidadeCasadaTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeCasadaExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CdAtivoBolsaOpcao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GerOperacaoBolsa";
				meta.Destination = "GerOperacaoBolsa";
				
				meta.spInsert = "proc_GerOperacaoBolsaInsert";				
				meta.spUpdate = "proc_GerOperacaoBolsaUpdate";		
				meta.spDelete = "proc_GerOperacaoBolsaDelete";
				meta.spLoadAll = "proc_GerOperacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_GerOperacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GerOperacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
