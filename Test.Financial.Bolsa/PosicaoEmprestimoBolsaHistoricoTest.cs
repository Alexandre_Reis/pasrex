﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Bolsa;
namespace Test.Financial.Bolsa {
    /// <summary>
    ///This is a test class for Financial.Bolsa.PosicaoEmprestimoBolsaHistorico and is intended
    ///to contain all Financial.Bolsa.PosicaoEmprestimoBolsaHistorico Unit Tests
    ///</summary>
    [TestClass()]
    public class PosicaoEmprestimoBolsaHistoricoTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        /// Utiliza PosicaoEmprestimoBolsaHistorico ids = 1,2
        ///</summary>
        [TestMethod()]
        public void CopiaPosicaoEmprestimoBolsaHistorico() {
            PosicaoEmprestimoBolsaHistorico target = new PosicaoEmprestimoBolsaHistorico();

            int idCliente = 2;
            DateTime data = new DateTime(2006, 1, 1);
            //
            //target.CopiaPosicaoEmprestimoBolsaHistorico(idCliente, data);
            //
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            posicaoEmprestimoBolsa.Query.Select(posicaoEmprestimoBolsa.Query.IdPosicao.Max());
            posicaoEmprestimoBolsa.Query.Load();
            int idPosicao1 = posicaoEmprestimoBolsa.IdPosicao.Value - 1;
            int idPosicao2 = posicaoEmprestimoBolsa.IdPosicao.Value;

            posicaoEmprestimoBolsa.QueryReset();
            posicaoEmprestimoBolsa.LoadByPrimaryKey(idPosicao1);
            //                                   
            Assert.AreEqual(1, posicaoEmprestimoBolsa.IdAgente, "IdAgente deveria ser 1");
            Assert.AreEqual(2, posicaoEmprestimoBolsa.IdCliente, "IdCliente deveria ser 2");
            Assert.AreEqual("PETR4", posicaoEmprestimoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser PETR4");
            Assert.AreEqual(1, posicaoEmprestimoBolsa.IdOperacao, "IdOperacao deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.Quantidade, "Quantidade deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.PUMercado, "PUMercado deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.PULiquidoOriginal, "PULiquidoOriginal deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.ValorCorrigidoJuros, "ValorCorrigidoJuros deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.ValorCorrigidoComissao, "ValorCorrigidoComissao deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.ValorCorrigidoCBLC, "ValorCorrigidoCBLC deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.ValorBase, "ValorBase deveria ser 1");
            Assert.AreEqual((Byte)1, posicaoEmprestimoBolsa.PontaEmprestimo, "PontaEmprestimo deveria ser 1");
            Assert.AreEqual(new DateTime(2006, 1, 1), posicaoEmprestimoBolsa.DataRegistro, "DataRegistro deveria ser {0}"+new DateTime(2006, 1, 1));
            Assert.AreEqual(new DateTime(2006, 1, 1), posicaoEmprestimoBolsa.DataVencimento, "DataVencimento deveria ser {0}" + new DateTime(2006, 1, 1));
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.TaxaOperacao, "TaxaOperacao deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.TaxaComissao, "TaxaComissao deveria ser 1");
            Assert.AreEqual(1, posicaoEmprestimoBolsa.NumeroContrato, "NumeroContrato deveria ser 1");
            Assert.AreEqual((Byte)1, posicaoEmprestimoBolsa.TipoEmprestimo, "TipoEmprestimo deveria ser 1");
            Assert.AreEqual(1M, posicaoEmprestimoBolsa.ValorMercado, "ValorMercado deveria ser 1");
                      
            // Deleta a base para não ficar lixo
            posicaoEmprestimoBolsa.MarkAsDeleted();
            posicaoEmprestimoBolsa.Save();
            //                            
            posicaoEmprestimoBolsa.QueryReset();
            posicaoEmprestimoBolsa.LoadByPrimaryKey(idPosicao2);
            Assert.AreEqual("VALE5", posicaoEmprestimoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser VALE5");
            // Deleta a base para não ficar lixo
            posicaoEmprestimoBolsa.MarkAsDeleted();
            posicaoEmprestimoBolsa.Save();
        }                     
    }
}
