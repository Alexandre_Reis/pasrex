﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.ContaCorrente;
using EntitySpaces.Interfaces;
using Financial.ContaCorrente.Enums;
namespace Test.Financial.Bolsa {
    /// <summary>
    ///This is a test class for Financial.Bolsa.OperacaoTermoBolsa and is intended
    ///to contain all Financial.Bolsa.OperacaoTermoBolsa Unit Tests
    ///</summary>
    [TestClass()]
    public class OperacaoTermoBolsaTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProcessaOperacaoTermo (int, DateTime)
        /// Utiliza IdOperacao = 169, 170
        /// ResultadoEsperado: 2 registros inseridos na tabela Liquidacao
        ///                    2 registros inseridos na tabela PosicaoTermoBolsa
        ///</summary>
        [TestMethod()]
        public void ProcessaOperacaoTermoTest() {
            using (esTransactionScope scope = new esTransactionScope()) {
                OperacaoTermoBolsa target = new OperacaoTermoBolsa();

                int idCliente = 30;
                DateTime data = new DateTime(2006, 2, 1);

                target.ProcessaOperacaoTermo(idCliente, data);
                //
                #region Confere liquidacao1
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.Query.Select(liquidacao.Query.IdLiquidacao.Max());
                liquidacao.Query.Load();
                int idLiquidacao1 = liquidacao.IdLiquidacao.Value - 1;
                int idLiquidacao2 = liquidacao.IdLiquidacao.Value;

                liquidacao.QueryReset();
                liquidacao.LoadByPrimaryKey(idLiquidacao1);
                //
                DateTime dataLancamento = new DateTime(2006, 2, 1);
                DateTime dataVencimento = new DateTime(2006, 3, 5);
                string descricao = "Compra Termo de 99 PETR4T01012007";
                decimal valor = -1000M;
                byte situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                int origem = (int)OrigemLancamentoLiquidacao.Bolsa.CompraTermo;
                int fonte = (int)FonteLancamentoLiquidacao.Interno;
                int idAgente = 1;
                idCliente = 30;
                int? idConta = null;
                int identificadorOrigem = 1;
                //
                Assert.AreEqual(dataLancamento, liquidacao.DataLancamento, "DataLancamento Liquidacao1 deveria ser: {0}", dataLancamento);
                Assert.AreEqual(dataVencimento, liquidacao.DataVencimento, "DataVencimento Liquidacao1 deveria ser: {0}", dataVencimento);
                Assert.AreEqual(descricao, liquidacao.Descricao, "Descricao Liquidacao1 deveria ser: {0}", descricao);
                Assert.AreEqual(valor, liquidacao.Valor, "Valor Liquidacao1 deveria ser: {0}", valor);
                Assert.AreEqual(situacao, liquidacao.Situacao, "Situacao Liquidacao1 deveria ser: {0}", situacao);
                Assert.AreEqual(origem, liquidacao.Origem, "Origem Liquidacao1 deveria ser: {0}", origem);
                Assert.AreEqual((byte)fonte, liquidacao.Fonte, "Fonte Liquidacao1 deveria ser: {0}", fonte);
                Assert.AreEqual(idAgente, liquidacao.IdAgente, "IdCorretora Liquidacao1 deveria ser: {0}", idAgente);
                Assert.AreEqual(idCliente, liquidacao.IdCliente, "IdCliente Liquidacao1 deveria ser: {0}", idCliente);
                Assert.IsNull(liquidacao.IdConta, "IdConta Liquidacao1 deveria ser: {0}", idConta);
                Assert.AreEqual(identificadorOrigem, liquidacao.IdentificadorOrigem, "IdentificadorOrigem Liquidacao1 deveria ser: {0}", identificadorOrigem);
                //
                #endregion

                #region Confere liquidacao2
                liquidacao.QueryReset();
                liquidacao.LoadByPrimaryKey(idLiquidacao2);
                //
                dataLancamento = new DateTime(2006, 2, 1);
                dataVencimento = new DateTime(2006, 3, 5);
                descricao = "Venda Termo de 99 PETR4T02012007";
                valor = 1000M;
                situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                origem = (int)OrigemLancamentoLiquidacao.Bolsa.VendaTermo;
                fonte = (int)FonteLancamentoLiquidacao.Interno;
                idAgente = 1;
                idCliente = 30;
                idConta = null;
                identificadorOrigem = 2;
                //
                Assert.AreEqual(liquidacao.DataLancamento, dataLancamento, "DataLancamento Liquidacao2 deveria ser: {0}", dataLancamento);
                Assert.AreEqual(liquidacao.DataVencimento, dataVencimento, "DataVencimento Liquidacao2 deveria ser: {0}", dataVencimento);
                Assert.AreEqual(liquidacao.Descricao, descricao, "Descricao Liquidacao2 deveria ser: {0}", descricao);
                Assert.AreEqual(liquidacao.Valor, valor, "Valor Liquidacao2 deveria ser: {0}", valor);
                Assert.AreEqual(liquidacao.Situacao, situacao, "Situacao Liquidacao2 deveria ser: {0}", situacao);
                Assert.AreEqual(liquidacao.Origem, origem, "Origem Liquidacao2 deveria ser: {0}", origem);
                Assert.AreEqual(liquidacao.Fonte, (byte)fonte, "Fonte Liquidacao2 deveria ser: {0}", fonte);
                Assert.AreEqual(liquidacao.IdAgente, idAgente, "IdCorretora Liquidacao2 deveria ser: {0}", idAgente);
                Assert.AreEqual(liquidacao.IdCliente, idCliente, "IdCliente Liquidacao2 deveria ser: {0}", idCliente);
                Assert.IsNull(liquidacao.IdConta, "IdConta Liquidacao2 deveria ser: {0}", idConta);
                Assert.AreEqual(liquidacao.IdentificadorOrigem, identificadorOrigem, "IdentificadorOrigem Liquidacao2 deveria ser: {0}", identificadorOrigem);
                //
                #endregion

                #region Confere PosicaoTermoBolsa1
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.IdPosicao.Max());
                posicaoTermoBolsa.Query.Load();
                int idPosicao1 = posicaoTermoBolsa.IdPosicao.Value - 1;
                int idPosicao2 = posicaoTermoBolsa.IdPosicao.Value;

                posicaoTermoBolsa.QueryReset();
                posicaoTermoBolsa.LoadByPrimaryKey(idPosicao1);
                //
                Console.WriteLine("posicaoTermoBolsa1");
                Assert.AreEqual(new DateTime(2006, 2, 1), posicaoTermoBolsa.DataOperacao, "DataOperacao deveria ser {0}" + new DateTime(2006, 2, 1));
                Assert.AreEqual(new DateTime(2006, 3, 5), posicaoTermoBolsa.DataVencimento, "DataVencimento deveria ser {0}" + new DateTime(2006, 3, 5));
                Assert.AreEqual(99M, posicaoTermoBolsa.Quantidade, "Quantidade deveria ser 99");
                Assert.AreEqual(99M, posicaoTermoBolsa.QuantidadeInicial, "QuantidadeInicial deveria ser 99");
                // Data entre 01/02/2006 - 05/03/2006  com 2 feriados = 20 dias
                Assert.AreEqual((Int16)20, posicaoTermoBolsa.PrazoTotal, "PrazoTotal deveria ser 20");
                Assert.AreEqual((Int16)20, posicaoTermoBolsa.PrazoDecorrer, "PrazoDecorrer deveria ser 20");
                Assert.AreEqual(0M, posicaoTermoBolsa.TaxaAno, "TaxaAno deveria ser 0");
                Assert.AreEqual(0M, posicaoTermoBolsa.TaxaMTM, "TaxaMTM deveria ser 0");                
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorTermo, "ValorTermo deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorTermoLiquido, "ValorTermoLiquido deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorMercado, "ValorMercado deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorCurva, "ValorCurva deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorCorrigido, "ValorCorrigido deveria ser 1000");
                Assert.AreEqual(0M, posicaoTermoBolsa.PUCustoLiquidoAcao, "PUCustoLiquidoAcao deveria ser 1");                
                Assert.AreEqual(1M, posicaoTermoBolsa.PUTermo, "PUTermo deveria ser 1");                
                Assert.AreEqual(1M, posicaoTermoBolsa.PUTermoLiquido, "PUTermoLiquido deveria ser 1");
                Assert.AreEqual(1M, posicaoTermoBolsa.PUMercado, "PUMercado deveria ser 1");                
                Assert.AreEqual(169, posicaoTermoBolsa.IdOperacao, "IdOperacao deveria ser 169");
                Assert.AreEqual(1, posicaoTermoBolsa.IdAgente, "IdAgente deveria ser 1");
                Assert.AreEqual(30, posicaoTermoBolsa.IdCliente, "IdCliente deveria ser 30");
                Assert.AreEqual("PETR4T01012007", posicaoTermoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser PETR4");
                #endregion

                #region Confere PosicaoTermoBolsa2
                posicaoTermoBolsa.QueryReset();
                posicaoTermoBolsa.LoadByPrimaryKey(idPosicao2);
                //        
                Console.WriteLine("posicaoTermoBolsa2");
                Assert.AreEqual(new DateTime(2006, 2, 1), posicaoTermoBolsa.DataOperacao, "DataOperacao deveria ser {0}" + new DateTime(2006, 2, 1));
                Assert.AreEqual(new DateTime(2006, 3, 5), posicaoTermoBolsa.DataVencimento, "DataVencimento deveria ser {0}" + new DateTime(2006, 3, 5));
                Assert.AreEqual(99M, posicaoTermoBolsa.Quantidade, "Quantidade deveria ser 99");
                Assert.AreEqual(99M, posicaoTermoBolsa.QuantidadeInicial, "QuantidadeInicial deveria ser 99");                
                Assert.AreEqual((Int16)20, posicaoTermoBolsa.PrazoTotal, "PrazoTotal deveria ser 20");
                Assert.AreEqual((Int16)20, posicaoTermoBolsa.PrazoDecorrer, "PrazoDecorrer deveria ser 20");
                // OBS: Dificil calcular
                Assert.AreEqual(25.2191M, posicaoTermoBolsa.TaxaAno, "TaxaAno deveria ser");                
                //
                Assert.AreEqual(0M, posicaoTermoBolsa.TaxaMTM, "TaxaMTM deveria ser");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorTermo, "ValorTermo deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorTermoLiquido, "ValorTermoLiquido deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorMercado, "ValorMercado deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorCurva, "ValorCurva deveria ser 1000");
                Assert.AreEqual(1000M, posicaoTermoBolsa.ValorCorrigido, "ValorCorrigido deveria ser 1000");
                //
                Assert.AreEqual(100M, posicaoTermoBolsa.PUCustoLiquidoAcao, "PUCustoLiquidoAcao deveria ser 10200");
                //
                Assert.AreEqual(1M, posicaoTermoBolsa.PUTermo, "PUTermo deveria ser 1");                
                Assert.AreEqual(107M, posicaoTermoBolsa.PUTermoLiquido, "PUTermoLiquido deveria ser 107");
                Assert.AreEqual(107M, posicaoTermoBolsa.PUMercado, "PUMercado deveria ser 107");
                Assert.AreEqual(170, posicaoTermoBolsa.IdOperacao, "IdOperacao deveria ser 170");
                Assert.AreEqual(1, posicaoTermoBolsa.IdAgente, "IdAgente deveria ser 1");
                Assert.AreEqual(30, posicaoTermoBolsa.IdCliente, "IdCliente deveria ser 30");
                Assert.AreEqual("PETR4T02012007", posicaoTermoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser PETR4T02012007");
                #endregion
            }
        }
    }
}
