﻿// ------------------------------------------------------------------------------
//<autogenerated>
//        This code was generated by Microsoft Visual Studio Team System 2005.
//
//        Changes to this file may cause incorrect behavior and will be lost if
//        the code is regenerated.
//</autogenerated>
//------------------------------------------------------------------------------
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Financial.Bolsa
{
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class BaseAccessor {
    
    protected Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject m_privateObject;
    
    protected BaseAccessor(object target, Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType type) {
        m_privateObject = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject(target, type);
    }
    
    protected BaseAccessor(Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType type) : 
            this(null, type) {
    }
    
    internal virtual object Target {
        get {
            return m_privateObject.Target;
        }
    }
    
    public override string ToString() {
        return this.Target.ToString();
    }
    
    public override bool Equals(object obj) {
        if (typeof(BaseAccessor).IsInstanceOfType(obj)) {
            obj = ((BaseAccessor)(obj)).Target;
        }
        return this.Target.Equals(obj);
    }
    
    public override int GetHashCode() {
        return this.Target.GetHashCode();
    }
}
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Financial_Bolsa_PosicaoBolsaAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType(typeof(global::Financial.Bolsa.PosicaoBolsa));
    
    internal Financial_Bolsa_PosicaoBolsaAccessor(global::Financial.Bolsa.PosicaoBolsa target) : 
            base(target, m_privateType) {
    }
    
    internal static global::log4net.ILog log {
        get {
            global::log4net.ILog ret = ((global::log4net.ILog)(m_privateType.GetStaticField("log")));
            return ret;
        }
        set {
            m_privateType.SetStaticField("log", value);
        }
    }
    
    internal global::Financial.Bolsa.TipoCarteiraBolsaCollection _TipoCarteiraBolsaCollection {
        get {
            global::Financial.Bolsa.TipoCarteiraBolsaCollection ret = ((global::Financial.Bolsa.TipoCarteiraBolsaCollection)(m_privateObject.GetField("_TipoCarteiraBolsaCollection")));
            return ret;
        }
        set {
            m_privateObject.SetField("_TipoCarteiraBolsaCollection", value);
        }
    }
    
    internal global::Financial.Bolsa.PosicaoBolsaDetalheCollection _PosicaoBolsaDetalheCollection {
        get {
            global::Financial.Bolsa.PosicaoBolsaDetalheCollection ret = ((global::Financial.Bolsa.PosicaoBolsaDetalheCollection)(m_privateObject.GetField("_PosicaoBolsaDetalheCollection")));
            return ret;
        }
        set {
            m_privateObject.SetField("_PosicaoBolsaDetalheCollection", value);
        }
    }
    
    internal global::Financial.Bolsa.PosicaoBolsaDetalheCollection _PosicaoBolsaDetalheCollectionByIdPosicao {
        get {
            global::Financial.Bolsa.PosicaoBolsaDetalheCollection ret = ((global::Financial.Bolsa.PosicaoBolsaDetalheCollection)(m_privateObject.GetField("_PosicaoBolsaDetalheCollectionByIdPosicao")));
            return ret;
        }
        set {
            m_privateObject.SetField("_PosicaoBolsaDetalheCollectionByIdPosicao", value);
        }
    }
    
    internal global::EntitySpaces.Interfaces.IMetadata Meta {
        get {
            global::EntitySpaces.Interfaces.IMetadata ret = ((global::EntitySpaces.Interfaces.IMetadata)(m_privateObject.GetProperty("Meta")));
            return ret;
        }
    }
    
    internal global::Financial.Bolsa.PosicaoBolsaQuery query {
        get {
            global::Financial.Bolsa.PosicaoBolsaQuery ret = ((global::Financial.Bolsa.PosicaoBolsaQuery)(m_privateObject.GetField("query")));
            return ret;
        }
        set {
            m_privateObject.SetField("query", value);
        }
    }
    
    internal void ExcluiPosicaoBolsaZerada(System.Collections.Generic.List<int> listaIdPosicao) {
        object[] args = new object[] {
                listaIdPosicao};
        m_privateObject.Invoke("ExcluiPosicaoBolsaZerada", new System.Type[] {
                    typeof(System.Collections.Generic.List<int>)}, args);
    }
    
    internal void ApplyPreSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPreSaveKeys", new System.Type[0], args);
    }
    
    internal void ApplyPostSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPostSaveKeys", new System.Type[0], args);
    }
    
    internal global::Financial.Bolsa.esPosicaoBolsaQuery GetDynamicQuery() {
        object[] args = new object[0];
        global::Financial.Bolsa.esPosicaoBolsaQuery ret = ((global::Financial.Bolsa.esPosicaoBolsaQuery)(m_privateObject.Invoke("GetDynamicQuery", new System.Type[0], args)));
        return ret;
    }
}
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Financial_Bolsa_EventoFisicoBolsaAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType(typeof(global::Financial.Bolsa.EventoFisicoBolsa));
    
    internal Financial_Bolsa_EventoFisicoBolsaAccessor(global::Financial.Bolsa.EventoFisicoBolsa target) : 
            base(target, m_privateType) {
    }
    
    internal static global::log4net.ILog log {
        get {
            global::log4net.ILog ret = ((global::log4net.ILog)(m_privateType.GetStaticField("log")));
            return ret;
        }
        set {
            m_privateType.SetStaticField("log", value);
        }
    }
    
    internal global::EntitySpaces.Interfaces.IMetadata Meta {
        get {
            global::EntitySpaces.Interfaces.IMetadata ret = ((global::EntitySpaces.Interfaces.IMetadata)(m_privateObject.GetProperty("Meta")));
            return ret;
        }
    }
    
    internal global::Financial.Bolsa.EventoFisicoBolsaQuery query {
        get {
            global::Financial.Bolsa.EventoFisicoBolsaQuery ret = ((global::Financial.Bolsa.EventoFisicoBolsaQuery)(m_privateObject.GetField("query")));
            return ret;
        }
        set {
            m_privateObject.SetField("query", value);
        }
    }
    
    internal void ProcessaEventoFisicoRetirada(int idCliente, global::System.DateTime data, global::Financial.Bolsa.EventoFisicoBolsa eventoFisico) {
        object[] args = new object[] {
                idCliente,
                data,
                eventoFisico};
        m_privateObject.Invoke("ProcessaEventoFisicoRetirada", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime),
                    typeof(global::Financial.Bolsa.EventoFisicoBolsa)}, args);
    }
    
    internal void ProcessaEventoFisicoDeposito(int idCliente, global::System.DateTime data, global::Financial.Bolsa.EventoFisicoBolsa eventoFisico) {
        object[] args = new object[] {
                idCliente,
                data,
                eventoFisico};
        m_privateObject.Invoke("ProcessaEventoFisicoDeposito", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime),
                    typeof(global::Financial.Bolsa.EventoFisicoBolsa)}, args);
    }
    
    internal void ApplyPreSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPreSaveKeys", new System.Type[0], args);
    }
    
    internal void ApplyPostSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPostSaveKeys", new System.Type[0], args);
    }
    
    internal global::Financial.Bolsa.esEventoFisicoBolsaQuery GetDynamicQuery() {
        object[] args = new object[0];
        global::Financial.Bolsa.esEventoFisicoBolsaQuery ret = ((global::Financial.Bolsa.esEventoFisicoBolsaQuery)(m_privateObject.Invoke("GetDynamicQuery", new System.Type[0], args)));
        return ret;
    }
}
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Financial_Util_TestaTruncateAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType("Financial.Util", "Financial.Util.TestaTruncate");
    
    internal Financial_Util_TestaTruncateAccessor(object target) : 
            base(target, m_privateType) {
    }
    
    internal void MathTest(decimal valor) {
        object[] args = new object[] {
                valor};
        m_privateObject.Invoke("MathTest", new System.Type[] {
                    typeof(decimal)}, args);
    }
    
    internal static object CreatePrivate() {
        Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject priv_obj = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject("Financial.Util", "Financial.Util.TestaTruncate", new object[0]);
        return priv_obj.Target;
    }
}
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Financial_Bolsa_OperacaoBolsaAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType(typeof(global::Financial.Bolsa.OperacaoBolsa));
    
    internal Financial_Bolsa_OperacaoBolsaAccessor(global::Financial.Bolsa.OperacaoBolsa target) : 
            base(target, m_privateType) {
    }
    
    internal static global::log4net.ILog log {
        get {
            global::log4net.ILog ret = ((global::log4net.ILog)(m_privateType.GetStaticField("log")));
            return ret;
        }
        set {
            m_privateType.SetStaticField("log", value);
        }
    }
    
    internal global::Financial.Bolsa.OperacaoTermoBolsaCollection _OperacaoTermoBolsaCollectionByIdOperacao {
        get {
            global::Financial.Bolsa.OperacaoTermoBolsaCollection ret = ((global::Financial.Bolsa.OperacaoTermoBolsaCollection)(m_privateObject.GetField("_OperacaoTermoBolsaCollectionByIdOperacao")));
            return ret;
        }
        set {
            m_privateObject.SetField("_OperacaoTermoBolsaCollectionByIdOperacao", value);
        }
    }
    
    internal global::Financial.Bolsa.PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByIdOperacao {
        get {
            global::Financial.Bolsa.PosicaoTermoBolsaCollection ret = ((global::Financial.Bolsa.PosicaoTermoBolsaCollection)(m_privateObject.GetField("_PosicaoTermoBolsaCollectionByIdOperacao")));
            return ret;
        }
        set {
            m_privateObject.SetField("_PosicaoTermoBolsaCollectionByIdOperacao", value);
        }
    }
    
    internal global::Financial.Bolsa.PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByIdOperacao {
        get {
            global::Financial.Bolsa.PosicaoTermoBolsaHistoricoCollection ret = ((global::Financial.Bolsa.PosicaoTermoBolsaHistoricoCollection)(m_privateObject.GetField("_PosicaoTermoBolsaHistoricoCollectionByIdOperacao")));
            return ret;
        }
        set {
            m_privateObject.SetField("_PosicaoTermoBolsaHistoricoCollectionByIdOperacao", value);
        }
    }
    
    internal global::EntitySpaces.Interfaces.IMetadata Meta {
        get {
            global::EntitySpaces.Interfaces.IMetadata ret = ((global::EntitySpaces.Interfaces.IMetadata)(m_privateObject.GetProperty("Meta")));
            return ret;
        }
    }
    
    internal global::Financial.Bolsa.OperacaoBolsaQuery query {
        get {
            global::Financial.Bolsa.OperacaoBolsaQuery ret = ((global::Financial.Bolsa.OperacaoBolsaQuery)(m_privateObject.GetField("query")));
            return ret;
        }
        set {
            m_privateObject.SetField("query", value);
        }
    }
    
    internal void CalculaCorretagem(int idCliente, global::System.DateTime data, int idAgente, global::Financial.Bolsa.OperacaoBolsaCollection operacaoBolsaCollection, decimal totalValor, decimal proporcionalValorAdicional) {
        object[] args = new object[] {
                idCliente,
                data,
                idAgente,
                operacaoBolsaCollection,
                totalValor,
                proporcionalValorAdicional};
        m_privateObject.Invoke("CalculaCorretagem", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime),
                    typeof(int),
                    typeof(global::Financial.Bolsa.OperacaoBolsaCollection),
                    typeof(decimal),
                    typeof(decimal)}, args);
    }
    
    internal void CalculaTaxasOpcoesDaytrade(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasOpcoesDaytrade", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasOpcoesFinal(int tipoCliente, int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                tipoCliente,
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasOpcoesFinal", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasFuturoDaytrade(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasFuturoDaytrade", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasFuturoFinal(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasFuturoFinal", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasTermo(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasTermo", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasExercicioOPC(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasExercicioOPC", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasMercadoVistaFinal(int tipoCliente, int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                tipoCliente,
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasMercadoVistaFinal", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxasMercadoVistaDaytrade(int idCliente, int idAgenteCorretora, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                idAgenteCorretora,
                data};
        m_privateObject.Invoke("CalculaTaxasMercadoVistaDaytrade", new System.Type[] {
                    typeof(int),
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CalculaTaxas(global::Financial.Bolsa.OperacaoBolsaCollection operacaoBolsaCollection, global::System.DateTime data, int tipoTaxa) {
        object[] args = new object[] {
                operacaoBolsaCollection,
                data,
                tipoTaxa};
        m_privateObject.Invoke("CalculaTaxas", new System.Type[] {
                    typeof(global::Financial.Bolsa.OperacaoBolsaCollection),
                    typeof(global::System.DateTime),
                    typeof(int)}, args);
    }
    
    internal void CompoeCustoOpcao(global::Financial.Bolsa.OperacaoBolsa operacaoBolsa) {
        object[] args = new object[] {
                operacaoBolsa};
        m_privateObject.Invoke("CompoeCustoOpcao", new System.Type[] {
                    typeof(global::Financial.Bolsa.OperacaoBolsa)}, args);
    }
    
    internal void ProcessaResultadoVencimentoExercicio(global::Financial.Bolsa.OperacaoBolsa operacaoBolsaCompra, global::Financial.Bolsa.OperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaExercicio) {
        object[] args = new object[] {
                operacaoBolsaCompra,
                operacaoBolsaVenda,
                quantidadeCasadaExercicio};
        m_privateObject.Invoke("ProcessaResultadoVencimentoExercicio", new System.Type[] {
                    typeof(global::Financial.Bolsa.OperacaoBolsa),
                    typeof(global::Financial.Bolsa.OperacaoBolsa),
                    typeof(decimal)}, args);
    }
    
    internal void ProcessaResultadoVencimentoTermo(global::Financial.Bolsa.OperacaoBolsa operacaoBolsaCompra, global::Financial.Bolsa.OperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaTermo) {
        object[] args = new object[] {
                operacaoBolsaCompra,
                operacaoBolsaVenda,
                quantidadeCasadaTermo};
        m_privateObject.Invoke("ProcessaResultadoVencimentoTermo", new System.Type[] {
                    typeof(global::Financial.Bolsa.OperacaoBolsa),
                    typeof(global::Financial.Bolsa.OperacaoBolsa),
                    typeof(decimal)}, args);
    }
    
    internal void CasaVencimentoTermo(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("CasaVencimentoTermo", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void CasaVencimentoExercicio(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("CasaVencimentoExercicio", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void LancaCCOperacaoAnalitico(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("LancaCCOperacaoAnalitico", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void LancaCCOperacaoConsolidado(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("LancaCCOperacaoConsolidado", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void LancaCCOperacaoDespesas(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("LancaCCOperacaoDespesas", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void ApplyPreSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPreSaveKeys", new System.Type[0], args);
    }
    
    internal void ApplyPostSaveKeys() {
        object[] args = new object[0];
        m_privateObject.Invoke("ApplyPostSaveKeys", new System.Type[0], args);
    }
    
    internal global::Financial.Bolsa.esOperacaoBolsaQuery GetDynamicQuery() {
        object[] args = new object[0];
        global::Financial.Bolsa.esOperacaoBolsaQuery ret = ((global::Financial.Bolsa.esOperacaoBolsaQuery)(m_privateObject.Invoke("GetDynamicQuery", new System.Type[0], args)));
        return ret;
    }
}
[System.Diagnostics.DebuggerStepThrough()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TestTools.UnitTestGeneration", "1.0.0.0")]
internal class Financial_Bolsa_Controller_ControllerBolsaAccessor : BaseAccessor {
    
    protected static Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType m_privateType = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType("Financial.Bolsa", "Financial.Bolsa.Controller.ControllerBolsa");
    
    internal Financial_Bolsa_Controller_ControllerBolsaAccessor(object target) : 
            base(target, m_privateType) {
    }
    
    internal static global::log4net.ILog log {
        get {
            global::log4net.ILog ret = ((global::log4net.ILog)(m_privateType.GetStaticField("log")));
            return ret;
        }
        set {
            m_privateType.SetStaticField("log", value);
        }
    }
    
    internal int tipoReprocessamento {
        get {
            int ret = ((int)(m_privateObject.GetField("tipoReprocessamento")));
            return ret;
        }
        set {
            m_privateObject.SetField("tipoReprocessamento", value);
        }
    }
    
    internal int TipoReprocessamento {
        get {
            int ret = ((int)(m_privateObject.GetProperty("TipoReprocessamento")));
            return ret;
        }
        set {
            m_privateObject.SetProperty("TipoReprocessamento", value);
        }
    }
    
    internal static object CreatePrivate(int tipoReprocessamento) {
        object[] args = new object[] {
                tipoReprocessamento};
        Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject priv_obj = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject("Financial.Bolsa", "Financial.Bolsa.Controller.ControllerBolsa", new System.Type[] {
                    typeof(int)}, args);
        return priv_obj.Target;
    }
    
    internal void ReprocessaPosicao(int idCliente, global::System.DateTime data, int tipoReprocessamento) {
        object[] args = new object[] {
                idCliente,
                data,
                tipoReprocessamento};
        m_privateObject.Invoke("ReprocessaPosicao", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime),
                    typeof(int)}, args);
    }
    
    internal bool IsTipoReprocessamentoFechamento() {
        object[] args = new object[0];
        bool ret = ((bool)(m_privateObject.Invoke("IsTipoReprocessamentoFechamento", new System.Type[0], args)));
        return ret;
    }
    
    internal bool IsTipoReprocessamentoAbertura() {
        object[] args = new object[0];
        bool ret = ((bool)(m_privateObject.Invoke("IsTipoReprocessamentoAbertura", new System.Type[0], args)));
        return ret;
    }
    
    internal void ExecutaAbertura(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("ExecutaAbertura", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void ExecutaFechamento(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("ExecutaFechamento", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
    
    internal void ExecutaComponente(int idCliente, global::System.DateTime data) {
        object[] args = new object[] {
                idCliente,
                data};
        m_privateObject.Invoke("ExecutaComponente", new System.Type[] {
                    typeof(int),
                    typeof(global::System.DateTime)}, args);
    }
}
}
