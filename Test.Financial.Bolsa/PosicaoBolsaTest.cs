﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.Bolsa;
using System;
using EntitySpaces.Interfaces;
using Financial.Bolsa.Enums;
using System.Configuration;
using System.Text;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Test.Financial.Bolsa {
    /// <summary>
    ///This is a test class for Financial.Bolsa.PosicaoBolsa and is intended
    ///to contain all Financial.Bolsa.PosicaoBolsa Unit Tests
    ///</summary>
    [TestClass()]
    public class PosicaoBolsaTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Busca Saldo da posicao quando o cliente, corretora e ativo existem
        ///</summary>
        [TestMethod()]
        public void BuscaPosicaoExisteTest() {
            PosicaoBolsa target = new PosicaoBolsa();

            int idCliente = 1;
            int idCorretora = 1;
            string cdAtivo = "PETR4";

            // resultadoEsperado: PosicaoBolsa com id=1
            bool resultadoEsperado = true;
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.LoadByPrimaryKey(1);
            // 

            bool actual = target.BuscaPosicaoBolsa(idCliente, idCorretora, cdAtivo);
            //
            Assert.AreEqual(resultadoEsperado, actual, "PosicaoBolsa Errada");
            Assert.AreEqual(posicaoBolsa.IdPosicao, target.IdPosicao, "PosicaoBolsa.IdPosicao deveria ser = 1");
            Assert.AreEqual(posicaoBolsa.Quantidade, target.Quantidade, "PosicaoBolsa.Quantidade deveria ser = 100");
        }

        /// <summary>
        ///Busca Saldo da posicao quando a combinação cliente, corretora e ativo não existe
        ///</summary>
        [TestMethod()]
        public void BuscaPosicaoNaoExisteTest() {
            PosicaoBolsa target = new PosicaoBolsa();
            int idCliente; int idCorretora; string cdAtivo;

            // resultadoEsperado: PosicaoBolsa com id=1
            bool resultadoEsperado = false;
            // 
            // idCliente não Existe
            idCliente = 1054; idCorretora = 1; cdAtivo = "PETR4";
            bool actual = target.BuscaPosicaoBolsa(idCliente, idCorretora, cdAtivo);
            Assert.AreEqual(resultadoEsperado, actual, "PosicaoBolsa Errada");

            // idCorretora não Existe
            idCliente = 1; idCorretora = 5433; cdAtivo = "PETR4";
            actual = target.BuscaPosicaoBolsa(idCliente, idCorretora, cdAtivo);
            Assert.AreEqual(resultadoEsperado, actual, "PosicaoBolsa Errada");

            // cdAtivo não Existe
            idCliente = 1; idCorretora = 1; cdAtivo = "AFJM4";
            actual = target.BuscaPosicaoBolsa(idCliente, idCorretora, cdAtivo);
            Assert.AreEqual(resultadoEsperado, actual, "PosicaoBolsa Errada");
        }

        /// <summary>
        /// Entrada: 
        ///    - idCliente = 1
        ///    - data: 01/01/2006
        /// ResultadoEsperado: 2 posicoes deletadas
        ///</summary>
        [TestMethod()]
        public void BaixaPosicaoVencimentoTest() {
            /* - Deprecated
                        BaixaPosicaoVencimentoInitialize();
                        PosicaoBolsa target = new PosicaoBolsa();

                        int idCliente = 1;
                        DateTime data = new DateTime(2006, 1, 1);
                        //
                        target.BaixaPosicaoVencimento(idCliente, data);

                        PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                        bool condicao = posicaoBolsaCollection.BuscaPosicaoBolsa(idCliente, data);
                        // Se posicao foi deletada então teste foi correto
                        Assert.IsFalse(condicao);
             */
        }

        /// <summary>
        /// 
        ///</summary>
        [DeploymentItem("Financial.Bolsa.dll")]
        [TestMethod()]
        public void ExcluiPosicaoBolsaZeradaTest() {
            ExcluiPosicaoBolsaZeradaInitialize();
            PosicaoBolsa target = new PosicaoBolsa();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            Test.Financial.Bolsa.Financial_Bolsa_PosicaoBolsaAccessor accessor = new Test.Financial.Bolsa.Financial_Bolsa_PosicaoBolsaAccessor(target);

            // seleciona a key que foi criada - maxID
            posicaoBolsa.Query.Select(posicaoBolsa.Query.IdPosicao.Max());
            posicaoBolsa.Query.Load();

            int idPosicao1 = posicaoBolsa.IdPosicao.Value;
            int idPosicao2 = idPosicao1 - 1;
            System.Collections.Generic.List<int> listaIdPosicao = new System.Collections.Generic.List<int>();
            // Adiciona na lista as duas IdPosicao Zeradas
            listaIdPosicao.Add(idPosicao1);
            listaIdPosicao.Add(idPosicao2);
            //
            accessor.ExcluiPosicaoBolsaZerada(listaIdPosicao);

            // confere resultado
            posicaoBolsa.QueryReset();
            posicaoBolsa.Query
                .Select(posicaoBolsa.Query.IdPosicao)
                .Where(posicaoBolsa.Query.IdPosicao == idPosicao1);
            Assert.IsFalse(posicaoBolsa.Query.Load());
            //
            posicaoBolsa.QueryReset();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.IdPosicao).Where(posicaoBolsa.Query.IdPosicao == idPosicao2);
            Assert.IsFalse(posicaoBolsa.Query.Load());
        }

        //
        //Inicialização com os dados para Baixar a posicão
        /// Deprecated
        private void BaixaPosicaoVencimentoInitialize() {
            // insere duas opcoes com vencimento em 01/01/2006
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBolsa posicaoBolsaInsereDados = new PosicaoBolsa();

            // Primeira posicao
            posicaoBolsaInsereDados = posicaoBolsaCollection.AddNew();
            posicaoBolsaInsereDados.IdCliente = 1;
            posicaoBolsaInsereDados.CdAtivoBolsa = "AMBVL42";
            posicaoBolsaInsereDados.DataVencimento = new DateTime(2006, 1, 1);
            // Valores que não fazem diferença
            posicaoBolsaInsereDados.IdAgente = 1;
            posicaoBolsaInsereDados.Quantidade = 1;
            posicaoBolsaInsereDados.PUCustoLiquido = 1;
            posicaoBolsaInsereDados.ResultadoRealizar = 0;
            posicaoBolsaInsereDados.QuantidadeInicial = 0;
            posicaoBolsaInsereDados.ValorCustoLiquido = 0;
            posicaoBolsaInsereDados.ValorMercado = 0;
            posicaoBolsaInsereDados.PUCusto = 0;
            posicaoBolsaInsereDados.PUMercado = 0;
            posicaoBolsaInsereDados.TipoMercado = "VIS";

            // Segunda Posicao
            posicaoBolsaInsereDados = posicaoBolsaCollection.AddNew();
            posicaoBolsaInsereDados.IdCliente = 1;
            posicaoBolsaInsereDados.CdAtivoBolsa = "PETRF44";
            posicaoBolsaInsereDados.DataVencimento = new DateTime(2006, 1, 1);
            // Valores que não fazem diferença
            posicaoBolsaInsereDados.IdAgente = 1;
            posicaoBolsaInsereDados.Quantidade = 1;
            posicaoBolsaInsereDados.PUCustoLiquido = 1;
            posicaoBolsaInsereDados.ResultadoRealizar = 0;
            posicaoBolsaInsereDados.ValorCustoLiquido = 0;
            posicaoBolsaInsereDados.ValorMercado = 0;
            posicaoBolsaInsereDados.PUCusto = 0;
            posicaoBolsaInsereDados.PUMercado = 0;
            posicaoBolsaInsereDados.TipoMercado = "VIS";

            // Salva a coleção
            posicaoBolsaCollection.Save();
        }

        //
        //Inicialização de posições zeradas
        ///        
        private void ExcluiPosicaoBolsaZeradaInitialize() {
            // insere duas posições zeradas (quantidade = 0)
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBolsa posicaoBolsaInsereDados = new PosicaoBolsa();

            // Primeira posicao
            posicaoBolsaInsereDados = posicaoBolsaCollection.AddNew();
            posicaoBolsaInsereDados.Quantidade = 0;// Posição zerada
            posicaoBolsaInsereDados.IdCliente = 1;
            posicaoBolsaInsereDados.CdAtivoBolsa = "GUAR4";
            posicaoBolsaInsereDados.DataVencimento = new DateTime(2006, 1, 1);
            posicaoBolsaInsereDados.IdAgente = 1;
            posicaoBolsaInsereDados.PUCustoLiquido = 1;
            posicaoBolsaInsereDados.ResultadoRealizar = 0;
            posicaoBolsaInsereDados.QuantidadeInicial = 0;
            posicaoBolsaInsereDados.ValorCustoLiquido = 0;
            posicaoBolsaInsereDados.ValorMercado = 0;
            posicaoBolsaInsereDados.PUCusto = 0;
            posicaoBolsaInsereDados.PUMercado = 0;
            posicaoBolsaInsereDados.TipoMercado = "VIS";

            // Segunda Posicao
            posicaoBolsaInsereDados = posicaoBolsaCollection.AddNew();
            posicaoBolsaInsereDados.IdCliente = 1;
            posicaoBolsaInsereDados.Quantidade = 0; // posição zerada
            posicaoBolsaInsereDados.CdAtivoBolsa = "ETER3";
            posicaoBolsaInsereDados.DataVencimento = new DateTime(2006, 1, 1);
            posicaoBolsaInsereDados.IdAgente = 1;
            posicaoBolsaInsereDados.PUCustoLiquido = 1;
            posicaoBolsaInsereDados.ResultadoRealizar = 0;
            posicaoBolsaInsereDados.QuantidadeInicial = 0;
            posicaoBolsaInsereDados.ValorCustoLiquido = 0;
            posicaoBolsaInsereDados.ValorMercado = 0;
            posicaoBolsaInsereDados.PUCusto = 0;
            posicaoBolsaInsereDados.PUMercado = 0;
            posicaoBolsaInsereDados.TipoMercado = "VIS";

            // Salva a coleção
            posicaoBolsaCollection.Save();
        }

        //
        // Cria uma posição zerada
        ///        
        private void AtualizaValoresInitialize() {
            // insere uma posicao zerada (quantidade = 0)            
            PosicaoBolsa posicaoBolsaInsereDados = new PosicaoBolsa();
            posicaoBolsaInsereDados.AddNew();
            posicaoBolsaInsereDados.Quantidade = 0;// Posição zerada
            posicaoBolsaInsereDados.IdCliente = 4;
            posicaoBolsaInsereDados.CdAtivoBolsa = "GUAR4";
            posicaoBolsaInsereDados.DataVencimento = new DateTime(2006, 1, 1);
            posicaoBolsaInsereDados.IdAgente = 1;
            posicaoBolsaInsereDados.PUCustoLiquido = 1;
            posicaoBolsaInsereDados.ResultadoRealizar = 0;
            posicaoBolsaInsereDados.QuantidadeInicial = 0;
            posicaoBolsaInsereDados.ValorCustoLiquido = 0;
            posicaoBolsaInsereDados.ValorMercado = 0;
            posicaoBolsaInsereDados.PUCusto = 0;
            posicaoBolsaInsereDados.PUMercado = 0;
            posicaoBolsaInsereDados.TipoMercado = "VIS";
            posicaoBolsaInsereDados.Save();
        }

        /// <summary>
        ///  Entradas: idCliente = 4
        ///  Data da cotação: 01/01/2003
        ///  Usa PosicaoBolsa id = 5
        ///  Saida:   idPosicao = 5
        ///           valorMercado = 100
        ///           valorCustoLiquido = 100
        ///           resultadoRealizar = 0
        /// 
        ///</summary>
        [TestMethod()]
        public void AtualizaValoresPosicaoBolsaTest() {
            //using (esTransactionScope scope = new esTransactionScope())
            //{
            // Cria uma posicao zerada (quantidade = 0)
            //AtualizaValoresInitialize();
            PosicaoBolsa target = new PosicaoBolsa();
            ////
            int idCliente = 13712;
            DateTime data = new DateTime(2007, 1, 10);
            ////

            //// Conferencias
            //PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            //decimal valorMercado = 100;
            //decimal valorCustoLiquido = 100;
            //decimal resultadoRealizar = 0;
            //decimal puMercado = 1;
            ////
            //// seleciona a key que foi criada - maxID
            //PosicaoBolsa posicaoBolsaAux = new PosicaoBolsa();
            //posicaoBolsaAux.Query.Select(posicaoBolsaAux.Query.IdPosicao.Max());
            //posicaoBolsaAux.Query.Load();
            //int idPosicao = posicaoBolsaAux.IdPosicao.Value;
            //
            target.AtualizaValores(idCliente, data);
            //
            //posicaoBolsa.LoadByPrimaryKey(5);
            //Assert.AreEqual(puMercado, posicaoBolsa.PUMercado, "posicaoBolsa.PUMercado deveria ser: " + puMercado);
            //Assert.AreEqual(valorMercado, posicaoBolsa.ValorMercado, "posicaoBolsa.ValorMercado deveria ser: " + valorMercado);
            //Assert.AreEqual(valorCustoLiquido, posicaoBolsa.ValorCustoLiquido, "posicaoBolsa.ValorCustoLiquido deveria ser: " + valorCustoLiquido);
            //Assert.AreEqual(resultadoRealizar, posicaoBolsa.ResultadoRealizar, "posicaoBolsa.ResultadoRealizar deveria ser: " + resultadoRealizar);
            //// verifica se excluiu posicao
            //posicaoBolsaAux.QueryReset();
            //posicaoBolsaAux.LoadByPrimaryKey(idPosicao);
            //Assert.IsFalse(posicaoBolsaAux.Query.Load());

            // Volta os valores anteriores
            /*posicaoBolsa.ValorMercado = 0;
            posicaoBolsa.ValorCustoLiquido = 0;
            posicaoBolsa.ResultadoRealizar = 0;
            posicaoBolsa.Save(); */
            //}
        }

        /// <summary>
        /// Utiliza PosicaoBolsa ids = 12,13
        /// idCliente = 8
        ///</summary>
        [TestMethod()]
        public void GeraBackupTest() {
            PosicaoBolsa target = new PosicaoBolsa();

            int idCliente = 13712;
            DateTime data = new DateTime(2007, 05, 28);
            //
            target.GeraBackup(idCliente, data);
            //
            //PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            //posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.IdPosicao.Max());
            //posicaoBolsaHistorico.Query.Load();
            //int idPosicao1 = posicaoBolsaHistorico.IdPosicao.Value - 1;
            //int idPosicao2 = posicaoBolsaHistorico.IdPosicao.Value;

            //posicaoBolsaHistorico.QueryReset();
            //posicaoBolsaHistorico.LoadByPrimaryKey(idPosicao1, data);
            ////
            //Assert.AreEqual(1M, posicaoBolsaHistorico.PUCusto, "PUCusto deveria ser 1");
            //Assert.AreEqual(1M, posicaoBolsaHistorico.PUCustoLiquido, "PUCustoLiquido deveria ser 1");
            //Assert.AreEqual(1M, posicaoBolsaHistorico.ValorCustoLiquido, "ValorCustoLiquido deveria ser 1");
            //Assert.AreEqual(1M, posicaoBolsaHistorico.Quantidade, "Quantidade deveria ser 1");
            //Assert.AreEqual(1, posicaoBolsaHistorico.IdAgente, "IdAgente deveria ser 1");
            //Assert.AreEqual(8, posicaoBolsaHistorico.IdCliente, "IdCliente deveria ser 1");
            //Assert.AreEqual("PETR4", posicaoBolsaHistorico.CdAtivoBolsa, "CdAtivoBolsa deveria ser PETR4");
            //Assert.AreEqual(data, posicaoBolsaHistorico.DataHistorico, "Data Historico deveria ser {0}", data);
            //// Deleta a base para não ficar lixo
            //posicaoBolsaHistorico.MarkAsDeleted();
            //posicaoBolsaHistorico.Save();
            ////                            
            //posicaoBolsaHistorico.QueryReset();
            //posicaoBolsaHistorico.LoadByPrimaryKey(idPosicao2, data);
            //Assert.AreEqual("VALE5", posicaoBolsaHistorico.CdAtivoBolsa, "CdAtivoBolsa deveria ser VALE5");
            //Assert.AreEqual(data, posicaoBolsaHistorico.DataHistorico, "Data Historico deveria ser {0}", data);
            //// Deleta a base para não ficar lixo
            //posicaoBolsaHistorico.MarkAsDeleted();
            //posicaoBolsaHistorico.Save();
        }

        /// <summary>
        /// Entrada: idCliente = 16
        ///          Data = 01/01/2006
        /// Utiliza IdPosicao = 18, 19        
        /// Saida: Inserção de duas Operacões
        ///</summary>
        [TestMethod()]
        public void ProcessaVencimentoOpcaoTest() {
            PosicaoBolsa target = new PosicaoBolsa();

            int idCliente = 13712;
            DateTime data = new DateTime(2007, 2, 12);
            //
            target.ProcessaVencimentoOpcao(idCliente, data);
            //
            //OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            //operacaoBolsa.Query.Select(operacaoBolsa.Query.IdOperacao.Max());
            //operacaoBolsa.Query.Load();
            //int idOperacao1 = operacaoBolsa.IdOperacao.Value - 1;
            //int idOperacao2 = operacaoBolsa.IdOperacao.Value;
            ////
            //operacaoBolsa.QueryReset();
            //operacaoBolsa.LoadByPrimaryKey(idOperacao1);
            ////            
            //#region Confere Operacao1
            ////int idConta = 2;                            
            //string tipoMercado = TipoMercadoBolsa.OpcaoCompra;
            //string tipoOperacao = TipoOperacaoBolsa.Retirada;
            //decimal Pu = 1;
            //decimal quantidade = 900;
            //string cdAtivoBolsa = "PETRF44";
            //int idAgenteCorretora = 1;
            //decimal puLiquido = 2;
            //byte origem = (byte)OrigemOperacaoBolsa.VencimentoOpcao;
            //int idClienteEsperado = 16;
            //decimal valorLiquido = 1800;
            //decimal resultadoRealizado = -1800;
            ////
            ////Assert.AreEqual(idConta, operacaoBolsa.IdConta,"Conta deveria ser {0}",idConta);
            //Assert.AreEqual(tipoMercado, operacaoBolsa.TipoMercado, "TipoMercado deveria ser {0}", tipoMercado);
            //Assert.AreEqual(tipoOperacao, operacaoBolsa.TipoOperacao, "TipoOperacao deveria ser {0}", tipoOperacao);
            //Assert.AreEqual(Pu, operacaoBolsa.Pu, "Pu deveria ser {0}", Pu);
            //Assert.AreEqual(quantidade, operacaoBolsa.Quantidade, "Quantidade deveria ser {0}", quantidade);
            //Assert.AreEqual(cdAtivoBolsa, operacaoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser {0}", cdAtivoBolsa);
            //Assert.AreEqual(idAgenteCorretora, operacaoBolsa.IdAgenteCorretora, "idAgenteCorretora deveria ser {0}", idAgenteCorretora);
            //Assert.AreEqual(puLiquido, operacaoBolsa.PULiquido, "PULiquido deveria ser {0}", puLiquido);
            //Assert.AreEqual(origem, operacaoBolsa.Origem, "Origem deveria ser {0}", origem);
            //Assert.AreEqual(idClienteEsperado, operacaoBolsa.IdCliente, "IdCliente deveria ser {0}", idClienteEsperado);
            //Assert.AreEqual(valorLiquido, operacaoBolsa.ValorLiquido, "ValorLiquido deveria ser {0}", valorLiquido);
            //Assert.AreEqual(resultadoRealizado, operacaoBolsa.ResultadoRealizado, "ResultadoRealizado deveria ser {0}", resultadoRealizado);

            //// Apaga Operacao para não ficar lixo
            //operacaoBolsa.MarkAsDeleted();
            //operacaoBolsa.Save();
            ////
            //#endregion
            ////
            //operacaoBolsa.QueryReset();
            //operacaoBolsa.LoadByPrimaryKey(idOperacao2);
            ////
            //#region Confere Operacao2
            ////idConta = 2;  
            //tipoMercado = TipoMercadoBolsa.OpcaoCompra;
            //tipoOperacao = TipoOperacaoBolsa.Deposito;
            //Pu = 1;
            //quantidade = -100;
            //cdAtivoBolsa = "PETRF46";
            //idAgenteCorretora = 1;
            //puLiquido = 2;
            //origem = (byte)OrigemOperacaoBolsa.VencimentoOpcao;
            //idCliente = 16;
            //valorLiquido = -200;
            //resultadoRealizado = 200;
            ////
            ////Assert.AreEqual(idConta, operacaoBolsa.IdConta,"Conta deveria ser {0}",idConta);
            //Assert.AreEqual(tipoMercado, operacaoBolsa.TipoMercado, "TipoMercado deveria ser {0}", tipoMercado);
            //Assert.AreEqual(Pu, operacaoBolsa.Pu, "Pu deveria ser {0}", Pu);
            //Assert.AreEqual(quantidade, operacaoBolsa.Quantidade, "Quantidade deveria ser {0}", quantidade);
            //Assert.AreEqual(cdAtivoBolsa, operacaoBolsa.CdAtivoBolsa, "CdAtivoBolsa deveria ser {0}", cdAtivoBolsa);
            //Assert.AreEqual(idAgenteCorretora, operacaoBolsa.IdAgenteCorretora, "idAgenteCorretora deveria ser {0}", idAgenteCorretora);
            //Assert.AreEqual(puLiquido, operacaoBolsa.PULiquido, "PULiquido deveria ser {0}", puLiquido);
            //Assert.AreEqual(origem, operacaoBolsa.Origem, "Origem deveria ser {0}", origem);
            //Assert.AreEqual(idCliente, operacaoBolsa.IdCliente, "IdCliente deveria ser {0}", idCliente);
            //Assert.AreEqual(valorLiquido, operacaoBolsa.ValorLiquido, "ValorLiquido deveria ser {0}", valorLiquido);
            //Assert.AreEqual(resultadoRealizado, operacaoBolsa.ResultadoRealizado, "ResultadoRealizado deveria ser {0}", resultadoRealizado);
            //// Apaga Operacao para não ficar lixo
            //operacaoBolsa.MarkAsDeleted();
            //operacaoBolsa.Save();
            //#endregion
        }

        /// <summary>
        ///A test for CarregaPosicaoCsgd (string, int, DateTime)
        ///</summary>
        [TestMethod()]
        public void CarregaPosicaoCsgdTest() {
            //using (esTransactionScope scope = new esTransactionScope())
            //{
            PosicaoBolsa target = new PosicaoBolsa();
            //
            DateTime data = new DateTime(2007, 1, 10);
            //
            string diretorioBase = ConfigurationManager.AppSettings["DiretorioBaseTeste"].ToString();
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(diretorioBase).Append("CSGD.txt");
            //
            //int idCliente = 516679;
            //
            // Pega o IdPosicao da proxima posição antes de inserir
            // Simula a insert de 1 registro para pegar o O novo ID
            //PosicaoBolsa posicaoAux = new PosicaoBolsa();
            //posicaoAux.AddNew();
            //posicaoAux.TipoMercado = "VIS";
            //posicaoAux.PUMercado = 1;
            //posicaoAux.PUCusto = 1;
            //posicaoAux.PUCustoLiquido = 1;
            //posicaoAux.ValorMercado = 1;
            //posicaoAux.ValorCustoLiquido = 1;
            //posicaoAux.Quantidade = 1;
            //posicaoAux.QuantidadeInicial = 1;
            //posicaoAux.QuantidadeAbertura = 1;
            //posicaoAux.ResultadoRealizar = 1;
            //posicaoAux.IdAgente = 1;
            //posicaoAux.IdCliente = 1;
            //posicaoAux.CdAtivoBolsa = "PETR4";
            //posicaoAux.Save();
            ////
            //PosicaoBolsa posicaoAux1 = new PosicaoBolsa();
            //posicaoAux1.Query.Select(posicaoAux1.Query.IdPosicao.Max());
            //posicaoAux1.Query.Load();
            //int idPosicao = posicaoAux1.IdPosicao.Value + 1;
            //
            target.CarregaPosicaoCsgd(nomeArquivo.ToString(), 20849, data);

            //// Apaga o resgistro inserido
            //posicaoAux.MarkAsDeleted();
            //// Conferir um registro insert e 1 registro Update
            //// Confere Insert
            //#region Insert
            //posicaoAux = new PosicaoBolsa();
            //posicaoAux.LoadByPrimaryKey(idPosicao);
            //Assert.AreEqual("VIS", posicaoAux.TipoMercado, "TipoMercado incorreto");
            //Assert.IsNull(posicaoAux.DataVencimento, "Data Vencimento incorreta");
            //Assert.AreEqual(0M, posicaoAux.ResultadoRealizar, "ResultadoRealizar incorreto");
            //Assert.AreEqual(0000000000000083.000M, posicaoAux.Quantidade, "Quantidade incorreta");
            //Assert.AreEqual(0000000000000083.000M, posicaoAux.QuantidadeInicial, "QuantidadeInicial incorreta");
            //Assert.AreEqual(0000000000000083.000M, posicaoAux.QuantidadeAbertura, "QuantidadeAbertura incorreta");
            //Assert.AreEqual(0516679, posicaoAux.IdCliente, "IdCliente incorreto");
            //Assert.AreEqual("ALLL11", posicaoAux.CdAtivoBolsa, "CdAtivoBolsa incorreto");
            //Assert.AreEqual(63, posicaoAux.IdAgente, "IdAgente incorreto");
            ////
            //Assert.AreEqual(22.02M, posicaoAux.PUMercado, "PUMercado incorreto");
            //Assert.AreEqual(22.02M, posicaoAux.PUCusto, "PUCusto incorreto");
            //Assert.AreEqual(22.02M, posicaoAux.PUCustoLiquido, "PUCustoLiquido incorreto");
            ////
            //Assert.AreEqual(22.02M * 83M, posicaoAux.ValorMercado, "ValorMercado incorreto");
            //Assert.AreEqual(22.02M * 83M, posicaoAux.ValorCustoLiquido, "ValorCustoLiquido incorreto");
            //#endregion

            //// Confere Update
            //#region Update
            //posicaoAux.QueryReset();
            //// registro 24
            //posicaoAux.LoadByPrimaryKey(idPosicao + 20);
            ////
            //Assert.AreEqual(9M + 43M, posicaoAux.Quantidade, "Quantidade incorreta");
            ////
            //Assert.AreEqual(52M * 64M, posicaoAux.ValorMercado, "ValorMercado incorreto");
            //Assert.AreEqual(52M * 64M, posicaoAux.ValorCustoLiquido, "ValorCustoLiquido incorreto");
            ////
            //#endregion
            //}
        }

        /// <summary>
        ///A test for CarregaPosicaoPosr (string, int, DateTime)
        ///</summary>
        [TestMethod()]
        public void CarregaPosicaoPosrTest() {
            //using (esTransactionScope scope = new esTransactionScope())
            //{
            PosicaoBolsa target = new PosicaoBolsa();
            //
            DateTime data = new DateTime(2007, 1, 03);
            //
            string diretorioBase = ConfigurationManager.AppSettings["DiretorioBaseTeste"].ToString();
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(diretorioBase).Append("POSR.txt");
            //
            int idCliente = 13712;
            //
            target.CarregaPosicaoPosr(nomeArquivo.ToString(), idCliente, data);

            //PosicaoBolsa posicaoAux = new PosicaoBolsa();
            //posicaoAux.Query.Select(posicaoAux.Query.IdPosicao.Max());
            //posicaoAux.Query.Load();
            //int idPosicao1 = posicaoAux.IdPosicao.Value - 1;
            //int idPosicao2 = posicaoAux.IdPosicao.Value;

            //#region Confere
            //posicaoAux.QueryReset();
            //posicaoAux.LoadByPrimaryKey(idPosicao1);
            //Assert.AreEqual("OPC", posicaoAux.TipoMercado, "TipoMercado incorreto");
            //Assert.IsNull(posicaoAux.DataVencimento, "Data Vencimento incorreta");
            //Assert.AreEqual(0M, posicaoAux.ResultadoRealizar, "ResultadoRealizar incorreto");
            //Assert.AreEqual(000000000010000M, posicaoAux.Quantidade, "Quantidade incorreta");
            //Assert.AreEqual(000000000010000M, posicaoAux.QuantidadeInicial, "QuantidadeInicial incorreta");
            //Assert.AreEqual(000000000010000M, posicaoAux.QuantidadeAbertura, "QuantidadeAbertura incorreta");
            //Assert.AreEqual(2, posicaoAux.IdCliente, "IdCliente incorreto");
            //Assert.AreEqual("PETRA50", posicaoAux.CdAtivoBolsa, "CdAtivoBolsa incorreto");
            //Assert.AreEqual(63, posicaoAux.IdAgente, "IdAgente incorreto");
            ////
            //Assert.AreEqual(10M, posicaoAux.PUMercado, "PUMercado incorreto");
            //Assert.AreEqual(10M, posicaoAux.PUCusto, "PUCusto incorreto");
            //Assert.AreEqual(10M, posicaoAux.PUCustoLiquido, "PUCustoLiquido incorreto");
            ////
            //Assert.AreEqual(100000M, posicaoAux.ValorMercado, "ValorMercado incorreto");
            //Assert.AreEqual(100000M, posicaoAux.ValorCustoLiquido, "ValorCustoLiquido incorreto");

            //posicaoAux.QueryReset();
            //posicaoAux.LoadByPrimaryKey(idPosicao2);
            //Assert.AreEqual("OPC", posicaoAux.TipoMercado, "TipoMercado incorreto");
            //Assert.IsNull(posicaoAux.DataVencimento, "Data Vencimento incorreta");
            //Assert.AreEqual(0M, posicaoAux.ResultadoRealizar, "ResultadoRealizar incorreto");
            //Assert.AreEqual(000000000015000M, posicaoAux.Quantidade, "Quantidade incorreta");
            //Assert.AreEqual(000000000015000M, posicaoAux.QuantidadeInicial, "QuantidadeInicial incorreta");
            //Assert.AreEqual(000000000015000M, posicaoAux.QuantidadeAbertura, "QuantidadeAbertura incorreta");
            //Assert.AreEqual(2, posicaoAux.IdCliente, "IdCliente incorreto");
            //Assert.AreEqual("VALEA56", posicaoAux.CdAtivoBolsa, "CdAtivoBolsa incorreto");
            //Assert.AreEqual(63, posicaoAux.IdAgente, "IdAgente incorreto");
            ////
            //Assert.AreEqual(10M, posicaoAux.PUMercado, "PUMercado incorreto");
            //Assert.AreEqual(10M, posicaoAux.PUCusto, "PUCusto incorreto");
            //Assert.AreEqual(10M, posicaoAux.PUCustoLiquido, "PUCustoLiquido incorreto");
            ////
            //Assert.AreEqual(150000M, posicaoAux.ValorMercado, "ValorMercado incorreto");
            //Assert.AreEqual(150000M, posicaoAux.ValorCustoLiquido, "ValorCustoLiquido incorreto");

            //#endregion
            //}
        }

        /// <summary>
        ///A test for CalculaTaxaCustodia (int, DateTime)
        /// Utiliza PosicaoBolsaHistorico ids = 17-22 - idAgente = 3 (pelo menos 5 registros) - 1 mesma data
        /// Utiliza PosicaoBolsaHistorico ids = 1,2,3 - idAgente = 1 (pelo menos 3 registros)
        /// Utiliza PosicaoBolsaHistorico ids = 4 - 16 - idAgente = 2 (pelo menos 1 registros)
        /// Resultado: Insercao de tres registros em liquidacao
        ///</summary>
        [TestMethod()]
        public void CalculaTaxaCustodiaTest() {
            using (esTransactionScope scope = new esTransactionScope()) {
                PosicaoBolsa target = new PosicaoBolsa();

                int idCliente = 1;
                DateTime data = new DateTime(2006, 1, 31);
                //
                // Pega o IdLiuidacao+1 antes de inserir
                // Insere um registro falso só para saber o novo ID                
                #region Registro Falso
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.AddNew();
                liquidacao.DataLancamento = new DateTime(2006, 1, 1);
                liquidacao.DataVencimento = new DateTime(2006, 1, 1);
                liquidacao.Descricao = "teste";
                liquidacao.Valor = 1;
                liquidacao.Situacao = 1;
                liquidacao.Origem = 1;
                liquidacao.Fonte = 1;
                liquidacao.IdCliente = 1;
                liquidacao.Save();
                //
                int idLiquidacao = liquidacao.IdLiquidacao.Value + 1;
                //
                #endregion

                target.CalculaTaxaCustodia(idCliente, data);
                //                             

                #region Confere Liquidacao1
                // Confere o 1 registro
                liquidacao = new Liquidacao();
                liquidacao.LoadByPrimaryKey(idLiquidacao);
                //
                Assert.AreEqual(new DateTime(2006, 1, 31), liquidacao.DataLancamento.Value, "DataLancamento1 incorreta");
                Assert.AreEqual(new DateTime(2006, 2, 1), liquidacao.DataVencimento.Value, "DataVencimento1 incorreta");
                Assert.AreEqual("Taxa de Custódia CBLC", liquidacao.Descricao, "Descricao1 incorreta");
                Assert.AreEqual(4M, liquidacao.Valor.Value, "Valor1 incorreto");
                Assert.AreEqual((byte)SituacaoLancamentoLiquidacao.Normal, liquidacao.Situacao, "Situacao1 incorreta");
                Assert.AreEqual(OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia, liquidacao.Origem, "Origem1 incorreta");
                Assert.AreEqual((byte)FonteLancamentoLiquidacao.Interno, liquidacao.Fonte, "Fonte1 incorreta");
                Assert.AreEqual(1, liquidacao.IdCliente, "IdCliente1 incorreto");
                Assert.AreEqual(1, liquidacao.IdAgente, "IdAgente1 incorreto");
                Assert.IsNull(liquidacao.IdConta, "IdConta1 deveria ser null");
                Assert.IsNull(liquidacao.IdentificadorOrigem, "IdentificadorOrigem1 deveria ser null");
                //
                #endregion

                #region Confere Liquidacao2
                liquidacao = new Liquidacao();
                liquidacao.LoadByPrimaryKey(idLiquidacao + 1);
                //
                Assert.AreEqual(new DateTime(2006, 1, 31), liquidacao.DataLancamento.Value, "DataLancamento2 incorreta");
                Assert.AreEqual(new DateTime(2006, 2, 1), liquidacao.DataVencimento.Value, "DataVencimento2 incorreta");
                Assert.AreEqual("Taxa de Custódia CBLC", liquidacao.Descricao, "Descricao2 incorreta");
                Assert.AreEqual(5M, liquidacao.Valor.Value, "Valor2 incorreto");
                Assert.AreEqual((byte)SituacaoLancamentoLiquidacao.Normal, liquidacao.Situacao, "Situacao2 incorreta");
                Assert.AreEqual(OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia, liquidacao.Origem, "Origem2 incorreta");
                Assert.AreEqual((byte)FonteLancamentoLiquidacao.Interno, liquidacao.Fonte, "Fonte2 incorreta");
                Assert.AreEqual(1, liquidacao.IdCliente, "IdCliente2 incorreto");
                Assert.AreEqual(2, liquidacao.IdAgente, "IdAgente2 incorreto");
                Assert.IsNull(liquidacao.IdConta, "IdConta2 deveria ser null");
                Assert.IsNull(liquidacao.IdentificadorOrigem, "IdentificadorOrigem2 deveria ser null");
                //
                #endregion

                #region Confere Liquidacao3
                liquidacao = new Liquidacao();
                liquidacao.LoadByPrimaryKey(idLiquidacao + 2);
                //
                Assert.AreEqual(new DateTime(2006, 1, 31), liquidacao.DataLancamento.Value, "DataLancamento3 incorreta");
                Assert.AreEqual(new DateTime(2006, 2, 1), liquidacao.DataVencimento.Value, "DataVencimento3 incorreta");
                Assert.AreEqual("Taxa de Custódia CBLC", liquidacao.Descricao, "Descricao3 incorreta");
                Assert.AreEqual(3M, liquidacao.Valor.Value, "Valor3 incorreto");
                Assert.AreEqual((byte)SituacaoLancamentoLiquidacao.Normal, liquidacao.Situacao, "Situacao3 incorreta");
                Assert.AreEqual(OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia, liquidacao.Origem, "Origem3 incorreta");
                Assert.AreEqual((byte)FonteLancamentoLiquidacao.Interno, liquidacao.Fonte, "Fonte3 incorreta");
                Assert.AreEqual(1, liquidacao.IdCliente, "IdCliente3 incorreto");
                Assert.AreEqual(3, liquidacao.IdAgente, "IdAgente3 incorreto");
                Assert.IsNull(liquidacao.IdConta, "IdConta3 deveria ser null");
                Assert.IsNull(liquidacao.IdentificadorOrigem, "IdentificadorOrigem3 deveria ser null");
                //
                #endregion

            }
        }

        /// <summary>
        ///A test for ConsolidaCustoMedioCompra (int)
        ///</summary>
        [TestMethod()]
        public void ConsolidaCustoMedioCompraTest() {
            PosicaoBolsa target = new PosicaoBolsa();

            int idCliente = 1;
            DateTime data = new DateTime(2007, 04, 09);

            //target.ConsolidaCustoMedioCompra(idCliente, data);

            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RetornaValorMercadoAcaoEnquadra (int, int?, int?)
        ///</summary>
        [TestMethod()]
        public void RetornaValorMercadoAcaoEnquadraTest() {
            PosicaoBolsa target = new PosicaoBolsa();
            //
            int idCliente = 13712;
            System.Nullable<int> idEmissor = new System.Nullable<int>();
            System.Nullable<int> idSetor = new System.Nullable<int>();
            //
            decimal expected = 0;
            decimal actual = target.RetornaValorMercadoAcaoEnquadra(idCliente, idEmissor, idSetor);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RetornaValorMercadoOpcaoEnquadra (int)
        ///</summary>
        [TestMethod()]
        public void RetornaValorMercadoOpcaoEnquadraTest() {
            
            
            
            
            PosicaoBolsa target = new PosicaoBolsa();

            int idCliente = 13712;
            decimal expected = 0;

            decimal actual = target.RetornaValorMercadoOpcaoEnquadra(idCliente);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
