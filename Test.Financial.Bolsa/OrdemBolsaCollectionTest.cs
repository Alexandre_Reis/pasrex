﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Bolsa;
namespace Test.Financial.Bolsa {
    /// <summary>
    ///This is a test class for Financial.Bolsa.OrdemBolsaCollection and is intended
    ///to contain all Financial.Bolsa.OrdemBolsaCollection Unit Tests
    ///</summary>
    [TestClass()]
    public class OrdemBolsaCollectionTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AtualizaOrdemBolsa (int, DateTime, decimal)
        ///</summary>
        [TestMethod()]
        public void AtualizaOrdemBolsaZeraQuantidadeDaytradeTest() {
            OrdemBolsaCollection target = new OrdemBolsaCollection();

            int idCliente = 1;
            DateTime data = new DateTime(2006, 1, 1);
            decimal quantidadeDaytrade = 0;

            target.AtualizaOrdemBolsa(idCliente, data, quantidadeDaytrade);
            //
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsaCollection.BuscaOrdemBolsa(idCliente, data);

            for (int i = 0; i < ordemBolsaCollection.Count; i++) {
                ordemBolsa = ordemBolsaCollection[i];
                Assert.AreEqual(quantidadeDaytrade, ordemBolsa.QuantidadeDayTrade, "QuantidadeDaytrade deveria ser 0");
            }
            // Muda quantidade daytrade para 15            
            for (int i = 0; i < ordemBolsaCollection.Count; i++) {
                ordemBolsa = ordemBolsaCollection[i];
                ordemBolsa.QuantidadeDayTrade = 1;
            }
            ordemBolsaCollection.Save();
        }

        /// <summary>
        ///A test for BuscaOrdemBolsaCollectionVenda (int, DateTime)
        ///</summary>
        [TestMethod()]
        public void BuscaOrdemBolsaCollectionVendaTest() {
            OrdemBolsaCollection target = new OrdemBolsaCollection();

            int idCliente = 1;
            DateTime data = new DateTime(2006, 1, 1);

            bool actual = target.BuscaOrdemBolsaVenda(idCliente, data);
            int numRegistroEsperado = 3;

            Assert.AreEqual(numRegistroEsperado, target.Count, "Deveria ter retornado " + numRegistroEsperado + " registros");
        }

        /// <summary>
        ///A test for BuscaOrdemBolsaCollectionCompra (int, DateTime, int, string)
        ///</summary>
        [TestMethod()]
        public void BuscaOrdemBolsaCollectionCompraTest() {
            OrdemBolsaCollection target = new OrdemBolsaCollection();

            int idCliente = 1;
            DateTime data = new DateTime(2006,1,1); 
            int idCorretora = 1; 
            string cdAtivoBolsa = "PETR4";

            bool actual = target.BuscaOrdemBolsaCompra(idCliente, data, idCorretora, cdAtivoBolsa);
            int numRegistroEsperado = 5;

            Assert.AreEqual(numRegistroEsperado, target.Count, "Deveria ter retornado " + numRegistroEsperado + " registros");            
        }
    }

}
