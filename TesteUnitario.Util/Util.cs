using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TesteUnitario.Util
{
    public class Util
    {
        // Recebe o caminho da planilha, um select de uma pasta da planilha, e se a planilha possui header, e retorna um DataSet com o resultado
        public DataSet RetornaDataSetPlanilha(string caminhoPlanilha, string select, bool header)
        {
            caminhoPlanilha.Trim();
            caminhoPlanilha.ToLower();
            select.Trim();

            string versaoExcel = "8.0";
            string hdr = "YES";

            if (caminhoPlanilha.PadRight(4) == "xlsx")
                versaoExcel = "12.0";

            if (!header)
                hdr = "NO";

            string conexao = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminhoPlanilha + ";Extended Properties='Excel " + versaoExcel + ";HDR=" + hdr + ";IMEX=1;';";

            try
            {
                OleDbConnection objConn = new OleDbConnection(@conexao);

                OleDbDataAdapter adapter = new OleDbDataAdapter(select, objConn);
                DataSet ds = new DataSet();

                objConn.Open();

                adapter.Fill(ds);

                objConn.Close();

                return ds;

            }
            catch (OleDbException e)
            {
                string errorMessages = "";

                for (int i = 0; i < e.Errors.Count; i++)
                {
                    errorMessages += "Index #" + i + "\n" +
                                     "Message: " + e.Errors[i].Message + "\n" +
                                     "NativeError: " + e.Errors[i].NativeError + "\n" +
                                     "Source: " + e.Errors[i].Source + "\n" +
                                     "SQLState: " + e.Errors[i].SQLState + "\n";
                }

                Console.Write(errorMessages);
                throw new Exception(errorMessages);
            }

        }

        // Recebe o caminho da planilha, a pasta da planilha, o campo que quer retornar em lista, a condicao da pesquisa, e se planilha possui header, retorna uma lista do tipo que foi passado
        public List<T> RetornaListPlanilha<T>(string caminhoPlanilha, string pastaPlanilha, string campoLista, string condicao, bool header)
        {
            string select = "Select " + campoLista.Trim() + " from [" + pastaPlanilha.Trim() + "]";
            if (condicao.Trim() != "")
                select += " where " + condicao.Trim();

            DataSet dataSet = new DataSet();
            dataSet = this.RetornaDataSetPlanilha(caminhoPlanilha, select, header);

            List<T> listaRetorno = new List<T>();

            foreach (DataRow dataRow in dataSet.Tables[0].Rows)
            {
                listaRetorno.Add((T)Convert.ChangeType(dataRow[campoLista], typeof(T)));
            }
            
            return listaRetorno;
        }

        // Recebe o contexto de teste, a pasta da planilha que quer atualizar seu campo, o valor do campo a ser alterado, a condicao das linhas a serem alteradas e se atualiza campo DataHora da planilha
        public void AtualizaValorPlanilha(TestContext testContextInstance, string pastaPlanilha, string campo, string valor, string condicao, bool dataHora)
        {
            System.Data.Common.DbCommand cmd = testContextInstance.DataConnection.CreateCommand();
            cmd.CommandText = "UPDATE [" + pastaPlanilha + "] SET " + campo + " = " + valor;
            if (dataHora)
                cmd.CommandText += ", DataHora = '" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "'";
            if (condicao.Trim() != "")
                cmd.CommandText += " WHERE " + condicao.Trim();
            cmd.ExecuteReader();
        }

    }
}
