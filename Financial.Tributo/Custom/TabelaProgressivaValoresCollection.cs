/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:16:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Tributo
{
	public partial class TabelaProgressivaValoresCollection : esTabelaProgressivaValoresCollection
	{

        /// <summary>
        /// Seleciona aliquota vigente de acordo com o valor
        /// </summary>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>        
        public void BuscaTabelaVigente(int idTabelaProgressiva, decimal Valor)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdTabelaProgressiva.Equal(idTabelaProgressiva))
                 .Where(this.Query.Valor.LessThanOrEqual(Valor))
                 .OrderBy(this.Query.Valor.Descending);

            this.query.es.Top = 1;

            this.Query.Load();
        }
	}
}
