using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Tributo
{
	public partial class ApuracaoRendaVariavelIRCollection : esApuracaoRendaVariavelIRCollection
	{
        /// <summary>
        /// 
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaApuracaoDataMaior(int idCliente, int ano, int mes)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdCliente, this.Query.Ano, this.Query.Mes)
                    .Where(this.Query.IdCliente == idCliente &
                            (this.Query.Ano.GreaterThan(ano) | 
                                (this.Query.Ano.Equal(ano) & this.Query.Mes.GreaterThan(mes))
                             )
                           );

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
