﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Fundo;
using log4net;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.Tributo.Enums;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Core;
using Financial.Investidor.Enums;
using Financial.Tributo.Debug.CalculoTributo;
using Financial.Investidor;
using Financial.Util.Enums;
using Financial.Common;

namespace Financial.Tributo.Custom
{
    public class CalculoTributo
    {
        public enum ProjecaoIRCotista
        {
            QuartaFeiraSeguinte = 1,
            DiaLiquidacaoResgate = 2,
            PorDecendio = 3,
            AlgunsDiasAposResgate = 4
        }

        #region Atributos e properties da classe
        decimal iof;
        decimal rendimentoAnterior; //Rendimento antes do abatimento do prej. a compensar
        decimal rendimentoPosterior; //Rendimento antes do abatimento do prej. a compensar
        decimal rendimentoAnteriorCompensado; //Rendimento depois do abatimento do prej. a compensar
        decimal rendimentoPosteriorCompensado; //Rendimento depois do abatimento do prej. a compensar
        decimal prejuizoUsado; //Valor do prejuízo que foi usado para compensar rendimentos positivos                
        decimal ir;
        decimal rendimentoComeCotas;

        public decimal IOF
        {
            get { return iof; }
            set { iof = value; }
        }

        public decimal RendimentoAnterior
        {
            get { return rendimentoAnterior; }
            set { rendimentoAnterior = value; }
        }

        public decimal RendimentoPosterior
        {
            get { return rendimentoPosterior; }
            set { rendimentoPosterior = value; }
        }

        public decimal RendimentoAnteriorCompensado
        {
            get { return rendimentoAnteriorCompensado; }
            set { rendimentoAnteriorCompensado = value; }
        }

        public decimal RendimentoPosteriorCompensado
        {
            get { return rendimentoPosteriorCompensado; }
            set { rendimentoPosteriorCompensado = value; }
        }

        public decimal PrejuizoUsado
        {
            get { return prejuizoUsado; }
            set { prejuizoUsado = value; }
        }

        public decimal IR
        {
            get { return ir; }
            set { ir = value; }
        }

        public decimal RendimentoComeCotas
        {
            get { return rendimentoComeCotas; }
            set { rendimentoComeCotas = value; }
        }
        #endregion

        /// <summary>
        /// Retorna a próxima data para pagamento de IR.
        /// De acordo com a Lei nº 11.196/05 ("MP do BEM") o recolhimento de IR passa a ser por decêndio
        /// e não mais cada quarta feira subsequente à cotização.
        /// Do dia 01 ao 10 - recolhimento terceiro dia útil após o dia 10.
        /// Do dia 11 ao 20 - recolhimento terceiro dia útil após o dia 20.
        /// Acima do dia 20 - recolhimento terceiro dia útil após o último dia do mês.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="projecaoIRCotista"></param>
        /// <returns></returns>
        public DateTime RetornaDiaPagamentoIR(DateTime data, ProjecaoIRCotista projecaoIRCotista, int diasAposResgate)
        {
            return RetornaDiaPagamentoIR(data, projecaoIRCotista, diasAposResgate, null);
        }

        /// <summary>
        /// Retorna a próxima data para pagamento de IR.
        /// De acordo com a Lei nº 11.196/05 ("MP do BEM") o recolhimento de IR passa a ser por decêndio
        /// e não mais cada quarta feira subsequente à cotização.
        /// Do dia 01 ao 10 - recolhimento terceiro dia útil após o dia 10.
        /// Do dia 11 ao 20 - recolhimento terceiro dia útil após o dia 20.
        /// Acima do dia 20 - recolhimento terceiro dia útil após o último dia do mês.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="projecaoIRCotista"></param>
        /// <returns></returns>
        public DateTime RetornaDiaPagamentoIR(DateTime data, ProjecaoIRCotista projecaoIRCotista, int diasAposResgate, int? idLocalFeriado)
        {
            DateTime dataPagamentoIR = data;

            if (projecaoIRCotista == ProjecaoIRCotista.QuartaFeiraSeguinte)
            {
                DateTime data4aFeira;
                DateTime dataProximaSemana = data.AddDays(7);
                if (dataProximaSemana.DayOfWeek == DayOfWeek.Wednesday)
                {
                    data4aFeira = dataProximaSemana;
                }
                else if (dataProximaSemana.DayOfWeek == DayOfWeek.Monday)
                {
                    data4aFeira = dataProximaSemana.AddDays(2);
                }
                else if (dataProximaSemana.DayOfWeek == DayOfWeek.Tuesday)
                {
                    data4aFeira = dataProximaSemana.AddDays(1);
                }
                else if (dataProximaSemana.DayOfWeek == DayOfWeek.Thursday)
                {
                    data4aFeira = dataProximaSemana.AddDays(-1);
                }
                else //Friday
                {
                    data4aFeira = dataProximaSemana.AddDays(-2);
                }

                if (!Calendario.IsDiaUtil(data4aFeira, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    data4aFeira = Calendario.AdicionaDiaUtil(data4aFeira, 1);
                }

                dataPagamentoIR = data4aFeira;
            }
            else if (projecaoIRCotista == ProjecaoIRCotista.AlgunsDiasAposResgate)
            {
                if(idLocalFeriado.HasValue && idLocalFeriado.Value != LocalFeriadoFixo.Brasil)
                    dataPagamentoIR = Calendario.AdicionaDiaUtil(dataPagamentoIR, diasAposResgate, idLocalFeriado.Value, TipoFeriado.Outros);
                else
                    dataPagamentoIR = Calendario.AdicionaDiaUtil(dataPagamentoIR, diasAposResgate, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else if (projecaoIRCotista == ProjecaoIRCotista.PorDecendio)
            {
                int dia = data.Day;

                if (dia <= 10)
                {
                    DateTime dataAux = new DateTime(data.Year, data.Month, 10);
                    dataPagamentoIR = dataAux;
                }
                else if (dia <= 20)
                {
                    DateTime dataAux = new DateTime(data.Year, data.Month, 20);
                    dataPagamentoIR = dataAux;
                }
                else
                {
                    DateTime dataAux = Calendario.RetornaUltimoDiaCorridoMes(data, 0);
                    dataPagamentoIR = dataAux;
                }

                //Soma 3 dias úteis à data
                dataPagamentoIR = Calendario.AdicionaDiaUtil(dataPagamentoIR, 3, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            return dataPagamentoIR;
        }

        /// <summary>
        /// Retorna a alíquota de Imposto de Renda para fundos de previdencia com tabela regressiva:
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns>Alíquota de IR para o período especificado</returns>
        public decimal RetornaAliquotaIRPrevidenciaRegressivo(DateTime dataInicio, DateTime dataFim)
        {
            int diasCorridos = Calendario.NumeroDias(dataInicio, dataFim);

            if (diasCorridos <= 720) //2 anos
            {
                return 35M;
            }
            if (diasCorridos <= 1440) //4 anos
            {
                return 30M;
            }
            if (diasCorridos <= 2160) //6 anos
            {
                return 25M;
            }
            if (diasCorridos <= 2880) //8 anos
            {
                return 20M;
            }
            if (diasCorridos <= 3600) //10 anos
            {
                return 15M;
            }
            else //Acima de 10 anos
            {
                return 10M;
            }
        }

        /// <summary>
        /// Retorna a alíquota de Imposto de Renda para fundos tributados como Curto Prazo, conforme abaixo:
        /// até 180 dias: 22,5%
        /// acima de 180 dias: 20%
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns>Alíquota de IR para o período especificado</returns>
        public decimal RetornaAliquotaIRCurtoPrazo(DateTime dataInicio, DateTime dataFim)
        {
            //DateTime dataBase = new DateTime(2004, 12, 30);
            //if (dataInicio <= dataBase)
            //{
            //    dataInicio = new DateTime(2004, 07, 01);
            //}

            int diasCorridos = Calendario.NumeroDias(dataInicio, dataFim);

            if (diasCorridos <= 180)
            {
                return 22.5M;
            }
            else
            {
                return 20M;
            }
        }

        /// <summary>
        /// Retorna a alíquota de Imposto de Renda para fundos tributados como Longo Prazo, conforme abaixo:
        /// até 180 dias: 22,5%
        /// 181 a 360 dias: 20%
        /// 361 a 720 dias: 17,5%
        /// acima de 720 dias: 15%.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns>Alíquota de IR para o período especificado</returns>
        public decimal RetornaAliquotaIRLongoPrazo(DateTime dataInicio, DateTime dataFim)
        {
            //DateTime dataBase = new DateTime(2004, 12, 22);
            //if (dataInicio <= dataBase)
            //{
            //    dataInicio = new DateTime(2004, 07, 01);
            //}

            int diasCorridos = Calendario.NumeroDias(dataInicio, dataFim);

            if (diasCorridos <= 180)
            {
                return 22.5M;
            }
            else if (diasCorridos <= 360)
            {
                return 20M;
            }
            else if (diasCorridos <= 720)
            {
                return 17.5M;
            }
            else
            {
                return 15;
            }
        }

        /// <summary>
        /// Calcula o valor do IOF, dado o período passado e o rendimento.
        /// Se passou dos 30 dias corridos, o IOF é igual a zero.
        /// Deve ser montada a classe do tipo CalculoIOF com os parâmetros corretos pelo método chamador.
        /// </summary>
        /// <param name="calculoIOF"></param>
        /// <param name="trunca">Indica se os valores calculados de rendimento e IOF serão truncados</param>        
        public void CalculaIOFFundo(CalculoIOF calculoIOF)
        {
            // Throw ArgumentException se alguma property da classe calculoIOF não estiver preenchida
            if (!calculoIOF.HasValue())
            {
                throw new ArgumentException("Parâmetros não preenchidos corretamente de CalculoIOF.");
            }

            #region Carrega variáveis locais com os valores dos parâmetros da calculoIOF
            decimal quantidade = calculoIOF.Quantidade.Value;
            decimal cotaCalculo = calculoIOF.CotaCalculo.Value;
            decimal cotaAplicacao = calculoIOF.CotaAplicacao.Value;
            DateTime dataAplicacao = calculoIOF.DataAplicacao.Value;
            DateTime dataCalculo = calculoIOF.DataCalculo.Value;
            int diasLiquidacaoResgate = calculoIOF.DiasLiquidacaoResgate.Value;
            #endregion

            DateTime dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculo, diasLiquidacaoResgate);

            if (cotaCalculo < cotaAplicacao)
            {
                this.iof = 0;
                return;
            }

            TabelaIOF tabelaIOF = new TabelaIOF();
            decimal aliquotaIOF = tabelaIOF.RetornaAliquotaIOF(dataAplicacao, dataCalculoIOF);

            decimal rendimento;
            decimal IOF;
            rendimento = Math.Round(quantidade * (cotaCalculo - cotaAplicacao), 2, MidpointRounding.AwayFromZero);
            IOF = Math.Round(rendimento * aliquotaIOF / 100, 2, MidpointRounding.AwayFromZero);

            this.iof = IOF;
        }

        /// <summary>
        /// Calcula o valor do IOF, dado o período passado e o rendimento.
        /// Se passou dos 30 dias corridos, o IOF é igual a zero.
        /// </summary>
        /// <param name="quantidade"></param>
        /// <param name="cotaCalculo"></param>
        /// <param name="cotaAplicacao"></param>
        /// <param name="dataCalculo"></param>
        /// <param name="dataAplicacao"></param>
        /// <returns>Valor do IOF calculado</returns>
        public decimal CalculaIOFFundo(decimal quantidade, decimal cotaCalculo, decimal cotaAplicacao,
                                       DateTime dataCalculo, DateTime dataAplicacao)
        {
            if (cotaCalculo < cotaAplicacao)
            {
                return 0;
            }

            TabelaIOF tabelaIOF = new TabelaIOF();
            decimal aliquotaIOF = tabelaIOF.RetornaAliquotaIOF(dataAplicacao, dataCalculo);

            decimal rendimento = Math.Round(quantidade * (cotaCalculo - cotaAplicacao), 2, MidpointRounding.AwayFromZero);
            decimal IOF = Math.Round(rendimento * aliquotaIOF / 100, 2, MidpointRounding.AwayFromZero);

            return IOF;
        }

        /// <summary>
        /// Cálculo IR para Fundos de Previdencia - Passivo
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="cotaCalculo"></param>
        /// <param name="quantidade"></param>
        /// <param name="isentoIOF"></param>
        /// <param name="diasLiquidacaoResgate"></param>
        /// <param name="data"></param>
        public void calculoIRFieCotista(int idPosicao, 
                                        decimal cotaCalculo, 
                                        decimal quantidade, 
                                        bool isentoIOF, 
                                        int diasLiquidacaoResgate,
                                        DateTime data)
        {
            decimal valorIr = 0;
            decimal aliquotaIr = 0;

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.LoadByPrimaryKey(idPosicao);

            OperacaoCotista operacaoCotista = new OperacaoCotista();
            operacaoCotista.LoadByPrimaryKey(posicaoCotista.IdOperacao.Value);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            decimal valorBaseCalculo = (quantidade * cotaCalculo);
            int anos = Calendario.NumeroAnos(posicaoCotista.DataAplicacao.Value, data);
            decimal valorTotalIr = 0;
            decimal rendimento = 0;

            if (FieModalidade.PGBL.GetHashCode().Equals(operacaoCotista.FieModalidade.GetValueOrDefault(0)))
            {
                aliquotaIr = this.AliquotaFie(operacaoCotista.FieTabelaIr.Value, valorBaseCalculo, anos, data);

                valorTotalIr = Utilitario.Truncate(valorBaseCalculo * aliquotaIr/100, 2);
            }
            else
            {
                TributacaoCautelaFieCollection tributacaoCautelaFieCollection = new TributacaoCautelaFieCollection();
                tributacaoCautelaFieCollection.Query.Where(tributacaoCautelaFieCollection.Query.IdOperacao.Equal(posicaoCotista.IdOperacao.Value));
                tributacaoCautelaFieCollection.Query.Where(tributacaoCautelaFieCollection.Query.Data.LessThanOrEqual(data));
                tributacaoCautelaFieCollection.Query.OrderBy(tributacaoCautelaFieCollection.Query.Data.Ascending);
                tributacaoCautelaFieCollection.Query.Load();

                if (!operacaoCotista.FieTabelaIr.HasValue)
                    return;

                int tabelaIr = operacaoCotista.FieTabelaIr.Value;
                decimal cotaAnterior = posicaoCotista.CotaAplicacao.Value;

                if(tributacaoCautelaFieCollection.Count > 0)
                {
                    foreach(TributacaoCautelaFie tributacaoCautelaFie in tributacaoCautelaFieCollection)
                    {

                        #region Busca o valor da cota na data
                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCotaDia(posicaoCotista.IdCarteira.Value, tributacaoCautelaFie.Data.Value);

                        decimal cotaDia = carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;

                        if (cotaDia == 0)
                        {
                            throw new Exception("Cota zerada na data " + tributacaoCautelaFie.Data.Value + " para a carteira " + posicaoCotista.IdCarteira.Value);
                        }
                        #endregion

                        rendimento = quantidade * (cotaDia - cotaAnterior);

                        aliquotaIr = this.AliquotaFie(tabelaIr, rendimento, anos, data);

                        if(rendimento > 0)
                            valorIr = Utilitario.Truncate(rendimento * aliquotaIr/100, 2);

                        valorTotalIr += valorIr;

                        cotaAnterior = cotaDia;
                        tabelaIr = tributacaoCautelaFie.FieTabelaIr.Value;
                        valorIr = 0;
                    }
                }
                rendimento = quantidade * (posicaoCotista.CotaDia.Value - cotaAnterior);
                aliquotaIr = this.AliquotaFie(tabelaIr, rendimento, anos, data);
                if(rendimento > 0)
                    valorIr = Utilitario.Truncate(rendimento * aliquotaIr/100, 2);

                if (!posicaoCotista.FieTabelaIr.Equals(tabelaIr))
                {
                    posicaoCotista.FieTabelaIr = tabelaIr;
                    posicaoCotista.Save();
                }

                valorTotalIr += valorIr;

            }

            this.ir = valorTotalIr;
        }

        /// <summary>
        /// Cálculo IR para Fundos de Previdencia - Ativo
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="cotaCalculo"></param>
        /// <param name="quantidade"></param>
        /// <param name="isentoIOF"></param>
        /// <param name="diasLiquidacaoResgate"></param>
        /// <param name="data"></param>
        public void calculoIRFieFundo(int idPosicao,
                                        decimal cotaCalculo,
                                        decimal quantidade,
                                        bool isentoIOF,
                                        int diasLiquidacaoResgate,
                                        DateTime data)
        {
            decimal valorIr = 0;
            decimal aliquotaIr = 0;

            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.LoadByPrimaryKey(idPosicao);

            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.LoadByPrimaryKey(posicaoFundo.IdOperacao.Value);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoFundo.IdCarteira.Value);

            decimal valorBaseCalculo = (quantidade * cotaCalculo);
            int anos = Calendario.NumeroAnos(posicaoFundo.DataAplicacao.Value, data);
            decimal valorTotalIr = 0;
            decimal rendimento = 0;

            if (FieModalidade.PGBL.GetHashCode().Equals(operacaoFundo.FieModalidade.GetValueOrDefault(0)))
            {
                aliquotaIr = this.AliquotaFie(operacaoFundo.FieTabelaIr.Value, valorBaseCalculo, anos, data);

                valorTotalIr = Utilitario.Truncate(valorBaseCalculo * aliquotaIr / 100, 2);
            }
            else
            {
                TributacaoFundoFieCollection tributacaoFundoFieCollection = new TributacaoFundoFieCollection();
                tributacaoFundoFieCollection.Query.Where(tributacaoFundoFieCollection.Query.IdOperacao.Equal(posicaoFundo.IdOperacao.Value));
                tributacaoFundoFieCollection.Query.Where(tributacaoFundoFieCollection.Query.Data.LessThanOrEqual(data));
                tributacaoFundoFieCollection.Query.OrderBy(tributacaoFundoFieCollection.Query.Data.Ascending);
                tributacaoFundoFieCollection.Query.Load();

                if (!operacaoFundo.FieTabelaIr.HasValue)
                    return;

                int tabelaIr = operacaoFundo.FieTabelaIr.Value;
                decimal cotaAnterior = posicaoFundo.CotaAplicacao.Value;

                if (tributacaoFundoFieCollection.Count > 0)
                {
                    foreach (TributacaoFundoFie tributacaoFundoFie in tributacaoFundoFieCollection)
                    {

                        #region Busca o valor da cota na data
                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCotaDia(posicaoFundo.IdCarteira.Value, tributacaoFundoFie.Data.Value);

                        decimal cotaDia = carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura ? historicoCota.CotaAbertura.Value : historicoCota.CotaFechamento.Value;

                        if (cotaDia == 0)
                        {
                            throw new Exception("Cota zerada na data " + tributacaoFundoFie.Data.Value + " para a carteira " + posicaoFundo.IdCarteira.Value);
                        }
                        #endregion

                        rendimento = quantidade * (cotaDia - cotaAnterior);

                        aliquotaIr = this.AliquotaFie(tabelaIr, rendimento, anos, data);

                        if (rendimento > 0)
                            valorIr = Utilitario.Truncate(rendimento * aliquotaIr / 100, 2);

                        valorTotalIr += valorIr;

                        cotaAnterior = cotaDia;
                        tabelaIr = tributacaoFundoFie.FieTabelaIr.Value;
                        valorIr = 0;
                    }
                }
                rendimento = quantidade * (posicaoFundo.CotaDia.Value - cotaAnterior);
                aliquotaIr = this.AliquotaFie(tabelaIr, rendimento, anos, data);
                if (rendimento > 0)
                    valorIr = Utilitario.Truncate(rendimento * aliquotaIr / 100, 2);

                if (!posicaoFundo.FieTabelaIr.Equals(tabelaIr))
                {
                    posicaoFundo.FieTabelaIr = tabelaIr;
                    posicaoFundo.Save();
                }

                valorTotalIr += valorIr;

            }

            this.ir = valorTotalIr;
        }

        private decimal AliquotaFie(int fieTabelaIr, decimal valorBaseCalculo, int anos, DateTime data)
        {
            decimal aliquota = 0;

            if (FieTabelaIR.Progressiva.GetHashCode().Equals(fieTabelaIr))
            {
                TabelaProgressivaVigenciaCollection tabelaProgressivaVigenciaCollection = new TabelaProgressivaVigenciaCollection();
                tabelaProgressivaVigenciaCollection.BuscaTabelaVigente(data);
                if (tabelaProgressivaVigenciaCollection.Count == 0)
                {
                    throw new Exception("Não existe tabela de IR vigente para o dia " + data);
                }
                else
                {
                    TabelaProgressivaValoresCollection tabelaProgressivaValoresCollection = new TabelaProgressivaValoresCollection();
                    tabelaProgressivaValoresCollection.BuscaTabelaVigente(tabelaProgressivaVigenciaCollection[0].IdTabelaProgressiva.Value, valorBaseCalculo);
                    if (tabelaProgressivaValoresCollection.Count > 0)
                    {
                        aliquota = tabelaProgressivaValoresCollection[0].Aliquota.Value;
                    }
                }
            }
            else
            {
                TabelaRegressivaVigenciaCollection tabelaRegressivaVigenciaCollection = new TabelaRegressivaVigenciaCollection();
                tabelaRegressivaVigenciaCollection.BuscaTabelaVigente(data);
                if (tabelaRegressivaVigenciaCollection.Count == 0)
                {
                    throw new Exception("Não existe tabela de IR vigente para o dia " + data);
                }
                else
                {
                    TabelaRegressivaValoresCollection tabelaRegressivaValoresCollection = new TabelaRegressivaValoresCollection();
                    tabelaRegressivaValoresCollection.BuscaTabelaVigente(tabelaRegressivaVigenciaCollection[0].IdTabelaRegressiva.Value, anos);
                    if (tabelaRegressivaValoresCollection.Count > 0)
                    {
                        aliquota = tabelaRegressivaValoresCollection[0].Aliquota.Value;
                    }
                }
            }

            return aliquota;
        }

        /// <summary>
        /// Calcula o IR e os rendimentos (base de cálculo) para fundos de Renda Fixa Curto e Longo Prazo.
        /// Leva em conta o prejuízo a compensar em todos os fundos da mesma categoria e administrador.
        /// Deve ser montada a classe do tipo CalculoIRFundoRendaFixa com os parâmetros corretos pelo método chamador.
        /// </summary>
        /// <param name="calculoIR"></param>
        /// <param name="trunca">Indica se os valores calculados de rendimento e IR serão truncados</param>
        public void CalculaIRFundoRendaFixa(CalculoIRFundoRendaFixa calculoIR, bool trunca,
            out DebugCalculaIRFundoRendaFixa debugInfo)
        {
            debugInfo = new DebugCalculaIRFundoRendaFixa();
            AgendaComeCotasCollection agendaComeCotasColl = new AgendaComeCotasCollection();

            #region Carrega variáveis locais com os valores dos parâmetros da estruturaCalculoIR
            int idFundo = calculoIR.IdFundo.Value;
            int tipoCota = calculoIR.TipoCota.Value;
            DateTime dataAplicacao = calculoIR.DataAplicacao.Value;
            DateTime dataConversao = calculoIR.DataConversao.Value;
            DateTime? dataLiquidacao = calculoIR.DataLiquidacao;
            DateTime? dataOperacaoResgate = calculoIR.DataOperacaoResgate;
            DateTime? dataConversaoResgate = calculoIR.DataConversaoResgate;
            DateTime? dataLiquidacaoResgate = calculoIR.DataLiquidacaoResgate;
            DateTime dataCalculo = calculoIR.DataCalculo.Value;
            DateTime dataUltimaCobrancaIR = calculoIR.DataUltimaCobrancaIR.Value;
            decimal cotaAplicacao = calculoIR.CotaAplicacao.Value;
            decimal cotaCalculo = calculoIR.CotaCalculo.Value;
            decimal quantidade = calculoIR.Quantidade.Value;
            decimal quantidadeAntesCortes = calculoIR.QuantidadeAntesCortes.Value;
            decimal quantidadePosicao = calculoIR.QuantidadePosicao.Value;
            decimal prejuizoCompensar = calculoIR.PrejuizoCompensar.Value;
            //Esse é o valor liquido referente ao resgate de 1 aplicação especifica
            //Só é carregado para resgates do tipo liquido
            decimal? valorLiquido = calculoIR.ValorLiquido;
            int diasLiquidacaoResgate = calculoIR.DiasLiquidacaoResgate.Value;
            int diasLiquidacaoComeCotas = calculoIR.DiasLiquidacaoComeCotas.Value;
            bool isentoIOF = calculoIR.IsentoIOF.Value;
            int? idPosicao = calculoIR.IdPosicao;           
            DateTime dataUltComeCotas = CalculoTributo.RetornaDataComeCotasAnterior(dataCalculo);
            bool usaCotaDaAplicacao = false;

            bool projecaoIR = false;
            if (calculoIR.ProjecaoIR.HasValue)
            {
                projecaoIR = calculoIR.ProjecaoIR.Value;
            }

            #region Busca cota na ultima cobrança de IR
            decimal cotaAnterior = cotaAplicacao;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idFundo, dataUltimaCobrancaIR);
            if (tipoCota == (byte)TipoCotaFundo.Abertura)
            {
                cotaAnterior = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaAnterior = historicoCota.CotaFechamento.Value;
            }
            #endregion

            #endregion

            #region Busca tipo truncagem e nr casas qtde, nr dias cotizacao resgate e nr dias liquidacao resgate, CalculaIOF e PrioridadeOperacao
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaQuantidade);
            campos.Add(carteira.Query.CasasDecimaisQuantidade);
            campos.Add(carteira.Query.DiasCotizacaoResgate);
            campos.Add(carteira.Query.DiasLiquidacaoResgate);
            campos.Add(carteira.Query.CalculaIOF);
            campos.Add(carteira.Query.PrioridadeOperacao);
            campos.Add(carteira.Query.IdCarteira);
            campos.Add(carteira.Query.TipoCusto);
            campos.Add(carteira.Query.ContagemPrazoIOF);
            campos.Add(carteira.Query.DataInicioContIOF);
            campos.Add(carteira.Query.DataFimContIOF);
            carteira.LoadByPrimaryKey(campos, idFundo);
            
            debugInfo.IsFDIC = carteira.IsFDIC();

            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            byte casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            #endregion

            #region Verifica aliquotas
            decimal aliquotaIR = 0;

            decimal aliquotaComeCotas = 0;

            int diasTotal = Calendario.NumeroDias(dataConversao, dataCalculo);

            if (calculoIR.TributaNaoResidente.Value)
            {
                if(calculoIR.TipoTributacao == (int)TipoTributacaoFundo.Acoes)
                    aliquotaIR = 10;
                else
                    aliquotaIR = 15;
            }
            else if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.AliquotaEspecifica)
            {
                AliquotaEspecificaCollection aliquotaEspecificaCollection = new AliquotaEspecificaCollection();
                aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.IdCarteira.Equal(idFundo));
                aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.DataValidade.LessThanOrEqual(dataCalculo));
                aliquotaEspecificaCollection.Query.OrderBy(aliquotaEspecificaCollection.Query.DataValidade.Descending);
                aliquotaEspecificaCollection.Query.es.Top = 1;
                if (aliquotaEspecificaCollection.Query.Load())
                {
                    aliquotaIR = aliquotaEspecificaCollection[0].Taxa.GetValueOrDefault(0);
                }
                else
                {
                    throw new Exception("Não foi encontrado cadastro de aliquota específica para o fundo " + idFundo);
                }

            }
            else
            {
                #region Busca aliquotas (pelo tipo de fundo - longo/curto prazo/ações)
                if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.Acoes)
                {
                    aliquotaIR = 15;
                }
                else
                {
                    if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                        calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                    {
                        aliquotaIR = RetornaAliquotaIRCurtoPrazo(dataAplicacao, dataCalculo);
                    }
                    else
                    {
                        aliquotaIR = RetornaAliquotaIRLongoPrazo(dataAplicacao, dataCalculo);
                    }
                }
                #endregion
            }
            #endregion

            #region Verifica se possui evento de come cotas anterior
            decimal ValorIRAgendado = 0;
            decimal ValorIOFVirtual = 0;
            int execucaoRecolhimento = 2; //1:Não Recolhe, 2:Recolhe na Data, 3:Recolhe com data agendada
            decimal rendimentoCompensadoAnterior = 0;
            decimal quantidadeAntesCortesAnterior = 0;
            decimal valorIRAgendado = 0;
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo));
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;
            if (agendaComeCotasCollection.Query.Load())
            {
                if (agendaComeCotasCollection.Count > 0)
                {
                    ValorIRAgendado = agendaComeCotasCollection[0].ValorIRAgendado.Value * quantidadeAntesCortes;
                    ValorIOFVirtual = agendaComeCotasCollection[0].ValorIOF.Value * quantidadeAntesCortes;
                    execucaoRecolhimento = agendaComeCotasCollection[0].ExecucaoRecolhimento.Value;
                    rendimentoCompensadoAnterior = agendaComeCotasCollection[0].RendimentoCompensado.Value;
                    quantidadeAntesCortesAnterior = agendaComeCotasCollection[0].QuantidadeAntesCortes.Value;
                    valorIRAgendado = agendaComeCotasCollection[0].ValorIRAgendado.Value;
                }
            }
            #endregion

            #region Define a partir de qual data deve efetuar pesquisa de IRResidual de eventos anteriores.
            // Se ultimo evento foi pela aliquota cheia, pesquisa será efetuada a a partir da data do último evento de fundo
            DateTime dataPesquisa = dataConversao;
            agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo),
                                                  agendaComeCotasCollection.Query.TipoAliquotaIR == 2);
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;

            if (agendaComeCotasCollection.Query.Load() && agendaComeCotasCollection.Count > 0)
            {
                dataPesquisa = agendaComeCotasCollection[0].DataLancamento.Value;
            }
            #endregion

            #region Soma IR residual de come cotas anteriores
            //decimal qtdEquivalenteQtdAntesCortes = quantidade * (quantidadeAntesCortes / quantidadePosicao);
            decimal IRResidual = 0;
            agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Select(agendaComeCotasCollection.Query.Residuo15.Sum(),
                                                   agendaComeCotasCollection.Query.Residuo175.Sum(),
                                                   agendaComeCotasCollection.Query.Residuo20.Sum(),
                                                   agendaComeCotasCollection.Query.Residuo225.Sum());
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo),
                                                  agendaComeCotasCollection.Query.DataLancamento.GreaterThan(dataPesquisa));
            if (agendaComeCotasCollection.Query.Load())
            {
                //esta quantidade j� � a quantidade equivalente antes cortes.
                if (aliquotaIR.Equals(new Decimal(15)) && agendaComeCotasCollection[0].Residuo15.HasValue)
                    IRResidual = agendaComeCotasCollection[0].Residuo15.Value * quantidade;
                if (aliquotaIR.Equals(new Decimal(17.5)) && agendaComeCotasCollection[0].Residuo175.HasValue)
                    IRResidual = agendaComeCotasCollection[0].Residuo175.Value * quantidade;
                if (aliquotaIR.Equals(new Decimal(20)) && agendaComeCotasCollection[0].Residuo20.HasValue)
                    IRResidual = agendaComeCotasCollection[0].Residuo20.Value * quantidade;
                if (aliquotaIR.Equals(new Decimal(22.5)) && agendaComeCotasCollection[0].Residuo225.HasValue)
                    IRResidual = agendaComeCotasCollection[0].Residuo225.Value * quantidade;

            }
            #endregion

            #region Verifica se ocorreu Desenquadramento tributário com origem em RV
            bool utilizaCotaAnterior = false;
            agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo),
                                                  agendaComeCotasCollection.Query.TipoEvento.Equal(FonteOperacaoCotista.MudancaClassificacao));
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;

            if (agendaComeCotasCollection.Query.Load() && agendaComeCotasCollection.Count > 0)
            {
                dataPesquisa = agendaComeCotasCollection[0].DataLancamento.Value;

                int tipoTributacao = Carteira.RetornaTipoTributacao(carteira.IdCarteira.Value, dataPesquisa);
                if (tipoTributacao == (int)ClassificacaoTributaria.RendaVariavels)
                {
                    utilizaCotaAnterior = true;
                    ValorIOFVirtual = 0;
                }
            }
            #endregion

            if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas ||
                calculoIR.TipoTributacao == (int)TipoTributacaoFundo.LPrazo_SemComeCotas)
            {
                usaCotaDaAplicacao = true;
            }

            #region Calculo IOF
            DateTime dataCalculoIOF = dataCalculo.AddDays((double)diasLiquidacaoResgate);
            decimal valorIOFPosicao = 0;
            decimal valorIOFResgate = 0;
            if (!isentoIOF && calculoIR.TipoTributacao != (int)TipoTributacaoFundo.Acoes)
            {
                DateTime dataInicioIOF = dataAplicacao;
                DateTime dataFimIOF = dataCalculoIOF;

                #region Contagem IOF personalizada
                if (carteira.ContagemPrazoIOF == (byte)ContagemDiasPrazoIOF.Personalizado)
                {
                    dataInicioIOF = this.RetornaDataInicioIOF(dataAplicacao, dataConversao, dataLiquidacao.Value, carteira.DataInicioContIOF.Value);
                    dataFimIOF = this.RetornaDataFimIOF(dataOperacaoResgate.Value, dataConversaoResgate.Value, dataLiquidacaoResgate.Value, carteira.DataFimContIOF.Value);
                }
                #endregion


                if (utilizaCotaAnterior)
                {
                    valorIOFPosicao = CalculaIOFFundo(quantidadePosicao, cotaCalculo, cotaAnterior, dataFimIOF, dataInicioIOF);
                    valorIOFResgate = CalculaIOFFundo(quantidade, cotaCalculo, cotaAnterior, dataFimIOF, dataInicioIOF);
                }
                else
                {
                    valorIOFPosicao = CalculaIOFFundo(quantidadePosicao, cotaCalculo, cotaAplicacao, dataFimIOF, dataInicioIOF);
                    valorIOFResgate = CalculaIOFFundo(quantidade, cotaCalculo, cotaAplicacao, dataFimIOF, dataInicioIOF);
                }
            }
            #endregion

            #region Calculo IR
            decimal rendimentoBrutoDesdeAplicacao = 0;
            if (utilizaCotaAnterior)
                rendimentoBrutoDesdeAplicacao = Math.Round((cotaCalculo - cotaAnterior) * quantidadePosicao, 2, MidpointRounding.AwayFromZero);
            else
                rendimentoBrutoDesdeAplicacao = Math.Round((cotaCalculo - cotaAplicacao) * quantidadePosicao, 2, MidpointRounding.AwayFromZero);

            //Verifica de utiliza Custo Medio
            decimal rendimentoBrutoDesdeUltimoPagamentoIR = 0;
            if (carteira.TipoCusto.Value == (int)TipoCustoFundo.MedioAplicado || usaCotaDaAplicacao)
                rendimentoBrutoDesdeUltimoPagamentoIR = Math.Round((cotaCalculo - cotaAplicacao) * quantidadePosicao, 2, MidpointRounding.AwayFromZero);
            else
                rendimentoBrutoDesdeUltimoPagamentoIR = Math.Round((cotaCalculo - cotaAnterior) * quantidadePosicao, 2, MidpointRounding.AwayFromZero);

            decimal rendimentoDepoisIOF = Math.Round(rendimentoBrutoDesdeUltimoPagamentoIR - valorIOFPosicao + ValorIOFVirtual, 2, MidpointRounding.AwayFromZero);
           

            decimal prejuizoUsado = 0;
            decimal rendimentoCompensado = 0;
            decimal rendimentoBaseIr = (rendimentoDepoisIOF / quantidadeAntesCortes * quantidade);
            rendimentoCompensado = Math.Round(quantidade * (rendimentoDepoisIOF / quantidadeAntesCortes) - prejuizoCompensar, 2, MidpointRounding.AwayFromZero);
            if (rendimentoBaseIr > 0)
            {
                if (prejuizoCompensar > rendimentoBaseIr)
                {
                    rendimentoCompensado = 0; //Rendimento compensado 100%!
                    prejuizoUsado = rendimentoDepoisIOF / quantidadeAntesCortes * quantidade;
                    prejuizoCompensar -= rendimentoDepoisIOF / quantidadeAntesCortes * quantidade; //Diminui o prejuizo a compensar!                        
                }
                else
                {
                    rendimentoCompensado = Math.Round(quantidade * (rendimentoDepoisIOF / quantidadeAntesCortes) - prejuizoCompensar, 2, MidpointRounding.AwayFromZero);
                    prejuizoUsado = prejuizoCompensar;
                    prejuizoCompensar = 0; //Zera o prejuizo a compensar!                        
                }
            }

            decimal IRaPagarUltimoEvento = 0;
            if(execucaoRecolhimento == 1)
                IRaPagarUltimoEvento = valorIRAgendado * quantidade;
                //IRaPagarUltimoEvento = rendimentoCompensadoAnterior/quantidadeAntesCortesAnterior * quantidade * (aliquotaIR/100);
            decimal IRdevidoDesdeUltimoPagamentoIR = Utilitario.Truncate(rendimentoCompensado * (aliquotaIR / 100) + IRaPagarUltimoEvento, 2);            
            decimal IRResgate = IRdevidoDesdeUltimoPagamentoIR + IRResidual;
            IRResgate = Math.Max(0M, IRResgate);
            #endregion

            this.rendimentoAnterior = 0;
            this.rendimentoPosterior = rendimentoDepoisIOF / quantidadeAntesCortes * quantidade;
            this.rendimentoAnteriorCompensado = 0;
            this.rendimentoPosteriorCompensado = rendimentoCompensado;
            this.prejuizoUsado = prejuizoUsado;
            if(projecaoIR)
                this.iof = valorIOFPosicao;
            else
                this.iof = valorIOFResgate;
            this.ir = IRResgate;

            #region Preenchimento Informacoes de Debug
            debugInfo.AliquotaComeCotas = 0;
            debugInfo.AliquotaIR = aliquotaIR;
            debugInfo.CotaCalculo = cotaCalculo;
            debugInfo.DataCalculo = dataCalculo;
            debugInfo.PrejuizoCompensar = prejuizoCompensar;
            debugInfo.RendimentoAnterior = this.rendimentoAnterior;
            debugInfo.RendimentoPosterior = this.rendimentoPosterior;
            debugInfo.RendimentoComeCotas = this.rendimentoComeCotas;
            debugInfo.TotalPrejuizoUsado = prejuizoUsado;
            debugInfo.TotalRendimento = rendimentoDepoisIOF;
            debugInfo.TotalRendimentoCompensado = rendimentoCompensado;
            debugInfo.ValorIOF = valorIOFResgate;
            debugInfo.ValorIR = IRResgate;
            #endregion
        }

        private bool VerificaEventoAnterior(int idPosicao, DateTime data)
        {
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(data));
            agendaComeCotasCollection.Query.Load();
            return agendaComeCotasCollection.Count > 0;
        }

        /// <summary>
        /// Calcula o IR e os rendimentos (base de cálculo) para fundos de Renda Fixa Curto e Longo Prazo.
        /// Leva em conta o prejuízo a compensar em todos os fundos da mesma categoria e administrador.
        /// Deve ser montada a classe do tipo CalculoIRFundoRendaFixa com os parâmetros corretos pelo método chamador.
        /// </summary>
        /// <param name="calculoIR"></param>
        /// <param name="trunca">Indica se os valores calculados de rendimento e IR serão truncados</param>
        public void CalculaIRComeCotasEvento(CalculoIRFundoRendaFixa calculoIR, FonteOperacaoCotista fonteOperacaoCotista, TipoComeCotas tipoComeCotas, int? idEvento)
        {

            #region Carrega variáveis locais com os valores dos parâmetros da estruturaCalculoIR
            int idFundo = calculoIR.IdFundo.Value;
            int tipoCota = calculoIR.TipoCota.Value;
            DateTime dataAplicacao = calculoIR.DataAplicacao.Value;
            DateTime dataConversao = calculoIR.DataConversao.Value;
            DateTime dataCalculo = calculoIR.DataCalculo.Value;
            DateTime dataUltimaCobrancaIR = calculoIR.DataUltimaCobrancaIR.Value;
            decimal cotaAplicacao = calculoIR.CotaAplicacao.Value;
            decimal cotaCalculo = calculoIR.CotaCalculo.Value;
            decimal quantidade = calculoIR.Quantidade.Value;
            decimal quantidadeAntesCortes = calculoIR.QuantidadeAntesCortes.Value;
            decimal prejuizoCompensar = calculoIR.PrejuizoCompensar.Value;
            //Esse é o valor liquido referente ao resgate de 1 aplicação especifica
            //Só é carregado para resgates do tipo liquido
            decimal? valorLiquido = calculoIR.ValorLiquido;
            int diasLiquidacaoResgate = calculoIR.DiasLiquidacaoResgate.Value;
            int diasLiquidacaoComeCotas = calculoIR.DiasLiquidacaoComeCotas.Value;
            bool isentoIOF = calculoIR.IsentoIOF.Value;
            int idPosicao = calculoIR.IdPosicao.Value;
            int tipoAliquotaCC = 1; //1: Utilizar alíquota IR padrão do Come-Cota, 2: Utilizar alíquota IR vigente de cada cautela
            DateTime dataAgendamento = calculoIR.DataCalculo.Value;
            bool efetuaCobrancaComeCotas = true;
            bool projecaoIR = false;
            if (calculoIR.ProjecaoIR.HasValue)
            {
                projecaoIR = calculoIR.ProjecaoIR.Value;
            }
            TabelaIOF tabelaIOF = new TabelaIOF();
            #endregion

            #region Busca cota na ultima cobrança de IR
            decimal cotaAnterior = cotaAplicacao;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idFundo, dataUltimaCobrancaIR);
            if (tipoCota == (byte)TipoCotaFundo.Abertura)
            {
                cotaAnterior = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaAnterior = historicoCota.CotaFechamento.Value;
            }
            #endregion

            #region Verifica se é um evento
            bool isMudancaClassificacao = false;
            int execucaoRecolhimento = 2; //1:Não recolher IR, 2:Recolhe IR na propria data, 3:Agenda o recollhimento do IR
            if(fonteOperacaoCotista.Equals(FonteOperacaoCotista.CisaoIncorporacaoFusao))
            {
                EventoFundo eventoFundo = new EventoFundo();
                if(eventoFundo.LoadByPrimaryKey(idEvento.Value))
                {
                    if(eventoFundo.DataRecolhimento.HasValue)
                        dataAgendamento = eventoFundo.DataRecolhimento.Value;
                    if (eventoFundo.ExecucaoRecolhimento.Value == 1)
                    {
                        efetuaCobrancaComeCotas = false;
                    }
                    else
                    {
                        tipoAliquotaCC = eventoFundo.AliquotaIR.Value;
                    }
                    execucaoRecolhimento = eventoFundo.ExecucaoRecolhimento.Value;
                }

            }
            if(fonteOperacaoCotista.Equals(FonteOperacaoCotista.MudancaClassificacao))
            {
                isMudancaClassificacao = true;

                DesenquadramentoTributario desenquadramentoTributario = new DesenquadramentoTributario();
                if(desenquadramentoTributario.LoadByPrimaryKey(idEvento.Value))
                {
                    if(desenquadramentoTributario.DataRecolhimento.HasValue)
                        dataAgendamento = desenquadramentoTributario.DataRecolhimento.Value;
                    if(desenquadramentoTributario.ExecucaoRecolhimento.Value == 1)
                    {
                        efetuaCobrancaComeCotas = false;
                    }
                    else
                    {
                        tipoAliquotaCC = desenquadramentoTributario.AliquotaIR.Value;
                    }
                    execucaoRecolhimento = desenquadramentoTributario.ExecucaoRecolhimento.Value;
                }

            }
            if(fonteOperacaoCotista.Equals(FonteOperacaoCotista.MudancaCondominio))
            {
                FundoInvestimentoFormaCondominio fundoInvestimentoFormaCondominio = new FundoInvestimentoFormaCondominio();
                if(fundoInvestimentoFormaCondominio.LoadByPrimaryKey(idEvento.Value))
                {
                    tipoAliquotaCC = fundoInvestimentoFormaCondominio.AliquotaIR.Value;
                    if(fundoInvestimentoFormaCondominio.DataRecolhimento.HasValue)
                        dataAgendamento = fundoInvestimentoFormaCondominio.DataRecolhimento.Value;
                    if(fundoInvestimentoFormaCondominio.ExecucaoRecolhimento.Value == 1)
                    {
                        efetuaCobrancaComeCotas = false;
                    }
                    else
                    {
                        tipoAliquotaCC = fundoInvestimentoFormaCondominio.AliquotaIR.Value;
                    }
                    execucaoRecolhimento = fundoInvestimentoFormaCondominio.ExecucaoRecolhimento.Value;
                }
            }
            #endregion
                
            #region Busca tipo truncagem e nr casas qtde, nr dias cotizacao resgate e nr dias liquidacao resgate, CalculaIOF e PrioridadeOperacao
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaQuantidade);
            campos.Add(carteira.Query.CasasDecimaisQuantidade);
            campos.Add(carteira.Query.DiasCotizacaoResgate);
            campos.Add(carteira.Query.DiasLiquidacaoResgate);
            campos.Add(carteira.Query.CalculaIOF);
            campos.Add(carteira.Query.PrioridadeOperacao);
            campos.Add(carteira.Query.IdCarteira);
            campos.Add(carteira.Query.ContagemPrazoIOF);
            campos.Add(carteira.Query.ContagemDiasConversaoResgate);
            campos.Add(carteira.Query.ContaPrzIOFVirtual);
            carteira.LoadByPrimaryKey(campos, idFundo);

            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            byte casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            #endregion

            #region Calculo IOF
            DateTime dataCalculoIOF = new DateTime();
            if ((int)ContagemIOFVirtual.ContagemUltDiaUtil == carteira.ContaPrzIOFVirtual.Value)
            {
                dataCalculoIOF = dataAgendamento;
            }
            else if ((int)ContagemIOFVirtual.ContagemUltDiaCorrido == carteira.ContaPrzIOFVirtual.Value)
            {
                dataCalculoIOF = Calendario.RetornaUltimoDiaCorridoMes(dataAgendamento, 0);
            }
            else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value && (int)ContagemDiasPrazoIOF.Santander == carteira.ContagemPrazoIOF.Value)
            {
                dataCalculoIOF = dataCalculo.AddDays((double)diasLiquidacaoResgate);

                if (!Calendario.IsDiaUtil(dataCalculoIOF, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                    dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculoIOF, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                dataAgendamento = dataCalculoIOF;
            }
            else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value)
            {
                if (carteira.ContagemDiasConversaoResgate.Equals((int)ContagemDias.Uteis))
                    dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculo, diasLiquidacaoResgate);
                else
                    dataCalculoIOF = dataCalculo.AddDays((double)diasLiquidacaoResgate);
            }
            #region Verifica se ocorreu mudanca de classificação tributária
            bool calculaIOF = true;
            if (isMudancaClassificacao)
            {
                //Verifica se Regime tributário era de RV - Não cobra IOF
                if (Carteira.RetornaTipoTributacao(idFundo, dataCalculo).Equals((int)TipoTributacaoFundo.Acoes)) calculaIOF = false;
                //Verifica se novo Regime tributário é de RV - Não cobra IOF
                if (Carteira.RetornaTipoTributacao(idFundo, dataCalculo.AddDays(1)).Equals((int)TipoTributacaoFundo.Acoes)) calculaIOF = false;
            }
            #endregion


            decimal valorIOF = 0;
            if (!isentoIOF && calculaIOF)
            {
                valorIOF = CalculaIOFFundo(quantidade, cotaCalculo, cotaAplicacao, dataCalculoIOF, dataAplicacao);
            }
            #endregion

            #region Verifica aliquotas
            decimal aliquotaComeCotas = 0;
            if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.Acoes)
                aliquotaComeCotas = 15;
            else
            {
                if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    //tipoAliquotaIR = 1: Utilizar alíquota IR padrão do Come-Cota, tipoAliquotaIR = 2: Utilizar alíquota IR vigente de cada cautela
                    if (tipoAliquotaCC == 1)
                        aliquotaComeCotas = 20;
                    else
                        aliquotaComeCotas = RetornaAliquotaIRCurtoPrazo(dataAplicacao, dataCalculo);
                }
                else
                {
                    //tipoAliquotaIR = 1: Utilizar alíquota IR padrão do Come-Cota, tipoAliquotaIR = 2: Utilizar alíquota IR vigente de cada cautela
                    if (tipoAliquotaCC == 1)
                        aliquotaComeCotas = 15;
                    else
                        aliquotaComeCotas = RetornaAliquotaIRLongoPrazo(dataAplicacao, dataCalculo);

                }
            }
            #endregion

            #region Seleciona comecotas anterior
            decimal ValorIRAgendado = 0;
            decimal ValorIOFVirtual = 0;
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo));
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;
            agendaComeCotasCollection.Query.Load();
            if (agendaComeCotasCollection.Count > 0)
            {
                ValorIRAgendado = agendaComeCotasCollection[0].ValorIRAgendado.Value * quantidadeAntesCortes;
                ValorIOFVirtual = agendaComeCotasCollection[0].ValorIOF.Value * quantidadeAntesCortes;
            }
            #endregion

            #region Soma IR residual de come cotas anteriores se estiver utilizando aliquota da cautela
            decimal IRResidual = 0;
            if(tipoAliquotaCC == 2)
            {
                agendaComeCotasCollection = new AgendaComeCotasCollection();
                agendaComeCotasCollection.Query.Select(agendaComeCotasCollection.Query.Residuo15.Sum(),
                                                       agendaComeCotasCollection.Query.Residuo175.Sum(),
                                                       agendaComeCotasCollection.Query.Residuo20.Sum(),
                                                       agendaComeCotasCollection.Query.Residuo225.Sum());
                agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                      agendaComeCotasCollection.Query.DataLancamento.LessThan(dataCalculo));
                if (agendaComeCotasCollection.Query.Load())
                {

                    if (aliquotaComeCotas.Equals(new Decimal(15)) && agendaComeCotasCollection[0].Residuo15.HasValue)
                        IRResidual = agendaComeCotasCollection[0].Residuo15.Value * quantidadeAntesCortes;
                    if (aliquotaComeCotas.Equals(new Decimal(17.5)) && agendaComeCotasCollection[0].Residuo175.HasValue)
                        IRResidual = agendaComeCotasCollection[0].Residuo175.Value * quantidadeAntesCortes;
                    if (aliquotaComeCotas.Equals(new Decimal(20)) && agendaComeCotasCollection[0].Residuo20.HasValue)
                        IRResidual = agendaComeCotasCollection[0].Residuo20.Value * quantidadeAntesCortes;
                    if (aliquotaComeCotas.Equals(new Decimal(22.5)) && agendaComeCotasCollection[0].Residuo225.HasValue)
                        IRResidual = agendaComeCotasCollection[0].Residuo225.Value * quantidadeAntesCortes;

                }

            }
            #endregion

            #region Calculo do IR
            decimal rendimentoBrutoDesdeUltimoPagamentoIR = Math.Round((cotaCalculo - cotaAnterior) * quantidade, 2, MidpointRounding.AwayFromZero);
            decimal rendimentoDepoisIOF = rendimentoBrutoDesdeUltimoPagamentoIR - valorIOF + ValorIOFVirtual;

            decimal prejuizoUsado = 0;
            decimal rendimentoCompensado = 0;
            if (prejuizoCompensar > rendimentoDepoisIOF)
            {
                rendimentoCompensado = 0; //Rendimento compensado 100%!
                prejuizoUsado = rendimentoDepoisIOF;
                prejuizoCompensar -= rendimentoDepoisIOF; //Diminui o prejuizo a compensar!                        
            }
            else
            {
                rendimentoCompensado = rendimentoDepoisIOF - prejuizoCompensar;
                prejuizoUsado = prejuizoCompensar;
                prejuizoCompensar = 0; //Zera o prejuizo a compensar!                        
            }

            decimal IRComeCotas = Utilitario.Truncate(IRResidual + ValorIRAgendado + (rendimentoCompensado * aliquotaComeCotas / 100), 2);
            #endregion
            
            #region Quantidade Come Cotas
            decimal qtdComidas = 0;
            if(efetuaCobrancaComeCotas)
            {
                if (truncaQuantidade)
                {
                    qtdComidas = Utilitario.Truncate(IRComeCotas / cotaCalculo, casasDecimaisQuantidade);
                }
                else
                {
                    qtdComidas = Math.Round(IRComeCotas / cotaCalculo, casasDecimaisQuantidade, MidpointRounding.AwayFromZero);
                }
            }
            #endregion

            decimal residuo15  = new decimal(0.15);
            decimal residuo175 = new decimal(0.175);
            decimal residuo20  = new decimal(0.20);
            decimal residuo225 = new decimal(0.225);

            int idCarteira = 0;
            if (tipoComeCotas == TipoComeCotas.Fundo)
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                if (posicaoFundo.LoadByPrimaryKey(idPosicao))
                {
                    idCarteira = posicaoFundo.IdCliente.Value;
                }
            }

            #region Cria agenda come cotas
            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
            agendaComeCotas.IdCarteira = idCarteira == 0 ? idFundo : idCarteira;
            agendaComeCotas.DataLancamento = dataCalculo;
            agendaComeCotas.DataVencimento = dataAgendamento;
            agendaComeCotas.TipoEvento = (int)fonteOperacaoCotista;
            agendaComeCotas.IdPosicao = idPosicao;
            agendaComeCotas.TipoPosicao = (int)tipoComeCotas;
            agendaComeCotas.DataUltimoIR = dataUltimaCobrancaIR;
            agendaComeCotas.Quantidade = quantidade;
            agendaComeCotas.QuantidadeAntesCortes = quantidadeAntesCortes;
            agendaComeCotas.ValorCotaAplic = cotaAplicacao;
            agendaComeCotas.ValorCotaUltimoPagamentoIR = cotaAnterior;
            agendaComeCotas.ValorCota = cotaCalculo;
            agendaComeCotas.AliquotaCC = aliquotaComeCotas;
            agendaComeCotas.RendimentoBrutoDesdeAplicacao = Math.Round((cotaCalculo - cotaAplicacao) * quantidade, 2, MidpointRounding.AwayFromZero);
            agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = rendimentoBrutoDesdeUltimoPagamentoIR;
            agendaComeCotas.NumDiasCorridosDesdeAquisicao = Calendario.NumeroDias(dataConversao, dataCalculo);
            agendaComeCotas.PrazoIOF = Calendario.NumeroDias(dataAplicacao, dataCalculoIOF);
            agendaComeCotas.AliquotaIOF =  tabelaIOF.RetornaAliquotaIOF(dataAplicacao, dataCalculo);
            agendaComeCotas.ValorIOF = valorIOF / quantidadeAntesCortes;
            agendaComeCotas.ValorIOFVirtual = ValorIOFVirtual / quantidadeAntesCortes;
            agendaComeCotas.PrejuizoUsado = prejuizoCompensar;
            agendaComeCotas.RendimentoCompensado = rendimentoCompensado;
            agendaComeCotas.ValorIRAgendado = (IRComeCotas - (qtdComidas * cotaCalculo)) / quantidadeAntesCortes;
            agendaComeCotas.ValorIRPago = Math.Round(qtdComidas * cotaCalculo, 2, MidpointRounding.AwayFromZero);

            if (Carteira.RetornaTipoTributacao(idFundo, dataCalculo).Equals((int)TipoTributacaoFundo.Acoes))
            {
                agendaComeCotas.Residuo15 = 0;
                agendaComeCotas.Residuo175 = 0;
                agendaComeCotas.Residuo20 = 0;
                agendaComeCotas.Residuo225 = 0;
            }
            else
            {
                agendaComeCotas.Residuo15 = Convert.ToDecimal(((residuo15 - (aliquotaComeCotas / 100)) * rendimentoCompensado) / quantidadeAntesCortes);
                agendaComeCotas.Residuo175 = Convert.ToDecimal(((residuo175 - (aliquotaComeCotas / 100)) * rendimentoCompensado) / quantidadeAntesCortes);
                agendaComeCotas.Residuo20 = Convert.ToDecimal(((residuo20 - (aliquotaComeCotas / 100)) * rendimentoCompensado) / quantidadeAntesCortes);
                agendaComeCotas.Residuo225 = Convert.ToDecimal(((residuo225 - (aliquotaComeCotas / 100)) * rendimentoCompensado) / quantidadeAntesCortes);
            }
            agendaComeCotas.QuantidadeComida = qtdComidas;
            agendaComeCotas.QuantidadeFinal = quantidade - qtdComidas;
            agendaComeCotas.ExecucaoRecolhimento = execucaoRecolhimento;
            agendaComeCotas.TipoAliquotaIR = tipoAliquotaCC;
            agendaComeCotas.Save();
            #endregion

            this.rendimentoAnterior = 0;
            this.rendimentoPosterior = rendimentoDepoisIOF;
            this.rendimentoAnteriorCompensado = 0;
            this.rendimentoPosteriorCompensado = rendimentoCompensado;
            this.prejuizoUsado = prejuizoUsado;
            this.iof = valorIOF;
            this.ir = IRComeCotas;

        }

        private void AjustaQuantidadeIncorporacao(int idCarteira, int tipoCota, DateTime dataCalculo, DateTime dataConversao, DateTime dataFocal, int idPosicao,
                                                  ref decimal quantidade, ref decimal quantidadeFocal)
        {
            HistoricoIncorporacaoPosicaoCollection historicoIncorporacaoPosicaoCollection = new HistoricoIncorporacaoPosicaoCollection();
            historicoIncorporacaoPosicaoCollection.Query.Where(historicoIncorporacaoPosicaoCollection.Query.IdPosicao.Equal(idPosicao),
                                                               historicoIncorporacaoPosicaoCollection.Query.IdCarteiraDestino.Equal(idCarteira),
                                                               historicoIncorporacaoPosicaoCollection.Query.DataConversao.Equal(dataConversao),                                                               
                                                               historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.GreaterThan(dataFocal),                                                               
                                                               historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.LessThanOrEqual(dataCalculo));
            historicoIncorporacaoPosicaoCollection.Query.OrderBy(historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.Descending);
            historicoIncorporacaoPosicaoCollection.Query.Load();

            foreach (HistoricoIncorporacaoPosicao historicoIncorporacaoPosicaoAjuste in historicoIncorporacaoPosicaoCollection)
            {
                HistoricoCota historicoCotaIncorporacao = new HistoricoCota();
                historicoCotaIncorporacao.BuscaValorCotaDia(historicoIncorporacaoPosicaoAjuste.IdCarteiraDestino.Value, historicoIncorporacaoPosicaoAjuste.DataIncorporacao.Value);
                decimal cotaDestino;
                if (tipoCota == (byte)TipoCotaFundo.Abertura)
                {
                    cotaDestino = historicoCotaIncorporacao.CotaAbertura.Value;
                }
                else
                {
                    cotaDestino = historicoCotaIncorporacao.CotaFechamento.Value;
                }

                decimal saldoFocalIncorporacao = Math.Round(quantidadeFocal * cotaDestino, 2);
                decimal saldoFocal = Math.Round(quantidade * cotaDestino, 2);

                historicoCotaIncorporacao = new HistoricoCota();
                historicoCotaIncorporacao.BuscaValorCotaDia(historicoIncorporacaoPosicaoAjuste.IdCarteiraOrigem.Value, historicoIncorporacaoPosicaoAjuste.DataIncorporacao.Value);
                decimal cotaOrigem;
                if (tipoCota == (byte)TipoCotaFundo.Abertura)
                {
                    cotaOrigem = historicoCotaIncorporacao.CotaAbertura.Value;
                }
                else
                {
                    cotaOrigem = historicoCotaIncorporacao.CotaFechamento.Value;
                }

                quantidadeFocal = saldoFocalIncorporacao / cotaOrigem;
                quantidade = saldoFocal / cotaOrigem;
            }
        }

        
        /// <summary>
        /// Método usado exclusivamente para o cálculo do IR para resgates líquidos em fundos de RF.
        /// </summary>
        /// <param name="calculoIR"></param>
        /// <param name="prejuizoCompensar"></param>
        /// <param name="valorLiquido"></param>
        public void CalculaIRFundoRendaFixaLiquido(CalculoIRFundoRendaFixa calculoIR, decimal prejuizoCompensar, decimal saldoIR,
                                                   decimal saldoLiquido, decimal valorLiquido)
        {
            #region Busca aliquotas (pelo tipo de fundo - longo/curto prazo)
            decimal aliquotaIR;
            DateTime dataAplicacao = calculoIR.DataAplicacao.Value;
            DateTime dataCalculo = calculoIR.DataCalculo.Value;
            
            if (calculoIR.TributaNaoResidente.Value)
            {
                aliquotaIR = 15;
            }
            else
            {
                if (calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    calculoIR.TipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    aliquotaIR = RetornaAliquotaIRCurtoPrazo(dataAplicacao, dataCalculo);

                }
                else
                {
                    aliquotaIR = RetornaAliquotaIRLongoPrazo(dataAplicacao, dataCalculo);
                }
            }
            #endregion

            decimal valorIR = Math.Round(saldoIR * valorLiquido / saldoLiquido, 2, MidpointRounding.AwayFromZero);
            decimal rendimento = Math.Round(valorIR / (aliquotaIR / 100), 2, MidpointRounding.AwayFromZero);

            decimal prejuizoUsado = 0;
            if (prejuizoCompensar > 0)
            {
                decimal IRCompensar = Math.Round(prejuizoCompensar * (aliquotaIR / 100), 2, MidpointRounding.AwayFromZero);
                if (IRCompensar < valorIR)
                {
                    valorIR -= IRCompensar;
                    prejuizoUsado = prejuizoCompensar;
                }
                else
                {
                    prejuizoUsado = Math.Round(valorIR / (aliquotaIR / 100), 2, MidpointRounding.AwayFromZero);
                    valorIR = 0;
                }
            }

            decimal rendimentoCompensado = rendimento - prejuizoUsado;

            this.ir = valorIR;
            this.prejuizoUsado = prejuizoUsado;
            this.rendimentoAnterior = 0;
            this.rendimentoAnteriorCompensado = 0;
            this.rendimentoPosterior = rendimento;
            this.rendimentoPosteriorCompensado = rendimentoCompensado;
        }

        /// <summary>
        /// Verifica se ocorreu alteração de classificação tributária
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected decimal SelecionaCotaMudancaEvento(int idCarteira, DateTime data, int? idPosicao, decimal cotaAplicacao )
        {
            decimal cota = cotaAplicacao;

            HistoricoCota historicoCota = new HistoricoCota();

            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(idPosicao),
                                                  agendaComeCotasCollection.Query.DataLancamento.LessThan(data),
                                                  agendaComeCotasCollection.Query.TipoEvento.Equal(FonteOperacaoCotista.MudancaClassificacao));
            agendaComeCotasCollection.Query.OrderBy(agendaComeCotasCollection.Query.DataLancamento.Descending);
            agendaComeCotasCollection.Query.es.Top = 1;

            if (agendaComeCotasCollection.Query.Load() && agendaComeCotasCollection.Count > 0)
            {
                DateTime dataPesquisa = agendaComeCotasCollection[0].DataLancamento.Value;

                historicoCota.BuscaValorCota(idCarteira, dataPesquisa);

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                if (carteira.TipoCota.Value.Equals((int)TipoCotaFundo.Fechamento))
                    cota = historicoCota.CotaFechamento.Value;
                else
                    cota = historicoCota.CotaAbertura.Value;

            }

            return cota;

        }


        /// <summary>
        /// Calcula o IR e os rendimentos (base de cálculo) para fundos de Ações.
        /// Leva em conta o prejuízo a compensar em todos os fundos da mesma categoria e administrador.
        /// Deve ser montada a classe do tipo CalculoIRFundoRendaFixa com os parâmetros corretos pelo método chamador.
        /// </summary>
        /// <param name="calculoIR"></param>
        /// <param name="trunca">Indica se os valores calculados de rendimento e IR serão truncados</param>
        public void CalculaIRFundoAcoes(CalculoIRFundoAcoes calculoIR)
        {
            // Throw ArgumentException se alguma property da classe calculoIR não estiver preenchida
            if (!calculoIR.HasValue())
            {
                throw new ArgumentException("Parâmetros não preenchidos corretamente de CalculoIRFundoAcoes.");
            }

            DateTime dataAplicacao = calculoIR.DataAplicacao;
            DateTime dataCalculo = calculoIR.DataCalculo.Value;
            decimal quantidade = calculoIR.Quantidade.Value;
            decimal cotaAplicacao = calculoIR.CotaAplicacao.Value;
            cotaAplicacao = this.SelecionaCotaMudancaEvento(calculoIR.IdCarteira, calculoIR.DataCalculo.Value, calculoIR.IdPosicao.Value, cotaAplicacao);
            decimal cotaCalculo = calculoIR.CotaCalculo.Value;
            decimal prejuizoCompensar = calculoIR.PrejuizoCompensar.Value;
            int idCarteira = calculoIR.IdCarteira;
            int tipoInvestidor = calculoIR.TipoInvestidor;

            DateTime dataAlteracaoAliquota = new DateTime(2001, 12, 31);
            DateTime dataTransicaoIR = new DateTime(2005, 01, 01);

            #region Cálculo do rendimento e do IR
            decimal rendimentoAnteriorAlteracaoAliquota = 0;
            decimal valorIrAnteriorAlteracaoAliquota = 0;
            

            if (dataAplicacao <= dataAlteracaoAliquota)
            {
                #region Busca o valor da cota na data de alteração da aliquota
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorCotaDia(idCarteira, dataAlteracaoAliquota);

                decimal cotaDiaAlteracaoAliquota;
                if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                {
                    cotaDiaAlteracaoAliquota = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaDiaAlteracaoAliquota = historicoCota.CotaFechamento.Value;
                }
                #endregion

                rendimentoAnteriorAlteracaoAliquota = Math.Round(quantidade * (cotaDiaAlteracaoAliquota - cotaAplicacao), 2, MidpointRounding.AwayFromZero);
                if(rendimentoAnteriorAlteracaoAliquota > 0)
                    valorIrAnteriorAlteracaoAliquota = Math.Round(rendimentoAnteriorAlteracaoAliquota * 0.10M, 2, MidpointRounding.AwayFromZero);

                cotaAplicacao = cotaDiaAlteracaoAliquota;
            }

            decimal prejuizoUsado = 0;
            decimal rendimento = Math.Round(quantidade * (cotaCalculo - cotaAplicacao), 2, MidpointRounding.AwayFromZero);

            //Compensa prejuizo de rendimento anterior a 31/12/2001
            if (rendimentoAnteriorAlteracaoAliquota < 0 || rendimento < 0)
            {
                valorIrAnteriorAlteracaoAliquota = 0;
                rendimento += rendimentoAnteriorAlteracaoAliquota;
            }

            decimal rendimentoCompensado = rendimento;

            if (prejuizoCompensar > 0)
            {
                //Compensa prejuizo
                if (rendimento > 0)
                {
                    if (prejuizoCompensar > rendimento)
                    {
                        rendimentoCompensado = 0; //Rendimento compensado 100%!
                        prejuizoUsado += rendimento;
                        prejuizoCompensar -= rendimento; //Diminui o prejuizo a compensar!                        
                    }
                    else
                    {
                        rendimentoCompensado -= prejuizoCompensar; //Rendimento compensado!
                        prejuizoUsado += prejuizoCompensar;
                        prejuizoCompensar = 0; //Zera o prejuizo a compensar!                        
                    }
                }
            }

            decimal valorIR = 0;
            if (rendimentoCompensado > 0)
            {
                ExcecoesTributacaoIR excecoesTributacaoIR = new ExcecoesTributacaoIR();
                excecoesTributacaoIR.RetornaAliquota(idCarteira.ToString(), dataCalculo, TipoMercado.Fundos, (ListaTipoInvestidor)tipoInvestidor);

                if (excecoesTributacaoIR.IsencaoIR != null)
                {
                    if (excecoesTributacaoIR.IsencaoIR.Equals(ListaIsencaoIR.Tributado))
                        valorIR = rendimentoCompensado * (Convert.ToDecimal(excecoesTributacaoIR.AliquotaIR) / 100);
                    else
                        valorIR = 0;
                }
                else
                {
                    if (calculoIR.TributaNaoResidente.Value)
                    {
                        valorIR = Utilitario.Truncate(rendimentoCompensado * 0.10M, 2);
                    }
                    else if (dataCalculo < dataTransicaoIR)
                    {
                        valorIR = Utilitario.Truncate(rendimentoCompensado * 0.20M, 2);
                    }
                    else
                    {
                        valorIR = Utilitario.Truncate(rendimentoCompensado * 0.15M, 2);
                    }                    
                }
            }

            valorIR = valorIR + valorIrAnteriorAlteracaoAliquota;

            this.rendimentoAnterior = 0;
            this.rendimentoPosterior = rendimento;
            this.rendimentoAnteriorCompensado = 0;
            this.rendimentoPosteriorCompensado = rendimentoCompensado;
            this.prejuizoUsado = prejuizoUsado;
            this.ir = valorIR;
            #endregion

        }

        
        private DateTime DataDesenquadramentoTributario(DateTime dataUltimoComeCotas, DateTime dataCalculo)
        {
            DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
            desenquadramentoTributarioCollection.Query.Where(desenquadramentoTributarioCollection.Query.Data.GreaterThan(dataUltimoComeCotas),
                                                             desenquadramentoTributarioCollection.Query.Data.LessThan(dataCalculo));
            if (desenquadramentoTributarioCollection.Query.Load())
            {
                if (desenquadramentoTributarioCollection.Count > 0)
                {
                    return desenquadramentoTributarioCollection[0].Data.Value;
                }
            }
            return dataUltimoComeCotas;
        }

        private AgendaComeCotas SelecionaAgendaTrocaTipoTributacao(int idPosicao, DateTime data)
        {
            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
            agendaComeCotas.Query.Where(agendaComeCotas.Query.IdPosicao.Equal(idPosicao),
                                        agendaComeCotas.Query.DataLancamento.Equal(data),
                                        agendaComeCotas.Query.TipoEvento == 10);
            agendaComeCotas.Query.Load();

            return agendaComeCotas;
        }

        
        /// <summary>
        /// Retorna a data do próximo come cotas em relação à data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaDataComeCotasPosterior(DateTime data)
        {
            DateTime dataComeCotasPosterior = new DateTime();

            int ano = data.Year;

            DateTime finalNovembro = new DateTime(ano, 11, 30);
            if (!Calendario.IsDiaUtil(finalNovembro))
            {
                finalNovembro = Calendario.SubtraiDiaUtil(finalNovembro, 1);
            }

            DateTime finalMaio = new DateTime(ano, 05, 31);
            if (!Calendario.IsDiaUtil(finalMaio))
            {
                finalMaio = Calendario.SubtraiDiaUtil(finalMaio, 1);
            }

            if (DateTime.Compare(data, finalMaio) < 0)
            {
                dataComeCotasPosterior = finalMaio;
            }
            else if (DateTime.Compare(data, finalNovembro) < 0)
            {
                dataComeCotasPosterior = finalNovembro;
            }
            else //Busca final de maio do ano posterior
            {
                DateTime finalMaioPosterior = new DateTime(ano + 1, 05, 31);
                dataComeCotasPosterior = finalMaioPosterior;
                if (!Calendario.IsDiaUtil(dataComeCotasPosterior))
                {
                    dataComeCotasPosterior = Calendario.SubtraiDiaUtil(dataComeCotasPosterior, 1);
                }
            }

            return dataComeCotasPosterior;
        }

        /// <summary>
        /// Retorna a data do come cotas anterior em relação à data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaDataComeCotasAnterior(DateTime data)
        {
            DateTime dataComeCotasAnterior = new DateTime();

            int ano = data.Year;

            DateTime finalNovembro = new DateTime(ano, 11, 30);
            if (!Calendario.IsDiaUtil(finalNovembro))
            {
                finalNovembro = Calendario.SubtraiDiaUtil(finalNovembro, 1);
            }

            DateTime finalMaio = new DateTime(ano, 05, 31);
            if (!Calendario.IsDiaUtil(finalMaio))
            {
                finalMaio = Calendario.SubtraiDiaUtil(finalMaio, 1);
            }

            if (DateTime.Compare(data, finalNovembro) > 0)
            {
                dataComeCotasAnterior = finalNovembro;
            }
            else if (DateTime.Compare(data, finalMaio) > 0)
            {
                dataComeCotasAnterior = finalMaio;
            }
            else //Busca final de novembro do ano anterior
            {
                DateTime finalNovembroAnterior = new DateTime(ano - 1, 11, 30);
                dataComeCotasAnterior = finalNovembroAnterior;
                if (!Calendario.IsDiaUtil(dataComeCotasAnterior))
                {
                    dataComeCotasAnterior = Calendario.SubtraiDiaUtil(dataComeCotasAnterior, 1);
                }
            }

            return dataComeCotasAnterior;
        }

        /// <summary>
        /// Retorna a data da próxima incorporação em relação à data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static HistoricoIncorporacaoPosicao RetornaIncorporacaoPosterior(int idPosicao, int idCarteira, DateTime dataConversao, DateTime dataCalculo, DateTime dataFocal)
        {
            HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = new HistoricoIncorporacaoPosicao();

            HistoricoIncorporacaoPosicaoCollection historicoIncorporacaoPosicaoCollection = new HistoricoIncorporacaoPosicaoCollection();
            historicoIncorporacaoPosicaoCollection.Query.Where(historicoIncorporacaoPosicaoCollection.Query.IdPosicao.Equal(idPosicao),
                                                               historicoIncorporacaoPosicaoCollection.Query.IdCarteiraDestino.Equal(idCarteira),
                                                               historicoIncorporacaoPosicaoCollection.Query.DataConversao.Equal(dataConversao),
                                                               historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.GreaterThan(dataFocal),
                                                               historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.LessThanOrEqual(dataCalculo));
            historicoIncorporacaoPosicaoCollection.Query.OrderBy(historicoIncorporacaoPosicaoCollection.Query.DataIncorporacao.Descending);
            historicoIncorporacaoPosicaoCollection.Query.Load();

            if (historicoIncorporacaoPosicaoCollection.Count != 0)
            {
                historicoIncorporacaoPosicao = historicoIncorporacaoPosicaoCollection[0];
            }

            return historicoIncorporacaoPosicao;
        }

        /// <summary>
        /// Calcula a qtde de cotas antes de cortes, dada a quantidade real passada.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal CalculaQuantidadeCortesHistoricos(DateTime dataCalculo, esEntity posicao)
        {
            int idPosicao = (int)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdPosicao);
            int idCarteira = (int)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.IdCarteira);
            DateTime dataConversao = (DateTime)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataConversao);
            DateTime dataAplicacao = (DateTime)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataAplicacao);
            DateTime dataUltimaCobrancaIR = (DateTime)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR);
            decimal cotaAplicacao = (decimal)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
            decimal quantidade = (decimal)posicao.GetColumn(PosicaoCotistaMetadata.ColumnNames.Quantidade);

            #region Busca tipo truncagem, nr casas qtde, tipo de cota e tipo tributacao
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaQuantidade);
            campos.Add(carteira.Query.CasasDecimaisQuantidade);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.TipoTributacao);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            byte casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            byte tipoCota = carteira.TipoCota.Value;
            byte tipoTributacao = carteira.TipoTributacao.Value;
            #endregion

            bool aliquotaDiferenciada = true;
            int diasTotal = Calendario.NumeroDias(dataConversao, dataCalculo);

            if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo && diasTotal > 180 ||
                tipoTributacao == (int)TipoTributacaoFundo.LongoPrazo && diasTotal > 720 ||
                dataAplicacao >= dataUltimaCobrancaIR ||
                tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas ||
                tipoTributacao == (int)TipoTributacaoFundo.LPrazo_SemComeCotas ||
                Carteira.RetornaCondominioCarteira(idCarteira, dataCalculo) == (int)TipoFundo.Fechado)
            {
                return 0;
            }

            #region Busca aliquotas (pelo tipo de fundo - longo/curto prazo)
            decimal aliquotaIR;
            decimal aliquotaComeCotas;
            if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo)
            {
                aliquotaIR = RetornaAliquotaIRCurtoPrazo(dataAplicacao, dataCalculo);
                aliquotaComeCotas = 20;
            }
            else
            {
                aliquotaIR = RetornaAliquotaIRLongoPrazo(dataAplicacao, dataCalculo);
                aliquotaComeCotas = 15;
            }
            #endregion

            decimal valorIOF = 0;
            decimal totalQuantidade = 0;
            if (aliquotaDiferenciada)
            {
                #region Loop com o cálculo do histórico de IR em cada comecotas (somente para aliquotaDiferenciada)
                DateTime dataProximoComeCotas = RetornaDataComeCotasPosterior(dataConversao);
                DateTime dataFocal = dataProximoComeCotas;
                DateTime dataAnterior = dataConversao;
                decimal valorIOFAnterior = valorIOF;
                decimal quantidadeFocal = quantidade;
                decimal rendimentoAnterior = 0;
                decimal cotaComeCotasAnterior = 0;
                bool isDataComeCotas = false;

                if (dataCalculo == dataProximoComeCotas)
                {
                    isDataComeCotas = true;
                }

                decimal totalCotas = 0;
                while (DateTime.Compare(dataCalculo, dataFocal) >= 0)
                {
                    #region Busca cota na data focal
                    HistoricoCota historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(idCarteira, dataFocal);
                    decimal cotaFocal;
                    if (tipoCota == (byte)TipoCotaFundo.Abertura)
                    {
                        cotaFocal = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaFocal = historicoCota.CotaFechamento.Value;
                    }
                    #endregion

                    #region Busca cota na data do come cotas anterior à data focal
                    if (rendimentoAnterior >= 0)
                    {
                        historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCotaDia(idCarteira, dataAnterior);

                        if (tipoCota == (byte)TipoCotaFundo.Abertura)
                        {
                            cotaComeCotasAnterior = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaComeCotasAnterior = historicoCota.CotaFechamento.Value;
                        }
                    }
                    #endregion

                    valorIOF = CalculaIOFFundo(quantidade, cotaFocal, cotaAplicacao, dataFocal, dataAplicacao);

                    //Calcula rendimento e IR
                    decimal quantidadeComeCotas = 0;
                    decimal IRComeCotas = 0;
                    decimal rendimento = Math.Round(valorIOFAnterior + ((cotaFocal - cotaComeCotasAnterior) * quantidadeFocal), 2, MidpointRounding.AwayFromZero);
                    decimal rendimentoCompensado = rendimento;

                    rendimentoAnterior = rendimento;

                    if ((dataCalculo != dataFocal) || isDataComeCotas)
                    {
                        if (rendimento > 0)
                        {
                            decimal prejuizoUsadoComeCotas = 0;
                            #region Busca o prejuizo usado para abater da posição à época do come-cotas sendo 'simulado'
                            if (posicao is PosicaoCotista)
                            {
                                OperacaoCotista operacaoCotista = new OperacaoCotista();
                                operacaoCotista.Query.Select(operacaoCotista.Query.PrejuizoUsado);
                                operacaoCotista.Query.Where(operacaoCotista.Query.IdPosicaoResgatada.Equal(idPosicao),
                                                            operacaoCotista.Query.DataConversao.Equal(dataFocal));
                                if (operacaoCotista.Query.Load())
                                {
                                    prejuizoUsadoComeCotas = operacaoCotista.PrejuizoUsado.Value;
                                }

                                //Precisa proporcionalizar pela qtde de cotas (aplicada) na data do come-cotas
                                if (prejuizoUsadoComeCotas > 0)
                                {
                                    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                                    posicaoCotistaHistorico.Query.Select(posicaoCotistaHistorico.Query.QuantidadeAntesCortes);
                                    posicaoCotistaHistorico.Query.Where(posicaoCotistaHistorico.Query.DataHistorico.Equal(dataFocal),
                                                                        posicaoCotistaHistorico.Query.IdPosicao.Equal(idPosicao));
                                    if (posicaoCotistaHistorico.Query.Load())
                                    {
                                        if (posicaoCotistaHistorico.QuantidadeAntesCortes.HasValue && posicaoCotistaHistorico.QuantidadeAntesCortes.Value > 0)
                                        {
                                            prejuizoUsadoComeCotas = Math.Round(prejuizoUsadoComeCotas *
                                                                        (quantidade / posicaoCotistaHistorico.QuantidadeAntesCortes.Value), 2, MidpointRounding.AwayFromZero);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                OperacaoFundo operacaoFundo = new OperacaoFundo();
                                operacaoFundo.Query.Select(operacaoFundo.Query.PrejuizoUsado);
                                operacaoFundo.Query.Where(operacaoFundo.Query.IdPosicaoResgatada.Equal(idPosicao),
                                                          operacaoFundo.Query.DataConversao.Equal(dataFocal));
                                if (operacaoFundo.Query.Load())
                                {
                                    prejuizoUsadoComeCotas = operacaoFundo.PrejuizoUsado.Value;
                                }

                                //Precisa proporcionalizar pela qtde de cotas (aplicada) na data do come-cotas
                                if (prejuizoUsadoComeCotas > 0)
                                {
                                    PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                                    posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.QuantidadeAntesCortes);
                                    posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.DataHistorico.Equal(dataFocal),
                                                                      posicaoFundoHistorico.Query.IdPosicao.Equal(idPosicao));
                                    if (posicaoFundoHistorico.Query.Load())
                                    {
                                        if (posicaoFundoHistorico.QuantidadeAntesCortes.HasValue && posicaoFundoHistorico.QuantidadeAntesCortes.Value > 0)
                                        {
                                            prejuizoUsadoComeCotas = Math.Round(prejuizoUsadoComeCotas *
                                                                        (quantidade / posicaoFundoHistorico.QuantidadeAntesCortes.Value), 2, MidpointRounding.AwayFromZero);
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (prejuizoUsadoComeCotas > rendimento)
                            {
                                rendimentoCompensado = 0; //Rendimento compensado 100%!
                            }
                            else
                            {
                                rendimentoCompensado -= prejuizoUsadoComeCotas; //Rendimento compensado!                                
                            }

                            IRComeCotas = Math.Round((rendimentoCompensado - valorIOF) * aliquotaComeCotas / 100, 2, MidpointRounding.AwayFromZero);

                            if (truncaQuantidade)
                            {
                                quantidadeComeCotas = Utilitario.Truncate(IRComeCotas / cotaFocal, casasDecimaisQuantidade);
                            }
                            else
                            {
                                quantidadeComeCotas = Math.Round(IRComeCotas / cotaFocal, casasDecimaisQuantidade, MidpointRounding.AwayFromZero);
                            }

                            totalQuantidade += quantidadeComeCotas;
                            quantidadeFocal -= quantidadeComeCotas;
                        }

                        valorIOFAnterior = valorIOF;
                        dataAnterior = dataFocal;

                        if (isDataComeCotas)
                        {
                            break;
                        }

                        DateTime dataComeCotasPosterior = RetornaDataComeCotasPosterior(dataFocal);
                        if (dataComeCotasPosterior > dataCalculo)
                        {
                            dataFocal = dataCalculo;
                        }
                        else
                        {
                            dataFocal = dataComeCotasPosterior;

                            if (dataComeCotasPosterior == dataCalculo)
                            {
                                isDataComeCotas = true;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                }
                #endregion
            }

            return totalQuantidade;
        }

        private DateTime RetornaDataInicioIOF(DateTime dataAplicacao, DateTime dataConversao, DateTime dataLiquidacao, short enumInicioContIOF)
        {
            DateTime dataInicio = new DateTime();

            if ((short)Financial.Tributo.Enums.DataContagemIOF.DataSolicitacao == enumInicioContIOF)
                dataInicio = dataAplicacao.AddDays(-1);
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataSolicitacaoD1 == enumInicioContIOF)
                dataInicio = Calendario.AdicionaDiaUtil(dataAplicacao, 1);
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataConversao == enumInicioContIOF)
                dataInicio = dataConversao;
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataLiqFinanceira == enumInicioContIOF)
                dataInicio = dataLiquidacao;

            return dataInicio;
        }

        private DateTime RetornaDataFimIOF(DateTime dataAplicacao, DateTime dataConversao, DateTime dataLiquidacao, short enumFimContIOF)
        {
            DateTime dataFim = new DateTime();

            if ((short)Financial.Tributo.Enums.DataContagemIOF.DataSolicitacao == enumFimContIOF)
                dataFim = dataAplicacao;
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataSolicitacaoD1 == enumFimContIOF)
                dataFim = Calendario.AdicionaDiaUtil(dataAplicacao, 1);
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataConversao == enumFimContIOF)
                dataFim = dataConversao;
            else if ((short)Financial.Tributo.Enums.DataContagemIOF.DataLiqFinanceira == enumFimContIOF)
                dataFim = dataLiquidacao;

            return dataFim;
        }

        public bool calculoExcecaoIR(int idCarteira, DateTime dataAplicacao, DateTime dataCalculo, decimal quantidade, bool isentoIOF, ListaTipoInvestidor tipoInvestidor)
        {
            decimal ir = 0;
            decimal iof = 0;
            decimal cotaAplicacao = 0;
            decimal cotaCalculo = 0;
            decimal rendimento = 0;
            decimal aliquota = 0;

            ExcecoesTributacaoIR excecoesTributacaoIR = new ExcecoesTributacaoIR();
            excecoesTributacaoIR.RetornaAliquota(idCarteira.ToString(), dataCalculo, TipoMercado.Fundos, tipoInvestidor);

            if (excecoesTributacaoIR.IsencaoIR != null)
            {
                if (excecoesTributacaoIR.IsencaoIR.Equals(ListaIsencaoIR.Tributado))
                    aliquota = Convert.ToDecimal(excecoesTributacaoIR.AliquotaIR);
                else
                    aliquota = 0;
            }
            else
            {
                return false;
            }


            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            #region cota aplicacao
            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(dataAplicacao, idCarteira))
            {
                if (carteira.TipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaAplicacao = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaAplicacao = historicoCota.CotaFechamento.Value;
                }
            }
            else
            {
                throw new Exception("Cota não encontrada para carteira " + idCarteira.ToString() + " no dia " + dataAplicacao.ToString());
            }
            #endregion

            #region cota calculo
            historicoCota.QueryReset();
            if (historicoCota.LoadByPrimaryKey(dataCalculo, idCarteira))
            {
                if (carteira.TipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaCalculo = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaCalculo = historicoCota.CotaFechamento.Value;
                }
            }
            else
            {
                throw new Exception("Cota não encontrada para carteira " + idCarteira.ToString() + " no dia " + dataCalculo.ToString());
            }
            #endregion

            if (!isentoIOF)
            {
                iof = CalculaIOFFundo(quantidade, cotaCalculo, cotaAplicacao, dataCalculo, dataAplicacao);
            }

            rendimento = quantidade * (cotaCalculo - cotaAplicacao) - iof;

            if (rendimento > 0)
            {
                ir = rendimento * (aliquota / 100);
            }

            this.ir = ir;
            this.iof = iof;
            this.rendimentoPosterior = rendimento;

            return true;
        }

        public void calculoAliquotaEspecifica(int idCarteira, DateTime dataAplicacao, DateTime dataCalculo, decimal quantidade, bool isentoIOF)
        {
            decimal ir = 0;
            decimal iof = 0;
            decimal prejuizoCompensar = 0;
            decimal cotaInicial = 0;
            decimal cotaFinal = 0;
            decimal cotaAplicacao = 0;
            decimal cotaCalculo = 0;
            decimal rendimento = 0;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            //Seleciona Primeira aliquota válida
            AliquotaEspecificaCollection aliquotaEspecificaCollection = new AliquotaEspecificaCollection();
            aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.IdCarteira.Equal(idCarteira));
            aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.DataValidade.LessThanOrEqual(dataAplicacao));
            aliquotaEspecificaCollection.Query.OrderBy(aliquotaEspecificaCollection.Query.DataValidade.Ascending);
            aliquotaEspecificaCollection.Query.es.Top = 1;
            aliquotaEspecificaCollection.Query.Load();

            if (aliquotaEspecificaCollection.Count == 0)
            {
                throw new Exception("Não foi encontrado cadastro de aliquota específica para a carteira " + idCarteira);
            }

            DateTime dataValida = aliquotaEspecificaCollection[0].DataValidade.Value;

            aliquotaEspecificaCollection.QueryReset();
            aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.IdCarteira.Equal(idCarteira));
            aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.DataValidade.GreaterThanOrEqual(dataValida));
            aliquotaEspecificaCollection.Query.Where(aliquotaEspecificaCollection.Query.DataValidade.LessThanOrEqual(dataCalculo));
            aliquotaEspecificaCollection.Query.OrderBy(aliquotaEspecificaCollection.Query.DataValidade.Ascending);
            aliquotaEspecificaCollection.Query.Load();

            if(aliquotaEspecificaCollection.Count == 0)
            {
                throw new Exception("Não foi encontrado cadastro de aliquota específica para a carteira " + idCarteira);
            }

            int contador = 0;

            foreach(AliquotaEspecifica aliquotaEspecifica in aliquotaEspecificaCollection)
            {
                contador++;

                #region cota aplicacao
                HistoricoCota historicoCota = new HistoricoCota();
                if(historicoCota.LoadByPrimaryKey(dataAplicacao, idCarteira))
                {
                    if (carteira.TipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        cotaAplicacao = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaAplicacao = historicoCota.CotaFechamento.Value;
                    }
                }
                else
                {
                    throw new Exception("Cota não encontrada para carteira " + idCarteira.ToString() + " no dia " + dataAplicacao.ToString());
                }
                #endregion

                if(cotaInicial == 0) cotaInicial = cotaAplicacao;

                #region cota calculo
                DateTime dataPesquisa = aliquotaEspecifica.DataValidade.Value;
                if (contador == aliquotaEspecificaCollection.Count)
                {
                    dataPesquisa = dataCalculo;
                }
                historicoCota.QueryReset();
                if (historicoCota.LoadByPrimaryKey(dataPesquisa, idCarteira))
                {
                    if (carteira.TipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        cotaCalculo = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaCalculo = historicoCota.CotaFechamento.Value;
                    }
                }
                else
                {
                    throw new Exception("Cota não encontrada para carteira " + idCarteira.ToString() + " no dia " + dataCalculo.ToString());
                }
                #endregion

                if (cotaFinal == 0) cotaFinal = cotaCalculo;

                if (!isentoIOF)
                {
                    iof = CalculaIOFFundo(quantidade, cotaCalculo, cotaAplicacao, dataPesquisa, dataAplicacao);
                }

                rendimento = quantidade * (cotaCalculo - cotaAplicacao) - iof;

                dataAplicacao = aliquotaEspecifica.DataValidade.Value;

                if (carteira.CompensacaoPrejuizo == (byte)TipoCompensacaoPrejuizo.Padrao)
                {
                    rendimento -= prejuizoCompensar;
                }

                if (rendimento > 0)
                {
                    ir += rendimento * (aliquotaEspecifica.Taxa.Value / 100);
                }
                else
                {
                    prejuizoCompensar = rendimento;
                }

                this.iof += iof;

            }
            this.ir = ir;
            this.rendimentoPosterior = quantidade * (cotaFinal - cotaInicial);
        }        
    }
}
