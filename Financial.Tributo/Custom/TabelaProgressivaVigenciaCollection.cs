/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:16:13
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Tributo
{
	public partial class TabelaProgressivaVigenciaCollection : esTabelaProgressivaVigenciaCollection
	{

        /// <summary>
        /// Seleciona a data vigente a partir de uma data
        /// </summary>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>        
        public void BuscaTabelaVigente(DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.query.Data.LessThanOrEqual(data));

            this.query.es.Top = 1;

            this.Query.Load();
        }

	}
}
