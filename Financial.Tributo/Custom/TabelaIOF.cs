﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;

namespace Financial.Tributo
{
	public partial class TabelaIOF : esTabelaIOF
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaIOF));

        /// <summary>
        /// Retorna a alíquota de IOF, dado o prazo em dias corridos entre a dataInicio e dataFim.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaAliquotaIOF(DateTime dataInicio, DateTime dataFim)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            int diasCorridos = Calendario.NumeroDias(dataInicio, dataFim);

            this.QueryReset();
            this.Query
                 .Select(this.query.AliquotaIOF)
                 .Where(this.query.Prazo == diasCorridos);

            this.Query.Load();
            
            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.query.es.LastQuery;
                sql = sql.Replace("@Prazo1", "'" + diasCorridos + "'");
                log.Info(sql);
            }
            #endregion
                        
            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nome Metodo: ");
            }
            #endregion

            if (!this.es.HasData)
            {
                return 0;
            }
            else
            {
                return this.AliquotaIOF.Value;
            }
        }

	}
}
