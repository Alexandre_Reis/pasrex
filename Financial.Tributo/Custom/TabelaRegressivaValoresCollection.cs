/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:16:13
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Tributo
{
	public partial class TabelaRegressivaValoresCollection : esTabelaRegressivaValoresCollection
	{
        /// <summary>
        /// Seleciona aliquota vigente de acordo com o periodos (anos).
        /// </summary>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>        
        public void BuscaTabelaVigente(int IdTabelaRegressiva, int anos)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdTabelaRegressiva.Equal(IdTabelaRegressiva))
                 .Where(this.Query.Periodo.LessThanOrEqual(anos))
                 .OrderBy(this.Query.Periodo.Descending);

            this.query.es.Top = 1;

            this.Query.Load();
        }
	}
}
