﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;

namespace Financial.Tributo
{
    public partial class ApuracaoIRImobiliario : esApuracaoIRImobiliario
	{
        /// <summary>
        /// Carrega o  objeto ApuracaoIRImobiliario com os campos PrejuizoCompensar, PrejuizoCompensarDT.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="mes"></param>
        /// <param name="ano"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPrejuizoMes(int idCliente, int mes, int ano)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PrejuizoCompensar, this.Query.PrejuizoCompensarDT)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Mes == mes,
                        this.Query.Ano == ano);

            this.Query.Load();

            return this.es.HasData;            
        }

        /// <summary>
        /// Carrega o  objeto ApuracaoIRImobiliario com o campo IRDayTradeCompensar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="mes"></param>
        /// <param name="ano"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaIRDayTradeCompensarMes(int idCliente, int mes, int ano)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IRDayTradeCompensar)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Mes == mes,
                        this.Query.Ano == ano);

            this.Query.Load();

            return this.es.HasData;
        }

        /// <summary>
        /// Deleta a ApuracaoIRImobiliario (o método pega o mês e ano pela data passada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void DeletaGanhoImobiliario(int idCliente, DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;

            ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollection = new ApuracaoIRImobiliarioCollection();
            
            apuracaoIRImobiliarioCollection.Query
                 .Select(apuracaoIRImobiliarioCollection.Query.IdCliente, 
                         apuracaoIRImobiliarioCollection.Query.Mes,
                         apuracaoIRImobiliarioCollection.Query.Ano)
                 .Where(apuracaoIRImobiliarioCollection.Query.IdCliente == idCliente,
                        apuracaoIRImobiliarioCollection.Query.Mes == mes,
                        apuracaoIRImobiliarioCollection.Query.Ano == ano);

            apuracaoIRImobiliarioCollection.Query.Load();

            apuracaoIRImobiliarioCollection.MarkAllAsDeleted();
            apuracaoIRImobiliarioCollection.Save();
        }

        /// <summary>
        /// Consolida todos os resultados em operações de FII e calcula o IR sobre operações normais e DT
        /// descontando o IR DayTrade e aplicando a compensação de prejuízos em períodos anteriores.
        /// O cálculo é feito do primeiro dia do mês até a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGanhoImobiliario(int idCliente, DateTime data)
        {
            //Deleto o registro na ApuracaoGanhoRendaVariavel referente ao mês/ano da data passada
            this.DeletaGanhoImobiliario(idCliente, data);

            //Primeiro dia corrido do mês
            DateTime dataInicio = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);

            #region Resultado Normal
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.BuscaTotalResultadoNormal(idCliente, dataInicio, data, TipoMercadoBolsa.Imobiliario);
            //Resultado em mercado a vista (operações normais)
            decimal resultadoFII = operacaoBolsa.ResultadoRealizado.Value;
            #endregion

            #region Resultado DayTrade
            operacaoBolsa = new OperacaoBolsa();
            decimal resultadoFII_DayTrade = operacaoBolsa.RetornaTotalResultadoDayTrade(idCliente, dataInicio, data, TipoMercadoBolsa.Imobiliario);
            #endregion

            decimal resultadoMes = resultadoFII;

            decimal resultadoMesDayTrade = resultadoFII_DayTrade;

            
            #region Mês e ano corrente e anterior
            int mes = data.Month;
            int ano = data.Year;
            int mesAnterior;
            int anoAnterior;
            if (mes == 1)
            {
                mesAnterior = 12;
                anoAnterior = ano - 1;
            }
            else
            {
                mesAnterior = mes - 1;
                anoAnterior = ano;
            }
            #endregion

            #region Busca os valores de prejuízo carregado no mês anterior
            decimal prejuizoMesAnterior = 0;
            decimal prejuizoDayTradeMesAnterior = 0;
            ApuracaoIRImobiliario apuracaoIRImobiliarioMesAnterior = new ApuracaoIRImobiliario();
            if (apuracaoIRImobiliarioMesAnterior.BuscaPrejuizoMes(idCliente, mesAnterior, anoAnterior))
            {
                prejuizoMesAnterior = apuracaoIRImobiliarioMesAnterior.PrejuizoCompensar.Value;
                prejuizoDayTradeMesAnterior = apuracaoIRImobiliarioMesAnterior.PrejuizoCompensarDT.Value;
            }
            #endregion

            #region Base de cálculo e apuração do IR (ou prezuizo a compensar para o proximo mes)
            decimal baseCalculo = 0;
            decimal baseCalculoDayTrade = 0;
            decimal prejuizoCompensar = 0;
            decimal prejuizoCompensarDayTrade = 0;

            if (resultadoMes - prejuizoMesAnterior > 0)
            {
                baseCalculo = resultadoMes - prejuizoMesAnterior;
            }
            else
            {
                prejuizoCompensar = Math.Abs(resultadoMes - prejuizoMesAnterior);
            }

            if (resultadoMesDayTrade - prejuizoDayTradeMesAnterior > 0)
            {
                baseCalculoDayTrade = resultadoMesDayTrade - prejuizoDayTradeMesAnterior;
            }
            else
            {
                prejuizoCompensarDayTrade = Math.Abs(resultadoMesDayTrade - prejuizoDayTradeMesAnterior);
            }

            decimal valorIR = 0;
            if (baseCalculo > 0)
            {
                //Valor do IR = 20% da base de cálculo final do mês
                valorIR = Utilitario.Truncate(baseCalculo * 0.20M, 2);
            }

            decimal valorIRDayTrade = 0;
            if (baseCalculoDayTrade > 0)
            {
                //Valor do IR = 20% da base de cálculo DayTrade final do mês
                valorIRDayTrade = Utilitario.Truncate(baseCalculoDayTrade * 0.20M, 2);
            }
            decimal totalIR = valorIR + valorIRDayTrade;
            #endregion

            #region Busca o valor de IR DayTrade a compensar do mês anterior
            decimal irDayTradeAnteriorCompensar = 0;

            if (mes > 1) //Não se leva IR fonte a compensar de ano fiscal para o seguinte
            {
                apuracaoIRImobiliarioMesAnterior = new ApuracaoIRImobiliario();
                if (apuracaoIRImobiliarioMesAnterior.BuscaIRDayTradeCompensarMes(idCliente, mesAnterior, anoAnterior))
                {
                    irDayTradeAnteriorCompensar = apuracaoIRImobiliarioMesAnterior.IRDayTradeCompensar.Value;
                }
            }
            #endregion

            #region Busca o valor de IR Fonte (0,005%) do mês
            IRFonte irFonte = new IRFonte();
            decimal irFonteMes = irFonte.RetornaValorIRFonte_FII(idCliente, dataInicio, data);

            if (irFonteMes < 1)
            {
                irFonteMes = 0; //Mantem a mesma logica de menor que 1 real, não calcula...nem paga!
            }
            #endregion

            #region Busca o valor de IR Fonte DayTrade do mês
            irFonte = new IRFonte();
            decimal irFonteDayTradeMes = irFonte.RetornaValorIRFonteDayTrade_FII(idCliente, dataInicio, data);
            #endregion

            //Soma o valor do IR Fonte do mês com o valor do IR Fonte a compensar do mês anterior
            //IR Fonte a compensar do mês anterior => IRFonte DT q não foi usado p/ abater da base do IR no mês anterior
            decimal irFonteDayTradeAbater = irFonteDayTradeMes + irDayTradeAnteriorCompensar;

            //Abate do valor de IR a pagar, o valor de IR Fonte DT a abater (IR Fonte DT do mês + mês anterior)
            decimal irPagar = totalIR - irFonteDayTradeAbater - irFonteMes;

            decimal irFonteCompensar = 0;
            if (irPagar < 0)
            {
                irFonteCompensar = Math.Abs(irPagar);
                irPagar = 0;
            }

            #region Insert na ApuracaoIRImobiliario
            ApuracaoIRImobiliario apuracaoIRImobiliarioInsert = new ApuracaoIRImobiliario();
            apuracaoIRImobiliarioInsert.IdCliente = idCliente;
            apuracaoIRImobiliarioInsert.Ano = ano;
            apuracaoIRImobiliarioInsert.Mes = (byte)mes;
            apuracaoIRImobiliarioInsert.ResultadoNormal = resultadoFII;
            apuracaoIRImobiliarioInsert.ResultadoDT = resultadoFII_DayTrade;
            
            apuracaoIRImobiliarioInsert.ResultadoLiquidoMes = resultadoMes;
            apuracaoIRImobiliarioInsert.ResultadoLiquidoMesDT = resultadoMesDayTrade;
            apuracaoIRImobiliarioInsert.ResultadoNegativoAnterior = prejuizoMesAnterior;
            apuracaoIRImobiliarioInsert.ResultadoNegativoAnteriorDT = prejuizoDayTradeMesAnterior;
            apuracaoIRImobiliarioInsert.BaseCalculo = baseCalculo;
            apuracaoIRImobiliarioInsert.BaseCalculoDT = baseCalculoDayTrade;
            apuracaoIRImobiliarioInsert.PrejuizoCompensar = prejuizoCompensar;
            apuracaoIRImobiliarioInsert.PrejuizoCompensarDT = prejuizoCompensarDayTrade;
            apuracaoIRImobiliarioInsert.ImpostoCalculado = valorIR;
            apuracaoIRImobiliarioInsert.ImpostoCalculadoDT = valorIRDayTrade;
            apuracaoIRImobiliarioInsert.TotalImposto = totalIR;
            apuracaoIRImobiliarioInsert.IRDayTradeMes = irFonteDayTradeMes;
            apuracaoIRImobiliarioInsert.IRDayTradeMesAnterior = irDayTradeAnteriorCompensar;
            apuracaoIRImobiliarioInsert.IRDayTradeCompensar = irFonteCompensar;
            apuracaoIRImobiliarioInsert.IRFonteNormal = irFonteMes;
            apuracaoIRImobiliarioInsert.ImpostoPagar = irPagar;
            //O imposto pago na verdade pode incluir outros valores que não constam do Ganho de Renda Variável
            apuracaoIRImobiliarioInsert.ImpostoPago = irFonteMes + irFonteDayTradeMes;

            apuracaoIRImobiliarioInsert.Save();
            #endregion
        }

        /// <summary>
        /// Lança em Liquidacao o valor de IR calculado do mes anterior.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaIRGanhoLiquidacao(int idCliente, DateTime data)
        {
            if (data.CompareTo(Calendario.RetornaUltimoDiaUtilMes(data)) != 0)
            {
                return;
            }

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            #region Mês e ano corrente e anterior
            int mes = data.Month;
            int ano = data.Year;
            int mesAnterior;
            int anoAnterior;
            if (mes == 1)
            {
                mesAnterior = 12;
                anoAnterior = ano - 1;
            }
            else
            {
                mesAnterior = mes - 1;
                anoAnterior = ano;
            }
            #endregion

            ApuracaoIRImobiliario apuracaoIRImobiliario = new ApuracaoIRImobiliario();
            if (apuracaoIRImobiliario.LoadByPrimaryKey(idCliente, anoAnterior, (byte)mesAnterior))
            {
                #region Nova Liquidacao
                if (apuracaoIRImobiliario.ImpostoPagar.Value > 0)
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "Pagto IR mês (Ganhos de Capital em FII)";
                    liquidacao.Valor = apuracaoIRImobiliario.ImpostoPagar.Value * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRRendaVariavel;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();
                }
                #endregion
            }
        }
	}
}
