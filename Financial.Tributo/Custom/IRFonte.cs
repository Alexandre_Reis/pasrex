﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Tributo.Enums;
using Financial.Common;
using Financial.BMF;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Investidor;
using Financial.Interfaces.Sinacor;
using Financial.BMF.Enums;

namespace Financial.Tributo {
    public partial class IRFonte : esIRFonte 
    {
        // Atributo const indicando o valor Minimo do IR
        /* Ate 1 real o IR é guardado -
               Quando somar 1 real deve ser liquidado - pago 
        */

        private const decimal valorMinimoIR = 1M;

        // Imposto sobre ações e Opções  = 0,005%
        private static decimal aliquotaIRVenda = 0.005M / 100M;

        // Imposto de renda Daytrade = 1%
        private static decimal aliquotaIRDaytrade = 1M / 100M;

        /// <summary>
        /// Property de AliquotaIRVenda.
        /// </summary>
        public static decimal AliquotaIRVenda {
            get { return IRFonte.aliquotaIRVenda; }
        }

        /// <summary>
        /// Property de AliquotaIRDaytrade.
        /// </summary>
        public static decimal AliquotaIRDaytrade {
            get { return IRFonte.aliquotaIRDaytrade; }
        }

        /// <summary>
        /// Calcula IR Fonte sobre venda de ações, Net de Opções e baixa de futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>
        private void CalculaIRFonte(int idCliente, DateTime data, int idAgenteCorretora) 
        {
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

            // IR Acões            
            decimal vendaAcoes = operacaoBolsa.BuscaValorSum(idCliente, data, TipoOperacaoBolsa.Venda,
                OrigemOperacaoBolsa.Primaria, TipoMercadoBolsa.MercadoVista, idAgenteCorretora);

            // IR Opções
            string tipoMercado = TipoMercadoBolsa.OpcaoCompra+"'" + "," + "'"+TipoMercadoBolsa.OpcaoVenda;
            decimal vendaOpcoes = operacaoBolsa.BuscaValorSum(idCliente, data, TipoOperacaoBolsa.Venda,
                OrigemOperacaoBolsa.Primaria, tipoMercado, idAgenteCorretora);

            tipoMercado = TipoMercadoBolsa.OpcaoCompra+"'"+ "," +"'"+TipoMercadoBolsa.OpcaoVenda;
            decimal compraOpcoes = operacaoBolsa.BuscaValorSum(idCliente, data, TipoOperacaoBolsa.Compra,
                OrigemOperacaoBolsa.Primaria, tipoMercado, idAgenteCorretora);

            decimal deltaOpcoes = vendaOpcoes - compraOpcoes;

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Query.Select(operacaoBMF.Query.ResultadoRealizado.Sum());
            operacaoBMF.Query.Where(operacaoBMF.Query.IdCliente.Equal(idCliente),
                                    operacaoBMF.Query.IdAgenteCorretora.Equal(idAgenteCorretora),
                                    operacaoBMF.Query.Data.Equal(data),
                                    operacaoBMF.Query.TipoMercado.Equal((byte)TipoMercadoBMF.Futuro));
            operacaoBMF.Query.Load();

            decimal resultadoBMF = 0;
            if (operacaoBMF.ResultadoRealizado.HasValue && operacaoBMF.ResultadoRealizado.Value > 0)
            {
                resultadoBMF = operacaoBMF.ResultadoRealizado.Value;
            }

            decimal irFonteAcoes = 0;
            decimal irFonteOpcoes = 0;
            decimal irFonteBMF = 0;
            decimal irFonteMes = 0;
            if (vendaAcoes > 0) {
                irFonteAcoes = Utilitario.Truncate((vendaAcoes * AliquotaIRVenda), 2);
            }

            if (deltaOpcoes > 0) {
                irFonteOpcoes = Utilitario.Truncate((deltaOpcoes * AliquotaIRVenda), 2);
            }

            if (resultadoBMF > 0)
            {
                irFonteBMF = Utilitario.Truncate((resultadoBMF * AliquotaIRVenda), 2);
            }

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            if (dataAnterior.Month == data.Month) //O que ocorreu no mês anterior é simplesmente descartado!
            {
                irFonteMes = this.RetornaValorIR(idCliente, dataAnterior, idAgenteCorretora);
            }

            DateTime dataVencimento;
            decimal valor = 0;
            if ((irFonteMes + irFonteAcoes + irFonteOpcoes + irFonteBMF) > valorMinimoIR) 
            {                
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //

                if (irFonteAcoes != 0) {
                    dataVencimento = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valor = irFonteAcoes * -1;

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataVencimento;
                    liquidacao.Descricao = "IR Fonte";
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }

                if (irFonteOpcoes != 0 || irFonteBMF != 0)
                {
                    //Junta IR Fonte de Opções Bolsa com IR Fonte de Futuros BMF
                    dataVencimento = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valor = (irFonteOpcoes + irFonteBMF) * -1;

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataVencimento;
                    liquidacao.Descricao = "IR Fonte";
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }

                /*
                Se o IRMes passou do valor minimo assume-se que os valores do mes
                até a data anterior ja foram liquidados, portanto nao lança o IRMes de novo
                */
                if (irFonteMes != 0 && irFonteMes < valorMinimoIR) {
                    valor = irFonteMes * -1;

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "IR Fonte";
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }
            }

            if (irFonteAcoes != 0) {
                #region Novo IRFonte
                IRFonte irFonte = new IRFonte();
                irFonte.AddNew();
                irFonte.IdCliente = idCliente;
                irFonte.Data = data;
                irFonte.Identificador = (Int16)IdentificadorIR.IRFonteAcoes;
                irFonte.IdAgente = idAgenteCorretora;
                irFonte.ValorBase = vendaAcoes;
                irFonte.ValorIR = irFonteAcoes;
                irFonte.Save();
                #endregion
            }

            if (irFonteOpcoes != 0) {
                #region Novo IRFonte
                IRFonte irFonte = new IRFonte();
                irFonte.AddNew();
                irFonte.IdCliente = idCliente;
                irFonte.Data = data;
                irFonte.Identificador = (Int16)IdentificadorIR.IRFonteOpcoes;
                irFonte.IdAgente = idAgenteCorretora;
                irFonte.ValorBase = deltaOpcoes;
                irFonte.ValorIR = irFonteOpcoes;
                irFonte.Save();
                #endregion
            }

            if (irFonteBMF != 0)
            {
                #region Novo IRFonte
                IRFonte irFonte = new IRFonte();
                irFonte.AddNew();
                irFonte.IdCliente = idCliente;
                irFonte.Data = data;
                irFonte.Identificador = (Int16)IdentificadorIR.IRFonteFuturos;
                irFonte.IdAgente = idAgenteCorretora;
                irFonte.ValorBase = resultadoBMF;
                irFonte.ValorIR = irFonteBMF;
                irFonte.Save();
                #endregion
            }
        }

        /// <summary>
        /// Calcula IR Fonte sobre venda de ações, Net de Opções e baixa de futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaIRFonte(int idCliente, DateTime data) 
        {
            //Deleta todos os IR Fonte Normal (0,005%) na data, para o cliente
            IRFonteCollection irFonteCollectionDeletar = new IRFonteCollection();
            irFonteCollectionDeletar.DeletaIRFonteNormal(idCliente, data);

            OperacaoBolsaCollection coll = new OperacaoBolsaCollection();
            coll.Query.Select(coll.Query.IdAgenteCorretora)
                 .Where(coll.Query.IdCliente == idCliente &
                        coll.Query.Data.Equal(data) &
                            (
                                (
                                    coll.Query.TipoOperacao == TipoOperacaoBolsa.Venda &
                                    coll.Query.Origem == OrigemOperacaoBolsa.Primaria &
                                    coll.Query.TipoMercado == TipoMercadoBolsa.MercadoVista
                                )
                                |
                                (
                                    coll.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.Compra) &
                                    coll.Query.Origem == OrigemOperacaoBolsa.Primaria &
                                    coll.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda)
                                )
                            )
                        );
            coll.Query.es.Distinct = true;
            coll.Query.Load();

            List<int> listaCorretoras = new List<int>();

            foreach (OperacaoBolsa operacaoBolsa in coll)
            {
                listaCorretoras.Add(operacaoBolsa.IdAgenteCorretora.Value);
            }

            OperacaoBMFCollection collBMF = new OperacaoBMFCollection();
            collBMF.Query.Select(collBMF.Query.IdAgenteCorretora)
                 .Where(collBMF.Query.IdCliente == idCliente &
                        collBMF.Query.Data.Equal(data) &
                            (
                                (
                                    collBMF.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.Compra) &
                                    collBMF.Query.Origem == OrigemOperacaoBMF.Primaria &
                                    collBMF.Query.TipoMercado == TipoMercadoBMF.Futuro
                                )                                
                            )
                        );
            collBMF.Query.es.Distinct = true;
            collBMF.Query.Load();

            foreach (OperacaoBMF operacaoBMF in collBMF)
            {
                if (!listaCorretoras.Contains(operacaoBMF.IdAgenteCorretora.Value))
                {
                    listaCorretoras.Add(operacaoBMF.IdAgenteCorretora.Value);
                }
            }

            // Calcula o Ir Fonte por Corretora
            foreach (int idAgente in listaCorretoras)
            {
                this.CalculaIRFonte(idCliente, data, idAgente);
            }
        }

        /// <summary>
        /// Calcula IR Fonte sobre venda de FII.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>
        private void CalculaIRFonte_FII(int idCliente, DateTime data, int idAgenteCorretora)
        {
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

            // IR FII
            decimal vendaFII = operacaoBolsa.BuscaValorSum(idCliente, data, TipoOperacaoBolsa.Venda,
                OrigemOperacaoBolsa.Primaria, TipoMercadoBolsa.Imobiliario, idAgenteCorretora);
                        
            decimal irFonteFII = 0;
            decimal irFonteMes = 0;
            if (vendaFII > 0)
            {
                irFonteFII = Utilitario.Truncate((vendaFII * aliquotaIRVenda), 2);
            }

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            if (dataAnterior.Month == data.Month) //O que ocorreu no mês anterior é simplesmente descartado!
            {
                irFonteMes = this.RetornaValorIR_FII(idCliente, dataAnterior, idAgenteCorretora);
            }

            DateTime dataVencimento;
            decimal valor = 0;
            if ((irFonteMes + irFonteFII) > valorMinimoIR)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //

                if (irFonteFII != 0)
                {
                    dataVencimento = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valor = irFonteFII * -1;

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataVencimento;
                    liquidacao.Descricao = "IR Fonte s/ FII";
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }
                
                /*
                Se o IRMes passou do valor minimo assume-se que os valores do mes
                até a data anterior ja foram liquidados, portanto nao lança o IRMes de novo
                */
                if (irFonteMes != 0 && irFonteMes < valorMinimoIR)
                {
                    valor = irFonteMes * -1;

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "IR Fonte s/ FII";
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }
            }

            if (irFonteFII != 0)
            {
                #region Novo IRFonte
                IRFonte irFonte = new IRFonte();
                irFonte.AddNew();
                irFonte.IdCliente = idCliente;
                irFonte.Data = data;
                irFonte.Identificador = (Int16)IdentificadorIR.IRFonteFII;
                irFonte.IdAgente = idAgenteCorretora;
                irFonte.ValorBase = vendaFII;
                irFonte.ValorIR = irFonteFII;
                irFonte.Save();
                #endregion
            }
            
        }

        /// <summary>
        /// Calcula IR Fonte sobre venda de FII.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaIRFonte_FII(int idCliente, DateTime data)
        {
            //Deleta todos os IR Fonte Normal (0,005%) na data, para o cliente
            IRFonteCollection irFonteCollectionDeletar = new IRFonteCollection();
            irFonteCollectionDeletar.DeletaIRFonteNormal_FII(idCliente, data);

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaIrFonte_FII(idCliente, data);
            // Calcula o Ir Fonte por Corretora
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                int idAgente = operacaoBolsaCollection[i].IdAgenteCorretora.Value;
                this.CalculaIRFonte_FII(idCliente, data, idAgente);
            }
        }

        /// <summary>
        /// Retorna o valor do IR Fonte do mês até a data em questão.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>        
        /// <returns></returns>
        public decimal RetornaValorIR(int idCliente, DateTime data, int idAgenteCorretora) 
        {        
            DateTime dataInicioMes = new DateTime(data.Year, data.Month, 01);

            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Between(dataInicioMes, data),
                        this.Query.Identificador.In((Int16)IdentificadorIR.IRFonteAcoes, (Int16)IdentificadorIR.IRFonteOpcoes),
                        this.Query.IdAgente == idAgenteCorretora);

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;

            return valorIR;
        }

        /// <summary>
        /// Retorna o valor do IR Fonte do mês até a data em questão.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>        
        /// <returns></returns>
        public decimal RetornaValorIR_FII(int idCliente, DateTime data, int idAgenteCorretora)
        {
            DateTime dataInicioMes = new DateTime(data.Year, data.Month, 01);

            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Between(dataInicioMes, data),
                        this.Query.Identificador.Equal((Int16)IdentificadorIR.IRFonteFII),
                        this.Query.IdAgente == idAgenteCorretora);

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;

            return valorIR;
        }

        /// <summary>
        /// Integra o IR Fonte direto da V_TBOMOVCL.
        /// Pressupoe que a Liquidacao seja preeenchida diretamente com o c/c do sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void IntegraIRFonteDayTradeSinacor(int idCliente, DateTime data)
        {
            List<int> codigosCliente = new List<int>();
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            codigosCliente = clienteBolsa.RetornaCodigosBovespa(idCliente);
            
            if (codigosCliente.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //

                //Busca o IdAgenteMercado default
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
                
                IRFonteCollection iRFonteCollection = new IRFonteCollection();
                iRFonteCollection.Query.Where(iRFonteCollection.Query.IdCliente.Equal(idCliente),
                                              iRFonteCollection.Query.Data.Equal(data),
                                              iRFonteCollection.Query.Identificador.Equal((Int16)IdentificadorIR.IRFonteDaytrade));  
                iRFonteCollection.Query.Load();
                iRFonteCollection.MarkAllAsDeleted();
                iRFonteCollection.Save();

                VTbomovclCollection vTbomovclCollection = new VTbomovclCollection();
                vTbomovclCollection.BuscaIRDayTradeRetido(codigosCliente, data);

                decimal irFonteDayTradeAcoes = 0;
                decimal irFonteDayTradeOpcoes = 0;
                decimal valorBaseIRAcoes = 0;
                decimal valorBaseIROpcoes = 0;

                DateTime dataLiquidacaoOpcoes = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                DateTime dataLiquidacaoAcoes = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                foreach (VTbomovcl vTbomovcl in vTbomovclCollection)
                {
                    if (vTbomovcl.VlIrretido.HasValue && vTbomovcl.VlBaseirdt.HasValue && vTbomovcl.VlIrretido.Value != 0)
                    {
                        if (vTbomovcl.PzProjCc > dataLiquidacaoOpcoes.AddDays(1)) //Soma 1 dia para assegurar que vai pegar somente ações mesmo no if
                        {
                            irFonteDayTradeAcoes += Math.Abs(vTbomovcl.VlIrretido.Value);
                            valorBaseIRAcoes += Math.Abs(vTbomovcl.VlBaseirdt.Value);                            
                        }
                        else
                        {
                            irFonteDayTradeOpcoes += Math.Abs(vTbomovcl.VlIrretido.Value);
                            valorBaseIROpcoes += Math.Abs(vTbomovcl.VlBaseirdt.Value);
                        }
                    }                    
                }

                //PARA FUTUROS E OPÇÕES DE BMF PODE SER CALCULADO SEM PROBLEMAS, POIS AS OPERAÇÕES JÁ VEM ESPECIFICACADAS CERTAS COMO DT
                OperacaoBMF operacaoBMF = new OperacaoBMF();
                decimal valorResultadoBMFDTOpcoes = operacaoBMF.RetornaTotalResultadoDayTrade(idCliente, idAgenteMercado, data, data);
                decimal ajusteDayTrade = operacaoBMF.RetornaTotalAjusteDayTrade(idCliente, idAgenteMercado, data, data);
                decimal valorResultadoBMFDT = valorResultadoBMFDTOpcoes + ajusteDayTrade;

                decimal irFonteDaytradeBMF = 0;
                if (valorResultadoBMFDT > 0)
                {
                    irFonteDaytradeBMF = Utilitario.Truncate(valorResultadoBMFDT * AliquotaIRDaytrade, 2);
                }
                else
                {
                    valorResultadoBMFDT = 0;
                }
                //

                decimal valorIR_D1 = irFonteDaytradeBMF + irFonteDayTradeOpcoes;
                decimal valorBaseD1 = valorResultadoBMFDT + valorBaseIROpcoes;
                if (valorIR_D1 != 0)
                {
                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacaoOpcoes;
                    liquidacao.Descricao = "IR Fonte DayTrade (Opções/Futuros)";
                    liquidacao.Valor = valorIR_D1 * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteMercado;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    liquidacao.InsereLiquidacao(liquidacao);
                }

                if (irFonteDayTradeAcoes != 0)
                {
                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacaoAcoes;
                    liquidacao.Descricao = "IR Fonte DayTrade (Ações)";
                    liquidacao.Valor = irFonteDayTradeAcoes * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteMercado;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    liquidacao.InsereLiquidacao(liquidacao);
                }

                #region Novo IRFonte (soma de opções/futuros + ações)
                IRFonte irFonte = new IRFonte();
                irFonte.IdCliente = idCliente;
                irFonte.Data = data;
                irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
                irFonte.ValorBase = valorBaseIRAcoes + valorBaseD1;
                irFonte.ValorIR = irFonteDayTradeAcoes + valorIR_D1;
                irFonte.IdAgente = idAgenteMercado;
                // Adiciona na collection
                irFonte.Save();
                #endregion
            }
        }

        /// <summary>
        /// Calcula IR Fonte DayTrade sobre operações na Bovespa e na BMF, compensando eventuais prejuízos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaIRFonteDayTrade(int idCliente, DateTime data) 
        {
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            // Usado para Salvar a collection no final do processo
            IRFonteCollection irFonteCollection = new IRFonteCollection();
            //

            //Deleta todos os IR Fonte DayTrade na data, para o cliente
            IRFonteCollection irFonteCollectionDeletar = new IRFonteCollection();
            irFonteCollectionDeletar.DeletaIRFonteDayTrade(idCliente, data);

            List<int> listaCorretoras = RetornaCorretorasOperadas(idCliente, data);
            
            for (int i = 0; i < listaCorretoras.Count; i++)
            {
                int idAgenteCorretora = listaCorretoras[i];
                
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                decimal valorResultadoBolsaDTAcoes = operacaoBolsa.RetornaTotalResultadoDayTradeAcoes(idCliente, 
                                                                                  idAgenteCorretora, data, data);

                DateTime dataVencimentoAcoes = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                decimal valorResultadoBolsaDTOpcoes = operacaoBolsa.RetornaTotalResultadoDayTradeOpcoes(idCliente,
                                                                                  idAgenteCorretora, data, data);

                DateTime dataVencimentoOpcoes = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                OperacaoBMF operacaoBMF = new OperacaoBMF();
                decimal valorResultadoBMFDTOpcoes = operacaoBMF.RetornaTotalResultadoDayTrade(idCliente, idAgenteCorretora,
                                                                                        data, data);

                decimal ajusteDayTrade = operacaoBMF.RetornaTotalAjusteDayTrade(idCliente, idAgenteCorretora,
                                                                                 data, data);

                decimal valorResultadoBMFDT = valorResultadoBMFDTOpcoes + ajusteDayTrade;

                DateTime dataVencimentoFuturosBMF = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.FuturosBMF, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                decimal valorResultadoTotalDT = valorResultadoBolsaDTAcoes + valorResultadoBolsaDTOpcoes + valorResultadoBMFDT;

                if (valorResultadoTotalDT > 0)
                {                    
                    //Os resultados são compensados, em bolsa e BMF, mas lançados separadamente de acordo com o mercado
                    if (valorResultadoBolsaDTAcoes < 0 || valorResultadoBolsaDTOpcoes < 0 || valorResultadoBMFDT < 0)
                    {
                        #region Tratamento quando algum dos resultados é < 0
                        if (valorResultadoBolsaDTAcoes > 0 && valorResultadoBolsaDTOpcoes <= 0 && valorResultadoBMFDT <= 0)
                        {
                            valorResultadoBolsaDTAcoes = valorResultadoTotalDT;
                        }
                        else if (valorResultadoBolsaDTAcoes <= 0 && valorResultadoBolsaDTOpcoes > 0 && valorResultadoBMFDT <= 0)
                        {
                            valorResultadoBolsaDTOpcoes = valorResultadoTotalDT;
                        }
                        else if (valorResultadoBolsaDTAcoes <= 0 && valorResultadoBolsaDTOpcoes <= 0 && valorResultadoBMFDT > 0)
                        {
                            valorResultadoBMFDT = valorResultadoTotalDT;
                        }
                        else //como última opção (mais de 1 mercado > 0), compensa em ações mesmo
                        {
                            valorResultadoBolsaDTAcoes = valorResultadoTotalDT;
                            //Precisa zerar os resultados abaixo, para não gerar duplicidade
                            valorResultadoBolsaDTOpcoes = 0;
                            valorResultadoBMFDT = 0;
                            //
                        }

                        if (valorResultadoBolsaDTAcoes > 0)
                        {
                            decimal irFonteDaytrade = Utilitario.Truncate(valorResultadoBolsaDTAcoes * AliquotaIRDaytrade, 2);

                            #region Novo IRFonte
                            IRFonte irFonte = new IRFonte();
                            irFonte.AddNew();
                            irFonte.IdCliente = idCliente;
                            irFonte.Data = data;
                            irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
                            irFonte.ValorBase = valorResultadoBolsaDTAcoes;
                            irFonte.ValorIR = irFonteDaytrade;
                            irFonte.IdAgente = idAgenteCorretora;
                            // Adiciona na collection
                            irFonteCollection.AttachEntity(irFonte);
                            #endregion

                            #region Nova Liquidacao
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataVencimentoAcoes;
                            liquidacao.Descricao = "IR Fonte DayTrade";
                            liquidacao.Valor = irFonteDaytrade * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgenteCorretora;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaDefault;
                            #endregion

                            // Insere uma Liquidacao 
                            liquidacao.InsereLiquidacao(liquidacao);
                        }

                        if (valorResultadoBolsaDTOpcoes > 0)
                        {
                            decimal irFonteDaytrade = Utilitario.Truncate(valorResultadoBolsaDTOpcoes * AliquotaIRDaytrade, 2);

                            #region Novo IRFonte
                            IRFonte irFonte = new IRFonte();
                            irFonte.AddNew();
                            irFonte.IdCliente = idCliente;
                            irFonte.Data = data;
                            irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
                            irFonte.ValorBase = valorResultadoBolsaDTOpcoes;
                            irFonte.ValorIR = irFonteDaytrade;
                            irFonte.IdAgente = idAgenteCorretora;
                            // Adiciona na collection
                            irFonteCollection.AttachEntity(irFonte);
                            #endregion

                            #region Nova Liquidacao
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataVencimentoOpcoes;
                            liquidacao.Descricao = "IR Fonte DayTrade";
                            liquidacao.Valor = irFonteDaytrade * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgenteCorretora;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaDefault;
                            #endregion

                            // Insere uma Liquidacao 
                            liquidacao.InsereLiquidacao(liquidacao);
                        }

                        if (valorResultadoBMFDT > 0)
                        {
                            decimal irFonteDaytrade = Utilitario.Truncate(valorResultadoBMFDT * AliquotaIRDaytrade, 2);

                            #region Novo IRFonte
                            IRFonte irFonte = new IRFonte();
                            irFonte.AddNew();
                            irFonte.IdCliente = idCliente;
                            irFonte.Data = data;
                            irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
                            irFonte.ValorBase = valorResultadoBMFDT;
                            irFonte.ValorIR = irFonteDaytrade;
                            irFonte.IdAgente = idAgenteCorretora;
                            // Adiciona na collection
                            irFonteCollection.AttachEntity(irFonte);
                            #endregion

                            #region Nova Liquidacao
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataVencimentoFuturosBMF;
                            liquidacao.Descricao = "IR Fonte DayTrade";
                            liquidacao.Valor = irFonteDaytrade * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgenteCorretora;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaDefault;
                            #endregion

                            // Insere uma Liquidacao 
                            liquidacao.InsereLiquidacao(liquidacao);
                        }
                        #endregion
                    }                    
                    else
                    {
                        #region Tratamento quando todos os resultados são > 0
                        decimal irFonteDaytrade = Utilitario.Truncate(valorResultadoTotalDT * AliquotaIRDaytrade, 2);

                        #region Novo IRFonte
                        IRFonte irFonte = new IRFonte();
                        irFonte.AddNew();
                        irFonte.IdCliente = idCliente;
                        irFonte.Data = data;
                        irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
                        irFonte.ValorBase = valorResultadoTotalDT;
                        irFonte.ValorIR = irFonteDaytrade;
                        irFonte.IdAgente = idAgenteCorretora;
                        // Adiciona na collection
                        irFonteCollection.AttachEntity(irFonte);
                        #endregion

                        DateTime dataLiquidacao = new DateTime();
                        if (valorResultadoBolsaDTAcoes > 0)
                        {
                            dataLiquidacao = dataVencimentoAcoes;
                        }
                        else
                        {
                            dataLiquidacao = dataVencimentoFuturosBMF;
                        }

                        #region Nova Liquidacao
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataLiquidacao;
                        liquidacao.Descricao = "IR Fonte DayTrade";
                        liquidacao.Valor = irFonteDaytrade * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgenteCorretora;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idContaDefault;
                        #endregion

                        // Insere uma Liquidacao 
                        liquidacao.InsereLiquidacao(liquidacao);
                        #endregion
                    }
                    
                }
            }

            // Salva IrFonte
            irFonteCollection.Save();
        }
        
        /// <summary>
        /// Retorna o valor do IR Fonte DayTrade do período especificado.
        /// Filtra somente com Identificador = IdentificadorIR.IRFonteDaytrade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaValorIRFonteDayTrade(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.Identificador.In((Int16)IdentificadorIR.IRFonteDaytrade)
                        );

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;
                        
            return valorIR;
        }

        /// <summary>
        /// Calcula IR Fonte DayTrade sobre operações com FII, compensando eventuais prejuízos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaIRFonteDayTrade_FII(int idCliente, DateTime data)
        {
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            // Usado para Salvar a collection no final do processo
            IRFonteCollection irFonteCollection = new IRFonteCollection();
            //

            //Deleta todos os IR Fonte DayTrade na data, para o cliente
            IRFonteCollection irFonteCollectionDeletar = new IRFonteCollection();
            irFonteCollectionDeletar.DeletaIRFonteDayTrade_FII(idCliente, data);

            List<int> listaCorretoras = RetornaCorretorasOperadas(idCliente, data);

            for (int i = 0; i < listaCorretoras.Count; i++)
            {
                int idAgenteCorretora = listaCorretoras[i];

                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                decimal valorResultadoDT = operacaoBolsa.RetornaTotalResultadoDayTradeFII(idCliente, idAgenteCorretora, data, data);

                DateTime dataVencimento = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                
                if (valorResultadoDT > 0)
                {
                    decimal irFonteDaytrade = Utilitario.Truncate(valorResultadoDT * AliquotaIRDaytrade, 2);

                    #region Novo IRFonte
                    IRFonte irFonte = new IRFonte();
                    irFonte.AddNew();
                    irFonte.IdCliente = idCliente;
                    irFonte.Data = data;
                    irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDayTrade_FII;
                    irFonte.ValorBase = valorResultadoDT;
                    irFonte.ValorIR = irFonteDaytrade;
                    irFonte.IdAgente = idAgenteCorretora;
                    // Adiciona na collection
                    irFonteCollection.AttachEntity(irFonte);
                    #endregion

                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    
                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "IR Fonte DayTrade s/ FII";
                    liquidacao.Valor = irFonteDaytrade * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteCorretora;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    // Insere uma Liquidacao 
                    liquidacao.InsereLiquidacao(liquidacao);
                }
            }

            // Salva IrFonte
            irFonteCollection.Save();
        }

        /// <summary>
        /// Retorna o valor do IR Fonte DayTrade do período especificado.
        /// Filtra somente com Identificador = IdentificadorIR.IRFonteDaytrade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaValorIRFonteDayTrade_FII(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.Identificador.In((Int16)IdentificadorIR.IRFonteDayTrade_FII)
                        );

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;

            return valorIR;
        }

        /// <summary>
        /// Retorna o valor do IR Fonte do período especificado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaValorIRFonte(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.Identificador.In((Int16)IdentificadorIR.IRFonteAcoes,
                                                    (Int16)IdentificadorIR.IRFonteOpcoes,
                                                    (Int16)IdentificadorIR.IRFonteFuturos)
                        );

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;

            return valorIR;
        }

        /// <summary>
        /// Retorna o valor do IR Fonte do período especificado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaValorIRFonte_FII(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorIR.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.Identificador.Equal((Int16)IdentificadorIR.IRFonteFII)
                        );

            this.Query.Load();

            decimal valorIR = (this.ValorIR.HasValue) ? this.ValorIR.Value : 0;

            return valorIR;
        }

        /// <summary>
        /// Retorna todas as corretoras onde tiveram operações de bolsa ou BMF na data.
        /// Se operou na mesma corretora em bolsa e BMF, retorna essa corretora apenas 1 vez na collection.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns>Lista de corretoras</returns>
        public List<int> RetornaCorretorasOperadas(int idCliente, DateTime data)
        {
            List<int> listaCorretoras = new List<int>();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaCorretorasOperadasDayTrade(idCliente, data);

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                int idAgenteCorretora = operacaoBolsa.IdAgenteCorretora.Value;

                listaCorretoras.Add(idAgenteCorretora);
            }

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.BuscaCorretorasOperadasDayTrade(idCliente, data);

            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                int idAgenteCorretora = operacaoBMF.IdAgenteCorretora.Value;

                if (!listaCorretoras.Contains(idAgenteCorretora))
                    listaCorretoras.Add(idAgenteCorretora);
            }

            return listaCorretoras;
        }
    }
}

