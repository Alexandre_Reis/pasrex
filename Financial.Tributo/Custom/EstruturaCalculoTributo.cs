﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.InvestidorCotista.Enums;

namespace Financial.Tributo.Custom
{
    /// <summary>
    /// Usada para organizar os parâmetros a serem passados nos métodos de cálculo de IR.
    /// </summary>
    public class CalculoIRFundoRendaFixa
    {
        #region Atributos da classe
        int? idFundo;
        int? idPosicao;
        int? tipoTributacao;
        int? tipoCota;
        decimal? quantidade;
        decimal? quantidadeAntesCortes;
        decimal? quantidadePosicao;
        decimal? cotaCalculo;
        decimal? cotaAplicacao;
        DateTime? dataConversao;
        DateTime? dataLiquidacao;
        DateTime? dataCalculo;
        DateTime? dataAplicacao;
        DateTime? dataUltimaCobrancaIR;
        decimal? iofVirtual;
        decimal? prejuizoCompensar;
        decimal? valorLiquido;
        TipoProcessaResgate? tipoProcessamentoResgate;
        int? diasLiquidacaoResgate;
        int? diasLiquidacaoComeCotas;
        int? idTipo;
        bool? isentoIOF;
        bool? tributaNaoResidente;
        bool? projecaoIR;

        bool? fie;
        DateTime? dataOperacaoResgate;
        DateTime? dataConversaoResgate;
        DateTime? dataLiquidacaoResgate;

        bool? desconsideraUltComeCotas;

        #endregion

        #region Properties da classe
        public int? IdFundo
        {
            get { return idFundo; }
            set { idFundo = value; }
        }

        public int? IdPosicao
        {
            get { return idPosicao; }
            set { idPosicao = value; }
        }

        public int? TipoTributacao
        {
            get { return tipoTributacao; }
            set { tipoTributacao = value; }
        }

        public int? TipoCota
        {
            get { return tipoCota; }
            set { tipoCota = value; }
        }

        public decimal? Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal? QuantidadeAntesCortes
        {
            get { return quantidadeAntesCortes; }
            set { quantidadeAntesCortes = value; }
        }

        public decimal? QuantidadePosicao
        {
            get { return quantidadePosicao; }
            set { quantidadePosicao = value; }
        }

        public decimal? CotaCalculo
        {
            get { return cotaCalculo; }
            set { cotaCalculo = value; }
        }

        public decimal? CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }

        public DateTime? DataCalculo
        {
            get { return dataCalculo; }
            set { dataCalculo = value; }
        }

        public DateTime? DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }

        public DateTime? DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        public DateTime? DataOperacaoResgate
        {
            get { return dataOperacaoResgate; }
            set { dataOperacaoResgate = value; }
        }

        public DateTime? DataConversaoResgate
        {
            get { return dataConversaoResgate; }
            set { dataConversaoResgate = value; }
        }

        public DateTime? DataLiquidacaoResgate
        {
            get { return dataLiquidacaoResgate; }
            set { dataLiquidacaoResgate = value; }
        }

        public DateTime? DataUltimaCobrancaIR
        {
            get { return dataUltimaCobrancaIR; }
            set { dataUltimaCobrancaIR = value; }
        }

        public decimal? IOFVirtual
        {
            get { return iofVirtual; }
            set { iofVirtual = value; }
        }

        public decimal? PrejuizoCompensar
        {
            get { return prejuizoCompensar; }
            set { prejuizoCompensar = value; }
        }

        public decimal? ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }

        public TipoProcessaResgate? TipoProcessamentoResgate
        {
            get { return tipoProcessamentoResgate; }
            set { tipoProcessamentoResgate = value; }
        }

        public int? DiasLiquidacaoResgate
        {
            get { return diasLiquidacaoResgate; }
            set { diasLiquidacaoResgate = value; }
        }

        public int? DiasLiquidacaoComeCotas
        {
            get { return diasLiquidacaoComeCotas; }
            set { diasLiquidacaoComeCotas = value; }
        }

        public int? IdTipo
        {
            get { return idTipo; }
            set { idTipo = value; }
        }

        public bool? IsentoIOF
        {
            get { return isentoIOF; }
            set { isentoIOF = value; }
        }

        public bool? TributaNaoResidente
        {
            get { return tributaNaoResidente; }
            set { tributaNaoResidente = value; }
        }

        public bool? ProjecaoIR
        {
            get { return projecaoIR; }
            set { projecaoIR = value; }
        }


        public bool? Fie
        {
            get { return fie; }
            set { fie = value; }
        }


        public bool? DesconsideraUltComeCotas
        {
            get { return desconsideraUltComeCotas; }
            set { desconsideraUltComeCotas = value; }
        }

        #endregion

        public override string ToString() {
            base.ToString();
            return "IdFundo= " + this.idFundo + " " +
                   "tipoTributacao= " + this.tipoTributacao;
        }
    }

    /// <summary>
    /// Usada para organizar os parâmetros a serem passados nos métodos de cálculo de IR.
    /// </summary>
    public class CalculoIRFundoAcoes
    {
        #region Atributos da classe
        decimal? quantidade;
        decimal? cotaCalculo;
        decimal? cotaAplicacao;
        decimal? prejuizoCompensar;
        bool? tributaNaoResidente;
        DateTime? dataCalculo;
        int? idPosicao;
        DateTime dataAplicacao;
        int idCarteira;
        int tipoInvestidor;
        #endregion

        #region Properties da classe
        public DateTime DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }

        public DateTime? DataCalculo
        {
            get { return dataCalculo; }
            set { dataCalculo = value; }
        }

        public decimal? Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal? CotaCalculo
        {
            get { return cotaCalculo; }
            set { cotaCalculo = value; }
        }

        public decimal? CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }

        public decimal? PrejuizoCompensar
        {
            get { return prejuizoCompensar; }
            set { prejuizoCompensar = value; }
        }

        public bool? TributaNaoResidente
        {
            get { return tributaNaoResidente; }
            set { tributaNaoResidente = value; }
        }

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public int? IdPosicao
        {
            get { return idPosicao; }
            set { idPosicao = value; }
        }

        public int TipoInvestidor
        {
            get { return tipoInvestidor; }
            set { tipoInvestidor = value; }
        }

        #endregion

        public bool HasValue()
        {
            return quantidade.HasValue && cotaCalculo.HasValue && cotaAplicacao.HasValue && prejuizoCompensar.HasValue;
        }
    }

    /// <summary>
    /// Usada para organizar os parâmetros a serem passados nos métodos de cálculo de IOF.
    /// </summary>
    public class CalculoIOF
    {
        #region Atributos da classe
        int? idFundo;
        decimal? quantidade;
        decimal? cotaCalculo;
        decimal? cotaAplicacao;
        DateTime? dataCalculo;
        DateTime? dataAplicacao;
        int? diasLiquidacaoResgate;
        #endregion

        #region Properties da classe
        public int? IdFundo
        {
            get { return idFundo; }
            set { idFundo = value; }
        }

        public decimal? Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal? CotaCalculo
        {
            get { return cotaCalculo; }
            set { cotaCalculo = value; }
        }

        public decimal? CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }

        public DateTime? DataCalculo
        {
            get { return dataCalculo; }
            set { dataCalculo = value; }
        }

        public DateTime? DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }

        public int? DiasLiquidacaoResgate
        {
            get { return diasLiquidacaoResgate; }
            set { diasLiquidacaoResgate = value; }
        }
        #endregion

        public bool HasValue()
        {
            return idFundo.HasValue && quantidade.HasValue && dataCalculo.HasValue && dataAplicacao.HasValue &&
                   cotaCalculo.HasValue && cotaAplicacao.HasValue;
        }
    }
}
