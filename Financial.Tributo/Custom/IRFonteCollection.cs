﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           5/12/2006 15:51:20
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Tributo.Enums;

namespace Financial.Tributo
{
	public partial class IRFonteCollection : esIRFonteCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(IRFonteCollection));

        /// <summary>
        /// Deleta IR Fonte sobre operações normais e baixa de futuros (0,005%).
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        public void DeletaIRFonteNormal(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(data),
                        this.Query.Identificador.In((int)IdentificadorIR.IRFonteAcoes, 
                                                    (int)IdentificadorIR.IRFonteOpcoes,
                                                    (int)IdentificadorIR.IRFonteFuturos));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta IR Fonte sobre operações daytrade (1%).
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        public void DeletaIRFonteDayTrade(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.Identificador.Equal(IdentificadorIR.IRFonteDaytrade));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta IR Fonte sobre operações normais de FII.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        public void DeletaIRFonteNormal_FII(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(data),
                        this.Query.Identificador.Equal((int)IdentificadorIR.IRFonteFII));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta IR Fonte sobre operações daytrade (1%) de FII.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        public void DeletaIRFonteDayTrade_FII(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.Identificador.Equal(IdentificadorIR.IRFonteDayTrade_FII));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
