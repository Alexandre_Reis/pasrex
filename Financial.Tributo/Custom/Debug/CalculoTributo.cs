using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Fundo;
using log4net;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.Tributo.Enums;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Core;
using Financial.Investidor.Enums;

namespace Financial.Tributo.Debug.CalculoTributo
{
    public class DebugCalculaIRFundoRendaFixa
    {
        public decimal? ValorIOF = 0;
        public decimal? RendimentoAnterior = 0;
        public decimal? RendimentoPosterior = 0;
        public decimal? PrejuizoCompensar = 0;
        public decimal? TotalPrejuizoUsado = 0;
        public decimal? RendimentoComeCotas = 0;
        public decimal? CotaUltimoComeCotas = 0;
        public decimal? ValorIR = 0;
        public bool? IsFDIC = false;
        public DateTime? DataCalculo = new DateTime(1900,1,1);
        public decimal? CotaCalculo = 0;
        public decimal? AliquotaIR = 0;
        public decimal? AliquotaComeCotas = 0;
        public decimal? TotalRendimentoCompensado = 0;
        public decimal? TotalRendimento = 0;
        public List<DebugComeCotas> DebugComeCotasList;

        public DebugCalculaIRFundoRendaFixa()
        {
            this.DebugComeCotasList = new List<DebugComeCotas>();
        }
    }

    public class DebugComeCotas
    {
        public DateTime? DataProximoComeCotas;
        public DateTime? DataFocal;
        public DateTime? DataAnterior;
        public decimal? ValorIOFAnterior = 0;
        public decimal? QuantidadeFocal;
        public decimal? TotalIRRestoPagar;
        public decimal? RendimentoAnterior;
        public decimal? UltimoIRCheio = 0;
        public decimal? CotaComeCotasAnterior;
        public bool? IsDataComeCotas;
        public decimal? CotaFocal;
        public decimal? QuantidadeComeCotas;
        public decimal? IRComeCotas;
        public decimal? IRCheio;
        public decimal? IRRestoPagar;
        public decimal? RendimentoCompensado;
        public decimal? PrejuizoUsadoComeCotas;
        public DateTime? DataComeCotasPosterior;
        public decimal? ResidualRendimento = 0;
        public decimal? ResidualIRCompensado = 0;
    }

}
