﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Investidor.Enums;
using Financial.Common;
using Financial.Util.Enums;
using Financial.Common.Enums;


namespace Financial.Tributo
{
	public partial class ApuracaoRendaVariavelIR : esApuracaoRendaVariavelIR
	{
        /// <summary>
        /// Carrega o  objeto ApuracaoRendaVariavelIR com os campos PrejuizoCompensar, PrejuizoCompensarDT.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="mes"></param>
        /// <param name="ano"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPrejuizoMes(int idCliente, int mes, int ano)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PrejuizoCompensar, this.Query.PrejuizoCompensarDT, this.Query.ResultadoLiquidoMes)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Mes == mes,
                        this.Query.Ano == ano);

            this.Query.Load();

            return this.es.HasData;
            
        }

        /// <summary>
        /// Carrega o  objeto ApuracaoRendaVariavelIR com o campo IRDayTradeCompensar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="mes"></param>
        /// <param name="ano"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaIRDayTradeCompensarMes(int idCliente, int mes, int ano)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IRDayTradeCompensar)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Mes == mes,
                        this.Query.Ano == ano);

            this.Query.Load();

            return this.es.HasData;
        }

        /// <summary>
        /// Deleta a ApuracaoGanhoRendaVariavel (o método pega o mês e ano pela data passada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void DeletaGanhoRendaVariavel(int idCliente, DateTime data)
        {
            int ano = data.Year;
            int mes = data.Month;

            ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollection = new ApuracaoRendaVariavelIRCollection();
            
            apuracaoRendaVariavelIRCollection.Query
                 .Select(apuracaoRendaVariavelIRCollection.Query.IdCliente, 
                         apuracaoRendaVariavelIRCollection.Query.Mes,
                         apuracaoRendaVariavelIRCollection.Query.Ano)
                 .Where(apuracaoRendaVariavelIRCollection.Query.IdCliente == idCliente,
                        apuracaoRendaVariavelIRCollection.Query.Mes == mes,
                        apuracaoRendaVariavelIRCollection.Query.Ano == ano);

            apuracaoRendaVariavelIRCollection.Query.Load();

            apuracaoRendaVariavelIRCollection.MarkAllAsDeleted();
            apuracaoRendaVariavelIRCollection.Save();
        }

        /// <summary>
        /// Consolida todos os resultados em operações de Bolsa e BMF e calcula o IR sobre operações normais e DT
        /// descontando o IR DayTrade e aplicando a compensação de prejuízos em períodos anteriores.
        /// O cálculo é feito do primeiro dia do mês até a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGanhoRendaVariavel(int idCliente, DateTime data)
        {
            //Deleto o registro na ApuracaoGanhoRendaVariavel referente ao mês/ano da data passada
            this.DeletaGanhoRendaVariavel(idCliente, data);

            //Primeiro dia corrido do mês
            DateTime dataInicio = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);

            #region Resultado Normal em Ações + Exercicio Opções oriundo de "trava" com termo
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.BuscaTotalResultadoNormal(idCliente, dataInicio, data, TipoMercadoBolsa.MercadoVista);
            //Resultado em mercado a vista (operações normais)
            decimal resultadoAcoes = operacaoBolsa.ResultadoRealizado.Value;
            
            //Soma o resultado em mercado a vista com o resultado de exercício
            decimal resultadoExercicio = operacaoBolsa.ResultadoExercicio.Value;
            resultadoAcoes += resultadoExercicio;
            #endregion
            
            //Resultados de termo serão demonstrados à parte do a vista            
            decimal resultadoTermo = operacaoBolsa.ResultadoTermo.Value;

            decimal resultadoETF = operacaoBolsa.RetornaTotalResultadoETF(idCliente, dataInicio, data);

            resultadoAcoes -= resultadoETF;
            
            #region Resultado Normal em Opções e Pó (OPC e OPV)
            operacaoBolsa = new OperacaoBolsa();
            string tipoMercadoOpcaoBolsa = TipoMercadoBolsa.OpcaoCompra + "','" + TipoMercadoBolsa.OpcaoVenda;
            operacaoBolsa.BuscaTotalResultadoNormal(idCliente, dataInicio, data, tipoMercadoOpcaoBolsa);
            //Resultado em opções (operações normais)
            decimal resultadoOpcoes = operacaoBolsa.ResultadoRealizado.Value;
            #endregion

            #region Resultado DayTrade em Ações
            operacaoBolsa = new OperacaoBolsa();
            decimal resultadoAcoesDayTrade = operacaoBolsa.RetornaTotalResultadoDayTrade(idCliente, dataInicio, data, TipoMercadoBolsa.MercadoVista);
            #endregion

            #region Resultado DayTrade em Opções (OPC e OPV)
            operacaoBolsa = new OperacaoBolsa();
            decimal resultadoOpcoesDayTrade = operacaoBolsa.RetornaTotalResultadoDayTrade(idCliente, dataInicio, data, tipoMercadoOpcaoBolsa);
            #endregion

            #region Resultado Normal em Ouro
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            decimal resultadoVistaBMF = operacaoBMF.RetornaTotalResultadoNormal(idCliente, dataInicio, data, TipoMercadoBMF.Disponivel);            
            #endregion

            #region Resultado DayTrade em Ouro
            operacaoBMF = new OperacaoBMF();
            decimal resultadoVistaBMFDayTrade = operacaoBMF.RetornaTotalResultadoDayTrade(idCliente, dataInicio, data, TipoMercadoBMF.Disponivel);
            #endregion

            #region Resultado Normal em Opções de BMF (sobre Disponivel e sobre Futuro)
            operacaoBMF = new OperacaoBMF();
            decimal resultadoOpcoesBMF = operacaoBMF.RetornaTotalResultadoNormalOpcoes(idCliente, dataInicio, data);
            #endregion

            #region Resultado DayTrade em Opções de BMF (sobre Disponivel e sobre Futuro)
            operacaoBMF = new OperacaoBMF();
            decimal resultadoOpcoesBMFDayTrade = operacaoBMF.RetornaTotalResultadoDayTradeOpcoes(idCliente, dataInicio, data);
            #endregion

            #region Resultado em Futuros de Dólar (Ajustes de operação)
            decimal ajusteDolar = operacaoBMF.RetornaTotalAjusteNormal(idCliente, dataInicio, data, "DOL', 'WDL");            
            #endregion

            #region Resultado em Futuros de Indice (Ajustes de posição e operação)
            decimal ajusteIndice = operacaoBMF.RetornaTotalAjusteNormal(idCliente, dataInicio, data, "IND', 'WIN");
            #endregion

            #region Resultado em Futuros de Juros (Ajustes de posição e operação)
            decimal ajusteJuros = operacaoBMF.RetornaTotalAjusteNormal(idCliente, dataInicio, data, "DI1', 'DDI");            
            #endregion

            #region Resultado em Futuros de Outros ativos, inclusive Agro (Ajustes de posição e operação)
            decimal ajusteOutros = operacaoBMF.RetornaTotalAjusteNormalOutros(idCliente, dataInicio, data);
            #endregion

            #region Resultado em Futuros de Dólar DayTrade (Ajustes de operação Daytrade)
            decimal ajusteDolarDayTrade = operacaoBMF.RetornaTotalAjusteDayTrade(idCliente, dataInicio, data, "DOL', 'WDL");
            #endregion

            #region Resultado em Futuros de Indice DayTrade (Ajustes de operação Daytrade)
            decimal ajusteIndiceDayTrade = operacaoBMF.RetornaTotalAjusteDayTrade(idCliente, dataInicio, data, "IND', 'WIN");
            #endregion

            #region Resultado em Futuros de Juros DayTrade (Ajustes de operação Daytrade)
            decimal ajusteJurosDayTrade = operacaoBMF.RetornaTotalAjusteDayTrade(idCliente, dataInicio, data, "DI1', 'DDI");
            #endregion

            #region Resultado em Futuros de Outros ativos DayTrade, inclusive Agro (Ajustes de operação Daytrade)
            decimal ajusteOutrosDayTrade = operacaoBMF.RetornaTotalAjusteDayTradeOutros(idCliente, dataInicio, data);
            #endregion

            decimal resultadoMes = resultadoAcoes + resultadoETF + resultadoTermo + resultadoOpcoes + resultadoVistaBMF +
                                   resultadoOpcoesBMF + ajusteDolar + ajusteIndice + ajusteJuros +
                                   ajusteOutros;
                                   
            decimal resultadoMesDayTrade = resultadoAcoesDayTrade + resultadoOpcoesDayTrade + 
                                           resultadoOpcoesBMFDayTrade + ajusteDolarDayTrade + ajusteIndiceDayTrade +
                                           ajusteJurosDayTrade + ajusteOutrosDayTrade;

            #region Mês e ano corrente e anterior
            int mes = data.Month;
            int ano = data.Year;
            int mesAnterior;
            int anoAnterior;
            if (mes == 1)
            {
                mesAnterior = 12;
                anoAnterior = ano - 1;
            }
            else
            {
                mesAnterior = mes - 1;
                anoAnterior = ano;
            }
            #endregion

            #region Busca os valores de prejuízo carregado no mês anterior
            decimal prejuizoMesAnterior = 0;
            decimal prejuizoDayTradeMesAnterior = 0;
            decimal resultadoMesAnterior = 0;
            ApuracaoRendaVariavelIR apuracaoRendaVariavelIRMesAnterior = new ApuracaoRendaVariavelIR();
            if (apuracaoRendaVariavelIRMesAnterior.BuscaPrejuizoMes(idCliente, mesAnterior, anoAnterior))
            {
                prejuizoMesAnterior = apuracaoRendaVariavelIRMesAnterior.PrejuizoCompensar.Value;
                prejuizoDayTradeMesAnterior = apuracaoRendaVariavelIRMesAnterior.PrejuizoCompensarDT.Value;
                resultadoMesAnterior = apuracaoRendaVariavelIRMesAnterior.ResultadoLiquidoMes.Value;
            }
            #endregion

            //Tratamento para volume de vendas em ações < 20.000 reais
            if (resultadoAcoes > 0)
            {
                bool aplicaIsencaoIRAcoes = ParametrosConfiguracaoSistema.Bolsa.IsencaoIRAcoes;

                if (aplicaIsencaoIRAcoes)
                {
                    DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    DateTime dataFimMes = Calendario.RetornaUltimoDiaCorridoMes(data, 0);
                    OperacaoBolsa operacaoBolsaVendas = new OperacaoBolsa();
                    decimal valorVendasAcoes = operacaoBolsaVendas.RetornaTotalVendasAcoes(idCliente, dataInicioMes, dataFimMes);

                    decimal resultadoASubtrair = 0;
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdTipo);
                    cliente.LoadByPrimaryKey(campos, idCliente);

                    if (cliente.IdTipo == (byte)TipoClienteFixo.ClientePessoaFisica)
                    {
                        //resultadoMes -= resultadoASubtrair;
                        //Só não soma se o resultado em ações for > 0 e volume inferior a 20 mil reais e o resultado do mês anterior seja menor que 0
                        if (valorVendasAcoes < 20000M && resultadoAcoes > 0 && resultadoMesAnterior >= 0)
                        {
                            resultadoASubtrair = resultadoAcoes;
                        }
                        else if (resultadoMesAnterior < 0 && resultadoMes > 0 && (resultadoMesAnterior + resultadoMes) < 0)
                        {
                            resultadoASubtrair = 0;
                        }
                        else if (resultadoMesAnterior < 0 && resultadoMes > 0 && (resultadoMesAnterior + resultadoMes) > 0)
                        {
                            if(valorVendasAcoes < 20000M)
                                resultadoASubtrair = resultadoMes + resultadoMesAnterior;
                        }

                        if (resultadoASubtrair != 0)
                            resultadoMes -= resultadoASubtrair;
                    }
                }
            }
            //

            #region Base de cálculo e apuração do IR (ou prezuizo a compensar para o proximo mes)
            decimal baseCalculo = 0;
            decimal baseCalculoDayTrade = 0;
            decimal prejuizoCompensar = 0;
            decimal prejuizoCompensarDayTrade = 0;

            if (resultadoMes - prejuizoMesAnterior > 0)
            {
                baseCalculo = resultadoMes - prejuizoMesAnterior;
            }
            else
            {
                prejuizoCompensar = Math.Abs(resultadoMes - prejuizoMesAnterior);
            }

            if (resultadoMesDayTrade - prejuizoDayTradeMesAnterior > 0)
            {
                baseCalculoDayTrade = resultadoMesDayTrade - prejuizoDayTradeMesAnterior;
            }
            else
            {
                prejuizoCompensarDayTrade = Math.Abs(resultadoMesDayTrade - prejuizoDayTradeMesAnterior);
            }

            #region desconta rendimento de acoes que tenham excecao de tributacao de IR
            ExcecoesTributacaoIRCollection excecoesTributacaoIRCollection = new ExcecoesTributacaoIRCollection();
            excecoesTributacaoIRCollection.Query.Where(excecoesTributacaoIRCollection.Query.DataInicio.LessThanOrEqual(data),
                                                       excecoesTributacaoIRCollection.Query.DataFim.GreaterThan(data),
                                                       excecoesTributacaoIRCollection.Query.Mercado.Equal(TipoMercado.Bolsa));

            excecoesTributacaoIRCollection.Query.Load();

            foreach (ExcecoesTributacaoIR excecoesTributacaoIR in excecoesTributacaoIRCollection)
            {
                if (excecoesTributacaoIR.IsencaoIR.Equals(ListaIsencaoIR.Isento.GetHashCode()))
                {
                    OperacaoBolsa operacao = new OperacaoBolsa();
                    operacao.Query
                     .Select(operacao.Query.ResultadoRealizado.Sum(), operacao.Query.ResultadoExercicio.Sum())
                     .Where(operacao.Query.IdCliente == idCliente,
                            operacao.Query.Data.GreaterThanOrEqual(dataInicio),
                            operacao.Query.Data.LessThanOrEqual(data),
                            operacao.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                            operacao.Query.CdAtivoBolsa.Equal(excecoesTributacaoIR.CdAtivoBolsa),
                            operacao.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.CompraDaytrade,
                                                           TipoOperacaoBolsa.VendaDaytrade));

                    if (operacao.Query.Load())
                        baseCalculo -= operacao.ResultadoRealizado.Value + operacao.ResultadoExercicio.Value;
                }
            }

            #endregion
            
            decimal valorIR = 0;            
            if (baseCalculo > 0)
            {
                valorIR = Utilitario.Truncate(baseCalculo * 0.15M, 2);
            }

            decimal valorIRDayTrade = 0;
            if (baseCalculoDayTrade > 0)
            {
                valorIRDayTrade = Utilitario.Truncate(baseCalculoDayTrade * 0.20M, 2);
            }
            decimal totalIR = valorIR + valorIRDayTrade;
            #endregion

            #region Busca o valor de IR DayTrade a compensar do mês anterior
            decimal irDayTradeAnteriorCompensar = 0;

            if (mes > 1) //Não se leva IR fonte a compensar de ano fiscal para o seguinte
            {
                apuracaoRendaVariavelIRMesAnterior = new ApuracaoRendaVariavelIR();
                if (apuracaoRendaVariavelIRMesAnterior.BuscaIRDayTradeCompensarMes(idCliente, mesAnterior, anoAnterior))
                {
                    irDayTradeAnteriorCompensar = apuracaoRendaVariavelIRMesAnterior.IRDayTradeCompensar.Value;
                }
            }
            #endregion

            #region Busca o valor de IR Fonte (0,005%) do mês
            IRFonte irFonte = new IRFonte();
            decimal irFonteMes = irFonte.RetornaValorIRFonte(idCliente, dataInicio, data);

            if (irFonteMes < 1)
            {
                irFonteMes = 0; //Mantem a mesma logica de menor que 1 real, não calcula...nem paga!
            }
            #endregion

            #region Busca o valor de IR Fonte DayTrade do mês
            irFonte = new IRFonte();
            decimal irFonteDayTradeMes = irFonte.RetornaValorIRFonteDayTrade(idCliente, dataInicio, data);
            #endregion

            //Soma o valor do IR Fonte do mês com o valor do IR Fonte a compensar do mês anterior
            //IR Fonte a compensar do mês anterior => IRFonte DT q não foi usado p/ abater da base do IR no mês anterior
            decimal irFonteDayTradeAbater = irFonteDayTradeMes + irDayTradeAnteriorCompensar;

            //Abate do valor de IR a pagar, o valor de IR Fonte DT a abater (IR Fonte DT do mês + mês anterior)
            decimal irPagar = totalIR - irFonteDayTradeAbater - irFonteMes;

            decimal irFonteCompensar = 0;
            if (irPagar < 0)
            {
                irFonteCompensar = Math.Abs(irPagar);
                irPagar = 0;
            }

            #region Insert na ApuracaoRendaVariavelIR
            ApuracaoRendaVariavelIR apuracaoRendaVariavelIRInsert = new ApuracaoRendaVariavelIR();
            apuracaoRendaVariavelIRInsert.IdCliente = idCliente;
            apuracaoRendaVariavelIRInsert.Ano = ano;
            apuracaoRendaVariavelIRInsert.Mes = (byte)mes;
            apuracaoRendaVariavelIRInsert.ResultadoAcoes = resultadoAcoes;
            apuracaoRendaVariavelIRInsert.ResultadoOuro = resultadoVistaBMF;
            apuracaoRendaVariavelIRInsert.ResultadoOpcoes = resultadoOpcoes;
            apuracaoRendaVariavelIRInsert.ResultadoOpcoesOutros = resultadoOpcoesBMF;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoDolar = ajusteDolar;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoIndice = ajusteIndice;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoJuros = ajusteJuros;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoOutros = ajusteOutros;
            apuracaoRendaVariavelIRInsert.ResultadoTermo = resultadoTermo;
            apuracaoRendaVariavelIRInsert.ResultadoAcoesDT = resultadoAcoesDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoOuroDT = resultadoVistaBMFDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoOpcoesDT = resultadoOpcoesDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoOpcoesOutrosDT = resultadoOpcoesBMFDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoDolarDT = ajusteDolarDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoIndiceDT = ajusteIndiceDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoJurosDT = ajusteJurosDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoFuturoOutrosDT = ajusteOutrosDayTrade;

            apuracaoRendaVariavelIRInsert.ResultadoLiquidoMes = resultadoMes;
            apuracaoRendaVariavelIRInsert.ResultadoLiquidoMesDT = resultadoMesDayTrade;
            apuracaoRendaVariavelIRInsert.ResultadoNegativoAnterior = prejuizoMesAnterior;
            apuracaoRendaVariavelIRInsert.ResultadoNegativoAnteriorDT = prejuizoDayTradeMesAnterior;
            apuracaoRendaVariavelIRInsert.BaseCalculo = baseCalculo;
            apuracaoRendaVariavelIRInsert.BaseCalculoDT = baseCalculoDayTrade;
            apuracaoRendaVariavelIRInsert.PrejuizoCompensar = prejuizoCompensar;
            apuracaoRendaVariavelIRInsert.PrejuizoCompensarDT = prejuizoCompensarDayTrade;
            apuracaoRendaVariavelIRInsert.ImpostoCalculado = valorIR;
            apuracaoRendaVariavelIRInsert.ImpostoCalculadoDT = valorIRDayTrade;
            apuracaoRendaVariavelIRInsert.TotalImposto = totalIR;
            apuracaoRendaVariavelIRInsert.IRDayTradeMes = irFonteDayTradeMes;
            apuracaoRendaVariavelIRInsert.IRDayTradeMesAnterior = irDayTradeAnteriorCompensar;
            apuracaoRendaVariavelIRInsert.IRDayTradeCompensar = irFonteCompensar;
            apuracaoRendaVariavelIRInsert.IRFonteNormal = irFonteMes;
            apuracaoRendaVariavelIRInsert.ImpostoPagar = irPagar;
            //O imposto pago na verdade pode incluir outros valores que não constam do Ganho de Renda Variável
            apuracaoRendaVariavelIRInsert.ImpostoPago = irFonteMes + irFonteDayTradeMes;

            apuracaoRendaVariavelIRInsert.Save();
            #endregion
        }

        /// <summary>
        /// Lança em Liquidacao o valor de IR calculado do mes anterior.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaIRGanhoLiquidacao(int idCliente, DateTime data)
        {
            if (data.CompareTo(Calendario.RetornaUltimoDiaUtilMes(data)) != 0)
            {
                return;
            }

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            #region Mês e ano corrente e anterior
            int mes = data.Month;
            int ano = data.Year;
            int mesAnterior;
            int anoAnterior;
            if (mes == 1)
            {
                mesAnterior = 12;
                anoAnterior = ano - 1;
            }
            else
            {
                mesAnterior = mes - 1;
                anoAnterior = ano;
            }
            #endregion

            ApuracaoRendaVariavelIR apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
            if (apuracaoRendaVariavelIR.LoadByPrimaryKey(idCliente, anoAnterior, (byte)mesAnterior))
            {
                #region Nova Liquidacao
                if (apuracaoRendaVariavelIR.ImpostoPagar.Value > 0)
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "Pagto IR mês (Ganhos em Renda Variável)";
                    liquidacao.Valor = apuracaoRendaVariavelIR.ImpostoPagar.Value * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.IR.IRRendaVariavel;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();
                }
                #endregion
            }
        }
	}
}
