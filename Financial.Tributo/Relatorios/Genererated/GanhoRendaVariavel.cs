/*
===============================================================================
                    EntitySpaces(TM) 2008 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET  
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2008.1.0623.0
                       MyGeneration Version # 1.2.0.7
                       Date Generated       : 04/07/2008 12:56:32
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		























		






	
















				
		
		




		









		
				
				








	










	



					
								
											
	









		





		




				
				







namespace Financial.Tributo.Relatorio
{

	[Serializable]
	abstract public class esGanhoRendaVariavelCollection : esEntityCollection
	{
		public esGanhoRendaVariavelCollection()
		{

		}	
		
		protected override string GetCollectionName()
		{
			return "GanhoRendaVariavelCollection";
		}		
		
		#region Query Logic
		protected void InitQuery(esGanhoRendaVariavelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGanhoRendaVariavelQuery);
		}		
		#endregion
		
		virtual public GanhoRendaVariavel DetachEntity(GanhoRendaVariavel entity)
		{
			return base.DetachEntity(entity) as GanhoRendaVariavel;
		}
		
		virtual public GanhoRendaVariavel AttachEntity(GanhoRendaVariavel entity)
		{
			return base.AttachEntity(entity) as GanhoRendaVariavel;
		}
		
		virtual public void Combine(GanhoRendaVariavelCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GanhoRendaVariavel this[int index]
		{
			get
			{
				return base[index] as GanhoRendaVariavel;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GanhoRendaVariavel);
		}
	}


	[Serializable]
	abstract public class esGanhoRendaVariavel : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGanhoRendaVariavelQuery GetDynamicQuery()
		{
			return null;
		}
		
		public esGanhoRendaVariavel()
		{
	
		}
	
		public esGanhoRendaVariavel(DataRow row)
			: base(row)
		{
	
		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTeste)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTeste);
			else
				return LoadByPrimaryKeyStoredProcedure(idTeste);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTeste)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGanhoRendaVariavelQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTeste == idTeste);
		
			return query.Load();
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// </remarks>
		/// <param name="sqlAccessType">Either esSqlAccessType StoredProcedure or DynamicSQL</param>
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTeste)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTeste);
			else
				return LoadByPrimaryKeyStoredProcedure(idTeste);
		}
	
		private bool LoadByPrimaryKeyDynamic(System.Int32 idTeste)
		{
			esGanhoRendaVariavelQuery query = this.GetDynamicQuery();
			query.Where(query.IdTeste == idTeste);
			return query.Load();
		}
	
		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTeste)
		{
			esParameters parms = new esParameters();
			parms.Add("idTeste",idTeste);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}		
		#endregion
		
				
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTeste": this.str.IdTeste = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTeste":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTeste = (System.Int32?)value;
							break;
						
					
						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GanhoRendaVariavel.idTeste
		/// </summary>
		virtual public System.Int32? IdTeste
		{
			get
			{
				return base.GetSystemInt32(GanhoRendaVariavelMetadata.ColumnNames.IdTeste);
			}
			
			set
			{
				if(base.SetSystemInt32(GanhoRendaVariavelMetadata.ColumnNames.IdTeste, value))
				{
					this.MarkFieldAsModified(GanhoRendaVariavelMetadata.ColumnNames.IdTeste);
				}
			}
		}
		
		#endregion	

		#region String Properties
		
		/// <summary>
		/// Converts an entity's properties to
		/// and from strings.
		/// </summary>
		/// <remarks>
		/// The str properties Get and Set provide easy conversion
		/// between a string and a property's data type. Not all
		/// data types will get a str property.
		/// </remarks>
		/// <example>
		/// Set a datetime from a string.
		/// <code>
		/// Employees entity = new Employees();
		/// entity.LoadByPrimaryKey(10);
		/// entity.str.HireDate = "2007-01-01 00:00:00";
		/// entity.Save();
		/// </code>
		/// Get a datetime as a string.
		/// <code>
		/// Employees entity = new Employees();
		/// entity.LoadByPrimaryKey(10);
		/// string theDate = entity.str.HireDate;
		/// </code>
		/// </example>

		[BrowsableAttribute( false )]		
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGanhoRendaVariavel entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTeste
			{
				get
				{
					System.Int32? data = entity.IdTeste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTeste = null;
					else entity.IdTeste = Convert.ToInt32(value);
				}
			}
			

			private esGanhoRendaVariavel entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGanhoRendaVariavelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGanhoRendaVariavel can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}

	
	public partial class GanhoRendaVariavel : esGanhoRendaVariavel
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}


	[Serializable]
	abstract public class esGanhoRendaVariavelQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GanhoRendaVariavelMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTeste
		{
			get
			{
				return new esQueryItem(this, GanhoRendaVariavelMetadata.ColumnNames.IdTeste, esSystemType.Int32);
			}
		} 
		
	}


	[Serializable]
	[XmlType("GanhoRendaVariavelCollection")]
	public partial class GanhoRendaVariavelCollection : esGanhoRendaVariavelCollection, IEnumerable<GanhoRendaVariavel>
	{
		public GanhoRendaVariavelCollection()
		{

		}	
		
		public static implicit operator List<GanhoRendaVariavel>(GanhoRendaVariavelCollection coll)
		{
			List<GanhoRendaVariavel> list = new List<GanhoRendaVariavel>();
			
			foreach (GanhoRendaVariavel emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}		
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GanhoRendaVariavelMetadata.Meta();
			}
		}	

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GanhoRendaVariavelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GanhoRendaVariavel(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GanhoRendaVariavel();
		}
		
			
		#endregion

		/// <summary>
		/// This represents the Query on the class.
		/// </summary>
		/// <remarks>
		/// Extensive documentation and examples on the
		/// Query API can be found at the
		/// <a target="_blank" href="http://www.entityspaces.net">EntitySpaces site</a>.
		/// </remarks>

		[BrowsableAttribute( false )]
		public GanhoRendaVariavelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GanhoRendaVariavelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		/// <summary>
		/// Useful for building up conditional queries.
		/// In most cases, before loading an entity or collection,
		/// you should instantiate a new one. This method was added
		/// to handle specialized circumstances, and should not be
		/// used as a substitute for that.
		/// </summary>
		/// <remarks>
		/// This just sets obj.Query to null/Nothing.
		/// In most cases, you will 'new' your object before
		/// loading it, rather than calling this method.
		/// It only affects obj.Query.Load(), so is not useful
		/// when Joins are involved, or for many other situations.
		/// Because it clears out any obj.Query.Where clauses,
		/// it can be useful for building conditional queries on the fly.
		/// <code>
		/// public bool ReQuery(string lastName, string firstName)
		/// {
		///     this.QueryReset();
		///     
		///     if(!String.IsNullOrEmpty(lastName))
		///     {
		///         this.Query.Where(
		///             this.Query.LastName == lastName);
		///     }
		///     if(!String.IsNullOrEmpty(firstName))
		///     {
		///         this.Query.Where(
		///             this.Query.FirstName == firstName);
		///     }
		///     
		///     return this.Query.Load();
		/// }
		/// </code>
		/// <code lang="vbnet">
		/// Public Function ReQuery(ByVal lastName As String, _
		///     ByVal firstName As String) As Boolean
		/// 
		///     Me.QueryReset()
		/// 
		///     If Not [String].IsNullOrEmpty(lastName) Then
		///         Me.Query.Where(Me.Query.LastName = lastName)
		///     End If
		///     If Not [String].IsNullOrEmpty(firstName) Then
		///         Me.Query.Where(Me.Query.FirstName = firstName)
		///     End If
		/// 
		///     Return Me.Query.Load()
		/// End Function
		/// </code>
		/// </remarks>
		public void QueryReset()
		{
			this.query = null;
		}
		
		/// <summary>
		/// Used to custom load a Join query.
		/// Returns true if at least one record was loaded.
		/// </summary>
		/// <remarks>
		/// Provides support for InnerJoin, LeftJoin,
		/// RightJoin, and FullJoin. You must provide an alias
		/// for each query when instantiating them.
		/// <code>
		/// EmployeeCollection collection = new EmployeeCollection();
		/// 
		/// EmployeeQuery emp = new EmployeeQuery("eq");
		/// CustomerQuery cust = new CustomerQuery("cq");
		/// 
		/// emp.Select(emp.EmployeeID, emp.LastName, cust.CustomerName);
		/// emp.LeftJoin(cust).On(emp.EmployeeID == cust.StaffAssigned);
		/// 
		/// collection.Load(emp);
		/// </code>
		/// <code lang="vbnet">
		/// Dim collection As New EmployeeCollection()
		/// 
		/// Dim emp As New EmployeeQuery("eq")
		/// Dim cust As New CustomerQuery("cq")
		/// 
		/// emp.Select(emp.EmployeeID, emp.LastName, cust.CustomerName)
		/// emp.LeftJoin(cust).On(emp.EmployeeID = cust.StaffAssigned)
		/// 
		/// collection.Load(emp)
		/// </code>
		/// </remarks>
		/// <param name="query">The query object instance name.</param>
		/// <returns>True if at least one record was loaded.</returns>
		public bool Load(GanhoRendaVariavelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}		
		
		/// <summary>
		/// Adds a new entity to the collection.
		/// Always calls AddNew() on the entity, in case it is overridden.
		/// </summary>
		public GanhoRendaVariavel AddNew()
		{
			GanhoRendaVariavel entity = base.AddNewEntity() as GanhoRendaVariavel;
			
			return entity;		
		}

		public GanhoRendaVariavel FindByPrimaryKey(System.Int32 idTeste)
		{
			return base.FindByPrimaryKey(idTeste) as GanhoRendaVariavel;
		}


		#region IEnumerable<GanhoRendaVariavel> Members

		IEnumerator<GanhoRendaVariavel> IEnumerable<GanhoRendaVariavel>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GanhoRendaVariavel;
			}
		}

		#endregion
		
		private GanhoRendaVariavelQuery query;
	}

	/// <summary>
	/// Encapsulates the 'GanhoRendaVariavel' table
	/// </summary>

	[Serializable]
	public partial class GanhoRendaVariavel : esGanhoRendaVariavel
	{
		public GanhoRendaVariavel()
		{

		}	
	
		public GanhoRendaVariavel(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GanhoRendaVariavelMetadata.Meta();
			}
		}

		override protected esGanhoRendaVariavelQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GanhoRendaVariavelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		
		

		/// <summary>
		/// This represents the Query on the class.
		/// </summary>
		/// <remarks>
		/// Extensive documentation and examples on the
		/// Query API can be found at the
		/// <a target="_blank" href="http://www.entityspaces.net">EntitySpaces site</a>.
		/// </remarks>

		[BrowsableAttribute( false )]
		public GanhoRendaVariavelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GanhoRendaVariavelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		/// <summary>
		/// Useful for building up conditional queries.
		/// In most cases, before loading an entity or collection,
		/// you should instantiate a new one. This method was added
		/// to handle specialized circumstances, and should not be
		/// used as a substitute for that.
		/// </summary>
		/// <remarks>
		/// This just sets obj.Query to null/Nothing.
		/// In most cases, you will 'new' your object before
		/// loading it, rather than calling this method.
		/// It only affects obj.Query.Load(), so is not useful
		/// when Joins are involved, or for many other situations.
		/// Because it clears out any obj.Query.Where clauses,
		/// it can be useful for building conditional queries on the fly.
		/// <code>
		/// public bool ReQuery(string lastName, string firstName)
		/// {
		///     this.QueryReset();
		///     
		///     if(!String.IsNullOrEmpty(lastName))
		///     {
		///         this.Query.Where(
		///             this.Query.LastName == lastName);
		///     }
		///     if(!String.IsNullOrEmpty(firstName))
		///     {
		///         this.Query.Where(
		///             this.Query.FirstName == firstName);
		///     }
		///     
		///     return this.Query.Load();
		/// }
		/// </code>
		/// <code lang="vbnet">
		/// Public Function ReQuery(ByVal lastName As String, _
		///     ByVal firstName As String) As Boolean
		/// 
		///     Me.QueryReset()
		/// 
		///     If Not [String].IsNullOrEmpty(lastName) Then
		///         Me.Query.Where(Me.Query.LastName = lastName)
		///     End If
		///     If Not [String].IsNullOrEmpty(firstName) Then
		///         Me.Query.Where(Me.Query.FirstName = firstName)
		///     End If
		/// 
		///     Return Me.Query.Load()
		/// End Function
		/// </code>
		/// </remarks>
		public void QueryReset()
		{
			this.query = null;
		}
		
		/// <summary>
		/// Used to custom load a Join query.
		/// Returns true if at least one row is loaded.
		/// For an entity, an exception will be thrown
		/// if more than one row is loaded.
		/// </summary>
		/// <remarks>
		/// Provides support for InnerJoin, LeftJoin,
		/// RightJoin, and FullJoin. You must provide an alias
		/// for each query when instantiating them.
		/// <code>
		/// EmployeeCollection collection = new EmployeeCollection();
		/// 
		/// EmployeeQuery emp = new EmployeeQuery("eq");
		/// CustomerQuery cust = new CustomerQuery("cq");
		/// 
		/// emp.Select(emp.EmployeeID, emp.LastName, cust.CustomerName);
		/// emp.LeftJoin(cust).On(emp.EmployeeID == cust.StaffAssigned);
		/// 
		/// collection.Load(emp);
		/// </code>
		/// <code lang="vbnet">
		/// Dim collection As New EmployeeCollection()
		/// 
		/// Dim emp As New EmployeeQuery("eq")
		/// Dim cust As New CustomerQuery("cq")
		/// 
		/// emp.Select(emp.EmployeeID, emp.LastName, cust.CustomerName)
		/// emp.LeftJoin(cust).On(emp.EmployeeID = cust.StaffAssigned)
		/// 
		/// collection.Load(emp)
		/// </code>
		/// </remarks>
		/// <param name="query">The query object instance name.</param>
		/// <returns>True if at least one record was loaded.</returns>
		public bool Load(GanhoRendaVariavelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}			
		
		private GanhoRendaVariavelQuery query;
	}


	[Serializable]
	public partial class GanhoRendaVariavelQuery : esGanhoRendaVariavelQuery
	{
		public GanhoRendaVariavelQuery()
		{

		}		
		
		public GanhoRendaVariavelQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}		

	}


	[Serializable]
	public partial class GanhoRendaVariavelMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GanhoRendaVariavelMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GanhoRendaVariavelMetadata.ColumnNames.IdTeste, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GanhoRendaVariavelMetadata.PropertyNames.IdTeste;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GanhoRendaVariavelMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTeste = "idTeste";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTeste = "IdTeste";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GanhoRendaVariavelMetadata))
			{
				if(GanhoRendaVariavelMetadata.mapDelegates == null)
				{
					GanhoRendaVariavelMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GanhoRendaVariavelMetadata.meta == null)
				{
					GanhoRendaVariavelMetadata.meta = new GanhoRendaVariavelMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("idTeste", new esTypeMap("int", "System.Int32"));			
				meta.Catalog = "Financial";
				meta.Schema = "dbo";
				
				meta.Source = "GanhoRendaVariavel";
				meta.Destination = "GanhoRendaVariavel";
				
				meta.spInsert = "proc_GanhoRendaVariavelInsert";				
				meta.spUpdate = "proc_GanhoRendaVariavelUpdate";		
				meta.spDelete = "proc_GanhoRendaVariavelDelete";
				meta.spLoadAll = "proc_GanhoRendaVariavelLoadAll";
				meta.spLoadByPrimaryKey = "proc_GanhoRendaVariavelLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GanhoRendaVariavelMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}