using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Tributo.Relatorio {
    public partial class GanhoFII : esGanhoFII {
        decimal resultadoNormal;

        public decimal ResultadoNormal {
            get { return resultadoNormal; }
            set { resultadoNormal = value; }
        }
        decimal resultadoNormal1;

        public decimal ResultadoNormal1
        {
            get { return resultadoNormal1; }
            set { resultadoNormal1 = value; }
        }
        decimal resultadoNormal2;

        public decimal ResultadoNormal2
        {
            get { return resultadoNormal2; }
            set { resultadoNormal2 = value; }
        }
        decimal resultadoNormal3;

        public decimal ResultadoNormal3
        {
            get { return resultadoNormal3; }
            set { resultadoNormal3 = value; }
        }
                
        decimal resultadoDT;
        public decimal ResultadoDT {
            get { return resultadoDT; }
            set { resultadoDT = value; }
        }

        decimal resultadoDT1;
        public decimal ResultadoDT1 {
            get { return resultadoDT1; }
            set { resultadoDT1 = value; }
        }
        decimal resultadoDT2;

        public decimal ResultadoDT2 {
            get { return resultadoDT2; }
            set { resultadoDT2 = value; }
        }
        decimal resultadoDT3;

        public decimal ResultadoDT3 {
            get { return resultadoDT3; }
            set { resultadoDT3 = value; }
        }
                
        decimal resultadoLiquidoMes;

        public decimal ResultadoLiquidoMes {
            get { return resultadoLiquidoMes; }
            set { resultadoLiquidoMes = value; }
        }
        decimal resultadoLiquidoMes1;

        public decimal ResultadoLiquidoMes1 {
            get { return resultadoLiquidoMes1; }
            set { resultadoLiquidoMes1 = value; }
        }
        decimal resultadoLiquidoMes2;

        public decimal ResultadoLiquidoMes2 {
            get { return resultadoLiquidoMes2; }
            set { resultadoLiquidoMes2 = value; }
        }
        decimal resultadoLiquidoMes3;

        public decimal ResultadoLiquidoMes3 {
            get { return resultadoLiquidoMes3; }
            set { resultadoLiquidoMes3 = value; }
        }

        decimal resultadoNegativoAnterior;

        public decimal ResultadoNegativoAnterior {
            get { return resultadoNegativoAnterior; }
            set { resultadoNegativoAnterior = value; }
        }
        decimal resultadoNegativoAnterior1;

        public decimal ResultadoNegativoAnterior1 {
            get { return resultadoNegativoAnterior1; }
            set { resultadoNegativoAnterior1 = value; }
        }
        decimal resultadoNegativoAnterior2;

        public decimal ResultadoNegativoAnterior2 {
            get { return resultadoNegativoAnterior2; }
            set { resultadoNegativoAnterior2 = value; }
        }
        decimal resultadoNegativoAnterior3;

        public decimal ResultadoNegativoAnterior3 {
            get { return resultadoNegativoAnterior3; }
            set { resultadoNegativoAnterior3 = value; }
        }

        decimal baseCalculo;

        public decimal BaseCalculo {
            get { return baseCalculo; }
            set { baseCalculo = value; }
        }
        decimal baseCalculo1;

        public decimal BaseCalculo1 {
            get { return baseCalculo1; }
            set { baseCalculo1 = value; }
        }
        decimal baseCalculo2;

        public decimal BaseCalculo2 {
            get { return baseCalculo2; }
            set { baseCalculo2 = value; }
        }
        decimal baseCalculo3;

        public decimal BaseCalculo3 {
            get { return baseCalculo3; }
            set { baseCalculo3 = value; }
        }

        decimal prejuizoCompensar;

        public decimal PrejuizoCompensar {
            get { return prejuizoCompensar; }
            set { prejuizoCompensar = value; }
        }

        decimal prejuizoCompensar1;

        public decimal PrejuizoCompensar1 {
            get { return prejuizoCompensar1; }
            set { prejuizoCompensar1 = value; }
        }
        decimal prejuizoCompensar2;

        public decimal PrejuizoCompensar2 {
            get { return prejuizoCompensar2; }
            set { prejuizoCompensar2 = value; }
        }
        decimal prejuizoCompensar3;

        public decimal PrejuizoCompensar3 {
            get { return prejuizoCompensar3; }
            set { prejuizoCompensar3 = value; }
        }

        decimal impostoCalculado;

        public decimal ImpostoCalculado {
            get { return impostoCalculado; }
            set { impostoCalculado = value; }
        }
        decimal impostoCalculado1;

        public decimal ImpostoCalculado1 {
            get { return impostoCalculado1; }
            set { impostoCalculado1 = value; }
        }
        decimal impostoCalculado2;

        public decimal ImpostoCalculado2 {
            get { return impostoCalculado2; }
            set { impostoCalculado2 = value; }
        }
        decimal impostoCalculado3;

        public decimal ImpostoCalculado3 {
            get { return impostoCalculado3; }
            set { impostoCalculado3 = value; }
        }

        decimal resultadoLiquidoMesDT;

        public decimal ResultadoLiquidoMesDT {
            get { return resultadoLiquidoMesDT; }
            set { resultadoLiquidoMesDT = value; }
        }
        decimal resultadoLiquidoMesDT1;

        public decimal ResultadoLiquidoMesDT1 {
            get { return resultadoLiquidoMesDT1; }
            set { resultadoLiquidoMesDT1 = value; }
        }
        decimal resultadoLiquidoMesDT2;

        public decimal ResultadoLiquidoMesDT2 {
            get { return resultadoLiquidoMesDT2; }
            set { resultadoLiquidoMesDT2 = value; }
        }
        decimal resultadoLiquidoMesDT3;

        public decimal ResultadoLiquidoMesDT3 {
            get { return resultadoLiquidoMesDT3; }
            set { resultadoLiquidoMesDT3 = value; }
        }

        decimal resultadoNegativoAnteriorDT;

        public decimal ResultadoNegativoAnteriorDT {
            get { return resultadoNegativoAnteriorDT; }
            set { resultadoNegativoAnteriorDT = value; }
        }
        decimal resultadoNegativoAnteriorDT1;

        public decimal ResultadoNegativoAnteriorDT1 {
            get { return resultadoNegativoAnteriorDT1; }
            set { resultadoNegativoAnteriorDT1 = value; }
        }
        decimal resultadoNegativoAnteriorDT2;

        public decimal ResultadoNegativoAnteriorDT2 {
            get { return resultadoNegativoAnteriorDT2; }
            set { resultadoNegativoAnteriorDT2 = value; }
        }
        decimal resultadoNegativoAnteriorDT3;

        public decimal ResultadoNegativoAnteriorDT3 {
            get { return resultadoNegativoAnteriorDT3; }
            set { resultadoNegativoAnteriorDT3 = value; }
        }

        decimal baseCalculoDT;

        public decimal BaseCalculoDT {
            get { return baseCalculoDT; }
            set { baseCalculoDT = value; }
        }
        decimal baseCalculoDT1;

        public decimal BaseCalculoDT1 {
            get { return baseCalculoDT1; }
            set { baseCalculoDT1 = value; }
        }
        decimal baseCalculoDT2;

        public decimal BaseCalculoDT2 {
            get { return baseCalculoDT2; }
            set { baseCalculoDT2 = value; }
        }
        decimal baseCalculoDT3;

        public decimal BaseCalculoDT3 {
            get { return baseCalculoDT3; }
            set { baseCalculoDT3 = value; }
        }

        decimal prejuizoCompensarDT;

        public decimal PrejuizoCompensarDT {
            get { return prejuizoCompensarDT; }
            set { prejuizoCompensarDT = value; }
        }
        decimal prejuizoCompensarDT1;

        public decimal PrejuizoCompensarDT1 {
            get { return prejuizoCompensarDT1; }
            set { prejuizoCompensarDT1 = value; }
        }
        decimal prejuizoCompensarDT2;

        public decimal PrejuizoCompensarDT2 {
            get { return prejuizoCompensarDT2; }
            set { prejuizoCompensarDT2 = value; }
        }
        decimal prejuizoCompensarDT3;

        public decimal PrejuizoCompensarDT3 {
            get { return prejuizoCompensarDT3; }
            set { prejuizoCompensarDT3 = value; }
        }

        decimal impostoCalculadoDT;

        public decimal ImpostoCalculadoDT {
            get { return impostoCalculadoDT; }
            set { impostoCalculadoDT = value; }
        }
        decimal impostoCalculadoDT1;

        public decimal ImpostoCalculadoDT1 {
            get { return impostoCalculadoDT1; }
            set { impostoCalculadoDT1 = value; }
        }
        decimal impostoCalculadoDT2;

        public decimal ImpostoCalculadoDT2 {
            get { return impostoCalculadoDT2; }
            set { impostoCalculadoDT2 = value; }
        }
        decimal impostoCalculadoDT3;

        public decimal ImpostoCalculadoDT3 {
            get { return impostoCalculadoDT3; }
            set { impostoCalculadoDT3 = value; }
        }

        decimal totalImposto;

        public decimal TotalImposto {
            get { return totalImposto; }
            set { totalImposto = value; }
        }
        decimal totalImposto1;

        public decimal TotalImposto1 {
            get { return totalImposto1; }
            set { totalImposto1 = value; }
        }
        decimal totalImposto2;

        public decimal TotalImposto2 {
            get { return totalImposto2; }
            set { totalImposto2 = value; }
        }
        decimal totalImposto3;

        public decimal TotalImposto3 {
            get { return totalImposto3; }
            set { totalImposto3 = value; }
        }

        decimal iRDayTradeMes;

        public decimal IRDayTradeMes {
            get { return iRDayTradeMes; }
            set { iRDayTradeMes = value; }
        }
        decimal iRDayTradeMes1;

        public decimal IRDayTradeMes1 {
            get { return iRDayTradeMes1; }
            set { iRDayTradeMes1 = value; }
        }
        decimal iRDayTradeMes2;

        public decimal IRDayTradeMes2 {
            get { return iRDayTradeMes2; }
            set { iRDayTradeMes2 = value; }
        }
        decimal iRDayTradeMes3;

        public decimal IRDayTradeMes3 {
            get { return iRDayTradeMes3; }
            set { iRDayTradeMes3 = value; }
        }

        decimal iRDayTradeMesAnterior;

        public decimal IRDayTradeMesAnterior {
            get { return iRDayTradeMesAnterior; }
            set { iRDayTradeMesAnterior = value; }
        }
        decimal iRDayTradeMesAnterior1;

        public decimal IRDayTradeMesAnterior1 {
            get { return iRDayTradeMesAnterior1; }
            set { iRDayTradeMesAnterior1 = value; }
        }
        decimal iRDayTradeMesAnterior2;

        public decimal IRDayTradeMesAnterior2 {
            get { return iRDayTradeMesAnterior2; }
            set { iRDayTradeMesAnterior2 = value; }
        }
        decimal iRDayTradeMesAnterior3;

        public decimal IRDayTradeMesAnterior3 {
            get { return iRDayTradeMesAnterior3; }
            set { iRDayTradeMesAnterior3 = value; }
        }

        decimal iRDayTradeCompensar;

        public decimal IRDayTradeCompensar {
            get { return iRDayTradeCompensar; }
            set { iRDayTradeCompensar = value; }
        }
        decimal iRDayTradeCompensar1;

        public decimal IRDayTradeCompensar1 {
            get { return iRDayTradeCompensar1; }
            set { iRDayTradeCompensar1 = value; }
        }
        decimal iRDayTradeCompensar2;

        public decimal IRDayTradeCompensar2 {
            get { return iRDayTradeCompensar2; }
            set { iRDayTradeCompensar2 = value; }
        }
        decimal iRDayTradeCompensar3;

        public decimal IRDayTradeCompensar3 {
            get { return iRDayTradeCompensar3; }
            set { iRDayTradeCompensar3 = value; }
        }

        decimal iRFonteNormal;

        public decimal IRFonteNormal {
            get { return iRFonteNormal; }
            set { iRFonteNormal = value; }
        }
        decimal iRFonteNormal1;

        public decimal IRFonteNormal1 {
            get { return iRFonteNormal1; }
            set { iRFonteNormal1 = value; }
        }
        decimal iRFonteNormal2;

        public decimal IRFonteNormal2 {
            get { return iRFonteNormal2; }
            set { iRFonteNormal2 = value; }
        }
        decimal iRFonteNormal3;

        public decimal IRFonteNormal3 {
            get { return iRFonteNormal3; }
            set { iRFonteNormal3 = value; }
        }

        decimal impostoPagar;

        public decimal ImpostoPagar {
            get { return impostoPagar; }
            set { impostoPagar = value; }
        }
        decimal impostoPagar1;

        public decimal ImpostoPagar1 {
            get { return impostoPagar1; }
            set { impostoPagar1 = value; }
        }
        decimal impostoPagar2;

        public decimal ImpostoPagar2 {
            get { return impostoPagar2; }
            set { impostoPagar2 = value; }
        }
        decimal impostoPagar3;

        public decimal ImpostoPagar3 {
            get { return impostoPagar3; }
            set { impostoPagar3 = value; }
        }

        decimal impostoPago;

        public decimal ImpostoPago {
            get { return impostoPago; }
            set { impostoPago = value; }
        }
        decimal impostoPago1;

        public decimal ImpostoPago1 {
            get { return impostoPago1; }
            set { impostoPago1 = value; }
        }
        decimal impostoPago2;

        public decimal ImpostoPago2 {
            get { return impostoPago2; }
            set { impostoPago2 = value; }
        }
        decimal impostoPago3;

        public decimal ImpostoPago3 {
            get { return impostoPago3; }
            set { impostoPago3 = value; }
        }
    }
}
