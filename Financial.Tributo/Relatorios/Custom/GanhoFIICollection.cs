﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Reflection;
using Financial.Investidor;

namespace Financial.Tributo.Relatorio {

    public partial class GanhoFIICollection : esGanhoFIICollection
    {   
        // Atributos Privates
        private esColumnMetadataCollection extendedProperties;
        private DataTable dataTable = new DataTable(); // DataTable usado no relatorio

        // colunas DataTable      
        #region ColunasDataTable
        private class ColunasDataTable {
            public string ResultadoNormal = "ResultadoNormal";
            public string ResultadoNormal1 = "ResultadoNormal1";
            public string ResultadoNormal2 = "ResultadoNormal2";
            public string ResultadoNormal3 = "ResultadoNormal3";
            public string ResultadoDT = "ResultadoDT";
            public string ResultadoDT1 = "ResultadoDT1";
            public string ResultadoDT2 = "ResultadoDT2";
            public string ResultadoDT3 = "ResultadoDT3";
            public string ResultadoLiquidoMes = "ResultadoLiquidoMes";
            public string ResultadoLiquidoMes1 = "ResultadoLiquidoMes1";
            public string ResultadoLiquidoMes2 = "ResultadoLiquidoMes2";
            public string ResultadoLiquidoMes3 = "ResultadoLiquidoMes3";
            public string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
            public string ResultadoNegativoAnterior1 = "ResultadoNegativoAnterior1";
            public string ResultadoNegativoAnterior2 = "ResultadoNegativoAnterior2";
            public string ResultadoNegativoAnterior3 = "ResultadoNegativoAnterior3";
            public string BaseCalculo = "BaseCalculo";
            public string BaseCalculo1 = "BaseCalculo1";
            public string BaseCalculo2 = "BaseCalculo2";
            public string BaseCalculo3 = "BaseCalculo3";
            public string PrejuizoCompensar = "PrejuizoCompensar";
            public string PrejuizoCompensar1 = "PrejuizoCompensar1";
            public string PrejuizoCompensar2 = "PrejuizoCompensar2";
            public string PrejuizoCompensar3 = "PrejuizoCompensar3";
            public string ImpostoCalculado = "ImpostoCalculado";
            public string ImpostoCalculado1 = "ImpostoCalculado1";
            public string ImpostoCalculado2 = "ImpostoCalculado2";
            public string ImpostoCalculado3 = "ImpostoCalculado3";
            public string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
            public string ResultadoLiquidoMesDT1 = "ResultadoLiquidoMesDT1";
            public string ResultadoLiquidoMesDT2 = "ResultadoLiquidoMesDT2";
            public string ResultadoLiquidoMesDT3 = "ResultadoLiquidoMesDT3";
            public string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
            public string ResultadoNegativoAnteriorDT1 = "ResultadoNegativoAnteriorDT1";
            public string ResultadoNegativoAnteriorDT2 = "ResultadoNegativoAnteriorDT2";
            public string ResultadoNegativoAnteriorDT3 = "ResultadoNegativoAnteriorDT3";
            public string BaseCalculoDT = "BaseCalculoDT";
            public string BaseCalculoDT1 = "BaseCalculoDT1";
            public string BaseCalculoDT2 = "BaseCalculoDT2";
            public string BaseCalculoDT3 = "BaseCalculoDT3";
            public string PrejuizoCompensarDT = "PrejuizoCompensarDT";
            public string PrejuizoCompensarDT1 = "PrejuizoCompensarDT1";
            public string PrejuizoCompensarDT2 = "PrejuizoCompensarDT2";
            public string PrejuizoCompensarDT3 = "PrejuizoCompensarDT3";
            public string ImpostoCalculadoDT = "ImpostoCalculadoDT";
            public string ImpostoCalculadoDT1 = "ImpostoCalculadoDT1";
            public string ImpostoCalculadoDT2 = "ImpostoCalculadoDT2";
            public string ImpostoCalculadoDT3 = "ImpostoCalculadoDT3";
            public string TotalImposto = "TotalImposto";
            public string TotalImposto1 = "TotalImposto1";
            public string TotalImposto2 = "TotalImposto2";
            public string TotalImposto3 = "TotalImposto3";
            public string IRDayTradeMes = "IRDayTradeMes";
            public string IRDayTradeMes1 = "IRDayTradeMes1";
            public string IRDayTradeMes2 = "IRDayTradeMes2";
            public string IRDayTradeMes3 = "IRDayTradeMes3";
            public string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
            public string IRDayTradeMesAnterior1 = "IRDayTradeMesAnterior1";
            public string IRDayTradeMesAnterior2 = "IRDayTradeMesAnterior2";
            public string IRDayTradeMesAnterior3 = "IRDayTradeMesAnterior3";
            public string IRDayTradeCompensar = "IRDayTradeCompensar";
            public string IRDayTradeCompensar1 = "IRDayTradeCompensar1";
            public string IRDayTradeCompensar2 = "IRDayTradeCompensar2";
            public string IRDayTradeCompensar3 = "IRDayTradeCompensar3";
            public string IRFonteNormal = "IRFonteNormal";
            public string IRFonteNormal1 = "IRFonteNormal1";
            public string IRFonteNormal2 = "IRFonteNormal2";
            public string IRFonteNormal3 = "IRFonteNormal3";
            public string ImpostoPagar = "ImpostoPagar";
            public string ImpostoPagar1 = "ImpostoPagar1";
            public string ImpostoPagar2 = "ImpostoPagar2";
            public string ImpostoPagar3 = "ImpostoPagar3";
            public string ImpostoPago = "ImpostoPago";
            public string ImpostoPago1 = "ImpostoPago1";
            public string ImpostoPago2 = "ImpostoPago2";
            public string ImpostoPago3 = "ImpostoPago3";
        }
        #endregion

        private ColunasDataTable colunasDataTable = new ColunasDataTable();

        protected void CreateExtendedProperties(esColumnMetadataCollection extendedProps) {
            if (null == extendedProps["ResultadoNormal"])
            {
                #region Atributos
                esColumnMetadata col = new esColumnMetadata("ResultadoNormal", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNormal";
                col.IsTransient = true;
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNormal", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNormal";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNormal1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNormal1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNormal2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNormal2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNormal3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNormal3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoDT3";
                extendedProps.Add(col);
                                
                col = new esColumnMetadata("ResultadoLiquidoMes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior3";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo1";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo2";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo3";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto1";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto2";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago3";
                extendedProps.Add(col);

                #endregion
            }                      
        } 

        /// <summary>
        /// Inicializa o DataTable com os mesmos atributos da classe
        /// </summary>         
        private void InicializaDataTablePrivate() {
            if (this.extendedProperties == null) this.extendedProperties = new esColumnMetadataCollection();
            this.CreateExtendedProperties(this.extendedProperties);
            foreach (esColumnMetadata extEsCol in this.extendedProperties) {
                if (!this.dataTable.Columns.Contains(extEsCol.Name)) {
                    this.dataTable.Columns.Add(new DataColumn(extEsCol.Name, extEsCol.Type));
                }
            }
        }
                                           
        /// <summary>
        ///     Método usado para preencher o DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable FillDataTable(int idCliente, List<int> mesExecucao, List<int> anoExecucao) {
            //
            this.InicializaDataTablePrivate();

            ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollection = new ApuracaoIRImobiliarioCollection();
            
            #region Consultas
            for (int i = 0; i < 4; i++) {
                ApuracaoIRImobiliario apuracaoIRImobiliario = new ApuracaoIRImobiliario();
                apuracaoIRImobiliario.AddNew();
                if (!apuracaoIRImobiliario.LoadByPrimaryKey(idCliente, anoExecucao[i], (byte)mesExecucao[i]))
                {
                    // Se nao existe registro, carrega om valores 0
                    #region Prenche com valores vazios
                    apuracaoIRImobiliario.AddNew();
                    apuracaoIRImobiliario.IdCliente = idCliente;
                    apuracaoIRImobiliario.Ano = anoExecucao[i];
                    apuracaoIRImobiliario.Mes = (byte)mesExecucao[i];
                    apuracaoIRImobiliario.ResultadoNormal = 0;
                    apuracaoIRImobiliario.ResultadoDT = 0;
                    apuracaoIRImobiliario.ResultadoLiquidoMes = 0;
                    apuracaoIRImobiliario.ResultadoNegativoAnterior = 0;
                    apuracaoIRImobiliario.BaseCalculo = 0;
                    apuracaoIRImobiliario.PrejuizoCompensar = 0;
                    apuracaoIRImobiliario.ImpostoCalculado = 0;
                    apuracaoIRImobiliario.ResultadoLiquidoMesDT = 0;
                    apuracaoIRImobiliario.ResultadoNegativoAnteriorDT = 0;
                    apuracaoIRImobiliario.BaseCalculoDT = 0;
                    apuracaoIRImobiliario.PrejuizoCompensarDT = 0;
                    apuracaoIRImobiliario.ImpostoCalculadoDT = 0;
                    apuracaoIRImobiliario.TotalImposto = 0;
                    apuracaoIRImobiliario.IRDayTradeMes = 0;
                    apuracaoIRImobiliario.IRDayTradeMesAnterior = 0;
                    apuracaoIRImobiliario.IRDayTradeCompensar = 0;
                    apuracaoIRImobiliario.IRFonteNormal = 0;
                    apuracaoIRImobiliario.ImpostoPagar = 0;
                    apuracaoIRImobiliario.ImpostoPago = 0;
                    #endregion
                }
                apuracaoIRImobiliarioCollection.AttachEntity(apuracaoIRImobiliario);
            }                        
            #endregion

            DataRow dataRow = this.dataTable.NewRow();

            #region Preenche Dados
            #region ResultadoNormal
            dataRow[this.colunasDataTable.ResultadoNormal] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoNormal;
            #endregion

            #region ResultadoNormal1
            dataRow[this.colunasDataTable.ResultadoNormal1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoNormal;
            #endregion

            #region ResultadoAcoes2
            dataRow[this.colunasDataTable.ResultadoNormal2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoNormal;
            #endregion

            #region ResultadoAcoes3
            dataRow[this.colunasDataTable.ResultadoNormal3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoNormal;
            #endregion

            #region ResultadoAcoesDT
            dataRow[this.colunasDataTable.ResultadoDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoDT;
            #endregion

            #region ResultadoAcoesDT1
            dataRow[this.colunasDataTable.ResultadoDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoDT;
            #endregion

            #region ResultadoAcoesDT2
            dataRow[this.colunasDataTable.ResultadoDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoDT;
            #endregion

            #region ResultadoAcoesDT3
            dataRow[this.colunasDataTable.ResultadoDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoDT;
            #endregion

            #region ResultadoLiquidoMes
            dataRow[this.colunasDataTable.ResultadoLiquidoMes] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes1
            dataRow[this.colunasDataTable.ResultadoLiquidoMes1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes2
            dataRow[this.colunasDataTable.ResultadoLiquidoMes2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes3
            dataRow[this.colunasDataTable.ResultadoLiquidoMes3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoNegativoAnterior
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior1
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior2
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior3
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoNegativoAnterior;
            #endregion

            #region BaseCalculo
            dataRow[this.colunasDataTable.BaseCalculo] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).BaseCalculo;
            #endregion

            #region BaseCalculo1
            dataRow[this.colunasDataTable.BaseCalculo1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).BaseCalculo;
            #endregion

            #region BaseCalculo2
            dataRow[this.colunasDataTable.BaseCalculo2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).BaseCalculo;
            #endregion

            #region BaseCalculo3
            dataRow[this.colunasDataTable.BaseCalculo3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).BaseCalculo;
            #endregion

            #region PrejuizoCompensar
            dataRow[this.colunasDataTable.PrejuizoCompensar] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar1
            dataRow[this.colunasDataTable.PrejuizoCompensar1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar2
            dataRow[this.colunasDataTable.PrejuizoCompensar2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar3
            dataRow[this.colunasDataTable.PrejuizoCompensar3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).PrejuizoCompensar;
            #endregion

            #region ImpostoCalculado
            dataRow[this.colunasDataTable.ImpostoCalculado] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado1
            dataRow[this.colunasDataTable.ImpostoCalculado1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado2
            dataRow[this.colunasDataTable.ImpostoCalculado2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado3
            dataRow[this.colunasDataTable.ImpostoCalculado3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ImpostoCalculado;
            #endregion

            #region ResultadoLiquidoMesDT
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT1
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT2
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT3
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoNegativoAnteriorDT
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT1
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT2
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT3
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ResultadoNegativoAnteriorDT;
            #endregion

            #region BaseCalculoDT
            dataRow[this.colunasDataTable.BaseCalculoDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT1
            dataRow[this.colunasDataTable.BaseCalculoDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT2
            dataRow[this.colunasDataTable.BaseCalculoDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT3
            dataRow[this.colunasDataTable.BaseCalculoDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).BaseCalculoDT;
            #endregion

            #region PrejuizoCompensarDT
            dataRow[this.colunasDataTable.PrejuizoCompensarDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT1
            dataRow[this.colunasDataTable.PrejuizoCompensarDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT2
            dataRow[this.colunasDataTable.PrejuizoCompensarDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT3
            dataRow[this.colunasDataTable.PrejuizoCompensarDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).PrejuizoCompensarDT;
            #endregion

            #region ImpostoCalculadoDT
            dataRow[this.colunasDataTable.ImpostoCalculadoDT] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT1
            dataRow[this.colunasDataTable.ImpostoCalculadoDT1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT2
            dataRow[this.colunasDataTable.ImpostoCalculadoDT2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT3
            dataRow[this.colunasDataTable.ImpostoCalculadoDT3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ImpostoCalculadoDT;
            #endregion

            #region TotalImposto
            dataRow[this.colunasDataTable.TotalImposto] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).TotalImposto;
            #endregion

            #region TotalImposto1
            dataRow[this.colunasDataTable.TotalImposto1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).TotalImposto;
            #endregion

            #region TotalImposto2
            dataRow[this.colunasDataTable.TotalImposto2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).TotalImposto;
            #endregion

            #region TotalImposto3
            dataRow[this.colunasDataTable.TotalImposto3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).TotalImposto;
            #endregion

            #region IRDayTradeMes
            dataRow[this.colunasDataTable.IRDayTradeMes] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes1
            dataRow[this.colunasDataTable.IRDayTradeMes1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes2
            dataRow[this.colunasDataTable.IRDayTradeMes2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes3
            dataRow[this.colunasDataTable.IRDayTradeMes3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMesAnterior
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior1
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior2
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior3
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeCompensar
            dataRow[this.colunasDataTable.IRDayTradeCompensar] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar1
            dataRow[this.colunasDataTable.IRDayTradeCompensar1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar2
            dataRow[this.colunasDataTable.IRDayTradeCompensar2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar3
            dataRow[this.colunasDataTable.IRDayTradeCompensar3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).IRDayTradeCompensar;
            #endregion

            #region IRFonteNormal
            dataRow[this.colunasDataTable.IRFonteNormal] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).IRFonteNormal;
            #endregion

            #region IRFonteNormal1
            dataRow[this.colunasDataTable.IRFonteNormal1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).IRFonteNormal;
            #endregion

            #region IRFonteNormal2
            dataRow[this.colunasDataTable.IRFonteNormal2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).IRFonteNormal;
            #endregion

            #region IRFonteNormal3
            dataRow[this.colunasDataTable.IRFonteNormal3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).IRFonteNormal;
            #endregion

            #region ImpostoPagar
            dataRow[this.colunasDataTable.ImpostoPagar] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ImpostoPagar;
            #endregion

            #region ImpostoPagar1
            dataRow[this.colunasDataTable.ImpostoPagar1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ImpostoPagar;
            #endregion

            #region ImpostoPagar2
            dataRow[this.colunasDataTable.ImpostoPagar2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ImpostoPagar;
            #endregion

            #region ImpostoPagar3
            dataRow[this.colunasDataTable.ImpostoPagar3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ImpostoPagar;
            #endregion

            #region ImpostoPago
            dataRow[this.colunasDataTable.ImpostoPago] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[0]).ImpostoPago;
            #endregion

            #region ImpostoPago1
            dataRow[this.colunasDataTable.ImpostoPago1] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[1]).ImpostoPago;
            #endregion

            #region ImpostoPago2
            dataRow[this.colunasDataTable.ImpostoPago2] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[2]).ImpostoPago;
            #endregion

            #region ImpostoPago3
            dataRow[this.colunasDataTable.ImpostoPago3] = ((ApuracaoIRImobiliario)apuracaoIRImobiliarioCollection[3]).ImpostoPago;
            #endregion

            #endregion

            //
            this.dataTable.Rows.Add(dataRow);			
            //
            return this.dataTable;        
        }
	}
}

