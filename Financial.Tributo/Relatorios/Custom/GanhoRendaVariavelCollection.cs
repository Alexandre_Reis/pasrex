﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           10/4/2007 15:14:37
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Reflection;
using Financial.Investidor;

namespace Financial.Tributo.Relatorio {
	
    public partial class GanhoRendaVariavelCollection : esGanhoRendaVariavelCollection {   
        // Atributos Privates
        private esColumnMetadataCollection extendedProperties;
        private DataTable dataTable = new DataTable(); // DataTable usado no relatorio

        // colunas DataTable      
        #region ColunasDataTable
        private class ColunasDataTable {
            public string ResultadoAcoes = "ResultadoAcoes";
            public string ResultadoAcoes1 = "ResultadoAcoes1";
            public string ResultadoAcoes2 = "ResultadoAcoes2";
            public string ResultadoAcoes3 = "ResultadoAcoes3";
            public string ResultadoOuro = "ResultadoOuro";
            public string ResultadoOuro1 = "ResultadoOuro1";
            public string ResultadoOuro2 = "ResultadoOuro2";
            public string ResultadoOuro3 = "ResultadoOuro3";
            public string ResultadoOuroNaoBolsa = "ResultadoOuroNaoBolsa";
            public string ResultadoOuroNaoBolsa1 = "ResultadoOuroNaoBolsa1";
            public string ResultadoOuroNaoBolsa2 = "ResultadoOuroNaoBolsa2";
            public string ResultadoOuroNaoBolsa3 = "ResultadoOuroNaoBolsa3";
            public string ResultadoOpcoes = "ResultadoOpcoes";
            public string ResultadoOpcoes1 = "ResultadoOpcoes1";
            public string ResultadoOpcoes2 = "ResultadoOpcoes2";
            public string ResultadoOpcoes3 = "ResultadoOpcoes3";
            public string ResultadoOpcoesOuro = "ResultadoOpcoesOuro";
            public string ResultadoOpcoesOuro1 = "ResultadoOpcoesOuro1";
            public string ResultadoOpcoesOuro2 = "ResultadoOpcoesOuro2";
            public string ResultadoOpcoesOuro3 = "ResultadoOpcoesOuro3";
            public string ResultadoOpcoesNaoBolsa = "ResultadoOpcoesNaoBolsa";
            public string ResultadoOpcoesNaoBolsa1 = "ResultadoOpcoesNaoBolsa1";
            public string ResultadoOpcoesNaoBolsa2 = "ResultadoOpcoesNaoBolsa2";
            public string ResultadoOpcoesNaoBolsa3 = "ResultadoOpcoesNaoBolsa3";
            public string ResultadoOpcoesOutros = "ResultadoOpcoesOutros";
            public string ResultadoOpcoesOutros1 = "ResultadoOpcoesOutros1";
            public string ResultadoOpcoesOutros2 = "ResultadoOpcoesOutros2";
            public string ResultadoOpcoesOutros3 = "ResultadoOpcoesOutros3";
            public string ResultadoFuturoDolar = "ResultadoFuturoDolar";
            public string ResultadoFuturoDolar1 = "ResultadoFuturoDolar1";
            public string ResultadoFuturoDolar2 = "ResultadoFuturoDolar2";
            public string ResultadoFuturoDolar3 = "ResultadoFuturoDolar3";
            public string ResultadoFuturoIndice = "ResultadoFuturoIndice";
            public string ResultadoFuturoIndice1 = "ResultadoFuturoIndice1";
            public string ResultadoFuturoIndice2 = "ResultadoFuturoIndice2";
            public string ResultadoFuturoIndice3 = "ResultadoFuturoIndice3";
            public string ResultadoFuturoJuros = "ResultadoFuturoJuros";
            public string ResultadoFuturoJuros1 = "ResultadoFuturoJuros1";
            public string ResultadoFuturoJuros2 = "ResultadoFuturoJuros2";
            public string ResultadoFuturoJuros3 = "ResultadoFuturoJuros3";
            public string ResultadoFuturoOutros = "ResultadoFuturoOutros";
            public string ResultadoFuturoOutros1 = "ResultadoFuturoOutros1";
            public string ResultadoFuturoOutros2 = "ResultadoFuturoOutros2";
            public string ResultadoFuturoOutros3 = "ResultadoFuturoOutros3";
            public string ResultadoTermo = "ResultadoTermo";
            public string ResultadoTermo1 = "ResultadoTermo1";
            public string ResultadoTermo2 = "ResultadoTermo2";
            public string ResultadoTermo3 = "ResultadoTermo3";
            public string ResultadoTermoOutros = "ResultadoTermoOutros";
            public string ResultadoTermoOutros1 = "ResultadoTermoOutros1";
            public string ResultadoTermoOutros2 = "ResultadoTermoOutros2";
            public string ResultadoTermoOutros3 = "ResultadoTermoOutros3";
            public string ResultadoAcoesDT = "ResultadoAcoesDT";
            public string ResultadoAcoesDT1 = "ResultadoAcoesDT1";
            public string ResultadoAcoesDT2 = "ResultadoAcoesDT2";
            public string ResultadoAcoesDT3 = "ResultadoAcoesDT3";
            public string ResultadoOuroDT = "ResultadoOuroDT";
            public string ResultadoOuroDT1 = "ResultadoOuroDT1";
            public string ResultadoOuroDT2 = "ResultadoOuroDT2";
            public string ResultadoOuroDT3 = "ResultadoOuroDT3";
            public string ResultadoOuroNaoBolsaDT = "ResultadoOuroNaoBolsaDT";
            public string ResultadoOuroNaoBolsaDT1 = "ResultadoOuroNaoBolsaDT1";
            public string ResultadoOuroNaoBolsaDT2 = "ResultadoOuroNaoBolsaDT2";
            public string ResultadoOuroNaoBolsaDT3 = "ResultadoOuroNaoBolsaDT3";
            public string ResultadoOpcoesDT = "ResultadoOpcoesDT";
            public string ResultadoOpcoesDT1 = "ResultadoOpcoesDT1";
            public string ResultadoOpcoesDT2 = "ResultadoOpcoesDT2";
            public string ResultadoOpcoesDT3 = "ResultadoOpcoesDT3";
            public string ResultadoOpcoesOuroDT = "ResultadoOpcoesOuroDT";
            public string ResultadoOpcoesOuroDT1 = "ResultadoOpcoesOuroDT1";
            public string ResultadoOpcoesOuroDT2 = "ResultadoOpcoesOuroDT2";
            public string ResultadoOpcoesOuroDT3 = "ResultadoOpcoesOuroDT3";
            public string ResultadoOpcoesNaoBolsaDT = "ResultadoOpcoesNaoBolsaDT";
            public string ResultadoOpcoesNaoBolsaDT1 = "ResultadoOpcoesNaoBolsaDT1";
            public string ResultadoOpcoesNaoBolsaDT2 = "ResultadoOpcoesNaoBolsaDT2";
            public string ResultadoOpcoesNaoBolsaDT3 = "ResultadoOpcoesNaoBolsaDT3";
            public string ResultadoOpcoesOutrosDT = "ResultadoOpcoesOutrosDT";
            public string ResultadoOpcoesOutrosDT1 = "ResultadoOpcoesOutrosDT1";
            public string ResultadoOpcoesOutrosDT2 = "ResultadoOpcoesOutrosDT2";
            public string ResultadoOpcoesOutrosDT3 = "ResultadoOpcoesOutrosDT3";
            public string ResultadoFuturoDolarDT = "ResultadoFuturoDolarDT";
            public string ResultadoFuturoDolarDT1 = "ResultadoFuturoDolarDT1";
            public string ResultadoFuturoDolarDT2 = "ResultadoFuturoDolarDT2";
            public string ResultadoFuturoDolarDT3 = "ResultadoFuturoDolarDT3";
            public string ResultadoFuturoIndiceDT = "ResultadoFuturoIndiceDT";
            public string ResultadoFuturoIndiceDT1 = "ResultadoFuturoIndiceDT1";
            public string ResultadoFuturoIndiceDT2 = "ResultadoFuturoIndiceDT2";
            public string ResultadoFuturoIndiceDT3 = "ResultadoFuturoIndiceDT3";
            public string ResultadoFuturoJurosDT = "ResultadoFuturoJurosDT";
            public string ResultadoFuturoJurosDT1 = "ResultadoFuturoJurosDT1";
            public string ResultadoFuturoJurosDT2 = "ResultadoFuturoJurosDT2";
            public string ResultadoFuturoJurosDT3 = "ResultadoFuturoJurosDT3";
            public string ResultadoFuturoOutrosDT = "ResultadoFuturoOutrosDT";
            public string ResultadoFuturoOutrosDT1 = "ResultadoFuturoOutrosDT1";
            public string ResultadoFuturoOutrosDT2 = "ResultadoFuturoOutrosDT2";
            public string ResultadoFuturoOutrosDT3 = "ResultadoFuturoOutrosDT3";
            public string ResultadoTermoDT = "ResultadoTermoDT";
            public string ResultadoTermoDT1 = "ResultadoTermoDT1";
            public string ResultadoTermoDT2 = "ResultadoTermoDT2";
            public string ResultadoTermoDT3 = "ResultadoTermoDT3";
            public string ResultadoTermoOutrosDT = "ResultadoTermoOutrosDT";
            public string ResultadoTermoOutrosDT1 = "ResultadoTermoOutrosDT1";
            public string ResultadoTermoOutrosDT2 = "ResultadoTermoOutrosDT2";
            public string ResultadoTermoOutrosDT3 = "ResultadoTermoOutrosDT3";
            public string ResultadoLiquidoMes = "ResultadoLiquidoMes";
            public string ResultadoLiquidoMes1 = "ResultadoLiquidoMes1";
            public string ResultadoLiquidoMes2 = "ResultadoLiquidoMes2";
            public string ResultadoLiquidoMes3 = "ResultadoLiquidoMes3";
            public string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
            public string ResultadoNegativoAnterior1 = "ResultadoNegativoAnterior1";
            public string ResultadoNegativoAnterior2 = "ResultadoNegativoAnterior2";
            public string ResultadoNegativoAnterior3 = "ResultadoNegativoAnterior3";
            public string BaseCalculo = "BaseCalculo";
            public string BaseCalculo1 = "BaseCalculo1";
            public string BaseCalculo2 = "BaseCalculo2";
            public string BaseCalculo3 = "BaseCalculo3";
            public string PrejuizoCompensar = "PrejuizoCompensar";
            public string PrejuizoCompensar1 = "PrejuizoCompensar1";
            public string PrejuizoCompensar2 = "PrejuizoCompensar2";
            public string PrejuizoCompensar3 = "PrejuizoCompensar3";
            public string ImpostoCalculado = "ImpostoCalculado";
            public string ImpostoCalculado1 = "ImpostoCalculado1";
            public string ImpostoCalculado2 = "ImpostoCalculado2";
            public string ImpostoCalculado3 = "ImpostoCalculado3";
            public string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
            public string ResultadoLiquidoMesDT1 = "ResultadoLiquidoMesDT1";
            public string ResultadoLiquidoMesDT2 = "ResultadoLiquidoMesDT2";
            public string ResultadoLiquidoMesDT3 = "ResultadoLiquidoMesDT3";
            public string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
            public string ResultadoNegativoAnteriorDT1 = "ResultadoNegativoAnteriorDT1";
            public string ResultadoNegativoAnteriorDT2 = "ResultadoNegativoAnteriorDT2";
            public string ResultadoNegativoAnteriorDT3 = "ResultadoNegativoAnteriorDT3";
            public string BaseCalculoDT = "BaseCalculoDT";
            public string BaseCalculoDT1 = "BaseCalculoDT1";
            public string BaseCalculoDT2 = "BaseCalculoDT2";
            public string BaseCalculoDT3 = "BaseCalculoDT3";
            public string PrejuizoCompensarDT = "PrejuizoCompensarDT";
            public string PrejuizoCompensarDT1 = "PrejuizoCompensarDT1";
            public string PrejuizoCompensarDT2 = "PrejuizoCompensarDT2";
            public string PrejuizoCompensarDT3 = "PrejuizoCompensarDT3";
            public string ImpostoCalculadoDT = "ImpostoCalculadoDT";
            public string ImpostoCalculadoDT1 = "ImpostoCalculadoDT1";
            public string ImpostoCalculadoDT2 = "ImpostoCalculadoDT2";
            public string ImpostoCalculadoDT3 = "ImpostoCalculadoDT3";
            public string TotalImposto = "TotalImposto";
            public string TotalImposto1 = "TotalImposto1";
            public string TotalImposto2 = "TotalImposto2";
            public string TotalImposto3 = "TotalImposto3";
            public string IRDayTradeMes = "IRDayTradeMes";
            public string IRDayTradeMes1 = "IRDayTradeMes1";
            public string IRDayTradeMes2 = "IRDayTradeMes2";
            public string IRDayTradeMes3 = "IRDayTradeMes3";
            public string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
            public string IRDayTradeMesAnterior1 = "IRDayTradeMesAnterior1";
            public string IRDayTradeMesAnterior2 = "IRDayTradeMesAnterior2";
            public string IRDayTradeMesAnterior3 = "IRDayTradeMesAnterior3";
            public string IRDayTradeCompensar = "IRDayTradeCompensar";
            public string IRDayTradeCompensar1 = "IRDayTradeCompensar1";
            public string IRDayTradeCompensar2 = "IRDayTradeCompensar2";
            public string IRDayTradeCompensar3 = "IRDayTradeCompensar3";
            public string IRFonteNormal = "IRFonteNormal";
            public string IRFonteNormal1 = "IRFonteNormal1";
            public string IRFonteNormal2 = "IRFonteNormal2";
            public string IRFonteNormal3 = "IRFonteNormal3";
            public string ImpostoPagar = "ImpostoPagar";
            public string ImpostoPagar1 = "ImpostoPagar1";
            public string ImpostoPagar2 = "ImpostoPagar2";
            public string ImpostoPagar3 = "ImpostoPagar3";
            public string ImpostoPago = "ImpostoPago";
            public string ImpostoPago1 = "ImpostoPago1";
            public string ImpostoPago2 = "ImpostoPago2";
            public string ImpostoPago3 = "ImpostoPago3";
        }
        #endregion

        private ColunasDataTable colunasDataTable = new ColunasDataTable();

        protected void CreateExtendedProperties(esColumnMetadataCollection extendedProps) {
            if (null == extendedProps["ResultadoAcoes"]) {
                #region Atributos
                esColumnMetadata col = new esColumnMetadata("ResultadoAcoes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoes";
                col.IsTransient = true;
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoes";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuro", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuro";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuro1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuro1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuro2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuro2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuro3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuro3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsa", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsa";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsa1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsa1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsa2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsa2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsa3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsa3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoes";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuro", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuro";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuro1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuro1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuro2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuro2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuro3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuro3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsa", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsa";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsa1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsa1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsa2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsa2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsa3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsa3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutros", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutros";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutros1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutros1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutros2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutros2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutros3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutros3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolar";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndice", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndice";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndice1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndice1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndice2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndice2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndice3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndice3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJuros", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJuros";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJuros1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJuros1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJuros2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJuros2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJuros3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJuros3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutros", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutros";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutros1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutros1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutros2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutros2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutros3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutros3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermo", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermo";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermo1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermo1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermo2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermo2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermo3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermo3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutros", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutros";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutros1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutros1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutros2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutros2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutros3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutros3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoesDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoesDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoesDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoesDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoesDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoesDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoAcoesDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoAcoesDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsaDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsaDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsaDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsaDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsaDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsaDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOuroNaoBolsaDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOuroNaoBolsaDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuroDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuroDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuroDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuroDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuroDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuroDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOuroDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOuroDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsaDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsaDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsaDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsaDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsaDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsaDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesNaoBolsaDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesNaoBolsaDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutrosDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutrosDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutrosDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutrosDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutrosDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutrosDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoOpcoesOutrosDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoOpcoesOutrosDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolarDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolarDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolarDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolarDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolarDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolarDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoDolarDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoDolarDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndiceDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndiceDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndiceDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndiceDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndiceDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndiceDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoIndiceDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoIndiceDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJurosDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJurosDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJurosDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJurosDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJurosDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJurosDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoJurosDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoJurosDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutrosDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutrosDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutrosDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutrosDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutrosDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutrosDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoFuturoOutrosDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoFuturoOutrosDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutrosDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutrosDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutrosDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutrosDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutrosDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutrosDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoTermoOutrosDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoTermoOutrosDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnterior3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnterior3";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo1";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo2";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculo3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculo3";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculado3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculado3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoLiquidoMesDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoLiquidoMesDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ResultadoNegativoAnteriorDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ResultadoNegativoAnteriorDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("BaseCalculoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "BaseCalculoDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("PrejuizoCompensarDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "PrejuizoCompensarDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoCalculadoDT3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoCalculadoDT3";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto1";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto2";
                extendedProps.Add(col);

                col = new esColumnMetadata("TotalImposto3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "TotalImposto3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMes3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMes3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeMesAnterior3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeMesAnterior3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRDayTradeCompensar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRDayTradeCompensar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal1";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal2";
                extendedProps.Add(col);

                col = new esColumnMetadata("IRFonteNormal3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "IRFonteNormal3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPagar3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPagar3";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago1", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago1";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago2", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago2";
                extendedProps.Add(col);

                col = new esColumnMetadata("ImpostoPago3", this.Meta.Columns.Count + 1, Type.GetType("System.Decimal"));
                col.PropertyName = "ImpostoPago3";
                extendedProps.Add(col);

                #endregion
            }                      
        } 

        /// <summary>
        /// Inicializa o DataTable com os mesmos atributos da classe
        /// </summary>         
        private void InicializaDataTablePrivate() {
            if (this.extendedProperties == null) this.extendedProperties = new esColumnMetadataCollection();
            this.CreateExtendedProperties(this.extendedProperties);
            foreach (esColumnMetadata extEsCol in this.extendedProperties) {
                if (!this.dataTable.Columns.Contains(extEsCol.Name)) {
                    this.dataTable.Columns.Add(new DataColumn(extEsCol.Name, extEsCol.Type));
                }
            }
        }
                                           
        /// <summary>
        ///     Método usado para preencher o DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable FillDataTable(int idCliente, List<int> mesExecucao, List<int> anoExecucao) {
            //
            this.InicializaDataTablePrivate();
            
            ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollection = new ApuracaoRendaVariavelIRCollection();
            
            #region Consultas
            for (int i = 0; i < 4; i++) {
                ApuracaoRendaVariavelIR apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
                apuracaoRendaVariavelIR.AddNew();
                if (!apuracaoRendaVariavelIR.LoadByPrimaryKey(idCliente, anoExecucao[i], (byte)mesExecucao[i])) {
                    // Se nao existe registro, carrega om valores 0
                    #region Prenche com valores vazios
                    apuracaoRendaVariavelIR.AddNew();
                    apuracaoRendaVariavelIR.IdCliente = idCliente;
                    apuracaoRendaVariavelIR.Ano = anoExecucao[i];
                    apuracaoRendaVariavelIR.Mes = (byte)mesExecucao[i];
                    apuracaoRendaVariavelIR.ResultadoAcoes = 0;
                    apuracaoRendaVariavelIR.ResultadoOuro = 0;
                    apuracaoRendaVariavelIR.ResultadoOuroNaoBolsa = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoes = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesOuro = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsa = 0; 
                    apuracaoRendaVariavelIR.ResultadoOpcoesOutros = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoDolar = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoIndice = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoJuros = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoOutros = 0;
                    apuracaoRendaVariavelIR.ResultadoTermo = 0;
                    apuracaoRendaVariavelIR.ResultadoTermoOutros = 0;
                    apuracaoRendaVariavelIR.ResultadoAcoesDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOuroDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOuroNaoBolsaDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesOuroDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsaDT = 0;
                    apuracaoRendaVariavelIR.ResultadoOpcoesOutrosDT = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoDolarDT = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoIndiceDT = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoJurosDT = 0;
                    apuracaoRendaVariavelIR.ResultadoFuturoOutrosDT = 0;
                    apuracaoRendaVariavelIR.ResultadoTermoDT = 0;
                    apuracaoRendaVariavelIR.ResultadoTermoOutrosDT = 0;
                    apuracaoRendaVariavelIR.ResultadoLiquidoMes = 0;
                    apuracaoRendaVariavelIR.ResultadoNegativoAnterior = 0;
                    apuracaoRendaVariavelIR.BaseCalculo = 0;
                    apuracaoRendaVariavelIR.PrejuizoCompensar = 0;
                    apuracaoRendaVariavelIR.ImpostoCalculado = 0;
                    apuracaoRendaVariavelIR.ResultadoLiquidoMesDT = 0;
                    apuracaoRendaVariavelIR.ResultadoNegativoAnteriorDT = 0;
                    apuracaoRendaVariavelIR.BaseCalculoDT = 0;
                    apuracaoRendaVariavelIR.PrejuizoCompensarDT = 0;
                    apuracaoRendaVariavelIR.ImpostoCalculadoDT = 0;
                    apuracaoRendaVariavelIR.TotalImposto = 0;
                    apuracaoRendaVariavelIR.IRDayTradeMes = 0;
                    apuracaoRendaVariavelIR.IRDayTradeMesAnterior = 0;
                    apuracaoRendaVariavelIR.IRDayTradeCompensar = 0;
                    apuracaoRendaVariavelIR.IRFonteNormal = 0;
                    apuracaoRendaVariavelIR.ImpostoPagar = 0;
                    apuracaoRendaVariavelIR.ImpostoPago = 0;
                    #endregion
                }
                apuracaoRendaVariavelIRCollection.AttachEntity(apuracaoRendaVariavelIR);
            }                        
            #endregion

            DataRow dataRow = this.dataTable.NewRow();

            #region Preenche Dados
            #region ResultadoAcoes
            dataRow[this.colunasDataTable.ResultadoAcoes] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoAcoes;
            #endregion

            #region ResultadoAcoes1
            dataRow[this.colunasDataTable.ResultadoAcoes1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoAcoes;
            #endregion

            #region ResultadoAcoes2
            dataRow[this.colunasDataTable.ResultadoAcoes2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoAcoes;
            #endregion

            #region ResultadoAcoes3
            dataRow[this.colunasDataTable.ResultadoAcoes3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoAcoes;
            #endregion

            #region ResultadoOuro
            dataRow[this.colunasDataTable.ResultadoOuro] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOuro;
            #endregion

            #region ResultadoOuro1
            dataRow[this.colunasDataTable.ResultadoOuro1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOuro;
            #endregion

            #region ResultadoOuro2
            dataRow[this.colunasDataTable.ResultadoOuro2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOuro;
            #endregion

            #region ResultadoOuro3
            dataRow[this.colunasDataTable.ResultadoOuro3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOuro;
            #endregion

            #region ResultadoOuroNaoBolsa
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsa] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOuroNaoBolsa;
            #endregion

            #region ResultadoOuroNaoBolsa1
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsa1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOuroNaoBolsa;
            #endregion

            #region ResultadoOuroNaoBolsa2
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsa2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOuroNaoBolsa;
            #endregion

            #region ResultadoOuroNaoBolsa3
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsa3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOuroNaoBolsa;
            #endregion

            #region ResultadoOpcoes
            dataRow[this.colunasDataTable.ResultadoOpcoes] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoes;
            #endregion

            #region ResultadoOpcoes1
            dataRow[this.colunasDataTable.ResultadoOpcoes1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoes;
            #endregion

            #region ResultadoOpcoes2
            dataRow[this.colunasDataTable.ResultadoOpcoes2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoes;
            #endregion

            #region ResultadoOpcoes3
            dataRow[this.colunasDataTable.ResultadoOpcoes3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoes;
            #endregion

            #region ResultadoOpcoesOuro
            dataRow[this.colunasDataTable.ResultadoOpcoesOuro] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesOuro;
            #endregion

            #region ResultadoOpcoesOuro1
            dataRow[this.colunasDataTable.ResultadoOpcoesOuro1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesOuro;
            #endregion

            #region ResultadoOpcoesOuro2
            dataRow[this.colunasDataTable.ResultadoOpcoesOuro2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesOuro;
            #endregion

            #region ResultadoOpcoesOuro3
            dataRow[this.colunasDataTable.ResultadoOpcoesOuro3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesOuro;
            #endregion

            #region ResultadoOpcoesNaoBolsa
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsa] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesNaoBolsa;
            #endregion

            #region ResultadoOpcoesNaoBolsa1
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsa1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesNaoBolsa;
            #endregion

            #region ResultadoOpcoesNaoBolsa2
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsa2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesNaoBolsa;
            #endregion

            #region ResultadoOpcoesNaoBolsa3
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsa3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesNaoBolsa;
            #endregion

            #region ResultadoOpcoesOutros
            dataRow[this.colunasDataTable.ResultadoOpcoesOutros] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesOutros;
            #endregion

            #region ResultadoOpcoesOutros1
            dataRow[this.colunasDataTable.ResultadoOpcoesOutros1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesOutros;
            #endregion

            #region ResultadoOpcoesOutros2
            dataRow[this.colunasDataTable.ResultadoOpcoesOutros2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesOutros;
            #endregion

            #region ResultadoOpcoesOutros3
            dataRow[this.colunasDataTable.ResultadoOpcoesOutros3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesOutros;
            #endregion

            #region ResultadoFuturoDolar
            dataRow[this.colunasDataTable.ResultadoFuturoDolar] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoDolar;
            #endregion

            #region ResultadoFuturoDolar1
            dataRow[this.colunasDataTable.ResultadoFuturoDolar1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoDolar;
            #endregion

            #region ResultadoFuturoDolar2
            dataRow[this.colunasDataTable.ResultadoFuturoDolar2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoDolar;
            #endregion

            #region ResultadoFuturoDolar3
            dataRow[this.colunasDataTable.ResultadoFuturoDolar3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoDolar;
            #endregion

            #region ResultadoFuturoIndice
            dataRow[this.colunasDataTable.ResultadoFuturoIndice] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoIndice;
            #endregion

            #region ResultadoFuturoIndice1
            dataRow[this.colunasDataTable.ResultadoFuturoIndice1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoIndice;
            #endregion

            #region ResultadoFuturoIndice2
            dataRow[this.colunasDataTable.ResultadoFuturoIndice2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoIndice;
            #endregion

            #region ResultadoFuturoIndice3
            dataRow[this.colunasDataTable.ResultadoFuturoIndice3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoIndice;
            #endregion

            #region ResultadoFuturoJuros
            dataRow[this.colunasDataTable.ResultadoFuturoJuros] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoJuros;
            #endregion

            #region ResultadoFuturoJuros1
            dataRow[this.colunasDataTable.ResultadoFuturoJuros1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoJuros;
            #endregion

            #region ResultadoFuturoJuros2
            dataRow[this.colunasDataTable.ResultadoFuturoJuros2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoJuros;
            #endregion

            #region ResultadoFuturoJuros3
            dataRow[this.colunasDataTable.ResultadoFuturoJuros3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoJuros;
            #endregion

            #region ResultadoFuturoOutros
            dataRow[this.colunasDataTable.ResultadoFuturoOutros] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoOutros;
            #endregion

            #region ResultadoFuturoOutros1
            dataRow[this.colunasDataTable.ResultadoFuturoOutros1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoOutros;
            #endregion

            #region ResultadoFuturoOutros2
            dataRow[this.colunasDataTable.ResultadoFuturoOutros2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoOutros;
            #endregion

            #region ResultadoFuturoOutros3
            dataRow[this.colunasDataTable.ResultadoFuturoOutros3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoOutros;
            #endregion

            #region ResultadoTermo
            dataRow[this.colunasDataTable.ResultadoTermo] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoTermo;
            #endregion

            #region ResultadoTermo1
            dataRow[this.colunasDataTable.ResultadoTermo1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoTermo;
            #endregion

            #region ResultadoTermo2
            dataRow[this.colunasDataTable.ResultadoTermo2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoTermo;
            #endregion

            #region ResultadoTermo3
            dataRow[this.colunasDataTable.ResultadoTermo3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoTermo;
            #endregion

            #region ResultadoTermoOutros
            dataRow[this.colunasDataTable.ResultadoTermoOutros] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoTermoOutros;
            #endregion

            #region ResultadoTermoOutros1
            dataRow[this.colunasDataTable.ResultadoTermoOutros1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoTermoOutros;
            #endregion

            #region ResultadoTermoOutros2
            dataRow[this.colunasDataTable.ResultadoTermoOutros2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoTermoOutros;
            #endregion

            #region ResultadoTermoOutros3
            dataRow[this.colunasDataTable.ResultadoTermoOutros3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoTermoOutros;
            #endregion

            #region ResultadoAcoesDT
            dataRow[this.colunasDataTable.ResultadoAcoesDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoAcoesDT;
            #endregion

            #region ResultadoAcoesDT1
            dataRow[this.colunasDataTable.ResultadoAcoesDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoAcoesDT;
            #endregion

            #region ResultadoAcoesDT2
            dataRow[this.colunasDataTable.ResultadoAcoesDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoAcoesDT;
            #endregion

            #region ResultadoAcoesDT3
            dataRow[this.colunasDataTable.ResultadoAcoesDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoAcoesDT;
            #endregion

            #region ResultadoOuroDT
            dataRow[this.colunasDataTable.ResultadoOuroDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOuroDT;
            #endregion

            #region ResultadoOuroDT1
            dataRow[this.colunasDataTable.ResultadoOuroDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOuroDT;
            #endregion

            #region ResultadoOuroDT2
            dataRow[this.colunasDataTable.ResultadoOuroDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOuroDT;
            #endregion

            #region ResultadoOuroDT3
            dataRow[this.colunasDataTable.ResultadoOuroDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOuroDT;
            #endregion

            #region ResultadoOuroNaoBolsaDT
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsaDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOuroNaoBolsaDT;
            #endregion

            #region ResultadoOuroNaoBolsaDT1
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsaDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOuroNaoBolsaDT;
            #endregion

            #region ResultadoOuroNaoBolsaDT2
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsaDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOuroNaoBolsaDT;
            #endregion

            #region ResultadoOuroNaoBolsaDT3
            dataRow[this.colunasDataTable.ResultadoOuroNaoBolsaDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOuroNaoBolsaDT;
            #endregion

            #region ResultadoOpcoesDT
            dataRow[this.colunasDataTable.ResultadoOpcoesDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesDT;
            #endregion

            #region ResultadoOpcoesDT1
            dataRow[this.colunasDataTable.ResultadoOpcoesDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesDT;
            #endregion

            #region ResultadoOpcoesDT2
            dataRow[this.colunasDataTable.ResultadoOpcoesDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesDT;
            #endregion

            #region ResultadoOpcoesDT3
            dataRow[this.colunasDataTable.ResultadoOpcoesDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesDT;
            #endregion

            #region ResultadoOpcoesOuroDT
            dataRow[this.colunasDataTable.ResultadoOpcoesOuroDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesOuroDT;
            #endregion

            #region ResultadoOpcoesOuroDT1
            dataRow[this.colunasDataTable.ResultadoOpcoesOuroDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesOuroDT;
            #endregion

            #region ResultadoOpcoesOuroDT2
            dataRow[this.colunasDataTable.ResultadoOpcoesOuroDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesOuroDT;
            #endregion

            #region ResultadoOpcoesOuroDT3
            dataRow[this.colunasDataTable.ResultadoOpcoesOuroDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesOuroDT;
            #endregion

            #region ResultadoOpcoesNaoBolsaDT
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsaDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesNaoBolsaDT;
            #endregion

            #region ResultadoOpcoesNaoBolsaDT1
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsaDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesNaoBolsaDT;
            #endregion

            #region ResultadoOpcoesNaoBolsaDT2
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsaDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesNaoBolsaDT;
            #endregion

            #region ResultadoOpcoesNaoBolsaDT3
            dataRow[this.colunasDataTable.ResultadoOpcoesNaoBolsaDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesNaoBolsaDT;
            #endregion

            #region ResultadoOpcoesOutrosDT
            dataRow[this.colunasDataTable.ResultadoOpcoesOutrosDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoOpcoesOutrosDT;
            #endregion

            #region ResultadoOpcoesOutrosDT1
            dataRow[this.colunasDataTable.ResultadoOpcoesOutrosDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoOpcoesOutrosDT;
            #endregion

            #region ResultadoOpcoesOutrosDT2
            dataRow[this.colunasDataTable.ResultadoOpcoesOutrosDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoOpcoesOutrosDT;
            #endregion

            #region ResultadoOpcoesOutrosDT3
            dataRow[this.colunasDataTable.ResultadoOpcoesOutrosDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoOpcoesOutrosDT;
            #endregion

            #region ResultadoFuturoDolarDT
            dataRow[this.colunasDataTable.ResultadoFuturoDolarDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoDolarDT;
            #endregion

            #region ResultadoFuturoDolarDT1
            dataRow[this.colunasDataTable.ResultadoFuturoDolarDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoDolarDT;
            #endregion

            #region ResultadoFuturoDolarDT2
            dataRow[this.colunasDataTable.ResultadoFuturoDolarDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoDolarDT;
            #endregion

            #region ResultadoFuturoDolarDT3
            dataRow[this.colunasDataTable.ResultadoFuturoDolarDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoDolarDT;
            #endregion

            #region ResultadoFuturoIndiceDT
            dataRow[this.colunasDataTable.ResultadoFuturoIndiceDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoIndiceDT;
            #endregion

            #region ResultadoFuturoIndiceDT1
            dataRow[this.colunasDataTable.ResultadoFuturoIndiceDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoIndiceDT;
            #endregion

            #region ResultadoFuturoIndiceDT2
            dataRow[this.colunasDataTable.ResultadoFuturoIndiceDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoIndiceDT;
            #endregion

            #region ResultadoFuturoIndiceDT3
            dataRow[this.colunasDataTable.ResultadoFuturoIndiceDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoIndiceDT;
            #endregion

            #region ResultadoFuturoJurosDT
            dataRow[this.colunasDataTable.ResultadoFuturoJurosDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoJurosDT;
            #endregion

            #region ResultadoFuturoJurosDT1
            dataRow[this.colunasDataTable.ResultadoFuturoJurosDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoJurosDT;
            #endregion

            #region ResultadoFuturoJurosDT2
            dataRow[this.colunasDataTable.ResultadoFuturoJurosDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoJurosDT;
            #endregion

            #region ResultadoFuturoJurosDT3
            dataRow[this.colunasDataTable.ResultadoFuturoJurosDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoJurosDT;
            #endregion

            #region ResultadoFuturoOutrosDT
            dataRow[this.colunasDataTable.ResultadoFuturoOutrosDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoFuturoOutrosDT;
            #endregion

            #region ResultadoFuturoOutrosDT1
            dataRow[this.colunasDataTable.ResultadoFuturoOutrosDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoFuturoOutrosDT;
            #endregion

            #region ResultadoFuturoOutrosDT2
            dataRow[this.colunasDataTable.ResultadoFuturoOutrosDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoFuturoOutrosDT;
            #endregion

            #region ResultadoFuturoOutrosDT3
            dataRow[this.colunasDataTable.ResultadoFuturoOutrosDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoFuturoOutrosDT;
            #endregion

            #region ResultadoTermoDT
            dataRow[this.colunasDataTable.ResultadoTermoDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoTermoDT;
            #endregion

            #region ResultadoTermoDT1
            dataRow[this.colunasDataTable.ResultadoTermoDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoTermoDT;
            #endregion

            #region ResultadoTermoDT2
            dataRow[this.colunasDataTable.ResultadoTermoDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoTermoDT;
            #endregion

            #region ResultadoTermoDT3
            dataRow[this.colunasDataTable.ResultadoTermoDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoTermoDT;
            #endregion

            #region ResultadoTermoOutrosDT
            dataRow[this.colunasDataTable.ResultadoTermoOutrosDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoTermoOutrosDT;
            #endregion

            #region ResultadoTermoOutrosDT1
            dataRow[this.colunasDataTable.ResultadoTermoOutrosDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoTermoOutrosDT;
            #endregion

            #region ResultadoTermoOutrosDT2
            dataRow[this.colunasDataTable.ResultadoTermoOutrosDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoTermoOutrosDT;
            #endregion

            #region ResultadoTermoOutrosDT3
            dataRow[this.colunasDataTable.ResultadoTermoOutrosDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoTermoOutrosDT;
            #endregion

            #region ResultadoLiquidoMes
            dataRow[this.colunasDataTable.ResultadoLiquidoMes] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes1
            dataRow[this.colunasDataTable.ResultadoLiquidoMes1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes2
            dataRow[this.colunasDataTable.ResultadoLiquidoMes2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoLiquidoMes3
            dataRow[this.colunasDataTable.ResultadoLiquidoMes3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoLiquidoMes;
            #endregion

            #region ResultadoNegativoAnterior
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior1
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior2
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoNegativoAnterior;
            #endregion

            #region ResultadoNegativoAnterior3
            dataRow[this.colunasDataTable.ResultadoNegativoAnterior3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoNegativoAnterior;
            #endregion

            #region BaseCalculo
            dataRow[this.colunasDataTable.BaseCalculo] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).BaseCalculo;
            #endregion

            #region BaseCalculo1
            dataRow[this.colunasDataTable.BaseCalculo1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).BaseCalculo;
            #endregion

            #region BaseCalculo2
            dataRow[this.colunasDataTable.BaseCalculo2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).BaseCalculo;
            #endregion

            #region BaseCalculo3
            dataRow[this.colunasDataTable.BaseCalculo3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).BaseCalculo;
            #endregion

            #region PrejuizoCompensar
            dataRow[this.colunasDataTable.PrejuizoCompensar] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar1
            dataRow[this.colunasDataTable.PrejuizoCompensar1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar2
            dataRow[this.colunasDataTable.PrejuizoCompensar2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).PrejuizoCompensar;
            #endregion

            #region PrejuizoCompensar3
            dataRow[this.colunasDataTable.PrejuizoCompensar3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).PrejuizoCompensar;
            #endregion

            #region ImpostoCalculado
            dataRow[this.colunasDataTable.ImpostoCalculado] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado1
            dataRow[this.colunasDataTable.ImpostoCalculado1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado2
            dataRow[this.colunasDataTable.ImpostoCalculado2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ImpostoCalculado;
            #endregion

            #region ImpostoCalculado3
            dataRow[this.colunasDataTable.ImpostoCalculado3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ImpostoCalculado;
            #endregion

            #region ResultadoLiquidoMesDT
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT1
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT2
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoLiquidoMesDT3
            dataRow[this.colunasDataTable.ResultadoLiquidoMesDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoLiquidoMesDT;
            #endregion

            #region ResultadoNegativoAnteriorDT
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT1
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT2
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ResultadoNegativoAnteriorDT;
            #endregion

            #region ResultadoNegativoAnteriorDT3
            dataRow[this.colunasDataTable.ResultadoNegativoAnteriorDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ResultadoNegativoAnteriorDT;
            #endregion

            #region BaseCalculoDT
            dataRow[this.colunasDataTable.BaseCalculoDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT1
            dataRow[this.colunasDataTable.BaseCalculoDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT2
            dataRow[this.colunasDataTable.BaseCalculoDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).BaseCalculoDT;
            #endregion

            #region BaseCalculoDT3
            dataRow[this.colunasDataTable.BaseCalculoDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).BaseCalculoDT;
            #endregion

            #region PrejuizoCompensarDT
            dataRow[this.colunasDataTable.PrejuizoCompensarDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT1
            dataRow[this.colunasDataTable.PrejuizoCompensarDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT2
            dataRow[this.colunasDataTable.PrejuizoCompensarDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).PrejuizoCompensarDT;
            #endregion

            #region PrejuizoCompensarDT3
            dataRow[this.colunasDataTable.PrejuizoCompensarDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).PrejuizoCompensarDT;
            #endregion

            #region ImpostoCalculadoDT
            dataRow[this.colunasDataTable.ImpostoCalculadoDT] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT1
            dataRow[this.colunasDataTable.ImpostoCalculadoDT1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT2
            dataRow[this.colunasDataTable.ImpostoCalculadoDT2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ImpostoCalculadoDT;
            #endregion

            #region ImpostoCalculadoDT3
            dataRow[this.colunasDataTable.ImpostoCalculadoDT3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ImpostoCalculadoDT;
            #endregion

            #region TotalImposto
            dataRow[this.colunasDataTable.TotalImposto] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).TotalImposto;
            #endregion

            #region TotalImposto1
            dataRow[this.colunasDataTable.TotalImposto1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).TotalImposto;
            #endregion

            #region TotalImposto2
            dataRow[this.colunasDataTable.TotalImposto2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).TotalImposto;
            #endregion

            #region TotalImposto3
            dataRow[this.colunasDataTable.TotalImposto3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).TotalImposto;
            #endregion

            #region IRDayTradeMes
            dataRow[this.colunasDataTable.IRDayTradeMes] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes1
            dataRow[this.colunasDataTable.IRDayTradeMes1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes2
            dataRow[this.colunasDataTable.IRDayTradeMes2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMes3
            dataRow[this.colunasDataTable.IRDayTradeMes3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).IRDayTradeMes;
            #endregion

            #region IRDayTradeMesAnterior
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior1
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior2
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeMesAnterior3
            dataRow[this.colunasDataTable.IRDayTradeMesAnterior3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).IRDayTradeMesAnterior;
            #endregion

            #region IRDayTradeCompensar
            dataRow[this.colunasDataTable.IRDayTradeCompensar] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar1
            dataRow[this.colunasDataTable.IRDayTradeCompensar1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar2
            dataRow[this.colunasDataTable.IRDayTradeCompensar2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).IRDayTradeCompensar;
            #endregion

            #region IRDayTradeCompensar3
            dataRow[this.colunasDataTable.IRDayTradeCompensar3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).IRDayTradeCompensar;
            #endregion

            #region IRFonteNormal
            dataRow[this.colunasDataTable.IRFonteNormal] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).IRFonteNormal;
            #endregion

            #region IRFonteNormal1
            dataRow[this.colunasDataTable.IRFonteNormal1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).IRFonteNormal;
            #endregion

            #region IRFonteNormal2
            dataRow[this.colunasDataTable.IRFonteNormal2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).IRFonteNormal;
            #endregion

            #region IRFonteNormal3
            dataRow[this.colunasDataTable.IRFonteNormal3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).IRFonteNormal;
            #endregion

            #region ImpostoPagar
            dataRow[this.colunasDataTable.ImpostoPagar] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ImpostoPagar;
            #endregion

            #region ImpostoPagar1
            dataRow[this.colunasDataTable.ImpostoPagar1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ImpostoPagar;
            #endregion

            #region ImpostoPagar2
            dataRow[this.colunasDataTable.ImpostoPagar2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ImpostoPagar;
            #endregion

            #region ImpostoPagar3
            dataRow[this.colunasDataTable.ImpostoPagar3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ImpostoPagar;
            #endregion

            #region ImpostoPago
            dataRow[this.colunasDataTable.ImpostoPago] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[0]).ImpostoPago;
            #endregion

            #region ImpostoPago1
            dataRow[this.colunasDataTable.ImpostoPago1] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[1]).ImpostoPago;
            #endregion

            #region ImpostoPago2
            dataRow[this.colunasDataTable.ImpostoPago2] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[2]).ImpostoPago;
            #endregion

            #region ImpostoPago3
            dataRow[this.colunasDataTable.ImpostoPago3] = ((ApuracaoRendaVariavelIR)apuracaoRendaVariavelIRCollection[3]).ImpostoPago;
            #endregion

            #endregion

            //
            this.dataTable.Rows.Add(dataRow);			
            //
            return this.dataTable;        
        }
	}
}

