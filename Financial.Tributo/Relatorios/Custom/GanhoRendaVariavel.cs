/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           10/4/2007 15:14:38
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Tributo.Relatorio {
    public partial class GanhoRendaVariavel : esGanhoRendaVariavel {
        decimal resultadoAcoes;

        public decimal ResultadoAcoes {
            get { return resultadoAcoes; }
            set { resultadoAcoes = value; }
        }
        decimal resultadoAcoes1;

        public decimal ResultadoAcoes1 {
            get { return resultadoAcoes1; }
            set { resultadoAcoes1 = value; }
        }
        decimal resultadoAcoes2;

        public decimal ResultadoAcoes2 {
            get { return resultadoAcoes2; }
            set { resultadoAcoes2 = value; }
        }
        decimal resultadoAcoes3;

        public decimal ResultadoAcoes3 {
            get { return resultadoAcoes3; }
            set { resultadoAcoes3 = value; }
        }

        decimal resultadoOuro;

        public decimal ResultadoOuro {
            get { return resultadoOuro; }
            set { resultadoOuro = value; }
        }
        decimal resultadoOuro1;

        public decimal ResultadoOuro1 {
            get { return resultadoOuro1; }
            set { resultadoOuro1 = value; }
        }
        decimal resultadoOuro2;

        public decimal ResultadoOuro2 {
            get { return resultadoOuro2; }
            set { resultadoOuro2 = value; }
        }
        decimal resultadoOuro3;

        public decimal ResultadoOuro3 {
            get { return resultadoOuro3; }
            set { resultadoOuro3 = value; }
        }

        decimal resultadoOuroNaoBolsa;

        public decimal ResultadoOuroNaoBolsa {
            get { return resultadoOuroNaoBolsa; }
            set { resultadoOuroNaoBolsa = value; }
        }
        decimal resultadoOuroNaoBolsa1;

        public decimal ResultadoOuroNaoBolsa1 {
            get { return resultadoOuroNaoBolsa1; }
            set { resultadoOuroNaoBolsa1 = value; }
        }
        decimal resultadoOuroNaoBolsa2;

        public decimal ResultadoOuroNaoBolsa2 {
            get { return resultadoOuroNaoBolsa2; }
            set { resultadoOuroNaoBolsa2 = value; }
        }
        decimal resultadoOuroNaoBolsa3;

        public decimal ResultadoOuroNaoBolsa3 {
            get { return resultadoOuroNaoBolsa3; }
            set { resultadoOuroNaoBolsa3 = value; }
        }

        decimal resultadoOpcoes;

        public decimal ResultadoOpcoes {
            get { return resultadoOpcoes; }
            set { resultadoOpcoes = value; }
        }
        decimal resultadoOpcoes1;

        public decimal ResultadoOpcoes1 {
            get { return resultadoOpcoes1; }
            set { resultadoOpcoes1 = value; }
        }
        decimal resultadoOpcoes2;

        public decimal ResultadoOpcoes2 {
            get { return resultadoOpcoes2; }
            set { resultadoOpcoes2 = value; }
        }
        decimal resultadoOpcoes3;

        public decimal ResultadoOpcoes3 {
            get { return resultadoOpcoes3; }
            set { resultadoOpcoes3 = value; }
        }

        decimal resultadoOpcoesOuro;

        public decimal ResultadoOpcoesOuro {
            get { return resultadoOpcoesOuro; }
            set { resultadoOpcoesOuro = value; }
        }
        decimal resultadoOpcoesOuro1;

        public decimal ResultadoOpcoesOuro1 {
            get { return resultadoOpcoesOuro1; }
            set { resultadoOpcoesOuro1 = value; }
        }
        decimal resultadoOpcoesOuro2;

        public decimal ResultadoOpcoesOuro2 {
            get { return resultadoOpcoesOuro2; }
            set { resultadoOpcoesOuro2 = value; }
        }
        decimal resultadoOpcoesOuro3;

        public decimal ResultadoOpcoesOuro3 {
            get { return resultadoOpcoesOuro3; }
            set { resultadoOpcoesOuro3 = value; }
        }

        decimal resultadoOpcoesNaoBolsa;

        public decimal ResultadoOpcoesNaoBolsa {
            get { return resultadoOpcoesNaoBolsa; }
            set { resultadoOpcoesNaoBolsa = value; }
        }
        decimal resultadoOpcoesNaoBolsa1;

        public decimal ResultadoOpcoesNaoBolsa1 {
            get { return resultadoOpcoesNaoBolsa1; }
            set { resultadoOpcoesNaoBolsa1 = value; }
        }
        decimal resultadoOpcoesNaoBolsa2;

        public decimal ResultadoOpcoesNaoBolsa2 {
            get { return resultadoOpcoesNaoBolsa2; }
            set { resultadoOpcoesNaoBolsa2 = value; }
        }
        decimal resultadoOpcoesNaoBolsa3;

        public decimal ResultadoOpcoesNaoBolsa3 {
            get { return resultadoOpcoesNaoBolsa3; }
            set { resultadoOpcoesNaoBolsa3 = value; }
        }

        decimal resultadoOpcoesOutros;

        public decimal ResultadoOpcoesOutros {
            get { return resultadoOpcoesOutros; }
            set { resultadoOpcoesOutros = value; }
        }
        decimal resultadoOpcoesOutros1;

        public decimal ResultadoOpcoesOutros1 {
            get { return resultadoOpcoesOutros1; }
            set { resultadoOpcoesOutros1 = value; }
        }
        decimal resultadoOpcoesOutros2;

        public decimal ResultadoOpcoesOutros2 {
            get { return resultadoOpcoesOutros2; }
            set { resultadoOpcoesOutros2 = value; }
        }
        decimal resultadoOpcoesOutros3;

        public decimal ResultadoOpcoesOutros3 {
            get { return resultadoOpcoesOutros3; }
            set { resultadoOpcoesOutros3 = value; }
        }

        decimal resultadoFuturoDolar;

        public decimal ResultadoFuturoDolar {
            get { return resultadoFuturoDolar; }
            set { resultadoFuturoDolar = value; }
        }
        decimal resultadoFuturoDolar1;

        public decimal ResultadoFuturoDolar1 {
            get { return resultadoFuturoDolar1; }
            set { resultadoFuturoDolar1 = value; }
        }
        decimal resultadoFuturoDolar2;

        public decimal ResultadoFuturoDolar2 {
            get { return resultadoFuturoDolar2; }
            set { resultadoFuturoDolar2 = value; }
        }
        decimal resultadoFuturoDolar3;

        public decimal ResultadoFuturoDolar3 {
            get { return resultadoFuturoDolar3; }
            set { resultadoFuturoDolar3 = value; }
        }

        decimal resultadoFuturoIndice;

        public decimal ResultadoFuturoIndice {
            get { return resultadoFuturoIndice; }
            set { resultadoFuturoIndice = value; }
        }
        decimal resultadoFuturoIndice1;

        public decimal ResultadoFuturoIndice1 {
            get { return resultadoFuturoIndice1; }
            set { resultadoFuturoIndice1 = value; }
        }
        decimal resultadoFuturoIndice2;

        public decimal ResultadoFuturoIndice2 {
            get { return resultadoFuturoIndice2; }
            set { resultadoFuturoIndice2 = value; }
        }
        decimal resultadoFuturoIndice3;

        public decimal ResultadoFuturoIndice3 {
            get { return resultadoFuturoIndice3; }
            set { resultadoFuturoIndice3 = value; }
        }

        decimal resultadoFuturoJuros;

        public decimal ResultadoFuturoJuros {
            get { return resultadoFuturoJuros; }
            set { resultadoFuturoJuros = value; }
        }
        decimal resultadoFuturoJuros1;

        public decimal ResultadoFuturoJuros1 {
            get { return resultadoFuturoJuros1; }
            set { resultadoFuturoJuros1 = value; }
        }
        decimal resultadoFuturoJuros2;

        public decimal ResultadoFuturoJuros2 {
            get { return resultadoFuturoJuros2; }
            set { resultadoFuturoJuros2 = value; }
        }
        decimal resultadoFuturoJuros3;

        public decimal ResultadoFuturoJuros3 {
            get { return resultadoFuturoJuros3; }
            set { resultadoFuturoJuros3 = value; }
        }

        decimal resultadoFuturoOutros;

        public decimal ResultadoFuturoOutros {
            get { return resultadoFuturoOutros; }
            set { resultadoFuturoOutros = value; }
        }
        decimal resultadoFuturoOutros1;

        public decimal ResultadoFuturoOutros1 {
            get { return resultadoFuturoOutros1; }
            set { resultadoFuturoOutros1 = value; }
        }
        decimal resultadoFuturoOutros2;

        public decimal ResultadoFuturoOutros2 {
            get { return resultadoFuturoOutros2; }
            set { resultadoFuturoOutros2 = value; }
        }
        decimal resultadoFuturoOutros3;

        public decimal ResultadoFuturoOutros3 {
            get { return resultadoFuturoOutros3; }
            set { resultadoFuturoOutros3 = value; }
        }

        decimal resultadoTermo;

        public decimal ResultadoTermo {
            get { return resultadoTermo; }
            set { resultadoTermo = value; }
        }
        decimal resultadoTermo1;

        public decimal ResultadoTermo1 {
            get { return resultadoTermo1; }
            set { resultadoTermo1 = value; }
        }
        decimal resultadoTermo2;

        public decimal ResultadoTermo2 {
            get { return resultadoTermo2; }
            set { resultadoTermo2 = value; }
        }
        decimal resultadoTermo3;

        public decimal ResultadoTermo3 {
            get { return resultadoTermo3; }
            set { resultadoTermo3 = value; }
        }

        decimal resultadoTermoOutros;

        public decimal ResultadoTermoOutros {
            get { return resultadoTermoOutros; }
            set { resultadoTermoOutros = value; }
        }

        decimal resultadoTermoOutros1;

        public decimal ResultadoTermoOutros1 {
            get { return resultadoTermoOutros1; }
            set { resultadoTermoOutros1 = value; }
        }
        decimal resultadoTermoOutros2;

        public decimal ResultadoTermoOutros2 {
            get { return resultadoTermoOutros2; }
            set { resultadoTermoOutros2 = value; }
        }
        decimal resultadoTermoOutros3;

        public decimal ResultadoTermoOutros3 {
            get { return resultadoTermoOutros3; }
            set { resultadoTermoOutros3 = value; }
        }

        decimal resultadoAcoesDT;
        public decimal ResultadoAcoesDT {
            get { return resultadoAcoesDT; }
            set { resultadoAcoesDT = value; }
        }

        decimal resultadoAcoesDT1;
        public decimal ResultadoAcoesDT1 {
            get { return resultadoAcoesDT1; }
            set { resultadoAcoesDT1 = value; }
        }
        decimal resultadoAcoesDT2;

        public decimal ResultadoAcoesDT2 {
            get { return resultadoAcoesDT2; }
            set { resultadoAcoesDT2 = value; }
        }
        decimal resultadoAcoesDT3;

        public decimal ResultadoAcoesDT3 {
            get { return resultadoAcoesDT3; }
            set { resultadoAcoesDT3 = value; }
        }

        decimal resultadoOuroDT;

        public decimal ResultadoOuroDT {
            get { return resultadoOuroDT; }
            set { resultadoOuroDT = value; }
        }
        decimal resultadoOuroDT1;

        public decimal ResultadoOuroDT1 {
            get { return resultadoOuroDT1; }
            set { resultadoOuroDT1 = value; }
        }
        decimal resultadoOuroDT2;

        public decimal ResultadoOuroDT2 {
            get { return resultadoOuroDT2; }
            set { resultadoOuroDT2 = value; }
        }
        decimal resultadoOuroDT3;

        public decimal ResultadoOuroDT3 {
            get { return resultadoOuroDT3; }
            set { resultadoOuroDT3 = value; }
        }

        decimal resultadoOuroNaoBolsaDT;

        public decimal ResultadoOuroNaoBolsaDT {
            get { return resultadoOuroNaoBolsaDT; }
            set { resultadoOuroNaoBolsaDT = value; }
        }
        decimal resultadoOuroNaoBolsaDT1;

        public decimal ResultadoOuroNaoBolsaDT1 {
            get { return resultadoOuroNaoBolsaDT1; }
            set { resultadoOuroNaoBolsaDT1 = value; }
        }
        decimal resultadoOuroNaoBolsaDT2;

        public decimal ResultadoOuroNaoBolsaDT2 {
            get { return resultadoOuroNaoBolsaDT2; }
            set { resultadoOuroNaoBolsaDT2 = value; }
        }
        decimal resultadoOuroNaoBolsaDT3;

        public decimal ResultadoOuroNaoBolsaDT3 {
            get { return resultadoOuroNaoBolsaDT3; }
            set { resultadoOuroNaoBolsaDT3 = value; }
        }

        decimal resultadoOpcoesDT;

        public decimal ResultadoOpcoesDT {
            get { return resultadoOpcoesDT; }
            set { resultadoOpcoesDT = value; }
        }
        decimal resultadoOpcoesDT1;

        public decimal ResultadoOpcoesDT1 {
            get { return resultadoOpcoesDT1; }
            set { resultadoOpcoesDT1 = value; }
        }
        decimal resultadoOpcoesDT2;

        public decimal ResultadoOpcoesDT2 {
            get { return resultadoOpcoesDT2; }
            set { resultadoOpcoesDT2 = value; }
        }
        decimal resultadoOpcoesDT3;

        public decimal ResultadoOpcoesDT3 {
            get { return resultadoOpcoesDT3; }
            set { resultadoOpcoesDT3 = value; }
        }

        decimal resultadoOpcoesOuroDT;

        public decimal ResultadoOpcoesOuroDT {
            get { return resultadoOpcoesOuroDT; }
            set { resultadoOpcoesOuroDT = value; }
        }

        decimal resultadoOpcoesOuroDT1;

        public decimal ResultadoOpcoesOuroDT1 {
            get { return resultadoOpcoesOuroDT1; }
            set { resultadoOpcoesOuroDT1 = value; }
        }
        decimal resultadoOpcoesOuroDT2;

        public decimal ResultadoOpcoesOuroDT2 {
            get { return resultadoOpcoesOuroDT2; }
            set { resultadoOpcoesOuroDT2 = value; }
        }
        decimal resultadoOpcoesOuroDT3;

        public decimal ResultadoOpcoesOuroDT3 {
            get { return resultadoOpcoesOuroDT3; }
            set { resultadoOpcoesOuroDT3 = value; }
        }

        decimal resultadoOpcoesNaoBolsaDT;

        public decimal ResultadoOpcoesNaoBolsaDT {
            get { return resultadoOpcoesNaoBolsaDT; }
            set { resultadoOpcoesNaoBolsaDT = value; }
        }

        decimal resultadoOpcoesNaoBolsaDT1;

        public decimal ResultadoOpcoesNaoBolsaDT1 {
            get { return resultadoOpcoesNaoBolsaDT1; }
            set { resultadoOpcoesNaoBolsaDT1 = value; }
        }
        decimal resultadoOpcoesNaoBolsaDT2;

        public decimal ResultadoOpcoesNaoBolsaDT2 {
            get { return resultadoOpcoesNaoBolsaDT2; }
            set { resultadoOpcoesNaoBolsaDT2 = value; }
        }
        decimal resultadoOpcoesNaoBolsaDT3;

        public decimal ResultadoOpcoesNaoBolsaDT3 {
            get { return resultadoOpcoesNaoBolsaDT3; }
            set { resultadoOpcoesNaoBolsaDT3 = value; }
        }

        decimal resultadoOpcoesOutrosDT;

        public decimal ResultadoOpcoesOutrosDT {
            get { return resultadoOpcoesOutrosDT; }
            set { resultadoOpcoesOutrosDT = value; }
        }
        decimal resultadoOpcoesOutrosDT1;

        public decimal ResultadoOpcoesOutrosDT1 {
            get { return resultadoOpcoesOutrosDT1; }
            set { resultadoOpcoesOutrosDT1 = value; }
        }
        decimal resultadoOpcoesOutrosDT2;

        public decimal ResultadoOpcoesOutrosDT2 {
            get { return resultadoOpcoesOutrosDT2; }
            set { resultadoOpcoesOutrosDT2 = value; }
        }
        decimal resultadoOpcoesOutrosDT3;

        public decimal ResultadoOpcoesOutrosDT3 {
            get { return resultadoOpcoesOutrosDT3; }
            set { resultadoOpcoesOutrosDT3 = value; }
        }

        decimal resultadoFuturoDolarDT;

        public decimal ResultadoFuturoDolarDT {
            get { return resultadoFuturoDolarDT; }
            set { resultadoFuturoDolarDT = value; }
        }
        decimal resultadoFuturoDolarDT1;

        public decimal ResultadoFuturoDolarDT1 {
            get { return resultadoFuturoDolarDT1; }
            set { resultadoFuturoDolarDT1 = value; }
        }
        decimal resultadoFuturoDolarDT2;

        public decimal ResultadoFuturoDolarDT2 {
            get { return resultadoFuturoDolarDT2; }
            set { resultadoFuturoDolarDT2 = value; }
        }
        decimal resultadoFuturoDolarDT3;

        public decimal ResultadoFuturoDolarDT3 {
            get { return resultadoFuturoDolarDT3; }
            set { resultadoFuturoDolarDT3 = value; }
        }

        decimal resultadoFuturoIndiceDT;

        public decimal ResultadoFuturoIndiceDT {
            get { return resultadoFuturoIndiceDT; }
            set { resultadoFuturoIndiceDT = value; }
        }
        decimal resultadoFuturoIndiceDT1;

        public decimal ResultadoFuturoIndiceDT1 {
            get { return resultadoFuturoIndiceDT1; }
            set { resultadoFuturoIndiceDT1 = value; }
        }
        decimal resultadoFuturoIndiceDT2;

        public decimal ResultadoFuturoIndiceDT2 {
            get { return resultadoFuturoIndiceDT2; }
            set { resultadoFuturoIndiceDT2 = value; }
        }
        decimal resultadoFuturoIndiceDT3;

        public decimal ResultadoFuturoIndiceDT3 {
            get { return resultadoFuturoIndiceDT3; }
            set { resultadoFuturoIndiceDT3 = value; }
        }

        decimal resultadoFuturoJurosDT;

        public decimal ResultadoFuturoJurosDT {
            get { return resultadoFuturoJurosDT; }
            set { resultadoFuturoJurosDT = value; }
        }
        decimal resultadoFuturoJurosDT1;

        public decimal ResultadoFuturoJurosDT1 {
            get { return resultadoFuturoJurosDT1; }
            set { resultadoFuturoJurosDT1 = value; }
        }
        decimal resultadoFuturoJurosDT2;

        public decimal ResultadoFuturoJurosDT2 {
            get { return resultadoFuturoJurosDT2; }
            set { resultadoFuturoJurosDT2 = value; }
        }
        decimal resultadoFuturoJurosDT3;

        public decimal ResultadoFuturoJurosDT3 {
            get { return resultadoFuturoJurosDT3; }
            set { resultadoFuturoJurosDT3 = value; }
        }

        decimal resultadoFuturoOutrosDT;

        public decimal ResultadoFuturoOutrosDT {
            get { return resultadoFuturoOutrosDT; }
            set { resultadoFuturoOutrosDT = value; }
        }
        decimal resultadoFuturoOutrosDT1;

        public decimal ResultadoFuturoOutrosDT1 {
            get { return resultadoFuturoOutrosDT1; }
            set { resultadoFuturoOutrosDT1 = value; }
        }
        decimal resultadoFuturoOutrosDT2;

        public decimal ResultadoFuturoOutrosDT2 {
            get { return resultadoFuturoOutrosDT2; }
            set { resultadoFuturoOutrosDT2 = value; }
        }
        decimal resultadoFuturoOutrosDT3;

        public decimal ResultadoFuturoOutrosDT3 {
            get { return resultadoFuturoOutrosDT3; }
            set { resultadoFuturoOutrosDT3 = value; }
        }

        decimal resultadoTermoDT;

        public decimal ResultadoTermoDT {
            get { return resultadoTermoDT; }
            set { resultadoTermoDT = value; }
        }

        decimal resultadoTermoDT1;

        public decimal ResultadoTermoDT1 {
            get { return resultadoTermoDT1; }
            set { resultadoTermoDT1 = value; }
        }
        decimal resultadoTermoDT2;

        public decimal ResultadoTermoDT2 {
            get { return resultadoTermoDT2; }
            set { resultadoTermoDT2 = value; }
        }
        decimal resultadoTermoDT3;

        public decimal ResultadoTermoDT3 {
            get { return resultadoTermoDT3; }
            set { resultadoTermoDT3 = value; }
        }

        decimal resultadoTermoOutrosDT;

        public decimal ResultadoTermoOutrosDT {
            get { return resultadoTermoOutrosDT; }
            set { resultadoTermoOutrosDT = value; }
        }
        decimal resultadoTermoOutrosDT1;

        public decimal ResultadoTermoOutrosDT1 {
            get { return resultadoTermoOutrosDT1; }
            set { resultadoTermoOutrosDT1 = value; }
        }
        decimal resultadoTermoOutrosDT2;

        public decimal ResultadoTermoOutrosDT2 {
            get { return resultadoTermoOutrosDT2; }
            set { resultadoTermoOutrosDT2 = value; }
        }
        decimal resultadoTermoOutrosDT3;

        public decimal ResultadoTermoOutrosDT3 {
            get { return resultadoTermoOutrosDT3; }
            set { resultadoTermoOutrosDT3 = value; }
        }

        decimal resultadoLiquidoMes;

        public decimal ResultadoLiquidoMes {
            get { return resultadoLiquidoMes; }
            set { resultadoLiquidoMes = value; }
        }
        decimal resultadoLiquidoMes1;

        public decimal ResultadoLiquidoMes1 {
            get { return resultadoLiquidoMes1; }
            set { resultadoLiquidoMes1 = value; }
        }
        decimal resultadoLiquidoMes2;

        public decimal ResultadoLiquidoMes2 {
            get { return resultadoLiquidoMes2; }
            set { resultadoLiquidoMes2 = value; }
        }
        decimal resultadoLiquidoMes3;

        public decimal ResultadoLiquidoMes3 {
            get { return resultadoLiquidoMes3; }
            set { resultadoLiquidoMes3 = value; }
        }

        decimal resultadoNegativoAnterior;

        public decimal ResultadoNegativoAnterior {
            get { return resultadoNegativoAnterior; }
            set { resultadoNegativoAnterior = value; }
        }
        decimal resultadoNegativoAnterior1;

        public decimal ResultadoNegativoAnterior1 {
            get { return resultadoNegativoAnterior1; }
            set { resultadoNegativoAnterior1 = value; }
        }
        decimal resultadoNegativoAnterior2;

        public decimal ResultadoNegativoAnterior2 {
            get { return resultadoNegativoAnterior2; }
            set { resultadoNegativoAnterior2 = value; }
        }
        decimal resultadoNegativoAnterior3;

        public decimal ResultadoNegativoAnterior3 {
            get { return resultadoNegativoAnterior3; }
            set { resultadoNegativoAnterior3 = value; }
        }

        decimal baseCalculo;

        public decimal BaseCalculo {
            get { return baseCalculo; }
            set { baseCalculo = value; }
        }
        decimal baseCalculo1;

        public decimal BaseCalculo1 {
            get { return baseCalculo1; }
            set { baseCalculo1 = value; }
        }
        decimal baseCalculo2;

        public decimal BaseCalculo2 {
            get { return baseCalculo2; }
            set { baseCalculo2 = value; }
        }
        decimal baseCalculo3;

        public decimal BaseCalculo3 {
            get { return baseCalculo3; }
            set { baseCalculo3 = value; }
        }

        decimal prejuizoCompensar;

        public decimal PrejuizoCompensar {
            get { return prejuizoCompensar; }
            set { prejuizoCompensar = value; }
        }

        decimal prejuizoCompensar1;

        public decimal PrejuizoCompensar1 {
            get { return prejuizoCompensar1; }
            set { prejuizoCompensar1 = value; }
        }
        decimal prejuizoCompensar2;

        public decimal PrejuizoCompensar2 {
            get { return prejuizoCompensar2; }
            set { prejuizoCompensar2 = value; }
        }
        decimal prejuizoCompensar3;

        public decimal PrejuizoCompensar3 {
            get { return prejuizoCompensar3; }
            set { prejuizoCompensar3 = value; }
        }

        decimal impostoCalculado;

        public decimal ImpostoCalculado {
            get { return impostoCalculado; }
            set { impostoCalculado = value; }
        }
        decimal impostoCalculado1;

        public decimal ImpostoCalculado1 {
            get { return impostoCalculado1; }
            set { impostoCalculado1 = value; }
        }
        decimal impostoCalculado2;

        public decimal ImpostoCalculado2 {
            get { return impostoCalculado2; }
            set { impostoCalculado2 = value; }
        }
        decimal impostoCalculado3;

        public decimal ImpostoCalculado3 {
            get { return impostoCalculado3; }
            set { impostoCalculado3 = value; }
        }

        decimal resultadoLiquidoMesDT;

        public decimal ResultadoLiquidoMesDT {
            get { return resultadoLiquidoMesDT; }
            set { resultadoLiquidoMesDT = value; }
        }
        decimal resultadoLiquidoMesDT1;

        public decimal ResultadoLiquidoMesDT1 {
            get { return resultadoLiquidoMesDT1; }
            set { resultadoLiquidoMesDT1 = value; }
        }
        decimal resultadoLiquidoMesDT2;

        public decimal ResultadoLiquidoMesDT2 {
            get { return resultadoLiquidoMesDT2; }
            set { resultadoLiquidoMesDT2 = value; }
        }
        decimal resultadoLiquidoMesDT3;

        public decimal ResultadoLiquidoMesDT3 {
            get { return resultadoLiquidoMesDT3; }
            set { resultadoLiquidoMesDT3 = value; }
        }

        decimal resultadoNegativoAnteriorDT;

        public decimal ResultadoNegativoAnteriorDT {
            get { return resultadoNegativoAnteriorDT; }
            set { resultadoNegativoAnteriorDT = value; }
        }
        decimal resultadoNegativoAnteriorDT1;

        public decimal ResultadoNegativoAnteriorDT1 {
            get { return resultadoNegativoAnteriorDT1; }
            set { resultadoNegativoAnteriorDT1 = value; }
        }
        decimal resultadoNegativoAnteriorDT2;

        public decimal ResultadoNegativoAnteriorDT2 {
            get { return resultadoNegativoAnteriorDT2; }
            set { resultadoNegativoAnteriorDT2 = value; }
        }
        decimal resultadoNegativoAnteriorDT3;

        public decimal ResultadoNegativoAnteriorDT3 {
            get { return resultadoNegativoAnteriorDT3; }
            set { resultadoNegativoAnteriorDT3 = value; }
        }

        decimal baseCalculoDT;

        public decimal BaseCalculoDT {
            get { return baseCalculoDT; }
            set { baseCalculoDT = value; }
        }
        decimal baseCalculoDT1;

        public decimal BaseCalculoDT1 {
            get { return baseCalculoDT1; }
            set { baseCalculoDT1 = value; }
        }
        decimal baseCalculoDT2;

        public decimal BaseCalculoDT2 {
            get { return baseCalculoDT2; }
            set { baseCalculoDT2 = value; }
        }
        decimal baseCalculoDT3;

        public decimal BaseCalculoDT3 {
            get { return baseCalculoDT3; }
            set { baseCalculoDT3 = value; }
        }

        decimal prejuizoCompensarDT;

        public decimal PrejuizoCompensarDT {
            get { return prejuizoCompensarDT; }
            set { prejuizoCompensarDT = value; }
        }
        decimal prejuizoCompensarDT1;

        public decimal PrejuizoCompensarDT1 {
            get { return prejuizoCompensarDT1; }
            set { prejuizoCompensarDT1 = value; }
        }
        decimal prejuizoCompensarDT2;

        public decimal PrejuizoCompensarDT2 {
            get { return prejuizoCompensarDT2; }
            set { prejuizoCompensarDT2 = value; }
        }
        decimal prejuizoCompensarDT3;

        public decimal PrejuizoCompensarDT3 {
            get { return prejuizoCompensarDT3; }
            set { prejuizoCompensarDT3 = value; }
        }

        decimal impostoCalculadoDT;

        public decimal ImpostoCalculadoDT {
            get { return impostoCalculadoDT; }
            set { impostoCalculadoDT = value; }
        }
        decimal impostoCalculadoDT1;

        public decimal ImpostoCalculadoDT1 {
            get { return impostoCalculadoDT1; }
            set { impostoCalculadoDT1 = value; }
        }
        decimal impostoCalculadoDT2;

        public decimal ImpostoCalculadoDT2 {
            get { return impostoCalculadoDT2; }
            set { impostoCalculadoDT2 = value; }
        }
        decimal impostoCalculadoDT3;

        public decimal ImpostoCalculadoDT3 {
            get { return impostoCalculadoDT3; }
            set { impostoCalculadoDT3 = value; }
        }

        decimal totalImposto;

        public decimal TotalImposto {
            get { return totalImposto; }
            set { totalImposto = value; }
        }
        decimal totalImposto1;

        public decimal TotalImposto1 {
            get { return totalImposto1; }
            set { totalImposto1 = value; }
        }
        decimal totalImposto2;

        public decimal TotalImposto2 {
            get { return totalImposto2; }
            set { totalImposto2 = value; }
        }
        decimal totalImposto3;

        public decimal TotalImposto3 {
            get { return totalImposto3; }
            set { totalImposto3 = value; }
        }

        decimal iRDayTradeMes;

        public decimal IRDayTradeMes {
            get { return iRDayTradeMes; }
            set { iRDayTradeMes = value; }
        }
        decimal iRDayTradeMes1;

        public decimal IRDayTradeMes1 {
            get { return iRDayTradeMes1; }
            set { iRDayTradeMes1 = value; }
        }
        decimal iRDayTradeMes2;

        public decimal IRDayTradeMes2 {
            get { return iRDayTradeMes2; }
            set { iRDayTradeMes2 = value; }
        }
        decimal iRDayTradeMes3;

        public decimal IRDayTradeMes3 {
            get { return iRDayTradeMes3; }
            set { iRDayTradeMes3 = value; }
        }

        decimal iRDayTradeMesAnterior;

        public decimal IRDayTradeMesAnterior {
            get { return iRDayTradeMesAnterior; }
            set { iRDayTradeMesAnterior = value; }
        }
        decimal iRDayTradeMesAnterior1;

        public decimal IRDayTradeMesAnterior1 {
            get { return iRDayTradeMesAnterior1; }
            set { iRDayTradeMesAnterior1 = value; }
        }
        decimal iRDayTradeMesAnterior2;

        public decimal IRDayTradeMesAnterior2 {
            get { return iRDayTradeMesAnterior2; }
            set { iRDayTradeMesAnterior2 = value; }
        }
        decimal iRDayTradeMesAnterior3;

        public decimal IRDayTradeMesAnterior3 {
            get { return iRDayTradeMesAnterior3; }
            set { iRDayTradeMesAnterior3 = value; }
        }

        decimal iRDayTradeCompensar;

        public decimal IRDayTradeCompensar {
            get { return iRDayTradeCompensar; }
            set { iRDayTradeCompensar = value; }
        }
        decimal iRDayTradeCompensar1;

        public decimal IRDayTradeCompensar1 {
            get { return iRDayTradeCompensar1; }
            set { iRDayTradeCompensar1 = value; }
        }
        decimal iRDayTradeCompensar2;

        public decimal IRDayTradeCompensar2 {
            get { return iRDayTradeCompensar2; }
            set { iRDayTradeCompensar2 = value; }
        }
        decimal iRDayTradeCompensar3;

        public decimal IRDayTradeCompensar3 {
            get { return iRDayTradeCompensar3; }
            set { iRDayTradeCompensar3 = value; }
        }

        decimal iRFonteNormal;

        public decimal IRFonteNormal {
            get { return iRFonteNormal; }
            set { iRFonteNormal = value; }
        }
        decimal iRFonteNormal1;

        public decimal IRFonteNormal1 {
            get { return iRFonteNormal1; }
            set { iRFonteNormal1 = value; }
        }
        decimal iRFonteNormal2;

        public decimal IRFonteNormal2 {
            get { return iRFonteNormal2; }
            set { iRFonteNormal2 = value; }
        }
        decimal iRFonteNormal3;

        public decimal IRFonteNormal3 {
            get { return iRFonteNormal3; }
            set { iRFonteNormal3 = value; }
        }

        decimal impostoPagar;

        public decimal ImpostoPagar {
            get { return impostoPagar; }
            set { impostoPagar = value; }
        }
        decimal impostoPagar1;

        public decimal ImpostoPagar1 {
            get { return impostoPagar1; }
            set { impostoPagar1 = value; }
        }
        decimal impostoPagar2;

        public decimal ImpostoPagar2 {
            get { return impostoPagar2; }
            set { impostoPagar2 = value; }
        }
        decimal impostoPagar3;

        public decimal ImpostoPagar3 {
            get { return impostoPagar3; }
            set { impostoPagar3 = value; }
        }

        decimal impostoPago;

        public decimal ImpostoPago {
            get { return impostoPago; }
            set { impostoPago = value; }
        }
        decimal impostoPago1;

        public decimal ImpostoPago1 {
            get { return impostoPago1; }
            set { impostoPago1 = value; }
        }
        decimal impostoPago2;

        public decimal ImpostoPago2 {
            get { return impostoPago2; }
            set { impostoPago2 = value; }
        }
        decimal impostoPago3;

        public decimal ImpostoPago3 {
            get { return impostoPago3; }
            set { impostoPago3 = value; }
        }
    }
}
