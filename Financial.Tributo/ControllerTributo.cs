﻿using log4net;
using Financial.Tributo.Enums;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Util.Enums;
using Financial.Investidor.Enums;

namespace Financial.Tributo.Controller
{
    public class ControllerTributo
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ControllerTributo));
                        
        /// <summary>
        /// Realiza todos os cálculos financeiros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCliente, DateTime data)
        {
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(clienteBolsa.Query.IsentoIR);
            clienteBolsa.LoadByPrimaryKey(campos, idCliente);

            if (clienteBolsa.IsentoIR == "N")
            {
                if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoCC == (int)IntegracaoCC.Sinacor_IRFonte)
                {
                    //Se integra do Sinacor o IR Fonte DT, então não calcula no Financial
                    IRFonte irFonte = new IRFonte();
                    irFonte.IntegraIRFonteDayTradeSinacor(idCliente, data);

                    irFonte = new IRFonte();
                    irFonte.CalculaIRFonte(idCliente, data); //0,005% sobre venda de ações, net de opções, baixa de futuros

                    irFonte = new IRFonte();
                    irFonte.CalculaIRFonte_FII(idCliente, data); //0,005% sobre venda de FII
                }
                else
                {
                    IRFonte irFonte = new IRFonte();
                    irFonte.CalculaIRFonteDayTrade(idCliente, data); //Daytrade de bovespa e bmf

                    irFonte = new IRFonte();
                    irFonte.CalculaIRFonteDayTrade_FII(idCliente, data); //Daytrade de FII

                    irFonte = new IRFonte();
                    irFonte.CalculaIRFonte(idCliente, data); //0,005% sobre venda de ações, net de opções, baixa de futuros

                    irFonte = new IRFonte();
                    irFonte.CalculaIRFonte_FII(idCliente, data); //0,005% sobre venda de FII
                }
            }

            Cliente cliente = new Cliente();
            campos = new List<esQueryItem>();
            campos.Add(cliente.Query.ApuraGanhoRV);
            cliente.LoadByPrimaryKey(campos, idCliente);

            if (cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.ApuraComLiquidacao ||
                cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.ApuraSemLiquidacao)
            {
                ApuracaoRendaVariavelIR apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
                apuracaoRendaVariavelIR.ProcessaGanhoRendaVariavel(idCliente, data);

                ApuracaoIRImobiliario apuracaoIRImobiliario = new ApuracaoIRImobiliario();
                apuracaoIRImobiliario.ProcessaGanhoImobiliario(idCliente, data);

                if (cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.ApuraComLiquidacao)
                {
                    apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
                    apuracaoRendaVariavelIR.LancaIRGanhoLiquidacao(idCliente, data);
                }
            }
        }

        /// <summary>
        /// Deleta todos cálculos de ganho de RV da data para a frente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ReprocessaPosicao(int idCliente, DateTime data)
        {
            ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollection = new ApuracaoRendaVariavelIRCollection();
            apuracaoRendaVariavelIRCollection.DeletaApuracaoDataMaior(idCliente, data.Year, data.Month);

            ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollection = new ApuracaoIRImobiliarioCollection();
            apuracaoIRImobiliarioCollection.DeletaApuracaoDataMaior(idCliente, data.Year, data.Month);
        }

    }
}
