/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/10/2013 17:26:37
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Tributo
{

	[Serializable]
	abstract public class esTabelaIOFCollection : esEntityCollection
	{
		public esTabelaIOFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaIOFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaIOFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaIOFQuery);
		}
		#endregion
		
		virtual public TabelaIOF DetachEntity(TabelaIOF entity)
		{
			return base.DetachEntity(entity) as TabelaIOF;
		}
		
		virtual public TabelaIOF AttachEntity(TabelaIOF entity)
		{
			return base.AttachEntity(entity) as TabelaIOF;
		}
		
		virtual public void Combine(TabelaIOFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaIOF this[int index]
		{
			get
			{
				return base[index] as TabelaIOF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaIOF);
		}
	}



	[Serializable]
	abstract public class esTabelaIOF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaIOFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaIOF()
		{

		}

		public esTabelaIOF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 prazo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(prazo);
			else
				return LoadByPrimaryKeyStoredProcedure(prazo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 prazo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaIOFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Prazo == prazo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 prazo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(prazo);
			else
				return LoadByPrimaryKeyStoredProcedure(prazo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 prazo)
		{
			esTabelaIOFQuery query = this.GetDynamicQuery();
			query.Where(query.Prazo == prazo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 prazo)
		{
			esParameters parms = new esParameters();
			parms.Add("Prazo",prazo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Prazo": this.str.Prazo = (string)value; break;							
						case "AliquotaIOF": this.str.AliquotaIOF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Prazo":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.Prazo = (System.Int16?)value;
							break;
						
						case "AliquotaIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIOF = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaIOF.Prazo
		/// </summary>
		virtual public System.Int16? Prazo
		{
			get
			{
				return base.GetSystemInt16(TabelaIOFMetadata.ColumnNames.Prazo);
			}
			
			set
			{
				base.SetSystemInt16(TabelaIOFMetadata.ColumnNames.Prazo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaIOF.AliquotaIOF
		/// </summary>
		virtual public System.Decimal? AliquotaIOF
		{
			get
			{
				return base.GetSystemDecimal(TabelaIOFMetadata.ColumnNames.AliquotaIOF);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaIOFMetadata.ColumnNames.AliquotaIOF, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaIOF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Prazo
			{
				get
				{
					System.Int16? data = entity.Prazo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Prazo = null;
					else entity.Prazo = Convert.ToInt16(value);
				}
			}
				
			public System.String AliquotaIOF
			{
				get
				{
					System.Decimal? data = entity.AliquotaIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIOF = null;
					else entity.AliquotaIOF = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaIOF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaIOFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaIOF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaIOF : esTabelaIOF
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaIOFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaIOFMetadata.Meta();
			}
		}	
		

		public esQueryItem Prazo
		{
			get
			{
				return new esQueryItem(this, TabelaIOFMetadata.ColumnNames.Prazo, esSystemType.Int16);
			}
		} 
		
		public esQueryItem AliquotaIOF
		{
			get
			{
				return new esQueryItem(this, TabelaIOFMetadata.ColumnNames.AliquotaIOF, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaIOFCollection")]
	public partial class TabelaIOFCollection : esTabelaIOFCollection, IEnumerable<TabelaIOF>
	{
		public TabelaIOFCollection()
		{

		}
		
		public static implicit operator List<TabelaIOF>(TabelaIOFCollection coll)
		{
			List<TabelaIOF> list = new List<TabelaIOF>();
			
			foreach (TabelaIOF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaIOFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaIOFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaIOF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaIOF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaIOFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaIOFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaIOFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaIOF AddNew()
		{
			TabelaIOF entity = base.AddNewEntity() as TabelaIOF;
			
			return entity;
		}

		public TabelaIOF FindByPrimaryKey(System.Int16 prazo)
		{
			return base.FindByPrimaryKey(prazo) as TabelaIOF;
		}


		#region IEnumerable<TabelaIOF> Members

		IEnumerator<TabelaIOF> IEnumerable<TabelaIOF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaIOF;
			}
		}

		#endregion
		
		private TabelaIOFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaIOF' table
	/// </summary>

	[Serializable]
	public partial class TabelaIOF : esTabelaIOF
	{
		public TabelaIOF()
		{

		}
	
		public TabelaIOF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaIOFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaIOFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaIOFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaIOFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaIOFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaIOFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaIOFQuery query;
	}



	[Serializable]
	public partial class TabelaIOFQuery : esTabelaIOFQuery
	{
		public TabelaIOFQuery()
		{

		}		
		
		public TabelaIOFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaIOFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaIOFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaIOFMetadata.ColumnNames.Prazo, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaIOFMetadata.PropertyNames.Prazo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaIOFMetadata.ColumnNames.AliquotaIOF, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaIOFMetadata.PropertyNames.AliquotaIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaIOFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Prazo = "Prazo";
			 public const string AliquotaIOF = "AliquotaIOF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Prazo = "Prazo";
			 public const string AliquotaIOF = "AliquotaIOF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaIOFMetadata))
			{
				if(TabelaIOFMetadata.mapDelegates == null)
				{
					TabelaIOFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaIOFMetadata.meta == null)
				{
					TabelaIOFMetadata.meta = new TabelaIOFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Prazo", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("AliquotaIOF", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaIOF";
				meta.Destination = "TabelaIOF";
				
				meta.spInsert = "proc_TabelaIOFInsert";				
				meta.spUpdate = "proc_TabelaIOFUpdate";		
				meta.spDelete = "proc_TabelaIOFDelete";
				meta.spLoadAll = "proc_TabelaIOFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaIOFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaIOFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
