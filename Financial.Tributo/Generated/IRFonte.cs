/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/10/2013 17:26:37
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				






		
using Financial.Common;
using Financial.Investidor;			






	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Tributo
{

	[Serializable]
	abstract public class esIRFonteCollection : esEntityCollection
	{
		public esIRFonteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IRFonteCollection";
		}

		#region Query Logic
		protected void InitQuery(esIRFonteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIRFonteQuery);
		}
		#endregion
		
		virtual public IRFonte DetachEntity(IRFonte entity)
		{
			return base.DetachEntity(entity) as IRFonte;
		}
		
		virtual public IRFonte AttachEntity(IRFonte entity)
		{
			return base.AttachEntity(entity) as IRFonte;
		}
		
		virtual public void Combine(IRFonteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IRFonte this[int index]
		{
			get
			{
				return base[index] as IRFonte;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IRFonte);
		}
	}



	[Serializable]
	abstract public class esIRFonte : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIRFonteQuery GetDynamicQuery()
		{
			return null;
		}

		public esIRFonte()
		{

		}

		public esIRFonte(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, data, identificador, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, data, identificador, idAgente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esIRFonteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.Data == data, query.Identificador == identificador, query.IdAgente == idAgente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, data, identificador, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, data, identificador, idAgente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			esIRFonteQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.Data == data, query.Identificador == identificador, query.IdAgente == idAgente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("Data",data);			parms.Add("Identificador",identificador);			parms.Add("IdAgente",idAgente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Identificador": this.str.Identificador = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Identificador":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.Identificador = (System.Int16?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IRFonte.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(IRFonteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(IRFonteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IRFonte.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(IRFonteMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(IRFonteMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to IRFonte.Identificador
		/// </summary>
		virtual public System.Int16? Identificador
		{
			get
			{
				return base.GetSystemInt16(IRFonteMetadata.ColumnNames.Identificador);
			}
			
			set
			{
				base.SetSystemInt16(IRFonteMetadata.ColumnNames.Identificador, value);
			}
		}
		
		/// <summary>
		/// Maps to IRFonte.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(IRFonteMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(IRFonteMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IRFonte.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(IRFonteMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(IRFonteMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to IRFonte.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(IRFonteMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(IRFonteMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIRFonte entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Identificador
			{
				get
				{
					System.Int16? data = entity.Identificador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Identificador = null;
					else entity.Identificador = Convert.ToInt16(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
			

			private esIRFonte entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIRFonteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIRFonte can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IRFonte : esIRFonte
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_IRFonte_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_IRFonte_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIRFonteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IRFonteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Identificador
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.Identificador, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, IRFonteMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IRFonteCollection")]
	public partial class IRFonteCollection : esIRFonteCollection, IEnumerable<IRFonte>
	{
		public IRFonteCollection()
		{

		}
		
		public static implicit operator List<IRFonte>(IRFonteCollection coll)
		{
			List<IRFonte> list = new List<IRFonte>();
			
			foreach (IRFonte emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IRFonteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IRFonteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IRFonte(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IRFonte();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IRFonteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IRFonteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IRFonteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IRFonte AddNew()
		{
			IRFonte entity = base.AddNewEntity() as IRFonte;
			
			return entity;
		}

		public IRFonte FindByPrimaryKey(System.Int32 idCliente, System.DateTime data, System.Int16 identificador, System.Int32 idAgente)
		{
			return base.FindByPrimaryKey(idCliente, data, identificador, idAgente) as IRFonte;
		}


		#region IEnumerable<IRFonte> Members

		IEnumerator<IRFonte> IEnumerable<IRFonte>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IRFonte;
			}
		}

		#endregion
		
		private IRFonteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IRFonte' table
	/// </summary>

	[Serializable]
	public partial class IRFonte : esIRFonte
	{
		public IRFonte()
		{

		}
	
		public IRFonte(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IRFonteMetadata.Meta();
			}
		}
		
		
		
		override protected esIRFonteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IRFonteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IRFonteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IRFonteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IRFonteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IRFonteQuery query;
	}



	[Serializable]
	public partial class IRFonteQuery : esIRFonteQuery
	{
		public IRFonteQuery()
		{

		}		
		
		public IRFonteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IRFonteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IRFonteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IRFonteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IRFonteMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.Identificador, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = IRFonteMetadata.PropertyNames.Identificador;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IRFonteMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.ValorBase, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IRFonteMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IRFonteMetadata.ColumnNames.ValorIR, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IRFonteMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IRFonteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string Identificador = "Identificador";
			 public const string IdAgente = "IdAgente";
			 public const string ValorBase = "ValorBase";
			 public const string ValorIR = "ValorIR";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string Identificador = "Identificador";
			 public const string IdAgente = "IdAgente";
			 public const string ValorBase = "ValorBase";
			 public const string ValorIR = "ValorIR";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IRFonteMetadata))
			{
				if(IRFonteMetadata.mapDelegates == null)
				{
					IRFonteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IRFonteMetadata.meta == null)
				{
					IRFonteMetadata.meta = new IRFonteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Identificador", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "IRFonte";
				meta.Destination = "IRFonte";
				
				meta.spInsert = "proc_IRFonteInsert";				
				meta.spUpdate = "proc_IRFonteUpdate";		
				meta.spDelete = "proc_IRFonteDelete";
				meta.spLoadAll = "proc_IRFonteLoadAll";
				meta.spLoadByPrimaryKey = "proc_IRFonteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IRFonteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
