/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:17:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Tributo
{

	[Serializable]
	abstract public class esTabelaProgressivaValoresCollection : esEntityCollection
	{
		public esTabelaProgressivaValoresCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaProgressivaValoresCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaProgressivaValoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaProgressivaValoresQuery);
		}
		#endregion
		
		virtual public TabelaProgressivaValores DetachEntity(TabelaProgressivaValores entity)
		{
			return base.DetachEntity(entity) as TabelaProgressivaValores;
		}
		
		virtual public TabelaProgressivaValores AttachEntity(TabelaProgressivaValores entity)
		{
			return base.AttachEntity(entity) as TabelaProgressivaValores;
		}
		
		virtual public void Combine(TabelaProgressivaValoresCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaProgressivaValores this[int index]
		{
			get
			{
				return base[index] as TabelaProgressivaValores;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaProgressivaValores);
		}
	}



	[Serializable]
	abstract public class esTabelaProgressivaValores : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaProgressivaValoresQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaProgressivaValores()
		{

		}

		public esTabelaProgressivaValores(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabelaProgressiva, System.Decimal valor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaProgressiva, valor);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaProgressiva, valor);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabelaProgressiva, System.Decimal valor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaProgressiva, valor);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaProgressiva, valor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabelaProgressiva, System.Decimal valor)
		{
			esTabelaProgressivaValoresQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabelaProgressiva == idTabelaProgressiva, query.Valor == valor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabelaProgressiva, System.Decimal valor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabelaProgressiva",idTabelaProgressiva);			parms.Add("Valor",valor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabelaProgressiva": this.str.IdTabelaProgressiva = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Aliquota": this.str.Aliquota = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabelaProgressiva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaProgressiva = (System.Int32?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Aliquota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Aliquota = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaProgressivaValores.IdTabelaProgressiva
		/// </summary>
		virtual public System.Int32? IdTabelaProgressiva
		{
			get
			{
				return base.GetSystemInt32(TabelaProgressivaValoresMetadata.ColumnNames.IdTabelaProgressiva);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaProgressivaValoresMetadata.ColumnNames.IdTabelaProgressiva, value))
				{
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaProgressivaValores.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(TabelaProgressivaValoresMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaProgressivaValoresMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProgressivaValores.Aliquota
		/// </summary>
		virtual public System.Decimal? Aliquota
		{
			get
			{
				return base.GetSystemDecimal(TabelaProgressivaValoresMetadata.ColumnNames.Aliquota);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaProgressivaValoresMetadata.ColumnNames.Aliquota, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaProgressivaVigencia _UpToTabelaProgressivaVigenciaByIdTabelaProgressiva;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaProgressivaValores entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabelaProgressiva
			{
				get
				{
					System.Int32? data = entity.IdTabelaProgressiva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaProgressiva = null;
					else entity.IdTabelaProgressiva = Convert.ToInt32(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Aliquota
			{
				get
				{
					System.Decimal? data = entity.Aliquota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Aliquota = null;
					else entity.Aliquota = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaProgressivaValores entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaProgressivaValoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaProgressivaValores can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaProgressivaValores : esTabelaProgressivaValores
	{

				
		#region UpToTabelaProgressivaVigenciaByIdTabelaProgressiva - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaProgressivaValores_TabelaProgressivaVigencia_FK
		/// </summary>

		[XmlIgnore]
		public TabelaProgressivaVigencia UpToTabelaProgressivaVigenciaByIdTabelaProgressiva
		{
			get
			{
				if(this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva == null
					&& IdTabelaProgressiva != null					)
				{
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva = new TabelaProgressivaVigencia();
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaProgressivaVigenciaByIdTabelaProgressiva", this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva);
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva.Query.Where(this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva.Query.IdTabelaProgressiva == this.IdTabelaProgressiva);
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva.Query.Load();
				}

				return this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaProgressivaVigenciaByIdTabelaProgressiva");
				

				if(value == null)
				{
					this.IdTabelaProgressiva = null;
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva = null;
				}
				else
				{
					this.IdTabelaProgressiva = value.IdTabelaProgressiva;
					this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva = value;
					this.SetPreSave("UpToTabelaProgressivaVigenciaByIdTabelaProgressiva", this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva != null)
			{
				this.IdTabelaProgressiva = this._UpToTabelaProgressivaVigenciaByIdTabelaProgressiva.IdTabelaProgressiva;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaProgressivaValoresQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProgressivaValoresMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabelaProgressiva
		{
			get
			{
				return new esQueryItem(this, TabelaProgressivaValoresMetadata.ColumnNames.IdTabelaProgressiva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, TabelaProgressivaValoresMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Aliquota
		{
			get
			{
				return new esQueryItem(this, TabelaProgressivaValoresMetadata.ColumnNames.Aliquota, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaProgressivaValoresCollection")]
	public partial class TabelaProgressivaValoresCollection : esTabelaProgressivaValoresCollection, IEnumerable<TabelaProgressivaValores>
	{
		public TabelaProgressivaValoresCollection()
		{

		}
		
		public static implicit operator List<TabelaProgressivaValores>(TabelaProgressivaValoresCollection coll)
		{
			List<TabelaProgressivaValores> list = new List<TabelaProgressivaValores>();
			
			foreach (TabelaProgressivaValores emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaProgressivaValoresMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProgressivaValoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaProgressivaValores(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaProgressivaValores();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaProgressivaValoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProgressivaValoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaProgressivaValoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaProgressivaValores AddNew()
		{
			TabelaProgressivaValores entity = base.AddNewEntity() as TabelaProgressivaValores;
			
			return entity;
		}

		public TabelaProgressivaValores FindByPrimaryKey(System.Int32 idTabelaProgressiva, System.Decimal valor)
		{
			return base.FindByPrimaryKey(idTabelaProgressiva, valor) as TabelaProgressivaValores;
		}


		#region IEnumerable<TabelaProgressivaValores> Members

		IEnumerator<TabelaProgressivaValores> IEnumerable<TabelaProgressivaValores>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaProgressivaValores;
			}
		}

		#endregion
		
		private TabelaProgressivaValoresQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaProgressivaValores' table
	/// </summary>

	[Serializable]
	public partial class TabelaProgressivaValores : esTabelaProgressivaValores
	{
		public TabelaProgressivaValores()
		{

		}
	
		public TabelaProgressivaValores(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProgressivaValoresMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaProgressivaValoresQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProgressivaValoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaProgressivaValoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProgressivaValoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaProgressivaValoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaProgressivaValoresQuery query;
	}



	[Serializable]
	public partial class TabelaProgressivaValoresQuery : esTabelaProgressivaValoresQuery
	{
		public TabelaProgressivaValoresQuery()
		{

		}		
		
		public TabelaProgressivaValoresQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaProgressivaValoresMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaProgressivaValoresMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaProgressivaValoresMetadata.ColumnNames.IdTabelaProgressiva, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProgressivaValoresMetadata.PropertyNames.IdTabelaProgressiva;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProgressivaValoresMetadata.ColumnNames.Valor, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaProgressivaValoresMetadata.PropertyNames.Valor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProgressivaValoresMetadata.ColumnNames.Aliquota, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaProgressivaValoresMetadata.PropertyNames.Aliquota;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaProgressivaValoresMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabelaProgressiva = "IdTabelaProgressiva";
			 public const string Valor = "Valor";
			 public const string Aliquota = "Aliquota";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabelaProgressiva = "IdTabelaProgressiva";
			 public const string Valor = "Valor";
			 public const string Aliquota = "Aliquota";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaProgressivaValoresMetadata))
			{
				if(TabelaProgressivaValoresMetadata.mapDelegates == null)
				{
					TabelaProgressivaValoresMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaProgressivaValoresMetadata.meta == null)
				{
					TabelaProgressivaValoresMetadata.meta = new TabelaProgressivaValoresMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabelaProgressiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Aliquota", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaProgressivaValores";
				meta.Destination = "TabelaProgressivaValores";
				
				meta.spInsert = "proc_TabelaProgressivaValoresInsert";				
				meta.spUpdate = "proc_TabelaProgressivaValoresUpdate";		
				meta.spDelete = "proc_TabelaProgressivaValoresDelete";
				meta.spLoadAll = "proc_TabelaProgressivaValoresLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaProgressivaValoresLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaProgressivaValoresMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
