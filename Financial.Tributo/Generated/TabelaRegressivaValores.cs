/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:17:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Tributo
{

	[Serializable]
	abstract public class esTabelaRegressivaValoresCollection : esEntityCollection
	{
		public esTabelaRegressivaValoresCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRegressivaValoresCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRegressivaValoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRegressivaValoresQuery);
		}
		#endregion
		
		virtual public TabelaRegressivaValores DetachEntity(TabelaRegressivaValores entity)
		{
			return base.DetachEntity(entity) as TabelaRegressivaValores;
		}
		
		virtual public TabelaRegressivaValores AttachEntity(TabelaRegressivaValores entity)
		{
			return base.AttachEntity(entity) as TabelaRegressivaValores;
		}
		
		virtual public void Combine(TabelaRegressivaValoresCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRegressivaValores this[int index]
		{
			get
			{
				return base[index] as TabelaRegressivaValores;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRegressivaValores);
		}
	}



	[Serializable]
	abstract public class esTabelaRegressivaValores : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRegressivaValoresQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRegressivaValores()
		{

		}

		public esTabelaRegressivaValores(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabelaRegressiva, System.Int32 periodo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaRegressiva, periodo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaRegressiva, periodo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabelaRegressiva, System.Int32 periodo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaRegressiva, periodo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaRegressiva, periodo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabelaRegressiva, System.Int32 periodo)
		{
			esTabelaRegressivaValoresQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabelaRegressiva == idTabelaRegressiva, query.Periodo == periodo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabelaRegressiva, System.Int32 periodo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabelaRegressiva",idTabelaRegressiva);			parms.Add("Periodo",periodo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabelaRegressiva": this.str.IdTabelaRegressiva = (string)value; break;							
						case "Periodo": this.str.Periodo = (string)value; break;							
						case "Aliquota": this.str.Aliquota = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabelaRegressiva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaRegressiva = (System.Int32?)value;
							break;
						
						case "Periodo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Periodo = (System.Int32?)value;
							break;
						
						case "Aliquota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Aliquota = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRegressivaValores.IdTabelaRegressiva
		/// </summary>
		virtual public System.Int32? IdTabelaRegressiva
		{
			get
			{
				return base.GetSystemInt32(TabelaRegressivaValoresMetadata.ColumnNames.IdTabelaRegressiva);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaRegressivaValoresMetadata.ColumnNames.IdTabelaRegressiva, value))
				{
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaRegressivaValores.Periodo
		/// </summary>
		virtual public System.Int32? Periodo
		{
			get
			{
				return base.GetSystemInt32(TabelaRegressivaValoresMetadata.ColumnNames.Periodo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaRegressivaValoresMetadata.ColumnNames.Periodo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRegressivaValores.Aliquota
		/// </summary>
		virtual public System.Decimal? Aliquota
		{
			get
			{
				return base.GetSystemDecimal(TabelaRegressivaValoresMetadata.ColumnNames.Aliquota);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRegressivaValoresMetadata.ColumnNames.Aliquota, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaRegressivaVigencia _UpToTabelaRegressivaVigenciaByIdTabelaRegressiva;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRegressivaValores entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabelaRegressiva
			{
				get
				{
					System.Int32? data = entity.IdTabelaRegressiva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaRegressiva = null;
					else entity.IdTabelaRegressiva = Convert.ToInt32(value);
				}
			}
				
			public System.String Periodo
			{
				get
				{
					System.Int32? data = entity.Periodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Periodo = null;
					else entity.Periodo = Convert.ToInt32(value);
				}
			}
				
			public System.String Aliquota
			{
				get
				{
					System.Decimal? data = entity.Aliquota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Aliquota = null;
					else entity.Aliquota = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaRegressivaValores entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRegressivaValoresQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRegressivaValores can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRegressivaValores : esTabelaRegressivaValores
	{

				
		#region UpToTabelaRegressivaVigenciaByIdTabelaRegressiva - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaRegressivaValores_TabelaRegressivaVigencia_FK
		/// </summary>

		[XmlIgnore]
		public TabelaRegressivaVigencia UpToTabelaRegressivaVigenciaByIdTabelaRegressiva
		{
			get
			{
				if(this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva == null
					&& IdTabelaRegressiva != null					)
				{
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva = new TabelaRegressivaVigencia();
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaRegressivaVigenciaByIdTabelaRegressiva", this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva);
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva.Query.Where(this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva.Query.IdTabelaRegressiva == this.IdTabelaRegressiva);
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva.Query.Load();
				}

				return this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaRegressivaVigenciaByIdTabelaRegressiva");
				

				if(value == null)
				{
					this.IdTabelaRegressiva = null;
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva = null;
				}
				else
				{
					this.IdTabelaRegressiva = value.IdTabelaRegressiva;
					this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva = value;
					this.SetPreSave("UpToTabelaRegressivaVigenciaByIdTabelaRegressiva", this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva != null)
			{
				this.IdTabelaRegressiva = this._UpToTabelaRegressivaVigenciaByIdTabelaRegressiva.IdTabelaRegressiva;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRegressivaValoresQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRegressivaValoresMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabelaRegressiva
		{
			get
			{
				return new esQueryItem(this, TabelaRegressivaValoresMetadata.ColumnNames.IdTabelaRegressiva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Periodo
		{
			get
			{
				return new esQueryItem(this, TabelaRegressivaValoresMetadata.ColumnNames.Periodo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Aliquota
		{
			get
			{
				return new esQueryItem(this, TabelaRegressivaValoresMetadata.ColumnNames.Aliquota, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRegressivaValoresCollection")]
	public partial class TabelaRegressivaValoresCollection : esTabelaRegressivaValoresCollection, IEnumerable<TabelaRegressivaValores>
	{
		public TabelaRegressivaValoresCollection()
		{

		}
		
		public static implicit operator List<TabelaRegressivaValores>(TabelaRegressivaValoresCollection coll)
		{
			List<TabelaRegressivaValores> list = new List<TabelaRegressivaValores>();
			
			foreach (TabelaRegressivaValores emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRegressivaValoresMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRegressivaValoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRegressivaValores(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRegressivaValores();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRegressivaValoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRegressivaValoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRegressivaValoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRegressivaValores AddNew()
		{
			TabelaRegressivaValores entity = base.AddNewEntity() as TabelaRegressivaValores;
			
			return entity;
		}

		public TabelaRegressivaValores FindByPrimaryKey(System.Int32 idTabelaRegressiva, System.Int32 periodo)
		{
			return base.FindByPrimaryKey(idTabelaRegressiva, periodo) as TabelaRegressivaValores;
		}


		#region IEnumerable<TabelaRegressivaValores> Members

		IEnumerator<TabelaRegressivaValores> IEnumerable<TabelaRegressivaValores>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRegressivaValores;
			}
		}

		#endregion
		
		private TabelaRegressivaValoresQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRegressivaValores' table
	/// </summary>

	[Serializable]
	public partial class TabelaRegressivaValores : esTabelaRegressivaValores
	{
		public TabelaRegressivaValores()
		{

		}
	
		public TabelaRegressivaValores(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRegressivaValoresMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRegressivaValoresQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRegressivaValoresQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRegressivaValoresQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRegressivaValoresQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRegressivaValoresQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRegressivaValoresQuery query;
	}



	[Serializable]
	public partial class TabelaRegressivaValoresQuery : esTabelaRegressivaValoresQuery
	{
		public TabelaRegressivaValoresQuery()
		{

		}		
		
		public TabelaRegressivaValoresQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRegressivaValoresMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRegressivaValoresMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRegressivaValoresMetadata.ColumnNames.IdTabelaRegressiva, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRegressivaValoresMetadata.PropertyNames.IdTabelaRegressiva;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRegressivaValoresMetadata.ColumnNames.Periodo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRegressivaValoresMetadata.PropertyNames.Periodo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRegressivaValoresMetadata.ColumnNames.Aliquota, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRegressivaValoresMetadata.PropertyNames.Aliquota;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRegressivaValoresMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabelaRegressiva = "IdTabelaRegressiva";
			 public const string Periodo = "Periodo";
			 public const string Aliquota = "Aliquota";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabelaRegressiva = "IdTabelaRegressiva";
			 public const string Periodo = "Periodo";
			 public const string Aliquota = "Aliquota";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRegressivaValoresMetadata))
			{
				if(TabelaRegressivaValoresMetadata.mapDelegates == null)
				{
					TabelaRegressivaValoresMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRegressivaValoresMetadata.meta == null)
				{
					TabelaRegressivaValoresMetadata.meta = new TabelaRegressivaValoresMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabelaRegressiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Periodo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Aliquota", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaRegressivaValores";
				meta.Destination = "TabelaRegressivaValores";
				
				meta.spInsert = "proc_TabelaRegressivaValoresInsert";				
				meta.spUpdate = "proc_TabelaRegressivaValoresUpdate";		
				meta.spDelete = "proc_TabelaRegressivaValoresDelete";
				meta.spLoadAll = "proc_TabelaRegressivaValoresLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRegressivaValoresLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRegressivaValoresMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
