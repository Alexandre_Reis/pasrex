/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:17:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Tributo
{

	[Serializable]
	abstract public class esTabelaRegressivaVigenciaCollection : esEntityCollection
	{
		public esTabelaRegressivaVigenciaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRegressivaVigenciaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRegressivaVigenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRegressivaVigenciaQuery);
		}
		#endregion
		
		virtual public TabelaRegressivaVigencia DetachEntity(TabelaRegressivaVigencia entity)
		{
			return base.DetachEntity(entity) as TabelaRegressivaVigencia;
		}
		
		virtual public TabelaRegressivaVigencia AttachEntity(TabelaRegressivaVigencia entity)
		{
			return base.AttachEntity(entity) as TabelaRegressivaVigencia;
		}
		
		virtual public void Combine(TabelaRegressivaVigenciaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRegressivaVigencia this[int index]
		{
			get
			{
				return base[index] as TabelaRegressivaVigencia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRegressivaVigencia);
		}
	}



	[Serializable]
	abstract public class esTabelaRegressivaVigencia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRegressivaVigenciaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRegressivaVigencia()
		{

		}

		public esTabelaRegressivaVigencia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabelaRegressiva)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaRegressiva);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaRegressiva);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabelaRegressiva)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaRegressiva);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaRegressiva);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabelaRegressiva)
		{
			esTabelaRegressivaVigenciaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabelaRegressiva == idTabelaRegressiva);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabelaRegressiva)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabelaRegressiva",idTabelaRegressiva);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabelaRegressiva": this.str.IdTabelaRegressiva = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabelaRegressiva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaRegressiva = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRegressivaVigencia.IdTabelaRegressiva
		/// </summary>
		virtual public System.Int32? IdTabelaRegressiva
		{
			get
			{
				return base.GetSystemInt32(TabelaRegressivaVigenciaMetadata.ColumnNames.IdTabelaRegressiva);
			}
			
			set
			{
				base.SetSystemInt32(TabelaRegressivaVigenciaMetadata.ColumnNames.IdTabelaRegressiva, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRegressivaVigencia.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaRegressivaVigenciaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRegressivaVigenciaMetadata.ColumnNames.Data, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRegressivaVigencia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabelaRegressiva
			{
				get
				{
					System.Int32? data = entity.IdTabelaRegressiva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaRegressiva = null;
					else entity.IdTabelaRegressiva = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
			

			private esTabelaRegressivaVigencia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRegressivaVigenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRegressivaVigencia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRegressivaVigencia : esTabelaRegressivaVigencia
	{

				
		#region TabelaRegressivaValoresCollectionByIdTabelaRegressiva - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaRegressivaValores_TabelaRegressivaVigencia_FK
		/// </summary>

		[XmlIgnore]
		public TabelaRegressivaValoresCollection TabelaRegressivaValoresCollectionByIdTabelaRegressiva
		{
			get
			{
				if(this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva == null)
				{
					this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva = new TabelaRegressivaValoresCollection();
					this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRegressivaValoresCollectionByIdTabelaRegressiva", this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva);
				
					if(this.IdTabelaRegressiva != null)
					{
						this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva.Query.Where(this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva.Query.IdTabelaRegressiva == this.IdTabelaRegressiva);
						this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva.fks.Add(TabelaRegressivaValoresMetadata.ColumnNames.IdTabelaRegressiva, this.IdTabelaRegressiva);
					}
				}

				return this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva != null) 
				{ 
					this.RemovePostSave("TabelaRegressivaValoresCollectionByIdTabelaRegressiva"); 
					this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva = null;
					
				} 
			} 			
		}

		private TabelaRegressivaValoresCollection _TabelaRegressivaValoresCollectionByIdTabelaRegressiva;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaRegressivaValoresCollectionByIdTabelaRegressiva", typeof(TabelaRegressivaValoresCollection), new TabelaRegressivaValores()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva != null)
			{
				foreach(TabelaRegressivaValores obj in this._TabelaRegressivaValoresCollectionByIdTabelaRegressiva)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabelaRegressiva = this.IdTabelaRegressiva;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRegressivaVigenciaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRegressivaVigenciaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabelaRegressiva
		{
			get
			{
				return new esQueryItem(this, TabelaRegressivaVigenciaMetadata.ColumnNames.IdTabelaRegressiva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaRegressivaVigenciaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRegressivaVigenciaCollection")]
	public partial class TabelaRegressivaVigenciaCollection : esTabelaRegressivaVigenciaCollection, IEnumerable<TabelaRegressivaVigencia>
	{
		public TabelaRegressivaVigenciaCollection()
		{

		}
		
		public static implicit operator List<TabelaRegressivaVigencia>(TabelaRegressivaVigenciaCollection coll)
		{
			List<TabelaRegressivaVigencia> list = new List<TabelaRegressivaVigencia>();
			
			foreach (TabelaRegressivaVigencia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRegressivaVigenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRegressivaVigenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRegressivaVigencia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRegressivaVigencia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRegressivaVigenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRegressivaVigenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRegressivaVigenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRegressivaVigencia AddNew()
		{
			TabelaRegressivaVigencia entity = base.AddNewEntity() as TabelaRegressivaVigencia;
			
			return entity;
		}

		public TabelaRegressivaVigencia FindByPrimaryKey(System.Int32 idTabelaRegressiva)
		{
			return base.FindByPrimaryKey(idTabelaRegressiva) as TabelaRegressivaVigencia;
		}


		#region IEnumerable<TabelaRegressivaVigencia> Members

		IEnumerator<TabelaRegressivaVigencia> IEnumerable<TabelaRegressivaVigencia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRegressivaVigencia;
			}
		}

		#endregion
		
		private TabelaRegressivaVigenciaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRegressivaVigencia' table
	/// </summary>

	[Serializable]
	public partial class TabelaRegressivaVigencia : esTabelaRegressivaVigencia
	{
		public TabelaRegressivaVigencia()
		{

		}
	
		public TabelaRegressivaVigencia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRegressivaVigenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRegressivaVigenciaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRegressivaVigenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRegressivaVigenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRegressivaVigenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRegressivaVigenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRegressivaVigenciaQuery query;
	}



	[Serializable]
	public partial class TabelaRegressivaVigenciaQuery : esTabelaRegressivaVigenciaQuery
	{
		public TabelaRegressivaVigenciaQuery()
		{

		}		
		
		public TabelaRegressivaVigenciaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRegressivaVigenciaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRegressivaVigenciaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRegressivaVigenciaMetadata.ColumnNames.IdTabelaRegressiva, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaRegressivaVigenciaMetadata.PropertyNames.IdTabelaRegressiva;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRegressivaVigenciaMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRegressivaVigenciaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRegressivaVigenciaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabelaRegressiva = "IdTabelaRegressiva";
			 public const string Data = "Data";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabelaRegressiva = "IdTabelaRegressiva";
			 public const string Data = "Data";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRegressivaVigenciaMetadata))
			{
				if(TabelaRegressivaVigenciaMetadata.mapDelegates == null)
				{
					TabelaRegressivaVigenciaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRegressivaVigenciaMetadata.meta == null)
				{
					TabelaRegressivaVigenciaMetadata.meta = new TabelaRegressivaVigenciaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabelaRegressiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "TabelaRegressivaVigencia";
				meta.Destination = "TabelaRegressivaVigencia";
				
				meta.spInsert = "proc_TabelaRegressivaVigenciaInsert";				
				meta.spUpdate = "proc_TabelaRegressivaVigenciaUpdate";		
				meta.spDelete = "proc_TabelaRegressivaVigenciaDelete";
				meta.spLoadAll = "proc_TabelaRegressivaVigenciaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRegressivaVigenciaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRegressivaVigenciaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
