/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/10/2013 17:26:36
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				







		
using Financial.Investidor;





	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Tributo
{

	[Serializable]
	abstract public class esApuracaoRendaVariavelIRCollection : esEntityCollection
	{
		public esApuracaoRendaVariavelIRCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ApuracaoRendaVariavelIRCollection";
		}

		#region Query Logic
		protected void InitQuery(esApuracaoRendaVariavelIRQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esApuracaoRendaVariavelIRQuery);
		}
		#endregion
		
		virtual public ApuracaoRendaVariavelIR DetachEntity(ApuracaoRendaVariavelIR entity)
		{
			return base.DetachEntity(entity) as ApuracaoRendaVariavelIR;
		}
		
		virtual public ApuracaoRendaVariavelIR AttachEntity(ApuracaoRendaVariavelIR entity)
		{
			return base.AttachEntity(entity) as ApuracaoRendaVariavelIR;
		}
		
		virtual public void Combine(ApuracaoRendaVariavelIRCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ApuracaoRendaVariavelIR this[int index]
		{
			get
			{
				return base[index] as ApuracaoRendaVariavelIR;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ApuracaoRendaVariavelIR);
		}
	}



	[Serializable]
	abstract public class esApuracaoRendaVariavelIR : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esApuracaoRendaVariavelIRQuery GetDynamicQuery()
		{
			return null;
		}

		public esApuracaoRendaVariavelIR()
		{

		}

		public esApuracaoRendaVariavelIR(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, ano, mes);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, ano, mes);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esApuracaoRendaVariavelIRQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.Ano == ano, query.Mes == mes);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, ano, mes);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, ano, mes);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esApuracaoRendaVariavelIRQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.Ano == ano, query.Mes == mes);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("Ano",ano);			parms.Add("Mes",mes);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Ano": this.str.Ano = (string)value; break;							
						case "Mes": this.str.Mes = (string)value; break;							
						case "ResultadoAcoes": this.str.ResultadoAcoes = (string)value; break;							
						case "ResultadoOuro": this.str.ResultadoOuro = (string)value; break;							
						case "ResultadoOuroNaoBolsa": this.str.ResultadoOuroNaoBolsa = (string)value; break;							
						case "ResultadoOpcoes": this.str.ResultadoOpcoes = (string)value; break;							
						case "ResultadoOpcoesOuro": this.str.ResultadoOpcoesOuro = (string)value; break;							
						case "ResultadoOpcoesNaoBolsa": this.str.ResultadoOpcoesNaoBolsa = (string)value; break;							
						case "ResultadoOpcoesOutros": this.str.ResultadoOpcoesOutros = (string)value; break;							
						case "ResultadoFuturoDolar": this.str.ResultadoFuturoDolar = (string)value; break;							
						case "ResultadoFuturoIndice": this.str.ResultadoFuturoIndice = (string)value; break;							
						case "ResultadoFuturoJuros": this.str.ResultadoFuturoJuros = (string)value; break;							
						case "ResultadoFuturoOutros": this.str.ResultadoFuturoOutros = (string)value; break;							
						case "ResultadoTermo": this.str.ResultadoTermo = (string)value; break;							
						case "ResultadoTermoOutros": this.str.ResultadoTermoOutros = (string)value; break;							
						case "ResultadoAcoesDT": this.str.ResultadoAcoesDT = (string)value; break;							
						case "ResultadoOuroDT": this.str.ResultadoOuroDT = (string)value; break;							
						case "ResultadoOuroNaoBolsaDT": this.str.ResultadoOuroNaoBolsaDT = (string)value; break;							
						case "ResultadoOpcoesDT": this.str.ResultadoOpcoesDT = (string)value; break;							
						case "ResultadoOpcoesOuroDT": this.str.ResultadoOpcoesOuroDT = (string)value; break;							
						case "ResultadoOpcoesNaoBolsaDT": this.str.ResultadoOpcoesNaoBolsaDT = (string)value; break;							
						case "ResultadoOpcoesOutrosDT": this.str.ResultadoOpcoesOutrosDT = (string)value; break;							
						case "ResultadoFuturoDolarDT": this.str.ResultadoFuturoDolarDT = (string)value; break;							
						case "ResultadoFuturoIndiceDT": this.str.ResultadoFuturoIndiceDT = (string)value; break;							
						case "ResultadoFuturoJurosDT": this.str.ResultadoFuturoJurosDT = (string)value; break;							
						case "ResultadoFuturoOutrosDT": this.str.ResultadoFuturoOutrosDT = (string)value; break;							
						case "ResultadoTermoDT": this.str.ResultadoTermoDT = (string)value; break;							
						case "ResultadoTermoOutrosDT": this.str.ResultadoTermoOutrosDT = (string)value; break;							
						case "ResultadoLiquidoMes": this.str.ResultadoLiquidoMes = (string)value; break;							
						case "ResultadoNegativoAnterior": this.str.ResultadoNegativoAnterior = (string)value; break;							
						case "BaseCalculo": this.str.BaseCalculo = (string)value; break;							
						case "PrejuizoCompensar": this.str.PrejuizoCompensar = (string)value; break;							
						case "ImpostoCalculado": this.str.ImpostoCalculado = (string)value; break;							
						case "ResultadoLiquidoMesDT": this.str.ResultadoLiquidoMesDT = (string)value; break;							
						case "ResultadoNegativoAnteriorDT": this.str.ResultadoNegativoAnteriorDT = (string)value; break;							
						case "BaseCalculoDT": this.str.BaseCalculoDT = (string)value; break;							
						case "PrejuizoCompensarDT": this.str.PrejuizoCompensarDT = (string)value; break;							
						case "ImpostoCalculadoDT": this.str.ImpostoCalculadoDT = (string)value; break;							
						case "TotalImposto": this.str.TotalImposto = (string)value; break;							
						case "IRDayTradeMes": this.str.IRDayTradeMes = (string)value; break;							
						case "IRDayTradeMesAnterior": this.str.IRDayTradeMesAnterior = (string)value; break;							
						case "IRDayTradeCompensar": this.str.IRDayTradeCompensar = (string)value; break;							
						case "IRFonteNormal": this.str.IRFonteNormal = (string)value; break;							
						case "ImpostoPagar": this.str.ImpostoPagar = (string)value; break;							
						case "ImpostoPago": this.str.ImpostoPago = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Ano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Ano = (System.Int32?)value;
							break;
						
						case "Mes":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Mes = (System.Byte?)value;
							break;
						
						case "ResultadoAcoes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoAcoes = (System.Decimal?)value;
							break;
						
						case "ResultadoOuro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOuro = (System.Decimal?)value;
							break;
						
						case "ResultadoOuroNaoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOuroNaoBolsa = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoes = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesOuro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesOuro = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesNaoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesNaoBolsa = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesOutros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesOutros = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoDolar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoDolar = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoIndice":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoIndice = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoJuros = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoOutros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoOutros = (System.Decimal?)value;
							break;
						
						case "ResultadoTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermo = (System.Decimal?)value;
							break;
						
						case "ResultadoTermoOutros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermoOutros = (System.Decimal?)value;
							break;
						
						case "ResultadoAcoesDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoAcoesDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOuroDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOuroDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOuroNaoBolsaDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOuroNaoBolsaDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesOuroDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesOuroDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesNaoBolsaDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesNaoBolsaDT = (System.Decimal?)value;
							break;
						
						case "ResultadoOpcoesOutrosDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoOpcoesOutrosDT = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoDolarDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoDolarDT = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoIndiceDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoIndiceDT = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoJurosDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoJurosDT = (System.Decimal?)value;
							break;
						
						case "ResultadoFuturoOutrosDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoFuturoOutrosDT = (System.Decimal?)value;
							break;
						
						case "ResultadoTermoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermoDT = (System.Decimal?)value;
							break;
						
						case "ResultadoTermoOutrosDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermoOutrosDT = (System.Decimal?)value;
							break;
						
						case "ResultadoLiquidoMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoLiquidoMes = (System.Decimal?)value;
							break;
						
						case "ResultadoNegativoAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNegativoAnterior = (System.Decimal?)value;
							break;
						
						case "BaseCalculo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.BaseCalculo = (System.Decimal?)value;
							break;
						
						case "PrejuizoCompensar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoCompensar = (System.Decimal?)value;
							break;
						
						case "ImpostoCalculado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoCalculado = (System.Decimal?)value;
							break;
						
						case "ResultadoLiquidoMesDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoLiquidoMesDT = (System.Decimal?)value;
							break;
						
						case "ResultadoNegativoAnteriorDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNegativoAnteriorDT = (System.Decimal?)value;
							break;
						
						case "BaseCalculoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.BaseCalculoDT = (System.Decimal?)value;
							break;
						
						case "PrejuizoCompensarDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoCompensarDT = (System.Decimal?)value;
							break;
						
						case "ImpostoCalculadoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoCalculadoDT = (System.Decimal?)value;
							break;
						
						case "TotalImposto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalImposto = (System.Decimal?)value;
							break;
						
						case "IRDayTradeMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeMes = (System.Decimal?)value;
							break;
						
						case "IRDayTradeMesAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeMesAnterior = (System.Decimal?)value;
							break;
						
						case "IRDayTradeCompensar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeCompensar = (System.Decimal?)value;
							break;
						
						case "IRFonteNormal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRFonteNormal = (System.Decimal?)value;
							break;
						
						case "ImpostoPagar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoPagar = (System.Decimal?)value;
							break;
						
						case "ImpostoPago":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoPago = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ApuracaoRendaVariavelIRMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ApuracaoRendaVariavelIRMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.Ano
		/// </summary>
		virtual public System.Int32? Ano
		{
			get
			{
				return base.GetSystemInt32(ApuracaoRendaVariavelIRMetadata.ColumnNames.Ano);
			}
			
			set
			{
				base.SetSystemInt32(ApuracaoRendaVariavelIRMetadata.ColumnNames.Ano, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.Mes
		/// </summary>
		virtual public System.Byte? Mes
		{
			get
			{
				return base.GetSystemByte(ApuracaoRendaVariavelIRMetadata.ColumnNames.Mes);
			}
			
			set
			{
				base.SetSystemByte(ApuracaoRendaVariavelIRMetadata.ColumnNames.Mes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoAcoes
		/// </summary>
		virtual public System.Decimal? ResultadoAcoes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOuro
		/// </summary>
		virtual public System.Decimal? ResultadoOuro
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuro);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuro, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOuroNaoBolsa
		/// </summary>
		virtual public System.Decimal? ResultadoOuroNaoBolsa
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoes
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesOuro
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesOuro
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuro);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuro, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsa
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesNaoBolsa
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesOutros
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesOutros
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutros);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutros, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoDolar
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoDolar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoIndice
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoIndice
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndice);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoJuros
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoJuros
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJuros);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoOutros
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoOutros
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutros);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutros, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoTermo
		/// </summary>
		virtual public System.Decimal? ResultadoTermo
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermo);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoTermoOutros
		/// </summary>
		virtual public System.Decimal? ResultadoTermoOutros
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutros);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutros, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoAcoesDT
		/// </summary>
		virtual public System.Decimal? ResultadoAcoesDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoesDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoesDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOuroDT
		/// </summary>
		virtual public System.Decimal? ResultadoOuroDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOuroNaoBolsaDT
		/// </summary>
		virtual public System.Decimal? ResultadoOuroNaoBolsaDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsaDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsaDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesDT
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesOuroDT
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesOuroDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuroDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuroDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesNaoBolsaDT
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesNaoBolsaDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsaDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsaDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoOpcoesOutrosDT
		/// </summary>
		virtual public System.Decimal? ResultadoOpcoesOutrosDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutrosDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutrosDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoDolarDT
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoDolarDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolarDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolarDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoIndiceDT
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoIndiceDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndiceDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndiceDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoJurosDT
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoJurosDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJurosDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJurosDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoFuturoOutrosDT
		/// </summary>
		virtual public System.Decimal? ResultadoFuturoOutrosDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutrosDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutrosDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoTermoDT
		/// </summary>
		virtual public System.Decimal? ResultadoTermoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoTermoOutrosDT
		/// </summary>
		virtual public System.Decimal? ResultadoTermoOutrosDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutrosDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutrosDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoLiquidoMes
		/// </summary>
		virtual public System.Decimal? ResultadoLiquidoMes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoNegativoAnterior
		/// </summary>
		virtual public System.Decimal? ResultadoNegativoAnterior
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.BaseCalculo
		/// </summary>
		virtual public System.Decimal? BaseCalculo
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculo);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.PrejuizoCompensar
		/// </summary>
		virtual public System.Decimal? PrejuizoCompensar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ImpostoCalculado
		/// </summary>
		virtual public System.Decimal? ImpostoCalculado
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculado);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculado, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoLiquidoMesDT
		/// </summary>
		virtual public System.Decimal? ResultadoLiquidoMesDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMesDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMesDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ResultadoNegativoAnteriorDT
		/// </summary>
		virtual public System.Decimal? ResultadoNegativoAnteriorDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnteriorDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnteriorDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.BaseCalculoDT
		/// </summary>
		virtual public System.Decimal? BaseCalculoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.PrejuizoCompensarDT
		/// </summary>
		virtual public System.Decimal? PrejuizoCompensarDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensarDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensarDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ImpostoCalculadoDT
		/// </summary>
		virtual public System.Decimal? ImpostoCalculadoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculadoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculadoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.TotalImposto
		/// </summary>
		virtual public System.Decimal? TotalImposto
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.TotalImposto);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.TotalImposto, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.IRDayTradeMes
		/// </summary>
		virtual public System.Decimal? IRDayTradeMes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.IRDayTradeMesAnterior
		/// </summary>
		virtual public System.Decimal? IRDayTradeMesAnterior
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMesAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMesAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.IRDayTradeCompensar
		/// </summary>
		virtual public System.Decimal? IRDayTradeCompensar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeCompensar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeCompensar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.IRFonteNormal
		/// </summary>
		virtual public System.Decimal? IRFonteNormal
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRFonteNormal);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRFonteNormal, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ImpostoPagar
		/// </summary>
		virtual public System.Decimal? ImpostoPagar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPagar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPagar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoRendaVariavelIR.ImpostoPago
		/// </summary>
		virtual public System.Decimal? ImpostoPago
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPago);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPago, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esApuracaoRendaVariavelIR entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Ano
			{
				get
				{
					System.Int32? data = entity.Ano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ano = null;
					else entity.Ano = Convert.ToInt32(value);
				}
			}
				
			public System.String Mes
			{
				get
				{
					System.Byte? data = entity.Mes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mes = null;
					else entity.Mes = Convert.ToByte(value);
				}
			}
				
			public System.String ResultadoAcoes
			{
				get
				{
					System.Decimal? data = entity.ResultadoAcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoAcoes = null;
					else entity.ResultadoAcoes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOuro
			{
				get
				{
					System.Decimal? data = entity.ResultadoOuro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOuro = null;
					else entity.ResultadoOuro = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOuroNaoBolsa
			{
				get
				{
					System.Decimal? data = entity.ResultadoOuroNaoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOuroNaoBolsa = null;
					else entity.ResultadoOuroNaoBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoes
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoes = null;
					else entity.ResultadoOpcoes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesOuro
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesOuro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesOuro = null;
					else entity.ResultadoOpcoesOuro = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesNaoBolsa
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesNaoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesNaoBolsa = null;
					else entity.ResultadoOpcoesNaoBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesOutros
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesOutros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesOutros = null;
					else entity.ResultadoOpcoesOutros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoDolar
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoDolar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoDolar = null;
					else entity.ResultadoFuturoDolar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoIndice
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoIndice = null;
					else entity.ResultadoFuturoIndice = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoJuros
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoJuros = null;
					else entity.ResultadoFuturoJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoOutros
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoOutros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoOutros = null;
					else entity.ResultadoFuturoOutros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermo
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermo = null;
					else entity.ResultadoTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermoOutros
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermoOutros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermoOutros = null;
					else entity.ResultadoTermoOutros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoAcoesDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoAcoesDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoAcoesDT = null;
					else entity.ResultadoAcoesDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOuroDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOuroDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOuroDT = null;
					else entity.ResultadoOuroDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOuroNaoBolsaDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOuroNaoBolsaDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOuroNaoBolsaDT = null;
					else entity.ResultadoOuroNaoBolsaDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesDT = null;
					else entity.ResultadoOpcoesDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesOuroDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesOuroDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesOuroDT = null;
					else entity.ResultadoOpcoesOuroDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesNaoBolsaDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesNaoBolsaDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesNaoBolsaDT = null;
					else entity.ResultadoOpcoesNaoBolsaDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoOpcoesOutrosDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoOpcoesOutrosDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoOpcoesOutrosDT = null;
					else entity.ResultadoOpcoesOutrosDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoDolarDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoDolarDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoDolarDT = null;
					else entity.ResultadoFuturoDolarDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoIndiceDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoIndiceDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoIndiceDT = null;
					else entity.ResultadoFuturoIndiceDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoJurosDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoJurosDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoJurosDT = null;
					else entity.ResultadoFuturoJurosDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoFuturoOutrosDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoFuturoOutrosDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoFuturoOutrosDT = null;
					else entity.ResultadoFuturoOutrosDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermoDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermoDT = null;
					else entity.ResultadoTermoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermoOutrosDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermoOutrosDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermoOutrosDT = null;
					else entity.ResultadoTermoOutrosDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoLiquidoMes
			{
				get
				{
					System.Decimal? data = entity.ResultadoLiquidoMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoLiquidoMes = null;
					else entity.ResultadoLiquidoMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoNegativoAnterior
			{
				get
				{
					System.Decimal? data = entity.ResultadoNegativoAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNegativoAnterior = null;
					else entity.ResultadoNegativoAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseCalculo
			{
				get
				{
					System.Decimal? data = entity.BaseCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseCalculo = null;
					else entity.BaseCalculo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoCompensar
			{
				get
				{
					System.Decimal? data = entity.PrejuizoCompensar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoCompensar = null;
					else entity.PrejuizoCompensar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoCalculado
			{
				get
				{
					System.Decimal? data = entity.ImpostoCalculado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoCalculado = null;
					else entity.ImpostoCalculado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoLiquidoMesDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoLiquidoMesDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoLiquidoMesDT = null;
					else entity.ResultadoLiquidoMesDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoNegativoAnteriorDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoNegativoAnteriorDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNegativoAnteriorDT = null;
					else entity.ResultadoNegativoAnteriorDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseCalculoDT
			{
				get
				{
					System.Decimal? data = entity.BaseCalculoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseCalculoDT = null;
					else entity.BaseCalculoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoCompensarDT
			{
				get
				{
					System.Decimal? data = entity.PrejuizoCompensarDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoCompensarDT = null;
					else entity.PrejuizoCompensarDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoCalculadoDT
			{
				get
				{
					System.Decimal? data = entity.ImpostoCalculadoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoCalculadoDT = null;
					else entity.ImpostoCalculadoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalImposto
			{
				get
				{
					System.Decimal? data = entity.TotalImposto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalImposto = null;
					else entity.TotalImposto = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeMes
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeMes = null;
					else entity.IRDayTradeMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeMesAnterior
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeMesAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeMesAnterior = null;
					else entity.IRDayTradeMesAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeCompensar
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeCompensar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeCompensar = null;
					else entity.IRDayTradeCompensar = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRFonteNormal
			{
				get
				{
					System.Decimal? data = entity.IRFonteNormal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRFonteNormal = null;
					else entity.IRFonteNormal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoPagar
			{
				get
				{
					System.Decimal? data = entity.ImpostoPagar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoPagar = null;
					else entity.ImpostoPagar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoPago
			{
				get
				{
					System.Decimal? data = entity.ImpostoPago;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoPago = null;
					else entity.ImpostoPago = Convert.ToDecimal(value);
				}
			}
			

			private esApuracaoRendaVariavelIR entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esApuracaoRendaVariavelIRQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esApuracaoRendaVariavelIR can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ApuracaoRendaVariavelIR : esApuracaoRendaVariavelIR
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_ApuracaoRendaVariavelIR_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esApuracaoRendaVariavelIRQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ApuracaoRendaVariavelIRMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Ano
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.Ano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mes
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.Mes, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ResultadoAcoes
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOuro
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOuroNaoBolsa
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoes
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesOuro
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesNaoBolsa
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesOutros
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoDolar
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoIndice
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndice, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoJuros
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoOutros
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermo
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermoOutros
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoAcoesDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoesDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOuroDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOuroNaoBolsaDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsaDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesOuroDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuroDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesNaoBolsaDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsaDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoOpcoesOutrosDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutrosDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoDolarDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolarDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoIndiceDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndiceDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoJurosDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJurosDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoFuturoOutrosDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutrosDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermoOutrosDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutrosDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoLiquidoMes
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoNegativoAnterior
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseCalculo
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoCompensar
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoCalculado
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoLiquidoMesDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMesDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoNegativoAnteriorDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnteriorDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseCalculoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoCompensarDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensarDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoCalculadoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculadoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalImposto
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.TotalImposto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeMes
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeMesAnterior
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMesAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeCompensar
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeCompensar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRFonteNormal
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.IRFonteNormal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoPagar
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPagar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoPago
		{
			get
			{
				return new esQueryItem(this, ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPago, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ApuracaoRendaVariavelIRCollection")]
	public partial class ApuracaoRendaVariavelIRCollection : esApuracaoRendaVariavelIRCollection, IEnumerable<ApuracaoRendaVariavelIR>
	{
		public ApuracaoRendaVariavelIRCollection()
		{

		}
		
		public static implicit operator List<ApuracaoRendaVariavelIR>(ApuracaoRendaVariavelIRCollection coll)
		{
			List<ApuracaoRendaVariavelIR> list = new List<ApuracaoRendaVariavelIR>();
			
			foreach (ApuracaoRendaVariavelIR emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ApuracaoRendaVariavelIRMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ApuracaoRendaVariavelIRQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ApuracaoRendaVariavelIR(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ApuracaoRendaVariavelIR();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ApuracaoRendaVariavelIRQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ApuracaoRendaVariavelIRQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ApuracaoRendaVariavelIRQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ApuracaoRendaVariavelIR AddNew()
		{
			ApuracaoRendaVariavelIR entity = base.AddNewEntity() as ApuracaoRendaVariavelIR;
			
			return entity;
		}

		public ApuracaoRendaVariavelIR FindByPrimaryKey(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			return base.FindByPrimaryKey(idCliente, ano, mes) as ApuracaoRendaVariavelIR;
		}


		#region IEnumerable<ApuracaoRendaVariavelIR> Members

		IEnumerator<ApuracaoRendaVariavelIR> IEnumerable<ApuracaoRendaVariavelIR>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ApuracaoRendaVariavelIR;
			}
		}

		#endregion
		
		private ApuracaoRendaVariavelIRQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ApuracaoRendaVariavelIR' table
	/// </summary>

	[Serializable]
	public partial class ApuracaoRendaVariavelIR : esApuracaoRendaVariavelIR
	{
		public ApuracaoRendaVariavelIR()
		{

		}
	
		public ApuracaoRendaVariavelIR(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ApuracaoRendaVariavelIRMetadata.Meta();
			}
		}
		
		
		
		override protected esApuracaoRendaVariavelIRQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ApuracaoRendaVariavelIRQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ApuracaoRendaVariavelIRQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ApuracaoRendaVariavelIRQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ApuracaoRendaVariavelIRQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ApuracaoRendaVariavelIRQuery query;
	}



	[Serializable]
	public partial class ApuracaoRendaVariavelIRQuery : esApuracaoRendaVariavelIRQuery
	{
		public ApuracaoRendaVariavelIRQuery()
		{

		}		
		
		public ApuracaoRendaVariavelIRQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ApuracaoRendaVariavelIRMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ApuracaoRendaVariavelIRMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.Ano, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.Ano;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.Mes, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.Mes;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoes, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoAcoes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuro, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOuro;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOuroNaoBolsa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoes, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuro, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesOuro;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsa, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesNaoBolsa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutros, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesOutros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolar, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoDolar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndice, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoIndice;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJuros, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoJuros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutros, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoOutros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermo, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutros, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoTermoOutros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoAcoesDT, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoAcoesDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroDT, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOuroDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOuroNaoBolsaDT, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOuroNaoBolsaDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesDT, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOuroDT, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesOuroDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesNaoBolsaDT, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesNaoBolsaDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoOpcoesOutrosDT, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoOpcoesOutrosDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoDolarDT, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoDolarDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoIndiceDT, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoIndiceDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoJurosDT, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoJurosDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoFuturoOutrosDT, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoFuturoOutrosDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoDT, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoTermoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoTermoOutrosDT, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoTermoOutrosDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMes, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoLiquidoMes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnterior, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoNegativoAnterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculo, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.BaseCalculo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensar, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.PrejuizoCompensar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculado, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ImpostoCalculado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoLiquidoMesDT, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoLiquidoMesDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ResultadoNegativoAnteriorDT, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ResultadoNegativoAnteriorDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.BaseCalculoDT, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.BaseCalculoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.PrejuizoCompensarDT, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.PrejuizoCompensarDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoCalculadoDT, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ImpostoCalculadoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.TotalImposto, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.TotalImposto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMes, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.IRDayTradeMes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeMesAnterior, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.IRDayTradeMesAnterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRDayTradeCompensar, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.IRDayTradeCompensar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.IRFonteNormal, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.IRFonteNormal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPagar, 44, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ImpostoPagar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoRendaVariavelIRMetadata.ColumnNames.ImpostoPago, 45, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoRendaVariavelIRMetadata.PropertyNames.ImpostoPago;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ApuracaoRendaVariavelIRMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Ano = "Ano";
			 public const string Mes = "Mes";
			 public const string ResultadoAcoes = "ResultadoAcoes";
			 public const string ResultadoOuro = "ResultadoOuro";
			 public const string ResultadoOuroNaoBolsa = "ResultadoOuroNaoBolsa";
			 public const string ResultadoOpcoes = "ResultadoOpcoes";
			 public const string ResultadoOpcoesOuro = "ResultadoOpcoesOuro";
			 public const string ResultadoOpcoesNaoBolsa = "ResultadoOpcoesNaoBolsa";
			 public const string ResultadoOpcoesOutros = "ResultadoOpcoesOutros";
			 public const string ResultadoFuturoDolar = "ResultadoFuturoDolar";
			 public const string ResultadoFuturoIndice = "ResultadoFuturoIndice";
			 public const string ResultadoFuturoJuros = "ResultadoFuturoJuros";
			 public const string ResultadoFuturoOutros = "ResultadoFuturoOutros";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string ResultadoTermoOutros = "ResultadoTermoOutros";
			 public const string ResultadoAcoesDT = "ResultadoAcoesDT";
			 public const string ResultadoOuroDT = "ResultadoOuroDT";
			 public const string ResultadoOuroNaoBolsaDT = "ResultadoOuroNaoBolsaDT";
			 public const string ResultadoOpcoesDT = "ResultadoOpcoesDT";
			 public const string ResultadoOpcoesOuroDT = "ResultadoOpcoesOuroDT";
			 public const string ResultadoOpcoesNaoBolsaDT = "ResultadoOpcoesNaoBolsaDT";
			 public const string ResultadoOpcoesOutrosDT = "ResultadoOpcoesOutrosDT";
			 public const string ResultadoFuturoDolarDT = "ResultadoFuturoDolarDT";
			 public const string ResultadoFuturoIndiceDT = "ResultadoFuturoIndiceDT";
			 public const string ResultadoFuturoJurosDT = "ResultadoFuturoJurosDT";
			 public const string ResultadoFuturoOutrosDT = "ResultadoFuturoOutrosDT";
			 public const string ResultadoTermoDT = "ResultadoTermoDT";
			 public const string ResultadoTermoOutrosDT = "ResultadoTermoOutrosDT";
			 public const string ResultadoLiquidoMes = "ResultadoLiquidoMes";
			 public const string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
			 public const string BaseCalculo = "BaseCalculo";
			 public const string PrejuizoCompensar = "PrejuizoCompensar";
			 public const string ImpostoCalculado = "ImpostoCalculado";
			 public const string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
			 public const string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
			 public const string BaseCalculoDT = "BaseCalculoDT";
			 public const string PrejuizoCompensarDT = "PrejuizoCompensarDT";
			 public const string ImpostoCalculadoDT = "ImpostoCalculadoDT";
			 public const string TotalImposto = "TotalImposto";
			 public const string IRDayTradeMes = "IRDayTradeMes";
			 public const string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
			 public const string IRDayTradeCompensar = "IRDayTradeCompensar";
			 public const string IRFonteNormal = "IRFonteNormal";
			 public const string ImpostoPagar = "ImpostoPagar";
			 public const string ImpostoPago = "ImpostoPago";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Ano = "Ano";
			 public const string Mes = "Mes";
			 public const string ResultadoAcoes = "ResultadoAcoes";
			 public const string ResultadoOuro = "ResultadoOuro";
			 public const string ResultadoOuroNaoBolsa = "ResultadoOuroNaoBolsa";
			 public const string ResultadoOpcoes = "ResultadoOpcoes";
			 public const string ResultadoOpcoesOuro = "ResultadoOpcoesOuro";
			 public const string ResultadoOpcoesNaoBolsa = "ResultadoOpcoesNaoBolsa";
			 public const string ResultadoOpcoesOutros = "ResultadoOpcoesOutros";
			 public const string ResultadoFuturoDolar = "ResultadoFuturoDolar";
			 public const string ResultadoFuturoIndice = "ResultadoFuturoIndice";
			 public const string ResultadoFuturoJuros = "ResultadoFuturoJuros";
			 public const string ResultadoFuturoOutros = "ResultadoFuturoOutros";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string ResultadoTermoOutros = "ResultadoTermoOutros";
			 public const string ResultadoAcoesDT = "ResultadoAcoesDT";
			 public const string ResultadoOuroDT = "ResultadoOuroDT";
			 public const string ResultadoOuroNaoBolsaDT = "ResultadoOuroNaoBolsaDT";
			 public const string ResultadoOpcoesDT = "ResultadoOpcoesDT";
			 public const string ResultadoOpcoesOuroDT = "ResultadoOpcoesOuroDT";
			 public const string ResultadoOpcoesNaoBolsaDT = "ResultadoOpcoesNaoBolsaDT";
			 public const string ResultadoOpcoesOutrosDT = "ResultadoOpcoesOutrosDT";
			 public const string ResultadoFuturoDolarDT = "ResultadoFuturoDolarDT";
			 public const string ResultadoFuturoIndiceDT = "ResultadoFuturoIndiceDT";
			 public const string ResultadoFuturoJurosDT = "ResultadoFuturoJurosDT";
			 public const string ResultadoFuturoOutrosDT = "ResultadoFuturoOutrosDT";
			 public const string ResultadoTermoDT = "ResultadoTermoDT";
			 public const string ResultadoTermoOutrosDT = "ResultadoTermoOutrosDT";
			 public const string ResultadoLiquidoMes = "ResultadoLiquidoMes";
			 public const string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
			 public const string BaseCalculo = "BaseCalculo";
			 public const string PrejuizoCompensar = "PrejuizoCompensar";
			 public const string ImpostoCalculado = "ImpostoCalculado";
			 public const string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
			 public const string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
			 public const string BaseCalculoDT = "BaseCalculoDT";
			 public const string PrejuizoCompensarDT = "PrejuizoCompensarDT";
			 public const string ImpostoCalculadoDT = "ImpostoCalculadoDT";
			 public const string TotalImposto = "TotalImposto";
			 public const string IRDayTradeMes = "IRDayTradeMes";
			 public const string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
			 public const string IRDayTradeCompensar = "IRDayTradeCompensar";
			 public const string IRFonteNormal = "IRFonteNormal";
			 public const string ImpostoPagar = "ImpostoPagar";
			 public const string ImpostoPago = "ImpostoPago";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ApuracaoRendaVariavelIRMetadata))
			{
				if(ApuracaoRendaVariavelIRMetadata.mapDelegates == null)
				{
					ApuracaoRendaVariavelIRMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ApuracaoRendaVariavelIRMetadata.meta == null)
				{
					ApuracaoRendaVariavelIRMetadata.meta = new ApuracaoRendaVariavelIRMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Ano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mes", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ResultadoAcoes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOuro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOuroNaoBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesOuro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesNaoBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesOutros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoDolar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoIndice", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoOutros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermoOutros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoAcoesDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOuroDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOuroNaoBolsaDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesOuroDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesNaoBolsaDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoOpcoesOutrosDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoDolarDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoIndiceDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoJurosDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoFuturoOutrosDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermoOutrosDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoLiquidoMes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoNegativoAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseCalculo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoCompensar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoCalculado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoLiquidoMesDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoNegativoAnteriorDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseCalculoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoCompensarDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoCalculadoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalImposto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeMes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeMesAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeCompensar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRFonteNormal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoPagar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoPago", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ApuracaoRendaVariavelIR";
				meta.Destination = "ApuracaoRendaVariavelIR";
				
				meta.spInsert = "proc_ApuracaoRendaVariavelIRInsert";				
				meta.spUpdate = "proc_ApuracaoRendaVariavelIRUpdate";		
				meta.spDelete = "proc_ApuracaoRendaVariavelIRDelete";
				meta.spLoadAll = "proc_ApuracaoRendaVariavelIRLoadAll";
				meta.spLoadByPrimaryKey = "proc_ApuracaoRendaVariavelIRLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ApuracaoRendaVariavelIRMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
