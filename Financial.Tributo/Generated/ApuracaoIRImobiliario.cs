/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/10/2013 17:26:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Tributo
{

	[Serializable]
	abstract public class esApuracaoIRImobiliarioCollection : esEntityCollection
	{
		public esApuracaoIRImobiliarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ApuracaoIRImobiliarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esApuracaoIRImobiliarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esApuracaoIRImobiliarioQuery);
		}
		#endregion
		
		virtual public ApuracaoIRImobiliario DetachEntity(ApuracaoIRImobiliario entity)
		{
			return base.DetachEntity(entity) as ApuracaoIRImobiliario;
		}
		
		virtual public ApuracaoIRImobiliario AttachEntity(ApuracaoIRImobiliario entity)
		{
			return base.AttachEntity(entity) as ApuracaoIRImobiliario;
		}
		
		virtual public void Combine(ApuracaoIRImobiliarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ApuracaoIRImobiliario this[int index]
		{
			get
			{
				return base[index] as ApuracaoIRImobiliario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ApuracaoIRImobiliario);
		}
	}



	[Serializable]
	abstract public class esApuracaoIRImobiliario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esApuracaoIRImobiliarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esApuracaoIRImobiliario()
		{

		}

		public esApuracaoIRImobiliario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, ano, mes);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, ano, mes);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esApuracaoIRImobiliarioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.Ano == ano, query.Mes == mes);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, ano, mes);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, ano, mes);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esApuracaoIRImobiliarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.Ano == ano, query.Mes == mes);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("Ano",ano);			parms.Add("Mes",mes);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Ano": this.str.Ano = (string)value; break;							
						case "Mes": this.str.Mes = (string)value; break;							
						case "ResultadoNormal": this.str.ResultadoNormal = (string)value; break;							
						case "ResultadoDT": this.str.ResultadoDT = (string)value; break;							
						case "ResultadoLiquidoMes": this.str.ResultadoLiquidoMes = (string)value; break;							
						case "ResultadoNegativoAnterior": this.str.ResultadoNegativoAnterior = (string)value; break;							
						case "BaseCalculo": this.str.BaseCalculo = (string)value; break;							
						case "PrejuizoCompensar": this.str.PrejuizoCompensar = (string)value; break;							
						case "ImpostoCalculado": this.str.ImpostoCalculado = (string)value; break;							
						case "ResultadoLiquidoMesDT": this.str.ResultadoLiquidoMesDT = (string)value; break;							
						case "ResultadoNegativoAnteriorDT": this.str.ResultadoNegativoAnteriorDT = (string)value; break;							
						case "BaseCalculoDT": this.str.BaseCalculoDT = (string)value; break;							
						case "PrejuizoCompensarDT": this.str.PrejuizoCompensarDT = (string)value; break;							
						case "ImpostoCalculadoDT": this.str.ImpostoCalculadoDT = (string)value; break;							
						case "TotalImposto": this.str.TotalImposto = (string)value; break;							
						case "IRDayTradeMes": this.str.IRDayTradeMes = (string)value; break;							
						case "IRDayTradeMesAnterior": this.str.IRDayTradeMesAnterior = (string)value; break;							
						case "IRDayTradeCompensar": this.str.IRDayTradeCompensar = (string)value; break;							
						case "IRFonteNormal": this.str.IRFonteNormal = (string)value; break;							
						case "ImpostoPagar": this.str.ImpostoPagar = (string)value; break;							
						case "ImpostoPago": this.str.ImpostoPago = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Ano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Ano = (System.Int32?)value;
							break;
						
						case "Mes":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Mes = (System.Byte?)value;
							break;
						
						case "ResultadoNormal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNormal = (System.Decimal?)value;
							break;
						
						case "ResultadoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoDT = (System.Decimal?)value;
							break;
						
						case "ResultadoLiquidoMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoLiquidoMes = (System.Decimal?)value;
							break;
						
						case "ResultadoNegativoAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNegativoAnterior = (System.Decimal?)value;
							break;
						
						case "BaseCalculo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.BaseCalculo = (System.Decimal?)value;
							break;
						
						case "PrejuizoCompensar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoCompensar = (System.Decimal?)value;
							break;
						
						case "ImpostoCalculado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoCalculado = (System.Decimal?)value;
							break;
						
						case "ResultadoLiquidoMesDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoLiquidoMesDT = (System.Decimal?)value;
							break;
						
						case "ResultadoNegativoAnteriorDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoNegativoAnteriorDT = (System.Decimal?)value;
							break;
						
						case "BaseCalculoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.BaseCalculoDT = (System.Decimal?)value;
							break;
						
						case "PrejuizoCompensarDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoCompensarDT = (System.Decimal?)value;
							break;
						
						case "ImpostoCalculadoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoCalculadoDT = (System.Decimal?)value;
							break;
						
						case "TotalImposto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalImposto = (System.Decimal?)value;
							break;
						
						case "IRDayTradeMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeMes = (System.Decimal?)value;
							break;
						
						case "IRDayTradeMesAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeMesAnterior = (System.Decimal?)value;
							break;
						
						case "IRDayTradeCompensar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRDayTradeCompensar = (System.Decimal?)value;
							break;
						
						case "IRFonteNormal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.IRFonteNormal = (System.Decimal?)value;
							break;
						
						case "ImpostoPagar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoPagar = (System.Decimal?)value;
							break;
						
						case "ImpostoPago":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ImpostoPago = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ApuracaoIRImobiliarioMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ApuracaoIRImobiliarioMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.Ano
		/// </summary>
		virtual public System.Int32? Ano
		{
			get
			{
				return base.GetSystemInt32(ApuracaoIRImobiliarioMetadata.ColumnNames.Ano);
			}
			
			set
			{
				base.SetSystemInt32(ApuracaoIRImobiliarioMetadata.ColumnNames.Ano, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.Mes
		/// </summary>
		virtual public System.Byte? Mes
		{
			get
			{
				return base.GetSystemByte(ApuracaoIRImobiliarioMetadata.ColumnNames.Mes);
			}
			
			set
			{
				base.SetSystemByte(ApuracaoIRImobiliarioMetadata.ColumnNames.Mes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoNormal
		/// </summary>
		virtual public System.Decimal? ResultadoNormal
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNormal);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNormal, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoDT
		/// </summary>
		virtual public System.Decimal? ResultadoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoLiquidoMes
		/// </summary>
		virtual public System.Decimal? ResultadoLiquidoMes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoNegativoAnterior
		/// </summary>
		virtual public System.Decimal? ResultadoNegativoAnterior
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.BaseCalculo
		/// </summary>
		virtual public System.Decimal? BaseCalculo
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculo);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.PrejuizoCompensar
		/// </summary>
		virtual public System.Decimal? PrejuizoCompensar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ImpostoCalculado
		/// </summary>
		virtual public System.Decimal? ImpostoCalculado
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculado);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculado, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoLiquidoMesDT
		/// </summary>
		virtual public System.Decimal? ResultadoLiquidoMesDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMesDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMesDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ResultadoNegativoAnteriorDT
		/// </summary>
		virtual public System.Decimal? ResultadoNegativoAnteriorDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnteriorDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnteriorDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.BaseCalculoDT
		/// </summary>
		virtual public System.Decimal? BaseCalculoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.PrejuizoCompensarDT
		/// </summary>
		virtual public System.Decimal? PrejuizoCompensarDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensarDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensarDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ImpostoCalculadoDT
		/// </summary>
		virtual public System.Decimal? ImpostoCalculadoDT
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculadoDT);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculadoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.TotalImposto
		/// </summary>
		virtual public System.Decimal? TotalImposto
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.TotalImposto);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.TotalImposto, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.IRDayTradeMes
		/// </summary>
		virtual public System.Decimal? IRDayTradeMes
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMes);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMes, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.IRDayTradeMesAnterior
		/// </summary>
		virtual public System.Decimal? IRDayTradeMesAnterior
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMesAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMesAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.IRDayTradeCompensar
		/// </summary>
		virtual public System.Decimal? IRDayTradeCompensar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeCompensar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeCompensar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.IRFonteNormal
		/// </summary>
		virtual public System.Decimal? IRFonteNormal
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRFonteNormal);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.IRFonteNormal, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ImpostoPagar
		/// </summary>
		virtual public System.Decimal? ImpostoPagar
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPagar);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPagar, value);
			}
		}
		
		/// <summary>
		/// Maps to ApuracaoIRImobiliario.ImpostoPago
		/// </summary>
		virtual public System.Decimal? ImpostoPago
		{
			get
			{
				return base.GetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPago);
			}
			
			set
			{
				base.SetSystemDecimal(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPago, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esApuracaoIRImobiliario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Ano
			{
				get
				{
					System.Int32? data = entity.Ano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ano = null;
					else entity.Ano = Convert.ToInt32(value);
				}
			}
				
			public System.String Mes
			{
				get
				{
					System.Byte? data = entity.Mes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mes = null;
					else entity.Mes = Convert.ToByte(value);
				}
			}
				
			public System.String ResultadoNormal
			{
				get
				{
					System.Decimal? data = entity.ResultadoNormal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNormal = null;
					else entity.ResultadoNormal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoDT = null;
					else entity.ResultadoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoLiquidoMes
			{
				get
				{
					System.Decimal? data = entity.ResultadoLiquidoMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoLiquidoMes = null;
					else entity.ResultadoLiquidoMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoNegativoAnterior
			{
				get
				{
					System.Decimal? data = entity.ResultadoNegativoAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNegativoAnterior = null;
					else entity.ResultadoNegativoAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseCalculo
			{
				get
				{
					System.Decimal? data = entity.BaseCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseCalculo = null;
					else entity.BaseCalculo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoCompensar
			{
				get
				{
					System.Decimal? data = entity.PrejuizoCompensar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoCompensar = null;
					else entity.PrejuizoCompensar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoCalculado
			{
				get
				{
					System.Decimal? data = entity.ImpostoCalculado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoCalculado = null;
					else entity.ImpostoCalculado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoLiquidoMesDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoLiquidoMesDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoLiquidoMesDT = null;
					else entity.ResultadoLiquidoMesDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoNegativoAnteriorDT
			{
				get
				{
					System.Decimal? data = entity.ResultadoNegativoAnteriorDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoNegativoAnteriorDT = null;
					else entity.ResultadoNegativoAnteriorDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseCalculoDT
			{
				get
				{
					System.Decimal? data = entity.BaseCalculoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseCalculoDT = null;
					else entity.BaseCalculoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoCompensarDT
			{
				get
				{
					System.Decimal? data = entity.PrejuizoCompensarDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoCompensarDT = null;
					else entity.PrejuizoCompensarDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoCalculadoDT
			{
				get
				{
					System.Decimal? data = entity.ImpostoCalculadoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoCalculadoDT = null;
					else entity.ImpostoCalculadoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalImposto
			{
				get
				{
					System.Decimal? data = entity.TotalImposto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalImposto = null;
					else entity.TotalImposto = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeMes
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeMes = null;
					else entity.IRDayTradeMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeMesAnterior
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeMesAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeMesAnterior = null;
					else entity.IRDayTradeMesAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRDayTradeCompensar
			{
				get
				{
					System.Decimal? data = entity.IRDayTradeCompensar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRDayTradeCompensar = null;
					else entity.IRDayTradeCompensar = Convert.ToDecimal(value);
				}
			}
				
			public System.String IRFonteNormal
			{
				get
				{
					System.Decimal? data = entity.IRFonteNormal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IRFonteNormal = null;
					else entity.IRFonteNormal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoPagar
			{
				get
				{
					System.Decimal? data = entity.ImpostoPagar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoPagar = null;
					else entity.ImpostoPagar = Convert.ToDecimal(value);
				}
			}
				
			public System.String ImpostoPago
			{
				get
				{
					System.Decimal? data = entity.ImpostoPago;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpostoPago = null;
					else entity.ImpostoPago = Convert.ToDecimal(value);
				}
			}
			

			private esApuracaoIRImobiliario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esApuracaoIRImobiliarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esApuracaoIRImobiliario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ApuracaoIRImobiliario : esApuracaoIRImobiliario
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_ApuracaoIRImobiliario_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esApuracaoIRImobiliarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ApuracaoIRImobiliarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Ano
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.Ano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mes
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.Mes, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ResultadoNormal
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNormal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoLiquidoMes
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoNegativoAnterior
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseCalculo
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoCompensar
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoCalculado
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoLiquidoMesDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMesDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoNegativoAnteriorDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnteriorDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseCalculoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoCompensarDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensarDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoCalculadoDT
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculadoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalImposto
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.TotalImposto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeMes
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeMesAnterior
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMesAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRDayTradeCompensar
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeCompensar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IRFonteNormal
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.IRFonteNormal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoPagar
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPagar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ImpostoPago
		{
			get
			{
				return new esQueryItem(this, ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPago, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ApuracaoIRImobiliarioCollection")]
	public partial class ApuracaoIRImobiliarioCollection : esApuracaoIRImobiliarioCollection, IEnumerable<ApuracaoIRImobiliario>
	{
		public ApuracaoIRImobiliarioCollection()
		{

		}
		
		public static implicit operator List<ApuracaoIRImobiliario>(ApuracaoIRImobiliarioCollection coll)
		{
			List<ApuracaoIRImobiliario> list = new List<ApuracaoIRImobiliario>();
			
			foreach (ApuracaoIRImobiliario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ApuracaoIRImobiliarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ApuracaoIRImobiliarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ApuracaoIRImobiliario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ApuracaoIRImobiliario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ApuracaoIRImobiliarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ApuracaoIRImobiliarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ApuracaoIRImobiliarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ApuracaoIRImobiliario AddNew()
		{
			ApuracaoIRImobiliario entity = base.AddNewEntity() as ApuracaoIRImobiliario;
			
			return entity;
		}

		public ApuracaoIRImobiliario FindByPrimaryKey(System.Int32 idCliente, System.Int32 ano, System.Byte mes)
		{
			return base.FindByPrimaryKey(idCliente, ano, mes) as ApuracaoIRImobiliario;
		}


		#region IEnumerable<ApuracaoIRImobiliario> Members

		IEnumerator<ApuracaoIRImobiliario> IEnumerable<ApuracaoIRImobiliario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ApuracaoIRImobiliario;
			}
		}

		#endregion
		
		private ApuracaoIRImobiliarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ApuracaoIRImobiliario' table
	/// </summary>

	[Serializable]
	public partial class ApuracaoIRImobiliario : esApuracaoIRImobiliario
	{
		public ApuracaoIRImobiliario()
		{

		}
	
		public ApuracaoIRImobiliario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ApuracaoIRImobiliarioMetadata.Meta();
			}
		}
		
		
		
		override protected esApuracaoIRImobiliarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ApuracaoIRImobiliarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ApuracaoIRImobiliarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ApuracaoIRImobiliarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ApuracaoIRImobiliarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ApuracaoIRImobiliarioQuery query;
	}



	[Serializable]
	public partial class ApuracaoIRImobiliarioQuery : esApuracaoIRImobiliarioQuery
	{
		public ApuracaoIRImobiliarioQuery()
		{

		}		
		
		public ApuracaoIRImobiliarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ApuracaoIRImobiliarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ApuracaoIRImobiliarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.Ano, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.Ano;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.Mes, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.Mes;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNormal, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoNormal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoDT, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMes, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoLiquidoMes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnterior, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoNegativoAnterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculo, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.BaseCalculo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensar, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.PrejuizoCompensar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ImpostoCalculado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoLiquidoMesDT, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoLiquidoMesDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ResultadoNegativoAnteriorDT, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ResultadoNegativoAnteriorDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.BaseCalculoDT, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.BaseCalculoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.PrejuizoCompensarDT, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.PrejuizoCompensarDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoCalculadoDT, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ImpostoCalculadoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.TotalImposto, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.TotalImposto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMes, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.IRDayTradeMes;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeMesAnterior, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.IRDayTradeMesAnterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.IRDayTradeCompensar, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.IRDayTradeCompensar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.IRFonteNormal, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.IRFonteNormal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPagar, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ImpostoPagar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ApuracaoIRImobiliarioMetadata.ColumnNames.ImpostoPago, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ApuracaoIRImobiliarioMetadata.PropertyNames.ImpostoPago;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ApuracaoIRImobiliarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Ano = "Ano";
			 public const string Mes = "Mes";
			 public const string ResultadoNormal = "ResultadoNormal";
			 public const string ResultadoDT = "ResultadoDT";
			 public const string ResultadoLiquidoMes = "ResultadoLiquidoMes";
			 public const string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
			 public const string BaseCalculo = "BaseCalculo";
			 public const string PrejuizoCompensar = "PrejuizoCompensar";
			 public const string ImpostoCalculado = "ImpostoCalculado";
			 public const string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
			 public const string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
			 public const string BaseCalculoDT = "BaseCalculoDT";
			 public const string PrejuizoCompensarDT = "PrejuizoCompensarDT";
			 public const string ImpostoCalculadoDT = "ImpostoCalculadoDT";
			 public const string TotalImposto = "TotalImposto";
			 public const string IRDayTradeMes = "IRDayTradeMes";
			 public const string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
			 public const string IRDayTradeCompensar = "IRDayTradeCompensar";
			 public const string IRFonteNormal = "IRFonteNormal";
			 public const string ImpostoPagar = "ImpostoPagar";
			 public const string ImpostoPago = "ImpostoPago";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Ano = "Ano";
			 public const string Mes = "Mes";
			 public const string ResultadoNormal = "ResultadoNormal";
			 public const string ResultadoDT = "ResultadoDT";
			 public const string ResultadoLiquidoMes = "ResultadoLiquidoMes";
			 public const string ResultadoNegativoAnterior = "ResultadoNegativoAnterior";
			 public const string BaseCalculo = "BaseCalculo";
			 public const string PrejuizoCompensar = "PrejuizoCompensar";
			 public const string ImpostoCalculado = "ImpostoCalculado";
			 public const string ResultadoLiquidoMesDT = "ResultadoLiquidoMesDT";
			 public const string ResultadoNegativoAnteriorDT = "ResultadoNegativoAnteriorDT";
			 public const string BaseCalculoDT = "BaseCalculoDT";
			 public const string PrejuizoCompensarDT = "PrejuizoCompensarDT";
			 public const string ImpostoCalculadoDT = "ImpostoCalculadoDT";
			 public const string TotalImposto = "TotalImposto";
			 public const string IRDayTradeMes = "IRDayTradeMes";
			 public const string IRDayTradeMesAnterior = "IRDayTradeMesAnterior";
			 public const string IRDayTradeCompensar = "IRDayTradeCompensar";
			 public const string IRFonteNormal = "IRFonteNormal";
			 public const string ImpostoPagar = "ImpostoPagar";
			 public const string ImpostoPago = "ImpostoPago";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ApuracaoIRImobiliarioMetadata))
			{
				if(ApuracaoIRImobiliarioMetadata.mapDelegates == null)
				{
					ApuracaoIRImobiliarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ApuracaoIRImobiliarioMetadata.meta == null)
				{
					ApuracaoIRImobiliarioMetadata.meta = new ApuracaoIRImobiliarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Ano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mes", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ResultadoNormal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoLiquidoMes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoNegativoAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseCalculo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoCompensar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoCalculado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoLiquidoMesDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoNegativoAnteriorDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseCalculoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoCompensarDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoCalculadoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalImposto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeMes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeMesAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRDayTradeCompensar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IRFonteNormal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoPagar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ImpostoPago", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ApuracaoIRImobiliario";
				meta.Destination = "ApuracaoIRImobiliario";
				
				meta.spInsert = "proc_ApuracaoIRImobiliarioInsert";				
				meta.spUpdate = "proc_ApuracaoIRImobiliarioUpdate";		
				meta.spDelete = "proc_ApuracaoIRImobiliarioDelete";
				meta.spLoadAll = "proc_ApuracaoIRImobiliarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_ApuracaoIRImobiliarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ApuracaoIRImobiliarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
