/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2015 12:17:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Tributo
{

	[Serializable]
	abstract public class esTabelaProgressivaVigenciaCollection : esEntityCollection
	{
		public esTabelaProgressivaVigenciaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaProgressivaVigenciaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaProgressivaVigenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaProgressivaVigenciaQuery);
		}
		#endregion
		
		virtual public TabelaProgressivaVigencia DetachEntity(TabelaProgressivaVigencia entity)
		{
			return base.DetachEntity(entity) as TabelaProgressivaVigencia;
		}
		
		virtual public TabelaProgressivaVigencia AttachEntity(TabelaProgressivaVigencia entity)
		{
			return base.AttachEntity(entity) as TabelaProgressivaVigencia;
		}
		
		virtual public void Combine(TabelaProgressivaVigenciaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaProgressivaVigencia this[int index]
		{
			get
			{
				return base[index] as TabelaProgressivaVigencia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaProgressivaVigencia);
		}
	}



	[Serializable]
	abstract public class esTabelaProgressivaVigencia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaProgressivaVigenciaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaProgressivaVigencia()
		{

		}

		public esTabelaProgressivaVigencia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabelaProgressiva)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaProgressiva);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaProgressiva);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabelaProgressiva)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaProgressiva);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaProgressiva);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabelaProgressiva)
		{
			esTabelaProgressivaVigenciaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabelaProgressiva == idTabelaProgressiva);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabelaProgressiva)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabelaProgressiva",idTabelaProgressiva);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabelaProgressiva": this.str.IdTabelaProgressiva = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabelaProgressiva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaProgressiva = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaProgressivaVigencia.IdTabelaProgressiva
		/// </summary>
		virtual public System.Int32? IdTabelaProgressiva
		{
			get
			{
				return base.GetSystemInt32(TabelaProgressivaVigenciaMetadata.ColumnNames.IdTabelaProgressiva);
			}
			
			set
			{
				base.SetSystemInt32(TabelaProgressivaVigenciaMetadata.ColumnNames.IdTabelaProgressiva, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProgressivaVigencia.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaProgressivaVigenciaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaProgressivaVigenciaMetadata.ColumnNames.Data, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaProgressivaVigencia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabelaProgressiva
			{
				get
				{
					System.Int32? data = entity.IdTabelaProgressiva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaProgressiva = null;
					else entity.IdTabelaProgressiva = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
			

			private esTabelaProgressivaVigencia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaProgressivaVigenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaProgressivaVigencia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaProgressivaVigencia : esTabelaProgressivaVigencia
	{

				
		#region TabelaProgressivaValoresCollectionByIdTabelaProgressiva - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaProgressivaValores_TabelaProgressivaVigencia_FK
		/// </summary>

		[XmlIgnore]
		public TabelaProgressivaValoresCollection TabelaProgressivaValoresCollectionByIdTabelaProgressiva
		{
			get
			{
				if(this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva == null)
				{
					this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva = new TabelaProgressivaValoresCollection();
					this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProgressivaValoresCollectionByIdTabelaProgressiva", this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva);
				
					if(this.IdTabelaProgressiva != null)
					{
						this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva.Query.Where(this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva.Query.IdTabelaProgressiva == this.IdTabelaProgressiva);
						this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva.fks.Add(TabelaProgressivaValoresMetadata.ColumnNames.IdTabelaProgressiva, this.IdTabelaProgressiva);
					}
				}

				return this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva != null) 
				{ 
					this.RemovePostSave("TabelaProgressivaValoresCollectionByIdTabelaProgressiva"); 
					this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva = null;
					
				} 
			} 			
		}

		private TabelaProgressivaValoresCollection _TabelaProgressivaValoresCollectionByIdTabelaProgressiva;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaProgressivaValoresCollectionByIdTabelaProgressiva", typeof(TabelaProgressivaValoresCollection), new TabelaProgressivaValores()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva != null)
			{
				foreach(TabelaProgressivaValores obj in this._TabelaProgressivaValoresCollectionByIdTabelaProgressiva)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabelaProgressiva = this.IdTabelaProgressiva;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaProgressivaVigenciaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProgressivaVigenciaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabelaProgressiva
		{
			get
			{
				return new esQueryItem(this, TabelaProgressivaVigenciaMetadata.ColumnNames.IdTabelaProgressiva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaProgressivaVigenciaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaProgressivaVigenciaCollection")]
	public partial class TabelaProgressivaVigenciaCollection : esTabelaProgressivaVigenciaCollection, IEnumerable<TabelaProgressivaVigencia>
	{
		public TabelaProgressivaVigenciaCollection()
		{

		}
		
		public static implicit operator List<TabelaProgressivaVigencia>(TabelaProgressivaVigenciaCollection coll)
		{
			List<TabelaProgressivaVigencia> list = new List<TabelaProgressivaVigencia>();
			
			foreach (TabelaProgressivaVigencia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaProgressivaVigenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProgressivaVigenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaProgressivaVigencia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaProgressivaVigencia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaProgressivaVigenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProgressivaVigenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaProgressivaVigenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaProgressivaVigencia AddNew()
		{
			TabelaProgressivaVigencia entity = base.AddNewEntity() as TabelaProgressivaVigencia;
			
			return entity;
		}

		public TabelaProgressivaVigencia FindByPrimaryKey(System.Int32 idTabelaProgressiva)
		{
			return base.FindByPrimaryKey(idTabelaProgressiva) as TabelaProgressivaVigencia;
		}


		#region IEnumerable<TabelaProgressivaVigencia> Members

		IEnumerator<TabelaProgressivaVigencia> IEnumerable<TabelaProgressivaVigencia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaProgressivaVigencia;
			}
		}

		#endregion
		
		private TabelaProgressivaVigenciaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaProgressivaVigencia' table
	/// </summary>

	[Serializable]
	public partial class TabelaProgressivaVigencia : esTabelaProgressivaVigencia
	{
		public TabelaProgressivaVigencia()
		{

		}
	
		public TabelaProgressivaVigencia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProgressivaVigenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaProgressivaVigenciaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProgressivaVigenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaProgressivaVigenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProgressivaVigenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaProgressivaVigenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaProgressivaVigenciaQuery query;
	}



	[Serializable]
	public partial class TabelaProgressivaVigenciaQuery : esTabelaProgressivaVigenciaQuery
	{
		public TabelaProgressivaVigenciaQuery()
		{

		}		
		
		public TabelaProgressivaVigenciaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaProgressivaVigenciaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaProgressivaVigenciaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaProgressivaVigenciaMetadata.ColumnNames.IdTabelaProgressiva, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProgressivaVigenciaMetadata.PropertyNames.IdTabelaProgressiva;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProgressivaVigenciaMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaProgressivaVigenciaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaProgressivaVigenciaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabelaProgressiva = "IdTabelaProgressiva";
			 public const string Data = "Data";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabelaProgressiva = "IdTabelaProgressiva";
			 public const string Data = "Data";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaProgressivaVigenciaMetadata))
			{
				if(TabelaProgressivaVigenciaMetadata.mapDelegates == null)
				{
					TabelaProgressivaVigenciaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaProgressivaVigenciaMetadata.meta == null)
				{
					TabelaProgressivaVigenciaMetadata.meta = new TabelaProgressivaVigenciaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabelaProgressiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "TabelaProgressivaVigencia";
				meta.Destination = "TabelaProgressivaVigencia";
				
				meta.spInsert = "proc_TabelaProgressivaVigenciaInsert";				
				meta.spUpdate = "proc_TabelaProgressivaVigenciaUpdate";		
				meta.spDelete = "proc_TabelaProgressivaVigenciaDelete";
				meta.spLoadAll = "proc_TabelaProgressivaVigenciaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaProgressivaVigenciaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaProgressivaVigenciaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
