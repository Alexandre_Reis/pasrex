﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Tributo.Enums {
    
    public enum IdentificadorIR {
        IRFonteAcoes = 1, //Venda de Ações
        IRFonteOpcoes = 2, //Net de opções
        IRFonteFuturos = 3, //Baixa de futuros
        IRFonteDaytrade = 4, //Sobre resultado daytrade (1%)
        IRFonteFII = 5, //Venda de FII
        IRFonteDayTrade_FII = 6 //Sobre resultado daytrade de FII (1%)
    }

    public class TipoApuracaoGanhoRV
    {
        public const string ApuraComLiquidacao = "S";
        public const string ApuraSemLiquidacao = "A";
        public const string NaoApura = "N";        
    }

    public enum FieModalidade
    {
        PGBL = 1,
        VGBL = 2
    }

    public enum FieTabelaIR
    {
        Progressiva = 1,
        Regressiva = 2
    }

    public enum DataContagemIOF
    {
        DataSolicitacao = 1,
        DataSolicitacaoD1 = 2,
        DataConversao = 3,
        DataLiqFinanceira = 4
    }

    public enum ContagemIOFVirtual
    {
        //Contagem até último dia útil de maio/novembro
        SomaDiasConvResg = 0,
        //Contagem até último dia útil de maio/novembro
        ContagemUltDiaUtil = 1,
        //Contagem até último dia corrido de maio/novembro
        ContagemUltDiaCorrido = 2
    }
}
