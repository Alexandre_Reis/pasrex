﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Net;
using System.Configuration;

namespace Financial.WebServices.Provider
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class FileController : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string wsProviderURL = Financial.WebServices.Provider.Properties.Settings.Default.WebServicesProviderUrl;

            if (String.IsNullOrEmpty(wsProviderURL))
            {
                string url = HttpUtility.HtmlDecode(context.Request["url"]);
                string filename = HttpUtility.HtmlDecode(context.Request["filename"]);

                WebClient webClient = new WebClient();
                byte[] binaryData = webClient.DownloadData(url);

                context.Response.Buffer = true;
                context.Response.Charset = "";
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.ContentType = webClient.ResponseHeaders["content-type"];
                context.Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                context.Response.BinaryWrite(binaryData);
                context.Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                string url = context.Request["url"];
                string filename = context.Request["filename"];

                const string FILE_CONTROLLER_URL = "FileController.ashx";
                wsProviderURL = wsProviderURL + "/" + FILE_CONTROLLER_URL;

                string urlProxy = String.Format("{0}?url={1}&filename={2}", wsProviderURL, HttpUtility.HtmlEncode(url), HttpUtility.HtmlEncode(filename));

                WebClient webClient = new WebClient();
                byte[] binaryData = webClient.DownloadData(urlProxy);
                if (binaryData.Length == 0)
                {
                    throw new Exception("Não foi possível fazer o download do arquivo: " + urlProxy);
                }

                context.Response.Buffer = true;
                context.Response.Charset = "";
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.ContentType = webClient.ResponseHeaders["content-type"];
                context.Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                context.Response.BinaryWrite(binaryData);
                context.Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}