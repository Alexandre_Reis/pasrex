<%@ WebHandler Language="C#" Class="ControllerTestaSinacor" %>

using System;
using System.Web;
using Newtonsoft.Json;
using Financial.Util;
using Financial.Investidor;
using System.Collections.Generic;
using System.Configuration;
using EntitySpaces.Interfaces;
using Financial.Processamento;
using Financial.Investidor.Controller;
using Financial.Web.Enums;
using Financial.Investidor.Enums;
using Financial.Security;
using Financial.Interfaces.Sinacor;


public class ControllerTestaSinacor : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
    
        
    
        string codigoSinacor = context.Request["codigosinacor"];
        
        string output = "";
        
                //Tentar encontrar codigo no Sinacor
        TscclibolCollection tscclibolCollection = new TscclibolCollection();
        //
        tscclibolCollection.es.Connection.Name = "Sinacor";
        tscclibolCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        //tscclibolCollection.Query.Select(tscclibolCollection.Query.CdCpfcgc);
        tscclibolCollection.Query.Where(tscclibolCollection.Query.CdCliente.Equal(codigoSinacor));

        tscclibolCollection.Query.Load();

        if (tscclibolCollection.Count == 0)
        {
            //C�digo n�o encontrado no Sinacor
            throw new Exception("C�digo de cliente n�o encontrado no Sinacor: " + codigoSinacor);
        }
        
         TscclictaCollection tscclictaCollection = new TscclictaCollection();
        tscclictaCollection.es.Connection.Name = "Sinacor";
        tscclictaCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        //
        tscclictaCollection.Query.Where(tscclictaCollection.Query.CdCliente == tscclibolCollection[0].CdCliente,
            tscclictaCollection.Query.InInativa == "N", tscclictaCollection.Query.InPrincipal == "S");
        //
        tscclictaCollection.Query.Load();

        if (tscclictaCollection.Count <=0)
        {
            throw new Exception("Nenhuma conta atende aos criterios do query para: " + codigoSinacor);
        }

        output = "Conta encontrada: " + tscclictaCollection[0].CdBanco + "-" + tscclictaCollection[0].CdAgencia + "-" + tscclictaCollection[0].NrConta;
        
        context.Response.ContentType = "text/html";
        context.Response.Write(output);

    }

    

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}