using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Configuration;

namespace Financial.WebServices.Provider
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class SIAnbid : System.Web.Services.WebService
    {
        private SIAnbidWebService.Service service;

        private void InitializeService()
        {
            this.service = new SIAnbidWebService.Service();
            string serviceUrl = Financial.WebServices.Provider.Properties.Settings.Default.SIAnbid_URL;
            
            if (!string.IsNullOrEmpty(serviceUrl))
            {
                this.service.Url = serviceUrl;
            }
            else
            {
                this.service.Url = "http://atatika.financialonline.com.br/Financial.WebServices.SIAnbid/Service.asmx";
            }

            string proxyInfo = ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo))
            {
                //Utilizar padrao de proxy definido no Windows
                service.Proxy = System.Net.WebProxy.GetDefaultProxy();
                service.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default")
                {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    service.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1)
                    {
                        //Foram passadas tambem as credenciais de rede
                        service.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }
        }

        [WebMethod]
        public DataTable RetornaCotas(string codFundo, DateTime? dataInicio)
        {
            if (service == null)
            {
                this.InitializeService();
            }
            
            return service.RetornaCotas(codFundo, dataInicio);
        }

        [WebMethod]
        public DataTable CarregaFundo(string codFundo, string selectColumns, bool loadExtendedProperties)
        {
            if (service == null)
            {
                this.InitializeService();
            }

            return service.CarregaFundo(codFundo, selectColumns, loadExtendedProperties);
        }

        [WebMethod]
        public DataTable BuscaFundos(string cnpj, string nomeFantasia, string codFundo, string selectColumns)
        {
            if (service == null)
            {
                this.InitializeService();
            }

            return service.BuscaFundos(cnpj, nomeFantasia, codFundo, selectColumns);
        }


    }
}
