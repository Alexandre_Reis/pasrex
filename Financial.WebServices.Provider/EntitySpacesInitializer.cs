﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Financial.WebServices.Provider
{
    public class EntitySpacesInitializer : IHttpModule
    {
        public void Application_Start(object sender, EventArgs e)
        {


        }
        public void Dispose()
        {
        }

        private void AddEntitySpacesSqlConnection(string connectionName, string encryptedConnectionString)
        {
            EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

            //Decrypt connection string
            conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

            conn.Name = connectionName;
            conn.Provider = "EntitySpaces.SqlClientProvider";
            conn.ProviderClass = "DataProvider";
            conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
            conn.ProviderMetadataKey = "esDefault";
            conn.DatabaseVersion = "2005";
            EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
        }

        private void AddEntitySpacesOracleConnection(string connectionName, string encryptedConnectionString)
        {
            EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

            //Decrypt connection string
            conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

            conn.Name = connectionName;
            conn.Provider = "EntitySpaces.OracleClientProvider";
            conn.ProviderClass = "DataProvider";
            conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
            conn.ProviderMetadataKey = "esDefault";
            conn.DatabaseVersion = "2005";
            EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
        }

        private string DecryptConnectionString(string encryptedConnectionString)
        {
            const string SHARED_SECRET = "financial@$";
            string[] connectionStringTokens = encryptedConnectionString.Split(';');
            System.Collections.Generic.List<string> decryptedConnectionStringList = new System.Collections.Generic.List<string>();

            foreach (string connectionStringToken in connectionStringTokens)
            {
                string token = connectionStringToken.Trim();
                if (token.ToLower().StartsWith("password"))
                {
                    int passwordStart = token.IndexOf("=") + 1;
                    string password = token.Substring(passwordStart);
                    string decryptedPassword = Financial.Util.Crypto.DecryptStringAES(password, SHARED_SECRET);
                    token = "Password=" + decryptedPassword;
                }

                decryptedConnectionStringList.Add(token);
            }

            string decryptedConnectionString = String.Join(";", decryptedConnectionStringList.ToArray());
            return decryptedConnectionString;
        }

        private static bool HasAppStarted = false;
        private readonly static object _syncObject = new object();

        public void Init(HttpApplication application)
        {
            if (!HasAppStarted)
            {
                lock (_syncObject)
                {
                    if (!HasAppStarted)
                    {
                        HasAppStarted = true;
                        const string FINANCIAL_CONNECTION_NAME = "Financial";
                        const string SINACOR_CONNECTION_NAME = "Sinacor";

                        EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
                        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Clear();

                        ConnectionStringSettings connectionSettings = ConfigurationManager.ConnectionStrings[FINANCIAL_CONNECTION_NAME];
                        if (connectionSettings == null)
                        {
                            throw new Exception("Não foi possível encontrar a conexão Financial no Web.Config");
                        }

                        string encryptedConnectionString = connectionSettings.ConnectionString;
                        AddEntitySpacesSqlConnection(FINANCIAL_CONNECTION_NAME, encryptedConnectionString);
                        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Default = FINANCIAL_CONNECTION_NAME;


                        connectionSettings = ConfigurationManager.ConnectionStrings[SINACOR_CONNECTION_NAME];
                        if (connectionSettings == null)
                        {
                            throw new Exception("Não foi possível encontrar a conexão Oracle no Web.Config");
                        }

                        encryptedConnectionString = connectionSettings.ConnectionString;
                        AddEntitySpacesOracleConnection(SINACOR_CONNECTION_NAME, encryptedConnectionString);

                        /*bool webconfigEncrypted = ConfigurationManager.AppSettings["CriptografaSenhaConexao"] == "true";
                        if (webconfigEncrypted)
                        {
                            const string FINANCIAL_CONNECTION_NAME = "Financial";
                            const string SINACOR_CONNECTION_NAME = "Sinacor";

                            ConnectionStringSettings connectionSettings = ConfigurationManager.ConnectionStrings[FINANCIAL_CONNECTION_NAME];
                            if (connectionSettings == null)
                            {
                                throw new Exception("Não foi possível encontrar a conexão Financial no Web.Config");
                            }
            
                            string encryptedConnectionString = connectionSettings.ConnectionString;

                            //Hoje consideramos que se estamos encriptando estamos usando o Financial no SQL Server. 
                            //Se mudar, precisamos rever esse IF e passar pela connection string o provider
                            AddEntitySpacesSqlConnection(FINANCIAL_CONNECTION_NAME, encryptedConnectionString);
                            EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Default = FINANCIAL_CONNECTION_NAME;

                            connectionSettings = ConfigurationManager.ConnectionStrings[SINACOR_CONNECTION_NAME];
                            if (connectionSettings != null)
                            {
                                encryptedConnectionString = connectionSettings.ConnectionString;
                                if (!string.IsNullOrEmpty(encryptedConnectionString))
                                {
                                    AddEntitySpacesOracleConnection(SINACOR_CONNECTION_NAME, encryptedConnectionString);
                                }
                            }
                        }

                        EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
                        string arquivo = Financial.WebConfigConfiguration.DiretorioAplicacao.DiretorioBaseAplicacao + "web.Config";
                        log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(arquivo));

                        if (EntitySpaces.Interfaces.esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
                        {
                            EntitySpaces.Core.esUtility u = new EntitySpaces.Core.esUtility();
                            string sql = " set language english ";
                            u.ExecuteNonQuery(EntitySpaces.Interfaces.esQueryType.Text, sql, "");
                        }*/
                    }
                }
            }
        }
    }
}
