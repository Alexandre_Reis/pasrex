﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Financial.RendaFixa;
using Financial.Integracao.Excel;
using System.Collections.Generic;
using Financial.Security;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using System.Configuration;
using Financial.CRM;
using Financial.ContaCorrente;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Interfaces.Sinacor;
using Financial.Util;
using Financial.CRM.Enums;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.RendaFixa.Custom2;
using EntitySpaces.Core;

namespace Financial.WebServices.Provider.Custom.Itau
{
    /// <summary>
    /// Summary description for RendaFixa
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class RendaFixa : System.Web.Services.WebService
    {
        public Dictionary<int, string> TabelaHistorico;

        const int ID_INICIAL_FUNDOS = 90000000;

        public enum CdProdutoTR
        {
            Debenture = 80040,
            CRIIsento = 60040,
            CRITributado = 60140,
            DebentureInfra = 60240,

            LCIIsento = 60000,
            LCITributado = 1,

            LCAIsento = 60070,
            LCATributado = 2,

            LF = 60130,
            CRAIsento = 60200,
            CRATributado = 60210
        }

        public enum CdHistorico
        {
            AplicacaoCDBCliente = 4473,
            PagamentoResgate = 4479,
            VendaDebenture = 4475,
            IRRF = 4460,
            IOFTituloRendaFixa = 4477,
            AmortizacaoTitulo = 4481,
            RemuneracaoTitulo = 4483,
            PagamentoJurosDebenture = 4485
        }

        public ValidateLogin Authentication;

        private void InitTabelaHistorico()
        {
            TabelaHistorico = new Dictionary<int, string>();
            TabelaHistorico.Add((int)CdHistorico.AplicacaoCDBCliente, "COMPRA – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.PagamentoResgate, "RESGATE – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.VendaDebenture, "VENDA – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.IRRF, "IRRF – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.IOFTituloRendaFixa, "IOF – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.AmortizacaoTitulo, "AMORTIZAÇÃO – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.RemuneracaoTitulo, "REMUNERAÇÃO – RENDA FIXA");
            TabelaHistorico.Add((int)CdHistorico.PagamentoJurosDebenture, "JUROS – RENDA FIXA");
        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public DataTable Movimentacao(DateTime data)
        {
         
            esUtility es = new esUtility();
            esParameters parametros = new esParameters();
            parametros.Add(new esParameter("DataOperacao",data.Date));
            DataTable dt = es.FillDataTable(esQueryType.Text, "select * from dbo.Movimentacao where DataOperacao = @DataOperacao",parametros);
            
            //,new esParameter("DataOperacao",data,esParameterDirection.Input,DbType.DateTime,1)
            return dt;
        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public DataTable Posicao(DateTime data)
        {
            esUtility es = new esUtility();
            esParameters parametros = new esParameters();
            parametros.Add(new esParameter("DataOperacao", data.Date));
            DataTable dt = es.FillDataTable(esQueryType.Text, "select * from dbo.Posicao where DataOperacao = @DataOperacao", parametros);

            return dt;

        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public DataTable PB(DateTime data)
        {
            esUtility es = new esUtility();
            esParameters parametros = new esParameters();
            parametros.Add(new esParameter("DataOperacao", data.Date));
            DataTable dt = es.FillDataTable(esQueryType.Text, "select * from dbo.PB where DataOperacao = @DataOperacao", parametros);


            return dt;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public void ImportaOperacaoRendaFixa(int IdCliente, DateTime DataOperacao, string TipoOperacao, decimal Quantidade, decimal PUOperacao, string CodigoIsin, byte IdCustodia, byte IdLiquidacao, decimal ValorCorretagem, int NumeroNota, decimal ValorISS, decimal Emolumento, int TipoLiquidacaoSinacor, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            ValoresExcelOperacaoRendaFixa operacaoExcel = new ValoresExcelOperacaoRendaFixa();

            operacaoExcel.IdCliente = IdCliente;
            operacaoExcel.DataOperacao = DataOperacao;
            if (TipoOperacao == "C")
            {
                operacaoExcel.TipoOperacao = "CompraFinal";
            }
            else if (TipoOperacao == "V")
            {
                operacaoExcel.TipoOperacao = "VendaFinal";
            }
            else
            {
                string msgErro = "Tipo de Operação não suportada: " + TipoOperacao;
                throw new Exception(msgErro);
            }

            operacaoExcel.Quantidade = Quantidade;
            operacaoExcel.PuOperacao = PUOperacao;
            operacaoExcel.Valor = Quantidade * PUOperacao;

            //Localizar titulo pelo CodigoIsin
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            if (!tituloRendaFixa.BuscaPorCodigoIsin(CodigoIsin))
            {
                string msgErro = "Título com Código ISIN:" + CodigoIsin + " não encontrado";
                MsgErro = msgErro;
                return;
            }
            operacaoExcel.IdTitulo = tituloRendaFixa.IdTitulo.Value;

            operacaoExcel.IdCustodia = IdCustodia;
            operacaoExcel.IdLiquidacao = IdLiquidacao;
            operacaoExcel.ValorCorretagem = ValorCorretagem;
            operacaoExcel.NumeroNota = NumeroNota;
            operacaoExcel.ValorISS = ValorISS;
            operacaoExcel.Emolumento = Emolumento;
            operacaoExcel.DataOriginal = DataOperacao;
            if (TipoLiquidacaoSinacor == 2)
            {
                operacaoExcel.DataLiquidacao = DataOperacao;
            }
            else if (TipoLiquidacaoSinacor == 1)
            {
                operacaoExcel.DataLiquidacao = Calendario.AdicionaDiaUtil(DataOperacao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }


            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

            operacaoExcel.IdAgenteCorretora = idAgenteMercado;


            ImportacaoBasePage basePage = new ImportacaoBasePage();
            basePage.valoresExcelOperacaoRendaFixa = new List<ValoresExcelOperacaoRendaFixa>();
            basePage.valoresExcelOperacaoRendaFixa.Add(operacaoExcel);

            basePage.CarregaOperacaoRendaFixa(false);


        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public List<int> ExportaIdCliente(out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            ClienteCollection clientes = new ClienteCollection();
            clientes.Query.Where(clientes.Query.IdTipo.NotEqual((int)TipoClienteFixo.Fundo));
            clientes.Load(clientes.Query);

            List<int> idsClientes = new List<int>();
            foreach (Cliente cliente in clientes)
            {
                idsClientes.Add(cliente.IdCliente.Value);
            }

            return idsClientes;
        }

        private void RemoveEnderecosNaoCorrespondencia()
        {
            PessoaEnderecoCollection enderecos = new PessoaEnderecoCollection();
            enderecos.Query.Where(enderecos.Query.RecebeCorrespondencia.Equal("N"));
            enderecos.Load(enderecos.Query);
            enderecos.MarkAllAsDeleted();
            enderecos.Save();

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool ImportaClienteSinacor(int IdClienteSinacor, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            CadastrosBasicos_Pessoa cadastroBasicoPessoa = new CadastrosBasicos_Pessoa();
            cadastroBasicoPessoa.ImportarCadastroSinacorClienteCarteira(IdClienteSinacor, true);

            this.RemoveEnderecosNaoCorrespondencia();

            return true;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool AtualizaClienteSinacor(int IdClienteSinacor, out string MsgErro)
        {
            List<int> idsClienteSinacor = new List<int>();
            idsClienteSinacor.Add(IdClienteSinacor);
            this.AtualizaClientesSinacor(idsClienteSinacor, out MsgErro);
            return true;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<PosicaoRendaFixaCustodiaViewModel> RetornaPosicaoCustodia(int? IdCustodia, int? IdCliente)
        {

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            //PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            EmissorQuery emissorQuery = new EmissorQuery("E");

            posicaoRendaFixaQuery.Select(
                                          posicaoRendaFixaQuery.IdCliente,
                                          posicaoRendaFixaQuery.PUMercado.Avg(),
                                          emissorQuery.Nome.As("NomeEmissor"),
                //papelRendaFixaQuery.Descricao.As("Papel"),
                                          tituloRendaFixaQuery.Descricao.As("Descricao"),
                                          posicaoRendaFixaQuery.Quantidade.Sum().As("Quantidade"),
                                          posicaoRendaFixaQuery.ValorMercado.Sum().As("ValorMercado")
                                          );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            //posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
            posicaoRendaFixaQuery.InnerJoin(emissorQuery).On(tituloRendaFixaQuery.IdEmissor == emissorQuery.IdEmissor);

            if (IdCustodia.HasValue)
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCustodia.Equal(IdCustodia.Value));
            }

            if (IdCliente.HasValue)
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(IdCliente.Value));
            }

            posicaoRendaFixaQuery.GroupBy(posicaoRendaFixaQuery.IdCliente,
                tituloRendaFixaQuery.Descricao, emissorQuery.Nome);

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            List<PosicaoRendaFixaCustodiaViewModel> posicoesRendaFixaViewModel = new List<PosicaoRendaFixaCustodiaViewModel>();
            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                PosicaoRendaFixaCustodiaViewModel posicaoRendaFixaViewModel = new PosicaoRendaFixaCustodiaViewModel(posicaoRendaFixa);
                posicoesRendaFixaViewModel.Add(posicaoRendaFixaViewModel);

            }

            return posicoesRendaFixaViewModel;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<int> RetornaClientesCustoMensal(DateTime Data)
        {
            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
            List<int> todosIds = tabelaCustosRendaFixa.RetornaClientesCustoMensal(Data);
            List<int> idsRendaFixa = new List<int>();
            foreach (int id in todosIds)
            {
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(id) && cliente.IdTipo != (int)TipoClienteFixo.Fundo && id < ID_INICIAL_FUNDOS)
                {
                    idsRendaFixa.Add(id);
                }
            }
            return idsRendaFixa;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<Financial.RendaFixa.TabelaCustosRendaFixa.TaxaCustodia> RetornaLiquidacaoTaxaCustodia(DateTime Data)
        {
            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
            List<TabelaCustosRendaFixa.TaxaCustodia> todosIds = tabelaCustosRendaFixa.RetornaLiquidacaoTaxaCustodia(Data);
            List<TabelaCustosRendaFixa.TaxaCustodia> idsRendaFixa = new List<TabelaCustosRendaFixa.TaxaCustodia>();
            foreach (TabelaCustosRendaFixa.TaxaCustodia id in todosIds)
            {
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(id.idCliente) && cliente.IdTipo != (int)TipoClienteFixo.Fundo)
                {
                    idsRendaFixa.Add(id);
                }
            }
            return idsRendaFixa;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<Financial.RendaFixa.TabelaCustosRendaFixa.TaxaCustodiaAnual> RetornaValorCustodiaAnual(DateTime Data)
        {
            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
            List<TabelaCustosRendaFixa.TaxaCustodiaAnual> todosIds = tabelaCustosRendaFixa.RetornaValorCustodiaAnual(Data);
            List<TabelaCustosRendaFixa.TaxaCustodiaAnual> idsRendaFixa = new List<TabelaCustosRendaFixa.TaxaCustodiaAnual>();
            foreach (TabelaCustosRendaFixa.TaxaCustodiaAnual id in todosIds)
            {
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(id.idCliente) && cliente.IdTipo != (int)TipoClienteFixo.Fundo)
                {
                    idsRendaFixa.Add(id);
                }
            }
            return idsRendaFixa;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool AtualizaClientesSinacor(List<int> IdClienteSinacorList, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            CadastrosBasicos_Pessoa cadastroBasicoPessoa = new CadastrosBasicos_Pessoa();
            cadastroBasicoPessoa.ImportarPessoaSinacor(IdClienteSinacorList, true);

            this.RemoveEnderecosNaoCorrespondencia();

            return true;
        }

        private int CalculaDiasProjecao(OperacaoRendaFixa operacaoRendaFixa)
        {
            if (operacaoRendaFixa.DataLiquidacao.Value.Date != operacaoRendaFixa.DataOperacao.Value)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private int CalculaDiasProjecao(LiquidacaoRendaFixa liquidacaoRendaFixa)
        {
            return 0;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<LiquidacaoViewModel> ExportaLiquidacaoFinanceira(DateTime DataLancamento, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            this.InitTabelaHistorico();
            List<LiquidacaoViewModel> liquidacoesViewModel = new List<LiquidacaoViewModel>();

            OperacaoRendaFixaCollection operacaoCollection = new OperacaoRendaFixaCollection();
            operacaoCollection.Query.Where(operacaoCollection.Query.DataOperacao.Equal(DataLancamento));
            operacaoCollection.Query.OrderBy(operacaoCollection.Query.DataOperacao.Ascending, operacaoCollection.Query.IdOperacao.Ascending);
            operacaoCollection.Query.Load();

            foreach (OperacaoRendaFixa operacao in operacaoCollection)
            {
                if (operacao.TipoOperacao != (byte)TipoOperacaoTitulo.CompraFinal && (operacao.Status == (byte)StatusOperacaoRendaFixa.Digitado ||
                        operacao.Status == (byte)StatusOperacaoRendaFixa.Ajustado))
                {
                    //Se for venda que nao se encontra no status liberado, ignorar
                    continue;
                }

                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(operacao.IdTitulo.Value);
                PapelRendaFixa papel = new PapelRendaFixa();
                papel.LoadByPrimaryKey(titulo.IdPapel.Value);

                //Lancar IOF pois o mesmo vale para Cetip e CBLC
                if (operacao.ValorIOF.Value > 0)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = operacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.IOFTituloRendaFixa;
                    liquidacaoViewModel.Valor = operacao.ValorIOF.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(operacao);
                    liquidacaoViewModel.Quantidade = operacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = operacao.IdCustodia;

                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }

                if (operacao.IdCustodia.Value == (byte)CustodiaTitulo.CBLC)
                {
                    //Ignorar demais lançamentos de CBLC (só considerar IOF)
                    continue;
                }

                if (operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = operacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.AplicacaoCDBCliente;
                    liquidacaoViewModel.Valor = operacao.Valor.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(operacao);
                    liquidacaoViewModel.Quantidade = operacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = operacao.IdCustodia;
                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }
                else if (operacao.TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal ||
                    operacao.TipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    if (papel.Classe == (int)ClasseRendaFixa.Debenture)
                    {
                        liquidacaoViewModel = new LiquidacaoViewModel();
                        liquidacaoViewModel.IdCliente = operacao.IdCliente.Value;
                        liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.VendaDebenture;
                        liquidacaoViewModel.Valor = operacao.Valor.Value;
                        liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(operacao);
                        liquidacaoViewModel.Quantidade = operacao.Quantidade.Value;
                        liquidacaoViewModel.Descricao = titulo.Descricao;
                        liquidacaoViewModel.IdCustodia = operacao.IdCustodia;
                        liquidacoesViewModel.Add(liquidacaoViewModel);
                    }
                    else
                    {
                        liquidacaoViewModel = new LiquidacaoViewModel();
                        liquidacaoViewModel.IdCliente = operacao.IdCliente.Value;
                        liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.PagamentoResgate;
                        liquidacaoViewModel.Valor = operacao.Valor.Value;
                        liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(operacao);
                        liquidacaoViewModel.Quantidade = operacao.Quantidade.Value;
                        liquidacaoViewModel.Descricao = titulo.Descricao;
                        liquidacaoViewModel.IdCustodia = operacao.IdCustodia;

                        liquidacoesViewModel.Add(liquidacaoViewModel);
                    }
                }
            }

            LiquidacaoRendaFixaCollection liquidacaoCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataLiquidacao.Equal(DataLancamento),
                liquidacaoCollection.Query.Status.In((byte)StatusOperacaoRendaFixa.LiberadoSemAjuste, (byte)StatusOperacaoRendaFixa.LiberadoComAjuste));
            liquidacaoCollection.Query.OrderBy(liquidacaoCollection.Query.DataLiquidacao.Ascending, liquidacaoCollection.Query.IdLiquidacao.Ascending);
            liquidacaoCollection.Query.Load();

            foreach (LiquidacaoRendaFixa liquidacao in liquidacaoCollection)
            {
                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(liquidacao.IdTitulo.Value);

                int? idCustodia = null;
                if (!liquidacao.IdPosicaoResgatada.HasValue)
                {
                    continue;
                }

                PosicaoRendaFixaHistorico posicaoResgatada = new PosicaoRendaFixaHistorico();
                posicaoResgatada.Query.Where(posicaoResgatada.Query.IdPosicao.Equal(liquidacao.IdPosicaoResgatada.Value));
                posicaoResgatada.Query.es.Top = 1;
                posicaoResgatada.Query.Load();

                if (!(posicaoResgatada.es.HasData && posicaoResgatada.IdCustodia.HasValue))
                {
                    continue;
                }

                idCustodia = posicaoResgatada.IdCustodia.Value;

                if (posicaoResgatada.IdCustodia.Value == (byte)CustodiaTitulo.CBLC)
                {
                    //Ignorar operacoes de cblc - só vale atualmente pra CETIP
                    continue;
                }


                PapelRendaFixa papel = new PapelRendaFixa();
                papel.LoadByPrimaryKey(titulo.IdPapel.Value);

                if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.Venda)
                {
                    //Nao fazer nada pois estamos fazendo na Operacao do tipo Venda                   
                }
                else if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.Juros)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.PagamentoJurosDebenture;
                    liquidacaoViewModel.Valor = liquidacao.ValorBruto.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                    liquidacaoViewModel.Quantidade = liquidacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = idCustodia;

                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }
                else if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.Amortizacao)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.AmortizacaoTitulo;
                    liquidacaoViewModel.Valor = liquidacao.ValorBruto.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                    liquidacaoViewModel.Quantidade = liquidacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = idCustodia;

                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }
                else if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.PagtoPrincipal)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.AmortizacaoTitulo;
                    liquidacaoViewModel.Valor = liquidacao.ValorBruto.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                    liquidacaoViewModel.Quantidade = liquidacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = idCustodia;

                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }
                else if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.Vencimento)
                {
                    LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                    liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                    liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.PagamentoResgate;
                    liquidacaoViewModel.Valor = liquidacao.ValorBruto.Value;
                    liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                    liquidacaoViewModel.Quantidade = liquidacao.Quantidade.Value;
                    liquidacaoViewModel.Descricao = titulo.Descricao;
                    liquidacaoViewModel.IdCustodia = idCustodia;

                    liquidacoesViewModel.Add(liquidacaoViewModel);
                }


                //Fazer lancamentos de IR
                /*if (liquidacao.ValorIR > 0)
                {
                    if (liquidacao.TipoLancamento == (byte)TipoLancamentoLiquidacao.Amortizacao)
                    {
                        LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                        liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                        liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.IRRFAmortizacaoTituloRendaFixa;
                        liquidacaoViewModel.Valor = liquidacao.ValorIR.Value;
                        liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                        liquidacoesViewModel.Add(liquidacaoViewModel);
                    }
                    else
                    {
                        LiquidacaoViewModel liquidacaoViewModel = new LiquidacaoViewModel();
                        liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                        liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.IRRF;
                        liquidacaoViewModel.Valor = liquidacao.ValorIR.Value;
                        liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                        liquidacoesViewModel.Add(liquidacaoViewModel);

                        if (papel.Classe == (int)ClasseRendaFixa.Debenture || papel.Classe == (int)ClasseRendaFixa.CRI_CCI ||
                            papel.Classe == (int)ClasseRendaFixa.CRI_CCI_Cetip)
                        {
                            liquidacaoViewModel = new LiquidacaoViewModel();
                            liquidacaoViewModel.IdCliente = liquidacao.IdCliente.Value;
                            liquidacaoViewModel.CdHistorico = (int)RendaFixa.CdHistorico.IROperacaoNPDebentureCRI;
                            liquidacaoViewModel.Valor = liquidacao.ValorIR.Value;
                            liquidacaoViewModel.DiasProjecao = CalculaDiasProjecao(liquidacao);
                            liquidacoesViewModel.Add(liquidacaoViewModel);
                        }
                    }
                }
                */



            }
            return liquidacoesViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<OperacaoRendaFixaViewModel> ExportaOperacaoRendaFixa(DateTime DataExportacao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");

            operacaoRendaFixaQuery.Select(
                                          papelRendaFixaQuery.IdPapel,
                                          operacaoRendaFixaQuery.DataLiquidacao,
                                          operacaoRendaFixaQuery.DataOperacao,
                                          operacaoRendaFixaQuery.DataVolta,
                                          operacaoRendaFixaQuery.Fonte,
                                          operacaoRendaFixaQuery.IdAgenteCorretora,
                                          operacaoRendaFixaQuery.IdCliente,
                                          operacaoRendaFixaQuery.IdCustodia,
                                            operacaoRendaFixaQuery.IdLiquidacao,
                                            operacaoRendaFixaQuery.IdOperacao,
                                            operacaoRendaFixaQuery.IdOperacaoResgatada,
                                            operacaoRendaFixaQuery.IdPosicaoResgatada,
                                            operacaoRendaFixaQuery.IdTitulo,
                                            operacaoRendaFixaQuery.NumeroNota,
                                            operacaoRendaFixaQuery.PUOperacao,
                                            operacaoRendaFixaQuery.PUVolta,
                                            operacaoRendaFixaQuery.Quantidade,
                                            operacaoRendaFixaQuery.Rendimento,
                                            operacaoRendaFixaQuery.TaxaNegociacao,
                                            operacaoRendaFixaQuery.TaxaOperacao,
                                            operacaoRendaFixaQuery.TaxaVolta,
                                            operacaoRendaFixaQuery.TipoNegociacao,
                                            operacaoRendaFixaQuery.TipoOperacao,
                                            operacaoRendaFixaQuery.Valor,
                                            operacaoRendaFixaQuery.ValorCorretagem,
                                            operacaoRendaFixaQuery.Emolumento,
                                            operacaoRendaFixaQuery.ValorIOF,
                                            operacaoRendaFixaQuery.ValorIR,
                                            operacaoRendaFixaQuery.ValorLiquido,
                                            operacaoRendaFixaQuery.ValorVolta
                                          );

            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);

            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.Equal(DataExportacao));
            operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Ascending,
                operacaoRendaFixaQuery.IdOperacao.Ascending);

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            List<OperacaoRendaFixaViewModel> operacoesRendaFixaViewModel = new List<OperacaoRendaFixaViewModel>();
            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                OperacaoRendaFixaViewModel operacaoRendaFixaViewModel = new OperacaoRendaFixaViewModel(operacaoRendaFixa);
                operacoesRendaFixaViewModel.Add(operacaoRendaFixaViewModel);

            }

            return operacoesRendaFixaViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<OperacaoRendaFixaBatimentoViewModel> ExportaOperacaoRendaFixaBatimento(int? IdCliente, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta, tituloRendaFixaQuery.DataEmissao);
            operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);

            //
            operacaoRendaFixaQuery.Where(
                operacaoRendaFixaQuery.IdCustodia.Equal((byte)CustodiaTitulo.Cetip) &
                (operacaoRendaFixaQuery.StatusExportacao.IsNull() |
                operacaoRendaFixaQuery.StatusExportacao.NotIn(
                    (int)StatusExportacaoRendaFixa.Exportado) &
                               operacaoRendaFixaQuery.StatusExportacao.NotIn(
                    (int)StatusExportacaoRendaFixa.LiberadoExportacao) &
                               operacaoRendaFixaQuery.StatusExportacao.NotIn(
                    (int)StatusExportacaoRendaFixa.ProcessadoSucesso)
                    ));

            if (IdCliente.HasValue)
            {
                operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(IdCliente.Value));
            }

            operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Descending);

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            List<OperacaoRendaFixaBatimentoViewModel> operacoesRendaFixaViewModel = new List<OperacaoRendaFixaBatimentoViewModel>();
            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                OperacaoRendaFixaBatimentoViewModel operacaoRendaFixaViewModel = new OperacaoRendaFixaBatimentoViewModel(operacaoRendaFixa);
                operacoesRendaFixaViewModel.Add(operacaoRendaFixaViewModel);
            }

            return operacoesRendaFixaViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<PosicaoRendaFixaMensalViewModel> ExportaPosicaoMensal(DateTime DataExportacao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }


            DateTime dataExportacaoAjustada = Calendario.IsDiaUtil(DataExportacao) ? DataExportacao : Calendario.SubtraiDiaUtil(DataExportacao, 1);

            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(
                                          posicaoRendaFixaQuery.IdCliente,
                                          papelRendaFixaQuery.Classe.As("ClassePapel"),
                                          tituloRendaFixaQuery.DebentureInfra.As("DebentureInfra"),
                                          posicaoRendaFixaQuery.Quantidade.Sum().As("Quantidade"),
                                          (posicaoRendaFixaQuery.Quantidade * posicaoRendaFixaQuery.PUMercado).As("ValorMercado").Sum()
                                          );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);

            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataHistorico.Equal(dataExportacaoAjustada));

            posicaoRendaFixaQuery.GroupBy(posicaoRendaFixaQuery.IdCliente,
                papelRendaFixaQuery.Classe, tituloRendaFixaQuery.DebentureInfra);

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            operacaoRendaFixaQuery.Select(
                                          operacaoRendaFixaQuery.IdCliente,
                                          operacaoRendaFixaQuery.TipoOperacao,
                                          papelRendaFixaQuery.Classe.As("ClassePapel"),
                                          tituloRendaFixaQuery.DebentureInfra.As("DebentureInfra"),
                                          operacaoRendaFixaQuery.Quantidade.Sum().As("Quantidade"),
                                          (operacaoRendaFixaQuery.Quantidade * operacaoRendaFixaQuery.PUOperacao).As("Valor").Sum()
                                          );

            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);

            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.LessThanOrEqual(dataExportacaoAjustada),
                operacaoRendaFixaQuery.DataLiquidacao.GreaterThan(dataExportacaoAjustada));

            operacaoRendaFixaQuery.GroupBy(operacaoRendaFixaQuery.TipoOperacao, operacaoRendaFixaQuery.IdCliente,
                papelRendaFixaQuery.Classe, tituloRendaFixaQuery.DebentureInfra);

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            //Indexar operacoes para poder casa-las com as posicoes
            Dictionary<string, OperacaoRendaFixa> dicOperacoesRendaFixaCompra = new Dictionary<string, OperacaoRendaFixa>();
            Dictionary<string, OperacaoRendaFixa> dicOperacoesRendaFixaVenda = new Dictionary<string, OperacaoRendaFixa>();
            Dictionary<string, PosicaoRendaFixaHistorico> dicPosicaoRendaFixaVenda = new Dictionary<string, PosicaoRendaFixaHistorico>();


            List<PosicaoRendaFixaMensalViewModel> posicoesRendaFixaViewModel = new List<PosicaoRendaFixaMensalViewModel>();

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                string chave = operacaoRendaFixa.IdCliente.Value + "#" + Convert.ToInt32(operacaoRendaFixa.GetColumn("ClassePapel")) +
                    "#" + operacaoRendaFixa.GetColumn("DebentureInfra");
                if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal || operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraCasada ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda || operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.Deposito)
                {
                    if (dicOperacoesRendaFixaCompra.ContainsKey(chave))
                    {
                        dicOperacoesRendaFixaCompra[chave].Quantidade += operacaoRendaFixa.Quantidade.Value;
                        dicOperacoesRendaFixaCompra[chave].Valor += operacaoRendaFixa.Valor.Value;

                    }
                    else
                    {
                        dicOperacoesRendaFixaCompra.Add(chave, operacaoRendaFixa);
                    }
                }
                else if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal || operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.Retirada ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaCasada || operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
                {
                    if (dicOperacoesRendaFixaVenda.ContainsKey(chave))
                    {
                        dicOperacoesRendaFixaVenda[chave].Quantidade += operacaoRendaFixa.Quantidade.Value;
                        dicOperacoesRendaFixaVenda[chave].Valor += operacaoRendaFixa.Valor.Value;
                    }
                    else
                    {
                        dicOperacoesRendaFixaVenda.Add(chave, operacaoRendaFixa);
                    }
                }
                else if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                {
                    //Adicionar a posicao inteira
                    posicoesRendaFixaViewModel.Add(new PosicaoRendaFixaMensalViewModel(operacaoRendaFixa));
                }
            }

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
            {

                //Tentar casar operacao
                string chave = posicaoRendaFixa.IdCliente.Value + "#" + Convert.ToInt32(posicaoRendaFixa.GetColumn("ClassePapel")) +
                    "#" + posicaoRendaFixa.GetColumn("DebentureInfra");

                if (dicOperacoesRendaFixaCompra.ContainsKey(chave))
                {
                    OperacaoRendaFixa operacaoRendaFixa = dicOperacoesRendaFixaCompra[chave];

                    posicaoRendaFixa.Quantidade = posicaoRendaFixa.Quantidade - operacaoRendaFixa.Quantidade.Value;

                    posicaoRendaFixa.ValorMercado = posicaoRendaFixa.ValorMercado - operacaoRendaFixa.Valor.Value;
                }

                if (dicOperacoesRendaFixaVenda.ContainsKey(chave))
                {
                    OperacaoRendaFixa operacaoRendaFixa = dicOperacoesRendaFixaVenda[chave];

                    posicaoRendaFixa.Quantidade = posicaoRendaFixa.Quantidade + operacaoRendaFixa.Quantidade.Value;

                    posicaoRendaFixa.ValorMercado = posicaoRendaFixa.ValorMercado + operacaoRendaFixa.Valor.Value;

                }

                PosicaoRendaFixaMensalViewModel posicaoRendaFixaViewModel = new PosicaoRendaFixaMensalViewModel(posicaoRendaFixa);
                posicoesRendaFixaViewModel.Add(posicaoRendaFixaViewModel);
                
                dicPosicaoRendaFixaVenda.Add(chave,posicaoRendaFixa);
            }

            #region volta para a posição as vendas finais que zeram posição
            foreach (KeyValuePair<string,OperacaoRendaFixa>  dicOperacaoVenda in dicOperacoesRendaFixaVenda)
            {
                string chave = dicOperacaoVenda.Key;
                if(!dicPosicaoRendaFixaVenda.ContainsKey(chave))
                {
                    OperacaoRendaFixa operacaoRendaFixa = dicOperacaoVenda.Value;

                    //Adicionar a posicao inteira
                    posicoesRendaFixaViewModel.Add(new PosicaoRendaFixaMensalViewModel(operacaoRendaFixa));
                }
            }
            #endregion



            //Consolidar posicoes
            Dictionary<string, PosicaoRendaFixaMensalViewModel> dicPosicoesRendaFixaViewModel = new Dictionary<string, PosicaoRendaFixaMensalViewModel>();
            List<PosicaoRendaFixaMensalViewModel> posicoesConsolidadas = new List<PosicaoRendaFixaMensalViewModel>();
            foreach (PosicaoRendaFixaMensalViewModel posicao in posicoesRendaFixaViewModel)
            {
                string chave = posicao.IdCliente + "#" + posicao.CodigoProduto;
                if (dicPosicoesRendaFixaViewModel.ContainsKey(chave))
                {
                    dicPosicoesRendaFixaViewModel[chave].Quantidade += posicao.Quantidade;
                    dicPosicoesRendaFixaViewModel[chave].Valor += posicao.Valor;
                }
                else
                {
                    dicPosicoesRendaFixaViewModel.Add(chave, posicao);
                }
            }

            foreach (KeyValuePair<string, PosicaoRendaFixaMensalViewModel> kvp in dicPosicoesRendaFixaViewModel)
            {

                string chave = kvp.Key;
                PosicaoRendaFixaMensalViewModel posRendaFixa = kvp.Value;

                if (posRendaFixa.Quantidade != 0)
                {
                    posicoesConsolidadas.Add(kvp.Value);
                }
            }

            return posicoesConsolidadas;

        }
        
        [SoapHeader("Authentication")]
        [WebMethod]
        public List<IntegracaoTR> ExportaIntegracaoTR(DateTime DataExportacao, out string MsgErro)
        {

            MsgErro = "";
            this.InitTabelaHistorico();

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            #region Consulta Sql OperacaoRendaFixaCollection
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            //
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.Equal(DataExportacao),
                operacaoRendaFixaQuery.Status.In((byte)StatusOperacaoRendaFixa.LiberadoComAjuste, (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste));
            //

            operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.IdCliente.Ascending);
            //
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);
            #endregion

            #region Consulta Sql LiquidacaoRendaFixaCollection
            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            //
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Vencimento,
                (byte)TipoLancamentoLiquidacao.Juros, (byte)TipoLancamentoLiquidacao.PagtoPrincipal) &&
                                            liquidacaoRendaFixaQuery.DataLiquidacao.Equal(DataExportacao) &&
                                            liquidacaoRendaFixaQuery.Status.In((int)StatusLiquidacaoRendaFixa.LiberadoSemAjuste,
                                                (int)StatusLiquidacaoRendaFixa.LiberadoComAjuste));
            //
            liquidacaoRendaFixaQuery.OrderBy(liquidacaoRendaFixaQuery.IdCliente.Ascending);
            //
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);
            #endregion

            List<IntegracaoTR> integracoesTR = new List<IntegracaoTR>();

            for (int operacaoCount = 0; operacaoCount < operacaoRendaFixaCollection.Count; operacaoCount++)
            {
                IntegracaoTR integracaoTR = null;
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[operacaoCount];
                #region Preenchimento do Lancamento de IR
                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(operacaoRendaFixa.IdTitulo.Value);
                PapelRendaFixa papel = new PapelRendaFixa();
                papel.LoadByPrimaryKey(titulo.IdPapel.Value);

                if (papel.Classe == (int)ClasseRendaFixa.Debenture || papel.Classe == (int)ClasseRendaFixa.LF)
                {
                    integracaoTR = new IntegracaoTR();
                    integracaoTR.CdHistorico = (int)CdHistorico.VendaDebenture;

                    if (papel.Classe == (int)ClasseRendaFixa.Debenture)
                    {
                        integracaoTR.ProdutoTR = titulo.DebentureInfra == "S" ? (int)CdProdutoTR.DebentureInfra : (int)CdProdutoTR.Debenture;
                    }
                    else if (papel.Classe == (int)ClasseRendaFixa.LF)
                    {
                        integracaoTR.ProdutoTR = (int)CdProdutoTR.LF;
                    }

                    integracaoTR.IdCliente = operacaoRendaFixa.IdCliente.Value;
                    integracaoTR.DescricaoLancamento = TabelaHistorico[integracaoTR.CdHistorico];
                    integracaoTR.ValorLancamento = operacaoRendaFixa.Valor.Value;
                    integracaoTR.DataLancamento = operacaoRendaFixa.DataOperacao.Value;
                    integracaoTR.ValorCompra = 0;
                    integracaoTR.ValorRendimento = operacaoRendaFixa.Rendimento.Value;
                    integracaoTR.ValorIR = operacaoRendaFixa.ValorIR.Value;
                    integracaoTR.Estorno = "N";
                    integracaoTR.DiasProjecao = CalculaDiasProjecao(operacaoRendaFixa);
                    integracaoTR.Quantidade = operacaoRendaFixa.Quantidade.Value;
                    integracaoTR.Descricao = titulo.Descricao;
                    integracaoTR.IdCustodia = operacaoRendaFixa.IdCustodia;
                    if (integracaoTR.ValorRendimento > 0 || integracaoTR.ValorIR > 0)
                    {
                        integracoesTR.Add(integracaoTR);
                    }

                }
                else if (papel.Classe == (int)ClasseRendaFixa.CRI || papel.Classe == (int)ClasseRendaFixa.CCI ||
                    papel.Classe == (int)ClasseRendaFixa.CRA || papel.Classe == (int)ClasseRendaFixa.LCI ||
                     papel.Classe == (int)ClasseRendaFixa.LCA)
                {

                    LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
                    RendaOperacao rendaOperacao = liquidacaoRendaFixa.CalculaRendaItau(operacaoRendaFixa.IdOperacao.Value);

                    integracaoTR = new IntegracaoTR();
                    integracaoTR.CdHistorico = (int)CdHistorico.IRRF;
                    if (papel.Classe == (int)ClasseRendaFixa.CRI || papel.Classe == (int)ClasseRendaFixa.CCI)
                    {
                        integracaoTR.ProdutoTR = (int)CdProdutoTR.CRIIsento;
                    }
                    else if (papel.Classe == (int)ClasseRendaFixa.CRA)
                    {
                        integracaoTR.ProdutoTR = (int)CdProdutoTR.CRAIsento;
                    }
                    else if (papel.Classe == (int)ClasseRendaFixa.LCI)
                    {
                        integracaoTR.ProdutoTR = (int)CdProdutoTR.LCIIsento;
                    }
                    else if (papel.Classe == (int)ClasseRendaFixa.LCA)
                    {
                        integracaoTR.ProdutoTR = (int)CdProdutoTR.LCAIsento;
                    }

                    integracaoTR.IdCliente = operacaoRendaFixa.IdCliente.Value;
                    integracaoTR.DescricaoLancamento = TabelaHistorico[integracaoTR.CdHistorico];
                    integracaoTR.ValorLancamento = operacaoRendaFixa.Valor.Value;
                    integracaoTR.DataLancamento = operacaoRendaFixa.DataOperacao.Value;
                    integracaoTR.ValorCompra = 0;
                    decimal valorRendimentoNaoTributavel = rendaOperacao.rendaNaoTributavel;
                    decimal valorRendimentoTributavel = rendaOperacao.rendaTributavel;
                    integracaoTR.ValorRendimento = valorRendimentoNaoTributavel;
                    integracaoTR.ValorIR = 0;
                    integracaoTR.Estorno = "N";
                    integracaoTR.DiasProjecao = CalculaDiasProjecao(operacaoRendaFixa);
                    integracaoTR.Quantidade = operacaoRendaFixa.Quantidade.Value;
                    integracaoTR.Descricao = titulo.Descricao;
                    integracaoTR.IdCustodia = operacaoRendaFixa.IdCustodia;
                    if (integracaoTR.ValorRendimento > 0 || integracaoTR.ValorIR > 0)
                    {
                        integracoesTR.Add(integracaoTR);
                    }

                    if (valorRendimentoTributavel > 0)
                    {
                        integracaoTR = new IntegracaoTR();
                        integracaoTR.CdHistorico = (int)CdHistorico.IRRF;

                        if (papel.Classe == (int)ClasseRendaFixa.CRI || papel.Classe == (int)ClasseRendaFixa.CCI)
                        {
                            integracaoTR.ProdutoTR = (int)CdProdutoTR.CRITributado;
                        }
                        else if (papel.Classe == (int)ClasseRendaFixa.CRA)
                        {
                            integracaoTR.ProdutoTR = (int)CdProdutoTR.CRATributado;
                        }
                        else if (papel.Classe == (int)ClasseRendaFixa.LCI)
                        {
                            integracaoTR.ProdutoTR = (int)CdProdutoTR.LCITributado;
                        }
                        else if (papel.Classe == (int)ClasseRendaFixa.LCA)
                        {
                            integracaoTR.ProdutoTR = (int)CdProdutoTR.LCATributado;
                        }

                        integracaoTR.IdCliente = operacaoRendaFixa.IdCliente.Value;
                        integracaoTR.DescricaoLancamento = TabelaHistorico[integracaoTR.CdHistorico];
                        integracaoTR.ValorLancamento = operacaoRendaFixa.Valor.Value;
                        integracaoTR.DataLancamento = operacaoRendaFixa.DataOperacao.Value;
                        integracaoTR.ValorCompra = 0;
                        integracaoTR.ValorRendimento = valorRendimentoTributavel;
                        integracaoTR.ValorIR = operacaoRendaFixa.ValorIR.Value;
                        integracaoTR.Estorno = "N";
                        integracaoTR.DiasProjecao = CalculaDiasProjecao(operacaoRendaFixa);
                        integracaoTR.Quantidade = operacaoRendaFixa.Quantidade.Value;
                        integracaoTR.Descricao = titulo.Descricao;
                        integracaoTR.IdCustodia = operacaoRendaFixa.IdCustodia;
                        if (integracaoTR.ValorRendimento > 0 || integracaoTR.ValorIR > 0)
                        {
                            integracoesTR.Add(integracaoTR);
                        }
                    }
                }

                #endregion
            }

            for (int operacaoCount = 0; operacaoCount < liquidacaoRendaFixaCollection.Count; operacaoCount++)
            {
                LiquidacaoRendaFixa liquidacaoRendaFixa = liquidacaoRendaFixaCollection[operacaoCount];

                #region Preenchimento do Lancamento de IR

                IntegracaoTR integracaoTR = new IntegracaoTR();

                //Vencimento, juros, pagto principal
                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(liquidacaoRendaFixa.IdTitulo.Value);
                PapelRendaFixa papel = new PapelRendaFixa();
                papel.LoadByPrimaryKey(titulo.IdPapel.Value);

                if (liquidacaoRendaFixa.TipoLancamento.Value == (byte)TipoLancamentoLiquidacao.Vencimento)
                {
                    integracaoTR.CdHistorico = (int)CdHistorico.PagamentoResgate;
                }
                else if (liquidacaoRendaFixa.TipoLancamento.Value == (byte)TipoLancamentoLiquidacao.Juros)
                {
                    integracaoTR.CdHistorico = (int)CdHistorico.RemuneracaoTitulo;
                }
                else if (liquidacaoRendaFixa.TipoLancamento.Value == (byte)TipoLancamentoLiquidacao.PagtoPrincipal)
                {
                    integracaoTR.CdHistorico = (int)CdHistorico.AmortizacaoTitulo;
                }
                else
                {
                    continue;
                }

                integracaoTR.IdCliente = liquidacaoRendaFixa.IdCliente.Value;
                integracaoTR.DescricaoLancamento = TabelaHistorico[integracaoTR.CdHistorico];
                integracaoTR.ValorLancamento = liquidacaoRendaFixa.ValorBruto.Value;
                integracaoTR.DataLancamento = liquidacaoRendaFixa.DataLiquidacao.Value;
                integracaoTR.ValorCompra = 0;
                integracaoTR.ValorRendimento = liquidacaoRendaFixa.Rendimento.Value;
                integracaoTR.ValorIR = liquidacaoRendaFixa.ValorIR.Value;
                integracaoTR.Estorno = "N";
                integracaoTR.DiasProjecao = CalculaDiasProjecao(liquidacaoRendaFixa);
                integracaoTR.Quantidade = liquidacaoRendaFixa.Quantidade.Value;
                integracaoTR.Descricao = titulo.Descricao;

                #region Busca IdCustodia
                if (liquidacaoRendaFixa.IdPosicaoResgatada.HasValue)
                {
                    PosicaoRendaFixaHistorico posicaoResgatada = new PosicaoRendaFixaHistorico();
                    posicaoResgatada.Query.Where(posicaoResgatada.Query.IdPosicao.Equal(liquidacaoRendaFixa.IdPosicaoResgatada.Value));
                    posicaoResgatada.Query.es.Top = 1;
                    posicaoResgatada.Query.Load();

                    if (posicaoResgatada.es.HasData && posicaoResgatada.IdCustodia.HasValue)
                    {
                        integracaoTR.IdCustodia = posicaoResgatada.IdCustodia.Value;
                    }
                }
                #endregion


                if (papel.Classe == (int)ClasseRendaFixa.Debenture)
                {
                    integracaoTR.ProdutoTR = titulo.DebentureInfra == "S" ? (int)CdProdutoTR.DebentureInfra : (int)CdProdutoTR.Debenture;
                }
                else if (papel.Classe == (int)ClasseRendaFixa.LF)
                {
                    integracaoTR.ProdutoTR = (int)CdProdutoTR.LF;
                }

                else if (papel.Classe == (int)ClasseRendaFixa.CRI || papel.Classe == (int)ClasseRendaFixa.CCI)
                {
                    integracaoTR.ProdutoTR = (int)CdProdutoTR.CRIIsento;
                }
                else if (papel.Classe == (int)ClasseRendaFixa.CRA)
                {
                    integracaoTR.ProdutoTR = (int)CdProdutoTR.CRAIsento;
                }
                else if (papel.Classe == (int)ClasseRendaFixa.LCI)
                {
                    integracaoTR.ProdutoTR = (int)CdProdutoTR.LCIIsento;
                }
                else if (papel.Classe == (int)ClasseRendaFixa.LCA)
                {
                    integracaoTR.ProdutoTR = (int)CdProdutoTR.LCAIsento;
                }

                else
                {
                    continue; //Nao importar para ProdutoTR desconhecido
                }

                if (integracaoTR.ValorRendimento > 0 || integracaoTR.ValorIR > 0)
                {
                    integracoesTR.Add(integracaoTR);
                }

                #endregion
            }

            return integracoesTR;
        }

        [WebMethod]
        public List<int> ListaClienteBloqueadoSinacor()
        {
            //HistoricoLog historicoLog = new HistoricoLog();
            //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "01-Entrando método ListaClienteBloqueadoSinacor", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);
            List<int> retorno = new List<int>();
            //return retorno;
            TscdocsCollection tscdocsCol = new TscdocsCollection();
            tscdocsCol.es.Connection.Name = "Sinacor";
            tscdocsCol.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            //
            tscdocsCol.Query.Select(tscdocsCol.Query.DtBalPatrimonial, tscdocsCol.Query.CdCpfcgc);
            tscdocsCol.Query.Where(tscdocsCol.Query.DtBalPatrimonial < DateTime.Today);
            //

            //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "02-Antes do Select Parcial no Sinacor", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);
            tscdocsCol.Query.Load();

            //tscdocsCol.LoadAll();
            //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "03-Após Load no Sinacor", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);

            if (tscdocsCol.Count > 0)
            {
                //Carregar pessoas na memoria
                Dictionary<decimal, int> pessoasDictionary = new Dictionary<decimal, int>();
                PessoaCollection pessoasRF = new PessoaCollection();
                pessoasRF.Query.Where(pessoasRF.Query.IdPessoa < ID_INICIAL_FUNDOS);
                //  historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "04-Antes Load Pessoas FOL", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);
                pessoasRF.Load(pessoasRF.Query);
                //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "05-Após Load Pessoas FOL", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);

                foreach (Pessoa pessoaRF in pessoasRF)
                {
                    decimal cpfcnpj;
                    if (Decimal.TryParse(Utilitario.RemoveCaracteresEspeciais(pessoaRF.Cpfcnpj), out cpfcnpj))
                    {
                        if ((cpfcnpj > 0) && !pessoasDictionary.ContainsKey(cpfcnpj))
                        {
                            pessoasDictionary.Add(cpfcnpj, pessoaRF.IdPessoa.Value);
                        }
                    }
                }
                //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "06-Após Loop Pessoas", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);


                foreach (Tscdocs tscdocs in tscdocsCol)
                {
                    if (tscdocs.DtBalPatrimonial != null)
                    {
                        if (tscdocs.CdCpfcgc.HasValue && (tscdocs.DtBalPatrimonial.Value.Date < DateTime.Today))
                        {
                            int idPessoa;
                            if (pessoasDictionary.TryGetValue(tscdocs.CdCpfcgc.Value, out idPessoa))
                            {
                                retorno.Add(idPessoa);
                            }
                        }
                    }
                }
                //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "07-Após Loop TSCDOCS", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);
            }

            //historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, "08-Fim método ListaClienteBloqueadoSinacor", "", "", "", Financial.Security.Enums.HistoricoLogOrigem.Outros);
            return retorno;
        }

        private Pessoa BuscaPessoaPorCPFNPJ(string cpfcnpj)
        {
            Pessoa pessoaRF = null;

            PessoaCollection pessoas = new PessoaCollection().BuscaPessoasPorCPFCNPJ(cpfcnpj);
            if (pessoas != null && pessoas.Count > 0)
            {
                foreach (Pessoa pessoa in pessoas)
                {
                    if (pessoa.IdPessoa.Value < ID_INICIAL_FUNDOS)
                    {
                        pessoaRF = pessoa;
                        break;
                    }
                }
            }

            return pessoaRF;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<TituloRendaFixaViewModel> ExportaTituloRendaFixa(out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            List<TituloRendaFixaViewModel> titulosViewModel = new List<TituloRendaFixaViewModel>();
            TituloRendaFixaCollection titulos = new TituloRendaFixaCollection();
            TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelQuery = new PapelRendaFixaQuery("P");
            tituloQuery.Select(tituloQuery.Descricao, tituloQuery.CodigoIsin, tituloQuery.DataVencimento, papelQuery.Descricao.As("DescricaoPapel"));
            tituloQuery.LeftJoin(papelQuery).On(tituloQuery.IdPapel.Equal(papelQuery.IdPapel));
            titulos.Load(tituloQuery);

            foreach (TituloRendaFixa titulo in titulos)
            {
                TituloRendaFixaViewModel tituloViewModel = new TituloRendaFixaViewModel(titulo);
                titulosViewModel.Add(tituloViewModel);
            }

            return titulosViewModel;
        }

        [WebMethod]
        public string VerificaBloqueioOperacaoSinacor(int idCliente, DateTime dataOperacao, string codigoCBLC, int indexTipoOperacao)
        {
            string bloqueio = "";

            TorbloqueioCollection torbloqueioCollection = new TorbloqueioCollection();

            torbloqueioCollection.es.Connection.Name = "Sinacor";
            torbloqueioCollection.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            torbloqueioCollection.Query.Where(torbloqueioCollection.Query.CdCliente.Equal(idCliente),
                                              torbloqueioCollection.Query.DtInicio.LessThanOrEqual(dataOperacao),
                                              torbloqueioCollection.Query.DtFinal.GreaterThanOrEqual(dataOperacao));
            torbloqueioCollection.Query.Load();

            foreach (Torbloqueio torbloqueio in torbloqueioCollection)
            {
                bool bloqueado = false;

                string codigoAtivo = codigoCBLC;

                if (String.IsNullOrEmpty(torbloqueio.CdCodneg) || (torbloqueio.CdCodneg == codigoAtivo))
                {
                    if (torbloqueio.TpBloqueio == "A" ||
                        (torbloqueio.TpBloqueio == "C" && indexTipoOperacao == 0) || //Compra Final
                        (torbloqueio.TpBloqueio == "V" && indexTipoOperacao == 1))    //Venda Final
                    {
                        bloqueado = true;
                    }
                }

                if (bloqueado)
                {
                    bloqueio = "Bloqueado! " + torbloqueio.DsBloqueio;
                }
            }

            return bloqueio;
        }

        [WebMethod]
        public bool IsClienteBloqueadoSinacor(int IdCliente)
        {
            return false;
            bool isBloqueado = false;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(IdCliente);
            decimal cpfCGC = Convert.ToDecimal(Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj));
            //select dt_bal_patrimonial from corrwin.tscdocs

            TscdocsCollection tscdocsCol = new TscdocsCollection();
            tscdocsCol.es.Connection.Name = "Sinacor";
            tscdocsCol.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            //
            tscdocsCol.Query.Where(tscdocsCol.Query.CdCpfcgc == cpfCGC);
            //

            tscdocsCol.Query.Load();

            if (tscdocsCol.Count >= 1)
            {
                Tscdocs tscdocs = tscdocsCol[0];
                if (tscdocs.DtBalPatrimonial != null)
                {
                    if (tscdocs.DtBalPatrimonial.Value.Date < DateTime.Today)
                    {
                        isBloqueado = true;
                    }
                }
            }

            return isBloqueado;
        }

        public class ValidateLogin : SoapHeader
        {
            public string Username;
            public string Password;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<NotaCorretagemRendaFixa> ExportaNotaCorretagem(DateTime dataInicio, DateTime dataFim, int? idAgente, int? idCustodia, int? idCliente, int? numeroNota)
        {

            if (idAgente == null)
            {
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            }

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }


            List<int> listaIdClientes = new List<int>();
            if (idCliente.HasValue)
            {
                listaIdClientes.Add(idCliente.Value);
            }
            else
            {
                ClienteQuery c = new ClienteQuery("C");
                c.Select(c.IdCliente);

                ClienteCollection cliente = new ClienteCollection();
                cliente.Load(c);

                foreach (Cliente c1 in cliente)
                {
                    listaIdClientes.Add(c1.IdCliente.Value);
                }
            }

            #region Datas
            List<DateTime> listaDatas = new List<DateTime>();

            DateTime dataAux = dataInicio;
            while (dataAux <= dataFim)
            {
                listaDatas.Add(dataAux);
                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            #endregion


            List<NotaCorretagemRendaFixa> notas = new List<NotaCorretagemRendaFixa>();

            for (int i = 0; i < listaIdClientes.Count; i++)
            {

                for (int j = 0; j < listaDatas.Count; j++)
                {
                    NotaCorretagemRendaFixa n = new NotaCorretagemRendaFixa(listaIdClientes[i], listaDatas[j],
                                                                            idAgente.Value, idCustodia,
                                                                            null, null, null, "", numeroNota);

                    if (n.GetQuantidadeRegistros() != 0)
                    {
                        notas.Add(n);
                    }
                }
            }

            return notas;
        }

        private bool CheckAuthentication(string username, string password)
        {
            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            NameValueCollection listaValores = new NameValueCollection();
            listaValores.Add("passwordFormat", "Encrypted");
            financialMembershipProvider.Initialize(null, listaValores);

            bool autenticado = financialMembershipProvider.ValidateUser(username, password);
            return autenticado;
        }


        public class IntegracaoTR
        {
            public int IdCliente;
            public int CdHistorico;
            public string DescricaoLancamento;
            public decimal ValorLancamento;
            public DateTime DataLancamento;
            public decimal ValorCompra;
            public decimal ValorRendimento;
            public decimal ValorIR;
            public int ProdutoTR;
            public string Estorno;
            public int DiasProjecao;
            public decimal Quantidade;
            public string Descricao;
            public int? IdCustodia;

            public IntegracaoTR()
            {
            }

        }

        public class LiquidacaoViewModel
        {
            public int IdCliente;
            public int CdHistorico;
            public decimal Valor;
            public int DiasProjecao;
            public bool Exportar;
            public string Descricao;
            public decimal Quantidade;
            public int? IdCustodia;

            public LiquidacaoViewModel()
            {
            }

        }

        public class OperacaoRendaFixaViewModel
        {

            public int ClassePapel;
            public DateTime DataLiquidacao;
            public DateTime DataOperacao;
            public DateTime? DataVolta;
            public byte Fonte;
            public int? IdAgenteCorretora;
            public int IdCliente;
            public int IdCustodia;
            public int IdLiquidacao;
            public int IdOperacao;
            public int? IdOperacaoResgatada;
            public int? IdPosicaoResgatada;
            public int IdTitulo;
            public int? NumeroNota;
            public decimal PUOperacao;
            public decimal? PUVolta;
            public decimal Quantidade;
            public decimal Rendimento;
            public decimal TaxaNegociacao;
            public decimal? TaxaOperacao;
            public decimal? TaxaVolta;
            public byte TipoNegociacao;
            public byte TipoOperacao;
            public decimal Valor;
            public decimal ValorCorretagem;
            public decimal Emolumento;
            public decimal ValorIOF;
            public decimal ValorIR;
            public decimal ValorLiquido;
            public decimal? ValorVolta;

            public OperacaoRendaFixaViewModel()
            {
            }
            public OperacaoRendaFixaViewModel(OperacaoRendaFixa operacaoRendaFixa)
            {
                this.ClassePapel = Convert.ToInt32(operacaoRendaFixa.GetColumn("IdPapel"));
                this.DataLiquidacao = operacaoRendaFixa.DataLiquidacao.Value;
                this.DataOperacao = operacaoRendaFixa.DataOperacao.Value;
                this.DataVolta = operacaoRendaFixa.DataVolta;
                this.Fonte = operacaoRendaFixa.Fonte.Value;
                this.IdAgenteCorretora = operacaoRendaFixa.IdAgenteCorretora;
                this.IdCliente = operacaoRendaFixa.IdCliente.Value;
                this.IdCustodia = operacaoRendaFixa.IdCustodia.Value;
                this.IdLiquidacao = operacaoRendaFixa.IdLiquidacao.Value;
                this.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
                this.IdOperacaoResgatada = operacaoRendaFixa.IdOperacaoResgatada;
                this.IdPosicaoResgatada = operacaoRendaFixa.IdPosicaoResgatada;
                this.IdTitulo = operacaoRendaFixa.IdTitulo.Value;
                this.NumeroNota = operacaoRendaFixa.NumeroNota;
                this.PUOperacao = operacaoRendaFixa.PUOperacao.Value;
                this.PUVolta = operacaoRendaFixa.PUVolta;
                this.Quantidade = operacaoRendaFixa.Quantidade.Value;
                this.Rendimento = operacaoRendaFixa.Rendimento.Value;
                this.TaxaNegociacao = operacaoRendaFixa.TaxaNegociacao.Value;
                this.TaxaOperacao = operacaoRendaFixa.TaxaOperacao;
                this.TaxaVolta = operacaoRendaFixa.TaxaVolta;
                this.TipoNegociacao = operacaoRendaFixa.TipoNegociacao.Value;
                this.TipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                this.Valor = operacaoRendaFixa.Valor.Value;
                this.ValorCorretagem = operacaoRendaFixa.ValorCorretagem.Value;
                this.Emolumento = operacaoRendaFixa.Emolumento.Value;
                this.ValorIOF = operacaoRendaFixa.ValorIOF.Value;
                this.ValorIR = operacaoRendaFixa.ValorIR.Value;
                this.ValorLiquido = operacaoRendaFixa.ValorLiquido.Value;
                this.ValorVolta = operacaoRendaFixa.ValorVolta;
            }
        }

        public class OperacaoRendaFixaBatimentoViewModel
        {
            public int? Status;
            public string Titulo;
            public DateTime DataEmissaoTitulo;
            public int IdOperacao;
            public byte TipoOperacao;

            public decimal Quantidade;
            public decimal PU;
            public decimal Valor;
            public int IdCliente;
            public string ApelidoCliente;
            public string Contraparte;
            public DateTime Data;
            public int? IdVinculado;
            string DetalheErro;

            public OperacaoRendaFixaBatimentoViewModel()
            {
            }
            public OperacaoRendaFixaBatimentoViewModel(OperacaoRendaFixa operacaoRendaFixa)
            {
                this.ApelidoCliente = Convert.ToString(operacaoRendaFixa.GetColumn("Apelido"));
                this.Contraparte = operacaoRendaFixa.CodigoContraParte;
                this.Data = operacaoRendaFixa.DataOperacao.Value;
                this.DataEmissaoTitulo = Convert.ToDateTime(operacaoRendaFixa.GetColumn("DataEmissao"));
                this.IdCliente = operacaoRendaFixa.IdCliente.Value;
                this.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
                this.IdVinculado = operacaoRendaFixa.IdOperacaoVinculo;
                this.PU = operacaoRendaFixa.PUOperacao.Value;
                this.Quantidade = operacaoRendaFixa.Quantidade.Value;
                this.Status = operacaoRendaFixa.StatusExportacao;
                this.TipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                if (this.Status.HasValue && this.Status.Value == (int)StatusExportacaoRendaFixa.ProcessadoErroOutros)
                {
                    this.DetalheErro = operacaoRendaFixa.Observacao;
                }

                this.Titulo = Convert.ToString(operacaoRendaFixa.GetColumn("DescricaoCompleta"));
                this.Valor = operacaoRendaFixa.Valor.Value;
            }
        }

        public class PosicaoRendaFixaCustodiaViewModel
        {
            public int IdCliente;
            public string Papel;
            public decimal Quantidade;
            public decimal Valor;
            public decimal PUMercado;
            public string NomeEmissor;

            public PosicaoRendaFixaCustodiaViewModel()
            {
            }
            public PosicaoRendaFixaCustodiaViewModel(PosicaoRendaFixa posicaoRendaFixa)
            {
                this.Papel = Convert.ToString(posicaoRendaFixa.GetColumn("Descricao"));
                this.IdCliente = posicaoRendaFixa.IdCliente.Value;
                this.Quantidade = posicaoRendaFixa.Quantidade.Value;
                this.Valor = posicaoRendaFixa.ValorMercado.Value;
                this.PUMercado = posicaoRendaFixa.PUMercado.Value;
                this.NomeEmissor = Convert.ToString(posicaoRendaFixa.GetColumn("NomeEmissor"));
            }
        }

        public class PosicaoRendaFixaMensalViewModel
        {
            public int IdCliente;
            public int CodigoProduto;
            public decimal Quantidade;
            public decimal Valor;

            public PosicaoRendaFixaMensalViewModel()
            {
            }

            public PosicaoRendaFixaMensalViewModel(OperacaoRendaFixa operacaoRendaFixa)
            {
                int classePapel = Convert.ToInt32(operacaoRendaFixa.GetColumn("ClassePapel"));
                bool isDebentureInfra = Convert.ToString(operacaoRendaFixa.GetColumn("DebentureInfra")) == "S";
                this.CodigoProduto = RetornaCodigoProduto(classePapel, isDebentureInfra);

                this.IdCliente = operacaoRendaFixa.IdCliente.Value;
                this.Quantidade = operacaoRendaFixa.Quantidade.Value;
                this.Valor = Convert.ToDecimal(operacaoRendaFixa.GetColumn("Valor"));
            }

            private int RetornaCodigoProduto(int classePapel, bool isDebentureInfra){
                int codigoProduto = 0;

                if (classePapel == (int)ClasseRendaFixa.LCI)
                {
                    codigoProduto = (int)CdProdutoTR.LCIIsento;
                }
                else if (classePapel == (int)ClasseRendaFixa.LCA)
                {
                    codigoProduto = (int)CdProdutoTR.LCAIsento;
                }
                else if (classePapel == (int)ClasseRendaFixa.LF)
                {
                    codigoProduto = (int)CdProdutoTR.LF;
                }
                else if (classePapel == (int)ClasseRendaFixa.Debenture)
                {
                    if (isDebentureInfra)
                    {
                        codigoProduto = (int)CdProdutoTR.DebentureInfra;
                    }
                    else
                    {
                        codigoProduto = (int)CdProdutoTR.Debenture;
                    }
                }
                else if (classePapel == (int)ClasseRendaFixa.CRI)
                {
                    codigoProduto = (int)CdProdutoTR.CRIIsento;
                }
                else if (classePapel == (int)ClasseRendaFixa.CRA)
                {
                    codigoProduto = (int)CdProdutoTR.CRAIsento;
                }

                return codigoProduto;
            }

            public PosicaoRendaFixaMensalViewModel(PosicaoRendaFixaHistorico posicaoRendaFixa)
            {
                int classePapel = Convert.ToInt32(posicaoRendaFixa.GetColumn("ClassePapel"));
                bool isDebentureInfra = Convert.ToString(posicaoRendaFixa.GetColumn("DebentureInfra")) == "S";
                this.CodigoProduto = RetornaCodigoProduto(classePapel, isDebentureInfra);

                this.IdCliente = posicaoRendaFixa.IdCliente.Value;
                this.Quantidade = posicaoRendaFixa.Quantidade.Value;
                this.Valor = posicaoRendaFixa.ValorMercado.Value;
            }
        }

        public class TituloRendaFixaViewModel
        {
            public string Descricao;
            public string DescricaoPapel;
            public string CodigoIsin;
            public DateTime? DataVencimento;
            public TituloRendaFixaViewModel()
            {
            }
            public TituloRendaFixaViewModel(TituloRendaFixa titulo)
            {
                this.Descricao = titulo.Descricao;
                this.DescricaoPapel = Convert.ToString(titulo.GetColumn("DescricaoPapel"));
                this.CodigoIsin = titulo.CodigoIsin;
                this.DataVencimento = titulo.DataVencimento;
            }
        }
    }
}

namespace Financial.RendaFixa.Custom2
{
    public class NotaCorretagemRendaFixa
    {

        public NotaCorretagemRendaFixa()
        {
        }

        #region Privates
        private OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

        private int idCliente;
        private DateTime dataNota;
        private int idAgenteCorretora;

        private int? idCustodia;
        private CustodiaTitulo custodia;

        private int? idAssessor;
        private int? idGrupo;
        private int? idPapel;

        private string cnpj;

        private int? numeroNota;

        /* Header Interno */
        public Header header = new Header();

        /* Footer Interno */
        public Footer footer = new Footer();

        /* Lista interna de Detalhes */
        private List<Detail> lista = new List<Detail>();
        #endregion

        /// <summary>
        /// Total de registros dessa nota
        /// </summary>
        /// <returns></returns>
        public int GetQuantidadeRegistros()
        {
            return this.operacaoRendaFixaCollection.Count;
        }

        #region Properties
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public DateTime DataNota
        {
            get { return dataNota; }
            set { dataNota = value; }
        }

        public int IdAgenteCorretora
        {
            get { return idAgenteCorretora; }
            set { idAgenteCorretora = value; }
        }
        public int? IdCustodia
        {
            get { return idCustodia; }
            set { idCustodia = value; }
        }
        #endregion

        #region Acesso Externo
        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Header GetHeader
        {
            get { return this.header; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public Footer GetFooter
        {
            get { return this.footer; }
        }

        /// <summary>
        /// Acesso Externo
        /// </summary>
        public List<Detail> GetListDetail
        {
            get { return this.lista; }
        }
        #endregion

        #region Construtores

        public NotaCorretagemRendaFixa(int idCliente, DateTime dataNota,
                                       int idAgenteCorretora, int? idCustodia,
                                       int? idAssessor, int? idGrupo, int? idPapel, string cnpj, int? numeroNota)
        {

            this.idCliente = idCliente;
            //
            this.dataNota = dataNota;
            //
            this.idAgenteCorretora = idAgenteCorretora;
            this.idCustodia = idCustodia;
            //
            this.idAssessor = idAssessor;
            this.idGrupo = idGrupo;
            this.idPapel = idPapel;
            //

            this.cnpj = cnpj;
            //
            this.numeroNota = numeroNota;

            #region Consulta OperacaoRendaFixa
            this.operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            OperacaoRendaFixaQuery op = new OperacaoRendaFixaQuery("OP");
            TituloRendaFixaQuery t = new TituloRendaFixaQuery("T");
            ClienteRendaFixaQuery crf = new ClienteRendaFixaQuery("C");
            ClienteQuery cliente = new ClienteQuery("C1");
            PapelRendaFixaQuery papelRF = new PapelRendaFixaQuery("P");
            PessoaQuery pessoa = new PessoaQuery("P1");

            op.Select(op, t.DescricaoCompleta, t.Descricao);
            op.InnerJoin(t).On(op.IdTitulo == t.IdTitulo);
            op.InnerJoin(crf).On(op.IdCliente == crf.IdCliente);
            op.InnerJoin(cliente).On(op.IdCliente == cliente.IdCliente);
            op.InnerJoin(papelRF).On(t.IdPapel == papelRF.IdPapel);
            op.InnerJoin(pessoa).On(op.IdCliente == pessoa.IdPessoa);

            op.Where(op.IdCliente == idCliente,
                     op.DataOperacao == this.dataNota,
                     op.IdAgenteCorretora == idAgenteCorretora,
                     op.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal, (byte)TipoOperacaoTitulo.VendaFinal));

            if (this.idCustodia.HasValue)
            {
                op.Where(op.IdCustodia == (byte)idCustodia);
            }

            if (this.idAssessor.HasValue)
            {
                op.Where(crf.IdAssessor == idAssessor);
            }

            if (this.idGrupo.HasValue)
            {
                op.Where(cliente.IdGrupoAfinidade == idGrupo);
            }

            if (this.idPapel.HasValue)
            {
                op.Where(papelRF.IdPapel == idPapel);
            }

            if (!String.IsNullOrEmpty(this.cnpj))
            {
                op.Where(pessoa.Cpfcnpj.Like(this.cnpj));
            }

            if (this.numeroNota.HasValue)
            {
                op.Where(op.NumeroNota == numeroNota);
            }
            this.operacaoRendaFixaCollection.Load(op);

            #endregion

            this.SetaHeader();
            this.SetaDetail();
            this.SetaFooter();
        }
        #endregion

        #region Inner Class
        public class Header
        {

            public Header()
            {
            }

            public Header(int? a, DateTime b,
                          string c, string d, string e,
                          string f, string g, string h,
                          string i, string j, string k,
                          string l, string m, string n,
                          string o, string p, string q,
                          string r, string s, string t,
                          string u, string v, string x,
                          string y, string w, string z,
                          string a1, string b1, string c1,
                          string d1, string e1, string f1, string g1)
            {

                this.numeroNota = a;
                this.dataPregao = b;
                this.nomeAgente = c;
                this.enderecoCompletoAgente = d;
                this.telefoneCompletoAgente = e;
                this.siteAgente = f;
                this.emailAgente = g;
                this.cnpjAgente = h;
                this.cartaPatente = i;
                this.codigoCliente = j;
                this.cidadeCliente = k;
                this.nomeCliente = l;
                this.enderecoCliente = m;
                this.bairroCliente = n;
                this.cepCliente = o;
                this.ufCliente = p;
                this.telefoneCliente = q;
                this.cpfCNPJCliente = r;
                this.codigoBovespaAgente = s;
                this.codigoAssessor = t;
                this.nomeAssessor = u;
                this.agenteCompensacao = v;
                this.Cliente = x;
                this.Valor = y;
                this.Custodiante = w;
                this.CI = z;
                this.Banco = a1;
                this.Agencia = b1;
                this.ContaCorrente = c1;
                this.Acionista = d1;
                this.Administrador = e1;
                this.ComplementoNome = f1;
                this.Pvinc = g1;
            }

            //Dados da nota
            public int? numeroNota;
            public DateTime dataPregao;

            //Dados da corretora
            public string nomeAgente;
            public string enderecoCompletoAgente;
            public string telefoneCompletoAgente;
            public string faxAgente;
            public string siteAgente;
            public string emailAgente;
            public string cnpjAgente;
            public string cartaPatente;

            //Dados do cliente
            public string codigoCliente;
            public string nomeCliente;
            public string enderecoCliente;
            public string bairroCliente;
            public string cepCliente;
            public string cidadeCliente;
            public string ufCliente;
            public string telefoneCliente;
            public string cpfCNPJCliente;
            public string codigoBovespaAgente; //Está na seção de cliente pois aparece no quadro de codigo de cliente

            //Dados do Assessor
            public string codigoAssessor;
            public string nomeAssessor;

            //Diversos
            public string agenteCompensacao;
            public string Cliente;
            public string Valor;
            public string Custodiante;
            public string CI;
            public string Banco;
            public string Agencia;
            public string ContaCorrente;
            public string Acionista;
            public string Administrador;
            public string ComplementoNome;
            public string Pvinc;
        }

        public class Detail
        {
            public Detail() { }


            public Detail(string a, string b, string c, string d, string e,
                          decimal f, decimal g, decimal h, string i, string j)
            {

                this.tipoOperacao = a;
                this.tipoMercado = b;
                this.prazo = c;
                this.especificacao = d;
                this.observacao = e;
                //
                this.quantidade = f;
                this.preco = g;
                this.valor = h;
                this.q = i;
                this.negociacao = j;
                this.dc = this.tipoOperacao == "C" ? "D" : "C";
            }

            private string tipoOperacao; //C ou V
            private string tipoMercado; //vista
            private string prazo; //hoje em branco
            private string especificacao;
            private string observacao;
            private decimal quantidade;
            private decimal preco;
            private decimal valor;
            private string q;
            private string negociacao;
            private string dc;

            #region Properties Necessarias para o Binding do Grid
            public string Q
            {
                get { return q; }
                set { q = value; }
            }
            public string Negociacao
            {
                get { return negociacao; }
                set { negociacao = value; }
            }
            public string DC
            {
                get { return dc; }
                set { dc = value; }
            }
            public string TipoOperacao
            {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            public string TipoMercado
            {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            public string Prazo
            {
                get { return prazo; }
                set { prazo = value; }
            }

            public string Especificacao
            {
                get { return especificacao; }
                set { especificacao = value; }
            }

            public string Observacao
            {
                get { return observacao; }
                set { observacao = value; }
            }

            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            public decimal Preco
            {
                get { return preco; }
                set { preco = value; }
            }

            public decimal Valor
            {
                get { return valor; }
                set { valor = value; }
            }
            #endregion
        }

        public class Footer
        {
            public Footer() { }

            public Footer(decimal a, decimal b, decimal c, decimal d, decimal e, decimal f,
                        decimal g, string h, decimal i, decimal j, decimal k, decimal l,
                        decimal m, decimal n, decimal o, decimal p, DateTime q, decimal r, string s)
            {
                this.vendasVista = a;
                this.comprasVista = b;
                this.valorLiquido = c;
                this.corretagem = d;
                this.iss = e;
                this.ir = f;
                this.iof = g;
                this.cidadeAdministrador = h;
                this.opcoesCompras = i;
                this.opcoesVendas = j;
                this.valorOperacoes = k;
                this.taxaLiquidacao = l;
                this.taxaRegistro = m;
                this.totalCBLC = n;
                this.totalBovespa = o;
                this.totalDespesas = p;
                this.liquidoPara = q;
                this.valorTotal = r;
                this.mercado = s;
            }

            public decimal vendasVista;
            public decimal comprasVista;
            public decimal opcoesCompras;
            public decimal opcoesVendas;
            public decimal operacoesTermo;
            public decimal operacoesTitulosPublicos;
            public decimal valorOperacoes;

            public decimal valorLiquido;
            public string dcValorLiquido;

            public decimal taxaLiquidacao;
            public string dcTaxaLiquidacao;

            public decimal taxaRegistro;
            public string dcTaxaRegistro;

            public decimal totalCBLC;
            public string dcTotalCBLC;

            public decimal taxaTermoOpcoes;
            public string dcTaxaTermoOpcoes;

            public decimal taxaANA;
            public string dcTaxaANA;

            public decimal emolumentos;
            public string dcEmolumentos;

            public decimal totalBovespa;
            public string dcTotalBovespa;

            public decimal corretagem;
            public string dcCorretagem;

            public decimal iss;
            public string dcIss;

            public decimal ir;
            public string dcIr;

            public decimal iof;
            public string dcIof;

            public decimal totalDespesas;
            public string dcTotalDespesas;

            public DateTime? liquidoPara;

            public string cidadeAdministrador = "";

            public decimal valorTotal;
            public string mercado;

            public DateTime? dataLiquidacao;

        }

        #endregion

        #region Funções
        private void SetaHeader()
        {

            this.header.dataPregao = this.dataNota;

            #region Dados Corretora
            AgenteMercado a = new AgenteMercado();
            a.LoadByPrimaryKey(this.idAgenteCorretora);

            #region DDD
            string ddd = "";
            if (!String.IsNullOrEmpty(a.str.Ddd))
            {
                ddd = "(" + a.str.Ddd + ") ";
            }
            #endregion

            #region CidadeEstado
            string cidadeEstado = "";
            string cidade = "";
            if (!String.IsNullOrEmpty(a.str.Cidade))
            {
                cidadeEstado += a.str.Cidade;
                cidade = a.str.Cidade;
            }
            if (!String.IsNullOrEmpty(a.str.Uf))
            {
                cidadeEstado += " - " + a.str.Uf;
            }
            #endregion

            #region Endereco
            string endereco = "";
            if (!String.IsNullOrEmpty(a.str.Endereco))
            {
                endereco += a.str.Endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Bairro))
            {
                if (!String.IsNullOrEmpty(a.str.Endereco))
                {
                    endereco += ", ";
                }
                endereco += a.str.Bairro;
            }
            #endregion

            #region EnderecoCompleto
            string enderecoCompleto = "";
            if (!String.IsNullOrEmpty(endereco))
            {
                enderecoCompleto += endereco;
            }
            if (!String.IsNullOrEmpty(a.str.Cep))
            {
                enderecoCompleto += "                      " + a.str.Cep;
            }
            if (!String.IsNullOrEmpty(cidadeEstado))
            {

                if (String.IsNullOrEmpty(a.str.Cep))
                {
                    enderecoCompleto += "                      " + cidadeEstado;
                }
                else
                {
                    enderecoCompleto += " " + cidadeEstado;
                }
            }
            // Adapta tamanho maximo
            if (enderecoCompleto.Length >= 97)
            {
                if (enderecoCompleto.Contains("                      "))
                {
                    enderecoCompleto = enderecoCompleto.Replace("                      ", " ");
                }
            }
            #endregion

            this.footer.cidadeAdministrador = cidade;

            this.header.nomeAgente = a.Nome.Trim();
            this.header.enderecoCompletoAgente = enderecoCompleto;
            this.header.telefoneCompletoAgente = !String.IsNullOrEmpty(a.str.Telefone) ? ddd + a.str.Telefone : "";
            this.header.faxAgente = !String.IsNullOrEmpty(a.str.Fax) ? ddd + a.str.Fax : "";
            this.header.siteAgente = a.str.Website;
            this.header.emailAgente = a.str.Email;
            this.header.cnpjAgente = a.str.Cnpj == String.Empty ? "" : Utilitario.MascaraCNPJ(a.str.Cnpj);
            this.header.cartaPatente = a.str.CartaPatente;
            #endregion

            #region Dados Cliente

            PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");

            p.Select(p.Endereco, p.Numero, p.Complemento, p.Bairro, p.Cidade, p.Cep, p.Uf, p.IdPessoa);
            p.Where(p.RecebeCorrespondencia == "S" & p.IdPessoa == this.idCliente);

            PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
            pessoaEnderecoCollection.Load(p);

            string enderecoCompletoCliente = "";
            string cep = "";
            string cidadeEstadoCliente = "";
            string telefone = "";
            string bairro = "";
            string uf = "";
            //
            if (pessoaEnderecoCollection.HasData)
            {
                #region EnderecoCompleto
                string enderecoCliente = pessoaEnderecoCollection[0].str.Endereco.Trim();

                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Numero.Trim()) || !String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Complemento.Trim()))
                {
                    enderecoCliente += ", " + pessoaEnderecoCollection[0].str.Numero.Trim() + "  - " + pessoaEnderecoCollection[0].str.Complemento.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Bairro.Trim()))
                {
                    enderecoCliente += " " + pessoaEnderecoCollection[0].str.Bairro.Trim();
                }

                enderecoCompletoCliente = enderecoCliente;
                #endregion

                #region Cep
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cep.Trim()))
                {
                    cep += Utilitario.MascaraCEP(pessoaEnderecoCollection[0].str.Cep);
                }
                #endregion

                #region CidadeEstado
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Cidade.Trim()))
                {
                    cidadeEstadoCliente += pessoaEnderecoCollection[0].str.Cidade.Trim();
                }
                if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim()))
                {
                    cidadeEstadoCliente += " - " + pessoaEnderecoCollection[0].str.Uf.Trim();
                }
                #endregion

                bairro = pessoaEnderecoCollection[0].Bairro.ToString().Trim();

                uf = bairro = pessoaEnderecoCollection[0].Uf.ToString().Trim();

                Pessoa pes = new Pessoa();
                pes.LoadByPrimaryKey(pessoaEnderecoCollection[0].IdPessoa.Value);

                #region Telefone
                if (!String.IsNullOrEmpty(pes.DDD_Fone))
                {
                    telefone += pes.DDD_Fone;
                }
                #endregion
            }

            Cliente b = new Cliente();
            //
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(b.Query.IdCliente);
            campos.Add(b.Query.Nome);
            b.LoadByPrimaryKey(campos, this.idCliente);

            this.header.codigoCliente = this.idCliente.ToString();
            this.header.nomeCliente = b.Nome;
            this.header.enderecoCliente = enderecoCompletoCliente;
            this.header.telefoneCliente = telefone;
            this.header.cepCliente = cep;
            this.header.cidadeCliente = cidadeEstadoCliente;
            this.header.bairroCliente = bairro;
            this.header.ufCliente = uf;

            //

            #region CNPJ
            string cpfcnpj = "";
            //
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.idCliente);

            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj))
            {
                cpfcnpj += pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }
            #endregion

            this.header.cpfCNPJCliente = cpfcnpj;
            this.header.codigoBovespaAgente = a.CodigoBovespa.HasValue ? a.CodigoBovespa.Value.ToString() : "";
            if (a.DigitoCodigoBovespa.HasValue)
            {
                this.header.codigoBovespaAgente = this.header.codigoBovespaAgente + "-" + a.DigitoCodigoBovespa.Value.ToString().Substring(0, 1);
            }
            #endregion

            #region Dados Assessor
            ClienteRendaFixa c = new ClienteRendaFixa();
            c.LoadByPrimaryKey(this.idCliente);

            string nomeAssessor = "";
            if (c.IdAssessor.HasValue)
            {
                Assessor s = new Assessor();
                if (s.LoadByPrimaryKey(c.IdAssessor.Value))
                {
                    nomeAssessor = s.str.Nome;
                }
                this.header.codigoAssessor = c.IdAssessor.Value.ToString();
                this.header.nomeAssessor = nomeAssessor;
            }
            #endregion

            #region Diversos
            this.header.agenteCompensacao = "";
            this.header.Cliente = "";
            this.header.Valor = "0,00";
            this.header.Custodiante = "";
            this.header.CI = "N";

            this.header.Banco = "";
            this.header.Agencia = "";
            this.header.ContaCorrente = "";
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

            contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(this.IdCliente),
                                    contaCorrenteCollection.Query.ContaDefault.Equal("S"));

            if (contaCorrenteCollection.Query.Load())
            {
                if (contaCorrenteCollection[0].IdBanco.HasValue)
                {
                    Banco banco = new Banco();
                    if (banco.LoadByPrimaryKey((int)contaCorrenteCollection[0].IdBanco))
                    {
                        this.header.Banco = banco.CodigoCompensacao;
                    }
                }
                if (contaCorrenteCollection[0].IdAgencia.HasValue)
                {
                    Agencia agencia = new Agencia();
                    if (agencia.LoadByPrimaryKey((int)contaCorrenteCollection[0].IdAgencia))
                    {
                        this.header.Agencia = agencia.Codigo;
                    }
                }
                this.header.ContaCorrente = contaCorrenteCollection[0].Numero + contaCorrenteCollection[0].DigitoConta;
            }

            this.header.Acionista = "";
            this.header.Administrador = "";
            this.header.ComplementoNome = "";
            this.header.Pvinc = "";
            #endregion
        }

        private void SetaFooter()
        {
            decimal totalVendas = 0, totalCompras = 0, totalValorLiquido = 0,
                    totalCorretagem = 0, totalEmolumentos = 0, totalIss = 0,
                    totalIR = 0, totalIOF = 0, totalTermo = 0, totalTitulosPublicos = 0;

            DateTime dataLiquidacao = Convert.ToDateTime("1900-01-01");

            for (int i = 0; i < this.operacaoRendaFixaCollection.Count; i++)
            {
                if (operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal)
                {
                    totalCompras += operacaoRendaFixaCollection[i].Valor.Value;
                }

                if (operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal)
                {
                    totalVendas += operacaoRendaFixaCollection[i].Valor.Value;
                }

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                if (tituloRendaFixa.LoadByPrimaryKey((int)operacaoRendaFixaCollection[i].IdTitulo))
                {
                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    if (papelRendaFixa.LoadByPrimaryKey((int)tituloRendaFixa.IdPapel))
                    {
                        if (papelRendaFixa.TipoPapel == (byte)TipoPapelTitulo.Publico)
                        {
                            totalTitulosPublicos += operacaoRendaFixaCollection[i].Valor.Value;
                        }
                    }
                }

                if (operacaoRendaFixaCollection[i].OperacaoTermo == "S")
                {
                    totalTermo += operacaoRendaFixaCollection[i].Valor.Value;
                }

                totalValorLiquido += operacaoRendaFixaCollection[i].ValorLiquido.Value;
                totalIR += operacaoRendaFixaCollection[i].ValorIR.Value;
                totalIss += operacaoRendaFixaCollection[i].ValorISS.Value;
                totalCorretagem += operacaoRendaFixaCollection[i].ValorCorretagem.Value;
                totalEmolumentos += operacaoRendaFixaCollection[i].Emolumento.Value;
                totalIOF += operacaoRendaFixaCollection[i].ValorIOF.Value;

                if (this.operacaoRendaFixaCollection[i].DataLiquidacao.HasValue)
                {
                    dataLiquidacao = (DateTime)this.operacaoRendaFixaCollection[i].DataLiquidacao;
                }
            }

            this.footer.vendasVista = totalVendas;
            this.footer.comprasVista = totalCompras;
            this.footer.opcoesCompras = 0;
            this.footer.opcoesVendas = 0;
            this.footer.valorOperacoes = totalVendas + totalCompras;

            decimal taxa = 0;

            this.footer.valorLiquido = Math.Abs(totalVendas - totalCompras);
            this.footer.taxaLiquidacao = taxa;
            this.footer.taxaRegistro = taxa;
            this.footer.totalCBLC = Math.Abs((totalVendas - totalCompras) + taxa);

            this.footer.taxaTermoOpcoes = 0;
            this.footer.taxaANA = 0;
            this.footer.emolumentos = totalEmolumentos;
            this.footer.totalBovespa = totalEmolumentos;

            this.footer.corretagem = totalCorretagem;
            this.footer.iss = totalIss;
            this.footer.iof = totalIOF;
            this.footer.ir = totalIR;
            this.footer.totalDespesas = totalCorretagem + totalIss + totalIR + totalIOF;

            this.footer.operacoesTermo = totalTermo;
            this.footer.operacoesTitulosPublicos = totalTitulosPublicos;

            this.footer.liquidoPara = this.dataNota;

            this.footer.dcValorLiquido = ((totalVendas - totalCompras) >= 0 ? "C" : "D");
            this.footer.dcTaxaLiquidacao = "D";
            this.footer.dcTaxaRegistro = "D";
            this.footer.dcTotalCBLC = (((totalVendas - totalCompras) + taxa) >= 0 ? "C" : "D");
            this.footer.dcTaxaTermoOpcoes = "D";
            this.footer.dcTaxaANA = "D";
            this.footer.dcEmolumentos = "D";
            this.footer.dcTotalBovespa = "D";
            this.footer.dcCorretagem = "D";
            this.footer.dcIss = "D";
            this.footer.dcIof = "D";
            this.footer.dcIr = "D";
            this.footer.dcTotalDespesas = "D";

            this.footer.valorTotal = Math.Abs((totalVendas - totalCompras) - this.footer.totalDespesas);
            this.footer.mercado = "Renda Fixa";

            if (dataLiquidacao != Convert.ToDateTime("1900-01-01"))
            {
                this.footer.dataLiquidacao = dataLiquidacao;
            }
            else
            {
                this.footer.dataLiquidacao = null;
            }

        }

        private void SetaDetail()
        {
            for (int i = 0; i < this.operacaoRendaFixaCollection.Count; i++)
            {
                Detail d = new Detail();
                //
                d.TipoOperacao = operacaoRendaFixaCollection[i].TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ? "C" : "V";
                d.TipoMercado = "VISTA";
                d.Prazo = "";
                d.Especificacao = Convert.ToString(operacaoRendaFixaCollection[i].GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                d.Observacao = "";
                //
                d.Quantidade = operacaoRendaFixaCollection[i].Quantidade.Value;
                d.Preco = operacaoRendaFixaCollection[i].PUOperacao.Value;
                d.Valor = operacaoRendaFixaCollection[i].Valor.Value;
                //
                d.Q = "";
                //
                d.Negociacao = "";
                switch ((byte)operacaoRendaFixaCollection[i].IdCustodia)
                {
                    case (byte)CustodiaTitulo.Cetip: d.Negociacao = "CETIP";
                        break;
                    case (byte)CustodiaTitulo.Selic: d.Negociacao = "SELIC";
                        break;
                    case (byte)CustodiaTitulo.CBLC: d.Negociacao = "7-BOVESPA";
                        break;
                }
                //
                d.DC = (d.TipoOperacao == "C") ? "D" : "C";
                //
                this.lista.Add(d);
            }
        }


        #endregion
    }
}