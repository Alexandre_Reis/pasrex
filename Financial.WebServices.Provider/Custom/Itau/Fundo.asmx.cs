﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Financial.RendaFixa;
using Financial.Integracao.Excel;
using System.Collections.Generic;
using Financial.Security;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using System.Configuration;
using Financial.CRM;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.InvestidorCotista.Custom;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using System.Diagnostics;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

namespace Financial.WebServices.Provider.Custom.Itau
{
    /// <summary>
    /// Summary description for Fundo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Fundo : System.Web.Services.WebService
    {
        public ValidateLogin Authentication;
        const int ID_INICIAL_FUNDOS = 90000000;

        private Carteira BuscaCarteiraFundoPorCNPJ(string cnpj)
        {
            Carteira carteiraFundo = null;

            CarteiraCollection carteiras = new CarteiraCollection().BuscaCarteirasPorCNPJ(cnpj);
            if (carteiras != null && carteiras.Count > 0)
            {
                foreach (Carteira carteira in carteiras)
                {
                    if (carteira.IdCarteira.Value > ID_INICIAL_FUNDOS)
                    {
                        carteiraFundo = carteira;
                        break;
                    }
                }
            }

            return carteiraFundo;
        }

        private Financial.InvestidorCotista.Cotista BuscaCotistaFundoPorCPFNPJ(string cpfcnpj)
        {
            Financial.InvestidorCotista.Cotista cotistaFundo = null;

            CotistaCollection cotistas = new CotistaCollection().BuscaCotistasPorCPFCNPJ(cpfcnpj);
            if (cotistas != null && cotistas.Count > 0)
            {
                foreach (Financial.InvestidorCotista.Cotista cotista in cotistas)
                {
                    if (cotista.IdCotista.Value > ID_INICIAL_FUNDOS)
                    {
                        cotistaFundo = cotista;
                        break;
                    }
                }
            }

            return cotistaFundo;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public CotistaViewModel ExportaCotista(string CpfcnpjCotista, out string MsgErro)
        {

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);
            if (cotista == null)
            {
                MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                return null;
            }
            CotistaViewModel cotistaViewModel = new CotistaViewModel(cotista);

            return cotistaViewModel;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public void AtualizaCodigoInterfaceCotista(string CpfcnpjCotista, string codigoInterface,
            out string MsgErro)
        {

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);
            if (cotista == null)
            {
                MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                return;
            }

            cotista.CodigoInterface = codigoInterface;
            cotista.Save();

        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public ResgateCotistaViewModel ExportaCalculoResgateCotista(string CpfcnpjCotista, string CpfcnpjCarteira, decimal ValorBrutoResgate,
            out string MsgErro)
        {

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(CpfcnpjCarteira);
            if (carteira == null)
            {
                MsgErro = "Não foi possível encontrar a carteira com o CNPJ:" + CpfcnpjCarteira;
                return null;
            }

            Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);
            if (cotista == null)
            {
                MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                return null;
            }

            SimulacaoResgate simulacaoResgate = new SimulacaoResgate(carteira.IdCarteira.Value, cotista.IdCotista.Value,
                TipoOperacaoCotista.ResgateBruto, TipoResgateCotista.FIFO, ValorBrutoResgate);


            ResgateCotistaViewModel resgate = new ResgateCotistaViewModel();
            resgate.CotaOperacao = simulacaoResgate.cotaOperacao;
            resgate.CpfcnpjCarteira = CpfcnpjCarteira;
            resgate.CpfcnpjCotista = CpfcnpjCotista;
            resgate.Ganho = simulacaoResgate.rendimento;
            resgate.NomeCarteira = carteira.Nome;
            resgate.Quantidade = simulacaoResgate.quantidade;
            resgate.ValorBruto = ValorBrutoResgate;
            resgate.ValorIOF = simulacaoResgate.valorIOF;
            resgate.ValorIR = simulacaoResgate.valorIR;
            resgate.ValorLiquido = simulacaoResgate.valorLiquido;

            return resgate;

        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public CarteiraViewModel ExportaCarteira(string Cpfcnpj, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(Cpfcnpj);
            if (carteira == null)
            {
                MsgErro = "Não foi possível encontrar a carteira com o CNPJ:" + Cpfcnpj;
                return null;
            }

            CarteiraViewModel carteiraViewModel = new CarteiraViewModel(carteira, true);


            return carteiraViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<CarteiraViewModel> ExportaListaFundos(out string MsgErro)
        {
            List<CarteiraViewModel> listaFundos = new List<CarteiraViewModel>();

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            CarteiraCollection fundos = new CarteiraCollection();
            fundos.BuscaCarteirasFundos();

            foreach (Carteira fundo in fundos)
            {
                Carteira fundoCompleto = new Carteira();
                fundoCompleto.LoadByPrimaryKey(fundo.IdCarteira.Value);

                if (fundoCompleto.IdCarteira.Value > ID_INICIAL_FUNDOS)
                {
                    listaFundos.Add(new CarteiraViewModel(fundoCompleto, false));
                }
            }

            return listaFundos;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<string> ExportaListaFundosTrace()
        {
            List<CarteiraViewModel> listaFundos = new List<CarteiraViewModel>();
            List<string> trace = new List<string>();

            Stopwatch stopwatch = new Stopwatch();

            trace.Add("Iniciando conexão: " + DateTime.Now);
            stopwatch.Start();

            trace.Add(String.Format("Conexão iniciada: {0}", stopwatch.Elapsed));
            stopwatch.Reset();
            stopwatch.Start();

            /*if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }*/

            CarteiraCollection fundos = new CarteiraCollection();

            trace.Add("Buscando carteiras");
            fundos.BuscaCarteirasFundos();
            trace.Add(String.Format("Busca finalizada: {0}", stopwatch.Elapsed));
            stopwatch.Reset();
            stopwatch.Start();

            foreach (Carteira fundo in fundos)
            {
                Carteira fundoCompleto = new Carteira();
                fundoCompleto.LoadByPrimaryKey(fundo.IdCarteira.Value);

                listaFundos.Add(new CarteiraViewModel(fundoCompleto, false));
                trace.Add(String.Format("Tempo busca carteira {0}-{1}: {2}", fundoCompleto.IdCarteira.Value, fundoCompleto.Nome, stopwatch.Elapsed));
                stopwatch.Reset();
                stopwatch.Start();
            }

            trace.Add("Término: " + DateTime.Now);

            return trace;
        }

        private void AddRentabilidadeMes(List<HistoricoRentabilidadeMes> rentabilidades, DataRow row, int ano, int mesNum, string mesNome, string cpfcnpjCarteira)
        {
            object rentabilidadeColumnValue = row["Rentabilidade" + mesNome];
            if (rentabilidadeColumnValue != System.DBNull.Value)
            {
                HistoricoRentabilidadeMes rentabilidade = new HistoricoRentabilidadeMes();
                rentabilidade.Cpfcnpj = cpfcnpjCarteira;
                rentabilidade.Data = new DateTime(ano, mesNum, 1);
                rentabilidade.Rentabilidade = Convert.ToDecimal(rentabilidadeColumnValue);

                object rentabilidadeIndiceColumnValue = row["RentabIndice" + mesNome];
                if (rentabilidadeColumnValue != System.DBNull.Value)
                {
                    rentabilidade.RentabilidadeIndice = Convert.ToDecimal(rentabilidadeIndiceColumnValue);
                }
                rentabilidades.Add(rentabilidade);
            }
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool ImportaCotista(int? IdCotista, string Nome, byte Tipo, string Cpfcnpj, string IsentoIR, string IsentoIOF,
           byte StatusAtivo, byte TipoTributacao, byte TipoCotistaCVM, string CodigoInterface, string Endereco, string Numero,
            string Complemento, string Bairro, string Cidade, string CEP, string UF, string Pais, string EnderecoCom,
            string NumeroCom, string ComplementoCom, string BairroCom, string CidadeCom, string CEPCom, string UFCom,
            string PaisCom, string Fone, string Email, string FoneCom, string EmailCom, byte EstadoCivil, string NumeroRG,
            string EmissorRG, DateTime? DataEmissaoRG, string Sexo, DateTime? DataNascimento, string Profissao, out string MsgErro)
        {
            MsgErro = "";

            if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            //Verificar se já existe cotista
            Financial.InvestidorCotista.Cotista cotistaExistente = new Financial.InvestidorCotista.Cotista();
            int idCotistaInsert;
            if (IdCotista.HasValue)
            {
                if (cotistaExistente.LoadByPrimaryKey(IdCotista.Value))
                {
                    MsgErro = "Cotista com Id " + IdCotista.Value + " já existente";
                    return false;
                }
                idCotistaInsert = IdCotista.Value;
            }
            else
            {
                if (ExisteCotista(Cpfcnpj))
                {
                    MsgErro = "Cotista com CPFCNPJ " + Cpfcnpj + " já existente";
                    return false;
                }

                //Se Id cotista não foi informado precisamos pegar o MAX de Pessoa
                idCotistaInsert = new Pessoa().GeraIdPessoa();
            }

            ValoresExcelCadastroCotista item = new ValoresExcelCadastroCotista();
            //                                                                                  
            item.IdCotista = idCotistaInsert;
            item.Nome = Nome;
            item.TipoPessoa = (Financial.CRM.Enums.TipoPessoa)Tipo;
            item.Cnpj = Cpfcnpj;
            //
            item.IsentoIR = IsentoIR;
            item.IsentoIOF = IsentoIOF;
            item.StatusCotista = (Financial.InvestidorCotista.Enums.StatusAtivoCotista)StatusAtivo;
            //
            item.TipoTributacaoCotista = (Financial.InvestidorCotista.Enums.TipoTributacaoCotista)TipoTributacao;
            //
            item.TipoCotistaCVM = (Financial.InvestidorCotista.Enums.TipoCotista)TipoCotistaCVM;
            //
            item.CodigoInterface = CodigoInterface;
            //
            item.EstadoCivilCotista = (Financial.CRM.Enums.EstadoCivilPessoa)EstadoCivil;
            item.Rg = NumeroRG;
            item.EmissorRg = EmissorRG;
            //
            item.DataEmissaoRg = DataEmissaoRG;

            //
            item.Sexo = Sexo;
            item.DataNascimento = DataNascimento;
            item.Profissao = Profissao;
            //                                                                              
            ValoresExcelCadastroCotista.Endereco[] endereco = {
                            new ValoresExcelCadastroCotista.Endereco(
                                Endereco,  //Endereco
                                Numero,  //Numero
                                Complemento, //Complemento 
                                Bairro, //Bairro
                                Cidade, //Cidade
                                CEP, //CEP
                                UF, //UF
                                Pais, //Pais,
                                "",//DDD
                                Fone, //Fone,
                                "",//RAMAL
                                Email  //Email
                                ),
                            new ValoresExcelCadastroCotista.Endereco(
                                EnderecoCom, //EnderecoCom
                                NumeroCom, //NumeroCom
                                ComplementoCom, //ComplementoCom
                                BairroCom, //BairroCom
                                CidadeCom, //CidadeCom
                                CEPCom, //CEPCom
                                UFCom, //UFCom
                                PaisCom, //Pais
                                "",//DDD
                                FoneCom, //Fone
                                "",//RAMAL
                                EmailCom  //Email
                                )
                        };

            item.EnderecoPessoa = endereco[0];
            item.EnderecoComercialPessoa = endereco[1];
            item.CriadoPor = Authentication.Username;


            ImportacaoBasePage basePage = new ImportacaoBasePage();
            basePage.valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();
            basePage.valoresExcelCadastroCotista.Add(item);

            basePage.CarregaCadastroCotista();
            return true;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool ExisteCotista(string Cpfcnpj)
        {

            Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(Cpfcnpj);

            return cotista != null;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<HistoricoRentabilidadeMes> ExportaRentabilidadeMes(string Cpfcnpj, DateTime DataInicio, DateTime DataFim, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(Cpfcnpj);

            if (carteira == null)
            {
                MsgErro = "Não foi possível encontrar a carteira com CPFCNPJ: " + Cpfcnpj;
                return null;
            }

            CalculoMedida calculoMedida;
            calculoMedida = new CalculoMedida(carteira.IdCarteira.Value, (int)carteira.IdIndiceBenchmark.Value, DataInicio, DataFim);
            calculoMedida.InitListaRetornoMensal(DataInicio, DataFim);

            DataTable quadroRetorno = calculoMedida.RetornaTabelaRetornoMensal();

            List<HistoricoRentabilidadeMes> rentabilidades = new List<HistoricoRentabilidadeMes>();
            foreach (DataRow row in quadroRetorno.Rows)
            {
                int ano = Convert.ToInt32(row["Ano"]);
                AddRentabilidadeMes(rentabilidades, row, ano, 1, "Janeiro", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 2, "Fevereiro", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 3, "Marco", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 4, "Abril", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 5, "Maio", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 6, "Junho", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 7, "Julho", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 8, "Agosto", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 9, "Setembro", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 10, "Outubro", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 11, "Novembro", Cpfcnpj);
                AddRentabilidadeMes(rentabilidades, row, ano, 12, "Dezembro", Cpfcnpj);
            }

            return rentabilidades;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<HistoricoCotaViewModel> ExportaHistoricoCota(string Cpfcnpj, DateTime? DataInicio, DateTime? DataFim, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            List<HistoricoCotaViewModel> historicoCotasViewModel = new List<HistoricoCotaViewModel>();
            Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(Cpfcnpj);

            if (carteira == null)
            {
                MsgErro = "Não foi possível encontrar a carteira com CPFCNPJ: " + Cpfcnpj;
                return null;
            }


            HistoricoCotaCollection cotas = new HistoricoCotaCollection();

            if (DataInicio.HasValue)
            {
                cotas.Query.Where(cotas.Query.Data.GreaterThanOrEqual(DataInicio.Value));
            }

            if (DataFim.HasValue)
            {
                cotas.Query.Where(cotas.Query.Data.LessThanOrEqual(DataFim.Value));
            }

            cotas.Query.Where(cotas.Query.IdCarteira.Equal(carteira.IdCarteira.Value));

            cotas.Query.OrderBy(cotas.Query.Data.Ascending);
            cotas.Load(cotas.Query);

            foreach (HistoricoCota cota in cotas)
            {
                HistoricoCotaViewModel historicoCotaViewModel = new HistoricoCotaViewModel(cota, carteira);
                historicoCotasViewModel.Add(historicoCotaViewModel);
            }

            return historicoCotasViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<OperacaoCotistaViewModel> ExportaOperacaoCotista(string CpfcnpjCotista, string CpfcnpjCarteira, DateTime? DataInicio, DateTime? DataFim, int? TipoOperacao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();

            if (TipoOperacao.HasValue)
            {
                operacoes.Query.Where(operacoes.Query.TipoOperacao.Equal(TipoOperacao));
            }

            if (!String.IsNullOrEmpty(CpfcnpjCotista))
            {
                Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);
                if (cotista == null)
                {
                    MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                    return null;
                }
                operacoes.Query.Where(operacoes.Query.IdCotista.Equal(cotista.IdCotista.Value));
            }

            if (!string.IsNullOrEmpty(CpfcnpjCarteira))
            {
                Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(CpfcnpjCarteira);
                if (carteira == null)
                {
                    MsgErro = "Não foi possível encontrar a carteira com CPFCNPJ: " + CpfcnpjCarteira;
                    return null;
                }
                operacoes.Query.Where(operacoes.Query.IdCarteira.Equal(carteira.IdCarteira.Value));
            }

            if (DataInicio.HasValue)
            {
                operacoes.Query.Where(operacoes.Query.DataOperacao.GreaterThanOrEqual(DataInicio.Value), operacoes.Query.DataOperacao.LessThanOrEqual(DataFim.Value));
            }

            operacoes.Load(operacoes.Query);

            List<OperacaoCotistaViewModel> listaOperacoes = new List<OperacaoCotistaViewModel>();
            foreach (OperacaoCotista operacao in operacoes)
            {
                listaOperacoes.Add(new OperacaoCotistaViewModel(operacao));
            }

            return listaOperacoes;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public bool ApagaOperacaoCotista(int IdOperacao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            OperacaoCotista operacao = new OperacaoCotista();
            if (!operacao.LoadByPrimaryKey(IdOperacao))
            {
                MsgErro = "Não foi possível encontrar a operação: " + IdOperacao;
                return false;
            }

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(operacao.IdCarteira.Value);

            if (cliente.DataDia.Value.Date != operacao.DataOperacao.Value.Date)
            {
                MsgErro = "Carteira " + cliente.Nome + " está em data diferente da data da operação";
                return false;
            }

            operacao.MarkAsDeleted();
            operacao.Save();
            return true;
        }


        [SoapHeader("Authentication")]
        [WebMethod]
        public List<ImpostoViewModel> ExportaImposto(DateTime DataOperacao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            List<ImpostoViewModel> impostos = new List<ImpostoViewModel>();
            OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();
            operacoes.Query.Where(operacoes.Query.DataConversao.Equal(DataOperacao) &&
                (operacoes.Query.ValorIR.GreaterThan(0) || operacoes.Query.ValorIOF.GreaterThan(0)));
            operacoes.Load(operacoes.Query);

            foreach (OperacaoCotista operacao in operacoes)
            {
                if ((operacao.IdCotista.Value < ID_INICIAL_FUNDOS) || (operacao.IdCarteira.Value < ID_INICIAL_FUNDOS))
                {
                    continue;
                }

                Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
                cotista.LoadByPrimaryKey(operacao.IdCotista.Value);
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(operacao.IdCarteira.Value);

                Pessoa pessoaCarteira = new Pessoa();
                pessoaCarteira.LoadByPrimaryKey(carteira.IdCarteira.Value);
                Pessoa pessoaCotista = new Pessoa();
                pessoaCotista.LoadByPrimaryKey(cotista.IdCotista.Value);

                ImpostoViewModel imposto = new ImpostoViewModel();
                imposto.CodigoInterfaceCotista = cotista.CodigoInterface;
                imposto.CpfcnpjCarteira = pessoaCarteira.Cpfcnpj;
                imposto.CpfcnpjCotista = pessoaCotista.Cpfcnpj;
                imposto.DataLiquidacao = operacao.DataLiquidacao.Value;
                imposto.DataOperacao = operacao.DataConversao.Value;
                imposto.IdCotista = operacao.IdCotista.Value;
                imposto.RendimentoResgate = operacao.RendimentoResgate.Value;
                imposto.ValorBaseCalculo = operacao.RendimentoResgate.Value;
                imposto.TipoCarteiraFV = carteira.TipoCarteira == (byte)TipoCarteiraFundo.RendaFixa ? "F" : "V";

                bool inserirDoisImpostos = false;
                if (operacao.ValorIR > 0 && operacao.ValorIOF > 0)
                {
                    imposto.TipoImposto = "IR";
                    imposto.ValorImposto = operacao.ValorIR.Value;
                    inserirDoisImpostos = true;
                }
                else if (operacao.ValorIR > 0)
                {
                    imposto.TipoImposto = "IR";
                    imposto.ValorImposto = operacao.ValorIR.Value;
                }
                else
                {
                    imposto.TipoImposto = "IOF";
                    imposto.ValorImposto = operacao.ValorIOF.Value;
                }

                impostos.Add(imposto);
                if (inserirDoisImpostos)
                {
                    ImpostoViewModel impostoIOF = (ImpostoViewModel)imposto.Clone();
                    //Precisamos complementar com infos do IOF
                    impostoIOF.TipoImposto = "IOF";
                    impostoIOF.ValorImposto = operacao.ValorIOF.Value;
                    impostos.Add(impostoIOF);
                }

            }

            return impostos;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<ImpostoViewModel2> ExportaImposto2(DateTime DataReferencia, out string MsgErro)
        {            
            MsgErro = "";            
           
            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }            
           
            List<InformeRendimento> informeRendimento = new List<InformeRendimento>();
            List<ImpostoViewModel2> impostos = new List<ImpostoViewModel2>();
            
            ClienteQuery clienteQuery = new ClienteQuery("L");
            clienteQuery.Select(clienteQuery);
            clienteQuery.Where(clienteQuery.IdCliente >= ID_INICIAL_FUNDOS);
            clienteQuery.Where(clienteQuery.StatusAtivo == (Byte) StatusAtivoCliente.Ativo);
            clienteQuery.Where(clienteQuery.IdTipo == (Int64) TipoClienteFixo.Fundo);            
           
            ClienteCollection coll = new ClienteCollection();
            coll.Load(clienteQuery);

            foreach (Cliente cliente in coll)
            {
                informeRendimento.AddRange(new InformeRendimento().RetornaSaldos(cliente.IdCliente, null, DataReferencia));
            }
            if (informeRendimento.Count > 0)
            {

                #region Detalhe
                for (int i = 0; i < informeRendimento.Count; i++)
                {
                    ImpostoViewModel2 imposto = new ImpostoViewModel2();
                    imposto.Ano = informeRendimento[i].Ano;
                    imposto.Tipo = informeRendimento[i].Tipo;
                    imposto.IdFundo =    informeRendimento[i].IdFundo;
                    imposto.CodFundo =   informeRendimento[i].CodFundo;
                    imposto.NomeFundo =   informeRendimento[i].NomeFundo;
                    imposto.CpfcnpjCotista = informeRendimento[i].CpfcnpjCotista;
                    imposto.IdCotista =    informeRendimento[i].IdCotista;
                    imposto.CodigoInterfaceCotista = informeRendimento[i].CodigoInterfaceCotista;
                    imposto.CpfcnpjCarteira = informeRendimento[i].CpfcnpjCarteira;
                    imposto.CodCotista =    informeRendimento[i].CodCotista;
                    imposto.NomeCotista =    informeRendimento[i].NomeCotista;
                    imposto.DataSaldo =    informeRendimento[i].DataSaldo;
                    imposto.QuantidadeCotas =     informeRendimento[i].QuantidadeCotas;
                    imposto.SaldoFinanceiro = informeRendimento[i].SaldoFinanceiro;

                    impostos.Add(imposto);
                }
                #endregion
            }

            return impostos;
           
        }

        /*[SoapHeader("Authentication")]
        [WebMethod]
        public OperacaoCotistaViewModel ImportaOperacaoCotista()
        {
            string CpfcnpjCotista = "29037745873";
            string CpfcnpjCarteira = "01623535000181";
            DateTime DataOperacao = DateTime.Today;
            DateTime? DataConversao = null;
            DateTime? DataLiquidacao = null;
             byte TipoOperacao = 1;
            byte TipoResgate = 0;
            decimal Quantidade = 10;
            decimal CotaOperacao = 1;
            decimal ValorBruto = 100;
            decimal ValorLiquido = 100;
            decimal ValorIR = 0;
            decimal ValorIOF = 0;
            decimal ValorPerformance = 0;
            decimal RendimentoResgate = 0;
            string MsgErro;

            //Importacao pelo site, fazer critica de horario da operacao contra o horario do servidor
            return ImportaOperacaoCotistaPrivate(CpfcnpjCotista, CpfcnpjCarteira, DataOperacao, DataConversao, DataLiquidacao, TipoOperacao, TipoResgate,
                  Quantidade, CotaOperacao, ValorBruto, ValorLiquido, ValorIR, ValorIOF, ValorPerformance, RendimentoResgate, out MsgErro, false);
        }*/

        [SoapHeader("Authentication")]
        [WebMethod]
        public OperacaoCotistaViewModel ImportaOperacaoCotista(string CpfcnpjCotista, string CpfcnpjCarteira, DateTime DataOperacao,
            DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
            decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate, out string MsgErro)
        {
            //Importacao pelo site, fazer critica de horario da operacao contra o horario do servidor
            return ImportaOperacaoCotistaPrivate(CpfcnpjCotista, CpfcnpjCarteira, DataOperacao, DataConversao, DataLiquidacao, TipoOperacao, TipoResgate,
                  Quantidade, CotaOperacao, ValorBruto, ValorLiquido, ValorIR, ValorIOF, ValorPerformance, RendimentoResgate, out MsgErro, false);
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public OperacaoCotistaViewModel ImportaOperacaoCotistaRobo(string CpfcnpjCotista, string CpfcnpjCarteira, DateTime DataOperacao,
            DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
            decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate, out string MsgErro)
        {
            //IMportacao em batch pelo robo. Acatar horario de operacao informada   
            return ImportaOperacaoCotistaPrivate(CpfcnpjCotista, CpfcnpjCarteira, DataOperacao, DataConversao, DataLiquidacao, TipoOperacao, TipoResgate,
                Quantidade, CotaOperacao, ValorBruto, ValorLiquido, ValorIR, ValorIOF, ValorPerformance, RendimentoResgate, out MsgErro, true);
        }

        private OperacaoCotistaViewModel ImportaOperacaoCotistaPrivate(string CpfcnpjCotista, string CpfcnpjCarteira, DateTime DataOperacao,
            DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
            decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate, out string MsgErro, bool ignoraHorarioLimite)
        {

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);

            if (cotista == null)
            {
                MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                return null;
            }

            int IdCotista = cotista.IdCotista.Value;
           
            Carteira carteira = this.BuscaCarteiraFundoPorCNPJ( CpfcnpjCarteira);

            if (carteira == null)
            {
                MsgErro = "Não foi possível encontrar a carteira com CPFCNPJ: " + CpfcnpjCarteira;
                return null;
            }

            int IdCarteira = carteira.IdCarteira.Value;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(carteira.IdCarteira.Value);

            bool dataOperacaoPosteriorDataImplantacao = DataOperacao.Date > cliente.DataImplantacao;

            /*HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            String.Format("DataOperacao: {0} - DataImplantacao: {1}", DataOperacao.Date, cliente.DataImplantacao),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.IntegracaoItau);*/

            if (dataOperacaoPosteriorDataImplantacao)
            {
                //Validar saldo para resgate apenas se data implantacao
                if (TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                    TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                    TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                    TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                {

                    if (carteira.LiberaResgate == "N")
                    {
                        MsgErro = "Favor entrar em contato com a nossa Central de Atendimento Itau Coretora nos telefones: 0800-7223131 ou 011 - 4004-3005, para as informações necessárias para os procedimentos do resgate";
                        return null;
                    }

                    PosicaoCotista posicaoCotistaLivre = new PosicaoCotista().RetornaPosicaoLivre(IdCarteira, IdCotista, cliente.DataDia.Value, null);
                    if (ValorBruto > posicaoCotistaLivre.ValorBruto.Value)
                    {
                        MsgErro = "Saldo insuficiente para operação";
                        return null;
                    }
                }


                #region Avalia valores e saldo mínimo
                if (TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {

                    /*historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    String.Format("Aplicacao: ValorMinimoInicial carteira: {0} - ValorMinimoAplicacao: {1}", carteira.ValorMinimoInicial.Value, carteira.ValorMinimoAplicacao.Value),
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.IntegracaoItau);*/

                    if (carteira.LiberaAplicacao == "N")
                    {
                        MsgErro = "Favor entrar em contato com a nossa Central de Atendimento Itau Coretora nos telefones: 0800-7223131 ou 011 - 4004-3005, para as informações necessárias para os procedimentos de Aplicação.";
                        return null;
                    }


                    if (carteira.ValorMinimoInicial.Value != 0 || carteira.ValorMinimoAplicacao.Value != 0)
                    {
                        decimal quantidadeCotas = 0;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        posicaoCotista.Query.Select(posicaoCotista.Query.Quantidade.Sum());
                        posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(IdCarteira),
                                                   posicaoCotista.Query.IdCotista.Equal(IdCotista));
                        posicaoCotista.Query.Load();

                        if (posicaoCotista.Quantidade.HasValue)
                        {
                            quantidadeCotas = posicaoCotista.Quantidade.Value;
                        }

                        /*historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        String.Format("Aplicacao: QuantidadeCotas: {0} - ValorBruto: {1}", quantidadeCotas, ValorBruto),
                                                        HttpContext.Current.User.Identity.Name,
                                                        "",
                                                        "",
                                                        HistoricoLogOrigem.IntegracaoItau);*/


                        if (quantidadeCotas == 0 && ValorBruto < carteira.ValorMinimoInicial.Value)
                        {
                            MsgErro = "Operação com valor inferior ao valor mínimo inicial (" + carteira.ValorMinimoInicial.Value.ToString() + ") . Operação não pode ser realizada";
                            return null;
                        }
                        else if (quantidadeCotas != 0 && ValorBruto < carteira.ValorMinimoAplicacao.Value)
                        {
                            MsgErro = "Operação com valor inferior ao valor de aplicação exigido (" + carteira.ValorMinimoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                            return null;
                        }
                    }
                }
                else if (TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                         TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                {
                    if (carteira.ValorMinimoResgate.Value != 0 || carteira.ValorMinimoSaldo.Value != 0)
                    {
                        decimal saldoLiquido = 0;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        posicaoCotista.Query.Select(posicaoCotista.Query.ValorLiquido.Sum());
                        posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(IdCarteira),
                                                   posicaoCotista.Query.IdCotista.Equal(IdCotista));
                        posicaoCotista.Query.Load();

                        if (posicaoCotista.ValorLiquido.HasValue)
                        {
                            saldoLiquido = posicaoCotista.ValorLiquido.Value;
                        }

                        decimal saldoProjetado = saldoLiquido - ValorBruto;
                        if (saldoProjetado < carteira.ValorMinimoSaldo.Value)
                        {
                            MsgErro = "Saldo projetado com valor inferior ao valor mínimo requerido (" + carteira.ValorMinimoSaldo.Value.ToString() + ") . Operação não pode ser realizada";
                            return null;
                        }
                        else if (ValorBruto < carteira.ValorMinimoResgate.Value)
                        {
                            MsgErro = "Operação com valor inferior ao valor de resgate exigido (" + carteira.ValorMinimoResgate.Value.ToString() + ") . Operação não pode ser realizada";
                            return null;
                        }
                    }
                }
                #endregion

            }

            ValoresExcelOperacaoCotista item = new ValoresExcelOperacaoCotista();
            item.IdCotista = IdCotista;
            item.IdCarteira = IdCarteira;

            item.DataAgendamento = DataOperacao; //Vamos setar DataAgendamento antes de remover eventual TIME-PART da operação - esta linha tem que preceder a próxima

            bool isAplicacao = TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;

            DateTime? horarioLimite = null;
            if (isAplicacao && carteira.HorarioFim.HasValue)
            {
                horarioLimite = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                         carteira.HorarioFim.Value.Hour, carteira.HorarioFim.Value.Minute, 0);
            }
            else if (!isAplicacao && carteira.HorarioFimResgate.HasValue)
            {
                horarioLimite = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                         carteira.HorarioFimResgate.Value.Hour, carteira.HorarioFimResgate.Value.Minute, 0);
            }

            bool foraHorario = false;

            if (!ignoraHorarioLimite && horarioLimite.HasValue && DateTime.Now > horarioLimite)
            {
                foraHorario = true;
            }

            if (!Calendario.IsDiaUtil(DataOperacao.Date) || foraHorario)
            {
                DataOperacao = Calendario.AdicionaDiaUtil(DataOperacao, 1);
            }
            item.DataOperacao = DataOperacao.Date;

            if (DataConversao.HasValue)
            {
                if (!Calendario.IsDiaUtil(DataConversao.Value.Date) || foraHorario)
                {
                    DataConversao = Calendario.AdicionaDiaUtil(DataConversao.Value, 1);
                }

                item.DataConversao = DataConversao.Value.Date;
            }

            if (DataLiquidacao.HasValue)
            {
                if (!Calendario.IsDiaUtil(DataLiquidacao.Value.Date) || foraHorario)
                {
                    DataLiquidacao = Calendario.AdicionaDiaUtil(DataLiquidacao.Value, 1);
                }
                item.DataLiquidacao = DataLiquidacao.Value.Date;
            }

            item.TipoOperacao = TipoOperacao;
            item.TipoResgate = TipoResgate;
            item.Quantidade = Quantidade;
            item.CotaOperacao = CotaOperacao;
            item.ValorBruto = ValorBruto;
            item.ValorLiquido = ValorLiquido;
            item.ValorIR = ValorIR;
            item.ValorIOF = ValorIOF;
            item.ValorPerformance = ValorPerformance;
            item.RendimentoResgate = RendimentoResgate;

            ImportacaoBasePage basePage = new ImportacaoBasePage();
            basePage.valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();
            basePage.valoresExcelOperacaoCotista.Add(item);

            OperacaoCotistaCollection operacoes = basePage.CarregaOperacaoCotista(false);

            OperacaoCotistaViewModel operacaoCotistaViewModel = null;
            if (operacoes.Count > 0)
            {
                OperacaoCotista operacaoCotista = operacoes[0];
                operacaoCotistaViewModel = new OperacaoCotistaViewModel(operacaoCotista);
            }
            return operacaoCotistaViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]
        public List<PosicaoCotistaViewModel> ExportaPosicaoCotista(int? IdPosicao, string CpfcnpjCotista, string CpfcnpjCarteira, out string MsgErro)
        {

            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
            if (IdPosicao.HasValue)
            {
                posicoes.Query.Where(posicoes.Query.IdPosicao.Equal(IdPosicao.Value));
            }

            if (!String.IsNullOrEmpty(CpfcnpjCotista))
            {
                Financial.InvestidorCotista.Cotista cotista = this.BuscaCotistaFundoPorCPFNPJ(CpfcnpjCotista);
                if (cotista == null)
                {
                    MsgErro = "Não foi possível encontrar o cotista com CPFCNPJ: " + CpfcnpjCotista;
                    return null;
                }

                posicoes.Query.Where(posicoes.Query.IdCotista.Equal(cotista.IdCotista.Value));
            }

            if (!string.IsNullOrEmpty(CpfcnpjCarteira))
            {
                Carteira carteira = this.BuscaCarteiraFundoPorCNPJ(CpfcnpjCarteira);

                if (carteira == null)
                {
                    MsgErro = "Não foi possível encontrar a carteira com CPFCNPJ: " + CpfcnpjCarteira;
                    return null;
                }

                posicoes.Query.Where(posicoes.Query.IdCarteira.Equal(carteira.IdCarteira.Value));
            }

            //posicoes.Query.Where(posicoes.Query.Quantidade.NotEqual(0));

            posicoes.Load(posicoes.Query);

            List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();

            Dictionary<string, PosicaoCotista> distinctCotistaCarteira = new Dictionary<string, PosicaoCotista>();
            foreach (PosicaoCotista posicaoCotista in posicoes)
            {
                string keyCotistaCarteira = String.Format("{0}|{1}", posicaoCotista.IdCotista, posicaoCotista.IdCarteira);
                if (!distinctCotistaCarteira.ContainsKey(keyCotistaCarteira))
                {
                    distinctCotistaCarteira.Add(keyCotistaCarteira, posicaoCotista);
                }
            }

            foreach (KeyValuePair<string, PosicaoCotista> posicaoDistinct in distinctCotistaCarteira)
            {
                string[] splittedKey = posicaoDistinct.Key.Split('|');
                int idCotista = Convert.ToInt32(splittedKey[0]);
                int idCarteira = Convert.ToInt32(splittedKey[1]);

                //Descobrir data mais recente da abertura
                DateTime dataHistorico = DateTime.MinValue;
                PosicaoCotistaAbertura posicaoMaisRecente = new PosicaoCotistaAbertura();
                posicaoMaisRecente.Query.Select(posicaoMaisRecente.Query.DataHistorico.Max());
                posicaoMaisRecente.Query.Where(posicaoMaisRecente.Query.IdCarteira.Equal(idCarteira),
                        posicaoMaisRecente.Query.IdCotista.Equal(idCotista));

                if (posicaoMaisRecente.Query.Load())
                {
                    if (posicaoMaisRecente.DataHistorico.HasValue)
                    {
                        dataHistorico = posicaoMaisRecente.DataHistorico.Value;
                    }
                }

                Carteira carteiraFundo = new Carteira();
                carteiraFundo.LoadByPrimaryKey(idCarteira);
                if (carteiraFundo.StatusAtivo == (int)StatusAtivoCarteira.Ativo)
                {

                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    String.Format("Carteira: {0} Cotista: {1} - DataHistorico: {2}", idCarteira, idCotista, dataHistorico),
                                                    HttpContext.Current.User.Identity.Name,
                                                    "",
                                                    "",
                                                    HistoricoLogOrigem.IntegracaoItau);

                    PosicaoCotista posicaoLivre = new PosicaoCotista().RetornaPosicaoLivre(idCarteira, idCotista, dataHistorico, null);
                    posicoesViewModel.Add(new PosicaoCotistaViewModel(posicaoLivre));

                }
            }

            return posicoesViewModel;
        }

        [SoapHeader("Authentication")]
        [WebMethod]

        public List<PosicaoCotistaViewModel> ExportaPosicaoCotistaPorData(DateTime DataPosicao, out string MsgErro)
        {
            MsgErro = "";

            if (Authentication == null)
            {
                string msgErro = "Credenciais de autenticação não foram informadas";
                throw new Exception(msgErro);
            }
            else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
            {
                string msgErro = "Usuário não autenticado";
                throw new Exception(msgErro);
            }

            PosicaoCotistaHistoricoCollection posicoes = new PosicaoCotistaHistoricoCollection();
            posicoes.Query.Where(posicoes.Query.DataHistorico.Equal(DataPosicao));
            posicoes.Query.Where(posicoes.Query.Quantidade.NotEqual(0));
            posicoes.Query.Where(posicoes.Query.IdCarteira.GreaterThanOrEqual(ID_INICIAL_FUNDOS));
            posicoes.Query.Where(posicoes.Query.IdCotista.GreaterThanOrEqual(ID_INICIAL_FUNDOS));


            posicoes.Load(posicoes.Query);

            List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();

            Dictionary<string, PosicaoCotistaHistorico> distinctCotistaCarteira = new Dictionary<string, PosicaoCotistaHistorico>();
            foreach (PosicaoCotistaHistorico posicaoCotista in posicoes)
            {
                string keyCotistaCarteira = String.Format("{0}|{1}", posicaoCotista.IdCotista, posicaoCotista.IdCarteira);
                if (!distinctCotistaCarteira.ContainsKey(keyCotistaCarteira))
                {
                    distinctCotistaCarteira.Add(keyCotistaCarteira, posicaoCotista);
                }
            }

            foreach (KeyValuePair<string, PosicaoCotistaHistorico> posicaoDistinct in distinctCotistaCarteira)
            {
                string[] splittedKey = posicaoDistinct.Key.Split('|');
                int idCotista = Convert.ToInt32(splittedKey[0]);
                int idCarteira = Convert.ToInt32(splittedKey[1]);

                Carteira carteiraFundo = new Carteira();
                carteiraFundo.LoadByPrimaryKey(idCarteira);
                if (carteiraFundo.StatusAtivo == (int)StatusAtivoCarteira.Ativo)
                {

                    PosicaoCotista posicaoLivre = new PosicaoCotista().RetornaPosicaoLivre(idCarteira, idCotista, DataPosicao, null);
                    posicoesViewModel.Add(new PosicaoCotistaViewModel(posicaoLivre));
                }

            }

            return posicoesViewModel;
        }

        public class ValidateLogin : SoapHeader
        {
            public string Username;
            public string Password;
        }

        private bool CheckAuthentication(string username, string password)
        {
            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            NameValueCollection listaValores = new NameValueCollection();
            listaValores.Add("passwordFormat", "Encrypted");
            financialMembershipProvider.Initialize(null, listaValores);

            bool autenticado = financialMembershipProvider.ValidateUser(username, password);
            return autenticado;
        }

        public class HistoricoRentabilidadeMes
        {
            public DateTime Data;
            public string Cpfcnpj;
            public decimal Rentabilidade;
            public decimal RentabilidadeIndice;
            public HistoricoRentabilidadeMes()
            {
            }
        }

        public class PosicaoCotistaViewModel
        {
            public int? IdPosicao;
            public int? IdOperacao;
            public int? IdCotista;
            public int? IdCarteira;
            public string CpfcnpjCarteira;
            public string CpfcnpjCotista;
            public decimal? ValorAplicacao;
            public DateTime? DataAplicacao;
            public DateTime? DataConversao;
            public decimal? CotaAplicacao;
            public decimal? CotaDia;
            public decimal? ValorBruto;
            public decimal? ValorLiquido;
            public decimal? QuantidadeInicial;
            public decimal? Quantidade;
            public decimal? QuantidadeBloqueada;
            public DateTime? DataUltimaCobrancaIR;
            public decimal? ValorIR;
            public decimal? ValorIOF;
            public decimal? ValorPerformance;
            public decimal? ValorIOFVirtual;
            public decimal? QuantidadeAntesCortes;
            public decimal? ValorRendimento;
            public DateTime? DataUltimoCortePfee;
            public string PosicaoIncorporada;
            public string NomeCarteira;
            public decimal? ValorIRMaio;
            public decimal? ValorIRNovembro;
            public string CodigoInterfaceCotista;

            public PosicaoCotistaViewModel()
            {
            }
            public PosicaoCotistaViewModel(PosicaoCotista posicaoCotista)
            {
                this.CotaAplicacao = posicaoCotista.CotaAplicacao;
                this.CotaDia = posicaoCotista.CotaDia;
                this.DataAplicacao = posicaoCotista.DataAplicacao;
                this.DataConversao = posicaoCotista.DataConversao;
                this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
                this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                this.IdCarteira = posicaoCotista.IdCarteira;
                this.IdCotista = posicaoCotista.IdCotista;
                this.IdOperacao = posicaoCotista.IdOperacao;
                this.IdPosicao = posicaoCotista.IdPosicao;
                this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
                this.Quantidade = posicaoCotista.Quantidade;
                this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
                this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
                this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
                this.ValorAplicacao = posicaoCotista.ValorAplicacao;
                this.ValorBruto = posicaoCotista.ValorBruto;
                this.ValorIOF = posicaoCotista.ValorIOF;
                this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
                this.ValorIR = posicaoCotista.ValorIR;
                this.ValorLiquido = posicaoCotista.ValorLiquido;
                this.ValorPerformance = posicaoCotista.ValorPerformance;
                this.ValorRendimento = posicaoCotista.ValorRendimento;

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(this.IdCarteira.Value);
                this.NomeCarteira = carteira.Nome;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
                this.CpfcnpjCarteira = pessoa.Cpfcnpj;

                pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCotista.Value);
                this.CpfcnpjCotista = pessoa.Cpfcnpj;

                Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
                cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);
                this.CodigoInterfaceCotista = cotista.CodigoInterface;

                this.ValorIRMaio = 0;
                this.ValorIRNovembro = 0;
            }

            public PosicaoCotistaViewModel(PosicaoCotistaHistorico posicaoCotista)
            {
                this.CotaAplicacao = posicaoCotista.CotaAplicacao;
                this.CotaDia = posicaoCotista.CotaDia;
                this.DataAplicacao = posicaoCotista.DataAplicacao;
                this.DataConversao = posicaoCotista.DataConversao;
                this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
                this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                this.IdCarteira = posicaoCotista.IdCarteira;
                this.IdCotista = posicaoCotista.IdCotista;
                this.IdOperacao = posicaoCotista.IdOperacao;
                this.IdPosicao = posicaoCotista.IdPosicao;
                this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
                this.Quantidade = posicaoCotista.Quantidade;
                this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
                this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
                this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
                this.ValorAplicacao = posicaoCotista.ValorAplicacao;
                this.ValorBruto = posicaoCotista.ValorBruto;
                this.ValorIOF = posicaoCotista.ValorIOF;
                this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
                this.ValorIR = posicaoCotista.ValorIR;
                this.ValorLiquido = posicaoCotista.ValorLiquido;
                this.ValorPerformance = posicaoCotista.ValorPerformance;
                this.ValorRendimento = posicaoCotista.ValorRendimento;

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(this.IdCarteira.Value);
                this.NomeCarteira = carteira.Nome;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
                this.CpfcnpjCarteira = pessoa.Cpfcnpj;

                pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(this.IdCotista.Value);
                this.CpfcnpjCotista = pessoa.Cpfcnpj;

                Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
                cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);
                this.CodigoInterfaceCotista = cotista.CodigoInterface;

                this.ValorIRMaio = 0;
                this.ValorIRNovembro = 0;
            }
        }

        public class CarteiraViewModel
        {
            public string Nome;
            public string Cpfcnpj;
            public int IdCarteira;
            public string Estrategia;
            public decimal ValorMinimoAplicacao;
            public byte StatusAtivo;

            public string Categoria;
            public string GrauRisco;
            public string AgenteGestor;
            public string Objetivo;
            public string AgenteAdministrador;

            public string EnderecoAgenteAdministrador;
            public string CNPJAgenteAdministrador;
            public string CEPAgenteAdministrador;
            public string CidadeAgenteAdministrador;
            public string EstadoAgenteAdministrador;

            public DateTime DataInicioCota;
            public decimal PLMedio12Meses;

            public decimal ValorUltimaCota;
            public decimal PLMaisRecente;
            public string Benchmark;

            public byte TipoTributacao;

            public decimal? RetornoMes;
            public decimal? RetornoUltimoMes;
            public decimal? RetornoAno;
            public decimal? Retorno12Meses;
            public decimal? Retorno24Meses;
            public decimal? Retorno36Meses;
            public decimal ValorMinimoInicial;
            public decimal ValorMinimoResgate;
            public decimal ValorMinimoSaldo;
            public string TaxaAdministracao;
            public string TaxaPerformance;
            public int TipoCota;
            public int DiasCotizacaoAplicacao;
            public int DiasCotizacaoResgate;
            public int DiasLiquidacaoAplicacao;
            public int DiasLiquidacaoResgate;
            public DateTime? HorarioFim;
            public DateTime? HorarioLimiteCotizacao;

            public CarteiraViewModel()
            {
            }
            public CarteiraViewModel(Carteira carteira, bool calculaPLMedio)
            {

                this.Nome = carteira.Nome;
                this.StatusAtivo = carteira.StatusAtivo.Value;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(carteira.IdCarteira.Value);
                this.Cpfcnpj = pessoa.Cpfcnpj;

                this.IdCarteira = carteira.IdCarteira.Value;

                if (carteira.IdGrauRisco.HasValue)
                {
                    GrauRisco grauRisco = new GrauRisco();
                    grauRisco.LoadByPrimaryKey(carteira.IdGrauRisco.Value);
                    this.GrauRisco = grauRisco.Descricao;
                }

                AgenteMercado agenteGestor = new AgenteMercado();
                agenteGestor.LoadByPrimaryKey(carteira.IdAgenteGestor.Value);
                this.AgenteGestor = agenteGestor.Nome;

                this.Objetivo = carteira.Objetivo;
                AgenteMercado agenteAdministrador = new AgenteMercado();
                agenteAdministrador.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
                this.AgenteAdministrador = agenteAdministrador.Nome;
                this.CNPJAgenteAdministrador = agenteAdministrador.Cnpj;
                this.EnderecoAgenteAdministrador = agenteAdministrador.Endereco;
                this.CEPAgenteAdministrador = agenteAdministrador.Cep;
                this.CidadeAgenteAdministrador = agenteAdministrador.Cidade;
                this.EstadoAgenteAdministrador = agenteAdministrador.Uf;

                this.DataInicioCota = carteira.DataInicioCota.Value;

                CategoriaFundo categoriaFundo = new CategoriaFundo();
                categoriaFundo.LoadByPrimaryKey(carteira.IdCategoria.Value);
                this.Categoria = categoriaFundo.Descricao;

                //Patrimonio mais recente
                HistoricoCota historicoCotaMax = new HistoricoCota();
                DateTime? dataMaxCota = historicoCotaMax.BuscaDataMaxCota(carteira.IdCarteira.Value);
                DateTime dataFimCalculoMedida;
                if (dataMaxCota.HasValue)
                {
                    historicoCotaMax = new HistoricoCota();
                    historicoCotaMax.LoadByPrimaryKey(dataMaxCota.Value, carteira.IdCarteira.Value);
                    if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                    {
                        this.ValorUltimaCota = historicoCotaMax.CotaAbertura.Value;
                        this.PLMaisRecente = historicoCotaMax.PLAbertura.Value;
                    }
                    else
                    {
                        this.ValorUltimaCota = historicoCotaMax.CotaFechamento.Value;
                        this.PLMaisRecente = historicoCotaMax.PLFechamento.Value;
                    }
                    dataFimCalculoMedida = dataMaxCota.Value;
                }
                else
                {
                    dataFimCalculoMedida = DateTime.Today;
                }

                CalculoMedida calculoMedida = new CalculoMedida(carteira.IdCarteira.Value, carteira.IdIndiceBenchmark.Value, carteira.DataInicioCota.Value, dataFimCalculoMedida);
                calculoMedida.SetAjustaCota(true);

                if (carteira.IdEstrategia.HasValue)
                {
                    Estrategia estrategia = new Estrategia();
                    estrategia.LoadByPrimaryKey(carteira.IdEstrategia.Value);
                    this.Estrategia = estrategia.Descricao;
                }

                if (calculaPLMedio)
                {
                    try
                    {
                        this.PLMedio12Meses = calculoMedida.CalculaPLMedio12MesesInMemory(calculoMedida.DataFimJanela.Value);
                    }
                    catch { };
                }

                Indice indiceBenchmark = new Indice();
                indiceBenchmark.LoadByPrimaryKey(carteira.IdIndiceBenchmark.Value);
                this.Benchmark = indiceBenchmark.Descricao;

                this.TipoTributacao = (byte)carteira.TipoTributacao;

                try
                {
                    this.RetornoMes = calculoMedida.CalculaRetornoMes(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                try
                {
                    this.RetornoUltimoMes = calculoMedida.CalculaRetornoMesFechado(DateTime.Today.AddMonths(-1));
                }
                catch { }

                try
                {
                    this.RetornoAno = calculoMedida.CalculaRetornoAno(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                try
                {
                    this.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 12);
                }
                catch { }

                try
                {
                    this.Retorno24Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 24);
                }
                catch { }

                try { this.Retorno36Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 36); }
                catch { }

                this.ValorMinimoAplicacao = carteira.ValorMinimoAplicacao.Value;
                this.ValorMinimoInicial = carteira.ValorMinimoInicial.Value;
                this.ValorMinimoResgate = carteira.ValorMinimoResgate.Value;
                this.ValorMinimoSaldo = carteira.ValorMinimoSaldo.Value;
                this.TipoCota = carteira.TipoCota.Value;
                this.ValorMinimoSaldo = carteira.ValorMinimoSaldo.Value;
                this.DiasCotizacaoAplicacao = carteira.DiasCotizacaoAplicacao.Value;
                this.DiasCotizacaoResgate = carteira.DiasCotizacaoResgate.Value;
                this.DiasLiquidacaoAplicacao = carteira.DiasLiquidacaoAplicacao.Value;
                this.DiasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
                if (carteira.HorarioFim.HasValue)
                {
                    this.HorarioFim = carteira.HorarioFim.Value;
                }

                if (carteira.HorarioFimResgate.HasValue)
                {
                    this.HorarioLimiteCotizacao = carteira.HorarioFimResgate.Value; //Temos que manter a propriedade HorarioLimiteCotizacao por conta do Itau que nao vai ser reescrito - nao tinhamos HorarioFimResgate na epoca do dev
                }

                decimal? taxaAdministracaoNum;

                TabelaTaxaAdministracaoCollection taxas = new TabelaTaxaAdministracaoCollection();
                taxas.BuscaTabelaTaxaAdministracao(carteira.IdCarteira.Value);
                if (taxas.Count >= 1)
                {
                    taxaAdministracaoNum = taxas[0].Taxa;
                }
                else
                {
                    taxaAdministracaoNum = null;
                }
                this.TaxaPerformance = "";


                if (taxaAdministracaoNum.HasValue)
                {
                    string percentFormat = Financial.Util.StringEnum.GetStringValue(Financial.Relatorio.ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                    this.TaxaAdministracao = (taxaAdministracaoNum / 100).Value.ToString(percentFormat);
                }

            }
        }

        public class ResgateCotistaViewModel
        {
            public string CpfcnpjCotista;
            public string CpfcnpjCarteira;
            public decimal ValorBruto;
            public decimal ValorLiquido;
            public decimal Quantidade;
            public decimal CotaOperacao;
            public decimal ValorIR;
            public decimal ValorIOF;
            public decimal Ganho;
            public string NomeCarteira;

            public ResgateCotistaViewModel()
            {

            }
        }

        public class HistoricoCotaViewModel
        {
            public int IdCarteira;
            public string Cpfcnpj;
            public string Nome;
            public DateTime Data;
            public decimal Cota;

            public HistoricoCotaViewModel()
            {
            }

            public HistoricoCotaViewModel(HistoricoCota historicoCota, Carteira carteira)
            {
                this.IdCarteira = historicoCota.IdCarteira.Value;
                this.Nome = carteira.Nome;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(historicoCota.IdCarteira.Value);
                this.Cpfcnpj = pessoa.Cpfcnpj;

                this.Data = historicoCota.Data.Value;
                if (carteira.TipoCota == (byte)TipoCotaFundo.Abertura)
                {
                    this.Cota = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    this.Cota = historicoCota.CotaFechamento.Value;
                }
            }
        }

        public class OperacaoCotistaViewModel
        {
            public int IdOperacao;
            public int IdCotista;
            public int IdCarteira;
            public DateTime DataOperacao;
            public DateTime DataConversao;
            public DateTime DataLiquidacao;
            public DateTime DataAgendamento;
            public byte TipoOperacao;
            public byte? TipoResgate;
            public int? IdPosicaoResgatada;
            public byte IdFormaLiquidacao;
            public decimal Quantidade;
            public decimal CotaOperacao;
            public decimal ValorBruto;
            public decimal ValorLiquido;
            public decimal ValorIR;
            public decimal ValorIOF;
            public decimal ValorCPMF;
            public decimal ValorPerformance;
            public decimal PrejuizoUsado;
            public decimal RendimentoResgate;
            public decimal VariacaoResgate;
            public string Observacao;
            public string DadosBancarios;
            public string CpfcnpjCarteira;
            public string CpfcnpjCotista;
            public byte Fonte;
            public int? IdConta;
            public decimal? CotaInformada;
            public int? IdAgenda;
            public int? IdOperacaoResgatada;

            public OperacaoCotistaViewModel()
            {
            }
            public OperacaoCotistaViewModel(OperacaoCotista operacaoCotista)
            {
                Pessoa pessoaCarteira = new Pessoa();
                pessoaCarteira.LoadByPrimaryKey(operacaoCotista.IdCarteira.Value);
                this.CpfcnpjCarteira = pessoaCarteira.Cpfcnpj;

                Pessoa pessoaCotista = new Pessoa();
                pessoaCotista.LoadByPrimaryKey(operacaoCotista.IdCotista.Value);
                this.CpfcnpjCotista = pessoaCotista.Cpfcnpj;

                this.CotaInformada = operacaoCotista.CotaInformada;
                this.CotaOperacao = operacaoCotista.CotaOperacao.Value;
                this.DadosBancarios = operacaoCotista.DadosBancarios;
                this.DataAgendamento = operacaoCotista.DataAgendamento.Value;
                this.DataConversao = operacaoCotista.DataConversao.Value;
                this.DataLiquidacao = operacaoCotista.DataLiquidacao.Value;
                this.DataOperacao = operacaoCotista.DataOperacao.Value;
                this.Fonte = operacaoCotista.Fonte.Value;
                this.IdAgenda = operacaoCotista.IdAgenda;
                this.IdCarteira = operacaoCotista.IdCarteira.Value;
                this.IdConta = operacaoCotista.IdConta;
                this.IdCotista = operacaoCotista.IdCotista.Value;
                this.IdFormaLiquidacao = operacaoCotista.IdFormaLiquidacao.Value;
                this.IdOperacao = operacaoCotista.IdOperacao.Value;
                this.IdOperacaoResgatada = operacaoCotista.IdOperacaoResgatada;
                this.IdPosicaoResgatada = operacaoCotista.IdPosicaoResgatada;
                this.Observacao = operacaoCotista.Observacao;
                this.PrejuizoUsado = operacaoCotista.PrejuizoUsado.Value;
                this.Quantidade = operacaoCotista.Quantidade.Value;
                this.RendimentoResgate = operacaoCotista.RendimentoResgate.Value;
                this.TipoOperacao = operacaoCotista.TipoOperacao.Value;
                this.ValorBruto = operacaoCotista.ValorBruto.Value;
                this.ValorCPMF = operacaoCotista.ValorCPMF.Value;
                this.ValorIOF = operacaoCotista.ValorIOF.Value;
                this.ValorIR = operacaoCotista.ValorIR.Value;
                this.ValorLiquido = operacaoCotista.ValorLiquido.Value;
                this.ValorPerformance = operacaoCotista.ValorPerformance.Value;
                this.VariacaoResgate = operacaoCotista.VariacaoResgate.Value;
            }
        }

        public class ImpostoViewModel : ICloneable
        {
            public string CpfcnpjCotista;
            public int IdCotista;
            public string CodigoInterfaceCotista;
            public string CpfcnpjCarteira;
            public string TipoCarteiraFV;
            public decimal RendimentoResgate;
            public decimal ValorBaseCalculo;
            public string TipoImposto;
            public decimal ValorImposto;
            public DateTime DataOperacao;
            public DateTime DataLiquidacao;
            public object Clone() { return this.MemberwiseClone(); }
        }

        public class ImpostoViewModel2 
        {
            public int      Ano;
            public string   Tipo;
            public int      IdFundo;
            public string   CodFundo;
            public string   NomeFundo;
            public string   CpfcnpjCotista;
            public int      IdCotista;
            public string   CodigoInterfaceCotista;
            public string   CpfcnpjCarteira;
            public string   CodCotista;
            public string   NomeCotista;
            public DateTime DataSaldo;
            public decimal  QuantidadeCotas;
            public decimal  SaldoFinanceiro;           
        }
        

        public class CotistaViewModel
        {
            public string Cpfcnpj;
            public int IdCotista;
            public string Nome;
            public string Apelido;
            public string IsentoIR;
            public string IsentoIOF;
            public byte StatusAtivo;
            public int? TipoCotistaCVM;
            public string CodigoInterface;
            public byte TipoTributacao;
            public DateTime? DataExpiracao;
            public string PendenciaCadastral;

            public CotistaViewModel()
            {
            }

            public CotistaViewModel(Financial.InvestidorCotista.Cotista cotista)
            {
                this.Apelido = cotista.Apelido;
                this.CodigoInterface = cotista.CodigoInterface;
                this.DataExpiracao = cotista.DataExpiracao;
                this.IdCotista = cotista.IdCotista.Value;
                this.IsentoIOF = cotista.IsentoIOF;
                this.IsentoIR = cotista.IsentoIR;
                this.Nome = cotista.Nome;
                this.PendenciaCadastral = cotista.PendenciaCadastral;
                this.StatusAtivo = cotista.StatusAtivo.Value;
                this.TipoCotistaCVM = cotista.TipoCotistaCVM;
                this.TipoTributacao = cotista.TipoTributacao.Value;
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(cotista.IdCotista.Value);
                this.Cpfcnpj = pessoa.Cpfcnpj;
            }
        }


    }
}
