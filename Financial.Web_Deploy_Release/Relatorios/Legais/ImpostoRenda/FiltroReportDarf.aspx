﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportDarf, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('textNome'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Darf" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                   
                                        
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>  
            </td>
            </tr>
                        
            <tr>                            
            <td class="td_Label">
                <asp:Label ID="labelMes" runat="server" CssClass="labelRequired" Text="Mês:"/>
            </td>
            
            <td>
                <dxe:ASPxComboBox ID="dropMes" runat="server" ClientInstanceName="dropMes"
                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                   DropDownRows ="13" Height="5px">
                <Items>
                    <dxe:ListEditItem Text="Janeiro" Value="1" Selected="true" />
                    <dxe:ListEditItem Text="Fevereiro" Value="2" />
                    <dxe:ListEditItem Text="Março" Value="3" />
                    <dxe:ListEditItem Text="Abril" Value="4" />
                    <dxe:ListEditItem Text="Maio" Value="5" />
                    <dxe:ListEditItem Text="Junho" Value="6" />
                    <dxe:ListEditItem Text="Julho" Value="7" />
                    <dxe:ListEditItem Text="Agosto" Value="8" />
                    <dxe:ListEditItem Text="Setembro" Value="9" />
                    <dxe:ListEditItem Text="Outubro" Value="10" />
                    <dxe:ListEditItem Text="Novembro" Value="11" />
                    <dxe:ListEditItem Text="Dezembro" Value="12" />

                </Items>                                                               
                </dxe:ASPxComboBox>                                    
            </td>            
            
            <td class="td_Label">
                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Ano:"/>
            </td>
            <td>                
                <dxe:ASPxDateEdit ID="textData_yyyy" runat="server" ClientInstanceName="textData_yyyy" EditFormat="Custom" EditFormatString="yyyy" />
            </td>
            </tr>
            
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>           
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
   </form>
</body>
</html>