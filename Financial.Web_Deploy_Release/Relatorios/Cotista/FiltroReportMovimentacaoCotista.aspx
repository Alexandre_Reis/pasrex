﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportMovimentacaoCotista, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function OnGetDataCarteira(values) {
    btnEditCodigoCarteira.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());        
    btnEditCodigoCarteira.Focus();    
}

function OnGetDataCotista(values) {
    btnEditCodigoCotista.SetValue(values);                    
    popupCotista.HideWindow();
    ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
    btnEditCodigoCotista.Focus();    
}
</script>

<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, 
                                      btnEditCodigoCarteira, document.getElementById('textNomeCarteira'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, 
                                      btnEditCodigoCotista, document.getElementById('textNomeCotista'));
        }        
        "/>
    </dxcb:ASPxCallback>

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Operações de Aplicação/Resgate de Cotistas" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                 
                                        
        <table cellpadding="2" cellspacing="2">
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True"
                                    ClientInstanceName="btnEditCodigoCarteira" SpinButtons-ShowIncrementButtons="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCarteira').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True"
                                    ClientInstanceName="btnEditCodigoCotista" SpinButtons-ShowIncrementButtons="false">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                         ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td></td>
            <td>
                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Data Operação" Font-Underline="true"></asp:Label>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicioOperacao" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicioOperacao" runat="server" ClientInstanceName="textDataInicioOperacao"/>
            </td> 
            
            <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFimOperacao" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFimOperacao" runat="server" ClientInstanceName="textDataFimOperacao"/></td></tr>
                </table>
            </td>                                                                
            </tr>
            
            <tr>
            <td></td>
            <td>
                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Data Conversão" Font-Underline="true"></asp:Label>
            </td>
            </tr>
                        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicioConversao" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicioConversao" runat="server" ClientInstanceName="textDataInicioConversao"/>
            </td>
            
            <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFimConversao" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFimConversao" runat="server" ClientInstanceName="textDataFimConversao"/></td></tr>
                </table>
            </td>                                                                
            </tr>
            
            <tr>
            <td></td>
            <td>
                <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Data Liquidação" Font-Underline="true"></asp:Label>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicioLiquidacao" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
            </td>
            <td>
                <dxe:ASPxDateEdit ID="textDataInicioLiquidacao" runat="server" ClientInstanceName="textDataInicioLiquidacao"/>
            </td> 
            
            <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFimLiquidacao" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFimLiquidacao" runat="server" ClientInstanceName="textDataFimLiquidacao"/></td></tr>
                </table>
            </td>                                                                
            </tr>
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
            </td>
            <td colspan="3">
                <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropTipoOperacao">                    
                <Items>
                    <dxe:ListEditItem Value="0" Text="" />
                    <dxe:ListEditItem Value="1" Text="Aplicação" />
                    <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                    <dxe:ListEditItem Value="3" Text="Resgate Líquido" />
                    <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                    <dxe:ListEditItem Value="5" Text="Resgate Total" />
                    <dxe:ListEditItem Value="20" Text="Come Cotas" />
                    <dxe:ListEditItem Value="80" Text="Amortização" />
                    <dxe:ListEditItem Value="82" Text="Juros" />
                    <dxe:ListEditItem Value="84" Text="Amort.Juros" />
                    <dxe:ListEditItem Value="200" Text="Apenas Resgate" />
                    <dxe:ListEditItem Value="201" Text="Resgate+Comecota" />
                </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label" >
                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Mostra:"/>
            </td>
            
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropOpcoesCampos" runat="server" CssClass="dropDownListCurto_4" 
                            ClientInstanceName="dropOpcoesCampos" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Rendimento" />
                        <dxe:ListEditItem Value="2" Text="Valor Pfee" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            <tr>
            <td nowrap>
                <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Mostra ID Operação:"/>
            </td>
            <td>
            <dxe:ASPxCheckBox ID="chkIdOperacao" runat="server" ClientInstanceName="chkIdOperacao" />
            </td></tr>

        </table>                                                                                 
        
        </div>
    
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    
   </form>
</body>
</html>