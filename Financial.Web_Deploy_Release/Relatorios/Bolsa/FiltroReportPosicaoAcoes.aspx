﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportPosicaoAcoes, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script language="JavaScript" type="text/javascript">
    function btnExportaClick(tipoExportacao){              
       callbackExportaExcel.SendCallback(tipoExportacao);
    }    
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
       
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Posição Consolidada de Bolsa" /></div>
         
    <div id="mainContentSpace">
           
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                
                                        
        <table cellpadding="2" cellspacing="2">
        
         
        <tr>                                                                                                                     
            <td class="td_Label">                
                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Data Posição:"></asp:Label>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataPosicao" runat="server" ClientInstanceName="textDataPosicao" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
            </td>
        </tr>
                                   
        </table>                                                                                 
        
        </div>
        
       <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <table><tr>
           <td>
                <div id="Div1" class="linkButton linkButtonNoBorder">
                    <asp:LinkButton ID="btnExcelPosicaoBolsa" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal1" runat="server" Text="Exportar Posição Bolsa"/>
                        <div></div>
                    </asp:LinkButton>
            </td>

            <td>
                <div id="Div2" class="linkButton linkButtonNoBorder">
                    <asp:LinkButton ID="btnPdfPosicaoBolsa" runat="server" Font-Overline="false" CssClass="btnPdf"  OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Exportar Posição Bolsa"/>
                        <div></div>
                    </asp:LinkButton>
                </div>                        
            </td>
           </tr></table>
        </div>
        
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
   </form>
</body>
</html>