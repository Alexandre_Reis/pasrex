﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportRebateCliente, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="Javascript" src="../../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="Label1" runat="server" Text="Rebate por Cliente"/></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
                                                
        <table cellpadding="3">                        
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelMes" runat="server" CssClass="labelNormal" Text="Mês:" />
            </td>
            
            <td>
                <asp:DropDownList ID="dropMes" runat="server" CssClass="dropDownListCurto">
                    <asp:ListItem Text="Janeiro" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Fevereiro" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Março" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Abril" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Maio" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Junho" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Julho" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Agosto" Value="8"></asp:ListItem>
                    <asp:ListItem Text="Setembro" Value="9"></asp:ListItem>
                    <asp:ListItem Text="Outubro" Value="10"></asp:ListItem>
                    <asp:ListItem Text="Novembro" Value="11"></asp:ListItem>
                    <asp:ListItem Text="Dezembro" Value="12"></asp:ListItem>
                </asp:DropDownList>
            </td>
            
            <td>
                <asp:DropDownList ID="dropAno" runat="server" CssClass="dropDownListCurto_1" />
            </td>
            </tr>
            
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
   </form>
</body>
</html>