﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TabelaExcecaoAdm, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
        
    function OnGetDataTabelaAdm(data) {        
        hiddenIdTabela.SetValue(data);
        ASPxCallback2.SendCallback(data);
        popupTabelaAdm.HideWindow();
        btnEditTabelaAdm.Focus();
    }
    
    function TrataCampos(segmento)
    {
        popupCodigoAtivo.HideWindow();
        if (segmento==1 || segmento==10 || segmento==50 || segmento==80 )
        {
            btnCodigoAtivo.SetEnabled(false);            
            labelCodigoAtivo.GetMainElement().className = 'labelNormal'; 
        }
        else
        {
            btnCodigoAtivo.SetEnabled(true);
            labelCodigoAtivo.GetMainElement().className = 'labelRequired'; 
        }        
    }
    
    function OnGetDataCodigoAtivo(data) 
    {
        callBackPopupCodigoAtivo.SendCallback(data);
        popupCodigoAtivo.HideWindow();
        btnCodigoAtivo.Focus();
    }     
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callBackPopupCodigoAtivo" runat="server" OnCallback="callBackPopupCodigoAtivo_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnCodigoAtivo.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTabelaAdm.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupCodigoAtivo" ClientInstanceName="popupCodigoAtivo"
        runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlCodigoAtivo" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridCodigoAtivo" runat="server" Width="100%" ClientInstanceName="gridCodigoAtivo"
                        AutoGenerateColumns="False" DataSourceID="EsDSCodigoAtivo" KeyFieldName="CodigoAtivo"
                        OnCustomDataCallback="gridCodigoAtivo_CustomDataCallback" OnCustomCallback="gridCodigoAtivo_CustomCallback">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="CodigoAtivo" Caption="Código Ativo"
                                VisibleIndex="0" Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridCodigoAtivo.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCodigoAtivo);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Código Ativo." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCodigoAtivo.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="popupTabelaAdm" ClientInstanceName="popupTabelaAdm" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabelaAdm" runat="server" Width="100%"
                    ClientInstanceName="gridTabelaAdm"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaAdm" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabelaAdm_CustomDataCallback" 
                    OnCustomCallback="gridTabelaAdm_CustomCallback"
                    OnHtmlRowCreated="gridTabelaAdm_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" VisibleIndex="0" Width="14%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="60%"/>
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência" VisibleIndex="2" Width="15%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridTabelaAdm.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTabelaAdm);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Taxa Adm." />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabelaAdm.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Exceção de Ativos para Taxa de Administração"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdTabelaExcecao" DataSourceID="EsDSTabelaExcecaoAdm"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >

            <Columns>
                         
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdTabelaExcecao" Visible="false"></dxwgv:GridViewDataColumn>
                                           
                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Tabela Adm." UnboundType="String" VisibleIndex="1" Width="50%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoGrupo" Caption="Tipo Segmento" VisibleIndex="2" Width="15%">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                        
                            <dxe:ListEditItem Value="1" Text="Exceto Ações" />
                            <dxe:ListEditItem Value="2" Text="Exceto Ações Ativo" />
                            <dxe:ListEditItem Value="10" Text="Exceto Opções" />
                            <dxe:ListEditItem Value="11" Text="Exceto Opções Ativo" />
                            <dxe:ListEditItem Value="50" Text="Exceto Renda Fixa" />
                            <dxe:ListEditItem Value="51" Text="Exceto Renda Fixa Papel" />
                            <dxe:ListEditItem Value="52" Text="Exceto Renda Fixa Titulo" />
                            <dxe:ListEditItem Value="80" Text="Exceto Fundos" />
                            <dxe:ListEditItem Value="81" Text="Exceto Fundos Ativo" />
                            <dxe:ListEditItem Value="82" Text="Exceto Fundos Gestor" /> 
                        
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>                   

                <dxwgv:GridViewDataColumn FieldName="CodigoAtivo" Caption="Ativo" UnboundType="String" VisibleIndex="3" Width="35%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                                  
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdTabela" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTabela")%>' ClientInstanceName="hiddenIdTabela" />
                        
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Taxa Adm.:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditTabelaAdm" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditTabelaAdm" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCompleta")%>' OnLoad="btnEditTabelaAdm_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupTabelaAdm.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                                                                                                                                                                       
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelSegmento" runat="server" CssClass="labelRequired" Text="Segmento:"/>
                                </td>
                                <td colspan="3">      
                                    <dxe:ASPxComboBox ID="dropSegmento" runat="server" 
                                                        ShowShadow="false" ClientInstanceName="dropSegmento"
                                                        CssClass="dropDownListCurto" Text='<%#Eval("TipoGrupo")%>'>
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="Exceto Ações" />
                                        <dxe:ListEditItem Value="2" Text="Exceto Ações Ativo" />
                                        <dxe:ListEditItem Value="10" Text="Exceto Opções" />
                                        <dxe:ListEditItem Value="11" Text="Exceto Opções Ativo" />
                                        <dxe:ListEditItem Value="50" Text="Exceto Renda Fixa" />
                                        <dxe:ListEditItem Value="51" Text="Exceto Renda Fixa Papel" />
                                        <dxe:ListEditItem Value="52" Text="Exceto Renda Fixa Titulo" />
                                        <dxe:ListEditItem Value="80" Text="Exceto Fundos" />
                                        <dxe:ListEditItem Value="81" Text="Exceto Fundos Ativo" /> 
                                        <dxe:ListEditItem Value="82" Text="Exceto Fundos Gestor" />                                    
                                    </Items>
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                                btnCodigoAtivo.SetText('');
                                                                                TrataCampos(s.GetValue());
                                                                            }" />  
                                    
                                    </dxe:ASPxComboBox>    
                                </td>                                                  
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                        <dxe:ASPxLabel ID="labelCodigoAtivo" ClientInstanceName="labelCodigoAtivo" AssociatedControlID="btnCodigoAtivo" runat="server" CssClass="labelNormal" Text="Código Associado:" />
                                </td>
                                <td>
                                        <dxe:ASPxButtonEdit ID="btnCodigoAtivo" runat="server" CssClass="textButtonEdit"
                                            Text='<%#(Eval("CodigoAtivo"))%>' ClientEnabled="false"
                                            EnableClientSideAPI="True" ClientInstanceName="btnCodigoAtivo" ReadOnly="true"
                                            Width="340px">
                                            <Buttons>
                                                <dxe:EditButton />
                                            </Buttons>
                                            <ClientSideEvents ButtonClick="function(s, e) {popupCodigoAtivo.ShowAtElementByID(s.name); gridCodigoAtivo.PerformCallback('btnRefresh');}" 
                                             Init="function(s, e) { TrataCampos(dropSegmento.GetSelectedItem().value);}" />
                                        </dxe:ASPxButtonEdit>
                                 </td>                                               
                            </tr>
                            
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaExcecaoAdm" runat="server" OnesSelect="EsDSTabelaExcecaoAdm_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaAdm" runat="server" OnesSelect="EsDSTabelaAdm_esSelect" />        
    <cc1:esDataSource ID="EsDSCodigoAtivo" runat="server" OnesSelect="EsDSCodigoAtivo_esSelect" />        
    </form>
</body>
</html>