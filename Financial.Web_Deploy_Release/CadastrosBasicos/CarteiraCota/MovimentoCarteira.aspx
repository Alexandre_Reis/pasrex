﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_MovimentoCarteira, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {
                alert(e.result);             
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }                
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>    
            
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {                
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') {
                                
                    if ( resultSplit[1] != '') {
                                                                                
                        if (resultSplit[1] != null && resultSplit[1] != '') {                        
                            var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                            textDataOperacao.SetValue(newDate);
                        }                    
                        else {
                            textDataOperacao.SetValue(null);
                        }
                    }
                }
                
                if (gridCadastro.cpMultiConta == 'True' ) { dropConta.PerformCallback(); }
            }
        }        
        "/>
    </dxcb:ASPxCallback>
                
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Movimento Carteira"/>
    </div>
        
    <div id="mainContent">
            
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" EnableClientSideAPI="true"
                                            CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteiraFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>        
                                                
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                                             
                            </td>  
                            
                         <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                           
                        </tr>    
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
                            </td> 
                            <td colspan="3">
                                 <dxe:ASPxComboBox ID="dropTipoOperacaoFiltro" runat="server" ClientInstanceName="dropTipoOperacaoFiltro" 
                                                            ShowShadow="true"
                                                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                            CssClass="dropDownListCurto">
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Aporte" />
                                <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                                <dxe:ListEditItem Value="3" Text="Resgate Liquido" />
                                <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                                <dxe:ListEditItem Value="5" Text="Resgate Total" />
                                <dxe:ListEditItem Value="10" Text="Aplicacão Cotas Especial" />
                                <dxe:ListEditItem Value="11" Text="Aplicação Ações Especial" />
                                <dxe:ListEditItem Value="12" Text="Resgate Cotas Especial" />
                                <dxe:ListEditItem Value="20" Text="Come Cotas" />
                                <dxe:ListEditItem Value="80" Text="Amortização" />
                                <dxe:ListEditItem Value="82" Text="Juros" />
                                <dxe:ListEditItem Value="84" Text="Amort.+ Juros" />
                                <dxe:ListEditItem Value="100" Text="Incorporação Resgate" />
                                <dxe:ListEditItem Value="101" Text="Incorporação Aplicação" />
                                <dxe:ListEditItem Value="102" Text="Retirada" />
                                <dxe:ListEditItem Value="103" Text="Depósito" />
                                </Items>                                                                                          
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdOperacao" DataSourceID="EsDSOperacaoCotista"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"   
                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                        OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"   
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"                                              
                        >                    
                    <Columns>                    
                                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Nota" VisibleIndex="0" Width="5%"                                
                                CellStyle-HorizontalAlign="Left" />
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Carteira" VisibleIndex="1" Width="20%"/>
                                                
                        <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="2" Width="10%"/> 
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="TipoOperacao" VisibleIndex="3" Width="12%">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Aporte'>Aporte</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Resgate Bruto'>Resgate Bruto</div>" />
                                    <dxe:ListEditItem Value="3" Text="<div title='Resgate Liquido'>Resgate Liquido</div>" />
                                    <dxe:ListEditItem Value="4" Text="<div title='Resgate Cotas'>Resgate Cotas</div>" />
                                    <dxe:ListEditItem Value="5" Text="<div title='Resgate Total'>Resgate Total</div>" />
                                    <dxe:ListEditItem Value="10" Text="<div title='Aplicacão Cotas Especial'>Aplicacão Cotas Especial</div>" />
                                    <dxe:ListEditItem Value="11" Text="<div title='Aplicação Ações Especial'>Aplicação Ações Especial</div>" />
                                    <dxe:ListEditItem Value="12" Text="<div title='Resgate Cotas Especial'>Resgate Cotas Especial</div>" />
                                    <dxe:ListEditItem Value="20" Text="<div title='Come Cotas'>Come Cotas</div>" />
                                    <dxe:ListEditItem Value="80" Text="<div title='Amortização'>Amortização</div>" />
                                    <dxe:ListEditItem Value="82" Text="<div title='Juros'>Juros</div>" />
                                    <dxe:ListEditItem Value="84" Text="<div title='Amort.+ Juros'>Amort.+ Juros</div>" />
                                    <dxe:ListEditItem Value="100" Text="<div title='Incorporação Resgate'>Incorporação Resgate</div>" />
                                    <dxe:ListEditItem Value="101" Text="<div title='Incorporação Aplicação'>Incorporação Aplicação</div>" />
                                    <dxe:ListEditItem Value="102" Text="<div title='Retirada'>Retirada</div>" />
                                    <dxe:ListEditItem Value="103" Text="<div title='Depósito'>Depósito</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Valor" VisibleIndex="4" Width="12%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Caption="Quantidade" VisibleIndex="5" Width="15%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="Observacao" Caption="Observação" VisibleIndex="6" Width="20%" Settings-AutoFilterCondition="Contains" />
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="Quantidade" Visible="false"/>
                        <dxwgv:GridViewDataTextColumn FieldName="ContaDescricao" Caption="Conta" UnboundType="String"
                                                VisibleIndex="11" Width="7%" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" />
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm" >
                            
                            <table border="0">                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                    </td>        

                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>'                                                    
                                                    MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                 ValueChanged="function(s, e) {if (gridCadastro.cpMultiConta == 'True' ) 
                                                                                    {dropConta.PerformCallback(); } 
                                                                               }"  />               
                                        </dxe:ASPxSpinEdit>
                                    </td>

                                    <td colspan="2">
                                        <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCarteira")%>' ></asp:TextBox>
                                    </td> 
                                </tr>
                                                                                                              
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                    </td>         

                                    <td>
                                        <dxe:ASPxDateEdit ID="textDataOperacao" runat="server" ClientInstanceName="textDataOperacao" Value='<%#Eval("DataOperacao")%>' />
                                    </td> 
                                                                                                                                   
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"/>                    
                                    </td>                    
                                    <td>      
                                        <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ClientInstanceName="dropTipoOperacao" 
                                                            ShowShadow="true" DropDownStyle="DropDownList" 
                                                            CssClass="dropDownListCurto_1" Text='<%#Eval("TipoOperacao")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Aporte" />
                                            <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                                            <dxe:ListEditItem Value="3" Text="Resgate Liquido" />
                                            <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                                            <dxe:ListEditItem Value="5" Text="Resgate Total" />
                                            <dxe:ListEditItem Value="10" Text="Aplicacão Cotas Especial" />
                                            <dxe:ListEditItem Value="11" Text="Aplicação Ações Especial" />
                                            <dxe:ListEditItem Value="12" Text="Resgate Cotas Especial" />
                                            <dxe:ListEditItem Value="20" Text="Come Cotas" />
                                            <dxe:ListEditItem Value="80" Text="Amortização" />
                                            <dxe:ListEditItem Value="82" Text="Juros" />
                                            <dxe:ListEditItem Value="84" Text="Amort.+ Juros" />
                                            <dxe:ListEditItem Value="100" Text="Incorporação Resgate" />
                                            <dxe:ListEditItem Value="101" Text="Incorporação Aplicação" />
                                            <dxe:ListEditItem Value="102" Text="Retirada" />
                                            <dxe:ListEditItem Value="103" Text="Depósito" />
                                        </Items>                                                                                                           
                                        </dxe:ASPxComboBox>              
                                    </td>
                                                                        
                                </tr>

                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelValor" runat="server" CssClass="labelNormal" Text="Valor/Qtde:" />
                                    </td>
                                    
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                MaxLength="28" NumberType="Float" DecimalPlaces="12" />
                                    </td>                                
                                </tr>
                                
                                <tr>
                                <td class="td_Label">
                                <asp:Label ID="labelMoeda" runat="server" CssClass="labelNormal" Text="Moeda:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropMoeda" runat="server" ClientInstanceName="dropMoeda"
                                                        DataSourceID="EsDSMoeda" ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownListCurto" TextField="Nome" ValueField="IdMoeda"> 
                                        <ClientSideEvents                                                           
                                             ValueChanged="function(s, e) {if (gridCadastro.cpMultiConta == 'True' ) 
                                                                                {dropConta.PerformCallback(); }
                                                                           }"/>
                                    </dxe:ASPxComboBox>    
                                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                <asp:Label ID="labelConta" runat="server" CssClass="labelNormal" Text="Conta:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropConta" runat="server" ClientInstanceName="dropConta" EnableSynchronizationOnPerformCallback="true"
                                                        DataSourceID="EsDSContaCorrente" ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownListCurto" TextField="Numero" ValueField="IdConta"
                                                        Text='<%#Eval("IdConta") %>' OnCallback="dropConta_Callback"> 
                                        <ValidationSettings>
                                        <RequiredField IsRequired="True" />
                                        </ValidationSettings>                                                                                                                                       
                                    </dxe:ASPxComboBox>    
                                                    
                                </td>
                            </tr>
                                
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"/>
                                    </td>
                                    
                                    <td colspan="3">
                                        <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5" Text='<%#Eval("Observacao")%>'/>
                                    </td>                    
                                </tr>
                                
                            </table>
                            
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal2" runat="server" Text="OK+"/><div></div></asp:LinkButton>        

                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal11" runat="server" Text="OK"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal12" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>
                            
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="550px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSOperacaoCotista" runat="server" OnesSelect="EsDSOperacaoCotista_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />    
    <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
    
    </form>
</body>
</html>