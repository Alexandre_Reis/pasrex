﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Lamina, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }            
    </script>   
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                                                      
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
          if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                        
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="callBackClonar" runat="server" OnCallback="callBackClonar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelClonar.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupClonar.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        " />
    </dxcb:ASPxCallback>
                        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Lâmina de Informações Essenciais"></asp:Label>
    </div>
        
    <div id="mainContent">

        <dxpc:ASPxPopupControl ID="popupClonar" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="150" Top="10" HeaderText="Clonar"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0" cellspacing="2" cellpadding="2">
                                                                                                                                                
                                                <tr>
                                                    <td class="td_Label">
                                                     <asp:Label ID="labelInfo" runat="server" CssClass="labelRequired" Text="Carteira a Clonar:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropLamina" ClientInstanceName="dropInfoComplementares" runat="server"                                                         
                                                        KeyFieldName="CompositeKey" DataSourceID="EsDSLaminaClonar"
                                                                       Width="350px" TextFormatString="{0} - {1} - {3}" Font-Size="11px" AllowUserInput="false">
                                                         <Columns>
                                                             <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                             <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                             <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false"/>                            
                                                             <dxwgv:GridViewDataDateColumn FieldName="InicioVigencia" Width="10%"/>
                                                             <dxwgv:GridViewDataColumn FieldName="DataInicioVigenciaString" Visible="false" />    
                                                             <dxwgv:GridViewDataColumn FieldName="CompositeKey" Visible="false" />
                                                        </Columns>
                                                        
                                                        <GridViewProperties>
                                                            <Settings ShowFilterRow="True" />
                                                        </GridViewProperties>
                                                        
                                                        </dx:ASPxGridLookup>
                                                   </td>                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Nova Carteira:"/>
                                                    </td>
                                                    
                                                    <td class="td_Label">
                                                        <dx:ASPxGridLookup ID="dropCarteiraDestino" ClientInstanceName="dropCarteiraDestino" runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteiraClonar"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Início Vigência:"></asp:Label>
                                                    </td>
                                                 <td>       
                                                    <dxe:ASPxDateEdit ID="textDataClonar" runat="server" ClientInstanceName="textDataClonar" />
                                                </td>  
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaClonar" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"
                                                
                                                    OnClientClick="                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                   LoadingPanelClonar.Show();
                                                                   callBackClonar.SendCallback();                                                                        
                                                                   
                                                                   return false;
                                                                   ">
                                                    
                                                    <asp:Literal ID="Literal15" runat="server" Text="Clonar" /><div></div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                       
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
           
           <asp:LinkButton ID="btnClonar" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="
                                                                                
                                            dropInfoComplementares.SetText('');
                                            dropInfoComplementares.SetValue('');
                                            
                                            dropCarteiraDestino.SetText('');
                                            dropCarteiraDestino.SetValue('');
                                            
                                            textDataClonar.SetValue(null);
                                        
                                        popupClonar.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Clonar" /><div></div>
            </asp:LinkButton>         
           
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSLamina"                    
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"                    
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnInitNewRow="gridCadastro_InitNewRow"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    >     
                
                <Columns>                    
                   
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                                           
                    <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="9%" CellStyle-HorizontalAlign="left"/>                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="20%"/>                                                           
                    <dxwgv:GridViewDataDateColumn FieldName="InicioVigencia" Caption="Data Vigência" VisibleIndex="4" Width="10%"/>
                </Columns>
                
                <Templates>
                    <EditForm>
                        <div class="editForm">
                                        <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                            Height="100%" Width="100%" ActiveTabIndex="0" TabSpacing="0px">
                                            <TabPages>
                                                <dxtc:TabPage Text="Informações I">
                                                    <ContentCollection>
                                                        <dxw:ContentControl runat="server">
                            
                            <dxe:ASPxTextBox ID="CompositeKeyHidden" ClientInstanceName="CompositeKeyHidden" runat="server"  
                            Text='<%#Eval("CompositeKey")%>'  Visible="false"> 
                            </dxe:ASPxTextBox>                                                        
                            
                        <table border="0">
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1" ClientInstanceName="btnEditCodigoCarteira"
                                                                            Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer"  OnInit="btnEditCodigoCarteira_Init">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                    
                            <td>
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                            </td>
                        </tr>
                        
                        
                        <tr>                                                                        
                            <td class="td_Label">
                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Data Inicial:" />
                            </td>
                            <td colspan="2">
                                <dxe:ASPxDateEdit ID="textDataInicioVigencia" runat="server" ClientInstanceName="textDataInicioVigencia"
                                    Value='<%#Eval("InicioVigencia")%>'  OnInit="textDataInicioVigencia_Init" />
                            </td>                                     
                                                                                                                                                                           
                        </tr>
                        
                        <tr>                                                                                                                                                                                                                                    
                            <td class="td_Label_Longo">
                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="End. Eletrônico Fundo:"> </asp:Label>
                            </td>
                            <td colspan="2">
                                <dxe:ASPxTextBox ID="textEnderecoEletronico" runat="server" ClientInstanceName="textEnderecoEletronico" CssClass="textLongo5"  Text='<%# Eval("EnderecoEletronico") %>' />
                            </td>
                        </tr>
                                                
                        <tr>                                
                         <td class="td_Label_Longo">
                        <asp:Label ID="label21" runat="server" CssClass="labelRequired" Text="Descrição do Público Alvo:"> </asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="textDescricaoPublicoAlvo" runat="server" TextMode="multiline" Rows="5" MaxLength="500" Width="450" CssClass="textLongo5"
                                                Text='<%#Eval("DescricaoPublicoAlvo")%>' />
                        </td>
                        </tr>
                        
                        <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="label18" runat="server" CssClass="labelRequired" Text="Objetivos do Fundo:"> </asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="textObjetivoFundo" runat="server" TextMode="multiline" Rows="5" MaxLength="500" Width="450" CssClass="textLongo5"
                                                Text='<%#Eval("ObjetivoFundo")%>' />
                        </td>
                                                        
                        </tr>
                                                                                                                                                                                                   
                        <tr>
                        <td class="td_Label_Longo">
                                    <asp:Label ID="label23" runat="server" CssClass="labelRequired" Text="Descrição da Política de Investimentos:"> </asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="textDescricaoPoliticaInvestimento" runat="server" TextMode="multiline" Rows="5" MaxLength="500"  Width="450" CssClass="textLongo5" 
                                                Text='<%#Eval("DescricaoPoliticaInvestimento")%>' />
                        </td>                                                                        
                        </tr>
                                                                                                                                                                                                   
                        <tr>
                        <td class="td_Label_Longo">
                             <asp:Label ID="label35" runat="server" CssClass="labelRequired" Text="Classificação Risco:"> </asp:Label>
                        </td>
                        <td colspan="2">
                             <dxe:ASPxComboBox ID="dropRisco" runat="server" ClientInstanceName="dropRisco" ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("Risco") %>' ValueType="System.String">
                            <Items>
                                <dxe:ListEditItem Value="" Text="" />
                                <dxe:ListEditItem Value="1" Text="1" />
                                <dxe:ListEditItem Value="2" Text="2" />
                                <dxe:ListEditItem Value="3" Text="3" />
                                <dxe:ListEditItem Value="4" Text="4" />
                                <dxe:ListEditItem Value="5" Text="5" />
                            </Items>
                        </dxe:ASPxComboBox>
                        </td>
                                                                                                                                                                       
                        </tr>
                        </table>

                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>
                                                <dxtc:TabPage Text="Informações II" >
                                                    <ContentCollection>
                                                        <dxw:ContentControl runat="server">
                                <table border="0">
                                
                                <tr>
                                    <th colspan="6"><asp:Label ID="Label6" runat="server" Text="O Fundo Pode?" Font-Underline="true"></asp:Label></th>                                
                                </tr>

                                <tr>
                                 <td class="td_Label_Longo">
                                             <asp:Label ID="label31" runat="server" CssClass="labelRequired" Text="Limite Alavancagem:"> </asp:Label>
                                        </td>                                                                                                                             
                                        <td colspan="5">
                                            <dxe:ASPxSpinEdit ID="textLimiteAlavancagem" runat="server" CssClass="textValor_5" ClientInstanceName="textLimiteAlavancagem"
                                                MaxLength="14" NumberType="Float" DecimalPlaces="2"
                                                Text='<%# Eval("LimiteAlavancagem") %>' >
                                            </dxe:ASPxSpinEdit>
                                        </td>    
                                </tr>
                                
                                <tr>
                                
                                 <td class="td_Label_Longo">
                                         <asp:Label ID="label22" runat="server" CssClass="labelRequired" Text="Limite Credito Privado:"> </asp:Label>
                                    </td>                                                                                                                             
                                    <td colspan="5">                                        
                                            <dxe:ASPxSpinEdit ID="textLimiteCreditoPrivado" runat="server" CssClass="textValor_5" ClientInstanceName="textLimiteCreditoPrivado"
                                                MaxLength="14" NumberType="Float" DecimalPlaces="2"
                                                Text='<%# Eval("LimiteCreditoPrivado") %>' >
                                            </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>                                                                                                
                                <tr>                                                                                                                                                                                                                                                                                                                
                                    <td class="td_Label_Longo">
                                         <asp:Label ID="label20" runat="server" CssClass="labelRequired" Text="Limite Aplicação Exterior:"> </asp:Label>
                                    </td>                                                                                                                             
                                    <td colspan="5">                                        
                                        <dxe:ASPxSpinEdit ID="textLimiteAplicacaoExterior" runat="server" CssClass="textValor_5" ClientInstanceName="textLimiteAplicacaoExterior"
                                                MaxLength="14" NumberType="Float" DecimalPlaces="2"
                                                Text='<%# Eval("LimiteAplicacaoExterior") %>' >
                                            </dxe:ASPxSpinEdit>
                                            
                                    </td>         
                                </tr>
                                
                                <tr>                                                                                                                                                                                                                                    
                                    <td class="td_Label">
                                        <asp:Label ID="label30" runat="server" CssClass="labelRequired" Text="Utiliza Derivativos Proteção de Carteira:"></asp:Label>
                                    </td>
                                    <td colspan="5">
                                        <dxe:ASPxComboBox ID="dropDerivativosProtecaoCarteira" runat="server" ClientInstanceName="dropDerivativosProtecaoCarteira"
                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("DerivativosProtecaoCarteira")%>'  >
                                            <Items>
                                                <dxe:ListEditItem Value="" Text="" />
                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                <dxe:ListEditItem Value="N" Text="Não" />
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                 <td class="td_Label_Longo">
                                             <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Limite Concentração Emissor:"> </asp:Label>
                                 </td>                                                                                                                             
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textLimiteConcentracaoEmissor" runat="server" CssClass="textValor_5" ClientInstanceName="textLimiteConcentracaoEmissor"
                                                MaxLength="14" NumberType="Float" DecimalPlaces="2"
                                                Text='<%# Eval("LimiteConcentracaoEmissor") %>' >
                                            </dxe:ASPxSpinEdit>
                                        </td>    
                                        <td>            
                                            <!-- quando desclicar LimiteConcentracaoEmissor recebe vazio -->
                                            <dxe:ASPxCheckBox ID="chkVersao1" ClientInstanceName="chkVersao1" runat="server" Width="8px"
                                            OnDataBinding="versao_Init">                   
                                            
                                             <ClientSideEvents CheckedChanged="function(s, e){
                                                                              if( !s.GetChecked() ) {
                                                                                textLimiteConcentracaoEmissor.SetValue(null);
                                                                              }                                                                                                                                                                          
                                                                           }"
                                                                     />                                                                                                                 
                                            </dxe:ASPxCheckBox>                                            
                                        </td>
                                        <%--<td style="width:2px"></td>--%>
                                        <td colspan="3"><asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Marcar p/ Lâmina Versão 1.0" /></td>
                                </tr>
                                                               
                                <tr>
                                <th colspan="6"><asp:Label ID="Label5" runat="server" Text="Metodologia Lim. Alavancagem" Font-Underline="true"></asp:Label></th>
                                
                                </tr>
                                
                                <tr>
                                 <td class="td_Label_Longo">
                                        <asp:Label ID="label40" runat="server" CssClass="labelRequired" Text="Fundos De Investimento:"> </asp:Label>
                                </td>
                                <td colspan="5">
                                    <asp:TextBox ID="textFundosInvestimento" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo5" Width="450"
                                                        Text='<%#Eval("FundosInvestimento")%>' />
                                </td> 
                                </tr>
                                
                                <tr>
                                
                                    <td class="td_Label_Longo">
                                        <asp:Label ID="label25" runat="server" CssClass="labelRequired" Text="Fundos De Investimento em Cotas de Fundos de Investimento:"> </asp:Label>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="textFundosInvestimentoCotas" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo" Width="450"
                                                            Text='<%#Eval("FundosInvestimentoCotas")%>' />
                                    </td>     

                                </tr>
                                
                                <tr>
                                <th colspan="6"><asp:Label runat="server" Text="Estratégias X Perdas" Font-Underline="true"></asp:Label>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton></td>                                                                                                
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                            <asp:Label ID="label42" runat="server" CssClass="labelRequired" Text="Exibir:"></asp:Label>
                                        </td>
                                        <td colspan="5">
                                            <dxe:ASPxComboBox ID="dropEstrategiaPerdas" runat="server" ClientInstanceName="dropEstrategiaPerdas"
                                                ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("EstrategiaPerdas")%>' >
                                                <Items>
                                                    <dxe:ListEditItem Value="" Text="" />
                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                </tr>
                                
                                <tr>
                                <td class="td_Label_Longo">
                                    <asp:Label ID="label44" runat="server" CssClass="labelRequired" Text="Regulamento permite operações com perdas patrimoniais:"> </asp:Label>
                                </td>
                                <td colspan="5">
                                    <asp:TextBox ID="textRegulamentoPerdasPatrimoniais" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo" Width="450"
                                                        Text='<%#Eval("RegulamentoPerdasPatrimoniais")%>' />
                                </td>     
                                </tr>
                                <tr>                                
                                 <td class="td_Label_Longo">
                                        <asp:Label ID="label43" runat="server" CssClass="labelRequired" Text="Regulamento permite operações com patrimônio líquido negativo:"> </asp:Label>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="textRegulamentoPatrimonioNegativo" runat="server" TextMode="multiline" Rows="5" MaxLength="500" CssClass="textLongo" Width="450"
                                                            Text='<%#Eval("RegulamentoPatrimonioNegativo")%>' />
                                    </td>     
                                </tr>
                                </table>                                                                            
                                                                                                                         
                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>
                                                <dxtc:TabPage Text="Informações III" >
                                                    <ContentCollection>
                                                        <dxw:ContentControl runat="server">
                                                            <table border=0>
                                                            
                                                            <tr>
                                                            <th colspan="5"><asp:Label ID="Label4" runat="server" Text="Condições de Investimento" Font-Underline="true"></asp:Label></td>                                                                                                
                                                            </tr>

                                                            <tr>                                                                                                                                                                                                                                    
                        
                                                            <td>            
                                                                <dxe:ASPxCheckBox ID="existeCarencia" ClientInstanceName="existeCarencia" runat="server" Width="10px"
                                                                CheckState='<%#  Eval("ExisteCarencia") == DBNull.Value ? CheckState.Unchecked : (string)Eval("ExisteCarencia") == "S" ? CheckState.Checked : CheckState.Unchecked  %>'
                                                                ToolTip="Indica se existe Carência">
                                                                                                                                    
                                                                    <ClientSideEvents CheckedChanged="function(s, e){   
                                                                                var prazo = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textPrazoCarencia');                                                                                
                                                                                prazo.disabled = !s.GetChecked();
                                                                           }" 
                                                                     />
                                                                    
                                                                </dxe:ASPxCheckBox>
                                                                
                                                            </td>
                                                            <td style="width:2px">
                                                            <td><asp:Label ID="label9" Width="100px" runat="server" CssClass="labelNormal" Text="Existe Carência" /></td>
                        
                                                            <td>
                                                                <asp:Label ID="label32" runat="server" CssClass="labelRequired" Text="Prazo de Carência:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textPrazoCarencia" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo" Width="450"
                                                                Text='<%# Eval("PrazoCarencia") %>' OnDataBinding="textPrazoCarencia_Init" />
                                                            </td>
                                                            </tr>                                                                                                                                                                                                                                                                                                                                                                       
                                                            <tr>
                                                            
                                                            <td>
                                                                <dxe:ASPxCheckBox ID="existeTaxaEntrada" ClientInstanceName="existeTaxaEntrada" runat="server" Width="10px" 
                                                                CheckState='<%#  Eval("ExisteTaxaEntrada") == DBNull.Value ? CheckState.Unchecked : (string)Eval("ExisteTaxaEntrada") == "S" ? CheckState.Checked : CheckState.Unchecked  %>'
                                                                ToolTip="Indica se existe Taxa Entrada">
                                                                
                                                                    <ClientSideEvents CheckedChanged="function(s, e){   
                                                                                var entrada = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textTaxaEntrada');                                                                                
                                                                                entrada.disabled = !s.GetChecked();
                                                                           }" 
                                                                     />
                                                                    
                                                                </dxe:ASPxCheckBox>
                                                                
                                                                
                                                            </td>
                                                            <td style="width:2px">
                                                            <td><asp:Label ID="label7" Width="100px" runat="server" CssClass="labelNormal" Text="Existe Taxa Entrada" /></td>
                                                                                    
                                                            <td>
                                                                <asp:Label ID="label33" runat="server" CssClass="labelRequired" Text="Taxa Entrada:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox  ID="textTaxaEntrada" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo" 
                                                                Width="450" Text='<%# Eval("TaxaEntrada") %>' OnDataBinding="textTaxaEntrada_Init" />
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            
                                                            <td>
                                                                <dxe:ASPxCheckBox ID="existeTaxaSaida" ClientInstanceName="existeTaxaSaida" runat="server" Width="10px" 
                                                                CheckState='<%#  Eval("ExisteTaxaSaida") == DBNull.Value ? CheckState.Unchecked : (string)Eval("ExisteTaxaSaida") == "S" ? CheckState.Checked : CheckState.Unchecked  %>'
                                                                ToolTip="Indica se existe Taxa Saída">
                                                                
                                                                 <ClientSideEvents CheckedChanged="function(s, e){   
                                                                                var saida = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textTaxaSaida');                                                                                
                                                                                saida.disabled = !s.GetChecked();
                                                                           }" 
                                                                     />
                                                                    
                                                                </dxe:ASPxCheckBox>
                                                                
                                                            </td>
                                                            <td style="width:2px">
                                                            <td><asp:Label ID="label8" Width="100px" runat="server" CssClass="labelNormal" Text="Existe Taxa Saída" /></td>
                                                            
                                                            <td>
                                                                <asp:Label ID="label34" runat="server" CssClass="labelRequired" Text="Taxa Saída:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox  ID="textTaxaSaida" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" CssClass="textLongo" 
                                                                Width="450" Text='<%# Eval("TaxaSaida") %>' OnDataBinding="textTaxaSaida_Init" />
                                                            </td>
                                                            </tr>
                                                            </table>                                                                                                                         
                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>
                                                <dxtc:TabPage Text="Informações IV" >
                                                    <ContentCollection>
                                                        <dxw:ContentControl runat="server">
                                                            <table>
                                                            
                                                            
                        <tr>
                        <td class="td_Label">
                            <asp:Label ID="label36" runat="server" CssClass="labelRequired" Text="Fundo Estruturado:"></asp:Label>
                        </td>
                        <td>
                            <dxe:ASPxComboBox ID="dropFundoEstruturado" runat="server" ClientInstanceName="dropFundoEstruturado"
                                ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("FundoEstruturado")%>' >
                                <Items>
                                    <dxe:ListEditItem Value="" Text="" />
                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                    <dxe:ListEditItem Value="N" Text="Não" />
                                </Items>
                            </dxe:ASPxComboBox>
                        </td>
                        </tr>
                        <tr>
                        <td class="td_Label_Longo">
                                    <asp:Label ID="label24" runat="server" CssClass="labelRequired" Text="Cenários p/ Apuração rentabilidade:"> </asp:Label>
                        </td>
                        <td >
                            <asp:TextBox ID="textCenariosApuracaoRentabilidade" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500"  Width="450" CssClass="textLongo5"
                                                Text='<%#Eval("CenariosApuracaoRentabilidade")%>' />
                        </td>
                        </tr>
                        <tr>
                        
                        <td class="td_Label_Longo">
                                    <asp:Label ID="label37" runat="server" CssClass="labelRequired" Text="Exemplo de Desempenho do Fundo:"> </asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="textDesempenhoFundo" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500"  Width="450" CssClass="textLongo5"
                                                Text='<%#Eval("DesempenhoFundo")%>' />
                        </td>
                        </tr>                                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                  
                        <tr>
                        <td class="td_Label_Longo">
                                    <asp:Label ID="label19" runat="server" CssClass="labelRequired" Text="Política de Distribuição:"> </asp:Label>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="textPoliticaDistribuicao" runat="server" TextMode="MultiLine" Rows="5" MaxLength="500" Width="450" CssClass="textLongo5"
                                                Text='<%#Eval("PoliticaDistribuicao")%>' />
                        </td>
                        </tr>
                                                            
                                                            
                                                            
                                                            </table>                                                                                                                         
                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>                                                                                                         
                                                <dxtc:TabPage Text=" Serviço Atendimento Cotista" >
                                                    <ContentCollection>
                                                        <dxw:ContentControl runat="server">
                                                            <table >
                        <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelTelefone" runat="server" CssClass="labelRequired" Text="Telefone:"> </asp:Label>
                        </td>
                                                                        
                        <td >
                            <dxe:ASPxTextBox ID="textTelefone" runat="server" ClientInstanceName="textTelefone" CssClass="textNormal10" Text='<%# Eval("Telefone") %>' />
                        </td>
                        </tr>
                        <tr>
                        <td class="td_Label">
                            <asp:Label ID="label15" runat="server" CssClass="labelRequired" Text="Atendimento URL:"> </asp:Label>
                        </td>
                        <td >
                            <dxe:ASPxTextBox ID="textUrlAtendimento" runat="server" ClientInstanceName="textUrlAtendimento" CssClass="textLongo5" Text='<%# Eval("UrlAtendimento") %>' />
                        </td>
                        </tr>
                        <tr>
                        <td class="td_Label">
                            <asp:Label ID="label16" runat="server" CssClass="labelRequired" Text="Reclamações:"> </asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="textReclamacoes" runat="server" CssClass="textLongo5" Rows="8" MaxLength="500" Width="450" TextMode="multiline" Text='<%# Eval("Reclamacoes") %>' />
                        </td>
                        
                    </tr>
                                                                                                                                                                                                                                                                                                                                                                     
                </table>                                                                                                                                
                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>                                                                                                   
                                            </TabPages>
                                        </dxtc:ASPxPageControl>
                                        <div class="linkButton linkButtonNoBorder popupFooter">                        
                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                                        
                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                        </div>
                                    </div>
                    </EditForm>
                </Templates>                
                
                <SettingsPopup EditForm-Width="500" />                        
                                                
            </dxwgv:ASPxGridView>
            
        </div>                                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSLamina" runat="server" OnesSelect="EsDSLamina_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        
        
    <dxlp:ASPxLoadingPanel ID="LoadingPanelClonar" runat="server" Text="Processando, aguarde..." ClientInstanceName="LoadingPanelClonar" Modal="True"/>        
    <cc1:esDataSource ID="EsDSLaminaClonar" runat="server" OnesSelect="EsDSLaminaClonar_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCarteiraClonar" runat="server" OnesSelect="EsDSCarteiraClonar_esSelect" LowLevelBind="true" />
        
    </form>
</body>
</html>