﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TabelaStress, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
                       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">

    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    </script>
</head>

<body>
    <form id="form1" runat="server">
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {
                alert(e.result);
            }
            else {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Stress" />
    </div>
           
    <div id="mainContent">
                                
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaStress"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                >        

            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                
                <dxwgv:GridViewDataTextColumn FieldName="IdAtivo" Caption="Id Ativo" Width="22%" VisibleIndex="1" CellStyle-HorizontalAlign="left">
                    <PropertiesTextEdit MaxLength="10" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data" VisibleIndex="2" Width="8%"/>

                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoAtivo" Caption="Tipo Ativo" VisibleIndex="3" Width="35%" ExportWidth="125">
                <PropertiesComboBox EncodeHtml="false">
                <Items>
                    <dxe:ListEditItem Value="1" Text="<div title='Estratégia'>Estratégia</div>" />
                    <dxe:ListEditItem Value="2" Text="<div title='Fundo'>Fundo</div>" />
                    <dxe:ListEditItem Value="3" Text="<div title='Carteira'>Carteira</div>" />
                </Items>
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Stress" VisibleIndex="4" Width="35%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" DecimalPlaces="2"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                        
            </Columns>                
            <Templates>
            <EditForm>
            
                <div class="editForm">    
                        
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelIdAtivo" runat="server" CssClass="labelRequired" Text="Id Ativo:" />                                    
                            </td>                                
                            <td>                                
                                <dxe:ASPxTextBox ID="textIdAtivo" ClientInstanceName="textIdAtivo" runat="server" CssClass="textNormal_5" 
                                MaxLength="10" Text='<%# Eval("IdAtivo") %>' OnLoad="textIdAtivo_Load" />                                    
                            </td>                        
                        </tr>
 
                        <tr>
                            <td class="td_Label">
                                 <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref:"  />
                            </td> 
                            <td colspan="2">
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init" />                            
                            </td>                              
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoAtivo" runat="server" CssClass="labelRequired" Text="Tipo Ativo:"/>                   
                            </td>                    
                            <td colspan="3">      
                                <dxe:ASPxComboBox ID="dropTipoAtivo" runat="server" ShowShadow="false" ClientInstanceName="dropTipoAtivo" CssClass="dropDownListCurto"
                                                    Text='<%#Eval("TipoAtivo")%>' OnLoad="dropTipoAtivo_Load"> 
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Estrategia" />
                                <dxe:ListEditItem Value="2" Text="Fundo" />
                                <dxe:ListEditItem Value="3" Text="Carteira" />                                                        
                                </Items>                                                            
                                </dxe:ASPxComboBox>                                                                 
                            </td>      
                        </tr> 
                                                                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelStress" runat="server" CssClass="labelRequired" Text="Stress:" />
                            </td>
                            <td colspan="2">
                                <dxe:ASPxSpinEdit ID="textStress" runat="server" ClientInstanceName="textStress" DisplayFormatString="{0:#,##0.000000;(#,##0.000000);0.000000}"
                                        Text='<%# Eval("Stress") %>' NumberType="Float" MaxLength="12" DecimalPlaces="6" CssClass="textValor_5" MinValue="-100000" MaxValue="100000">
                                </dxe:ASPxSpinEdit>
                            </td>                             
                        </tr>      
                        
                    </table>
                    
                    <div class="linhaH"></div>    
                                            
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                    
                </div>
                                         
            </EditForm>
            </Templates>
            
            <SettingsPopup EditForm-Width="300px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
                       
        </dxwgv:ASPxGridView>            
        </div>
                   
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaStress" runat="server" OnesSelect="EsDSTabelaStress_esSelect" LowLevelBind="true" />
    </form>
</body>
</html>