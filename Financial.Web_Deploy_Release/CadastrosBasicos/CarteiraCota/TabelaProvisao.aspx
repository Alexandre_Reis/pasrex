﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TabelaProvisao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    var gridEventoOperacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }
        
    function OnGetDataEvento(data) {        
        /* idEvento, Descricao */
        var resultSplit = data.split('|');
        
        if(gridEventoOperacao == 'eventoProvisao') {
            btnEditEventoProvisao.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoProvisao.Focus();
        }
        else if(gridEventoOperacao == 'eventoPagamento') {        
            btnEditEventoPagamento.SetValue(resultSplit[0] + resultSplit[1]);
            popupEvento.HideWindow();
            btnEditEventoPagamento.Focus();
        }
    }    
    
    function updatePeriodoApropriacao(){
        callbackUpdatePeriodoApropriacao.SendCallback();
    }
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackUpdatePeriodoApropriacao" runat="server" OnCallback="callbackUpdatePeriodoApropriacao_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            var el = document.getElementById('tdLabelPeriodoApropriacao');
            el.innerHTML = e.result;
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {  
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                                      
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <dxpc:ASPxPopupControl ID="popupEvento" ClientInstanceName="popupEvento" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridEvento" runat="server" Width="100%"
                    ClientInstanceName="gridEvento"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSEvento" KeyFieldName="IdEvento"
                    OnCustomDataCallback="gridEvento_CustomDataCallback" 
                    OnCustomCallback="gridEvento_CustomCallback"
                    OnHtmlRowCreated="gridEvento_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdEvento" VisibleIndex="0" Visible="false"/>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="3" Width="70%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="Conta Débito" VisibleIndex="1" Width="15%"/>
                <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="Conta Crédito" VisibleIndex="2" Width="15%"/>                
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
            gridEvento.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataEvento);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Roteiro Contábil" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridEvento.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Provisão"></asp:Label>
    </div>
        
    <div id="mainContent">
                 
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdTabela" DataSourceID="EsDSTabelaProvisao"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >
                <Columns>                    
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdTabela" Caption="Id" VisibleIndex="1" Width="7%"
                                              HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="8%" />                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="22%" />                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdCadastro" Caption="Descrição" VisibleIndex="5" Width="15%">
                        <PropertiesComboBox DataSourceID="EsDSCadastroProvisao" TextField="Descricao" ValueField="IdCadastro">
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCalculo" Caption="Tipo" VisibleIndex="6" Width="13%" ExportWidth="120">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Provisão'>Provisão</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Diferido'>Diferido</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>                
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ValorTotal" VisibleIndex="7" Width="15%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="ContagemDias" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="DiaRenovacao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroMesesRenovacao" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroDiasPagamento" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="IdCadastro" Visible="false" />                    
                    <dxwgv:GridViewDataColumn FieldName="DataFim" Visible="false" />                    
                    
                </Columns>
                
                <Templates>            
                <EditForm>
                    <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                        
                    <div class="editForm">
                    
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1" ClientInstanceName="btnEditCodigoCarteira"
                                                                            Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                    
                            <td colspan="2">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                            </td>
                        </tr>
                                                                                                                                                                                                                                                                                                                                                              
                        <tr>       
                            <td  class="td_Label">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Referência:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" ClientSideEvents-DateChanged="function(){updatePeriodoApropriacao();}" Value='<%#Eval("DataReferencia")%>'/>                            
                            </td>  
                            
                            <td class="td_Label">
                                <asp:Label ID="labelProvisao" runat="server" CssClass="labelRequired" Text="Cadastro:"></asp:Label>                    
                            </td>
                            
                            <td>
                                <dxe:ASPxComboBox ID="dropProvisao" runat="server" DataSourceID="EsDSCadastroProvisao" 
                                                    ShowShadow="false" CssClass="dropDownListCurto"                                                 
                                                    TextField="Descricao" ValueField="IdCadastro"
                                                    Text='<%#Eval("IdCadastro")%>'>
                                </dxe:ASPxComboBox>
                            </td>    
                        </tr>                                
                    
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoCalculo" runat="server" CssClass="labelRequired" Text="Tipo Cálculo:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropTipoCalculo" runat="server" ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("TipoCalculo")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Provisão" />
                                <dxe:ListEditItem Value="2" Text="Diferido" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>                                     
                                
                            </td>
                        
                            <td  class="td_Label">
                                <asp:Label ID="labelValorTotal" runat="server" CssClass="labelRequired" Text="Valor Total:"></asp:Label>
                            </td>
                            <td>
                                
                                <dxe:ASPxSpinEdit ID="textValorTotal" runat="server" CssClass="textValor_5" ClientInstanceName="textValorTotal"
                                                                Text='<%# Eval("ValorTotal") %>' MaxLength="10" NumberType="Float">            
                                </dxe:ASPxSpinEdit>
                            </td>                                  
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelContagemDias" runat="server" CssClass="labelRequired" Text="Contagem Dias:"></asp:Label>                    
                            </td>                    
                            <td>      
                                <dxe:ASPxComboBox ID="dropContagemDias" runat="server" ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("ContagemDias")%>'>
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Dias Úteis" />
                                <dxe:ListEditItem Value="2" Text="Dias Corridos" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>    
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelDiaRenovacao" runat="server" CssClass="labelRequired" Text="Dia Renov.:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textDiaRenovacao" runat="server" CssClass="textCurto" ClientInstanceName="textDiaRenovacao"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('DiaRenovacao')%>">            
                                </dxe:ASPxSpinEdit>                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelNumeroMesesRenovacao" runat="server" CssClass="labelRequired" Text="Meses Renov.:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroMesesRenovacao" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroMesesRenovacao"
		                                  MaxLength="2" NumberType="integer" Text="<%#Bind('NumeroMesesRenovacao')%>">            
                                </dxe:ASPxSpinEdit>                                                       
                            </td>
                            
                            <td class="td_Label">
                                <asp:Label ID="labelNumeroDiasPagamento" runat="server" CssClass="labelRequired" Text="Dias Pagto:"></asp:Label>                    
                            </td>                    
                            <td>
                                <dxe:ASPxSpinEdit ID="textNumeroDiasPagamento" runat="server" CssClass="textCurto" ClientInstanceName="textNumeroDiasPagamento"
		                                  MaxLength="3" NumberType="integer" Text="<%#Bind('NumeroDiasPagamento')%>">            
                                </dxe:ASPxSpinEdit>                                                                                   
                            </td>
                        </tr>
                        
                        <tr>
                            <td  class="td_Label">
                                <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Data Renovação:"  /></td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" ClientSideEvents-DateChanged="function(){updatePeriodoApropriacao();}" Value='<%#Eval("DataFim")%>'/>                                                        
                            </td>  
                            <td id="tdLabelPeriodoApropriacao" colspan="3">
                                <dxe:ASPxLabel runat="server" ID="labelPeriodoApropriacao"></dxe:ASPxLabel>
                            </td>
                        </tr>
                        
                    <div id="divEventos" runat="server" visible="true">
                        
                        <tr>
                        <td  class="td_Label">
                            <asp:Label ID="labelEventoProvisao" runat="server" CssClass="labelNormal" Text="Evento Provisão:" />
                        </td>
                        <td colspan="3">
                        
                             <dxe:ASPxButtonEdit ID="btnEditEventoProvisao" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditEventoProvisao" 
                                        ReadOnly="true" Width="420px"  Text='<%# Eval("DescricaoEventoProvisao") %>' >                                                                                                               
                            <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                               <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoProvisao'; popupEvento.ShowAtElementByID(s.name); gridEvento.PerformCallback('btnRefresh');}" />                                                                   
                            </dxe:ASPxButtonEdit>
                        </td>                                                               
                        </tr>
                    
                        <tr>
                        <td  class="td_Label">
                            <asp:Label ID="labelEventoPagamento" runat="server" CssClass="labelNormal" Text="Evento Pagamento:" />
                        </td>
                        <td colspan="3">
                        
                             <dxe:ASPxButtonEdit ID="btnEditEventoPagamento" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditEventoPagamento" 
                                        ReadOnly="true" Width="420px" Text='<%# Eval("DescricaoEventoPagamento") %>' >                                                                                                               
                            <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                               <ClientSideEvents ButtonClick="function(s, e) {gridEventoOperacao='eventoPagamento'; popupEvento.ShowAtElementByID(s.name); gridEvento.PerformCallback('btnRefresh');}" />                                                                   
                            </dxe:ASPxButtonEdit>
                        </td>                                                               
                        </tr>
                        
                    </div>
                    </table>
                    
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                        
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                    
                    </div>                        
                    
                    </asp:Panel>                    
                </EditForm>                    
                </Templates>                
                
                <SettingsPopup EditForm-Width="500" />

                <ClientSideEvents

                    BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }"

                    EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }"
                />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                                
            </dxwgv:ASPxGridView>
            
        </div>
                              
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSTabelaProvisao" runat="server" OnesSelect="EsDSTabelaProvisao_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCadastroProvisao" runat="server" OnesSelect="EsDSCadastroProvisao_esSelect" />    
    <cc1:esDataSource ID="EsDSEvento" runat="server" OnesSelect="EsDSEvento_esSelect" LowLevelBind="true" />
    </form>
</body>
</html>