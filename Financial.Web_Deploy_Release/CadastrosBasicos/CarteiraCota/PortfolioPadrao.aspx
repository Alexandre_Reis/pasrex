﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PortfolioPadrao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = false;
    var operacao = '';    
    document.onkeydown=onDocumentKeyDown;    
        
    function OnGetDataCarteira(data) 
    {       
        btnEditCodigoCarteira.SetValue(data);        
        callbackCarteira.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    
    function OnGetDataCarteira1(data) 
    {       
        btnEditCodigoCarteira1.SetValue(data);        
        callbackCarteira1.SendCallback(btnEditCodigoCarteira1.GetValue());
        popupCarteira1.HideWindow();
        btnEditCodigoCarteira1.Focus();
    }    
    
    function OnGetDataCarteira2(data) 
    {       
        btnEditCodigoCarteira2.SetValue(data);        
        callbackCarteira2.SendCallback(btnEditCodigoCarteira2.GetValue());
        popupCarteira2.HideWindow();
        btnEditCodigoCarteira2.Focus();
    }                            
 
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackCarteira" runat="server" OnCallback="callbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
            }" />
        </dxcb:ASPxCallback>    
        <dxcb:ASPxCallback ID="callbackCarteira1" runat="server" OnCallback="callbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira1');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira1, textNomeCarteira);
            }" />
        </dxcb:ASPxCallback>   
        <dxcb:ASPxCallback ID="callbackCarteira2" runat="server" OnCallback="callbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira2');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira2, textNomeCarteira);
            }" />
        </dxcb:ASPxCallback>                       
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Portifólio Padrão"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdPortfolioPadrao" DataSourceID="EsDSPortfolioPadrao"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnRowInserting="gridCadastro_RowInserting"
                                        OnRowUpdating="gridCadastro_RowUpdating" AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteiraTaxaGestao" Width="10%" VisibleIndex="1">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CarteiraTaxaGestao" Width="10%" VisibleIndex="2" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteiraTaxaPerformance" Width="10%" VisibleIndex="3">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CarteiraTaxaPerformance" Width="10%" VisibleIndex="4" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteiraDespesasReceitas" Width="10%"
                                                VisibleIndex="5">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CarteiraDespesasReceitas" Width="10%" VisibleIndex="6" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVigencia" Caption="Data Vigência" VisibleIndex="7"
                                                Width="10%" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira Taxa Gestão:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteiraTaxaGestao")%>'
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, callbackCarteira, btnEditCodigoCarteira);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Style="width: 386px;"
                                                                    Enabled="false" Text='<%#Eval("CarteiraTaxaGestao")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira1" runat="server" CssClass="labelRequired" Text="Carteira Taxa Performance:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira1" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCarteira1" Text='<%#Eval("IdCarteiraTaxaPerformance")%>'
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira1').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira1.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, callbackCarteira1, btnEditCodigoCarteira1);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textNomeCarteira1" runat="server" CssClass="textNome" Style="width: 386px;"
                                                                    Enabled="false" Text='<%#Eval("CarteiraTaxaPerformance")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira2" runat="server" CssClass="labelRequired" Text="Carteira Outras Despesas e Receitas:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira2" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCarteira2" Text='<%#Eval("IdCarteiraDespesasReceitas")%>'
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira2').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira2.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, callbackCarteira2, btnEditCodigoCarteira2);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textNomeCarteira2" runat="server" CssClass="textNome" Style="width: 386px;"
                                                                    Enabled="false" Text='<%#Eval("CarteiraDespesasReceitas")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataVigencia" runat="server" CssClass="labelRequired" Text="Data Vigência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVigencia" runat="server" ClientInstanceName="textDataVigencia"
                                                                    Value='<%#Eval("DataVigencia")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="480px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSPortfolioPadrao" runat="server" OnesSelect="EsDSPortfolioPadrao_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira2" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira1" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
