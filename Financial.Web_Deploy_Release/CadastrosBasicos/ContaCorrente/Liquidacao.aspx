﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Liquidacao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;    
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    
    function OnGetDataClienteFiltro(data){                   
        if (popupFiltro.IsVisible()) {
            btnEditCodigoClienteFiltro.SetValue(data);        
            ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
        }
        else if (popupSaldoCC.IsVisible()) {
            btnEditCodigoClienteSaldoCC.SetValue(data);        
            ASPxCallback2.SendCallback(btnEditCodigoClienteSaldoCC.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteSaldoCC.Focus();
        }
        else if (popupSinacor.IsVisible()) {
            btnEditCodigoClienteSinacorFiltro.SetValue(data);        
            ASPxCallbackSinacor.SendCallback(btnEditCodigoClienteSinacorFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteSinacorFiltro.Focus();
        }
    }    
    function OnClearFilterClick_Sinacor()  { 
        var textNomeSinacor = document.getElementById('popupSinacor_textNomeClienteSinacorFiltro');
        textNomeSinacor.value = '';
        //   
        btnEditCodigoClienteSinacorFiltro.SetValue(null);
        textDataInicioSinacor.SetValue(null);
        textDataFimSinacor.SetValue(null);
        //
        //Grid
        gridConsultaSinacor.SetVisible(false);
        btnSinacor1.SetClientVisible(false);
    }         
    function OnDataAlterada(s) {
        if(textDataLancamento.GetText() == textDataVencimento.GetText() || textDataVencimento.GetText() == '01/01/0100') {
            dropEventoVencimento.SetEnabled(false);
            dropEventoVencimento.SetValue(null);
        }                                                                                            
        else {
            dropEventoVencimento.SetEnabled(true);
        }
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                    
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }          
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {          
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');                
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {   
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');                
                if (gridCadastro.cp_EditVisibleIndex == 'new')
                {
	                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome, textDataLancamento);                                                        
                }
                else
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);
                }
                
                if(window.tmp_MultiContaPendente && window.tmp_MultiContaPendente === true){
                    //so vamos dar rebind no drop se o id do cliente tiver mudado
                    window.tmp_MultiContaPendente = false;
                    if (gridCadastro.cpMultiConta == 'True' ) { dropConta.PerformCallback(); }
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        { 
            var resultSplit = e.result.split('|');            
            var textNomeClienteSaldoCC = document.getElementById('popupSaldoCC_textNomeClienteSaldoCC');
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteSaldoCC, textNomeClienteSaldoCC, textDataSaldo);            
            
            textSaldoAbertura.SetValue(resultSplit[3]);
            textVencimentos.SetValue(resultSplit[4]);
            textSaldoFechamento.SetValue(resultSplit[5]);
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallbackSinacor" runat="server" OnCallback="ASPxCallbackSinacor_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                            
            if (gridCadastro.cp_EditVisibleIndex == -1) {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];            
                var textNomeClienteSinacorFiltro = document.getElementById('popupSinacor_textNomeClienteSinacorFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteSinacorFiltro, textNomeClienteSinacorFiltro);
            } 
            else {
                var textNomeClienteSinacor = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeClienteSinacor');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteSinacor, textNomeClienteSinacor);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErroSinacor" runat="server" OnCallback="callbackErroSinacor_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                    
            }
            else {
                gridConsultaSinacor.PerformCallback('btnSinacor');
                alert('Lançamentos Carregados com Sucesso.');
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Lançamentos - Liquidação Financeira"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </buttons>
                                                            <clientsideevents keypress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                buttonclick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" lostfocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Vcto Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Vcto Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Lcto Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicioLancamento" runat="server" ClientInstanceName="textDataInicioLancamento" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Lcto Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFimLancamento" runat="server" ClientInstanceName="textDataFimLancamento" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDescricao" runat="server" CssClass="labelNormal" Text="Descric:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="textDescricaoFiltro" runat="server" CssClass="textNormal"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupSaldoCC" AllowDragging="true" PopupElementID="popupSaldoCC"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Saldo projetado de C/C"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo2">
                                                        <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteSaldoCC" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteSaldoCC" MaxLength="10" NumberType="Integer">
                                                            <buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </buttons>
                                                            <clientsideevents keypress="function(s, e) {document.getElementById('popupSaldoCC_textNomeClienteSaldoCC').value = '';} "
                                                                gotfocus="function(s, e) {popupFiltro.SetVisible(false); }" buttonclick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}"
                                                                lostfocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback2, btnEditCodigoClienteSaldoCC, textDataSaldo); }" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="textNomeClienteSaldoCC" runat="server" CssClass="textNomeClienteSaldo"
                                                            Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Data Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataSaldo" runat="server" ClientInstanceName="textDataSaldo"
                                                            ClientEnabled="false" />
                                                    </td>
                                                    <td class="td_label_Col1">
                                                        <asp:Label ID="label5" runat="server" CssClass="labelBlock labelBlock1" Text="Saldo Abertura:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxTextBox ID="textSaldoAbertura" runat="server" ClientInstanceName="textSaldoAbertura"
                                                            ClientEnabled="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label6" runat="server" CssClass="labelBlock labelBlock1" Text="Vctos Dia:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxTextBox ID="textVencimentos" runat="server" ClientInstanceName="textVencimentos"
                                                            ClientEnabled="false" />
                                                    </td>
                                                    <td class="td_label_Col1">
                                                        <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Saldo Fech. :" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxTextBox ID="textSaldoFechamento" runat="server" ClientInstanceName="textSaldoFechamento"
                                                            ClientEnabled="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnAtualizaCC" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                                    OnClientClick="OnLostFocus(popupMensagemCliente, ASPxCallback2, btnEditCodigoClienteSaldoCC, textDataSaldo); return false;">
                                                    <asp:Literal ID="Literal14" runat="server" Text="Atualizar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                    <ClientSideEvents PopUp="function(s, e) {popupFiltro.SetVisible(false); }" />
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupSinacor" AllowDragging="true" PopupElementID="popupSinacor"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="450" Left="250" Top="70" HeaderText="Lançamentos de C/C Sinacor"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl runat="server">
                                            <!-- ***************************************************************-->

                                                    <table border="0" width="430">
                                                        <tr>
                                                            <td class="td_Label_Longo">
                                                                <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteSinacorFiltro" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoClienteSinacorFiltro" MaxLength="10" NumberType="Integer">
                                                                    <buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </buttons>
                                                                    <clientsideevents keypress="function(s, e) {document.getElementById('popupSinacor_textNomeClienteSinacorFiltro').value = '';} "
                                                                        buttonclick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" lostfocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallbackSinacor, btnEditCodigoClienteSinacorFiltro);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="textNomeClienteSinacorFiltro" runat="server" CssClass="textNome"
                                                                    Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-left: 18px;">
                                                                <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Início:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataInicioSinacor" runat="server" ClientInstanceName="textDataInicioSinacor" />
                                                            </td>
                                                            <td colspan="2">
                                                                <table border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataFimSinacor" runat="server" ClientInstanceName="textDataFimSinacor" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                        <asp:LinkButton ID="btnOKFilterSinacor" runat="server" Font-Overline="false" ForeColor="Black"
                                                            CssClass="btnOK" OnClick="btnSinacor_Click">
                                                            <asp:Literal ID="Literal13" runat="server" Text="Aplicar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnFilterCancelSinacor" runat="server" Font-Overline="false"
                                                            ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_Sinacor(); return false;">
                                                            <asp:Literal ID="Literal15" runat="server" Text="Limpar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <%--                   <asp:LinkButton ID="btnSinacorInsert" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"                     
                            OnClientClick=" gridConsultaSinacor.PerformCallback('btnSinacor'); return false;"
                            ToolTip="Insere Liquidação" Visible="true">
                                                          
                        <asp:Literal ID="Literal16" runat="server" Text="Inserir Liquidação"/><div></div>
                                                   
                    </asp:LinkButton>                    
--%>
                                                        <%-- Style para ASPxButton Funcional
                    <dxe:ASPxButton ID="ASPxButton1" ClientInstanceName="btnSinacor1" runat="server" Text="Inserir Liquidação" 
                    Font-Overline="False" ForeColor="Black" ToolTip="Insere Liquidação" AutoPostBack="false" ClientVisible = "false"
                    BackgroundImage-ImageUrl="../imagens/ico_form_ok.gif"
                    BackgroundImage-VerticalPosition="center"
                    BackgroundImage-Repeat = "NoRepeat"
                    BackgroundImage-HorizontalPosition="left"
                    ImageSpacing = "6px"
                    Image-Height = "10px"
                    Height = "10px" 
                    BackColor="ControlLight" 
                    Font-Size="7.5pt">                     
                    <Paddings PaddingLeft="2px" />
                    <ClientSideEvents Click="function(s, e) { 
                                                callbackErroSinacor.SendCallback();
                                                return false;
                                             }" 
                    />
                    
                    </dxe:ASPxButton>
--%>
                                                        <dxe:ASPxButton ID="btnSinacor1" ClientInstanceName="btnSinacor1" runat="server"
                                                            Text="Atualizar Financial" ToolTip="Insere lançamento de c/c no Financial" AutoPostBack="false"
                                                            ClientVisible="false">
                                                            <clientsideevents click="function(s, e) { 
                                                callbackErroSinacor.SendCallback();
                                                return false;
                                             }" />
                                                        </dxe:ASPxButton>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="divDataGrid">
                                                        <dxwgv:ASPxGridView ID="gridConsultaSinacor" runat="server" ClientInstanceName="gridConsultaSinacor"
                                                            DataSourceID="EsDS_Sinacor" Width="440" Settings-UseFixedTableLayout="true" EnableCallBacks="true"
                                                            KeyFieldName="CompositeKey" OnCustomUnboundColumnData="gridConsultaSinacor_CustomUnboundColumnData"
                                                            OnCustomCallback="gridConsultaSinacor_CustomCallback" OnHtmlDataCellPrepared="gridConsultaSinacor_HtmlDataCellPrepared">
                                                            <Columns>
                                                                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Visible="false" />
                                                                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30" ButtonType="Image" ShowClearFilterButton="True">
                                                                    <HeaderStyle HorizontalAlign="Center"/>
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="DtLiquidacao" Caption="Liquidação" VisibleIndex="1"
                                                                    Width="100" CellStyle-Wrap="False" />
                                                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="2"
                                                                    Width="210" CellStyle-Wrap="False" />
                                                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorLancamento" Caption="Valor" VisibleIndex="3"
                                                                    Width="100" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right"
                                                                    CellStyle-Wrap="False">
                                                                    <PropertiesSpinEdit NumberType="Float" DecimalPlaces="2" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesSpinEdit>
                                                                </dxwgv:GridViewDataSpinEditColumn>
                                                            </Columns>
                                                            <SettingsPager PageSize="20" />
                                                            <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible"
                                                                VerticalScrollableHeight="150" VerticalScrollBarStyle="Virtual" />
                                                            <SettingsText EmptyDataRow="0 registros" />
                                                            <SettingsCommandButton>
                                                                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                                            </SettingsCommandButton>
                                                        </dxwgv:ASPxGridView>
                                                    </div>
                                                    <div>
                                                    </div>

                                            <!-- ***************************************************************-->
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px" />
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnSaldoCC" runat="server" Font-Overline="false" CssClass="btnCalc"
                                        OnClientClick="popupSinacor.Hide(); popupSaldoCC.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Saldo C/C" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnSinacor" runat="server" Font-Overline="false" CssClass="btnView"
                                        OnPreRender="btnSinacorOnPreRender" OnClientClick="popupFiltro.Hide(); popupSaldoCC.Hide(); popupSinacor.ShowWindow(); btnEditCodigoClienteSinacorFiltro.Focus(); return false;">
                                        <asp:Literal ID="Literal12" runat="server" Text="Sinacor" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdLiquidacao"
                                        DataSourceID="EsDSLiquidacao" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnHtmlDataCellPrepared="gridCadastro_HtmlDataCellPrepared" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnInitNewRow="gridCadastro_InitNewRow" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="24%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="3"
                                                Width="9%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="4"
                                                Width="9%" />
                                            <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="5"
                                                Width="27%" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="Fonte" VisibleIndex="7" Width="7%" ExportWidth="110">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Interno'>Interno</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Manual'>Manual</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Sinacor'>Sinacor</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='CMDF'>CMDF</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="9" Width="11%" HeaderStyle-HorizontalAlign="Right"
                                                FooterCellStyle-HorizontalAlign="Right" GroupFooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ContaDescricao" Caption="Conta" UnboundType="String"
                                                VisibleIndex="11" Width="7%" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" />
                                            <dxwgv:GridViewDataColumn FieldName="IdAgente" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="Situacao" Visible="False" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCliente" Text='<%# Eval("IdCliente") %>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <buttons>                                           
                                        <dxe:EditButton />
                                    </buttons>
                                                                        <clientsideevents keypress="function(s, e) {window.tmp_MultiContaPendente = true; document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                            buttonclick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" gotfocus="function(s, e) {
                                             
                                                if(!window.tmp_CodigoCliente){
                                                    window.tmp_CodigoCliente=btnEditCodigoCliente.GetValue();
                                                }
                                                else if(window.tmp_CodigoCliente !== btnEditCodigoCliente.GetValue()){ 
                                                    window.tmp_CodigoCliente = btnEditCodigoCliente.GetValue();
                                                    window.tmp_MultiContaPendente = true; 
                                                    
                                                }
                                                
                                             }" lostfocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" valuechanged="function(s, e) {if (gridCadastro.cpMultiConta == 'true' ) 
                                                                           { dropConta.PerformCallback(); }
                                                                        }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelSituacao" runat="server" CssClass="labelRequired" Text="Situação:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropSituacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_5" Text='<%#Eval("Situacao")%>'>
                                                                        <clientsideevents lostfocus="function(s, e) {if(s.GetSelectedIndex() == -1) 
                                                                                    s.SetText(null);}">
                                                </clientsideevents>
                                                                        <items>
                                    <dxe:ListEditItem Value="1" Text="Normal" />
                                    <dxe:ListEditItem Value="2" Text="Pendente" />
                                    </items>
                                                                        <clientsideevents selectedindexchanged="function(s, e) { if (s.GetSelectedIndex() == 1)
                                                                                { textDataVencimento.SetEnabled(false); textDataVencimento.SetValue(null);
                                                                                  dropEventoVencimento.SetEnabled(false); dropEventoVencimento.SetValue(null)}
                                                                            else
                                                                                {textDataVencimento.SetEnabled(true);}
                                                                            }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataLancamento" runat="server" CssClass="labelRequired" Text="Lançamento:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataLancamento" runat="server" ClientInstanceName="textDataLancamento"
                                                                        Value='<%#Eval("DataLancamento")%>' OnInit="textDataLancamento_Init" ClientEnabled="false">
                                                                        <clientsideevents
                                                                            ValueChanged="function(s, e) { OnDataAlterada(s);}" />
                                                                    </dxe:ASPxDateEdit>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelRequired" Text="Vencimento:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                        Value='<%#Eval("DataVencimento")%>'>
                                                                        <clientsideevents
                                                                            ValueChanged="function(s, e) { OnDataAlterada(s);}"
                                                                            Init="function(s, e) { OnDataAlterada(s);}" />
                                                                    </dxe:ASPxDateEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"> </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="textDescricao" runat="server" CssClass="textDescricao" Text='<%#Eval("Descricao")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="textValor" runat="server" ClientInstanceName="textValor" Text='<%# Eval("Valor") %>' DisplayFormatString="N"
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="2" CssClass="textValor_5">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelMoeda" runat="server" CssClass="labelNormal" Text="Moeda:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropMoeda" runat="server" ClientInstanceName="dropMoeda" DataSourceID="EsDSMoeda"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                        TextField="Nome" ValueField="IdMoeda">
                                                                        <clientsideevents valuechanged="function(s, e) {if (gridCadastro.cpMultiConta == 'True' ) 
                                                                                { dropConta.PerformCallback(); }
                                                                           }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelConta" runat="server" CssClass="labelRequired" Text="Conta:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropConta" runat="server" ClientInstanceName="dropConta" EnableSynchronizationOnPerformCallback="true"
                                                                        DataSourceID="EsDSContaCorrente" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto" TextField="Numero" ValueField="IdConta" Text='<%#Eval("IdConta") %>'
                                                                        OnCallback="dropConta_Callback">
                                                                        <validationsettings>
                                        <RequiredField IsRequired="True" />
                                        </validationsettings>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAgenteMercado" runat="server" CssClass="labelNormal" Text="Liquidante:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropAgenteMercado" runat="server" ClientInstanceName="dropAgenteMercado"
                                                                        DataSourceID="EsDSAgenteMercado" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListLongo" TextField="Nome" ValueField="IdAgente" Text='<%#Eval("IdAgente")%>'>
                                                                    </dxe:ASPxComboBox>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelEvento" runat="server" CssClass="labelNormal" Text="Evento Contábil:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropEvento" runat="server" ClientInstanceName="dropEvento"
                                                                        DataSourceID="EsDSEvento" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListLongo"
                                                                        TextField="Descricao" ValueField="IdEvento" Text='<%#Eval("IdEvento")%>'>
                                                                    </dxe:ASPxComboBox>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelEventoFinanceiro" runat="server" CssClass="labelNormal" Text="Evento Financeiro:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropEventoFinanceiro" runat="server" ClientInstanceName="dropEventoFinanceiro"
                                                                        DataSourceID="EsDSEventoFinanceiro" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListLongo"
                                                                        TextField="Descricao" ValueField="IdEventoFinanceiro" Text='<%#Eval("IdEventoFinanceiro")%>'>
                                                                    </dxe:ASPxComboBox>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal9" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="30" RightMargin="30">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSLiquidacao" runat="server" OnesSelect="EsDSLiquidacao_esSelect"
            LowLevelBind="True" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSEvento" runat="server" OnesSelect="EsDSEvento_esSelect" />
        <cc1:esDataSource ID="EsDSEventoFinanceiro" runat="server" OnesSelect="EsDSEventoFinanceiro_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDS_Sinacor" runat="server" OnesSelect="EsDS_Sinacor_esSelect"
            LowLevelBind="true" />
    </form>
</body>
</html>
