﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ContabConta, Financial.Web_Deploy" culture="en-us" uiculture="en-us" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';    
    
    function OnGetData(data) 
    {
        document.getElementById(gridCadastro.cpHiddenIdConta).value = data;
        ASPxCallbackConta.SendCallback(data);
        btnEditConta.Focus();        
        popupContabConta.HideWindow();
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallbackConta" runat="server" OnCallback="ASPxCallbackConta_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    if (e.result != null) { btnEditConta.SetValue(e.result); } } " />
        </dxcb:ASPxCallback>
        
         <%--<dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {                
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') {
                                
                    if ( resultSplit[1] != '') {
                                                                                
                        if (resultSplit[1] != null && resultSplit[1] != '') {                        
                            var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                            textDataOperacao.SetValue(newDate);
                        }                    
                        else {
                            textDataOperacao.SetValue(null);
                        }
                    }
                }
                
                if (gridCadastro.cpMultiConta == 'True' ) { dropConta.PerformCallback(); }
            }
        }        
        "/>
    </dxcb:ASPxCallback>
        --%>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Contas Contábeis"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdConta"
                                        DataSourceID="EsDSContabConta" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnPreRender="gridCadastro_PreRender" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdConta" Visible="false" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdPlano" Caption="Plano" VisibleIndex="1"
                                                Width="12%">
                                                <PropertiesComboBox DataSourceID="EsDSPlano" TextField="Descricao" ValueField="IdPlano" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoConta" Caption="Tipo Conta" VisibleIndex="2"
                                                Width="6%" ExportWidth="90" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="A" Text="<div title='Ativo'>Ativo</div>" />
                                                        <dxe:ListEditItem Value="P" Text="<div title='Passivo'>Passivo</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="32%"
                                                VisibleIndex="3" ExportWidth="250" Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit MaxLength="200" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="FuncaoConta" Caption="Função" VisibleIndex="4"
                                                Width="9%" ExportWidth="90" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Operacional'>Operacional</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Resultado Crédito'>Resultado Crédito</div>" />
                                                        <dxe:ListEditItem Value="21" Text="<div title='Resultado Débito'>Resultado Débito</div>" />
                                                        <dxe:ListEditItem Value="30" Text="<div title='Compensado'>Compensado</div>" />
                                                        <dxe:ListEditItem Value="40" Text="<div title='Patrimonial'>Patrimonial</div>" />
                                                        <dxe:ListEditItem Value="50" Text="<div title='Resultados Acum.'>Resultados Acum.</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="IdContaMae" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoMae" Caption="Código Conta Mãe" Width="12%"
                                                VisibleIndex="5" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Codigo" Caption="Código Conta" Width="12%"
                                                VisibleIndex="6" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                                                <PropertiesTextEdit MaxLength="50" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Digito" Caption="Digito Conta" Width="12%"
                                                VisibleIndex="7" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                                                <PropertiesTextEdit MaxLength="3" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoReduzida" Caption="Código Reduzida"
                                                Width="8%" VisibleIndex="8" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left">
                                                <PropertiesTextEdit MaxLength="8" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <div class="editForm">
                                                    <%--Para Conferir validade do idContaMae no Update--%>
                                                    <dxe:ASPxTextBox ID="textIdConta" ClientInstanceName="textIdConta" runat="server"
                                                        Visible="false" Text='<%#Eval("IdConta")%>' />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelIdPlano" runat="server" CssClass="labelRequired" Text="Plano:"  />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropPlano" ClientInstanceName="dropPlano" runat="server" DataSourceID="EsDSPlano" ValueField="IdPlano"  ValueType="System.Int32"
                                                                     TextFormatString="{0}"
                                                                     TextField="Descricao" CssClass="dropDownListLongo" Text='<%#Eval("IdPlano")%>'>
                                                                    <Columns>
                                                                        <dxe:ListBoxColumn Caption="Descrição" FieldName="Descricao" Name="Descricao" />
                                                                        <dxe:ListBoxColumn Caption="Valida Digito" FieldName="ValidaDigitoConta" Name="Company" />
                                                                    </Columns>
                                                                     <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                        {
                                                                            codigoPanel.PerformCallback();

                                                                        }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Tipo:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropTipoConta" ClientInstanceName="dropTipoConta" runat="server" ShowShadow="false" CssClass="dropDownListCurto_2"
                                                                    DropDownStyle="DropDownList" Text='<%# Eval("TipoConta") %>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="A" Text="Ativo" />
                                                                        <dxe:ListEditItem Value="P" Text="Passivo" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                    CssClass="textLongo5" MaxLength="200" Text='<%#Eval("Descricao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Categoria:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropFuncaoConta" ClientInstanceName="dropFuncaoConta" runat="server" ShowShadow="false" CssClass="dropDownListCurto_1"
                                                                    DropDownStyle="DropDownList" Text='<%# Eval("FuncaoConta") %>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Operacional" />
                                                                        <dxe:ListEditItem Value="20" Text="Resultado Crédito" />
                                                                        <dxe:ListEditItem Value="21" Text="Resultado Débito" />
                                                                        <dxe:ListEditItem Value="30" Text="Compensado" />
                                                                        <dxe:ListEditItem Value="40" Text="Patrimonial" />
                                                                        <dxe:ListEditItem Value="50" Text="Resultados Acum." />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIdContaMae" runat="server" CssClass="labelNormal" Text="Conta Mãe:" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="hiddenIdConta" runat="server" CssClass="hiddenField" Text='<%#Eval("IdContaMae")%>' />
                                                                <dxe:ASPxButtonEdit ID="btnEditConta" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditConta"
                                                                    ReadOnly="true" Width="150px" Text='<%#Eval("CodigoMae")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupContabConta.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelCodigoComDigito" runat="server" CssClass="labelNormal" Text="Código:" />
                                                            </td>
                                                            <td colspan="2">
                                                            <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="codigoPanel" ID="codigoPanel"
                                                                        OnCallback="codigoPanel_Callback">
                                                                        <PanelCollection>
                                                                            <dxp:PanelContent runat="server">
                                                                <dxe:ASPxTextBox ID="textCodigo" ClientInstanceName="textCodigo" runat="server" CssClass="textNormal10" 
                                                                Value='<%#Eval("Codigo")%>'> 
                                                                <ClientSideEvents TextChanged="function(s, e) {digitoPanel.PerformCallback();}" />
                                                                </dxe:ASPxTextBox>
                                                                 </dxp:PanelContent>
                                                                        </PanelCollection>
                                                                    </dxcp:ASPxCallbackPanel>
                                                            </td>
                                                            <td colspan="3">
                                                                    <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="digitoPanel" ID="digitoPanel" 
                                                                        OnCallback="digitoPanel_Callback">
                                                                        <PanelCollection>
                                                                            <dxp:PanelContent runat="server">
                                                                                
                                                                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Digito Controle:" />
                                                                                <dxe:ASPxTextBox ID="textDigito" runat="server" ClientInstanceName="textDigito" CssClass="textNormal10" ReadOnly="true"
                                                                                    Value='<%#Eval("Digito")%>' />
                                                                                
                                                                            </dxp:PanelContent>
                                                                        </PanelCollection>
                                                                    </dxcp:ASPxCallbackPanel>
                                                                </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelCodigoReduzida" runat="server" CssClass="labelNormal" Text="Código Reduzida:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textCodigoReduzida" ClientInstanceName="textCodigoReduzida"
                                                                    runat="server" MaxLength="8" Text='<%#Eval("CodigoReduzida")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal12" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
            <SettingsPopup EditForm-Width="370px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="40" RightMargin="40" />
        <cc1:esDataSource ID="EsDSContabConta" runat="server" OnesSelect="EsDSContabConta_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSPlano" runat="server" OnesSelect="EsDSPlano_esSelect" />
    </form>
</body>
</html>
