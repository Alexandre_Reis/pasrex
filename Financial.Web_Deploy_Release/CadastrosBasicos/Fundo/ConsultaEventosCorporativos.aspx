﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ConsultaEventosCorporativos, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    function LimpaCampos() 
    {
        textDataInicio.SetValue(null);        
        textDataFim.SetValue(null);
        dropFundoInvestimento.SetValue(null);
        dropEventoTransformacaoMudancaClassificacao.SetValue(null);
    }    
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                gridCadastro.PerformCallback();                    
            }
            
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <form id="form1" runat="server">
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Consulta de Eventos Corporativos, Transformações e Mudanças na Classificação de Fundos de Investimento"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <table border="0">
                                    <tr>
                                        <td class="td_Label">
                                            <dxe:ASPxLabel ID="labelPeriodoInicio" runat="server" ClientInstanceName="labelPeriodoInicio"
                                                CssClass="labelRequired" Text="Período Inicio:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_Label">
                                            <dxe:ASPxLabel ID="labelPeriodoFim" runat="server" ClientInstanceName="labelPeriodoFim"
                                                CssClass="labelRequired" Text="Período Fim:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelFundoInvestimento" runat="server" Text="Fundo de Investimento:"> </asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropFundoInvestimento" runat="server" ClientInstanceName="dropFundoInvestimento"
                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" DataSourceID="EsDSCarteira"
                                                TextField="Nome" ValueType="System.Int32" ValueField="IdCarteira">
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelEventoTransformacaoMudancaClassificacao" runat="server" Text="Evento/Transformação/Mudança Classificação:"> </asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropEventoTransformacaoMudancaClassificacao" runat="server"
                                                ClientInstanceName="dropEventoTransformacaoMudancaClassificacao" ShowShadow="false"
                                                OnPreRender="gridCadastro_OnPreRender" OnLoad="dropEventoTransformacaoMudancaClassificacao_OnLoad"
                                                DropDownStyle="DropDown" CssClass="dropDownListLongo">
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <div class="linkButton linkButtonNoBorder popupFooter">
                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                    OnClientClick="if (operacao != '') return false; callbackErro.SendCallback(); return false;">
                                                    <asp:Literal ID="Literal3" runat="server" Text="Consultar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                    OnClientClick="LimpaCampos();">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText"
                                    OnCustomCallback="gridCadastro_CustomCallback" OnPageIndexChanged="gridCadastro_PageIndexChanged" OnLoad="gridCadastro_OnLoad" Width="100%">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" Visible="false" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Administrador" FieldName="Administrador" VisibleIndex="1" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fundo" FieldName="Fundo" VisibleIndex="2" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Nome" ValueField="IdCarteira" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" VisibleIndex="3" Width="5%">
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Evento/Transformação/Mudança" FieldName="EventoTransformacaoMudanca" VisibleIndex="4" Width="10%">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fundo Destino" FieldName="FundoDestino" VisibleIndex="5" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Nome" ValueField="IdCarteira" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Classificação Tributária Anterior" FieldName="ClassificacaoTributariaAnterior" VisibleIndex="6" Width="10%">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Classificação Tributária Atual" FieldName="ClassificacaoTributariaAtual" VisibleIndex="7" Width="10%">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataColumn FieldName="ValorCotaAnterior" Caption="Valor da Cota Anterior" VisibleIndex="8" />
                                        <dxwgv:GridViewDataColumn FieldName="ValorCotaAtual" Caption="Valor da Cota Atual" VisibleIndex="9" />
                                        <dxwgv:GridViewDataColumn FieldName="TotalIR" Caption="Total de IR" VisibleIndex="10" />
                                        <dxwgv:GridViewDataColumn FieldName="TipoFundoCarteira" Visible="False" />
                                        <dxwgv:GridViewDataColumn FieldName="TipoFundoCondominio" Visible="False" />
                                    </Columns>
                                    <SettingsPopup EditForm-Width="40%" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton>
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
    </form>
</body>
</html>
