﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_OrdemFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }
    function OnGetDataCarteira(data) {       
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function ClearTextObservacao()
    {
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = '';                                                                                            
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else if (operacao == 'aprovar')
                {
                    gridCadastro.PerformCallback('btnAprova');
                }
                else if (operacao == 'cancelar')
                {
                    gridCadastro.PerformCallback('btnCancela');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }          
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {  
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else
            {          
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
            }
            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ordens de Fundos (Aplicações e Resgates)"></asp:Label>
    </div>
        
    <div id="mainContent">   
            
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    
                    <table>
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td  colspan="2" width="450">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>        
                    
                    <tr>
                    <td>                
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                    </td>    
                                        
                    <td>
                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/>
                    </td>  
                    
                    <td colspan="2">
                    <table>
                        <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                        <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                    </table>
                    </td>                                                                          
                    </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>
            
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback(operacao);} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal14" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnAprova" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnTick" OnClientClick="if (confirm('Tem certeza que quer aprovar as ordens?')==true) {operacao='aprovar'; callbackErro.SendCallback(operacao);} return false;"><asp:Literal ID="Literal5" runat="server" Text="Aprovar Ordens"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnCancela" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnCross" OnClientClick="if (confirm('Tem certeza que quer cancelar as ordens?')==true) {operacao='cancelar'; callbackErro.SendCallback(operacao);} return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar Ordens"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
            
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdOperacao" DataSourceID="EsDSOrdemFundo"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"   
                        OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"       
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"
                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                        OnInitNewRow="gridCadastro_InitNewRow"
                        >
                                            
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id"  VisibleIndex="1" Width="6%" CellStyle-HorizontalAlign="Left"/>                        
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCliente" Caption="Cliente" VisibleIndex="4" Width="17%"/>
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Fundo" VisibleIndex="5" Width="17%"/>                                                
                        <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="8" Width="10%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="TipoOperacao" VisibleIndex="10" Width="7%" ExportWidth="120">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="<div title='Aplicação'>Aplicação</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Resgate Bruto'>Resg. Bruto</div>" />
                            <dxe:ListEditItem Value="3" Text="<div title='Resgate Líquido'>Resg. Líquido</div>" />
                            <dxe:ListEditItem Value="4" Text="<div title='Resgate Cotas'>Resg. Cotas</div>" />
                            <dxe:ListEditItem Value="5" Text="<div title='Resgate Total'>Resg. Total</div>" />                            
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Vl Bruto" VisibleIndex="12" Width="10%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="14" Width="10%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Vl Líquido" VisibleIndex="16" Width="10%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String" VisibleIndex="18" Width="8%"
                                            HeaderStyle-HorizontalAlign="Center" FooterCellStyle-HorizontalAlign="Center"
                                            CellStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains"/>
                                                
                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="DataConversao" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="TipoResgate" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="IdFormaLiquidacao" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="IdPosicaoResgatada" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="Status" Visible="false"/>
                        <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false"/>
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm">                            
                            
                            <table>                            
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                    </td>        
                                    
                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente" 
                                                CssClass="textButtonEdit" Text='<%#Eval("IdCliente")%>' MaxLength="10" NumberType="Integer">            
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                                 />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                    
                                    <td colspan="4">
                                        <asp:TextBox ID="textNomeCliente" CssClass="textNome" runat="server" Enabled="false" Text='<%#Eval("ApelidoCliente")%>' ></asp:TextBox>
                                    </td>                                        
                                </tr>
                                        
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                    </td>        
                                    
                                    <td>                                        
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>' 
                                                    MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                
                                    <td colspan="4">
                                        <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCarteira")%>' ></asp:TextBox>
                                    </td>                                    
                                </tr>                                                                        
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td>      
                                        <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ClientInstanceName="dropTipoOperacao" 
                                                            ShowShadow="true" DropDownStyle="DropDownList" 
                                                            CssClass="dropDownListCurto_2" Text='<%#Eval("TipoOperacao")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Aplicação" />
                                        <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                                        <dxe:ListEditItem Value="3" Text="Resgate Líquido" />
                                        <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                                        <dxe:ListEditItem Value="5" Text="Resgate Total" />                                        
                                        </Items>
                                        <ClientSideEvents
                                                 SelectedIndexChanged="function(s, e) { ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
                                                                            if (s.GetSelectedIndex() == 0 || s.GetSelectedIndex() == 5 || s.GetSelectedIndex() == 6 || s.GetSelectedIndex() == 8)
                                                                                { dropTipoResgate.SetSelectedIndex(-1); dropTipoResgate.SetEnabled(false); ClearTextObservacao(); }
                                                                            else
                                                                                {dropTipoResgate.SetEnabled(true);}    
                                                                            }"
                                                />                                                                                      
                                        </dxe:ASPxComboBox>                 
                                    </td>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                            DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown" 
                                                            CssClass="dropDownListCurto_2" TextField="Nome" ValueField="IdTrader"
                                                            Text='<%#Eval("IdTrader")%>'>
                                          <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } " />
                                        </dxe:ASPxComboBox>      
                                    </td>                                  
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelValor" runat="server" CssClass="labelNormal" Text="Valor/Qtde:"></asp:Label>
                                    </td>
                                    <td >
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                         MaxLength="28" NumberType="Float">            
                                        </dxe:ASPxSpinEdit>
                                    </td>                                  
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="labelFormaLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida por:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                            DataSourceID="EsDSFormaLiquidacao" ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownListCurto_5" TextField="Descricao" ValueField="IdFormaLiquidacao"
                                                            Text='<%# Eval("IdFormaLiquidacao") %>'>
                                        </dxe:ASPxComboBox>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Mov:"> </asp:Label>
                                    </td>                
                                    <td colspan="3">
                                        <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                            DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown"
                                                            CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                            Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                        </dxe:ASPxComboBox>         
                                    </td>
                                </tr>
                                                                
                                <tr>
                                     <td>&nbsp</td>
                                    <td colspan="2">
                                        <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="INFORMADO PELO GESTOR"> </asp:Label>
                                    </td>
                                    <td colspan="2"></td>
                                 </tr>
                                 
                                 <tr>   
                                    <td>&nbsp</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" MaxLength="400" Rows="8" CssClass="textLongo5" Text='<%#Eval("Observacao")%>'/>
                                    </td>                    
                                 </tr>
                                
                            </table>
                            
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK+"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="600px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSOrdemFundo" runat="server" OnesSelect="EsDSOrdemFundo_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSFormaLiquidacao" runat="server" OnesSelect="EsDSFormaLiquidacao_esSelect" />        
    <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />        
    <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
    
    </form>
</body>
</html>