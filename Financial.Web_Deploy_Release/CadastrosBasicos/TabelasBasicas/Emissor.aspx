﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Emissor, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = false;
    var operacao = '';
    document.onkeydown=onDocumentKeyDown;
    
    function OnGetDataPessoa(data) 
    {
        btnEditCodigoPessoa.SetValue(data);
        popupPessoa.HideWindow();
        callBackPessoa.SendCallback(btnEditCodigoPessoa.GetValue());
        btnEditCodigoPessoa.Focus();
    }
    
    function HabilitaCampo(enabled)
    {
        textCNPJ.SetEnabled(enabled);                    
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCamposPessoa" runat="server" OnCallback="CamposPessoa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {  
                var enabled = (e.result == 'S');                  
                HabilitaCampo(enabled);
                btnEditCodigoPessoa.SetEnabled(enabled);               
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPessoa" runat="server" OnCallback="pessoa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {   
               var parametros = e.result.split('|');
               
               e.result = parametros[0];
               textNome.SetValue(parametros[1]);               
               textCNPJ.SetValue(parametros[2]);
               HabilitaCampo(false);

               var textApelido = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelido');                                                                                               
               OnCallBackCompleteCliente(s, e, popupMensagemPessoa, btnEditCodigoPessoa, textApelido);                                                     
            }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Emissores"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdEmissor"
                                        DataSourceID="EsDSEmissor" OnCustomCallback="gridCadastro_CustomCallback" OnRowInserting="gridCadastro_RowInserting"
                                        OnRowUpdating="gridCadastro_RowUpdating">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdEmissor" Width="5%" CellStyle-HorizontalAlign="Center"
                                                VisibleIndex="1">
                                                <EditItemTemplate>
                                                    <dxe:ASPxLabel ID="lblEmissor" runat="server" Value='<%# Bind("IdEmissor") %>'>
                                                    </dxe:ASPxLabel>
                                                </EditItemTemplate>
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdPessoa" Width="5%" CellStyle-HorizontalAlign="Center"
                                                VisibleIndex="2">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Nome" Width="25%" VisibleIndex="3">
                                                <PropertiesTextEdit MaxLength="100">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdSetor" Caption="Setor" VisibleIndex="4"
                                                Width="15%">
                                                <PropertiesComboBox DataSourceID="EsDSSetor" TextField="Nome" ValueField="IdSetor">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEmissor" VisibleIndex="5" Width="10%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="União" />
                                                        <dxe:ListEditItem Value="2" Text="Estado" />
                                                        <dxe:ListEditItem Value="3" Text="Município" />
                                                        <dxe:ListEditItem Value="4" Text="P.Física" />
                                                        <dxe:ListEditItem Value="5" Text="P.Jurídica" />
                                                        <dxe:ListEditItem Value="6" Text="Inst.Financeira" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Cnpj" Width="10%" VisibleIndex="6">
                                                <PropertiesTextEdit MaxLength="20">
                                                    <ValidationSettings RequiredField-ErrorText="">
                                                    </ValidationSettings>
                                                    <ValidationSettings ErrorText="">
                                                    </ValidationSettings>
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Agente Mercado" VisibleIndex="7"
                                                Width="20%">
                                                <PropertiesComboBox DataSourceID="EsDSAgente" TextField="Nome" ValueField="IdAgente">
                                                    <ValidationSettings RequiredField-ErrorText="">
                                                    </ValidationSettings>
                                                    <ValidationSettings ErrorText="">
                                                    </ValidationSettings>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoInterface" Caption="Código Interface"
                                                Width="10%" VisibleIndex="8">
                                                <PropertiesTextEdit MaxLength="11">
                                                    <ValidationSettings RequiredField-ErrorText="">
                                                    </ValidationSettings>
                                                    <ValidationSettings ErrorText="">
                                                    </ValidationSettings>
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ApelidoPessoa" Caption="Apelido Pessoa" UnboundType="String" Visible="false"></dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPessoa" runat="server" CssClass="labelRequired" Text="Pessoa:"
                                                                    OnLoad="labelPessoa_Load"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit" ClientSideEvents-Init="function(s, e) { callBackCamposPessoa.SendCallback(); }"
                                                                    ClientInstanceName="btnEditCodigoPessoa" Text='<%# Eval("IdPessoa") %>'
                                                                    MaxLength="10" NumberType="Integer" Width="100%">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelido').value = '';} "
                                                                        ButtonClick="function(s, e) {popupPessoa.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemPessoa, callBackPessoa, btnEditCodigoPessoa);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="textApelido" runat="server" CssClass="textLongo" Enabled="False"
                                                                    Text='<%# Eval("ApelidoPessoa") %>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelNome" runat="server" AssociatedControlID="textNome" CssClass="labelRequired"
                                                                    Text="Nome:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textNome" ClientInstanceName="textNome" runat="server" Width="100%"
                                                                    MaxLength="50" Text='<%#Eval("Nome")%>'>
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblSetor" runat="server" CssClass="labelRequired" AssociatedControlID="dropSetor"
                                                                    Text="Setor:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropSetor" ClientInstanceName="dropSetor" Width="100%" runat="server"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("IdSetor")%>' DataSourceID="EsDSSetor"
                                                                    TextField="Nome" ValueField="IdSetor">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoEmissor" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoEmissor"
                                                                    Text="Tipo Emissor:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropTipoEmissor" Width="100%" runat="server" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoEmissor")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="União" />
                                                                        <dxe:ListEditItem Value="2" Text="Estado" />
                                                                        <dxe:ListEditItem Value="3" Text="Município" />
                                                                        <dxe:ListEditItem Value="4" Text="P.Física" />
                                                                        <dxe:ListEditItem Value="5" Text="P.Jurídica" />
                                                                        <dxe:ListEditItem Value="6" Text="Inst.Financeira" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblCNPJ" runat="server" CssClass="labelNormal" AssociatedControlID="textCNPJ"
                                                                    Text="CNPJ:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textCNPJ" ClientInstanceName="textCNPJ" runat="server" CssClass="textNormal"
                                                                    Text='<%#Eval("CNPJ")%>' MaxLength="18">
                                                                    <ClientSideEvents KeyPress="function(s,e) 
                                                                                        {
                                                                                            var cnpj    = textCNPJ.GetText();
                                                                                                cnpj = cnpj.replace(/\D/g,'');                     
                                                                                                cnpj = cnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cnpj = cnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cnpj = cnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCNPJ.SetValue(cnpj);
	                                                                                    }" />
                                                                    <ClientSideEvents Init="function(s,e) 
                                                                                        {
                                                                                            var cnpj = textCNPJ.GetText();
                                                                                                cnpj = cnpj.replace(/\D/g,'');                     
                                                                                                cnpj = cnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cnpj = cnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cnpj = cnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCNPJ.SetValue(cnpj);
	                                                                                    }" />
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblAgenteMercado" runat="server" CssClass="labelNormal" AssociatedControlID="dropAgenteMercado"
                                                                    Text="Agente Mercado:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxComboBox ID="dropAgenteMercado" ClientInstanceName="dropAgenteMercado" Width="100%"
                                                                    runat="server" CssClass="dropDownListCurto" Text='<%#Eval("IdAgente")%>' DataSourceID="EsDSAgente"
                                                                    TextField="Nome" ValueField="IdAgente">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblCodigoInterface" runat="server" CssClass="labelNormal" AssociatedControlID="textCodigoInterface"
                                                                    Text="Código Interface:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textCodigoInterface" ClientInstanceName="textCodigoInterface" MaxLength="40"
                                                                    Width="100%" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("CodigoInterface")%>'>
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="450px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSEmissor" runat="server" OnesSelect="EsDSEmissor_esSelect" />
        <cc1:esDataSource ID="EsDSSetor" runat="server" OnesSelect="EsDSSetor_esSelect" />
        <cc1:esDataSource ID="EsDSAgente" runat="server" OnesSelect="EsDSAgente_esSelect" />
        <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect" />
    </form>
</body>
</html>
