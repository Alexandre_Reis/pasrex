﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_OperacaoSwap, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;      
    var operacao = '';
      
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }  
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }   
    
    function OnGetDataAtivo(data) 
    {
        hiddenIdAtivo.SetValue(data);
        callBackPopupAtivo.SendCallback(data);                   
        popupAtivo.HideWindow();
        btnEditAtivo.Focus();
    }    
       
    function OnGetDataAtivoContraParte(data) 
    {
        hiddenIdAtivoContraParte.SetValue(data);
        callBackPopupAtivoContraParte.SendCallback(data); 
        popupAtivoContraParte.HideWindow();
        btnEditAtivoContraParte.Focus();        
    } 
       
    function ValidaPontaAtivo()
    {
        if(dropTipoPonta.GetSelectedItem() != null)
        {
            var tipoPonta = dropTipoPonta.GetSelectedItem().value;
            
            if(tipoPonta == '4') //Bolsa
            {
                DesabilitaCamposPontaAtivoIndexadores(true);                
                return;
            }
            else if(tipoPonta == '5') //Fundo
            {            
                DesabilitaCamposPontaAtivoIndexadores(true);
                return;
            }
        }

        DesabilitaCamposPontaAtivoIndexadores(false);            
    }       
     
    function HabilitaCamposReset() 
    {
        var habilita = false; 
               
        if(dropRealizaReset.GetSelectedItem() != null)
        {
            var realizaReset = dropRealizaReset.GetSelectedItem().value;
            var habilita = realizaReset == 'S';
        }
         
        var cssClass = habilita ? 'labelRequired' : 'labelNormal';
        
        textDataInicioReset.SetEnabled(habilita);
        textQtdePeriodicidade.SetEnabled(habilita);
        dropPeriodicidadeReset.SetEnabled(habilita);
        lblDataInicioReset.GetMainElement().className = cssClass;
        lblQtdePeriodicidade.GetMainElement().className = cssClass;
    }
        
    function DesabilitaCamposPontaAtivoIndexadores(desabilita)
    {
        dropTipoApropriacao.SetEnabled(!desabilita);
        dropContagemDias.SetEnabled(!desabilita);
        dropBaseAno.SetEnabled(!desabilita);
        textPercentual.SetEnabled(!desabilita);
        dropIndice.SetEnabled(!desabilita);
        textTaxaJuros.SetEnabled(!desabilita);
        textValorIndicePartida.SetEnabled(!desabilita);
                
        var cssIndexadores = ''; 
        var cssAtivo = ''; 
        
        if(!desabilita) //Se desabilita campos relacionado a indexadores
        {
            cssIndexadores = 'labelRequired';
            cssAtivo = 'labelNormal';
            btnEditAtivo.SetEnabled(false);                        
        }
        else
        {
            cssIndexadores = 'labelNormal';
            cssAtivo = 'labelRequired';
            btnEditAtivo.SetEnabled(true);
        }
        
        labelCalculo.GetMainElement().className = cssIndexadores;
        labelTipoApropriacao.GetMainElement().className = cssIndexadores;
        labelContagemDias.GetMainElement().className = cssIndexadores;
        labelBaseAno.GetMainElement().className = cssIndexadores;
        labelPercentual.GetMainElement().className = cssIndexadores;
        labelTaxaJuros.GetMainElement().className = cssIndexadores;
        labelValorIndicePartida.GetMainElement().className = cssIndexadores;
        lblAtivo.GetMainElement().className = cssAtivo;
    }    
   
    function ValidaPontaPassivo()
    {
        if(dropTipoPonta2.GetSelectedItem() != null)
        {
            var tipoPonta = dropTipoPonta2.GetSelectedItem().value;
            
            if(tipoPonta == '4') //Bolsa
            {
                DesabilitaCamposPontaPassivoIndexadores(true);
                return;
            }
            else if(tipoPonta == '5') //Fundo
            {            
                DesabilitaCamposPontaPassivoIndexadores(true);
                return;
            }
        }

        DesabilitaCamposPontaPassivoIndexadores(false);            
    }
   
    function DesabilitaCamposPontaPassivoIndexadores(desabilita)
    {         
        dropTipoApropriacao2.SetEnabled(!desabilita);
        dropContagemDias2.SetEnabled(!desabilita);
        dropBaseAno2.SetEnabled(!desabilita);
        textPercentual2.SetEnabled(!desabilita);
        dropIndice2.SetEnabled(!desabilita);
        textTaxaJuros2.SetEnabled(!desabilita);
        textValorIndicePartida2.SetEnabled(!desabilita);
                        
        var cssIndexadores = ''; 
        var cssAtivo = ''; 
        
        if(!desabilita) //Se desabilita campos relacionado a indexadores
        {
            cssIndexadores = 'labelRequired';
            cssAtivo = 'labelNormal';
            btnEditAtivoContraParte.SetEnabled(false);                        
        }
        else
        {
            cssIndexadores = 'labelNormal';
            cssAtivo = 'labelRequired';
            btnEditAtivoContraParte.SetEnabled(true);
        }
        
        labelCalculo2.GetMainElement().className = cssIndexadores;
        labelTipoApropriacao2.GetMainElement().className = cssIndexadores;
        labelContagemDias2.GetMainElement().className = cssIndexadores;
        labelBaseAno2.GetMainElement().className = cssIndexadores;
        labelPercentual2.GetMainElement().className = cssIndexadores;
        labelTaxaJuros2.GetMainElement().className = cssIndexadores;
        labelValorIndicePartida2.GetMainElement().className = cssIndexadores;
        lblAtivoContraParte.GetMainElement().className = cssAtivo;
    }    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callBackPopupAtivo" runat="server" OnCallback="callbackAtivo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { btnEditAtivo.SetValue(e.result); }" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupAtivoContraParte" runat="server" OnCallback="callbackAtivoContraParte_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { btnEditAtivoContraParte.SetValue(e.result); }" />
        </dxcb:ASPxCallback >              
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }                   
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                            
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                if (gridCadastro.cp_EditVisibleIndex == 'new')
                {                    
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textDataEmissao); 
                    textDataRegistro.SetValue(textDataEmissao.GetValue());
                    textDataInicioReset.SetValue(textDataEmissao.GetValue());
                }
                else
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
                }
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupAtivo" ClientInstanceName="popupAtivo" runat="server"
            Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControlAtivo" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridAtivo" runat="server" Width="100%" ClientInstanceName="gridAtivo"
                            AutoGenerateColumns="False" DataSourceID="EsDSAtivo" KeyFieldName="IdAtivo"
                            OnCustomDataCallback="gridAtivo_CustomDataCallback" OnCustomCallback="gridAtivo_CustomCallback"
                            OnHtmlRowCreated="gridAtivo_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdAtivo" Caption="IdAtivo" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) { gridAtivo.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataAtivo);}" 
                                              Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Ativo." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridAtivo.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>        
        <dxpc:ASPxPopupControl ID="popupAtivoContraParte" ClientInstanceName="popupAtivoContraParte" runat="server"
            Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControlAtivoContraParte" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridAtivoContraParte" runat="server" Width="100%" ClientInstanceName="gridAtivoContraParte"
                            AutoGenerateColumns="False" DataSourceID="EsDSAtivoContraParte" KeyFieldName="IdAtivo"
                            OnCustomDataCallback="gridAtivoContraParte_CustomDataCallback" OnCustomCallback="gridAtivoContraParte_CustomCallback"
                            OnHtmlRowCreated="gridAtivoContraParte_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdAtivo" Caption="IdAtivo" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) { gridAtivoContraParte.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataAtivoContraParte);}" 
                                              Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Ativo." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridAtivoContraParte.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>             
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Operações de Swap"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOperacaoSwap" 
                                        OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" 
                                        OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" 
                                        OnInitNewRow="gridCadastro_InitNewRow"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id.Operação" VisibleIndex="3" Width="6%"/>
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="22%"/>
                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" Caption="Registro" VisibleIndex="4" Width="8%" />
                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="5" Width="8%" />
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="Percentual" Caption="% Índice" VisibleIndex="6" Width="6%" 
                                                    HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>                                                
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="7" Width="12%">
                                                <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice"/>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="TaxaJuros" Caption="Taxa" VisibleIndex="8" Width="5%" 
                                                    HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.00);0.0000}"/>
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="PercentualContraParte" Caption="% Índice2" VisibleIndex="9" Width="8%" 
                                                    HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndiceContraParte" Caption="Índice2" VisibleIndex="10" Width="10%">
                                                <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice"/>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="TaxaJurosContraParte" Caption="Taxa2" VisibleIndex="11" Width="5%" 
                                                    HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.00);0.0000}"/>                                                
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorBase" VisibleIndex="12" Width="11%"
                                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>                                                
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoRegistro" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdAgente" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="NumeroContrato" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ComGarantia" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoPonta" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoApropriacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ContagemDias" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="BaseAno" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoPontaContraParte" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoApropriacaoContraParte" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ContagemDiasContraParte" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="BaseAnoContraParte" Visible="false" />
                                            
                                            <dxwgv:GridViewDataColumn FieldName="ValorIndicePartida" Visible="true"  />
                                            <dxwgv:GridViewDataColumn FieldName="ValorIndicePartidaContraParte" Visible="true"  />
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdEstrategia" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DataEmissao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Ativo" Visible="false" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="AtivoContraParte" Visible="false" UnboundType="String" />
                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxe:ASPxTextBox ID="hiddenIdAtivo" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdAtivo" Text='<%#Eval("Ativo")%>' />
                                                        <dxe:ASPxTextBox ID="hiddenIdAtivoContraParte" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdAtivoContraParte" Text='<%#Eval("AtivoContraParte")%>' />
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoRegistro" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoRegistro" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoRegistro")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="BMF" />
                                                                            <dxe:ListEditItem Value="2" Text="Cetip" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Data Registro:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                        ClientEnabled="false" Value='<%#Eval("DataRegistro")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAgente" runat="server" CssClass="labelNormal" Text="Agente:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropAgente" runat="server" ClientInstanceName="dropAgente"
                                                                        DataSourceID="EsDSAgente" IncrementalFilteringMode="Contains" ShowShadow="false"
                                                                        DropDownStyle="DropDown" CssClass="dropDownListLongo" TextField="Nome" ValueField="IdAgente"
                                                                        Text='<%#Eval("IdAgente")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) { 
                                                                                if(s.GetSelectedIndex() == -1)
                                                                                    s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelNumeroContrato" runat="server" CssClass="labelRequired" Text="Contrato:"></asp:Label>
                                                                </td>
                                                                
                                                                <td colspan="3">
                                                                    <table border="0">
                                                                        <tr>                                                                                                                                                                                                
                                                                            <td>
                                                                                <asp:TextBox ID="textNumeroContrato" runat="server" CssClass="textValor_15" MaxLength="20"
                                                                                    Text='<%#Eval("NumeroContrato")%>' />
                                                                            </td>
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="labelComGarantia" runat="server" CssClass="labelNormal_5" Text="C/ Garantia?:"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropComGarantia" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                    CssClass="dropDownListCurto_6" Text='<%#Eval("ComGarantia")%>'>
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                    </Items>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                         </tr>
                                                                    </table>    
                                                                 </td>   
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataEmissao" runat="server" CssClass="labelRequired" Text="Emissão:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataEmissao" runat="server" ClientInstanceName="textDataEmissao"
                                                                       Value='<%#Eval("DataEmissao")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelRequired" Text="Vencimento:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                        Value='<%#Eval("DataVencimento")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValorBase" runat="server" CssClass="labelRequired" Text="Valor Base:"/>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorBase" runat="server" CssClass="textValor_5" ClientInstanceName="textValorBase" DisplayFormatString="N"
                                                                        MaxLength="12" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorBase")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Estratégia:"/>
                                                                </td>                                                                
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropEstrategia" runat="server" ClientInstanceName="dropEstrategia"
                                                                        DataSourceID="EsDSEstrategia" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListCurto"
                                                                        TextField="Descricao" ValueField="IdEstrategia" Text='<%# Eval("IdEstrategia") %>'
                                                                        ValueType="System.String">
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="lblRealizaReset" ClientInstanceName="lblRealizaReset" runat="server" CssClass="labelRequired" Text="Realiza Reset:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropRealizaReset" ClientInstanceName="dropRealizaReset" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("RealizaReset")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                        <ClientSideEvents Init="function(s, e){ HabilitaCamposReset(); }" SelectedIndexChanged="function(s, e){ HabilitaCamposReset(); }"/>                                                                     
                                                                    </dxe:ASPxComboBox>
                                                                </td>                                                       
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="lblDataInicioReset" ClientInstanceName="lblDataInicioReset" runat="server" CssClass="labelNormal" Text="Data Início Reset:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataInicioReset" runat="server" ClientInstanceName="textDataInicioReset" Value='<%#Eval("DataInicioReset")%>' />
                                                                </td>                                                                
                                                            </tr>   
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
																            <td class="td_Label">
                                                                                <dxe:ASPxLabel ID="lblQtdePeriodicidade" ClientInstanceName="lblQtdePeriodicidade" runat="server" CssClass="labelNormal" Text="Período a cada:"></dxe:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                 <dxe:ASPxSpinEdit ID="textQtdePeriodicidade" runat="server" CssClass="textCurto" ClientInstanceName="textQtdePeriodicidade"
                                                                                    MaxLength="8" NumberType="Integer" Text='<%#Eval("QtdePeriodicidade")%>'>
                                                                                </dxe:ASPxSpinEdit>  
                                                                            </td>                                                         
                                                                            <td>
																	            <dxe:ASPxComboBox ID="dropPeriodicidadeReset" ClientInstanceName="dropPeriodicidadeReset" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
																		            CssClass="dropDownListCurto_2" Text='<%#Eval("PeriodicidadeReset")%>'>
																		            <Items>
																			            <dxe:ListEditItem Value="1" Text="Dias" />
																			            <dxe:ListEditItem Value="2" Text="Meses" />
																			            <dxe:ListEditItem Value="3" Text="Anos" />
																		            </Items>
																	            </dxe:ASPxComboBox>
																            </td>                                                                                                                                                                            
                                                                        </tr>    
                                                                    </table>   
                                                                </td>   
                                                            </tr>                                                
															<tr>
																<td class="td_Label">
                                                                    <asp:Label ID="label25" runat="server" AssociatedControlID="textCodigoIsin"
                                                                        CssClass="labelNormal" Text="Cód. ISIN:" />
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textCodigoIsin" runat="server" CssClass="textValor_5" MaxLength="12"
                                                                        Text='<%#Eval("CodigoIsin")%>'></asp:TextBox>
                                                                </td>

																<td class="td_Label">
                                                                    <asp:Label ID="label35" runat="server" AssociatedControlID="textCpfcnpjContraParte"
                                                                        CssClass="labelNormal" Text="Cpf/Cnpj Contraparte:" />
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textCpfcnpjContraParte" runat="server" CssClass="textValor_5" MaxLength="14"
                                                                        Text='<%#Eval("CpfcnpjContraParte")%>'></asp:TextBox>
                                                                </td>                                                                                                                                
															</tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Movimentação:"> </asp:Label>
                                                                </td>                
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                                                        DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                                                        CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                                                        Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                                                    </dxe:ASPxComboBox>         
                                                                </td>
                                                            </tr>     															
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="border-bottom: solid 1px #B7B7B7; width: 100%;">
                                                                        Ponta - Ativo</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoPonta" runat="server" CssClass="labelRequired" Text="Ponta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoPonta" ClientInstanceName="dropTipoPonta" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoPonta")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Pré-Fixado" />
                                                                            <dxe:ListEditItem Value="2" Text="Indexado" />
                                                                            <dxe:ListEditItem Value="3" Text="Indexado-D0" />
                                                                            <dxe:ListEditItem Value="4" Text="Ativo Bolsa" />
                                                                            <dxe:ListEditItem Value="5" Text="Fundo de Investimento" />
                                                                        </Items>
                                                                        <ClientSideEvents Init="function(s, e) { ValidaPontaAtivo(); }"  SelectedIndexChanged="function(s, e) { btnEditAtivo.SetValue(null);
                                                                                                                                                                                 hiddenIdAtivo.SetValue(null);
                                                                                                                                                                                 ValidaPontaAtivo(); 
                                                                                                                                                                               }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelTipoApropriacao" ClientInstanceName="labelTipoApropriacao" runat="server" CssClass="labelRequired" Text="Apropriação:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoApropriacao" ClientInstanceName="dropTipoApropriacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoApropriacao")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Exponencial" />
                                                                            <dxe:ListEditItem Value="2" Text="Linear" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelContagemDias" ClientInstanceName="labelContagemDias" runat="server" CssClass="labelRequired" Text="Contagem:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropContagemDias" ClientInstanceName="dropContagemDias" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("ContagemDias")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Dias Úteis" />
                                                                            <dxe:ListEditItem Value="2" Text="Dias Corridos" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelBaseAno" ClientInstanceName="labelBaseAno" runat="server" CssClass="labelRequired" Text="Base Ano:"></dxe:ASPxLabel >
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropBaseAno" ClientInstanceName="dropBaseAno" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_6" Text='<%#Eval("BaseAno")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="252" Text="252" />
                                                                            <dxe:ListEditItem Value="360" Text="360" />
                                                                            <dxe:ListEditItem Value="365" Text="365" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>                                                                                                                    
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelCalculo" ClientInstanceName="labelCalculo" runat="server" CssClass="labelRequired" Text="Cálculo:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dxe:ASPxSpinEdit ID="textPercentual" ClientInstanceName="textPercentual" runat="server" CssClass="textCurto" DisplayFormatString="N"
                                                                                    MaxLength="8" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Percentual")%>'>
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxLabel ID="labelPercentual" ClientInstanceName="labelPercentual" runat="server" CssClass="labelNormal" Text="%"></dxe:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropIndice" runat="server" ClientInstanceName="dropIndice" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                                                    DataSourceID="EsDSIndice" ShowShadow="false" CssClass="dropDownList" TextField="Descricao"
                                                                                    ValueField="IdIndice" Text='<%#Eval("IdIndice")%>'>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxLabel ID="labelTaxaJuros" ClientInstanceName="labelTaxaJuros" runat="server" CssClass="labelNormal" Text="+"></dxe:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxSpinEdit ID="textTaxaJuros" runat="server" CssClass="textCurto" ClientInstanceName="textTaxaJuros"
                                                                                    MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("TaxaJuros")%>'>
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelValorIndicePartida" ClientInstanceName="labelValorIndicePartida" runat="server" CssClass="labelNormal" Text="Índice Partida:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="textValorIndicePartida" runat="server" CssClass="textNormal"
                                                                        ClientInstanceName="textValorIndicePartida" MaxLength="20" NumberType="Float" AllowNull="false" MinValue="0" NullText="0"
                                                                        DecimalPlaces="10" Text='<%#Eval("ValorIndicePartida")%>'  >
                                                                    </dxe:ASPxSpinEdit>                                                                                                                                                                                                                                                                                
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>                                                                                                
                                                                <td  class="td_Label">
                                                                    <dxe:ASPxLabel ID="lblAtivo" ClientInstanceName="lblAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxButtonEdit ID="btnEditAtivo" runat="server" CssClass="textAtivo" Width="100%"
                                                                                        ClientInstanceName="btnEditAtivo" Text='<%#Eval("Ativo")%>' >            
                                                                    <Buttons>
                                                                        <dxe:EditButton></dxe:EditButton>                
                                                                    </Buttons>           
                                                                    <ClientSideEvents ButtonClick="function(s, e) {  gridAtivo.PerformCallback('btnRefresh'); popupAtivo.ShowAtElementByID(s.name);}" />                    
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>     
                                                            </tr>

                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="border-bottom: solid 1px #B7B7B7; width: 100%;">
                                                                        Ponta - Passivo</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoPonta2" runat="server" CssClass="labelRequired" Text="Ponta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoPonta2" ClientInstanceName="dropTipoPonta2" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoPontaContraParte")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Pré-Fixado" />
                                                                            <dxe:ListEditItem Value="2" Text="Indexado" />
                                                                            <dxe:ListEditItem Value="3" Text="Indexado-D0" />
                                                                            <dxe:ListEditItem Value="4" Text="Ativo Bolsa" />
                                                                            <dxe:ListEditItem Value="5" Text="Fundo de Investimento" />
                                                                        </Items>
                                                                        <ClientSideEvents Init="function(s, e) { ValidaPontaPassivo(); }" SelectedIndexChanged="function(s, e) { btnEditAtivoContraParte.SetValue(null);
                                                                                                                                                                                 hiddenIdAtivoContraParte.SetValue(null);
                                                                                                                                                                                 ValidaPontaPassivo(); 
                                                                                                                                                                               }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelTipoApropriacao2" ClientInstanceName="labelTipoApropriacao2" runat="server" CssClass="labelRequired" Text="Apropriação:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoApropriacao2" ClientInstanceName="dropTipoApropriacao2" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoApropriacaoContraParte")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Exponencial" />
                                                                            <dxe:ListEditItem Value="2" Text="Linear" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelContagemDias2" ClientInstanceName="labelContagemDias2" runat="server" CssClass="labelRequired" Text="Contagem:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropContagemDias2" ClientInstanceName="dropContagemDias2" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("ContagemDiasContraParte")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Dias Úteis" />
                                                                            <dxe:ListEditItem Value="2" Text="Dias Corridos" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelBaseAno2" ClientInstanceName="labelBaseAno2" runat="server" CssClass="labelRequired" Text="Base Ano:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropBaseAno2" ClientInstanceName="dropBaseAno2" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_6" Text='<%#Eval("BaseAnoContraParte")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="252" Text="252" />
                                                                            <dxe:ListEditItem Value="360" Text="360" />
                                                                            <dxe:ListEditItem Value="365" Text="365" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelCalculo2" ClientInstanceName="labelCalculo2" runat="server" CssClass="labelRequired" Text="Cálculo:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <dxe:ASPxSpinEdit ID="textPercentual2" runat="server" CssClass="textCurto" ClientInstanceName="textPercentual2" DisplayFormatString="N"
                                                                                    MaxLength="8" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("PercentualContraParte")%>'>
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxLabel ID="labelPercentual2" ClientInstanceName="labelPercentual2" runat="server" CssClass="labelNormal" Text="%"></dxe:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropIndice2" ClientInstanceName="dropIndice2" runat="server"
                                                                                    DataSourceID="EsDSIndice" ShowShadow="false" CssClass="dropDownList" TextField="Descricao"
                                                                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                                                    ValueField="IdIndice" Text='<%#Eval("IdIndiceContraParte")%>'>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxLabel ID="labelTaxaJuros2" ClientInstanceName="labelTaxaJuros2" runat="server" CssClass="labelNormal" Text="+"></dxe:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxSpinEdit ID="textTaxaJuros2" runat="server" CssClass="textCurto" ClientInstanceName="textTaxaJuros2"
                                                                                    MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("TaxaJurosContraParte")%>'>
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxLabel ID="labelValorIndicePartida2" ClientInstanceName="labelValorIndicePartida2" runat="server" CssClass="labelNormal" Text="Índice Partida:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="textValorIndicePartida2" runat="server" CssClass="textNormal"
                                                                        ClientInstanceName="textValorIndicePartida2" MaxLength="20" NumberType="Float" MinValue="0" NullText="0"
                                                                        DecimalPlaces="10" Text='<%#Eval("ValorIndicePartidaContraParte")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>                                                                                                
                                                                <td  class="td_Label">
                                                                    <dxe:ASPxLabel ID="lblAtivoContraParte" ClientInstanceName="lblAtivoContraParte" runat="server" CssClass="labelNormal" Text="Ativo:"></dxe:ASPxLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxButtonEdit ID="btnEditAtivoContraParte" runat="server" CssClass="textAtivo" Width="100%"
                                                                                        ClientInstanceName="btnEditAtivoContraParte" Text='<%#Eval("AtivoContraParte")%>' >            
                                                                    <Buttons>
                                                                        <dxe:EditButton></dxe:EditButton>                
                                                                    </Buttons>           
                                                                    <ClientSideEvents ButtonClick="function(s, e) { gridAtivoContraParte.PerformCallback('btnRefresh'); popupAtivoContraParte.ShowAtElementByID(s.name);}" />                    
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>     
                                                            </tr>                                                            
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal1" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                            
                                        </Templates>
                                        
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                        
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"/>
        
        <cc1:esDataSource ID="EsDSOperacaoSwap" runat="server" OnesSelect="EsDSOperacaoSwap_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSAgente" runat="server" OnesSelect="EsDSAgente_esSelect" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />        
        <cc1:esDataSource ID="EsDSAtivo" runat="server" OnesSelect="EsDSAtivo_esSelect" />      
        <cc1:esDataSource ID="EsDSAtivoContraParte" runat="server" OnesSelect="EsDSAtivoContraParte_esSelect" />      
    </form>
</body>
</html>