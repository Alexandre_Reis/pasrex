﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_AtivoBolsaHistorico, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">    
    popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Ativos (Histórico) de Opções"></asp:Label>
    </div>
        
    <div id="mainContent">
                
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
                        
        <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CdAtivoBolsa" DataSourceID="EsDSAtivoBolsaHistorico"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowUpdating="gridCadastro_RowUpdating"       
                    OnRowInserting="gridCadastro_RowInserting"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties">        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" Width="10%" VisibleIndex="0" />
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" Width="10%" VisibleIndex="3" />                    
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Width="75%" VisibleIndex="5" />
                    
                </Columns>
                
                <Templates>
                <EditForm>
                    
                    <div class="editForm">
                        
                        <table>                    
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCdAtivoBolsa" runat="server" AssociatedControlID="textCdAtivoBolsa" CssClass="labelRequired" Text="Código:" />                                    
                                </td>                                
                                <td>
                                    
                                    <dxe:ASPxTextBox ID="textCdAtivoBolsa" ClientInstanceName="textCdAtivoBolsa" runat="server" CssClass="textNormal_5" 
                                    MaxLength="20" Text='<%# Eval("CdAtivoBolsa") %>' OnInit="textCdAtivoBolsa_Init" />                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Referência:"></asp:Label>
                                </td>
                                                        
                                <td>                        
                                    <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" 
                                        Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init"/>
                                </td>
                                          
                                <td class="td_Label">
                                    <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelNormal" Text="Vencimento:"></asp:Label>
                                </td>
                                                        
                                <td>                        
                                    <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento" Value='<%#Eval("DataVencimento")%>'/>
                                </td>                                                    
                            </tr>
                             
                        </table>    
                    
                        <div class="linhaH"></div>
                        
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>    
                    
                    </div>                
                    
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSAtivoBolsaHistorico" runat="server" OnesSelect="EsDSAtivoBolsaHistorico_esSelect" />        
        
    </form>
</body>
</html>