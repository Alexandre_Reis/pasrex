﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_HistoricoLog, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    document.onkeydown=onDocumentKeyDownWithCallback;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Histórico de Log"></asp:Label>
    </div>
        
    <div id="mainContent">
                                        
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table cellpadding="2" cellspacing="2">        
                <tr>
                <td>                
                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                </td>    
                                    
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"   />    
                </td>  
                
                <td>
                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>
                </td>  
                
                <td>                
                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />    
                </td>                                                                            
                </tr>
                
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        
        
        <div class="linkButton" >               
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnCustomFields" OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Mais Campos"/><div></div></asp:LinkButton>
        </div>

        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdLog" DataSourceID="EsDSHistoricoLog"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        >
                            
                <Columns>
                     <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                         <HeaderTemplate>
                             <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                         </HeaderTemplate>
                     </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="DataInicio" Caption="Início" VisibleIndex="1" Width="10%" ShowInCustomizationForm="false">
                        <PropertiesDateEdit DisplayFormatString="g" >
                        </PropertiesDateEdit>
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="DataFim" Caption="Fim" VisibleIndex="2" Width="10%" ShowInCustomizationForm="false">
                        <PropertiesDateEdit DisplayFormatString="g" >
                        </PropertiesDateEdit>
                    </dxwgv:GridViewDataDateColumn>                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="3" Width="55%" ShowInCustomizationForm="false"/>                    
                    <dxwgv:GridViewDataTextColumn FieldName="Login" VisibleIndex="4" Width="10%" ShowInCustomizationForm="false" Settings-AutoFilterCondition="Contains" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="Maquina" Caption="Máquina" VisibleIndex="5" Width="10%" ShowInCustomizationForm="false"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Origem" Width="12%" Visible="false">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>                            
                            <dxe:ListEditItem Value="1" Text="<div title='Mudança de Senha'>Mudança de Senha</div>" />
                            <dxe:ListEditItem Value="100" Text="<div title='Processamento'>Processamento</div>" />
                            <dxe:ListEditItem Value="200" Text="<div title='Integração Itaú'>Integração Itaú</div>" />
							<dxe:ListEditItem Value="300" Text="<div title='Portal Institucional'>Portal Institucional</div>" />
							<dxe:ListEditItem Value="301" Text="<div title='Portal Cotista'>Portal Cotista</div>" />
                            <dxe:ListEditItem Value="999999" Text="<div title='Outros'>Outros</div>" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                
                    <dxwgv:GridViewDataComboBoxColumn FieldName="SubOrigem" Width="15%" Visible="false">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>                            
                            <dxe:ListEditItem Value="1" Text="<div title='Abertura(Sucesso)'>Abertura(Sucesso)</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Abertura(Erro)'>Abertura(Erro)</div>" />
                            <dxe:ListEditItem Value="3" Text="<div title='Cálculo(Sucesso)'>Cálculo(Sucesso)</div>" />
                            <dxe:ListEditItem Value="4" Text="<div title='Cálculo(Erro)'>Cálculo(Erro)</div>" />
                            <dxe:ListEditItem Value="5" Text="<div title='Fechamento(Sucesso)'>Fechamento(Sucesso)</div>" />
                            <dxe:ListEditItem Value="6" Text="<div title='Fechamento(Erro)'>Fechamento(Erro)</div>" />
                            <dxe:ListEditItem Value="7" Text="<div title='Avanço(Sucesso)'>Avanço(Sucesso)</div>" />
                            <dxe:ListEditItem Value="8" Text="<div title='Avanço(Erro)'>Avanço(Sucesso)</div>" />
                            <dxe:ListEditItem Value="9" Text="<div title='Retroação(Sucesso)'>Retroação(Sucesso)</div>" />
                            <dxe:ListEditItem Value="10" Text="<div title='Retroação(Erro)'>Retroação(Erro)</div>" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                </Columns>
                
                <Templates>
                <StatusBar>
                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                </StatusBar>  
                </Templates>  
                
                <SettingsBehavior EnableCustomizationWindow ="true" />
                <SettingsText CustomizationWindowCaption="Lista de campos" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                                
            </dxwgv:ASPxGridView>            
        </div>
             
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSHistoricoLog" runat="server" OnesSelect="EsDSHistoricoLog_esSelect" />    
        
    </form>
</body>
</html>