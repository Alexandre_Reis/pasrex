﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Book, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Books de Relatórios"></asp:Label>
    </div>
        
    <div id="mainContent">
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdBook" DataSourceID="EsDSBook" 
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnCustomJSProperties="gridCadastro_CustomJSProperties"
                OnBeforeGetCallbackResult="gridCadastro_PreRender">        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="40%" VisibleIndex="2"/>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="4" Width="10%" ExportWidth="120">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="<div title='Por Carteira'>Por Carteira</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Geral'>Geral</div>" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="EmailFrom" Caption="Email Origem" Width="45%" VisibleIndex="6"/>
            </Columns>
            
            <Templates>
                <EditForm>

                    <div class="editForm">    
                            
                        <table >                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"></asp:Label>
                                </td>
                                <td colspan="2">                                    
                                    <dxe:ASPxTextBox ID="textDescricao" runat="server" MaxLength="255" CssClass="textDescricao" Text='<%# Eval("Descricao") %>' />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipo" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                </td>
                                <td>      
                                    <dxe:ASPxComboBox ID="dropTipo" runat="server" ShowShadow="false" CssClass="dropDownList" 
                                                    Text='<%#Eval("Tipo")%>' OnLoad="dropTipo_Load">                                        
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Por Carteira" />
                                    <dxe:ListEditItem Value="2" Text="Geral" />
                                    </Items>                                                            
                                    </dxe:ASPxComboBox>                                                                 
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelEmailFrom" runat="server" CssClass="labelRequired" Text="Email Origem:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textEmailFrom" runat="server" MaxLength="100" CssClass="textDescricao" Text='<%# Eval("EmailFrom") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelEmail" runat="server" CssClass="labelNormal" Text="Email Destino:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textEmail" runat="server" MaxLength="1000" CssClass="textDescricao" Text='<%# Eval("Email") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAssunto" runat="server" CssClass="labelNormal" Text="Assunto:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textAssunto" runat="server" MaxLength="1000" CssClass="textDescricao" Text='<%# Eval("Assunto") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelMensagem" runat="server" CssClass="labelNormal" Text="Mensagem:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textMensagem" runat="server" MaxLength="2000" CssClass="textDescricao"
                                                Height="80px" TextMode="MultiLine" Rows="6"
                                                Text='<%# Eval("Mensagem") %>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>    
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>

                </EditForm>
                </Templates>
                                    
                <SettingsPopup EditForm-Width="500" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                    
                        
        </dxwgv:ASPxGridView>            
        </div>
           
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSBook" runat="server" OnesSelect="EsDSBook_esSelect" />
        
    </form>
</body>
</html>