﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PerfilCorretagemBMF, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }      
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                 
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackClone" runat="server" OnCallback="callbackClone_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {    
            alert('Clonagem executada');
            gridCadastro.UpdateEdit();
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Perfis de Corretagem (BMF)" />
    </div>
    
    <div id="mainContent">
               
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>

        <div class="divDataGrid">
        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
            KeyFieldName="CompositeKey" DataSourceID="EsDSPerfilCorretagemBMF"
            OnCustomCallback="gridCadastro_CustomCallback"
            OnRowInserting="gridCadastro_RowInserting"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnBeforeGetCallbackResult="gridCadastro_PreRender"
            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
            OnCustomJSProperties="gridCadastro_CustomJSProperties" >        
                
        <Columns>           
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="5%" ShowClearFilterButton="True">
                <HeaderTemplate>
                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                </HeaderTemplate>
            </dxwgv:GridViewCommandColumn>
            
            <dxwgv:GridViewDataTextColumn FieldName="IdPerfilCorretagem" VisibleIndex="0" Visible="false" />
            
            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left" />
            <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="20%" />
            
            <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="3" Width="18%">
                <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente" />
            </dxwgv:GridViewDataComboBoxColumn>
                                           
            <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="5" Width="10%"/>
                            
            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="6" Width="11%" ExportWidth="120">
                <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Disponível'>Disponível</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Futuro'>Futuro</div>" />
                        <dxe:ListEditItem Value="3" Text="<div title='Opção Disponível'>Opção Disponível</div>" />
                        <dxe:ListEditItem Value="4" Text="<div title='Opção Futuro'>Opção Futuro</div>" />
                        <dxe:ListEditItem Value="5" Text="<div title='Termo'>Termo</div>" />                            
                    </Items>
                </PropertiesComboBox>
            </dxwgv:GridViewDataComboBoxColumn>                               
                                           
            <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" VisibleIndex="7" Caption="Ativo BMF" Width="7%" Settings-AutoFilterCondition="Contains" />
                                            
            <dxwgv:GridViewDataTextColumn FieldName="GrupoAtivo" VisibleIndex="8" Width="8%" />                
            
            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagem" VisibleIndex="9" Width="11%" ExportWidth="110">
                <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='TOB'>TOB</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Valor Fixo'>VL. Fixo</div>" />
                    </Items>
                </PropertiesComboBox>
            </dxwgv:GridViewDataComboBoxColumn>
            
            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCorretagemDT" VisibleIndex="10" Width="12%" ExportWidth="110">
                <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='TOB'>TOB</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Valor Fixo'>VL. Fixo</div>" />
                    </Items>
                </PropertiesComboBox>
            </dxwgv:GridViewDataComboBoxColumn>
        </Columns>
        
        <Templates>
            <EditForm>
                <div class="editForm">       
                                                        
                    <asp:TextBox ID="textIdPerfilCorretagem" runat="server" CssClass="hiddenField" Text='<%#Eval("IdPerfilCorretagem")%>' />
                                       
                    <table>
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:" />
                            </td>        

                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente"
                                                            Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer" 
                                                            OnLoad="btnEditCodigoCliente_Load">
                                <Buttons>
                                    <dxe:EditButton />
                                </Buttons>
                                <ClientSideEvents
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                        /> 
                                </dxe:ASPxSpinEdit>
                            </td> 

                            <td>
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' />
                            </td>                               
                        </tr>                        
                                                                                                                                                                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref.:"  />
                            </td> 
                            <td colspan="2">
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init" />                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" AssociatedControlID="dropAgente" Text="Corretora:" />
                            </td>                                                    
                            <td colspan="2">
                                <dxe:ASPxComboBox ID="dropAgente" runat="server" DataSourceID="EsDSAgenteMercado"
                                                 ValueField="IdAgente" TextField="Nome" CssClass="dropDownList" Text='<%#Eval("IdAgente")%>' 
                                                 OnLoad="dropAgente_Load">
                                </dxe:ASPxComboBox>                                                                               
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelRequired" Text="Mercado:" />
                            </td>                    
                            <td colspan="2">
                                <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" ShowShadow="false" CssClass="dropDownListCurto"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                    Text='<%# Eval("TipoMercado") %>'>
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="Disponível" />
                                        <dxe:ListEditItem Value="2" Text="Futuro" />
                                        <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                                        <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                                        <dxe:ListEditItem Value="5" Text="Termo" />                                    
                                    </Items>                                                 
                                </dxe:ASPxComboBox>             
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo BMF:" />
                            </td>
                            <td colspan="2">
                                <dxe:ASPxButtonEdit ID="btnCdAtivo" runat="server" CssClass="textAtivoBMFCurto" ClientInstanceName="btnCdAtivo"
                                                    MaxLength="20" Text='<%# Eval("CdAtivoBMF") %>'>                                                        
                                </dxe:ASPxButtonEdit>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTipoCorretagem" runat="server" CssClass="labelRequired" Text="Normal:" />
                            </td>                    
                            <td colspan="2">
                                <dxe:ASPxComboBox ID="dropTipoCorretagem" runat="server" ShowShadow="false" CssClass="dropDownListCurto"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="startswith"
                                                    Text='<%# Eval("TipoCorretagem") %>'>
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="TOB" />
                                        <dxe:ListEditItem Value="2" Text="Valor Fixo" />                                                                   
                                    </Items>                                                 
                                </dxe:ASPxComboBox>             
                            </td>                       
                        </tr>
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelTipoCorretagemDT" runat="server" CssClass="labelRequired" Text="DayTrade:" />
                            </td>                    
                            <td colspan="2">
                                <dxe:ASPxComboBox ID="dropTipoCorretagemDT" runat="server" ShowShadow="false" CssClass="dropDownListCurto"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="startswith"
                                                    Text='<%# Eval("TipoCorretagemDT") %>'>
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="TOB" />
                                        <dxe:ListEditItem Value="2" Text="Valor Fixo" />                                                                   
                                    </Items>                                                 
                                </dxe:ASPxComboBox>             
                            </td>
                        </tr>
                        
                        <tr>                                 
                            <td  class="td_Label_Longo">                                        
                                <asp:Label ID="labelGrupoAtivo" runat="server" CssClass="labelNormal" Text="Grupo Ativo:"/>
                            </td>
                            <td colspan="2">
                                <dxe:ASPxButtonEdit ID="btnGrupoAtivo" runat="server" CssClass="textLongo" ClientInstanceName="btnGrupoAtivo"
                                                    MaxLength="100" Text='<%# Eval("GrupoAtivo") %>'>                                                        
                                </dxe:ASPxButtonEdit>
                            </td>                                                                  
                        </tr>
                    </table>
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-left:80px;">
                        <asp:LinkButton ID="btnClone" runat="server" Font-Overline="false" CssClass="btnCopy" OnInit="btnClone_Init"
                                                OnClientClick="callbackClone.SendCallback(); return false;">
                                <asp:Literal ID="Literal8" runat="server" Text="Clonar p/ Clientes Ativos"/><div></div></asp:LinkButton>
                    </div>
                    
                    <div class="linhaH"></div>
            
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"  OnInit="btnOKAdd_Init"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                </div>
                                
            </EditForm>                
                            
        </Templates>
        
        <SettingsPopup EditForm-Width="500px" />
        <SettingsCommandButton>
            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
        </SettingsCommandButton>
        
    </dxwgv:ASPxGridView>            
    </div>                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"  Landscape = "true"
    LeftMargin = "40"
    RightMargin = "40"    
    />
        
    <cc1:esDataSource ID="EsDSPerfilCorretagemBMF" runat="server" OnesSelect="EsDSPerfilCorretagemBMF_esSelect" LowLevelBind="true" />    
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>