﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_AtivoBMF, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Ativos de BMF"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSAtivoBMF" OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData" OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="12%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" Width="10%" VisibleIndex="1"
                                                Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit MaxLength="20">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Serie" Width="10%" VisibleIndex="2" Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit MaxLength="5">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="3" Width="20%"
                                                ExportWidth="130">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Disponivel'>Disponivel</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Futuro'>Futuro</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Opção Disponivel'>Opção Disponivel</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Opção Futuro'>Opção Futuro</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Termo'>Termo</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoIsin" Width="15%" VisibleIndex="4"
                                                Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoCusip" Caption="COSIP" Width="10%"
                                                VisibleIndex="5" Settings-AutoFilterCondition="Contains" CellStyle-HorizontalAlign="Left" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoSerie" Caption="Tipo Opção" VisibleIndex="6"
                                                Width="15%" ExportWidth="100">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="C" Text="<div title='Compra'>Compra</div>" />
                                                        <dxe:ListEditItem Value="V" Text="<div title='Venda'>Venda</div>" />
                                                        <dxe:ListEditItem Value="*" Text="<div title=''>*</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" Width="10%"
                                                VisibleIndex="7" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Moeda" FieldName="IdMoeda" VisibleIndex="8"
                                                Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSMoeda" TextField="Nome" ValueField="IdMoeda" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PrecoExercicio" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Peso" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdMoeda" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalCustodia" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdClearing" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalLiquidacao" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table border="0">
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCdAtivoBMF" runat="server" AssociatedControlID="textCdAtivoBMF"
                                                                    CssClass="labelRequired" Text="Código:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCdAtivoBMF" runat="server" CssClass="textNormal_5" MaxLength="20"
                                                                    ClientInstanceName="textCdAtivoBMF" Text='<%#Eval("CdAtivoBMF")%>' OnInit="textCdAtivoBMF_Init" />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelSerie" runat="server" AssociatedControlID="textSerie" CssClass="labelRequired"
                                                                    Text="Série:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textSerie" runat="server" ClientInstanceName="textSerie" CssClass="textCurto"
                                                                    MaxLength="5" Text='<%#Eval("Serie")%>' OnInit="textSerie_Init" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoMercado"
                                                                    Text="Mercado:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoMercado")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Disponível" />
                                                                        <dxe:ListEditItem Value="2" Text="Futuro" />
                                                                        <dxe:ListEditItem Value="3" Text="Opção Disponível" />
                                                                        <dxe:ListEditItem Value="4" Text="Opção Futuro" />
                                                                        <dxe:ListEditItem Value="5" Text="Termo" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoSerie" runat="server" CssClass="labelNormal" AssociatedControlID="dropTipoSerie"
                                                                    Text="Tipo Série:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoSerie" runat="server" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoSerie")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="C" Text="Compra" />
                                                                        <dxe:ListEditItem Value="V" Text="Venda" />
                                                                        <dxe:ListEditItem Value="*" Text="*" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelNormal" Text="Vencimento:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                    Value='<%#Eval("DataVencimento")%>' />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPrecoExercicio" runat="server" CssClass="labelNormal" Text="PU Exerc:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPrecoExercicio" runat="server" CssClass="textValor_4" ClientInstanceName="textPrecoExercicio"
                                                                    MaxLength="16" NumberType="Float" DecimalPlaces="4" Text="<%#Bind('PrecoExercicio')%> ">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPeso" runat="server" CssClass="labelRequired" Text="Peso:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPeso" runat="server" CssClass="textValor_4" ClientInstanceName="textPeso"
                                                                    MaxLength="16" NumberType="integer" Text="<%#Bind('Peso')%> ">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelMoeda" runat="server" CssClass="labelRequired" Text="Moeda:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropMoeda" runat="server" DataSourceID="EsDSMoeda" ValueField="IdMoeda"
                                                                    TextField="Nome" CssClass="dropDownListCurto" Text='<%#Eval("IdMoeda")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoFeeder" runat="server" CssClass="labelNormal" Text="Cód Feeder:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCodigoFeeder" runat="server" ClientInstanceName="textCodigoFeeder"
                                                                    CssClass="textValor_4" MaxLength="12" Text='<%#Eval("CodigoFeeder")%>' />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoIsin" runat="server" CssClass="labelNormal" Text="Cód. Isin:" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textCodigoIsin" runat="server" CssClass="textNormal_5" MaxLength="50"
                                                                    Text='<%#Eval("CodigoIsin")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEstrategia" runat="server" CssClass="labelRequired" Text="Estratégia:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEstrategia" runat="server" DataSourceID="EsDSEstrategia"
                                                                    ValueField="IdEstrategia" TextField="Descricao" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("IdEstrategia")%>' DropDownStyle="DropDown">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoCusip" runat="server" CssClass="labelNormal" Text="Cód. CUSIP:" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textCodigoCusip" runat="server" CssClass="textNormal_5" MaxLength="9"
                                                                    Text='<%#Eval("CodigoCusip")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblLocalCustodia" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalCustodia"
                                                                    Text="Local Custódia:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropLocalCustodia" runat="server" DataSourceID="EsDSLocalCustodia"
                                                                    ValueField="IdLocalCustodia" TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdLocalCustodia")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                    Text="Local Negociação:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao"
                                                                    ValueField="IdLocalNegociacao" TextField="Descricao" CssClass="dropDownList"
                                                                    Text='<%#Eval("IdLocalNegociacao")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblClearing" runat="server" CssClass="labelRequired" AssociatedControlID="dropClearing"
                                                                    Text="Clearing:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropClearing" runat="server" DataSourceID="EsDSClearing" ValueField="IdClearing"
                                                                    TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdClearing")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblCodigoCDA" runat="server" CssClass="labelNormal" Text="Codigo CDA:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textCodigoCDA" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoCDA"
                                                                    MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoCDA')%> ">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblCodigoBDS" runat="server" CssClass="labelNormal" Text="Codigo BDS:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textCodigoBDS" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoBDS"
                                                                    MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoBDS')%> ">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="Label36" runat="server" CssClass="labelNormal" Text="Investimento Coletivo – CVM:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropInvestimentoColetivoCvm" runat="server" ClientInstanceName="dropInvestimentoColetivoCvm"
                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("InvestimentoColetivoCvm") %>'
                                                                    ValueType="System.String">
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="300px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
						if (e.command == 'CUSTOMCALLBACK') {                            
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
						if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />
        <cc1:esDataSource ID="EsDSLocalCustodia" runat="server" OnesSelect="EsDSLocalCustodia_esSelect" />
        <cc1:esDataSource ID="EsDSClearing" runat="server" OnesSelect="EsDSClearing_esSelect" />
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />
    </form>
</body>
</html>
