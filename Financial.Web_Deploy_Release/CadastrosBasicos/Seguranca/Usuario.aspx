﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Usuario, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown=onDocumentKeyDown;
        var operacao = '';
        
        function OnGetDataCliente(data) {
           btnEditCodigoCliente.SetValue(data);        
           ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
           popupCliente.HideWindow();
        }
    </script>

    <script type="text/javascript" language="javascript">   
        function Uploader_OnUploadComplete(args) {
            if (args.callbackData != '') { 
                alert(args.callbackData);
            }
            else {
                alert('Logotipo definido com sucesso.');
                popupUsuarioLogotipo.Hide();
            }            
        }
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                                       
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_TextBoxNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);
            }
                
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackUsuariologotipo" runat="server" OnCallback="callbackUsuarioLogotipo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                uplLogotipoUsuario.UploadFile();
            }
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackUsuarioResetaLogotipo" runat="server" OnCallback="callbackUsuarioResetaLogotipo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
        }
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Usuários"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupUsuarioLogotipo" AllowDragging="true" PopupElementID="popupUsuarioLogotipo"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="350" Left="350" Top="50" HeaderText="Salva Logotipo Usuário"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0" width="350">
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxCheckBox ID="chkResetaLogo" ClientInstanceName="chkResetaLogo" runat="server"
                                                            Text="Reseta Logotipo" ToolTip="Reseta o Logotipo para os usuários selecionados do grid"
                                                            ClientSideEvents-CheckedChanged="function(s, e) {
                                                        uplLogotipoUsuario.SetVisible(!chkResetaLogo.GetChecked());
                                                        lblUsuario.SetVisible(!chkResetaLogo.GetChecked());
                                                        
                                                        if(chkResetaLogo.GetChecked()) {
                                                            document.getElementById('divButton').style.display = 'none'; // invisible
                                                            document.getElementById('divButton1').style.display = 'block'; // visible
                                                        }
                                                        else {
                                                            document.getElementById('divButton').style.display = 'block'; // visible
                                                            document.getElementById('divButton1').style.display = 'none'; // invisible
                                                        }
                                                    }" />
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxLabel ID="lblUsuario" ClientInstanceName="lblUsuario" runat="server" CssClass="labelBold"
                                                            Text="Salva Logotipo Usuário" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="line-height: 32px;">
                                                            <dxuc:ASPxUploadControl ID="uplLogotipoUsuario" runat="server" ClientInstanceName="uplLogotipoUsuario"
                                                                OnFileUploadComplete="uplLogotipoUsuario_FileUploadComplete" Width="200px" size="25"
                                                                FileUploadMode="OnPageLoad" EnableDefaultAppearance="False">
                                                                <validationsettings maxfilesize="100000000" />
                                                                <clientsideevents fileuploadcomplete="function(s, e) { Uploader_OnUploadComplete(e);  }" />
                                                            </dxuc:ASPxUploadControl>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="divButton" class="linkButton linkButtonNoBorder" style="margin-left: 40px;
                                                margin-top: 20px">
                                                <asp:LinkButton ID="btnRunlogotipo" runat="server" Font-Overline="false" CssClass="btnRun"
                                                    OnClientClick="
                                    {
                                        if(uplLogotipoUsuario.GetText()== '') {
                                            alert('Entre com o arquivo de Logotipo!');                                                                            
                                        }
                                        else {                                                                    
                                            callbackUsuariologotipo.SendCallback();                                                                             
                                        }
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal11" runat="server" Text="Salvar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnLogotipoUsuario" runat="server" Font-Overline="false" CssClass="btnClear"
                                                    OnClientClick="
                                    {
                                        uplLogotipoUsuario.ClearText();
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal14" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                            <div id="divButton1" class="linkButton linkButtonNoBorder" style="margin-left: 40px;
                                                margin-top: 20px; display: none;">
                                                <asp:LinkButton ID="btnRunlogotipoReset" runat="server" Font-Overline="false" CssClass="btnRun"
                                                    OnClientClick="
                                    {
                                        callbackUsuarioResetaLogotipo.SendCallback();                                                                                                                    
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal15" runat="server" Text="Salvar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit"
                                        OnClientClick="if (confirm('Tem certeza que quer ativar o(s) usuário(s)?')==true) gridCadastro.PerformCallback('btnAtivar');return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Ativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer desativar o(s) usuário(s)?')==true) gridCadastro.PerformCallback('btnDesativar');return false;">
                                        <asp:Literal ID="Literal7" runat="server" Text="Desativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesbloqueia" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnLockOpen" OnClientClick=" if (confirm('Tem certeza que quer desbloquear o(s) usuário(s)?')==true) gridCadastro.PerformCallback('btnDesbloqueia');return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Desbloqueia" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnLogOut" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnLockDelete" OnClientClick=" if (confirm('Tem certeza que quer forçar o logoff do(s) usuário(s)?')==true) gridCadastro.PerformCallback('btnLogout');return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Força Logout" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnUsuarioLogotipo" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupUsuarioLogotipo.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal5" runat="server" Text="Logotipo Usuário" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdUsuario"
                                        DataSourceID="EsDSUsuario" OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="13%" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdUsuario" Caption="Id" Width="6%" VisibleIndex="3"
                                                CellStyle-HorizontalAlign="Left">
                                                <PropertiesTextEdit MaxLength="60" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Nome" Width="20%" VisibleIndex="5">
                                                <PropertiesTextEdit MaxLength="60" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Login" Width="15%" VisibleIndex="7" Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit MaxLength="60" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Email" Width="15%" VisibleIndex="9" ExportWidth="250"
                                                Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit MaxLength="60" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdGrupo" Caption="Grupo" VisibleIndex="11"
                                                Width="13%">
                                                <PropertiesComboBox DataSourceID="EsDSGrupoUsuario" TextField="Descricao" ValueField="IdGrupo"
                                                    ValueType="System.String" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="13"
                                                Width="6%" ExportWidth="65">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Bloqueado" FieldName="StatusBloqueado"
                                                VisibleIndex="15" Width="6%" ExportWidth="100">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Logado" Caption="Logado" UnboundType="String"
                                                VisibleIndex="17" Width="6%" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelNome" runat="server" AssociatedControlID="textNome" CssClass="labelRequired"
                                                                    Text="Nome:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textNome" ClientInstanceName="textNome" runat="server" CssClass="textNormal"
                                                                    MaxLength="100" Text='<%#Eval("Nome")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelLogin" runat="server" AssociatedControlID="textLogin" CssClass="labelRequired"
                                                                    Text="Login:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textLogin" runat="server" CssClass="textNormal" MaxLength="50" Text='<%#Eval("Login")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelSenha" runat="server" CssClass="labelNormal" AssociatedControlID="textSenha"
                                                                    Text="Senha:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textSenha" runat="server" TextMode="Password" CssClass="textNormal"
                                                                    MaxLength="255"></asp:TextBox>
                                                                <br />
                                                                <asp:Label ID="labelInfoSenha" runat="server" CssClass="labelNormal" Text="(Deixe em branco, se não for editar)"
                                                                    Style="color: #58607C" OnInit="labelInfoSenha_Init" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelConfirmaSenha" runat="server" CssClass="labelNormal" AssociatedControlID="textConfirmaSenha"
                                                                    Text="Confirma Senha:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textConfirmaSenha" runat="server" TextMode="Password" CssClass="textNormal"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEnviaEmailAcesso" runat="server" CssClass="labelRequired" AssociatedControlID="dropEnviaEmailAcesso"
                                                                    Text="Enviar Senha via Email? :">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEnviaEmailAcesso" runat="server" CssClass="dropDownListCurto">
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Sim" Selected="true" />
                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" AssociatedControlID="dropAtivo"
                                                                    Text="Ativo? :">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropAtivo" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("StatusAtivo")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Ativo" />
                                                                        <dxe:ListEditItem Value="N" Text="Inativo" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEmail" runat="server" AssociatedControlID="textEmail" CssClass="labelNormal"
                                                                    Text="Email">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textEmail" runat="server" CssClass="textNormal" MaxLength="200"
                                                                    Text='<%#Eval("Email")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoTrava" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoTrava"
                                                                    Text="Tipo Trava:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoTrava" runat="server" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoTrava")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Sem Trava" />
                                                                        <dxe:ListEditItem Value="3" Text="Trava Cliente" />
                                                                        <dxe:ListEditItem Value="2" Text="Trava Cotista" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelGrupoUsuario" runat="server" CssClass="labelRequired" AssociatedControlID="dropGrupoUsuario"
                                                                    Text="Grupo:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropGrupoUsuario" runat="server" DataSourceID="EsDSGrupoUsuario"
                                                                    ValueField="IdGrupo" TextField="Descricao" CssClass="dropDownListCurto" Text='<%#Eval("IdGrupo")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                            </td>
                                                            <td colspan=3>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit textButtonEditFloat"
                                                                    ClientInstanceName="btnEditCodigoCliente" MaxLength="10" NumberType="Integer"
                                                                    Text='<%# Eval("IdCliente") %>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_TextBoxNomeCliente').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                </dxe:ASPxSpinEdit>
                                                                <asp:TextBox ID="TextBoxNomeCliente" runat="server" CssClass="inputAfterFloat" Enabled="false"
                                                                    Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Longo">
                                                                <asp:Label ID="labelMaximoAplicacao" runat="server" CssClass="labelNormal" Text="Máximo Aplic.:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textMaximoAplicacao" runat="server" CssClass="textValor_4"
                                                                    ClientInstanceName="textMaximoAplicacao" MaxLength="12" NumberType="integer"
                                                                    Text='<%#Eval("MaximoAplicacao")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Longo">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Máximo Resgate:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textMaximoResgate" runat="server" CssClass="textValor_4"
                                                                    ClientInstanceName="textMaximoResgate" MaxLength="12" NumberType="integer"
                                                                    Text='<%#Eval("MaximoResgate")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="textSenha"
                                                                    ControlToValidate="textConfirmaSenha" Display="Dynamic" ErrorMessage="Senha e Confirmação da Senha precisam ser iguais."
                                                                    ValidationGroup="ATK"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="dataMessage">
                                                        <asp:ValidationSummary ID="validationSummary1" runat="server" EnableClientScript="true"
                                                            DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="CreateUserWizard1" />
                                                    </div>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="380px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
                                                if (e.command == 'CUSTOMCALLBACK') {                            
                                                    isCustomCallback = true;
                                                }
                                            }" EndCallback="function(s, e) {
                                                if (isCustomCallback) {
                                                    isCustomCallback = false;
                                                    s.Refresh();
                                                }
                                            }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="35" RightMargin="30" />
        <cc1:esDataSource ID="EsDSUsuario" runat="server" OnesSelect="EsDSUsuario_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoUsuario" runat="server" OnesSelect="EsDSGrupoUsuario_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    </form>
</body>
</html>
