﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TransferenciaEntreCotista, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    {
        var popup = true;    
        document.onkeydown=onDocumentKeyDownWithCallback;
        var operacao = '';
          
        function OnGetDataCotistaOrigem(data) 
        {    
            hiddenCotistaOrigem.SetValue(data);     
            callBackPopupCotistaOrigem.SendCallback(data);
            popupCotistaOrigem.HideWindow();
            btnEditCodigoCotistaOrig.Focus();                                                
        }   

        function OnGetDataCotistaDestino(data) 
        {   
            hiddenCotistaDestino.SetValue(data);     
            callBackPopupCotistaDestino.SendCallback(data);
            popupCotistaDestino.HideWindow();
            btnEditCodigoCotistaDest.Focus();
            
        }
        
        function CloseGridLookup() 
        {
            dropPosicaoCotista.ConfirmCurrentSelection();
            dropPosicaoCotista.HideDropDown();
            dropPosicaoCotista.Focus();
        }   
        
        function LimpaTodosCampos()
        {               
            hiddenCotistaDestino.SetValue(null);  
            btnEditCodigoCotistaDest.SetValue(null);
            dropPosicaoCotista.GetGridView().UnselectAllRowsOnPage();                    
        }    
        
        function AtualizaGridLookup() 
        {            
            callBackAtualizaFiltro.PerformCallback();   
            dropPosicaoCotista.GetGridView().UnselectAllRowsOnPage(); 
            dropPosicaoCotista.GetGridView().PerformCallback();    
        }   
            
    }     
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxpc:ASPxPopupControl ID="popupCotistaOrigem" ClientInstanceName="popupCotistaOrigem"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridCotistaOrigem" runat="server" Width="100%" ClientInstanceName="gridCotistaOrigem"
                            AutoGenerateColumns="False" DataSourceID="EsDSCotistaOrigem" KeyFieldName="IdSerie"
                            OnCustomDataCallback="gridCotistaOrigem_CustomDataCallback" OnCustomCallback="gridCotistaOrigem_CustomCallback"
                            OnHtmlRowCreated="gridCotistaOrigem_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdCotista" Caption="Id.Cotista" VisibleIndex="0"
                                    Width="10%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Apelido" VisibleIndex="1" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) { LimpaTodosCampos;
                    gridCotistaOrigem.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCotistaOrigem);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cotista." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridCotistaOrigem.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupCotistaDestino" ClientInstanceName="popupCotistaDestino"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridCotistaDestino" runat="server" Width="100%" ClientInstanceName="gridCotistaDestino"
                            AutoGenerateColumns="False" DataSourceID="EsDSCotistaDestino" KeyFieldName="IdSerie"
                            OnCustomDataCallback="gridCotistaDestino_CustomDataCallback" OnCustomCallback="gridCotistaDestino_CustomCallback"
                            OnHtmlRowCreated="gridCotistaDestino_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdCotista" Caption="Id.Cotista" VisibleIndex="0"
                                    Width="10%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Apelido" VisibleIndex="1" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                    gridCotistaDestino.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCotistaDestino);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cotista." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridCotistaDestino.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackAtualizaFiltro" runat="server" OnCallback="callBackAtualizaFiltro_Callback">
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupCotistaOrigem" runat="server" OnCallback="callBackPopupCotistaOrigem_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                                                    if (e.result != null) 
                                                                    {
                                                                        btnEditCodigoCotistaOrig.SetValue(e.result); 
                                                                        AtualizaGridLookup();
                                                                    }
                                                                } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupCotistaDestino" runat="server" OnCallback="callBackPopupCotistaDestino_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCodigoCotistaDest.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Transferência de Cotas entre Cotistas"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdTransferenciaEntreCotista"
                                        DataSourceID="EsDSTransferenciaEntreCotista" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnCellEditorInitialize="gridCadastro_CellEditorInitialize"
                                        OnPreRender="gridCadastro_PreRender" AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTransferenciaEntreCotista" ReadOnly="True"
                                                VisibleIndex="1" Caption="Id.Transferência" Width="6%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataProcessamento" VisibleIndex="2" Caption="Data Execução"
                                                Width="6%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ApelidoOrigem" Caption="Origem" VisibleIndex="3"
                                                UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoDestino" Caption="Destino" VisibleIndex="4"
                                                UnboundType="String">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdAplicacao" Caption="Id.Aplicação" VisibleIndex="5"
                                                UnboundType="String" Width="6%">
                                            </dxwgv:GridViewDataTextColumn>                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="IdPosicao" Caption="Posição Afetada" VisibleIndex="5"
                                                UnboundType="String" Width="6%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacaoRetirada" Caption="Operação Retirada"
                                                VisibleIndex="6" UnboundType="String" Width="6%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacaoDeposito" Caption="Operação Depósito"
                                                VisibleIndex="7" UnboundType="String" Width="6%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCotistaOrigem" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCotistaDestino" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenCotistaOrigem" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCotistaOrigem")%>'
                                                        ClientInstanceName="hiddenCotistaOrigem" />
                                                    <dxe:ASPxTextBox ID="hiddenCotistaDestino" runat="server" CssClass="hiddenField"
                                                        Text='<%#Eval("IdCotistaDestino")%>' ClientInstanceName="hiddenCotistaDestino" />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCotistaOrig" runat="server" CssClass="labelRequired" Text="Cotista Origem:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxButtonEdit ID="btnEditCodigoCotistaOrig" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCotistaOrig" Text='<%#Eval("ApelidoOrigem")%>'
                                                                    EnableClientSideAPI="True" Width="380px">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) { popupCotistaOrigem.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCotistaDest" runat="server" CssClass="labelRequired" Text="Cotista Destino:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxButtonEdit ID="btnEditCodigoCotistaDest" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCotistaDest" Text='<%#Eval("ApelidoDestino")%>'
                                                                    EnableClientSideAPI="True" Width="380px">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) { popupCotistaDestino.ShowAtElementByID(s.name); gridCotistaDestino.PerformCallback('btnRefresh'); }" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPosicoes" runat="server" CssClass="labelRequired" Text="Posições:"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxgv:ASPxGridLookup ID="dropPosicaoCotista" runat="server" SelectionMode="Multiple"
                                                                    ClientInstanceName="dropPosicaoCotista" Width="340px" TextFormatString="{0}" 
                                                                    MultiTextSeparator=", " EnableClientSideAPI="true" KeyFieldName="IdPosicao" DataSourceID="EsDSPosicaoCotista">
                                                                    <Columns>
                                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                                        <dxwgv:GridViewDataColumn FieldName="IdPosicao" Caption="Id.Posição" />
                                                                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id.Carteira" />
                                                                        <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" Width="40%" />
                                                                        <dxwgv:GridViewDataColumn FieldName="DataProcessamento" Caption="Data Último processamento" />
                                                                        <dxwgv:GridViewDataColumn FieldName="DataAplicacao" Caption="Data Aplicação" />
                                                                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Width="12%"
                                                                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                            <propertiestextedit displayformatstring="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </propertiestextedit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Valor Bruto" VisibleIndex="9"
                                                                            Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">
                                                                            <propertiestextedit displayformatstring="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </propertiestextedit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" Caption="Valor IOF" VisibleIndex="9"
                                                                            Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">
                                                                            <propertiestextedit displayformatstring="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </propertiestextedit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorIR" Caption="Valor IR" VisibleIndex="9"
                                                                            Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">
                                                                            <propertiestextedit displayformatstring="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </propertiestextedit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Valor Líquido" VisibleIndex="9"
                                                                            Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">
                                                                            <propertiestextedit displayformatstring="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </propertiestextedit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <GridViewProperties>
                                                                    <SettingsPager PageSize="5" />
                                                                        <Templates>
                                                                            <StatusBar>
                                                                                <table class="OptionsTable" style="float: right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dxe:ASPxButton ID="Fechar" runat="server" AutoPostBack="false" Text="Fechar" ClientSideEvents-Click="CloseGridLookup" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </StatusBar>
                                                                        </Templates>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                    </GridViewProperties>
                                                                    <ClientSideEvents DropDown="function(s, e) { }" Init="AtualizaGridLookup" />
                                                                </dxgv:ASPxGridLookup>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550px" />
                                        <ClientSideEvents Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"
                                            BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSTransferenciaEntreCotista" runat="server" OnesSelect="EsDSTransferenciaEntreCotista_esSelect" />
        <cc1:esDataSource ID="EsDSCotistaOrigem" runat="server" OnesSelect="EsDSCotistaOrigem_esSelect" />
        <cc1:esDataSource ID="EsDSCotistaDestino" runat="server" OnesSelect="EsDSCotistaDestino_esSelect" />
        <cc1:esDataSource ID="EsDSPosicaoCotista" runat="server" OnesSelect="EsDSPosicaoCotista_esSelect" />
    </form>
</body>
</html>
