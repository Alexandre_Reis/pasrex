﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Cliente, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;   
    var operacao = ''; 
    var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
	var isUpdateEdit = false;   // usada para controlar a msg de impacto após edição do nome/documento da pessoa
     
    function OnGetDataPessoa(data) {
        btnEditCodigoPessoa.SetValue(data);
        popupPessoa.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoPessoa.GetValue());
        btnEditCodigoPessoa.Focus();
    }       
    </script>

    <script type="text/javascript">
        function OnKeyDown(s, e) {
            if ((e.htmlEvent.keyCode < 48 || e.htmlEvent.keyCode > 57) 
                && e.htmlEvent.keyCode != 0 && e.htmlEvent.keyCode != 8  && e.htmlEvent.keyCode != 9 && e.htmlEvent.keyCode != 13
                && e.htmlEvent.keyCode != 188 && e.htmlEvent.keyCode != 35 && e.htmlEvent.keyCode != 36
                && e.htmlEvent.keyCode != 37 && e.htmlEvent.keyCode != 39 && e.htmlEvent.keyCode != 46)
                return PreventEvent(e.htmlEvent);
        }
        function PreventEvent(evt) {
            if (evt.preventDefault)
                evt.preventDefault();
            else
                evt.returnValue = false;
            return false;
        }
    </script>

    <script type="text/javascript">
      // popup CriaCopia - função para checar checkbox todos
      function ChecaTodos() {
            var todos = chkBolsa.GetChecked() == true && chkBMF.GetChecked()== true &&
                        chkRendaFixa.GetChecked()== true && chkLiquidacao.GetChecked()== true &&
                        chkFundo.GetChecked()== true && chkHistoricoCota.GetChecked()== true;
                  
            chkTodos.SetChecked(todos);                       
      }
    </script>

    <script type="text/javascript" language="javascript">   
        function Uploader_OnUploadComplete(args) {
            if (args.callbackData != '') { 
                alert(args.callbackData);
            }
            else {
                alert('Logotipo definido com sucesso.');
                popupClienteLogotipo.Hide();
            }            
        }
        
        function HabilitaCampoIndiceAbertura()
        {
            if (dropAberturaIndexada.GetSelectedItem().value == 0) { 
                dropIndiceAbertura.SetSelectedIndex(-1); 
                dropIndiceAbertura.SetEnabled(false); 
            }
            else {
                dropIndiceAbertura.SetEnabled(true);
            }          
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000"
            EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var textApelido = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido');
                OnCallBackCompleteCliente(s, e, popupMensagemPessoa, btnEditCodigoPessoa, textApelido);                                                
                
                textNomeCliente.SetValue(textApelido.value);
                textApelidoCliente.SetValue(textApelido.value);
            }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callBackIdCliente" runat="server" OnCallback="callBackIdCliente_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                textIdCliente.SetValue(e.result);
            }        
        " />
        </dxcb:ASPxCallback>     
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                var resultSplit = e.result.split('|');
                  
                if(resultSplit[1] != null && resultSplit[1] == 'updateIdCliente')
                {
                    alert(resultSplit[0]);  
                    callBackIdCliente.SendCallback();
                }  
                else
                {                         
                    alert(e.result);                              
                }                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanel.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.' ||
                    e.result == 'Processo executado parcialmente.') {
                     
                    popupLote.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }                               
            }                                             
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackAlteraId" runat="server" OnCallback="callBackAlteraId_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanel.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {                     
                    popupAlteraId.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }                               
            }                                             
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCriaCopia" runat="server" OnCallback="callBackCriaCopia_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanel.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {                     
                    popupCriaCopia.Hide();                    
                }                               
            }                                             
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackClientelogotipo" runat="server" OnCallback="callbackClienteLogotipo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                uplLogotipoCliente.UploadFile();
            }
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackClienteResetaLogotipo" runat="server" OnCallback="callbackClienteResetaLogotipo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
        }
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Clientes" />
                            </div>
                            <div id="mainContent">
                            
                                <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="250" Top="70" HeaderText="Alteração em Lote"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelReseta" runat="server" CssClass="labelNormal" Text="Reseta Cliente:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropReset" runat="server" ClientInstanceName="dropReset" ShowShadow="false"
                                                            DropDownStyle="DropDownList" CssClass="dropDownList">
                                                            <items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="1" Text="Por Movimentação" />
                                                                <dxe:ListEditItem Value="2" Text="Por Data Implantação" />
                                                            </items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelDataImplantacao" runat="server" CssClass="labelNormal" Text="Dt. Implantação:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxDateEdit ID="textDataImplantacao" runat="server" ClientInstanceName="textDataImplantacao" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Dt. Proc. Bloqueio:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxDateEdit ID="textDataProcessamentoBloqueio" runat="server" ClientInstanceName="textDataProcessamentoBloqueio" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Style="white-space: nowrap"
                                                            Text="Tipo Controle:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropTipoControle" runat="server" ClientInstanceName="dropTipoControle"
                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                            <items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="1" Text="Apenas Cotação" />
                                                                <dxe:ListEditItem Value="2" Text="Carteira" />
                                                                <dxe:ListEditItem Value="3" Text="Carteira c/ Cota" />
                                                                <dxe:ListEditItem Value="4" Text="Cotista" />
                                                                <dxe:ListEditItem Value="5" Text="Completo" />
                                                                <dxe:ListEditItem Value="6" Text="IR Renda Variável" />
                                                                <dxe:ListEditItem Value="7" Text="Carteira Importada" />
                                                                <dxe:ListEditItem Value="8" Text="Carteira Simulada" />
                                                            </items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Tipo Cliente:"> </asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropTipoClienteLote" runat="server" ClientInstanceName="dropTipoClienteLote"
                                                            DataSourceID="EsDSTipoClienteLote" ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownList" TextField="Descricao" ValueField="IdTipo">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Grupo Proc.:"> </asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropGrupoProcessamentoLote" runat="server" ClientInstanceName="dropGrupoProcessamentoLote"
                                                            DataSourceID="EsDSGrupoProcessamento" ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownList" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label22" runat="server" CssClass="labelNormal" Text="PL x tributos:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropDescontoTributoLote" runat="server" ClientInstanceName="dropDescontoTributoLote"
                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                            <items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="1" Text="Líquido com desconto" />
                                                                <dxe:ListEditItem Value="2" Text="Bruto com resgate" />
                                                                <dxe:ListEditItem Value="3" Text="Bruto sem resgate" />
                                                            </items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label23" runat="server" CssClass="labelNormal" Text="GrossUP:"> </asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropGrossUPLote" runat="server" ClientInstanceName="dropGrossUPLote"
                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                            <items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="1" Text="Não Faz Grossup" />
                                                                <dxe:ListEditItem Value="2" Text="Grossup sempre" />
                                                                <dxe:ListEditItem Value="3" Text="Apenas em resgate" />
                                                            </items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Zera Caixa:"> </asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropZeraCaixaLote" runat="server" ClientInstanceName="dropZeraCaixaLote"
                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6">
                                                            <items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label" style="width: 85px">
                                                                    <asp:Label ID="labelIsentoIR" runat="server" CssClass="labelNormal" Text="Isento IR:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropIsentoIRLote" runat="server" ClientInstanceName="dropIsentoIRLote"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6">
                                                                        <items>
                                                                            <dxe:ListEditItem Value="" Text="" />
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td style="width: 15px">
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIsentoIOF" runat="server" CssClass="labelNormal" Text="Isento IOF:"> </asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropIsentoIOFLote" runat="server" ClientInstanceName="dropIsentoIOFLote"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6">
                                                                        <items>
                                                                            <dxe:ListEditItem Value="" Text="" />
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização em lote?')==true) {
                                                                        LoadingPanel.Show();
                                                                        callBackLote.SendCallback();
                                                                   }
                                                                   return false;
                                                                  ">
                                                    <asp:Literal ID="Literal15" runat="server" Text="Processa Atualização" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupAlteraId" AllowDragging="true" PopupElementID="popupAlteraId"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Alteração de Id"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelIdOrigem" runat="server" CssClass="labelRequired" Text="Id Origem:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textIdOrigem" runat="server" CssClass="textValor_5" ClientInstanceName="textIdOrigem"
                                                            MaxLength="12" NumberType="Integer" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelIdDestino" runat="server" CssClass="labelRequired" Text="Id Destino:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textIdDestino" runat="server" CssClass="textValor_5" ClientInstanceName="textIdDestino"
                                                            MaxLength="12" NumberType="Integer" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaAlteracao" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização de Id?')==true) {
                                                                        LoadingPanel.Show();
                                                                        callBackAlteraId.SendCallback();
                                                                   }
                                                                   return false;
                                                                  ">
                                                    <asp:Literal ID="Literal12" runat="server" Text="Processa Atualização" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupCriaCopia" AllowDragging="true" PopupElementID="popupCriaCopia"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Cria Cópia"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Id Cliente Origem:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textIdClienteOrigem" runat="server" CssClass="textValor_5"
                                                            ClientInstanceName="textIdClienteOrigem" MaxLength="12" NumberType="Integer" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label16" runat="server" CssClass="labelRequired" Text="Id Cliente Destino:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textIdClienteDestino" runat="server" CssClass="textValor_5"
                                                            ClientInstanceName="textIdClienteDestino" MaxLength="12" NumberType="Integer" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="divPanel">
                                                <table border="0" align="center" cellpadding="2" cellspacing="2" width="350">
                                                    <tr>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkTodos" runat="server" Width="10px" ClientInstanceName="chkTodos"
                                                                ClientSideEvents-CheckedChanged="function(s, e) {
                                                                                chkBolsa.SetChecked(chkTodos.GetChecked());
                                                                                chkBMF.SetChecked(chkTodos.GetChecked());
                                                                                chkRendaFixa.SetChecked(chkTodos.GetChecked());
                                                                                chkLiquidacao.SetChecked(chkTodos.GetChecked());
                                                                                chkFundo.SetChecked(chkTodos.GetChecked());
                                                                                chkHistoricoCota.SetChecked(chkTodos.GetChecked());
                                                                            }" />
                                                        </td>
                                                        <td colspan="3">
                                                            Todos</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" valign="top">
                                                            <hr>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkBolsa" runat="server" Width="10px" ClientInstanceName="chkBolsa"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Bolsa" />
                                                        </td>
                                                        <td style="width: 10px">
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkBMF" runat="server" Width="10px" ClientInstanceName="chkBMF"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="BMF" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkRendaFixa" runat="server" Width="10px" ClientInstanceName="chkRendaFixa"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="Renda Fixa" />
                                                        </td>
                                                        <td style="width: 10px">
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkLiquidacao" runat="server" Width="10px" ClientInstanceName="chkLiquidacao"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Liquidação" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkFundo" runat="server" Width="10px" ClientInstanceName="chkFundo"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Fundo" />
                                                        </td>
                                                        <td style="width: 10px">
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxCheckBox ID="chkHistoricoCota" runat="server" Width="10px" ClientInstanceName="chkHistoricoCota"
                                                                ClientSideEvents-CheckedChanged="function(s, e) { ChecaTodos(); }" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Histórico Cota" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaCopia" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que deseja realizar a cópia?')==true) {
                                                                        LoadingPanel.Show();
                                                                        callBackCriaCopia.SendCallback();
                                                                   }
                                                                   return false;
                                                                  ">
                                                    <asp:Literal ID="Literal14" runat="server" Text="Processa Cópia" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupClienteLogotipo" AllowDragging="true" PopupElementID="popupClienteLogotipo"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="350" Left="350" Top="50" HeaderText="Salva Logotipo Cliente"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                                            <table border="0" width="350">
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxCheckBox ID="chkResetaLogo" ClientInstanceName="chkResetaLogo" runat="server"
                                                            Text="Reseta Logotipo" ToolTip="Reseta o Logotipo para os clientes selecionados do grid"
                                                            ClientSideEvents-CheckedChanged="function(s, e) {
                                                        uplLogotipoCliente.SetVisible(!chkResetaLogo.GetChecked());
                                                        lblCliente.SetVisible(!chkResetaLogo.GetChecked());
                                                        
                                                        if(chkResetaLogo.GetChecked()) {
                                                            document.getElementById('divButton').style.display = 'none'; // invisible
                                                            document.getElementById('divButton1').style.display = 'block'; // visible
                                                        }
                                                        else {
                                                            document.getElementById('divButton').style.display = 'block'; // visible
                                                            document.getElementById('divButton1').style.display = 'none'; // invisible
                                                        }
                                                    }" />
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxLabel ID="lblCliente" ClientInstanceName="lblCliente" runat="server" CssClass="labelBold"
                                                            Text="Salva Logotipo Cliente" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="line-height: 32px;">
                                                            <dxuc:ASPxUploadControl ID="uplLogotipoCliente" runat="server" ClientInstanceName="uplLogotipoCliente"
                                                                OnFileUploadComplete="uplLogotipoCliente_FileUploadComplete" Width="200px" Size="25"
                                                                FileUploadMode="OnPageLoad" EnableDefaultAppearance="False">
                                                                <ValidationSettings MaxFileSize="100000000" />
                                                                <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e);  }" />
                                                            </dxuc:ASPxUploadControl>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="divButton" class="linkButton linkButtonNoBorder" style="margin-left: 40px;
                                                margin-top: 20px">
                                                <asp:LinkButton ID="btnRunlogotipo" runat="server" Font-Overline="false" CssClass="btnRun"
                                                    OnClientClick="
                                    {
                                        if(uplLogotipoCliente.GetText()== '') {
                                            alert('Entre com o arquivo de Logotipo!');                                                                            
                                        }
                                        else {                                                                    
                                            callbackClientelogotipo.SendCallback();                                                                             
                                        }
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal17" runat="server" Text="Salvar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnLogotipoCliente" runat="server" Font-Overline="false" CssClass="btnClear"
                                                    OnClientClick="
                                    {
                                        uplLogotipoCliente.ClearText();
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal18" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                            <div id="divButton1" class="linkButton linkButtonNoBorder" style="margin-left: 40px;
                                                margin-top: 20px; display: none;">
                                                <asp:LinkButton ID="btnRunlogotipoReset" runat="server" Font-Overline="false" CssClass="btnRun"
                                                    OnClientClick="
                                    {
                                        callbackClienteResetaLogotipo.SendCallback();                                                                                                                    
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal19" runat="server" Text="Salvar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que Deseja Excluir? Se Confirmado todo o Histórico de Posições/Operações do Cliente será Deletado.')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit"
                                        OnClientClick="if (confirm('Tem certeza que quer ativar os clientes?')==true) gridCadastro.PerformCallback('btnAtivar');return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Ativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer desativar os clientes?')==true) gridCadastro.PerformCallback('btnDesativar');return false;">
                                        <asp:Literal ID="Literal7" runat="server" Text="Desativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupLote.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Altera em Lote" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAlteraId" runat="server" Font-Overline="false" CssClass="btnCard"
                                        OnClientClick="popupAlteraId.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Altera Id" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCriaCopia" runat="server" Font-Overline="false" CssClass="btnCard"
                                        OnClientClick="popupCriaCopia.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal13" runat="server" Text="Cria Cópia" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnClienteLogotipo" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupClienteLogotipo.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal16" runat="server" Text="Logotipo Cliente" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdCliente"
                                        DataSourceID="EsDSCliente" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnInitNewRow="OnInit">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="15%" />
                                            <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="4" Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="6"
                                                Width="12%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                Width="5%" ExportWidth="100">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Grupo Proc." FieldName="IdGrupoProcessamento"
                                                VisibleIndex="10" Width="13%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSGrupoProcessamento" TextField="Descricao" ValueField="IdGrupoProcessamento" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Controle" FieldName="TipoControle"
                                                VisibleIndex="12" Width="10%" ExportWidth="100">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Apenas Cotação'>Apenas Cotação</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Carteira'>Carteira</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Carteira c/ Cota'>Carteira c/ Cota</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Cotista'>Cotista</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Completo'>Completo</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='IR Renda Variável'>IR Renda Variável</div>" />
                                                        <dxe:ListEditItem Value="7" Text="<div title='Carteira Importada'>Carteira Importada</div>" />
                                                        <dxe:ListEditItem Value="8" Text="<div title='Carteira Simulada'>Carteira Simulada</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataImplantacao" Caption="Implantação" VisibleIndex="14"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="Status" Visible="False" />
                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String"
                                                VisibleIndex="16" Width="12%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="18" Width="10%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataAtualizacao" Caption="Data Atualização" VisibleIndex="19" Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo Cetip" FieldName="AtivoCetip"
                                                VisibleIndex="21" Width="4%" ExportWidth="100">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Apenas Cotação'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Carteira'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TipoControle" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DataInicio" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CalculaGerencial" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CalculaContabil" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ContabilAcoes" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdMoeda" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIR" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIOF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ApuraGanhoRV" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoCVM" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoYMF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoGazeta" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoSwift" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="RegistroBovespa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoRetencaoDIRF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCotacaoBolsa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoSinacorBolsa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIR_Bolsa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoSinacorBolsa2" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdAssessorBolsa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCustoRF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoPlataforma" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="InvestidorInstitucional" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCotacaoBMF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoSinacorBMF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdAssessorBMF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="UsaCustoMedioRF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIR_RF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIOF_RF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Login" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="idCustodianteAcoes" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="idCustodianteOpcoes" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ZeraCaixa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DescontaTributoPL" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="GrossUP" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CategoriaContabil" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoInterfaceRF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="InterfaceSinacorCC" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="RegistroEspecialTributacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdLocalNegociacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="AmortizacaoRendimentoJuros" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxtc:ASPxPageControl ClientInstanceName="tabCadastro" ID="tabCadastro" runat="server" ActiveTabIndex="0" TabSpacing="0px"
                                                            TabAlign="Center" TabPosition="Top" Height="210px">
                                                            <TabPages>
                                                                <dxtc:TabPage Text="Informações Básicas">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ClientInstanceName="lblClienteBloqueado" CssClass="warning" Visible="false" runat="server"  ID="lblClienteBloqueado" Text="CLIENTE BLOQUEADO > "></dxe:ASPxLabel>
                                                                                        <asp:Label ID="labelIdPessoa" runat="server" CssClass="labelRequired" Text="Id Pessoa:"> </asp:Label>
                                                                                    </td>
                                                                                    <td width="10">
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnEditCodigoPessoa" Text='<%# Eval("IdPessoa") %>' OnLoad="btnEditCodigoPessoa_Load"
                                                                                            MaxLength="10" NumberType="Integer">
                                                                                            <buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </buttons>
                                                                                            <clientsideevents keypress="function(s, e) {textApelido = 'gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido';
                                                                                document.getElementById(textApelido).value = '' ;} " buttonclick="function(s, e) {popupPessoa.ShowAtElementByID(s.name);}"
                                                                                                lostfocus="function(s, e) {OnLostFocus(popupMensagemPessoa, ASPxCallback1, btnEditCodigoPessoa);}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="textApelido" runat="server" CssClass="textLongo" MaxLength="40"
                                                                                            Text='<%#Eval("ApelidoPessoa")%>' Enabled="false" />
                                                                                    </td>                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ClientInstanceName="lblIdCliente" CssClass="labelNormal" runat="server"  ID="lblIdCliente" Text="Id Cliente:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textIdCliente" ClientInstanceName="textIdCliente" runat="server" CssClass="textButtonEdit" BackColor="#e9ebe8"
                                                                                            Text='<%#Eval("IdCliente")%>' ClientEnabled="false">
                                                                                            <ClientSideEvents Init="function(s,e)
                                                                                                                    {
                                                                                                                        if(gridCadastro.IsNewRowEditing())
                                                                                                                            callBackIdCliente.SendCallback();
                                                                                                                    }" /> 
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>   
                                                                                                                                                                     
                                                                                </tr>
                                                                                <tr>                                                                                    
                                                                                    <td class="td_Label">
                                                                                    <asp:Label ID="labelApelidoCliente" runat="server" CssClass="labelRequired" Text="Apelido Cliente:"> </asp:Label>                                                                                    
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                    <dxe:ASPxTextBox ID="textApelidoCliente" runat="server" CssClass="textLongo" ClientInstanceName="textApelidoCliente"
                                                                                            Text='<%#Eval("Apelido")%>' MaxLength="100">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                    <asp:Label ID="labelNomeCliente" runat="server" CssClass="labelRequired" Text="Nome Cliente:"> </asp:Label>                                                                                    
                                                                                    </td>
                                                                                    <td colspan="2" width="10">
                                                                                    <dxe:ASPxTextBox ID="textNomeCliente" runat="server" CssClass="textLongo" ClientInstanceName="textNomeCliente"                                                                                    
                                                                                            Text='<%#Eval("Nome")%>' MaxLength="255">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                </tr>																				
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Tipo Controle:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropTipoControle" runat="server" ClientInstanceName="dropTipoControle"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("TipoControle")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Apenas Cotação" />
                                                                                                <dxe:ListEditItem Value="2" Text="Carteira" />
                                                                                                <dxe:ListEditItem Value="3" Text="Carteira c/ Cota" />
                                                                                                <dxe:ListEditItem Value="4" Text="Cotista" />
                                                                                                <dxe:ListEditItem Value="5" Text="Completo" />
                                                                                                <dxe:ListEditItem Value="6" Text="IR Renda Variável" />
                                                                                                <dxe:ListEditItem Value="7" Text="Carteira Importada" />
                                                                                                <dxe:ListEditItem Value="8" Text="Carteira Simulada" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Tipo Cliente:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropTipo" runat="server" ClientInstanceName="dropTipo" DataSourceID="EsDSTipoCliente"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList" TextField="Descricao"
                                                                                            ValueField="IdTipo" Text='<%#Eval("IdTipo")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelRequired" Text="Grupo Proc.:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                                                            DataSourceID="EsDSGrupoProcessamento" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownList" TextField="Descricao" ValueField="IdGrupoProcessamento"
                                                                                            Text='<%#Eval("IdGrupoProcessamento")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelMoeda" runat="server" CssClass="labelRequired" Text="Moeda:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropMoeda" runat="server" ClientInstanceName="dropMoeda" DataSourceID="EsDSMoeda"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_5"
                                                                                            TextField="Nome" ValueField="IdMoeda" Text='<%#Eval("IdMoeda")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Implantação:" /></td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxDateEdit ID="textDataImplantacao" runat="server" ClientInstanceName="textDataFim"
                                                                                            Value='<%#Eval("DataImplantacao")%>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelLocal" runat="server" CssClass="labelRequired" Text="Local:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropLocal" runat="server" ClientInstanceName="dropLocal" DataSourceID="EsDSLocal"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_5"
                                                                                            TextField="Nome" ValueField="IdLocal" Text='<%#Eval("IdLocal")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIsentoIR" runat="server" CssClass="labelRequired" Text="Isento IR:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropIsentoIR" runat="server" ClientInstanceName="dropIsentoIR"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("IsentoIR")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIsentoIOF" runat="server" CssClass="labelRequired" Text="Isento IOF:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIsentoIOF" runat="server" ClientInstanceName="dropIsentoIOF"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("IsentoIOF")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                                            Text="Local Negociação:">
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao"
                                                                                            ValueField="IdLocalNegociacao" TextField="Descricao"
                                                                                            CssClass="dropDownList" Text='<%#Eval("IdLocalNegociacao")%>'> 
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td> 
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelRegimeEspecialTributacao" runat="server" CssClass="labelNormal" Text="Reg. Especial Tributação:"> </asp:Label>                                                                                       
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropRegimeEspecialTributacao" runat="server" ClientInstanceName="dropRegimeEspecialTributacao"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6" 
                                                                                        Text='<%#Eval("RegimeEspecialTributacao")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelStatusAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropStatusAtivo" runat="server" ClientInstanceName="dropStatusAtivo"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("StatusAtivo")%>'>
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="2" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>  
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelLogin" runat="server" CssClass="labelNormal" Text="Login:" OnLoad="labelLogin_Load"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textLogin" runat="server" CssClass="textNormal" MaxLength="30" OnLoad="textLogin_Load" />
                                                                                    </td>
                                                                                    <td class="td_label">                                                                                    
                                                                                        <asp:Label ID="DataAtualizacao" runat="server" CssClass="labelNormal" Text="Atualização:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textDataAtualizacao" runat="server" CssClass="textNormal" MaxLength="30" Enabled="false" Text='<%#Eval("DataAtualizacao")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" align="right">
                                                                                        <asp:Label ID="labelAmortizacaoRendimentoJuros" runat="server" CssClass="labelNormal" Text="Amortização/Rendimento/Juros:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAmortizacaoRendimentoJuros" runat="server" ClientInstanceName="dropAmortizacaoRendimentoJuros"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("AmortizacaoRendimentoJuros")%>'>
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>  
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Cliente - Bolsa">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoCotacao" runat="server" CssClass="labelNormal" Text="Tipo Cotação:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCotacao" runat="server" ClientInstanceName="dropTipoCotacao"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("TipoCotacaoBolsa")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Médio" />
                                                                                                <dxe:ListEditItem Value="2" Text="Fechamento" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIsentoIRBolsa" runat="server" CssClass="labelNormal" Text="Isento IR:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropIsentoIRBolsa" runat="server" ClientInstanceName="dropIsentoIRBolsa"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("IsentoIR_Bolsa")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoSinacorBolsa" runat="server" CssClass="labelNormal" Text="Cód. Sinacor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoSinacorBolsa" runat="server" CssClass="textNormal_5"
                                                                                            Text='<%#Eval("CodigoSinacorBolsa")%>' MaxLength="200">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoSinacorBolsa2" runat="server" CssClass="labelNormal" Text="Cód. Sinacor 2:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoSinacorBolsa2" runat="server" CssClass="textNormal_5"
                                                                                            Text='<%#Eval("CodigoSinacorBolsa2")%>' MaxLength="20" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAssessorBolsa" runat="server" CssClass="labelNormal" Text="Assessor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAssessorBolsa" runat="server" ClientInstanceName="dropAssessorBolsa"
                                                                                            DataSourceID="EsDSAssessor" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList"
                                                                                            TextField="Nome" ValueField="IdAssessor" Text='<%#Eval("IdAssessorBolsa")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                                                                                                  
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIdCustodianteAcoes" runat="server" CssClass="labelNormal" Text="Custodiante Ações:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCustodianteAcoes" runat="server" ClientInstanceName="dropCustodianteAcoes"
                                                                                            DataSourceID="EsDSCustodiante" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdAgente" Text='<%#Eval("IdCustodianteAcoes")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Custodiante Opções:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCustodianteOpcoes" runat="server" ClientInstanceName="dropCustodianteOpcoes"
                                                                                            DataSourceID="EsDSCustodiante" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdAgente" Text='<%#Eval("IdCustodianteOpcoes")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Cliente - BMF">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoPlataforma" runat="server" CssClass="labelNormal" Text="Plataforma:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoPlataforma" runat="server" ClientInstanceName="dropTipoPlataforma"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssFilePath="../../css/forms.css"
                                                                                            CssClass="dropDownListCurto" Text='<%#Eval("TipoPlataforma")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Normal" />
                                                                                                <dxe:ListEditItem Value="2" Text="DMA" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelInvestidorInstitucional" runat="server" CssClass="labelNormal"
                                                                                            Text="Inv. Inst.:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropInvestidorInstitucional" runat="server" ClientInstanceName="dropInvestidorInstitucional"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("InvestidorInstitucional")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoCotacaoBMF" runat="server" CssClass="labelNormal" Text="Tipo Cotação:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCotacaoBMF" runat="server" ClientInstanceName="dropTipoCotacaoBMF"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("TipoCotacaoBMF")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Médio" />
                                                                                                <dxe:ListEditItem Value="2" Text="Fechamento" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoSinacorBMF" runat="server" CssClass="labelNormal" Text="Cód. Sinacor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoSinacorBMF" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoSinacorBMF")%>'
                                                                                            MaxLength="20" />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAssessorBMF" runat="server" CssClass="labelNormal" Text="Assessor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAssessorBMF" runat="server" ClientInstanceName="dropAssessorBMF"
                                                                                            DataSourceID="EsDSAssessor" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList"
                                                                                            TextField="Nome" ValueField="IdAssessor" Text='<%#Eval("IdAssessorBMF")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIdCustodianteBMF" runat="server" CssClass="labelNormal" Text="Custodiante BMF:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCustodianteBmf" runat="server" ClientInstanceName="dropCustodianteBmf"
                                                                                            DataSourceID="EsDSCustodiante" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdAgente" Text='<%#Eval("IdCustodianteBmf")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>                                                                                
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Cliente - Renda Fixa">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelUsaCustoMedio" runat="server" CssClass="labelRequired" Text="Custo Médio:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropUsaCustoMedio" runat="server" ClientInstanceName="dropUsaCustoMedio"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("UsaCustoMedioRF")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIsentoIR_RF" runat="server" CssClass="labelRequired" Text="Isento IR:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIsentoIR_RF" runat="server" ClientInstanceName="dropIsentoIR_RF"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("IsentoIR_RF")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIsentoIOF_RF" runat="server" CssClass="labelRequired" Text="Isento IOF:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIsentoIOF_RF" runat="server" ClientInstanceName="dropIsentoIOF_RF"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("IsentoIOF_RF")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Código Cetip:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoCetip" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoCetip")%>'
                                                                                            MaxLength="9">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label19" runat="server" CssClass="labelNormal" Text="Código Integracao:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoRendaFixa" runat="server" CssClass="textNormal_5"
                                                                                            Text='<%#Eval("CodigoInterfaceRF")%>' MaxLength="200">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Assessor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAssessorRF" runat="server" ClientInstanceName="dropAssessorRF"
                                                                                            DataSourceID="EsDSAssessor" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList"
                                                                                            TextField="Nome" ValueField="IdAssessor" Text='<%#Eval("IdAssessorRendaFixa")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAtivoCetip" runat="server" CssClass="labelRequired" Text="Ativo Cetip:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAtivoCetip" runat="server" ClientInstanceName="dropAtivoCetip"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("AtivoCetip")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>      
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Interfaces">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelRegistroBovespa" runat="server" CssClass="labelNormal" Text="Registro Bovespa:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textRegistroBovespa" runat="server" CssClass="textNormal_5" Text='<%#Eval("RegistroBovespa")%>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoCVM" runat="server" CssClass="labelNormal" Text="Cód. CVM:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoCVM" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoCVM")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCodigoYMF" runat="server" CssClass="labelNormal" Text="Cód. YMF:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoYMF" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoYMF")%>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoGazeta" runat="server" CssClass="labelNormal" Text="Cód. Gazeta:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoGazeta" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoGazeta")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                  <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCodigoIsin" runat="server" CssClass="labelNormal" Text="Cód. Isin:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoIsin" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoIsin")%>' MaxLength="12"/>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoCusip" runat="server" CssClass="labelNormal" Text="Cód. Cusip:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoCusip" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoCusip")%>' MaxLength="9"/>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCodigoSwift" runat="server" CssClass="labelNormal" Text="Cód. Swift:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoSwift" runat="server" CssClass="textNormal_5" Text='<%#Eval("CodigoSwift")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="CC Sinacor:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigosCC" runat="server" TextMode="MultiLine" Rows="5" MaxLength="2000" CssClass="textNormal10"
                                                                                                            Text='<%#Eval("InterfaceSinacorCC")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Cliente - Outros">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCalculaGerencial" runat="server" CssClass="labelNormal" Text="Gerencial:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCalculaGerencial" runat="server" ClientInstanceName="dropCalculaGerencial"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("CalculaGerencial")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Gerencial Operações" />
                                                                                                <dxe:ListEditItem Value="E" Text="Gerencial Estratégia" />
                                                                                                <dxe:ListEditItem Value="A" Text="Controla Ambos" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não Controla" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Grupo Afin.:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropGrupoAfinidade" runat="server" ClientInstanceName="dropGrupoAfinidade"
                                                                                            DataSourceID="EsDSGrupoAfinidade" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownList" TextField="Descricao" ValueField="IdGrupo" Text='<%#Eval("IdGrupoAfinidade")%>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Zera Caixa:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropZeraCaixa" runat="server" ClientInstanceName="dropZeraCaixa"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("ZeraCaixa")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="PL x tributos:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropDescontoTributo" runat="server" ClientInstanceName="dropDescontoTributo"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("DescontaTributoPL")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Líquido com desconto" />
                                                                                                <dxe:ListEditItem Value="2" Text="Bruto com resgate" />
                                                                                                <dxe:ListEditItem Value="3" Text="Bruto sem resgate" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="GrossUP:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropGrossUP" runat="server" ClientInstanceName="dropGrossUP"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                                            Text='<%#Eval("GrossUP")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="1" Text="Não Faz Grossup" />
                                                                                                <dxe:ListEditItem Value="2" Text="Grossup sempre" />
                                                                                                <dxe:ListEditItem Value="3" Text="Apenas em resgate" />
                                                                                                <dxe:ListEditItem Value="4" Text="Gross UP sem IOF" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelApuraGanhoRV" runat="server" CssClass="labelRequired" Text="Apura Ganho RV:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropApuraGanhoRV" runat="server" ClientInstanceName="dropApuraGanhoRV"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto1"
                                                                                            Text='<%#Eval("ApuraGanhoRV")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Apura Com Liquidação" />
                                                                                                <dxe:ListEditItem Value="A" Text="Apura Sem Liquidação" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não Apura" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="MultiMoeda:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropMultiMoeda" runat="server" ClientInstanceName="dropMultiMoeda"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                            Text='<%#Eval("MultiMoeda")%>'>
                                                                                            <items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelExecAberturaIndexada" runat="server" CssClass="labelRequired" Text="Abertura Indexada:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropAberturaIndexada" runat="server" ClientInstanceName="dropAberturaIndexada"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto1"
                                                                                            Text='<%#Eval("AberturaIndexada")%>' OnLoad="dropAberturaIndexada_Load">
                                                                                            <ClientSideEvents Init="HabilitaCampoIndiceAbertura" SelectedIndexChanged="HabilitaCampoIndiceAbertura" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIndiceAbertura" runat="server" CssClass="labelNormal" Text="Índice Abertura Indexada:" Visible="false"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropIndiceAbertura" runat="server" ClientInstanceName="dropIndiceAbertura"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6" ClientVisible="false"
                                                                                            Text='<%#Eval("IdIndiceAbertura")%>' DataSourceID="EsDSIndice" ValueField="IdIndice" TextField="Descricao" >
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>                                                                                
                                                                                
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                
                                                            </TabPages>
                                                        </dxtc:ASPxPageControl>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                         <ClientSideEvents BeginCallback="function(s, e) {
						                    if (e.command == 'CUSTOMCALLBACK') {                            
                                                isCustomCallback = true;                            
                                            }
                                            else if (e.command == 'UPDATEEDIT')
                                            {
                                                isUpdateEdit = true;      
                                            } 
                                            						
                                        }" 
                                        EndCallback="function(s, e) {
						                    if (isCustomCallback) 
						                    {
                                                isCustomCallback = false;
                                                s.Refresh();
                                            }
                                            else if (isUpdateEdit)
                                            {
                                                isUpdateEdit = false;
                                                if(gridCadastro.cpMessage != '' && gridCadastro.cpMessage != null)
                                                {
                                                    alert(gridCadastro.cpMessage);
                                                    gridCadastro.cpMessage = '';
                                                }
                                            }
                                        }" />
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTipoClienteLote" runat="server" OnesSelect="EsDSTipoClienteLote_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDSLocal" runat="server" OnesSelect="EsDSLocal_esSelect" />
        <cc1:esDataSource ID="EsDSAssessor" runat="server" OnesSelect="EsDSAssessor_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoAfinidade" runat="server" OnesSelect="EsDSGrupoAfinidade_esSelect" />
        <cc1:esDataSource ID="EsDSCustodiante" runat="server" OnesSelect="EsDSCustodiante_esSelect" />
		<cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />														
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Processando, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="True" />
    </form>
</body>
</html>
