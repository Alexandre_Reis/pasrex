﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_EnquadraItemRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web" TagPrefix="dxuc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';    
    </script>
</head>

<body>
    <form id="form1" runat="server">
            
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="Div1">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Itens de Enquadramento (Renda Fixa)"></asp:Label>
    </div>
           
    <div id="Div3">

            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false"  CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"  CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal8" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"  CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal9" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal10" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal11" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdItem" DataSourceID="EsDSEnquadraItemRendaFixa"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating" >        
                        
                <Columns>          
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" UnboundType="String" Width="15%" VisibleIndex="1">
                    <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPapel" VisibleIndex="2" Width="8%">
                        <PropertiesComboBox EncodeHtml="false">
                            <ValidationSettings requiredfield-errortext=""></ValidationSettings>                        
                            <Items>
                                <dxe:ListEditItem Value="1" Text="Público" />
                                <dxe:ListEditItem Value="2" Text="Privado" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoRentabilidade" Caption="Rentabilidade" VisibleIndex="3" Width="10%">
                    <PropertiesComboBox EncodeHtml="false">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                        <Items>
                            <dxe:ListEditItem Value="1" Text="Pós-Fixado" />
                            <dxe:ListEditItem Value="2" Text="Pré-Fixado" />
                            <dxe:ListEditItem Value="3" Text="Híbrido" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="OperacaoCompromissada" Caption="Comp.?" VisibleIndex="4" Width="6%" ExportWidth="110">
                    <PropertiesComboBox EncodeHtml="false">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    <Items>                
                        <dxe:ListEditItem Value="S" Text="Sim" />
                        <dxe:ListEditItem Value="N" Text="Não" />                                            
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="5" Width="13%">
                    <PropertiesComboBox DataSourceID="EsDSIndiceInner" TextField="Descricao" ValueField="IdIndice">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdPapel" Caption="Papel" VisibleIndex="6" Width="11%">
                    <PropertiesComboBox DataSourceID="EsDSPapelRendaFixaInner" TextField="Descricao" ValueField="IdPapel">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdEmissor" Caption="Emissor" VisibleIndex="7" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSEmissorInner" TextField="Nome" ValueField="IdEmissor">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEmissor" Caption="Tipo Emissor" VisibleIndex="8" Width="12%">
                    <PropertiesComboBox EncodeHtml="false">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                        <Items>
                            <dxe:ListEditItem Value="1" Text="União" />
                            <dxe:ListEditItem Value="2" Text="Estado" />
                            <dxe:ListEditItem Value="3" Text="Município" />
                            <dxe:ListEditItem Value="4" Text="P.Física" />
                            <dxe:ListEditItem Value="5" Text="P.Jurídica" />
                            <dxe:ListEditItem Value="6" Text="Inst.Financeira" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Bloqueado" Caption="Garantia" VisibleIndex="8" Width="8%">
                        <PropertiesComboBox EncodeHtml="false">
                            <ValidationSettings requiredfield-errortext=""></ValidationSettings>                        
                            <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não"/>                                
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                </Columns>
                
                <Templates>
                <EditForm>
                <div class="editForm">
                <table border="0" cellspacing="2" cellpadding="2">
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelDescricao" runat="server" CssClass="labelNormal" Text="Descrição"/>
                            </td>
                            <td>
                                <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                    CssClass="textNormal5" MaxLength="100" Text='<%# Eval("Descricao") %>' />
                            </td>                                                   
                        </tr>
                        <tr>                            
                            <td class="td_Label">
                                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Tipo Papel:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoPapel" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoPapel")%>' ClientInstanceName="dropTipoPapel">
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="Público" />
                                        <dxe:ListEditItem Value="2" Text="Privado" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>                                                    
                        </tr>
                        <tr>                            
                            <td class="td_Label">
                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Rentabilidade:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoRentabilidade" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoRentabilidade")%>' ClientInstanceName="dropTipoRentabilidade">
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="Pós-Fixado" />
                                        <dxe:ListEditItem Value="2" Text="Pré-Fixado" />
                                        <dxe:ListEditItem Value="3" Text="Híbrido" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>                                                    
                        </tr>
                        <tr>                            
                            <td class="td_Label">
                                <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Comp.?:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="dropOperacaoCompromissada" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("OperacaoCompromissada")%>' ClientInstanceName="dropOperacaoCompromissada">
                                    <Items>                
                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                        <dxe:ListEditItem Value="N" Text="Não" />                                            
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>                                                    
                        </tr>
                        <tr>
                            <td class="td_Label">
                                 <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Índice:" />
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropIndice" ClientInstanceName="dropIndice" runat="server"  TextField="Descricao" ValueField="IdIndice" Text='<%#Eval("IdIndice")%>' DataSourceID="EsDSIndice" />
                               </td>
                        </tr> 
                        <tr>
                            <td class="td_Label">
                                 <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Papel:" />
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropPapel" ClientInstanceName="dropPapel" runat="server"  TextField="Descricao" ValueField="IdPapel" Text='<%#Eval("IdPapel")%>' DataSourceID="EsDSPapelRendaFixa" />
                               </td>
                        </tr> 
                        <tr>
                            <td class="td_Label">
                                 <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Emissor:" />
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropEmissor" ClientInstanceName="dropEmissor" runat="server"  TextField="Nome" ValueField="IdEmissor" Text='<%#Eval("IdEmissor")%>' DataSourceID="EsDSEmissor" />
                               </td>
                        </tr> 
                        <tr>
                            <td class="td_Label">
                             <asp:Label ID="labelEmissor" runat="server" CssClass="labelNormal" Text="Tipo Emissor:" />
                            </td>
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropTipoEmissor" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoEmissor")%>' ClientInstanceName="dropTipoEmissor">
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="União" />
                                        <dxe:ListEditItem Value="2" Text="Estado" />
                                        <dxe:ListEditItem Value="3" Text="Município" />
                                        <dxe:ListEditItem Value="4" Text="P.Física" />
                                        <dxe:ListEditItem Value="5" Text="P.Jurídica" />
                                        <dxe:ListEditItem Value="6" Text="Inst.Financeira" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>                                                                                                                                                                                                                                                                                                                                                                                                                       
                        </tr>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Garantia:" />
                            </td>
                            <td class="td_Label">
                                <dxe:ASPxComboBox ID="dropBloqueado" runat="server"  Text='<%#Eval("Bloqueado")%>' CssClass="dropDownListCurto" ClientInstanceName="dropTipoMercado">                        
                            <Items>
                                <dxe:ListEditItem Value="S" Text="Sim" />
                                <dxe:ListEditItem Value="N" Text="Não"/>                                
                            </Items>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <div class="linhaH"></div>
                <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                            OnClientClick="gridCadastro.UpdateEdit(); return false;">
                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                            <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                            </div>
                        </asp:LinkButton>
                    
                </div>
                </div>
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="350px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                    <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        

    <cc1:esDataSource ID="EsDSEnquadraItemRendaFixa" runat="server" OnesSelect="EsDSEnquadraItemRendaFixa_esSelect" />        
    <cc1:esDataSource ID="EsDSPapelRendaFixa" runat="server" OnesSelect="EsDSPapelRendaFixa_esSelect" />        
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />        
    <cc1:esDataSource ID="EsDSEmissor" runat="server" OnesSelect="EsDSEmissor_esSelect" />        
    <cc1:esDataSource ID="EsDSPapelRendaFixaInner" runat="server" OnesSelect="EsDSPapelRendaFixaInner_esSelect" />        
    <cc1:esDataSource ID="EsDSIndiceInner" runat="server" OnesSelect="EsDSIndiceInner_esSelect" />        
    <cc1:esDataSource ID="EsDSEmissorInner" runat="server" OnesSelect="EsDSEmissorInner_esSelect" />
    
    </form>
</body>
</html>
