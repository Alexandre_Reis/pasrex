﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_EnquadraItemFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Itens de Enquadramento (Fundo)"></asp:Label>
    </div>
           
    <div id="mainContent">
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdItem" DataSourceID="EsDSEnquadraItemFundo"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnCustomCallback="gridCadastro_CustomCallback"                                        
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating">
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" UnboundType="String" Width="18%" VisibleIndex="1">
                    <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCarteira" Caption="Tipo" VisibleIndex="2" Width="8%">
                    <PropertiesComboBox DropDownStyle="DropDownList" EncodeHtml="false">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    <Items>
                        <dxe:ListEditItem Value="1" Text="R. Fixa" />
                        <dxe:ListEditItem Value="2" Text="R. Variável" />                    
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdCategoria" Caption="Categoria" VisibleIndex="6" Width="20%">
                    <PropertiesComboBox DataSourceID="EsDSCategoria" TextField="Descricao" ValueField="IdCategoria" DropDownStyle="DropDownList">
                        <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="3" Width="10%">
                    <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice" DropDownStyle="DropDownList">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdCarteira" Caption="Carteira" VisibleIndex="4" Width="20%">
                    <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Apelido" ValueField="IdCarteira" DropDownStyle="DropDownList">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgenteAdministrador" Caption="Administrador" VisibleIndex="5" Width="12%">
                    <PropertiesComboBox DataSourceID="EsDSAgenteAdministrador" TextField="Nome" ValueField="IdAgente" DropDownStyle="DropDownList">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgenteGestor" Caption="Gestor" VisibleIndex="6" Width="12%">
                    <PropertiesComboBox DataSourceID="EsDSAgenteGestor" TextField="Nome" ValueField="IdAgente" DropDownStyle="DropDownList">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                </Columns>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                    <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                </SettingsCommandButton>                
                
            </dxwgv:ASPxGridView>            
            </div>
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSEnquadraItemFundo" runat="server" OnesSelect="EsDSEnquadraItemFundo_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteAdministrador" runat="server" OnesSelect="EsDSAgenteAdministrador_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteGestor" runat="server" OnesSelect="EsDSAgenteGestor_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
    <cc1:esDataSource ID="EsDSCategoria" runat="server" OnesSelect="EsDSCategoria_esSelect" />
        
    </form>
</body>
</html>