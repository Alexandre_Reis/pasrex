﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PapelRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

                       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">    
        var popup = true;
        document.onkeydown=onDocumentKeyDown;
        var operacao = '';
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>
    
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Papéis de Renda Fixa"></asp:Label>
    </div>
        
    <div id="mainContent">
                      
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {gridCadastro.PerformCallback('btnDelete');} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
                        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdPapel" DataSourceID="EsDSPapelRendaFixa"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender">        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdPapel" Caption="Id" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="Left"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="15%" VisibleIndex="4"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPapel" VisibleIndex="6" Width="8%" ExportWidth="110">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Público'>Público</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Privado'>Privado</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoRentabilidade" VisibleIndex="8" Width="10%" ExportWidth="130">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Pós-Fixado'>Pós-Fixado</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Pré-Fixado'>Pré-Fixado</div>" />                                
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCurva" VisibleIndex="10" Width="10%" ExportWidth="110">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Exponencial'>Exponencial</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Linear'>Linear</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContagemDias" VisibleIndex="12" Width="10%" ExportWidth="110">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Úteis'>Úteis</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Corridos'>Corridos</div>" />
                                <dxe:ListEditItem Value="3" Text="<div title='Dias 360'>Dias 360</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="BaseAno" Caption="Base Ano" VisibleIndex="13" Width="15%" ExportWidth="110">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="252" Text="<div title='Ano 252'>Ano 252</div>" />
                                <dxe:ListEditItem Value="360" Text="<div title='Ano 360'>Ano 360</div>" />
                                <dxe:ListEditItem Value="365" Text="<div title='Ano 365'>Ano 365</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Classe" VisibleIndex="14" Width="20%" ExportWidth="130">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='LFT'>LFT</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='LFT'>LTN</div>" />
                                
                                <dxe:ListEditItem Value="10" Text="<div title='PréFixado Desconto'>PréFixado Desconto</div>" />
                                <dxe:ListEditItem Value="12" Text="<div title='PréFixado c/ Fluxo'>PréFixado c/ Fluxo</div>" />
                                <dxe:ListEditItem Value="15" Text="<div title='PréFixado Atualiza'>PréFixado Atualiza</div>" />
                                
                                <dxe:ListEditItem Value="20" Text="<div title='NTNs'>NTNs</div>" />
                                
                                <dxe:ListEditItem Value="30" Text="<div title='Pós-Fixado'>Pós-Fixado</div>" />
                                <dxe:ListEditItem Value="35" Text="<div title='Pós-Fixado Truncado'>Pós-Fixado Truncado</div>" />                                
                                <dxe:ListEditItem Value="45" Text="<div title='Pós-Fixado c/ Fluxo Corrigido'>Pós-Fixado c/ Fluxo Corrigido</div>" />
                                
                                <dxe:ListEditItem Value="1000" Text="<div title='CCB'>CCB</div>" />
                                <dxe:ListEditItem Value="1005" Text="<div title='CCE'>CCE</div>" />
                                <dxe:ListEditItem Value="1010" Text="<div title='NCE'>NCE</div>" />
                                <dxe:ListEditItem Value="1015" Text="<div title='ExportNote'>ExportNote</div>" />
                                
                                <dxe:ListEditItem Value="1100" Text="<div title='CDB'>CDB</div>" />
                                <dxe:ListEditItem Value="1105" Text="<div title='DPGE'>DPGE</div>" />
                                <dxe:ListEditItem Value="1110" Text="<div title='LF'>LF</div>" />                                
                                
                                <dxe:ListEditItem Value="1200" Text="<div title='CRI'>CRI</div>" />
                                <dxe:ListEditItem Value="1205" Text="<div title='CCI'>CCI</div>" />
                                <dxe:ListEditItem Value="1300" Text="<div title='LCI'>LCI</div>" />
                                <dxe:ListEditItem Value="1400" Text="<div title='LH'>LH</div>" />
                                
                                <dxe:ListEditItem Value="1500" Text="<div title='Nota Comercial'>Nota Comercial</div>" />                                                                
                                <dxe:ListEditItem Value="1700" Text="<div title='LC'>LC</div>" />
                                <dxe:ListEditItem Value="2000" Text="<div title='Debenture'>Debenture</div>" />
                                
                                <dxe:ListEditItem Value="2100" Text="<div title='LCA'>LCA</div>" />
                                <dxe:ListEditItem Value="2105" Text="<div title='CRA'>CRA</div>" />
                                <dxe:ListEditItem Value="2110" Text="<div title='CDCA'>CDCA</div>" />
                                <dxe:ListEditItem Value="2115" Text="<div title='CPR'>CPR</div>" />
                                <dxe:ListEditItem Value="2120" Text="<div title='CDA'>CDA</div>" />
                                
                                <dxe:ListEditItem Value="2200" Text="<div title='GLOBALS'>GLOBALS</div>" />
                                <dxe:ListEditItem Value="2210" Text="<div title='TBILL'>TBILL</div>" />
                                
                                <dxe:ListEditItem Value="5000" Text="<div title='TDA'>TDA</div>" />                                    
                                
                                                                 
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                     <dxwgv:GridViewDataComboBoxColumn FieldName="IsentoIR" Caption="Isento IR" VisibleIndex="15" Width="10%" ExportWidth="110">
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                 <dxe:ListEditItem Value="1" Text="Não Isento" />
                                 <dxe:ListEditItem Value="2" Text="Isento PF" />
                                 <dxe:ListEditItem Value="3" Text="Isento" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CasasDecimaisPU" Visible="false" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="TipoVolume" Visible="false" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="TipoCustodia" Visible="false"/>
                    <dxwgv:GridViewDataTextColumn FieldName="CodigoInterface" Visible="false" />
                    <dxwgv:GridViewDataTextColumn FieldName="IdLocalCustodia" Visible="false" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdClearing" Visible="false" />
                    <dxwgv:GridViewDataTextColumn FieldName="IdLocalNegociacao" Visible="false" />                      
                </Columns>
                
                <Templates>
                <EditForm>
                    
                    <div class="editForm">
                        
                        <table >                    
                            <tr>                                
                                <td class="td_Label">
                                    <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao" CssClass="labelRequired" Text="Descrição:"/>
                                </td>
                                
                                <td>                                                                                                                                    
                                    <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server" CssClass="textLongo" MaxLength="100" Text='<%#Eval("Descricao")%>' />
                                </td>
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoPapel" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoPapel" Text="Tipo Papel:"/>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoPapel" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoPapel")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Público" />
                                    <dxe:ListEditItem Value="2" Text="Privado" />                                    
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoRentabilidade" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoRentabilidade" Text="Rentabilidade:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoRentabilidade" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoRentabilidade")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Pós-Fixado" />
                                    <dxe:ListEditItem Value="2" Text="Pré-Fixado" />
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                            
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoCurva" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoCurva" Text="Curva:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoCurva" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("TipoCurva")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Exponencial" />
                                    <dxe:ListEditItem Value="2" Text="Linear" />                                                                        
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                            
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelContagemDias" runat="server" CssClass="labelRequired" AssociatedControlID="dropContagemDias" Text="Contagem Dias:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropContagemDias" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("ContagemDias")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Dias Úteis" />
                                    <dxe:ListEditItem Value="2" Text="Dias Corridos" />
                                    <dxe:ListEditItem Value="3" Text="Dias 360" />
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                            
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelBaseAno" runat="server" CssClass="labelRequired" AssociatedControlID="dropBaseAno" Text="Base:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropBaseAno" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("BaseAno")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="252" Text="Ano 252" />
                                    <dxe:ListEditItem Value="360" Text="Ano 360" />                                                                        
                                    <dxe:ListEditItem Value="365" Text="Ano 365" />                                                                        
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                            
                            </tr>
                            
                            <tr>     
                                <td class="td_Label">
                                    <asp:Label ID="labelCasasDecimaisPU" runat="server" AssociatedControlID="textCasasDecimaisPU" CssClass="labelRequired" Text="Casas PU:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxSpinEdit ID="textCasasDecimaisPU" runat="server" CssClass="textCurto" ClientInstanceName="textCasasDecimaisPU"
		                                      MaxLength="2" NumberType="integer" Increment="1" LargeIncrement="1" MaxValue="16" Text="<%#Bind('CasasDecimaisPU')%>">            
		                                      <SpinButtons ShowLargeIncrementButtons="true" Position="Right" ></SpinButtons> 
                                    </dxe:ASPxSpinEdit>                                                                
                                </td>
                                                                                                                         
                                <td class="td_Label">
                                    <asp:Label ID="labelClasse" runat="server" CssClass="labelRequired" AssociatedControlID="dropClasse" Text="Classe:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropClasse" runat="server" CssClass="dropDownListCurto2" Text='<%#Eval("Classe")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="LFT" />
                                    <dxe:ListEditItem Value="2" Text="LTN" />
                                    <dxe:ListEditItem Value="10" Text="PréFixado Desconto" />
                                    <dxe:ListEditItem Value="12" Text="PréFixado c/ Fluxo" />
                                    <dxe:ListEditItem Value="15" Text="PréFixado Atualiza" />
                                    <dxe:ListEditItem Value="20" Text="NTNs" />
                                    <dxe:ListEditItem Value="30" Text="Pós-Fixado" />
                                    <dxe:ListEditItem Value="35" Text="Pós-Fixado Truncado" />
                                    <dxe:ListEditItem Value="45" Text="Pós-Fixado c/ Fluxo Corrigido" />
                                    <dxe:ListEditItem Value="1000" Text="CCB" />
                                    <dxe:ListEditItem Value="1005" Text="CCE" />
                                    <dxe:ListEditItem Value="1010" Text="NCE" />
                                    <dxe:ListEditItem Value="1015" Text="ExportNote" />
                                    <dxe:ListEditItem Value="1100" Text="CDB" />
                                    <dxe:ListEditItem Value="1105" Text="DPGE" />
                                    <dxe:ListEditItem Value="1110" Text="LF" />
                                    <dxe:ListEditItem Value="1200" Text="CRI" />
                                    <dxe:ListEditItem Value="1205" Text="CCI" />
                                    <dxe:ListEditItem Value="1300" Text="LCI" />
                                    <dxe:ListEditItem Value="1400" Text="LH" />
                                    <dxe:ListEditItem Value="1500" Text="Nota Comercial" />
                                    <dxe:ListEditItem Value="1700" Text="LC" />
                                    <dxe:ListEditItem Value="2000" Text="Debenture" />
                                    <dxe:ListEditItem Value="2100" Text="LCA" />
                                    <dxe:ListEditItem Value="2105" Text="CRA" />
                                    <dxe:ListEditItem Value="2110" Text="CDCA" />
                                    <dxe:ListEditItem Value="2115" Text="CPR" />
                                    <dxe:ListEditItem Value="2120" Text="CDA" />
                                
                                    <dxe:ListEditItem Value="2200" Text="GLOBALS" />
                                    <dxe:ListEditItem Value="2210" Text="TBILL" />
                                                                    
                                    <dxe:ListEditItem Value="5000" Text="TDA" />
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                            
                            </tr>                              
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" AssociatedControlID="dropPagamentoJuros" Text="Pagto Juros:">
                                    </asp:Label>
                                </td>
                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropPagamentoJuros" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("PagamentoJuros")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Conta Dias" />
                                    <dxe:ListEditItem Value="2" Text="Cheio" />
                                    <dxe:ListEditItem Value="3" Text="Dias Corridos" />
                                    </Items>                                                                                    
                                    </dxe:ASPxComboBox>                            
                                </td>                                
                                <td class="td_Label_Curto">
                                    <asp:Label ID="label2" runat="server" AssociatedControlID="textCodigoInterface" CssClass="labelNormal" Text="Cód.Interface:"/>
                                </td>
                                <td>                                                                                                                                    
                                    <dxe:ASPxTextBox ID="textCodigoInterface" ClientInstanceName="textCodigoInterface" runat="server" MaxLength="40" Text='<%#Eval("CodigoInterface")%>' />
                                </td>
                            </tr>     
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelIsentoIR" runat="server" CssClass="labelRequired" Text="Isento IR:"/>
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropIsentoIR" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropIsentoIR" Text='<%#Eval("IsentoIR")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Não Isento" />
                                            <dxe:ListEditItem Value="2" Text="Isento PF" />
                                            <dxe:ListEditItem Value="3" Text="Isento" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>     
                             <tr>
                                 <td class="td_Label_Curto">
                                     <asp:Label ID="lblLocalCustodia" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalCustodia"
                                         Text="Local Custódia:">
                                     </asp:Label>
                                 </td>
                                 <td>
                                     <dxe:ASPxComboBox ID="dropLocalCustodia" runat="server" DataSourceID="EsDSLocalCustodia"
                                         ValueField="IdLocalCustodia" TextField="Descricao"
                                         CssClass="dropDownList" Text='<%#Eval("IdLocalCustodia")%>'> 
                                     </dxe:ASPxComboBox>
                                 </td>
                                 <td class="td_Label_Curto">
                                     <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                         Text="Local Negociação:">
                                     </asp:Label>
                                 </td>
                                 <td>
                                     <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao"
                                         ValueField="IdLocalNegociacao" TextField="Descricao"
                                         CssClass="dropDownList" Text='<%#Eval("IdLocalNegociacao")%>'> 
                                     </dxe:ASPxComboBox>
                                 </td>
                             </tr>	
						     <tr>
                                 <td class="td_Label_Curto">
                                     <asp:Label ID="lblClearing" runat="server" CssClass="labelRequired" AssociatedControlID="dropClearing"
                                         Text="Clearing:">
                                     </asp:Label>
                                 </td>
                                 <td>
                                     <dxe:ASPxComboBox ID="dropClearing" runat="server" DataSourceID="EsDSClearing"
                                         ValueField="IdClearing" TextField="Descricao"
                                         CssClass="dropDownList" Text='<%#Eval("IdClearing")%>'> 
                                     </dxe:ASPxComboBox>
                                 </td>
                                <td class="td_Label_Curto">
                                    <asp:Label ID="lblIdPapel" runat="server" AssociatedControlID="textIdPapel" CssClass="labelNormal" Text="Id.Papel:"/>
                                </td>
                                <td>                                                                                                                                    
                                    <dxe:ASPxTextBox ID="textIdPapel" ClientInstanceName="textIdPapel" runat="server" CssClass="textCurto" MaxLength="6" Text='<%#Eval("IdPapel")%>' ClientEnabled="false" />
                                </td>
                             </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="Label36" runat="server" CssClass="labelNormal" Text="Investimento Coletivo – CVM:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropInvestimentoColetivoCvm" runat="server" ClientInstanceName="dropInvestimentoColetivoCvm"
                                        ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("InvestimentoColetivoCvm") %>'
                                        ValueType="System.String">
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>	                            
                        </table>    
                    
                        <div class="linhaH"></div>
                        
                        <div class="linkButton linkButtonNoBorder popupFooter">
                             <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"  OnInit="btnOKAdd_Init"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                             <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal1" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>    
                    
                    </div>
                    
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="600px" />
                
                <ClientSideEvents           
                    Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"

                    BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }"

                    EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }"                                                            
                />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
            </dxwgv:ASPxGridView>            
            </div>
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" 
    LeftMargin = "50" RightMargin = "50"
    />
        
    <cc1:esDataSource ID="EsDSPapelRendaFixa" runat="server" OnesSelect="EsDSPapelRendaFixa_esSelect" />        
    <cc1:esDataSource ID="EsDSLocalCustodia" runat="server" OnesSelect="EsDSLocalCustodia_esSelect" />
    <cc1:esDataSource ID="EsDSClearing" runat="server" OnesSelect="EsDSClearing_esSelect" />
    <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />        
    </form>
</body>
</html>