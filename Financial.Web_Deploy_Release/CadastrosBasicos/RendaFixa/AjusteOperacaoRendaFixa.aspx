﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_AjusteOperacaoRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    
    var popup = true;
    
    var selectedIndex; 
    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    
    function OpenPopup(value) {
        
        if (value == 200 || value == 210) { // status = Liberado não edita
            alert('Operações liberadas não podem ser alteradas!');            
        }
        else {
            gridCadastro.StartEditRow(selectedIndex);
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else    
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Operações de Renda Fixa" />
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table border="0">
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCliente.HideWindow();
                                     				                ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
                                                                    }" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="labelIdOperacao" runat="server" CssClass="labelNormal" Text="Id Operação/Id Liquidação:"
                                                            ToolTip="Id Operação Resgatada / Id Liquidação" />
                                                    </td>
                                                    <td colspan="2">
                                                        <dxe:ASPxSpinEdit ID="textIdOperacaoFiltro" runat="server" CssClass="textValor" ClientInstanceName="textIdOperacaoFiltro"
                                                            MaxLength="8" NumberType="Integer" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td width="25">
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnLiberar" runat="server" Font-Overline="false" CssClass="btnTick"
                                        OnClientClick="if (confirm('Tem certeza que quer Liberar as Operações?')==true) gridCadastro.PerformCallback('btnLiberar');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Liberar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesfazer" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer Desfazer os Ajustes/Liberações ?')==true) gridCadastro.PerformCallback('btnDesfazer');return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Desfazer" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSOperacaoRendaFixa" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnPreRender="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnHtmlDataCellPrepared="gridCadastro_HtmlDataCellPrepared">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id Oper./Id Liquid." VisibleIndex="1"
                                                Width="7%" CellStyle-HorizontalAlign="left" HeaderStyle-Wrap="true" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoRegistro" Visible="false" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Operação" VisibleIndex="3"
                                                Width="8%" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id Cliente" VisibleIndex="4"
                                                Width="5%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="5" Width="18%"
                                                CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" VisibleIndex="5"
                                                Width="25%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tp. Operação"
                                                VisibleIndex="6" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="0" Text="" />
                                                        <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                        <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                                        <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoLancamento" Caption="Tp. Lançamento"
                                                VisibleIndex="7" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="0" Text="" />
                                                        <dxe:ListEditItem Value="1" Text="<div title='Vencimento'>Vencimento</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Amortização'>Amortização</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Juros'>Juros</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='Pagto. Principal'>Pagto. Principal</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="Status" VisibleIndex="8" Width="10%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Normal'>Digitado</div>" />
                                                        <dxe:ListEditItem Value="100" Text="<div title='Ajustado'>Ajustado</div>" />
                                                        <dxe:ListEditItem Value="200" Text="<div title='Liberado'>Liberado sem Ajuste</div>" />
                                                        <dxe:ListEditItem Value="210" Text="<div title='Liberado'>Liberado com Ajuste</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="10" Width="10%" CellStyle-HorizontalAlign="Right"
                                                HeaderStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIR" Caption="IR" VisibleIndex="12"
                                                Width="7%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" Caption="IOF" VisibleIndex="14"
                                                Width="7%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdCustodia" Caption="Clearing" VisibleIndex="15"
                                                Width="6%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="0" Text="<div title='-'>-</div>" />
                                                        <dxe:ListEditItem Value="1" Text="<div title='Selic'>Selic</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Cetip'>Cetip</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='CBLC'>CBLC</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Rendimento" CellStyle-HorizontalAlign="Right"
                                                Width="8%" HeaderStyle-HorizontalAlign="Right" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="RendimentoNaoTributavel" CellStyle-HorizontalAlign="Right"
                                                Width="8%" HeaderStyle-HorizontalAlign="Right" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente"
                                                                        CssClass="textButtonEdit" ClientEnabled="false" Text='<%#Eval("IdCliente")%>'>
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxTextBox ID="textNomeCliente" runat="server" CssClass="textNome" ClientInstanceName="textNomeCliente"
                                                                        ClientEnabled="false" BackColor="#EBEBEB" Text='<%#Eval("Apelido")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor Bruto:"
                                                                        OnInit="labelValor_Init" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textRendimento"
                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Valor")%>'
                                                                        OnInit="textValor_Init" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Rendimento:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textRendimento" runat="server" CssClass="textValor_5" ClientInstanceName="textRendimento"
                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Rendimento")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Rend. Não Tribut.:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textRendimentoNaoTributavel" runat="server" CssClass="textValor_5"
                                                                        ClientInstanceName="textRendimentoNaoTributavel" MaxLength="16" NumberType="Float"
                                                                        DecimalPlaces="2" Text='<%#Eval("RendimentoNaoTributavel")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Valor IR:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorIR" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIR"
                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorIR")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Valor IOF:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorIOF" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIOF"
                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorIOF")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixa" runat="server" OnesSelect="EsDSTituloRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect" />
    </form>
</body>
</html>
