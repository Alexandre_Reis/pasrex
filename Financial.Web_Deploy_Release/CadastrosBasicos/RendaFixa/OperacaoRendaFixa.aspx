﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_OperacaoRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" src="../../js/ext-core.js" charset="iso-8859-1"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function isNumber1(input) {
        var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
        var regex = RegExp(number);
        return regex.test(input) && input.length > 0;
    }

    function OnGetDataCliente(data) {          
        if(!isNumber(data)){
            alert(data);
            return false;
        }
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    function OnGetDataTituloRF(data) {
        document.getElementById(gridCadastro.cpHiddenIdTitulo).value = data;
        ASPxCallback2.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }
    function OnGetDataPosicao(data) {
        document.getElementById(gridCadastro.cpHiddenIdPosicao).value = data;
        ASPxCallback4.SendCallback(data);
        popupPosicao.HideWindow();
        btnEditPosicao.Focus();
    }    
    function TrataCompromisso(enable)
    {   
        textDataVolta.SetEnabled(enable);
        textTaxaVolta.SetEnabled(enable);
        textPUVolta.SetEnabled(enable);
        textValorVolta.SetEnabled(enable);
        dropIndiceVolta.SetEnabled(enable);
        
        if (!enable)
        {
            textDataVolta.SetText('');
            textTaxaVolta.SetText('');
            textPUVolta.SetText('');
            textValorVolta.SetText('');
            dropIndiceVolta.SetText('');
        }
    }
    
     /* tela de Filtro - salva o IdTitulo num campo Hidden*/
     function Validate(s, e) {
        var gv = ASPxClientGridView.Cast(dropTituloRendaFixa.GetGridView());    
        var keyValue = gv.GetRowKey(gv.GetFocusedRowIndex());
        //   
        if(keyValue != '' && keyValue!=null) {
            var titulo = document.getElementById('popupFiltro_hiddenIdTituloFiltro_I');
            titulo.value = keyValue;
        }
    }
    
    function executaFluxoSuitabilityItau(dadosOperacao){
        consultaPerfilClienteItau(dadosOperacao);
    }
    
    function consultaPerfilClienteItau(dadosOperacao){
        var dadosOperacaoEncoded = Ext.encode(dadosOperacao);
        Ext.Ajax.request({
            url: '../../jsCustom/itau/FluxoSuitability.ashx',
            params: { dadosOperacao: dadosOperacaoEncoded,
                action: 'ConsultarPerfilCliente' },
            success: function(response){
                var suitabilityEntidade = Ext.decode(response.responseText);
                if(suitabilityEntidade.success === false)
                {
                    alert(suitabilityEntidade.errorMessage + "\n" + suitabilityEntidade.errorDetail);
                    return;
                }
                if(suitabilityEntidade.SolicitarTCQ === true){
                    solicitaTCQ(suitabilityEntidade, dadosOperacao);
                }
                else{
                    consultaEnquadramentoSuitabilityItau(dadosOperacao);
                }
                
            },
            failure: function(response){                
                operacao = '';
            }
        });
    }
    
    function consultaEnquadramentoSuitabilityItau(dadosOperacao){
        var dadosOperacaoEncoded = Ext.encode(dadosOperacao);
        Ext.Ajax.request({
            url: '../../jsCustom/itau/FluxoSuitability.ashx',
            params: { dadosOperacao: dadosOperacaoEncoded,
                action: 'ConsultaEnquadramentoSuitability' },
            success: function(response){
                var suitabilityEntidade = Ext.decode(response.responseText);
                if(suitabilityEntidade.success === false)
                {
                    alert(suitabilityEntidade.errorMessage + "\n" + suitabilityEntidade.errorDetail);
                    
                    return;
                }
                if(suitabilityEntidade.SolicitarTCD === true){
                    solicitaTCD(suitabilityEntidade, dadosOperacao);
                }
                else if(suitabilityEntidade.ApresentarAlerta === true){
                    if(confirm(suitabilityEntidade.MensagemTermo)){
                        commitOperacao();
                    }
                }
                else{
                    var isCompra = dropTipoOperacao.GetValue() === '1';
                    if(isCompra){
                        if(confirm("Deseja confirmar a operação?")){
                            commitOperacao();
                        }else{
                            operacao = '';
                        }
                    }else{
                        commitOperacao();
                    }
                }
            },
            failure: function(){
                alert('Ocorreu um erro');
                operacao = '';
            }
        });
    }
    
    function solicitaTCQ(suitabilityEntidade, dadosOperacao){
        var dadosOperacaoEncoded = Ext.encode(dadosOperacao);
        if(confirm(suitabilityEntidade.MensagemTermo)){
            Ext.Ajax.request({
            url: '../../jsCustom/itau/FluxoSuitability.ashx',
            params: { dadosOperacao: dadosOperacaoEncoded,
                action: 'AssinarTCQ' },
            success: function(response){                
                var sucessoAssinatura = Ext.decode(response.responseText);
                
                if(sucessoAssinatura.success === false)
                {
                    alert(sucessoAssinatura.errorMessage + "\n" + sucessoAssinatura.errorDetail);
                    return;
                }
                if(sucessoAssinatura === true){
                    commitOperacao();
                }
                else{
                    alert('Ocorreu um erro ao assinar o termo');
                    operacao = '';
                }
            },
            failure: function(){
                alert('Ocorreu um erro');
                operacao = '';
            }
        });
        }
    }
    
    function solicitaTCD(suitabilityEntidade, dadosOperacao){
        var dadosOperacaoEncoded = Ext.encode(dadosOperacao);
        if(confirm(suitabilityEntidade.MensagemTermo)){
            Ext.Ajax.request({
            url: '../../jsCustom/itau/FluxoSuitability.ashx',
            params: { dadosOperacao: dadosOperacaoEncoded,
                action: 'AssinarTCD' },
            success: function(response){
                var sucessoAssinatura = Ext.decode(response.responseText);
                if(sucessoAssinatura.success === false)
                {
                    alert(sucessoAssinatura.errorMessage + "\n" + sucessoAssinatura.errorDetail);
                    return;
                }
                if(sucessoAssinatura === true){
                    commitOperacao();
                }else{
                    alert('Ocorreu um erro ao assinar o termo');
                    operacao = '';
                }
            },
            failure: function(){
                alert('Ocorreu um erro');
                operacao = '';
            }
        });
        }
    }
    
    function commitOperacao(){
         if (operacao == 'deletar')
        {
            gridCadastro.PerformCallback('btnDelete');                    
        }
        else
        {   
            if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
            {             
                gridCadastro.UpdateEdit();
            }
            else
            {
                callbackAdd.SendCallback();
            }    
        }
        operacao = '';
    }
    
    function dropMercadoClienteSelectedIndexChanged(s,e)
    {
        var trAgenteContraParte = document.getElementById('trAgenteContraParte');
        var trCarteiraContraParte = document.getElementById('trCarteiraContraParte');
        
        var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
        var textNomeAgente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente');
        
                
        if(s.GetValue() === "1")
        {
            trCarteiraContraParte.style.display = "none";
            trAgenteContraParte.style.display = "table-row";
        }
        else if(s.GetValue() ==="2")
        {
            trAgenteContraParte.style.display = "none";            
            trCarteiraContraParte.style.display = "table-row";                        
        }       
        
        
        
    }
    
    function dropTipoOperacaoSelectedIndexChanged(s,e)
    {    
        if (s.GetSelectedIndex() == 2 || s.GetSelectedIndex() == 3)
        {
           TrataCompromisso(true);     
        }
        else
        {   TrataCompromisso(false);    }    
        
        if (s.GetSelectedIndex() == 4)
        {
            textQuantidade.SetEnabled(false);
            textValor.SetEnabled(false);                                                                                                        
        }
        else
        {
            textQuantidade.SetEnabled(true);
            textValor.SetEnabled(true);
        } 
        
        PreencheTituloEPosicao();    
    }
    
    function PreencheTituloEPosicao()
    {
        //alert("teste");
    }
    
    function OnGetDataCarteira(data) {          
        btnCarteiraContraparte.SetValue(data);        
        CallbackCarteira.SendCallback(btnCarteiraContraparte.GetValue());
        
        popupCarteira.HideWindow();
        btnCarteiraContraparte.Focus();
    }
    
    function OnGetDataAgenteMercado(data) {          
        btnAgenteContraParte.SetValue(data);        
        CallbackAgenteMercado.SendCallback(btnAgenteContraParte.GetValue());
        
        popupAgenteMercado.HideWindow();
        btnAgenteContraParte.Focus();
    }
    
    
    function CallbackCarteiraComplete(s,e)
    {
        var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
        textNomeCarteira.value = e.result;   
        
        PreencheTituloEPosicao(); 
    }
    
    function CallbackAgenteMercadoComplete(s,e)
    {
        var textNomeAgente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente');
        textNomeAgente.value = e.result;        
    }

    function setTermo(dtLiquidacao, dtOperacao )
    {
        if(dtLiquidacao > dtOperacao)        
        { 
           document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_chkOperacaoTermo').checked = true;  
        }else
        {
            document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_chkOperacaoTermo').checked = false; 
        }
                 
    }  
    
    function callbackErroComplete(s,e)
    {     
    debugger
            var temFluxoSuitabilityItau = e.result.indexOf('PapelDescricao') > 0;
            if(temFluxoSuitabilityItau){
                var resultado = e.result;
                var dadosOperacao = eval('(' + resultado + ')');
                executaFluxoSuitabilityItau(dadosOperacao);
            }
            else{
            
                if (e.result != '')
                {                   
                    alert(e.result);                                              
                }
                else
                {
                    commitOperacao();
                }
                operacao = '';
            }
        }
        
    
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="callbackErroComplete" />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                textQuantidade.SetValue(null);
                textPUOperacao.SetValue(null);
                textValor.SetValue(null);
                alert('Operação feita com sucesso.');
                textQuantidade.Focus();
            }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callBackCalculaPremio" runat="server" OnCallback="callBackCalculaPremio_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                textPUOperacao.SetValue(e.result);                
            }        
        " />
        </dxcb:ASPxCallback>        
        
        <dxcb:ASPxCallback ID="callbackPosicaoOperacao" runat="server" OnCallback="callbackPosicaoOperacao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                btnEditPosicao.SetValue(e.result);                
            }        
        " />
        </dxcb:ASPxCallback>  
                
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {        
            
                var resultSplit = e.result.split('|');
                
                if (gridCadastro.cp_EditVisibleIndex == -1)
                {                
                    e.result = resultSplit[0];  
                    var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                    OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
                }
                else
                {                  
                    var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                                    
                    if (gridCadastro.cp_EditVisibleIndex == 'new')
                    {                
                        OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textDataRegistro);  
                          
                        if (resultSplit[1] != null && resultSplit.length >= 3)
                        {                                                                                                                           
                           var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                           var newDateLiq = LocalizedData(resultSplit[3], resultSplit[2]);
                                                      
                           textDataOperacao.SetValue(newDate);
                           textDataRegistro.SetValue(newDate);
                           textDataLiquidacao.SetValue(newDateLiq);   
                           debugger;  
                           setTermo(Date.parse(newDateLiq), Date.parse(newDate));                                                                           
                        }
                        else
                        {
                            textDataOperacao.SetValue(null);
                            textDataRegistro.SetValue(null);
                            textDataLiquidacao.SetValue(null);
                        }                                    
                    }
                    else
                    {
                        OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
                    }
                if (gridCadastro.cpMultiConta == 'True' ) { dropConta.PerformCallback(); }
                }                        
            }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
                                                {
                                                    if (e.result != null)
                                                    { 
                                                        var resultSplit = e.result.split('|'); 
                                                        btnEditTituloRF.SetValue(resultSplit[0]); 
                                                        textDataLiquidacao.SetValue(LocalizedData(resultSplit[2], resultSplit[3]));                                                         
                                                        textPercentualPuFace.SetValue(null);
                                                        
                                                        if(resultSplit[1] == '2200' || resultSplit[1] == '2210')                                                                                                             
                                                            textPercentualPuFace.SetEnabled(true);
                                                        else
                                                            textPercentualPuFace.SetEnabled(false);
                                                            
                                                        if(resultSplit[4] != null)
                                                            dropLocalNegociacao.SetValue(resultSplit[4])
                                                            
                                                        if(resultSplit[5] != null)
                                                            dropLocalCustodia.SetValue(resultSplit[5])

                                                        if(resultSplit[6] != null)
                                                            dropFormaLiquidacao.SetValue(resultSplit[6])                                                                                                                        
                                                        
                                                    }
                                                } " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback4" runat="server" OnCallback="ASPxCallback4_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) { 
                                                                    var resultSplit = e.result.split('|'); 
                                                                    btnEditPosicao.SetValue(resultSplit[0]);
                                                                    document.getElementById(gridCadastro.cpHiddenIdTitulo).value = resultSplit[2];
                                                                    ASPxCallback2.SendCallback(resultSplit[2]); }
                                                                 } " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback3" runat="server" OnCallback="ASPxCallback3_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                if (resultSplit != '')
                                {
                                    var resultSplit = e.result.split('|');                        
                                    textPUVolta.SetValue(resultSplit[0]);
                                    textValorVolta.SetValue(resultSplit[1]);
                                }    
                                    } " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackTaxa" runat="server" OnCallback="callbackTaxa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '') {

                if(isNumber1(e.result)) {                    
                    var valor = parseFloat(e.result);
                    textTaxaOperacao.SetValue(valor);
                }
                else {                
                    alert(e.result);
                }
            }
                
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackPU" runat="server" OnCallback="callbackPU_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '') {
                
                //alert(e.result);                                                
                //alert( isNumber1(e.result) );

                if(isNumber1(e.result)) {                    
                    var valor = parseFloat(e.result);
                    textPUOperacao.SetValue(valor);
                    //alert(e.result);
                }
                else {                
                    alert(e.result);
                }
                
                //textPUOperacao.SetValue(120);
                //textPUOperacao.SetValue(1094.22745083);
            }                
        }        
        " />
        </dxcb:ASPxCallback> 
        
        <dxcb:ASPxCallback ID="callbackPercentualPuFace" runat="server" OnCallback="callbackPercentualPuFace_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {
                var str = e.result;
                var res = str.split('|'); 
                                   
                textPUOperacao.SetValue(res[0]);
                textTaxaOperacao.SetValue(res[1]);
            }                
        }        
        " />
        </dxcb:ASPxCallback>   
        
        <dxcb:ASPxCallback ID="CallbackCarteira" runat="server" OnCallback="CallbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="CallbackCarteiraComplete" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="CallbackAgenteMercado" runat="server" OnCallback="CallbackAgenteMercado_Callback">
            <ClientSideEvents CallbackComplete="CallbackAgenteMercadoComplete" />
        </dxcb:ASPxCallback>

        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Operações de Renda Fixa"></asp:Label>

                                <asp:LinkButton ID="btnManual" runat="server" ToolTip="Acesso ao Manual Renda Fixa" BorderStyle="None"
                                    OnClick ="ManualRendaFixa_Click" OnClientClick="window.document.forms[0].target='_blank';">
                                                <img alt="" src="../../imagens/ico_report.gif" />
                                </asp:LinkButton>

                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0">                                                
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents 
                                                                        KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                                        LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"/>
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelTitulo" runat="server" CssClass="labelNormal" Text="Título:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropTituloRendaFixa" ClientInstanceName="dropTituloRendaFixa"
                                                            runat="server" SelectionMode="Single" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixaFiltro"
                                                            Width="500px" TextFormatString="{5}" Font-Size="11px" IncrementalFilteringMode="contains"
                                                            ClientSideEvents-ValueChanged="Validate" AllowUserInput="false">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Caption="Id" Width="10%" CellStyle-Font-Size="X-Small" />
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoPapel" Caption="Papel" CellStyle-Font-Size="X-Small"
                                                                    Settings-AutoFilterCondition="Contains" Width="20%" />
                                                                <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" CellStyle-Font-Size="X-Small"
                                                                    Settings-AutoFilterCondition="Contains" Width="45%" />
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Data Emissão" CellStyle-Font-Size="X-Small"
                                                                    Width="10%" />
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data Vencimento"
                                                                    CellStyle-Font-Size="X-Small" Width="10%" />
                                                                <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição Completa"
                                                                    CellStyle-Font-Size="X-Small" Settings-AutoFilterCondition="Contains" Visible="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <Settings ShowFilterRow="True" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxTextBox ID="hiddenIdTituloFiltro" runat="server" CssClass="hiddenField"
                                                            ClientInstanceName="hiddenIdTituloFiltro" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <dxe:ASPxLabel ID="labelTipo" ClientInstanceName="labelTipo" runat="server" Text="Tipo:"> </dxe:ASPxLabel>                                                                                    
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropTipoCliente" runat="server" ClientInstanceName="dropTipoCliente" ShowShadow="false" 
                                                            DropDownStyle="DropDown" CssClass="dropDownListCurto"  DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" >
        
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>                                                
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); 
                                                                                              popupFiltro_hiddenIdTituloFiltro_I.value='';
                                                                                              dropTituloRendaFixa.SetValue(null);
                                                                                              dropTipoCliente.SetValue(null);
                                                                                              return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupPosicao" runat="server" HeaderText="" Width="500px"
                                    ContentStyle-VerticalAlign="Top" EnableClientSideAPI="True" PopupVerticalAlign="Middle"
                                    PopupHorizontalAlign="OutsideRight" AllowDragging="True">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <div>
                                                <dxwgv:ASPxGridView ID="gridPosicao" runat="server" Width="100%" ClientInstanceName="gridPosicao"
                                                    AutoGenerateColumns="False" DataSourceID="EsDSPosicao" KeyFieldName="IdPosicao"
                                                    OnCustomDataCallback="gridPosicao_CustomDataCallback" OnHtmlRowCreated="gridPosicao_HtmlRowCreated"
                                                    OnCustomCallback="gridPosicao_CustomCallBack">
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn FieldName="IdPosicao" Caption="Id" VisibleIndex="1" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="3" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" Caption="Papel" VisibleIndex="4"
                                                            Settings-AutoFilterCondition="Contains" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição" VisibleIndex="5"
                                                            Settings-AutoFilterCondition="Contains" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Operação" VisibleIndex="7" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0;(#,##0);0}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU Operação" VisibleIndex="11">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" Caption="Valor Mercado" VisibleIndex="13">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>                                                        
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="IdCustodia" Caption="Clearing" VisibleIndex="14" Width="6%">
                                                            <PropertiesComboBox EncodeHtml="false">
                                                                <Items>
                                                                    <dxe:ListEditItem Value="0" Text="<div title='-'>-</div>" />
                                                                    <dxe:ListEditItem Value="1" Text="<div title='Selic'>Selic</div>" />
                                                                    <dxe:ListEditItem Value="2" Text=" <div title='Cetip'>Cetip</div>" />
                                                                    <dxe:ListEditItem Value="3" Text="<div title='CBLC'>CBLC</div>" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                    </Columns>
                                                    <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                                                    <SettingsBehavior ColumnResizeMode="Disabled" />
                                                    <ClientSideEvents RowDblClick="function(s, e) {
                                                                gridPosicao.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPosicao);}"
                                                        Init="function(s, e) {e.cancel = true; }" />
                                                    <SettingsDetail ShowDetailButtons="False" />
                                                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                                                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                    </Styles>
                                                    <Images>
                                                    </Images>
                                                    <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posição" />
                                                </dxwgv:ASPxGridView>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                    <ClientSideEvents CloseUp="function(s, e) {gridPosicao.ClearFilter();}" PopUp="function(s, e) {gridPosicao.PerformCallback();}" />
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOperacaoRendaFixa" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnPreRender="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnInitNewRow="gridCadastro_InitNewRow" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomUnboundColumnData = "gridCadastro_CustomUnboundColumnData" OnStartRowEditing="gridCadastro_RowEditing">
                                        
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdPosicao" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="15%" ExportWidth="200">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" Caption="Data" VisibleIndex="4" Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="5" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                        <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                                        <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                                        <dxe:ListEditItem Value="22" Text="<div title='Ingresso em Ativos com Impacto na Quantidade'>Ingresso em Ativos com Impacto na Quantidade</div>" />
                                                        <dxe:ListEditItem Value="23" Text="<div title='Ingresso em Ativos com Impacto na Cota'>Ingresso em Ativos com Impacto na Cota</div>" />
                                                        <dxe:ListEditItem Value="24" Text="<div title='Retirada em Ativos com Impacto na Quantidade'>Retirada em Ativos com Impacto na Quantidade</div>" />
                                                        <dxe:ListEditItem Value="25" Text="<div title='Retirada em Ativos com Impacto na Cota'>Retirada em Ativos com Impacto na Cota</div>" />                                                        
                                                        <dxe:ListEditItem Value="26" Text="<div title='Exercício Opção'>Exercício Opção</div>" />                                                        
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" VisibleIndex="6"
                                                Width="17%" Settings-AutoFilterCondition="Contains" >
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="12%"
                                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" VisibleIndex="8"
                                                Width="13%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorLiq" Caption="Valor" VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorTributos" VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>                                                                                     
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataRegistro" Visible="false">
                                            </dxwgv:GridViewDataColumn>                                                                                       
                                            <dxwgv:GridViewDataColumn FieldName="TaxaOperacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TipoNegociacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TaxaVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="PUVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ValorVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdLiquidacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataOperacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="OperacaoTermo" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IsIPO" Visible="false">
                                            </dxwgv:GridViewDataColumn>

                                            <dxwgv:GridViewDataColumn FieldName="PercentualPuFace" Visible="false">
                                            </dxwgv:GridViewDataColumn>                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdLocalNegociacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdAgenteCustodia" Visible="false">
                                            </dxwgv:GridViewDataColumn>                                            

                                            <%--<dxwgv:GridViewDataColumn FieldName="TipoContraparte" UnboundType="String" Visible="true"/>--%>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoContraparte" UnboundType="String" VisibleIndex="10" Caption="Contraparte"  Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Mercado" />
                                                        <dxe:ListEditItem Value="2" Text="Cliente" />
                                                        </Items>
                                                        </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NomeContraparte" UnboundType="String" VisibleIndex="11" Width="10%" Caption="Nome Contraparte" >
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacaoEspelho" Visible="false"/>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdLiquidacao" Caption="Clearing" VisibleIndex="12" Width="8%" ExportWidth="120">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="0" Text="-" />
                                                        <dxe:ListEditItem Value="1" Text="Selic" />
                                                        <dxe:ListEditItem Value="2" Text="Cetip" />
                                                        <dxe:ListEditItem Value="3" Text="CBLC" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                        </Columns>

                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents 
                                                                                            KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                                            ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                                                            LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                                                                            ValueChanged="function(s, e) {if (gridCadastro.cpMultiConta == 'True' ) 
                                                                                                                             {dropConta.PerformCallback(); } 
                                                                                                                             }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCliente" Style="width: 272px;" runat="server" CssClass="textNome"
                                                                        Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelMercadoCliente" runat="server" CssClass="labelRequired" Text="Mercado/Cliente:" OnInit="labelMercadoCliente_OnInit"></asp:Label>
                                                                </td>
                                                                <td>
                                                                <dxe:ASPxComboBox ID="dropMercadoCliente" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoContraparte")%>' OnLoad="dropMercadoCliente_OnLoad" OnInit="dropMercadoCliente_OnInit"  >
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Mercado" />
                                                                            <dxe:ListEditItem Value="2" Text="Cliente" />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="dropMercadoClienteSelectedIndexChanged" Init="dropMercadoClienteSelectedIndexChanged"/>                                                                            
                                                                </dxe:ASPxComboBox>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr id="trAgenteContraParte" style="display:none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdAgenteContraParte" runat="server" CssClass="labelRequired"
                                                                        Text="Contraparte:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnAgenteContraParte" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnAgenteContraParte" Text='<%#Eval("IdAgenteContraParte")%>'    OnInit="btnAgenteContraParte_OnInit"                                                                     MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente').value = '';} "
                                                                            ButtonClick="function(s, e) {popupAgenteMercado.ShowAtElementByID(s.name);}"
                                                                            LostFocus="function(s, e) {OnLostFocus(popupMensagemAgenteMercado,CallbackAgenteMercado, btnAgenteContraParte);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeAgente" runat="server" CssClass="textNomeAgente" Enabled="false" Text='<%#Eval("AgenteMercadoContraparte")%>'></asp:TextBox>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr id="trCarteiraContraParte" style="display:none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdCarteiraContraparte" runat="server" CssClass="labelRequired"
                                                                        Text="Contraparte:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnCarteiraContraparte" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnCarteiraContraparte" Text='<%#Eval("IdCarteiraContraparte")%>' OnLoad="btnCarteiraContraparte_OnLoad" OnInit="btnCarteiraContraparte_OnInit"
                                                                        MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira,CallbackCarteira, btnCarteiraContraparte);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNomeCarteira" Enabled="false"
                                                                    Text='<%#Eval("ApelidoCarteiraContraparte")%>'></asp:TextBox>
                                                                    
                                                                </td>
                                                            </tr>






                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList" ClientInstanceName="dropTipoOperacao"
                                                                        CssClass="dropDownListCurto_2" OnLoad="dropTipoOperacao_Load" Text='<%#Eval("TipoOperacao")%>'>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                                                                                                    if (s.GetSelectedItem().value == 3 || s.GetSelectedItem().value == 4)
                                                                                                    {   TrataCompromisso(true);     }
                                                                                                    else
                                                                                                    {   TrataCompromisso(false);    }    
                                                                                                    
                                                                                                    textQuantidade.SetEnabled(true);
                                                                                                    textValor.SetEnabled(true);
                                                                                                    
                                                                                                    if (s.GetSelectedItem().value == 6)
                                                                                                    {
                                                                                                        textQuantidade.SetEnabled(false);
                                                                                                        textValor.SetEnabled(false);                                                                                                        
                                                                                                    }
                                                                                                    else if (s.GetSelectedItem().value == 26)
                                                                                                    {
                                                                                                        callBackCalculaPremio.SendCallback();
                                                                                                    } 
                                                                                                
                                                                                                }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoNegociacao" runat="server" CssClass="labelNormal" Text="Categoria:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoNegociacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoNegociacao")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Negociação" />
                                                                            <dxe:ListEditItem Value="2" Text="Disp. Venda" />
                                                                            <dxe:ListEditItem Value="3" Text="Vencimento" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        
                                                        <table border="0">
                                                            <tr>
                                                                <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal" Visible="false"></asp:Label>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="hiddenIdOperacao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdOperacao")%>' />                                                                    
                                                                    <asp:TextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>' />
                                                                    <asp:TextBox ID="hiddenDataModificacao" runat="server" CssClass="hiddenField" Text='<%#Eval("DataModificacao")%>' />
                                                                    <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px" Text='<%#Eval("DescricaoVirtual")%>'>
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label ID="labelMensagem2" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                                                    Visible="false"></asp:Label>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelPosicao" runat="server" CssClass="labelNormal" Text="Id Posição:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="hiddenIdOperacaoResgatada" runat="server" CssClass="hiddenField" Text='<%#Eval("IdOperacaoResgatada")%>' />
                                                                    <asp:TextBox ID="hiddenIdPosicao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdPosicaoResgatada")%>'/>
                                                                    <dxe:ASPxButtonEdit ID="btnEditPosicao" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditPosicao" ReadOnly="true" Width="380px" ClientSideEvents-Init='function(s, e) { callbackPosicaoOperacao.SendCallback(); }'>
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupPosicao.ShowAtElementByID(s.name);}" />
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAgenteCorretora" runat="server" CssClass="labelNormal" Text="Corretora:"> </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropAgenteCorretora" runat="server" ClientInstanceName="dropAgenteCorretora"
                                                                        DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="startswith"
                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" TextField="Nome"
                                                                        ValueField="IdAgente" Text='<%#Eval("IdAgenteCorretora")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCorretora" runat="server" CssClass="labelNormal" Text="Agente de Custódia:"> </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropAgenteCustodia" runat="server" ClientInstanceName="dropAgenteCustodia"
                                                                        DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="startswith"
                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" TextField="Nome"
                                                                        ValueField="IdAgente" Text='<%#Eval("IdAgenteCustodia")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>		
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                        Text="Local Negociação:">
                                                                    </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" ClientInstanceName="dropLocalNegociacao" DataSourceID="EsDSLocalNegociacao"
                                                                        ValueField="IdLocalNegociacao" TextField="Descricao" IncrementalFilteringMode="startswith"
                                                                        ShowShadow="false" DropDownStyle="DropDown" 
                                                                        CssClass="dropDownListLongo" Text='<%#Eval("IdLocalNegociacao")%>'> 
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>	
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Registro: " />
                                                                </td>
                                                               <td>
                                                                    <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                        Value='<%#Eval("DataRegistro")%>' ClientEnabled="false" />
                                                                </td>
                                                                
                                                                <td align="right">
                                                                    <asp:Label ID="labelTaxaOperacao" runat="server" CssClass="labelRequired" Text="Tx Operação:"></asp:Label>
                                                                </td>
                                                                
                                                                <td style="width: 165px;">
                                                                    <dxe:ASPxSpinEdit ID="textTaxaOperacao" runat="server" CssClass="textValor_5" Style="float: left;"
                                                                        ClientInstanceName="textTaxaOperacao" MaxLength="25" NumberType="Float" DecimalPlaces="16"
                                                                        Text='<%#Eval("TaxaOperacao")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                        <asp:LinkButton ID="btnCalculaTaxa" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackTaxa.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="&nbsp" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                                
                                                            </tr>                                                                                                                        
                                                           
                                                            <tr>
                                                            <td class="td_Label">
                                                                    <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataOperacao" runat="server" ClientInstanceName="textDataOperacao"
                                                                        Value='<%#Eval("DataOperacao")%>' ClientSideEvents-DateChanged="function(s, e) {ASPxCallback2.SendCallback(document.getElementById(gridCadastro.cpHiddenIdTitulo).value);}"  />
                                                                </td>

                                                                <td align="right">
                                                                    <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelRequired" Text="Liquidação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao"
                                                                        Value='<%#Eval("DataLiquidacao")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td align="right">
                                                                    <asp:Label ID="labelDataVolta" runat="server" CssClass="labelNormal" Text="Data Volta:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataVolta" runat="server" ClientInstanceName="textDataVolta"
                                                                        ClientEnabled="false" Value='<%#Eval("DataVolta")%>' />
                                                                </td>
                                                            </tr>
                                                            
                                                            <!-- Opcional Forma Liquidacao/Custodia -->
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFormaLiquidacao" runat="server" CssClass="labelRequired" Text="Clearing:"
                                                                    OnInit="labelFormaLiquidacao_Init"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                                    CssClass="dropDownListCurto_5" DataSourceID="EsDSClearing"
                                                                        ValueField="IdClearing" TextField="Descricao" OnInit="dropFormaLiquidacao_Init"
                                                                        Text='<%# Eval("IdLiquidacao") %>'>                                                                          
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelLocalCustodia" runat="server" CssClass="labelRequired" Text="Local de Custódia:"
                                                                    OnInit="labelLocalCustodia_Init"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropLocalCustodia" ClientInstanceName="dropLocalCustodia" runat="server"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("IdCustodia")%>'  OnInit="dropLocalCustodia_Init"
                                                                        DataSourceID="EsDSLocalCustodia" ValueField="IdLocalCustodia" TextField="Descricao">
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                               <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Índice:"> </asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropIndiceVolta" runat="server" ClientInstanceName="dropIndiceVolta"
                                                                        ClientEnabled="false" CssClass="dropDownList" IncrementalFilteringMode="StartsWith"
                                                                        ShowShadow="false" DropDownStyle="DropDown" DataSourceID="EsDSIndice" ValueField="IdIndice"
                                                                        TextField="Descricao" Text='<%#Eval("IdIndiceVolta")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); }
                                                                                " />
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                                                                            if(s.GetSelectedIndex() != -1)
                                                                                {
                                                                                    textPUVolta.SetEnabled(false);
                                                                                    textValorVolta.SetEnabled(false);
                                                                                    textPUVolta.SetText('');
                                                                                    textValorVolta.SetText('');    
                                                                                }
                                                                                else
                                                                                {
                                                                                    textPUVolta.SetEnabled(true);
                                                                                    textValorVolta.SetEnabled(true);
                                                                                }
                                                                                
                                                                             }
                                                                                " />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Qtde:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade" DisplayFormatString="N"
                                                                        MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("Quantidade")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td align="right">
                                                                    <asp:Label ID="labelTaxaVolta" runat="server" CssClass="labelNormal" Text="Taxa Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTaxaVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaVolta"
                                                                        ClientEnabled="false" MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("TaxaVolta")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {if(dropIndiceVolta.GetSelectedIndex() == -1) ASPxCallback3.SendCallback(); }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelPUOperacao" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textPUOperacao" runat="server" Style="float: left;" CssClass="textValor_5"
                                                                        ClientInstanceName="textPUOperacao" MaxLength="25" NumberType="Float" DecimalPlaces="12" DisplayFormatString="N8"
                                                                        Text='<%#Eval("PUOperacao")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                    </dxe:ASPxSpinEdit>

                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                        <asp:LinkButton ID="btnCalculaPU" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackPU.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal11" runat="server" Text="&nbsp" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                                
                                                                <td align="right">
                                                                    <asp:Label ID="labelPUVolta" runat="server" CssClass="labelNormal" Text="PU Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textPUVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textPUVolta" DisplayFormatString="N8"
                                                                        ClientEnabled="false" MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("PUVolta")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblPercentualPuFace" runat="server" CssClass="labelNormal" Text="% PU Face:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textPercentualPuFace" runat="server" Style="float: left;" CssClass="textValor_5" DisplayFormatString="N8"
                                                                        ClientInstanceName="textPercentualPuFace" MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("PercentualPuFace")%>' 
                                                                        OnLoad="textPercentualPuFace_Load">
                                                                        <ClientSideEvents LostFocus="function(s, e) {callbackPercentualPuFace.SendCallback(); }"/>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td align="right">
                                                                    <asp:Label ID="labelValorVolta" runat="server" CssClass="labelNormal" Text="Valor Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textValorVolta"
                                                                        ClientEnabled="false" MaxLength="14" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorVolta")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                                                </td>
                                                                
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor" DisplayFormatString="N"
                                                                        MaxLength="12" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Valor")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1); }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>                                                                
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Valor Corretagem:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorCorretagem" runat="server" CssClass="textValor_5" ClientInstanceName="textValorCorretagem"
                                                                        MaxLength="19" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorCorretagem")%>'>                                                                        
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>                                                              
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                                                        DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown" 
                                                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdTrader"
                                                                                        Text='<%#Eval("IdTrader")%>'>
                                                                      <ClientSideEvents LostFocus="function(s, e) { 
                                                                                                        if(s.GetSelectedIndex() == -1)
                                                                                                            s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>      
                                                                </td>
                                                                <tr>
                                                                <td class="td_Label">
                                                                <asp:Label ID="labelConta" runat="server" CssClass="labelNormal" Text="Conta:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropConta" 
				                                                                      runat="server" 
				                                                                      ClientInstanceName="dropConta"
				                                                                      DataSourceID="EsDSContaCorrente" 
				                                                                      IncrementalFilteringMode="Contains"
				                                                                      ShowShadow="false" 
				                                                                      DropDownStyle="DropDown" 
				                                                                      CssClass="dropDownList" 
				                                                                      TextField="Numero" 
				                                                                      ValueField="IdConta"
				                                                                      OnCallback="dropConta_Callback"
				                                                                      Text='<%#Eval("IdConta")%>'>
                                                                    <ClientSideEvents LostFocus="function(s, e) { 
								                                                                    if(s.GetSelectedIndex() == -1)
									                                                                    s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>  
                                                                                    
                                                                </td>
                                                                </tr>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                                            </tr>
                                
                                                            <tr>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCheck" runat="server" CssClass="labelNormal" Text="Termo de RF:"></asp:Label>
                                                                </td>
                                                                
                                                                <td>
                                                                    <asp:CheckBox ID="chkOperacaoTermo" runat="server" Text="" />
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCodigoContraParte" runat="server" CssClass="labelNormal" Text="Contraparte:"
                                                                        OnInit="labelCodigoContraParte_Init"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textCodigoContraParte" runat="server" OnInit="textCodigoContraParte_Init" Text='<%# Eval("CodigoContraParte") %>'></asp:TextBox>
                                                                </td>                                                                                                                        
                                                                
                                                           </tr>
                                                           <tr>                                                           
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdVinculo" runat="server" CssClass="labelNormal" Text="Id Vinculado:" OnInit="labelIdVinculo_Init"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textIdVinculo" runat="server" CssClass="textValor_5" ClientInstanceName="textIdVinculo" OnInit="textIdVinculo_Init"
                                                                        MaxLength="12" NumberType="Integer" Text='<%#Eval("IdOperacaoVinculo")%>'/>
                                                                </td>                                                                
                                                            </tr>    
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Mov:"> </asp:Label>
                                                                </td>                
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                                                        DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                                                        CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                                                        Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                                                    </dxe:ASPxComboBox>         
                                                                </td>
                                                            </tr>                                                                 
                                                           
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"> </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" Style="width: 324px;"
                                                                        CssClass="textLongo5" Text='<%#Eval("Observacao")%>' />
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline-table">
                                                                        <asp:LinkButton ID="btnObservacao" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            Visible="false" OnClientClick="operacao='salvarObservacao'; gridCadastro.PerformCallback('btnObservacao'); return false;">
                                                                            <asp:Literal ID="Literal15" runat="server" Text="Salva Obs." /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>

                 											<tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="IPO:"></asp:Label>
                                                                </td>
                                                                
                                                                <td>
                                                                    <asp:CheckBox ID="chkIsIPO" runat="server" Text="" />
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <div class="linhaH"></div>
                                                        
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" LeftMargin="30" RightMargin="30"/>
        
        <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect" LowLevelBind="true" />
        
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSPosicao" runat="server" OnesSelect="EsDSPosicao_esSelect" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixaFiltro" runat="server" OnesSelect="EsDSTituloRendaFixaFiltro_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSLocalCustodia" runat="server" OnesSelect="EsDSLocalCustodia_esSelect" />
        <cc1:esDataSource ID="EsDSClearing" runat="server" OnesSelect="EsDSClearing_esSelect" />        
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />														        
        <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercadoCorretora" runat="server" OnesSelect="EsDSAgenteMercadoCorretora_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />

    </form>
</body>
</html>
