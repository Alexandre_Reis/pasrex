﻿<%@ page language="C#" autoeventwireup="true" inherits="Processamento_TravamentoCotas, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataCarteira(data) {
        var resultSplit = data.split('|');
        hiddenIdCarteira.SetValue(resultSplit[0]);
        textDataProcessamento.SetValue(new Date(resultSplit[1], resultSplit[2], resultSplit[3])); 
        textValorCota.SetValue(resultSplit[4].replace(/,/g, '.'));
        textValorCotaCalculada .SetValue(resultSplit[4].replace(/,/g, '.'));
        ASPxCallbackCarteira.SendCallback(resultSplit[0]);
        popupCarteira.HideWindow();
        btnEditCarteira.Focus();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallbackCarteira" runat="server" OnCallback="ASPxCallback2_Callback_Carteira">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCarteira.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupCarteira" ClientInstanceName="popupCarteira" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCarteira" runat="server" Width="100%"
                    ClientInstanceName="gridCarteira"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCarteira" KeyFieldName="CompositeCarteira"
                    OnCustomDataCallback="gridCarteira_CustomDataCallback" 
                    OnCustomUnboundColumnData="gridCarteira_CustomUnboundColumnData"
                    OnCustomCallback="gridCarteira_CustomCallback"
                    OnHtmlRowCreated="gridCarteira_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" VisibleIndex="0" Width="15%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="0" Width="45%"/>
                <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="0" Width="20%"/>
                <dxwgv:GridViewDataTextColumn FieldName="CotaFechamento" VisibleIndex="0" Width="20%"/>
                <dxwgv:GridViewDataTextColumn FieldName="CotaImportada" VisibleIndex="0" Width="20%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridCarteira.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCarteira); 
                }" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Carteira/Cotas" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCarteira.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Travamento de Cotas"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdTravamentoCota" DataSourceID="EsDSTravamentoCotas"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="ID Carteira" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left"></dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="DescricaoCarteira" Caption="Carteira" UnboundType="String" VisibleIndex="1" Width="40%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataColumn FieldName="DataProcessamento" Caption="Data" UnboundType="String" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoBloqueio" Caption="Tipo Bloqueio" VisibleIndex="3" Width="15%">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="<div title='Sem Trava'>Sem Trava</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Crava Cota'>Crava Cota</div>" />
                            <dxe:ListEditItem Value="3" Text="<div title='Shadow'>Shadow</div>" />
                            <dxe:ListEditItem Value="4" Text="<div title='Travar Reprocessamento'>Travar Reprocessamento</div>" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>  
                
                <dxwgv:GridViewDataTextColumn FieldName="ValorCota" Caption="Valor Cota" VisibleIndex="4" Width="6%" HeaderStyle-HorizontalAlign="Center">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.000000000000;(#,##0.000000000000);0.000000000000}"></PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdCarteira" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCarteira")%>' ClientInstanceName="hiddenIdCarteira" />
                        <dxe:ASPxSpinEdit ID="textValorCotaCalculada" runat="server" CssClass="hiddenField" Text="<%#Bind('ValorCota')%>" ClientInstanceName="textValorCotaCalculada"/>
                        
                        <table>
                   
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditCarteira" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditCarteira" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCarteira")%>' OnLoad="btnEditCarteira_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelDataProcessamento" runat="server" CssClass="labelRequired" Text="Data Processamento:"  /></td> 
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataProcessamento" runat="server" ClientInstanceName="textDataProcessamento" Value='<%#Eval("DataProcessamento")%>'/>
                                </td>                                                   
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelValorCota" runat="server" CssClass="labelRequired" Text="Valor Cota:"></asp:Label>                    
                                </td>                    
                                <td>
                                    <dxe:ASPxSpinEdit ID="textValorCota" runat="server" CssClass="textCurto" ClientInstanceName="textValorCota"
		                                      MaxLength="16" NumberType="Float" DecimalPlaces="12" Text="<%#Bind('ValorCota')%>" AllowNull="false">		                              
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoBloqueio" runat="server" CssClass="labelRequired" Text="Tipo Bloqueio:"></asp:Label>                    
                                </td>                    
                                <td>      
                                    <dxe:ASPxComboBox ID="dropTipoBloqueio" runat="server" 
                                                        ShowShadow="false" CssClass="dropDownListCurto_1" Text='<%#Eval("TipoBloqueio")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Sem Trava" />
                                            <dxe:ListEditItem Value="2" Text="Crava Cota"/>
                                            <dxe:ListEditItem Value="3" Text="Shadow"/>
                                            <dxe:ListEditItem Value="4" Text="Travar Reprocessamento"/>
                                         </Items>                                                            
                                    </dxe:ASPxComboBox>    
                                </td>
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTravamentoCotas" runat="server" OnesSelect="EsDSTravamentoCotas_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>