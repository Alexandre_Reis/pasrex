<%@ WebHandler Language="C#" Class="ControllerProcessaCliente" %>

using System;
using System.Web;
using Newtonsoft.Json;
using Financial.Util;
using Financial.Investidor;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.Specialized;

using EntitySpaces.Interfaces;
using Financial.Processamento;
using Financial.Investidor.Controller;
using Financial.Web.Enums;
using Financial.Investidor.Enums;
using Financial.Security;


public class ControllerProcessaCliente : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponse jsonResponse = new JSONResponse();

        Int32 idCliente = Convert.ToInt32(context.Request["idCliente"]);

        TipoProcessamento tipoProcessamento = (TipoProcessamento)Convert.ToInt32(context.Request["tipoProcesso"]);
        int perfil = Convert.ToInt32(context.Request["perfil"]);

        DateTime dataFinal = new DateTime();
        bool mantemFuturo = false;
        if (tipoProcessamento == TipoProcessamento.Retroacao || tipoProcessamento == TipoProcessamento.AvancoPeriodo)
        {
            dataFinal = Convert.ToDateTime(context.Request["dataFinal"]);
        }

        if (tipoProcessamento == TipoProcessamento.Retroacao)
        {
            mantemFuturo = Convert.ToBoolean(context.Request["mantemFuturo"]);
        }

        bool ignoraOperacoes = false;
        if (tipoProcessamento == TipoProcessamento.Abertura ||
            tipoProcessamento == TipoProcessamento.Fechamento ||
            tipoProcessamento == TipoProcessamento.Divulgacao)
        {
            ignoraOperacoes = Convert.ToBoolean(context.Request["ignoraOperacoes"]);
        }

        bool remuneraRF = false; //Para for�ar a remunera��o no pr�prio c�lculo do dia ou no avan�a ou retrocessa per�odo
        if (tipoProcessamento == TipoProcessamento.Fechamento || tipoProcessamento == TipoProcessamento.AvancoPeriodo || tipoProcessamento == TipoProcessamento.Retroacao)
        {
            if (tipoProcessamento == TipoProcessamento.Fechamento)
            {
                remuneraRF = Convert.ToBoolean(context.Request["remuneraRF"]);
            }
            else // No avan�a per�odo ou retrocesso deve ser remunerar, pois nao existe a op��o e deve remunerar
            {
                remuneraRF = true;
            }
        }

        bool cravaCota = false; //Para cravar a cota e jogar a diferenca no CPR de d+1
        if (tipoProcessamento == TipoProcessamento.Fechamento || tipoProcessamento == TipoProcessamento.AvancoPeriodo)
        {
            cravaCota = Convert.ToBoolean(context.Request["cravaCota"]);
        }

        bool aberturaAutomatica = false; //Para executar a Abertura Automatica
        if (tipoProcessamento == TipoProcessamento.AvancoPeriodo)
        {
            PerfilProcessamento perfilProc = new PerfilProcessamento();
            if (perfil > 0 && perfilProc.LoadByPrimaryKey(perfil))
            {
                aberturaAutomatica = perfilProc.AberturaAutomatica.ToUpper().Equals("S");
            }
        }
        
        bool resetCusto = false; //Reset de custo para cont�bil
        if (tipoProcessamento == TipoProcessamento.Fechamento)
        {
            resetCusto = Convert.ToBoolean(context.Request["resetCusto"]);
        }

        ParametrosProcessamento parametrosProcessamento = new ParametrosProcessamento();

        bool processaBolsa = Convert.ToBoolean(context.Request["processaBolsa"]);
        bool processaBMF = Convert.ToBoolean(context.Request["processaBMF"]);
        bool processaRendaFixa = Convert.ToBoolean(context.Request["processaRendaFixa"]);
        bool processaSwap = Convert.ToBoolean(context.Request["processaSwap"]);
        bool processaFundo = Convert.ToBoolean(context.Request["processaFundo"]);
        bool processaCambio = Convert.ToBoolean(context.Request["processaCambio"]);

        IntegracaoBolsa integracaoBolsa = (IntegracaoBolsa)ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa;
        IntegracaoBMF integracaoBMF = (IntegracaoBMF)ParametrosConfiguracaoSistema.Integracoes.IntegracaoBMF;
        IntegracaoCC integracaoCC = (IntegracaoCC)ParametrosConfiguracaoSistema.Integracoes.IntegracaoCC;
        IntegracaoRendaFixa integracaoRendaFixa = (IntegracaoRendaFixa)ParametrosConfiguracaoSistema.Integracoes.IntegracaoRendaFixa;

        bool integraBolsa = Convert.ToBoolean(context.Request["integraBolsa"]) == true && integracaoBolsa != IntegracaoBolsa.NaoIntegra;
        bool integraBTC = Convert.ToBoolean(context.Request["integraBolsa"]) == true && integracaoBolsa == IntegracaoBolsa.Sinacor_BTC;
        bool integraBMF = Convert.ToBoolean(context.Request["integraBMF"]) == true && integracaoBMF != IntegracaoBMF.NaoIntegra;
        bool integraCC = Convert.ToBoolean(context.Request["integraCC"]) == true && integracaoCC != IntegracaoCC.NaoIntegra;

        if (Convert.ToBoolean(context.Request["integraRendaFixa"]) == false)
        {
            integracaoRendaFixa = IntegracaoRendaFixa.NaoIntegra;
        }

        TabelaInterfaceClienteCollection tabelaInterfaceClienteCollection = new TabelaInterfaceClienteCollection();
        tabelaInterfaceClienteCollection.Query.Select(tabelaInterfaceClienteCollection.Query.TipoInterface);
        tabelaInterfaceClienteCollection.Query.Where(tabelaInterfaceClienteCollection.Query.IdCliente.Equal(idCliente));
        tabelaInterfaceClienteCollection.Query.Load();

        List<int> listaTabelaInterfaceCliente = new List<int>();

        foreach (TabelaInterfaceCliente tabelaInterfaceCliente in tabelaInterfaceClienteCollection)
        {
            listaTabelaInterfaceCliente.Add(tabelaInterfaceCliente.TipoInterface.Value);
        }
    
        parametrosProcessamento.ProcessaBolsa = processaBolsa;
        parametrosProcessamento.ProcessaBMF = processaBMF;
        parametrosProcessamento.ProcessaRendaFixa = processaRendaFixa;
        parametrosProcessamento.ProcessaSwap = processaSwap;
        parametrosProcessamento.ProcessaFundo = processaFundo;
        parametrosProcessamento.ProcessaCambio = processaCambio;
        parametrosProcessamento.IntegraBolsa = integraBolsa;
        parametrosProcessamento.IntegraBTC = integraBTC;
        parametrosProcessamento.IntegraBMF = integraBMF;
        parametrosProcessamento.IntegracaoRendaFixa = (int)integracaoRendaFixa;
        parametrosProcessamento.IntegraCC = integraCC;
        parametrosProcessamento.IgnoraOperacoes = ignoraOperacoes;
        parametrosProcessamento.MantemFuturo = mantemFuturo;
        parametrosProcessamento.RemuneraRF = remuneraRF;
        parametrosProcessamento.CravaCota = cravaCota;
        parametrosProcessamento.ResetCusto = resetCusto;
        parametrosProcessamento.ListaTabelaInterfaceCliente = listaTabelaInterfaceCliente;
        parametrosProcessamento.AberturaAutomatica = aberturaAutomatica;
        parametrosProcessamento.PerfilProcessamento = perfil;
        parametrosProcessamento.TipoProcessamento = tipoProcessamento;

        bool processamentoViaWebService = ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        
        try
        {
            if (tipoProcessamento == TipoProcessamento.Abertura && cliente.Status != (int)StatusCliente.Divulgado)
            {
                DateTime dataDia = cliente.DataDia.Value;

                DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataDia, 1, cliente.IdLocal.Value, Financial.Common.Enums.TipoFeriado.Outros);

                if (cliente.DataDia != cliente.DataImplantacao)
                {
                    ProcessoPeriodo processoPeriodo = new ProcessoPeriodo();
                    processoPeriodo.ProcessaPeriodo(idCliente, TipoProcessamento.Retroacao, parametrosProcessamento, dataAnterior);
                    processoPeriodo.ProcessaPeriodo(idCliente, TipoProcessamento.AvancoPeriodo, parametrosProcessamento, dataDia);
                }
                else
                {
                    cliente.Status = (int)StatusCliente.Aberto;
                    cliente.Save();

                }
            

            }
            else if (tipoProcessamento == TipoProcessamento.Abertura ||
                     tipoProcessamento == TipoProcessamento.Fechamento ||
                     tipoProcessamento == TipoProcessamento.Divulgacao)
            {

                if (processamentoViaWebService)
                {
                    WSInvestidor.Investidor wsInvestidor = this.InitWSInvestidor(context);
                    WSInvestidor.ParametrosProcessamento parametrosCasted = this.CastParametrosProcessamento(parametrosProcessamento);
                    wsInvestidor.ProcessaDiario(idCliente, (WSInvestidor.TipoProcessamento)tipoProcessamento, parametrosCasted);
                }
                else
                {
                    ProcessoDiario processoDiario = new ProcessoDiario();
                    processoDiario.ProcessaDiario(idCliente, tipoProcessamento, parametrosProcessamento);
                }
            }
            else
            {
                if (processamentoViaWebService)
                {
                    WSInvestidor.Investidor wsInvestidor = this.InitWSInvestidor(context);
                    WSInvestidor.ParametrosProcessamento parametrosCasted = this.CastParametrosProcessamento(parametrosProcessamento);
                    wsInvestidor.ProcessaPeriodo(idCliente, (WSInvestidor.TipoProcessamento)tipoProcessamento, parametrosCasted, dataFinal);
                }
                else
                {
                    ProcessoPeriodo processoPeriodo = new ProcessoPeriodo();
                    processoPeriodo.ProcessaPeriodo(idCliente, tipoProcessamento, parametrosProcessamento, dataFinal);
                }
            }
            jsonResponse.success = true;
        }
        catch (Exception e)
        {
            const string STACKTRACE_TAG = "#STACKTRACE#";
            jsonResponse.success = false;

            string errorMessage = e.Message;
            string errorDetail = e.StackTrace;
            
            if (e.Message.Contains(STACKTRACE_TAG))
            {
                string[] splitted = System.Text.RegularExpressions.Regex.Split(e.Message, STACKTRACE_TAG);
                errorMessage = splitted[0];
                errorDetail = splitted[1];
            }
            
            jsonResponse.errorMessage = errorMessage;
            jsonResponse.errorDetail = errorDetail;
            
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(e, true);
            // Get the top stack frame
            //var frame = st.GetFrame(0);
            System.Diagnostics.StackFrame frame = st.GetFrame(0);
            // Get the line number from the stack frame
            int line = frame.GetFileLineNumber();
            //frame.GetMethod
        }
        string serializedText = JsonConvert.SerializeObject(jsonResponse, Formatting.None);
        context.Response.ContentType = "text/html";
        context.Response.Write(serializedText);
    }

    private WSInvestidor.ParametrosProcessamento CastParametrosProcessamento(Financial.Investidor.Controller.ParametrosProcessamento paramIn)
    {
        WSInvestidor.ParametrosProcessamento paramOut = new WSInvestidor.ParametrosProcessamento();
        paramOut.IgnoraOperacoes = paramIn.IgnoraOperacoes;
        paramOut.IntegraBMF = paramIn.IntegraBMF;
        paramOut.IntegraBolsa = paramIn.IntegraBolsa;
        paramOut.IntegraBTC = paramIn.IntegraBTC;
        paramOut.IntegraCC = paramIn.IntegraCC;
        paramOut.IntegracaoRendaFixa = paramIn.IntegracaoRendaFixa;
        paramOut.MantemFuturo = paramIn.MantemFuturo;
        paramOut.ProcessaBMF = paramIn.ProcessaBMF;
        paramOut.ProcessaBolsa = paramIn.ProcessaBolsa;
        paramOut.ProcessaRendaFixa = paramIn.ProcessaRendaFixa;
        paramOut.ProcessaSwap = paramIn.ProcessaSwap;
        paramOut.ProcessaFundo = paramIn.ProcessaFundo;
        paramOut.ProcessaCambio = paramIn.ProcessaCambio;
        paramOut.RemuneraRF = paramIn.RemuneraRF;        

        return paramOut;
    }

    private WSInvestidor.Investidor InitWSInvestidor(HttpContext context)
    {
        WSInvestidor.Investidor wsInvestidor = new WSInvestidor.Investidor();
        string webServicesProviderUrl = ConfigurationManager.AppSettings["WebServicesProviderUrl"];
        if (!string.IsNullOrEmpty(webServicesProviderUrl))
        {
            wsInvestidor.Url = webServicesProviderUrl + "/Investidor.asmx";
        }

        WSInvestidor.ValidateLogin ticket = new WSInvestidor.ValidateLogin();

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(context.User.Identity.Name);

        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        ticket.Username = usuario.Login;
        ticket.Password = financialMembershipProvider.UnEncodePassword(usuario.Senha);

        wsInvestidor.ValidateLoginValue = ticket;
        return wsInvestidor;
    }

    public class JSONResponse
    {
        public bool success;
        public string errorMessage;
        public string errorDetail;
        public void Response()
        {
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}