<%@ WebService Language="C#" Class="InvestidorWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Security;
using Financial.Processamento;
using System.Collections.Specialized;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class InvestidorWS : System.Web.Services.WebService
{

    [SoapHeader("Authentication")]
    [WebMethod]
    public void ProcessaDiario(int idCliente, DateTime data, Financial.Investidor.Enums.TipoProcessamento tipoProcessamento, Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        Cliente cliente = new Cliente();
        cliente.Query.Select(cliente.Query.DataDia);
        cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
        cliente.Query.Load();

        if (cliente.DataDia.Value != data)
        {
            string descricao = "Processo autom�tico de c�lculo em data errada. Cliente " + idCliente.ToString();
            HistoricoLog h = new HistoricoLog();
            h.InsereHistoricoLog(DateTime.Now, 
                                 DateTime.Now, 
                                 descricao, 
                                 HttpContext.Current.User.Identity.Name,
                                 "", 
                                 "", 
                                 Financial.Security.Enums.HistoricoLogOrigem.Outros);
        }

        ProcessoDiario processoDiario = new ProcessoDiario();
        processoDiario.ProcessaDiario(idCliente, tipoProcessamento, parametrosProcessamento);
    }
    
    [SoapHeader("Authentication")]
    [WebMethod]
    public void ProcessaDiario(int idCliente, Financial.Investidor.Enums.TipoProcessamento tipoProcessamento, Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        ProcessoDiario processoDiario = new ProcessoDiario();
        processoDiario.ProcessaDiario(idCliente, tipoProcessamento, parametrosProcessamento);
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public void ProcessaPeriodo(int idCliente, Financial.Investidor.Enums.TipoProcessamento tipoProcessamento, Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento, DateTime dataFinal)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        ProcessoPeriodo processoPeriodo = new ProcessoPeriodo();
        processoPeriodo.ProcessaPeriodo(idCliente, tipoProcessamento, parametrosProcessamento, dataFinal);
    }

    public ValidateLogin Authentication;

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

}

