<%@ WebService Language="C#" Class="HistoricoCotaWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Financial.Fundo;
using Financial.Security;
using System.Collections.Specialized;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class HistoricoCotaWS : System.Web.Services.WebService
{

    public ValidateLogin Authentication;
    
    [SoapHeader("Authentication")]
    [WebMethod]
    [XmlInclude(typeof(HistoricoCotaRetorno))]
    public HistoricoCotaRetornoCollection Exporta(DateTime? DataInicio, DateTime? DataFim, int? IdCarteira, bool? calcularRetornos)
    {
        CalculoMedida calculoMedida = null;
        HistoricoCotaCollection cotas = new HistoricoCotaCollection();
        HistoricoCotaRetornoCollection cotasRetornos = new HistoricoCotaRetornoCollection();

        if (DataInicio.HasValue)
        {
            cotas.Query.Where(cotas.Query.Data.GreaterThanOrEqual(DataInicio.Value));
        }

        if (DataFim.HasValue)
        {
            cotas.Query.Where(cotas.Query.Data.LessThanOrEqual(DataFim.Value));
        }

        if (IdCarteira.HasValue)
        {
            cotas.Query.Where(cotas.Query.IdCarteira.Equal(IdCarteira.Value));
        }

        cotas.Query.OrderBy(cotas.Query.IdCarteira.Ascending, cotas.Query.Data.Ascending);
        cotas.Load(cotas.Query);

        calcularRetornos = calcularRetornos.HasValue && (calcularRetornos.Value == true);

        foreach (HistoricoCota cota in cotas)
        {
            if (calcularRetornos.Value)
            {
                if (calculoMedida == null || calculoMedida.carteira.IdCarteira != cota.IdCarteira)
                {
                    calculoMedida = new CalculoMedida(cota.IdCarteira.Value);
                }
            }
            HistoricoCotaRetorno cotaRetorno = new HistoricoCotaRetorno(cota, calculoMedida);
            cotasRetornos.Add(cotaRetorno);
        }

        return cotasRetornos;
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }



}

namespace Financial.Fundo
{
    public partial class HistoricoCotaRetorno : HistoricoCota
    {
        public decimal RentabilidadeDia;
        public decimal RentabilidadeMes;
        public decimal RentabilidadeAno;

        public HistoricoCotaRetorno()
        {
        }
        public HistoricoCotaRetorno(HistoricoCota historicoCota, CalculoMedida calculoMedida)
        {
            this.Data = historicoCota.Data;
            this.IdCarteira = historicoCota.IdCarteira;
            this.CotaAbertura = historicoCota.CotaAbertura;
            this.CotaFechamento = historicoCota.CotaFechamento;
            this.CotaBruta = historicoCota.CotaBruta;
            this.PLAbertura = historicoCota.PLAbertura;
            this.PLFechamento = historicoCota.PLFechamento;
            this.PatrimonioBruto = historicoCota.PatrimonioBruto;
            this.QuantidadeFechamento = historicoCota.QuantidadeFechamento;
            this.AjustePL = historicoCota.AjustePL;

            if(calculoMedida != null){

                decimal retornoDia;
                try
                {
                    retornoDia = calculoMedida.CalculaRetornoDia(this.Data.Value);
                }
                catch
                {
                    retornoDia = 0M;
                }

                decimal retornoMes;
                try
                {
                    retornoMes = calculoMedida.CalculaRetornoMes(this.Data.Value);
                }
                catch
                {
                    retornoMes = 0M;
                }

                decimal retornoAno;
                try
                {
                    retornoAno = calculoMedida.CalculaRetornoAno(this.Data.Value);
                }
                catch
                {
                    retornoAno = 0M;
                }
                                
                this.RentabilidadeDia = retornoDia;
                this.RentabilidadeMes = retornoMes;
                this.RentabilidadeAno = retornoAno;
            }
            
        }
    }


    public partial class HistoricoCotaRetornoCollection : System.Collections.Generic.List<HistoricoCotaRetorno>
    {

    }
}

