<%@ WebService Language="C#" Class="CadastroCotistaWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Financial.Integracao.Excel;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Specialized;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class CadastroCotistaWS : System.Web.Services.WebService
{
    public ValidateLogin Authentication;
    [SoapHeader("Authentication")]
    [WebMethod]
    public void Importa(int IdCotista, string Nome, byte Tipo, string Cpfcnpj, string IsentoIR, string IsentoIOF,
       byte StatusAtivo, byte TipoTributacao, byte TipoCotistaCVM, string CodigoInterface, string Endereco, string Numero, 
        string Complemento, string Bairro, string Cidade, string CEP, string UF, string Pais, string EnderecoCom, 
        string NumeroCom, string ComplementoCom, string BairroCom, string CidadeCom, string CEPCom, string UFCom, 
        string PaisCom, string Fone, string Email, string FoneCom, string EmailCom, byte EstadoCivil, string NumeroRG,
        string EmissorRG, DateTime DataEmissaoRG, string Sexo, DateTime DataNascimento, string Profissao)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        ValoresExcelCadastroCotista item = new ValoresExcelCadastroCotista();
        //                                                                                  
        item.IdCotista = IdCotista;
        item.Nome = Nome;

        int maxlength = (int)CotistaMetadata.Meta().Columns.FindByColumnName(CotistaMetadata.ColumnNames.Apelido).CharacterMaxLength;
        item.Apelido = (item.Nome.Length <= maxlength) ? item.Nome : item.Nome.Substring(0, maxlength - 1);
        
        item.TipoPessoa = (Financial.CRM.Enums.TipoPessoa)Tipo;
        item.Cnpj = Cpfcnpj;
        //
        item.IsentoIR = IsentoIR;
        item.IsentoIOF = IsentoIOF;
        item.StatusCotista = (Financial.InvestidorCotista.Enums.StatusAtivoCotista)StatusAtivo;
        //
        item.TipoTributacaoCotista = (Financial.InvestidorCotista.Enums.TipoTributacaoCotista)TipoTributacao;
        //
        item.TipoCotistaCVM = (Financial.InvestidorCotista.Enums.TipoCotista)TipoCotistaCVM;
        //
        item.CodigoInterface = CodigoInterface;
        //
        item.EstadoCivilCotista = (Financial.CRM.Enums.EstadoCivilPessoa)EstadoCivil;
        item.Rg = NumeroRG;
        item.EmissorRg = EmissorRG;
        //
        item.DataEmissaoRg = DataEmissaoRG;
        
        //
        item.Sexo = Sexo;
        item.DataNascimento = DataNascimento;
        item.Profissao = Profissao;
        //                                                                              
        ValoresExcelCadastroCotista.Endereco[] endereco = {
                            new ValoresExcelCadastroCotista.Endereco(
                                Endereco,  //Endereco
                                Numero,  //Numero
                                Complemento, //Complemento 
                                Bairro, //Bairro
                                Cidade, //Cidade
                                CEP, //CEP
                                UF, //UF
                                Pais, //Pais
                                "",//DDD
                                Fone, //Fone
                                "", //Ramal
                                Email  //Email
                                ),
                            new ValoresExcelCadastroCotista.Endereco(
                                EnderecoCom, //EnderecoCom
                                NumeroCom, //NumeroCom
                                ComplementoCom, //ComplementoCom
                                BairroCom, //BairroCom
                                CidadeCom, //CidadeCom
                                CEPCom, //CEPCom
                                UFCom, //UFCom
                                PaisCom, //Pais
                                "",//DDD
                                FoneCom, //Fone
                                "", //Ramal
                                EmailCom  //Email
                                )
                        };

        item.EnderecoPessoa = endereco[0];
        item.EnderecoComercialPessoa = endereco[1];
        item.CriadoPor = Authentication.Username;

        
        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();
        basePage.valoresExcelCadastroCotista.Add(item);

        basePage.CarregaCadastroCotista();
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }
    
    [WebMethod]
    [SoapHeader("Authentication")]
    public Cotista Exporta(int IdCotista)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        Cotista cotista = new Cotista();
        if (!cotista.LoadByPrimaryKey(IdCotista))
        {
            cotista = null;
        }
        
        return cotista;
    }

    [WebMethod]
    [SoapHeader("Authentication")]
    public Cotista ExportaPorCpfcnpj(string Cpfcnpj)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        CotistaCollection cotistas = new CotistaCollection().BuscaCotistasPorCPFCNPJ(Cpfcnpj);
        Cotista cotista = null;

        if (cotistas == null || cotistas.Count == 0)
        {
            cotista = null;
        }
        else if (cotistas.Count == 1)
        {
            cotista = cotistas[0];
        }else if(cotistas.Count > 1){
            foreach (Cotista cotistaLoop in cotistas)
            {
                if (cotistaLoop.IsAtivo)
                {
                    cotista = cotistaLoop;
                    break;
                }
            }
        }
        
        return cotista;
    }
}