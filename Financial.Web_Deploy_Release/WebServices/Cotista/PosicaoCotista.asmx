<%@ WebService Language="C#" Class="PosicaoCotistaWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Specialized;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class PosicaoCotistaWS  : System.Web.Services.WebService {

    public ValidateLogin Authentication;
    [SoapHeader("Authentication")]
    [WebMethod]
    public PosicaoCotistaCollection Exporta(int? IdPosicao, int? IdCotista, int? IdCarteira)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
        if (IdPosicao.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdPosicao.Equal(IdPosicao.Value));
        }

        if (IdCotista.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdCotista.Equal(IdCotista.Value));
        }

        if (IdCarteira.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdCarteira.Equal(IdCarteira.Value));
        }
        
        posicoes.Load(posicoes.Query);
        return posicoes;
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }
    
}

