<%@ WebService Language="C#" Class="ContaCorrenteWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Specialized;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Investidor;
using Financial.Security;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ContaCorrenteWS : System.Web.Services.WebService
{

    public ValidateLogin Authentication;

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<ContaCorrenteViewModel> ExportaListaContaCorrente(int? IdPessoa)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        List<ContaCorrenteViewModel> listaContaCorrente = new List<ContaCorrenteViewModel>();

        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
        if (IdPessoa.HasValue)
        {
            contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(IdPessoa.Value));
        }

        contaCorrenteCollection.Load(contaCorrenteCollection.Query);
        foreach (ContaCorrente contaCorrente in contaCorrenteCollection)
        {
            listaContaCorrente.Add(new ContaCorrenteViewModel(contaCorrente));
        }

        return listaContaCorrente;
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<ContaCorrenteViewModel> ExportaListaContaCorrentePorCpfcnpjPessoa(string CpfcnpjPessoa)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        if (string.IsNullOrEmpty(CpfcnpjPessoa))
        {
            throw new Exception("Favor informar um CPF/CNPJ v�lido");
        }
        
        List<ContaCorrenteViewModel> listaContaCorrente = new List<ContaCorrenteViewModel>();
        Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCPFCNPJ(CpfcnpjPessoa);
        if (pessoa != null)
        {
            listaContaCorrente = this.ExportaListaContaCorrente(pessoa.IdPessoa.Value);
        }

        return listaContaCorrente;
    }

    public class ContaCorrenteViewModel
    {
        public int? IdConta;
        public int? IdBanco;
        public int? IdAgencia;
        public int? IdPessoa;
        public string Numero;
        public byte? TipoConta;
        public string ContaDefault;
        public int? IdMoeda;

        public ContaCorrenteViewModel()
        {
        }

        public ContaCorrenteViewModel(Financial.Investidor.ContaCorrente contaCorrente)
        {
            this.ContaDefault = contaCorrente.ContaDefault;
            this.IdAgencia = contaCorrente.IdAgencia;
            this.IdBanco = contaCorrente.IdBanco;
            this.IdConta = contaCorrente.IdConta;
            this.IdMoeda = contaCorrente.IdMoeda;
            this.IdPessoa = contaCorrente.IdPessoa;
            this.Numero = contaCorrente.Numero;

            TipoConta tipoConta = new TipoConta();
            tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
            tipoConta.Query.Load();
            if (!tipoConta.IdTipoConta.HasValue)
            {
                tipoConta = new TipoConta();
                tipoConta.Descricao = "Conta Dep�sito";
                tipoConta.Save();
            }
            contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;
        }
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

}

