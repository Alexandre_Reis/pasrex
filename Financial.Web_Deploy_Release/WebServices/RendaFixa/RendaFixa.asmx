<%@ WebService Language="C#" Class="OperacaoRendaFixaWS" %>

using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Financial.RendaFixa;
using Financial.Integracao.Excel;
using System.Collections.Generic;
using Financial.Security;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using System.Configuration;
using Financial.CRM;
using Financial.ContaCorrente;
using Financial.RendaFixa.Enums;
using Financial.Bolsa;
using Financial.Interfaces.Sinacor;
using Financial.Util;
using Financial.CRM.Enums;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

/// <summary>
/// Summary description for RendaFixa
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class OperacaoRendaFixaWS : System.Web.Services.WebService
{

    [SoapHeader("Authentication")]
    [WebMethod]
    public void ImportaTitulo(int IdTitulo, int IdClasse, int? IdIndice, string Descricao, decimal Taxa,
        decimal Percentual, DateTime DataEmissao, DateTime DataVencimento, decimal PuNominal, bool IsentoIR,
        bool IsentoIOF, string CodigoIsin, string CodigoCetip)
    {

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        ValoresExcelTituloRendaFixa item = new ValoresExcelTituloRendaFixa();

        item.IdTitulo = IdTitulo;
        item.IdClasse = (ClasseRendaFixa)IdClasse;
        item.IdIndice = IdIndice.HasValue ? (EnumIndice)IdIndice : EnumIndice.NONE;
        item.Descricao = Descricao;
        item.Taxa = Taxa;
        item.Percentual = Percentual;
        item.DataEmissao = DataEmissao;
        item.DataVencimento = DataVencimento;
        item.PuNominal = PuNominal;
        item.IsentoIR = IsentoIR ? "S" : "N";
        item.IsentoIOF = IsentoIOF ? "S" : "N";
        item.CodigoIsin = CodigoIsin;
        item.CodigoCetip = CodigoCetip;

        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelTituloRendaFixa = new List<ValoresExcelTituloRendaFixa>();
        basePage.valoresExcelTituloRendaFixa.Add(item);

        basePage.CarregaTituloRendaFixa();

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importa��o T�tulo Renda Fixa",
            HttpContext.Current.User.Identity.Name, Financial.Web.Util.UtilitarioWeb.GetIP(HttpContext.Current.Request), "", Financial.Security.Enums.HistoricoLogOrigem.Outros);

    }


    [SoapHeader("Authentication")]
    [WebMethod]
    public void Importa(int IdCliente, DateTime DataOperacao, string TipoOperacao, decimal Quantidade, decimal PUOperacao, string CodigoIsin,
        string CodigoCetip, byte IdCustodia, byte IdLiquidacao, decimal ValorCorretagem, int NumeroNota, decimal ValorISS, decimal Emolumento)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        ValoresExcelOperacaoRendaFixa operacaoExcel = new ValoresExcelOperacaoRendaFixa();

        operacaoExcel.IdCliente = IdCliente;
        operacaoExcel.DataOperacao = DataOperacao;
        if (TipoOperacao == "C")
        {
            operacaoExcel.TipoOperacao = "CompraFinal";
        }
        else if (TipoOperacao == "V")
        {
            operacaoExcel.TipoOperacao = "VendaFinal";
        }
        else
        {
            string msgErro = "Tipo de Opera��o n�o suportada: " + TipoOperacao;
            throw new Exception(msgErro);
        }

        operacaoExcel.Quantidade = Quantidade;
        operacaoExcel.PuOperacao = PUOperacao;
        operacaoExcel.Valor = Quantidade * PUOperacao;

        //Localizar titulo pelo CodigoIsin
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        if (!String.IsNullOrEmpty(CodigoIsin))
        {
            if (!tituloRendaFixa.BuscaPorCodigoIsin(CodigoIsin))
            {
                string msgErro = "T�tulo com C�digo ISIN:" + CodigoIsin + " n�o encontrado";
                throw new Exception(msgErro);
                return;
            }
        }
        else if (!String.IsNullOrEmpty(CodigoCetip))
        {
            if (!tituloRendaFixa.BuscaPorCodigoCetip(CodigoCetip))
            {
                string msgErro = "T�tulo com C�digo Cetip:" + CodigoCetip + " n�o encontrado";
                throw new Exception(msgErro);
                return;
            }
        }
        else
        {
            string msgErro = "C�digo ISIN ou C�digo Cetip devem ser passados.";
            throw new Exception(msgErro);
        }
        operacaoExcel.IdTitulo = tituloRendaFixa.IdTitulo.Value;

        operacaoExcel.IdCustodia = IdCustodia;
        operacaoExcel.IdLiquidacao = IdLiquidacao;
        operacaoExcel.ValorCorretagem = ValorCorretagem;
        operacaoExcel.NumeroNota = NumeroNota;
        operacaoExcel.ValorISS = ValorISS;
        operacaoExcel.Emolumento = Emolumento;
        operacaoExcel.DataOriginal = DataOperacao;

        int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
        AgenteMercado agenteMercado = new AgenteMercado();
        int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

        operacaoExcel.IdAgenteCorretora = idAgenteMercado;


        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelOperacaoRendaFixa = new List<ValoresExcelOperacaoRendaFixa>();
        basePage.valoresExcelOperacaoRendaFixa.Add(operacaoExcel);

        basePage.CarregaOperacaoRendaFixa(false);


    }


    [SoapHeader("Authentication")]
    [WebMethod]
    public List<OperacaoRendaFixaViewModel> Exporta(DateTime DataExportacao)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");

        operacaoRendaFixaQuery.Select(
                                      papelRendaFixaQuery.IdPapel,
                                      operacaoRendaFixaQuery.DataLiquidacao,
                                      operacaoRendaFixaQuery.DataOperacao,
                                      operacaoRendaFixaQuery.DataVolta,
                                      operacaoRendaFixaQuery.Fonte,
                                      operacaoRendaFixaQuery.IdAgenteCorretora,
                                      operacaoRendaFixaQuery.IdCliente,
                                      operacaoRendaFixaQuery.IdCustodia,
                                        operacaoRendaFixaQuery.IdLiquidacao,
                                        operacaoRendaFixaQuery.IdOperacao,
                                        operacaoRendaFixaQuery.IdOperacaoResgatada,
                                        operacaoRendaFixaQuery.IdPosicaoResgatada,
                                        operacaoRendaFixaQuery.IdTitulo,
                                        operacaoRendaFixaQuery.NumeroNota,
                                        operacaoRendaFixaQuery.PUOperacao,
                                        operacaoRendaFixaQuery.PUVolta,
                                        operacaoRendaFixaQuery.Quantidade,
                                        operacaoRendaFixaQuery.Rendimento,
                                        operacaoRendaFixaQuery.TaxaNegociacao,
                                        operacaoRendaFixaQuery.TaxaOperacao,
                                        operacaoRendaFixaQuery.TaxaVolta,
                                        operacaoRendaFixaQuery.TipoNegociacao,
                                        operacaoRendaFixaQuery.TipoOperacao,
                                        operacaoRendaFixaQuery.Valor,
                                        operacaoRendaFixaQuery.ValorCorretagem,
                                        operacaoRendaFixaQuery.Emolumento,
                                        operacaoRendaFixaQuery.ValorIOF,
                                        operacaoRendaFixaQuery.ValorIR,
                                        operacaoRendaFixaQuery.ValorLiquido,
                                        operacaoRendaFixaQuery.ValorVolta
                                      );

        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);

        operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.Equal(DataExportacao));
        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Ascending,
            operacaoRendaFixaQuery.IdOperacao.Ascending);

        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

        List<OperacaoRendaFixaViewModel> operacoesRendaFixaViewModel = new List<OperacaoRendaFixaViewModel>();
        foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
        {
            OperacaoRendaFixaViewModel operacaoRendaFixaViewModel = new OperacaoRendaFixaViewModel(operacaoRendaFixa);
            operacoesRendaFixaViewModel.Add(operacaoRendaFixaViewModel);

        }

        return operacoesRendaFixaViewModel;
    }



    [SoapHeader("Authentication")]
    [WebMethod]
    public List<OperacaoRendaFixaViewModel> ExportaComFiltro(DateTime? DataInicio, DateTime? DataFim, int? IdTitulo)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        ClienteBolsaQuery clienteBolsaQuery = new ClienteBolsaQuery("C");

        operacaoRendaFixaQuery.Select(
                                        clienteBolsaQuery.CodigoSinacor,
                                      papelRendaFixaQuery.IdPapel,
                                      operacaoRendaFixaQuery.DataLiquidacao,
                                      operacaoRendaFixaQuery.DataOperacao,
                                      operacaoRendaFixaQuery.DataVolta,
                                      operacaoRendaFixaQuery.Fonte,
                                      operacaoRendaFixaQuery.IdAgenteCorretora,
                                      operacaoRendaFixaQuery.IdCliente,
                                      operacaoRendaFixaQuery.IdCustodia,
                                        operacaoRendaFixaQuery.IdLiquidacao,
                                        operacaoRendaFixaQuery.IdOperacao,
                                        operacaoRendaFixaQuery.IdOperacaoResgatada,
                                        operacaoRendaFixaQuery.IdPosicaoResgatada,
                                        operacaoRendaFixaQuery.IdTitulo,
                                        operacaoRendaFixaQuery.NumeroNota,
                                        operacaoRendaFixaQuery.PUOperacao,
                                        operacaoRendaFixaQuery.PUVolta,
                                        operacaoRendaFixaQuery.Quantidade,
                                        operacaoRendaFixaQuery.Rendimento,
                                        operacaoRendaFixaQuery.TaxaNegociacao,
                                        operacaoRendaFixaQuery.TaxaOperacao,
                                        operacaoRendaFixaQuery.TaxaVolta,
                                        operacaoRendaFixaQuery.TipoNegociacao,
                                        operacaoRendaFixaQuery.TipoOperacao,
                                        operacaoRendaFixaQuery.Valor,
                                        operacaoRendaFixaQuery.ValorCorretagem,
                                        operacaoRendaFixaQuery.Emolumento,
                                        operacaoRendaFixaQuery.ValorIOF,
                                        operacaoRendaFixaQuery.ValorIR,
                                        operacaoRendaFixaQuery.ValorLiquido,
                                        operacaoRendaFixaQuery.ValorVolta
                                      );

        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
        operacaoRendaFixaQuery.InnerJoin(clienteBolsaQuery).On(clienteBolsaQuery.IdCliente == operacaoRendaFixaQuery.IdCliente);

        if (DataInicio.HasValue)
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.GreaterThanOrEqual(DataInicio.Value));
        }

        if (DataFim.HasValue)
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.LessThanOrEqual(DataFim.Value));
        }

        if (IdTitulo.HasValue)
        {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdTitulo.Equal(IdTitulo.Value));
        }

        operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.DataOperacao.Ascending,
            operacaoRendaFixaQuery.IdOperacao.Ascending);

        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

        List<OperacaoRendaFixaViewModel> operacoesRendaFixaViewModel = new List<OperacaoRendaFixaViewModel>();
        foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
        {
            OperacaoRendaFixaViewModel operacaoRendaFixaViewModel = new OperacaoRendaFixaViewModel(operacaoRendaFixa);
            operacoesRendaFixaViewModel.Add(operacaoRendaFixaViewModel);

        }

        return operacoesRendaFixaViewModel;
    }

    public ValidateLogin Authentication;

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

    public class OperacaoRendaFixaViewModel
    {

        public int ClassePapel;
        public DateTime DataLiquidacao;
        public DateTime DataOperacao;
        public DateTime? DataVolta;
        public byte Fonte;
        public int? IdAgenteCorretora;
        public int IdCliente;
        public int IdCustodia;
        public int IdLiquidacao;
        public int IdOperacao;
        public int? IdOperacaoResgatada;
        public int? IdPosicaoResgatada;
        public int IdTitulo;
        public int? NumeroNota;
        public decimal PUOperacao;
        public decimal? PUVolta;
        public decimal Quantidade;
        public decimal Rendimento;
        public decimal TaxaNegociacao;
        public decimal? TaxaOperacao;
        public decimal? TaxaVolta;
        public byte TipoNegociacao;
        public byte TipoOperacao;
        public decimal Valor;
        public decimal ValorCorretagem;
        public decimal Emolumento;
        public decimal ValorIOF;
        public decimal ValorIR;
        public decimal ValorLiquido;
        public decimal? ValorVolta;
        public string CodigoSinacor;
        public decimal ValorAliquota;

        public OperacaoRendaFixaViewModel()
        {
        }

        public OperacaoRendaFixaViewModel(OperacaoRendaFixa operacaoRendaFixa)
        {
            this.ClassePapel = Convert.ToInt32(operacaoRendaFixa.GetColumn("IdPapel"));
            this.DataLiquidacao = operacaoRendaFixa.DataLiquidacao.Value;
            this.DataOperacao = operacaoRendaFixa.DataOperacao.Value;
            this.DataVolta = operacaoRendaFixa.DataVolta;
            this.Fonte = operacaoRendaFixa.Fonte.Value;
            this.IdAgenteCorretora = operacaoRendaFixa.IdAgenteCorretora;
            this.IdCliente = operacaoRendaFixa.IdCliente.Value;
            this.IdCustodia = operacaoRendaFixa.IdCustodia.Value;
            this.IdLiquidacao = operacaoRendaFixa.IdLiquidacao.Value;
            this.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
            this.IdOperacaoResgatada = operacaoRendaFixa.IdOperacaoResgatada;
            this.IdPosicaoResgatada = operacaoRendaFixa.IdPosicaoResgatada;
            this.IdTitulo = operacaoRendaFixa.IdTitulo.Value;
            this.NumeroNota = operacaoRendaFixa.NumeroNota;
            this.PUOperacao = operacaoRendaFixa.PUOperacao.Value;
            this.PUVolta = operacaoRendaFixa.PUVolta;
            this.Quantidade = operacaoRendaFixa.Quantidade.Value;
            this.Rendimento = operacaoRendaFixa.Rendimento.Value;
            this.TaxaNegociacao = operacaoRendaFixa.TaxaNegociacao.Value;
            this.TaxaOperacao = operacaoRendaFixa.TaxaOperacao;
            this.TaxaVolta = operacaoRendaFixa.TaxaVolta;
            this.TipoNegociacao = operacaoRendaFixa.TipoNegociacao.Value;
            this.TipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
            this.Valor = operacaoRendaFixa.Valor.Value;
            this.ValorCorretagem = operacaoRendaFixa.ValorCorretagem.Value;
            this.Emolumento = operacaoRendaFixa.Emolumento.Value;
            this.ValorIOF = operacaoRendaFixa.ValorIOF.Value;
            this.ValorIR = operacaoRendaFixa.ValorIR.Value;
            this.ValorLiquido = operacaoRendaFixa.ValorLiquido.Value;
            this.ValorVolta = operacaoRendaFixa.ValorVolta;
            this.CodigoSinacor = Convert.ToString(operacaoRendaFixa.GetColumn("CodigoSinacor"));

            Financial.Tributo.Custom.CalculoTributo calculoTributo = new Financial.Tributo.Custom.CalculoTributo();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(operacaoRendaFixa.IdCliente.Value);

            DateTime dataInicioAliquota = operacaoRendaFixa.DataOperacao.Value;
            DateTime dataFimAliquota = cliente.DataDia.Value;

            if (dataInicioAliquota > dataFimAliquota)
            {
                dataInicioAliquota = dataFimAliquota;
            }
            this.ValorAliquota = calculoTributo.RetornaAliquotaIRLongoPrazo(dataInicioAliquota, dataFimAliquota);
        }
    }
}