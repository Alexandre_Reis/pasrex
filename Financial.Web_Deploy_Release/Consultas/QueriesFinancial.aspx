﻿<%@ page language="C#" autoeventwireup="true" inherits="QueryMainForm, Financial.Web_Deploy" enablesessionstate="True" trace="False" enableeventvalidation="False" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Korzh.EasyQuery.WebControls.CLR20" Namespace="Korzh.EasyQuery.WebControls"
    TagPrefix="keqwc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxmenu" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript" src="../js/jquery.js"></script>

    <script type="text/javascript" language="Javascript" src="../js/jquery.hotkeys.js"></script>

    <script type="text/javascript" language="Javascript">
        var processando = false;
        $.fn.caret = function (begin, end) {
            if (this.length == 0) return;
            if (typeof begin == 'number') {
                end = (typeof end == 'number') ? end : begin;
                return this.each(function () {
                    if (this.setSelectionRange) {
                        this.setSelectionRange(begin, end);
                    } else if (this.createTextRange) {
                        var range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', begin);
                        try { range.select(); } catch (ex) { }
                    }
                });
            } else {
                if (this[0].setSelectionRange) {
                    begin = this[0].selectionStart;
                    end = this[0].selectionEnd;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    begin = 0 - range.duplicate().moveStart('character', -100000);
                    end = begin + range.text.length;
                }
                return { begin: begin, end: end };
            }
        }


        jQuery(document).ready(
            function () {
                $(document).bind('keydown', 'Ctrl+return', function (evt) { $('#ManualQueries').click(); return false; });
                $("#SqlTextBox").bind('keydown', 'Ctrl+return', function (evt) { mostraResultado(getSql()); return false; });
            })
        // Value contains the "EmployeeID" field value returned from the server, not the list of values 

        function reverse(s) {
            return s.split("").reverse().join("");
        }

        function getSql() {
            var caret = $("#SqlTextBox").caret();
            var textoInteiro = $("#SqlTextBox").val();
            if (caret.begin != caret.end)
                return textoInteiro.substring(caret.begin, caret.end).trim();


            var queryInicio = reverse(textoInteiro.substring(0, caret.begin));
            queryInicio = reverse(queryInicio.substring(0, (queryInicio.indexOf(';') != -1 ? queryInicio.indexOf(';') : queryInicio.length)));

            var queryFim = textoInteiro.substring(caret.begin, textoInteiro.length);
            queryFim = queryFim.substring(0, (queryFim.indexOf(';') != -1 ? queryFim.indexOf(';') : queryFim.length))

            return (queryInicio + queryFim).trim();

        }

        function testea(Value) {
            $("#SelectField").val(query);
        }

        function gridQuery_CustomButtonClick(s, e) {
            gridQuery.GetRowValues(e.visibleIndex, 'QueryCustom', mostraResultado);
        }



        function mostraResultadoBotao() {
            if (processando == false) {
                var query = $("#SqlTextBox").val();
                mostraResultado(query);
            }
        }



        function mostraResultado(query) {
            if (processando == false) {
                $("#SelectField").val(query);
                gridResultado.PerformCallback();
            }
        }

        var queryAtual = "";
        function textQueryInit() {
            if (queryAtual != "") {
                textQuery.SetValue(queryAtual);
                queryAtual = "";
            }

        }




    </script>

    <title>Consultas Financial</title>
    <link href="../css/queryBuilder.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridQuery.UpdateEdit();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <table style="width: 100%; border: 0" cellpadding="0" cellspacing="0" id="table18">
            <tr>
                <td style="background-color: #4B9EDC" colspan="2">
                    <table style="border: 0; width: 100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="title1">
                                <asp:Literal ID="txtQueryDef" runat="server" Text="Consulta" />
                            </td>
                            <td style="background-color: #FFFFFF; text-align: right; vertical-align: top">
                                <asp:UpdateProgress ID="UpdateProgressColumns" runat="server" AssociatedUpdatePanelID="UpdatePanelColumns">
                                    <ProgressTemplate>
                                        <span style="font-weight: bold; font-size: 14px">
                                            <asp:Label ID="labelProcessando" runat="server" Text="Processando... " /></span>
                                        <img src="../imagens/progressBar2.gif" alt="Progress Bar" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdateProgress ID="UpdateProgressConditions" runat="server" AssociatedUpdatePanelID="UpdatePanelConditions">
                                    <ProgressTemplate>
                                        <span style="font-weight: bold; font-size: 14px">
                                            <asp:Label ID="labelProcessando1" runat="server" Text="Processando... " /></span>
                                        <img src="../imagens/progressBar2.gif" alt="Progress Bar" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 180px; height: 360px;" class="leftCol">
                    <asp:UpdatePanel ID="UpdatePanelEntities" runat="server">
                        <ContentTemplate>
                            <div class="title2">
                                <asp:Literal ID="Literal2" runat="server" Text="Tabelas" />
                            </div>
                            <keqwc:EntitiesPanel ID="EntitiesPanel1" runat="server" Width="100%" Height="360px"
                                BorderStyle="Solid" CssClass="bodytext" ToolTip="Tabelas e seus Campos">
                            </keqwc:EntitiesPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="height: 360px;" class="back_blue rightCol">
                    <table style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanelColumns" runat="server">
                                        <ContentTemplate>
                                            <div class="title2">
                                                <asp:Literal ID="txtResultColumns" runat="server" Text="Colunas Selecionadas" />
                                            </div>
                                            <keqwc:QueryColumnsPanel ID="QueryColumnsPanel1" runat="server" Height="150px" Width="100%"
                                                BorderStyle="Solid" ShowHeaders="True" CssClass="bodytext" ToolTip="Colunas Selecionadas"
                                                Appearance-RowButtonTooltip="Clique aqui para ver as operações disponíveis" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div class="title2">
                                        <asp:Literal ID="txtColumnSoring" runat="server" Text="Ordenação das Colunas" />
                                    </div>
                                    <keqwc:SortColumnsPanel ID="SortColumnsPanel1" runat="server" Height="150px" ToolTip="Ordenação das Colunas"
                                        Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 0; width: 100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td class="title2">
                                                <asp:Literal ID="txtQueryConditions" runat="server" Text="Condições da Consulta" />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <asp:UpdatePanel ID="UpdatePanelConditions" runat="server">
                                        <ContentTemplate>
                                            <keqwc:QueryPanel ID="QueryPanel1" runat="server" Height="160px" Width="100%" BorderWidth="1px"
                                                ScrollBars="Auto" OnSqlExecute="QueryPanel1_SqlExecute" CssClass="bodytext" OnListRequest="QueryPanel1_ListRequest"
                                                OnCreateValueElement="QueryPanel1_CreateValueElement" Appearance-ScriptMenuStyle-ItemMinWidth="160"
                                                Appearance-ScriptMenuStyle-BackColor="#FFE0C0" UseListCache="True" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <table style="border: 0; width: 100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td class="title2">
                                                <asp:Literal ID="Literal6" runat="server" Text="Operações" />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table id="table12" cellspacing="0" cellpadding="5" style="border: 1px solid; width: 100%">
                                        <tr>
                                            <td style="height: 34px"></td>
                                            <td style="height: 34px">
                                                <asp:Button ID="btnClear" runat="server" Height="24px" OnClick="ClearBtn_Click" Text="Limpar"
                                                    Width="80px" CssClass="btn" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="bottom" style="height: 17px">
                                                <div style="text-align: left">
                                                    <asp:Literal ID="txtQueryFile" runat="server" Text="Carregar Arquivo p/ Consulta" />
                                                </div>
                                                <asp:FileUpload ID="SavedQueryUpload" runat="server" Height="20px" />
                                            </td>
                                            <td valign="bottom" style="height: 17px">
                                                <asp:Button ID="btnLoad" runat="server" Height="24px" OnClick="LoadBtn_Click" Text="Carregar"
                                                    Width="80px" CssClass="btn" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnSave" runat="server" Height="24px" OnClick="SaveBtn_Click" Text="Salvar..."
                                                    Width="80px" CssClass="btn" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="Button1" runat="server" Height="24px" OnClientClick="popupFiltro.ShowWindow();return false;"
                                                    Text="teste..." Width="80px" CssClass="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="ErrorLabel" runat="server" Text="______" Font-Bold="True" ForeColor="Red"
            Visible="False" />
        <div style="height: 10px">
        </div>
        <table style="width: 100%; border: 0" cellpadding="0" cellspacing="5" id="table17">
            <tr>
                <td style="width: 50%">

                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1">
                        <tr>
                            <td style="background-color: #FFAD2B;">
                                <div class="title1">
                                    SQL
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF; border: solid 4px #FFAD2B; height: 102px;">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="SqlTextBox" runat="server" Width="98%" Height="295px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table8">
                        <tr>
                            <td style="background-color: #9A90BC">
                                <div class="title1">
                                    Queries Salvas
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #F8F7FC; border: solid 4px #9A90BC; height: 109px; background-image: url(../imagens/back_form.gif);">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridQuery.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridQuery.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal5" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridQuery.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div><dxwgv:ASPxGridView ID="gridQuery" runat="server" KeyFieldName="IdQuery" AutoGenerateColumns="False"
                                    OnHtmlRowPrepared="gridQuery_HtmlRowPrepared" DataSourceID="EsDSQuery" OnRowUpdating="gridQuery_RowUpdating"
                                    OnCustomCallback="gridQuery_CustomCallback" OnRowInserting="gridQuery_RowInserting"
                                    ClientSideEvents-CustomButtonClick="function(s,e){ gridQuery_CustomButtonClick(s,e) }"
                                    ClientSideEvents-RowDblClick="function(s, e) {editFormOpen=true; gridQuery.StartEditRow(e.visibleIndex);}">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn Width="4%" VisibleIndex="0" ButtonType="image">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas"
                                                    BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridQuery);}"
                                                    OnInit="CheckBoxSelectAll" />
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewCommandColumn Width="4%" VisibleIndex="1" ButtonType="Image">
                                            <CustomButtons>
                                                <dxwgv:GridViewCommandColumnCustomButton ID="btnRun" Image-Url="../imagens/ico_form_popup.gif"
                                                    Text="Executar">
                                                </dxwgv:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Nome" Width="14%" VisibleIndex="2">
                                            <PropertiesTextEdit MaxLength="60">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" Width="14%" VisibleIndex="2">
                                            <PropertiesTextEdit MaxLength="60">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="QueryCustom" Width="50 %" VisibleIndex="2">
                                            <PropertiesTextEdit MaxLength="60">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label><div class="editForm">
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelIdQuery" runat="server" CssClass="labelRequired" Text="Id Query:"></asp:Label></td><td>
                                                            <dxe:ASPxTextBox ID="textIdQuery" runat="server" CssClass="textNome" Enabled="false"
                                                                Text='<%#Eval("IdQuery")%>'>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelNome" runat="server" CssClass="labelRequired" Text="Nome:"></asp:Label></td><td>
                                                            <dxe:ASPxTextBox ID="textNome" runat="server" CssClass="textNome" Text='<%#Eval("Nome")%>'>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"></asp:Label></td><td>
                                                            <dxe:ASPxTextBox ID="textDescricao" runat="server" CssClass="textNome" TextMode="MultiLine"
                                                                Text='<%#Eval("Descricao")%>'>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelQuery" runat="server" CssClass="labelRequired" Text="Query:"></asp:Label></td><td>
                                                            <dxe:ASPxTextBox ClientSideEvents-Init="textQueryInit" ClientInstanceName="textQuery"
                                                                ID="textQuery" runat="server" CssClass="textNome" Text='<%#Eval("QueryCustom")%>'>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                        OnClientClick="gridQuery.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton></div></div></EditForm></Templates><Settings ShowFilterRow="True" />
                                </dxwgv:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="ConsultarBtn" Text="Consultar" Style="width: 112px; height: 20px;"
                        OnClientClick="mostraResultadoBotao(); return false;" />
                    <asp:Button runat="server" ID="SalvarBtn" Text="Salvar" Style="width: 112px; height: 20px;"
                        OnClientClick="gridQuery.AddNewRow(); queryAtual = $('#tabCadastro_SqlTextBox').val(); return false;" />
                </td>
            </tr>
        </table>
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table29">
                        <tr>
                            <td style="width: 506px; height: 22px">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="background-color: #00c62c; width: 160px; height: 22px;">
                                            <div class="title1">
                                                <asp:Literal ID="txtResult" runat="server" Text="Registros" />
                                            </div>
                                        </td>
                                        <td style="height: 22px">&nbsp;&nbsp; <asp:Button ID="btnExportExcel" runat="server" OnClick="ExportExcelBtn_Click" Text="Exportação Excel"
                                                Width="112px" Height="20px" CssClass="btn" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFF; border: solid 4px #00c62c; width: 506px;" class="back_blue">
                                <dxwgv:ASPxGridView ID="gridResultado" ClientInstanceName="gridResultado" runat="server" AutoGenerateColumns="True"
                                    OnCustomCallback="gridResultado_CustomCallback" OnBeforePerformDataSelect="gridResultado_OnBeforePerformDataSelect">
                                    <Settings ShowFilterRow="True" />
                                    <ClientSideEvents BeginCallback="function(s, e){ processando = true; }"                                         
                                        EndCallback="function(s, e){ 
                                        if(gridResultado.cpMessage != '' && gridResultado.cpMessage != null)
                                        {
                                            alert(gridResultado.cpMessage);
                                            gridResultado.cpMessage = '';
                                            mostraResultado('');
                                        }
                                        
                                        processando = false; }" />
                                </dxwgv:ASPxGridView>
                                <asp:HiddenField ID="SelectField" runat="server" />
                            </td>
                    </table>
                    </td> </tr> </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <asp:Literal ID="Literal4" runat="server"></asp:Literal></dxpc:PopupControlContentControl></ContentCollection></dxpc:ASPxPopupControl><div>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridResultado"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSQuery" runat="server" OnesSelect="EsDSQuery_esSelect" LowLevelBind="true" />
    </form>
</body>
</html>
