﻿<%@ page language="C#" autoeventwireup="true" inherits="Consultas_PrejuizoCotistaPorFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script language="JavaScript">
    function OnGetDataCarteira(values) {
        btnEditCodigoCarteira.SetValue(values);                    
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());        
        btnEditCodigoCarteira.Focus();    
    }
    function OnGetDataCotista(values) {
        btnEditCodigoCotista.SetValue(values);                    
        popupCotista.HideWindow();
        ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
        btnEditCodigoCotista.Focus();    
    }   
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {   
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, document.getElementById('textNome'));
            }        
            "/>
        </dxcb:ASPxCallback>        
        
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, 
                                      btnEditCodigoCotista, document.getElementById('textNomeCotista'));
        }        
        "/>
    </dxcb:ASPxCallback>
        
        <div class="divPanel">
        <table width="100%"><tr><td>
        <div id="container_small">
        
        <div id="header">
            <asp:Label ID="lblHeader" runat="server" Text="Prejuízos de Cotistas (Por Fundo)"></asp:Label>
        </div>
            
        <div id="mainContent">    
                        
            <div class="reportFilter">
                
                <div style="height:20px"></div>                                
                                            
                <table cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                        </td>
                    
                        <td>
                            <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteira"
                                                MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                            <Buttons>
                                <dxe:EditButton></dxe:EditButton>                                
                            </Buttons>        
                            <ClientSideEvents
                                     KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                                     ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                            />                            
                            </dxe:ASPxSpinEdit>   
                        </td>                          
                    
                        <td>
                            <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                        
                        </td>
                    </tr>
                    
                    <tr>
                    <td class="td_Label">
                        <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                    </td>
                    
                    <td>
                        <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCotista">  
                        <Buttons>
                            <dxe:EditButton></dxe:EditButton>                                
                        </Buttons>        
                        <ClientSideEvents
                                 KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                                 ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                        />                            
                        </dxe:ASPxSpinEdit>                
                    </td>                          
                    
                    <td>
                        <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
                    </td>
                    </tr>
                                                    
                    <tr>
                    <td class="td_Label">                    
                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" ></asp:Label>
                    </td>       
                                     
                    <td>
                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />
                    </td>
                </tr>               
                </table>
            </div>

            <div id="progressBar" class="progressBar" runat="server">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>            
                    <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller"/>
                    <asp:Label ID="lblCarregar" runat="server" Text="Carregando..."/>
                </ProgressTemplate>
            </asp:UpdateProgress>
            </div>
                                                           
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">            
            <ContentTemplate>
                
                <div class="linkButton linkButtonNoBorder" >
                   <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnRun_Click"><asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
                </div>                   
                            
                <div class="divDataGrid">    
         
                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" ClientInstanceName="gridConsulta" EnableCallBacks="true"    
                        DataSourceID="EsDSPrejuizoCotista" OnHtmlDataCellPrepared="gridConsulta_HtmlDataCellPrepared">
                        <Columns>                                                     
                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" Caption="Id Fundo" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left"/>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ApelidoCarteira" Caption="Nome Fundo" VisibleIndex="3" Width="25%"/>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="IdCotista" Caption="Id Cotista" VisibleIndex="5" Width="8%" CellStyle-HorizontalAlign="left"/>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ApelidoCotista" Caption="Nome Cotista" VisibleIndex="6" Width="24%"/>
                            
                            <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="8" Width="10%"/>    
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ValorPrejuizo" VisibleIndex="10" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>                          
                            
                            <dxwgv:GridViewDataDateColumn FieldName="DataLimiteCompensacao" Caption="Data Limite" VisibleIndex="12" Width="10%"/>    
                        </Columns>
                        
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                                FieldName="ValorPrejuizo" ShowInColumn="Prejuízo" SummaryType="Sum" />
                        </TotalSummary>                    
                    
                        <SettingsPager PageSize="15"/>
                        
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" 
                            VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                        
                        <SettingsText EmptyDataRow="0 registros"/>
                    </dxwgv:ASPxGridView>
                
                </div>
                        
            </ContentTemplate>
            </asp:UpdatePanel>
    
        </div>
        </div>
        </td></tr></table>
        </div>   
        
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" Landscape="true" />
        
        <cc1:esDataSource ID="EsDSPrejuizoCotista" runat="server" OnesSelect="EsDSPrejuizoCotista_esSelect" />        
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />                
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        
    </form>

<script type="text/javascript">
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        updateProgress1.style.display = '';
    }
}

function EndRequestHandler(sender, args) {
    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : '';     
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
</script>    
    
</body>
</html>