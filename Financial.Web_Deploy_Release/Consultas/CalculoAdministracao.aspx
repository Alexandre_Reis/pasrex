﻿<%@ page language="C#" autoeventwireup="true" inherits="Consultas_CalculoAdministracao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script language="JavaScript">
    //document.onkeydown=onBreakEnter;
    
    function OnGetData(values) {
        var resultSplit = values.split('|');
        document.getElementById('hiddenIdCarteira').value = resultSplit[1];
        document.getElementById('hiddenIdTabela').value = resultSplit[0];
        popupTabela.HideWindow();
        ASPxCallback1.SendCallback(resultSplit[0]);
        btnEditCodigo.Focus();
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result == '' && document.getElementById('hiddenIdTabela').value != '' 
                                            && document.getElementById('hiddenIdTabela').value != null)
            {                   
                if (document.getElementById('hiddenIdTabela').value != '' 
                                && document.getElementById('hiddenIdTabela').value != null)
                {
                    popupMensagem.ShowWindow();
                }                
                btnEditCodigo.SetValue(''); 
                btnEditCodigo.Focus();                 
            }
            else
            {
                btnEditCodigo.SetValue(e.result);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:HiddenField ID="hiddenIdCarteira" runat="server"></asp:HiddenField> 
    
    <dxpc:ASPxPopupControl ID="popupMensagem" PopupElementID="btnEditCodigo" CloseAction="MouseOut" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="Tabela não Existe." runat="server">
    </dxpc:ASPxPopupControl>
          
    <dxpc:ASPxPopupControl ID="popupTabela" runat="server" HeaderText="" Width="500px" 
                        ContentStyle-VerticalAlign="Top" EnableClientSideAPI="True"
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabela" runat="server" Width="100%"
                    ClientInstanceName="gridTabela" AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaTaxas" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabela_CustomDataCallback"                     
                    OnHtmlRowCreated="gridTabela_HtmlRowCreated">                    
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" Visible="false"/>                
                <dxwgv:GridViewDataTextColumn FieldName="IdCadastro" Visible="false"/>                                        
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" VisibleIndex="0" Width="10%"/>
                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" VisibleIndex="1" Width="10%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="40"/>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="40"/>
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridTabela.GetValuesOnCustomCallback(e.visibleIndex, OnGetData);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
            <SettingsDetail ShowDetailButtons="False" />
            <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images></Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Tabela" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabela.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
                        
    <div class="divPanel">
        <table width="100%"><tr><td>
        <div id="container_small">
        
        <div id="header">
            <asp:Label ID="lblHeader" runat="server" Text="Consulta Histórico de Taxa Adm/Gestão"></asp:Label>
        </div>
            
        <div id="mainContent">    
            
            <div class="reportFilter">
            
            <div style="height:20px"></div>                             
                                            
            <table>
                <tr>
                    <td class="td_Label">
                        <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Tabela:"></asp:Label>
                    </td>
                    
                    <td colspan="3">
                        <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" Visible="false"></asp:Label>
                        <asp:Label ID="labelTaxa" runat="server" Text="Tipo: " Visible="false"></asp:Label>
                        <asp:Label ID="labelDataReferencia" runat="server" Text="Data Referência: " Visible="false"></asp:Label>
                        
                        <dxe:ASPxButtonEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditCodigo" 
                                            ReadOnly="true" Width="600px">
                        <Buttons>
                            <dxe:EditButton>
                            </dxe:EditButton>                                
                        </Buttons>        
                        <ClientSideEvents
                                 KeyPress="function(s, e) {document.getElementById('hiddenIdTabela').value = '';}" 
                                 ButtonClick="function(s, e) {popupTabela.ShowAtElementByID(s.name);}"                          
                        />                            
                        </dxe:ASPxButtonEdit>
                        
                        <asp:TextBox ID="hiddenIdTabela" runat="server" CssClass="hiddenField" />             
                    </td>
                </tr>
            </table>
            
            <table cellpadding="2" cellspacing="2">
                <tr>
                    <td class="td_Label">                    
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
                    </td>                        
                    <td>
                       <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                     
                    </td>  
                    
                    <td>
                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label>                        
                    </td>                                                                            
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                    </td>                    
                </tr>               
            </table>
            </div>
                        
            <div id="progressBar" class="progressBar" runat="server">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
            
            <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller" />
            <asp:Label ID="lblCarregar" runat="server" Text="Carregando..."></asp:Label>                        
            
            </ProgressTemplate>
            </asp:UpdateProgress>
            </div>
                                                                        
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                
                <div class="linkButton linkButtonNoBorder" >
                   <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnRun_Click"><asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
                </div>
                            
                <div class="divDataGrid">                                   
                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" ClientInstanceName="gridConsulta" 
                        EnableCallBacks="true" DataSourceID="EsDSCalculoAdministracao">
                        
                        <Columns>                                                     
                            <dxwgv:GridViewDataDateColumn FieldName="DataHistorico" Caption="Data Histórico"  VisibleIndex="1" Width="10%"/>                            
                            <dxwgv:GridViewDataDateColumn FieldName="DataFimApropriacao" Caption="Fim Apropriação" VisibleIndex="2" Width="10%"/>                            
                            <dxwgv:GridViewDataDateColumn FieldName="DataPagamento" Caption="Pagamento"  VisibleIndex="3" Width="10%"/>                                 
                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="4" Width="30%"/>
                                                   
                            <dxwgv:GridViewDataTextColumn FieldName="ValorDia" VisibleIndex="4" Width="20%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                        
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>                                              
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ValorAcumulado" VisibleIndex="5" Width="20%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>                            
                        </Columns>                    
                    
                        <SettingsPager PageSize="15"/>
                        
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" 
                            VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual"  />
                        
                        <SettingsText EmptyDataRow="0 registros"/>
                        
                    </dxwgv:ASPxGridView>                
                </div>
                        
            </ContentTemplate>
            </asp:UpdatePanel>
    
        </div>        
        </td></tr>
        </table>
    </div>   
        
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" />
        
    <cc1:esDataSource ID="EsDSCalculoAdministracao" runat="server" OnesSelect="EsDSCalculoAdministracao_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaTaxas" runat="server" OnesSelect="EsDSTabelaTaxas_esSelect" />
        
    </form>
    
<script type="text/javascript">  
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        updateProgress1.style.display = '';
    }
}

function EndRequestHandler(sender, args) {
    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : '';     
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
</script>
    
</body>
</html>