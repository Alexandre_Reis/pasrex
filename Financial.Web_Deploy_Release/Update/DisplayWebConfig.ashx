<%@ WebHandler Language="C#" Class="DisplayWebConfig" %>

using System;
using System.Web;
using System.Xml;

public class DisplayWebConfig : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        XmlTextReader reader = new XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("..\\web.config"));
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(reader);

        
        reader.Close();
    
        context.Response.ContentType = "text/xml";
        context.Response.Write(xmlDoc.OuterXml);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}