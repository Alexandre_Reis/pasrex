﻿<%@ page language="C#" enableviewstate="false" autoeventwireup="true" inherits="Default, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxm" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial</title>
    <link rel="stylesheet" type="text/css" href="css/principal.css" />
    <link rel="stylesheet" type="text/css" href="css/carregador.css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" language="Javascript" src="js/global.js"></script>

    <script type="text/javascript" language="javascript">
    
    function turnVisible(strDiv){
        var objDiv = document.getElementById(strDiv);
        objDiv.style.display         ="block";
        objDiv.style.visibility      ="visible";
    }
    function turnInvisible(strDiv){
        var objDiv = document.getElementById(strDiv);
        objDiv.style.display         ="none";
        objDiv.style.visibility      ="hidden";
    }

    function loadPage()
    {        
        turnVisible("topo");
        //turnVisible("content");
        turnInvisible("carregador_pai");
        var objContent = document.getElementById("content");
        objContent.style.top="51px";
        document.getElementById('iframePrincipal').src = document.getElementById('hiddenPage').value;
    }
      
    function OpenNewWindow(url) {
       window.open(url,'_blank',"width=1000,height=500,status=no,toolbar=no,menubar=no");
    }
    
    function iterateThroughMenu() 
    {
	    if (document.getElementById('chkJanela').checked)
	    {
		    target = '_blank';
	    }
	    else
	    {
		    target='iframePrincipal';
	    }
	    var anchorArray = document.getElementsByTagName('a');

 	    for (var i=0; i<anchorArray.length; i++)
	    {	
		    anchorArray[i].target = target;	
	    }  	    
    }
    </script>

</head>
<body onload="loadPage();changeDisplay();" onresize="changeDisplay();">
    <form id="global" runat="server">
        <div id="carregador_pai">
            <div id="carregador">
                <div align="center">
                    Aguarde, carregando...</div>
                <div id="carregador_fundo">
                    <div align="center">
                        <asp:Image ID="imagemLoading" runat="server" ImageUrl="~/imagens/loading.gif" />
                    </div>
                </div>
            </div>
        </div>
        <dxcb:ASPxCallback ID="callback1" runat="server" OnCallback="callback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            var resultSplit = e.result.split('|');                        
            e.result = resultSplit[0];
            pagina = resultSplit[1];
            
            if (pagina != null)
            {
                document.getElementById('homePage').href = pagina;
            }
            alert(e.result);        
        }        
        " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupSobre" AllowDragging="true" PopupElementID="popupSobre"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            CloseAction="CloseButton" Width="220" Left="1600" Top="70" HeaderText="Sobre"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <table>
                        <tr>
                            <td class="td_Label_Longo" style="float: right;">
                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Versão:"></asp:Label>
                            </td>
                            <td>
                                <div class="linkButton linkButtonNoBorder">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ForeColor="Blue"
                                        CssClass="btnOK" OnClientClick="popupReleaseNotes.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="1.0.0.3" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label_Longo" style="float: right;">
                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Versão WEB:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblVersaoWeb" runat="server" CssClass="labelNormal"  Text=""
                                    OnPreRender="lblVersaoWeb_OnPreRender" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label_Longo" style="float: right;">
                                <asp:Label ID="labelVersao" runat="server" CssClass="labelNormal" Text="Versão Schema:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblVersao" runat="server" CssClass="labelNormal labelFloat" Text=""
                                    OnPreRender="lblVersao_OnPreRender" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label_Longo" style="float: right;">
                                <asp:Label ID="lblServidor" runat="server" CssClass="labelNormal" Text="Servidor:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblServidorNome" runat="server" CssClass="labelNormal labelFloat" OnLoad="lblServidorNome_Load" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label_Longo" style="float: right;">
                                <asp:Label ID="lblBanco" runat="server" CssClass="labelNormal" Text="Banco:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblBancoNome" runat="server" CssClass="labelNormal labelFloat" OnLoad="lblBancoNome_Load" /><br />
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        
        <dxpc:ASPxPopupControl ID="popupReleaseNotes" AllowDragging="true" PopupElementID="popupReleaseNotes"
            EnableClientSideAPI="True" CloseAction="CloseButton" Width="500px" Height="500px" HeaderText="Release Notes" ScrollBars="Auto"
            PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" 
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
        </dxpc:ASPxPopupControl>
        
        <asp:HiddenField ID="hiddenPage" runat="server" />
        <div id="topo" style="visibility: hidden; display: none">
            <div runat="server" class="logoCliente" id="divLogotipo" onprerender="divLogotipoOnPreRender">
            </div>
            <div id="topoBotoes">
                <div class="opcaoJanela group">
                    <asp:CheckBox ID="chkJanela" runat="server" AutoPostBack="false" Text="Nova Aba"
                        Enabled="true" />
                </div>
                <div id="home-wrapper" class="group">
                    <div id="home" class="first">
                        <a id="homePage" name="home" runat="server" target="iframePrincipal">
                            <img src="imagens/home.png" alt="Home" />Home</a>
                    </div>
                    <div id="newhome">
                        <a href="javascript:callback1.SendCallback();">
                            <img src="imagens/star.png" alt="New_Home" />Atualiza Home</a>
                    </div>
                </div>
                <div class="group">
                    <div id="usuario" class="first">
                        <a href="javascript:login_edit_submit();">
                            <img src="imagens/card-address.png" alt="" />
                            <asp:LoginName ID="loginName" runat="server" />
                        </a>
                    </div>
                    <div class="group">
                        <div class="linkButton linkButtonNoBorder">
                            <img src="imagens/help.png" alt="" />
                            <asp:LinkButton ID="btnOKSobre" runat="server" Font-Overline="false" ForeColor="Black"
                                CssClass="btnOK" OnClientClick="popupSobre.ShowWindow(); return false;">
                                <asp:Literal ID="Literal7" runat="server" Text="Sobre" /><div>
                                </div>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div id="sair">
                        <img src="imagens/lock--minus.png" alt="" /><a href="Login/LogOff.aspx">Sair</a>
                    </div>
                </div>
            </div>
            <img style="margin-top: 7px; margin-right: 20px;" src="imagens/logo_britech.png" alt=""/>
            <!--<asp:TextBox style="display:none" ID="textMsgCritica" runat="server"
	                 Text="Para continuar com sua busca, favor informar 4 ou mais caracteres."></asp:TextBox>
        <div id="search">                  
          <asp:TextBox style="display:none" ID="textTextoSearch" runat="server" Text="Pesquisar..."></asp:TextBox>     
          <asp:TextBox ID="textSearch" runat="server" onfocus="search_focus(this);" onblur="search_blur(this);" ></asp:TextBox>
          <img src="imagens/ico_search.gif";   />          
        </div>-->
        </div>
        <div id="content">
            <iframe runat="server" frameborder="0" id="iframePrincipal" name="iframePrincipal"
                scrolling="auto" width="100%" height="100%"></iframe>
        </div>
        <dxm:ASPxMenu ID="ASPxMenu1" AutoPostBack="false" ClientInstanceName="ASPxMenu1"
            EnableClientSideAPI="true" AppearAfter="0" EnableAnimation="false" SubMenuStyle-GutterWidth="0px"
            ShowSubMenuShadow="false" CssClass="menu" runat="server" OnDataBound="NavigationMenu_DataBound"
            DataSourceID="SiteMapDataSource2" Orientation="Vertical" Target="iframePrincipal">
            <ItemStyle CssClass="menu">
                <HoverStyle BackColor="#E6E6E6">
                </HoverStyle>
            </ItemStyle>
            <SubMenuItemStyle CssClass="submenu" Paddings-PaddingLeft="12px" />
            <Border BorderColor="Transparent" BorderStyle="None" />
        </dxm:ASPxMenu>
        <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" ShowStartingNode="false" />
        
    </form>
</body>
</html>
