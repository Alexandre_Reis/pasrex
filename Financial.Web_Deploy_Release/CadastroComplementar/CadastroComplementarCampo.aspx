﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastroComplementar_CadastroComplementarCampo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro Complementar Campos"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false; ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback('delete');} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdCamposComplementares"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" DataSourceID="EsDSCampoComplementares"
                                        OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                        OnPreRender="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCadastro" Caption="Cadastro" VisibleIndex="1"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="NomeCampo" Caption="Nome do Campo" VisibleIndex="2"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCampo" Caption="Descrição" VisibleIndex="3"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCampo" Caption="Tipo de Campo" VisibleIndex="4"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100"/>
                                            <dxwgv:GridViewDataColumn FieldName="Tamanho" Caption="Tamanho" VisibleIndex="5"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCadastro" runat="server" CssClass="labelRequired" Text="Cadastro:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoCadastro" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        OnLoad="dropTipoCadastro_OnLoad" CssClass="dropDownListLongo" Text='<%#Eval("TipoCadastro")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelNomeCampo" runat="server" CssClass="labelRequired" Text="Nome Campo:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxTextBox ID="textNomeCampo" runat="server" ClientInstanceName="textNomeCampo"
                                                                        Value='<%#Eval("NomeCampo")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDescricaoCampo" runat="server" Text="Descrição Campo:" />
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxTextBox ID="textDescricaoCampo" runat="server" ClientInstanceName="textDescricaoCampo" Width="100%"
                                                                        Value='<%#Eval("DescricaoCampo")%>' MaxLength="150" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoCampo" runat="server" CssClass="labelRequired" Text="Tipo Campo:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoCampo" ClientInstanceName="dropTipoCampo" runat="server"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" OnLoad="dropTipoCampo_OnLoad"
                                                                        CssClass="dropDownListLongo" Text='<%#Eval("TipoCampo")%>'>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbPanel.PerformCallback();}" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValorDefault" runat="server" Text="Valor Default:" />
                                                                </td>
                                                                <td>
                                                                    <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbPanel" ID="cbPanel"
                                                                        OnCallback="cbPanel_Callback">
                                                                        <PanelCollection>
                                                                            <dxp:PanelContent runat="server">
                                                                                <dxe:ASPxTextBox ID="textValorDefault" runat="server" ClientInstanceName="textValorDefault"
                                                                                    Value='<%#Eval("ValorDefault")%>' />
                                                                            </dxp:PanelContent>
                                                                        </PanelCollection>
                                                                    </dxcp:ASPxCallbackPanel>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCampoObrigatorio" runat="server" CssClass="labelRequired" Text="Obrigatório(S/N):" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropCampoObrigatorio" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("CampoObrigatorio")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTamanho" runat="server" Text="Tamanho:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textTamanho" runat="server" ClientInstanceName="textTamanho"
                                                                        Value='<%#Eval("Tamanho")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCasasDecimais" runat="server" Text="Casas Decimais:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textCasasDecimais" runat="server" ClientInstanceName="textCasasDecimais"
                                                                        Value='<%#Eval("CasasDecimais")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar';callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCampoComplementares" runat="server" OnesSelect="EsDSCamposComplementares_esSelect" />
    </form>
</body>
</html>
