﻿<%@ page language="C#" autoeventwireup="true" inherits="LembrarSenha, Financial.Web_Deploy" title="Login" meta:resourcekey="LembrarSenha" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<html>
<head id="Head1" runat="server">
    <link href="~/css/login.css?v2" rel="stylesheet" type="text/css" />
    <link href="~/cssCustom/login.css" rel="stylesheet" type="text/css" />
    <link href="~/css/keyboard.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" language="Javascript" src="../js/ext-core.js"></script>

    <script type="text/javascript" language="Javascript" src="../js/login/login.js"></script>

    <script type="text/javascript" language="Javascript" src="../jsCustom/login/login.js"></script>

</head>
<body onresize="window.location.reload();">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true">
        </asp:ScriptManager>
        <dxcb:ASPxCallback ID="callbackSenha" runat="server" OnCallback="callbackSenha_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            splitResult = e.result.split('|');            
            if (!splitResult[1])
            {                   
                alert(e.result);                              
            }
            else
            {
                alert('Senha enviada com sucesso para o email ' + splitResult[1] + '.');                          
            }
        }        
        " />
        </dxcb:ASPxCallback>

        <script type="text/javascript" language="JavaScript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            function InitializeRequest(sender, args) 
            {
            $get('Form1').style.cursor = 'wait'; 

            // Get a reference to the element that raised the postback,
            //   and disables it.
            $get(args._postBackElement.id).disabled = true;
            }

            function EndRequest(sender, args) 
            {
            $get('Form1').style.cursor = 'auto';

            // Get a reference to the element that raised the postback
            //   which is completing, and enable it.
            $get(sender._postBackSettings.sourceElement.id).disabled = false;
            
            Financial.Login.updateHTML();
            
            }            
        </script>

        <table id="Login1">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="UserName" runat="server" CssClass="input_text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                CssClass="failureText" ErrorMessage="Campo obrigatório." ToolTip="Campo obrigatório."
                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                            <asp:LinkButton ID="EnviarButton" runat="server" Font-Underline="false" CommandName="Login"
                                Text="{text}" CssClass="btn_enviar" ValidationGroup="Login1" OnClientClick="if(Page_ClientValidate()) { callbackSenha.SendCallback(); return false; } ">
                            </asp:LinkButton>
                            <asp:LinkButton ID="BackButton" runat="server" Font-Underline="false" CommandName="Login"
                                CssClass="btn_back" Text="{text}" OnClick="BackButton_Click">
                            </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>