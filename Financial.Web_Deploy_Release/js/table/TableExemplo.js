Ext.namespace('FDESK.panel.ZoomPosicaoCarteira');
FDESK.panel.ZoomPosicaoCarteira = function(config) {

	var tableRiscoRetorno1 = {
		xtype : 'basetable',
		readerConfig : {
			// idProperty : 'Col1',
			fields : [{
						name : 'Col1',
						type : 'string'
					}, {
						name : 'Col2',
						type : 'string'
					}, {
						name : 'Col3',
						type : 'string'
					}, {
						name : 'Tipo',
						type : 'string'
					}]
		},
		data : [{
					Col1 : 'Meses com retorno <span class="positivo">positivo</span>',
					Col2 : '11',
					Col3 : '22',
					Tipo : '1'
				}, {
					Col1 : 'Meses com retorno <span class="negativo">negativo</span>',
					Col2 : '66',
					Col3 : '77',
					Tipo : '1'
				}, {
					Col1 : 'Meses com retorno <span class="positivo">positivo</span>',
					Col2 : '11',
					Col3 : '22',
					Tipo : '2'
				}],

		listeners : {
			'click' : function(dataView, index, node, e) {
				Ext.trace('Abrir Window: '
						+ dataView.store.data.items[index].data.Col1);
			}
		},

		tpl : new Ext.XTemplate(
				'<table class="x-table-financial" width="453" cellspacing="0" cellpadding="0">',
					'<tr>', 
						'<th width="50%" class="centered">&nbsp</th>',
						'<th width="20%" class="centered">Qtd.</th>',
						'<th width="20%" class="centered">Perct.</th>', 
					'</tr>',
					'<tpl for=".">',
						'<tpl if="this.isQuebra(Tipo, Col1, xindex)">',//Exemplo de quebra para ser usado em uma das tabelas
				            '<tr><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td></tr>',
        				'</tpl>',
						'<tr class="selector {[xindex % 2 === 0 ? "dark-bg" : "light-bg"]}">',
							'<td width="20%">{Col1}</td>',
							'<td width="20%" class="centered">{Col2}</td>',
							'<td width="20%" class="centered">{Col3}</td>', 
						'</tr>',
					'</tpl>', 
				'</table>',
		{
					isQuebra : function(Tipo, Col1, xindex) {
						var quebra = (xindex > 1) && (this.currTipo)
								&& (this.currTipo !== Tipo)
						this.currTipo = Tipo;
						return quebra;
					}

				})
	};

	this.items = [{
				xtype : 'carteirasintetica',
				data : FDESK.AS.data.carteiraSintetica,
				collapsed : true
			}, {
				xtype : 'panelgrafico',
				title : 'Grafico de teste',
				height : 500,
				collapsed : true,
				collapsible : true
			}, {
				title : 'Teste tabela',
				height : 500,
				collapsed : false,
				collapsible : true,
				tbar : [{
							text : 'Reload Table Data',
							handler : function() {
								var table1 = this.ownerCt.ownerCt.items.get(0);
								table1.store.loadData([{
											Col1 : 'New Value',
											Col2 : '99',
											Col3 : '00'
										}])
							}
						}],
				items : [tableRiscoRetorno1]
			}];
	FDESK.panel.ZoomPosicaoCarteira.superclass.constructor.call(this, config);
};

Ext.extend(FDESK.panel.ZoomPosicaoCarteira, Ext.Panel);
Ext.reg('zoomposicaocarteira', FDESK.panel.ZoomPosicaoCarteira);