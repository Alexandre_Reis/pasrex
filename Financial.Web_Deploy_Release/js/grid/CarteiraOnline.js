Ext.namespace('FDESK.grid.CarteiraOnline');
FDESK.grid.CarteiraOnline = function(config) {

	var reader = new Ext.data.JsonReader({
				idProperty : 'IdAtivo',
				fields : [{
							name : 'IdAtivo',
							type : 'string'
						}, {
							name : 'IdTipo',
							type : 'int'
						}, {
							name : 'TipoPlural',
							type : 'string',
							sortType: function(value)
                            {
                               switch (value.toLowerCase())
                               {
                                  case 'a��es': return 1;
                                  case 'caixa': return 2;
                                  case 'multimercado': return 3;
                                  case 'renda fixa': return 4;
                                  case 'total geral': return 99;
                                  default: return 98;
                               }
                            }
						}, {
							name : 'Nome',
							type : 'string'
						}, {
							name : 'Quantidade',
							type : 'float'
						}, {
							name : 'PuCusto',
							type : 'float'
						}, {
							name : 'ValorCusto',
							type : 'float'
						}, {
							name : 'PuMercado',
							type : 'float'
						}, {
							name : 'ValorMercado',
							type : 'float'
						}, {
							name : 'ResultadoRealizar',
							type : 'float'
						}/*, {
							name : 'Var',
							type : 'float'
						}*/]

			});

	config = config || {};

	var summary = new Ext.ux.grid.GroupSummary();

	var store = Ext.extend(Ext.data.GroupingStore, {
				loadData : function(o, append) {
					this.fireEvent('beforeload', this);
					var r = this.reader.readRecords(o);
					this.loadRecords(r, {
								add : append
							}, true);
				}

			});

	Ext.apply(config, {
	    //autoScroll : true,
		ds : new store({
					reader : reader,
					data : config.data,
					groupField : 'TipoPlural',
					remoteSort : false,
					remoteGroup : false,
					sortInfo : {
						field : 'Nome',
						direction : 'ASC'
					}
				}),
		columns : [{
					header : 'Ativos',
					width : 250,
					sortable : false,
					dataIndex : 'Nome',
					hideable : false
				},/* {
					header : 'Tipo',
					width : 20,
					sortable : true,
					dataIndex : 'TipoPlural'
				},*/ {
					header : 'Quantidade',
					width : 100,
					sortable : true,
					dataIndex : 'Quantidade'/*,
					renderer : FDESK.util.Util.formatBRMoneySinal*/
					//FDESK.util.Util.formatBRMoneySinal(v, true);
				}, {
					header : 'PU Custo',
					width : 100,
					sortable : true,
					dataIndex : 'PuCusto',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Valor Custo',
					width : 100,
					sortable : true,
					dataIndex : 'ValorCusto',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'PU Mercado',
					width : 100,
					sortable : true,
					dataIndex : 'PuMercado',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Valor Mercado',
					width : 100,
					sortable : true,
					dataIndex : 'ValorMercado',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Resultado<br/>Realizar',
					width : 100,
					sortable : true,
					dataIndex : 'ResultadoRealizar',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}/*, {
					header : 'Var',
					width : 100,
					sortable : true,
					dataIndex : 'Var',
					//summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}*/],

		/*view : new FDESK.grid.GroupingView({
					forceFit : true,
					showGroupName : false,
					enableNoGroups : false,

					enableGroupingMenu : false,
					hideGroupedColumn : true,
					headersDisabled : true,
					disabledGroups : ['Caixa C/C', 'Total Geral']
				}),*/

        viewConfig: {
            onLoad: Ext.emptyFn, 
            listeners: { 
                beforerefresh: function(v) { 
                   v.scrollTop = v.scroller.dom.scrollTop; 
                   v.scrollHeight = v.scroller.dom.scrollHeight; 
                }, 
                refresh: function(v) { 
                   v.scroller.dom.scrollTop = v.scrollTop +  
                    (v.scrollTop == 0 ? 0 : v.scroller.dom.scrollHeight - v.scrollHeight); 
                } 
            },
        
            autoFill : true,
            loadMask: true,
            onStoreLoad: Ext.emptyFn 
        }/*,
		plugins : summary*/
/*
 * ,
 * 
 * tbar : [{ text : 'Reload novos valores', tooltip : 'Rload novos valores',
 * scope : this, handler : function() { this.store.loadData([{ IdAtivo : 3,
 * TipoPlural : 'Renda Fixa', IdTipo : 1, Nome : 'LFT - 15/11/2014',
 * SaldoAnterior : 136.44 }, { IdAtivo : 1, TipoPlural : 'A��es', IdTipo : 2,
 * Nome : 'BBDC4', SaldoAnterior : 126 }, { IdAtivo : 2, TipoPlural : 'A��es',
 * IdTipo : 2, Nome : 'CGAS5', SaldoAnterior : 145 }, { IdAtivo : "", IdTipo :
 * 0, TipoPlural : 'Total Geral', Nome : "", SaldoAnterior : 12781850.00 }]);
 *  } }]
 */
		})
		
	FDESK.grid.CarteiraOnline.superclass.constructor.call(this, config);

	//this.on('rowclick', this.onRowClick, this);

};

Ext.extend(FDESK.grid.CarteiraOnline, Ext.grid.GridPanel, {
			title : 'Carteira Online',
			frame : true,
			//autoHeight : true,
			
			clicksToEdit : 1,
			collapsible : true,
			animCollapse : false,
			trackMouseOver : false,
			columnLines : true,
			bodyBorder : false,
			border : false,
			cls : 'grid-financial grid-carteira-sintetica grid-carteira-online',

			loadData : function(data) {			    
				this.store.loadData(data);
				for(var i=0; i < data.length; i++){
				
				    //pumercado, valormercado, resultadorealizar
				    data[i].PuMercado =  (parseFloat(data[i].PuCusto) * (1 + (Math.floor((Math.random() * 10)-1)/100))).toString();
				    data[i].ValorMercado =  parseFloat(data[i].Quantidade) * parseFloat(data[i].PuMercado);
				    data[i].ResultadoRealizar =  parseFloat(data[i].ValorMercado) - parseFloat(data[i].ValorCusto);
				    //data[i].Var = Math.floor(Math.random() * 100);
				}
				this.loadData.defer(2000, this, [data]);
			}
		});
Ext.reg('carteiraonline', FDESK.grid.CarteiraOnline);
