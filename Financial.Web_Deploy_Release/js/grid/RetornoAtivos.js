Ext.namespace('FDESK.grid.RetornoAtivos');
FDESK.grid.RetornoAtivos = function(config) {

    if(FDESK.AS.user.isCarteiraSimulada){
        this.tbar = [{
            text: '+ Ativo',
            handler: function(){
        
                FDESK.LE.openDocInWin({
                    tipoRegistro: 'AtivoCarteiraSimulada', 
                    action: 'insert'}, 
                false);
            }
        }];
    }

	var reader = new Ext.data.JsonReader({
				idProperty : 'IdAtivo',
				fields : [{
							name : 'IdAtivo',
							type : 'string'
						}, {
							name : 'IdTipo',
							type : 'int'
						}, {
							name : 'TipoPlural',
							type : 'string'
						}, {
							name : 'Nome',
							type : 'string'
						}, {
							name : 'PercentualAlocacao',
							type : 'float'
						}, {
							name : 'RetornoMes',
							type : 'float'
						}, {
							name : 'RetornoAno',
							type : 'float'
						}, {
							name : 'Retorno3Meses',
							type : 'float'
						}, {
							name : 'Retorno6Meses',
							type : 'float'
						}, {
							name : 'Retorno12Meses',
							type : 'float'
						}, {
							name : 'Retorno24Meses',
							type : 'float'
						}, {
							name : 'Retorno36Meses',
							type : 'float'
						}]

			});

	config = config || {};

	var store = Ext.extend(Ext.data.GroupingStore, {
				loadData : function(o, append) {
					this.fireEvent('beforeload', this);
					var r = this.reader.readRecords(o);
					this.loadRecords(r, {
								add : append
							}, true);
				}

			});

	Ext.apply(config, {
		ds : new store({
					reader : reader,
					data : config.data,
					groupField : 'TipoPlural',
					remoteSort : false,
					remoteGroup : false,
					sortInfo : {
						field : 'Nome',
						direction : 'ASC'
					}
				}),
		columns : [{
					header : 'Ativos',
					width : 250,
					sortable : false,
					dataIndex : 'Nome',
					hideable : false
				}, {
					header : 'Tipo',
					width : 20,
					sortable : true,
					dataIndex : 'TipoPlural'
				}, FDESK.AS.user.isCarteiraSimulada ? {
					header : '% aloc.',
					width : 50,
					sortable : true,
					dataIndex : 'PercentualAlocacao',
					renderer : FDESK.util.Util.formatBRMoneySinal
				} : null,
				{
					header : 'M�s',
					width : 100,
					sortable : true,
					dataIndex : 'RetornoMes',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Ano',
					width : 100,
					sortable : true,
					dataIndex : 'RetornoAno',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '3 Meses',
					width : 100,
					sortable : true,
					dataIndex : 'Retorno3Meses',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '6 Meses',
					width : 100,
					sortable : true,
					dataIndex : 'Retorno6Meses',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '12 Meses',
					width : 100,
					sortable : true,
					dataIndex : 'Retorno12Meses',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '24 Meses',
					width : 100,
					sortable : true,
					dataIndex : 'Retorno24Meses',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '36 Meses',
					width : 100,
					sortable : true,
					dataIndex : 'Retorno36Meses',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}].compact(),

		view : new Ext.grid.GroupingView({
					forceFit : true,
					showGroupName : false,
					enableNoGroups : false,
		            startCollapsed : true,
					enableGroupingMenu : false,
					hideGroupedColumn : true,
					headersDisabled : true,
					disabledGroups : ['Caixa C/C', 'Total Geral']
				})
		})
		
	FDESK.grid.RetornoAtivos.superclass.constructor.call(this, config);
	this.on('rowclick', this.onRowClick, this);
    
};

Ext.extend(FDESK.grid.RetornoAtivos, Ext.grid.GridPanel, {
			title : 'Retorno dos Ativos',
			frame : true,
			clicksToEdit : 1,
			collapsible : true,
			animCollapse : false,
			trackMouseOver : false,
			columnLines : true,
			bodyBorder : false,
			border : false,
			cls : 'grid-financial grid-carteira-sintetica',
			onRowClick : function(grid, rowIndex) {
				var row = grid.selModel.getSelected();
				
				if(!row.data.IdAtivo.length){
				    return;
				}
				
				var pkRegistro = [];
				
				pkRegistro.push(FDESK.AS.user.idCliente);
				pkRegistro.push(row.data.IdTipo);
				pkRegistro.push(row.data.IdAtivo);
				
				FDESK.LE.openDocInWin({
				    pkRegistro: pkRegistro.join('|'),
                    tipoRegistro: 'AtivoCarteiraSimulada', 
                    action: 'read'}, 
                false);
				
				/*var rec = FDESK.AS.stores.tipoAtivo.getAt(FDESK.AS.stores.tipoAtivo.find(
						'IdTipo', row.data.IdTipo));
				
				if(!FDESK.win[rec.data.Tipo]){
				    Ext.Msg.alert('Portal do Cliente', 'N�o existe detalhamento para este ativo');
				    return;
				}
				
                this.showWindow({
							tipoRegistro : rec.data.Tipo,
							pkRegistro : row.data.IdAtivo
						});*/
			},

			loadData : function(data) {
				this.store.loadData(data);
			}
		});
Ext.reg('retornoativos', FDESK.grid.RetornoAtivos);
