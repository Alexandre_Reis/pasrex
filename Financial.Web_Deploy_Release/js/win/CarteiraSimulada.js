Ext.namespace('FDESK.win.CarteiraSimulada');
FDESK.win.CarteiraSimulada = function(config) {
	Ext.apply(this, config);

    this.fIdCarteira = new Ext.form.NumberField({
        fieldLabel: 'Id',
        width: 100,
        id: 'IdCarteira'
    });
    
    this.fNome = new Ext.form.TextField({
        fieldLabel: 'Nome',
        id: 'Nome',
        width: 250
    });
    

    this.form = new Ext.form.FormPanel({
		id : 'form-carteira-simulada',
		autoHeight : true,
		params : {},
		labelWidth: 60,
		defaults : {
			//style : 'margin-bottom: 10px;'
		},
		bodyStyle : 'padding:25px 15px 15px 15px; background-color: #DFE8F6;',
		border: false,
		frame : false,
		items : [this.fIdCarteira, this.fNome],
		border : false,
		autoScroll: true,
		tipoRegistro : this.tipoRegistro,
		url : FDESK.AppSettings.g_WebDbName + '/ashx/controllers/carteirasimulada.ashx'
		
	});
	
	FDESK.win.CarteiraSimulada.superclass.constructor.call(this, {
				width : 370,
				height : 176,
				autoScroll : true,
				resizable : false,
				modal : true,
				closeAction : 'hide',
				buttons : [{
							text : this.butOkText,
							id : 'btn-ok-' + this.tipoRegistro.toLowerCase(),
							//iconCls : 'icon-disk',
							scope : this,
							handler: this.submitForm
						}, {
							text : this.butCancelText,
							cls : 'x-btn-plain',
							//iconCls : 'icon-minus-circle',

							handler : this.hide.createDelegate(this, [])
						}],

				items : this.form

			});

};

Ext.extend(FDESK.win.CarteiraSimulada, Ext.Window, {
			title : 'CarteiraSimulada',
			tipoRegistro : 'CarteiraSimulada',
			butOkText : 'OK',
			butCancelText : 'Cancelar',

			updateForm : function(config, show) {
			    
			    var basicForm = this.form.getForm();
			    basicForm.reset();
			    this.action = config.action;
    			this.show();
                
				this.form.params.action = config.action;
			},
			submitForm : function(params) {

				var bForm = this.form.getForm();
				
                
				var msgErro;
				if (!bForm.isValid()) {
					alert('Todos os campos possuem preenchimento obrigatório');
					return;
				}
                
				bForm.submit({
							waitMsg : 'Gravando',
							scope : this,
							params : this.form.params,
							success : function(form, action) {
								var eventParams = {};
								eventParams.action = 'save';
								
								this.publishEvent(this.tipoRegistro
												.toLowerCase()
												+ 'updated', eventParams);
								this.hide();
								
								window.location = window.location.pathname + '?' + 'idCliente=' + action.result.data.IdCarteira;
								
							},
							failure : function(form, action) {
							    if(action && action.result
										&& action.result.erros){
										    alert(action.result.erros);
										}
							}
						});
			}
		});

