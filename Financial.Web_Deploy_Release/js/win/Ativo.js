Ext.namespace('FDESK.win.Ativo');

FDESK.win.Ativo = function(config) {

	var headerTitleTpl = new Ext.Template('<div><h4>Detalhamento por Ativo</h4>',
			'<h2>{NomeAtivo}</h2></div>', {
				compiled : true,
				disableFormats : true
			});

	var headerTitle = {
		xtype : 'box',
		cls : 'header-title',
		tpl : headerTitleTpl,
		ref : 'headerTitle'
	};

	Ext.apply(config, {
				id : 'crud-' + config.tipoRegistro + '-win',
				buttons : [{
							text : this.butOkText,
							handler : this.hide.createDelegate(this, [])
						}],

				items : [headerTitle].concat(config.items).compact()

			});

	FDESK.win.Ativo.superclass.constructor.call(this, config);

	var events = {};
	events[this.tipoRegistro.toLowerCase() + 'updated'] = true;
	this.addEvents(events);
};

Ext.extend(FDESK.win.Ativo, Ext.Window, {
			title : 'Ativo',
			flNome : 'Nome',
			butOkText : 'OK',
			width : 890,
			height : 386,
			resizable : false,
			cls : 'solve-window',
			modal : true,
			closeAction : 'hide',

			updateForm : function(config, show, extraParams) {
				if(config && config.pkRegistro){
					this.pk = config.pkRegistro;
				}
				var params = {
					popup : '1',
					sqlTable : this.tipoRegistro,
					pk : this.pk ,
					idCliente : FDESK.AS.user.idCliente
				};

				if (extraParams) {
					Ext.apply(params, extraParams);
				}

				if (!this.rendered) {
					this.show();
				}
				
				Ext.Ajax.request({
							scope : this,
							method : 'GET',
							params : params,
							url : FDESK.AS.g_WebDbName + '/ashx/table.ashx',
							failure : function(response) {
								var obj = Ext.decode(response.responseText);
								FDESK.AS.errHandler(obj && obj.erros);
								
								this.hide();
								this.fireEvent('formready');
							},
							success : function(response) {
								var obj = Ext.decode(response.responseText);
								this.afterUpdateForm(obj);
							}
						});
			},

			afterUpdateForm : function() {
				this.show();
				this.fireEvent('formready');
			}
		});