Ext.namespace('FDESK.panel.ZoomCarteiraOnline');
FDESK.panel.ZoomCarteiraOnline = function(config) {

	var panelCarteiraOnline = {
		xtype : 'carteiraonline',
		data : [],
		ref : 'tableCarteiraOnline',
		collapsible : false,
		collapsed : false,
		height : 350,
		style : 'margin: 30px 20px 30px 20px;'/*,
		tools : [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_2_1'});
			    }
		    }]*/
	};

	this.items = [panelCarteiraOnline];

	FDESK.panel.ZoomCarteiraOnline.superclass.constructor.call(this, config);
	this.subscribeEvent('tabcarteiraonlinetablesloaded', this.loadTables, this);
    
	this.subscribeEvent('zoomcardsresize', this.adjustHeight, this);
	
};

Ext.extend(FDESK.panel.ZoomCarteiraOnline, Ext.Panel, {
			border : false,
			loadTables : function(data) {
			    if(data.carteiraOnline){
				    this.tableCarteiraOnline.loadData(data.carteiraOnline);
				}
			},
			adjustHeight : function() {
				var h = this.ownerCt.getHeight() - 90;
                this.tableCarteiraOnline.setHeight(h);
			}
		});

Ext.reg('zoomcarteiraonline', FDESK.panel.ZoomCarteiraOnline);