Ext.namespace('FDESK.util.Util');

FDESK.util.Util = function() {
	return {
		formatBRMoney : function(v) {
		    if(v === ''){
		        return v;
		    }
			return Ext.util.Format.number(v, "0.000,00/i");
		},

		formatPercent : function(v) {
			return v && v + '%';
		},

		formatBRMoneyPercent : function(v) {
			return FDESK.util.Util.formatPercent(FDESK.util.Util
					.formatBRMoney(v));

		},
		
		formatBRMoneyPercentSinal : function(v, positivoAsNeutro) {
			return FDESK.util.Util.formatSinal(FDESK.util.Util.formatPercent(FDESK.util.Util
					.formatBRMoney(v)), positivoAsNeutro);

		},
		
		formatBRMoneySinal : function(v, positivoAsNeutro) {
			return FDESK.util.Util.formatSinal(FDESK.util.Util
					.formatBRMoney(v), positivoAsNeutro);

		},
		
		formatSinal : function(v, positivoAsNeutro) {
		    if(v == ''){
		        v = '-';
		    }
			if(v && Ext.type(v) !== 'string'){
				v.toString();
			}

			return String.format('<span class="{0}">{1}</span>', (v.charAt(0) == '-' && v.length > 1)
							? "negativo"
							: positivoAsNeutro === false ? "positivo" : "neutro", v);
		},
		formatSinalPercent : function(v, positivoAsNeutro) {
			return FDESK.util.Util
					.formatSinal(FDESK.util.Util.formatPercent(v), positivoAsNeutro);
		}
	}

}();

Ext.apply(Ext.form.VTypes, {
			daterange : function(val, field) {
				var date = field.parseDate(val);

				if (!date) {
					return;
				}
				if (field.startDateField
						&& (!this.dateRangeMax || (date.getTime() != this.dateRangeMax
								.getTime()))) {
					var start = Ext.getCmp(field.startDateField);
					start.setMaxValue(date);
					// start.validate();
					this.dateRangeMax = date;
				} else if (field.endDateField
						&& (!this.dateRangeMin || (date.getTime() != this.dateRangeMin
								.getTime()))) {
					var end = Ext.getCmp(field.endDateField);
					end.setMinValue(date);
					// end.validate();
					this.dateRangeMin = date;
				}
				/*
				 * Always return true since we're only using this vtype to set
				 * the min/max allowed values (these are tested for after the
				 * vtype test)
				 */
				return true;
			}
		});
		
		
		
		
// M�todos para dar hide no label, al�m do field
Ext.apply(Ext.Component.prototype, {
	hideAll : function() {
		var el = this.getEl().up('.x-form-item');
		el['set' + ((this.hideMode == 'visibility') ? 'Visible' : 'Displayed')](false);
	},
	showAll : function() {
		var el = this.getEl().up('.x-form-item');
		el['set' + ((this.hideMode == 'visibility') ? 'Visible' : 'Displayed')](true);
	}
});

// PageBus ou MessageBus - fila de eventos global
Ext.ns('Ext.ux.event');
Ext.ux.event.Broadcast = new Ext.util.Observable;
Ext.override(Ext.util.Observable, {

			/**
			 * This function is <b>addListener</b> analog for broadcasted
			 * messages. It accept the same parameters and have the same
			 * functionality. For further details please refer to
			 * <b>Ext.util.Observable</b> documentation
			 * 
			 * @name subscribeEvent
			 * @methodOf Ext.ux.event.Broadcast
			 * @param {String}
			 *            eventName
			 * @param {Function}
			 *            fn
			 * @param {Object}
			 *            scope
			 * @param {Object}
			 *            o
			 */
			subscribeEvent : function(eventName, fn, scope, o) {
				Ext.ux.event.Broadcast.addEvents(eventName);
				Ext.ux.event.Broadcast.on(eventName, fn, scope, o);
			},

			/**
			 * This function is <b>fireEvent</b> analog for broadcasted
			 * messages. It accept the same parameters and have the same
			 * functionality. For further details please refer to
			 * <b>Ext.util.Observable</b> documentation
			 * 
			 * @name publishEvent
			 * @methodOf Ext.ux.event.Broadcast
			 * @param {String}
			 *            eventName
			 * @param {Object}
			 *            args Variable number of parameters are passed to
			 *            handlers
			 * @return {Boolean} returns false if any of the handlers return
			 *         false otherwise it returns true
			 */
			publishEvent : function() {
				if (Ext.ux.event.Broadcast.eventsSuspended !== true) {
					var ce = Ext.ux.event.Broadcast.events
							? Ext.ux.event.Broadcast.events[arguments[0]
									.toLowerCase()]
							: false;
					if (typeof ce == "object") {
						return ce.fire.apply(ce, Array.prototype.slice.call(
										arguments, 1));
					}
				}
				return true;
			},

			/**
			 * This function is <b>removeListener</b> analog for broadcasted
			 * messages.
			 * 
			 * @name removeSubscriptionsFor
			 * @methodOf Ext.ux.event.Broadcast
			 * @param {String}
			 *            eventName The type of event which subscriptions will
			 *            be removed. If this parameter is evaluted to false,
			 *            then ALL subscriptions for ALL events will be removed.
			 */
			removeSubscriptionsFor : function(eventName) {
				for (var evt in Ext.ux.event.Broadcast.events) {
					if ((evt == eventName) || (!eventName)) {
						if (typeof Ext.ux.event.Broadcast.events[evt] == "object") {
							Ext.ux.event.Broadcast.events[evt].clearListeners();
						}
					}
				}
			}

		});

// dae/build/util/Misc.js
Ext.namespace('Solve.Util', 'Solve.Util.Array');

Solve.Util.Array = function() {
	return {

		objToMatrix : function(pObj) {
			var newArray = [];
			for (var prop in pObj) {
				newArray.push([prop, pObj[prop]]);
			};
			return newArray;
		},

		matrixToObj : function(pMatrix) {
			var newObj = {};
			for (var i = 0; i < pMatrix.length; i++) {
				newObj[pMatrix[i][0]] = pMatrix[i][1];
			};
			return newObj;
		},

		arrayToObj : function(pArray) {
			var obj = {};
			for (var i = 0; i < pArray.length; i++) {
				obj[pArray[i]] = '';
			}
			return obj;
		},

		invertObj : function(pObj) {
			var newObj = {};
			for (var prop in pObj) {
				newObj[pObj[prop]] = prop;
			};
			return newObj;
		}
	}
}();

Ext.namespace('Solve.Util.Store');
Solve.Util.Store = function() {
	return {
		makeStore : function(pArray, storeId) {
			var linkedStore = (pArray && (pArray.length > 0) && (pArray[0].length > 2));
			var fields = linkedStore ? ['value', 'text', 'valueParent'] : [
					'value', 'text'];

			var newStore = new Ext.data.SimpleStore({
				fields : fields,
				data : pArray,
				storeId : storeId,
				linked : linkedStore
					/*
					 * , sortInfo : { field : "text", direction : "ASC" }
					 */
				});

			return newStore;
		},

		rebuildStore : function(store, newArray) {
			store.removeAll();
			Ext.each(newArray, function(elNewArray) {
						var tmpArray = [];
						tmpArray['value'] = elNewArray[0];
						tmpArray['text'] = elNewArray[1];
						if (store.fields.length > 2) {
							tmpArray['valueParent'] = elNewArray[2];
						}

						var newRec = new Ext.data.Record(tmpArray);
						store.add(newRec);
					}, this);
			store.sort('text', 'ASC');
		}
	};
}();

// Slidebox
Ext.namespace('Ext.ux.SlideBox');

Ext.ux.SlideBox = function() {
	var msgCt;

	function createBox(t, s) {
		return [
				'<div class="msg">',
				'<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>',
				t,
				'</h3>',
				s,
				'</div></div></div>',
				'<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
				'</div>'].join('');
	}

	return {
		moveObj : function() {

		},

		msg : function(config) {
			var defConfig = {
				containerEl : Ext.getBody(),
				id : 'msg-div',
				alignTo : document,
				alignPosition : 't-t',
				alignLeft : 0,
				alignTop : -90,
				direction : 'down',
				offsetDir : 100,
				effectDuration : 1,
				pauseDelay : 3
			};

			Ext.apply(this, config, defConfig);

			if (config.icon) {
				this.title = '<img src="' + config.icon + '" /> ' + this.title;
			}

			if (!msgCt) {
				msgCt = Ext.DomHelper.insertFirst(this.containerEl, {
							id : this.id
						}, true);
			}

			msgCt.alignTo(this.alignTo, this.alignPosition, [this.alignLeft,
							this.alignTop]);
			var m = Ext.DomHelper.append(msgCt, {
						html : createBox(this.title, this.message)
					}, true);

			m.move(this.direction, this.offsetDir, {
						duration : this.effectDuration
					});

			m.pause(this.pauseDelay);
			m.ghost("t", {
						remove : true
					});
		}
	};
}();

//
Ext.namespace('Solve.Util.String');
Solve.Util.String = function() {
	return {
		parseQueryString : function(name) {
			name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
			var regexS = "[\\?&]" + name + "=([^&#]*)";
			var regex = new RegExp(regexS, 'i');
			var results = regex.exec(window.location.href);
			if (results === null) {
				return "";
			} else {
				return results[1];
			}
		}
	}
}();

// NiftyCorners
Ext.namespace('Solve.Util.Nifty');
Solve.Util.Nifty = function() {
	return {
		NiftyCheck : function() {
			if (!document.getElementById || !document.createElement)
				return (false);
			var b = navigator.userAgent.toLowerCase();
			if (b.indexOf("msie 5") > 0 && b.indexOf("opera") == -1)
				return (false);
			return (true);
		},

		Rounded : function(v, bk, color, size) {
			var i;

			var l = v.length;
			for (i = 0; i < l; i++) {
				this.AddTop(v[i], bk, color, size);
				this.AddBottom(v[i], bk, color, size);
			}
		},

		RoundedTop : function(v, bk, color, size) {
			var i;
			for (i = 0; i < v.length; i++)
				this.AddTop(v[i], bk, color, size);
		},

		RoundedBottom : function(v, bk, color, size) {
			var i;
			for (i = 0; i < v.length; i++)
				this.AddBottom(v[i], bk, color, size);
		},

		AddTop : function(el, bk, color, size) {
			var i;
			var d = document.createElement("b");
			var cn = "r";
			var lim = 4;
			if (size && size == "small") {
				cn = "rs";
				lim = 2
			}
			d.className = "rtop";
			d.style.backgroundColor = bk;
			for (i = 1; i <= lim; i++) {
				var x = document.createElement("b");
				x.className = cn + i;
				x.style.backgroundColor = color;
				d.appendChild(x);
			}

			el.insertBefore(d, el.firstChild);
		},

		AddBottom : function(el, bk, color, size) {
			var i;
			var d = document.createElement("b");
			var cn = "r";
			var lim = 4;
			if (size && size == "small") {
				cn = "rs";
				lim = 2
			}
			d.className = "rbottom";
			d.style.backgroundColor = bk;
			for (i = lim; i > 0; i--) {
				var x = document.createElement("b");
				x.className = cn + i;
				x.style.backgroundColor = color;
				d.appendChild(x);
			}
			el.appendChild(d, el.firstChild);
		}
	}
}();

// Plugin para adicionar t�tulo �s collapsed regions horizontais
Ext.ux.collapsedPanelTitlePlugin = function() {
	this.init = function(p) {
		if (p.collapsible) {
			var r = p.region;
			if ((r == 'north') || (r == 'south')) {
				p.on('render', function() {
					var ct = p.ownerCt;
					ct.on('afterlayout', function() {
						if (p.collapsedTitleEl || !ct.layout[r].collapsedEl) {
							return;
						}
						p.collapsedTitleEl = ct.layout[r].collapsedEl
								.createChild({
											tag : 'div',
											cls : 'x-panel-collapsed-text',
											html : p.title
										});
						p.setTitle = Ext.Panel.prototype.setTitle
								.createSequence(function(t) {
											p.collapsedTitleEl.dom.innerHTML = t;
										});

						ct.removeListener('afterlayout', arguments.callee);
					});
				});
			}
		}
	}
}

/**
 * Clone Function
 */
Ext.ux.clone = function(o) {
	if (!o || 'object' !== typeof o) {
		return o;
	}
	var c = '[object Array]' === Object.prototype.toString.call(o) ? [] : {};
	var p, v;
	for (p in o) {
		if (o.hasOwnProperty(p)) {
			v = o[p];
			if (v && 'object' === typeof v) {
				c[p] = Ext.ux.clone(v);
			} else {
				c[p] = v;
			}
		}
	}
	return c;
}; // eo function clone

Ext.override(Ext.data.Node, {
			findChild : function(attribute, value, deep) {
				var cs = this.childNodes, res, n;
				for (var i = 0, len = cs.length; i < len; i++) {
					n = cs[i];
					if (n.attributes[attribute] == value) {
						return n;
					} else if (deep) {
						res = n.findChild(attribute, value, deep);
						if (res != null) {
							return res;
						}
					}
				}
				return null;;
			},

			findChildBy : function(fn, scope, deep) {
				var cs = this.childNodes, res, n;
				for (var i = 0, len = cs.length; i < len; i++) {
					n = cs[i];
					if (fn.call(scope || n, n) === true) {
						return n;
					} else if (deep) {
						res = n.findChildBy(fn, scope, deep);
						if (res != null) {
							return res;
						}
					}
				}
				return null;
			}
		});

// dae-2.0/build/ux/ExtArray.js
/*
 * HEA - Hollis /(Ext)-\1ensions/
 */

/*
 * returns a type for an object @param {object} @return {sring} one of
 * domElement, domText, domWhitespace, arguments, array, object, string, number,
 * boolean, function, regexp, date, class, collection, null
 */
/*
 * HEA - Hollis /(Ext)-\1ensions/
 */

Ext.apply(Ext, {
			/*
			 * various tests for definedness and types @param {object} @return
			 * {boolean}
			 */
			isDefined : function(o) {
				return ((o !== undefined && o !== null));
			},
			isElement : function(o) {
				return Ext.type(o) == 'element';
			},
			isTextnode : function(o) {
				return Ext.type(o) == 'textnode';
			},
			isWhitespace : function(o) {
				return Ext.type(o) == 'whitespace';
			},
			isNodelist : function(o) {
				return Ext.type(o) == 'nodelist';
			},
			isArray : function(o) {
				return Ext.type(o) == 'array';
			},
			/*
			 * isObject : function(o) { return Ext.type(o) == 'object'; },
			 */
			isString : function(o) {
				return Ext.type(o) == 'string';
			},
			isNumber : function(o) {
				return Ext.type(o) == 'number';
			},
			isBoolean : function(o) {
				return Ext.type(o) == 'boolean';
			},
			isFunction : function(o) {
				return Ext.type(o) == 'function';
			},
			isRegexp : function(o) {
				return Ext.type(o) == 'regexp';
			},
			isNull : function(o) {
				return Ext.type(o) == 'null';
			},
			is : function(o, type) {
				return type == Ext.type(o);
			},

			trace : function(targetDebug, sep) {
				var traceStr = '';
				var sep = sep || '\n';
				if (typeof targetDebug == 'object') {
					for (prop in targetDebug) {
						traceStr += prop
								+ ': '
								+ ((typeof targetDebug[prop] == 'function')
										? 'function'
										: targetDebug[prop]) + sep;
					}
				} else {
					traceStr = targetDebug;
				}
				alert(traceStr);
			},

			/*
			 * creates a clone of an object @param {object,boolean} @return
			 * {object}
			 */
			clone : function(o, deep) {
				var objectClone = new o.constructor();

				for (var property in this) {
					if (!deep) {
						objectClone[property] = Ext.clone(o[property], deep);
					} else if (typeof o[property] == 'object') {
						objectClone[property] = Ext.clone(o[property], deep);
					} else {
						objectClone[property] = o[property];
					}
				}

				return objectClone;
			},

			/*
			 * takes a code block, a string or nothing returns an creates an
			 * appropriate function(item){}, for map, grep, etc - block, returns
			 * itself - 'string' returns a function that accesses property of
			 * item - 'string()' returns a function that calls method of item
			 * nothing returns item @param {[block|method]} @return {block}
			 */
			block : function(block) {
				// console.debug("b " + block);
				// if ( Ext.isDefined(block) )
				// return function ( item ) { return item };

				var type = Ext.type(block);

				if (type == 'function') {
					return block;
				}
				if (type == 'string' && block.search(/\(\)$/)) {
					return function(item) {
						return item[block].call();
					};
				}
				if (type == 'string') {
					return function(item) {
						return item[block];
					};
				}
				if (type == 'regexp') {
					return function(item) {
						return block.test(item);
					};
				}
				return function(item) {
					return item;
				};
			}
		});

// Array functions like those of prototype and more

/*
 * @class Array
 */

Ext.applyIf(Array.prototype, {
			/*
			 * clears the array (makes it empty). @param {} @return {object}
			 * returns this
			 */
			// clear : function () {
			// this = [];
			// return this;
			// },
			/*
			 * returns a duplicate of the array, leaving the original array
			 * intact. @param {[deep]} create a deep copy (clone) @return
			 * {object} returns copy of this
			 */
			clone : function(deep) {
				return Ext.clone(this, deep);
			},

			/*
			 * weed out false values @param {[block|methodName]} @return
			 * {object} returns a new version of the array, without any
			 * null/undefined values.
			 */
			compact : function(block) {
				return this.grep();
			},

			/*
			 * calls block for each item @param {block} @return {object} returns
			 * this
			 */
			forEach : function(block) {
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					b(this[i]);
				}
				return this;
			},

			/*
			 * calls block for each item, @param {[block|methodName]} @return
			 * {Array} returns array of results of applying the block to each
			 * element
			 */
			map : function(block) {
				var m = [];
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					m.push(b(this[i], i));
				}
				return m;
			},

			/*
			 * calls block for each item, @param {[block|methodName]} @return
			 * {Array} Returns an Array of all items for which the block returns
			 * true
			 */
			grep : function(block) {
				var m = [];
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					if (b(this[i])) {
						m.push(this[i]);
					}
				}
				return m;
			},

			/*
			 * calls block for each item, logically the negation of grep @param
			 * {[block|methodName]} @return {Array} returns an Array of all
			 * items for which the block returns false
			 */
			not : function(block) {
				var m = [];
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					if (!b(this[i])) {
						m.push(this[i]);
					}
				}
				return m;
			},

			/*
			 * partitions the elements in two groups: those regarded as true,
			 * and those considered false. @param {[block|methodName]} @return
			 * {TrueArray, FalseArray} returns two separate arrays
			 */
			partition : function(block) {
				var t = [];
				var f = [];
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					if (b(this[i])) {
						t.push(this[i]);
					} else {
						f.push(this[i]);
					}
				}
				return [t, f];
			},

			/*
			 * sorts an array by applying a simple schwartzian transform. This
			 * means the array is first transformed into a 2d array, whichs
			 * elements are an easy to compare string returned be the transform
			 * block and the original value. The sort then operates on the first
			 * value. This potentially save a lot of computing time if the
			 * comparison is expensive. @param {transform,compare, } beenThere :
			 * a function that returns the sort-key compare: the sort function
			 * @return {Array} Returns the sorted Array
			 */
			// untested
			schwartzian_sort : function(transform, compare) {
				var t = Ext.block(transform);
				var c = compare ? compare : function(a, b) {
					if (a == b) {
						return 0;
					}
					if (a > b) {
						return 1;
					}
					return -1;
				};

				return this.map(function(it) {
							return [t(it), it];
						}).sort(function(a, b) {
							return c(a[0], b[0]);
						}).map(function(it) {
							return it[1];
						});
			},

			/*
			 * @param {[block|methodName]} @return {number} Returns the number
			 * of elements in Array for which the criterion in block is true.
			 */
			'true' : function(block) {
				return this.grep(block).length;
			},

			/*
			 * @param {[block|methodName]} @return {number} Returns the number
			 * of elements in Array for which the criterion in block is false.
			 */
			'false' : function(block) {
				return this.not(block).length;
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Returns a true
			 * value if any item meets the criterion given through block.
			 */
			any : function(block) {
				return this.grep(block).length > 0 ? true : false;
			},

			/*
			 * @param {[block|methodName]} @return {boolean}Returns true ifblock
			 * returns true for all items
			 */
			all : function(block) {
				return this.grep(block).length == this.length ? true : false;
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Logically the
			 * negation of any. Returns a true value if no item in Array meets
			 * the criterion given through block.
			 */
			none : function(block) {
				return this.grep(block).length === 0 ? true : false;
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Returns the index
			 * of the first element in Array for which the criterion in block is
			 * true.
			 */
			firstIndex : function(block) {
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					if (b(this[i])) {
						return i;
					}
				}
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Returns the first
			 * element in Array for which the criterion in block is true.
			 */
			firstValue : function(block) {
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					if (b(this[i])) {
						return this[i];
					}
				}
			},
			// ==find(iterator) -> firstElement | undefined

			/*
			 * @param {[block|methodName]} @return {boolean} Returns the index
			 * of the lat element in Array for which the criterion in block is
			 * true.
			 */
			lastIndex : function(block) {
				var b = Ext.block(block);
				for (var i = this.length - 1; i >= 0; i--) {
					if (b(this[i])) {
						return i;
					}
				}
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Returns the index
			 * of the lat element in Array for which the criterion in block is
			 * true.
			 */
			lastValue : function(block) {
				var b = Ext.block(block);
				for (var i = this.length - 1; i >= 0; i--) {
					if (b(this[i])) {
						return this[i];
					}
				}
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Returns a list of
			 * the values of Array after (and not including) the point where
			 * block returns a true value.
			 */
			after : function(block, inclusive) {
				// console.debug(" + " + block, this.firstIndex(block));
				return this.slice(this.firstIndex(block) + 1);
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Same as after but
			 * also inclues the element for which block is true.
			 */
			afterInclusive : function(block) {
				// console.debug(" * " + block, this.firstIndex(block));
				return this.slice(this.firstIndex(block));
			},

			/*
			 * @param {[block|methodName]} @return {boolean}Returns a list of
			 * values of Array upto (and not including) the point where block
			 * returns a true value.
			 */
			before : function(block) {
				return this.slice(0, this.firstIndex(block));
			},

			/*
			 * @param {[block|methodName]} @return {boolean} Same as before but
			 * also includes the element for which block is true.
			 */
			beforeInclusive : function(block) {
				return this.slice(0, this.firstIndex(block) + 1);
			},

			/*
			 * @param {[block|methodName]} @return {Array} Evaluates block for
			 * each element in Array (assigned to $_) and returns a list of the
			 * indices of those elements for which block returned a true value.
			 * This is just like grep only that it returns indices instead of
			 * values:
			 */
			indexes : function(block) {
				var b = Ext.block(block);
				var m = [];

				for (var i = 0; i < this.length; i++) {
					if (b(this[i])) {
						m.push(i);
					}
				}
				return m;
			},

			/*
			 * @param {[block|methodName]} @return {number} Evaluates block for
			 * each element and returns the biggest element
			 */
			max : function(block) {
				var b = Ext.block(block);
				var r;
				for (var i = 0; i < this.length; i++) {
					if (typeof r == 'undefined') {
						r = this[i];
					}

					if (b(this[i]) > b(r)) {
						r = this[i];
					}
				}
				return r;
			},

			/*
			 * @param {[block|methodName]} @return {number} Evaluates block for
			 * each element and returns the smalles element
			 */
			min : function(block) {
				var b = Ext.block(block);
				var r;
				for (var i = 0; i < this.length; i++) {
					if (typeof r == 'undefined') {
						r = this[i];
					}

					if (b(this[i]) < b(r)) {
						r = this[i];
					}

				}
				return r;
			},

			/*
			 * @param {[block|methodName]} @return {Array} Returns a new array
			 * by stripping duplicate values in Array.
			 */
			uniq : function(block) {

				var s = {};
				var m = [];
				var b = Ext.block(block);

				for (var i = 0; i < this.length; i++) {
					var k = b(this[i]);

					if (!s[k]) {
						m.push(this[i]);
					}

					s[k] = true;
				}

				return m;
			},
			/*
			 * @param {[block|methodName]} @return {iitem} Returns the first
			 * item in the array, or undefined if the array is empty.
			 */
			first : function() {
				return this.length ? this[0] : undefined;
			},

			/*
			 * @param {[block|methodName]} @return {iitem} Returns the last item
			 * in the array, or undefined if the array is empty.
			 */
			last : function() {
				return this.length ? this[this.length - 1] : undefined;
			},

			/*
			 * @param {} @return {iitem} Returns a reversed version of the array
			 */
			reverse : function() {
				var m;
				for (var i = 0; i < this.length; i++) {
					m.push(this[i]);
				}
				return m;
			},

			/*
			 * @param {} @return {iitem} Returns a JSON repres. of the array
			 */
			toJSON : function() {
				return Ext.util.JSON.encode(this);
			}
		});

Ext.applyIf(Array.prototype, {
			apply : Ext.map,
			collect : Ext.map,
			reject : Ext.not,
			firstElement : Ext.firstValue,
			find : Ext.firstValue,
			lastElement : Ext.lastValue
		});

Array.prototype.each = Array.prototype.forEach;

// Field Tooltip
Ext.sequence(Ext.form.Field.prototype, 'afterRender', function() {

	if (this.helpText) {

		var helpIconSrc = Ext.BLANK_IMAGE_URL;

		if (this.helpIcon == 'warning') {
			helpIconSrc = 'css/icons/warning.png'
		};
		if (this.helpIcon == 'help') {
			helpIconSrc = 'css/icons/help.png'
		};

		var label = null;
		var wrapDiv = this.getEl().up('div.x-form-item');

		if (wrapDiv) {
			label = wrapDiv.child('label');
		}

		if (label) {
			var helpImage = label.createChild({
				tag : 'div',
				cls : 'x-tool x-tool-help x-tool-help-field'
				//src : helpIconSrc,
				});

			if (!this.helpDisplay || this.helpDisplay == 'both') {

				Ext.QuickTips.register({
							target : helpImage,
							title : this.helpTitle,
							text : this.helpText,
							enabled : true
						});
			}

			if (this.helpDisplay == 'field' || this.helpDisplay == 'both') {

				Ext.QuickTips.register({
							target : this,
							title : this.helpTitle,
							text : this.helpText,
							enabled : true
						});

			}
		}
	}
		// your stuff
	});		
	
	
// FIX para o DatePicker para impedir problemas de data com DST - Daylight
// Saving
// Time
Date.prototype.clearTime = function(clone) {
	if (clone) {
		return this.clone().clearTime();
	}
	if (this.getHours() == 23) {
		this.setTime(this.getTime() + (3600000));
	} else {
		this.setHours(0);
	}

	this.setMinutes(0);
	this.setSeconds(0);
	this.setMilliseconds(0);
	return this;
};