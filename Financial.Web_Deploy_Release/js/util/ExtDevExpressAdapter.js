window.ExtDevExpressAdapter = {
	aftersave : function(param){
		var extParentPanel = ExtDevExpressAdapter.getParentPanel();
		extParentPanel.fireEvent('aftersave', param);
	},
	
	getParentPanel : function(){
		return parent.Ext.getCmp(ExtDevExpressAdapter.extPanelId);
	},

	parseQueryString : function(name){
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results === null) {
			return "";
		} else {
			return results[1];
		}
	}
}

ExtDevExpressAdapter.extPanelId = ExtDevExpressAdapter.parseQueryString('extpanelid');
