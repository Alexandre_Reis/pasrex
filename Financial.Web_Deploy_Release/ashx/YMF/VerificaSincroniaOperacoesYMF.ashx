<%@ WebHandler Language="C#" Class="VerificaSincroniaOperacoesYMF" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.Fundo;
using System.Data.SqlClient;
using Financial.Investidor;
using Financial.InvestidorCotista;
using System.Data;
using System.Text;
using Financial.InvestidorCotista.Enums;

public class VerificaSincroniaOperacoesYMF : IHttpHandler
{


    public void ProcessRequest(HttpContext context)
    {
        const int NAO_VALIDADO = -2;
        const int VALIDADO = -1;
        const string DELIMITADOR = ";";

        SqlConnection conn = new Carteira().CreateSqlConnectionYMFCOT();

        CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(false);

        string output = "";
        List<string> fundosComErro = new List<string>();

        List<int> fundosIgnorados = new List<int>();
        /*fundosIgnorados.Add(9907);
        fundosIgnorados.Add(7893);
        fundosIgnorados.Add(10243);
        fundosIgnorados.Add(9926);
        fundosIgnorados.Add(9933);
        fundosIgnorados.Add(9112);
        fundosIgnorados.Add(9788);
        fundosIgnorados.Add(9790);
        */

        int countFundo = 0;
        foreach (Carteira fundo in fundos)
        {
            countFundo++;

            if (fundosIgnorados.Contains(fundo.IdCarteira.Value))
            {
                continue;
            }

            int idCliente = fundo.IdCarteira.Value;
            //idCliente = 7893;//, 9922, 9933, 9788
            //idCliente = 9249;//sem operacao na YMF

            //idCliente = 9950;//parece cancelado na ymf
            //idCliente = 9241;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(idCliente);
            string codFundo = clienteInterface.CodigoYMF;

            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            dataTable.TableName = "COT_MOVIMENTO";

            cmd.CommandText = String.Format(
                "select * from {0} where cd_fundo = '{1}' and CD_TIPO not in ('RD', 'RJ', 'TR', 'RA', 'VR') ",
                dataTable.TableName, codFundo);

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);


            //Jogar operacao YMF numa lista de OperacaoCotistaValidar pra facilitar comparacao
            List<OperacaoCotista> operacoesYMF = new List<OperacaoCotista>();
            CotistaCollection cotistaCollection = new CotistaCollection();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                string cdCotista = (string)dataRow["CD_COTISTA"];
                Cotista cotista = cotistaCollection.BuscaCotistaPorCodigoInterface(cdCotista.Trim());

                OperacaoCotista operacaoYMF = new OperacaoCotista();
                operacaoYMF.DataOperacao = (DateTime)dataRow["DT_MOVIMENTO"];
                string cdTipo = (string)dataRow["CD_TIPO"];
                operacaoYMF.TipoOperacao = MapTipoMovimentoYMF(cdTipo.Trim());
                if(cotista != null){
                    operacaoYMF.IdCotista = cotista.IdCotista;
                }
                operacaoYMF.IdCarteira = cliente.IdCliente;

                operacaoYMF.DataConversao = (DateTime)dataRow["DT_LIQUIDACAO_FISICA"];
                operacaoYMF.DataLiquidacao = (DateTime)dataRow["DT_LIQUIDACAO_FINANCEIRA"];

                operacaoYMF.DadosBancarios = cdCotista; //Usar dados bancarios pra meter o Id Cotista YMF
                if (dataRow["VL_BRUTO"] != System.DBNull.Value)
                {
                    operacaoYMF.ValorBruto = Convert.ToDecimal(dataRow["VL_BRUTO"]);
                }
                if (dataRow["VL_IR"] != System.DBNull.Value)
                {
                    operacaoYMF.ValorIR = Convert.ToDecimal(dataRow["VL_IR"]);
                }
                if (dataRow["VL_IOF"] != System.DBNull.Value)
                {
                    operacaoYMF.ValorIOF = Convert.ToDecimal(dataRow["VL_IOF"]);
                }

                if (dataRow["VL_LIQUIDO"] != System.DBNull.Value)
                {   
                    operacaoYMF.ValorLiquido = Convert.ToDecimal(dataRow["VL_LIQUIDO"]);
                }

                if (dataRow["QT_COTAS"] != System.DBNull.Value)
                {
                    operacaoYMF.Quantidade = Convert.ToDecimal(dataRow["QT_COTAS"]);
                }

                if (dataRow["DT_MOVIMENTO"] != System.DBNull.Value)
                {
                    operacaoYMF.DataAgendamento = (DateTime)dataRow["DT_MOVIMENTO"];
                }
                
                if (dataRow["DT_LIQUIDACAO_FISICA"] != System.DBNull.Value)
                {
                    operacaoYMF.DataConversao = (DateTime)dataRow["DT_LIQUIDACAO_FISICA"];
                }
                
                if (dataRow["DT_LIQUIDACAO_FINANCEIRA"] != System.DBNull.Value)
                {
                    operacaoYMF.DataLiquidacao = (DateTime)dataRow["DT_LIQUIDACAO_FINANCEIRA"];
                }
                                
                operacaoYMF.IdOperacao = NAO_VALIDADO;
                operacoesYMF.Add(operacaoYMF);
            }

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente));
            operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);
            foreach (OperacaoCotista operacaoFinancial in operacaoCotistaCollection)
            {
                operacaoFinancial.IdOperacao = NAO_VALIDADO;

                //Ajustar tipo de operacao para um tipo unico pois YMF soh mapeia pra amortizacao generica
                if (operacaoFinancial.TipoOperacao == (byte)TipoOperacaoCotista.AmortizacaoJuros ||
                    operacaoFinancial.TipoOperacao == (byte)TipoOperacaoCotista.Juros)
                {
                    operacaoFinancial.TipoOperacao = (byte)TipoOperacaoCotista.Amortizacao;
                }
                
                //Tentar localizacao operacao identica na YMF
                foreach (OperacaoCotista operacaoYMF in operacoesYMF)
                {
                    if (operacaoYMF.IdOperacao == NAO_VALIDADO &&
                        operacaoYMF.IdCotista == operacaoFinancial.IdCotista &&
                        operacaoYMF.DataOperacao == operacaoFinancial.DataOperacao &&
                        operacaoYMF.DataConversao == operacaoFinancial.DataConversao &&
                        (Math.Abs(operacaoYMF.ValorLiquido.Value - operacaoFinancial.ValorLiquido.Value) < 0.10M) &&
                        //operacaoYMF.ValorLiquido == operacaoFinancial.ValorLiquido &&
                        operacaoYMF.TipoOperacao == operacaoFinancial.TipoOperacao)
                    {
                        operacaoYMF.IdOperacao = VALIDADO;
                        operacaoFinancial.IdOperacao = VALIDADO;
                        break; //S� podemos validar UMA OPERACAO DE cada vez... Se n�o saimos fora, podemos processar duas vezes uma operacao parecida
                    }
                }
                if (operacaoFinancial.IdOperacao == NAO_VALIDADO)
                {
                    bool esseNaoValidou = true;
                }
            }

            cmd.Connection.Close();

            //Verificar se ficou algum nao validado
            StringBuilder csvYMF = new StringBuilder();
            bool faltouValidar = false;
            foreach (OperacaoCotista operacaoYMF in operacoesYMF)
            {
                if (operacaoYMF.IdOperacao != VALIDADO)
                {
                    faltouValidar = true;
                    csvYMF.AppendLine(String.Format("{0}{5}{1}{5}{2}{5}{3}{5}{4}{5}{6}{5}{7}{5}{8}{5}",
                        operacaoYMF.IdCotista,
                        operacaoYMF.DataOperacao,
                        operacaoYMF.TipoOperacao,
                        operacaoYMF.ValorBruto,
                        operacaoYMF.ValorLiquido,
                        DELIMITADOR,
                        operacaoYMF.DadosBancarios,
                        operacaoYMF.DataConversao,
                        operacaoYMF.DataLiquidacao
                    ));
                }
            }

            StringBuilder csvFinancial = new StringBuilder();
            foreach (OperacaoCotista operacaoFinancial in operacaoCotistaCollection)
            {
                if (operacaoFinancial.IdOperacao != VALIDADO)
                {
                    faltouValidar = true;
                    csvFinancial.AppendLine(String.Format("{0}{5}{1}{5}{2}{5}{3}{5}{4}{5}{6}{5}{7}{5}{8}{5}",
                        operacaoFinancial.IdCotista,
                        operacaoFinancial.DataOperacao,
                        operacaoFinancial.TipoOperacao,
                        operacaoFinancial.ValorBruto,
                        operacaoFinancial.ValorLiquido,
                        DELIMITADOR,
                        "",
                        operacaoFinancial.DataConversao,
                        operacaoFinancial.DataLiquidacao
                    ));
                }
            }


            if (faltouValidar)
            {
                fundosComErro.Add(fundo.IdCarteira.Value + ": " + cliente.Nome + "(" + codFundo + ") implanta��o: " + cliente.DataImplantacao.Value.Date + " data dia: " + cliente.DataDia.Value.Date);

                if (false)
                {
                    continue;
                }
                else
                {
                    string header = String.Format("IdCotista{0}DataOperacao{0}TipoOperacao{0}ValorBruto{0}ValorLiquido{0}IdCotistaYMF{0}\n", DELIMITADOR);
                    output += "\n\nCliente: " + cliente.IdCliente + " " + cliente.Nome + " cd ymf:" + clienteInterface.CodigoYMF + " " + countFundo + "/" + fundos.Count +
                        ";;;;;;;;;\nYMF\n" + header + csvYMF.ToString() + "\nFinancial\n" + header + csvFinancial.ToString() + "\n\n";

                }

                /*context.Response.Clear();
                context.Response.ClearContent();
                context.Response.ClearHeaders();

                context.Response.ContentType = "text/csv";
                context.Response.ContentEncoding = System.Text.Encoding.Unicode;
                context.Response.AddHeader("Content-Disposition", "attachment; filename=verifica_cot.csv");
                context.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                string header = String.Format("IdCotista{0}DataAplicacao{0}ValorAplicacao{0}CotaAplicacao{0}Quantidade{0}ValorBruto{0}ValorIR{0}ValorIOF{0}ValorPerformance{0}ValorIOFVirtual\n", DELIMITADOR);
                context.Response.Write("Cliente: " + cliente.IdCliente + " " + cliente.Nome + " " + countFundo + "/" + fundos.Count +
                    ";;;;;;;;;\nYMF\n" + header + csvYMF.ToString() + "\nFinancial\n" + header + csvFinancial.ToString());
                return;*/
            }

        }

        context.Response.ContentType = "text/html";

        if (fundosComErro.Count > 0)
        {
            if (false)
            {
                context.Response.Write("Fundos com erro:" + fundosComErro.Count + "<br /><br />" + String.Join("<br />", fundosComErro.ToArray()));
            }
            else
            {
                context.Response.Clear();
                context.Response.ClearContent();
                context.Response.ClearHeaders();

                context.Response.ContentType = "text/csv";
                context.Response.ContentEncoding = System.Text.Encoding.Unicode;
                context.Response.AddHeader("Content-Disposition", "attachment; filename=verifica_operacao.csv");
                context.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                context.Response.Write(output);
                
            }
        }
        else
        {
            context.Response.Write("Nenhum erro encontrado");
        }

    }

    private byte MapTipoMovimentoYMF(string tipoMovimentoYMF)
    {
        //Faz o mapeamento entro e ENUM TipoMovimentoYMF definido em Interfaces com o ENUM TipoOperacaoCotista
        byte tipoOperacaoCotista;
        if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.Aplicacao ||
            tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.AG)
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.Aplicacao;
        }
        else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateBruto) ||
            ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaBruto)))
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateBruto;
        }
        else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateCotas) ||
  ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaCotas)))
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateCotas;
        }
        else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateLiquido) ||
  ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaLiquido)))
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateLiquido;
        }
        else if ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateTotal) ||
  ((tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaTotal)))
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.ResgateTotal;
        }
        else if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateIR)
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.ComeCotas;
        }
        else if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.RM)
        {
            tipoOperacaoCotista = (byte)TipoOperacaoCotista.Amortizacao;
        }

        else
        {
            throw new Exception("Formato de Tipo de Opera��o Cotista n�o suportado: " + tipoMovimentoYMF);
        }
        return tipoOperacaoCotista;
    }

    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}