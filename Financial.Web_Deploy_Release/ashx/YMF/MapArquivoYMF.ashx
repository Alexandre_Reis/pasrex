<%@ WebHandler Language="C#" Class="MapArquivoYMF" %>

using System;
using System.Web;
using System.Collections.Generic;
using Bytescout.Spreadsheet;

public class MapArquivoYMF : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromFile(@"c:\temp\layoutYMF.xls");

        Worksheet workSheet = document.Workbook.Worksheets[0];

        const int COLUNA_NOME_ARQUIVO = 0;
        
        const int COLUNA_DATATYPE = 2;
        const int COLUNA_NOME_CAMPO = 1;
        
        //int linha = 1;
        const int LINHA_MAX = 2000;
        List<string> output = new List<String>();
        for (int linha = 1; linha < LINHA_MAX; linha++ )
        {
            
            bool obrigatorio;
            Cell datatypeCell = workSheet.Cell(linha, COLUNA_DATATYPE);
            Cell nomeCampoCell = workSheet.Cell(linha, COLUNA_NOME_CAMPO);
            Cell nomeArquivoCell = workSheet.Cell(linha, COLUNA_NOME_ARQUIVO);

            if (nomeArquivoCell.Value != null)
            {
                output.Add("<br /><br />" + nomeArquivoCell.ValueAsString);
            }
            
            if (datatypeCell.Value == null)
            {
                continue;
            }
            
            if (nomeCampoCell.FontColorIndex == 10)
            {
                obrigatorio = true;
            }
            else if (nomeCampoCell.FontColorIndex == 32767)
            {
                obrigatorio = false;
            }
            else if (nomeCampoCell.FontColorIndex == 8)
            {
                obrigatorio = false;
            }
            else
            {
                throw new Exception("Cor nao suportada: " + nomeCampoCell.FontColorIndex);
            }

            string nomeCampo = "";
            string[] nomeCampoSplit = nomeCampoCell.ValueAsString.ToUpper().Split('_');
            foreach (string nomeCampoPart in nomeCampoSplit)
            {
                nomeCampo += nomeCampoPart[0] + nomeCampoPart.Substring(1).ToLower();
            }

            
            string datatype = datatypeCell.ValueAsString.ToLower();

            string csharpDatatype = "";
            bool isString = false;
            if (datatype.Contains("char"))
            {
                csharpDatatype = "string";
                isString = true;
            }
            else if (datatype.Contains("text"))
            {
                csharpDatatype = "string";
                isString = true;
            }
            else if (datatype.Contains("int"))
            {
                csharpDatatype = "int";
            }
            else if (datatype.Contains("float"))
            {
                csharpDatatype = "decimal";
            }
            else if (datatype.Contains("decimal"))
            {
                csharpDatatype = "decimal";
            }
            else if (datatype.Contains("datatype"))
            {
                //ignore
            }
            else if (datatype.Contains("datetime"))
            {
                csharpDatatype = "DateTime";
            }
            else
            {
                throw new Exception("Datatype nao suportado: " + datatype);
            }

            string nullable = !obrigatorio && !isString ? "?" : "";
            
            string linhaFormatada = "";
            if (csharpDatatype == "DateTime")
            {
                linhaFormatada += "[FieldConverter(typeof(ConvertFlexDate))]<br />";
            }
            
            linhaFormatada += String.Format("public {0}{1} {2};", csharpDatatype, nullable, nomeCampo);
            //check obrigatorio
            output.Add(linhaFormatada);
           
        }       
        
        context.Response.ContentType = "text/html";
        context.Response.Write("Output:<br /><br />" + String.Join("<br />", output.ToArray()));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}