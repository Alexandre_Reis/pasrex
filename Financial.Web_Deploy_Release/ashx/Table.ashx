<%@ WebHandler Language="C#" Class="TableHandler" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data;
using System.Reflection;
using Newtonsoft.Json;
using FinancialDesk.BO.Series;
using FinancialDesk.BO.Tables;
using FinancialDesk.BO;
using Financial.Fundo;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.Security;

public class TableHandler : IHttpHandler
{
    private bool _explodeFundos;
    
    public void ProcessRequest(HttpContext context)
    {
        JsonResponse jsonResponse = new JsonResponse();

        try
        {
            ProcessaRequest(context, jsonResponse);
            jsonResponse.success = true;
        }
        catch (Exception exception)
        {
            jsonResponse.success = false;
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
        
        CustomDateTimeConverter customDateTimeConverter = new CustomDateTimeConverter();
        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
        jsonSerializerSettings.Converters.Add(customDateTimeConverter);
        jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        jsonSerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.XmlIgnoreContractResolver();

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.None, jsonSerializerSettings);


        context.Response.ContentType = "text/html";
        context.Response.Write(json);

    }

    public void ProcessaRequest(HttpContext context, JsonResponse jsonResponse)
    {
        _explodeFundos = false;
        string explodeFundosParam = context.Request["explodeFundos"];
        if (!String.IsNullOrEmpty(explodeFundosParam))
        {
            _explodeFundos = Convert.ToBoolean(context.Request["explodeFundos"]);
        }
                
        DateTime data = ConvertStringToDateTime(context.Request.Params["date"]);
        int? idCliente = Int32.Parse(context.Request.Params["idCliente"]);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

        if (!usuario.AcessaCliente(idCliente.Value))
        {
            throw new Exception("Este usu�rio n�o tem acesso a esta carteira");
        }
        
        short idBenchmark;
        bool hasBenchmark = false;

        CalculoMedida calculoMedida;

        if (short.TryParse(context.Request.Params["idindice"], out idBenchmark))
        {
            hasBenchmark = true;
            calculoMedida = new CalculoMedida(idCliente.Value, idBenchmark, DateTime.Now.Date, data, _explodeFundos);
        }
        else
        {
            calculoMedida = new CalculoMedida(idCliente.Value, DateTime.Now.Date, data);
        }

        string tablesParam = context.Request.Params["tables"];
        string[] tables = tablesParam.Split('|');

        bool isPopup = context.Request.Params["popup"] == "1";
        if (!isPopup)
        {
            if (Array.IndexOf(tables, "RetornoAtivos") >= 0)
            {
                ListaRetornoAtivo listaRetornoAtivo = new ListaRetornoAtivo();
                DataTable tableRetornoAtivos;
                if (_explodeFundos)
                {
                    tableRetornoAtivos = calculoMedida.RetornaTabelaRetornoAtivosExplodidos(data);
                }
                else
                {
                    tableRetornoAtivos = calculoMedida.RetornaTabelaRetornoAtivos(data);
                }
                if (tableRetornoAtivos != null && (tableRetornoAtivos.Rows.Count > 0))
                {
                    jsonResponse.retornoAtivos = listaRetornoAtivo.RetornaListaRetornoAtivo(tableRetornoAtivos);
                }
            }

            if (Array.IndexOf(tables, "CarteiraSintetica") >= 0)
            {
                ListaCarteiraSintetica listaCarteiraSintetica = new ListaCarteiraSintetica();
                DateTime dataInicioPosicaoAtivo = Financial.Util.Calendario.SubtraiDiaUtil(data, 1);
                DataTable tablePosicaoAtivos;

                tablePosicaoAtivos = calculoMedida.RetornaTabelaPosicaoAtivos(dataInicioPosicaoAtivo, data, _explodeFundos);
                
                if (tablePosicaoAtivos != null && (tablePosicaoAtivos.Rows.Count > 0))
                {
                    jsonResponse.carteiraSintetica = listaCarteiraSintetica.RetornaListaCarteiraSintetica(tablePosicaoAtivos);
                }
            }

            if (Array.IndexOf(tables, "ComposicaoFundo") >= 0)
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente.Value);
                if (cliente.TipoControle.Value == (byte)Financial.Investidor.Enums.TipoControleCliente.Completo ||
                    cliente.TipoControle.Value == (byte)Financial.Investidor.Enums.TipoControleCliente.Carteira)
                {
                    int idCarteiraOrigemAplicacao = Int32.Parse(context.Request.Params["idCarteiraOrigemAplicacao"]);
                    Carteira carteiraOrigemAplicacao = new Carteira();
                    carteiraOrigemAplicacao.LoadByPrimaryKey(idCarteiraOrigemAplicacao);

                    ListaComposicaoFundo listaComposicaoFundo = new ListaComposicaoFundo();

                    DateTime dataInicioPosicaoAtivo = Financial.Util.Calendario.SubtraiDiaUtil(data, 1);
                    DataTable tablePosicaoAtivos = calculoMedida.RetornaTabelaComposicaoFundo(carteiraOrigemAplicacao, dataInicioPosicaoAtivo, data);

                    if (tablePosicaoAtivos != null && (tablePosicaoAtivos.Rows.Count > 0))
                    {
                        jsonResponse.composicaoFundo = listaComposicaoFundo.RetornaListaComposicaoFundo(tablePosicaoAtivos);
                    }
                }
            }

            if (Array.IndexOf(tables, "CarteiraOnline") >= 0)
            {
                /*
                ListaCarteiraSintetica listaCarteiraSintetica = new ListaCarteiraSintetica();
                SubReportPosicaoAtivo subReportPosicaoAtivo = reportExtratoCliente.GetSubReportPosicaoAtivo;

                if (subReportPosicaoAtivo.DataSource != null)
                {
                    jsonResponse.carteiraOnline = listaCarteiraSintetica.RetornaListaCarteiraSintetica((System.Data.DataTable)subReportPosicaoAtivo.DataSource);
                }*/

                ListaCarteiraOnline listaCarteiraOnline = new ListaCarteiraOnline();
                jsonResponse.carteiraOnline = listaCarteiraOnline.RetornaListaCarteiraOnline(idCliente.Value);

            }

            if (hasBenchmark)
            {
                TableValuesIndicadores tableValues = new TableValuesIndicadores(idCliente.Value, idBenchmark, data);

                calculoMedida.InitListaRetornoMensal(calculoMedida.DataInicio.Value, data);

                if (Array.IndexOf(tables, "QuadroRetorno") >= 0)
                {
                    jsonResponse.quadroRetorno = calculoMedida.RetornaTabelaRetornoMensal();
                }

                if (Array.IndexOf(tables, "Estatistica1") >= 0 || Array.IndexOf(tables, "Estatistica2") >= 0)
                {
                    CalculoMedida.InfoRiscoRetorno infoRiscoRetorno = calculoMedida.RetornaInfoRiscoRetorno();

                    List<TableValuesIndicadores.Estatistica1> listaEstatistica1 = new List<TableValuesIndicadores.Estatistica1>();
                    TableValuesIndicadores.Estatistica1 estatistica1 = new TableValuesIndicadores.Estatistica1();
                    estatistica1.Descricao = "Meses acima do benchmark";
                    estatistica1.Quantidade = infoRiscoRetorno.MesesAcimaBenchMark;
                    estatistica1.Percentual = infoRiscoRetorno.PercentualAcimaBenchMark * 100;
                    listaEstatistica1.Add(estatistica1);
                    estatistica1 = new TableValuesIndicadores.Estatistica1();
                    estatistica1.Descricao = "Meses abaixo do benchmark";
                    estatistica1.Quantidade = infoRiscoRetorno.MesesAbaixoBenchMark;
                    estatistica1.Percentual = infoRiscoRetorno.PercentualAbaixoBenchMark * 100;
                    listaEstatistica1.Add(estatistica1);
                    jsonResponse.estatistica1 = listaEstatistica1;

                    List<TableValuesIndicadores.Estatistica2> listaEstatistica2 = new List<TableValuesIndicadores.Estatistica2>();
                    TableValuesIndicadores.Estatistica2 estatistica2 = new TableValuesIndicadores.Estatistica2();

                    estatistica2.Descricao = "Maior retorno da carteira";
                    estatistica2.Retorno = infoRiscoRetorno.MaiorRentabilidadeCarteira;
                    estatistica2.Mes = infoRiscoRetorno.MesMaiorRentabilidade;
                    listaEstatistica2.Add(estatistica2);
                    estatistica2 = new TableValuesIndicadores.Estatistica2();
                    estatistica2.Descricao = "Menor retorno da carteira";
                    estatistica2.Retorno = infoRiscoRetorno.MenorRentabilidadeCarteira;
                    estatistica2.Mes = infoRiscoRetorno.MesMenorRentabilidade;
                    listaEstatistica2.Add(estatistica2);
                    jsonResponse.estatistica2 = listaEstatistica2;

                    jsonResponse.indicadores = tableValues.RetornaIndicadores();
                    List<TableValuesIndicadores.RetornoPeriodo> listaRetornoPeriodo = new List<TableValuesIndicadores.RetornoPeriodo>();

                    string[] token_periodos = { "3Meses", "6Meses", "12Meses", "24Meses", "36Meses", "DesdeInicio" };
                    string[] token_periodos_descricao = { "3 meses", "6 meses", "12 meses", "24 meses", "36 meses", "Desde o in�cio" };
                    string[] token_fieldnames = { "rentabBenchMark", "rentabCarteira", "rentabDiferencial" };

                    //int countPeriodo = 0;

                    TableValuesIndicadores.RetornoPeriodo retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[0];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMark3Meses;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteira3Meses;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencial3Meses;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[1];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMark6Meses;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteira6Meses;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencial6Meses;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[2];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMark12Meses;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteira12Meses;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencial12Meses;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[3];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMark24Meses;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteira24Meses;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencial24Meses;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[4];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMark36Meses;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteira36Meses;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencial36Meses;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                    retornoPeriodo.Periodo = token_periodos_descricao[5];
                    retornoPeriodo.RetornoBenchmark = infoRiscoRetorno.rentabBenchMarkDesdeInicio;
                    retornoPeriodo.RetornoCarteira = infoRiscoRetorno.rentabCarteiraDesdeInicio;
                    retornoPeriodo.RetornoDiferencial = infoRiscoRetorno.rentabDiferencialDesdeInicio;
                    listaRetornoPeriodo.Add(retornoPeriodo);

                    /*  Como o codigo deveria ser - por algum motivo PropertyInfo retorna null
                    foreach (string periodo in token_periodos)
                    {
                        TableValuesIndicadores.RetornoPeriodo retornoPeriodo = new TableValuesIndicadores.RetornoPeriodo();
                        string periodoDescricao = token_periodos_descricao[countPeriodo];

                        retornoPeriodo.Periodo = periodoDescricao;

                        object debug3 = subReportRiscoRetorno.infoRentabilidade2.dadosRentabilidade;
                        PropertyInfo[] propertyInfos;
                        propertyInfos = infoRentabilidade2Type.GetProperties(BindingFlags.Public |
                                                                      BindingFlags.Static);
                    
                        string debug1 = token_fieldnames[0] + periodo;
                        PropertyInfo debug2 = infoRentabilidade2Type.GetProperty(debug1);
                    
                        retornoPeriodo.RetornoBenchmark = (decimal)infoRentabilidade2Type.GetProperty(
                            token_fieldnames[0] + periodo).GetValue(subReportRiscoRetorno.infoRentabilidade2, null);

                        retornoPeriodo.RetornoCarteira = (decimal)infoRentabilidade2Type.GetProperty(
                            token_fieldnames[1] + periodo).GetValue(subReportRiscoRetorno.infoRentabilidade2, null);

                        retornoPeriodo.RetornoDiferencial = (decimal)infoRentabilidade2Type.GetProperty(
                            token_fieldnames[2] + periodo).GetValue(subReportRiscoRetorno.infoRentabilidade2, null);

                        listaRetornoPeriodo.Add(retornoPeriodo);
                      
                        countPeriodo++;
                    }*/

                    jsonResponse.retornoPeriodo = listaRetornoPeriodo;

                }
            }
        }
        else
        {
            //POPUP
            string pk = context.Request.Params["pk"];
            TableValuesPosicaoMercado tableValuesPosicaoMercado = new TableValuesPosicaoMercado(idCliente.Value, data);
            TableValuesOperacaoMercado o = new TableValuesOperacaoMercado(idCliente.Value, ConvertStringToDateTime(context.Request.Params["dateBegin"]), ConvertStringToDateTime(context.Request.Params["dateEnd"]));

            EstruturaTipos estruturaTipos = new EstruturaTipos();

            if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.Bolsa].Tipo) >= 0)
            {
                jsonResponse.posicaoBolsaSintetica = tableValuesPosicaoMercado.RetornaPosicaoBolsaSintetica(pk);


                jsonResponse.operacaoBolsaAnalitica = o.RetornaOperacaoBolsaAnalitica(pk);
                jsonResponse.operacaoBolsaSintetica = o.RetornaOperacaoBolsaSintetica(pk);
            }

            if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.Fundos].Tipo) >= 0)
            {
                int pkResgateFundo;
                if (int.TryParse(context.Request.Params["pkResgateFundo"], out pkResgateFundo))
                {
                    //Quando queremos resgate nao queremos as outras tabelas
                    jsonResponse.operacaoResgateFundo = o.RetornaOperacaoResgateFundo(pkResgateFundo);

                }
                else
                {
                    int pkNumeric = Convert.ToInt32(pk);//10000016
                    jsonResponse.posicaoFundoSintetica = tableValuesPosicaoMercado.RetornaPosicaoFundoSintetica(pkNumeric);
                    jsonResponse.posicaoFundoAnalitica = tableValuesPosicaoMercado.RetornaPosicaoFundoAnalitica(pkNumeric);
                }
            }

            /*if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.FuturoBMF].Tipo) >= 0)
            {
                jsonResponse.posicaoFuturoBMFSintetica = tableValuesPosicaoMercado.RetornaPosicaoFuturoBMFSintetica(pk);
                jsonResponse.operacaoBMFAnalitica = o.RetornaOperacaoBMFAnalitica(pk);
            }

            if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.TermoBolsa].Tipo) >= 0)
            {
                jsonResponse.posicaoTermoBolsaSintetica = tableValuesPosicaoMercado.RetornaPosicaoTermoBolsaSintetica(pk);
            }*/

            if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.RendaFixa].Tipo) >= 0)
            {
                int pkNumeric = Convert.ToInt32(pk);

                jsonResponse.posicaoRendaFixaSintetica = tableValuesPosicaoMercado.RetornaPosicaoRendaFixaSintetica(pkNumeric);
                jsonResponse.operacaoRendaFixaAnalitica = o.RetornaOperacaoRendaFixaAnalitica(pkNumeric);
            }

            if (Array.IndexOf(tables, estruturaTipos.listaTiposNomeada[ListaTipoFixa.Liquidacao].Tipo) >= 0)
            {
                jsonResponse.valoresLiquidar = o.RetornaValoresLiquidar();
            }
        }
    }
    
    public DateTime ConvertStringToDateTime(string dataParam)
    {
        string[] dataParamSplit = dataParam.Split('-');
        return new DateTime(Convert.ToInt16(dataParamSplit[0]), Convert.ToInt16(dataParamSplit[1]), Convert.ToInt16(dataParamSplit[2]));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JsonResponse
    {
        public bool success;
        public List<RetornoAtivo> retornoAtivos = new List<RetornoAtivo>();
        public List<CarteiraSintetica> carteiraSintetica = new List<CarteiraSintetica>();
        public List<ComposicaoFundo> composicaoFundo = new List<ComposicaoFundo>();
        public List<CarteiraOnline> carteiraOnline;

        //public List<TableValuesIndicadores.QuadroRetorno> quadroRetorno;
        public DataTable quadroRetorno;
        public List<TableValuesIndicadores.Estatistica1> estatistica1;
        public List<TableValuesIndicadores.Estatistica2> estatistica2;
        public TableValuesIndicadores.Indicadores indicadores;
        public List<TableValuesIndicadores.RetornoPeriodo> retornoPeriodo;
        public TableValuesPosicaoMercado.PosicaoBolsaSintetica posicaoBolsaSintetica;
        public List<TableValuesOperacaoMercado.OperacaoBolsaAnalitica> operacaoBolsaAnalitica;
        public List<TableValuesOperacaoMercado.OperacaoBolsaSintetica> operacaoBolsaSintetica;
        public TableValuesPosicaoMercado.PosicaoFundoSintetica posicaoFundoSintetica;
        public List<TableValuesPosicaoMercado.PosicaoFundoAnalitica> posicaoFundoAnalitica;
        public List<TableValuesOperacaoMercado.OperacaoResgateFundo> operacaoResgateFundo;
        public TableValuesPosicaoMercado.PosicaoFuturoBMFSintetica posicaoFuturoBMFSintetica;
        public List<TableValuesOperacaoMercado.OperacaoBMFAnalitica> operacaoBMFAnalitica;
        public TableValuesPosicaoMercado.PosicaoTermoBolsaSintetica posicaoTermoBolsaSintetica;
        public TableValuesPosicaoMercado.PosicaoRendaFixaSintetica posicaoRendaFixaSintetica;
        public List<TableValuesOperacaoMercado.OperacaoRendaFixaAnalitica> operacaoRendaFixaAnalitica;
        public List<TableValuesOperacaoMercado.ValoresLiquidar> valoresLiquidar;
    }
    public class CustomDateTimeConverter : JsonConverter
    {

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            string text = "";

            if (value is DateTime)
            {
                DateTime dateTime = (DateTime)value;
                text = String.Format("{0:dd/MM/yyyy}", dateTime);
            }
            writer.WriteValue(text);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, JsonSerializer serializer)
        {
            return DateTime.Parse("");
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(DateTime).IsAssignableFrom(objectType);
        }

    }
}