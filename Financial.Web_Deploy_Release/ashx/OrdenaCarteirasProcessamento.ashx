<%@ WebHandler Language="C#" Class="OrdenaCarteirasProcessamento" %>

using System;
using System.Web;
using Financial.Investidor;
using Financial.Investidor.Controller;
using System.Collections.Generic;
using Newtonsoft.Json;

public class OrdenaCarteirasProcessamento : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string[] idsDesordenadosString = HttpUtility.HtmlDecode(context.Request["idClientes"]).Split(',');
        List<Cliente> clientesDesordenados = new List<Cliente>();
        
        foreach(string idDesordenadoString in idsDesordenadosString){
            int idDesordenado = Convert.ToInt32(idDesordenadoString);
            Cliente clienteDesordenado = new Cliente();
            clienteDesordenado.LoadByPrimaryKey(idDesordenado);
            clientesDesordenados.Add(clienteDesordenado);
        }
        
        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
        List<Cliente> clientesOrdenados = controllerInvestidor.OrdenaProcessamentoClientes(clientesDesordenados);

        List<int> idsClientesOrdenados = new List<int>();
        foreach (Cliente clienteOrdenado in clientesOrdenados)
        {
            idsClientesOrdenados.Add(clienteOrdenado.IdCliente.Value);
        }
        
        string json = JsonConvert.SerializeObject(idsClientesOrdenados, Formatting.None);

        context.Response.ContentType = "application/json";
        context.Response.Write(json);
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}