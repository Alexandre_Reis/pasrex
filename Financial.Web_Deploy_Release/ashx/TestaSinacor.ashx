<%@ WebHandler Language="C#" Class="TestaSinacor" %>

using System;
using System.Web;
using Financial.Interfaces.Sinacor;

public class TestaSinacor : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        string output = "";
        bool bloqueado = false;
        int codigoSinacor = Convert.ToInt32(context.Request.Params["codigosinacor"]);
            
        //Tentar encontrar codigo no Sinacor
        TscclibolCollection tscclibolCollection = new TscclibolCollection();
        //
        tscclibolCollection.es.Connection.Name = "Sinacor";
        tscclibolCollection.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
        tscclibolCollection.Query.Where(tscclibolCollection.Query.CdCliente.Equal(codigoSinacor));

        tscclibolCollection.Query.Load();

        if (tscclibolCollection.Count == 0)
        {
            //C�digo n�o encontrado no Sinacor
            output = "C�digo de cliente n�o encontrado no Sinacor: " + codigoSinacor.ToString();
        }
        else
        {
            output = "Clientes encontrados: " + tscclibolCollection.Count;

            decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
            //select dt_bal_patrimonial from corrwin.tscdocs

            TscdocsCollection tscdocsCol = new TscdocsCollection();
            tscdocsCol.es.Connection.Name = "Sinacor";
            tscdocsCol.es.Connection.Schema = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
            //
            tscdocsCol.Query.Where(tscdocsCol.Query.CdCpfcgc == cpfCGC);
            //

            tscdocsCol.Query.Load();

            //
            // Se tem pelo menos 1 endere�o para aquela pessoa no Sinacor
            if (tscdocsCol.Count >= 1)
            {
                Tscdocs tscdocs = tscdocsCol[0];
                if (tscdocs.DtBalPatrimonial != null)
                {
                    if (tscdocs.DtBalPatrimonial.Value.Date > DateTime.Today)
                    {
                        bloqueado = true;
                        output += "\nCliente bloqueado:Sim";
                    }
                }
            }
        }

        output += "\nCliente bloqueado: " + bloqueado;
        output += "\nProcessamento via WS: " + Financial.Util.ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService;
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}