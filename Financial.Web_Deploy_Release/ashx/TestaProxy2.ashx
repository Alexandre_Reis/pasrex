<%@ WebHandler Language="C#" Class="TestaProxy2" %>


using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Financial.Common.Enums;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;
using System.IO.Compression;
using Financial.Util.Enums;
using Financial.Common;
using System.Collections;
using Financial.Util;
using Financial.Util.Enums;
using System.Runtime.InteropServices;
using Financial.Util.Exceptions;
using System.Reflection;
using System.Diagnostics;
using Financial.Util.Properties;
using System.Globalization;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.BMF.Exceptions;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Util.ConfiguracaoSistema;
using System.Drawing;
using System.Text.RegularExpressions;
using FileHelpers;
using System.Net.Mail;
using System.Data;
using System.ComponentModel;
//using Microsoft.Office.Interop.Excel;
using System.Web;
using System.Security.Cryptography;


public class TestaProxy2 : IHttpHandler
{
    public void ProcessRequest(HttpContext context) {

        Uri proxyURI = new Uri("http://webproxy:8080");

  //      string proxyUsername = "username";
//        string proxyPassword = "password";

  //      ICredentials credentials = new NetworkCredential(proxyUsername, proxyPassword);
//        WebProxy proxy = new WebProxy(proxyURI, false, null, credentials);

        WebClient webClient = new WebClient();
//        webClient.Proxy = proxy;
webClient.Proxy = new WebProxy(proxyURI);
       // webClient.DownloadFile("http://www.debentures.com.br/exploreosnd/consultaadados/precificacao/arqs/db140206.exe", @"d:\financial\downloads\db140206.exe");

DateTime data = new DateTime(2014,2,6);
StringBuilder dataString = Utilitario.FormataData(data, PatternData.formato5);
string endereco = "http://www.debentures.com.br/exploreosnd/consultaadados/precificacao/arqs/db" + dataString.ToString() + ".exe";
//bool download = Utilitario.DownloadFile(endereco, @"d:\financial\downloads\db140206.exe");
string debug="";
bool download;
try
{
    download = DownloadFile(endereco, @"c:\inetpub\wwwroot\financial\financial.web\downloads\db140206.exe", out debug);
}
catch (Exception e)
{
    throw new Exception(debug);
}
//webClient.DownloadFile(endereco, @"d:\financial\downloads\db140206.exe");

        context.Response.ContentType = "text/plain";
        //context.Response.Write(webClient.DownloadString("http://whatismyipaddress.com//"));
context.Response.Write(download.ToString());
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public static bool DownloadFile(string endereco, string pathDestino, out string debug)
    {
        debug = "";
        try
        {
            WebClient webClient = new WebClient();
            const string FILE_CONTROLLER_URL = "FileController.ashx";
            string wsProviderURL = ConfigurationManager.AppSettings["WebServicesProviderUrl"];
            if (!string.IsNullOrEmpty(wsProviderURL))
            {
                debug="Estou no webservicesproviderURL";
                wsProviderURL = wsProviderURL + "/" + FILE_CONTROLLER_URL;
                string downloadURLEncoded = HttpUtility.HtmlEncode(endereco);

                string[] filenameSplitted = pathDestino.Split('/');
                string filename = filenameSplitted[filenameSplitted.Length - 1];
                string filenameEncoded = HttpUtility.HtmlEncode(filename);

                string url = String.Format("{0}?url={1}&filename={2}", wsProviderURL, downloadURLEncoded, filenameEncoded);

                webClient.DownloadFile(url, pathDestino);
                FileInfo fileInfo = new FileInfo(pathDestino);
                if (fileInfo.Length == 0)
                {
                    File.Delete(pathDestino);
                    return false;
                }

                return true;
            }
        }
        catch
        {
            return false;
        }

        debug = "vou conectar";
        if (Financial.Util.Utilitario.Internet.IsConnectedToInternet())
        {
            debug = "conectei";
            WebClient webClient = new WebClient();

            //Checa se tem proxy no Windows ou no Web.Config
            string proxyServer = WebProxyInfo.ProxyServer;
            string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];

            if (!String.IsNullOrEmpty(proxyServer) || !String.IsNullOrEmpty(internetProxy))
            {

                IWebProxy proxy = GetWebProxy();
                webClient.Proxy = proxy;
            }
            else
            {
                webClient.Proxy = GlobalProxySelection.GetEmptyWebProxy();
            }
            //  

            debug = "apos proxy";
            try
            {
                webClient.DownloadFile(endereco, pathDestino);
                debug = "download realizado";
            }
            catch (WebException e)
            {
                debug = "erro: " + e.Message;
                throw new Exception(e.Message);
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    StringBuilder msg = new StringBuilder();
                    msg.AppendLine("Endere�o http Inv�lido: (" + endereco + ")");
                    //msg.AppendLine("Status Code: " + ((HttpWebResponse)e.Response).StatusCode);
                    //msg.AppendLine("Status Description: " + ((HttpWebResponse)e.Response).StatusDescription);
                    //msg.AppendLine(e.Message + e.StackTrace);

                    throw new EnderecoHttpInvalido(msg.ToString());
                    //return false;
                }
                else
                {
                    if (e.Status == WebExceptionStatus.NameResolutionFailure)
                    {
                        // Try Again
                        WebClient ClientAux = new WebClient();
                        ClientAux.DownloadFile(endereco, pathDestino);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + e.StackTrace);
                return false;
            }

            return true;
        }
        // N�o est� Conectado Na Internet
        else
        {
            return false;
        }
    }


    public static IWebProxy GetWebProxy()
    {
        IWebProxy proxy = null;

        //Checa se tem proxy no Web.Config
        string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];
        if (!String.IsNullOrEmpty(internetProxy))
        {
            //Validate proxy address
            if (internetProxy.ToLower() == "default")
            {
                proxy = WebRequest.GetSystemWebProxy();
                if (proxy != null)
                {
                    proxy.Credentials = CredentialCache.DefaultCredentials;
                    //proxy.UseDefaultCredentials = true;
                }
                else
                {
                    throw new Exception("N�o existe default proxy configurado");
                }
            }
            else
            {

                Uri proxyURI = new Uri(internetProxy);

                string internetProxyUsername = ConfigurationManager.AppSettings["InternetProxyUsername"];
                if (!String.IsNullOrEmpty(internetProxyUsername))
                {
                    string proxyUsername = ConfigurationManager.AppSettings["InternetProxyUsername"];
                    string proxyPassword = ConfigurationManager.AppSettings["InternetProxyPassword"];
                    ICredentials credentials = new NetworkCredential(proxyUsername, proxyPassword);
                    proxy = new WebProxy(proxyURI, false, null, credentials);
                }
                else
                {
                    proxy = new WebProxy(proxyURI);
                }
            }
        }
        else
        {
            string proxyServer = WebProxyInfo.ProxyServer;
            proxy = new WebProxy(proxyServer, Convert.ToInt32(WebProxyInfo.ProxyPort));
            string proxyLogin = WebProxyInfo.ProxyLogin;

            if (!String.IsNullOrEmpty(proxyLogin))
            {
                string proxyDomain = WebProxyInfo.ProxyDomain;

                if (!String.IsNullOrEmpty(proxyDomain))
                {
                    proxy.Credentials = new NetworkCredential(proxyLogin, WebProxyInfo.ProxyPassword, proxyDomain);
                }
                else
                {
                    proxy.Credentials = new NetworkCredential(proxyLogin, WebProxyInfo.ProxyPassword);
                }
            }
        }

        return proxy;
    }

     
       
}

public static class WebProxyInfo
{
    private static string proxyServer = "";
    private static string proxyPort = "";
    private static string proxyLogin = "";
    private static string proxyPassword = "";
    private static string proxyDomain = "";

    /// <summary>
    /// Servidor Proxy
    /// </summary>
    public static string ProxyServer
    {
        get { return proxyServer; }
        set { proxyServer = value; }
    }

    /// <summary>
    /// Porta do Servidor Proxy
    /// </summary>
    public static string ProxyPort
    {
        get { return proxyPort; }
        set { proxyPort = value; }
    }

    /// <summary>
    /// Login do Proxy
    /// </summary>
    public static string ProxyLogin
    {
        get { return proxyLogin; }
        set { proxyLogin = value; }
    }

    /// <summary>
    /// Senha do Proxy
    /// </summary>
    public static string ProxyPassword
    {
        get { return proxyPassword; }
        set { proxyPassword = value; }
    }

    /// <summary>
    /// Dominio do Proxy
    /// </summary>
    public static string ProxyDomain
    {
        get { return proxyDomain; }
        set { proxyDomain = value; }
    }
}
