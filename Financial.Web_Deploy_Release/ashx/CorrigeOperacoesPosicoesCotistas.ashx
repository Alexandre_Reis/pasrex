<%@ WebHandler Language="C#" Class="CorrigeOperacoesPosicoesCotistas" %>

using System;
using System.Web;
using Financial.CRM;
using System.Collections.Generic;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Fundo.Enums;
using Financial.Tributo.Custom;
using Financial.Investidor;

public class CorrigeOperacoesPosicoesCotistas : IHttpHandler
{

    private class DadosCorrigir
    {
        public int idCarteira;
        public int idCotista;
        public decimal valorAplicacao;
        public DateTime dataOperacaoErrada;
        public DateTime? dataOperacaoCorreta;
        public DateTime dataConversaoCorreta;
        public DateTime dataLiquidacaoCorreta;
        public decimal quantidadeErrada;

        public DadosCorrigir(int idCarteira, int idCotista, decimal valorAplicacao, decimal quantidadeErrada,
      string dataOperacaoErradaString, string dataOperacaoCorretaString, string dataConversaoCorretaString)
        {
            this.idCarteira = idCarteira;
            this.idCotista = idCotista;
            this.valorAplicacao = valorAplicacao;
            this.quantidadeErrada = quantidadeErrada;

            this.dataOperacaoErrada = DateTime.ParseExact(dataOperacaoErradaString, "dd/MM/yyyy", null);
            if (!string.IsNullOrEmpty(dataOperacaoCorretaString))
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);

                this.dataOperacaoCorreta = DateTime.ParseExact(dataOperacaoCorretaString, "dd/MM/yyyy", null);
                this.dataLiquidacaoCorreta = Calendario.AdicionaDiaUtil(this.dataOperacaoCorreta.Value, carteira.DiasLiquidacaoAplicacao.Value); 
                
                if (!Calendario.IsDiaUtil(this.dataOperacaoCorreta.Value))
                {
                    throw new Exception("Data passada pelo Werneck como correta n�o � dia �til:" + this.dataOperacaoCorreta);
                }

                if (string.IsNullOrEmpty(dataConversaoCorretaString))
                {
                    this.dataConversaoCorreta = Calendario.AdicionaDiaUtil(this.dataOperacaoCorreta.Value, carteira.DiasCotizacaoAplicacao.Value);
                }
                else
                {
                    this.dataConversaoCorreta = DateTime.ParseExact(dataConversaoCorretaString, "dd/MM/yyyy", null);
                }

            }
        }
    }

    public void ProcessRequest(HttpContext context)
    {

        List<DadosCorrigir> listaDadosCorrigir = new List<DadosCorrigir>();
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999487, 1000M, 967.98811047M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999519, 60000M, 57221.56895324M, "31/01/2013", "01/02/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999712, 10000M, 9551.5630001M, "22/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999663, 6600M, 6303.07866715M, "25/03/2013", "25/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999451, 1000M, 967.98811047M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999717, 150000M, 143107.20429715M, "19/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999457, 30185.21M, 28807.83922037M, "08/03/2013", "11/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999478, 66000M, 63151.69197161M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999525, 12200M, 11639.3859495M, "19/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999476, 120000M, 114394.4478426M, "18/03/2013", "19/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999547, 34000M, 32572.31691431M, "10/01/2013", "10/01/2013", ""));//Cotista fudido por importacao de posicao no meio do resgate
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999520, 20000M, 19409.38484129M, "10/12/2012", "11/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999460, 39000M, 37751.53630845M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999456, 20000M, 19136.87635503M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999537, 5000M, 4784.21908876M, "22/01/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999537, 5000M, 4780.15512101M, "23/01/2013", "24/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999504, 55000M, 52626.40997634M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999541, 14000M, 13551.83354662M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999459, 96000M, 92979.9030332M, "07/12/2012", "10/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999462, 5000M, 4842.70328298M, "07/12/2012", "10/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999554, 57250M, 54833.20720099M, "21/01/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999563, 20000M, 19160.18642018M, "10/01/2013", "11/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315834, 9999557, 41000M, 39084.76967955M, "18/03/2013", "", ""));
        
        
        
        /*listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999487, 35000M, 34885.87748014M, "08/10/2012", "09/10/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 419515, 1000M, 903.9998286M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999451, 200000M, 179038.16187678M, "08/03/2013", "11/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999452, 1000M, 903.9998286M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999447, 5099.81M, 4622.3112011M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999717, 50000M, 45242.72764098M, "19/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999509, 10000M, 9039.998286020002M, "18/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999472, 22000M, 20576.31643531M, "21/12/2012", "21/12/2012", "26/12/2012"));//FERIADO - testar pra ver se vai converter em 26 com feriado bmf!!!
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999472, 15000M, 13621.71323611M, "24/01/2013", "24/01/2013", "28/01/2013"));//FERIADO - testar pra ver se vai converter em 26 com feriado bmf!!!
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999457, 70430M, 63048.28870491M, "08/03/2013", "11/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999495, 53000M, 49570.21686689M, "21/12/2012", "21/12/2012", "26/12/2012"));//FERIADO - testar pra ver se vai converter em 26 com feriado bmf!!!
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999461, 6000M, 5420.41335801M, "26/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999478, 33000M, 29910.18677878M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999453, 1000M, 903.9998286M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999484, 52500M, 50649.42939559M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999506, 200000M, 179978.28022114M, "31/01/2013", "01/02/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999485, 10000M, 9352.87110696M, "21/12/2012", "21/12/2012", "26/12/2012"));//FERIADO - testar pra ver se vai converter em 26 com feriado bmf!!!
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999713, 40000M, 36442.98267592M, "21/03/2013", "22/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999476, 75000M, 67799.98714512M, "18/03/2013", "19/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999476, 1000M, 903.40222634M, "26/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999482, 30000M, 29149.89406054M, "30/11/2012", "03/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999716, 20000M, 18097.09105639M, "19/03/2013", "20/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999507, 5000M, 4524.2727641M, "19/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999448, 1000M, 903.40222634M, "26/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999460, 30000M, 28942.53108319M, "12/12/2012", "13/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999460, 28000M, 25065.34266275M, "08/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999460, 11000M, 9857.11051942M, "13/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999460, 10000M, 9039.998286020002M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999460, 5000M, 4517.01113168M, "26/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999467, 43000M, 40217.34575993M, "21/12/2012", "21/12/2012", "26/12/2012"));//FERIADO - testar pra ver se vai converter em 26 com feriado bmf!!!
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999467, 1000M, 903.9998286M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999500, 270000M, 248660.53248148M, "04/01/2013", "07/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999456, 10000M, 9063.69296327M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999435, 19700M, 17935.62732192M, "16/01/2013", "17/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999479, 2500M, 2259.9995715M, "18/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999504, 50000M, 45318.46481634M, "22/01/2013", "23/01/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999480, 2000M, 1806.80445267M, "26/03/2013", "", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999459, 64000M, 61713.35600258M, "07/12/2012", "10/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999462, 5000M, 4821.3559377M, "07/12/2012", "10/12/2012", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999475, 10000M, 8951.90809384M, "08/03/2013", "11/03/2013", ""));
        listaDadosCorrigir.Add(new DadosCorrigir(315826, 9999714, 10000M, 9127.66303677M, "22/03/2013", "", ""));*/


        foreach (DadosCorrigir dadosCorrigir in listaDadosCorrigir)
        {

            //Encontrar operacao passaando todos os dados disponiveis
            OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();
            operacoes.Query.Where(operacoes.Query.IdCarteira.Equal(dadosCorrigir.idCarteira),
                operacoes.Query.IdCotista.Equal(dadosCorrigir.idCotista),
                operacoes.Query.ValorBruto.Equal(dadosCorrigir.valorAplicacao),
                operacoes.Query.Quantidade.Equal(dadosCorrigir.quantidadeErrada),
                operacoes.Query.DataOperacao.Equal(dadosCorrigir.dataOperacaoErrada));

            operacoes.Load(operacoes.Query);
            if (operacoes.Count == 0)
            {
                throw new Exception("n�o foi poss�vel encontrar a opera��o com os par�metros passados: ser� que houve resgate ????? Se o problema for a quantidade: cotista:" + dadosCorrigir.idCotista + " data: " + dadosCorrigir.dataOperacaoErrada + " valorAplic: " + dadosCorrigir.valorAplicacao);
            }
            else if (operacoes.Count > 1)
            {
                if (dadosCorrigir.idCotista == 9999460)
                {
                    //Esse cotista realmente tem 2 opera��es identicas -- ignorar
                }
                else
                {
                    throw new Exception("Encontramos mais de uma opera��o nessa data  cotista:" + dadosCorrigir.idCotista + " data: " + dadosCorrigir.dataOperacaoErrada + " valorAplic: " + dadosCorrigir.valorAplicacao);
                }
            }

            OperacaoCotista operacao = operacoes[0];



            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(dadosCorrigir.idCarteira);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(carteira.IdCarteira.Value);

            if (cliente.DataDia.Value != new DateTime(2013, 03, 28))
            {
                throw new Exception("Faltou retroagir carteira!");
            }


            //Se for exclusao remover tudo e prosseguir
            if (!dadosCorrigir.dataOperacaoCorreta.HasValue)
            {

                PosicaoCotista posicaoCotistaDeletar = new PosicaoCotista();
                posicaoCotistaDeletar.Query.Where(posicaoCotistaDeletar.Query.IdOperacao.Equal(operacao.IdOperacao));
                posicaoCotistaDeletar.Load(posicaoCotistaDeletar.Query);

                PosicaoCotistaAberturaCollection posicoesCotistaDeletarAbertura = new PosicaoCotistaAberturaCollection();
                posicoesCotistaDeletarAbertura.Query.Where(posicoesCotistaDeletarAbertura.Query.IdPosicao.Equal(posicaoCotistaDeletar.IdPosicao.Value));
                posicoesCotistaDeletarAbertura.Load(posicoesCotistaDeletarAbertura.Query);
                if (posicoesCotistaDeletarAbertura.Count > 1)
                {
                    throw new Exception("Estou apgando coisa demais!");
                }

                PosicaoCotistaHistoricoCollection posicoesCotistaDeletarHistorico = new PosicaoCotistaHistoricoCollection();
                posicoesCotistaDeletarHistorico.Query.Where(posicoesCotistaDeletarHistorico.Query.IdPosicao.Equal(posicaoCotistaDeletar.IdPosicao.Value));
                posicoesCotistaDeletarHistorico.Load(posicoesCotistaDeletarHistorico.Query);
                if (posicoesCotistaDeletarHistorico.Count > 1)
                {
                    throw new Exception("Estou apgando coisa demais!");
                }

                operacao.MarkAsDeleted();
                posicaoCotistaDeletar.MarkAsDeleted();
                posicoesCotistaDeletarAbertura.MarkAllAsDeleted();
                posicoesCotistaDeletarHistorico.MarkAllAsDeleted();

                operacoes.Save();
                posicaoCotistaDeletar.Save();
                posicoesCotistaDeletarAbertura.Save();
                posicoesCotistaDeletarHistorico.Save();
                continue;
            }


            string truncaQuantidade = carteira.TruncaQuantidade;
            byte tipoTributacao = carteira.TipoTributacao.Value;
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;

            int tipoCota = carteira.TipoCota.Value;
            HistoricoCota historicoCota = new HistoricoCota();
            decimal cotaAplicacao;

            if (!historicoCota.BuscaValorCota(dadosCorrigir.idCarteira, dadosCorrigir.dataConversaoCorreta))
            {
                cotaAplicacao = carteira.CotaInicial.Value;
            }
            else
            {
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaAplicacao = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    if (!historicoCota.CotaFechamento.HasValue)
                    {
                        cotaAplicacao = carteira.CotaInicial.Value;
                    }
                    else
                    {
                        cotaAplicacao = historicoCota.CotaFechamento.Value;
                    }
                }
            }

            //throw new Exception("Descorbrir como eh o preenchimento dos campso de uma aplicacao e refazer aqui: � cota fechamento mesmo ? Ver arredondamento e calculo da qtde corretos !");

            operacao.CotaOperacao = cotaAplicacao;
            decimal quantidade;

            if (truncaQuantidade == "S")
            {
                quantidade = Utilitario.Truncate(operacao.ValorBruto.Value / cotaAplicacao, casasDecimaisQuantidade);
            }
            else
            {
                quantidade = Math.Round(operacao.ValorBruto.Value / cotaAplicacao, casasDecimaisQuantidade);
            }
            operacao.Quantidade = quantidade;

            operacao.DataOperacao = dadosCorrigir.dataOperacaoCorreta;
            operacao.DataAgendamento = dadosCorrigir.dataOperacaoCorreta;
            operacao.DataConversao = dadosCorrigir.dataConversaoCorreta;
            operacao.DataLiquidacao = dadosCorrigir.dataLiquidacaoCorreta;

            //Ajustar posi��o
            DateTime dataUltimaCobrancaIR = dadosCorrigir.dataConversaoCorreta;
            if (tipoTributacao == (byte)TipoTributacaoFundo.CurtoPrazo ||
                tipoTributacao == (byte)TipoTributacaoFundo.LongoPrazo)
            {
                dataUltimaCobrancaIR = CalculoTributo.RetornaDataComeCotasAnterior(dadosCorrigir.dataConversaoCorreta);
            }

            //throw new Exception("voltar o fundo !");
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Query.Where(posicaoCotista.Query.IdOperacao.Equal(operacao.IdOperacao));
            posicaoCotista.Load(posicaoCotista.Query);

            posicaoCotista.ValorAplicacao = operacao.ValorBruto;
            posicaoCotista.DataAplicacao = operacao.DataOperacao;
            posicaoCotista.DataConversao = operacao.DataConversao;
            posicaoCotista.CotaAplicacao = operacao.CotaOperacao;

            posicaoCotista.QuantidadeInicial = operacao.Quantidade;
            posicaoCotista.Quantidade = operacao.Quantidade;
            posicaoCotista.QuantidadeAntesCortes = operacao.Quantidade;
            posicaoCotista.DataUltimaCobrancaIR = operacao.DataConversao;


            //Refazer pra posicaoaobertura
            PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
            posicaoCotistaAbertura.LoadByPrimaryKey(posicaoCotista.IdPosicao.Value, cliente.DataDia.Value);

            posicaoCotistaAbertura.ValorAplicacao = operacao.ValorBruto;
            posicaoCotistaAbertura.DataAplicacao = operacao.DataOperacao;
            posicaoCotistaAbertura.DataConversao = operacao.DataConversao;
            posicaoCotistaAbertura.CotaAplicacao = operacao.CotaOperacao;

            posicaoCotistaAbertura.QuantidadeInicial = operacao.Quantidade;
            posicaoCotistaAbertura.Quantidade = operacao.Quantidade;
            posicaoCotistaAbertura.QuantidadeAntesCortes = operacao.Quantidade;
            posicaoCotistaAbertura.DataUltimaCobrancaIR = operacao.DataConversao;


            //Valores que devem ser recomputados apos processamento: CHECAR !!!
            /*posicaoCotista.ValorBruto = 0;//DEIXAR CALCULAR NO PROCESSAMENTO!
            posicaoCotista.ValorLiquido = 0;//DEIXAR CALCULAR NO PROCESSAMENTO!
            posicaoCotista.ValorPerformance = 0;
            posicaoCotista.ValorRendimento = 0;*/
            //posicaoCotista.DataUltimoCortePfee = ???

            operacoes.Save();
            posicaoCotista.Save();
            posicaoCotistaAbertura.Save();

        }

        context.Response.ContentType = "text/plain";
        context.Response.Write("Cotistas alterados com sucesso");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}