<%@ WebHandler Language="C#" Class="ReportPricingDiarioTesteError" %>

using System;
using System.Web;

public class ReportPricingDiarioTesteError : IHttpHandler {
   
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string param = context.Request.Params["Name"];
        context.Response.Write("Hello, " + param);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}