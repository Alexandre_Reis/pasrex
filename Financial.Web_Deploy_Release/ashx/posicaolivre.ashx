<%@ WebHandler Language="C#" Class="posicaolivre" %>

using System;
using System.Web;
using Financial.InvestidorCotista;

public class posicaolivre : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        PosicaoCotista posicaoLivre = new PosicaoCotista().RetornaPosicaoLivre(175633, 55676, null, null);
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(posicaoLivre.ValorLiquido.Value);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}