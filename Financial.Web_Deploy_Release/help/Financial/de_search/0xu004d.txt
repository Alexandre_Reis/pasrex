MADRUGADA; 5;
MAIO; 50;
MAIOR; 30,2;
MAIORES; 58,7,53,21,6,2;
MAIORIA; 57,44,43,7;
MAIS; 58,56,7,2,69,65,47,70,64,63,51,50,44,37,34,13,6,4,3,71,66,61,55,53,52,30,28,22,20,19,18,17,15,14,12,9,5;
MAIÚSCULAS; 68;
MANDATÓRIO; 3;
MANTENDO-SE; 68;
MANTER; 52,7;
MANTIDA; 4;
MANTIDOS; 67;
MANTÉM; 62,3;
MANUAIS; 57,62,53,44,37;
MANUAL; 57,2,53,52,46,41,35,31,1;
MANUALMENTE; 5,2,46,41,32,29;
MANUSEADO; 42;
MAPA; 64,65;
MARCADA; 54,71,68,62,56;
MARCADAS; 70,11;
MARCADO; 57,45,40,71,69,62,56,55,20,4;
MARCANDO; 3;
MARCAR; 56,55,6,70,57,32,30,20,17;
MARGEM; 42;
MARQUE; 52;
MAS; 52,69,64,4,55,50,48,45,42,28,27,21,16,3,2;
MECÂNICA; 16;
MEDIDA; 66;
MEIO; 67;
MELHOR; 7,55,4;
MEMÓRIA; 34;
MEMÓRIAS; 29;
MENCIONADAS; 58;
MENCIONADO; 45,32;
MENCIONAR; 5,64,45;
MENOR; 55,50,24;
MENOS; 51,28,24,22,14;
MENSAGEM; 65,66,36,27,11,10,3;
MENSAL; 65,25,23;
MENTE; 44;
MENU; 69,64,57,54,32,3;
MERC; 27;
MERCADO; 56,21,4,28,27,54,51,44,35,32,2,53,46,41,37,34,30,29,16,5;
MERCADOS; 52,37,5,50,44,30,2,43,28,64,42,38,34,32,23,56,51,49,48,47,39,33,31;
MESES; 25,24,23;
MESMA; 2,51,47,62,59,57,54,50,49,45,44,40,36,34,29,25,22,21,5,4,3;
MESMAS; 69;
MESMO; 3,54,68,56,52,50,47,34,22,17,7,4,69,67,65,64,61,55,45,44,40,37,30,18,16,13,9,2;
MESMOS; 69;
METODOLOGIA; 24,34;
METODOLOGIAS; 24;
MIDDLE-OFFICE; 51;
MIGRADA; 58;
MILHÃO; 54,23;
MILHÕES; 54;
MINUTOS; 68;
MODELO; 17,55,57,50,47,44,37,32,31,8,6;
MODELOS; 17;
MODO; 21,62,64,61,53,48,42,35,30,23,20,7,6;
MOEDA; 4,14,30,28,27,7,26;
MOEDAS; 4;
MOMENTO; 17,51,2,69,65,61,56,52,48,47,42,25,24,23,22,21,4;
MONTADA; 66;
MONTADAS; 17;
MONTADO; 66;
MONTANTE; 54;
MOSTRA; 57,64,62,11,10,5;
MOSTRADA; 51;
MOSTRADAS; 14;
MOSTRADO; 24,49,6,3;
MOSTRADOS; 51,20;
MOSTRANDO; 48;
MOUSE; 65;
MOVIMENTAR; 21;
MOVIMENTAÇÃO; 65,7;
MOVIMENTAÇÕES; 58,64,55;
MOVIMENTO; 21,64,65,36,7,2;
MOVIMENTOS; 58,21;
MTA; 52;
MTM; 2,30,32;
MUDA; 2;
MUDANDO; 11;
MUDANÇA; 62,22,65,21,17;
MUDANÇAS; 58,52;
MUDARÁ; 71,51;
MUDOU; 3;
MUITAS; 66,64,62,12;
MUITO; 66,52,32,7;
MULTA; 53,2;
MULTIPLICA; 4;
MULTIPLICADO; 52;
MULTIPLICADOR; 28;
MÁQUINA; 63;
MÁQUINAS; 68;
MÁXIMA; 56,12,5;
MÁXIMO; 23,56,17,57,5,2;
MÂE; 22,7;
MÃE; 22,7,57;
MÃES; 22;
MÉDIA; 17;
MÉDIO; 7,52;
MÉTODO; 47,21,17;
MÊS; 65,64,25,24,7;
MÍN; 68;
MÍNIMA; 56,16;
MÍNIMO; 17,68,23,56,2;
MÓDULO; 64,51,56,69,7,57,53,27,17,2;
MÓDULOS; 70;
