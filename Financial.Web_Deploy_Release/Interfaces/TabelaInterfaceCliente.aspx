﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TabelaInterfaceCliente, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCliente(values) {
        btnEditCodigo.SetValue(values);                    
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
        btnEditCodigo.Focus();    
    }
    </script>
    
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');        
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                 
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Interface de Cliente"></asp:Label>
    </div>
           
    <div id="mainContent">
        
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>                          
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    DataSourceID="EsDSTabelaInterfaceCliente"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    KeyFieldName="CompositeKey"
                    >
                                                                                                                                                           
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="13%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                                        
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="TipoInterface" Visible="False" />
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataColumn FieldName="NomeCliente" Caption="Cliente" VisibleIndex="2" Width="30%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />

                    <dxwgv:GridViewDataComboBoxColumn Caption="Tipo de Interface" FieldName="TipoInterface" VisibleIndex="3" Width="40%" ExportWidth="350">
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox EncodeHtml="false">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Fundo - Não Importa'>Fundo - Não Importa</div>" />
                                <dxe:ListEditItem Value="2" Text="<div title='Fundo - Financial Posição'>Fundo - Financial Full</div>" />
                                <dxe:ListEditItem Value="101" Text="<div title='Bolsa - Não Importa'>Bolsa - Não Importa</div>" />
                                <dxe:ListEditItem Value="102" Text="<div title='Bolsa - Posição Inicial Ações/Opções/Termo'>Bolsa - Posição Inicial Ações/Opções/Termo</div>" />
                                <dxe:ListEditItem Value="103" Text="<div title='Bolsa - Posição Inicial BTC'>Posição Inicial BTC</div>" />
                                <dxe:ListEditItem Value="110" Text="<div title='Bolsa - Sinacor Operações Ações'>Bolsa - Sinacor Operações Ações</div>" />
                                <dxe:ListEditItem Value="111" Text="<div title='Bolsa - Sinacor Operações FII'>Bolsa - Sinacor Operações FII</div>" />
                                <dxe:ListEditItem Value="201" Text="<div title='BMF - Não Importa'>BMF - Não Importa</div>" />
                                <dxe:ListEditItem Value="202" Text="<div title='BMF - Posição Inicial'>BMF - Posição Inicial</div>" />
                                <dxe:ListEditItem Value="301" Text="<div title='Renda Fixa - Não Importa'>Renda Fixa - Não Importa</div>" />                                
                                <dxe:ListEditItem Value="302" Text="<div title='Renda Fixa - Virtual Operação'>Renda Fixa - Virtual Operação</div>" />
                                <dxe:ListEditItem Value="303" Text="<div title='Renda Fixa - Virtual Full'>Renda Fixa - Virtual Full</div>" />
                                <dxe:ListEditItem Value="310" Text="<div title='Renda Fixa - Sinacor'>Renda Fixa - Sinacor</div>" />
                                <dxe:ListEditItem Value="501" Text="<div title='Conta Corrente - Não Importa'>Conta Corrente - Não Importa</div>" />                                
                                <dxe:ListEditItem Value="502" Text="<div title='Conta Corrente - Sinacor Full'>Conta Corrente - Sinacor Full</div>" />  
                                <dxe:ListEditItem Value="601" Text="<div title='Extração Cyrnel Hub'>Extração Cyrnel Hub</div>" />                                              
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>

                </Columns>
                
                <Templates>
                <EditForm>                    
                    <div class="editForm">    
                            
                        <table border="0">
                            <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                            </td>
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                                            EnableClientSideAPI="True"
                                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>        
                                <ClientSideEvents                        
                                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                                />                            
                                </dxe:ASPxSpinEdit>                
                            </td>                          
                            
                            <td colspan="2" width="300">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>       
                            </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Tipo Interface "> </asp:Label>
                                </td>
                                <td colspan="4">
                                    <dxe:ASPxComboBox ID="dropTipoInterface" runat="server" ClientInstanceName="dropTipoInterface"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList" Text='<%#Eval("TipoInterface")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="" Text="" />
                                            <dxe:ListEditItem Value="1" Text="Fundo - Não Importa" />
                                            <dxe:ListEditItem Value="2" Text="Fundo - Financial Full" />
                                            <dxe:ListEditItem Value="101" Text="Bolsa - Não Importa" />
                                            <dxe:ListEditItem Value="102" Text="Bolsa - Posição Inicial Ações/Opções/Termo" />
                                            <dxe:ListEditItem Value="103" Text="Bolsa - Posição Inicial BTC" />
                                            <dxe:ListEditItem Value="110" Text="Bolsa - Sinacor Operações Ações" />
                                            <dxe:ListEditItem Value="111" Text="Bolsa - Sinacor Operações FII" />
                                            <dxe:ListEditItem Value="201" Text="BMF - Não Importa" />
                                            <dxe:ListEditItem Value="202" Text="BMF - Posição Inicial" />
                                            <dxe:ListEditItem Value="301" Text="Renda Fixa - Não Importa" />
                                            <dxe:ListEditItem Value="302" Text="Renda Fixa - Virtual Operação" />
                                            <dxe:ListEditItem Value="303" Text="Renda Fixa - Virtual Full" />
                                            <dxe:ListEditItem Value="310" Text="Renda Fixa - Sinacor" />
                                            <dxe:ListEditItem Value="501" Text="Conta Corrente - Não Importa" />
                                            <dxe:ListEditItem Value="502" Text="Conta Corrente - Sinacor Full" />          
                                            <dxe:ListEditItem Value="601" Text="Extração Cyrnel Hub" />                                   
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>    
                                                                                    
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                            OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                            <asp:Literal ID="Literal3" runat="server" Text="OK+" /><div>
                            </div>
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                            <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                            </div>
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                            <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                        
                    </div>                                              
                </EditForm>
                
                 <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                 </StatusBar> 
                    
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                                
            </dxwgv:ASPxGridView>            
            </div>
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" LeftMargin = "30" RightMargin = "30"/>
        
    <cc1:esDataSource ID="EsDSTabelaInterfaceCliente" runat="server" OnesSelect="EsDSTabelaInterfaceCliente_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />                    
    </form>
</body>
</html>