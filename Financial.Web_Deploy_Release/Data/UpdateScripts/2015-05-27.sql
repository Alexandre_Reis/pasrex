﻿declare @data_versao char(10)
set @data_versao = '2015-05-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)



IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
 ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
 exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END
GO


IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
 alter table PrazoMedio add IdPosicao int NULL;
END 
GO




BEGIN TRANSACTION DeParaTransaction

BEGIN
	
	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DePara') = 0	
	BEGIN
		CREATE TABLE [dbo].[DePara](
			[IdDePara] [int] IDENTITY(1,1) NOT NULL,
			[IdTipoDePara] [int] NOT NULL,
			[CodigoInterno] [varchar](100) NOT NULL,
			[CodigoExterno] [varchar](100) NOT NULL,
			[DescricaoOrigem] [varchar](200) NULL,
		PRIMARY KEY CLUSTERED 
		(
			[IdDePara] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		UNIQUE NONCLUSTERED 
		(
			[IdTipoDePara] ASC,
			[CodigoInterno] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END


	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoDePara') = 0	
	BEGIN
		CREATE TABLE [dbo].[TipoDePara](
			[IdTipoDePara] [int] IDENTITY(1,1) NOT NULL,
			[TipoCampo] [int] NOT NULL,
			[Descricao] [varchar](100) NOT NULL,
			[Observacao] [varchar](255) NULL,
			[EnumTipoDePara] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[IdTipoDePara] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END



	
	

	BEGIN
		set identity_insert TipoDePara on
		set identity_insert tipodepara on 
		delete depara where IdTipoDePara < 0	
		delete TipoDePara where IdTipoDePara < 0
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-2, 1,	'SituacaoLancamentoLiquidacao','Tabela Liquidacao',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-3, 1,	'FonteLancamentoLiquidacao','Tabela Liquidacao',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-4, 1,	'Origem MT_CAIXA','Tabela Liquidacao',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-5, 1,	'Tipo MT_CAIXA','Tabela Liquidacao',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-6, 1,	'Tipo Operacao Fundo','Tabela OperacaoFundo',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-7, 1,	'TipoTributacaoFundo','Tabela Carteira',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-8, 1,	'TipoCarteiraFundo','Tabela Carteira',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-9, 1,	'TipoOperacaoTitulo','Tabela OperacaoRendaFixa',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-10,1,	'PontaSwap','Tabela OperacaoSwap',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-11,1,	'TipoMercadoAtivoBolsa','Tabela AtivoBolsa',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-12,1,	'PontaEmprestimoBolsa','Tabela  PosicaoEmprestimoBolsaHistorico',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-14,1,	'TipoApropriacaoSwap',	'Tipo de curva usada (exponencial / Linear)',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-15,1,	'ContagemDiasSwap',	'Dias úteis ou corridos',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-16,1,	'TipoAtivoAuxiliar',	'Tabela de Prazo Médio',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-17,1,	'TipoOperacaoCotista',	'Tipo de Operações do Cotista',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-18,1,	'TipoOperacaoCotistaNota',	'Tipo de Operações do Cotista resgate por nota',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-19,1,	'TipoResgateCotista',	'Tipo de Resgate de Cotista',	NULL)
		insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-20,1,	'TipoMercadoAtivoBMF','Tabela AtivoBMF',	NULL)
		set identity_insert TipoDePara off
	END

	BEGIN
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,1,1,'Normal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,2,2,'Pendente')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,3,3,'Compensacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,1,1,'Interno')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,2,2,'Manual')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,3,3,'Sinacor')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,4,4,'ArquivoCMDF')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,0,0,'Bolsa-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1,1,'Bolsa-CompraAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2,2,'Bolsa-VendaAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3,3,'Bolsa-CompraOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,4,4,'Bolsa-VendaOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,5,5,'Bolsa-ExercicioCompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,6,6,'Bolsa-ExercicioVenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,7,7,'Bolsa-CompraTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,8,8,'Bolsa-VendaTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,9,9,'Bolsa-AntecipacaoTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,10,10,'Bolsa-EmprestimoDoado')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,11,11,'Bolsa-EmprestimoTomado')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,12,12,'Bolsa-AntecipacaoEmprestimo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,13,13,'Bolsa-AjusteOperacaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,14,14,'Bolsa-AjustePosicaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,15,15,'Bolsa-LiquidacaoFinalFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,16,16,'Bolsa-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,17,17,'Bolsa-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,18,18,'Bolsa-Dividendo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,19,19,'Bolsa-JurosCapital')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,20,20,'Bolsa-Rendimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,21,21,'Bolsa-RestituicaoCapital')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,22,22,'Bolsa-CreditoFracoesAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,23,23,'Bolsa-IRProventos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,24,24,'Bolsa-OutrosProventos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,25,25,'Bolsa-Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,40,40,'Bolsa-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,100,100,'Bolsa-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,200,200,'BMF-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,201,201,'BMF-CompraVista')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,202,202,'BMF-VendaVista')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,203,203,'BMF-CompraOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,204,204,'BMF-VendaOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,205,205,'BMF-ExercicioCompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,206,206,'BMF-ExercicioVenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,220,220,'BMF-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,221,221,'BMF-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,222,222,'BMF-Emolumento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,223,223,'BMF-Registro')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,224,224,'BMF-OutrosCustos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,250,250,'BMF-AjustePosicao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,251,251,'BMF-AjusteOperacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,260,260,'BMF-TaxaPermanencia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,300,300,'BMF-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,500,500,'RendaFixa-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,501,501,'RendaFixa-CompraFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,502,502,'RendaFixa-VendaFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,503,503,'RendaFixa-CompraRevenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,504,504,'RendaFixa-VendaRecompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,505,505,'RendaFixa-NetOperacaoCasada')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,510,510,'RendaFixa-Vencimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,511,511,'RendaFixa-Revenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,512,512,'RendaFixa-Recompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,520,520,'RendaFixa-Juros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,521,521,'RendaFixa-Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,530,530,'RendaFixa-IR')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,531,531,'RendaFixa-IOF')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,540,540,'RendaFixa-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,550,550,'RendaFixa-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,600,600,'RendaFixa-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,701,701,'Swap-LiquidacaoVencimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,702,702,'Swap-LiquidacaoAntecipacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,703,703,'Swap-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,801,801,'Provisao-TaxaAdministracao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,802,802,'Provisao-PagtoTaxaAdministracao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,810,810,'Provisao-TaxaGestao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,811,811,'Provisao-PagtoTaxaGestao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,820,820,'Provisao-TaxaPerformance')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,821,821,'Provisao-PagtoTaxaPerformance')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,830,830,'Provisao-CPMF')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,840,840,'Provisao-TaxaFiscalizacaoCVM')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,841,841,'Provisao-PagtoTaxaFiscalizacaoCVM')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,890,890,'Provisao-ProvisaoOutros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,891,891,'Provisao-PagtoProvisaoOutros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,851,851,'Provisao-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,852,852,'Provisao-PagtoTaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1000,1000,'Fundo-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1001,1001,'Fundo-Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1002,1002,'Fundo-Resgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1003,1003,'Fundo-IRResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1004,1004,'Fundo-IOFResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1005,1005,'Fundo-PfeeResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1006,1006,'Fundo-ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1010,1010,'Fundo-AplicacaoConverter')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1020,1020,'Fundo-TransferenciaSaldo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1100,1100,'Cotista-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1101,1101,'Cotista-Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1102,1102,'Cotista-Resgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1103,1103,'Cotista-IRResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1104,1104,'Cotista-IOFResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1105,1105,'Cotista-PfeeResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1106,1106,'Cotista-ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1110,1110,'Cotista-AplicacaoConverter')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2000,2000,'IR-IRFonteOperacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2001,2001,'IR-IRFonteDayTrade')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2002,2002,'IR-IRRendaVariavel')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3000,3000,'Margem-ChamadaMargem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3001,3001,'Margem-DevolucaoMargem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,5000,5000,'-AjusteCompensacaoCota')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,100000,100000,'-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,0,'RV','Bolsa-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1,'RV','Bolsa-CompraAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2,'RV','Bolsa-VendaAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3,'RV','Bolsa-CompraOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,4,'RV','Bolsa-VendaOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,5,'RV','Bolsa-ExercicioCompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,6,'RV','Bolsa-ExercicioVenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,7,'RV','Bolsa-CompraTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,8,'RV','Bolsa-VendaTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,9,'RV','Bolsa-AntecipacaoTermo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,10,'RV','Bolsa-EmprestimoDoado')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,11,'RV','Bolsa-EmprestimoTomado')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,12,'RV','Bolsa-AntecipacaoEmprestimo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,13,'RV','Bolsa-AjusteOperacaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,14,'RV','Bolsa-AjustePosicaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,15,'RV','Bolsa-LiquidacaoFinalFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,16,'RV','Bolsa-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,17,'RV','Bolsa-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,18,'RV','Bolsa-Dividendo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,19,'RV','Bolsa-JurosCapital')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,20,'RV','Bolsa-Rendimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,21,'RV','Bolsa-RestituicaoCapital')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,22,'RV','Bolsa-CreditoFracoesAcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,23,'RV','Bolsa-IRProventos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,24,'RV','Bolsa-OutrosProventos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,25,'RV','Bolsa-Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,40,'RV','Bolsa-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,100,'RV','Bolsa-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,200,'FU','BMF-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,201,'FU','BMF-CompraVista')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,202,'FU','BMF-VendaVista')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,203,'FU','BMF-CompraOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,204,'FU','BMF-VendaOpcoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,205,'FU','BMF-ExercicioCompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,206,'FU','BMF-ExercicioVenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,220,'FU','BMF-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,221,'FU','BMF-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,222,'FU','BMF-Emolumento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,223,'FU','BMF-Registro')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,224,'FU','BMF-OutrosCustos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,250,'FU','BMF-AjustePosicao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,251,'FU','BMF-AjusteOperacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,260,'FU','BMF-TaxaPermanencia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,300,'FU','BMF-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,500,'RF','RendaFixa-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,501,'RF','RendaFixa-CompraFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,502,'RF','RendaFixa-VendaFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,503,'RF','RendaFixa-CompraRevenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,504,'RF','RendaFixa-VendaRecompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,505,'RF','RendaFixa-NetOperacaoCasada')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,510,'RF','RendaFixa-Vencimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,511,'RF','RendaFixa-Revenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,512,'RF','RendaFixa-Recompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,520,'RF','RendaFixa-Juros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,521,'RF','RendaFixa-Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,530,'RF','RendaFixa-IR')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,531,'RF','RendaFixa-IOF')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,540,'RF','RendaFixa-Corretagem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,550,'RF','RendaFixa-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,600,'RF','RendaFixa-Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,701,'SW','Swap-LiquidacaoVencimento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,702,'SW','Swap-LiquidacaoAntecipacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,703,'SW','Swap-DespesasTaxas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,801,'CL','Provisao-TaxaAdministracao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,802,'CL','Provisao-PagtoTaxaAdministracao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,810,'CL','Provisao-TaxaGestao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,811,'CL','Provisao-PagtoTaxaGestao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,820,'CL','Provisao-TaxaPerformance')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,821,'CL','Provisao-PagtoTaxaPerformance')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,830,'CL','Provisao-CPMF')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,840,'CL','Provisao-TaxaFiscalizacaoCVM')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,841,'CL','Provisao-PagtoTaxaFiscalizacaoCVM')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,890,'CL','Provisao-ProvisaoOutros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,891,'CL','Provisao-PagtoProvisaoOutros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,851,'CL','Provisao-TaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,852,'CL','Provisao-PagtoTaxaCustodia')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1000,'FI','Fundo-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1001,'FI','Fundo-Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1002,'FI','Fundo-Resgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1003,'FI','Fundo-IRResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1004,'FI','Fundo-IOFResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1005,'FI','Fundo-PfeeResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1006,'FI','Fundo-ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1010,'FI','Fundo-AplicacaoConverter')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1020,'FI','Fundo-TransferenciaSaldo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1100,'COT','Cotista-None')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1101,'COT','Cotista-Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1102,'COT','Cotista-Resgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1103,'COT','Cotista-IRResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1104,'COT','Cotista-IOFResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1105,'COT','Cotista-PfeeResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1106,'COT','Cotista-ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1110,'COT','Cotista-AplicacaoConverter')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2000,'CL','IR-IRFonteOperacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2001,'CL','IR-IRFonteDayTrade')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2002,'CL','IR-IRRendaVariavel')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3000,'FU','Margem-ChamadaMargem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3001,'FU','Margem-DevolucaoMargem')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,5000,'CL','AjusteCompensacaoCota')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,100000,'MT','Outros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,1,'A','Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,2,'R','ResgateBruto')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,3,'R','ResgateLiquido')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,4,'R','ResgateCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,5,'R','ResgateTotal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,10,'A','AplicacaoCotasEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,11,'A','AplicacaoAcoesEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,12,'R','ResgateCotasEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,20,'','ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,40,'','AjustePosicao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,80,'','Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,82,'','Juros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,84,'','AmortizacaoJuros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,100,'','IncorporacaoResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,101,'','IncorporacaoAplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,1,'RF','CurtoPrazo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,2,'RF','LongoPrazo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,3,'RV','Acoes')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,4,'RF','CPrazo_SemComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,5,'RF','LPrazo_SemComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,20,'Isento','Isento')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-8,1,'RF','Renda Fixa')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-8,2,'RV','Renda Variável')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,1,'C','CompraFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,2,'V','VendaFinal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,3,'C','CompraRevenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,4,'V','VendaRecompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,6,'V','VendaTotal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,10,'C','CompraCasada')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,11,'V','VendaCasada')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,12,'V','AntecipacaoRevenda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,13,'C','AntecipacaoRecompra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,20,'D','Deposito')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,21,'R','Retirada')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,22,'D','IngressoAtivoImpactoQtde')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,23,'D','IngressoAtivoImpactoCota')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,24,'R','RetiradaAtivoImpactoQtde')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,25,'R','RetiradaAtivoImpactoCota')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-10,1,'C','Parte')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-10,2,'V','ContraParte')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'OPV','P','Opção de Venda')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'OPC','O','Opção de Compra')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'VIS','V','Papel a Vista')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-12,1,'D','Doador')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-12,2,'T','Tomador')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-13,3,'','OpcaoDisponivel')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-13,4,'','OpcaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-14,1,'E','Exponencial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-14,2,'L','Linear')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-15,1,'U','Uteis')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-15,2,'C','Corridos')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,1,'OperacaoBolsa','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,2,'OperacaoRendaFixa','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,3,'OperacaoFundos','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,4,'ContaCorrente','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,5,'OperacaoBMF','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,6,'OperacaoSwap','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,7,'Fundo','')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,1,'A','Aplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,2,'RB ','ResgateBruto')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,3,'RL ','ResgateLiquido')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,4,'RC ','ResgateCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,5,'RT ','ResgateTotal')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,10,'não mapeada','AplicacaoCotasEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,11,'não mapeada','AplicacaoAcoesEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,12,'não mapeada','ResgateCotasEspecial')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,20,'RL','ComeCotas')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,80,'não mapeada','Amortizacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,82,'não mapeada','Juros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,84,'não mapeada','AmortizacaoJuros')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,100,'não mapeada','IncorporacaoResgate')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,101,'não mapeada','IncorporacaoAplicacao')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,2,'NB','Resgate por NOTA - Valor Bruto')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,3,'NL','Resgate por NOTA -  Valor Líquido')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,5,'NT','Resgate por NOTA - Total')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,1,'não existe','Especifico')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,2,'F','FIFO')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,3,'não existe','LIFO')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,4,'J','MenorImposto')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,1,'DIS','Disponivel')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,2,'FUT','Futuro')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,3,'OPD','OpcaoDisponivel')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,4,'OPF','OpcaoFuturo')
		insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,5,'TER','Termo')


		set identity_insert tipodepara off
			
	END
END


COMMIT TRANSACTION DeParaTransaction;
GO









