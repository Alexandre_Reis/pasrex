declare @data_versao char(10)
set @data_versao = '2013-04-13'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

update contabconta set Codigo = SUBSTRING(Codigo, 0, 20)

alter table contabconta alter column Codigo varchar(20) null
go
alter table contabroteiro alter column ContaDebitoReduzida varchar(8) null
go
alter table contabroteiro alter column ContaCreditoReduzida varchar(8) null
go


