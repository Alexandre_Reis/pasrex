﻿declare @data_versao char(10)
set @data_versao = '2016-03-29'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

if (not exists (select * from sys.all_columns where name = 'InfluenciaGestorLocalCvm' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  InfluenciaGestorLocalCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'PapelRendaFixa'))
	begin
		ALTER TABLE PapelRendaFixa ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'AtivoBMF'))
	begin
		ALTER TABLE AtivoBMF ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'AtivoBolsa'))
	begin
		ALTER TABLE AtivoBolsa ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off



