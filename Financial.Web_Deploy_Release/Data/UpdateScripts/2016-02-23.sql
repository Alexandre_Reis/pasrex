﻿declare @data_versao char(10)
set @data_versao = '2016-02-23'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

/****** Criação dos campos de cadastro complementar  ******/

begin transaction 


 -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on
    
    insert into VersaoSchema values(@data_versao)

--Apaga o conteúdo do campo de CarteiraFundo
DELETE FROM CadastroComplementar
where  exists (select 'X' 
                from CadastroComplementarCampos A
				 where a.IdCamposComplementares = CadastroComplementar.IdCamposComplementares 
				       and a.NomeCampo in ('NomeFundo') 
					   and a.TipoCadastro = -8 --CarteiraFundo
			   )

-- Apaga a definição do Campo de CarteiraFundo
delete CadastroComplementarCampos 
 where TipoCadastro = -8 AND --CarteiraFundo
       NomeCampo in ('NomeFundo')

-- Cria um novo campo de CarteiraFundo
INSERT INTO [CadastroComplementarCampos] 
   ([TipoCadastro],[NomeCampo] ,[DescricaoCampo] ,[ValorDefault] ,[TipoCampo] ,[CampoObrigatorio],[Tamanho]  ,[CasasDecimais]) 
 VALUES  
   (-8 , 'NomeFundo','Nome Fundo' ,NULL ,-2 ,'N' ,50  ,0) 

-- INSERE Conteúdo default nos campos criados
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'NomeFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(30),Carteira.Apelido), 
           Convert(Varchar(30),Carteira.Nome)  -- Preenche o nome reduzido com os primeiros 10 caracteres do Apelido
	 from Carteira
	 -- where Carteira.idCarteira in (743259, 746295, 736745, 736744)  --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FC

commit transaction
go

