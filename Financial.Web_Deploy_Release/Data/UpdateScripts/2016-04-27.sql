declare @data_versao char(10)
set @data_versao = '2016-04-27'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on	
	
	insert into VersaoSchema values(@data_versao)

	IF EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'RecursosServicosGestor' AND ID = OBJECT_ID('InformacoesComplementaresFundo'))
	BEGIN
		alter table InformacoesComplementaresFundo alter column RecursosServicosGestor VarChar(150) NULL
	END
	
	IF EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'PrestadoresServicos' AND ID = OBJECT_ID('InformacoesComplementaresFundo'))
	BEGIN
		alter table InformacoesComplementaresFundo alter column PrestadoresServicos VarChar(150) NULL
	END
	
	

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off