declare @data_versao char(10)
set @data_versao = '2014-04-29'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table pessoa add PessoaVinculada char(1) null
go

update pessoa set pessoavinculada = b.pessoavinculada
from pessoa p, clientebolsa b
where p.idpessoa = b.idcliente
go

alter table clientebolsa drop constraint DF_ClienteBolsa_PessoaVinculada
go

alter table clientebolsa drop column PessoaVinculada
go

ALTER TABLE Pessoa ADD  CONSTRAINT DF_Pessoa_PessoaVinculada  DEFAULT ('N') FOR PessoaVinculada
GO

alter table ordemcotista add StatusIntegracao tinyint null
go