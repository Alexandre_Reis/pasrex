declare @data_versao char(10)
set @data_versao = '2014-06-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


ALTER TABLE cotacaoindice ALTER COLUMN FatorDiario decimal(28,18) NULL
ALTER TABLE cotacaoindice ALTER COLUMN FatorAcumulado decimal(28,18) NULL
go