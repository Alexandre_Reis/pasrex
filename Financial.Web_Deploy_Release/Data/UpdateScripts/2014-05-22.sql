declare @data_versao char(10)
set @data_versao = '2014-05-22'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


CREATE TABLE IndicadoresCarteira
	(
	Data datetime NOT NULL,
	IdCarteira int NOT NULL,
	RetornoDia decimal(20, 12) NULL,
	RetornoMes decimal(20, 12) NULL,
	RetornoAno decimal(20, 12) NULL,
	Volatilidade decimal(20, 12) NULL
	)  ON [PRIMARY]

GO

ALTER TABLE IndicadoresCarteira ADD CONSTRAINT
	PK_IndicadoresCarteira PRIMARY KEY CLUSTERED (Data,	IdCarteira) 

GO