declare @data_versao char(10)
set @data_versao = '2013-11-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

update PosicaoRendaFixaHistorico set ValorVolta = o.valorvolta
from PosicaoRendaFixaHistorico p, OperacaoRendaFixa o
where p.IdCliente = o.IdCliente
and p.DataOperacao = o.DataOperacao
and p.PUVolta = o.PUVolta
and p.DataVolta = o.DataVolta
and p.IdTitulo = o.IdTitulo
and p.TaxaVolta = o.TaxaVolta
and p.ValorVolta is null
and p.TipoOperacao = o.TipoOperacao
and p.TipoOperacao = 3
go
update PosicaoRendaFixa set ValorVolta = o.valorvolta
from PosicaoRendaFixa p, OperacaoRendaFixa o
where p.IdCliente = o.IdCliente
and p.DataOperacao = o.DataOperacao
and p.PUVolta = o.PUVolta
and p.DataVolta = o.DataVolta
and p.IdTitulo = o.IdTitulo
and p.TaxaVolta = o.TaxaVolta
and p.ValorVolta is null
and p.TipoOperacao = o.TipoOperacao
and p.TipoOperacao = 3
go

