declare @data_versao char(10)
set @data_versao = '2014-06-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


BEGIN TRANSACTION
GO
ALTER TABLE dbo.Carteira ADD
	OrdemRelatorio int NULL
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.GrupoEconomico ADD
	OrdemRelatorio int NULL
GO
COMMIT