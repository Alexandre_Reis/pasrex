declare @data_versao char(10)
set @data_versao = '2014-08-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


ALTER TABLE dbo.Cotista ADD
	IdClienteEspelho int NULL
GO
