declare @data_versao char(10)
set @data_versao = '2015-05-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarTipoLista') = 0
Begin
 CREATE TABLE CadastroComplementarTipoLista
 (  
   IdTipoLista INT PRIMARY KEY identity NOT NULL, 
   NomeLista varchar(50),
   TipoLista int NOT NULL,   
   CONSTRAINT TipoUnico UNIQUE (NomeLista,TipoLista)
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarItemLista') = 0
Begin
 CREATE TABLE CadastroComplementarItemLista
 (  
   IdLista INT PRIMARY KEY identity NOT NULL, 
   IdTipoLista int FOREIGN KEY REFERENCES CadastroComplementarTipoLista(IdTipoLista) NOT NULL,
   ItemLista varchar(50) NOT NULL,   
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarCampos') = 0
Begin
 CREATE TABLE CadastroComplementarCampos
 (  
   IdCamposComplementares INT  PRIMARY KEY identity NOT NULL, 
   TipoCadastro int           NOT NULL, 
   NomeCampo varchar(50)      NOT NULL, 
   DescricaoCampo varchar(50) NULL, 
   ValorDefault varchar(20)   NULL,
   TipoCampo int              NOT NULL,
   CampoObrigatorio Char      NOT NULL,
   Tamanho int                NULL,
   CasasDecimais int          NULL
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementar') = 0
Begin
 CREATE TABLE CadastroComplementar
 (  
   IdCadastroComplementares INT PRIMARY KEY identity NOT NULL, 
   IdCamposComplementares int FOREIGN KEY REFERENCES CadastroComplementarCampos(IdCamposComplementares),
   IdMercadoTipoPessoa int NOT NULL,
   DescricaoMercadoTipoPessoa varchar(50)    NOT NULL, 
   ValorCampo varchar(50)            NOT NULL
 ) 
End
GO

delete from permissaoMenu where idMenu in (480, 490, 500, 510, 520)
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,480
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,490
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,500
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,510
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,520
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END	
GO