﻿declare @data_versao char(10)
set @data_versao = '2015-10-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION

	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
	Begin
		CREATE TABLE [dbo].[OperacaoCotistaAux]
		(
			[IdOperacaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
			[IdOperacao] [int] NOT NULL,
			[IdCotista] [int] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[DataOperacao] [datetime] NOT NULL,
			[DataConversao] [datetime] NOT NULL,
			[DataLiquidacao] [datetime] NOT NULL,
			[DataAgendamento] [datetime] NOT NULL,
			[TipoOperacao] [tinyint] NOT NULL,
			[TipoResgate] [tinyint] NULL,
			[IdPosicaoResgatada] [int] NULL,
			[IdFormaLiquidacao] [tinyint] NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[CotaOperacao] [decimal](28, 12) NOT NULL,
			[ValorBruto] [decimal](16, 2) NOT NULL,
			[ValorLiquido] [decimal](16, 2) NOT NULL,
			[ValorIR] [decimal](16, 2) NOT NULL,
			[ValorIOF] [decimal](16, 2) NOT NULL,
			[ValorCPMF] [decimal](16, 2) NOT NULL,
			[ValorPerformance] [decimal](16, 2) NOT NULL,
			[PrejuizoUsado] [decimal](16, 2) NOT NULL,
			[RendimentoResgate] [decimal](16, 2) NOT NULL,
			[VariacaoResgate] [decimal](16, 2) NOT NULL,
			[Observacao] [varchar](400) NOT NULL,
			[DadosBancarios] [varchar](500) NULL,
			[Fonte] [tinyint] NOT NULL,
			[IdConta] [int] NULL,
			[CotaInformada] [decimal](28, 12) NULL,
			[IdAgenda] [int] NULL,
			[IdOperacaoResgatada] [int] NULL,
			[IdOperacaoAuxiliar] [int] NULL,
			[IdOrdem] [int] NULL
		)
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
	Begin
		CREATE TABLE [dbo].[DetalheResgateCotistaAux]
		(
			[IdOperacao] [int] NOT NULL,
			[IdPosicaoResgatada] [int] NOT NULL,
			[IdCotista] [int] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[ValorBruto] [decimal](16, 2) NOT NULL,
			[ValorLiquido] [decimal](16, 2) NOT NULL,
			[ValorCPMF] [decimal](16, 2) NOT NULL,
			[ValorPerformance] [decimal](16, 2) NOT NULL,
			[PrejuizoUsado] [decimal](16, 2) NOT NULL,
			[RendimentoResgate] [decimal](16, 2) NOT NULL,
			[VariacaoResgate] [decimal](16, 2) NOT NULL,
			[ValorIR] [decimal](16, 2) NOT NULL,
			[ValorIOF] [decimal](16, 2) NOT NULL,
			[DataOperacao] [datetime] NOT NULL,
			[TipoOperacao] [TinyInt] NOT NULL,
		 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
		(
			[IdOperacao] ASC,
			[IdPosicaoResgatada] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]
	END
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
	Begin
		CREATE TABLE [dbo].[PosicaoCotistaAux]
		(
			[IdPosicaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
			[DataReferencia] [datetime] NOT NULL,
			[IdPosicao] [int] NOT NULL,
			[IdOperacao] [int] NULL,
			[IdCotista] [int] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[ValorAplicacao] [decimal](16, 2) NOT NULL,
			[DataAplicacao] [datetime] NOT NULL,
			[DataConversao] [datetime] NOT NULL,
			[CotaAplicacao] [decimal](28, 12) NOT NULL,
			[CotaDia] [decimal](28, 12) NOT NULL,
			[ValorBruto] [decimal](16, 2) NOT NULL,
			[ValorLiquido] [decimal](16, 2) NOT NULL,
			[QuantidadeInicial] [decimal](28, 12) NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
			[DataUltimaCobrancaIR] [datetime] NOT NULL,
			[ValorIR] [decimal](16, 2) NOT NULL,
			[ValorIOF] [decimal](16, 2) NOT NULL,
			[ValorPerformance] [decimal](16, 2) NOT NULL,
			[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
			[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
			[ValorRendimento] [decimal](16, 2) NOT NULL,
			[DataUltimoCortePfee] [datetime] NULL,
			[PosicaoIncorporada] [char](1) NOT NULL
		)
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
	Begin
		CREATE TABLE [dbo].[OperacaoFundoAux](
			[IdOperacaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
			[IdOperacao] [int] NOT NULL,
			[IdCliente] [int] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[DataOperacao] [datetime] NOT NULL,
			[DataConversao] [datetime] NOT NULL,
			[DataLiquidacao] [datetime] NOT NULL,
			[DataAgendamento] [datetime] NOT NULL,
			[TipoOperacao] [tinyint] NOT NULL,
			[TipoResgate] [tinyint] NULL,
			[IdPosicaoResgatada] [int] NULL,
			[IdFormaLiquidacao] [tinyint] NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[CotaOperacao] [decimal](28, 12) NOT NULL,
			[ValorBruto] [decimal](16, 2) NOT NULL,
			[ValorLiquido] [decimal](16, 2) NOT NULL,
			[ValorIR] [decimal](16, 2) NOT NULL,
			[ValorIOF] [decimal](16, 2) NOT NULL,
			[ValorCPMF] [decimal](16, 2) NOT NULL,
			[ValorPerformance] [decimal](16, 2) NOT NULL,
			[PrejuizoUsado] [decimal](16, 2) NOT NULL,
			[RendimentoResgate] [decimal](16, 2) NOT NULL,
			[VariacaoResgate] [decimal](16, 2) NOT NULL,
			[IdConta] [int] NULL,
			[Observacao] [varchar](400) NOT NULL,
			[Fonte] [tinyint] NOT NULL,
			[IdAgenda] [int] NULL,
			[CotaInformada] [decimal](28, 12) NULL,
			[IdOperacaoResgatada] [int] NULL
		)
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
	Begin	
		CREATE TABLE [dbo].[PosicaoFundoAux]
		(
			[IdPosicaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
			[DataReferencia] [datetime] NOT NULL,
			[IdPosicao] [int] NOT NULL,
			[IdOperacao] [int] NULL,
			[IdCliente] [int] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[ValorAplicacao] [decimal](16, 2) NOT NULL,
			[DataAplicacao] [datetime] NOT NULL,
			[DataConversao] [datetime] NOT NULL,
			[CotaAplicacao] [decimal](28, 12) NOT NULL,
			[CotaDia] [decimal](28, 12) NOT NULL,
			[ValorBruto] [decimal](16, 2) NOT NULL,
			[ValorLiquido] [decimal](16, 2) NOT NULL,
			[QuantidadeInicial] [decimal](28, 12) NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[QuantidadeBloqueada] [decimal](28, 12) NULL,
			[DataUltimaCobrancaIR] [datetime] NOT NULL,
			[ValorIR] [decimal](16, 2) NOT NULL,
			[ValorIOF] [decimal](16, 2) NOT NULL,
			[ValorPerformance] [decimal](16, 2) NOT NULL,
			[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
			[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
			[ValorRendimento] [decimal](16, 2) NOT NULL,
			[DataUltimoCortePfee] [datetime] NULL,
			[PosicaoIncorporada] [char](1) NOT NULL
		)
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
	Begin
	CREATE TABLE [dbo].[DetalheResgateFundoAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [TinyInt] NOT NULL,
		CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
		(
			[IdOperacao] ASC,
			[IdPosicaoResgatada] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]
	END
	GO

	if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
	BEGIN
		ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
	END
	GO
		
	if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
	BEGIN
		ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
	END
	GO
		
	if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
	BEGIN
		ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
	END
	GO
		
	if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
	BEGIN
		ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
	END
	GO	

	if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
	BEGIN
		ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
	END	

	if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
	BEGIN
		ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
	END	

	if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
	BEGIN
		ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
	END	

	if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
	BEGIN
		ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
	END	

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotas]
		(
			IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
			TipoRegistro char(1) NOT NULL ,
			CodigoExterno char(15) NOT NULL ,
			DataReferencia [datetime] NOT NULL,
			DataOperacao [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			Quantidade [decimal](28, 12) NOT NULL,
			CotaOperacao [decimal](28, 12) NOT NULL,
			ValorBruto [decimal](28, 12) NOT NULL,
			ValorLiquido [decimal](28, 12) NOT NULL,
			ValorCPMF [decimal](28, 12) NOT NULL,
			ValorPerformance [decimal](28, 12) NOT NULL,
			PrejuizoUsado [decimal](28, 12) NOT NULL,
			RendimentoComeCotas [decimal](28, 12) NOT NULL,
			VariacaoComeCotas [decimal](28, 12) NOT NULL,
			ValorIR [decimal](28, 12) NOT NULL,
			ValorIOF [decimal](28, 12) NOT NULL
		)	
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
		(
			IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
			CodigoExterno char(15) NOT NULL ,
			DataReferencia [datetime] NOT NULL,
			DataOperacao [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			Quantidade [decimal](28, 12) NOT NULL,
			CotaOperacao [decimal](28, 12) NOT NULL,
			ValorBruto [decimal](28, 12) NOT NULL,
			ValorLiquido [decimal](28, 12) NOT NULL,
			ValorCPMF [decimal](28, 12) NOT NULL,
			ValorPerformance [decimal](28, 12) NOT NULL,
			PrejuizoUsado [decimal](28, 12) NOT NULL,
			RendimentoComeCotas [decimal](28, 12) NOT NULL,
			VariacaoComeCotas [decimal](28, 12) NOT NULL,
			ValorIR [decimal](28, 12) NOT NULL,
			ValorIOF [decimal](28, 12) NOT NULL,
		)	
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
		(
			IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
			IdColagemComeCotasDetalhe [int] NOT NULL,
			DataReferencia [datetime] NOT NULL,
			QuantidadeInicial [decimal](28, 12) NOT NULL,
			QuantidadeRestante [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
		(
			IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
			DataReferencia [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			DetalheImportado char(1) NOT NULL DEFAULT 'N',
			IdOperacaoComeCotasVinculada  [int] NOT NULL,	
			Participacao [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
		(
			IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
			IdCarteira  				[int] NOT NULL,
			IdCliente  					[int] NOT NULL,
			DataConversao 				[datetime] NULL,
			DataReferencia 				[datetime] NOT NULL,
			IdPosicaoVinculada  		[int] NOT NULL,
			PrejuizoUsado 				[decimal](28, 12) NOT NULL,
			RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
			ValorBruto 					[decimal](28, 12) NOT NULL,
			ValorCPMF 					[decimal](28, 12) NOT NULL,
			ValorIOF 					[decimal](28, 12) NOT NULL,
			ValorIR 					[decimal](28, 12) NOT NULL,
			ValorLiquido 				[decimal](28, 12) NOT NULL,
			ValorPerformance 			[decimal](28, 12) NOT NULL,
			VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
			Quantidade					[decimal](28, 12) NOT NULL,
			Participacao				[decimal](28, 12) NOT NULL,
			DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
		)		
	END	
	GO


	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
	Begin
		CREATE TABLE [dbo].[ColagemResgateMapaFundo]
		(
			IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
			DataReferencia [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			DetalheImportado char(1) NOT NULL DEFAULT 'N',
			IdOperacaoResgateVinculada  [int] NOT NULL,	
			Participacao [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
	Begin
		CREATE TABLE [dbo].[ColagemOperResgPosFundo]
		(
			IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
			IdCarteira  [int] NOT NULL,
			IdCliente  [int] NOT NULL,
			DataReferencia [datetime] NOT NULL,
			IdColagemResgateDetalhe  [int] NOT NULL,
			IdOperacaoResgateVinculada  [int] NOT NULL,
			IdPosicaoVinculada  [int] NOT NULL,
			QuantidadeVinculada [decimal](28, 12) NOT NULL
		)		
	END	
	GO


	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
	Begin
		CREATE TABLE [dbo].[ColagemResgate]
		(
			IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
			TipoRegistro char(1) NOT NULL ,
			CodigoExterno char(15) NOT NULL ,
			DataReferencia [datetime] NOT NULL,
			DataOperacao [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			Quantidade [decimal](28, 12) NOT NULL,
			CotaOperacao [decimal](28, 12) NOT NULL,
			ValorBruto [decimal](28, 12) NOT NULL,
			ValorLiquido [decimal](28, 12) NOT NULL,
			ValorCPMF [decimal](28, 12) NOT NULL,
			ValorPerformance [decimal](28, 12) NOT NULL,
			PrejuizoUsado [decimal](28, 12) NOT NULL,
			RendimentoResgate [decimal](28, 12) NOT NULL,
			VariacaoResgate [decimal](28, 12) NOT NULL,
			ValorIR [decimal](28, 12) NOT NULL,
			ValorIOF [decimal](28, 12) NOT NULL
		)	
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
	Begin
		CREATE TABLE [dbo].[ColagemResgateDetalhe]
		(
			IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
			CodigoExterno char(15) NOT NULL ,
			DataReferencia [datetime] NOT NULL,
			DataOperacao [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			Quantidade [decimal](28, 12) NOT NULL,
			CotaOperacao [decimal](28, 12) NOT NULL,
			ValorBruto [decimal](28, 12) NOT NULL,
			ValorLiquido [decimal](28, 12) NOT NULL,
			ValorCPMF [decimal](28, 12) NOT NULL,
			ValorPerformance [decimal](28, 12) NOT NULL,
			PrejuizoUsado [decimal](28, 12) NOT NULL,
			RendimentoResgate [decimal](28, 12) NOT NULL,
			VariacaoResgate [decimal](28, 12) NOT NULL,
			ValorIR [decimal](28, 12) NOT NULL,
			ValorIOF [decimal](28, 12) NOT NULL,
		)	
	END	
	GO

	 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
	Begin
		CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
		(
			IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
			IdColagemResgateDetalhe [int] NOT NULL,
			DataReferencia [datetime] NOT NULL,
			QuantidadeInicial [decimal](28, 12) NOT NULL,
			QuantidadeRestante [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
	Begin
		CREATE TABLE [dbo].[ColagemResgateMapaCotista]
		(
			IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
			DataReferencia [datetime] NOT NULL,
			IdCliente  [int] NOT NULL,
			IdCarteira  [int] NOT NULL,
			DataConversao [datetime] NULL,
			DetalheImportado char(1) NOT NULL DEFAULT 'N',
			IdOperacaoResgateVinculada  [int] NOT NULL,	
			Participacao [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
	Begin
		CREATE TABLE [dbo].[ColagemOperResgPosCotista]
		(
			IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
			IdCarteira  [int] NOT NULL,
			IdCliente  [int] NOT NULL,
			DataReferencia [datetime] NOT NULL,
			IdColagemResgateDetalhe  [int] NOT NULL,
			IdOperacaoResgateVinculada  [int] NOT NULL,
			IdPosicaoVinculada  [int] NOT NULL,
			QuantidadeVinculada [decimal](28, 12) NOT NULL
		)		
	END	
	GO

	if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
	END
	GO

	if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
	END
	GO

	if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
	END
	GO

	if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
	END
	GO

	if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
	END
	GO

	if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
	END
	GO


	if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
	END
	GO

	if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
	END
	GO

	if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
	BEGIN	
		ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
	END
	GO		

	if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
	BEGIN	
		ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
	END
	GO		

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
	Begin
		CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
		(
			IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
			IdCarteira  				[int] NOT NULL,
			IdCliente  					[int] NOT NULL,
			DataConversao 				[datetime] NULL,
			DataReferencia 				[datetime] NOT NULL,
			IdPosicaoVinculada  		[int] NOT NULL,
			PrejuizoUsado 				[decimal](28, 12) NOT NULL,
			RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
			ValorBruto 					[decimal](28, 12) NOT NULL,
			ValorCPMF 					[decimal](28, 12) NOT NULL,
			ValorIOF 					[decimal](28, 12) NOT NULL,
			ValorIR 					[decimal](28, 12) NOT NULL,
			ValorLiquido 				[decimal](28, 12) NOT NULL,
			ValorPerformance 			[decimal](28, 12) NOT NULL,
			VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
			Quantidade					[decimal](28, 12) NOT NULL,
			Participacao				[decimal](28, 12) NOT NULL,
			DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
		)		
	END	
	GO 

	UPDATE InformacoesComplementaresFundo SET Periodicidade = SUBSTRING(Periodicidade,1, 250);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN Periodicidade varchar(250) null;

	UPDATE InformacoesComplementaresFundo SET LocalFormaDivulgacao = SUBSTRING(LocalFormaDivulgacao,1, 300);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaDivulgacao varchar(300) null;

	UPDATE InformacoesComplementaresFundo SET LocalFormaSolicitacaoCotista = SUBSTRING(LocalFormaSolicitacaoCotista,1, 250);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaSolicitacaoCotista varchar(250) null;


	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN FatoresRisco varchar(2000) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaExercicioVoto varchar(1000) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN TributacaoAplicavel varchar(2500) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaAdministracaoRisco varchar(2500) null;

	UPDATE InformacoesComplementaresFundo SET AgenciaClassificacaoRisco = SUBSTRING(AgenciaClassificacaoRisco,1, 50);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN AgenciaClassificacaoRisco varchar(50) null;

	UPDATE InformacoesComplementaresFundo SET RecursosServicosGestor = SUBSTRING(RecursosServicosGestor,1, 100);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN RecursosServicosGestor varchar(100) null;

	UPDATE InformacoesComplementaresFundo SET PrestadoresServicos = SUBSTRING(PrestadoresServicos,1, 100);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PrestadoresServicos varchar(100) null;		

	
	ALTER TABLE SuitabilityTipoDispensa ALTER COLUMN Descricao VARCHAR(500) NOT NULL
	ALTER TABLE SuitabilityTipoDispensaHistorico ALTER COLUMN Descricao VARCHAR(500) NOT NULL
	
COMMIT TRANSACTION

GO	



