﻿declare @data_versao char(10)
set @data_versao = '2015-07-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION
BEGIN
	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);
	
	--apaga qualquer lixo na base(assumindo que não existam estes ids)
	delete TipoDePara where IdTipoDePara in (-25,-26)
	
	--insere os tiposdepara necessarios
	set identity_insert TipoDePara on
	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-25,1,'TipoRegraEnquadra','Tipo Regra Enquadramento',	null)		
	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-26,1,'TipoEnquadramentoEnquadra','Tipo de enquadramento',null)
	set identity_insert TipoDePara off
	

	--insere os depara
	delete DePara where IdTipoDepara in( -25,-26)
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	100	,'Limite Absoluto',	'Limite Absoluto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	200	,'Concentracao Cotista'	,'Concentracao Cotista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	300	,'Numero Cotista',	'Numero Cotista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	400	,'Posicao Descoberto',	'Posicao Descoberto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	500	,'Media Movel',	'Media Movel')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	600	,'Concentracao',	'Concentracao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	800	,'Limite Oscilacao',	'Limite Oscilacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	810	,'Limite DesvioMensal',	'Limite DesvioMensal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	811	,'Limite DesvioAnual',	'Limite DesvioAnual')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-25,	900	,'Participacao Fundo',	'Participacao Fundo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-26,	1	,'Legal',	'Legal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem)	values(-26,	2	,'Gerencial',	'Gerencial')
END
COMMIT TRANSACTION

GO	



