declare @data_versao char(10)
set @data_versao = '2016-02-29'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

/****** Criação dos campos de cadastro complementar  ******/

begin transaction 


 -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on
    
    insert into VersaoSchema values(@data_versao)

	if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina') > 0
	begin
		UPDATE CadastroComplementarCampos SET DescricaoCampo = 'Nome do fundo que será apresentado na lâmina' where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina'
	END	
	
commit transaction