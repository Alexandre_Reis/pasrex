﻿declare @data_versao char(10)
set @data_versao = '2016-03-02'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	--DDL
	if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo')
	begin
		ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(500) NOT NULL
	END
	--

	--DML
	update CadastroComplementar
	set ValorCampo = 'Proporcionar rentabilidade aos seus cotistas através de oportunidades oferecidas pelos mercados domésticos de taxas de juros pós-fixadas, prefixadas e de índices de preços.'
	where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'objetivo');

	update CadastroComplementar
	set ValorCampo = 'Destina-se a receber aplicações de recursos provenientes de investidores pessoas físicas e jurídicas em geral, com perfil de risco moderado.'
	where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'Publico Alvo');

	update CadastroComplementar
	set ValorCampo = 'O fundo pretende atingir seu objetivo investindo no mínimo 80% de seu patrimônio em títulos públicos e/ou privados de renda fixa pré ou pós fixadas de médio e longo prazo, sendo permitido o uso de derivativos à taxa de juros doméstica e/ou índices de preços, com objetivo exclusivo de proteger a carteira do fundo.'
	where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'Politica de Investimento');

	update CadastroComplementarCampos set DescricaoCampo = 'Define o público alvo do fundo' where TipoCadastro = -8 and nomecampo = 'Publico Alvo';
	update CadastroComplementarCampos set DescricaoCampo = 'Define a Política de Investimento do Fundo' where TipoCadastro = -8 and nomecampo = 'Politica de Investimento';	
	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off



