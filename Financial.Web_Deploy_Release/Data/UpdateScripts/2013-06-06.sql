declare @data_versao char(10)
set @data_versao = '2013-06-06'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Legais/Contabil/BalanceteContabil
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,25320,'N','S','S','S' from grupousuario where idgrupo <> 0
