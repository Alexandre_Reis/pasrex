﻿declare @data_versao char(10)
set @data_versao = '2013-06-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Legais >> DARF
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,25250,'N','S','S','S' from grupousuario where idgrupo <> 0

--Menus de Análise Agrupada de Gestão
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,16800,'N','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,16805,'N','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,16810,'N','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,16815,'N','S','S','S' from grupousuario where idgrupo <> 0


--Menus de Análise Agrupada de Captação
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,18700,'N','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,18705,'N','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,18710,'N','S','S','S' from grupousuario where idgrupo <> 0

