IF EXISTS(SELECT 1 FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'RetornaPosicaoCustodia')) 
drop Procedure RetornaPosicaoCustodia
go

Create Procedure RetornaPosicaoCustodia (@IdCustodia int = null, @IdCliente int = null)

AS
	BEGIN
		SELECT P.[IdCliente] AS 'IdCliente',
			AVG(P.[PUMercado]) AS 'PUMercado',
			E.[Nome] AS 'NomeEmissor',
			T.[Descricao] AS 'Papel',
			SUM(P.[Quantidade]) AS 'Quantidade',
			SUM(P.[ValorMercado]) AS 'Valor'  

		FROM [PosicaoRendaFixa] P 
	
		INNER JOIN [TituloRendaFixa] T ON T.[IdTitulo] = P.[IdTitulo] 
		INNER JOIN [Emissor] E ON T.[IdEmissor] = E.[IdEmissor] 
	
		WHERE
			(@IdCustodia IS NULL OR (P.[IdCustodia] = @IdCustodia))
            AND (@IdCliente  IS NULL OR (P.[IdCliente]  = @IdCliente ))
	
		
		GROUP BY P.[IdCliente],T.[Descricao],E.[Nome]
	END
go


